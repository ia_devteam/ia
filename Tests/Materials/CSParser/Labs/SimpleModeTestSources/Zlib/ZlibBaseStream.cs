using System;
using System.IO;
namespace Ionic.Zlib
{
  internal enum ZlibStreamFlavor
  {
    ZLIB = 1950,
    DEFLATE = 1951,
    GZIP = 1952
  }
  internal class ZlibBaseStream : System.IO.Stream
  {
    protected internal ZlibCodec _z = null;
    protected internal StreamMode _streamMode = StreamMode.Undefined;
    protected internal FlushType _flushMode;
    protected internal ZlibStreamFlavor _flavor;
    protected internal CompressionMode _compressionMode;
    protected internal CompressionLevel _level;
    protected internal bool _leaveOpen;
    protected internal byte[] _workingBuffer;
    protected internal int _bufferSize = ZlibConstants.WorkingBufferSizeDefault;
    protected internal byte[] _buf1 = new byte[1];
    protected internal System.IO.Stream _stream;
    protected internal CompressionStrategy Strategy = CompressionStrategy.Default;
    Ionic.Crc.CRC32 crc;
    protected internal string _GzipFileName;
    protected internal string _GzipComment;
    protected internal DateTime _GzipMtime;
    protected internal int _gzipHeaderByteCount;
    internal int Crc32 {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6014);
        if (crc == null) {
          System.Int32 RNTRNTRNT_682 = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(6015);
          return RNTRNTRNT_682;
        }
        System.Int32 RNTRNTRNT_683 = crc.Crc32Result;
        IACSharpSensor.IACSharpSensor.SensorReached(6016);
        return RNTRNTRNT_683;
      }
    }
    public ZlibBaseStream(System.IO.Stream stream, CompressionMode compressionMode, CompressionLevel level, ZlibStreamFlavor flavor, bool leaveOpen) : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6017);
      this._flushMode = FlushType.None;
      this._stream = stream;
      this._leaveOpen = leaveOpen;
      this._compressionMode = compressionMode;
      this._flavor = flavor;
      this._level = level;
      IACSharpSensor.IACSharpSensor.SensorReached(6018);
      if (flavor == ZlibStreamFlavor.GZIP) {
        IACSharpSensor.IACSharpSensor.SensorReached(6019);
        this.crc = new Ionic.Crc.CRC32();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6020);
    }
    protected internal bool _wantCompress {
      get {
        System.Boolean RNTRNTRNT_684 = (this._compressionMode == CompressionMode.Compress);
        IACSharpSensor.IACSharpSensor.SensorReached(6021);
        return RNTRNTRNT_684;
      }
    }
    private ZlibCodec z {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6022);
        if (_z == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(6023);
          bool wantRfc1950Header = (this._flavor == ZlibStreamFlavor.ZLIB);
          _z = new ZlibCodec();
          IACSharpSensor.IACSharpSensor.SensorReached(6024);
          if (this._compressionMode == CompressionMode.Decompress) {
            IACSharpSensor.IACSharpSensor.SensorReached(6025);
            _z.InitializeInflate(wantRfc1950Header);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(6026);
            _z.Strategy = Strategy;
            _z.InitializeDeflate(this._level, wantRfc1950Header);
          }
        }
        ZlibCodec RNTRNTRNT_685 = _z;
        IACSharpSensor.IACSharpSensor.SensorReached(6027);
        return RNTRNTRNT_685;
      }
    }
    private byte[] workingBuffer {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6028);
        if (_workingBuffer == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(6029);
          _workingBuffer = new byte[_bufferSize];
        }
        System.Byte[] RNTRNTRNT_686 = _workingBuffer;
        IACSharpSensor.IACSharpSensor.SensorReached(6030);
        return RNTRNTRNT_686;
      }
    }
    public override void Write(System.Byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6031);
      if (crc != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6032);
        crc.SlurpBlock(buffer, offset, count);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6033);
      if (_streamMode == StreamMode.Undefined) {
        IACSharpSensor.IACSharpSensor.SensorReached(6034);
        _streamMode = StreamMode.Writer;
      } else if (_streamMode != StreamMode.Writer) {
        IACSharpSensor.IACSharpSensor.SensorReached(6035);
        throw new ZlibException("Cannot Write after Reading.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6036);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6037);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6038);
      z.InputBuffer = buffer;
      _z.NextIn = offset;
      _z.AvailableBytesIn = count;
      bool done = false;
      IACSharpSensor.IACSharpSensor.SensorReached(6039);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(6040);
        _z.OutputBuffer = workingBuffer;
        _z.NextOut = 0;
        _z.AvailableBytesOut = _workingBuffer.Length;
        int rc = (_wantCompress) ? _z.Deflate(_flushMode) : _z.Inflate(_flushMode);
        IACSharpSensor.IACSharpSensor.SensorReached(6041);
        if (rc != ZlibConstants.Z_OK && rc != ZlibConstants.Z_STREAM_END) {
          IACSharpSensor.IACSharpSensor.SensorReached(6042);
          throw new ZlibException((_wantCompress ? "de" : "in") + "flating: " + _z.Message);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6043);
        _stream.Write(_workingBuffer, 0, _workingBuffer.Length - _z.AvailableBytesOut);
        done = _z.AvailableBytesIn == 0 && _z.AvailableBytesOut != 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6044);
        if (_flavor == ZlibStreamFlavor.GZIP && !_wantCompress) {
          IACSharpSensor.IACSharpSensor.SensorReached(6045);
          done = (_z.AvailableBytesIn == 8 && _z.AvailableBytesOut != 0);
        }
      } while (!done);
      IACSharpSensor.IACSharpSensor.SensorReached(6046);
    }
    private void finish()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6047);
      if (_z == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6048);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6049);
      if (_streamMode == StreamMode.Writer) {
        IACSharpSensor.IACSharpSensor.SensorReached(6050);
        bool done = false;
        IACSharpSensor.IACSharpSensor.SensorReached(6051);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(6052);
          _z.OutputBuffer = workingBuffer;
          _z.NextOut = 0;
          _z.AvailableBytesOut = _workingBuffer.Length;
          int rc = (_wantCompress) ? _z.Deflate(FlushType.Finish) : _z.Inflate(FlushType.Finish);
          IACSharpSensor.IACSharpSensor.SensorReached(6053);
          if (rc != ZlibConstants.Z_STREAM_END && rc != ZlibConstants.Z_OK) {
            IACSharpSensor.IACSharpSensor.SensorReached(6054);
            string verb = (_wantCompress ? "de" : "in") + "flating";
            IACSharpSensor.IACSharpSensor.SensorReached(6055);
            if (_z.Message == null) {
              IACSharpSensor.IACSharpSensor.SensorReached(6056);
              throw new ZlibException(String.Format("{0}: (rc = {1})", verb, rc));
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(6057);
              throw new ZlibException(verb + ": " + _z.Message);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(6058);
          if (_workingBuffer.Length - _z.AvailableBytesOut > 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6059);
            _stream.Write(_workingBuffer, 0, _workingBuffer.Length - _z.AvailableBytesOut);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(6060);
          done = _z.AvailableBytesIn == 0 && _z.AvailableBytesOut != 0;
          IACSharpSensor.IACSharpSensor.SensorReached(6061);
          if (_flavor == ZlibStreamFlavor.GZIP && !_wantCompress) {
            IACSharpSensor.IACSharpSensor.SensorReached(6062);
            done = (_z.AvailableBytesIn == 8 && _z.AvailableBytesOut != 0);
          }
        } while (!done);
        IACSharpSensor.IACSharpSensor.SensorReached(6063);
        Flush();
        IACSharpSensor.IACSharpSensor.SensorReached(6064);
        if (_flavor == ZlibStreamFlavor.GZIP) {
          IACSharpSensor.IACSharpSensor.SensorReached(6065);
          if (_wantCompress) {
            IACSharpSensor.IACSharpSensor.SensorReached(6066);
            int c1 = crc.Crc32Result;
            _stream.Write(BitConverter.GetBytes(c1), 0, 4);
            int c2 = (Int32)(crc.TotalBytesRead & 0xffffffffu);
            _stream.Write(BitConverter.GetBytes(c2), 0, 4);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(6067);
            throw new ZlibException("Writing with decompression is not supported.");
          }
        }
      } else if (_streamMode == StreamMode.Reader) {
        IACSharpSensor.IACSharpSensor.SensorReached(6068);
        if (_flavor == ZlibStreamFlavor.GZIP) {
          IACSharpSensor.IACSharpSensor.SensorReached(6069);
          if (!_wantCompress) {
            IACSharpSensor.IACSharpSensor.SensorReached(6070);
            if (_z.TotalBytesOut == 0L) {
              IACSharpSensor.IACSharpSensor.SensorReached(6071);
              return;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6072);
            byte[] trailer = new byte[8];
            IACSharpSensor.IACSharpSensor.SensorReached(6073);
            if (_z.AvailableBytesIn < 8) {
              IACSharpSensor.IACSharpSensor.SensorReached(6074);
              Array.Copy(_z.InputBuffer, _z.NextIn, trailer, 0, _z.AvailableBytesIn);
              int bytesNeeded = 8 - _z.AvailableBytesIn;
              int bytesRead = _stream.Read(trailer, _z.AvailableBytesIn, bytesNeeded);
              IACSharpSensor.IACSharpSensor.SensorReached(6075);
              if (bytesNeeded != bytesRead) {
                IACSharpSensor.IACSharpSensor.SensorReached(6076);
                throw new ZlibException(String.Format("Missing or incomplete GZIP trailer. Expected 8 bytes, got {0}.", _z.AvailableBytesIn + bytesRead));
              }
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(6077);
              Array.Copy(_z.InputBuffer, _z.NextIn, trailer, 0, trailer.Length);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6078);
            Int32 crc32_expected = BitConverter.ToInt32(trailer, 0);
            Int32 crc32_actual = crc.Crc32Result;
            Int32 isize_expected = BitConverter.ToInt32(trailer, 4);
            Int32 isize_actual = (Int32)(_z.TotalBytesOut & 0xffffffffu);
            IACSharpSensor.IACSharpSensor.SensorReached(6079);
            if (crc32_actual != crc32_expected) {
              IACSharpSensor.IACSharpSensor.SensorReached(6080);
              throw new ZlibException(String.Format("Bad CRC32 in GZIP trailer. (actual({0:X8})!=expected({1:X8}))", crc32_actual, crc32_expected));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6081);
            if (isize_actual != isize_expected) {
              IACSharpSensor.IACSharpSensor.SensorReached(6082);
              throw new ZlibException(String.Format("Bad size in GZIP trailer. (actual({0})!=expected({1}))", isize_actual, isize_expected));
            }
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(6083);
            throw new ZlibException("Reading with compression is not supported.");
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6084);
    }
    private void end()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6085);
      if (z == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6086);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6087);
      if (_wantCompress) {
        IACSharpSensor.IACSharpSensor.SensorReached(6088);
        _z.EndDeflate();
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(6089);
        _z.EndInflate();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6090);
      _z = null;
      IACSharpSensor.IACSharpSensor.SensorReached(6091);
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6092);
      if (_stream == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6093);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6094);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(6095);
        finish();
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(6096);
        end();
        IACSharpSensor.IACSharpSensor.SensorReached(6097);
        if (!_leaveOpen) {
          IACSharpSensor.IACSharpSensor.SensorReached(6098);
          _stream.Close();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6099);
        _stream = null;
        IACSharpSensor.IACSharpSensor.SensorReached(6100);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6101);
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6102);
      _stream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(6103);
    }
    public override System.Int64 Seek(System.Int64 offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6104);
      throw new NotImplementedException();
    }
    public override void SetLength(System.Int64 value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6105);
      _stream.SetLength(value);
      IACSharpSensor.IACSharpSensor.SensorReached(6106);
    }
    private bool nomoreinput = false;
    private string ReadZeroTerminatedString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6107);
      var list = new System.Collections.Generic.List<byte>();
      bool done = false;
      IACSharpSensor.IACSharpSensor.SensorReached(6108);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(6109);
        int n = _stream.Read(_buf1, 0, 1);
        IACSharpSensor.IACSharpSensor.SensorReached(6110);
        if (n != 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(6111);
          throw new ZlibException("Unexpected EOF reading GZIP header.");
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(6112);
          if (_buf1[0] == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6113);
            done = true;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(6114);
            list.Add(_buf1[0]);
          }
        }
      } while (!done);
      IACSharpSensor.IACSharpSensor.SensorReached(6115);
      byte[] a = list.ToArray();
      System.String RNTRNTRNT_687 = GZipStream.iso8859dash1.GetString(a, 0, a.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(6116);
      return RNTRNTRNT_687;
    }
    private int _ReadAndValidateGzipHeader()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6117);
      int totalBytesRead = 0;
      byte[] header = new byte[10];
      int n = _stream.Read(header, 0, header.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(6118);
      if (n == 0) {
        System.Int32 RNTRNTRNT_688 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6119);
        return RNTRNTRNT_688;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6120);
      if (n != 10) {
        IACSharpSensor.IACSharpSensor.SensorReached(6121);
        throw new ZlibException("Not a valid GZIP stream.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6122);
      if (header[0] != 0x1f || header[1] != 0x8b || header[2] != 8) {
        IACSharpSensor.IACSharpSensor.SensorReached(6123);
        throw new ZlibException("Bad GZIP header.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6124);
      Int32 timet = BitConverter.ToInt32(header, 4);
      _GzipMtime = GZipStream._unixEpoch.AddSeconds(timet);
      totalBytesRead += n;
      IACSharpSensor.IACSharpSensor.SensorReached(6125);
      if ((header[3] & 0x4) == 0x4) {
        IACSharpSensor.IACSharpSensor.SensorReached(6126);
        n = _stream.Read(header, 0, 2);
        totalBytesRead += n;
        Int16 extraLength = (Int16)(header[0] + header[1] * 256);
        byte[] extra = new byte[extraLength];
        n = _stream.Read(extra, 0, extra.Length);
        IACSharpSensor.IACSharpSensor.SensorReached(6127);
        if (n != extraLength) {
          IACSharpSensor.IACSharpSensor.SensorReached(6128);
          throw new ZlibException("Unexpected end-of-file reading GZIP header.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6129);
        totalBytesRead += n;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6130);
      if ((header[3] & 0x8) == 0x8) {
        IACSharpSensor.IACSharpSensor.SensorReached(6131);
        _GzipFileName = ReadZeroTerminatedString();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6132);
      if ((header[3] & 0x10) == 0x10) {
        IACSharpSensor.IACSharpSensor.SensorReached(6133);
        _GzipComment = ReadZeroTerminatedString();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6134);
      if ((header[3] & 0x2) == 0x2) {
        IACSharpSensor.IACSharpSensor.SensorReached(6135);
        Read(_buf1, 0, 1);
      }
      System.Int32 RNTRNTRNT_689 = totalBytesRead;
      IACSharpSensor.IACSharpSensor.SensorReached(6136);
      return RNTRNTRNT_689;
    }
    public override System.Int32 Read(System.Byte[] buffer, System.Int32 offset, System.Int32 count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6137);
      if (_streamMode == StreamMode.Undefined) {
        IACSharpSensor.IACSharpSensor.SensorReached(6138);
        if (!this._stream.CanRead) {
          IACSharpSensor.IACSharpSensor.SensorReached(6139);
          throw new ZlibException("The stream is not readable.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6140);
        _streamMode = StreamMode.Reader;
        z.AvailableBytesIn = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6141);
        if (_flavor == ZlibStreamFlavor.GZIP) {
          IACSharpSensor.IACSharpSensor.SensorReached(6142);
          _gzipHeaderByteCount = _ReadAndValidateGzipHeader();
          IACSharpSensor.IACSharpSensor.SensorReached(6143);
          if (_gzipHeaderByteCount == 0) {
            System.Int32 RNTRNTRNT_690 = 0;
            IACSharpSensor.IACSharpSensor.SensorReached(6144);
            return RNTRNTRNT_690;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6145);
      if (_streamMode != StreamMode.Reader) {
        IACSharpSensor.IACSharpSensor.SensorReached(6146);
        throw new ZlibException("Cannot Read after Writing.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6147);
      if (count == 0) {
        System.Int32 RNTRNTRNT_691 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6148);
        return RNTRNTRNT_691;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6149);
      if (nomoreinput && _wantCompress) {
        System.Int32 RNTRNTRNT_692 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6150);
        return RNTRNTRNT_692;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6151);
      if (buffer == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6152);
        throw new ArgumentNullException("buffer");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6153);
      if (count < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6154);
        throw new ArgumentOutOfRangeException("count");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6155);
      if (offset < buffer.GetLowerBound(0)) {
        IACSharpSensor.IACSharpSensor.SensorReached(6156);
        throw new ArgumentOutOfRangeException("offset");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6157);
      if ((offset + count) > buffer.GetLength(0)) {
        IACSharpSensor.IACSharpSensor.SensorReached(6158);
        throw new ArgumentOutOfRangeException("count");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6159);
      int rc = 0;
      _z.OutputBuffer = buffer;
      _z.NextOut = offset;
      _z.AvailableBytesOut = count;
      _z.InputBuffer = workingBuffer;
      IACSharpSensor.IACSharpSensor.SensorReached(6160);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(6161);
        if ((_z.AvailableBytesIn == 0) && (!nomoreinput)) {
          IACSharpSensor.IACSharpSensor.SensorReached(6162);
          _z.NextIn = 0;
          _z.AvailableBytesIn = _stream.Read(_workingBuffer, 0, _workingBuffer.Length);
          IACSharpSensor.IACSharpSensor.SensorReached(6163);
          if (_z.AvailableBytesIn == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6164);
            nomoreinput = true;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6165);
        rc = (_wantCompress) ? _z.Deflate(_flushMode) : _z.Inflate(_flushMode);
        IACSharpSensor.IACSharpSensor.SensorReached(6166);
        if (nomoreinput && (rc == ZlibConstants.Z_BUF_ERROR)) {
          System.Int32 RNTRNTRNT_693 = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(6167);
          return RNTRNTRNT_693;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6168);
        if (rc != ZlibConstants.Z_OK && rc != ZlibConstants.Z_STREAM_END) {
          IACSharpSensor.IACSharpSensor.SensorReached(6169);
          throw new ZlibException(String.Format("{0}flating:  rc={1}  msg={2}", (_wantCompress ? "de" : "in"), rc, _z.Message));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6170);
        if ((nomoreinput || rc == ZlibConstants.Z_STREAM_END) && (_z.AvailableBytesOut == count)) {
          IACSharpSensor.IACSharpSensor.SensorReached(6171);
          break;
        }
      } while (_z.AvailableBytesOut > 0 && !nomoreinput && rc == ZlibConstants.Z_OK);
      IACSharpSensor.IACSharpSensor.SensorReached(6172);
      if (_z.AvailableBytesOut > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6173);
        if (rc == ZlibConstants.Z_OK && _z.AvailableBytesIn == 0) {
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6174);
        if (nomoreinput) {
          IACSharpSensor.IACSharpSensor.SensorReached(6175);
          if (_wantCompress) {
            IACSharpSensor.IACSharpSensor.SensorReached(6176);
            rc = _z.Deflate(FlushType.Finish);
            IACSharpSensor.IACSharpSensor.SensorReached(6177);
            if (rc != ZlibConstants.Z_OK && rc != ZlibConstants.Z_STREAM_END) {
              IACSharpSensor.IACSharpSensor.SensorReached(6178);
              throw new ZlibException(String.Format("Deflating:  rc={0}  msg={1}", rc, _z.Message));
            }
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6179);
      rc = (count - _z.AvailableBytesOut);
      IACSharpSensor.IACSharpSensor.SensorReached(6180);
      if (crc != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6181);
        crc.SlurpBlock(buffer, offset, rc);
      }
      System.Int32 RNTRNTRNT_694 = rc;
      IACSharpSensor.IACSharpSensor.SensorReached(6182);
      return RNTRNTRNT_694;
    }
    public override System.Boolean CanRead {
      get {
        System.Boolean RNTRNTRNT_695 = this._stream.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(6183);
        return RNTRNTRNT_695;
      }
    }
    public override System.Boolean CanSeek {
      get {
        System.Boolean RNTRNTRNT_696 = this._stream.CanSeek;
        IACSharpSensor.IACSharpSensor.SensorReached(6184);
        return RNTRNTRNT_696;
      }
    }
    public override System.Boolean CanWrite {
      get {
        System.Boolean RNTRNTRNT_697 = this._stream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(6185);
        return RNTRNTRNT_697;
      }
    }
    public override System.Int64 Length {
      get {
        System.Int64 RNTRNTRNT_698 = _stream.Length;
        IACSharpSensor.IACSharpSensor.SensorReached(6186);
        return RNTRNTRNT_698;
      }
    }
    public override long Position {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6187);
        throw new NotImplementedException();
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6188);
        throw new NotImplementedException();
      }
    }
    internal enum StreamMode
    {
      Writer,
      Reader,
      Undefined
    }
    public static void CompressString(String s, Stream compressor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6189);
      byte[] uncompressed = System.Text.Encoding.UTF8.GetBytes(s);
      IACSharpSensor.IACSharpSensor.SensorReached(6190);
      using (compressor) {
        IACSharpSensor.IACSharpSensor.SensorReached(6191);
        compressor.Write(uncompressed, 0, uncompressed.Length);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6192);
    }
    public static void CompressBuffer(byte[] b, Stream compressor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6193);
      using (compressor) {
        IACSharpSensor.IACSharpSensor.SensorReached(6194);
        compressor.Write(b, 0, b.Length);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6195);
    }
    public static String UncompressString(byte[] compressed, Stream decompressor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6196);
      byte[] working = new byte[1024];
      var encoding = System.Text.Encoding.UTF8;
      IACSharpSensor.IACSharpSensor.SensorReached(6197);
      using (var output = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(6198);
        using (decompressor) {
          IACSharpSensor.IACSharpSensor.SensorReached(6199);
          int n;
          IACSharpSensor.IACSharpSensor.SensorReached(6200);
          while ((n = decompressor.Read(working, 0, working.Length)) != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6201);
            output.Write(working, 0, n);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6202);
        output.Seek(0, SeekOrigin.Begin);
        var sr = new StreamReader(output, encoding);
        String RNTRNTRNT_699 = sr.ReadToEnd();
        IACSharpSensor.IACSharpSensor.SensorReached(6203);
        return RNTRNTRNT_699;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed, Stream decompressor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6204);
      byte[] working = new byte[1024];
      IACSharpSensor.IACSharpSensor.SensorReached(6205);
      using (var output = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(6206);
        using (decompressor) {
          IACSharpSensor.IACSharpSensor.SensorReached(6207);
          int n;
          IACSharpSensor.IACSharpSensor.SensorReached(6208);
          while ((n = decompressor.Read(working, 0, working.Length)) != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6209);
            output.Write(working, 0, n);
          }
        }
        System.Byte[] RNTRNTRNT_700 = output.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(6210);
        return RNTRNTRNT_700;
      }
    }
  }
}
