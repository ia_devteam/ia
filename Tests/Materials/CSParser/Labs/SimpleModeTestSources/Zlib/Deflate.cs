using System;
namespace Ionic.Zlib
{
  internal enum BlockState
  {
    NeedMore = 0,
    BlockDone,
    FinishStarted,
    FinishDone
  }
  internal enum DeflateFlavor
  {
    Store,
    Fast,
    Slow
  }
  internal sealed class DeflateManager
  {
    private static readonly int MEM_LEVEL_MAX = 9;
    private static readonly int MEM_LEVEL_DEFAULT = 8;
    internal delegate BlockState CompressFunc(FlushType flush);
    internal class Config
    {
      internal int GoodLength;
      internal int MaxLazy;
      internal int NiceLength;
      internal int MaxChainLength;
      internal DeflateFlavor Flavor;
      private Config(int goodLength, int maxLazy, int niceLength, int maxChainLength, DeflateFlavor flavor)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(4599);
        this.GoodLength = goodLength;
        this.MaxLazy = maxLazy;
        this.NiceLength = niceLength;
        this.MaxChainLength = maxChainLength;
        this.Flavor = flavor;
        IACSharpSensor.IACSharpSensor.SensorReached(4600);
      }
      public static Config Lookup(CompressionLevel level)
      {
        Config RNTRNTRNT_513 = Table[(int)level];
        IACSharpSensor.IACSharpSensor.SensorReached(4601);
        return RNTRNTRNT_513;
      }
      static Config()
      {
        IACSharpSensor.IACSharpSensor.SensorReached(4602);
        Table = new Config[] {
          new Config(0, 0, 0, 0, DeflateFlavor.Store),
          new Config(4, 4, 8, 4, DeflateFlavor.Fast),
          new Config(4, 5, 16, 8, DeflateFlavor.Fast),
          new Config(4, 6, 32, 32, DeflateFlavor.Fast),
          new Config(4, 4, 16, 16, DeflateFlavor.Slow),
          new Config(8, 16, 32, 32, DeflateFlavor.Slow),
          new Config(8, 16, 128, 128, DeflateFlavor.Slow),
          new Config(8, 32, 128, 256, DeflateFlavor.Slow),
          new Config(32, 128, 258, 1024, DeflateFlavor.Slow),
          new Config(32, 258, 258, 4096, DeflateFlavor.Slow)
        };
        IACSharpSensor.IACSharpSensor.SensorReached(4603);
      }
      private static readonly Config[] Table;
    }
    private CompressFunc DeflateFunction;
    private static readonly System.String[] _ErrorMessage = new System.String[] {
      "need dictionary",
      "stream end",
      "",
      "file error",
      "stream error",
      "data error",
      "insufficient memory",
      "buffer error",
      "incompatible version",
      ""
    };
    private static readonly int PRESET_DICT = 0x20;
    private static readonly int INIT_STATE = 42;
    private static readonly int BUSY_STATE = 113;
    private static readonly int FINISH_STATE = 666;
    private static readonly int Z_DEFLATED = 8;
    private static readonly int STORED_BLOCK = 0;
    private static readonly int STATIC_TREES = 1;
    private static readonly int DYN_TREES = 2;
    private static readonly int Z_BINARY = 0;
    private static readonly int Z_ASCII = 1;
    private static readonly int Z_UNKNOWN = 2;
    private static readonly int Buf_size = 8 * 2;
    private static readonly int MIN_MATCH = 3;
    private static readonly int MAX_MATCH = 258;
    private static readonly int MIN_LOOKAHEAD = (MAX_MATCH + MIN_MATCH + 1);
    private static readonly int HEAP_SIZE = (2 * InternalConstants.L_CODES + 1);
    private static readonly int END_BLOCK = 256;
    internal ZlibCodec _codec;
    internal int status;
    internal byte[] pending;
    internal int nextPending;
    internal int pendingCount;
    internal sbyte data_type;
    internal int last_flush;
    internal int w_size;
    internal int w_bits;
    internal int w_mask;
    internal byte[] window;
    internal int window_size;
    internal short[] prev;
    internal short[] head;
    internal int ins_h;
    internal int hash_size;
    internal int hash_bits;
    internal int hash_mask;
    internal int hash_shift;
    internal int block_start;
    Config config;
    internal int match_length;
    internal int prev_match;
    internal int match_available;
    internal int strstart;
    internal int match_start;
    internal int lookahead;
    internal int prev_length;
    internal CompressionLevel compressionLevel;
    internal CompressionStrategy compressionStrategy;
    internal short[] dyn_ltree;
    internal short[] dyn_dtree;
    internal short[] bl_tree;
    internal Tree treeLiterals = new Tree();
    internal Tree treeDistances = new Tree();
    internal Tree treeBitLengths = new Tree();
    internal short[] bl_count = new short[InternalConstants.MAX_BITS + 1];
    internal int[] heap = new int[2 * InternalConstants.L_CODES + 1];
    internal int heap_len;
    internal int heap_max;
    internal sbyte[] depth = new sbyte[2 * InternalConstants.L_CODES + 1];
    internal int _lengthOffset;
    internal int lit_bufsize;
    internal int last_lit;
    internal int _distanceOffset;
    internal int opt_len;
    internal int static_len;
    internal int matches;
    internal int last_eob_len;
    internal short bi_buf;
    internal int bi_valid;
    internal DeflateManager()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4604);
      dyn_ltree = new short[HEAP_SIZE * 2];
      dyn_dtree = new short[(2 * InternalConstants.D_CODES + 1) * 2];
      bl_tree = new short[(2 * InternalConstants.BL_CODES + 1) * 2];
      IACSharpSensor.IACSharpSensor.SensorReached(4605);
    }
    private void _InitializeLazyMatch()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4606);
      window_size = 2 * w_size;
      Array.Clear(head, 0, hash_size);
      config = Config.Lookup(compressionLevel);
      SetDeflater();
      strstart = 0;
      block_start = 0;
      lookahead = 0;
      match_length = prev_length = MIN_MATCH - 1;
      match_available = 0;
      ins_h = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4607);
    }
    private void _InitializeTreeData()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4608);
      treeLiterals.dyn_tree = dyn_ltree;
      treeLiterals.staticTree = StaticTree.Literals;
      treeDistances.dyn_tree = dyn_dtree;
      treeDistances.staticTree = StaticTree.Distances;
      treeBitLengths.dyn_tree = bl_tree;
      treeBitLengths.staticTree = StaticTree.BitLengths;
      bi_buf = 0;
      bi_valid = 0;
      last_eob_len = 8;
      _InitializeBlocks();
      IACSharpSensor.IACSharpSensor.SensorReached(4609);
    }
    internal void _InitializeBlocks()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4610);
      for (int i = 0; i < InternalConstants.L_CODES; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4611);
        dyn_ltree[i * 2] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4612);
      for (int i = 0; i < InternalConstants.D_CODES; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4613);
        dyn_dtree[i * 2] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4614);
      for (int i = 0; i < InternalConstants.BL_CODES; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4615);
        bl_tree[i * 2] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4616);
      dyn_ltree[END_BLOCK * 2] = 1;
      opt_len = static_len = 0;
      last_lit = matches = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4617);
    }
    internal void pqdownheap(short[] tree, int k)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4618);
      int v = heap[k];
      int j = k << 1;
      IACSharpSensor.IACSharpSensor.SensorReached(4619);
      while (j <= heap_len) {
        IACSharpSensor.IACSharpSensor.SensorReached(4620);
        if (j < heap_len && _IsSmaller(tree, heap[j + 1], heap[j], depth)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4621);
          j++;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4622);
        if (_IsSmaller(tree, v, heap[j], depth)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4623);
          break;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4624);
        heap[k] = heap[j];
        k = j;
        j <<= 1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4625);
      heap[k] = v;
      IACSharpSensor.IACSharpSensor.SensorReached(4626);
    }
    static internal bool _IsSmaller(short[] tree, int n, int m, sbyte[] depth)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4627);
      short tn2 = tree[n * 2];
      short tm2 = tree[m * 2];
      System.Boolean RNTRNTRNT_514 = (tn2 < tm2 || (tn2 == tm2 && depth[n] <= depth[m]));
      IACSharpSensor.IACSharpSensor.SensorReached(4628);
      return RNTRNTRNT_514;
    }
    internal void scan_tree(short[] tree, int max_code)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4629);
      int n;
      int prevlen = -1;
      int curlen;
      int nextlen = (int)tree[0 * 2 + 1];
      int count = 0;
      int max_count = 7;
      int min_count = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(4630);
      if (nextlen == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4631);
        max_count = 138;
        min_count = 3;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4632);
      tree[(max_code + 1) * 2 + 1] = (short)0x7fff;
      IACSharpSensor.IACSharpSensor.SensorReached(4633);
      for (n = 0; n <= max_code; n++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4634);
        curlen = nextlen;
        nextlen = (int)tree[(n + 1) * 2 + 1];
        IACSharpSensor.IACSharpSensor.SensorReached(4635);
        if (++count < max_count && curlen == nextlen) {
          IACSharpSensor.IACSharpSensor.SensorReached(4636);
          continue;
        } else if (count < min_count) {
          IACSharpSensor.IACSharpSensor.SensorReached(4642);
          bl_tree[curlen * 2] = (short)(bl_tree[curlen * 2] + count);
        } else if (curlen != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4639);
          if (curlen != prevlen) {
            IACSharpSensor.IACSharpSensor.SensorReached(4640);
            bl_tree[curlen * 2]++;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4641);
          bl_tree[InternalConstants.REP_3_6 * 2]++;
        } else if (count <= 10) {
          IACSharpSensor.IACSharpSensor.SensorReached(4638);
          bl_tree[InternalConstants.REPZ_3_10 * 2]++;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4637);
          bl_tree[InternalConstants.REPZ_11_138 * 2]++;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4643);
        count = 0;
        prevlen = curlen;
        IACSharpSensor.IACSharpSensor.SensorReached(4644);
        if (nextlen == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4645);
          max_count = 138;
          min_count = 3;
        } else if (curlen == nextlen) {
          IACSharpSensor.IACSharpSensor.SensorReached(4647);
          max_count = 6;
          min_count = 3;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4646);
          max_count = 7;
          min_count = 4;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4648);
    }
    internal int build_bl_tree()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4649);
      int max_blindex;
      scan_tree(dyn_ltree, treeLiterals.max_code);
      scan_tree(dyn_dtree, treeDistances.max_code);
      treeBitLengths.build_tree(this);
      IACSharpSensor.IACSharpSensor.SensorReached(4650);
      for (max_blindex = InternalConstants.BL_CODES - 1; max_blindex >= 3; max_blindex--) {
        IACSharpSensor.IACSharpSensor.SensorReached(4651);
        if (bl_tree[Tree.bl_order[max_blindex] * 2 + 1] != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4652);
          break;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4653);
      opt_len += 3 * (max_blindex + 1) + 5 + 5 + 4;
      System.Int32 RNTRNTRNT_515 = max_blindex;
      IACSharpSensor.IACSharpSensor.SensorReached(4654);
      return RNTRNTRNT_515;
    }
    internal void send_all_trees(int lcodes, int dcodes, int blcodes)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4655);
      int rank;
      send_bits(lcodes - 257, 5);
      send_bits(dcodes - 1, 5);
      send_bits(blcodes - 4, 4);
      IACSharpSensor.IACSharpSensor.SensorReached(4656);
      for (rank = 0; rank < blcodes; rank++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4657);
        send_bits(bl_tree[Tree.bl_order[rank] * 2 + 1], 3);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4658);
      send_tree(dyn_ltree, lcodes - 1);
      send_tree(dyn_dtree, dcodes - 1);
      IACSharpSensor.IACSharpSensor.SensorReached(4659);
    }
    internal void send_tree(short[] tree, int max_code)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4660);
      int n;
      int prevlen = -1;
      int curlen;
      int nextlen = tree[0 * 2 + 1];
      int count = 0;
      int max_count = 7;
      int min_count = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(4661);
      if (nextlen == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4662);
        max_count = 138;
        min_count = 3;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4663);
      for (n = 0; n <= max_code; n++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4664);
        curlen = nextlen;
        nextlen = tree[(n + 1) * 2 + 1];
        IACSharpSensor.IACSharpSensor.SensorReached(4665);
        if (++count < max_count && curlen == nextlen) {
          IACSharpSensor.IACSharpSensor.SensorReached(4666);
          continue;
        } else if (count < min_count) {
          IACSharpSensor.IACSharpSensor.SensorReached(4672);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(4673);
            send_code(curlen, bl_tree);
          } while (--count != 0);
        } else if (curlen != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4669);
          if (curlen != prevlen) {
            IACSharpSensor.IACSharpSensor.SensorReached(4670);
            send_code(curlen, bl_tree);
            count--;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4671);
          send_code(InternalConstants.REP_3_6, bl_tree);
          send_bits(count - 3, 2);
        } else if (count <= 10) {
          IACSharpSensor.IACSharpSensor.SensorReached(4668);
          send_code(InternalConstants.REPZ_3_10, bl_tree);
          send_bits(count - 3, 3);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4667);
          send_code(InternalConstants.REPZ_11_138, bl_tree);
          send_bits(count - 11, 7);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4674);
        count = 0;
        prevlen = curlen;
        IACSharpSensor.IACSharpSensor.SensorReached(4675);
        if (nextlen == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4676);
          max_count = 138;
          min_count = 3;
        } else if (curlen == nextlen) {
          IACSharpSensor.IACSharpSensor.SensorReached(4678);
          max_count = 6;
          min_count = 3;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4677);
          max_count = 7;
          min_count = 4;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4679);
    }
    private void put_bytes(byte[] p, int start, int len)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4680);
      Array.Copy(p, start, pending, pendingCount, len);
      pendingCount += len;
      IACSharpSensor.IACSharpSensor.SensorReached(4681);
    }
    internal void send_code(int c, short[] tree)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4682);
      int c2 = c * 2;
      send_bits((tree[c2] & 0xffff), (tree[c2 + 1] & 0xffff));
      IACSharpSensor.IACSharpSensor.SensorReached(4683);
    }
    internal void send_bits(int value, int length)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4684);
      int len = length;
      unchecked {
        IACSharpSensor.IACSharpSensor.SensorReached(4685);
        if (bi_valid > (int)Buf_size - len) {
          IACSharpSensor.IACSharpSensor.SensorReached(4686);
          bi_buf |= (short)((value << bi_valid) & 0xffff);
          pending[pendingCount++] = (byte)bi_buf;
          pending[pendingCount++] = (byte)(bi_buf >> 8);
          bi_buf = (short)((uint)value >> (Buf_size - bi_valid));
          bi_valid += len - Buf_size;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4687);
          bi_buf |= (short)((value << bi_valid) & 0xffff);
          bi_valid += len;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4688);
    }
    internal void _tr_align()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4689);
      send_bits(STATIC_TREES << 1, 3);
      send_code(END_BLOCK, StaticTree.lengthAndLiteralsTreeCodes);
      bi_flush();
      IACSharpSensor.IACSharpSensor.SensorReached(4690);
      if (1 + last_eob_len + 10 - bi_valid < 9) {
        IACSharpSensor.IACSharpSensor.SensorReached(4691);
        send_bits(STATIC_TREES << 1, 3);
        send_code(END_BLOCK, StaticTree.lengthAndLiteralsTreeCodes);
        bi_flush();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4692);
      last_eob_len = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(4693);
    }
    internal bool _tr_tally(int dist, int lc)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4694);
      pending[_distanceOffset + last_lit * 2] = unchecked((byte)((uint)dist >> 8));
      pending[_distanceOffset + last_lit * 2 + 1] = unchecked((byte)dist);
      pending[_lengthOffset + last_lit] = unchecked((byte)lc);
      last_lit++;
      IACSharpSensor.IACSharpSensor.SensorReached(4695);
      if (dist == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4696);
        dyn_ltree[lc * 2]++;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4697);
        matches++;
        dist--;
        dyn_ltree[(Tree.LengthCode[lc] + InternalConstants.LITERALS + 1) * 2]++;
        dyn_dtree[Tree.DistanceCode(dist) * 2]++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4698);
      if ((last_lit & 0x1fff) == 0 && (int)compressionLevel > 2) {
        IACSharpSensor.IACSharpSensor.SensorReached(4699);
        int out_length = last_lit << 3;
        int in_length = strstart - block_start;
        int dcode;
        IACSharpSensor.IACSharpSensor.SensorReached(4700);
        for (dcode = 0; dcode < InternalConstants.D_CODES; dcode++) {
          IACSharpSensor.IACSharpSensor.SensorReached(4701);
          out_length = (int)(out_length + (int)dyn_dtree[dcode * 2] * (5L + Tree.ExtraDistanceBits[dcode]));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4702);
        out_length >>= 3;
        IACSharpSensor.IACSharpSensor.SensorReached(4703);
        if ((matches < (last_lit / 2)) && out_length < in_length / 2) {
          System.Boolean RNTRNTRNT_516 = true;
          IACSharpSensor.IACSharpSensor.SensorReached(4704);
          return RNTRNTRNT_516;
        }
      }
      System.Boolean RNTRNTRNT_517 = (last_lit == lit_bufsize - 1) || (last_lit == lit_bufsize);
      IACSharpSensor.IACSharpSensor.SensorReached(4705);
      return RNTRNTRNT_517;
    }
    internal void send_compressed_block(short[] ltree, short[] dtree)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4706);
      int distance;
      int lc;
      int lx = 0;
      int code;
      int extra;
      IACSharpSensor.IACSharpSensor.SensorReached(4707);
      if (last_lit != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4708);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(4709);
          int ix = _distanceOffset + lx * 2;
          distance = ((pending[ix] << 8) & 0xff00) | (pending[ix + 1] & 0xff);
          lc = (pending[_lengthOffset + lx]) & 0xff;
          lx++;
          IACSharpSensor.IACSharpSensor.SensorReached(4710);
          if (distance == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(4711);
            send_code(lc, ltree);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(4712);
            code = Tree.LengthCode[lc];
            send_code(code + InternalConstants.LITERALS + 1, ltree);
            extra = Tree.ExtraLengthBits[code];
            IACSharpSensor.IACSharpSensor.SensorReached(4713);
            if (extra != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(4714);
              lc -= Tree.LengthBase[code];
              send_bits(lc, extra);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(4715);
            distance--;
            code = Tree.DistanceCode(distance);
            send_code(code, dtree);
            extra = Tree.ExtraDistanceBits[code];
            IACSharpSensor.IACSharpSensor.SensorReached(4716);
            if (extra != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(4717);
              distance -= Tree.DistanceBase[code];
              send_bits(distance, extra);
            }
          }
        } while (lx < last_lit);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4718);
      send_code(END_BLOCK, ltree);
      last_eob_len = ltree[END_BLOCK * 2 + 1];
      IACSharpSensor.IACSharpSensor.SensorReached(4719);
    }
    internal void set_data_type()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4720);
      int n = 0;
      int ascii_freq = 0;
      int bin_freq = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4721);
      while (n < 7) {
        IACSharpSensor.IACSharpSensor.SensorReached(4722);
        bin_freq += dyn_ltree[n * 2];
        n++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4723);
      while (n < 128) {
        IACSharpSensor.IACSharpSensor.SensorReached(4724);
        ascii_freq += dyn_ltree[n * 2];
        n++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4725);
      while (n < InternalConstants.LITERALS) {
        IACSharpSensor.IACSharpSensor.SensorReached(4726);
        bin_freq += dyn_ltree[n * 2];
        n++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4727);
      data_type = (sbyte)(bin_freq > (ascii_freq >> 2) ? Z_BINARY : Z_ASCII);
      IACSharpSensor.IACSharpSensor.SensorReached(4728);
    }
    internal void bi_flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4729);
      if (bi_valid == 16) {
        IACSharpSensor.IACSharpSensor.SensorReached(4730);
        pending[pendingCount++] = (byte)bi_buf;
        pending[pendingCount++] = (byte)(bi_buf >> 8);
        bi_buf = 0;
        bi_valid = 0;
      } else if (bi_valid >= 8) {
        IACSharpSensor.IACSharpSensor.SensorReached(4731);
        pending[pendingCount++] = (byte)bi_buf;
        bi_buf >>= 8;
        bi_valid -= 8;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4732);
    }
    internal void bi_windup()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4733);
      if (bi_valid > 8) {
        IACSharpSensor.IACSharpSensor.SensorReached(4734);
        pending[pendingCount++] = (byte)bi_buf;
        pending[pendingCount++] = (byte)(bi_buf >> 8);
      } else if (bi_valid > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4735);
        pending[pendingCount++] = (byte)bi_buf;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4736);
      bi_buf = 0;
      bi_valid = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4737);
    }
    internal void copy_block(int buf, int len, bool header)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4738);
      bi_windup();
      last_eob_len = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(4739);
      if (header) {
        unchecked {
          IACSharpSensor.IACSharpSensor.SensorReached(4740);
          pending[pendingCount++] = (byte)len;
          pending[pendingCount++] = (byte)(len >> 8);
          pending[pendingCount++] = (byte)~len;
          pending[pendingCount++] = (byte)(~len >> 8);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4741);
      put_bytes(window, buf, len);
      IACSharpSensor.IACSharpSensor.SensorReached(4742);
    }
    internal void flush_block_only(bool eof)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4743);
      _tr_flush_block(block_start >= 0 ? block_start : -1, strstart - block_start, eof);
      block_start = strstart;
      _codec.flush_pending();
      IACSharpSensor.IACSharpSensor.SensorReached(4744);
    }
    internal BlockState DeflateNone(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4745);
      int max_block_size = 0xffff;
      int max_start;
      IACSharpSensor.IACSharpSensor.SensorReached(4746);
      if (max_block_size > pending.Length - 5) {
        IACSharpSensor.IACSharpSensor.SensorReached(4747);
        max_block_size = pending.Length - 5;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4748);
      while (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(4749);
        if (lookahead <= 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(4750);
          _fillWindow();
          IACSharpSensor.IACSharpSensor.SensorReached(4751);
          if (lookahead == 0 && flush == FlushType.None) {
            BlockState RNTRNTRNT_518 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(4752);
            return RNTRNTRNT_518;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4753);
          if (lookahead == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(4754);
            break;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4755);
        strstart += lookahead;
        lookahead = 0;
        max_start = block_start + max_block_size;
        IACSharpSensor.IACSharpSensor.SensorReached(4756);
        if (strstart == 0 || strstart >= max_start) {
          IACSharpSensor.IACSharpSensor.SensorReached(4757);
          lookahead = (int)(strstart - max_start);
          strstart = (int)max_start;
          flush_block_only(false);
          IACSharpSensor.IACSharpSensor.SensorReached(4758);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_519 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(4759);
            return RNTRNTRNT_519;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4760);
        if (strstart - block_start >= w_size - MIN_LOOKAHEAD) {
          IACSharpSensor.IACSharpSensor.SensorReached(4761);
          flush_block_only(false);
          IACSharpSensor.IACSharpSensor.SensorReached(4762);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_520 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(4763);
            return RNTRNTRNT_520;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4764);
      flush_block_only(flush == FlushType.Finish);
      IACSharpSensor.IACSharpSensor.SensorReached(4765);
      if (_codec.AvailableBytesOut == 0) {
        BlockState RNTRNTRNT_521 = (flush == FlushType.Finish) ? BlockState.FinishStarted : BlockState.NeedMore;
        IACSharpSensor.IACSharpSensor.SensorReached(4766);
        return RNTRNTRNT_521;
      }
      BlockState RNTRNTRNT_522 = flush == FlushType.Finish ? BlockState.FinishDone : BlockState.BlockDone;
      IACSharpSensor.IACSharpSensor.SensorReached(4767);
      return RNTRNTRNT_522;
    }
    internal void _tr_stored_block(int buf, int stored_len, bool eof)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4768);
      send_bits((STORED_BLOCK << 1) + (eof ? 1 : 0), 3);
      copy_block(buf, stored_len, true);
      IACSharpSensor.IACSharpSensor.SensorReached(4769);
    }
    internal void _tr_flush_block(int buf, int stored_len, bool eof)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4770);
      int opt_lenb, static_lenb;
      int max_blindex = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4771);
      if (compressionLevel > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4772);
        if (data_type == Z_UNKNOWN) {
          IACSharpSensor.IACSharpSensor.SensorReached(4773);
          set_data_type();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4774);
        treeLiterals.build_tree(this);
        treeDistances.build_tree(this);
        max_blindex = build_bl_tree();
        opt_lenb = (opt_len + 3 + 7) >> 3;
        static_lenb = (static_len + 3 + 7) >> 3;
        IACSharpSensor.IACSharpSensor.SensorReached(4775);
        if (static_lenb <= opt_lenb) {
          IACSharpSensor.IACSharpSensor.SensorReached(4776);
          opt_lenb = static_lenb;
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4777);
        opt_lenb = static_lenb = stored_len + 5;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4778);
      if (stored_len + 4 <= opt_lenb && buf != -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(4779);
        _tr_stored_block(buf, stored_len, eof);
      } else if (static_lenb == opt_lenb) {
        IACSharpSensor.IACSharpSensor.SensorReached(4781);
        send_bits((STATIC_TREES << 1) + (eof ? 1 : 0), 3);
        send_compressed_block(StaticTree.lengthAndLiteralsTreeCodes, StaticTree.distTreeCodes);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4780);
        send_bits((DYN_TREES << 1) + (eof ? 1 : 0), 3);
        send_all_trees(treeLiterals.max_code + 1, treeDistances.max_code + 1, max_blindex + 1);
        send_compressed_block(dyn_ltree, dyn_dtree);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4782);
      _InitializeBlocks();
      IACSharpSensor.IACSharpSensor.SensorReached(4783);
      if (eof) {
        IACSharpSensor.IACSharpSensor.SensorReached(4784);
        bi_windup();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4785);
    }
    private void _fillWindow()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4786);
      int n, m;
      int p;
      int more;
      IACSharpSensor.IACSharpSensor.SensorReached(4787);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(4788);
        more = (window_size - lookahead - strstart);
        IACSharpSensor.IACSharpSensor.SensorReached(4789);
        if (more == 0 && strstart == 0 && lookahead == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4790);
          more = w_size;
        } else if (more == -1) {
          IACSharpSensor.IACSharpSensor.SensorReached(4798);
          more--;
        } else if (strstart >= w_size + w_size - MIN_LOOKAHEAD) {
          IACSharpSensor.IACSharpSensor.SensorReached(4791);
          Array.Copy(window, w_size, window, 0, w_size);
          match_start -= w_size;
          strstart -= w_size;
          block_start -= w_size;
          n = hash_size;
          p = n;
          IACSharpSensor.IACSharpSensor.SensorReached(4792);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(4793);
            m = (head[--p] & 0xffff);
            head[p] = (short)((m >= w_size) ? (m - w_size) : 0);
          } while (--n != 0);
          IACSharpSensor.IACSharpSensor.SensorReached(4794);
          n = w_size;
          p = n;
          IACSharpSensor.IACSharpSensor.SensorReached(4795);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(4796);
            m = (prev[--p] & 0xffff);
            prev[p] = (short)((m >= w_size) ? (m - w_size) : 0);
          } while (--n != 0);
          IACSharpSensor.IACSharpSensor.SensorReached(4797);
          more += w_size;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4799);
        if (_codec.AvailableBytesIn == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4800);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4801);
        n = _codec.read_buf(window, strstart + lookahead, more);
        lookahead += n;
        IACSharpSensor.IACSharpSensor.SensorReached(4802);
        if (lookahead >= MIN_MATCH) {
          IACSharpSensor.IACSharpSensor.SensorReached(4803);
          ins_h = window[strstart] & 0xff;
          ins_h = (((ins_h) << hash_shift) ^ (window[strstart + 1] & 0xff)) & hash_mask;
        }
      } while (lookahead < MIN_LOOKAHEAD && _codec.AvailableBytesIn != 0);
      IACSharpSensor.IACSharpSensor.SensorReached(4804);
    }
    internal BlockState DeflateFast(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4805);
      int hash_head = 0;
      bool bflush;
      IACSharpSensor.IACSharpSensor.SensorReached(4806);
      while (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(4807);
        if (lookahead < MIN_LOOKAHEAD) {
          IACSharpSensor.IACSharpSensor.SensorReached(4808);
          _fillWindow();
          IACSharpSensor.IACSharpSensor.SensorReached(4809);
          if (lookahead < MIN_LOOKAHEAD && flush == FlushType.None) {
            BlockState RNTRNTRNT_523 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(4810);
            return RNTRNTRNT_523;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4811);
          if (lookahead == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(4812);
            break;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4813);
        if (lookahead >= MIN_MATCH) {
          IACSharpSensor.IACSharpSensor.SensorReached(4814);
          ins_h = (((ins_h) << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
          hash_head = (head[ins_h] & 0xffff);
          prev[strstart & w_mask] = head[ins_h];
          head[ins_h] = unchecked((short)strstart);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4815);
        if (hash_head != 0L && ((strstart - hash_head) & 0xffff) <= w_size - MIN_LOOKAHEAD) {
          IACSharpSensor.IACSharpSensor.SensorReached(4816);
          if (compressionStrategy != CompressionStrategy.HuffmanOnly) {
            IACSharpSensor.IACSharpSensor.SensorReached(4817);
            match_length = longest_match(hash_head);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4818);
        if (match_length >= MIN_MATCH) {
          IACSharpSensor.IACSharpSensor.SensorReached(4819);
          bflush = _tr_tally(strstart - match_start, match_length - MIN_MATCH);
          lookahead -= match_length;
          IACSharpSensor.IACSharpSensor.SensorReached(4820);
          if (match_length <= config.MaxLazy && lookahead >= MIN_MATCH) {
            IACSharpSensor.IACSharpSensor.SensorReached(4821);
            match_length--;
            IACSharpSensor.IACSharpSensor.SensorReached(4822);
            do {
              IACSharpSensor.IACSharpSensor.SensorReached(4823);
              strstart++;
              ins_h = ((ins_h << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
              hash_head = (head[ins_h] & 0xffff);
              prev[strstart & w_mask] = head[ins_h];
              head[ins_h] = unchecked((short)strstart);
            } while (--match_length != 0);
            IACSharpSensor.IACSharpSensor.SensorReached(4824);
            strstart++;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(4825);
            strstart += match_length;
            match_length = 0;
            ins_h = window[strstart] & 0xff;
            ins_h = (((ins_h) << hash_shift) ^ (window[strstart + 1] & 0xff)) & hash_mask;
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4826);
          bflush = _tr_tally(0, window[strstart] & 0xff);
          lookahead--;
          strstart++;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4827);
        if (bflush) {
          IACSharpSensor.IACSharpSensor.SensorReached(4828);
          flush_block_only(false);
          IACSharpSensor.IACSharpSensor.SensorReached(4829);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_524 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(4830);
            return RNTRNTRNT_524;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4831);
      flush_block_only(flush == FlushType.Finish);
      IACSharpSensor.IACSharpSensor.SensorReached(4832);
      if (_codec.AvailableBytesOut == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4833);
        if (flush == FlushType.Finish) {
          BlockState RNTRNTRNT_525 = BlockState.FinishStarted;
          IACSharpSensor.IACSharpSensor.SensorReached(4834);
          return RNTRNTRNT_525;
        } else {
          BlockState RNTRNTRNT_526 = BlockState.NeedMore;
          IACSharpSensor.IACSharpSensor.SensorReached(4835);
          return RNTRNTRNT_526;
        }
      }
      BlockState RNTRNTRNT_527 = flush == FlushType.Finish ? BlockState.FinishDone : BlockState.BlockDone;
      IACSharpSensor.IACSharpSensor.SensorReached(4836);
      return RNTRNTRNT_527;
    }
    internal BlockState DeflateSlow(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4837);
      int hash_head = 0;
      bool bflush;
      IACSharpSensor.IACSharpSensor.SensorReached(4838);
      while (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(4839);
        if (lookahead < MIN_LOOKAHEAD) {
          IACSharpSensor.IACSharpSensor.SensorReached(4840);
          _fillWindow();
          IACSharpSensor.IACSharpSensor.SensorReached(4841);
          if (lookahead < MIN_LOOKAHEAD && flush == FlushType.None) {
            BlockState RNTRNTRNT_528 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(4842);
            return RNTRNTRNT_528;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4843);
          if (lookahead == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(4844);
            break;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4845);
        if (lookahead >= MIN_MATCH) {
          IACSharpSensor.IACSharpSensor.SensorReached(4846);
          ins_h = (((ins_h) << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
          hash_head = (head[ins_h] & 0xffff);
          prev[strstart & w_mask] = head[ins_h];
          head[ins_h] = unchecked((short)strstart);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4847);
        prev_length = match_length;
        prev_match = match_start;
        match_length = MIN_MATCH - 1;
        IACSharpSensor.IACSharpSensor.SensorReached(4848);
        if (hash_head != 0 && prev_length < config.MaxLazy && ((strstart - hash_head) & 0xffff) <= w_size - MIN_LOOKAHEAD) {
          IACSharpSensor.IACSharpSensor.SensorReached(4849);
          if (compressionStrategy != CompressionStrategy.HuffmanOnly) {
            IACSharpSensor.IACSharpSensor.SensorReached(4850);
            match_length = longest_match(hash_head);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4851);
          if (match_length <= 5 && (compressionStrategy == CompressionStrategy.Filtered || (match_length == MIN_MATCH && strstart - match_start > 4096))) {
            IACSharpSensor.IACSharpSensor.SensorReached(4852);
            match_length = MIN_MATCH - 1;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4853);
        if (prev_length >= MIN_MATCH && match_length <= prev_length) {
          IACSharpSensor.IACSharpSensor.SensorReached(4854);
          int max_insert = strstart + lookahead - MIN_MATCH;
          bflush = _tr_tally(strstart - 1 - prev_match, prev_length - MIN_MATCH);
          lookahead -= (prev_length - 1);
          prev_length -= 2;
          IACSharpSensor.IACSharpSensor.SensorReached(4855);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(4856);
            if (++strstart <= max_insert) {
              IACSharpSensor.IACSharpSensor.SensorReached(4857);
              ins_h = (((ins_h) << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
              hash_head = (head[ins_h] & 0xffff);
              prev[strstart & w_mask] = head[ins_h];
              head[ins_h] = unchecked((short)strstart);
            }
          } while (--prev_length != 0);
          IACSharpSensor.IACSharpSensor.SensorReached(4858);
          match_available = 0;
          match_length = MIN_MATCH - 1;
          strstart++;
          IACSharpSensor.IACSharpSensor.SensorReached(4859);
          if (bflush) {
            IACSharpSensor.IACSharpSensor.SensorReached(4860);
            flush_block_only(false);
            IACSharpSensor.IACSharpSensor.SensorReached(4861);
            if (_codec.AvailableBytesOut == 0) {
              BlockState RNTRNTRNT_529 = BlockState.NeedMore;
              IACSharpSensor.IACSharpSensor.SensorReached(4862);
              return RNTRNTRNT_529;
            }
          }
        } else if (match_available != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4864);
          bflush = _tr_tally(0, window[strstart - 1] & 0xff);
          IACSharpSensor.IACSharpSensor.SensorReached(4865);
          if (bflush) {
            IACSharpSensor.IACSharpSensor.SensorReached(4866);
            flush_block_only(false);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4867);
          strstart++;
          lookahead--;
          IACSharpSensor.IACSharpSensor.SensorReached(4868);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_530 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(4869);
            return RNTRNTRNT_530;
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4863);
          match_available = 1;
          strstart++;
          lookahead--;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4870);
      if (match_available != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4871);
        bflush = _tr_tally(0, window[strstart - 1] & 0xff);
        match_available = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4872);
      flush_block_only(flush == FlushType.Finish);
      IACSharpSensor.IACSharpSensor.SensorReached(4873);
      if (_codec.AvailableBytesOut == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4874);
        if (flush == FlushType.Finish) {
          BlockState RNTRNTRNT_531 = BlockState.FinishStarted;
          IACSharpSensor.IACSharpSensor.SensorReached(4875);
          return RNTRNTRNT_531;
        } else {
          BlockState RNTRNTRNT_532 = BlockState.NeedMore;
          IACSharpSensor.IACSharpSensor.SensorReached(4876);
          return RNTRNTRNT_532;
        }
      }
      BlockState RNTRNTRNT_533 = flush == FlushType.Finish ? BlockState.FinishDone : BlockState.BlockDone;
      IACSharpSensor.IACSharpSensor.SensorReached(4877);
      return RNTRNTRNT_533;
    }
    internal int longest_match(int cur_match)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4878);
      int chain_length = config.MaxChainLength;
      int scan = strstart;
      int match;
      int len;
      int best_len = prev_length;
      int limit = strstart > (w_size - MIN_LOOKAHEAD) ? strstart - (w_size - MIN_LOOKAHEAD) : 0;
      int niceLength = config.NiceLength;
      int wmask = w_mask;
      int strend = strstart + MAX_MATCH;
      byte scan_end1 = window[scan + best_len - 1];
      byte scan_end = window[scan + best_len];
      IACSharpSensor.IACSharpSensor.SensorReached(4879);
      if (prev_length >= config.GoodLength) {
        IACSharpSensor.IACSharpSensor.SensorReached(4880);
        chain_length >>= 2;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4881);
      if (niceLength > lookahead) {
        IACSharpSensor.IACSharpSensor.SensorReached(4882);
        niceLength = lookahead;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4883);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(4884);
        match = cur_match;
        IACSharpSensor.IACSharpSensor.SensorReached(4885);
        if (window[match + best_len] != scan_end || window[match + best_len - 1] != scan_end1 || window[match] != window[scan] || window[++match] != window[scan + 1]) {
          IACSharpSensor.IACSharpSensor.SensorReached(4886);
          continue;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4887);
        scan += 2;
        match++;
        IACSharpSensor.IACSharpSensor.SensorReached(4888);
        do {
        } while (window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && scan < strend);
        IACSharpSensor.IACSharpSensor.SensorReached(4889);
        len = MAX_MATCH - (int)(strend - scan);
        scan = strend - MAX_MATCH;
        IACSharpSensor.IACSharpSensor.SensorReached(4890);
        if (len > best_len) {
          IACSharpSensor.IACSharpSensor.SensorReached(4891);
          match_start = cur_match;
          best_len = len;
          IACSharpSensor.IACSharpSensor.SensorReached(4892);
          if (len >= niceLength) {
            IACSharpSensor.IACSharpSensor.SensorReached(4893);
            break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4894);
          scan_end1 = window[scan + best_len - 1];
          scan_end = window[scan + best_len];
        }
      } while ((cur_match = (prev[cur_match & wmask] & 0xffff)) > limit && --chain_length != 0);
      IACSharpSensor.IACSharpSensor.SensorReached(4895);
      if (best_len <= lookahead) {
        System.Int32 RNTRNTRNT_534 = best_len;
        IACSharpSensor.IACSharpSensor.SensorReached(4896);
        return RNTRNTRNT_534;
      }
      System.Int32 RNTRNTRNT_535 = lookahead;
      IACSharpSensor.IACSharpSensor.SensorReached(4897);
      return RNTRNTRNT_535;
    }
    private bool Rfc1950BytesEmitted = false;
    private bool _WantRfc1950HeaderBytes = true;
    internal bool WantRfc1950HeaderBytes {
      get {
        System.Boolean RNTRNTRNT_536 = _WantRfc1950HeaderBytes;
        IACSharpSensor.IACSharpSensor.SensorReached(4898);
        return RNTRNTRNT_536;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4899);
        _WantRfc1950HeaderBytes = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4900);
      }
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level)
    {
      System.Int32 RNTRNTRNT_537 = Initialize(codec, level, ZlibConstants.WindowBitsMax);
      IACSharpSensor.IACSharpSensor.SensorReached(4901);
      return RNTRNTRNT_537;
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level, int bits)
    {
      System.Int32 RNTRNTRNT_538 = Initialize(codec, level, bits, MEM_LEVEL_DEFAULT, CompressionStrategy.Default);
      IACSharpSensor.IACSharpSensor.SensorReached(4902);
      return RNTRNTRNT_538;
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level, int bits, CompressionStrategy compressionStrategy)
    {
      System.Int32 RNTRNTRNT_539 = Initialize(codec, level, bits, MEM_LEVEL_DEFAULT, compressionStrategy);
      IACSharpSensor.IACSharpSensor.SensorReached(4903);
      return RNTRNTRNT_539;
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level, int windowBits, int memLevel, CompressionStrategy strategy)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4904);
      _codec = codec;
      _codec.Message = null;
      IACSharpSensor.IACSharpSensor.SensorReached(4905);
      if (windowBits < 9 || windowBits > 15) {
        IACSharpSensor.IACSharpSensor.SensorReached(4906);
        throw new ZlibException("windowBits must be in the range 9..15.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4907);
      if (memLevel < 1 || memLevel > MEM_LEVEL_MAX) {
        IACSharpSensor.IACSharpSensor.SensorReached(4908);
        throw new ZlibException(String.Format("memLevel must be in the range 1.. {0}", MEM_LEVEL_MAX));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4909);
      _codec.dstate = this;
      w_bits = windowBits;
      w_size = 1 << w_bits;
      w_mask = w_size - 1;
      hash_bits = memLevel + 7;
      hash_size = 1 << hash_bits;
      hash_mask = hash_size - 1;
      hash_shift = ((hash_bits + MIN_MATCH - 1) / MIN_MATCH);
      window = new byte[w_size * 2];
      prev = new short[w_size];
      head = new short[hash_size];
      lit_bufsize = 1 << (memLevel + 6);
      pending = new byte[lit_bufsize * 4];
      _distanceOffset = lit_bufsize;
      _lengthOffset = (1 + 2) * lit_bufsize;
      this.compressionLevel = level;
      this.compressionStrategy = strategy;
      Reset();
      System.Int32 RNTRNTRNT_540 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(4910);
      return RNTRNTRNT_540;
    }
    internal void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4911);
      _codec.TotalBytesIn = _codec.TotalBytesOut = 0;
      _codec.Message = null;
      pendingCount = 0;
      nextPending = 0;
      Rfc1950BytesEmitted = false;
      status = (WantRfc1950HeaderBytes) ? INIT_STATE : BUSY_STATE;
      _codec._Adler32 = Adler.Adler32(0, null, 0, 0);
      last_flush = (int)FlushType.None;
      _InitializeTreeData();
      _InitializeLazyMatch();
      IACSharpSensor.IACSharpSensor.SensorReached(4912);
    }
    internal int End()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4913);
      if (status != INIT_STATE && status != BUSY_STATE && status != FINISH_STATE) {
        System.Int32 RNTRNTRNT_541 = ZlibConstants.Z_STREAM_ERROR;
        IACSharpSensor.IACSharpSensor.SensorReached(4914);
        return RNTRNTRNT_541;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4915);
      pending = null;
      head = null;
      prev = null;
      window = null;
      System.Int32 RNTRNTRNT_542 = status == BUSY_STATE ? ZlibConstants.Z_DATA_ERROR : ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(4916);
      return RNTRNTRNT_542;
    }
    private void SetDeflater()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4917);
      switch (config.Flavor) {
        case DeflateFlavor.Store:
          IACSharpSensor.IACSharpSensor.SensorReached(4918);
          DeflateFunction = DeflateNone;
          IACSharpSensor.IACSharpSensor.SensorReached(4919);
          break;
        case DeflateFlavor.Fast:
          IACSharpSensor.IACSharpSensor.SensorReached(4920);
          DeflateFunction = DeflateFast;
          IACSharpSensor.IACSharpSensor.SensorReached(4921);
          break;
        case DeflateFlavor.Slow:
          IACSharpSensor.IACSharpSensor.SensorReached(4922);
          DeflateFunction = DeflateSlow;
          IACSharpSensor.IACSharpSensor.SensorReached(4923);
          break;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4924);
    }
    internal int SetParams(CompressionLevel level, CompressionStrategy strategy)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4925);
      int result = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(4926);
      if (compressionLevel != level) {
        IACSharpSensor.IACSharpSensor.SensorReached(4927);
        Config newConfig = Config.Lookup(level);
        IACSharpSensor.IACSharpSensor.SensorReached(4928);
        if (newConfig.Flavor != config.Flavor && _codec.TotalBytesIn != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4929);
          result = _codec.Deflate(FlushType.Partial);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4930);
        compressionLevel = level;
        config = newConfig;
        SetDeflater();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4931);
      compressionStrategy = strategy;
      System.Int32 RNTRNTRNT_543 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(4932);
      return RNTRNTRNT_543;
    }
    internal int SetDictionary(byte[] dictionary)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4933);
      int length = dictionary.Length;
      int index = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4934);
      if (dictionary == null || status != INIT_STATE) {
        IACSharpSensor.IACSharpSensor.SensorReached(4935);
        throw new ZlibException("Stream error.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4936);
      _codec._Adler32 = Adler.Adler32(_codec._Adler32, dictionary, 0, dictionary.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(4937);
      if (length < MIN_MATCH) {
        System.Int32 RNTRNTRNT_544 = ZlibConstants.Z_OK;
        IACSharpSensor.IACSharpSensor.SensorReached(4938);
        return RNTRNTRNT_544;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4939);
      if (length > w_size - MIN_LOOKAHEAD) {
        IACSharpSensor.IACSharpSensor.SensorReached(4940);
        length = w_size - MIN_LOOKAHEAD;
        index = dictionary.Length - length;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4941);
      Array.Copy(dictionary, index, window, 0, length);
      strstart = length;
      block_start = length;
      ins_h = window[0] & 0xff;
      ins_h = (((ins_h) << hash_shift) ^ (window[1] & 0xff)) & hash_mask;
      IACSharpSensor.IACSharpSensor.SensorReached(4942);
      for (int n = 0; n <= length - MIN_MATCH; n++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4943);
        ins_h = (((ins_h) << hash_shift) ^ (window[(n) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
        prev[n & w_mask] = head[ins_h];
        head[ins_h] = (short)n;
      }
      System.Int32 RNTRNTRNT_545 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(4944);
      return RNTRNTRNT_545;
    }
    internal int Deflate(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4945);
      int old_flush;
      IACSharpSensor.IACSharpSensor.SensorReached(4946);
      if (_codec.OutputBuffer == null || (_codec.InputBuffer == null && _codec.AvailableBytesIn != 0) || (status == FINISH_STATE && flush != FlushType.Finish)) {
        IACSharpSensor.IACSharpSensor.SensorReached(4947);
        _codec.Message = _ErrorMessage[ZlibConstants.Z_NEED_DICT - (ZlibConstants.Z_STREAM_ERROR)];
        IACSharpSensor.IACSharpSensor.SensorReached(4948);
        throw new ZlibException(String.Format("Something is fishy. [{0}]", _codec.Message));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4949);
      if (_codec.AvailableBytesOut == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4950);
        _codec.Message = _ErrorMessage[ZlibConstants.Z_NEED_DICT - (ZlibConstants.Z_BUF_ERROR)];
        IACSharpSensor.IACSharpSensor.SensorReached(4951);
        throw new ZlibException("OutputBuffer is full (AvailableBytesOut == 0)");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4952);
      old_flush = last_flush;
      last_flush = (int)flush;
      IACSharpSensor.IACSharpSensor.SensorReached(4953);
      if (status == INIT_STATE) {
        IACSharpSensor.IACSharpSensor.SensorReached(4954);
        int header = (Z_DEFLATED + ((w_bits - 8) << 4)) << 8;
        int level_flags = (((int)compressionLevel - 1) & 0xff) >> 1;
        IACSharpSensor.IACSharpSensor.SensorReached(4955);
        if (level_flags > 3) {
          IACSharpSensor.IACSharpSensor.SensorReached(4956);
          level_flags = 3;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4957);
        header |= (level_flags << 6);
        IACSharpSensor.IACSharpSensor.SensorReached(4958);
        if (strstart != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4959);
          header |= PRESET_DICT;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4960);
        header += 31 - (header % 31);
        status = BUSY_STATE;
        unchecked {
          IACSharpSensor.IACSharpSensor.SensorReached(4961);
          pending[pendingCount++] = (byte)(header >> 8);
          pending[pendingCount++] = (byte)header;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4962);
        if (strstart != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4963);
          pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff000000u) >> 24);
          pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff0000) >> 16);
          pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff00) >> 8);
          pending[pendingCount++] = (byte)(_codec._Adler32 & 0xff);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4964);
        _codec._Adler32 = Adler.Adler32(0, null, 0, 0);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4965);
      if (pendingCount != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4966);
        _codec.flush_pending();
        IACSharpSensor.IACSharpSensor.SensorReached(4967);
        if (_codec.AvailableBytesOut == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4968);
          last_flush = -1;
          System.Int32 RNTRNTRNT_546 = ZlibConstants.Z_OK;
          IACSharpSensor.IACSharpSensor.SensorReached(4969);
          return RNTRNTRNT_546;
        }
      } else if (_codec.AvailableBytesIn == 0 && (int)flush <= old_flush && flush != FlushType.Finish) {
        System.Int32 RNTRNTRNT_547 = ZlibConstants.Z_OK;
        IACSharpSensor.IACSharpSensor.SensorReached(4970);
        return RNTRNTRNT_547;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4971);
      if (status == FINISH_STATE && _codec.AvailableBytesIn != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4972);
        _codec.Message = _ErrorMessage[ZlibConstants.Z_NEED_DICT - (ZlibConstants.Z_BUF_ERROR)];
        IACSharpSensor.IACSharpSensor.SensorReached(4973);
        throw new ZlibException("status == FINISH_STATE && _codec.AvailableBytesIn != 0");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4974);
      if (_codec.AvailableBytesIn != 0 || lookahead != 0 || (flush != FlushType.None && status != FINISH_STATE)) {
        IACSharpSensor.IACSharpSensor.SensorReached(4975);
        BlockState bstate = DeflateFunction(flush);
        IACSharpSensor.IACSharpSensor.SensorReached(4976);
        if (bstate == BlockState.FinishStarted || bstate == BlockState.FinishDone) {
          IACSharpSensor.IACSharpSensor.SensorReached(4977);
          status = FINISH_STATE;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4978);
        if (bstate == BlockState.NeedMore || bstate == BlockState.FinishStarted) {
          IACSharpSensor.IACSharpSensor.SensorReached(4979);
          if (_codec.AvailableBytesOut == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(4980);
            last_flush = -1;
          }
          System.Int32 RNTRNTRNT_548 = ZlibConstants.Z_OK;
          IACSharpSensor.IACSharpSensor.SensorReached(4981);
          return RNTRNTRNT_548;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4982);
        if (bstate == BlockState.BlockDone) {
          IACSharpSensor.IACSharpSensor.SensorReached(4983);
          if (flush == FlushType.Partial) {
            IACSharpSensor.IACSharpSensor.SensorReached(4984);
            _tr_align();
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(4985);
            _tr_stored_block(0, 0, false);
            IACSharpSensor.IACSharpSensor.SensorReached(4986);
            if (flush == FlushType.Full) {
              IACSharpSensor.IACSharpSensor.SensorReached(4987);
              for (int i = 0; i < hash_size; i++) {
                IACSharpSensor.IACSharpSensor.SensorReached(4988);
                head[i] = 0;
              }
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4989);
          _codec.flush_pending();
          IACSharpSensor.IACSharpSensor.SensorReached(4990);
          if (_codec.AvailableBytesOut == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(4991);
            last_flush = -1;
            System.Int32 RNTRNTRNT_549 = ZlibConstants.Z_OK;
            IACSharpSensor.IACSharpSensor.SensorReached(4992);
            return RNTRNTRNT_549;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4993);
      if (flush != FlushType.Finish) {
        System.Int32 RNTRNTRNT_550 = ZlibConstants.Z_OK;
        IACSharpSensor.IACSharpSensor.SensorReached(4994);
        return RNTRNTRNT_550;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4995);
      if (!WantRfc1950HeaderBytes || Rfc1950BytesEmitted) {
        System.Int32 RNTRNTRNT_551 = ZlibConstants.Z_STREAM_END;
        IACSharpSensor.IACSharpSensor.SensorReached(4996);
        return RNTRNTRNT_551;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4997);
      pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff000000u) >> 24);
      pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff0000) >> 16);
      pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff00) >> 8);
      pending[pendingCount++] = (byte)(_codec._Adler32 & 0xff);
      _codec.flush_pending();
      Rfc1950BytesEmitted = true;
      System.Int32 RNTRNTRNT_552 = pendingCount != 0 ? ZlibConstants.Z_OK : ZlibConstants.Z_STREAM_END;
      IACSharpSensor.IACSharpSensor.SensorReached(4998);
      return RNTRNTRNT_552;
    }
  }
}
