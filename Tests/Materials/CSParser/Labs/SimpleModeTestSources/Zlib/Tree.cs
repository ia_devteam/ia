using System;
namespace Ionic.Zlib
{
  sealed class Tree
  {
    private static readonly int HEAP_SIZE = (2 * InternalConstants.L_CODES + 1);
    static internal readonly int[] ExtraLengthBits = new int[] {
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      1,
      1,
      1,
      1,
      2,
      2,
      2,
      2,
      3,
      3,
      3,
      3,
      4,
      4,
      4,
      4,
      5,
      5,
      5,
      5,
      0
    };
    static internal readonly int[] ExtraDistanceBits = new int[] {
      0,
      0,
      0,
      0,
      1,
      1,
      2,
      2,
      3,
      3,
      4,
      4,
      5,
      5,
      6,
      6,
      7,
      7,
      8,
      8,
      9,
      9,
      10,
      10,
      11,
      11,
      12,
      12,
      13,
      13
    };
    static internal readonly int[] extra_blbits = new int[] {
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      2,
      3,
      7
    };
    static internal readonly sbyte[] bl_order = new sbyte[] {
      16,
      17,
      18,
      0,
      8,
      7,
      9,
      6,
      10,
      5,
      11,
      4,
      12,
      3,
      13,
      2,
      14,
      1,
      15
    };
    internal const int Buf_size = 8 * 2;
    private static readonly sbyte[] _dist_code = new sbyte[] {
      0,
      1,
      2,
      3,
      4,
      4,
      5,
      5,
      6,
      6,
      6,
      6,
      7,
      7,
      7,
      7,
      8,
      8,
      8,
      8,
      8,
      8,
      8,
      8,
      9,
      9,
      9,
      9,
      9,
      9,
      9,
      9,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      10,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      11,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      12,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      13,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      14,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      15,
      0,
      0,
      16,
      17,
      18,
      18,
      19,
      19,
      20,
      20,
      20,
      20,
      21,
      21,
      21,
      21,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      28,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29,
      29
    };
    static internal readonly sbyte[] LengthCode = new sbyte[] {
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      8,
      9,
      9,
      10,
      10,
      11,
      11,
      12,
      12,
      12,
      12,
      13,
      13,
      13,
      13,
      14,
      14,
      14,
      14,
      15,
      15,
      15,
      15,
      16,
      16,
      16,
      16,
      16,
      16,
      16,
      16,
      17,
      17,
      17,
      17,
      17,
      17,
      17,
      17,
      18,
      18,
      18,
      18,
      18,
      18,
      18,
      18,
      19,
      19,
      19,
      19,
      19,
      19,
      19,
      19,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      20,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      21,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      22,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      23,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      24,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      25,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      26,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      27,
      28
    };
    static internal readonly int[] LengthBase = new int[] {
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      10,
      12,
      14,
      16,
      20,
      24,
      28,
      32,
      40,
      48,
      56,
      64,
      80,
      96,
      112,
      128,
      160,
      192,
      224,
      0
    };
    static internal readonly int[] DistanceBase = new int[] {
      0,
      1,
      2,
      3,
      4,
      6,
      8,
      12,
      16,
      24,
      32,
      48,
      64,
      96,
      128,
      192,
      256,
      384,
      512,
      768,
      1024,
      1536,
      2048,
      3072,
      4096,
      6144,
      8192,
      12288,
      16384,
      24576
    };
    static internal int DistanceCode(int dist)
    {
      System.Int32 RNTRNTRNT_672 = (dist < 256) ? _dist_code[dist] : _dist_code[256 + SharedUtils.URShift(dist, 7)];
      IACSharpSensor.IACSharpSensor.SensorReached(5920);
      return RNTRNTRNT_672;
    }
    internal short[] dyn_tree;
    internal int max_code;
    internal StaticTree staticTree;
    internal void gen_bitlen(DeflateManager s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5921);
      short[] tree = dyn_tree;
      short[] stree = staticTree.treeCodes;
      int[] extra = staticTree.extraBits;
      int base_Renamed = staticTree.extraBase;
      int max_length = staticTree.maxLength;
      int h;
      int n, m;
      int bits;
      int xbits;
      short f;
      int overflow = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5922);
      for (bits = 0; bits <= InternalConstants.MAX_BITS; bits++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5923);
        s.bl_count[bits] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5924);
      tree[s.heap[s.heap_max] * 2 + 1] = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5925);
      for (h = s.heap_max + 1; h < HEAP_SIZE; h++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5926);
        n = s.heap[h];
        bits = tree[tree[n * 2 + 1] * 2 + 1] + 1;
        IACSharpSensor.IACSharpSensor.SensorReached(5927);
        if (bits > max_length) {
          IACSharpSensor.IACSharpSensor.SensorReached(5928);
          bits = max_length;
          overflow++;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5929);
        tree[n * 2 + 1] = (short)bits;
        IACSharpSensor.IACSharpSensor.SensorReached(5930);
        if (n > max_code) {
          IACSharpSensor.IACSharpSensor.SensorReached(5931);
          continue;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5932);
        s.bl_count[bits]++;
        xbits = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(5933);
        if (n >= base_Renamed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5934);
          xbits = extra[n - base_Renamed];
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5935);
        f = tree[n * 2];
        s.opt_len += f * (bits + xbits);
        IACSharpSensor.IACSharpSensor.SensorReached(5936);
        if (stree != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(5937);
          s.static_len += f * (stree[n * 2 + 1] + xbits);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5938);
      if (overflow == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5939);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5940);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(5941);
        bits = max_length - 1;
        IACSharpSensor.IACSharpSensor.SensorReached(5942);
        while (s.bl_count[bits] == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5943);
          bits--;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5944);
        s.bl_count[bits]--;
        s.bl_count[bits + 1] = (short)(s.bl_count[bits + 1] + 2);
        s.bl_count[max_length]--;
        overflow -= 2;
      } while (overflow > 0);
      IACSharpSensor.IACSharpSensor.SensorReached(5945);
      for (bits = max_length; bits != 0; bits--) {
        IACSharpSensor.IACSharpSensor.SensorReached(5946);
        n = s.bl_count[bits];
        IACSharpSensor.IACSharpSensor.SensorReached(5947);
        while (n != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5948);
          m = s.heap[--h];
          IACSharpSensor.IACSharpSensor.SensorReached(5949);
          if (m > max_code) {
            IACSharpSensor.IACSharpSensor.SensorReached(5950);
            continue;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5951);
          if (tree[m * 2 + 1] != bits) {
            IACSharpSensor.IACSharpSensor.SensorReached(5952);
            s.opt_len = (int)(s.opt_len + ((long)bits - (long)tree[m * 2 + 1]) * (long)tree[m * 2]);
            tree[m * 2 + 1] = (short)bits;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5953);
          n--;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5954);
    }
    internal void build_tree(DeflateManager s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5955);
      short[] tree = dyn_tree;
      short[] stree = staticTree.treeCodes;
      int elems = staticTree.elems;
      int n, m;
      int max_code = -1;
      int node;
      s.heap_len = 0;
      s.heap_max = HEAP_SIZE;
      IACSharpSensor.IACSharpSensor.SensorReached(5956);
      for (n = 0; n < elems; n++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5957);
        if (tree[n * 2] != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5958);
          s.heap[++s.heap_len] = max_code = n;
          s.depth[n] = 0;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5959);
          tree[n * 2 + 1] = 0;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5960);
      while (s.heap_len < 2) {
        IACSharpSensor.IACSharpSensor.SensorReached(5961);
        node = s.heap[++s.heap_len] = (max_code < 2 ? ++max_code : 0);
        tree[node * 2] = 1;
        s.depth[node] = 0;
        s.opt_len--;
        IACSharpSensor.IACSharpSensor.SensorReached(5962);
        if (stree != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(5963);
          s.static_len -= stree[node * 2 + 1];
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5964);
      this.max_code = max_code;
      IACSharpSensor.IACSharpSensor.SensorReached(5965);
      for (n = s.heap_len / 2; n >= 1; n--) {
        IACSharpSensor.IACSharpSensor.SensorReached(5966);
        s.pqdownheap(tree, n);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5967);
      node = elems;
      IACSharpSensor.IACSharpSensor.SensorReached(5968);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(5969);
        n = s.heap[1];
        s.heap[1] = s.heap[s.heap_len--];
        s.pqdownheap(tree, 1);
        m = s.heap[1];
        s.heap[--s.heap_max] = n;
        s.heap[--s.heap_max] = m;
        tree[node * 2] = unchecked((short)(tree[n * 2] + tree[m * 2]));
        s.depth[node] = (sbyte)(System.Math.Max((byte)s.depth[n], (byte)s.depth[m]) + 1);
        tree[n * 2 + 1] = tree[m * 2 + 1] = (short)node;
        s.heap[1] = node++;
        s.pqdownheap(tree, 1);
      } while (s.heap_len >= 2);
      IACSharpSensor.IACSharpSensor.SensorReached(5970);
      s.heap[--s.heap_max] = s.heap[1];
      gen_bitlen(s);
      gen_codes(tree, max_code, s.bl_count);
      IACSharpSensor.IACSharpSensor.SensorReached(5971);
    }
    static internal void gen_codes(short[] tree, int max_code, short[] bl_count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5972);
      short[] next_code = new short[InternalConstants.MAX_BITS + 1];
      short code = 0;
      int bits;
      int n;
      IACSharpSensor.IACSharpSensor.SensorReached(5973);
      for (bits = 1; bits <= InternalConstants.MAX_BITS; bits++) {
        unchecked {
          IACSharpSensor.IACSharpSensor.SensorReached(5974);
          next_code[bits] = code = (short)((code + bl_count[bits - 1]) << 1);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5975);
      for (n = 0; n <= max_code; n++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5976);
        int len = tree[n * 2 + 1];
        IACSharpSensor.IACSharpSensor.SensorReached(5977);
        if (len == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5978);
          continue;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5979);
        tree[n * 2] = unchecked((short)(bi_reverse(next_code[len]++, len)));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5980);
    }
    static internal int bi_reverse(int code, int len)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5981);
      int res = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5982);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(5983);
        res |= code & 1;
        code >>= 1;
        res <<= 1;
      } while (--len > 0);
      System.Int32 RNTRNTRNT_673 = res >> 1;
      IACSharpSensor.IACSharpSensor.SensorReached(5984);
      return RNTRNTRNT_673;
    }
  }
}
