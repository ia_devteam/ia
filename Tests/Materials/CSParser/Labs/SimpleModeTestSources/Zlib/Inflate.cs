using System;
namespace Ionic.Zlib
{
  sealed class InflateBlocks
  {
    private const int MANY = 1440;
    static internal readonly int[] border = new int[] {
      16,
      17,
      18,
      0,
      8,
      7,
      9,
      6,
      10,
      5,
      11,
      4,
      12,
      3,
      13,
      2,
      14,
      1,
      15
    };
    private enum InflateBlockMode
    {
      TYPE = 0,
      LENS = 1,
      STORED = 2,
      TABLE = 3,
      BTREE = 4,
      DTREE = 5,
      CODES = 6,
      DRY = 7,
      DONE = 8,
      BAD = 9
    }
    private InflateBlockMode mode;
    internal int left;
    internal int table;
    internal int index;
    internal int[] blens;
    internal int[] bb = new int[1];
    internal int[] tb = new int[1];
    internal InflateCodes codes = new InflateCodes();
    internal int last;
    internal ZlibCodec _codec;
    internal int bitk;
    internal int bitb;
    internal int[] hufts;
    internal byte[] window;
    internal int end;
    internal int readAt;
    internal int writeAt;
    internal System.Object checkfn;
    internal uint check;
    internal InfTree inftree = new InfTree();
    internal InflateBlocks(ZlibCodec codec, System.Object checkfn, int w)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5181);
      _codec = codec;
      hufts = new int[MANY * 3];
      window = new byte[w];
      end = w;
      this.checkfn = checkfn;
      mode = InflateBlockMode.TYPE;
      Reset();
      IACSharpSensor.IACSharpSensor.SensorReached(5182);
    }
    internal uint Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5183);
      uint oldCheck = check;
      mode = InflateBlockMode.TYPE;
      bitk = 0;
      bitb = 0;
      readAt = writeAt = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5184);
      if (checkfn != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(5185);
        _codec._Adler32 = check = Adler.Adler32(0, null, 0, 0);
      }
      System.UInt32 RNTRNTRNT_588 = oldCheck;
      IACSharpSensor.IACSharpSensor.SensorReached(5186);
      return RNTRNTRNT_588;
    }
    internal int Process(int r)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5187);
      int t;
      int b;
      int k;
      int p;
      int n;
      int q;
      int m;
      p = _codec.NextIn;
      n = _codec.AvailableBytesIn;
      b = bitb;
      k = bitk;
      q = writeAt;
      m = (int)(q < readAt ? readAt - q - 1 : end - q);
      IACSharpSensor.IACSharpSensor.SensorReached(5188);
      while (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(5189);
        switch (mode) {
          case InflateBlockMode.TYPE:
            IACSharpSensor.IACSharpSensor.SensorReached(5190);
            while (k < (3)) {
              IACSharpSensor.IACSharpSensor.SensorReached(5191);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5192);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5193);
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_589 = Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5194);
                return RNTRNTRNT_589;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5195);
              n--;
              b |= (_codec.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5196);
            t = (int)(b & 7);
            last = t & 1;
            IACSharpSensor.IACSharpSensor.SensorReached(5197);
            switch ((uint)t >> 1) {
              case 0:
                IACSharpSensor.IACSharpSensor.SensorReached(5198);
                b >>= 3;
                k -= (3);
                t = k & 7;
                b >>= t;
                k -= t;
                mode = InflateBlockMode.LENS;
                IACSharpSensor.IACSharpSensor.SensorReached(5199);
                break;
              case 1:
                IACSharpSensor.IACSharpSensor.SensorReached(5200);
                int[] bl = new int[1];
                int[] bd = new int[1];
                int[][] tl = new int[1][];
                int[][] td = new int[1][];
                InfTree.inflate_trees_fixed(bl, bd, tl, td, _codec);
                codes.Init(bl[0], bd[0], tl[0], 0, td[0], 0);
                b >>= 3;
                k -= 3;
                mode = InflateBlockMode.CODES;
                IACSharpSensor.IACSharpSensor.SensorReached(5201);
                break;
              case 2:
                IACSharpSensor.IACSharpSensor.SensorReached(5202);
                b >>= 3;
                k -= 3;
                mode = InflateBlockMode.TABLE;
                IACSharpSensor.IACSharpSensor.SensorReached(5203);
                break;
              case 3:
                IACSharpSensor.IACSharpSensor.SensorReached(5204);
                b >>= 3;
                k -= 3;
                mode = InflateBlockMode.BAD;
                _codec.Message = "invalid block type";
                r = ZlibConstants.Z_DATA_ERROR;
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_590 = Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5205);
                return RNTRNTRNT_590;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5206);
            break;
          case InflateBlockMode.LENS:
            IACSharpSensor.IACSharpSensor.SensorReached(5207);
            while (k < (32)) {
              IACSharpSensor.IACSharpSensor.SensorReached(5208);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5209);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5210);
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_591 = Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5211);
                return RNTRNTRNT_591;
              }
              ;
              n--;
              b |= (_codec.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5212);
            if ((((~b) >> 16) & 0xffff) != (b & 0xffff)) {
              IACSharpSensor.IACSharpSensor.SensorReached(5213);
              mode = InflateBlockMode.BAD;
              _codec.Message = "invalid stored block lengths";
              r = ZlibConstants.Z_DATA_ERROR;
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_592 = Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(5214);
              return RNTRNTRNT_592;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5215);
            left = (b & 0xffff);
            b = k = 0;
            mode = left != 0 ? InflateBlockMode.STORED : (last != 0 ? InflateBlockMode.DRY : InflateBlockMode.TYPE);
            IACSharpSensor.IACSharpSensor.SensorReached(5216);
            break;
          case InflateBlockMode.STORED:
            IACSharpSensor.IACSharpSensor.SensorReached(5217);
            if (n == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5218);
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_593 = Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(5219);
              return RNTRNTRNT_593;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5220);
            if (m == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5221);
              if (q == end && readAt != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5222);
                q = 0;
                m = (int)(q < readAt ? readAt - q - 1 : end - q);
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5223);
              if (m == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5224);
                writeAt = q;
                r = Flush(r);
                q = writeAt;
                m = (int)(q < readAt ? readAt - q - 1 : end - q);
                IACSharpSensor.IACSharpSensor.SensorReached(5225);
                if (q == end && readAt != 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5226);
                  q = 0;
                  m = (int)(q < readAt ? readAt - q - 1 : end - q);
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5227);
                if (m == 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5228);
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_594 = Flush(r);
                  IACSharpSensor.IACSharpSensor.SensorReached(5229);
                  return RNTRNTRNT_594;
                }
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5230);
            r = ZlibConstants.Z_OK;
            t = left;
            IACSharpSensor.IACSharpSensor.SensorReached(5231);
            if (t > n) {
              IACSharpSensor.IACSharpSensor.SensorReached(5232);
              t = n;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5233);
            if (t > m) {
              IACSharpSensor.IACSharpSensor.SensorReached(5234);
              t = m;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5235);
            Array.Copy(_codec.InputBuffer, p, window, q, t);
            p += t;
            n -= t;
            q += t;
            m -= t;
            IACSharpSensor.IACSharpSensor.SensorReached(5236);
            if ((left -= t) != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5237);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5238);
            mode = last != 0 ? InflateBlockMode.DRY : InflateBlockMode.TYPE;
            IACSharpSensor.IACSharpSensor.SensorReached(5239);
            break;
          case InflateBlockMode.TABLE:
            IACSharpSensor.IACSharpSensor.SensorReached(5240);
            while (k < (14)) {
              IACSharpSensor.IACSharpSensor.SensorReached(5241);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5242);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5243);
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_595 = Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5244);
                return RNTRNTRNT_595;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5245);
              n--;
              b |= (_codec.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5246);
            table = t = (b & 0x3fff);
            IACSharpSensor.IACSharpSensor.SensorReached(5247);
            if ((t & 0x1f) > 29 || ((t >> 5) & 0x1f) > 29) {
              IACSharpSensor.IACSharpSensor.SensorReached(5248);
              mode = InflateBlockMode.BAD;
              _codec.Message = "too many length or distance symbols";
              r = ZlibConstants.Z_DATA_ERROR;
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_596 = Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(5249);
              return RNTRNTRNT_596;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5250);
            t = 258 + (t & 0x1f) + ((t >> 5) & 0x1f);
            IACSharpSensor.IACSharpSensor.SensorReached(5251);
            if (blens == null || blens.Length < t) {
              IACSharpSensor.IACSharpSensor.SensorReached(5252);
              blens = new int[t];
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(5253);
              Array.Clear(blens, 0, t);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5254);
            b >>= 14;
            k -= 14;
            index = 0;
            mode = InflateBlockMode.BTREE;
            IACSharpSensor.IACSharpSensor.SensorReached(5255);
            goto case InflateBlockMode.BTREE;
          case InflateBlockMode.BTREE:
            IACSharpSensor.IACSharpSensor.SensorReached(5256);
            while (index < 4 + (table >> 10)) {
              IACSharpSensor.IACSharpSensor.SensorReached(5257);
              while (k < (3)) {
                IACSharpSensor.IACSharpSensor.SensorReached(5258);
                if (n != 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5259);
                  r = ZlibConstants.Z_OK;
                } else {
                  IACSharpSensor.IACSharpSensor.SensorReached(5260);
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_597 = Flush(r);
                  IACSharpSensor.IACSharpSensor.SensorReached(5261);
                  return RNTRNTRNT_597;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5262);
                n--;
                b |= (_codec.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5263);
              blens[border[index++]] = b & 7;
              b >>= 3;
              k -= 3;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5264);
            while (index < 19) {
              IACSharpSensor.IACSharpSensor.SensorReached(5265);
              blens[border[index++]] = 0;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5266);
            bb[0] = 7;
            t = inftree.inflate_trees_bits(blens, bb, tb, hufts, _codec);
            IACSharpSensor.IACSharpSensor.SensorReached(5267);
            if (t != ZlibConstants.Z_OK) {
              IACSharpSensor.IACSharpSensor.SensorReached(5268);
              r = t;
              IACSharpSensor.IACSharpSensor.SensorReached(5269);
              if (r == ZlibConstants.Z_DATA_ERROR) {
                IACSharpSensor.IACSharpSensor.SensorReached(5270);
                blens = null;
                mode = InflateBlockMode.BAD;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5271);
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_598 = Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(5272);
              return RNTRNTRNT_598;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5273);
            index = 0;
            mode = InflateBlockMode.DTREE;
            IACSharpSensor.IACSharpSensor.SensorReached(5274);
            goto case InflateBlockMode.DTREE;
          case InflateBlockMode.DTREE:
            IACSharpSensor.IACSharpSensor.SensorReached(5275);
            while (true) {
              IACSharpSensor.IACSharpSensor.SensorReached(5276);
              t = table;
              IACSharpSensor.IACSharpSensor.SensorReached(5277);
              if (!(index < 258 + (t & 0x1f) + ((t >> 5) & 0x1f))) {
                IACSharpSensor.IACSharpSensor.SensorReached(5278);
                break;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5279);
              int i, j, c;
              t = bb[0];
              IACSharpSensor.IACSharpSensor.SensorReached(5280);
              while (k < t) {
                IACSharpSensor.IACSharpSensor.SensorReached(5281);
                if (n != 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5282);
                  r = ZlibConstants.Z_OK;
                } else {
                  IACSharpSensor.IACSharpSensor.SensorReached(5283);
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_599 = Flush(r);
                  IACSharpSensor.IACSharpSensor.SensorReached(5284);
                  return RNTRNTRNT_599;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5285);
                n--;
                b |= (_codec.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5286);
              t = hufts[(tb[0] + (b & InternalInflateConstants.InflateMask[t])) * 3 + 1];
              c = hufts[(tb[0] + (b & InternalInflateConstants.InflateMask[t])) * 3 + 2];
              IACSharpSensor.IACSharpSensor.SensorReached(5287);
              if (c < 16) {
                IACSharpSensor.IACSharpSensor.SensorReached(5288);
                b >>= t;
                k -= t;
                blens[index++] = c;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5289);
                i = c == 18 ? 7 : c - 14;
                j = c == 18 ? 11 : 3;
                IACSharpSensor.IACSharpSensor.SensorReached(5290);
                while (k < (t + i)) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5291);
                  if (n != 0) {
                    IACSharpSensor.IACSharpSensor.SensorReached(5292);
                    r = ZlibConstants.Z_OK;
                  } else {
                    IACSharpSensor.IACSharpSensor.SensorReached(5293);
                    bitb = b;
                    bitk = k;
                    _codec.AvailableBytesIn = n;
                    _codec.TotalBytesIn += p - _codec.NextIn;
                    _codec.NextIn = p;
                    writeAt = q;
                    System.Int32 RNTRNTRNT_600 = Flush(r);
                    IACSharpSensor.IACSharpSensor.SensorReached(5294);
                    return RNTRNTRNT_600;
                  }
                  IACSharpSensor.IACSharpSensor.SensorReached(5295);
                  n--;
                  b |= (_codec.InputBuffer[p++] & 0xff) << k;
                  k += 8;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5296);
                b >>= t;
                k -= t;
                j += (b & InternalInflateConstants.InflateMask[i]);
                b >>= i;
                k -= i;
                i = index;
                t = table;
                IACSharpSensor.IACSharpSensor.SensorReached(5297);
                if (i + j > 258 + (t & 0x1f) + ((t >> 5) & 0x1f) || (c == 16 && i < 1)) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5298);
                  blens = null;
                  mode = InflateBlockMode.BAD;
                  _codec.Message = "invalid bit length repeat";
                  r = ZlibConstants.Z_DATA_ERROR;
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_601 = Flush(r);
                  IACSharpSensor.IACSharpSensor.SensorReached(5299);
                  return RNTRNTRNT_601;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5300);
                c = (c == 16) ? blens[i - 1] : 0;
                IACSharpSensor.IACSharpSensor.SensorReached(5301);
                do {
                  IACSharpSensor.IACSharpSensor.SensorReached(5302);
                  blens[i++] = c;
                } while (--j != 0);
                IACSharpSensor.IACSharpSensor.SensorReached(5303);
                index = i;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5304);
            tb[0] = -1;
            IACSharpSensor.IACSharpSensor.SensorReached(5312);
            
            {
              IACSharpSensor.IACSharpSensor.SensorReached(5305);
              int[] bl = new int[] { 9 };
              int[] bd = new int[] { 6 };
              int[] tl = new int[1];
              int[] td = new int[1];
              t = table;
              t = inftree.inflate_trees_dynamic(257 + (t & 0x1f), 1 + ((t >> 5) & 0x1f), blens, bl, bd, tl, td, hufts, _codec);
              IACSharpSensor.IACSharpSensor.SensorReached(5306);
              if (t != ZlibConstants.Z_OK) {
                IACSharpSensor.IACSharpSensor.SensorReached(5307);
                if (t == ZlibConstants.Z_DATA_ERROR) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5308);
                  blens = null;
                  mode = InflateBlockMode.BAD;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5309);
                r = t;
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_602 = Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5310);
                return RNTRNTRNT_602;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5311);
              codes.Init(bl[0], bd[0], hufts, tl[0], hufts, td[0]);
            }

            mode = InflateBlockMode.CODES;
            IACSharpSensor.IACSharpSensor.SensorReached(5313);
            goto case InflateBlockMode.CODES;
          case InflateBlockMode.CODES:
            IACSharpSensor.IACSharpSensor.SensorReached(5314);
            bitb = b;
            bitk = k;
            _codec.AvailableBytesIn = n;
            _codec.TotalBytesIn += p - _codec.NextIn;
            _codec.NextIn = p;
            writeAt = q;
            r = codes.Process(this, r);
            IACSharpSensor.IACSharpSensor.SensorReached(5315);
            if (r != ZlibConstants.Z_STREAM_END) {
              System.Int32 RNTRNTRNT_603 = Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(5316);
              return RNTRNTRNT_603;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5317);
            r = ZlibConstants.Z_OK;
            p = _codec.NextIn;
            n = _codec.AvailableBytesIn;
            b = bitb;
            k = bitk;
            q = writeAt;
            m = (int)(q < readAt ? readAt - q - 1 : end - q);
            IACSharpSensor.IACSharpSensor.SensorReached(5318);
            if (last == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5319);
              mode = InflateBlockMode.TYPE;
              IACSharpSensor.IACSharpSensor.SensorReached(5320);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5321);
            mode = InflateBlockMode.DRY;
            IACSharpSensor.IACSharpSensor.SensorReached(5322);
            goto case InflateBlockMode.DRY;
          case InflateBlockMode.DRY:
            IACSharpSensor.IACSharpSensor.SensorReached(5323);
            writeAt = q;
            r = Flush(r);
            q = writeAt;
            m = (int)(q < readAt ? readAt - q - 1 : end - q);
            IACSharpSensor.IACSharpSensor.SensorReached(5324);
            if (readAt != writeAt) {
              IACSharpSensor.IACSharpSensor.SensorReached(5325);
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_604 = Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(5326);
              return RNTRNTRNT_604;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5327);
            mode = InflateBlockMode.DONE;
            IACSharpSensor.IACSharpSensor.SensorReached(5328);
            goto case InflateBlockMode.DONE;
          case InflateBlockMode.DONE:
            IACSharpSensor.IACSharpSensor.SensorReached(5329);
            r = ZlibConstants.Z_STREAM_END;
            bitb = b;
            bitk = k;
            _codec.AvailableBytesIn = n;
            _codec.TotalBytesIn += p - _codec.NextIn;
            _codec.NextIn = p;
            writeAt = q;
            System.Int32 RNTRNTRNT_605 = Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5330);
            return RNTRNTRNT_605;
          case InflateBlockMode.BAD:
            IACSharpSensor.IACSharpSensor.SensorReached(5331);
            r = ZlibConstants.Z_DATA_ERROR;
            bitb = b;
            bitk = k;
            _codec.AvailableBytesIn = n;
            _codec.TotalBytesIn += p - _codec.NextIn;
            _codec.NextIn = p;
            writeAt = q;
            System.Int32 RNTRNTRNT_606 = Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5332);
            return RNTRNTRNT_606;
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(5333);
            r = ZlibConstants.Z_STREAM_ERROR;
            bitb = b;
            bitk = k;
            _codec.AvailableBytesIn = n;
            _codec.TotalBytesIn += p - _codec.NextIn;
            _codec.NextIn = p;
            writeAt = q;
            System.Int32 RNTRNTRNT_607 = Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5334);
            return RNTRNTRNT_607;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5335);
    }
    internal void Free()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5336);
      Reset();
      window = null;
      hufts = null;
      IACSharpSensor.IACSharpSensor.SensorReached(5337);
    }
    internal void SetDictionary(byte[] d, int start, int n)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5338);
      Array.Copy(d, start, window, 0, n);
      readAt = writeAt = n;
      IACSharpSensor.IACSharpSensor.SensorReached(5339);
    }
    internal int SyncPoint()
    {
      System.Int32 RNTRNTRNT_608 = mode == InflateBlockMode.LENS ? 1 : 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5340);
      return RNTRNTRNT_608;
    }
    internal int Flush(int r)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5341);
      int nBytes;
      IACSharpSensor.IACSharpSensor.SensorReached(5342);
      for (int pass = 0; pass < 2; pass++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5343);
        if (pass == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5344);
          nBytes = (int)((readAt <= writeAt ? writeAt : end) - readAt);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5345);
          nBytes = writeAt - readAt;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5346);
        if (nBytes == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5347);
          if (r == ZlibConstants.Z_BUF_ERROR) {
            IACSharpSensor.IACSharpSensor.SensorReached(5348);
            r = ZlibConstants.Z_OK;
          }
          System.Int32 RNTRNTRNT_609 = r;
          IACSharpSensor.IACSharpSensor.SensorReached(5349);
          return RNTRNTRNT_609;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5350);
        if (nBytes > _codec.AvailableBytesOut) {
          IACSharpSensor.IACSharpSensor.SensorReached(5351);
          nBytes = _codec.AvailableBytesOut;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5352);
        if (nBytes != 0 && r == ZlibConstants.Z_BUF_ERROR) {
          IACSharpSensor.IACSharpSensor.SensorReached(5353);
          r = ZlibConstants.Z_OK;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5354);
        _codec.AvailableBytesOut -= nBytes;
        _codec.TotalBytesOut += nBytes;
        IACSharpSensor.IACSharpSensor.SensorReached(5355);
        if (checkfn != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(5356);
          _codec._Adler32 = check = Adler.Adler32(check, window, readAt, nBytes);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5357);
        Array.Copy(window, readAt, _codec.OutputBuffer, _codec.NextOut, nBytes);
        _codec.NextOut += nBytes;
        readAt += nBytes;
        IACSharpSensor.IACSharpSensor.SensorReached(5358);
        if (readAt == end && pass == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5359);
          readAt = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(5360);
          if (writeAt == end) {
            IACSharpSensor.IACSharpSensor.SensorReached(5361);
            writeAt = 0;
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5362);
          pass++;
        }
      }
      System.Int32 RNTRNTRNT_610 = r;
      IACSharpSensor.IACSharpSensor.SensorReached(5363);
      return RNTRNTRNT_610;
    }
  }
  static internal class InternalInflateConstants
  {
    static internal readonly int[] InflateMask = new int[] {
      0x0,
      0x1,
      0x3,
      0x7,
      0xf,
      0x1f,
      0x3f,
      0x7f,
      0xff,
      0x1ff,
      0x3ff,
      0x7ff,
      0xfff,
      0x1fff,
      0x3fff,
      0x7fff,
      0xffff
    };
  }
  sealed class InflateCodes
  {
    private const int START = 0;
    private const int LEN = 1;
    private const int LENEXT = 2;
    private const int DIST = 3;
    private const int DISTEXT = 4;
    private const int COPY = 5;
    private const int LIT = 6;
    private const int WASH = 7;
    private const int END = 8;
    private const int BADCODE = 9;
    internal int mode;
    internal int len;
    internal int[] tree;
    internal int tree_index = 0;
    internal int need;
    internal int lit;
    internal int bitsToGet;
    internal int dist;
    internal byte lbits;
    internal byte dbits;
    internal int[] ltree;
    internal int ltree_index;
    internal int[] dtree;
    internal int dtree_index;
    internal InflateCodes()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5364);
    }
    internal void Init(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5365);
      mode = START;
      lbits = (byte)bl;
      dbits = (byte)bd;
      ltree = tl;
      ltree_index = tl_index;
      dtree = td;
      dtree_index = td_index;
      tree = null;
      IACSharpSensor.IACSharpSensor.SensorReached(5366);
    }
    internal int Process(InflateBlocks blocks, int r)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5367);
      int j;
      int tindex;
      int e;
      int b = 0;
      int k = 0;
      int p = 0;
      int n;
      int q;
      int m;
      int f;
      ZlibCodec z = blocks._codec;
      p = z.NextIn;
      n = z.AvailableBytesIn;
      b = blocks.bitb;
      k = blocks.bitk;
      q = blocks.writeAt;
      m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
      IACSharpSensor.IACSharpSensor.SensorReached(5368);
      while (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(5369);
        switch (mode) {
          case START:
            IACSharpSensor.IACSharpSensor.SensorReached(5370);
            if (m >= 258 && n >= 10) {
              IACSharpSensor.IACSharpSensor.SensorReached(5371);
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              r = InflateFast(lbits, dbits, ltree, ltree_index, dtree, dtree_index, blocks, z);
              p = z.NextIn;
              n = z.AvailableBytesIn;
              b = blocks.bitb;
              k = blocks.bitk;
              q = blocks.writeAt;
              m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
              IACSharpSensor.IACSharpSensor.SensorReached(5372);
              if (r != ZlibConstants.Z_OK) {
                IACSharpSensor.IACSharpSensor.SensorReached(5373);
                mode = (r == ZlibConstants.Z_STREAM_END) ? WASH : BADCODE;
                IACSharpSensor.IACSharpSensor.SensorReached(5374);
                break;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5375);
            need = lbits;
            tree = ltree;
            tree_index = ltree_index;
            mode = LEN;
            IACSharpSensor.IACSharpSensor.SensorReached(5376);
            goto case LEN;
          case LEN:
            IACSharpSensor.IACSharpSensor.SensorReached(5377);
            j = need;
            IACSharpSensor.IACSharpSensor.SensorReached(5378);
            while (k < j) {
              IACSharpSensor.IACSharpSensor.SensorReached(5379);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5380);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5381);
                blocks.bitb = b;
                blocks.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                blocks.writeAt = q;
                System.Int32 RNTRNTRNT_611 = blocks.Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5382);
                return RNTRNTRNT_611;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5383);
              n--;
              b |= (z.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5384);
            tindex = (tree_index + (b & InternalInflateConstants.InflateMask[j])) * 3;
            b >>= (tree[tindex + 1]);
            k -= (tree[tindex + 1]);
            e = tree[tindex];
            IACSharpSensor.IACSharpSensor.SensorReached(5385);
            if (e == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5386);
              lit = tree[tindex + 2];
              mode = LIT;
              IACSharpSensor.IACSharpSensor.SensorReached(5387);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5388);
            if ((e & 16) != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5389);
              bitsToGet = e & 15;
              len = tree[tindex + 2];
              mode = LENEXT;
              IACSharpSensor.IACSharpSensor.SensorReached(5390);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5391);
            if ((e & 64) == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5392);
              need = e;
              tree_index = tindex / 3 + tree[tindex + 2];
              IACSharpSensor.IACSharpSensor.SensorReached(5393);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5394);
            if ((e & 32) != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5395);
              mode = WASH;
              IACSharpSensor.IACSharpSensor.SensorReached(5396);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5397);
            mode = BADCODE;
            z.Message = "invalid literal/length code";
            r = ZlibConstants.Z_DATA_ERROR;
            blocks.bitb = b;
            blocks.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            blocks.writeAt = q;
            System.Int32 RNTRNTRNT_612 = blocks.Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5398);
            return RNTRNTRNT_612;
          case LENEXT:
            IACSharpSensor.IACSharpSensor.SensorReached(5399);
            j = bitsToGet;
            IACSharpSensor.IACSharpSensor.SensorReached(5400);
            while (k < j) {
              IACSharpSensor.IACSharpSensor.SensorReached(5401);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5402);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5403);
                blocks.bitb = b;
                blocks.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                blocks.writeAt = q;
                System.Int32 RNTRNTRNT_613 = blocks.Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5404);
                return RNTRNTRNT_613;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5405);
              n--;
              b |= (z.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5406);
            len += (b & InternalInflateConstants.InflateMask[j]);
            b >>= j;
            k -= j;
            need = dbits;
            tree = dtree;
            tree_index = dtree_index;
            mode = DIST;
            IACSharpSensor.IACSharpSensor.SensorReached(5407);
            goto case DIST;
          case DIST:
            IACSharpSensor.IACSharpSensor.SensorReached(5408);
            j = need;
            IACSharpSensor.IACSharpSensor.SensorReached(5409);
            while (k < j) {
              IACSharpSensor.IACSharpSensor.SensorReached(5410);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5411);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5412);
                blocks.bitb = b;
                blocks.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                blocks.writeAt = q;
                System.Int32 RNTRNTRNT_614 = blocks.Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5413);
                return RNTRNTRNT_614;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5414);
              n--;
              b |= (z.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5415);
            tindex = (tree_index + (b & InternalInflateConstants.InflateMask[j])) * 3;
            b >>= tree[tindex + 1];
            k -= tree[tindex + 1];
            e = (tree[tindex]);
            IACSharpSensor.IACSharpSensor.SensorReached(5416);
            if ((e & 0x10) != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5417);
              bitsToGet = e & 15;
              dist = tree[tindex + 2];
              mode = DISTEXT;
              IACSharpSensor.IACSharpSensor.SensorReached(5418);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5419);
            if ((e & 64) == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5420);
              need = e;
              tree_index = tindex / 3 + tree[tindex + 2];
              IACSharpSensor.IACSharpSensor.SensorReached(5421);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5422);
            mode = BADCODE;
            z.Message = "invalid distance code";
            r = ZlibConstants.Z_DATA_ERROR;
            blocks.bitb = b;
            blocks.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            blocks.writeAt = q;
            System.Int32 RNTRNTRNT_615 = blocks.Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5423);
            return RNTRNTRNT_615;
          case DISTEXT:
            IACSharpSensor.IACSharpSensor.SensorReached(5424);
            j = bitsToGet;
            IACSharpSensor.IACSharpSensor.SensorReached(5425);
            while (k < j) {
              IACSharpSensor.IACSharpSensor.SensorReached(5426);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5427);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5428);
                blocks.bitb = b;
                blocks.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                blocks.writeAt = q;
                System.Int32 RNTRNTRNT_616 = blocks.Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5429);
                return RNTRNTRNT_616;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5430);
              n--;
              b |= (z.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5431);
            dist += (b & InternalInflateConstants.InflateMask[j]);
            b >>= j;
            k -= j;
            mode = COPY;
            IACSharpSensor.IACSharpSensor.SensorReached(5432);
            goto case COPY;
          case COPY:
            IACSharpSensor.IACSharpSensor.SensorReached(5433);
            f = q - dist;
            IACSharpSensor.IACSharpSensor.SensorReached(5434);
            while (f < 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5435);
              f += blocks.end;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5436);
            while (len != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5437);
              if (m == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5438);
                if (q == blocks.end && blocks.readAt != 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5439);
                  q = 0;
                  m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5440);
                if (m == 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5441);
                  blocks.writeAt = q;
                  r = blocks.Flush(r);
                  q = blocks.writeAt;
                  m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                  IACSharpSensor.IACSharpSensor.SensorReached(5442);
                  if (q == blocks.end && blocks.readAt != 0) {
                    IACSharpSensor.IACSharpSensor.SensorReached(5443);
                    q = 0;
                    m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                  }
                  IACSharpSensor.IACSharpSensor.SensorReached(5444);
                  if (m == 0) {
                    IACSharpSensor.IACSharpSensor.SensorReached(5445);
                    blocks.bitb = b;
                    blocks.bitk = k;
                    z.AvailableBytesIn = n;
                    z.TotalBytesIn += p - z.NextIn;
                    z.NextIn = p;
                    blocks.writeAt = q;
                    System.Int32 RNTRNTRNT_617 = blocks.Flush(r);
                    IACSharpSensor.IACSharpSensor.SensorReached(5446);
                    return RNTRNTRNT_617;
                  }
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5447);
              blocks.window[q++] = blocks.window[f++];
              m--;
              IACSharpSensor.IACSharpSensor.SensorReached(5448);
              if (f == blocks.end) {
                IACSharpSensor.IACSharpSensor.SensorReached(5449);
                f = 0;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5450);
              len--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5451);
            mode = START;
            IACSharpSensor.IACSharpSensor.SensorReached(5452);
            break;
          case LIT:
            IACSharpSensor.IACSharpSensor.SensorReached(5453);
            if (m == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5454);
              if (q == blocks.end && blocks.readAt != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5455);
                q = 0;
                m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5456);
              if (m == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5457);
                blocks.writeAt = q;
                r = blocks.Flush(r);
                q = blocks.writeAt;
                m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                IACSharpSensor.IACSharpSensor.SensorReached(5458);
                if (q == blocks.end && blocks.readAt != 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5459);
                  q = 0;
                  m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5460);
                if (m == 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5461);
                  blocks.bitb = b;
                  blocks.bitk = k;
                  z.AvailableBytesIn = n;
                  z.TotalBytesIn += p - z.NextIn;
                  z.NextIn = p;
                  blocks.writeAt = q;
                  System.Int32 RNTRNTRNT_618 = blocks.Flush(r);
                  IACSharpSensor.IACSharpSensor.SensorReached(5462);
                  return RNTRNTRNT_618;
                }
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5463);
            r = ZlibConstants.Z_OK;
            blocks.window[q++] = (byte)lit;
            m--;
            mode = START;
            IACSharpSensor.IACSharpSensor.SensorReached(5464);
            break;
          case WASH:
            IACSharpSensor.IACSharpSensor.SensorReached(5465);
            if (k > 7) {
              IACSharpSensor.IACSharpSensor.SensorReached(5466);
              k -= 8;
              n++;
              p--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5467);
            blocks.writeAt = q;
            r = blocks.Flush(r);
            q = blocks.writeAt;
            m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
            IACSharpSensor.IACSharpSensor.SensorReached(5468);
            if (blocks.readAt != blocks.writeAt) {
              IACSharpSensor.IACSharpSensor.SensorReached(5469);
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              System.Int32 RNTRNTRNT_619 = blocks.Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(5470);
              return RNTRNTRNT_619;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5471);
            mode = END;
            IACSharpSensor.IACSharpSensor.SensorReached(5472);
            goto case END;
          case END:
            IACSharpSensor.IACSharpSensor.SensorReached(5473);
            r = ZlibConstants.Z_STREAM_END;
            blocks.bitb = b;
            blocks.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            blocks.writeAt = q;
            System.Int32 RNTRNTRNT_620 = blocks.Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5474);
            return RNTRNTRNT_620;
          case BADCODE:
            IACSharpSensor.IACSharpSensor.SensorReached(5475);
            r = ZlibConstants.Z_DATA_ERROR;
            blocks.bitb = b;
            blocks.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            blocks.writeAt = q;
            System.Int32 RNTRNTRNT_621 = blocks.Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5476);
            return RNTRNTRNT_621;
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(5477);
            r = ZlibConstants.Z_STREAM_ERROR;
            blocks.bitb = b;
            blocks.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            blocks.writeAt = q;
            System.Int32 RNTRNTRNT_622 = blocks.Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5478);
            return RNTRNTRNT_622;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5479);
    }
    internal int InflateFast(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index, InflateBlocks s, ZlibCodec z)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5480);
      int t;
      int[] tp;
      int tp_index;
      int e;
      int b;
      int k;
      int p;
      int n;
      int q;
      int m;
      int ml;
      int md;
      int c;
      int d;
      int r;
      int tp_index_t_3;
      p = z.NextIn;
      n = z.AvailableBytesIn;
      b = s.bitb;
      k = s.bitk;
      q = s.writeAt;
      m = q < s.readAt ? s.readAt - q - 1 : s.end - q;
      ml = InternalInflateConstants.InflateMask[bl];
      md = InternalInflateConstants.InflateMask[bd];
      IACSharpSensor.IACSharpSensor.SensorReached(5481);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(5482);
        while (k < (20)) {
          IACSharpSensor.IACSharpSensor.SensorReached(5483);
          n--;
          b |= (z.InputBuffer[p++] & 0xff) << k;
          k += 8;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5484);
        t = b & ml;
        tp = tl;
        tp_index = tl_index;
        tp_index_t_3 = (tp_index + t) * 3;
        IACSharpSensor.IACSharpSensor.SensorReached(5485);
        if ((e = tp[tp_index_t_3]) == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5486);
          b >>= (tp[tp_index_t_3 + 1]);
          k -= (tp[tp_index_t_3 + 1]);
          s.window[q++] = (byte)tp[tp_index_t_3 + 2];
          m--;
          IACSharpSensor.IACSharpSensor.SensorReached(5487);
          continue;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5488);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(5489);
          b >>= (tp[tp_index_t_3 + 1]);
          k -= (tp[tp_index_t_3 + 1]);
          IACSharpSensor.IACSharpSensor.SensorReached(5490);
          if ((e & 16) != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(5491);
            e &= 15;
            c = tp[tp_index_t_3 + 2] + ((int)b & InternalInflateConstants.InflateMask[e]);
            b >>= e;
            k -= e;
            IACSharpSensor.IACSharpSensor.SensorReached(5492);
            while (k < 15) {
              IACSharpSensor.IACSharpSensor.SensorReached(5493);
              n--;
              b |= (z.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5494);
            t = b & md;
            tp = td;
            tp_index = td_index;
            tp_index_t_3 = (tp_index + t) * 3;
            e = tp[tp_index_t_3];
            IACSharpSensor.IACSharpSensor.SensorReached(5495);
            do {
              IACSharpSensor.IACSharpSensor.SensorReached(5496);
              b >>= (tp[tp_index_t_3 + 1]);
              k -= (tp[tp_index_t_3 + 1]);
              IACSharpSensor.IACSharpSensor.SensorReached(5497);
              if ((e & 16) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5498);
                e &= 15;
                IACSharpSensor.IACSharpSensor.SensorReached(5499);
                while (k < e) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5500);
                  n--;
                  b |= (z.InputBuffer[p++] & 0xff) << k;
                  k += 8;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5501);
                d = tp[tp_index_t_3 + 2] + (b & InternalInflateConstants.InflateMask[e]);
                b >>= e;
                k -= e;
                m -= c;
                IACSharpSensor.IACSharpSensor.SensorReached(5502);
                if (q >= d) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5503);
                  r = q - d;
                  IACSharpSensor.IACSharpSensor.SensorReached(5504);
                  if (q - r > 0 && 2 > (q - r)) {
                    IACSharpSensor.IACSharpSensor.SensorReached(5505);
                    s.window[q++] = s.window[r++];
                    s.window[q++] = s.window[r++];
                    c -= 2;
                  } else {
                    IACSharpSensor.IACSharpSensor.SensorReached(5506);
                    Array.Copy(s.window, r, s.window, q, 2);
                    q += 2;
                    r += 2;
                    c -= 2;
                  }
                } else {
                  IACSharpSensor.IACSharpSensor.SensorReached(5507);
                  r = q - d;
                  IACSharpSensor.IACSharpSensor.SensorReached(5508);
                  do {
                    IACSharpSensor.IACSharpSensor.SensorReached(5509);
                    r += s.end;
                  } while (r < 0);
                  IACSharpSensor.IACSharpSensor.SensorReached(5510);
                  e = s.end - r;
                  IACSharpSensor.IACSharpSensor.SensorReached(5511);
                  if (c > e) {
                    IACSharpSensor.IACSharpSensor.SensorReached(5512);
                    c -= e;
                    IACSharpSensor.IACSharpSensor.SensorReached(5513);
                    if (q - r > 0 && e > (q - r)) {
                      IACSharpSensor.IACSharpSensor.SensorReached(5514);
                      do {
                        IACSharpSensor.IACSharpSensor.SensorReached(5515);
                        s.window[q++] = s.window[r++];
                      } while (--e != 0);
                    } else {
                      IACSharpSensor.IACSharpSensor.SensorReached(5516);
                      Array.Copy(s.window, r, s.window, q, e);
                      q += e;
                      r += e;
                      e = 0;
                    }
                    IACSharpSensor.IACSharpSensor.SensorReached(5517);
                    r = 0;
                  }
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5518);
                if (q - r > 0 && c > (q - r)) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5519);
                  do {
                    IACSharpSensor.IACSharpSensor.SensorReached(5520);
                    s.window[q++] = s.window[r++];
                  } while (--c != 0);
                } else {
                  IACSharpSensor.IACSharpSensor.SensorReached(5521);
                  Array.Copy(s.window, r, s.window, q, c);
                  q += c;
                  r += c;
                  c = 0;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5522);
                break;
              } else if ((e & 64) == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5525);
                t += tp[tp_index_t_3 + 2];
                t += (b & InternalInflateConstants.InflateMask[e]);
                tp_index_t_3 = (tp_index + t) * 3;
                e = tp[tp_index_t_3];
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5523);
                z.Message = "invalid distance code";
                c = z.AvailableBytesIn - n;
                c = (k >> 3) < c ? k >> 3 : c;
                n += c;
                p -= c;
                k -= (c << 3);
                s.bitb = b;
                s.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                s.writeAt = q;
                System.Int32 RNTRNTRNT_623 = ZlibConstants.Z_DATA_ERROR;
                IACSharpSensor.IACSharpSensor.SensorReached(5524);
                return RNTRNTRNT_623;
              }
            } while (true);
            IACSharpSensor.IACSharpSensor.SensorReached(5526);
            break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5527);
          if ((e & 64) == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(5528);
            t += tp[tp_index_t_3 + 2];
            t += (b & InternalInflateConstants.InflateMask[e]);
            tp_index_t_3 = (tp_index + t) * 3;
            IACSharpSensor.IACSharpSensor.SensorReached(5529);
            if ((e = tp[tp_index_t_3]) == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5530);
              b >>= (tp[tp_index_t_3 + 1]);
              k -= (tp[tp_index_t_3 + 1]);
              s.window[q++] = (byte)tp[tp_index_t_3 + 2];
              m--;
              IACSharpSensor.IACSharpSensor.SensorReached(5531);
              break;
            }
          } else if ((e & 32) != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(5534);
            c = z.AvailableBytesIn - n;
            c = (k >> 3) < c ? k >> 3 : c;
            n += c;
            p -= c;
            k -= (c << 3);
            s.bitb = b;
            s.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            s.writeAt = q;
            System.Int32 RNTRNTRNT_625 = ZlibConstants.Z_STREAM_END;
            IACSharpSensor.IACSharpSensor.SensorReached(5535);
            return RNTRNTRNT_625;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(5532);
            z.Message = "invalid literal/length code";
            c = z.AvailableBytesIn - n;
            c = (k >> 3) < c ? k >> 3 : c;
            n += c;
            p -= c;
            k -= (c << 3);
            s.bitb = b;
            s.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            s.writeAt = q;
            System.Int32 RNTRNTRNT_624 = ZlibConstants.Z_DATA_ERROR;
            IACSharpSensor.IACSharpSensor.SensorReached(5533);
            return RNTRNTRNT_624;
          }
        } while (true);
      } while (m >= 258 && n >= 10);
      IACSharpSensor.IACSharpSensor.SensorReached(5536);
      c = z.AvailableBytesIn - n;
      c = (k >> 3) < c ? k >> 3 : c;
      n += c;
      p -= c;
      k -= (c << 3);
      s.bitb = b;
      s.bitk = k;
      z.AvailableBytesIn = n;
      z.TotalBytesIn += p - z.NextIn;
      z.NextIn = p;
      s.writeAt = q;
      System.Int32 RNTRNTRNT_626 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(5537);
      return RNTRNTRNT_626;
    }
  }
  internal sealed class InflateManager
  {
    private const int PRESET_DICT = 0x20;
    private const int Z_DEFLATED = 8;
    private enum InflateManagerMode
    {
      METHOD = 0,
      FLAG = 1,
      DICT4 = 2,
      DICT3 = 3,
      DICT2 = 4,
      DICT1 = 5,
      DICT0 = 6,
      BLOCKS = 7,
      CHECK4 = 8,
      CHECK3 = 9,
      CHECK2 = 10,
      CHECK1 = 11,
      DONE = 12,
      BAD = 13
    }
    private InflateManagerMode mode;
    internal ZlibCodec _codec;
    internal int method;
    internal uint computedCheck;
    internal uint expectedCheck;
    internal int marker;
    private bool _handleRfc1950HeaderBytes = true;
    internal bool HandleRfc1950HeaderBytes {
      get {
        System.Boolean RNTRNTRNT_627 = _handleRfc1950HeaderBytes;
        IACSharpSensor.IACSharpSensor.SensorReached(5538);
        return RNTRNTRNT_627;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5539);
        _handleRfc1950HeaderBytes = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5540);
      }
    }
    internal int wbits;
    internal InflateBlocks blocks;
    public InflateManager()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5541);
    }
    public InflateManager(bool expectRfc1950HeaderBytes)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5542);
      _handleRfc1950HeaderBytes = expectRfc1950HeaderBytes;
      IACSharpSensor.IACSharpSensor.SensorReached(5543);
    }
    internal int Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5544);
      _codec.TotalBytesIn = _codec.TotalBytesOut = 0;
      _codec.Message = null;
      mode = HandleRfc1950HeaderBytes ? InflateManagerMode.METHOD : InflateManagerMode.BLOCKS;
      blocks.Reset();
      System.Int32 RNTRNTRNT_628 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(5545);
      return RNTRNTRNT_628;
    }
    internal int End()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5546);
      if (blocks != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(5547);
        blocks.Free();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5548);
      blocks = null;
      System.Int32 RNTRNTRNT_629 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(5549);
      return RNTRNTRNT_629;
    }
    internal int Initialize(ZlibCodec codec, int w)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5550);
      _codec = codec;
      _codec.Message = null;
      blocks = null;
      IACSharpSensor.IACSharpSensor.SensorReached(5551);
      if (w < 8 || w > 15) {
        IACSharpSensor.IACSharpSensor.SensorReached(5552);
        End();
        IACSharpSensor.IACSharpSensor.SensorReached(5553);
        throw new ZlibException("Bad window size.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5554);
      wbits = w;
      blocks = new InflateBlocks(codec, HandleRfc1950HeaderBytes ? this : null, 1 << w);
      Reset();
      System.Int32 RNTRNTRNT_630 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(5555);
      return RNTRNTRNT_630;
    }
    internal int Inflate(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5556);
      int b;
      IACSharpSensor.IACSharpSensor.SensorReached(5557);
      if (_codec.InputBuffer == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(5558);
        throw new ZlibException("InputBuffer is null. ");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5559);
      int f = ZlibConstants.Z_OK;
      int r = ZlibConstants.Z_BUF_ERROR;
      IACSharpSensor.IACSharpSensor.SensorReached(5560);
      while (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(5561);
        switch (mode) {
          case InflateManagerMode.METHOD:
            IACSharpSensor.IACSharpSensor.SensorReached(5562);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_631 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(5563);
              return RNTRNTRNT_631;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5564);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            IACSharpSensor.IACSharpSensor.SensorReached(5565);
            if (((method = _codec.InputBuffer[_codec.NextIn++]) & 0xf) != Z_DEFLATED) {
              IACSharpSensor.IACSharpSensor.SensorReached(5566);
              mode = InflateManagerMode.BAD;
              _codec.Message = String.Format("unknown compression method (0x{0:X2})", method);
              marker = 5;
              IACSharpSensor.IACSharpSensor.SensorReached(5567);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5568);
            if ((method >> 4) + 8 > wbits) {
              IACSharpSensor.IACSharpSensor.SensorReached(5569);
              mode = InflateManagerMode.BAD;
              _codec.Message = String.Format("invalid window size ({0})", (method >> 4) + 8);
              marker = 5;
              IACSharpSensor.IACSharpSensor.SensorReached(5570);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5571);
            mode = InflateManagerMode.FLAG;
            IACSharpSensor.IACSharpSensor.SensorReached(5572);
            break;
          case InflateManagerMode.FLAG:
            IACSharpSensor.IACSharpSensor.SensorReached(5573);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_632 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(5574);
              return RNTRNTRNT_632;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5575);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            b = (_codec.InputBuffer[_codec.NextIn++]) & 0xff;
            IACSharpSensor.IACSharpSensor.SensorReached(5576);
            if ((((method << 8) + b) % 31) != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5577);
              mode = InflateManagerMode.BAD;
              _codec.Message = "incorrect header check";
              marker = 5;
              IACSharpSensor.IACSharpSensor.SensorReached(5578);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5579);
            mode = ((b & PRESET_DICT) == 0) ? InflateManagerMode.BLOCKS : InflateManagerMode.DICT4;
            IACSharpSensor.IACSharpSensor.SensorReached(5580);
            break;
          case InflateManagerMode.DICT4:
            IACSharpSensor.IACSharpSensor.SensorReached(5581);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_633 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(5582);
              return RNTRNTRNT_633;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5583);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck = (uint)((_codec.InputBuffer[_codec.NextIn++] << 24) & 0xff000000u);
            mode = InflateManagerMode.DICT3;
            IACSharpSensor.IACSharpSensor.SensorReached(5584);
            break;
          case InflateManagerMode.DICT3:
            IACSharpSensor.IACSharpSensor.SensorReached(5585);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_634 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(5586);
              return RNTRNTRNT_634;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5587);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 16) & 0xff0000);
            mode = InflateManagerMode.DICT2;
            IACSharpSensor.IACSharpSensor.SensorReached(5588);
            break;
          case InflateManagerMode.DICT2:
            IACSharpSensor.IACSharpSensor.SensorReached(5589);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_635 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(5590);
              return RNTRNTRNT_635;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5591);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 8) & 0xff00);
            mode = InflateManagerMode.DICT1;
            IACSharpSensor.IACSharpSensor.SensorReached(5592);
            break;
          case InflateManagerMode.DICT1:
            IACSharpSensor.IACSharpSensor.SensorReached(5593);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_636 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(5594);
              return RNTRNTRNT_636;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5595);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck += (uint)(_codec.InputBuffer[_codec.NextIn++] & 0xff);
            _codec._Adler32 = expectedCheck;
            mode = InflateManagerMode.DICT0;
            System.Int32 RNTRNTRNT_637 = ZlibConstants.Z_NEED_DICT;
            IACSharpSensor.IACSharpSensor.SensorReached(5596);
            return RNTRNTRNT_637;
          case InflateManagerMode.DICT0:
            IACSharpSensor.IACSharpSensor.SensorReached(5597);
            mode = InflateManagerMode.BAD;
            _codec.Message = "need dictionary";
            marker = 0;
            System.Int32 RNTRNTRNT_638 = ZlibConstants.Z_STREAM_ERROR;
            IACSharpSensor.IACSharpSensor.SensorReached(5598);
            return RNTRNTRNT_638;
          case InflateManagerMode.BLOCKS:
            IACSharpSensor.IACSharpSensor.SensorReached(5599);
            r = blocks.Process(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5600);
            if (r == ZlibConstants.Z_DATA_ERROR) {
              IACSharpSensor.IACSharpSensor.SensorReached(5601);
              mode = InflateManagerMode.BAD;
              marker = 0;
              IACSharpSensor.IACSharpSensor.SensorReached(5602);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5603);
            if (r == ZlibConstants.Z_OK) {
              IACSharpSensor.IACSharpSensor.SensorReached(5604);
              r = f;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5605);
            if (r != ZlibConstants.Z_STREAM_END) {
              System.Int32 RNTRNTRNT_639 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(5606);
              return RNTRNTRNT_639;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5607);
            r = f;
            computedCheck = blocks.Reset();
            IACSharpSensor.IACSharpSensor.SensorReached(5608);
            if (!HandleRfc1950HeaderBytes) {
              IACSharpSensor.IACSharpSensor.SensorReached(5609);
              mode = InflateManagerMode.DONE;
              System.Int32 RNTRNTRNT_640 = ZlibConstants.Z_STREAM_END;
              IACSharpSensor.IACSharpSensor.SensorReached(5610);
              return RNTRNTRNT_640;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5611);
            mode = InflateManagerMode.CHECK4;
            IACSharpSensor.IACSharpSensor.SensorReached(5612);
            break;
          case InflateManagerMode.CHECK4:
            IACSharpSensor.IACSharpSensor.SensorReached(5613);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_641 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(5614);
              return RNTRNTRNT_641;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5615);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck = (uint)((_codec.InputBuffer[_codec.NextIn++] << 24) & 0xff000000u);
            mode = InflateManagerMode.CHECK3;
            IACSharpSensor.IACSharpSensor.SensorReached(5616);
            break;
          case InflateManagerMode.CHECK3:
            IACSharpSensor.IACSharpSensor.SensorReached(5617);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_642 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(5618);
              return RNTRNTRNT_642;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5619);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 16) & 0xff0000);
            mode = InflateManagerMode.CHECK2;
            IACSharpSensor.IACSharpSensor.SensorReached(5620);
            break;
          case InflateManagerMode.CHECK2:
            IACSharpSensor.IACSharpSensor.SensorReached(5621);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_643 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(5622);
              return RNTRNTRNT_643;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5623);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 8) & 0xff00);
            mode = InflateManagerMode.CHECK1;
            IACSharpSensor.IACSharpSensor.SensorReached(5624);
            break;
          case InflateManagerMode.CHECK1:
            IACSharpSensor.IACSharpSensor.SensorReached(5625);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_644 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(5626);
              return RNTRNTRNT_644;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5627);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck += (uint)(_codec.InputBuffer[_codec.NextIn++] & 0xff);
            IACSharpSensor.IACSharpSensor.SensorReached(5628);
            if (computedCheck != expectedCheck) {
              IACSharpSensor.IACSharpSensor.SensorReached(5629);
              mode = InflateManagerMode.BAD;
              _codec.Message = "incorrect data check";
              marker = 5;
              IACSharpSensor.IACSharpSensor.SensorReached(5630);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5631);
            mode = InflateManagerMode.DONE;
            System.Int32 RNTRNTRNT_645 = ZlibConstants.Z_STREAM_END;
            IACSharpSensor.IACSharpSensor.SensorReached(5632);
            return RNTRNTRNT_645;
          case InflateManagerMode.DONE:
            System.Int32 RNTRNTRNT_646 = ZlibConstants.Z_STREAM_END;
            IACSharpSensor.IACSharpSensor.SensorReached(5633);
            return RNTRNTRNT_646;
          case InflateManagerMode.BAD:
            IACSharpSensor.IACSharpSensor.SensorReached(5634);
            throw new ZlibException(String.Format("Bad state ({0})", _codec.Message));
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(5635);
            throw new ZlibException("Stream error.");
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5636);
    }
    internal int SetDictionary(byte[] dictionary)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5637);
      int index = 0;
      int length = dictionary.Length;
      IACSharpSensor.IACSharpSensor.SensorReached(5638);
      if (mode != InflateManagerMode.DICT0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5639);
        throw new ZlibException("Stream error.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5640);
      if (Adler.Adler32(1, dictionary, 0, dictionary.Length) != _codec._Adler32) {
        System.Int32 RNTRNTRNT_647 = ZlibConstants.Z_DATA_ERROR;
        IACSharpSensor.IACSharpSensor.SensorReached(5641);
        return RNTRNTRNT_647;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5642);
      _codec._Adler32 = Adler.Adler32(0, null, 0, 0);
      IACSharpSensor.IACSharpSensor.SensorReached(5643);
      if (length >= (1 << wbits)) {
        IACSharpSensor.IACSharpSensor.SensorReached(5644);
        length = (1 << wbits) - 1;
        index = dictionary.Length - length;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5645);
      blocks.SetDictionary(dictionary, index, length);
      mode = InflateManagerMode.BLOCKS;
      System.Int32 RNTRNTRNT_648 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(5646);
      return RNTRNTRNT_648;
    }
    private static readonly byte[] mark = new byte[] {
      0,
      0,
      0xff,
      0xff
    };
    internal int Sync()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5647);
      int n;
      int p;
      int m;
      long r, w;
      IACSharpSensor.IACSharpSensor.SensorReached(5648);
      if (mode != InflateManagerMode.BAD) {
        IACSharpSensor.IACSharpSensor.SensorReached(5649);
        mode = InflateManagerMode.BAD;
        marker = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5650);
      if ((n = _codec.AvailableBytesIn) == 0) {
        System.Int32 RNTRNTRNT_649 = ZlibConstants.Z_BUF_ERROR;
        IACSharpSensor.IACSharpSensor.SensorReached(5651);
        return RNTRNTRNT_649;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5652);
      p = _codec.NextIn;
      m = marker;
      IACSharpSensor.IACSharpSensor.SensorReached(5653);
      while (n != 0 && m < 4) {
        IACSharpSensor.IACSharpSensor.SensorReached(5654);
        if (_codec.InputBuffer[p] == mark[m]) {
          IACSharpSensor.IACSharpSensor.SensorReached(5655);
          m++;
        } else if (_codec.InputBuffer[p] != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5657);
          m = 0;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5656);
          m = 4 - m;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5658);
        p++;
        n--;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5659);
      _codec.TotalBytesIn += p - _codec.NextIn;
      _codec.NextIn = p;
      _codec.AvailableBytesIn = n;
      marker = m;
      IACSharpSensor.IACSharpSensor.SensorReached(5660);
      if (m != 4) {
        System.Int32 RNTRNTRNT_650 = ZlibConstants.Z_DATA_ERROR;
        IACSharpSensor.IACSharpSensor.SensorReached(5661);
        return RNTRNTRNT_650;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5662);
      r = _codec.TotalBytesIn;
      w = _codec.TotalBytesOut;
      Reset();
      _codec.TotalBytesIn = r;
      _codec.TotalBytesOut = w;
      mode = InflateManagerMode.BLOCKS;
      System.Int32 RNTRNTRNT_651 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(5663);
      return RNTRNTRNT_651;
    }
    internal int SyncPoint(ZlibCodec z)
    {
      System.Int32 RNTRNTRNT_652 = blocks.SyncPoint();
      IACSharpSensor.IACSharpSensor.SensorReached(5664);
      return RNTRNTRNT_652;
    }
  }
}
