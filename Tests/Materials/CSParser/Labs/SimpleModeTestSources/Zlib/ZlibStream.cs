using System;
using System.IO;
namespace Ionic.Zlib
{
  public class ZlibStream : System.IO.Stream
  {
    internal ZlibBaseStream _baseStream;
    bool _disposed;
    public ZlibStream(System.IO.Stream stream, CompressionMode mode) : this(stream, mode, CompressionLevel.Default, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6294);
    }
    public ZlibStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level) : this(stream, mode, level, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6295);
    }
    public ZlibStream(System.IO.Stream stream, CompressionMode mode, bool leaveOpen) : this(stream, mode, CompressionLevel.Default, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6296);
    }
    public ZlibStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6297);
      _baseStream = new ZlibBaseStream(stream, mode, level, ZlibStreamFlavor.ZLIB, leaveOpen);
      IACSharpSensor.IACSharpSensor.SensorReached(6298);
    }
    public virtual FlushType FlushMode {
      get {
        FlushType RNTRNTRNT_722 = (this._baseStream._flushMode);
        IACSharpSensor.IACSharpSensor.SensorReached(6299);
        return RNTRNTRNT_722;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6300);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(6301);
          throw new ObjectDisposedException("ZlibStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6302);
        this._baseStream._flushMode = value;
        IACSharpSensor.IACSharpSensor.SensorReached(6303);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_723 = this._baseStream._bufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(6304);
        return RNTRNTRNT_723;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6305);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(6306);
          throw new ObjectDisposedException("ZlibStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6307);
        if (this._baseStream._workingBuffer != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(6308);
          throw new ZlibException("The working buffer is already set.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6309);
        if (value < ZlibConstants.WorkingBufferSizeMin) {
          IACSharpSensor.IACSharpSensor.SensorReached(6310);
          throw new ZlibException(String.Format("Don't be silly. {0} bytes?? Use a bigger buffer, at least {1}.", value, ZlibConstants.WorkingBufferSizeMin));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6311);
        this._baseStream._bufferSize = value;
        IACSharpSensor.IACSharpSensor.SensorReached(6312);
      }
    }
    public virtual long TotalIn {
      get {
        System.Int64 RNTRNTRNT_724 = this._baseStream._z.TotalBytesIn;
        IACSharpSensor.IACSharpSensor.SensorReached(6313);
        return RNTRNTRNT_724;
      }
    }
    public virtual long TotalOut {
      get {
        System.Int64 RNTRNTRNT_725 = this._baseStream._z.TotalBytesOut;
        IACSharpSensor.IACSharpSensor.SensorReached(6314);
        return RNTRNTRNT_725;
      }
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6315);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(6316);
        if (!_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(6317);
          if (disposing && (this._baseStream != null)) {
            IACSharpSensor.IACSharpSensor.SensorReached(6318);
            this._baseStream.Close();
          }
          IACSharpSensor.IACSharpSensor.SensorReached(6319);
          _disposed = true;
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(6320);
        base.Dispose(disposing);
        IACSharpSensor.IACSharpSensor.SensorReached(6321);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6322);
    }
    public override bool CanRead {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6323);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(6324);
          throw new ObjectDisposedException("ZlibStream");
        }
        System.Boolean RNTRNTRNT_726 = _baseStream._stream.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(6325);
        return RNTRNTRNT_726;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_727 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(6326);
        return RNTRNTRNT_727;
      }
    }
    public override bool CanWrite {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6327);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(6328);
          throw new ObjectDisposedException("ZlibStream");
        }
        System.Boolean RNTRNTRNT_728 = _baseStream._stream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(6329);
        return RNTRNTRNT_728;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6330);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(6331);
        throw new ObjectDisposedException("ZlibStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6332);
      _baseStream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(6333);
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6334);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6335);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Writer) {
          System.Int64 RNTRNTRNT_729 = this._baseStream._z.TotalBytesOut;
          IACSharpSensor.IACSharpSensor.SensorReached(6336);
          return RNTRNTRNT_729;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6337);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Reader) {
          System.Int64 RNTRNTRNT_730 = this._baseStream._z.TotalBytesIn;
          IACSharpSensor.IACSharpSensor.SensorReached(6338);
          return RNTRNTRNT_730;
        }
        System.Int64 RNTRNTRNT_731 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6339);
        return RNTRNTRNT_731;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6340);
        throw new NotSupportedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6341);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(6342);
        throw new ObjectDisposedException("ZlibStream");
      }
      System.Int32 RNTRNTRNT_732 = _baseStream.Read(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(6343);
      return RNTRNTRNT_732;
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6344);
      throw new NotSupportedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6345);
      throw new NotSupportedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6346);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(6347);
        throw new ObjectDisposedException("ZlibStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6348);
      _baseStream.Write(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(6349);
    }
    public static byte[] CompressString(String s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6350);
      using (var ms = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(6351);
        Stream compressor = new ZlibStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressString(s, compressor);
        System.Byte[] RNTRNTRNT_733 = ms.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(6352);
        return RNTRNTRNT_733;
      }
    }
    public static byte[] CompressBuffer(byte[] b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6353);
      using (var ms = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(6354);
        Stream compressor = new ZlibStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressBuffer(b, compressor);
        System.Byte[] RNTRNTRNT_734 = ms.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(6355);
        return RNTRNTRNT_734;
      }
    }
    public static String UncompressString(byte[] compressed)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6356);
      using (var input = new MemoryStream(compressed)) {
        IACSharpSensor.IACSharpSensor.SensorReached(6357);
        Stream decompressor = new ZlibStream(input, CompressionMode.Decompress);
        String RNTRNTRNT_735 = ZlibBaseStream.UncompressString(compressed, decompressor);
        IACSharpSensor.IACSharpSensor.SensorReached(6358);
        return RNTRNTRNT_735;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6359);
      using (var input = new MemoryStream(compressed)) {
        IACSharpSensor.IACSharpSensor.SensorReached(6360);
        Stream decompressor = new ZlibStream(input, CompressionMode.Decompress);
        System.Byte[] RNTRNTRNT_736 = ZlibBaseStream.UncompressBuffer(compressed, decompressor);
        IACSharpSensor.IACSharpSensor.SensorReached(6361);
        return RNTRNTRNT_736;
      }
    }
  }
}
