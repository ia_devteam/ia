using System;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Zlib
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000D")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public sealed class ZlibCodec
  {
    public byte[] InputBuffer;
    public int NextIn;
    public int AvailableBytesIn;
    public long TotalBytesIn;
    public byte[] OutputBuffer;
    public int NextOut;
    public int AvailableBytesOut;
    public long TotalBytesOut;
    public System.String Message;
    internal DeflateManager dstate;
    internal InflateManager istate;
    internal uint _Adler32;
    public CompressionLevel CompressLevel = CompressionLevel.Default;
    public int WindowBits = ZlibConstants.WindowBitsDefault;
    public CompressionStrategy Strategy = CompressionStrategy.Default;
    public int Adler32 {
      get {
        System.Int32 RNTRNTRNT_701 = (int)_Adler32;
        IACSharpSensor.IACSharpSensor.SensorReached(6211);
        return RNTRNTRNT_701;
      }
    }
    public ZlibCodec()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6212);
    }
    public ZlibCodec(CompressionMode mode)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6213);
      if (mode == CompressionMode.Compress) {
        IACSharpSensor.IACSharpSensor.SensorReached(6214);
        int rc = InitializeDeflate();
        IACSharpSensor.IACSharpSensor.SensorReached(6215);
        if (rc != ZlibConstants.Z_OK) {
          IACSharpSensor.IACSharpSensor.SensorReached(6216);
          throw new ZlibException("Cannot initialize for deflate.");
        }
      } else if (mode == CompressionMode.Decompress) {
        IACSharpSensor.IACSharpSensor.SensorReached(6218);
        int rc = InitializeInflate();
        IACSharpSensor.IACSharpSensor.SensorReached(6219);
        if (rc != ZlibConstants.Z_OK) {
          IACSharpSensor.IACSharpSensor.SensorReached(6220);
          throw new ZlibException("Cannot initialize for inflate.");
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(6217);
        throw new ZlibException("Invalid ZlibStreamFlavor.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6221);
    }
    public int InitializeInflate()
    {
      System.Int32 RNTRNTRNT_702 = InitializeInflate(this.WindowBits);
      IACSharpSensor.IACSharpSensor.SensorReached(6222);
      return RNTRNTRNT_702;
    }
    public int InitializeInflate(bool expectRfc1950Header)
    {
      System.Int32 RNTRNTRNT_703 = InitializeInflate(this.WindowBits, expectRfc1950Header);
      IACSharpSensor.IACSharpSensor.SensorReached(6223);
      return RNTRNTRNT_703;
    }
    public int InitializeInflate(int windowBits)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6224);
      this.WindowBits = windowBits;
      System.Int32 RNTRNTRNT_704 = InitializeInflate(windowBits, true);
      IACSharpSensor.IACSharpSensor.SensorReached(6225);
      return RNTRNTRNT_704;
    }
    public int InitializeInflate(int windowBits, bool expectRfc1950Header)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6226);
      this.WindowBits = windowBits;
      IACSharpSensor.IACSharpSensor.SensorReached(6227);
      if (dstate != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6228);
        throw new ZlibException("You may not call InitializeInflate() after calling InitializeDeflate().");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6229);
      istate = new InflateManager(expectRfc1950Header);
      System.Int32 RNTRNTRNT_705 = istate.Initialize(this, windowBits);
      IACSharpSensor.IACSharpSensor.SensorReached(6230);
      return RNTRNTRNT_705;
    }
    public int Inflate(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6231);
      if (istate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6232);
        throw new ZlibException("No Inflate State!");
      }
      System.Int32 RNTRNTRNT_706 = istate.Inflate(flush);
      IACSharpSensor.IACSharpSensor.SensorReached(6233);
      return RNTRNTRNT_706;
    }
    public int EndInflate()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6234);
      if (istate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6235);
        throw new ZlibException("No Inflate State!");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6236);
      int ret = istate.End();
      istate = null;
      System.Int32 RNTRNTRNT_707 = ret;
      IACSharpSensor.IACSharpSensor.SensorReached(6237);
      return RNTRNTRNT_707;
    }
    public int SyncInflate()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6238);
      if (istate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6239);
        throw new ZlibException("No Inflate State!");
      }
      System.Int32 RNTRNTRNT_708 = istate.Sync();
      IACSharpSensor.IACSharpSensor.SensorReached(6240);
      return RNTRNTRNT_708;
    }
    public int InitializeDeflate()
    {
      System.Int32 RNTRNTRNT_709 = _InternalInitializeDeflate(true);
      IACSharpSensor.IACSharpSensor.SensorReached(6241);
      return RNTRNTRNT_709;
    }
    public int InitializeDeflate(CompressionLevel level)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6242);
      this.CompressLevel = level;
      System.Int32 RNTRNTRNT_710 = _InternalInitializeDeflate(true);
      IACSharpSensor.IACSharpSensor.SensorReached(6243);
      return RNTRNTRNT_710;
    }
    public int InitializeDeflate(CompressionLevel level, bool wantRfc1950Header)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6244);
      this.CompressLevel = level;
      System.Int32 RNTRNTRNT_711 = _InternalInitializeDeflate(wantRfc1950Header);
      IACSharpSensor.IACSharpSensor.SensorReached(6245);
      return RNTRNTRNT_711;
    }
    public int InitializeDeflate(CompressionLevel level, int bits)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6246);
      this.CompressLevel = level;
      this.WindowBits = bits;
      System.Int32 RNTRNTRNT_712 = _InternalInitializeDeflate(true);
      IACSharpSensor.IACSharpSensor.SensorReached(6247);
      return RNTRNTRNT_712;
    }
    public int InitializeDeflate(CompressionLevel level, int bits, bool wantRfc1950Header)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6248);
      this.CompressLevel = level;
      this.WindowBits = bits;
      System.Int32 RNTRNTRNT_713 = _InternalInitializeDeflate(wantRfc1950Header);
      IACSharpSensor.IACSharpSensor.SensorReached(6249);
      return RNTRNTRNT_713;
    }
    private int _InternalInitializeDeflate(bool wantRfc1950Header)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6250);
      if (istate != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6251);
        throw new ZlibException("You may not call InitializeDeflate() after calling InitializeInflate().");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6252);
      dstate = new DeflateManager();
      dstate.WantRfc1950HeaderBytes = wantRfc1950Header;
      System.Int32 RNTRNTRNT_714 = dstate.Initialize(this, this.CompressLevel, this.WindowBits, this.Strategy);
      IACSharpSensor.IACSharpSensor.SensorReached(6253);
      return RNTRNTRNT_714;
    }
    public int Deflate(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6254);
      if (dstate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6255);
        throw new ZlibException("No Deflate State!");
      }
      System.Int32 RNTRNTRNT_715 = dstate.Deflate(flush);
      IACSharpSensor.IACSharpSensor.SensorReached(6256);
      return RNTRNTRNT_715;
    }
    public int EndDeflate()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6257);
      if (dstate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6258);
        throw new ZlibException("No Deflate State!");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6259);
      dstate = null;
      System.Int32 RNTRNTRNT_716 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(6260);
      return RNTRNTRNT_716;
    }
    public void ResetDeflate()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6261);
      if (dstate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6262);
        throw new ZlibException("No Deflate State!");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6263);
      dstate.Reset();
      IACSharpSensor.IACSharpSensor.SensorReached(6264);
    }
    public int SetDeflateParams(CompressionLevel level, CompressionStrategy strategy)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6265);
      if (dstate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6266);
        throw new ZlibException("No Deflate State!");
      }
      System.Int32 RNTRNTRNT_717 = dstate.SetParams(level, strategy);
      IACSharpSensor.IACSharpSensor.SensorReached(6267);
      return RNTRNTRNT_717;
    }
    public int SetDictionary(byte[] dictionary)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6268);
      if (istate != null) {
        System.Int32 RNTRNTRNT_718 = istate.SetDictionary(dictionary);
        IACSharpSensor.IACSharpSensor.SensorReached(6269);
        return RNTRNTRNT_718;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6270);
      if (dstate != null) {
        System.Int32 RNTRNTRNT_719 = dstate.SetDictionary(dictionary);
        IACSharpSensor.IACSharpSensor.SensorReached(6271);
        return RNTRNTRNT_719;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6272);
      throw new ZlibException("No Inflate or Deflate state!");
    }
    internal void flush_pending()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6273);
      int len = dstate.pendingCount;
      IACSharpSensor.IACSharpSensor.SensorReached(6274);
      if (len > AvailableBytesOut) {
        IACSharpSensor.IACSharpSensor.SensorReached(6275);
        len = AvailableBytesOut;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6276);
      if (len == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6277);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6278);
      if (dstate.pending.Length <= dstate.nextPending || OutputBuffer.Length <= NextOut || dstate.pending.Length < (dstate.nextPending + len) || OutputBuffer.Length < (NextOut + len)) {
        IACSharpSensor.IACSharpSensor.SensorReached(6279);
        throw new ZlibException(String.Format("Invalid State. (pending.Length={0}, pendingCount={1})", dstate.pending.Length, dstate.pendingCount));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6280);
      Array.Copy(dstate.pending, dstate.nextPending, OutputBuffer, NextOut, len);
      NextOut += len;
      dstate.nextPending += len;
      TotalBytesOut += len;
      AvailableBytesOut -= len;
      dstate.pendingCount -= len;
      IACSharpSensor.IACSharpSensor.SensorReached(6281);
      if (dstate.pendingCount == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6282);
        dstate.nextPending = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6283);
    }
    internal int read_buf(byte[] buf, int start, int size)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6284);
      int len = AvailableBytesIn;
      IACSharpSensor.IACSharpSensor.SensorReached(6285);
      if (len > size) {
        IACSharpSensor.IACSharpSensor.SensorReached(6286);
        len = size;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6287);
      if (len == 0) {
        System.Int32 RNTRNTRNT_720 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6288);
        return RNTRNTRNT_720;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6289);
      AvailableBytesIn -= len;
      IACSharpSensor.IACSharpSensor.SensorReached(6290);
      if (dstate.WantRfc1950HeaderBytes) {
        IACSharpSensor.IACSharpSensor.SensorReached(6291);
        _Adler32 = Adler.Adler32(_Adler32, InputBuffer, NextIn, len);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6292);
      Array.Copy(InputBuffer, NextIn, buf, start, len);
      NextIn += len;
      TotalBytesIn += len;
      System.Int32 RNTRNTRNT_721 = len;
      IACSharpSensor.IACSharpSensor.SensorReached(6293);
      return RNTRNTRNT_721;
    }
  }
}
