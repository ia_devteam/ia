using System;
using System.Collections.Generic;
using System.Threading;
using Ionic.Zlib;
using System.IO;
namespace Ionic.Zlib
{
  internal class WorkItem
  {
    public byte[] buffer;
    public byte[] compressed;
    public int crc;
    public int index;
    public int ordinal;
    public int inputBytesAvailable;
    public int compressedBytesAvailable;
    public ZlibCodec compressor;
    public WorkItem(int size, Ionic.Zlib.CompressionLevel compressLevel, CompressionStrategy strategy, int ix)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5759);
      this.buffer = new byte[size];
      int n = size + ((size / 32768) + 1) * 5 * 2;
      this.compressed = new byte[n];
      this.compressor = new ZlibCodec();
      this.compressor.InitializeDeflate(compressLevel, false);
      this.compressor.OutputBuffer = this.compressed;
      this.compressor.InputBuffer = this.buffer;
      this.index = ix;
      IACSharpSensor.IACSharpSensor.SensorReached(5760);
    }
  }
  public class ParallelDeflateOutputStream : System.IO.Stream
  {
    private static readonly int IO_BUFFER_SIZE_DEFAULT = 64 * 1024;
    private static readonly int BufferPairsPerCore = 4;
    private System.Collections.Generic.List<WorkItem> _pool;
    private bool _leaveOpen;
    private bool emitting;
    private System.IO.Stream _outStream;
    private int _maxBufferPairs;
    private int _bufferSize = IO_BUFFER_SIZE_DEFAULT;
    private AutoResetEvent _newlyCompressedBlob;
    private object _outputLock = new object();
    private bool _isClosed;
    private bool _firstWriteDone;
    private int _currentlyFilling;
    private int _lastFilled;
    private int _lastWritten;
    private int _latestCompressed;
    private int _Crc32;
    private Ionic.Crc.CRC32 _runningCrc;
    private object _latestLock = new object();
    private System.Collections.Generic.Queue<int> _toWrite;
    private System.Collections.Generic.Queue<int> _toFill;
    private Int64 _totalBytesProcessed;
    private Ionic.Zlib.CompressionLevel _compressLevel;
    private volatile Exception _pendingException;
    private bool _handlingException;
    private object _eLock = new Object();
    private TraceBits _DesiredTrace = TraceBits.Session | TraceBits.Compress | TraceBits.WriteTake | TraceBits.WriteEnter | TraceBits.EmitEnter | TraceBits.EmitDone | TraceBits.EmitLock | TraceBits.EmitSkip | TraceBits.EmitBegin;
    public ParallelDeflateOutputStream(System.IO.Stream stream) : this(stream, CompressionLevel.Default, CompressionStrategy.Default, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5761);
    }
    public ParallelDeflateOutputStream(System.IO.Stream stream, CompressionLevel level) : this(stream, level, CompressionStrategy.Default, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5762);
    }
    public ParallelDeflateOutputStream(System.IO.Stream stream, bool leaveOpen) : this(stream, CompressionLevel.Default, CompressionStrategy.Default, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5763);
    }
    public ParallelDeflateOutputStream(System.IO.Stream stream, CompressionLevel level, bool leaveOpen) : this(stream, CompressionLevel.Default, CompressionStrategy.Default, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5764);
    }
    public ParallelDeflateOutputStream(System.IO.Stream stream, CompressionLevel level, CompressionStrategy strategy, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5765);
      TraceOutput(TraceBits.Lifecycle | TraceBits.Session, "-------------------------------------------------------");
      TraceOutput(TraceBits.Lifecycle | TraceBits.Session, "Create {0:X8}", this.GetHashCode());
      _outStream = stream;
      _compressLevel = level;
      Strategy = strategy;
      _leaveOpen = leaveOpen;
      this.MaxBufferPairs = 16;
      IACSharpSensor.IACSharpSensor.SensorReached(5766);
    }
    public CompressionStrategy Strategy { get; private set; }
    public int MaxBufferPairs {
      get {
        System.Int32 RNTRNTRNT_663 = _maxBufferPairs;
        IACSharpSensor.IACSharpSensor.SensorReached(5767);
        return RNTRNTRNT_663;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5768);
        if (value < 4) {
          IACSharpSensor.IACSharpSensor.SensorReached(5769);
          throw new ArgumentException("MaxBufferPairs", "Value must be 4 or greater.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5770);
        _maxBufferPairs = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5771);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_664 = _bufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(5772);
        return RNTRNTRNT_664;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5773);
        if (value < 1024) {
          IACSharpSensor.IACSharpSensor.SensorReached(5774);
          throw new ArgumentOutOfRangeException("BufferSize", "BufferSize must be greater than 1024 bytes");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5775);
        _bufferSize = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5776);
      }
    }
    public int Crc32 {
      get {
        System.Int32 RNTRNTRNT_665 = _Crc32;
        IACSharpSensor.IACSharpSensor.SensorReached(5777);
        return RNTRNTRNT_665;
      }
    }
    public Int64 BytesProcessed {
      get {
        Int64 RNTRNTRNT_666 = _totalBytesProcessed;
        IACSharpSensor.IACSharpSensor.SensorReached(5778);
        return RNTRNTRNT_666;
      }
    }
    private void _InitializePoolOfWorkItems()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5779);
      _toWrite = new Queue<int>();
      _toFill = new Queue<int>();
      _pool = new System.Collections.Generic.List<WorkItem>();
      int nTasks = BufferPairsPerCore * Environment.ProcessorCount;
      nTasks = Math.Min(nTasks, _maxBufferPairs);
      IACSharpSensor.IACSharpSensor.SensorReached(5780);
      for (int i = 0; i < nTasks; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5781);
        _pool.Add(new WorkItem(_bufferSize, _compressLevel, Strategy, i));
        _toFill.Enqueue(i);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5782);
      _newlyCompressedBlob = new AutoResetEvent(false);
      _runningCrc = new Ionic.Crc.CRC32();
      _currentlyFilling = -1;
      _lastFilled = -1;
      _lastWritten = -1;
      _latestCompressed = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(5783);
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5784);
      bool mustWait = false;
      IACSharpSensor.IACSharpSensor.SensorReached(5785);
      if (_isClosed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5786);
        throw new InvalidOperationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5787);
      if (_pendingException != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(5788);
        _handlingException = true;
        var pe = _pendingException;
        _pendingException = null;
        IACSharpSensor.IACSharpSensor.SensorReached(5789);
        throw pe;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5790);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5791);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5792);
      if (!_firstWriteDone) {
        IACSharpSensor.IACSharpSensor.SensorReached(5793);
        _InitializePoolOfWorkItems();
        _firstWriteDone = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5794);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(5795);
        EmitPendingBuffers(false, mustWait);
        mustWait = false;
        int ix = -1;
        IACSharpSensor.IACSharpSensor.SensorReached(5796);
        if (_currentlyFilling >= 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5797);
          ix = _currentlyFilling;
          TraceOutput(TraceBits.WriteTake, "Write    notake   wi({0}) lf({1})", ix, _lastFilled);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5798);
          TraceOutput(TraceBits.WriteTake, "Write    take?");
          IACSharpSensor.IACSharpSensor.SensorReached(5799);
          if (_toFill.Count == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(5800);
            mustWait = true;
            IACSharpSensor.IACSharpSensor.SensorReached(5801);
            continue;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5802);
          ix = _toFill.Dequeue();
          TraceOutput(TraceBits.WriteTake, "Write    take     wi({0}) lf({1})", ix, _lastFilled);
          ++_lastFilled;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5803);
        WorkItem workitem = _pool[ix];
        int limit = ((workitem.buffer.Length - workitem.inputBytesAvailable) > count) ? count : (workitem.buffer.Length - workitem.inputBytesAvailable);
        workitem.ordinal = _lastFilled;
        TraceOutput(TraceBits.Write, "Write    lock     wi({0}) ord({1}) iba({2})", workitem.index, workitem.ordinal, workitem.inputBytesAvailable);
        Buffer.BlockCopy(buffer, offset, workitem.buffer, workitem.inputBytesAvailable, limit);
        count -= limit;
        offset += limit;
        workitem.inputBytesAvailable += limit;
        IACSharpSensor.IACSharpSensor.SensorReached(5804);
        if (workitem.inputBytesAvailable == workitem.buffer.Length) {
          IACSharpSensor.IACSharpSensor.SensorReached(5805);
          TraceOutput(TraceBits.Write, "Write    QUWI     wi({0}) ord({1}) iba({2}) nf({3})", workitem.index, workitem.ordinal, workitem.inputBytesAvailable);
          IACSharpSensor.IACSharpSensor.SensorReached(5806);
          if (!ThreadPool.QueueUserWorkItem(_DeflateOne, workitem)) {
            IACSharpSensor.IACSharpSensor.SensorReached(5807);
            throw new Exception("Cannot enqueue workitem");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5808);
          _currentlyFilling = -1;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5809);
          _currentlyFilling = ix;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5810);
        if (count > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5811);
          TraceOutput(TraceBits.WriteEnter, "Write    more");
        }
      } while (count > 0);
      IACSharpSensor.IACSharpSensor.SensorReached(5812);
      TraceOutput(TraceBits.WriteEnter, "Write    exit");
      IACSharpSensor.IACSharpSensor.SensorReached(5813);
      return;
    }
    private void _FlushFinish()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5814);
      byte[] buffer = new byte[128];
      var compressor = new ZlibCodec();
      int rc = compressor.InitializeDeflate(_compressLevel, false);
      compressor.InputBuffer = null;
      compressor.NextIn = 0;
      compressor.AvailableBytesIn = 0;
      compressor.OutputBuffer = buffer;
      compressor.NextOut = 0;
      compressor.AvailableBytesOut = buffer.Length;
      rc = compressor.Deflate(FlushType.Finish);
      IACSharpSensor.IACSharpSensor.SensorReached(5815);
      if (rc != ZlibConstants.Z_STREAM_END && rc != ZlibConstants.Z_OK) {
        IACSharpSensor.IACSharpSensor.SensorReached(5816);
        throw new Exception("deflating: " + compressor.Message);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5817);
      if (buffer.Length - compressor.AvailableBytesOut > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5818);
        TraceOutput(TraceBits.EmitBegin, "Emit     begin    flush bytes({0})", buffer.Length - compressor.AvailableBytesOut);
        _outStream.Write(buffer, 0, buffer.Length - compressor.AvailableBytesOut);
        TraceOutput(TraceBits.EmitDone, "Emit     done     flush");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5819);
      compressor.EndDeflate();
      _Crc32 = _runningCrc.Crc32Result;
      IACSharpSensor.IACSharpSensor.SensorReached(5820);
    }
    private void _Flush(bool lastInput)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5821);
      if (_isClosed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5822);
        throw new InvalidOperationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5823);
      if (emitting) {
        IACSharpSensor.IACSharpSensor.SensorReached(5824);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5825);
      if (_currentlyFilling >= 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5826);
        WorkItem workitem = _pool[_currentlyFilling];
        _DeflateOne(workitem);
        _currentlyFilling = -1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5827);
      if (lastInput) {
        IACSharpSensor.IACSharpSensor.SensorReached(5828);
        EmitPendingBuffers(true, false);
        _FlushFinish();
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(5829);
        EmitPendingBuffers(false, false);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5830);
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5831);
      if (_pendingException != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(5832);
        _handlingException = true;
        var pe = _pendingException;
        _pendingException = null;
        IACSharpSensor.IACSharpSensor.SensorReached(5833);
        throw pe;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5834);
      if (_handlingException) {
        IACSharpSensor.IACSharpSensor.SensorReached(5835);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5836);
      _Flush(false);
      IACSharpSensor.IACSharpSensor.SensorReached(5837);
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5838);
      TraceOutput(TraceBits.Session, "Close {0:X8}", this.GetHashCode());
      IACSharpSensor.IACSharpSensor.SensorReached(5839);
      if (_pendingException != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(5840);
        _handlingException = true;
        var pe = _pendingException;
        _pendingException = null;
        IACSharpSensor.IACSharpSensor.SensorReached(5841);
        throw pe;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5842);
      if (_handlingException) {
        IACSharpSensor.IACSharpSensor.SensorReached(5843);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5844);
      if (_isClosed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5845);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5846);
      _Flush(true);
      IACSharpSensor.IACSharpSensor.SensorReached(5847);
      if (!_leaveOpen) {
        IACSharpSensor.IACSharpSensor.SensorReached(5848);
        _outStream.Close();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5849);
      _isClosed = true;
      IACSharpSensor.IACSharpSensor.SensorReached(5850);
    }
    public new void Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5851);
      TraceOutput(TraceBits.Lifecycle, "Dispose  {0:X8}", this.GetHashCode());
      Close();
      _pool = null;
      Dispose(true);
      IACSharpSensor.IACSharpSensor.SensorReached(5852);
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5853);
      base.Dispose(disposing);
      IACSharpSensor.IACSharpSensor.SensorReached(5854);
    }
    public void Reset(Stream stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5855);
      TraceOutput(TraceBits.Session, "-------------------------------------------------------");
      TraceOutput(TraceBits.Session, "Reset {0:X8} firstDone({1})", this.GetHashCode(), _firstWriteDone);
      IACSharpSensor.IACSharpSensor.SensorReached(5856);
      if (!_firstWriteDone) {
        IACSharpSensor.IACSharpSensor.SensorReached(5857);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5858);
      _toWrite.Clear();
      _toFill.Clear();
      IACSharpSensor.IACSharpSensor.SensorReached(5859);
      foreach (var workitem in _pool) {
        IACSharpSensor.IACSharpSensor.SensorReached(5860);
        _toFill.Enqueue(workitem.index);
        workitem.ordinal = -1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5861);
      _firstWriteDone = false;
      _totalBytesProcessed = 0L;
      _runningCrc = new Ionic.Crc.CRC32();
      _isClosed = false;
      _currentlyFilling = -1;
      _lastFilled = -1;
      _lastWritten = -1;
      _latestCompressed = -1;
      _outStream = stream;
      IACSharpSensor.IACSharpSensor.SensorReached(5862);
    }
    private void EmitPendingBuffers(bool doAll, bool mustWait)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5863);
      if (emitting) {
        IACSharpSensor.IACSharpSensor.SensorReached(5864);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5865);
      emitting = true;
      IACSharpSensor.IACSharpSensor.SensorReached(5866);
      if (doAll || mustWait) {
        IACSharpSensor.IACSharpSensor.SensorReached(5867);
        _newlyCompressedBlob.WaitOne();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5868);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(5869);
        int firstSkip = -1;
        int millisecondsToWait = doAll ? 200 : (mustWait ? -1 : 0);
        int nextToWrite = -1;
        IACSharpSensor.IACSharpSensor.SensorReached(5870);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(5871);
          if (Monitor.TryEnter(_toWrite, millisecondsToWait)) {
            IACSharpSensor.IACSharpSensor.SensorReached(5872);
            nextToWrite = -1;
            IACSharpSensor.IACSharpSensor.SensorReached(5873);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(5874);
              if (_toWrite.Count > 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5875);
                nextToWrite = _toWrite.Dequeue();
              }
            } finally {
              IACSharpSensor.IACSharpSensor.SensorReached(5876);
              Monitor.Exit(_toWrite);
              IACSharpSensor.IACSharpSensor.SensorReached(5877);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5878);
            if (nextToWrite >= 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5879);
              WorkItem workitem = _pool[nextToWrite];
              IACSharpSensor.IACSharpSensor.SensorReached(5880);
              if (workitem.ordinal != _lastWritten + 1) {
                IACSharpSensor.IACSharpSensor.SensorReached(5881);
                TraceOutput(TraceBits.EmitSkip, "Emit     skip     wi({0}) ord({1}) lw({2}) fs({3})", workitem.index, workitem.ordinal, _lastWritten, firstSkip);
                lock (_toWrite) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5882);
                  _toWrite.Enqueue(nextToWrite);
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5883);
                if (firstSkip == nextToWrite) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5884);
                  _newlyCompressedBlob.WaitOne();
                  firstSkip = -1;
                } else if (firstSkip == -1) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5885);
                  firstSkip = nextToWrite;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5886);
                continue;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5887);
              firstSkip = -1;
              TraceOutput(TraceBits.EmitBegin, "Emit     begin    wi({0}) ord({1})              cba({2})", workitem.index, workitem.ordinal, workitem.compressedBytesAvailable);
              _outStream.Write(workitem.compressed, 0, workitem.compressedBytesAvailable);
              _runningCrc.Combine(workitem.crc, workitem.inputBytesAvailable);
              _totalBytesProcessed += workitem.inputBytesAvailable;
              workitem.inputBytesAvailable = 0;
              TraceOutput(TraceBits.EmitDone, "Emit     done     wi({0}) ord({1})              cba({2}) mtw({3})", workitem.index, workitem.ordinal, workitem.compressedBytesAvailable, millisecondsToWait);
              _lastWritten = workitem.ordinal;
              _toFill.Enqueue(workitem.index);
              IACSharpSensor.IACSharpSensor.SensorReached(5888);
              if (millisecondsToWait == -1) {
                IACSharpSensor.IACSharpSensor.SensorReached(5889);
                millisecondsToWait = 0;
              }
            }
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(5890);
            nextToWrite = -1;
          }
        } while (nextToWrite >= 0);
      } while (doAll && (_lastWritten != _latestCompressed));
      IACSharpSensor.IACSharpSensor.SensorReached(5891);
      emitting = false;
      IACSharpSensor.IACSharpSensor.SensorReached(5892);
    }
    private void _DeflateOne(Object wi)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5893);
      WorkItem workitem = (WorkItem)wi;
      IACSharpSensor.IACSharpSensor.SensorReached(5894);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(5895);
        int myItem = workitem.index;
        Ionic.Crc.CRC32 crc = new Ionic.Crc.CRC32();
        crc.SlurpBlock(workitem.buffer, 0, workitem.inputBytesAvailable);
        DeflateOneSegment(workitem);
        workitem.crc = crc.Crc32Result;
        TraceOutput(TraceBits.Compress, "Compress          wi({0}) ord({1}) len({2})", workitem.index, workitem.ordinal, workitem.compressedBytesAvailable);
        lock (_latestLock) {
          IACSharpSensor.IACSharpSensor.SensorReached(5896);
          if (workitem.ordinal > _latestCompressed) {
            IACSharpSensor.IACSharpSensor.SensorReached(5897);
            _latestCompressed = workitem.ordinal;
          }
        }
        lock (_toWrite) {
          IACSharpSensor.IACSharpSensor.SensorReached(5898);
          _toWrite.Enqueue(workitem.index);
        }
        _newlyCompressedBlob.Set();
      } catch (System.Exception exc1) {
        lock (_eLock) {
          IACSharpSensor.IACSharpSensor.SensorReached(5899);
          if (_pendingException != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(5900);
            _pendingException = exc1;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5901);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5902);
    }
    private bool DeflateOneSegment(WorkItem workitem)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5903);
      ZlibCodec compressor = workitem.compressor;
      int rc = 0;
      compressor.ResetDeflate();
      compressor.NextIn = 0;
      compressor.AvailableBytesIn = workitem.inputBytesAvailable;
      compressor.NextOut = 0;
      compressor.AvailableBytesOut = workitem.compressed.Length;
      IACSharpSensor.IACSharpSensor.SensorReached(5904);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(5905);
        compressor.Deflate(FlushType.None);
      } while (compressor.AvailableBytesIn > 0 || compressor.AvailableBytesOut == 0);
      IACSharpSensor.IACSharpSensor.SensorReached(5906);
      rc = compressor.Deflate(FlushType.Sync);
      workitem.compressedBytesAvailable = (int)compressor.TotalBytesOut;
      System.Boolean RNTRNTRNT_667 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(5907);
      return RNTRNTRNT_667;
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceOutput(TraceBits bits, string format, params object[] varParams)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5908);
      if ((bits & _DesiredTrace) != 0) {
        lock (_outputLock) {
          IACSharpSensor.IACSharpSensor.SensorReached(5909);
          int tid = Thread.CurrentThread.GetHashCode();
          Console.ForegroundColor = (ConsoleColor)(tid % 8 + 8);
          Console.Write("{0:000} PDOS ", tid);
          Console.WriteLine(format, varParams);
          Console.ResetColor();
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5910);
    }
    [Flags()]
    enum TraceBits : uint
    {
      None = 0,
      NotUsed1 = 1,
      EmitLock = 2,
      EmitEnter = 4,
      EmitBegin = 8,
      EmitDone = 16,
      EmitSkip = 32,
      EmitAll = 58,
      Flush = 64,
      Lifecycle = 128,
      Session = 256,
      Synch = 512,
      Instance = 1024,
      Compress = 2048,
      Write = 4096,
      WriteEnter = 8192,
      WriteTake = 16384,
      All = 0xffffffffu
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_668 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(5911);
        return RNTRNTRNT_668;
      }
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_669 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(5912);
        return RNTRNTRNT_669;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_670 = _outStream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(5913);
        return RNTRNTRNT_670;
      }
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5914);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_671 = _outStream.Position;
        IACSharpSensor.IACSharpSensor.SensorReached(5915);
        return RNTRNTRNT_671;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5916);
        throw new NotSupportedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5917);
      throw new NotSupportedException();
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5918);
      throw new NotSupportedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5919);
      throw new NotSupportedException();
    }
  }
}
