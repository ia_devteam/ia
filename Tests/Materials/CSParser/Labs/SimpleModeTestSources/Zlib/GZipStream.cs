using System;
using System.IO;
namespace Ionic.Zlib
{
  public class GZipStream : System.IO.Stream
  {
    public String Comment {
      get {
        String RNTRNTRNT_569 = _Comment;
        IACSharpSensor.IACSharpSensor.SensorReached(5072);
        return RNTRNTRNT_569;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5073);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5074);
          throw new ObjectDisposedException("GZipStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5075);
        _Comment = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5076);
      }
    }
    public String FileName {
      get {
        String RNTRNTRNT_570 = _FileName;
        IACSharpSensor.IACSharpSensor.SensorReached(5077);
        return RNTRNTRNT_570;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5078);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5079);
          throw new ObjectDisposedException("GZipStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5080);
        _FileName = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5081);
        if (_FileName == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(5082);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5083);
        if (_FileName.IndexOf("/") != -1) {
          IACSharpSensor.IACSharpSensor.SensorReached(5084);
          _FileName = _FileName.Replace("/", "\\");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5085);
        if (_FileName.EndsWith("\\")) {
          IACSharpSensor.IACSharpSensor.SensorReached(5086);
          throw new Exception("Illegal filename");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5087);
        if (_FileName.IndexOf("\\") != -1) {
          IACSharpSensor.IACSharpSensor.SensorReached(5088);
          _FileName = Path.GetFileName(_FileName);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5089);
      }
    }
    public DateTime? LastModified;
    public int Crc32 {
      get {
        System.Int32 RNTRNTRNT_571 = _Crc32;
        IACSharpSensor.IACSharpSensor.SensorReached(5090);
        return RNTRNTRNT_571;
      }
    }
    private int _headerByteCount;
    internal ZlibBaseStream _baseStream;
    bool _disposed;
    bool _firstReadDone;
    string _FileName;
    string _Comment;
    int _Crc32;
    public GZipStream(Stream stream, CompressionMode mode) : this(stream, mode, CompressionLevel.Default, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5091);
    }
    public GZipStream(Stream stream, CompressionMode mode, CompressionLevel level) : this(stream, mode, level, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5092);
    }
    public GZipStream(Stream stream, CompressionMode mode, bool leaveOpen) : this(stream, mode, CompressionLevel.Default, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5093);
    }
    public GZipStream(Stream stream, CompressionMode mode, CompressionLevel level, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5094);
      _baseStream = new ZlibBaseStream(stream, mode, level, ZlibStreamFlavor.GZIP, leaveOpen);
      IACSharpSensor.IACSharpSensor.SensorReached(5095);
    }
    public virtual FlushType FlushMode {
      get {
        FlushType RNTRNTRNT_572 = (this._baseStream._flushMode);
        IACSharpSensor.IACSharpSensor.SensorReached(5096);
        return RNTRNTRNT_572;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5097);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5098);
          throw new ObjectDisposedException("GZipStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5099);
        this._baseStream._flushMode = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5100);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_573 = this._baseStream._bufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(5101);
        return RNTRNTRNT_573;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5102);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5103);
          throw new ObjectDisposedException("GZipStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5104);
        if (this._baseStream._workingBuffer != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(5105);
          throw new ZlibException("The working buffer is already set.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5106);
        if (value < ZlibConstants.WorkingBufferSizeMin) {
          IACSharpSensor.IACSharpSensor.SensorReached(5107);
          throw new ZlibException(String.Format("Don't be silly. {0} bytes?? Use a bigger buffer, at least {1}.", value, ZlibConstants.WorkingBufferSizeMin));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5108);
        this._baseStream._bufferSize = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5109);
      }
    }
    public virtual long TotalIn {
      get {
        System.Int64 RNTRNTRNT_574 = this._baseStream._z.TotalBytesIn;
        IACSharpSensor.IACSharpSensor.SensorReached(5110);
        return RNTRNTRNT_574;
      }
    }
    public virtual long TotalOut {
      get {
        System.Int64 RNTRNTRNT_575 = this._baseStream._z.TotalBytesOut;
        IACSharpSensor.IACSharpSensor.SensorReached(5111);
        return RNTRNTRNT_575;
      }
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5112);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(5113);
        if (!_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5114);
          if (disposing && (this._baseStream != null)) {
            IACSharpSensor.IACSharpSensor.SensorReached(5115);
            this._baseStream.Close();
            this._Crc32 = _baseStream.Crc32;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5116);
          _disposed = true;
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(5117);
        base.Dispose(disposing);
        IACSharpSensor.IACSharpSensor.SensorReached(5118);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5119);
    }
    public override bool CanRead {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5120);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5121);
          throw new ObjectDisposedException("GZipStream");
        }
        System.Boolean RNTRNTRNT_576 = _baseStream._stream.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(5122);
        return RNTRNTRNT_576;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_577 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(5123);
        return RNTRNTRNT_577;
      }
    }
    public override bool CanWrite {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5124);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5125);
          throw new ObjectDisposedException("GZipStream");
        }
        System.Boolean RNTRNTRNT_578 = _baseStream._stream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(5126);
        return RNTRNTRNT_578;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5127);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5128);
        throw new ObjectDisposedException("GZipStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5129);
      _baseStream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(5130);
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5131);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5132);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Writer) {
          System.Int64 RNTRNTRNT_579 = this._baseStream._z.TotalBytesOut + _headerByteCount;
          IACSharpSensor.IACSharpSensor.SensorReached(5133);
          return RNTRNTRNT_579;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5134);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Reader) {
          System.Int64 RNTRNTRNT_580 = this._baseStream._z.TotalBytesIn + this._baseStream._gzipHeaderByteCount;
          IACSharpSensor.IACSharpSensor.SensorReached(5135);
          return RNTRNTRNT_580;
        }
        System.Int64 RNTRNTRNT_581 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(5136);
        return RNTRNTRNT_581;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5137);
        throw new NotImplementedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5138);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5139);
        throw new ObjectDisposedException("GZipStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5140);
      int n = _baseStream.Read(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(5141);
      if (!_firstReadDone) {
        IACSharpSensor.IACSharpSensor.SensorReached(5142);
        _firstReadDone = true;
        FileName = _baseStream._GzipFileName;
        Comment = _baseStream._GzipComment;
      }
      System.Int32 RNTRNTRNT_582 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(5143);
      return RNTRNTRNT_582;
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5144);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5145);
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5146);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5147);
        throw new ObjectDisposedException("GZipStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5148);
      if (_baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Undefined) {
        IACSharpSensor.IACSharpSensor.SensorReached(5149);
        if (_baseStream._wantCompress) {
          IACSharpSensor.IACSharpSensor.SensorReached(5150);
          _headerByteCount = EmitHeader();
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5151);
          throw new InvalidOperationException();
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5152);
      _baseStream.Write(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(5153);
    }
    static internal readonly System.DateTime _unixEpoch = new System.DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    static internal readonly System.Text.Encoding iso8859dash1 = System.Text.Encoding.GetEncoding("iso-8859-1");
    private int EmitHeader()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5154);
      byte[] commentBytes = (Comment == null) ? null : iso8859dash1.GetBytes(Comment);
      byte[] filenameBytes = (FileName == null) ? null : iso8859dash1.GetBytes(FileName);
      int cbLength = (Comment == null) ? 0 : commentBytes.Length + 1;
      int fnLength = (FileName == null) ? 0 : filenameBytes.Length + 1;
      int bufferLength = 10 + cbLength + fnLength;
      byte[] header = new byte[bufferLength];
      int i = 0;
      header[i++] = 0x1f;
      header[i++] = 0x8b;
      header[i++] = 8;
      byte flag = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5155);
      if (Comment != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(5156);
        flag ^= 0x10;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5157);
      if (FileName != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(5158);
        flag ^= 0x8;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5159);
      header[i++] = flag;
      IACSharpSensor.IACSharpSensor.SensorReached(5160);
      if (!LastModified.HasValue) {
        IACSharpSensor.IACSharpSensor.SensorReached(5161);
        LastModified = DateTime.Now;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5162);
      System.TimeSpan delta = LastModified.Value - _unixEpoch;
      Int32 timet = (Int32)delta.TotalSeconds;
      Array.Copy(BitConverter.GetBytes(timet), 0, header, i, 4);
      i += 4;
      header[i++] = 0;
      header[i++] = 0xff;
      IACSharpSensor.IACSharpSensor.SensorReached(5163);
      if (fnLength != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5164);
        Array.Copy(filenameBytes, 0, header, i, fnLength - 1);
        i += fnLength - 1;
        header[i++] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5165);
      if (cbLength != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5166);
        Array.Copy(commentBytes, 0, header, i, cbLength - 1);
        i += cbLength - 1;
        header[i++] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5167);
      _baseStream._stream.Write(header, 0, header.Length);
      System.Int32 RNTRNTRNT_583 = header.Length;
      IACSharpSensor.IACSharpSensor.SensorReached(5168);
      return RNTRNTRNT_583;
    }
    public static byte[] CompressString(String s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5169);
      using (var ms = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(5170);
        System.IO.Stream compressor = new GZipStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressString(s, compressor);
        System.Byte[] RNTRNTRNT_584 = ms.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(5171);
        return RNTRNTRNT_584;
      }
    }
    public static byte[] CompressBuffer(byte[] b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5172);
      using (var ms = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(5173);
        System.IO.Stream compressor = new GZipStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressBuffer(b, compressor);
        System.Byte[] RNTRNTRNT_585 = ms.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(5174);
        return RNTRNTRNT_585;
      }
    }
    public static String UncompressString(byte[] compressed)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5175);
      using (var input = new MemoryStream(compressed)) {
        IACSharpSensor.IACSharpSensor.SensorReached(5176);
        Stream decompressor = new GZipStream(input, CompressionMode.Decompress);
        String RNTRNTRNT_586 = ZlibBaseStream.UncompressString(compressed, decompressor);
        IACSharpSensor.IACSharpSensor.SensorReached(5177);
        return RNTRNTRNT_586;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5178);
      using (var input = new System.IO.MemoryStream(compressed)) {
        IACSharpSensor.IACSharpSensor.SensorReached(5179);
        System.IO.Stream decompressor = new GZipStream(input, CompressionMode.Decompress);
        System.Byte[] RNTRNTRNT_587 = ZlibBaseStream.UncompressBuffer(compressed, decompressor);
        IACSharpSensor.IACSharpSensor.SensorReached(5180);
        return RNTRNTRNT_587;
      }
    }
  }
}
