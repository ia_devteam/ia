using System;
namespace Ionic.Zlib
{
  public class DeflateStream : System.IO.Stream
  {
    internal ZlibBaseStream _baseStream;
    internal System.IO.Stream _innerStream;
    bool _disposed;
    public DeflateStream(System.IO.Stream stream, CompressionMode mode) : this(stream, mode, CompressionLevel.Default, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4999);
    }
    public DeflateStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level) : this(stream, mode, level, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5000);
    }
    public DeflateStream(System.IO.Stream stream, CompressionMode mode, bool leaveOpen) : this(stream, mode, CompressionLevel.Default, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5001);
    }
    public DeflateStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5002);
      _innerStream = stream;
      _baseStream = new ZlibBaseStream(stream, mode, level, ZlibStreamFlavor.DEFLATE, leaveOpen);
      IACSharpSensor.IACSharpSensor.SensorReached(5003);
    }
    public virtual FlushType FlushMode {
      get {
        FlushType RNTRNTRNT_553 = (this._baseStream._flushMode);
        IACSharpSensor.IACSharpSensor.SensorReached(5004);
        return RNTRNTRNT_553;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5005);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5006);
          throw new ObjectDisposedException("DeflateStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5007);
        this._baseStream._flushMode = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5008);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_554 = this._baseStream._bufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(5009);
        return RNTRNTRNT_554;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5010);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5011);
          throw new ObjectDisposedException("DeflateStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5012);
        if (this._baseStream._workingBuffer != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(5013);
          throw new ZlibException("The working buffer is already set.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5014);
        if (value < ZlibConstants.WorkingBufferSizeMin) {
          IACSharpSensor.IACSharpSensor.SensorReached(5015);
          throw new ZlibException(String.Format("Don't be silly. {0} bytes?? Use a bigger buffer, at least {1}.", value, ZlibConstants.WorkingBufferSizeMin));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5016);
        this._baseStream._bufferSize = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5017);
      }
    }
    public CompressionStrategy Strategy {
      get {
        CompressionStrategy RNTRNTRNT_555 = this._baseStream.Strategy;
        IACSharpSensor.IACSharpSensor.SensorReached(5018);
        return RNTRNTRNT_555;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5019);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5020);
          throw new ObjectDisposedException("DeflateStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5021);
        this._baseStream.Strategy = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5022);
      }
    }
    public virtual long TotalIn {
      get {
        System.Int64 RNTRNTRNT_556 = this._baseStream._z.TotalBytesIn;
        IACSharpSensor.IACSharpSensor.SensorReached(5023);
        return RNTRNTRNT_556;
      }
    }
    public virtual long TotalOut {
      get {
        System.Int64 RNTRNTRNT_557 = this._baseStream._z.TotalBytesOut;
        IACSharpSensor.IACSharpSensor.SensorReached(5024);
        return RNTRNTRNT_557;
      }
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5025);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(5026);
        if (!_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5027);
          if (disposing && (this._baseStream != null)) {
            IACSharpSensor.IACSharpSensor.SensorReached(5028);
            this._baseStream.Close();
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5029);
          _disposed = true;
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(5030);
        base.Dispose(disposing);
        IACSharpSensor.IACSharpSensor.SensorReached(5031);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5032);
    }
    public override bool CanRead {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5033);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5034);
          throw new ObjectDisposedException("DeflateStream");
        }
        System.Boolean RNTRNTRNT_558 = _baseStream._stream.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(5035);
        return RNTRNTRNT_558;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_559 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(5036);
        return RNTRNTRNT_559;
      }
    }
    public override bool CanWrite {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5037);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5038);
          throw new ObjectDisposedException("DeflateStream");
        }
        System.Boolean RNTRNTRNT_560 = _baseStream._stream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(5039);
        return RNTRNTRNT_560;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5040);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5041);
        throw new ObjectDisposedException("DeflateStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5042);
      _baseStream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(5043);
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5044);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5045);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Writer) {
          System.Int64 RNTRNTRNT_561 = this._baseStream._z.TotalBytesOut;
          IACSharpSensor.IACSharpSensor.SensorReached(5046);
          return RNTRNTRNT_561;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5047);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Reader) {
          System.Int64 RNTRNTRNT_562 = this._baseStream._z.TotalBytesIn;
          IACSharpSensor.IACSharpSensor.SensorReached(5048);
          return RNTRNTRNT_562;
        }
        System.Int64 RNTRNTRNT_563 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(5049);
        return RNTRNTRNT_563;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5050);
        throw new NotImplementedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5051);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5052);
        throw new ObjectDisposedException("DeflateStream");
      }
      System.Int32 RNTRNTRNT_564 = _baseStream.Read(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(5053);
      return RNTRNTRNT_564;
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5054);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5055);
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5056);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5057);
        throw new ObjectDisposedException("DeflateStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5058);
      _baseStream.Write(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(5059);
    }
    public static byte[] CompressString(String s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5060);
      using (var ms = new System.IO.MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(5061);
        System.IO.Stream compressor = new DeflateStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressString(s, compressor);
        System.Byte[] RNTRNTRNT_565 = ms.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(5062);
        return RNTRNTRNT_565;
      }
    }
    public static byte[] CompressBuffer(byte[] b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5063);
      using (var ms = new System.IO.MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(5064);
        System.IO.Stream compressor = new DeflateStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressBuffer(b, compressor);
        System.Byte[] RNTRNTRNT_566 = ms.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(5065);
        return RNTRNTRNT_566;
      }
    }
    public static String UncompressString(byte[] compressed)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5066);
      using (var input = new System.IO.MemoryStream(compressed)) {
        IACSharpSensor.IACSharpSensor.SensorReached(5067);
        System.IO.Stream decompressor = new DeflateStream(input, CompressionMode.Decompress);
        String RNTRNTRNT_567 = ZlibBaseStream.UncompressString(compressed, decompressor);
        IACSharpSensor.IACSharpSensor.SensorReached(5068);
        return RNTRNTRNT_567;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5069);
      using (var input = new System.IO.MemoryStream(compressed)) {
        IACSharpSensor.IACSharpSensor.SensorReached(5070);
        System.IO.Stream decompressor = new DeflateStream(input, CompressionMode.Decompress);
        System.Byte[] RNTRNTRNT_568 = ZlibBaseStream.UncompressBuffer(compressed, decompressor);
        IACSharpSensor.IACSharpSensor.SensorReached(5071);
        return RNTRNTRNT_568;
      }
    }
  }
}
