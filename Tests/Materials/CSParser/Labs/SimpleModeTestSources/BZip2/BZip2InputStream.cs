using System;
using System.IO;
namespace Ionic.BZip2
{
  public class BZip2InputStream : System.IO.Stream
  {
    bool _disposed;
    bool _leaveOpen;
    Int64 totalBytesRead;
    private int last;
    private int origPtr;
    private int blockSize100k;
    private bool blockRandomised;
    private int bsBuff;
    private int bsLive;
    private readonly Ionic.Crc.CRC32 crc = new Ionic.Crc.CRC32(true);
    private int nInUse;
    private Stream input;
    private int currentChar = -1;
    enum CState
    {
      EOF = 0,
      START_BLOCK = 1,
      RAND_PART_A = 2,
      RAND_PART_B = 3,
      RAND_PART_C = 4,
      NO_RAND_PART_A = 5,
      NO_RAND_PART_B = 6,
      NO_RAND_PART_C = 7
    }
    private CState currentState = CState.START_BLOCK;
    private uint storedBlockCRC, storedCombinedCRC;
    private uint computedBlockCRC, computedCombinedCRC;
    private int su_count;
    private int su_ch2;
    private int su_chPrev;
    private int su_i2;
    private int su_j2;
    private int su_rNToGo;
    private int su_rTPos;
    private int su_tPos;
    private char su_z;
    private BZip2InputStream.DecompressionState data;
    public BZip2InputStream(Stream input) : this(input, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4039);
    }
    public BZip2InputStream(Stream input, bool leaveOpen) : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4040);
      this.input = input;
      this._leaveOpen = leaveOpen;
      init();
      IACSharpSensor.IACSharpSensor.SensorReached(4041);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4042);
      if (offset < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4043);
        throw new IndexOutOfRangeException(String.Format("offset ({0}) must be > 0", offset));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4044);
      if (count < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4045);
        throw new IndexOutOfRangeException(String.Format("count ({0}) must be > 0", count));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4046);
      if (offset + count > buffer.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(4047);
        throw new IndexOutOfRangeException(String.Format("offset({0}) count({1}) bLength({2})", offset, count, buffer.Length));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4048);
      if (this.input == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4049);
        throw new IOException("the stream is not open");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4050);
      int hi = offset + count;
      int destOffset = offset;
      IACSharpSensor.IACSharpSensor.SensorReached(4051);
      for (int b; (destOffset < hi) && ((b = ReadByte()) >= 0);) {
        IACSharpSensor.IACSharpSensor.SensorReached(4052);
        buffer[destOffset++] = (byte)b;
      }
      System.Int32 RNTRNTRNT_486 = (destOffset == offset) ? -1 : (destOffset - offset);
      IACSharpSensor.IACSharpSensor.SensorReached(4053);
      return RNTRNTRNT_486;
    }
    private void MakeMaps()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4054);
      bool[] inUse = this.data.inUse;
      byte[] seqToUnseq = this.data.seqToUnseq;
      int n = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4055);
      for (int i = 0; i < 256; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4056);
        if (inUse[i]) {
          IACSharpSensor.IACSharpSensor.SensorReached(4057);
          seqToUnseq[n++] = (byte)i;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4058);
      this.nInUse = n;
      IACSharpSensor.IACSharpSensor.SensorReached(4059);
    }
    public override int ReadByte()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4060);
      int retChar = this.currentChar;
      totalBytesRead++;
      IACSharpSensor.IACSharpSensor.SensorReached(4061);
      switch (this.currentState) {
        case CState.EOF:
          System.Int32 RNTRNTRNT_487 = -1;
          IACSharpSensor.IACSharpSensor.SensorReached(4062);
          return RNTRNTRNT_487;
        case CState.START_BLOCK:
          IACSharpSensor.IACSharpSensor.SensorReached(4063);
          throw new IOException("bad state");
        case CState.RAND_PART_A:
          IACSharpSensor.IACSharpSensor.SensorReached(4064);
          throw new IOException("bad state");
        case CState.RAND_PART_B:
          IACSharpSensor.IACSharpSensor.SensorReached(4065);
          SetupRandPartB();
          IACSharpSensor.IACSharpSensor.SensorReached(4066);
          break;
        case CState.RAND_PART_C:
          IACSharpSensor.IACSharpSensor.SensorReached(4067);
          SetupRandPartC();
          IACSharpSensor.IACSharpSensor.SensorReached(4068);
          break;
        case CState.NO_RAND_PART_A:
          IACSharpSensor.IACSharpSensor.SensorReached(4069);
          throw new IOException("bad state");
        case CState.NO_RAND_PART_B:
          IACSharpSensor.IACSharpSensor.SensorReached(4070);
          SetupNoRandPartB();
          IACSharpSensor.IACSharpSensor.SensorReached(4071);
          break;
        case CState.NO_RAND_PART_C:
          IACSharpSensor.IACSharpSensor.SensorReached(4072);
          SetupNoRandPartC();
          IACSharpSensor.IACSharpSensor.SensorReached(4073);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(4074);
          throw new IOException("bad state");
      }
      System.Int32 RNTRNTRNT_488 = retChar;
      IACSharpSensor.IACSharpSensor.SensorReached(4075);
      return RNTRNTRNT_488;
    }
    public override bool CanRead {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4076);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(4077);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_489 = this.input.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(4078);
        return RNTRNTRNT_489;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_490 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(4079);
        return RNTRNTRNT_490;
      }
    }
    public override bool CanWrite {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4080);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(4081);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_491 = input.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(4082);
        return RNTRNTRNT_491;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4083);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(4084);
        throw new ObjectDisposedException("BZip2Stream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4085);
      input.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(4086);
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4087);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_492 = this.totalBytesRead;
        IACSharpSensor.IACSharpSensor.SensorReached(4088);
        return RNTRNTRNT_492;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4089);
        throw new NotImplementedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4090);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4091);
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4092);
      throw new NotImplementedException();
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4093);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(4094);
        if (!_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(4095);
          if (disposing && (this.input != null)) {
            IACSharpSensor.IACSharpSensor.SensorReached(4096);
            this.input.Close();
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4097);
          _disposed = true;
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(4098);
        base.Dispose(disposing);
        IACSharpSensor.IACSharpSensor.SensorReached(4099);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4100);
    }
    void init()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4101);
      if (null == this.input) {
        IACSharpSensor.IACSharpSensor.SensorReached(4102);
        throw new IOException("No input Stream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4103);
      if (!this.input.CanRead) {
        IACSharpSensor.IACSharpSensor.SensorReached(4104);
        throw new IOException("Unreadable input Stream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4105);
      CheckMagicChar('B', 0);
      CheckMagicChar('Z', 1);
      CheckMagicChar('h', 2);
      int blockSize = this.input.ReadByte();
      IACSharpSensor.IACSharpSensor.SensorReached(4106);
      if ((blockSize < '1') || (blockSize > '9')) {
        IACSharpSensor.IACSharpSensor.SensorReached(4107);
        throw new IOException("Stream is not BZip2 formatted: illegal " + "blocksize " + (char)blockSize);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4108);
      this.blockSize100k = blockSize - '0';
      InitBlock();
      SetupBlock();
      IACSharpSensor.IACSharpSensor.SensorReached(4109);
    }
    void CheckMagicChar(char expected, int position)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4110);
      int magic = this.input.ReadByte();
      IACSharpSensor.IACSharpSensor.SensorReached(4111);
      if (magic != (int)expected) {
        IACSharpSensor.IACSharpSensor.SensorReached(4112);
        var msg = String.Format("Not a valid BZip2 stream. byte {0}, expected '{1}', got '{2}'", position, (int)expected, magic);
        IACSharpSensor.IACSharpSensor.SensorReached(4113);
        throw new IOException(msg);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4114);
    }
    void InitBlock()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4115);
      char magic0 = bsGetUByte();
      char magic1 = bsGetUByte();
      char magic2 = bsGetUByte();
      char magic3 = bsGetUByte();
      char magic4 = bsGetUByte();
      char magic5 = bsGetUByte();
      IACSharpSensor.IACSharpSensor.SensorReached(4116);
      if (magic0 == 0x17 && magic1 == 0x72 && magic2 == 0x45 && magic3 == 0x38 && magic4 == 0x50 && magic5 == 0x90) {
        IACSharpSensor.IACSharpSensor.SensorReached(4117);
        complete();
      } else if (magic0 != 0x31 || magic1 != 0x41 || magic2 != 0x59 || magic3 != 0x26 || magic4 != 0x53 || magic5 != 0x59) {
        IACSharpSensor.IACSharpSensor.SensorReached(4122);
        this.currentState = CState.EOF;
        var msg = String.Format("bad block header at offset 0x{0:X}", this.input.Position);
        IACSharpSensor.IACSharpSensor.SensorReached(4123);
        throw new IOException(msg);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4118);
        this.storedBlockCRC = bsGetInt();
        this.blockRandomised = (GetBits(1) == 1);
        IACSharpSensor.IACSharpSensor.SensorReached(4119);
        if (this.data == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4120);
          this.data = new DecompressionState(this.blockSize100k);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4121);
        getAndMoveToFrontDecode();
        this.crc.Reset();
        this.currentState = CState.START_BLOCK;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4124);
    }
    private void EndBlock()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4125);
      this.computedBlockCRC = (uint)this.crc.Crc32Result;
      IACSharpSensor.IACSharpSensor.SensorReached(4126);
      if (this.storedBlockCRC != this.computedBlockCRC) {
        IACSharpSensor.IACSharpSensor.SensorReached(4127);
        var msg = String.Format("BZip2 CRC error (expected {0:X8}, computed {1:X8})", this.storedBlockCRC, this.computedBlockCRC);
        IACSharpSensor.IACSharpSensor.SensorReached(4128);
        throw new IOException(msg);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4129);
      this.computedCombinedCRC = (this.computedCombinedCRC << 1) | (this.computedCombinedCRC >> 31);
      this.computedCombinedCRC ^= this.computedBlockCRC;
      IACSharpSensor.IACSharpSensor.SensorReached(4130);
    }
    private void complete()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4131);
      this.storedCombinedCRC = bsGetInt();
      this.currentState = CState.EOF;
      this.data = null;
      IACSharpSensor.IACSharpSensor.SensorReached(4132);
      if (this.storedCombinedCRC != this.computedCombinedCRC) {
        IACSharpSensor.IACSharpSensor.SensorReached(4133);
        var msg = String.Format("BZip2 CRC error (expected {0:X8}, computed {1:X8})", this.storedCombinedCRC, this.computedCombinedCRC);
        IACSharpSensor.IACSharpSensor.SensorReached(4134);
        throw new IOException(msg);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4135);
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4136);
      Stream inShadow = this.input;
      IACSharpSensor.IACSharpSensor.SensorReached(4137);
      if (inShadow != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4138);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(4139);
          if (!this._leaveOpen) {
            IACSharpSensor.IACSharpSensor.SensorReached(4140);
            inShadow.Close();
          }
        } finally {
          IACSharpSensor.IACSharpSensor.SensorReached(4141);
          this.data = null;
          this.input = null;
          IACSharpSensor.IACSharpSensor.SensorReached(4142);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4143);
    }
    private int GetBits(int n)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4144);
      int bsLiveShadow = this.bsLive;
      int bsBuffShadow = this.bsBuff;
      IACSharpSensor.IACSharpSensor.SensorReached(4145);
      if (bsLiveShadow < n) {
        IACSharpSensor.IACSharpSensor.SensorReached(4146);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(4147);
          int thech = this.input.ReadByte();
          IACSharpSensor.IACSharpSensor.SensorReached(4148);
          if (thech < 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(4149);
            throw new IOException("unexpected end of stream");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4150);
          bsBuffShadow = (bsBuffShadow << 8) | thech;
          bsLiveShadow += 8;
        } while (bsLiveShadow < n);
        IACSharpSensor.IACSharpSensor.SensorReached(4151);
        this.bsBuff = bsBuffShadow;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4152);
      this.bsLive = bsLiveShadow - n;
      System.Int32 RNTRNTRNT_493 = (bsBuffShadow >> (bsLiveShadow - n)) & ((1 << n) - 1);
      IACSharpSensor.IACSharpSensor.SensorReached(4153);
      return RNTRNTRNT_493;
    }
    private bool bsGetBit()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4154);
      int bit = GetBits(1);
      System.Boolean RNTRNTRNT_494 = bit != 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4155);
      return RNTRNTRNT_494;
    }
    private char bsGetUByte()
    {
      System.Char RNTRNTRNT_495 = (char)GetBits(8);
      IACSharpSensor.IACSharpSensor.SensorReached(4156);
      return RNTRNTRNT_495;
    }
    private uint bsGetInt()
    {
      System.UInt32 RNTRNTRNT_496 = (uint)((((((GetBits(8) << 8) | GetBits(8)) << 8) | GetBits(8)) << 8) | GetBits(8));
      IACSharpSensor.IACSharpSensor.SensorReached(4157);
      return RNTRNTRNT_496;
    }
    private static void hbCreateDecodeTables(int[] limit, int[] bbase, int[] perm, char[] length, int minLen, int maxLen, int alphaSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4158);
      for (int i = minLen, pp = 0; i <= maxLen; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4159);
        for (int j = 0; j < alphaSize; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(4160);
          if (length[j] == i) {
            IACSharpSensor.IACSharpSensor.SensorReached(4161);
            perm[pp++] = j;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4162);
      for (int i = BZip2.MaxCodeLength; --i > 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(4163);
        bbase[i] = 0;
        limit[i] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4164);
      for (int i = 0; i < alphaSize; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4165);
        bbase[length[i] + 1]++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4166);
      for (int i = 1, b = bbase[0]; i < BZip2.MaxCodeLength; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4167);
        b += bbase[i];
        bbase[i] = b;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4168);
      for (int i = minLen, vec = 0, b = bbase[i]; i <= maxLen; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4169);
        int nb = bbase[i + 1];
        vec += nb - b;
        b = nb;
        limit[i] = vec - 1;
        vec <<= 1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4170);
      for (int i = minLen + 1; i <= maxLen; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4171);
        bbase[i] = ((limit[i - 1] + 1) << 1) - bbase[i];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4172);
    }
    private void recvDecodingTables()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4173);
      var s = this.data;
      bool[] inUse = s.inUse;
      byte[] pos = s.recvDecodingTables_pos;
      int inUse16 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4174);
      for (int i = 0; i < 16; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4175);
        if (bsGetBit()) {
          IACSharpSensor.IACSharpSensor.SensorReached(4176);
          inUse16 |= 1 << i;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4177);
      for (int i = 256; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(4178);
        inUse[i] = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4179);
      for (int i = 0; i < 16; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4180);
        if ((inUse16 & (1 << i)) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4181);
          int i16 = i << 4;
          IACSharpSensor.IACSharpSensor.SensorReached(4182);
          for (int j = 0; j < 16; j++) {
            IACSharpSensor.IACSharpSensor.SensorReached(4183);
            if (bsGetBit()) {
              IACSharpSensor.IACSharpSensor.SensorReached(4184);
              inUse[i16 + j] = true;
            }
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4185);
      MakeMaps();
      int alphaSize = this.nInUse + 2;
      int nGroups = GetBits(3);
      int nSelectors = GetBits(15);
      IACSharpSensor.IACSharpSensor.SensorReached(4186);
      for (int i = 0; i < nSelectors; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4187);
        int j = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(4188);
        while (bsGetBit()) {
          IACSharpSensor.IACSharpSensor.SensorReached(4189);
          j++;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4190);
        s.selectorMtf[i] = (byte)j;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4191);
      for (int v = nGroups; --v >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(4192);
        pos[v] = (byte)v;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4193);
      for (int i = 0; i < nSelectors; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4194);
        int v = s.selectorMtf[i];
        byte tmp = pos[v];
        IACSharpSensor.IACSharpSensor.SensorReached(4195);
        while (v > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4196);
          pos[v] = pos[v - 1];
          v--;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4197);
        pos[0] = tmp;
        s.selector[i] = tmp;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4198);
      char[][] len = s.temp_charArray2d;
      IACSharpSensor.IACSharpSensor.SensorReached(4199);
      for (int t = 0; t < nGroups; t++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4200);
        int curr = GetBits(5);
        char[] len_t = len[t];
        IACSharpSensor.IACSharpSensor.SensorReached(4201);
        for (int i = 0; i < alphaSize; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(4202);
          while (bsGetBit()) {
            IACSharpSensor.IACSharpSensor.SensorReached(4203);
            curr += bsGetBit() ? -1 : 1;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4204);
          len_t[i] = (char)curr;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4205);
      createHuffmanDecodingTables(alphaSize, nGroups);
      IACSharpSensor.IACSharpSensor.SensorReached(4206);
    }
    private void createHuffmanDecodingTables(int alphaSize, int nGroups)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4207);
      var s = this.data;
      char[][] len = s.temp_charArray2d;
      IACSharpSensor.IACSharpSensor.SensorReached(4208);
      for (int t = 0; t < nGroups; t++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4209);
        int minLen = 32;
        int maxLen = 0;
        char[] len_t = len[t];
        IACSharpSensor.IACSharpSensor.SensorReached(4210);
        for (int i = alphaSize; --i >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(4211);
          char lent = len_t[i];
          IACSharpSensor.IACSharpSensor.SensorReached(4212);
          if (lent > maxLen) {
            IACSharpSensor.IACSharpSensor.SensorReached(4213);
            maxLen = lent;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4214);
          if (lent < minLen) {
            IACSharpSensor.IACSharpSensor.SensorReached(4215);
            minLen = lent;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4216);
        hbCreateDecodeTables(s.gLimit[t], s.gBase[t], s.gPerm[t], len[t], minLen, maxLen, alphaSize);
        s.gMinlen[t] = minLen;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4217);
    }
    private void getAndMoveToFrontDecode()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4218);
      var s = this.data;
      this.origPtr = GetBits(24);
      IACSharpSensor.IACSharpSensor.SensorReached(4219);
      if (this.origPtr < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4220);
        throw new IOException("BZ_DATA_ERROR");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4221);
      if (this.origPtr > 10 + BZip2.BlockSizeMultiple * this.blockSize100k) {
        IACSharpSensor.IACSharpSensor.SensorReached(4222);
        throw new IOException("BZ_DATA_ERROR");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4223);
      recvDecodingTables();
      byte[] yy = s.getAndMoveToFrontDecode_yy;
      int limitLast = this.blockSize100k * BZip2.BlockSizeMultiple;
      IACSharpSensor.IACSharpSensor.SensorReached(4224);
      for (int i = 256; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(4225);
        yy[i] = (byte)i;
        s.unzftab[i] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4226);
      int groupNo = 0;
      int groupPos = BZip2.G_SIZE - 1;
      int eob = this.nInUse + 1;
      int nextSym = getAndMoveToFrontDecode0(0);
      int bsBuffShadow = this.bsBuff;
      int bsLiveShadow = this.bsLive;
      int lastShadow = -1;
      int zt = s.selector[groupNo] & 0xff;
      int[] base_zt = s.gBase[zt];
      int[] limit_zt = s.gLimit[zt];
      int[] perm_zt = s.gPerm[zt];
      int minLens_zt = s.gMinlen[zt];
      IACSharpSensor.IACSharpSensor.SensorReached(4227);
      while (nextSym != eob) {
        IACSharpSensor.IACSharpSensor.SensorReached(4228);
        if ((nextSym == BZip2.RUNA) || (nextSym == BZip2.RUNB)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4229);
          int es = -1;
          IACSharpSensor.IACSharpSensor.SensorReached(4230);
          for (int n = 1; true; n <<= 1) {
            IACSharpSensor.IACSharpSensor.SensorReached(4231);
            if (nextSym == BZip2.RUNA) {
              IACSharpSensor.IACSharpSensor.SensorReached(4232);
              es += n;
            } else if (nextSym == BZip2.RUNB) {
              IACSharpSensor.IACSharpSensor.SensorReached(4234);
              es += n << 1;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(4233);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(4235);
            if (groupPos == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(4236);
              groupPos = BZip2.G_SIZE - 1;
              zt = s.selector[++groupNo] & 0xff;
              base_zt = s.gBase[zt];
              limit_zt = s.gLimit[zt];
              perm_zt = s.gPerm[zt];
              minLens_zt = s.gMinlen[zt];
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(4237);
              groupPos--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(4238);
            int zn = minLens_zt;
            IACSharpSensor.IACSharpSensor.SensorReached(4239);
            while (bsLiveShadow < zn) {
              IACSharpSensor.IACSharpSensor.SensorReached(4240);
              int thech = this.input.ReadByte();
              IACSharpSensor.IACSharpSensor.SensorReached(4241);
              if (thech >= 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(4242);
                bsBuffShadow = (bsBuffShadow << 8) | thech;
                bsLiveShadow += 8;
                IACSharpSensor.IACSharpSensor.SensorReached(4243);
                continue;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(4244);
                throw new IOException("unexpected end of stream");
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(4245);
            int zvec = (bsBuffShadow >> (bsLiveShadow - zn)) & ((1 << zn) - 1);
            bsLiveShadow -= zn;
            IACSharpSensor.IACSharpSensor.SensorReached(4246);
            while (zvec > limit_zt[zn]) {
              IACSharpSensor.IACSharpSensor.SensorReached(4247);
              zn++;
              IACSharpSensor.IACSharpSensor.SensorReached(4248);
              while (bsLiveShadow < 1) {
                IACSharpSensor.IACSharpSensor.SensorReached(4249);
                int thech = this.input.ReadByte();
                IACSharpSensor.IACSharpSensor.SensorReached(4250);
                if (thech >= 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4251);
                  bsBuffShadow = (bsBuffShadow << 8) | thech;
                  bsLiveShadow += 8;
                  IACSharpSensor.IACSharpSensor.SensorReached(4252);
                  continue;
                } else {
                  IACSharpSensor.IACSharpSensor.SensorReached(4253);
                  throw new IOException("unexpected end of stream");
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(4254);
              bsLiveShadow--;
              zvec = (zvec << 1) | ((bsBuffShadow >> bsLiveShadow) & 1);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(4255);
            nextSym = perm_zt[zvec - base_zt[zn]];
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4256);
          byte ch = s.seqToUnseq[yy[0]];
          s.unzftab[ch & 0xff] += es + 1;
          IACSharpSensor.IACSharpSensor.SensorReached(4257);
          while (es-- >= 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(4258);
            s.ll8[++lastShadow] = ch;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4259);
          if (lastShadow >= limitLast) {
            IACSharpSensor.IACSharpSensor.SensorReached(4260);
            throw new IOException("block overrun");
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4261);
          if (++lastShadow >= limitLast) {
            IACSharpSensor.IACSharpSensor.SensorReached(4262);
            throw new IOException("block overrun");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4263);
          byte tmp = yy[nextSym - 1];
          s.unzftab[s.seqToUnseq[tmp] & 0xff]++;
          s.ll8[lastShadow] = s.seqToUnseq[tmp];
          IACSharpSensor.IACSharpSensor.SensorReached(4264);
          if (nextSym <= 16) {
            IACSharpSensor.IACSharpSensor.SensorReached(4265);
            for (int j = nextSym - 1; j > 0;) {
              IACSharpSensor.IACSharpSensor.SensorReached(4266);
              yy[j] = yy[--j];
            }
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(4267);
            System.Buffer.BlockCopy(yy, 0, yy, 1, nextSym - 1);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4268);
          yy[0] = tmp;
          IACSharpSensor.IACSharpSensor.SensorReached(4269);
          if (groupPos == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(4270);
            groupPos = BZip2.G_SIZE - 1;
            zt = s.selector[++groupNo] & 0xff;
            base_zt = s.gBase[zt];
            limit_zt = s.gLimit[zt];
            perm_zt = s.gPerm[zt];
            minLens_zt = s.gMinlen[zt];
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(4271);
            groupPos--;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4272);
          int zn = minLens_zt;
          IACSharpSensor.IACSharpSensor.SensorReached(4273);
          while (bsLiveShadow < zn) {
            IACSharpSensor.IACSharpSensor.SensorReached(4274);
            int thech = this.input.ReadByte();
            IACSharpSensor.IACSharpSensor.SensorReached(4275);
            if (thech >= 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(4276);
              bsBuffShadow = (bsBuffShadow << 8) | thech;
              bsLiveShadow += 8;
              IACSharpSensor.IACSharpSensor.SensorReached(4277);
              continue;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(4278);
              throw new IOException("unexpected end of stream");
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4279);
          int zvec = (bsBuffShadow >> (bsLiveShadow - zn)) & ((1 << zn) - 1);
          bsLiveShadow -= zn;
          IACSharpSensor.IACSharpSensor.SensorReached(4280);
          while (zvec > limit_zt[zn]) {
            IACSharpSensor.IACSharpSensor.SensorReached(4281);
            zn++;
            IACSharpSensor.IACSharpSensor.SensorReached(4282);
            while (bsLiveShadow < 1) {
              IACSharpSensor.IACSharpSensor.SensorReached(4283);
              int thech = this.input.ReadByte();
              IACSharpSensor.IACSharpSensor.SensorReached(4284);
              if (thech >= 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(4285);
                bsBuffShadow = (bsBuffShadow << 8) | thech;
                bsLiveShadow += 8;
                IACSharpSensor.IACSharpSensor.SensorReached(4286);
                continue;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(4287);
                throw new IOException("unexpected end of stream");
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(4288);
            bsLiveShadow--;
            zvec = (zvec << 1) | ((bsBuffShadow >> bsLiveShadow) & 1);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4289);
          nextSym = perm_zt[zvec - base_zt[zn]];
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4290);
      this.last = lastShadow;
      this.bsLive = bsLiveShadow;
      this.bsBuff = bsBuffShadow;
      IACSharpSensor.IACSharpSensor.SensorReached(4291);
    }
    private int getAndMoveToFrontDecode0(int groupNo)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4292);
      var s = this.data;
      int zt = s.selector[groupNo] & 0xff;
      int[] limit_zt = s.gLimit[zt];
      int zn = s.gMinlen[zt];
      int zvec = GetBits(zn);
      int bsLiveShadow = this.bsLive;
      int bsBuffShadow = this.bsBuff;
      IACSharpSensor.IACSharpSensor.SensorReached(4293);
      while (zvec > limit_zt[zn]) {
        IACSharpSensor.IACSharpSensor.SensorReached(4294);
        zn++;
        IACSharpSensor.IACSharpSensor.SensorReached(4295);
        while (bsLiveShadow < 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(4296);
          int thech = this.input.ReadByte();
          IACSharpSensor.IACSharpSensor.SensorReached(4297);
          if (thech >= 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(4298);
            bsBuffShadow = (bsBuffShadow << 8) | thech;
            bsLiveShadow += 8;
            IACSharpSensor.IACSharpSensor.SensorReached(4299);
            continue;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(4300);
            throw new IOException("unexpected end of stream");
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4301);
        bsLiveShadow--;
        zvec = (zvec << 1) | ((bsBuffShadow >> bsLiveShadow) & 1);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4302);
      this.bsLive = bsLiveShadow;
      this.bsBuff = bsBuffShadow;
      System.Int32 RNTRNTRNT_497 = s.gPerm[zt][zvec - s.gBase[zt][zn]];
      IACSharpSensor.IACSharpSensor.SensorReached(4303);
      return RNTRNTRNT_497;
    }
    private void SetupBlock()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4304);
      if (this.data == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4305);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4306);
      int i;
      var s = this.data;
      int[] tt = s.initTT(this.last + 1);
      IACSharpSensor.IACSharpSensor.SensorReached(4307);
      for (i = 0; i <= 255; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4308);
        if (s.unzftab[i] < 0 || s.unzftab[i] > this.last) {
          IACSharpSensor.IACSharpSensor.SensorReached(4309);
          throw new Exception("BZ_DATA_ERROR");
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4310);
      s.cftab[0] = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4311);
      for (i = 1; i <= 256; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4312);
        s.cftab[i] = s.unzftab[i - 1];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4313);
      for (i = 1; i <= 256; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4314);
        s.cftab[i] += s.cftab[i - 1];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4315);
      for (i = 0; i <= 256; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4316);
        if (s.cftab[i] < 0 || s.cftab[i] > this.last + 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(4317);
          var msg = String.Format("BZ_DATA_ERROR: cftab[{0}]={1} last={2}", i, s.cftab[i], this.last);
          IACSharpSensor.IACSharpSensor.SensorReached(4318);
          throw new Exception(msg);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4319);
      for (i = 1; i <= 256; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4320);
        if (s.cftab[i - 1] > s.cftab[i]) {
          IACSharpSensor.IACSharpSensor.SensorReached(4321);
          throw new Exception("BZ_DATA_ERROR");
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4322);
      int lastShadow;
      IACSharpSensor.IACSharpSensor.SensorReached(4323);
      for (i = 0,lastShadow = this.last; i <= lastShadow; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4324);
        tt[s.cftab[s.ll8[i] & 0xff]++] = i;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4325);
      if ((this.origPtr < 0) || (this.origPtr >= tt.Length)) {
        IACSharpSensor.IACSharpSensor.SensorReached(4326);
        throw new IOException("stream corrupted");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4327);
      this.su_tPos = tt[this.origPtr];
      this.su_count = 0;
      this.su_i2 = 0;
      this.su_ch2 = 256;
      IACSharpSensor.IACSharpSensor.SensorReached(4328);
      if (this.blockRandomised) {
        IACSharpSensor.IACSharpSensor.SensorReached(4329);
        this.su_rNToGo = 0;
        this.su_rTPos = 0;
        SetupRandPartA();
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4330);
        SetupNoRandPartA();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4331);
    }
    private void SetupRandPartA()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4332);
      if (this.su_i2 <= this.last) {
        IACSharpSensor.IACSharpSensor.SensorReached(4333);
        this.su_chPrev = this.su_ch2;
        int su_ch2Shadow = this.data.ll8[this.su_tPos] & 0xff;
        this.su_tPos = this.data.tt[this.su_tPos];
        IACSharpSensor.IACSharpSensor.SensorReached(4334);
        if (this.su_rNToGo == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4335);
          this.su_rNToGo = Rand.Rnums(this.su_rTPos) - 1;
          IACSharpSensor.IACSharpSensor.SensorReached(4336);
          if (++this.su_rTPos == 512) {
            IACSharpSensor.IACSharpSensor.SensorReached(4337);
            this.su_rTPos = 0;
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4338);
          this.su_rNToGo--;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4339);
        this.su_ch2 = su_ch2Shadow ^= (this.su_rNToGo == 1) ? 1 : 0;
        this.su_i2++;
        this.currentChar = su_ch2Shadow;
        this.currentState = CState.RAND_PART_B;
        this.crc.UpdateCRC((byte)su_ch2Shadow);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4340);
        EndBlock();
        InitBlock();
        SetupBlock();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4341);
    }
    private void SetupNoRandPartA()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4342);
      if (this.su_i2 <= this.last) {
        IACSharpSensor.IACSharpSensor.SensorReached(4343);
        this.su_chPrev = this.su_ch2;
        int su_ch2Shadow = this.data.ll8[this.su_tPos] & 0xff;
        this.su_ch2 = su_ch2Shadow;
        this.su_tPos = this.data.tt[this.su_tPos];
        this.su_i2++;
        this.currentChar = su_ch2Shadow;
        this.currentState = CState.NO_RAND_PART_B;
        this.crc.UpdateCRC((byte)su_ch2Shadow);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4344);
        this.currentState = CState.NO_RAND_PART_A;
        EndBlock();
        InitBlock();
        SetupBlock();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4345);
    }
    private void SetupRandPartB()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4346);
      if (this.su_ch2 != this.su_chPrev) {
        IACSharpSensor.IACSharpSensor.SensorReached(4347);
        this.currentState = CState.RAND_PART_A;
        this.su_count = 1;
        SetupRandPartA();
      } else if (++this.su_count >= 4) {
        IACSharpSensor.IACSharpSensor.SensorReached(4349);
        this.su_z = (char)(this.data.ll8[this.su_tPos] & 0xff);
        this.su_tPos = this.data.tt[this.su_tPos];
        IACSharpSensor.IACSharpSensor.SensorReached(4350);
        if (this.su_rNToGo == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4351);
          this.su_rNToGo = Rand.Rnums(this.su_rTPos) - 1;
          IACSharpSensor.IACSharpSensor.SensorReached(4352);
          if (++this.su_rTPos == 512) {
            IACSharpSensor.IACSharpSensor.SensorReached(4353);
            this.su_rTPos = 0;
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4354);
          this.su_rNToGo--;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4355);
        this.su_j2 = 0;
        this.currentState = CState.RAND_PART_C;
        IACSharpSensor.IACSharpSensor.SensorReached(4356);
        if (this.su_rNToGo == 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(4357);
          this.su_z ^= (char)1;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4358);
        SetupRandPartC();
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4348);
        this.currentState = CState.RAND_PART_A;
        SetupRandPartA();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4359);
    }
    private void SetupRandPartC()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4360);
      if (this.su_j2 < this.su_z) {
        IACSharpSensor.IACSharpSensor.SensorReached(4361);
        this.currentChar = this.su_ch2;
        this.crc.UpdateCRC((byte)this.su_ch2);
        this.su_j2++;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4362);
        this.currentState = CState.RAND_PART_A;
        this.su_i2++;
        this.su_count = 0;
        SetupRandPartA();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4363);
    }
    private void SetupNoRandPartB()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4364);
      if (this.su_ch2 != this.su_chPrev) {
        IACSharpSensor.IACSharpSensor.SensorReached(4365);
        this.su_count = 1;
        SetupNoRandPartA();
      } else if (++this.su_count >= 4) {
        IACSharpSensor.IACSharpSensor.SensorReached(4367);
        this.su_z = (char)(this.data.ll8[this.su_tPos] & 0xff);
        this.su_tPos = this.data.tt[this.su_tPos];
        this.su_j2 = 0;
        SetupNoRandPartC();
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4366);
        SetupNoRandPartA();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4368);
    }
    private void SetupNoRandPartC()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4369);
      if (this.su_j2 < this.su_z) {
        IACSharpSensor.IACSharpSensor.SensorReached(4370);
        int su_ch2Shadow = this.su_ch2;
        this.currentChar = su_ch2Shadow;
        this.crc.UpdateCRC((byte)su_ch2Shadow);
        this.su_j2++;
        this.currentState = CState.NO_RAND_PART_C;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4371);
        this.su_i2++;
        this.su_count = 0;
        SetupNoRandPartA();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4372);
    }
    private sealed class DecompressionState
    {
      public readonly bool[] inUse = new bool[256];
      public readonly byte[] seqToUnseq = new byte[256];
      public readonly byte[] selector = new byte[BZip2.MaxSelectors];
      public readonly byte[] selectorMtf = new byte[BZip2.MaxSelectors];
      public readonly int[] unzftab;
      public readonly int[][] gLimit;
      public readonly int[][] gBase;
      public readonly int[][] gPerm;
      public readonly int[] gMinlen;
      public readonly int[] cftab;
      public readonly byte[] getAndMoveToFrontDecode_yy;
      public readonly char[][] temp_charArray2d;
      public readonly byte[] recvDecodingTables_pos;
      public int[] tt;
      public byte[] ll8;
      public DecompressionState(int blockSize100k)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(4373);
        this.unzftab = new int[256];
        this.gLimit = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.gBase = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.gPerm = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.gMinlen = new int[BZip2.NGroups];
        this.cftab = new int[257];
        this.getAndMoveToFrontDecode_yy = new byte[256];
        this.temp_charArray2d = BZip2.InitRectangularArray<char>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.recvDecodingTables_pos = new byte[BZip2.NGroups];
        this.ll8 = new byte[blockSize100k * BZip2.BlockSizeMultiple];
        IACSharpSensor.IACSharpSensor.SensorReached(4374);
      }
      public int[] initTT(int length)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(4375);
        int[] ttShadow = this.tt;
        IACSharpSensor.IACSharpSensor.SensorReached(4376);
        if ((ttShadow == null) || (ttShadow.Length < length)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4377);
          this.tt = ttShadow = new int[length];
        }
        System.Int32[] RNTRNTRNT_498 = ttShadow;
        IACSharpSensor.IACSharpSensor.SensorReached(4378);
        return RNTRNTRNT_498;
      }
    }
  }
  static internal class BZip2
  {
    static internal T[][] InitRectangularArray<T>(int d1, int d2)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4379);
      var x = new T[d1][];
      IACSharpSensor.IACSharpSensor.SensorReached(4380);
      for (int i = 0; i < d1; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4381);
        x[i] = new T[d2];
      }
      T[][] RNTRNTRNT_499 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(4382);
      return RNTRNTRNT_499;
    }
    public static readonly int BlockSizeMultiple = 100000;
    public static readonly int MinBlockSize = 1;
    public static readonly int MaxBlockSize = 9;
    public static readonly int MaxAlphaSize = 258;
    public static readonly int MaxCodeLength = 23;
    public static readonly char RUNA = (char)0;
    public static readonly char RUNB = (char)1;
    public static readonly int NGroups = 6;
    public static readonly int G_SIZE = 50;
    public static readonly int N_ITERS = 4;
    public static readonly int MaxSelectors = (2 + (900000 / G_SIZE));
    public static readonly int NUM_OVERSHOOT_BYTES = 20;
    static internal readonly int QSORT_STACK_SIZE = 1000;
  }
}
