using System;
using System.IO;
namespace Ionic.BZip2
{
  internal class BZip2Compressor
  {
    private int blockSize100k;
    private int currentByte = -1;
    private int runLength = 0;
    private int last;
    private int outBlockFillThreshold;
    private CompressionState cstate;
    private readonly Ionic.Crc.CRC32 crc = new Ionic.Crc.CRC32(true);
    BitWriter bw;
    int runs;
    private int workDone;
    private int workLimit;
    private bool firstAttempt;
    private bool blockRandomised;
    private int origPtr;
    private int nInUse;
    private int nMTF;
    private static readonly int SETMASK = (1 << 21);
    private static readonly int CLEARMASK = (~SETMASK);
    private static readonly byte GREATER_ICOST = 15;
    private static readonly byte LESSER_ICOST = 0;
    private static readonly int SMALL_THRESH = 20;
    private static readonly int DEPTH_THRESH = 10;
    private static readonly int WORK_FACTOR = 30;
    private static readonly int[] increments = {
      1,
      4,
      13,
      40,
      121,
      364,
      1093,
      3280,
      9841,
      29524,
      88573,
      265720,
      797161,
      2391484
    };
    public BZip2Compressor(BitWriter writer) : this(writer, BZip2.MaxBlockSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3587);
    }
    public BZip2Compressor(BitWriter writer, int blockSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3588);
      this.blockSize100k = blockSize;
      this.bw = writer;
      this.outBlockFillThreshold = (blockSize * BZip2.BlockSizeMultiple) - 20;
      this.cstate = new CompressionState(blockSize);
      Reset();
      IACSharpSensor.IACSharpSensor.SensorReached(3589);
    }
    void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3590);
      this.crc.Reset();
      this.currentByte = -1;
      this.runLength = 0;
      this.last = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(3591);
      for (int i = 256; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(3592);
        this.cstate.inUse[i] = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3593);
    }
    public int BlockSize {
      get {
        System.Int32 RNTRNTRNT_472 = this.blockSize100k;
        IACSharpSensor.IACSharpSensor.SensorReached(3594);
        return RNTRNTRNT_472;
      }
    }
    public uint Crc32 { get; private set; }
    public int AvailableBytesOut { get; private set; }
    public int UncompressedBytes {
      get {
        System.Int32 RNTRNTRNT_473 = this.last + 1;
        IACSharpSensor.IACSharpSensor.SensorReached(3595);
        return RNTRNTRNT_473;
      }
    }
    public int Fill(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3596);
      if (this.last >= this.outBlockFillThreshold) {
        System.Int32 RNTRNTRNT_474 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(3597);
        return RNTRNTRNT_474;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3598);
      int bytesWritten = 0;
      int limit = offset + count;
      int rc;
      IACSharpSensor.IACSharpSensor.SensorReached(3599);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(3600);
        rc = write0(buffer[offset++]);
        IACSharpSensor.IACSharpSensor.SensorReached(3601);
        if (rc > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3602);
          bytesWritten++;
        }
      } while (offset < limit && rc == 1);
      System.Int32 RNTRNTRNT_475 = bytesWritten;
      IACSharpSensor.IACSharpSensor.SensorReached(3603);
      return RNTRNTRNT_475;
    }
    private int write0(byte b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3604);
      bool rc;
      IACSharpSensor.IACSharpSensor.SensorReached(3605);
      if (this.currentByte == -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(3606);
        this.currentByte = b;
        this.runLength++;
        System.Int32 RNTRNTRNT_476 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(3607);
        return RNTRNTRNT_476;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3608);
      if (this.currentByte == b) {
        IACSharpSensor.IACSharpSensor.SensorReached(3609);
        if (++this.runLength > 254) {
          IACSharpSensor.IACSharpSensor.SensorReached(3610);
          rc = AddRunToOutputBlock(false);
          this.currentByte = -1;
          this.runLength = 0;
          System.Int32 RNTRNTRNT_477 = (rc) ? 2 : 1;
          IACSharpSensor.IACSharpSensor.SensorReached(3611);
          return RNTRNTRNT_477;
        }
        System.Int32 RNTRNTRNT_478 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(3612);
        return RNTRNTRNT_478;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3613);
      rc = AddRunToOutputBlock(false);
      IACSharpSensor.IACSharpSensor.SensorReached(3614);
      if (rc) {
        IACSharpSensor.IACSharpSensor.SensorReached(3615);
        this.currentByte = -1;
        this.runLength = 0;
        System.Int32 RNTRNTRNT_479 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(3616);
        return RNTRNTRNT_479;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3617);
      this.runLength = 1;
      this.currentByte = b;
      System.Int32 RNTRNTRNT_480 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(3618);
      return RNTRNTRNT_480;
    }
    private bool AddRunToOutputBlock(bool final)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3619);
      runs++;
      int previousLast = this.last;
      IACSharpSensor.IACSharpSensor.SensorReached(3620);
      if (previousLast >= this.outBlockFillThreshold && !final) {
        IACSharpSensor.IACSharpSensor.SensorReached(3621);
        var msg = String.Format("block overrun(final={2}): {0} >= threshold ({1})", previousLast, this.outBlockFillThreshold, final);
        IACSharpSensor.IACSharpSensor.SensorReached(3622);
        throw new Exception(msg);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3623);
      byte b = (byte)this.currentByte;
      byte[] block = this.cstate.block;
      this.cstate.inUse[b] = true;
      int rl = this.runLength;
      this.crc.UpdateCRC(b, rl);
      IACSharpSensor.IACSharpSensor.SensorReached(3624);
      switch (rl) {
        case 1:
          IACSharpSensor.IACSharpSensor.SensorReached(3625);
          block[previousLast + 2] = b;
          this.last = previousLast + 1;
          IACSharpSensor.IACSharpSensor.SensorReached(3626);
          break;
        case 2:
          IACSharpSensor.IACSharpSensor.SensorReached(3627);
          block[previousLast + 2] = b;
          block[previousLast + 3] = b;
          this.last = previousLast + 2;
          IACSharpSensor.IACSharpSensor.SensorReached(3628);
          break;
        case 3:
          IACSharpSensor.IACSharpSensor.SensorReached(3629);
          block[previousLast + 2] = b;
          block[previousLast + 3] = b;
          block[previousLast + 4] = b;
          this.last = previousLast + 3;
          IACSharpSensor.IACSharpSensor.SensorReached(3630);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(3631);
          rl -= 4;
          this.cstate.inUse[rl] = true;
          block[previousLast + 2] = b;
          block[previousLast + 3] = b;
          block[previousLast + 4] = b;
          block[previousLast + 5] = b;
          block[previousLast + 6] = (byte)rl;
          this.last = previousLast + 5;
          IACSharpSensor.IACSharpSensor.SensorReached(3632);
          break;
      }
      System.Boolean RNTRNTRNT_481 = (this.last >= this.outBlockFillThreshold);
      IACSharpSensor.IACSharpSensor.SensorReached(3633);
      return RNTRNTRNT_481;
    }
    public void CompressAndWrite()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3634);
      if (this.runLength > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3635);
        AddRunToOutputBlock(true);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3636);
      this.currentByte = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(3637);
      if (this.last == -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(3638);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3639);
      blockSort();
      this.bw.WriteByte(0x31);
      this.bw.WriteByte(0x41);
      this.bw.WriteByte(0x59);
      this.bw.WriteByte(0x26);
      this.bw.WriteByte(0x53);
      this.bw.WriteByte(0x59);
      this.Crc32 = (uint)this.crc.Crc32Result;
      this.bw.WriteInt(this.Crc32);
      this.bw.WriteBits(1, (this.blockRandomised) ? 1u : 0u);
      moveToFrontCodeAndSend();
      Reset();
      IACSharpSensor.IACSharpSensor.SensorReached(3640);
    }
    private void randomiseBlock()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3641);
      bool[] inUse = this.cstate.inUse;
      byte[] block = this.cstate.block;
      int lastShadow = this.last;
      IACSharpSensor.IACSharpSensor.SensorReached(3642);
      for (int i = 256; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(3643);
        inUse[i] = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3644);
      int rNToGo = 0;
      int rTPos = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3645);
      for (int i = 0, j = 1; i <= lastShadow; i = j,j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3646);
        if (rNToGo == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3647);
          rNToGo = (char)Rand.Rnums(rTPos);
          IACSharpSensor.IACSharpSensor.SensorReached(3648);
          if (++rTPos == 512) {
            IACSharpSensor.IACSharpSensor.SensorReached(3649);
            rTPos = 0;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3650);
        rNToGo--;
        block[j] ^= (byte)((rNToGo == 1) ? 1 : 0);
        inUse[block[j] & 0xff] = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3651);
      this.blockRandomised = true;
      IACSharpSensor.IACSharpSensor.SensorReached(3652);
    }
    private void mainSort()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3653);
      CompressionState dataShadow = this.cstate;
      int[] runningOrder = dataShadow.mainSort_runningOrder;
      int[] copy = dataShadow.mainSort_copy;
      bool[] bigDone = dataShadow.mainSort_bigDone;
      int[] ftab = dataShadow.ftab;
      byte[] block = dataShadow.block;
      int[] fmap = dataShadow.fmap;
      char[] quadrant = dataShadow.quadrant;
      int lastShadow = this.last;
      int workLimitShadow = this.workLimit;
      bool firstAttemptShadow = this.firstAttempt;
      IACSharpSensor.IACSharpSensor.SensorReached(3654);
      for (int i = 65537; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(3655);
        ftab[i] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3656);
      for (int i = 0; i < BZip2.NUM_OVERSHOOT_BYTES; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3657);
        block[lastShadow + i + 2] = block[(i % (lastShadow + 1)) + 1];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3658);
      for (int i = lastShadow + BZip2.NUM_OVERSHOOT_BYTES + 1; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(3659);
        quadrant[i] = '\0';
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3660);
      block[0] = block[lastShadow + 1];
      int c1 = block[0] & 0xff;
      IACSharpSensor.IACSharpSensor.SensorReached(3661);
      for (int i = 0; i <= lastShadow; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3662);
        int c2 = block[i + 1] & 0xff;
        ftab[(c1 << 8) + c2]++;
        c1 = c2;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3663);
      for (int i = 1; i <= 65536; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3664);
        ftab[i] += ftab[i - 1];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3665);
      c1 = block[1] & 0xff;
      IACSharpSensor.IACSharpSensor.SensorReached(3666);
      for (int i = 0; i < lastShadow; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3667);
        int c2 = block[i + 2] & 0xff;
        fmap[--ftab[(c1 << 8) + c2]] = i;
        c1 = c2;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3668);
      fmap[--ftab[((block[lastShadow + 1] & 0xff) << 8) + (block[1] & 0xff)]] = lastShadow;
      IACSharpSensor.IACSharpSensor.SensorReached(3669);
      for (int i = 256; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(3670);
        bigDone[i] = false;
        runningOrder[i] = i;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3671);
      for (int h = 364; h != 1;) {
        IACSharpSensor.IACSharpSensor.SensorReached(3672);
        h /= 3;
        IACSharpSensor.IACSharpSensor.SensorReached(3673);
        for (int i = h; i <= 255; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(3674);
          int vv = runningOrder[i];
          int a = ftab[(vv + 1) << 8] - ftab[vv << 8];
          int b = h - 1;
          int j = i;
          IACSharpSensor.IACSharpSensor.SensorReached(3675);
          for (int ro = runningOrder[j - h]; (ftab[(ro + 1) << 8] - ftab[ro << 8]) > a; ro = runningOrder[j - h]) {
            IACSharpSensor.IACSharpSensor.SensorReached(3676);
            runningOrder[j] = ro;
            j -= h;
            IACSharpSensor.IACSharpSensor.SensorReached(3677);
            if (j <= b) {
              IACSharpSensor.IACSharpSensor.SensorReached(3678);
              break;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3679);
          runningOrder[j] = vv;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3680);
      for (int i = 0; i <= 255; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3681);
        int ss = runningOrder[i];
        IACSharpSensor.IACSharpSensor.SensorReached(3682);
        for (int j = 0; j <= 255; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(3683);
          int sb = (ss << 8) + j;
          int ftab_sb = ftab[sb];
          IACSharpSensor.IACSharpSensor.SensorReached(3684);
          if ((ftab_sb & SETMASK) != SETMASK) {
            IACSharpSensor.IACSharpSensor.SensorReached(3685);
            int lo = ftab_sb & CLEARMASK;
            int hi = (ftab[sb + 1] & CLEARMASK) - 1;
            IACSharpSensor.IACSharpSensor.SensorReached(3686);
            if (hi > lo) {
              IACSharpSensor.IACSharpSensor.SensorReached(3687);
              mainQSort3(dataShadow, lo, hi, 2);
              IACSharpSensor.IACSharpSensor.SensorReached(3688);
              if (firstAttemptShadow && (this.workDone > workLimitShadow)) {
                IACSharpSensor.IACSharpSensor.SensorReached(3689);
                return;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3690);
            ftab[sb] = ftab_sb | SETMASK;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3691);
        for (int j = 0; j <= 255; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(3692);
          copy[j] = ftab[(j << 8) + ss] & CLEARMASK;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3693);
        for (int j = ftab[ss << 8] & CLEARMASK, hj = (ftab[(ss + 1) << 8] & CLEARMASK); j < hj; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(3694);
          int fmap_j = fmap[j];
          c1 = block[fmap_j] & 0xff;
          IACSharpSensor.IACSharpSensor.SensorReached(3695);
          if (!bigDone[c1]) {
            IACSharpSensor.IACSharpSensor.SensorReached(3696);
            fmap[copy[c1]] = (fmap_j == 0) ? lastShadow : (fmap_j - 1);
            copy[c1]++;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3697);
        for (int j = 256; --j >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(3698);
          ftab[(j << 8) + ss] |= SETMASK;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3699);
        bigDone[ss] = true;
        IACSharpSensor.IACSharpSensor.SensorReached(3700);
        if (i < 255) {
          IACSharpSensor.IACSharpSensor.SensorReached(3701);
          int bbStart = ftab[ss << 8] & CLEARMASK;
          int bbSize = (ftab[(ss + 1) << 8] & CLEARMASK) - bbStart;
          int shifts = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(3702);
          while ((bbSize >> shifts) > 65534) {
            IACSharpSensor.IACSharpSensor.SensorReached(3703);
            shifts++;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3704);
          for (int j = 0; j < bbSize; j++) {
            IACSharpSensor.IACSharpSensor.SensorReached(3705);
            int a2update = fmap[bbStart + j];
            char qVal = (char)(j >> shifts);
            quadrant[a2update] = qVal;
            IACSharpSensor.IACSharpSensor.SensorReached(3706);
            if (a2update < BZip2.NUM_OVERSHOOT_BYTES) {
              IACSharpSensor.IACSharpSensor.SensorReached(3707);
              quadrant[a2update + lastShadow + 1] = qVal;
            }
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3708);
    }
    private void blockSort()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3709);
      this.workLimit = WORK_FACTOR * this.last;
      this.workDone = 0;
      this.blockRandomised = false;
      this.firstAttempt = true;
      mainSort();
      IACSharpSensor.IACSharpSensor.SensorReached(3710);
      if (this.firstAttempt && (this.workDone > this.workLimit)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3711);
        randomiseBlock();
        this.workLimit = this.workDone = 0;
        this.firstAttempt = false;
        mainSort();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3712);
      int[] fmap = this.cstate.fmap;
      this.origPtr = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(3713);
      for (int i = 0, lastShadow = this.last; i <= lastShadow; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3714);
        if (fmap[i] == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3715);
          this.origPtr = i;
          IACSharpSensor.IACSharpSensor.SensorReached(3716);
          break;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3717);
    }
    private bool mainSimpleSort(CompressionState dataShadow, int lo, int hi, int d)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3718);
      int bigN = hi - lo + 1;
      IACSharpSensor.IACSharpSensor.SensorReached(3719);
      if (bigN < 2) {
        System.Boolean RNTRNTRNT_482 = this.firstAttempt && (this.workDone > this.workLimit);
        IACSharpSensor.IACSharpSensor.SensorReached(3720);
        return RNTRNTRNT_482;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3721);
      int hp = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3722);
      while (increments[hp] < bigN) {
        IACSharpSensor.IACSharpSensor.SensorReached(3723);
        hp++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3724);
      int[] fmap = dataShadow.fmap;
      char[] quadrant = dataShadow.quadrant;
      byte[] block = dataShadow.block;
      int lastShadow = this.last;
      int lastPlus1 = lastShadow + 1;
      bool firstAttemptShadow = this.firstAttempt;
      int workLimitShadow = this.workLimit;
      int workDoneShadow = this.workDone;
      IACSharpSensor.IACSharpSensor.SensorReached(3725);
      while (--hp >= 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3726);
        int h = increments[hp];
        int mj = lo + h - 1;
        IACSharpSensor.IACSharpSensor.SensorReached(3727);
        for (int i = lo + h; i <= hi;) {
          IACSharpSensor.IACSharpSensor.SensorReached(3728);
          for (int k = 3; (i <= hi) && (--k >= 0); i++) {
            IACSharpSensor.IACSharpSensor.SensorReached(3729);
            int v = fmap[i];
            int vd = v + d;
            int j = i;
            bool onceRunned = false;
            int a = 0;
            HAMMER:
            IACSharpSensor.IACSharpSensor.SensorReached(3730);
            while (true) {
              IACSharpSensor.IACSharpSensor.SensorReached(3731);
              if (onceRunned) {
                IACSharpSensor.IACSharpSensor.SensorReached(3732);
                fmap[j] = a;
                IACSharpSensor.IACSharpSensor.SensorReached(3733);
                if ((j -= h) <= mj) {
                  IACSharpSensor.IACSharpSensor.SensorReached(3734);
                  goto END_HAMMER;
                }
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(3735);
                onceRunned = true;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(3736);
              a = fmap[j - h];
              int i1 = a + d;
              int i2 = vd;
              IACSharpSensor.IACSharpSensor.SensorReached(3737);
              if (block[i1 + 1] == block[i2 + 1]) {
                IACSharpSensor.IACSharpSensor.SensorReached(3738);
                if (block[i1 + 2] == block[i2 + 2]) {
                  IACSharpSensor.IACSharpSensor.SensorReached(3739);
                  if (block[i1 + 3] == block[i2 + 3]) {
                    IACSharpSensor.IACSharpSensor.SensorReached(3740);
                    if (block[i1 + 4] == block[i2 + 4]) {
                      IACSharpSensor.IACSharpSensor.SensorReached(3741);
                      if (block[i1 + 5] == block[i2 + 5]) {
                        IACSharpSensor.IACSharpSensor.SensorReached(3742);
                        if (block[(i1 += 6)] == block[(i2 += 6)]) {
                          IACSharpSensor.IACSharpSensor.SensorReached(3743);
                          int x = lastShadow;
                          X:
                          IACSharpSensor.IACSharpSensor.SensorReached(3744);
                          while (x > 0) {
                            IACSharpSensor.IACSharpSensor.SensorReached(3745);
                            x -= 4;
                            IACSharpSensor.IACSharpSensor.SensorReached(3746);
                            if (block[i1 + 1] == block[i2 + 1]) {
                              IACSharpSensor.IACSharpSensor.SensorReached(3747);
                              if (quadrant[i1] == quadrant[i2]) {
                                IACSharpSensor.IACSharpSensor.SensorReached(3748);
                                if (block[i1 + 2] == block[i2 + 2]) {
                                  IACSharpSensor.IACSharpSensor.SensorReached(3749);
                                  if (quadrant[i1 + 1] == quadrant[i2 + 1]) {
                                    IACSharpSensor.IACSharpSensor.SensorReached(3750);
                                    if (block[i1 + 3] == block[i2 + 3]) {
                                      IACSharpSensor.IACSharpSensor.SensorReached(3751);
                                      if (quadrant[i1 + 2] == quadrant[i2 + 2]) {
                                        IACSharpSensor.IACSharpSensor.SensorReached(3752);
                                        if (block[i1 + 4] == block[i2 + 4]) {
                                          IACSharpSensor.IACSharpSensor.SensorReached(3753);
                                          if (quadrant[i1 + 3] == quadrant[i2 + 3]) {
                                            IACSharpSensor.IACSharpSensor.SensorReached(3754);
                                            if ((i1 += 4) >= lastPlus1) {
                                              IACSharpSensor.IACSharpSensor.SensorReached(3755);
                                              i1 -= lastPlus1;
                                            }
                                            IACSharpSensor.IACSharpSensor.SensorReached(3756);
                                            if ((i2 += 4) >= lastPlus1) {
                                              IACSharpSensor.IACSharpSensor.SensorReached(3757);
                                              i2 -= lastPlus1;
                                            }
                                            IACSharpSensor.IACSharpSensor.SensorReached(3758);
                                            workDoneShadow++;
                                            IACSharpSensor.IACSharpSensor.SensorReached(3759);
                                            goto X;
                                          } else if ((quadrant[i1 + 3] > quadrant[i2 + 3])) {
                                            IACSharpSensor.IACSharpSensor.SensorReached(3761);
                                            goto HAMMER;
                                          } else {
                                            IACSharpSensor.IACSharpSensor.SensorReached(3760);
                                            goto END_HAMMER;
                                          }
                                        } else if ((block[i1 + 4] & 0xff) > (block[i2 + 4] & 0xff)) {
                                          IACSharpSensor.IACSharpSensor.SensorReached(3763);
                                          goto HAMMER;
                                        } else {
                                          IACSharpSensor.IACSharpSensor.SensorReached(3762);
                                          goto END_HAMMER;
                                        }
                                      } else if ((quadrant[i1 + 2] > quadrant[i2 + 2])) {
                                        IACSharpSensor.IACSharpSensor.SensorReached(3765);
                                        goto HAMMER;
                                      } else {
                                        IACSharpSensor.IACSharpSensor.SensorReached(3764);
                                        goto END_HAMMER;
                                      }
                                    } else if ((block[i1 + 3] & 0xff) > (block[i2 + 3] & 0xff)) {
                                      IACSharpSensor.IACSharpSensor.SensorReached(3767);
                                      goto HAMMER;
                                    } else {
                                      IACSharpSensor.IACSharpSensor.SensorReached(3766);
                                      goto END_HAMMER;
                                    }
                                  } else if ((quadrant[i1 + 1] > quadrant[i2 + 1])) {
                                    IACSharpSensor.IACSharpSensor.SensorReached(3769);
                                    goto HAMMER;
                                  } else {
                                    IACSharpSensor.IACSharpSensor.SensorReached(3768);
                                    goto END_HAMMER;
                                  }
                                } else if ((block[i1 + 2] & 0xff) > (block[i2 + 2] & 0xff)) {
                                  IACSharpSensor.IACSharpSensor.SensorReached(3771);
                                  goto HAMMER;
                                } else {
                                  IACSharpSensor.IACSharpSensor.SensorReached(3770);
                                  goto END_HAMMER;
                                }
                              } else if ((quadrant[i1] > quadrant[i2])) {
                                IACSharpSensor.IACSharpSensor.SensorReached(3773);
                                goto HAMMER;
                              } else {
                                IACSharpSensor.IACSharpSensor.SensorReached(3772);
                                goto END_HAMMER;
                              }
                            } else if ((block[i1 + 1] & 0xff) > (block[i2 + 1] & 0xff)) {
                              IACSharpSensor.IACSharpSensor.SensorReached(3775);
                              goto HAMMER;
                            } else {
                              IACSharpSensor.IACSharpSensor.SensorReached(3774);
                              goto END_HAMMER;
                            }
                          }
                          IACSharpSensor.IACSharpSensor.SensorReached(3776);
                          goto END_HAMMER;
                        } else {
                          IACSharpSensor.IACSharpSensor.SensorReached(3777);
                          if ((block[i1] & 0xff) > (block[i2] & 0xff)) {
                            IACSharpSensor.IACSharpSensor.SensorReached(3778);
                            goto HAMMER;
                          } else {
                            IACSharpSensor.IACSharpSensor.SensorReached(3779);
                            goto END_HAMMER;
                          }
                        }
                      } else if ((block[i1 + 5] & 0xff) > (block[i2 + 5] & 0xff)) {
                        IACSharpSensor.IACSharpSensor.SensorReached(3781);
                        goto HAMMER;
                      } else {
                        IACSharpSensor.IACSharpSensor.SensorReached(3780);
                        goto END_HAMMER;
                      }
                    } else if ((block[i1 + 4] & 0xff) > (block[i2 + 4] & 0xff)) {
                      IACSharpSensor.IACSharpSensor.SensorReached(3783);
                      goto HAMMER;
                    } else {
                      IACSharpSensor.IACSharpSensor.SensorReached(3782);
                      goto END_HAMMER;
                    }
                  } else if ((block[i1 + 3] & 0xff) > (block[i2 + 3] & 0xff)) {
                    IACSharpSensor.IACSharpSensor.SensorReached(3785);
                    goto HAMMER;
                  } else {
                    IACSharpSensor.IACSharpSensor.SensorReached(3784);
                    goto END_HAMMER;
                  }
                } else if ((block[i1 + 2] & 0xff) > (block[i2 + 2] & 0xff)) {
                  IACSharpSensor.IACSharpSensor.SensorReached(3787);
                  goto HAMMER;
                } else {
                  IACSharpSensor.IACSharpSensor.SensorReached(3786);
                  goto END_HAMMER;
                }
              } else if ((block[i1 + 1] & 0xff) > (block[i2 + 1] & 0xff)) {
                IACSharpSensor.IACSharpSensor.SensorReached(3789);
                goto HAMMER;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(3788);
                goto END_HAMMER;
              }
            }
            END_HAMMER:
            IACSharpSensor.IACSharpSensor.SensorReached(3790);
            fmap[j] = v;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3791);
          if (firstAttemptShadow && (i <= hi) && (workDoneShadow > workLimitShadow)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3792);
            goto END_HP;
          }
        }
      }
      END_HP:
      IACSharpSensor.IACSharpSensor.SensorReached(3793);
      this.workDone = workDoneShadow;
      System.Boolean RNTRNTRNT_483 = firstAttemptShadow && (workDoneShadow > workLimitShadow);
      IACSharpSensor.IACSharpSensor.SensorReached(3794);
      return RNTRNTRNT_483;
    }
    private static void vswap(int[] fmap, int p1, int p2, int n)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3795);
      n += p1;
      IACSharpSensor.IACSharpSensor.SensorReached(3796);
      while (p1 < n) {
        IACSharpSensor.IACSharpSensor.SensorReached(3797);
        int t = fmap[p1];
        fmap[p1++] = fmap[p2];
        fmap[p2++] = t;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3798);
    }
    private static byte med3(byte a, byte b, byte c)
    {
      System.Byte RNTRNTRNT_484 = (a < b) ? (b < c ? b : a < c ? c : a) : (b > c ? b : a > c ? c : a);
      IACSharpSensor.IACSharpSensor.SensorReached(3799);
      return RNTRNTRNT_484;
    }
    private void mainQSort3(CompressionState dataShadow, int loSt, int hiSt, int dSt)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3800);
      int[] stack_ll = dataShadow.stack_ll;
      int[] stack_hh = dataShadow.stack_hh;
      int[] stack_dd = dataShadow.stack_dd;
      int[] fmap = dataShadow.fmap;
      byte[] block = dataShadow.block;
      stack_ll[0] = loSt;
      stack_hh[0] = hiSt;
      stack_dd[0] = dSt;
      IACSharpSensor.IACSharpSensor.SensorReached(3801);
      for (int sp = 1; --sp >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(3802);
        int lo = stack_ll[sp];
        int hi = stack_hh[sp];
        int d = stack_dd[sp];
        IACSharpSensor.IACSharpSensor.SensorReached(3803);
        if ((hi - lo < SMALL_THRESH) || (d > DEPTH_THRESH)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3804);
          if (mainSimpleSort(dataShadow, lo, hi, d)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3805);
            return;
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(3806);
          int d1 = d + 1;
          int med = med3(block[fmap[lo] + d1], block[fmap[hi] + d1], block[fmap[(lo + hi) >> 1] + d1]) & 0xff;
          int unLo = lo;
          int unHi = hi;
          int ltLo = lo;
          int gtHi = hi;
          IACSharpSensor.IACSharpSensor.SensorReached(3807);
          while (true) {
            IACSharpSensor.IACSharpSensor.SensorReached(3808);
            while (unLo <= unHi) {
              IACSharpSensor.IACSharpSensor.SensorReached(3809);
              int n = (block[fmap[unLo] + d1] & 0xff) - med;
              IACSharpSensor.IACSharpSensor.SensorReached(3810);
              if (n == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(3811);
                int temp = fmap[unLo];
                fmap[unLo++] = fmap[ltLo];
                fmap[ltLo++] = temp;
              } else if (n < 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(3813);
                unLo++;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(3812);
                break;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3814);
            while (unLo <= unHi) {
              IACSharpSensor.IACSharpSensor.SensorReached(3815);
              int n = (block[fmap[unHi] + d1] & 0xff) - med;
              IACSharpSensor.IACSharpSensor.SensorReached(3816);
              if (n == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(3817);
                int temp = fmap[unHi];
                fmap[unHi--] = fmap[gtHi];
                fmap[gtHi--] = temp;
              } else if (n > 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(3819);
                unHi--;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(3818);
                break;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3820);
            if (unLo <= unHi) {
              IACSharpSensor.IACSharpSensor.SensorReached(3821);
              int temp = fmap[unLo];
              fmap[unLo++] = fmap[unHi];
              fmap[unHi--] = temp;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(3822);
              break;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3823);
          if (gtHi < ltLo) {
            IACSharpSensor.IACSharpSensor.SensorReached(3824);
            stack_ll[sp] = lo;
            stack_hh[sp] = hi;
            stack_dd[sp] = d1;
            sp++;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(3825);
            int n = ((ltLo - lo) < (unLo - ltLo)) ? (ltLo - lo) : (unLo - ltLo);
            vswap(fmap, lo, unLo - n, n);
            int m = ((hi - gtHi) < (gtHi - unHi)) ? (hi - gtHi) : (gtHi - unHi);
            vswap(fmap, unLo, hi - m + 1, m);
            n = lo + unLo - ltLo - 1;
            m = hi - (gtHi - unHi) + 1;
            stack_ll[sp] = lo;
            stack_hh[sp] = n;
            stack_dd[sp] = d;
            sp++;
            stack_ll[sp] = n + 1;
            stack_hh[sp] = m - 1;
            stack_dd[sp] = d1;
            sp++;
            stack_ll[sp] = m;
            stack_hh[sp] = hi;
            stack_dd[sp] = d;
            sp++;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3826);
    }
    private void generateMTFValues()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3827);
      int lastShadow = this.last;
      CompressionState dataShadow = this.cstate;
      bool[] inUse = dataShadow.inUse;
      byte[] block = dataShadow.block;
      int[] fmap = dataShadow.fmap;
      char[] sfmap = dataShadow.sfmap;
      int[] mtfFreq = dataShadow.mtfFreq;
      byte[] unseqToSeq = dataShadow.unseqToSeq;
      byte[] yy = dataShadow.generateMTFValues_yy;
      int nInUseShadow = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3828);
      for (int i = 0; i < 256; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3829);
        if (inUse[i]) {
          IACSharpSensor.IACSharpSensor.SensorReached(3830);
          unseqToSeq[i] = (byte)nInUseShadow;
          nInUseShadow++;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3831);
      this.nInUse = nInUseShadow;
      int eob = nInUseShadow + 1;
      IACSharpSensor.IACSharpSensor.SensorReached(3832);
      for (int i = eob; i >= 0; i--) {
        IACSharpSensor.IACSharpSensor.SensorReached(3833);
        mtfFreq[i] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3834);
      for (int i = nInUseShadow; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(3835);
        yy[i] = (byte)i;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3836);
      int wr = 0;
      int zPend = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3837);
      for (int i = 0; i <= lastShadow; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3838);
        byte ll_i = unseqToSeq[block[fmap[i]] & 0xff];
        byte tmp = yy[0];
        int j = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(3839);
        while (ll_i != tmp) {
          IACSharpSensor.IACSharpSensor.SensorReached(3840);
          j++;
          byte tmp2 = tmp;
          tmp = yy[j];
          yy[j] = tmp2;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3841);
        yy[0] = tmp;
        IACSharpSensor.IACSharpSensor.SensorReached(3842);
        if (j == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3843);
          zPend++;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(3844);
          if (zPend > 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(3845);
            zPend--;
            IACSharpSensor.IACSharpSensor.SensorReached(3846);
            while (true) {
              IACSharpSensor.IACSharpSensor.SensorReached(3847);
              if ((zPend & 1) == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(3848);
                sfmap[wr] = BZip2.RUNA;
                wr++;
                mtfFreq[BZip2.RUNA]++;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(3849);
                sfmap[wr] = BZip2.RUNB;
                wr++;
                mtfFreq[BZip2.RUNB]++;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(3850);
              if (zPend >= 2) {
                IACSharpSensor.IACSharpSensor.SensorReached(3851);
                zPend = (zPend - 2) >> 1;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(3852);
                break;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3853);
            zPend = 0;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3854);
          sfmap[wr] = (char)(j + 1);
          wr++;
          mtfFreq[j + 1]++;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3855);
      if (zPend > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3856);
        zPend--;
        IACSharpSensor.IACSharpSensor.SensorReached(3857);
        while (true) {
          IACSharpSensor.IACSharpSensor.SensorReached(3858);
          if ((zPend & 1) == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(3859);
            sfmap[wr] = BZip2.RUNA;
            wr++;
            mtfFreq[BZip2.RUNA]++;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(3860);
            sfmap[wr] = BZip2.RUNB;
            wr++;
            mtfFreq[BZip2.RUNB]++;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3861);
          if (zPend >= 2) {
            IACSharpSensor.IACSharpSensor.SensorReached(3862);
            zPend = (zPend - 2) >> 1;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(3863);
            break;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3864);
      sfmap[wr] = (char)eob;
      mtfFreq[eob]++;
      this.nMTF = wr + 1;
      IACSharpSensor.IACSharpSensor.SensorReached(3865);
    }
    private static void hbAssignCodes(int[] code, byte[] length, int minLen, int maxLen, int alphaSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3866);
      int vec = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3867);
      for (int n = minLen; n <= maxLen; n++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3868);
        for (int i = 0; i < alphaSize; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(3869);
          if ((length[i] & 0xff) == n) {
            IACSharpSensor.IACSharpSensor.SensorReached(3870);
            code[i] = vec;
            vec++;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3871);
        vec <<= 1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3872);
    }
    private void sendMTFValues()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3873);
      byte[][] len = this.cstate.sendMTFValues_len;
      int alphaSize = this.nInUse + 2;
      IACSharpSensor.IACSharpSensor.SensorReached(3874);
      for (int t = BZip2.NGroups; --t >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(3875);
        byte[] len_t = len[t];
        IACSharpSensor.IACSharpSensor.SensorReached(3876);
        for (int v = alphaSize; --v >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(3877);
          len_t[v] = GREATER_ICOST;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3878);
      int nGroups = (this.nMTF < 200) ? 2 : (this.nMTF < 600) ? 3 : (this.nMTF < 1200) ? 4 : (this.nMTF < 2400) ? 5 : 6;
      sendMTFValues0(nGroups, alphaSize);
      int nSelectors = sendMTFValues1(nGroups, alphaSize);
      sendMTFValues2(nGroups, nSelectors);
      sendMTFValues3(nGroups, alphaSize);
      sendMTFValues4();
      sendMTFValues5(nGroups, nSelectors);
      sendMTFValues6(nGroups, alphaSize);
      sendMTFValues7(nSelectors);
      IACSharpSensor.IACSharpSensor.SensorReached(3879);
    }
    private void sendMTFValues0(int nGroups, int alphaSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3880);
      byte[][] len = this.cstate.sendMTFValues_len;
      int[] mtfFreq = this.cstate.mtfFreq;
      int remF = this.nMTF;
      int gs = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3881);
      for (int nPart = nGroups; nPart > 0; nPart--) {
        IACSharpSensor.IACSharpSensor.SensorReached(3882);
        int tFreq = remF / nPart;
        int ge = gs - 1;
        int aFreq = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(3883);
        for (int a = alphaSize - 1; (aFreq < tFreq) && (ge < a);) {
          IACSharpSensor.IACSharpSensor.SensorReached(3884);
          aFreq += mtfFreq[++ge];
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3885);
        if ((ge > gs) && (nPart != nGroups) && (nPart != 1) && (((nGroups - nPart) & 1) != 0)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3886);
          aFreq -= mtfFreq[ge--];
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3887);
        byte[] len_np = len[nPart - 1];
        IACSharpSensor.IACSharpSensor.SensorReached(3888);
        for (int v = alphaSize; --v >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(3889);
          if ((v >= gs) && (v <= ge)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3890);
            len_np[v] = LESSER_ICOST;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(3891);
            len_np[v] = GREATER_ICOST;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3892);
        gs = ge + 1;
        remF -= aFreq;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3893);
    }
    private static void hbMakeCodeLengths(byte[] len, int[] freq, CompressionState state1, int alphaSize, int maxLen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3894);
      int[] heap = state1.heap;
      int[] weight = state1.weight;
      int[] parent = state1.parent;
      IACSharpSensor.IACSharpSensor.SensorReached(3895);
      for (int i = alphaSize; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(3896);
        weight[i + 1] = (freq[i] == 0 ? 1 : freq[i]) << 8;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3897);
      for (bool tooLong = true; tooLong;) {
        IACSharpSensor.IACSharpSensor.SensorReached(3898);
        tooLong = false;
        int nNodes = alphaSize;
        int nHeap = 0;
        heap[0] = 0;
        weight[0] = 0;
        parent[0] = -2;
        IACSharpSensor.IACSharpSensor.SensorReached(3899);
        for (int i = 1; i <= alphaSize; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(3900);
          parent[i] = -1;
          nHeap++;
          heap[nHeap] = i;
          int zz = nHeap;
          int tmp = heap[zz];
          IACSharpSensor.IACSharpSensor.SensorReached(3901);
          while (weight[tmp] < weight[heap[zz >> 1]]) {
            IACSharpSensor.IACSharpSensor.SensorReached(3902);
            heap[zz] = heap[zz >> 1];
            zz >>= 1;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3903);
          heap[zz] = tmp;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3904);
        while (nHeap > 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(3905);
          int n1 = heap[1];
          heap[1] = heap[nHeap];
          nHeap--;
          int yy = 0;
          int zz = 1;
          int tmp = heap[1];
          IACSharpSensor.IACSharpSensor.SensorReached(3906);
          while (true) {
            IACSharpSensor.IACSharpSensor.SensorReached(3907);
            yy = zz << 1;
            IACSharpSensor.IACSharpSensor.SensorReached(3908);
            if (yy > nHeap) {
              IACSharpSensor.IACSharpSensor.SensorReached(3909);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3910);
            if ((yy < nHeap) && (weight[heap[yy + 1]] < weight[heap[yy]])) {
              IACSharpSensor.IACSharpSensor.SensorReached(3911);
              yy++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3912);
            if (weight[tmp] < weight[heap[yy]]) {
              IACSharpSensor.IACSharpSensor.SensorReached(3913);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3914);
            heap[zz] = heap[yy];
            zz = yy;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3915);
          heap[zz] = tmp;
          int n2 = heap[1];
          heap[1] = heap[nHeap];
          nHeap--;
          yy = 0;
          zz = 1;
          tmp = heap[1];
          IACSharpSensor.IACSharpSensor.SensorReached(3916);
          while (true) {
            IACSharpSensor.IACSharpSensor.SensorReached(3917);
            yy = zz << 1;
            IACSharpSensor.IACSharpSensor.SensorReached(3918);
            if (yy > nHeap) {
              IACSharpSensor.IACSharpSensor.SensorReached(3919);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3920);
            if ((yy < nHeap) && (weight[heap[yy + 1]] < weight[heap[yy]])) {
              IACSharpSensor.IACSharpSensor.SensorReached(3921);
              yy++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3922);
            if (weight[tmp] < weight[heap[yy]]) {
              IACSharpSensor.IACSharpSensor.SensorReached(3923);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3924);
            heap[zz] = heap[yy];
            zz = yy;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3925);
          heap[zz] = tmp;
          nNodes++;
          parent[n1] = parent[n2] = nNodes;
          int weight_n1 = weight[n1];
          int weight_n2 = weight[n2];
          weight[nNodes] = (int)(((uint)weight_n1 & 0xffffff00u) + ((uint)weight_n2 & 0xffffff00u)) | (1 + (((weight_n1 & 0xff) > (weight_n2 & 0xff)) ? (weight_n1 & 0xff) : (weight_n2 & 0xff)));
          parent[nNodes] = -1;
          nHeap++;
          heap[nHeap] = nNodes;
          tmp = 0;
          zz = nHeap;
          tmp = heap[zz];
          int weight_tmp = weight[tmp];
          IACSharpSensor.IACSharpSensor.SensorReached(3926);
          while (weight_tmp < weight[heap[zz >> 1]]) {
            IACSharpSensor.IACSharpSensor.SensorReached(3927);
            heap[zz] = heap[zz >> 1];
            zz >>= 1;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3928);
          heap[zz] = tmp;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3929);
        for (int i = 1; i <= alphaSize; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(3930);
          int j = 0;
          int k = i;
          IACSharpSensor.IACSharpSensor.SensorReached(3931);
          for (int parent_k; (parent_k = parent[k]) >= 0;) {
            IACSharpSensor.IACSharpSensor.SensorReached(3932);
            k = parent_k;
            j++;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3933);
          len[i - 1] = (byte)j;
          IACSharpSensor.IACSharpSensor.SensorReached(3934);
          if (j > maxLen) {
            IACSharpSensor.IACSharpSensor.SensorReached(3935);
            tooLong = true;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3936);
        if (tooLong) {
          IACSharpSensor.IACSharpSensor.SensorReached(3937);
          for (int i = 1; i < alphaSize; i++) {
            IACSharpSensor.IACSharpSensor.SensorReached(3938);
            int j = weight[i] >> 8;
            j = 1 + (j >> 1);
            weight[i] = j << 8;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3939);
    }
    private int sendMTFValues1(int nGroups, int alphaSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3940);
      CompressionState dataShadow = this.cstate;
      int[][] rfreq = dataShadow.sendMTFValues_rfreq;
      int[] fave = dataShadow.sendMTFValues_fave;
      short[] cost = dataShadow.sendMTFValues_cost;
      char[] sfmap = dataShadow.sfmap;
      byte[] selector = dataShadow.selector;
      byte[][] len = dataShadow.sendMTFValues_len;
      byte[] len_0 = len[0];
      byte[] len_1 = len[1];
      byte[] len_2 = len[2];
      byte[] len_3 = len[3];
      byte[] len_4 = len[4];
      byte[] len_5 = len[5];
      int nMTFShadow = this.nMTF;
      int nSelectors = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3941);
      for (int iter = 0; iter < BZip2.N_ITERS; iter++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3942);
        for (int t = nGroups; --t >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(3943);
          fave[t] = 0;
          int[] rfreqt = rfreq[t];
          IACSharpSensor.IACSharpSensor.SensorReached(3944);
          for (int i = alphaSize; --i >= 0;) {
            IACSharpSensor.IACSharpSensor.SensorReached(3945);
            rfreqt[i] = 0;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3946);
        nSelectors = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(3947);
        for (int gs = 0; gs < this.nMTF;) {
          IACSharpSensor.IACSharpSensor.SensorReached(3948);
          int ge = Math.Min(gs + BZip2.G_SIZE - 1, nMTFShadow - 1);
          IACSharpSensor.IACSharpSensor.SensorReached(3949);
          if (nGroups == BZip2.NGroups) {
            IACSharpSensor.IACSharpSensor.SensorReached(3950);
            int[] c = new int[6];
            IACSharpSensor.IACSharpSensor.SensorReached(3951);
            for (int i = gs; i <= ge; i++) {
              IACSharpSensor.IACSharpSensor.SensorReached(3952);
              int icv = sfmap[i];
              c[0] += len_0[icv] & 0xff;
              c[1] += len_1[icv] & 0xff;
              c[2] += len_2[icv] & 0xff;
              c[3] += len_3[icv] & 0xff;
              c[4] += len_4[icv] & 0xff;
              c[5] += len_5[icv] & 0xff;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3953);
            cost[0] = (short)c[0];
            cost[1] = (short)c[1];
            cost[2] = (short)c[2];
            cost[3] = (short)c[3];
            cost[4] = (short)c[4];
            cost[5] = (short)c[5];
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(3954);
            for (int t = nGroups; --t >= 0;) {
              IACSharpSensor.IACSharpSensor.SensorReached(3955);
              cost[t] = 0;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3956);
            for (int i = gs; i <= ge; i++) {
              IACSharpSensor.IACSharpSensor.SensorReached(3957);
              int icv = sfmap[i];
              IACSharpSensor.IACSharpSensor.SensorReached(3958);
              for (int t = nGroups; --t >= 0;) {
                IACSharpSensor.IACSharpSensor.SensorReached(3959);
                cost[t] += (short)(len[t][icv] & 0xff);
              }
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3960);
          int bt = -1;
          IACSharpSensor.IACSharpSensor.SensorReached(3961);
          for (int t = nGroups, bc = 999999999; --t >= 0;) {
            IACSharpSensor.IACSharpSensor.SensorReached(3962);
            int cost_t = cost[t];
            IACSharpSensor.IACSharpSensor.SensorReached(3963);
            if (cost_t < bc) {
              IACSharpSensor.IACSharpSensor.SensorReached(3964);
              bc = cost_t;
              bt = t;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3965);
          fave[bt]++;
          selector[nSelectors] = (byte)bt;
          nSelectors++;
          int[] rfreq_bt = rfreq[bt];
          IACSharpSensor.IACSharpSensor.SensorReached(3966);
          for (int i = gs; i <= ge; i++) {
            IACSharpSensor.IACSharpSensor.SensorReached(3967);
            rfreq_bt[sfmap[i]]++;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3968);
          gs = ge + 1;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3969);
        for (int t = 0; t < nGroups; t++) {
          IACSharpSensor.IACSharpSensor.SensorReached(3970);
          hbMakeCodeLengths(len[t], rfreq[t], this.cstate, alphaSize, 20);
        }
      }
      System.Int32 RNTRNTRNT_485 = nSelectors;
      IACSharpSensor.IACSharpSensor.SensorReached(3971);
      return RNTRNTRNT_485;
    }
    private void sendMTFValues2(int nGroups, int nSelectors)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3972);
      CompressionState dataShadow = this.cstate;
      byte[] pos = dataShadow.sendMTFValues2_pos;
      IACSharpSensor.IACSharpSensor.SensorReached(3973);
      for (int i = nGroups; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(3974);
        pos[i] = (byte)i;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3975);
      for (int i = 0; i < nSelectors; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3976);
        byte ll_i = dataShadow.selector[i];
        byte tmp = pos[0];
        int j = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(3977);
        while (ll_i != tmp) {
          IACSharpSensor.IACSharpSensor.SensorReached(3978);
          j++;
          byte tmp2 = tmp;
          tmp = pos[j];
          pos[j] = tmp2;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3979);
        pos[0] = tmp;
        dataShadow.selectorMtf[i] = (byte)j;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3980);
    }
    private void sendMTFValues3(int nGroups, int alphaSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3981);
      int[][] code = this.cstate.sendMTFValues_code;
      byte[][] len = this.cstate.sendMTFValues_len;
      IACSharpSensor.IACSharpSensor.SensorReached(3982);
      for (int t = 0; t < nGroups; t++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3983);
        int minLen = 32;
        int maxLen = 0;
        byte[] len_t = len[t];
        IACSharpSensor.IACSharpSensor.SensorReached(3984);
        for (int i = alphaSize; --i >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(3985);
          int l = len_t[i] & 0xff;
          IACSharpSensor.IACSharpSensor.SensorReached(3986);
          if (l > maxLen) {
            IACSharpSensor.IACSharpSensor.SensorReached(3987);
            maxLen = l;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3988);
          if (l < minLen) {
            IACSharpSensor.IACSharpSensor.SensorReached(3989);
            minLen = l;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3990);
        hbAssignCodes(code[t], len[t], minLen, maxLen, alphaSize);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3991);
    }
    private void sendMTFValues4()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3992);
      bool[] inUse = this.cstate.inUse;
      bool[] inUse16 = this.cstate.sentMTFValues4_inUse16;
      IACSharpSensor.IACSharpSensor.SensorReached(3993);
      for (int i = 16; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(3994);
        inUse16[i] = false;
        int i16 = i * 16;
        IACSharpSensor.IACSharpSensor.SensorReached(3995);
        for (int j = 16; --j >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(3996);
          if (inUse[i16 + j]) {
            IACSharpSensor.IACSharpSensor.SensorReached(3997);
            inUse16[i] = true;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3998);
      uint u = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3999);
      for (int i = 0; i < 16; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4000);
        if (inUse16[i]) {
          IACSharpSensor.IACSharpSensor.SensorReached(4001);
          u |= 1u << (16 - i - 1);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4002);
      this.bw.WriteBits(16, u);
      IACSharpSensor.IACSharpSensor.SensorReached(4003);
      for (int i = 0; i < 16; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4004);
        if (inUse16[i]) {
          IACSharpSensor.IACSharpSensor.SensorReached(4005);
          int i16 = i * 16;
          u = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(4006);
          for (int j = 0; j < 16; j++) {
            IACSharpSensor.IACSharpSensor.SensorReached(4007);
            if (inUse[i16 + j]) {
              IACSharpSensor.IACSharpSensor.SensorReached(4008);
              u |= 1u << (16 - j - 1);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4009);
          this.bw.WriteBits(16, u);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4010);
    }
    private void sendMTFValues5(int nGroups, int nSelectors)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4011);
      this.bw.WriteBits(3, (uint)nGroups);
      this.bw.WriteBits(15, (uint)nSelectors);
      byte[] selectorMtf = this.cstate.selectorMtf;
      IACSharpSensor.IACSharpSensor.SensorReached(4012);
      for (int i = 0; i < nSelectors; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4013);
        for (int j = 0, hj = selectorMtf[i] & 0xff; j < hj; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(4014);
          this.bw.WriteBits(1, 1);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4015);
        this.bw.WriteBits(1, 0);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4016);
    }
    private void sendMTFValues6(int nGroups, int alphaSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4017);
      byte[][] len = this.cstate.sendMTFValues_len;
      IACSharpSensor.IACSharpSensor.SensorReached(4018);
      for (int t = 0; t < nGroups; t++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4019);
        byte[] len_t = len[t];
        uint curr = (uint)(len_t[0] & 0xff);
        this.bw.WriteBits(5, curr);
        IACSharpSensor.IACSharpSensor.SensorReached(4020);
        for (int i = 0; i < alphaSize; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(4021);
          int lti = len_t[i] & 0xff;
          IACSharpSensor.IACSharpSensor.SensorReached(4022);
          while (curr < lti) {
            IACSharpSensor.IACSharpSensor.SensorReached(4023);
            this.bw.WriteBits(2, 2u);
            curr++;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4024);
          while (curr > lti) {
            IACSharpSensor.IACSharpSensor.SensorReached(4025);
            this.bw.WriteBits(2, 3u);
            curr--;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4026);
          this.bw.WriteBits(1, 0u);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4027);
    }
    private void sendMTFValues7(int nSelectors)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4028);
      byte[][] len = this.cstate.sendMTFValues_len;
      int[][] code = this.cstate.sendMTFValues_code;
      byte[] selector = this.cstate.selector;
      char[] sfmap = this.cstate.sfmap;
      int nMTFShadow = this.nMTF;
      int selCtr = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4029);
      for (int gs = 0; gs < nMTFShadow;) {
        IACSharpSensor.IACSharpSensor.SensorReached(4030);
        int ge = Math.Min(gs + BZip2.G_SIZE - 1, nMTFShadow - 1);
        int ix = selector[selCtr] & 0xff;
        int[] code_selCtr = code[ix];
        byte[] len_selCtr = len[ix];
        IACSharpSensor.IACSharpSensor.SensorReached(4031);
        while (gs <= ge) {
          IACSharpSensor.IACSharpSensor.SensorReached(4032);
          int sfmap_i = sfmap[gs];
          int n = len_selCtr[sfmap_i] & 0xff;
          this.bw.WriteBits(n, (uint)code_selCtr[sfmap_i]);
          gs++;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4033);
        gs = ge + 1;
        selCtr++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4034);
    }
    private void moveToFrontCodeAndSend()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4035);
      this.bw.WriteBits(24, (uint)this.origPtr);
      generateMTFValues();
      sendMTFValues();
      IACSharpSensor.IACSharpSensor.SensorReached(4036);
    }
    private class CompressionState
    {
      public readonly bool[] inUse = new bool[256];
      public readonly byte[] unseqToSeq = new byte[256];
      public readonly int[] mtfFreq = new int[BZip2.MaxAlphaSize];
      public readonly byte[] selector = new byte[BZip2.MaxSelectors];
      public readonly byte[] selectorMtf = new byte[BZip2.MaxSelectors];
      public readonly byte[] generateMTFValues_yy = new byte[256];
      public byte[][] sendMTFValues_len;
      public int[][] sendMTFValues_rfreq;
      public readonly int[] sendMTFValues_fave = new int[BZip2.NGroups];
      public readonly short[] sendMTFValues_cost = new short[BZip2.NGroups];
      public int[][] sendMTFValues_code;
      public readonly byte[] sendMTFValues2_pos = new byte[BZip2.NGroups];
      public readonly bool[] sentMTFValues4_inUse16 = new bool[16];
      public readonly int[] stack_ll = new int[BZip2.QSORT_STACK_SIZE];
      public readonly int[] stack_hh = new int[BZip2.QSORT_STACK_SIZE];
      public readonly int[] stack_dd = new int[BZip2.QSORT_STACK_SIZE];
      public readonly int[] mainSort_runningOrder = new int[256];
      public readonly int[] mainSort_copy = new int[256];
      public readonly bool[] mainSort_bigDone = new bool[256];
      public int[] heap = new int[BZip2.MaxAlphaSize + 2];
      public int[] weight = new int[BZip2.MaxAlphaSize * 2];
      public int[] parent = new int[BZip2.MaxAlphaSize * 2];
      public readonly int[] ftab = new int[65537];
      public byte[] block;
      public int[] fmap;
      public char[] sfmap;
      public char[] quadrant;
      public CompressionState(int blockSize100k)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(4037);
        int n = blockSize100k * BZip2.BlockSizeMultiple;
        this.block = new byte[(n + 1 + BZip2.NUM_OVERSHOOT_BYTES)];
        this.fmap = new int[n];
        this.sfmap = new char[2 * n];
        this.quadrant = this.sfmap;
        this.sendMTFValues_len = BZip2.InitRectangularArray<byte>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.sendMTFValues_rfreq = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.sendMTFValues_code = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        IACSharpSensor.IACSharpSensor.SensorReached(4038);
      }
    }
  }
}
