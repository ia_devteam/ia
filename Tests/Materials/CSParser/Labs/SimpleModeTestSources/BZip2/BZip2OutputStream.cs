using System;
using System.IO;
namespace Ionic.BZip2
{
  public class BZip2OutputStream : System.IO.Stream
  {
    int totalBytesWrittenIn;
    bool leaveOpen;
    BZip2Compressor compressor;
    uint combinedCRC;
    Stream output;
    BitWriter bw;
    int blockSize100k;
    private TraceBits desiredTrace = TraceBits.Crc | TraceBits.Write;
    public BZip2OutputStream(Stream output) : this(output, BZip2.MaxBlockSize, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4383);
    }
    public BZip2OutputStream(Stream output, int blockSize) : this(output, blockSize, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4384);
    }
    public BZip2OutputStream(Stream output, bool leaveOpen) : this(output, BZip2.MaxBlockSize, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4385);
    }
    public BZip2OutputStream(Stream output, int blockSize, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4386);
      if (blockSize < BZip2.MinBlockSize || blockSize > BZip2.MaxBlockSize) {
        IACSharpSensor.IACSharpSensor.SensorReached(4387);
        var msg = String.Format("blockSize={0} is out of range; must be between {1} and {2}", blockSize, BZip2.MinBlockSize, BZip2.MaxBlockSize);
        IACSharpSensor.IACSharpSensor.SensorReached(4388);
        throw new ArgumentException(msg, "blockSize");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4389);
      this.output = output;
      IACSharpSensor.IACSharpSensor.SensorReached(4390);
      if (!this.output.CanWrite) {
        IACSharpSensor.IACSharpSensor.SensorReached(4391);
        throw new ArgumentException("The stream is not writable.", "output");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4392);
      this.bw = new BitWriter(this.output);
      this.blockSize100k = blockSize;
      this.compressor = new BZip2Compressor(this.bw, blockSize);
      this.leaveOpen = leaveOpen;
      this.combinedCRC = 0;
      EmitHeader();
      IACSharpSensor.IACSharpSensor.SensorReached(4393);
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4394);
      if (output != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4395);
        Stream o = this.output;
        Finish();
        IACSharpSensor.IACSharpSensor.SensorReached(4396);
        if (!leaveOpen) {
          IACSharpSensor.IACSharpSensor.SensorReached(4397);
          o.Close();
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4398);
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4399);
      if (this.output != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4400);
        this.bw.Flush();
        this.output.Flush();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4401);
    }
    private void EmitHeader()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4402);
      var magic = new byte[] {
        (byte)'B',
        (byte)'Z',
        (byte)'h',
        (byte)('0' + this.blockSize100k)
      };
      this.output.Write(magic, 0, magic.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(4403);
    }
    private void EmitTrailer()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4404);
      TraceOutput(TraceBits.Write, "total written out: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      this.bw.WriteByte(0x17);
      this.bw.WriteByte(0x72);
      this.bw.WriteByte(0x45);
      this.bw.WriteByte(0x38);
      this.bw.WriteByte(0x50);
      this.bw.WriteByte(0x90);
      this.bw.WriteInt(this.combinedCRC);
      this.bw.FinishAndPad();
      TraceOutput(TraceBits.Write, "final total: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      IACSharpSensor.IACSharpSensor.SensorReached(4405);
    }
    void Finish()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4406);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(4407);
        var totalBefore = this.bw.TotalBytesWrittenOut;
        this.compressor.CompressAndWrite();
        TraceOutput(TraceBits.Write, "out block length (bytes): {0} (0x{0:X})", this.bw.TotalBytesWrittenOut - totalBefore);
        TraceOutput(TraceBits.Crc, " combined CRC (before): {0:X8}", this.combinedCRC);
        this.combinedCRC = (this.combinedCRC << 1) | (this.combinedCRC >> 31);
        this.combinedCRC ^= (uint)compressor.Crc32;
        TraceOutput(TraceBits.Crc, " block    CRC         : {0:X8}", this.compressor.Crc32);
        TraceOutput(TraceBits.Crc, " combined CRC (final) : {0:X8}", this.combinedCRC);
        EmitTrailer();
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(4408);
        this.output = null;
        this.compressor = null;
        this.bw = null;
        IACSharpSensor.IACSharpSensor.SensorReached(4409);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4410);
    }
    public int BlockSize {
      get {
        System.Int32 RNTRNTRNT_500 = this.blockSize100k;
        IACSharpSensor.IACSharpSensor.SensorReached(4411);
        return RNTRNTRNT_500;
      }
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4412);
      if (offset < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4413);
        throw new IndexOutOfRangeException(String.Format("offset ({0}) must be > 0", offset));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4414);
      if (count < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4415);
        throw new IndexOutOfRangeException(String.Format("count ({0}) must be > 0", count));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4416);
      if (offset + count > buffer.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(4417);
        throw new IndexOutOfRangeException(String.Format("offset({0}) count({1}) bLength({2})", offset, count, buffer.Length));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4418);
      if (this.output == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4419);
        throw new IOException("the stream is not open");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4420);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4421);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4422);
      int bytesWritten = 0;
      int bytesRemaining = count;
      IACSharpSensor.IACSharpSensor.SensorReached(4423);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(4424);
        int n = compressor.Fill(buffer, offset, bytesRemaining);
        IACSharpSensor.IACSharpSensor.SensorReached(4425);
        if (n != bytesRemaining) {
          IACSharpSensor.IACSharpSensor.SensorReached(4426);
          var totalBefore = this.bw.TotalBytesWrittenOut;
          this.compressor.CompressAndWrite();
          TraceOutput(TraceBits.Write, "out block length (bytes): {0} (0x{0:X})", this.bw.TotalBytesWrittenOut - totalBefore);
          TraceOutput(TraceBits.Write, " remaining: {0} 0x{1:X}", this.bw.NumRemainingBits, this.bw.RemainingBits);
          TraceOutput(TraceBits.Crc, " combined CRC (before): {0:X8}", this.combinedCRC);
          this.combinedCRC = (this.combinedCRC << 1) | (this.combinedCRC >> 31);
          this.combinedCRC ^= (uint)compressor.Crc32;
          TraceOutput(TraceBits.Crc, " block    CRC         : {0:X8}", compressor.Crc32);
          TraceOutput(TraceBits.Crc, " combined CRC (after) : {0:X8}", this.combinedCRC);
          offset += n;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4427);
        bytesRemaining -= n;
        bytesWritten += n;
      } while (bytesRemaining > 0);
      IACSharpSensor.IACSharpSensor.SensorReached(4428);
      totalBytesWrittenIn += bytesWritten;
      IACSharpSensor.IACSharpSensor.SensorReached(4429);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_501 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(4430);
        return RNTRNTRNT_501;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_502 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(4431);
        return RNTRNTRNT_502;
      }
    }
    public override bool CanWrite {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4432);
        if (this.output == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4433);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_503 = this.output.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(4434);
        return RNTRNTRNT_503;
      }
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4435);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_504 = this.totalBytesWrittenIn;
        IACSharpSensor.IACSharpSensor.SensorReached(4436);
        return RNTRNTRNT_504;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4437);
        throw new NotImplementedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4438);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4439);
      throw new NotImplementedException();
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4440);
      throw new NotImplementedException();
    }
    [Flags()]
    enum TraceBits : uint
    {
      None = 0,
      Crc = 1,
      Write = 2,
      All = 0xffffffffu
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceOutput(TraceBits bits, string format, params object[] varParams)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4441);
      if ((bits & this.desiredTrace) != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4443);
        {
          IACSharpSensor.IACSharpSensor.SensorReached(4442);
          int tid = System.Threading.Thread.CurrentThread.GetHashCode();
          Console.ForegroundColor = (ConsoleColor)(tid % 8 + 10);
          Console.Write("{0:000} PBOS ", tid);
          Console.WriteLine(format, varParams);
          Console.ResetColor();
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4444);
    }
  }
}
