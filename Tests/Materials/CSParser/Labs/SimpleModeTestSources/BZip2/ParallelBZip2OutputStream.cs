using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
namespace Ionic.BZip2
{
  internal class WorkItem
  {
    public int index;
    public BZip2Compressor Compressor { get; private set; }
    public MemoryStream ms;
    public int ordinal;
    public BitWriter bw;
    public WorkItem(int ix, int blockSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4445);
      this.ms = new MemoryStream();
      this.bw = new BitWriter(ms);
      this.Compressor = new BZip2Compressor(bw, blockSize);
      this.index = ix;
      IACSharpSensor.IACSharpSensor.SensorReached(4446);
    }
  }
  public class ParallelBZip2OutputStream : System.IO.Stream
  {
    private static readonly int BufferPairsPerCore = 4;
    private int _maxWorkers;
    private bool firstWriteDone;
    private int lastFilled;
    private int lastWritten;
    private int latestCompressed;
    private int currentlyFilling;
    private volatile Exception pendingException;
    private bool handlingException;
    private bool emitting;
    private System.Collections.Generic.Queue<int> toWrite;
    private System.Collections.Generic.Queue<int> toFill;
    private System.Collections.Generic.List<WorkItem> pool;
    private object latestLock = new object();
    private object eLock = new object();
    private object outputLock = new object();
    private AutoResetEvent newlyCompressedBlob;
    long totalBytesWrittenIn;
    long totalBytesWrittenOut;
    bool leaveOpen;
    uint combinedCRC;
    Stream output;
    BitWriter bw;
    int blockSize100k;
    private TraceBits desiredTrace = TraceBits.Crc | TraceBits.Write;
    public ParallelBZip2OutputStream(Stream output) : this(output, BZip2.MaxBlockSize, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4447);
    }
    public ParallelBZip2OutputStream(Stream output, int blockSize) : this(output, blockSize, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4448);
    }
    public ParallelBZip2OutputStream(Stream output, bool leaveOpen) : this(output, BZip2.MaxBlockSize, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4449);
    }
    public ParallelBZip2OutputStream(Stream output, int blockSize, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4450);
      if (blockSize < BZip2.MinBlockSize || blockSize > BZip2.MaxBlockSize) {
        IACSharpSensor.IACSharpSensor.SensorReached(4451);
        var msg = String.Format("blockSize={0} is out of range; must be between {1} and {2}", blockSize, BZip2.MinBlockSize, BZip2.MaxBlockSize);
        IACSharpSensor.IACSharpSensor.SensorReached(4452);
        throw new ArgumentException(msg, "blockSize");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4453);
      this.output = output;
      IACSharpSensor.IACSharpSensor.SensorReached(4454);
      if (!this.output.CanWrite) {
        IACSharpSensor.IACSharpSensor.SensorReached(4455);
        throw new ArgumentException("The stream is not writable.", "output");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4456);
      this.bw = new BitWriter(this.output);
      this.blockSize100k = blockSize;
      this.leaveOpen = leaveOpen;
      this.combinedCRC = 0;
      this.MaxWorkers = 16;
      EmitHeader();
      IACSharpSensor.IACSharpSensor.SensorReached(4457);
    }
    private void InitializePoolOfWorkItems()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4458);
      this.toWrite = new Queue<int>();
      this.toFill = new Queue<int>();
      this.pool = new System.Collections.Generic.List<WorkItem>();
      int nWorkers = BufferPairsPerCore * Environment.ProcessorCount;
      nWorkers = Math.Min(nWorkers, this.MaxWorkers);
      IACSharpSensor.IACSharpSensor.SensorReached(4459);
      for (int i = 0; i < nWorkers; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4460);
        this.pool.Add(new WorkItem(i, this.blockSize100k));
        this.toFill.Enqueue(i);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4461);
      this.newlyCompressedBlob = new AutoResetEvent(false);
      this.currentlyFilling = -1;
      this.lastFilled = -1;
      this.lastWritten = -1;
      this.latestCompressed = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(4462);
    }
    public int MaxWorkers {
      get {
        System.Int32 RNTRNTRNT_505 = _maxWorkers;
        IACSharpSensor.IACSharpSensor.SensorReached(4463);
        return RNTRNTRNT_505;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4464);
        if (value < 4) {
          IACSharpSensor.IACSharpSensor.SensorReached(4465);
          throw new ArgumentException("MaxWorkers", "Value must be 4 or greater.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4466);
        _maxWorkers = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4467);
      }
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4468);
      if (this.pendingException != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4469);
        this.handlingException = true;
        var pe = this.pendingException;
        this.pendingException = null;
        IACSharpSensor.IACSharpSensor.SensorReached(4470);
        throw pe;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4471);
      if (this.handlingException) {
        IACSharpSensor.IACSharpSensor.SensorReached(4472);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4473);
      if (output == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4474);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4475);
      Stream o = this.output;
      IACSharpSensor.IACSharpSensor.SensorReached(4476);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(4477);
        FlushOutput(true);
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(4478);
        this.output = null;
        this.bw = null;
        IACSharpSensor.IACSharpSensor.SensorReached(4479);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4480);
      if (!leaveOpen) {
        IACSharpSensor.IACSharpSensor.SensorReached(4481);
        o.Close();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4482);
    }
    private void FlushOutput(bool lastInput)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4483);
      if (this.emitting) {
        IACSharpSensor.IACSharpSensor.SensorReached(4484);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4485);
      if (this.currentlyFilling >= 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4486);
        WorkItem workitem = this.pool[this.currentlyFilling];
        CompressOne(workitem);
        this.currentlyFilling = -1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4487);
      if (lastInput) {
        IACSharpSensor.IACSharpSensor.SensorReached(4488);
        EmitPendingBuffers(true, false);
        EmitTrailer();
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4489);
        EmitPendingBuffers(false, false);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4490);
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4491);
      if (this.output != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4492);
        FlushOutput(false);
        this.bw.Flush();
        this.output.Flush();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4493);
    }
    private void EmitHeader()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4494);
      var magic = new byte[] {
        (byte)'B',
        (byte)'Z',
        (byte)'h',
        (byte)('0' + this.blockSize100k)
      };
      this.output.Write(magic, 0, magic.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(4495);
    }
    private void EmitTrailer()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4496);
      TraceOutput(TraceBits.Write, "total written out: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      this.bw.WriteByte(0x17);
      this.bw.WriteByte(0x72);
      this.bw.WriteByte(0x45);
      this.bw.WriteByte(0x38);
      this.bw.WriteByte(0x50);
      this.bw.WriteByte(0x90);
      this.bw.WriteInt(this.combinedCRC);
      this.bw.FinishAndPad();
      TraceOutput(TraceBits.Write, "final total : {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      IACSharpSensor.IACSharpSensor.SensorReached(4497);
    }
    public int BlockSize {
      get {
        System.Int32 RNTRNTRNT_506 = this.blockSize100k;
        IACSharpSensor.IACSharpSensor.SensorReached(4498);
        return RNTRNTRNT_506;
      }
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4499);
      bool mustWait = false;
      IACSharpSensor.IACSharpSensor.SensorReached(4500);
      if (this.output == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4501);
        throw new IOException("the stream is not open");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4502);
      if (this.pendingException != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4503);
        this.handlingException = true;
        var pe = this.pendingException;
        this.pendingException = null;
        IACSharpSensor.IACSharpSensor.SensorReached(4504);
        throw pe;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4505);
      if (offset < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4506);
        throw new IndexOutOfRangeException(String.Format("offset ({0}) must be > 0", offset));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4507);
      if (count < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4508);
        throw new IndexOutOfRangeException(String.Format("count ({0}) must be > 0", count));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4509);
      if (offset + count > buffer.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(4510);
        throw new IndexOutOfRangeException(String.Format("offset({0}) count({1}) bLength({2})", offset, count, buffer.Length));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4511);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4512);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4513);
      if (!this.firstWriteDone) {
        IACSharpSensor.IACSharpSensor.SensorReached(4514);
        InitializePoolOfWorkItems();
        this.firstWriteDone = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4515);
      int bytesWritten = 0;
      int bytesRemaining = count;
      IACSharpSensor.IACSharpSensor.SensorReached(4516);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(4517);
        EmitPendingBuffers(false, mustWait);
        mustWait = false;
        int ix = -1;
        IACSharpSensor.IACSharpSensor.SensorReached(4518);
        if (this.currentlyFilling >= 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(4519);
          ix = this.currentlyFilling;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4520);
          if (this.toFill.Count == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(4521);
            mustWait = true;
            IACSharpSensor.IACSharpSensor.SensorReached(4522);
            continue;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4523);
          ix = this.toFill.Dequeue();
          ++this.lastFilled;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4524);
        WorkItem workitem = this.pool[ix];
        workitem.ordinal = this.lastFilled;
        int n = workitem.Compressor.Fill(buffer, offset, bytesRemaining);
        IACSharpSensor.IACSharpSensor.SensorReached(4525);
        if (n != bytesRemaining) {
          IACSharpSensor.IACSharpSensor.SensorReached(4526);
          if (!ThreadPool.QueueUserWorkItem(CompressOne, workitem)) {
            IACSharpSensor.IACSharpSensor.SensorReached(4527);
            throw new Exception("Cannot enqueue workitem");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4528);
          this.currentlyFilling = -1;
          offset += n;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4529);
          this.currentlyFilling = ix;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4530);
        bytesRemaining -= n;
        bytesWritten += n;
      } while (bytesRemaining > 0);
      IACSharpSensor.IACSharpSensor.SensorReached(4531);
      totalBytesWrittenIn += bytesWritten;
      IACSharpSensor.IACSharpSensor.SensorReached(4532);
      return;
    }
    private void EmitPendingBuffers(bool doAll, bool mustWait)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4533);
      if (emitting) {
        IACSharpSensor.IACSharpSensor.SensorReached(4534);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4535);
      emitting = true;
      IACSharpSensor.IACSharpSensor.SensorReached(4536);
      if (doAll || mustWait) {
        IACSharpSensor.IACSharpSensor.SensorReached(4537);
        this.newlyCompressedBlob.WaitOne();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4538);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(4539);
        int firstSkip = -1;
        int millisecondsToWait = doAll ? 200 : (mustWait ? -1 : 0);
        int nextToWrite = -1;
        IACSharpSensor.IACSharpSensor.SensorReached(4540);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(4541);
          if (Monitor.TryEnter(this.toWrite, millisecondsToWait)) {
            IACSharpSensor.IACSharpSensor.SensorReached(4542);
            nextToWrite = -1;
            IACSharpSensor.IACSharpSensor.SensorReached(4543);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(4544);
              if (this.toWrite.Count > 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(4545);
                nextToWrite = this.toWrite.Dequeue();
              }
            } finally {
              IACSharpSensor.IACSharpSensor.SensorReached(4546);
              Monitor.Exit(this.toWrite);
              IACSharpSensor.IACSharpSensor.SensorReached(4547);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(4548);
            if (nextToWrite >= 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(4549);
              WorkItem workitem = this.pool[nextToWrite];
              IACSharpSensor.IACSharpSensor.SensorReached(4550);
              if (workitem.ordinal != this.lastWritten + 1) {
                lock (this.toWrite) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4551);
                  this.toWrite.Enqueue(nextToWrite);
                }
                IACSharpSensor.IACSharpSensor.SensorReached(4552);
                if (firstSkip == nextToWrite) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4553);
                  this.newlyCompressedBlob.WaitOne();
                  firstSkip = -1;
                } else if (firstSkip == -1) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4554);
                  firstSkip = nextToWrite;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(4555);
                continue;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(4556);
              firstSkip = -1;
              TraceOutput(TraceBits.Write, "Writing block {0}", workitem.ordinal);
              var bw2 = workitem.bw;
              bw2.Flush();
              var ms = workitem.ms;
              ms.Seek(0, SeekOrigin.Begin);
              int n;
              int y = -1;
              long totOut = 0;
              var buffer = new byte[1024];
              IACSharpSensor.IACSharpSensor.SensorReached(4557);
              while ((n = ms.Read(buffer, 0, buffer.Length)) > 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(4558);
                y = n;
                IACSharpSensor.IACSharpSensor.SensorReached(4559);
                for (int k = 0; k < n; k++) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4560);
                  this.bw.WriteByte(buffer[k]);
                }
                IACSharpSensor.IACSharpSensor.SensorReached(4561);
                totOut += n;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(4562);
              TraceOutput(TraceBits.Write, " remaining bits: {0} 0x{1:X}", bw2.NumRemainingBits, bw2.RemainingBits);
              IACSharpSensor.IACSharpSensor.SensorReached(4563);
              if (bw2.NumRemainingBits > 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(4564);
                this.bw.WriteBits(bw2.NumRemainingBits, bw2.RemainingBits);
              }
              IACSharpSensor.IACSharpSensor.SensorReached(4565);
              TraceOutput(TraceBits.Crc, " combined CRC (before): {0:X8}", this.combinedCRC);
              this.combinedCRC = (this.combinedCRC << 1) | (this.combinedCRC >> 31);
              this.combinedCRC ^= (uint)workitem.Compressor.Crc32;
              TraceOutput(TraceBits.Crc, " block    CRC         : {0:X8}", workitem.Compressor.Crc32);
              TraceOutput(TraceBits.Crc, " combined CRC (after) : {0:X8}", this.combinedCRC);
              TraceOutput(TraceBits.Write, "total written out: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
              TraceOutput(TraceBits.Write | TraceBits.Crc, "");
              this.totalBytesWrittenOut += totOut;
              bw2.Reset();
              this.lastWritten = workitem.ordinal;
              workitem.ordinal = -1;
              this.toFill.Enqueue(workitem.index);
              IACSharpSensor.IACSharpSensor.SensorReached(4566);
              if (millisecondsToWait == -1) {
                IACSharpSensor.IACSharpSensor.SensorReached(4567);
                millisecondsToWait = 0;
              }
            }
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(4568);
            nextToWrite = -1;
          }
        } while (nextToWrite >= 0);
      } while (doAll && (this.lastWritten != this.latestCompressed));
      IACSharpSensor.IACSharpSensor.SensorReached(4569);
      if (doAll) {
        IACSharpSensor.IACSharpSensor.SensorReached(4570);
        TraceOutput(TraceBits.Crc, " combined CRC (final) : {0:X8}", this.combinedCRC);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4571);
      emitting = false;
      IACSharpSensor.IACSharpSensor.SensorReached(4572);
    }
    private void CompressOne(Object wi)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4573);
      WorkItem workitem = (WorkItem)wi;
      IACSharpSensor.IACSharpSensor.SensorReached(4574);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(4575);
        workitem.Compressor.CompressAndWrite();
        lock (this.latestLock) {
          IACSharpSensor.IACSharpSensor.SensorReached(4576);
          if (workitem.ordinal > this.latestCompressed) {
            IACSharpSensor.IACSharpSensor.SensorReached(4577);
            this.latestCompressed = workitem.ordinal;
          }
        }
        lock (this.toWrite) {
          IACSharpSensor.IACSharpSensor.SensorReached(4578);
          this.toWrite.Enqueue(workitem.index);
        }
        this.newlyCompressedBlob.Set();
      } catch (System.Exception exc1) {
        lock (this.eLock) {
          IACSharpSensor.IACSharpSensor.SensorReached(4579);
          if (this.pendingException != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(4580);
            this.pendingException = exc1;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4581);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4582);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_507 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(4583);
        return RNTRNTRNT_507;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_508 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(4584);
        return RNTRNTRNT_508;
      }
    }
    public override bool CanWrite {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4585);
        if (this.output == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4586);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_509 = this.output.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(4587);
        return RNTRNTRNT_509;
      }
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4588);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_510 = this.totalBytesWrittenIn;
        IACSharpSensor.IACSharpSensor.SensorReached(4589);
        return RNTRNTRNT_510;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4590);
        throw new NotImplementedException();
      }
    }
    public Int64 BytesWrittenOut {
      get {
        Int64 RNTRNTRNT_511 = totalBytesWrittenOut;
        IACSharpSensor.IACSharpSensor.SensorReached(4591);
        return RNTRNTRNT_511;
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4592);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4593);
      throw new NotImplementedException();
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4594);
      throw new NotImplementedException();
    }
    [Flags()]
    enum TraceBits : uint
    {
      None = 0,
      Crc = 1,
      Write = 2,
      All = 0xffffffffu
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceOutput(TraceBits bits, string format, params object[] varParams)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4595);
      if ((bits & this.desiredTrace) != 0) {
        lock (outputLock) {
          IACSharpSensor.IACSharpSensor.SensorReached(4596);
          int tid = Thread.CurrentThread.GetHashCode();
          Console.ForegroundColor = (ConsoleColor)(tid % 8 + 10);
          Console.Write("{0:000} PBOS ", tid);
          Console.WriteLine(format, varParams);
          Console.ResetColor();
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4597);
    }
  }
}
