using System;
using System.IO;
namespace Ionic.BZip2
{
  internal class BitWriter
  {
    uint accumulator;
    int nAccumulatedBits;
    Stream output;
    int totalBytesWrittenOut;
    public BitWriter(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3565);
      this.output = s;
      IACSharpSensor.IACSharpSensor.SensorReached(3566);
    }
    public byte RemainingBits {
      get {
        System.Byte RNTRNTRNT_469 = (byte)(this.accumulator >> (32 - this.nAccumulatedBits) & 0xff);
        IACSharpSensor.IACSharpSensor.SensorReached(3567);
        return RNTRNTRNT_469;
      }
    }
    public int NumRemainingBits {
      get {
        System.Int32 RNTRNTRNT_470 = this.nAccumulatedBits;
        IACSharpSensor.IACSharpSensor.SensorReached(3568);
        return RNTRNTRNT_470;
      }
    }
    public int TotalBytesWrittenOut {
      get {
        System.Int32 RNTRNTRNT_471 = this.totalBytesWrittenOut;
        IACSharpSensor.IACSharpSensor.SensorReached(3569);
        return RNTRNTRNT_471;
      }
    }
    public void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3570);
      this.accumulator = 0;
      this.nAccumulatedBits = 0;
      this.totalBytesWrittenOut = 0;
      this.output.Seek(0, SeekOrigin.Begin);
      this.output.SetLength(0);
      IACSharpSensor.IACSharpSensor.SensorReached(3571);
    }
    public void WriteBits(int nbits, uint value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3572);
      int nAccumulated = this.nAccumulatedBits;
      uint u = this.accumulator;
      IACSharpSensor.IACSharpSensor.SensorReached(3573);
      while (nAccumulated >= 8) {
        IACSharpSensor.IACSharpSensor.SensorReached(3574);
        this.output.WriteByte((byte)(u >> 24 & 0xff));
        this.totalBytesWrittenOut++;
        u <<= 8;
        nAccumulated -= 8;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3575);
      this.accumulator = u | (value << (32 - nAccumulated - nbits));
      this.nAccumulatedBits = nAccumulated + nbits;
      IACSharpSensor.IACSharpSensor.SensorReached(3576);
    }
    public void WriteByte(byte b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3577);
      WriteBits(8, b);
      IACSharpSensor.IACSharpSensor.SensorReached(3578);
    }
    public void WriteInt(uint u)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3579);
      WriteBits(8, (u >> 24) & 0xff);
      WriteBits(8, (u >> 16) & 0xff);
      WriteBits(8, (u >> 8) & 0xff);
      WriteBits(8, u & 0xff);
      IACSharpSensor.IACSharpSensor.SensorReached(3580);
    }
    public void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3581);
      WriteBits(0, 0);
      IACSharpSensor.IACSharpSensor.SensorReached(3582);
    }
    public void FinishAndPad()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3583);
      Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(3584);
      if (this.NumRemainingBits > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3585);
        byte b = (byte)((this.accumulator >> 24) & 0xff);
        this.output.WriteByte(b);
        this.totalBytesWrittenOut++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3586);
    }
  }
}
