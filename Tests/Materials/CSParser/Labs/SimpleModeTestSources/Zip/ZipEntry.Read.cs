using System;
using System.IO;
namespace Ionic.Zip
{
  public partial class ZipEntry
  {
    private int _readExtraDepth;
    private void ReadExtraField()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1785);
      _readExtraDepth++;
      long posn = this.ArchiveStream.Position;
      this.ArchiveStream.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      byte[] block = new byte[30];
      this.ArchiveStream.Read(block, 0, block.Length);
      int i = 26;
      Int16 filenameLength = (short)(block[i++] + block[i++] * 256);
      Int16 extraFieldLength = (short)(block[i++] + block[i++] * 256);
      this.ArchiveStream.Seek(filenameLength, SeekOrigin.Current);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      ProcessExtraField(this.ArchiveStream, extraFieldLength);
      this.ArchiveStream.Seek(posn, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      _readExtraDepth--;
      IACSharpSensor.IACSharpSensor.SensorReached(1786);
    }
    private static bool ReadHeader(ZipEntry ze, System.Text.Encoding defaultEncoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1787);
      int bytesRead = 0;
      ze._RelativeOffsetOfLocalHeader = ze.ArchiveStream.Position;
      int signature = Ionic.Zip.SharedUtilities.ReadEntrySignature(ze.ArchiveStream);
      bytesRead += 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1788);
      if (ZipEntry.IsNotValidSig(signature)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1789);
        ze.ArchiveStream.Seek(-4, SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(ze.ArchiveStream);
        IACSharpSensor.IACSharpSensor.SensorReached(1790);
        if (ZipEntry.IsNotValidZipDirEntrySig(signature) && (signature != ZipConstants.EndOfCentralDirectorySignature)) {
          IACSharpSensor.IACSharpSensor.SensorReached(1791);
          throw new BadReadException(String.Format("  Bad signature (0x{0:X8}) at position  0x{1:X8}", signature, ze.ArchiveStream.Position));
        }
        System.Boolean RNTRNTRNT_296 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1792);
        return RNTRNTRNT_296;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1793);
      byte[] block = new byte[26];
      int n = ze.ArchiveStream.Read(block, 0, block.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(1794);
      if (n != block.Length) {
        System.Boolean RNTRNTRNT_297 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1795);
        return RNTRNTRNT_297;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1796);
      bytesRead += n;
      int i = 0;
      ze._VersionNeeded = (Int16)(block[i++] + block[i++] * 256);
      ze._BitField = (Int16)(block[i++] + block[i++] * 256);
      ze._CompressionMethod_FromZipFile = ze._CompressionMethod = (Int16)(block[i++] + block[i++] * 256);
      ze._TimeBlob = block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256;
      ze._LastModified = Ionic.Zip.SharedUtilities.PackedToDateTime(ze._TimeBlob);
      ze._timestamp |= ZipEntryTimestamp.DOS;
      IACSharpSensor.IACSharpSensor.SensorReached(1797);
      if ((ze._BitField & 0x1) == 0x1) {
        IACSharpSensor.IACSharpSensor.SensorReached(1798);
        ze._Encryption_FromZipFile = ze._Encryption = EncryptionAlgorithm.PkzipWeak;
        ze._sourceIsEncrypted = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1802);
      {
        IACSharpSensor.IACSharpSensor.SensorReached(1799);
        ze._Crc32 = (Int32)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
        ze._CompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
        ze._UncompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
        IACSharpSensor.IACSharpSensor.SensorReached(1800);
        if ((uint)ze._CompressedSize == 0xffffffffu || (uint)ze._UncompressedSize == 0xffffffffu) {
          IACSharpSensor.IACSharpSensor.SensorReached(1801);
          ze._InputUsesZip64 = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1803);
      Int16 filenameLength = (short)(block[i++] + block[i++] * 256);
      Int16 extraFieldLength = (short)(block[i++] + block[i++] * 256);
      block = new byte[filenameLength];
      n = ze.ArchiveStream.Read(block, 0, block.Length);
      bytesRead += n;
      IACSharpSensor.IACSharpSensor.SensorReached(1804);
      if ((ze._BitField & 0x800) == 0x800) {
        IACSharpSensor.IACSharpSensor.SensorReached(1805);
        ze.AlternateEncoding = System.Text.Encoding.UTF8;
        ze.AlternateEncodingUsage = ZipOption.Always;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1806);
      ze._FileNameInArchive = ze.AlternateEncoding.GetString(block, 0, block.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(1807);
      if (ze._FileNameInArchive.EndsWith("/")) {
        IACSharpSensor.IACSharpSensor.SensorReached(1808);
        ze.MarkAsDirectory();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1809);
      bytesRead += ze.ProcessExtraField(ze.ArchiveStream, extraFieldLength);
      ze._LengthOfTrailer = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1810);
      if (!ze._FileNameInArchive.EndsWith("/") && (ze._BitField & 0x8) == 0x8) {
        IACSharpSensor.IACSharpSensor.SensorReached(1811);
        long posn = ze.ArchiveStream.Position;
        bool wantMore = true;
        long SizeOfDataRead = 0;
        int tries = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(1812);
        while (wantMore) {
          IACSharpSensor.IACSharpSensor.SensorReached(1813);
          tries++;
          IACSharpSensor.IACSharpSensor.SensorReached(1814);
          if (ze._container.ZipFile != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(1815);
            ze._container.ZipFile.OnReadBytes(ze);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1816);
          long d = Ionic.Zip.SharedUtilities.FindSignature(ze.ArchiveStream, ZipConstants.ZipEntryDataDescriptorSignature);
          IACSharpSensor.IACSharpSensor.SensorReached(1817);
          if (d == -1) {
            System.Boolean RNTRNTRNT_298 = false;
            IACSharpSensor.IACSharpSensor.SensorReached(1818);
            return RNTRNTRNT_298;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1819);
          SizeOfDataRead += d;
          IACSharpSensor.IACSharpSensor.SensorReached(1820);
          if (ze._InputUsesZip64) {
            IACSharpSensor.IACSharpSensor.SensorReached(1821);
            block = new byte[20];
            n = ze.ArchiveStream.Read(block, 0, block.Length);
            IACSharpSensor.IACSharpSensor.SensorReached(1822);
            if (n != 20) {
              System.Boolean RNTRNTRNT_299 = false;
              IACSharpSensor.IACSharpSensor.SensorReached(1823);
              return RNTRNTRNT_299;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1824);
            i = 0;
            ze._Crc32 = (Int32)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
            ze._CompressedSize = BitConverter.ToInt64(block, i);
            i += 8;
            ze._UncompressedSize = BitConverter.ToInt64(block, i);
            i += 8;
            ze._LengthOfTrailer += 24;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1825);
            block = new byte[12];
            n = ze.ArchiveStream.Read(block, 0, block.Length);
            IACSharpSensor.IACSharpSensor.SensorReached(1826);
            if (n != 12) {
              System.Boolean RNTRNTRNT_300 = false;
              IACSharpSensor.IACSharpSensor.SensorReached(1827);
              return RNTRNTRNT_300;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1828);
            i = 0;
            ze._Crc32 = (Int32)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
            ze._CompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
            ze._UncompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
            ze._LengthOfTrailer += 16;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1829);
          wantMore = (SizeOfDataRead != ze._CompressedSize);
          IACSharpSensor.IACSharpSensor.SensorReached(1830);
          if (wantMore) {
            IACSharpSensor.IACSharpSensor.SensorReached(1831);
            ze.ArchiveStream.Seek(-12, SeekOrigin.Current);
            Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(ze.ArchiveStream);
            SizeOfDataRead += 4;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1832);
        ze.ArchiveStream.Seek(posn, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(ze.ArchiveStream);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1833);
      ze._CompressedFileDataSize = ze._CompressedSize;
      IACSharpSensor.IACSharpSensor.SensorReached(1834);
      if ((ze._BitField & 0x1) == 0x1) {
        IACSharpSensor.IACSharpSensor.SensorReached(1835);
        if (ze.Encryption == EncryptionAlgorithm.WinZipAes128 || ze.Encryption == EncryptionAlgorithm.WinZipAes256) {
          IACSharpSensor.IACSharpSensor.SensorReached(1836);
          int bits = ZipEntry.GetKeyStrengthInBits(ze._Encryption_FromZipFile);
          ze._aesCrypto_forExtract = WinZipAesCrypto.ReadFromStream(null, bits, ze.ArchiveStream);
          bytesRead += ze._aesCrypto_forExtract.SizeOfEncryptionMetadata - 10;
          ze._CompressedFileDataSize -= ze._aesCrypto_forExtract.SizeOfEncryptionMetadata;
          ze._LengthOfTrailer += 10;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1837);
          ze._WeakEncryptionHeader = new byte[12];
          bytesRead += ZipEntry.ReadWeakEncryptionHeader(ze._archiveStream, ze._WeakEncryptionHeader);
          ze._CompressedFileDataSize -= 12;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1838);
      ze._LengthOfHeader = bytesRead;
      ze._TotalEntrySize = ze._LengthOfHeader + ze._CompressedFileDataSize + ze._LengthOfTrailer;
      System.Boolean RNTRNTRNT_301 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1839);
      return RNTRNTRNT_301;
    }
    static internal int ReadWeakEncryptionHeader(Stream s, byte[] buffer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1840);
      int additionalBytesRead = s.Read(buffer, 0, 12);
      IACSharpSensor.IACSharpSensor.SensorReached(1841);
      if (additionalBytesRead != 12) {
        IACSharpSensor.IACSharpSensor.SensorReached(1842);
        throw new ZipException(String.Format("Unexpected end of data at position 0x{0:X8}", s.Position));
      }
      System.Int32 RNTRNTRNT_302 = additionalBytesRead;
      IACSharpSensor.IACSharpSensor.SensorReached(1843);
      return RNTRNTRNT_302;
    }
    private static bool IsNotValidSig(int signature)
    {
      System.Boolean RNTRNTRNT_303 = (signature != ZipConstants.ZipEntrySignature);
      IACSharpSensor.IACSharpSensor.SensorReached(1844);
      return RNTRNTRNT_303;
    }
    static internal ZipEntry ReadEntry(ZipContainer zc, bool first)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1845);
      ZipFile zf = zc.ZipFile;
      Stream s = zc.ReadStream;
      System.Text.Encoding defaultEncoding = zc.AlternateEncoding;
      ZipEntry entry = new ZipEntry();
      entry._Source = ZipEntrySource.ZipFile;
      entry._container = zc;
      entry._archiveStream = s;
      IACSharpSensor.IACSharpSensor.SensorReached(1846);
      if (zf != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1847);
        zf.OnReadEntry(true, null);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1848);
      if (first) {
        IACSharpSensor.IACSharpSensor.SensorReached(1849);
        HandlePK00Prefix(s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1850);
      if (!ReadHeader(entry, defaultEncoding)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1851);
        return null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1852);
      entry.__FileDataPosition = entry.ArchiveStream.Position;
      s.Seek(entry._CompressedFileDataSize + entry._LengthOfTrailer, SeekOrigin.Current);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
      HandleUnexpectedDataDescriptor(entry);
      IACSharpSensor.IACSharpSensor.SensorReached(1853);
      if (zf != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1854);
        zf.OnReadBytes(entry);
        zf.OnReadEntry(false, entry);
      }
      ZipEntry RNTRNTRNT_304 = entry;
      IACSharpSensor.IACSharpSensor.SensorReached(1855);
      return RNTRNTRNT_304;
    }
    static internal void HandlePK00Prefix(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1856);
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      IACSharpSensor.IACSharpSensor.SensorReached(1857);
      if (datum != ZipConstants.PackedToRemovableMedia) {
        IACSharpSensor.IACSharpSensor.SensorReached(1858);
        s.Seek(-4, SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1859);
    }
    private static void HandleUnexpectedDataDescriptor(ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1860);
      Stream s = entry.ArchiveStream;
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      IACSharpSensor.IACSharpSensor.SensorReached(1861);
      if (datum == entry._Crc32) {
        IACSharpSensor.IACSharpSensor.SensorReached(1862);
        int sz = Ionic.Zip.SharedUtilities.ReadInt(s);
        IACSharpSensor.IACSharpSensor.SensorReached(1863);
        if (sz == entry._CompressedSize) {
          IACSharpSensor.IACSharpSensor.SensorReached(1864);
          sz = Ionic.Zip.SharedUtilities.ReadInt(s);
          IACSharpSensor.IACSharpSensor.SensorReached(1865);
          if (sz == entry._UncompressedSize) {
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1866);
            s.Seek(-12, SeekOrigin.Current);
            Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1867);
          s.Seek(-8, SeekOrigin.Current);
          Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(1868);
        s.Seek(-4, SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1869);
    }
    static internal int FindExtraFieldSegment(byte[] extra, int offx, UInt16 targetHeaderId)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1870);
      int j = offx;
      IACSharpSensor.IACSharpSensor.SensorReached(1871);
      while (j + 3 < extra.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(1872);
        UInt16 headerId = (UInt16)(extra[j++] + extra[j++] * 256);
        IACSharpSensor.IACSharpSensor.SensorReached(1873);
        if (headerId == targetHeaderId) {
          System.Int32 RNTRNTRNT_305 = j - 2;
          IACSharpSensor.IACSharpSensor.SensorReached(1874);
          return RNTRNTRNT_305;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1875);
        Int16 dataSize = (short)(extra[j++] + extra[j++] * 256);
        j += dataSize;
      }
      System.Int32 RNTRNTRNT_306 = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(1876);
      return RNTRNTRNT_306;
    }
    internal int ProcessExtraField(Stream s, Int16 extraFieldLength)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1877);
      int additionalBytesRead = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1878);
      if (extraFieldLength > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1879);
        byte[] buffer = this._Extra = new byte[extraFieldLength];
        additionalBytesRead = s.Read(buffer, 0, buffer.Length);
        long posn = s.Position - additionalBytesRead;
        int j = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(1880);
        while (j + 3 < buffer.Length) {
          IACSharpSensor.IACSharpSensor.SensorReached(1881);
          int start = j;
          UInt16 headerId = (UInt16)(buffer[j++] + buffer[j++] * 256);
          Int16 dataSize = (short)(buffer[j++] + buffer[j++] * 256);
          IACSharpSensor.IACSharpSensor.SensorReached(1882);
          switch (headerId) {
            case 0xa:
              IACSharpSensor.IACSharpSensor.SensorReached(1883);
              j = ProcessExtraFieldWindowsTimes(buffer, j, dataSize, posn);
              IACSharpSensor.IACSharpSensor.SensorReached(1884);
              break;
            case 0x5455:
              IACSharpSensor.IACSharpSensor.SensorReached(1885);
              j = ProcessExtraFieldUnixTimes(buffer, j, dataSize, posn);
              IACSharpSensor.IACSharpSensor.SensorReached(1886);
              break;
            case 0x5855:
              IACSharpSensor.IACSharpSensor.SensorReached(1887);
              j = ProcessExtraFieldInfoZipTimes(buffer, j, dataSize, posn);
              IACSharpSensor.IACSharpSensor.SensorReached(1888);
              break;
            case 0x7855:
              IACSharpSensor.IACSharpSensor.SensorReached(1889);
              break;
            case 0x7875:
              IACSharpSensor.IACSharpSensor.SensorReached(1890);
              break;
            case 0x1:
              IACSharpSensor.IACSharpSensor.SensorReached(1891);
              j = ProcessExtraFieldZip64(buffer, j, dataSize, posn);
              IACSharpSensor.IACSharpSensor.SensorReached(1892);
              break;
            case 0x9901:
              IACSharpSensor.IACSharpSensor.SensorReached(1893);
              j = ProcessExtraFieldWinZipAes(buffer, j, dataSize, posn);
              IACSharpSensor.IACSharpSensor.SensorReached(1894);
              break;
            case 0x17:
              IACSharpSensor.IACSharpSensor.SensorReached(1895);
              j = ProcessExtraFieldPkwareStrongEncryption(buffer, j);
              IACSharpSensor.IACSharpSensor.SensorReached(1896);
              break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1897);
          j = start + dataSize + 4;
        }
      }
      System.Int32 RNTRNTRNT_307 = additionalBytesRead;
      IACSharpSensor.IACSharpSensor.SensorReached(1898);
      return RNTRNTRNT_307;
    }
    private int ProcessExtraFieldPkwareStrongEncryption(byte[] Buffer, int j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1899);
      j += 2;
      _UnsupportedAlgorithmId = (UInt16)(Buffer[j++] + Buffer[j++] * 256);
      _Encryption_FromZipFile = _Encryption = EncryptionAlgorithm.Unsupported;
      System.Int32 RNTRNTRNT_308 = j;
      IACSharpSensor.IACSharpSensor.SensorReached(1900);
      return RNTRNTRNT_308;
    }
    private int ProcessExtraFieldWinZipAes(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1901);
      if (this._CompressionMethod == 0x63) {
        IACSharpSensor.IACSharpSensor.SensorReached(1902);
        if ((this._BitField & 0x1) != 0x1) {
          IACSharpSensor.IACSharpSensor.SensorReached(1903);
          throw new BadReadException(String.Format("  Inconsistent metadata at position 0x{0:X16}", posn));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1904);
        this._sourceIsEncrypted = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1905);
        if (dataSize != 7) {
          IACSharpSensor.IACSharpSensor.SensorReached(1906);
          throw new BadReadException(String.Format("  Inconsistent size (0x{0:X4}) in WinZip AES field at position 0x{1:X16}", dataSize, posn));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1907);
        this._WinZipAesMethod = BitConverter.ToInt16(buffer, j);
        j += 2;
        IACSharpSensor.IACSharpSensor.SensorReached(1908);
        if (this._WinZipAesMethod != 0x1 && this._WinZipAesMethod != 0x2) {
          IACSharpSensor.IACSharpSensor.SensorReached(1909);
          throw new BadReadException(String.Format("  Unexpected vendor version number (0x{0:X4}) for WinZip AES metadata at position 0x{1:X16}", this._WinZipAesMethod, posn));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1910);
        Int16 vendorId = BitConverter.ToInt16(buffer, j);
        j += 2;
        IACSharpSensor.IACSharpSensor.SensorReached(1911);
        if (vendorId != 0x4541) {
          IACSharpSensor.IACSharpSensor.SensorReached(1912);
          throw new BadReadException(String.Format("  Unexpected vendor ID (0x{0:X4}) for WinZip AES metadata at position 0x{1:X16}", vendorId, posn));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1913);
        int keystrength = (buffer[j] == 1) ? 128 : (buffer[j] == 3) ? 256 : -1;
        IACSharpSensor.IACSharpSensor.SensorReached(1914);
        if (keystrength < 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(1915);
          throw new BadReadException(String.Format("Invalid key strength ({0})", keystrength));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1916);
        _Encryption_FromZipFile = this._Encryption = (keystrength == 128) ? EncryptionAlgorithm.WinZipAes128 : EncryptionAlgorithm.WinZipAes256;
        j++;
        this._CompressionMethod_FromZipFile = this._CompressionMethod = BitConverter.ToInt16(buffer, j);
        j += 2;
      }
      System.Int32 RNTRNTRNT_309 = j;
      IACSharpSensor.IACSharpSensor.SensorReached(1917);
      return RNTRNTRNT_309;
    }
    private delegate T Func<T>();
    private int ProcessExtraFieldZip64(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1918);
      this._InputUsesZip64 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1919);
      if (dataSize > 28) {
        IACSharpSensor.IACSharpSensor.SensorReached(1920);
        throw new BadReadException(String.Format("  Inconsistent size (0x{0:X4}) for ZIP64 extra field at position 0x{1:X16}", dataSize, posn));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1921);
      int remainingData = dataSize;
      var slurp = new Func<Int64>(() =>
      {
        if (remainingData < 8)
          throw new BadReadException(String.Format("  Missing data for ZIP64 extra field, position 0x{0:X16}", posn));
        var x = BitConverter.ToInt64(buffer, j);
        j += 8;
        remainingData -= 8;
        return x;
      });
      IACSharpSensor.IACSharpSensor.SensorReached(1922);
      if (this._UncompressedSize == 0xffffffffu) {
        IACSharpSensor.IACSharpSensor.SensorReached(1923);
        this._UncompressedSize = slurp();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1924);
      if (this._CompressedSize == 0xffffffffu) {
        IACSharpSensor.IACSharpSensor.SensorReached(1925);
        this._CompressedSize = slurp();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1926);
      if (this._RelativeOffsetOfLocalHeader == 0xffffffffu) {
        IACSharpSensor.IACSharpSensor.SensorReached(1927);
        this._RelativeOffsetOfLocalHeader = slurp();
      }
      System.Int32 RNTRNTRNT_310 = j;
      IACSharpSensor.IACSharpSensor.SensorReached(1928);
      return RNTRNTRNT_310;
    }
    private int ProcessExtraFieldInfoZipTimes(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1929);
      if (dataSize != 12 && dataSize != 8) {
        IACSharpSensor.IACSharpSensor.SensorReached(1930);
        throw new BadReadException(String.Format("  Unexpected size (0x{0:X4}) for InfoZip v1 extra field at position 0x{1:X16}", dataSize, posn));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1931);
      Int32 timet = BitConverter.ToInt32(buffer, j);
      this._Mtime = _unixEpoch.AddSeconds(timet);
      j += 4;
      timet = BitConverter.ToInt32(buffer, j);
      this._Atime = _unixEpoch.AddSeconds(timet);
      j += 4;
      this._Ctime = DateTime.UtcNow;
      _ntfsTimesAreSet = true;
      _timestamp |= ZipEntryTimestamp.InfoZip1;
      System.Int32 RNTRNTRNT_311 = j;
      IACSharpSensor.IACSharpSensor.SensorReached(1932);
      return RNTRNTRNT_311;
    }
    private int ProcessExtraFieldUnixTimes(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1933);
      if (dataSize != 13 && dataSize != 9 && dataSize != 5) {
        IACSharpSensor.IACSharpSensor.SensorReached(1934);
        throw new BadReadException(String.Format("  Unexpected size (0x{0:X4}) for Extended Timestamp extra field at position 0x{1:X16}", dataSize, posn));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1935);
      int remainingData = dataSize;
      var slurp = new Func<DateTime>(() =>
      {
        Int32 timet = BitConverter.ToInt32(buffer, j);
        j += 4;
        remainingData -= 4;
        return _unixEpoch.AddSeconds(timet);
      });
      IACSharpSensor.IACSharpSensor.SensorReached(1936);
      if (dataSize == 13 || _readExtraDepth > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1937);
        byte flag = buffer[j++];
        remainingData--;
        IACSharpSensor.IACSharpSensor.SensorReached(1938);
        if ((flag & 0x1) != 0 && remainingData >= 4) {
          IACSharpSensor.IACSharpSensor.SensorReached(1939);
          this._Mtime = slurp();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1940);
        this._Atime = ((flag & 0x2) != 0 && remainingData >= 4) ? slurp() : DateTime.UtcNow;
        this._Ctime = ((flag & 0x4) != 0 && remainingData >= 4) ? slurp() : DateTime.UtcNow;
        _timestamp |= ZipEntryTimestamp.Unix;
        _ntfsTimesAreSet = true;
        _emitUnixTimes = true;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(1941);
        ReadExtraField();
      }
      System.Int32 RNTRNTRNT_312 = j;
      IACSharpSensor.IACSharpSensor.SensorReached(1942);
      return RNTRNTRNT_312;
    }
    private int ProcessExtraFieldWindowsTimes(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1943);
      if (dataSize != 32) {
        IACSharpSensor.IACSharpSensor.SensorReached(1944);
        throw new BadReadException(String.Format("  Unexpected size (0x{0:X4}) for NTFS times extra field at position 0x{1:X16}", dataSize, posn));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1945);
      j += 4;
      Int16 timetag = (Int16)(buffer[j] + buffer[j + 1] * 256);
      Int16 addlsize = (Int16)(buffer[j + 2] + buffer[j + 3] * 256);
      j += 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1946);
      if (timetag == 0x1 && addlsize == 24) {
        IACSharpSensor.IACSharpSensor.SensorReached(1947);
        Int64 z = BitConverter.ToInt64(buffer, j);
        this._Mtime = DateTime.FromFileTimeUtc(z);
        j += 8;
        z = BitConverter.ToInt64(buffer, j);
        this._Atime = DateTime.FromFileTimeUtc(z);
        j += 8;
        z = BitConverter.ToInt64(buffer, j);
        this._Ctime = DateTime.FromFileTimeUtc(z);
        j += 8;
        _ntfsTimesAreSet = true;
        _timestamp |= ZipEntryTimestamp.Windows;
        _emitNtfsTimes = true;
      }
      System.Int32 RNTRNTRNT_313 = j;
      IACSharpSensor.IACSharpSensor.SensorReached(1948);
      return RNTRNTRNT_313;
    }
  }
}
