using System;
using System.Collections.Generic;
using System.IO;
namespace Ionic.Zip
{
  internal class ZipSegmentedStream : System.IO.Stream
  {
    enum RwMode
    {
      None = 0,
      ReadOnly = 1,
      Write = 2
    }
    private RwMode rwMode;
    private bool _exceptionPending;
    private string _baseName;
    private string _baseDir;
    private string _currentName;
    private string _currentTempName;
    private uint _currentDiskNumber;
    private uint _maxDiskNumber;
    private int _maxSegmentSize;
    private System.IO.Stream _innerStream;
    private ZipSegmentedStream() : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(279);
      _exceptionPending = false;
      IACSharpSensor.IACSharpSensor.SensorReached(280);
    }
    public static ZipSegmentedStream ForReading(string name, uint initialDiskNumber, uint maxDiskNumber)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(281);
      ZipSegmentedStream zss = new ZipSegmentedStream {
        rwMode = RwMode.ReadOnly,
        CurrentSegment = initialDiskNumber,
        _maxDiskNumber = maxDiskNumber,
        _baseName = name
      };
      zss._SetReadStream();
      ZipSegmentedStream RNTRNTRNT_68 = zss;
      IACSharpSensor.IACSharpSensor.SensorReached(282);
      return RNTRNTRNT_68;
    }
    public static ZipSegmentedStream ForWriting(string name, int maxSegmentSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(283);
      ZipSegmentedStream zss = new ZipSegmentedStream {
        rwMode = RwMode.Write,
        CurrentSegment = 0,
        _baseName = name,
        _maxSegmentSize = maxSegmentSize,
        _baseDir = Path.GetDirectoryName(name)
      };
      IACSharpSensor.IACSharpSensor.SensorReached(284);
      if (zss._baseDir == "") {
        IACSharpSensor.IACSharpSensor.SensorReached(285);
        zss._baseDir = ".";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(286);
      zss._SetWriteStream(0);
      ZipSegmentedStream RNTRNTRNT_69 = zss;
      IACSharpSensor.IACSharpSensor.SensorReached(287);
      return RNTRNTRNT_69;
    }
    public static Stream ForUpdate(string name, uint diskNumber)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(288);
      if (diskNumber >= 99) {
        IACSharpSensor.IACSharpSensor.SensorReached(289);
        throw new ArgumentOutOfRangeException("diskNumber");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(290);
      string fname = String.Format("{0}.z{1:D2}", Path.Combine(Path.GetDirectoryName(name), Path.GetFileNameWithoutExtension(name)), diskNumber + 1);
      Stream RNTRNTRNT_70 = File.Open(fname, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
      IACSharpSensor.IACSharpSensor.SensorReached(291);
      return RNTRNTRNT_70;
    }
    public bool ContiguousWrite { get; set; }
    public UInt32 CurrentSegment {
      get {
        UInt32 RNTRNTRNT_71 = _currentDiskNumber;
        IACSharpSensor.IACSharpSensor.SensorReached(292);
        return RNTRNTRNT_71;
      }
      private set {
        IACSharpSensor.IACSharpSensor.SensorReached(293);
        _currentDiskNumber = value;
        _currentName = null;
        IACSharpSensor.IACSharpSensor.SensorReached(294);
      }
    }
    public String CurrentName {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(295);
        if (_currentName == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(296);
          _currentName = _NameForSegment(CurrentSegment);
        }
        String RNTRNTRNT_72 = _currentName;
        IACSharpSensor.IACSharpSensor.SensorReached(297);
        return RNTRNTRNT_72;
      }
    }
    public String CurrentTempName {
      get {
        String RNTRNTRNT_73 = _currentTempName;
        IACSharpSensor.IACSharpSensor.SensorReached(298);
        return RNTRNTRNT_73;
      }
    }
    private string _NameForSegment(uint diskNumber)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(299);
      if (diskNumber >= 99) {
        IACSharpSensor.IACSharpSensor.SensorReached(300);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(301);
        throw new OverflowException("The number of zip segments would exceed 99.");
      }
      System.String RNTRNTRNT_74 = String.Format("{0}.z{1:D2}", Path.Combine(Path.GetDirectoryName(_baseName), Path.GetFileNameWithoutExtension(_baseName)), diskNumber + 1);
      IACSharpSensor.IACSharpSensor.SensorReached(302);
      return RNTRNTRNT_74;
    }
    public UInt32 ComputeSegment(int length)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(303);
      if (_innerStream.Position + length > _maxSegmentSize) {
        UInt32 RNTRNTRNT_75 = CurrentSegment + 1;
        IACSharpSensor.IACSharpSensor.SensorReached(304);
        return RNTRNTRNT_75;
      }
      UInt32 RNTRNTRNT_76 = CurrentSegment;
      IACSharpSensor.IACSharpSensor.SensorReached(305);
      return RNTRNTRNT_76;
    }
    public override String ToString()
    {
      String RNTRNTRNT_77 = String.Format("{0}[{1}][{2}], pos=0x{3:X})", "ZipSegmentedStream", CurrentName, rwMode.ToString(), this.Position);
      IACSharpSensor.IACSharpSensor.SensorReached(306);
      return RNTRNTRNT_77;
    }
    private void _SetReadStream()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(307);
      if (_innerStream != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(308);
        _innerStream.Dispose();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(309);
      if (CurrentSegment + 1 == _maxDiskNumber) {
        IACSharpSensor.IACSharpSensor.SensorReached(310);
        _currentName = _baseName;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(311);
      _innerStream = File.OpenRead(CurrentName);
      IACSharpSensor.IACSharpSensor.SensorReached(312);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(313);
      if (rwMode != RwMode.ReadOnly) {
        IACSharpSensor.IACSharpSensor.SensorReached(314);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(315);
        throw new InvalidOperationException("Stream Error: Cannot Read.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(316);
      int r = _innerStream.Read(buffer, offset, count);
      int r1 = r;
      IACSharpSensor.IACSharpSensor.SensorReached(317);
      while (r1 != count) {
        IACSharpSensor.IACSharpSensor.SensorReached(318);
        if (_innerStream.Position != _innerStream.Length) {
          IACSharpSensor.IACSharpSensor.SensorReached(319);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(320);
          throw new ZipException(String.Format("Read error in file {0}", CurrentName));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(321);
        if (CurrentSegment + 1 == _maxDiskNumber) {
          System.Int32 RNTRNTRNT_78 = r;
          IACSharpSensor.IACSharpSensor.SensorReached(322);
          return RNTRNTRNT_78;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(323);
        CurrentSegment++;
        _SetReadStream();
        offset += r1;
        count -= r1;
        r1 = _innerStream.Read(buffer, offset, count);
        r += r1;
      }
      System.Int32 RNTRNTRNT_79 = r;
      IACSharpSensor.IACSharpSensor.SensorReached(324);
      return RNTRNTRNT_79;
    }
    private void _SetWriteStream(uint increment)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(325);
      if (_innerStream != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(326);
        _innerStream.Dispose();
        IACSharpSensor.IACSharpSensor.SensorReached(327);
        if (File.Exists(CurrentName)) {
          IACSharpSensor.IACSharpSensor.SensorReached(328);
          File.Delete(CurrentName);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(329);
        File.Move(_currentTempName, CurrentName);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(330);
      if (increment > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(331);
        CurrentSegment += increment;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(332);
      SharedUtilities.CreateAndOpenUniqueTempFile(_baseDir, out _innerStream, out _currentTempName);
      IACSharpSensor.IACSharpSensor.SensorReached(333);
      if (CurrentSegment == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(334);
        _innerStream.Write(BitConverter.GetBytes(ZipConstants.SplitArchiveSignature), 0, 4);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(335);
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(336);
      if (rwMode != RwMode.Write) {
        IACSharpSensor.IACSharpSensor.SensorReached(337);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(338);
        throw new InvalidOperationException("Stream Error: Cannot Write.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(339);
      if (ContiguousWrite) {
        IACSharpSensor.IACSharpSensor.SensorReached(340);
        if (_innerStream.Position + count > _maxSegmentSize) {
          IACSharpSensor.IACSharpSensor.SensorReached(341);
          _SetWriteStream(1);
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(342);
        while (_innerStream.Position + count > _maxSegmentSize) {
          IACSharpSensor.IACSharpSensor.SensorReached(343);
          int c = unchecked(_maxSegmentSize - (int)_innerStream.Position);
          _innerStream.Write(buffer, offset, c);
          _SetWriteStream(1);
          count -= c;
          offset += c;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(344);
      _innerStream.Write(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(345);
    }
    public long TruncateBackward(uint diskNumber, long offset)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(346);
      if (diskNumber >= 99) {
        IACSharpSensor.IACSharpSensor.SensorReached(347);
        throw new ArgumentOutOfRangeException("diskNumber");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(348);
      if (rwMode != RwMode.Write) {
        IACSharpSensor.IACSharpSensor.SensorReached(349);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(350);
        throw new ZipException("bad state.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(351);
      if (diskNumber == CurrentSegment) {
        IACSharpSensor.IACSharpSensor.SensorReached(352);
        var x = _innerStream.Seek(offset, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_innerStream);
        System.Int64 RNTRNTRNT_80 = x;
        IACSharpSensor.IACSharpSensor.SensorReached(353);
        return RNTRNTRNT_80;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(354);
      if (_innerStream != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(355);
        _innerStream.Dispose();
        IACSharpSensor.IACSharpSensor.SensorReached(356);
        if (File.Exists(_currentTempName)) {
          IACSharpSensor.IACSharpSensor.SensorReached(357);
          File.Delete(_currentTempName);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(358);
      for (uint j = CurrentSegment - 1; j > diskNumber; j--) {
        IACSharpSensor.IACSharpSensor.SensorReached(359);
        string s = _NameForSegment(j);
        IACSharpSensor.IACSharpSensor.SensorReached(360);
        if (File.Exists(s)) {
          IACSharpSensor.IACSharpSensor.SensorReached(361);
          File.Delete(s);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(362);
      CurrentSegment = diskNumber;
      IACSharpSensor.IACSharpSensor.SensorReached(363);
      for (int i = 0; i < 3; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(364);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(365);
          _currentTempName = SharedUtilities.InternalGetTempFileName();
          File.Move(CurrentName, _currentTempName);
          IACSharpSensor.IACSharpSensor.SensorReached(366);
          break;
        } catch (IOException) {
          IACSharpSensor.IACSharpSensor.SensorReached(367);
          if (i == 2) {
            IACSharpSensor.IACSharpSensor.SensorReached(368);
            throw;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(369);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(370);
      _innerStream = new FileStream(_currentTempName, FileMode.Open);
      var r = _innerStream.Seek(offset, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_innerStream);
      System.Int64 RNTRNTRNT_81 = r;
      IACSharpSensor.IACSharpSensor.SensorReached(371);
      return RNTRNTRNT_81;
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_82 = (rwMode == RwMode.ReadOnly && (_innerStream != null) && _innerStream.CanRead);
        IACSharpSensor.IACSharpSensor.SensorReached(372);
        return RNTRNTRNT_82;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_83 = (_innerStream != null) && _innerStream.CanSeek;
        IACSharpSensor.IACSharpSensor.SensorReached(373);
        return RNTRNTRNT_83;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_84 = (rwMode == RwMode.Write) && (_innerStream != null) && _innerStream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(374);
        return RNTRNTRNT_84;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(375);
      _innerStream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(376);
    }
    public override long Length {
      get {
        System.Int64 RNTRNTRNT_85 = _innerStream.Length;
        IACSharpSensor.IACSharpSensor.SensorReached(377);
        return RNTRNTRNT_85;
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_86 = _innerStream.Position;
        IACSharpSensor.IACSharpSensor.SensorReached(378);
        return RNTRNTRNT_86;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(379);
        _innerStream.Position = value;
        IACSharpSensor.IACSharpSensor.SensorReached(380);
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(381);
      var x = _innerStream.Seek(offset, origin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_innerStream);
      System.Int64 RNTRNTRNT_87 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(382);
      return RNTRNTRNT_87;
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(383);
      if (rwMode != RwMode.Write) {
        IACSharpSensor.IACSharpSensor.SensorReached(384);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(385);
        throw new InvalidOperationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(386);
      _innerStream.SetLength(value);
      IACSharpSensor.IACSharpSensor.SensorReached(387);
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(388);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(389);
        if (_innerStream != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(390);
          _innerStream.Dispose();
          IACSharpSensor.IACSharpSensor.SensorReached(391);
          if (rwMode == RwMode.Write) {
            IACSharpSensor.IACSharpSensor.SensorReached(392);
            if (_exceptionPending) {
            } else {
            }
          }
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(393);
        base.Dispose(disposing);
        IACSharpSensor.IACSharpSensor.SensorReached(394);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(395);
    }
  }
}
