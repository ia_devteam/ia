using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
namespace Ionic.Zip
{
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000B")]
  public class BadPasswordException : ZipException
  {
    public BadPasswordException()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(891);
    }
    public BadPasswordException(String message) : base(message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(892);
    }
    public BadPasswordException(String message, Exception innerException) : base(message, innerException)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(893);
    }
    protected BadPasswordException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(894);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000A")]
  public class BadReadException : ZipException
  {
    public BadReadException()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(895);
    }
    public BadReadException(String message) : base(message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(896);
    }
    public BadReadException(String message, Exception innerException) : base(message, innerException)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(897);
    }
    protected BadReadException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(898);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00009")]
  public class BadCrcException : ZipException
  {
    public BadCrcException()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(899);
    }
    public BadCrcException(String message) : base(message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(900);
    }
    protected BadCrcException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(901);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00008")]
  public class SfxGenerationException : ZipException
  {
    public SfxGenerationException()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(902);
    }
    public SfxGenerationException(String message) : base(message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(903);
    }
    protected SfxGenerationException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(904);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00007")]
  public class BadStateException : ZipException
  {
    public BadStateException()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(905);
    }
    public BadStateException(String message) : base(message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(906);
    }
    public BadStateException(String message, Exception innerException) : base(message, innerException)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(907);
    }
    protected BadStateException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(908);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00006")]
  public class ZipException : Exception
  {
    public ZipException()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(909);
    }
    public ZipException(String message) : base(message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(910);
    }
    public ZipException(String message, Exception innerException) : base(message, innerException)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(911);
    }
    protected ZipException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(912);
    }
  }
}
