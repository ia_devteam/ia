using System;
using System.IO;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Zip
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00004")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public partial class ZipEntry
  {
    public ZipEntry()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1311);
      _CompressionMethod = (Int16)CompressionMethod.Deflate;
      _CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
      _Encryption = EncryptionAlgorithm.None;
      _Source = ZipEntrySource.None;
      AlternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
      AlternateEncodingUsage = ZipOption.Never;
      IACSharpSensor.IACSharpSensor.SensorReached(1312);
    }
    public DateTime LastModified {
      get {
        DateTime RNTRNTRNT_227 = _LastModified.ToLocalTime();
        IACSharpSensor.IACSharpSensor.SensorReached(1313);
        return RNTRNTRNT_227;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1314);
        _LastModified = (value.Kind == DateTimeKind.Unspecified) ? DateTime.SpecifyKind(value, DateTimeKind.Local) : value.ToLocalTime();
        _Mtime = Ionic.Zip.SharedUtilities.AdjustTime_Reverse(_LastModified).ToUniversalTime();
        _metadataChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1315);
      }
    }
    private int BufferSize {
      get {
        System.Int32 RNTRNTRNT_228 = this._container.BufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(1316);
        return RNTRNTRNT_228;
      }
    }
    public DateTime ModifiedTime {
      get {
        DateTime RNTRNTRNT_229 = _Mtime;
        IACSharpSensor.IACSharpSensor.SensorReached(1317);
        return RNTRNTRNT_229;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1318);
        SetEntryTimes(_Ctime, _Atime, value);
        IACSharpSensor.IACSharpSensor.SensorReached(1319);
      }
    }
    public DateTime AccessedTime {
      get {
        DateTime RNTRNTRNT_230 = _Atime;
        IACSharpSensor.IACSharpSensor.SensorReached(1320);
        return RNTRNTRNT_230;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1321);
        SetEntryTimes(_Ctime, value, _Mtime);
        IACSharpSensor.IACSharpSensor.SensorReached(1322);
      }
    }
    public DateTime CreationTime {
      get {
        DateTime RNTRNTRNT_231 = _Ctime;
        IACSharpSensor.IACSharpSensor.SensorReached(1323);
        return RNTRNTRNT_231;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1324);
        SetEntryTimes(value, _Atime, _Mtime);
        IACSharpSensor.IACSharpSensor.SensorReached(1325);
      }
    }
    public void SetEntryTimes(DateTime created, DateTime accessed, DateTime modified)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1326);
      _ntfsTimesAreSet = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1327);
      if (created == _zeroHour && created.Kind == _zeroHour.Kind) {
        IACSharpSensor.IACSharpSensor.SensorReached(1328);
        created = _win32Epoch;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1329);
      if (accessed == _zeroHour && accessed.Kind == _zeroHour.Kind) {
        IACSharpSensor.IACSharpSensor.SensorReached(1330);
        accessed = _win32Epoch;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1331);
      if (modified == _zeroHour && modified.Kind == _zeroHour.Kind) {
        IACSharpSensor.IACSharpSensor.SensorReached(1332);
        modified = _win32Epoch;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1333);
      _Ctime = created.ToUniversalTime();
      _Atime = accessed.ToUniversalTime();
      _Mtime = modified.ToUniversalTime();
      _LastModified = _Mtime;
      IACSharpSensor.IACSharpSensor.SensorReached(1334);
      if (!_emitUnixTimes && !_emitNtfsTimes) {
        IACSharpSensor.IACSharpSensor.SensorReached(1335);
        _emitNtfsTimes = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1336);
      _metadataChanged = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1337);
    }
    public bool EmitTimesInWindowsFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_232 = _emitNtfsTimes;
        IACSharpSensor.IACSharpSensor.SensorReached(1338);
        return RNTRNTRNT_232;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1339);
        _emitNtfsTimes = value;
        _metadataChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1340);
      }
    }
    public bool EmitTimesInUnixFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_233 = _emitUnixTimes;
        IACSharpSensor.IACSharpSensor.SensorReached(1341);
        return RNTRNTRNT_233;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1342);
        _emitUnixTimes = value;
        _metadataChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1343);
      }
    }
    public ZipEntryTimestamp Timestamp {
      get {
        ZipEntryTimestamp RNTRNTRNT_234 = _timestamp;
        IACSharpSensor.IACSharpSensor.SensorReached(1344);
        return RNTRNTRNT_234;
      }
    }
    public System.IO.FileAttributes Attributes {
      get {
        System.IO.FileAttributes RNTRNTRNT_235 = (System.IO.FileAttributes)_ExternalFileAttrs;
        IACSharpSensor.IACSharpSensor.SensorReached(1345);
        return RNTRNTRNT_235;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1346);
        _ExternalFileAttrs = (int)value;
        _VersionMadeBy = (0 << 8) + 45;
        _metadataChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1347);
      }
    }
    internal string LocalFileName {
      get {
        System.String RNTRNTRNT_236 = _LocalFileName;
        IACSharpSensor.IACSharpSensor.SensorReached(1348);
        return RNTRNTRNT_236;
      }
    }
    public string FileName {
      get {
        System.String RNTRNTRNT_237 = _FileNameInArchive;
        IACSharpSensor.IACSharpSensor.SensorReached(1349);
        return RNTRNTRNT_237;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1350);
        if (_container.ZipFile == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1351);
          throw new ZipException("Cannot rename; this is not supported in ZipOutputStream/ZipInputStream.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1352);
        if (String.IsNullOrEmpty(value)) {
          IACSharpSensor.IACSharpSensor.SensorReached(1353);
          throw new ZipException("The FileName must be non empty and non-null.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1354);
        var filename = ZipEntry.NameInArchive(value, null);
        IACSharpSensor.IACSharpSensor.SensorReached(1355);
        if (_FileNameInArchive == filename) {
          IACSharpSensor.IACSharpSensor.SensorReached(1356);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1357);
        this._container.ZipFile.RemoveEntry(this);
        this._container.ZipFile.InternalAddEntry(filename, this);
        _FileNameInArchive = filename;
        _container.ZipFile.NotifyEntryChanged();
        _metadataChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1358);
      }
    }
    public Stream InputStream {
      get {
        Stream RNTRNTRNT_238 = _sourceStream;
        IACSharpSensor.IACSharpSensor.SensorReached(1359);
        return RNTRNTRNT_238;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1360);
        if (this._Source != ZipEntrySource.Stream) {
          IACSharpSensor.IACSharpSensor.SensorReached(1361);
          throw new ZipException("You must not set the input stream for this entry.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1362);
        _sourceWasJitProvided = true;
        _sourceStream = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1363);
      }
    }
    public bool InputStreamWasJitProvided {
      get {
        System.Boolean RNTRNTRNT_239 = _sourceWasJitProvided;
        IACSharpSensor.IACSharpSensor.SensorReached(1364);
        return RNTRNTRNT_239;
      }
    }
    public ZipEntrySource Source {
      get {
        ZipEntrySource RNTRNTRNT_240 = _Source;
        IACSharpSensor.IACSharpSensor.SensorReached(1365);
        return RNTRNTRNT_240;
      }
    }
    public Int16 VersionNeeded {
      get {
        Int16 RNTRNTRNT_241 = _VersionNeeded;
        IACSharpSensor.IACSharpSensor.SensorReached(1366);
        return RNTRNTRNT_241;
      }
    }
    public string Comment {
      get {
        System.String RNTRNTRNT_242 = _Comment;
        IACSharpSensor.IACSharpSensor.SensorReached(1367);
        return RNTRNTRNT_242;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1368);
        _Comment = value;
        _metadataChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1369);
      }
    }
    public Nullable<bool> RequiresZip64 {
      get {
        Nullable<System.Boolean> RNTRNTRNT_243 = _entryRequiresZip64;
        IACSharpSensor.IACSharpSensor.SensorReached(1370);
        return RNTRNTRNT_243;
      }
    }
    public Nullable<bool> OutputUsedZip64 {
      get {
        Nullable<System.Boolean> RNTRNTRNT_244 = _OutputUsesZip64;
        IACSharpSensor.IACSharpSensor.SensorReached(1371);
        return RNTRNTRNT_244;
      }
    }
    public Int16 BitField {
      get {
        Int16 RNTRNTRNT_245 = _BitField;
        IACSharpSensor.IACSharpSensor.SensorReached(1372);
        return RNTRNTRNT_245;
      }
    }
    public CompressionMethod CompressionMethod {
      get {
        CompressionMethod RNTRNTRNT_246 = (CompressionMethod)_CompressionMethod;
        IACSharpSensor.IACSharpSensor.SensorReached(1373);
        return RNTRNTRNT_246;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1374);
        if (value == (CompressionMethod)_CompressionMethod) {
          IACSharpSensor.IACSharpSensor.SensorReached(1375);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1376);
        if (value != CompressionMethod.None && value != CompressionMethod.Deflate && value != CompressionMethod.BZip2) {
          IACSharpSensor.IACSharpSensor.SensorReached(1377);
          throw new InvalidOperationException("Unsupported compression method.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1378);
        _CompressionMethod = (Int16)value;
        IACSharpSensor.IACSharpSensor.SensorReached(1379);
        if (_CompressionMethod == (Int16)Ionic.Zip.CompressionMethod.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(1380);
          _CompressionLevel = Ionic.Zlib.CompressionLevel.None;
        } else if (CompressionLevel == Ionic.Zlib.CompressionLevel.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(1381);
          _CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1382);
        if (_container.ZipFile != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1383);
          _container.ZipFile.NotifyEntryChanged();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1384);
        _restreamRequiredOnSave = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1385);
      }
    }
    public Ionic.Zlib.CompressionLevel CompressionLevel {
      get {
        Ionic.Zlib.CompressionLevel RNTRNTRNT_247 = _CompressionLevel;
        IACSharpSensor.IACSharpSensor.SensorReached(1386);
        return RNTRNTRNT_247;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1387);
        if (_CompressionMethod != (short)CompressionMethod.Deflate && _CompressionMethod != (short)CompressionMethod.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(1388);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1389);
        if (value == Ionic.Zlib.CompressionLevel.Default && _CompressionMethod == (short)CompressionMethod.Deflate) {
          IACSharpSensor.IACSharpSensor.SensorReached(1390);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1391);
        _CompressionLevel = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1392);
        if (value == Ionic.Zlib.CompressionLevel.None && _CompressionMethod == (short)CompressionMethod.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(1393);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1394);
        if (_CompressionLevel == Ionic.Zlib.CompressionLevel.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(1395);
          _CompressionMethod = (short)Ionic.Zip.CompressionMethod.None;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1396);
          _CompressionMethod = (short)Ionic.Zip.CompressionMethod.Deflate;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1397);
        if (_container.ZipFile != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1398);
          _container.ZipFile.NotifyEntryChanged();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1399);
        _restreamRequiredOnSave = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1400);
      }
    }
    public Int64 CompressedSize {
      get {
        Int64 RNTRNTRNT_248 = _CompressedSize;
        IACSharpSensor.IACSharpSensor.SensorReached(1401);
        return RNTRNTRNT_248;
      }
    }
    public Int64 UncompressedSize {
      get {
        Int64 RNTRNTRNT_249 = _UncompressedSize;
        IACSharpSensor.IACSharpSensor.SensorReached(1402);
        return RNTRNTRNT_249;
      }
    }
    public Double CompressionRatio {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1403);
        if (UncompressedSize == 0) {
          Double RNTRNTRNT_250 = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(1404);
          return RNTRNTRNT_250;
        }
        Double RNTRNTRNT_251 = 100 * (1.0 - (1.0 * CompressedSize) / (1.0 * UncompressedSize));
        IACSharpSensor.IACSharpSensor.SensorReached(1405);
        return RNTRNTRNT_251;
      }
    }
    public Int32 Crc {
      get {
        Int32 RNTRNTRNT_252 = _Crc32;
        IACSharpSensor.IACSharpSensor.SensorReached(1406);
        return RNTRNTRNT_252;
      }
    }
    public bool IsDirectory {
      get {
        System.Boolean RNTRNTRNT_253 = _IsDirectory;
        IACSharpSensor.IACSharpSensor.SensorReached(1407);
        return RNTRNTRNT_253;
      }
    }
    public bool UsesEncryption {
      get {
        System.Boolean RNTRNTRNT_254 = (_Encryption_FromZipFile != EncryptionAlgorithm.None);
        IACSharpSensor.IACSharpSensor.SensorReached(1408);
        return RNTRNTRNT_254;
      }
    }
    public EncryptionAlgorithm Encryption {
      get {
        EncryptionAlgorithm RNTRNTRNT_255 = _Encryption;
        IACSharpSensor.IACSharpSensor.SensorReached(1409);
        return RNTRNTRNT_255;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1410);
        if (value == _Encryption) {
          IACSharpSensor.IACSharpSensor.SensorReached(1411);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1412);
        if (value == EncryptionAlgorithm.Unsupported) {
          IACSharpSensor.IACSharpSensor.SensorReached(1413);
          throw new InvalidOperationException("You may not set Encryption to that value.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1414);
        _Encryption = value;
        _restreamRequiredOnSave = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1415);
        if (_container.ZipFile != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1416);
          _container.ZipFile.NotifyEntryChanged();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1417);
      }
    }
    public string Password {
      private get {
        System.String RNTRNTRNT_256 = _Password;
        IACSharpSensor.IACSharpSensor.SensorReached(1418);
        return RNTRNTRNT_256;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1419);
        _Password = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1420);
        if (_Password == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1421);
          _Encryption = EncryptionAlgorithm.None;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1422);
          if (this._Source == ZipEntrySource.ZipFile && !_sourceIsEncrypted) {
            IACSharpSensor.IACSharpSensor.SensorReached(1423);
            _restreamRequiredOnSave = true;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1424);
          if (Encryption == EncryptionAlgorithm.None) {
            IACSharpSensor.IACSharpSensor.SensorReached(1425);
            _Encryption = EncryptionAlgorithm.PkzipWeak;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1426);
      }
    }
    internal bool IsChanged {
      get {
        System.Boolean RNTRNTRNT_257 = _restreamRequiredOnSave | _metadataChanged;
        IACSharpSensor.IACSharpSensor.SensorReached(1427);
        return RNTRNTRNT_257;
      }
    }
    public ExtractExistingFileAction ExtractExistingFile { get; set; }
    public ZipErrorAction ZipErrorAction { get; set; }
    public bool IncludedInMostRecentSave {
      get {
        System.Boolean RNTRNTRNT_258 = !_skippedDuringSave;
        IACSharpSensor.IACSharpSensor.SensorReached(1428);
        return RNTRNTRNT_258;
      }
    }
    public SetCompressionCallback SetCompression { get; set; }
    [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete.  It will be removed in a future version of the library. Your applications should  use AlternateEncoding and AlternateEncodingUsage instead.")]
    public bool UseUnicodeAsNecessary {
      get {
        System.Boolean RNTRNTRNT_259 = (AlternateEncoding == System.Text.Encoding.GetEncoding("UTF-8")) && (AlternateEncodingUsage == ZipOption.AsNecessary);
        IACSharpSensor.IACSharpSensor.SensorReached(1429);
        return RNTRNTRNT_259;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1430);
        if (value) {
          IACSharpSensor.IACSharpSensor.SensorReached(1431);
          AlternateEncoding = System.Text.Encoding.GetEncoding("UTF-8");
          AlternateEncodingUsage = ZipOption.AsNecessary;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1432);
          AlternateEncoding = Ionic.Zip.ZipFile.DefaultEncoding;
          AlternateEncodingUsage = ZipOption.Never;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1433);
      }
    }
    [Obsolete("This property is obsolete since v1.9.1.6. Use AlternateEncoding and AlternateEncodingUsage instead.", true)]
    public System.Text.Encoding ProvisionalAlternateEncoding { get; set; }
    public System.Text.Encoding AlternateEncoding { get; set; }
    public ZipOption AlternateEncodingUsage { get; set; }
    static internal string NameInArchive(String filename, string directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1434);
      string result = null;
      IACSharpSensor.IACSharpSensor.SensorReached(1435);
      if (directoryPathInArchive == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1436);
        result = filename;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(1437);
        if (String.IsNullOrEmpty(directoryPathInArchive)) {
          IACSharpSensor.IACSharpSensor.SensorReached(1438);
          result = Path.GetFileName(filename);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1439);
          result = Path.Combine(directoryPathInArchive, Path.GetFileName(filename));
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1440);
      result = SharedUtilities.NormalizePathForUseInZipFile(result);
      System.String RNTRNTRNT_260 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(1441);
      return RNTRNTRNT_260;
    }
    static internal ZipEntry CreateFromNothing(String nameInArchive)
    {
      ZipEntry RNTRNTRNT_261 = Create(nameInArchive, ZipEntrySource.None, null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(1442);
      return RNTRNTRNT_261;
    }
    static internal ZipEntry CreateFromFile(String filename, string nameInArchive)
    {
      ZipEntry RNTRNTRNT_262 = Create(nameInArchive, ZipEntrySource.FileSystem, filename, null);
      IACSharpSensor.IACSharpSensor.SensorReached(1443);
      return RNTRNTRNT_262;
    }
    static internal ZipEntry CreateForStream(String entryName, Stream s)
    {
      ZipEntry RNTRNTRNT_263 = Create(entryName, ZipEntrySource.Stream, s, null);
      IACSharpSensor.IACSharpSensor.SensorReached(1444);
      return RNTRNTRNT_263;
    }
    static internal ZipEntry CreateForWriter(String entryName, WriteDelegate d)
    {
      ZipEntry RNTRNTRNT_264 = Create(entryName, ZipEntrySource.WriteDelegate, d, null);
      IACSharpSensor.IACSharpSensor.SensorReached(1445);
      return RNTRNTRNT_264;
    }
    static internal ZipEntry CreateForJitStreamProvider(string nameInArchive, OpenDelegate opener, CloseDelegate closer)
    {
      ZipEntry RNTRNTRNT_265 = Create(nameInArchive, ZipEntrySource.JitStream, opener, closer);
      IACSharpSensor.IACSharpSensor.SensorReached(1446);
      return RNTRNTRNT_265;
    }
    static internal ZipEntry CreateForZipOutputStream(string nameInArchive)
    {
      ZipEntry RNTRNTRNT_266 = Create(nameInArchive, ZipEntrySource.ZipOutputStream, null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(1447);
      return RNTRNTRNT_266;
    }
    private static ZipEntry Create(string nameInArchive, ZipEntrySource source, Object arg1, Object arg2)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1448);
      if (String.IsNullOrEmpty(nameInArchive)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1449);
        throw new Ionic.Zip.ZipException("The entry name must be non-null and non-empty.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1450);
      ZipEntry entry = new ZipEntry();
      entry._VersionMadeBy = (0 << 8) + 45;
      entry._Source = source;
      entry._Mtime = entry._Atime = entry._Ctime = DateTime.UtcNow;
      IACSharpSensor.IACSharpSensor.SensorReached(1451);
      if (source == ZipEntrySource.Stream) {
        IACSharpSensor.IACSharpSensor.SensorReached(1452);
        entry._sourceStream = (arg1 as Stream);
      } else if (source == ZipEntrySource.WriteDelegate) {
        IACSharpSensor.IACSharpSensor.SensorReached(1466);
        entry._WriteDelegate = (arg1 as WriteDelegate);
      } else if (source == ZipEntrySource.JitStream) {
        IACSharpSensor.IACSharpSensor.SensorReached(1465);
        entry._OpenDelegate = (arg1 as OpenDelegate);
        entry._CloseDelegate = (arg2 as CloseDelegate);
      } else if (source == ZipEntrySource.ZipOutputStream) {
      } else if (source == ZipEntrySource.None) {
        IACSharpSensor.IACSharpSensor.SensorReached(1464);
        entry._Source = ZipEntrySource.FileSystem;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(1453);
        String filename = (arg1 as String);
        IACSharpSensor.IACSharpSensor.SensorReached(1454);
        if (String.IsNullOrEmpty(filename)) {
          IACSharpSensor.IACSharpSensor.SensorReached(1455);
          throw new Ionic.Zip.ZipException("The filename must be non-null and non-empty.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1456);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(1457);
          entry._Mtime = File.GetLastWriteTime(filename).ToUniversalTime();
          entry._Ctime = File.GetCreationTime(filename).ToUniversalTime();
          entry._Atime = File.GetLastAccessTime(filename).ToUniversalTime();
          IACSharpSensor.IACSharpSensor.SensorReached(1458);
          if (File.Exists(filename) || Directory.Exists(filename)) {
            IACSharpSensor.IACSharpSensor.SensorReached(1459);
            entry._ExternalFileAttrs = (int)File.GetAttributes(filename);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1460);
          entry._ntfsTimesAreSet = true;
          entry._LocalFileName = Path.GetFullPath(filename);
        } catch (System.IO.PathTooLongException ptle) {
          IACSharpSensor.IACSharpSensor.SensorReached(1461);
          var msg = String.Format("The path is too long, filename={0}", filename);
          IACSharpSensor.IACSharpSensor.SensorReached(1462);
          throw new ZipException(msg, ptle);
          IACSharpSensor.IACSharpSensor.SensorReached(1463);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1467);
      entry._LastModified = entry._Mtime;
      entry._FileNameInArchive = SharedUtilities.NormalizePathForUseInZipFile(nameInArchive);
      ZipEntry RNTRNTRNT_267 = entry;
      IACSharpSensor.IACSharpSensor.SensorReached(1468);
      return RNTRNTRNT_267;
    }
    internal void MarkAsDirectory()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1469);
      _IsDirectory = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1470);
      if (!_FileNameInArchive.EndsWith("/")) {
        IACSharpSensor.IACSharpSensor.SensorReached(1471);
        _FileNameInArchive += "/";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1472);
    }
    public bool IsText {
      get {
        System.Boolean RNTRNTRNT_268 = _IsText;
        IACSharpSensor.IACSharpSensor.SensorReached(1473);
        return RNTRNTRNT_268;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1474);
        _IsText = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1475);
      }
    }
    public override String ToString()
    {
      String RNTRNTRNT_269 = String.Format("ZipEntry::{0}", FileName);
      IACSharpSensor.IACSharpSensor.SensorReached(1476);
      return RNTRNTRNT_269;
    }
    internal Stream ArchiveStream {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1477);
        if (_archiveStream == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1478);
          if (_container.ZipFile != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(1479);
            var zf = _container.ZipFile;
            zf.Reset(false);
            _archiveStream = zf.StreamForDiskNumber(_diskNumber);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1480);
            _archiveStream = _container.ZipOutputStream.OutputStream;
          }
        }
        Stream RNTRNTRNT_270 = _archiveStream;
        IACSharpSensor.IACSharpSensor.SensorReached(1481);
        return RNTRNTRNT_270;
      }
    }
    private void SetFdpLoh()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1482);
      long origPosition = this.ArchiveStream.Position;
      IACSharpSensor.IACSharpSensor.SensorReached(1483);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(1484);
        this.ArchiveStream.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      } catch (System.IO.IOException exc1) {
        IACSharpSensor.IACSharpSensor.SensorReached(1485);
        string description = String.Format("Exception seeking  entry({0}) offset(0x{1:X8}) len(0x{2:X8})", this.FileName, this._RelativeOffsetOfLocalHeader, this.ArchiveStream.Length);
        IACSharpSensor.IACSharpSensor.SensorReached(1486);
        throw new BadStateException(description, exc1);
        IACSharpSensor.IACSharpSensor.SensorReached(1487);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1488);
      byte[] block = new byte[30];
      this.ArchiveStream.Read(block, 0, block.Length);
      Int16 filenameLength = (short)(block[26] + block[27] * 256);
      Int16 extraFieldLength = (short)(block[28] + block[29] * 256);
      this.ArchiveStream.Seek(filenameLength + extraFieldLength, SeekOrigin.Current);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      this._LengthOfHeader = 30 + extraFieldLength + filenameLength + GetLengthOfCryptoHeaderBytes(_Encryption_FromZipFile);
      this.__FileDataPosition = _RelativeOffsetOfLocalHeader + _LengthOfHeader;
      this.ArchiveStream.Seek(origPosition, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      IACSharpSensor.IACSharpSensor.SensorReached(1489);
    }
    private static int GetKeyStrengthInBits(EncryptionAlgorithm a)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1490);
      if (a == EncryptionAlgorithm.WinZipAes256) {
        System.Int32 RNTRNTRNT_271 = 256;
        IACSharpSensor.IACSharpSensor.SensorReached(1491);
        return RNTRNTRNT_271;
      } else if (a == EncryptionAlgorithm.WinZipAes128) {
        System.Int32 RNTRNTRNT_272 = 128;
        IACSharpSensor.IACSharpSensor.SensorReached(1492);
        return RNTRNTRNT_272;
      }
      System.Int32 RNTRNTRNT_273 = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(1493);
      return RNTRNTRNT_273;
    }
    static internal int GetLengthOfCryptoHeaderBytes(EncryptionAlgorithm a)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1494);
      if (a == EncryptionAlgorithm.None) {
        System.Int32 RNTRNTRNT_274 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(1495);
        return RNTRNTRNT_274;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1496);
      if (a == EncryptionAlgorithm.WinZipAes128 || a == EncryptionAlgorithm.WinZipAes256) {
        IACSharpSensor.IACSharpSensor.SensorReached(1497);
        int KeyStrengthInBits = GetKeyStrengthInBits(a);
        int sizeOfSaltAndPv = ((KeyStrengthInBits / 8 / 2) + 2);
        System.Int32 RNTRNTRNT_275 = sizeOfSaltAndPv;
        IACSharpSensor.IACSharpSensor.SensorReached(1498);
        return RNTRNTRNT_275;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1499);
      if (a == EncryptionAlgorithm.PkzipWeak) {
        System.Int32 RNTRNTRNT_276 = 12;
        IACSharpSensor.IACSharpSensor.SensorReached(1500);
        return RNTRNTRNT_276;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1501);
      throw new ZipException("internal error");
    }
    internal long FileDataPosition {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1502);
        if (__FileDataPosition == -1) {
          IACSharpSensor.IACSharpSensor.SensorReached(1503);
          SetFdpLoh();
        }
        System.Int64 RNTRNTRNT_277 = __FileDataPosition;
        IACSharpSensor.IACSharpSensor.SensorReached(1504);
        return RNTRNTRNT_277;
      }
    }
    private int LengthOfHeader {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1505);
        if (_LengthOfHeader == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(1506);
          SetFdpLoh();
        }
        System.Int32 RNTRNTRNT_278 = _LengthOfHeader;
        IACSharpSensor.IACSharpSensor.SensorReached(1507);
        return RNTRNTRNT_278;
      }
    }
    private ZipCrypto _zipCrypto_forExtract;
    private ZipCrypto _zipCrypto_forWrite;
    private WinZipAesCrypto _aesCrypto_forExtract;
    private WinZipAesCrypto _aesCrypto_forWrite;
    private Int16 _WinZipAesMethod;
    internal DateTime _LastModified;
    private DateTime _Mtime, _Atime, _Ctime;
    private bool _ntfsTimesAreSet;
    private bool _emitNtfsTimes = true;
    private bool _emitUnixTimes;
    private bool _TrimVolumeFromFullyQualifiedPaths = true;
    internal string _LocalFileName;
    private string _FileNameInArchive;
    internal Int16 _VersionNeeded;
    internal Int16 _BitField;
    internal Int16 _CompressionMethod;
    private Int16 _CompressionMethod_FromZipFile;
    private Ionic.Zlib.CompressionLevel _CompressionLevel;
    internal string _Comment;
    private bool _IsDirectory;
    private byte[] _CommentBytes;
    internal Int64 _CompressedSize;
    internal Int64 _CompressedFileDataSize;
    internal Int64 _UncompressedSize;
    internal Int32 _TimeBlob;
    private bool _crcCalculated;
    internal Int32 _Crc32;
    internal byte[] _Extra;
    private bool _metadataChanged;
    private bool _restreamRequiredOnSave;
    private bool _sourceIsEncrypted;
    private bool _skippedDuringSave;
    private UInt32 _diskNumber;
    private static System.Text.Encoding ibm437 = System.Text.Encoding.GetEncoding("IBM437");
    private System.Text.Encoding _actualEncoding;
    internal ZipContainer _container;
    private long __FileDataPosition = -1;
    private byte[] _EntryHeader;
    internal Int64 _RelativeOffsetOfLocalHeader;
    private Int64 _future_ROLH;
    private Int64 _TotalEntrySize;
    private int _LengthOfHeader;
    private int _LengthOfTrailer;
    internal bool _InputUsesZip64;
    private UInt32 _UnsupportedAlgorithmId;
    internal string _Password;
    internal ZipEntrySource _Source;
    internal EncryptionAlgorithm _Encryption;
    internal EncryptionAlgorithm _Encryption_FromZipFile;
    internal byte[] _WeakEncryptionHeader;
    internal Stream _archiveStream;
    private Stream _sourceStream;
    private Nullable<Int64> _sourceStreamOriginalPosition;
    private bool _sourceWasJitProvided;
    private bool _ioOperationCanceled;
    private bool _presumeZip64;
    private Nullable<bool> _entryRequiresZip64;
    private Nullable<bool> _OutputUsesZip64;
    private bool _IsText;
    private ZipEntryTimestamp _timestamp;
    private static System.DateTime _unixEpoch = new System.DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    private static System.DateTime _win32Epoch = System.DateTime.FromFileTimeUtc(0L);
    private static System.DateTime _zeroHour = new System.DateTime(1, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    private WriteDelegate _WriteDelegate;
    private OpenDelegate _OpenDelegate;
    private CloseDelegate _CloseDelegate;
  }
  [Flags()]
  public enum ZipEntryTimestamp
  {
    None = 0,
    DOS = 1,
    Windows = 2,
    Unix = 4,
    InfoZip1 = 8
  }
  public enum CompressionMethod
  {
    None = 0,
    Deflate = 8,
    BZip2 = 12
  }
}
