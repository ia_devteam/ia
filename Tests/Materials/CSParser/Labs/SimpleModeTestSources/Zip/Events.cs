using System;
using System.Collections.Generic;
using System.Text;
namespace Ionic.Zip
{
  public delegate void WriteDelegate(string entryName, System.IO.Stream stream);
  public delegate System.IO.Stream OpenDelegate(string entryName);
  public delegate void CloseDelegate(string entryName, System.IO.Stream stream);
  public delegate Ionic.Zlib.CompressionLevel SetCompressionCallback(string localFileName, string fileNameInArchive);
  public enum ZipProgressEventType
  {
    Adding_Started,
    Adding_AfterAddEntry,
    Adding_Completed,
    Reading_Started,
    Reading_BeforeReadEntry,
    Reading_AfterReadEntry,
    Reading_Completed,
    Reading_ArchiveBytesRead,
    Saving_Started,
    Saving_BeforeWriteEntry,
    Saving_AfterWriteEntry,
    Saving_Completed,
    Saving_AfterSaveTempArchive,
    Saving_BeforeRenameTempArchive,
    Saving_AfterRenameTempArchive,
    Saving_AfterCompileSelfExtractor,
    Saving_EntryBytesRead,
    Extracting_BeforeExtractEntry,
    Extracting_AfterExtractEntry,
    Extracting_ExtractEntryWouldOverwrite,
    Extracting_EntryBytesWritten,
    Extracting_BeforeExtractAll,
    Extracting_AfterExtractAll,
    Error_Saving
  }
  public class ZipProgressEventArgs : EventArgs
  {
    private int _entriesTotal;
    private bool _cancel;
    private ZipEntry _latestEntry;
    private ZipProgressEventType _flavor;
    private String _archiveName;
    private Int64 _bytesTransferred;
    private Int64 _totalBytesToTransfer;
    internal ZipProgressEventArgs()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1164);
    }
    internal ZipProgressEventArgs(string archiveName, ZipProgressEventType flavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1165);
      this._archiveName = archiveName;
      this._flavor = flavor;
      IACSharpSensor.IACSharpSensor.SensorReached(1166);
    }
    public int EntriesTotal {
      get {
        System.Int32 RNTRNTRNT_192 = _entriesTotal;
        IACSharpSensor.IACSharpSensor.SensorReached(1167);
        return RNTRNTRNT_192;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1168);
        _entriesTotal = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1169);
      }
    }
    public ZipEntry CurrentEntry {
      get {
        ZipEntry RNTRNTRNT_193 = _latestEntry;
        IACSharpSensor.IACSharpSensor.SensorReached(1170);
        return RNTRNTRNT_193;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1171);
        _latestEntry = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1172);
      }
    }
    public bool Cancel {
      get {
        System.Boolean RNTRNTRNT_194 = _cancel;
        IACSharpSensor.IACSharpSensor.SensorReached(1173);
        return RNTRNTRNT_194;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1174);
        _cancel = _cancel || value;
        IACSharpSensor.IACSharpSensor.SensorReached(1175);
      }
    }
    public ZipProgressEventType EventType {
      get {
        ZipProgressEventType RNTRNTRNT_195 = _flavor;
        IACSharpSensor.IACSharpSensor.SensorReached(1176);
        return RNTRNTRNT_195;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1177);
        _flavor = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1178);
      }
    }
    public String ArchiveName {
      get {
        String RNTRNTRNT_196 = _archiveName;
        IACSharpSensor.IACSharpSensor.SensorReached(1179);
        return RNTRNTRNT_196;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1180);
        _archiveName = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1181);
      }
    }
    public Int64 BytesTransferred {
      get {
        Int64 RNTRNTRNT_197 = _bytesTransferred;
        IACSharpSensor.IACSharpSensor.SensorReached(1182);
        return RNTRNTRNT_197;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1183);
        _bytesTransferred = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1184);
      }
    }
    public Int64 TotalBytesToTransfer {
      get {
        Int64 RNTRNTRNT_198 = _totalBytesToTransfer;
        IACSharpSensor.IACSharpSensor.SensorReached(1185);
        return RNTRNTRNT_198;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1186);
        _totalBytesToTransfer = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1187);
      }
    }
  }
  public class ReadProgressEventArgs : ZipProgressEventArgs
  {
    internal ReadProgressEventArgs()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1188);
    }
    private ReadProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1189);
    }
    static internal ReadProgressEventArgs Before(string archiveName, int entriesTotal)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1190);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_BeforeReadEntry);
      x.EntriesTotal = entriesTotal;
      ReadProgressEventArgs RNTRNTRNT_199 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1191);
      return RNTRNTRNT_199;
    }
    static internal ReadProgressEventArgs After(string archiveName, ZipEntry entry, int entriesTotal)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1192);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_AfterReadEntry);
      x.EntriesTotal = entriesTotal;
      x.CurrentEntry = entry;
      ReadProgressEventArgs RNTRNTRNT_200 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1193);
      return RNTRNTRNT_200;
    }
    static internal ReadProgressEventArgs Started(string archiveName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1194);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_Started);
      ReadProgressEventArgs RNTRNTRNT_201 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1195);
      return RNTRNTRNT_201;
    }
    static internal ReadProgressEventArgs ByteUpdate(string archiveName, ZipEntry entry, Int64 bytesXferred, Int64 totalBytes)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1196);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_ArchiveBytesRead);
      x.CurrentEntry = entry;
      x.BytesTransferred = bytesXferred;
      x.TotalBytesToTransfer = totalBytes;
      ReadProgressEventArgs RNTRNTRNT_202 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1197);
      return RNTRNTRNT_202;
    }
    static internal ReadProgressEventArgs Completed(string archiveName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1198);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_Completed);
      ReadProgressEventArgs RNTRNTRNT_203 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1199);
      return RNTRNTRNT_203;
    }
  }
  public class AddProgressEventArgs : ZipProgressEventArgs
  {
    internal AddProgressEventArgs()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1200);
    }
    private AddProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1201);
    }
    static internal AddProgressEventArgs AfterEntry(string archiveName, ZipEntry entry, int entriesTotal)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1202);
      var x = new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_AfterAddEntry);
      x.EntriesTotal = entriesTotal;
      x.CurrentEntry = entry;
      AddProgressEventArgs RNTRNTRNT_204 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1203);
      return RNTRNTRNT_204;
    }
    static internal AddProgressEventArgs Started(string archiveName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1204);
      var x = new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_Started);
      AddProgressEventArgs RNTRNTRNT_205 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1205);
      return RNTRNTRNT_205;
    }
    static internal AddProgressEventArgs Completed(string archiveName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1206);
      var x = new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_Completed);
      AddProgressEventArgs RNTRNTRNT_206 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1207);
      return RNTRNTRNT_206;
    }
  }
  public class SaveProgressEventArgs : ZipProgressEventArgs
  {
    private int _entriesSaved;
    internal SaveProgressEventArgs(string archiveName, bool before, int entriesTotal, int entriesSaved, ZipEntry entry) : base(archiveName, (before) ? ZipProgressEventType.Saving_BeforeWriteEntry : ZipProgressEventType.Saving_AfterWriteEntry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1208);
      this.EntriesTotal = entriesTotal;
      this.CurrentEntry = entry;
      this._entriesSaved = entriesSaved;
      IACSharpSensor.IACSharpSensor.SensorReached(1209);
    }
    internal SaveProgressEventArgs()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1210);
    }
    internal SaveProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1211);
    }
    static internal SaveProgressEventArgs ByteUpdate(string archiveName, ZipEntry entry, Int64 bytesXferred, Int64 totalBytes)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1212);
      var x = new SaveProgressEventArgs(archiveName, ZipProgressEventType.Saving_EntryBytesRead);
      x.ArchiveName = archiveName;
      x.CurrentEntry = entry;
      x.BytesTransferred = bytesXferred;
      x.TotalBytesToTransfer = totalBytes;
      SaveProgressEventArgs RNTRNTRNT_207 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1213);
      return RNTRNTRNT_207;
    }
    static internal SaveProgressEventArgs Started(string archiveName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1214);
      var x = new SaveProgressEventArgs(archiveName, ZipProgressEventType.Saving_Started);
      SaveProgressEventArgs RNTRNTRNT_208 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1215);
      return RNTRNTRNT_208;
    }
    static internal SaveProgressEventArgs Completed(string archiveName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1216);
      var x = new SaveProgressEventArgs(archiveName, ZipProgressEventType.Saving_Completed);
      SaveProgressEventArgs RNTRNTRNT_209 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1217);
      return RNTRNTRNT_209;
    }
    public int EntriesSaved {
      get {
        System.Int32 RNTRNTRNT_210 = _entriesSaved;
        IACSharpSensor.IACSharpSensor.SensorReached(1218);
        return RNTRNTRNT_210;
      }
    }
  }
  public class ExtractProgressEventArgs : ZipProgressEventArgs
  {
    private int _entriesExtracted;
    private string _target;
    internal ExtractProgressEventArgs(string archiveName, bool before, int entriesTotal, int entriesExtracted, ZipEntry entry, string extractLocation) : base(archiveName, (before) ? ZipProgressEventType.Extracting_BeforeExtractEntry : ZipProgressEventType.Extracting_AfterExtractEntry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1219);
      this.EntriesTotal = entriesTotal;
      this.CurrentEntry = entry;
      this._entriesExtracted = entriesExtracted;
      this._target = extractLocation;
      IACSharpSensor.IACSharpSensor.SensorReached(1220);
    }
    internal ExtractProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1221);
    }
    internal ExtractProgressEventArgs()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1222);
    }
    static internal ExtractProgressEventArgs BeforeExtractEntry(string archiveName, ZipEntry entry, string extractLocation)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1223);
      var x = new ExtractProgressEventArgs {
        ArchiveName = archiveName,
        EventType = ZipProgressEventType.Extracting_BeforeExtractEntry,
        CurrentEntry = entry,
        _target = extractLocation
      };
      ExtractProgressEventArgs RNTRNTRNT_211 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1224);
      return RNTRNTRNT_211;
    }
    static internal ExtractProgressEventArgs ExtractExisting(string archiveName, ZipEntry entry, string extractLocation)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1225);
      var x = new ExtractProgressEventArgs {
        ArchiveName = archiveName,
        EventType = ZipProgressEventType.Extracting_ExtractEntryWouldOverwrite,
        CurrentEntry = entry,
        _target = extractLocation
      };
      ExtractProgressEventArgs RNTRNTRNT_212 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1226);
      return RNTRNTRNT_212;
    }
    static internal ExtractProgressEventArgs AfterExtractEntry(string archiveName, ZipEntry entry, string extractLocation)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1227);
      var x = new ExtractProgressEventArgs {
        ArchiveName = archiveName,
        EventType = ZipProgressEventType.Extracting_AfterExtractEntry,
        CurrentEntry = entry,
        _target = extractLocation
      };
      ExtractProgressEventArgs RNTRNTRNT_213 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1228);
      return RNTRNTRNT_213;
    }
    static internal ExtractProgressEventArgs ExtractAllStarted(string archiveName, string extractLocation)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1229);
      var x = new ExtractProgressEventArgs(archiveName, ZipProgressEventType.Extracting_BeforeExtractAll);
      x._target = extractLocation;
      ExtractProgressEventArgs RNTRNTRNT_214 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1230);
      return RNTRNTRNT_214;
    }
    static internal ExtractProgressEventArgs ExtractAllCompleted(string archiveName, string extractLocation)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1231);
      var x = new ExtractProgressEventArgs(archiveName, ZipProgressEventType.Extracting_AfterExtractAll);
      x._target = extractLocation;
      ExtractProgressEventArgs RNTRNTRNT_215 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1232);
      return RNTRNTRNT_215;
    }
    static internal ExtractProgressEventArgs ByteUpdate(string archiveName, ZipEntry entry, Int64 bytesWritten, Int64 totalBytes)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1233);
      var x = new ExtractProgressEventArgs(archiveName, ZipProgressEventType.Extracting_EntryBytesWritten);
      x.ArchiveName = archiveName;
      x.CurrentEntry = entry;
      x.BytesTransferred = bytesWritten;
      x.TotalBytesToTransfer = totalBytes;
      ExtractProgressEventArgs RNTRNTRNT_216 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1234);
      return RNTRNTRNT_216;
    }
    public int EntriesExtracted {
      get {
        System.Int32 RNTRNTRNT_217 = _entriesExtracted;
        IACSharpSensor.IACSharpSensor.SensorReached(1235);
        return RNTRNTRNT_217;
      }
    }
    public String ExtractLocation {
      get {
        String RNTRNTRNT_218 = _target;
        IACSharpSensor.IACSharpSensor.SensorReached(1236);
        return RNTRNTRNT_218;
      }
    }
  }
  public class ZipErrorEventArgs : ZipProgressEventArgs
  {
    private Exception _exc;
    private ZipErrorEventArgs()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1237);
    }
    static internal ZipErrorEventArgs Saving(string archiveName, ZipEntry entry, Exception exception)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1238);
      var x = new ZipErrorEventArgs {
        EventType = ZipProgressEventType.Error_Saving,
        ArchiveName = archiveName,
        CurrentEntry = entry,
        _exc = exception
      };
      ZipErrorEventArgs RNTRNTRNT_219 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1239);
      return RNTRNTRNT_219;
    }
    public Exception Exception {
      get {
        Exception RNTRNTRNT_220 = _exc;
        IACSharpSensor.IACSharpSensor.SensorReached(1240);
        return RNTRNTRNT_220;
      }
    }
    public String FileName {
      get {
        String RNTRNTRNT_221 = CurrentEntry.LocalFileName;
        IACSharpSensor.IACSharpSensor.SensorReached(1241);
        return RNTRNTRNT_221;
      }
    }
  }
}
