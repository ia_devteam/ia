using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    public ZipEntry AddItem(string fileOrDirectoryName)
    {
      ZipEntry RNTRNTRNT_340 = AddItem(fileOrDirectoryName, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2427);
      return RNTRNTRNT_340;
    }
    public ZipEntry AddItem(String fileOrDirectoryName, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2428);
      if (File.Exists(fileOrDirectoryName)) {
        ZipEntry RNTRNTRNT_341 = AddFile(fileOrDirectoryName, directoryPathInArchive);
        IACSharpSensor.IACSharpSensor.SensorReached(2429);
        return RNTRNTRNT_341;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2430);
      if (Directory.Exists(fileOrDirectoryName)) {
        ZipEntry RNTRNTRNT_342 = AddDirectory(fileOrDirectoryName, directoryPathInArchive);
        IACSharpSensor.IACSharpSensor.SensorReached(2431);
        return RNTRNTRNT_342;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2432);
      throw new FileNotFoundException(String.Format("That file or directory ({0}) does not exist!", fileOrDirectoryName));
    }
    public ZipEntry AddFile(string fileName)
    {
      ZipEntry RNTRNTRNT_343 = AddFile(fileName, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2433);
      return RNTRNTRNT_343;
    }
    public ZipEntry AddFile(string fileName, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2434);
      string nameInArchive = ZipEntry.NameInArchive(fileName, directoryPathInArchive);
      ZipEntry ze = ZipEntry.CreateFromFile(fileName, nameInArchive);
      IACSharpSensor.IACSharpSensor.SensorReached(2435);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(2436);
        StatusMessageTextWriter.WriteLine("adding {0}...", fileName);
      }
      ZipEntry RNTRNTRNT_344 = _InternalAddEntry(ze);
      IACSharpSensor.IACSharpSensor.SensorReached(2437);
      return RNTRNTRNT_344;
    }
    public void RemoveEntries(System.Collections.Generic.ICollection<ZipEntry> entriesToRemove)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2438);
      if (entriesToRemove == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2439);
        throw new ArgumentNullException("entriesToRemove");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2440);
      foreach (ZipEntry e in entriesToRemove) {
        IACSharpSensor.IACSharpSensor.SensorReached(2441);
        this.RemoveEntry(e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2442);
    }
    public void RemoveEntries(System.Collections.Generic.ICollection<String> entriesToRemove)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2443);
      if (entriesToRemove == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2444);
        throw new ArgumentNullException("entriesToRemove");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2445);
      foreach (String e in entriesToRemove) {
        IACSharpSensor.IACSharpSensor.SensorReached(2446);
        this.RemoveEntry(e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2447);
    }
    public void AddFiles(System.Collections.Generic.IEnumerable<String> fileNames)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2448);
      this.AddFiles(fileNames, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2449);
    }
    public void UpdateFiles(System.Collections.Generic.IEnumerable<String> fileNames)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2450);
      this.UpdateFiles(fileNames, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2451);
    }
    public void AddFiles(System.Collections.Generic.IEnumerable<String> fileNames, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2452);
      AddFiles(fileNames, false, directoryPathInArchive);
      IACSharpSensor.IACSharpSensor.SensorReached(2453);
    }
    public void AddFiles(System.Collections.Generic.IEnumerable<String> fileNames, bool preserveDirHierarchy, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2454);
      if (fileNames == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2455);
        throw new ArgumentNullException("fileNames");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2456);
      _addOperationCanceled = false;
      OnAddStarted();
      IACSharpSensor.IACSharpSensor.SensorReached(2457);
      if (preserveDirHierarchy) {
        IACSharpSensor.IACSharpSensor.SensorReached(2458);
        foreach (var f in fileNames) {
          IACSharpSensor.IACSharpSensor.SensorReached(2459);
          if (_addOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(2460);
            break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2461);
          if (directoryPathInArchive != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2462);
            string s = Path.GetFullPath(Path.Combine(directoryPathInArchive, Path.GetDirectoryName(f)));
            this.AddFile(f, s);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2463);
            this.AddFile(f, null);
          }
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2464);
        foreach (var f in fileNames) {
          IACSharpSensor.IACSharpSensor.SensorReached(2465);
          if (_addOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(2466);
            break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2467);
          this.AddFile(f, directoryPathInArchive);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2468);
      if (!_addOperationCanceled) {
        IACSharpSensor.IACSharpSensor.SensorReached(2469);
        OnAddCompleted();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2470);
    }
    public void UpdateFiles(System.Collections.Generic.IEnumerable<String> fileNames, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2471);
      if (fileNames == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2472);
        throw new ArgumentNullException("fileNames");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2473);
      OnAddStarted();
      IACSharpSensor.IACSharpSensor.SensorReached(2474);
      foreach (var f in fileNames) {
        IACSharpSensor.IACSharpSensor.SensorReached(2475);
        this.UpdateFile(f, directoryPathInArchive);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2476);
      OnAddCompleted();
      IACSharpSensor.IACSharpSensor.SensorReached(2477);
    }
    public ZipEntry UpdateFile(string fileName)
    {
      ZipEntry RNTRNTRNT_345 = UpdateFile(fileName, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2478);
      return RNTRNTRNT_345;
    }
    public ZipEntry UpdateFile(string fileName, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2479);
      var key = ZipEntry.NameInArchive(fileName, directoryPathInArchive);
      IACSharpSensor.IACSharpSensor.SensorReached(2480);
      if (this[key] != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2481);
        this.RemoveEntry(key);
      }
      ZipEntry RNTRNTRNT_346 = this.AddFile(fileName, directoryPathInArchive);
      IACSharpSensor.IACSharpSensor.SensorReached(2482);
      return RNTRNTRNT_346;
    }
    public ZipEntry UpdateDirectory(string directoryName)
    {
      ZipEntry RNTRNTRNT_347 = UpdateDirectory(directoryName, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2483);
      return RNTRNTRNT_347;
    }
    public ZipEntry UpdateDirectory(string directoryName, String directoryPathInArchive)
    {
      ZipEntry RNTRNTRNT_348 = this.AddOrUpdateDirectoryImpl(directoryName, directoryPathInArchive, AddOrUpdateAction.AddOrUpdate);
      IACSharpSensor.IACSharpSensor.SensorReached(2484);
      return RNTRNTRNT_348;
    }
    public void UpdateItem(string itemName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2485);
      UpdateItem(itemName, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2486);
    }
    public void UpdateItem(string itemName, string directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2487);
      if (File.Exists(itemName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(2488);
        UpdateFile(itemName, directoryPathInArchive);
      } else if (Directory.Exists(itemName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(2490);
        UpdateDirectory(itemName, directoryPathInArchive);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2489);
        throw new FileNotFoundException(String.Format("That file or directory ({0}) does not exist!", itemName));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2491);
    }
    public ZipEntry AddEntry(string entryName, string content)
    {
      ZipEntry RNTRNTRNT_349 = AddEntry(entryName, content, System.Text.Encoding.Default);
      IACSharpSensor.IACSharpSensor.SensorReached(2492);
      return RNTRNTRNT_349;
    }
    public ZipEntry AddEntry(string entryName, string content, System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2493);
      var ms = new MemoryStream();
      var sw = new StreamWriter(ms, encoding);
      sw.Write(content);
      sw.Flush();
      ms.Seek(0, SeekOrigin.Begin);
      ZipEntry RNTRNTRNT_350 = AddEntry(entryName, ms);
      IACSharpSensor.IACSharpSensor.SensorReached(2494);
      return RNTRNTRNT_350;
    }
    public ZipEntry AddEntry(string entryName, Stream stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2495);
      ZipEntry ze = ZipEntry.CreateForStream(entryName, stream);
      ze.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      IACSharpSensor.IACSharpSensor.SensorReached(2496);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(2497);
        StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
      }
      ZipEntry RNTRNTRNT_351 = _InternalAddEntry(ze);
      IACSharpSensor.IACSharpSensor.SensorReached(2498);
      return RNTRNTRNT_351;
    }
    public ZipEntry AddEntry(string entryName, WriteDelegate writer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2499);
      ZipEntry ze = ZipEntry.CreateForWriter(entryName, writer);
      IACSharpSensor.IACSharpSensor.SensorReached(2500);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(2501);
        StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
      }
      ZipEntry RNTRNTRNT_352 = _InternalAddEntry(ze);
      IACSharpSensor.IACSharpSensor.SensorReached(2502);
      return RNTRNTRNT_352;
    }
    public ZipEntry AddEntry(string entryName, OpenDelegate opener, CloseDelegate closer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2503);
      ZipEntry ze = ZipEntry.CreateForJitStreamProvider(entryName, opener, closer);
      ze.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      IACSharpSensor.IACSharpSensor.SensorReached(2504);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(2505);
        StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
      }
      ZipEntry RNTRNTRNT_353 = _InternalAddEntry(ze);
      IACSharpSensor.IACSharpSensor.SensorReached(2506);
      return RNTRNTRNT_353;
    }
    private ZipEntry _InternalAddEntry(ZipEntry ze)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2507);
      ze._container = new ZipContainer(this);
      ze.CompressionMethod = this.CompressionMethod;
      ze.CompressionLevel = this.CompressionLevel;
      ze.ExtractExistingFile = this.ExtractExistingFile;
      ze.ZipErrorAction = this.ZipErrorAction;
      ze.SetCompression = this.SetCompression;
      ze.AlternateEncoding = this.AlternateEncoding;
      ze.AlternateEncodingUsage = this.AlternateEncodingUsage;
      ze.Password = this._Password;
      ze.Encryption = this.Encryption;
      ze.EmitTimesInWindowsFormatWhenSaving = this._emitNtfsTimes;
      ze.EmitTimesInUnixFormatWhenSaving = this._emitUnixTimes;
      InternalAddEntry(ze.FileName, ze);
      AfterAddEntry(ze);
      ZipEntry RNTRNTRNT_354 = ze;
      IACSharpSensor.IACSharpSensor.SensorReached(2508);
      return RNTRNTRNT_354;
    }
    public ZipEntry UpdateEntry(string entryName, string content)
    {
      ZipEntry RNTRNTRNT_355 = UpdateEntry(entryName, content, System.Text.Encoding.Default);
      IACSharpSensor.IACSharpSensor.SensorReached(2509);
      return RNTRNTRNT_355;
    }
    public ZipEntry UpdateEntry(string entryName, string content, System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2510);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_356 = AddEntry(entryName, content, encoding);
      IACSharpSensor.IACSharpSensor.SensorReached(2511);
      return RNTRNTRNT_356;
    }
    public ZipEntry UpdateEntry(string entryName, WriteDelegate writer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2512);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_357 = AddEntry(entryName, writer);
      IACSharpSensor.IACSharpSensor.SensorReached(2513);
      return RNTRNTRNT_357;
    }
    public ZipEntry UpdateEntry(string entryName, OpenDelegate opener, CloseDelegate closer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2514);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_358 = AddEntry(entryName, opener, closer);
      IACSharpSensor.IACSharpSensor.SensorReached(2515);
      return RNTRNTRNT_358;
    }
    public ZipEntry UpdateEntry(string entryName, Stream stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2516);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_359 = AddEntry(entryName, stream);
      IACSharpSensor.IACSharpSensor.SensorReached(2517);
      return RNTRNTRNT_359;
    }
    private void RemoveEntryForUpdate(string entryName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2518);
      if (String.IsNullOrEmpty(entryName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(2519);
        throw new ArgumentNullException("entryName");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2520);
      string directoryPathInArchive = null;
      IACSharpSensor.IACSharpSensor.SensorReached(2521);
      if (entryName.IndexOf('\\') != -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2522);
        directoryPathInArchive = Path.GetDirectoryName(entryName);
        entryName = Path.GetFileName(entryName);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2523);
      var key = ZipEntry.NameInArchive(entryName, directoryPathInArchive);
      IACSharpSensor.IACSharpSensor.SensorReached(2524);
      if (this[key] != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2525);
        this.RemoveEntry(key);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2526);
    }
    public ZipEntry AddEntry(string entryName, byte[] byteContent)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2527);
      if (byteContent == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2528);
        throw new ArgumentException("bad argument", "byteContent");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2529);
      var ms = new MemoryStream(byteContent);
      ZipEntry RNTRNTRNT_360 = AddEntry(entryName, ms);
      IACSharpSensor.IACSharpSensor.SensorReached(2530);
      return RNTRNTRNT_360;
    }
    public ZipEntry UpdateEntry(string entryName, byte[] byteContent)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2531);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_361 = AddEntry(entryName, byteContent);
      IACSharpSensor.IACSharpSensor.SensorReached(2532);
      return RNTRNTRNT_361;
    }
    public ZipEntry AddDirectory(string directoryName)
    {
      ZipEntry RNTRNTRNT_362 = AddDirectory(directoryName, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2533);
      return RNTRNTRNT_362;
    }
    public ZipEntry AddDirectory(string directoryName, string directoryPathInArchive)
    {
      ZipEntry RNTRNTRNT_363 = AddOrUpdateDirectoryImpl(directoryName, directoryPathInArchive, AddOrUpdateAction.AddOnly);
      IACSharpSensor.IACSharpSensor.SensorReached(2534);
      return RNTRNTRNT_363;
    }
    public ZipEntry AddDirectoryByName(string directoryNameInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2535);
      ZipEntry dir = ZipEntry.CreateFromNothing(directoryNameInArchive);
      dir._container = new ZipContainer(this);
      dir.MarkAsDirectory();
      dir.AlternateEncoding = this.AlternateEncoding;
      dir.AlternateEncodingUsage = this.AlternateEncodingUsage;
      dir.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      dir.EmitTimesInWindowsFormatWhenSaving = _emitNtfsTimes;
      dir.EmitTimesInUnixFormatWhenSaving = _emitUnixTimes;
      dir._Source = ZipEntrySource.Stream;
      InternalAddEntry(dir.FileName, dir);
      AfterAddEntry(dir);
      ZipEntry RNTRNTRNT_364 = dir;
      IACSharpSensor.IACSharpSensor.SensorReached(2536);
      return RNTRNTRNT_364;
    }
    private ZipEntry AddOrUpdateDirectoryImpl(string directoryName, string rootDirectoryPathInArchive, AddOrUpdateAction action)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2537);
      if (rootDirectoryPathInArchive == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2538);
        rootDirectoryPathInArchive = "";
      }
      ZipEntry RNTRNTRNT_365 = AddOrUpdateDirectoryImpl(directoryName, rootDirectoryPathInArchive, action, true, 0);
      IACSharpSensor.IACSharpSensor.SensorReached(2539);
      return RNTRNTRNT_365;
    }
    internal void InternalAddEntry(String name, ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2540);
      _entries.Add(name, entry);
      _zipEntriesAsList = null;
      _contentsChanged = true;
      IACSharpSensor.IACSharpSensor.SensorReached(2541);
    }
    private ZipEntry AddOrUpdateDirectoryImpl(string directoryName, string rootDirectoryPathInArchive, AddOrUpdateAction action, bool recurse, int level)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2542);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(2543);
        StatusMessageTextWriter.WriteLine("{0} {1}...", (action == AddOrUpdateAction.AddOnly) ? "adding" : "Adding or updating", directoryName);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2544);
      if (level == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2545);
        _addOperationCanceled = false;
        OnAddStarted();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2546);
      if (_addOperationCanceled) {
        IACSharpSensor.IACSharpSensor.SensorReached(2547);
        return null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2548);
      string dirForEntries = rootDirectoryPathInArchive;
      ZipEntry baseDir = null;
      IACSharpSensor.IACSharpSensor.SensorReached(2549);
      if (level > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2550);
        int f = directoryName.Length;
        IACSharpSensor.IACSharpSensor.SensorReached(2551);
        for (int i = level; i > 0; i--) {
          IACSharpSensor.IACSharpSensor.SensorReached(2552);
          f = directoryName.LastIndexOfAny("/\\".ToCharArray(), f - 1, f - 1);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2553);
        dirForEntries = directoryName.Substring(f + 1);
        dirForEntries = Path.Combine(rootDirectoryPathInArchive, dirForEntries);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2554);
      if (level > 0 || rootDirectoryPathInArchive != "") {
        IACSharpSensor.IACSharpSensor.SensorReached(2555);
        baseDir = ZipEntry.CreateFromFile(directoryName, dirForEntries);
        baseDir._container = new ZipContainer(this);
        baseDir.AlternateEncoding = this.AlternateEncoding;
        baseDir.AlternateEncodingUsage = this.AlternateEncodingUsage;
        baseDir.MarkAsDirectory();
        baseDir.EmitTimesInWindowsFormatWhenSaving = _emitNtfsTimes;
        baseDir.EmitTimesInUnixFormatWhenSaving = _emitUnixTimes;
        IACSharpSensor.IACSharpSensor.SensorReached(2556);
        if (!_entries.ContainsKey(baseDir.FileName)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2557);
          InternalAddEntry(baseDir.FileName, baseDir);
          AfterAddEntry(baseDir);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2558);
        dirForEntries = baseDir.FileName;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2559);
      if (!_addOperationCanceled) {
        IACSharpSensor.IACSharpSensor.SensorReached(2560);
        String[] filenames = Directory.GetFiles(directoryName);
        IACSharpSensor.IACSharpSensor.SensorReached(2561);
        if (recurse) {
          IACSharpSensor.IACSharpSensor.SensorReached(2562);
          foreach (String filename in filenames) {
            IACSharpSensor.IACSharpSensor.SensorReached(2563);
            if (_addOperationCanceled) {
              IACSharpSensor.IACSharpSensor.SensorReached(2564);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2565);
            if (action == AddOrUpdateAction.AddOnly) {
              IACSharpSensor.IACSharpSensor.SensorReached(2566);
              AddFile(filename, dirForEntries);
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(2567);
              UpdateFile(filename, dirForEntries);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2568);
          if (!_addOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(2569);
            String[] dirnames = Directory.GetDirectories(directoryName);
            IACSharpSensor.IACSharpSensor.SensorReached(2570);
            foreach (String dir in dirnames) {
              IACSharpSensor.IACSharpSensor.SensorReached(2571);
              FileAttributes fileAttrs = System.IO.File.GetAttributes(dir);
              IACSharpSensor.IACSharpSensor.SensorReached(2572);
              if (this.AddDirectoryWillTraverseReparsePoints || ((fileAttrs & FileAttributes.ReparsePoint) == 0)) {
                IACSharpSensor.IACSharpSensor.SensorReached(2573);
                AddOrUpdateDirectoryImpl(dir, rootDirectoryPathInArchive, action, recurse, level + 1);
              }
            }
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2574);
      if (level == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2575);
        OnAddCompleted();
      }
      ZipEntry RNTRNTRNT_366 = baseDir;
      IACSharpSensor.IACSharpSensor.SensorReached(2576);
      return RNTRNTRNT_366;
    }
  }
}
