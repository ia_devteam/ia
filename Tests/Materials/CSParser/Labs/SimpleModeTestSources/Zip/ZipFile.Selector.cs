using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  partial class ZipFile
  {
    public void AddSelectedFiles(String selectionCriteria)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3313);
      this.AddSelectedFiles(selectionCriteria, ".", null, false);
      IACSharpSensor.IACSharpSensor.SensorReached(3314);
    }
    public void AddSelectedFiles(String selectionCriteria, bool recurseDirectories)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3315);
      this.AddSelectedFiles(selectionCriteria, ".", null, recurseDirectories);
      IACSharpSensor.IACSharpSensor.SensorReached(3316);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3317);
      this.AddSelectedFiles(selectionCriteria, directoryOnDisk, null, false);
      IACSharpSensor.IACSharpSensor.SensorReached(3318);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk, bool recurseDirectories)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3319);
      this.AddSelectedFiles(selectionCriteria, directoryOnDisk, null, recurseDirectories);
      IACSharpSensor.IACSharpSensor.SensorReached(3320);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3321);
      this.AddSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, false);
      IACSharpSensor.IACSharpSensor.SensorReached(3322);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive, bool recurseDirectories)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3323);
      _AddOrUpdateSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, recurseDirectories, false);
      IACSharpSensor.IACSharpSensor.SensorReached(3324);
    }
    public void UpdateSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive, bool recurseDirectories)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3325);
      _AddOrUpdateSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, recurseDirectories, true);
      IACSharpSensor.IACSharpSensor.SensorReached(3326);
    }
    private string EnsureendInSlash(string s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3327);
      if (s.EndsWith("\\")) {
        System.String RNTRNTRNT_448 = s;
        IACSharpSensor.IACSharpSensor.SensorReached(3328);
        return RNTRNTRNT_448;
      }
      System.String RNTRNTRNT_449 = s + "\\";
      IACSharpSensor.IACSharpSensor.SensorReached(3329);
      return RNTRNTRNT_449;
    }
    private void _AddOrUpdateSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive, bool recurseDirectories, bool wantUpdate)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3330);
      if (directoryOnDisk == null && (Directory.Exists(selectionCriteria))) {
        IACSharpSensor.IACSharpSensor.SensorReached(3331);
        directoryOnDisk = selectionCriteria;
        selectionCriteria = "*.*";
      } else if (String.IsNullOrEmpty(directoryOnDisk)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3332);
        directoryOnDisk = ".";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3333);
      while (directoryOnDisk.EndsWith("\\")) {
        IACSharpSensor.IACSharpSensor.SensorReached(3334);
        directoryOnDisk = directoryOnDisk.Substring(0, directoryOnDisk.Length - 1);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3335);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(3336);
        StatusMessageTextWriter.WriteLine("adding selection '{0}' from dir '{1}'...", selectionCriteria, directoryOnDisk);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3337);
      Ionic.FileSelector ff = new Ionic.FileSelector(selectionCriteria, AddDirectoryWillTraverseReparsePoints);
      var itemsToAdd = ff.SelectFiles(directoryOnDisk, recurseDirectories);
      IACSharpSensor.IACSharpSensor.SensorReached(3338);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(3339);
        StatusMessageTextWriter.WriteLine("found {0} files...", itemsToAdd.Count);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3340);
      OnAddStarted();
      AddOrUpdateAction action = (wantUpdate) ? AddOrUpdateAction.AddOrUpdate : AddOrUpdateAction.AddOnly;
      IACSharpSensor.IACSharpSensor.SensorReached(3341);
      foreach (var item in itemsToAdd) {
        IACSharpSensor.IACSharpSensor.SensorReached(3342);
        string dirInArchive = (directoryPathInArchive == null) ? null : ReplaceLeadingDirectory(Path.GetDirectoryName(item), directoryOnDisk, directoryPathInArchive);
        IACSharpSensor.IACSharpSensor.SensorReached(3343);
        if (File.Exists(item)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3344);
          if (wantUpdate) {
            IACSharpSensor.IACSharpSensor.SensorReached(3345);
            this.UpdateFile(item, dirInArchive);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(3346);
            this.AddFile(item, dirInArchive);
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(3347);
          AddOrUpdateDirectoryImpl(item, dirInArchive, action, false, 0);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3348);
      OnAddCompleted();
      IACSharpSensor.IACSharpSensor.SensorReached(3349);
    }
    private static string ReplaceLeadingDirectory(string original, string pattern, string replacement)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3350);
      string upperString = original.ToUpper();
      string upperPattern = pattern.ToUpper();
      int p1 = upperString.IndexOf(upperPattern);
      IACSharpSensor.IACSharpSensor.SensorReached(3351);
      if (p1 != 0) {
        System.String RNTRNTRNT_450 = original;
        IACSharpSensor.IACSharpSensor.SensorReached(3352);
        return RNTRNTRNT_450;
      }
      System.String RNTRNTRNT_451 = replacement + original.Substring(upperPattern.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(3353);
      return RNTRNTRNT_451;
    }
    public ICollection<ZipEntry> SelectEntries(String selectionCriteria)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3354);
      Ionic.FileSelector ff = new Ionic.FileSelector(selectionCriteria, AddDirectoryWillTraverseReparsePoints);
      ICollection<ZipEntry> RNTRNTRNT_452 = ff.SelectEntries(this);
      IACSharpSensor.IACSharpSensor.SensorReached(3355);
      return RNTRNTRNT_452;
    }
    public ICollection<ZipEntry> SelectEntries(String selectionCriteria, string directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3356);
      Ionic.FileSelector ff = new Ionic.FileSelector(selectionCriteria, AddDirectoryWillTraverseReparsePoints);
      ICollection<ZipEntry> RNTRNTRNT_453 = ff.SelectEntries(this, directoryPathInArchive);
      IACSharpSensor.IACSharpSensor.SensorReached(3357);
      return RNTRNTRNT_453;
    }
    public int RemoveSelectedEntries(String selectionCriteria)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3358);
      var selection = this.SelectEntries(selectionCriteria);
      this.RemoveEntries(selection);
      System.Int32 RNTRNTRNT_454 = selection.Count;
      IACSharpSensor.IACSharpSensor.SensorReached(3359);
      return RNTRNTRNT_454;
    }
    public int RemoveSelectedEntries(String selectionCriteria, string directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3360);
      var selection = this.SelectEntries(selectionCriteria, directoryPathInArchive);
      this.RemoveEntries(selection);
      System.Int32 RNTRNTRNT_455 = selection.Count;
      IACSharpSensor.IACSharpSensor.SensorReached(3361);
      return RNTRNTRNT_455;
    }
    public void ExtractSelectedEntries(String selectionCriteria)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3362);
      foreach (ZipEntry e in SelectEntries(selectionCriteria)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3363);
        e.Password = _Password;
        e.Extract();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3364);
    }
    public void ExtractSelectedEntries(String selectionCriteria, ExtractExistingFileAction extractExistingFile)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3365);
      foreach (ZipEntry e in SelectEntries(selectionCriteria)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3366);
        e.Password = _Password;
        e.Extract(extractExistingFile);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3367);
    }
    public void ExtractSelectedEntries(String selectionCriteria, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3368);
      foreach (ZipEntry e in SelectEntries(selectionCriteria, directoryPathInArchive)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3369);
        e.Password = _Password;
        e.Extract();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3370);
    }
    public void ExtractSelectedEntries(String selectionCriteria, string directoryInArchive, string extractDirectory)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3371);
      foreach (ZipEntry e in SelectEntries(selectionCriteria, directoryInArchive)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3372);
        e.Password = _Password;
        e.Extract(extractDirectory);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3373);
    }
    public void ExtractSelectedEntries(String selectionCriteria, string directoryPathInArchive, string extractDirectory, ExtractExistingFileAction extractExistingFile)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3374);
      foreach (ZipEntry e in SelectEntries(selectionCriteria, directoryPathInArchive)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3375);
        e.Password = _Password;
        e.Extract(extractDirectory, extractExistingFile);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3376);
    }
  }
}
namespace Ionic
{
  internal abstract partial class SelectionCriterion
  {
    internal abstract bool Evaluate(Ionic.Zip.ZipEntry entry);
  }
  internal partial class NameCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3377);
      string transformedFileName = entry.FileName.Replace("/", "\\");
      System.Boolean RNTRNTRNT_456 = _Evaluate(transformedFileName);
      IACSharpSensor.IACSharpSensor.SensorReached(3378);
      return RNTRNTRNT_456;
    }
  }
  internal partial class SizeCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      System.Boolean RNTRNTRNT_457 = _Evaluate(entry.UncompressedSize);
      IACSharpSensor.IACSharpSensor.SensorReached(3379);
      return RNTRNTRNT_457;
    }
  }
  internal partial class TimeCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3380);
      DateTime x;
      IACSharpSensor.IACSharpSensor.SensorReached(3381);
      switch (Which) {
        case WhichTime.atime:
          IACSharpSensor.IACSharpSensor.SensorReached(3382);
          x = entry.AccessedTime;
          IACSharpSensor.IACSharpSensor.SensorReached(3383);
          break;
        case WhichTime.mtime:
          IACSharpSensor.IACSharpSensor.SensorReached(3384);
          x = entry.ModifiedTime;
          IACSharpSensor.IACSharpSensor.SensorReached(3385);
          break;
        case WhichTime.ctime:
          IACSharpSensor.IACSharpSensor.SensorReached(3386);
          x = entry.CreationTime;
          IACSharpSensor.IACSharpSensor.SensorReached(3387);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(3388);
          throw new ArgumentException("??time");
      }
      System.Boolean RNTRNTRNT_458 = _Evaluate(x);
      IACSharpSensor.IACSharpSensor.SensorReached(3389);
      return RNTRNTRNT_458;
    }
  }
  internal partial class TypeCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3390);
      bool result = (ObjectType == 'D') ? entry.IsDirectory : !entry.IsDirectory;
      IACSharpSensor.IACSharpSensor.SensorReached(3391);
      if (Operator != ComparisonOperator.EqualTo) {
        IACSharpSensor.IACSharpSensor.SensorReached(3392);
        result = !result;
      }
      System.Boolean RNTRNTRNT_459 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(3393);
      return RNTRNTRNT_459;
    }
  }
  internal partial class AttributesCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3394);
      FileAttributes fileAttrs = entry.Attributes;
      System.Boolean RNTRNTRNT_460 = _Evaluate(fileAttrs);
      IACSharpSensor.IACSharpSensor.SensorReached(3395);
      return RNTRNTRNT_460;
    }
  }
  internal partial class CompoundCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3396);
      bool result = Left.Evaluate(entry);
      IACSharpSensor.IACSharpSensor.SensorReached(3397);
      switch (Conjunction) {
        case LogicalConjunction.AND:
          IACSharpSensor.IACSharpSensor.SensorReached(3398);
          if (result) {
            IACSharpSensor.IACSharpSensor.SensorReached(3399);
            result = Right.Evaluate(entry);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3400);
          break;
        case LogicalConjunction.OR:
          IACSharpSensor.IACSharpSensor.SensorReached(3401);
          if (!result) {
            IACSharpSensor.IACSharpSensor.SensorReached(3402);
            result = Right.Evaluate(entry);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3403);
          break;
        case LogicalConjunction.XOR:
          IACSharpSensor.IACSharpSensor.SensorReached(3404);
          result ^= Right.Evaluate(entry);
          IACSharpSensor.IACSharpSensor.SensorReached(3405);
          break;
      }
      System.Boolean RNTRNTRNT_461 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(3406);
      return RNTRNTRNT_461;
    }
  }
  public partial class FileSelector
  {
    private bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3407);
      bool result = _Criterion.Evaluate(entry);
      System.Boolean RNTRNTRNT_462 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(3408);
      return RNTRNTRNT_462;
    }
    public ICollection<Ionic.Zip.ZipEntry> SelectEntries(Ionic.Zip.ZipFile zip)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3409);
      if (zip == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3410);
        throw new ArgumentNullException("zip");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3411);
      var list = new List<Ionic.Zip.ZipEntry>();
      IACSharpSensor.IACSharpSensor.SensorReached(3412);
      foreach (Ionic.Zip.ZipEntry e in zip) {
        IACSharpSensor.IACSharpSensor.SensorReached(3413);
        if (this.Evaluate(e)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3414);
          list.Add(e);
        }
      }
      ICollection<Ionic.Zip.ZipEntry> RNTRNTRNT_463 = list;
      IACSharpSensor.IACSharpSensor.SensorReached(3415);
      return RNTRNTRNT_463;
    }
    public ICollection<Ionic.Zip.ZipEntry> SelectEntries(Ionic.Zip.ZipFile zip, string directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3416);
      if (zip == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3417);
        throw new ArgumentNullException("zip");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3418);
      var list = new List<Ionic.Zip.ZipEntry>();
      string slashSwapped = (directoryPathInArchive == null) ? null : directoryPathInArchive.Replace("/", "\\");
      IACSharpSensor.IACSharpSensor.SensorReached(3419);
      if (slashSwapped != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3420);
        while (slashSwapped.EndsWith("\\")) {
          IACSharpSensor.IACSharpSensor.SensorReached(3421);
          slashSwapped = slashSwapped.Substring(0, slashSwapped.Length - 1);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3422);
      foreach (Ionic.Zip.ZipEntry e in zip) {
        IACSharpSensor.IACSharpSensor.SensorReached(3423);
        if (directoryPathInArchive == null || (Path.GetDirectoryName(e.FileName) == directoryPathInArchive) || (Path.GetDirectoryName(e.FileName) == slashSwapped)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3424);
          if (this.Evaluate(e)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3425);
            list.Add(e);
          }
        }
      }
      ICollection<Ionic.Zip.ZipEntry> RNTRNTRNT_464 = list;
      IACSharpSensor.IACSharpSensor.SensorReached(3426);
      return RNTRNTRNT_464;
    }
  }
}
