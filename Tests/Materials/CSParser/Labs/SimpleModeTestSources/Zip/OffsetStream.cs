using System;
using System.IO;
namespace Ionic.Zip
{
  internal class OffsetStream : System.IO.Stream, System.IDisposable
  {
    private Int64 _originalPosition;
    private Stream _innerStream;
    public OffsetStream(Stream s) : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(403);
      _originalPosition = s.Position;
      _innerStream = s;
      IACSharpSensor.IACSharpSensor.SensorReached(404);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      System.Int32 RNTRNTRNT_93 = _innerStream.Read(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(405);
      return RNTRNTRNT_93;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(406);
      throw new NotImplementedException();
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_94 = _innerStream.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(407);
        return RNTRNTRNT_94;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_95 = _innerStream.CanSeek;
        IACSharpSensor.IACSharpSensor.SensorReached(408);
        return RNTRNTRNT_95;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_96 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(409);
        return RNTRNTRNT_96;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(410);
      _innerStream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(411);
    }
    public override long Length {
      get {
        System.Int64 RNTRNTRNT_97 = _innerStream.Length;
        IACSharpSensor.IACSharpSensor.SensorReached(412);
        return RNTRNTRNT_97;
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_98 = _innerStream.Position - _originalPosition;
        IACSharpSensor.IACSharpSensor.SensorReached(413);
        return RNTRNTRNT_98;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(414);
        _innerStream.Position = _originalPosition + value;
        IACSharpSensor.IACSharpSensor.SensorReached(415);
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      System.Int64 RNTRNTRNT_99 = _innerStream.Seek(_originalPosition + offset, origin) - _originalPosition;
      IACSharpSensor.IACSharpSensor.SensorReached(416);
      return RNTRNTRNT_99;
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(417);
      throw new NotImplementedException();
    }
    void IDisposable.Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(418);
      Close();
      IACSharpSensor.IACSharpSensor.SensorReached(419);
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(420);
      base.Close();
      IACSharpSensor.IACSharpSensor.SensorReached(421);
    }
  }
}
