using System;
using System.Threading;
using System.Collections.Generic;
using System.IO;
using Ionic.Zip;
namespace Ionic.Zip
{
  public class ZipInputStream : Stream
  {
    public ZipInputStream(Stream stream) : this(stream, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(217);
    }
    public ZipInputStream(String fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(218);
      Stream stream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
      _Init(stream, false, fileName);
      IACSharpSensor.IACSharpSensor.SensorReached(219);
    }
    public ZipInputStream(Stream stream, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(220);
      _Init(stream, leaveOpen, null);
      IACSharpSensor.IACSharpSensor.SensorReached(221);
    }
    private void _Init(Stream stream, bool leaveOpen, string name)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(222);
      _inputStream = stream;
      IACSharpSensor.IACSharpSensor.SensorReached(223);
      if (!_inputStream.CanRead) {
        IACSharpSensor.IACSharpSensor.SensorReached(224);
        throw new ZipException("The stream must be readable.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(225);
      _container = new ZipContainer(this);
      _provisionalAlternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
      _leaveUnderlyingStreamOpen = leaveOpen;
      _findRequired = true;
      _name = name ?? "(stream)";
      IACSharpSensor.IACSharpSensor.SensorReached(226);
    }
    public override String ToString()
    {
      String RNTRNTRNT_56 = String.Format("ZipInputStream::{0}(leaveOpen({1})))", _name, _leaveUnderlyingStreamOpen);
      IACSharpSensor.IACSharpSensor.SensorReached(227);
      return RNTRNTRNT_56;
    }
    public System.Text.Encoding ProvisionalAlternateEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_57 = _provisionalAlternateEncoding;
        IACSharpSensor.IACSharpSensor.SensorReached(228);
        return RNTRNTRNT_57;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(229);
        _provisionalAlternateEncoding = value;
        IACSharpSensor.IACSharpSensor.SensorReached(230);
      }
    }
    public int CodecBufferSize { get; set; }
    public String Password {
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(231);
        if (_closed) {
          IACSharpSensor.IACSharpSensor.SensorReached(232);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(233);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(234);
        _Password = value;
        IACSharpSensor.IACSharpSensor.SensorReached(235);
      }
    }
    private void SetupStream()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(236);
      _crcStream = _currentEntry.InternalOpenReader(_Password);
      _LeftToRead = _crcStream.Length;
      _needSetup = false;
      IACSharpSensor.IACSharpSensor.SensorReached(237);
    }
    internal Stream ReadStream {
      get {
        Stream RNTRNTRNT_58 = _inputStream;
        IACSharpSensor.IACSharpSensor.SensorReached(238);
        return RNTRNTRNT_58;
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(239);
      if (_closed) {
        IACSharpSensor.IACSharpSensor.SensorReached(240);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(241);
        throw new System.InvalidOperationException("The stream has been closed.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(242);
      if (_needSetup) {
        IACSharpSensor.IACSharpSensor.SensorReached(243);
        SetupStream();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(244);
      if (_LeftToRead == 0) {
        System.Int32 RNTRNTRNT_59 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(245);
        return RNTRNTRNT_59;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(246);
      int len = (_LeftToRead > count) ? count : (int)_LeftToRead;
      int n = _crcStream.Read(buffer, offset, len);
      _LeftToRead -= n;
      IACSharpSensor.IACSharpSensor.SensorReached(247);
      if (_LeftToRead == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(248);
        int CrcResult = _crcStream.Crc;
        _currentEntry.VerifyCrcAfterExtract(CrcResult);
        _inputStream.Seek(_endOfEntry, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_inputStream);
      }
      System.Int32 RNTRNTRNT_60 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(249);
      return RNTRNTRNT_60;
    }
    public ZipEntry GetNextEntry()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(250);
      if (_findRequired) {
        IACSharpSensor.IACSharpSensor.SensorReached(251);
        long d = SharedUtilities.FindSignature(_inputStream, ZipConstants.ZipEntrySignature);
        IACSharpSensor.IACSharpSensor.SensorReached(252);
        if (d == -1) {
          IACSharpSensor.IACSharpSensor.SensorReached(253);
          return null;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(254);
        _inputStream.Seek(-4, SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_inputStream);
      } else if (_firstEntry) {
        IACSharpSensor.IACSharpSensor.SensorReached(255);
        _inputStream.Seek(_endOfEntry, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_inputStream);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(256);
      _currentEntry = ZipEntry.ReadEntry(_container, !_firstEntry);
      _endOfEntry = _inputStream.Position;
      _firstEntry = true;
      _needSetup = true;
      _findRequired = false;
      ZipEntry RNTRNTRNT_61 = _currentEntry;
      IACSharpSensor.IACSharpSensor.SensorReached(257);
      return RNTRNTRNT_61;
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(258);
      if (_closed) {
        IACSharpSensor.IACSharpSensor.SensorReached(259);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(260);
      if (disposing) {
        IACSharpSensor.IACSharpSensor.SensorReached(261);
        if (_exceptionPending) {
          IACSharpSensor.IACSharpSensor.SensorReached(262);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(263);
        if (!_leaveUnderlyingStreamOpen) {
          IACSharpSensor.IACSharpSensor.SensorReached(264);
          _inputStream.Dispose();
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(265);
      _closed = true;
      IACSharpSensor.IACSharpSensor.SensorReached(266);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_62 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(267);
        return RNTRNTRNT_62;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_63 = _inputStream.CanSeek;
        IACSharpSensor.IACSharpSensor.SensorReached(268);
        return RNTRNTRNT_63;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_64 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(269);
        return RNTRNTRNT_64;
      }
    }
    public override long Length {
      get {
        System.Int64 RNTRNTRNT_65 = _inputStream.Length;
        IACSharpSensor.IACSharpSensor.SensorReached(270);
        return RNTRNTRNT_65;
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_66 = _inputStream.Position;
        IACSharpSensor.IACSharpSensor.SensorReached(271);
        return RNTRNTRNT_66;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(272);
        Seek(value, SeekOrigin.Begin);
        IACSharpSensor.IACSharpSensor.SensorReached(273);
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(274);
      throw new NotSupportedException("Flush");
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(275);
      throw new NotSupportedException("Write");
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(276);
      _findRequired = true;
      var x = _inputStream.Seek(offset, origin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_inputStream);
      System.Int64 RNTRNTRNT_67 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(277);
      return RNTRNTRNT_67;
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(278);
      throw new NotSupportedException();
    }
    private Stream _inputStream;
    private System.Text.Encoding _provisionalAlternateEncoding;
    private ZipEntry _currentEntry;
    private bool _firstEntry;
    private bool _needSetup;
    private ZipContainer _container;
    private Ionic.Crc.CrcCalculatorStream _crcStream;
    private Int64 _LeftToRead;
    internal String _Password;
    private Int64 _endOfEntry;
    private string _name;
    private bool _leaveUnderlyingStreamOpen;
    private bool _closed;
    private bool _findRequired;
    private bool _exceptionPending;
  }
}
