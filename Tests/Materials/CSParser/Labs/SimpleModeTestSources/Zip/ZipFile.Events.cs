using System;
using System.IO;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    private string ArchiveNameForEvent {
      get {
        System.String RNTRNTRNT_420 = (_name != null) ? _name : "(stream)";
        IACSharpSensor.IACSharpSensor.SensorReached(2862);
        return RNTRNTRNT_420;
      }
    }
    public event EventHandler<SaveProgressEventArgs> SaveProgress;
    internal bool OnSaveBlock(ZipEntry entry, Int64 bytesXferred, Int64 totalBytesToXfer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2863);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2864);
      if (sp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2865);
        var e = SaveProgressEventArgs.ByteUpdate(ArchiveNameForEvent, entry, bytesXferred, totalBytesToXfer);
        sp(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(2866);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(2867);
          _saveOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_421 = _saveOperationCanceled;
      IACSharpSensor.IACSharpSensor.SensorReached(2868);
      return RNTRNTRNT_421;
    }
    private void OnSaveEntry(int current, ZipEntry entry, bool before)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2869);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2870);
      if (sp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2871);
        var e = new SaveProgressEventArgs(ArchiveNameForEvent, before, _entries.Count, current, entry);
        sp(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(2872);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(2873);
          _saveOperationCanceled = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2874);
    }
    private void OnSaveEvent(ZipProgressEventType eventFlavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2875);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2876);
      if (sp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2877);
        var e = new SaveProgressEventArgs(ArchiveNameForEvent, eventFlavor);
        sp(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(2878);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(2879);
          _saveOperationCanceled = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2880);
    }
    private void OnSaveStarted()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2881);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2882);
      if (sp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2883);
        var e = SaveProgressEventArgs.Started(ArchiveNameForEvent);
        sp(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(2884);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(2885);
          _saveOperationCanceled = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2886);
    }
    private void OnSaveCompleted()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2887);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2888);
      if (sp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2889);
        var e = SaveProgressEventArgs.Completed(ArchiveNameForEvent);
        sp(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2890);
    }
    public event EventHandler<ReadProgressEventArgs> ReadProgress;
    private void OnReadStarted()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2891);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2892);
      if (rp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2893);
        var e = ReadProgressEventArgs.Started(ArchiveNameForEvent);
        rp(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2894);
    }
    private void OnReadCompleted()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2895);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2896);
      if (rp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2897);
        var e = ReadProgressEventArgs.Completed(ArchiveNameForEvent);
        rp(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2898);
    }
    internal void OnReadBytes(ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2899);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2900);
      if (rp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2901);
        var e = ReadProgressEventArgs.ByteUpdate(ArchiveNameForEvent, entry, ReadStream.Position, LengthOfReadStream);
        rp(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2902);
    }
    internal void OnReadEntry(bool before, ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2903);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2904);
      if (rp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2905);
        ReadProgressEventArgs e = (before) ? ReadProgressEventArgs.Before(ArchiveNameForEvent, _entries.Count) : ReadProgressEventArgs.After(ArchiveNameForEvent, entry, _entries.Count);
        rp(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2906);
    }
    private Int64 _lengthOfReadStream = -99;
    private Int64 LengthOfReadStream {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2907);
        if (_lengthOfReadStream == -99) {
          IACSharpSensor.IACSharpSensor.SensorReached(2908);
          _lengthOfReadStream = (_ReadStreamIsOurs) ? SharedUtilities.GetFileLength(_name) : -1L;
        }
        Int64 RNTRNTRNT_422 = _lengthOfReadStream;
        IACSharpSensor.IACSharpSensor.SensorReached(2909);
        return RNTRNTRNT_422;
      }
    }
    public event EventHandler<ExtractProgressEventArgs> ExtractProgress;
    private void OnExtractEntry(int current, bool before, ZipEntry currentEntry, string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2910);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2911);
      if (ep != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2912);
        var e = new ExtractProgressEventArgs(ArchiveNameForEvent, before, _entries.Count, current, currentEntry, path);
        ep(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(2913);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(2914);
          _extractOperationCanceled = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2915);
    }
    internal bool OnExtractBlock(ZipEntry entry, Int64 bytesWritten, Int64 totalBytesToWrite)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2916);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2917);
      if (ep != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2918);
        var e = ExtractProgressEventArgs.ByteUpdate(ArchiveNameForEvent, entry, bytesWritten, totalBytesToWrite);
        ep(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(2919);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(2920);
          _extractOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_423 = _extractOperationCanceled;
      IACSharpSensor.IACSharpSensor.SensorReached(2921);
      return RNTRNTRNT_423;
    }
    internal bool OnSingleEntryExtract(ZipEntry entry, string path, bool before)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2922);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2923);
      if (ep != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2924);
        var e = (before) ? ExtractProgressEventArgs.BeforeExtractEntry(ArchiveNameForEvent, entry, path) : ExtractProgressEventArgs.AfterExtractEntry(ArchiveNameForEvent, entry, path);
        ep(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(2925);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(2926);
          _extractOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_424 = _extractOperationCanceled;
      IACSharpSensor.IACSharpSensor.SensorReached(2927);
      return RNTRNTRNT_424;
    }
    internal bool OnExtractExisting(ZipEntry entry, string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2928);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2929);
      if (ep != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2930);
        var e = ExtractProgressEventArgs.ExtractExisting(ArchiveNameForEvent, entry, path);
        ep(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(2931);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(2932);
          _extractOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_425 = _extractOperationCanceled;
      IACSharpSensor.IACSharpSensor.SensorReached(2933);
      return RNTRNTRNT_425;
    }
    private void OnExtractAllCompleted(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2934);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2935);
      if (ep != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2936);
        var e = ExtractProgressEventArgs.ExtractAllCompleted(ArchiveNameForEvent, path);
        ep(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2937);
    }
    private void OnExtractAllStarted(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2938);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2939);
      if (ep != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2940);
        var e = ExtractProgressEventArgs.ExtractAllStarted(ArchiveNameForEvent, path);
        ep(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2941);
    }
    public event EventHandler<AddProgressEventArgs> AddProgress;
    private void OnAddStarted()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2942);
      EventHandler<AddProgressEventArgs> ap = AddProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2943);
      if (ap != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2944);
        var e = AddProgressEventArgs.Started(ArchiveNameForEvent);
        ap(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(2945);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(2946);
          _addOperationCanceled = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2947);
    }
    private void OnAddCompleted()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2948);
      EventHandler<AddProgressEventArgs> ap = AddProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2949);
      if (ap != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2950);
        var e = AddProgressEventArgs.Completed(ArchiveNameForEvent);
        ap(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2951);
    }
    internal void AfterAddEntry(ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2952);
      EventHandler<AddProgressEventArgs> ap = AddProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(2953);
      if (ap != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2954);
        var e = AddProgressEventArgs.AfterEntry(ArchiveNameForEvent, entry, _entries.Count);
        ap(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(2955);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(2956);
          _addOperationCanceled = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2957);
    }
    public event EventHandler<ZipErrorEventArgs> ZipError;
    internal bool OnZipErrorSaving(ZipEntry entry, Exception exc)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2958);
      if (ZipError != null) {
        lock (LOCK) {
          IACSharpSensor.IACSharpSensor.SensorReached(2959);
          var e = ZipErrorEventArgs.Saving(this.Name, entry, exc);
          ZipError(this, e);
          IACSharpSensor.IACSharpSensor.SensorReached(2960);
          if (e.Cancel) {
            IACSharpSensor.IACSharpSensor.SensorReached(2961);
            _saveOperationCanceled = true;
          }
        }
      }
      System.Boolean RNTRNTRNT_426 = _saveOperationCanceled;
      IACSharpSensor.IACSharpSensor.SensorReached(2962);
      return RNTRNTRNT_426;
    }
  }
}
