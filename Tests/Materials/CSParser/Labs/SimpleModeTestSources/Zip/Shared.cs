using System;
using System.IO;
using System.Security.Permissions;
namespace Ionic.Zip
{
  static internal class SharedUtilities
  {
    public static Int64 GetFileLength(string fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(913);
      if (!File.Exists(fileName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(914);
        throw new System.IO.FileNotFoundException(fileName);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(915);
      long fileLength = 0L;
      FileShare fs = FileShare.ReadWrite;
      fs |= FileShare.Delete;
      IACSharpSensor.IACSharpSensor.SensorReached(916);
      using (var s = File.Open(fileName, FileMode.Open, FileAccess.Read, fs)) {
        IACSharpSensor.IACSharpSensor.SensorReached(917);
        fileLength = s.Length;
      }
      Int64 RNTRNTRNT_150 = fileLength;
      IACSharpSensor.IACSharpSensor.SensorReached(918);
      return RNTRNTRNT_150;
    }
    [System.Diagnostics.Conditional("NETCF")]
    public static void Workaround_Ladybug318918(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(919);
      s.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(920);
    }
    private static System.Text.RegularExpressions.Regex doubleDotRegex1 = new System.Text.RegularExpressions.Regex("^(.*/)?([^/\\\\.]+/\\\\.\\\\./)(.+)$");
    private static string SimplifyFwdSlashPath(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(921);
      if (path.StartsWith("./")) {
        IACSharpSensor.IACSharpSensor.SensorReached(922);
        path = path.Substring(2);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(923);
      path = path.Replace("/./", "/");
      path = doubleDotRegex1.Replace(path, "$1$3");
      System.String RNTRNTRNT_151 = path;
      IACSharpSensor.IACSharpSensor.SensorReached(924);
      return RNTRNTRNT_151;
    }
    public static string NormalizePathForUseInZipFile(string pathName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(925);
      if (String.IsNullOrEmpty(pathName)) {
        System.String RNTRNTRNT_152 = pathName;
        IACSharpSensor.IACSharpSensor.SensorReached(926);
        return RNTRNTRNT_152;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(927);
      if ((pathName.Length >= 2) && ((pathName[1] == ':') && (pathName[2] == '\\'))) {
        IACSharpSensor.IACSharpSensor.SensorReached(928);
        pathName = pathName.Substring(3);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(929);
      pathName = pathName.Replace('\\', '/');
      IACSharpSensor.IACSharpSensor.SensorReached(930);
      while (pathName.StartsWith("/")) {
        IACSharpSensor.IACSharpSensor.SensorReached(931);
        pathName = pathName.Substring(1);
      }
      System.String RNTRNTRNT_153 = SimplifyFwdSlashPath(pathName);
      IACSharpSensor.IACSharpSensor.SensorReached(932);
      return RNTRNTRNT_153;
    }
    static System.Text.Encoding ibm437 = System.Text.Encoding.GetEncoding("IBM437");
    static System.Text.Encoding utf8 = System.Text.Encoding.GetEncoding("UTF-8");
    static internal byte[] StringToByteArray(string value, System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(933);
      byte[] a = encoding.GetBytes(value);
      System.Byte[] RNTRNTRNT_154 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(934);
      return RNTRNTRNT_154;
    }
    static internal byte[] StringToByteArray(string value)
    {
      System.Byte[] RNTRNTRNT_155 = StringToByteArray(value, ibm437);
      IACSharpSensor.IACSharpSensor.SensorReached(935);
      return RNTRNTRNT_155;
    }
    static internal string Utf8StringFromBuffer(byte[] buf)
    {
      System.String RNTRNTRNT_156 = StringFromBuffer(buf, utf8);
      IACSharpSensor.IACSharpSensor.SensorReached(936);
      return RNTRNTRNT_156;
    }
    static internal string StringFromBuffer(byte[] buf, System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(937);
      string s = encoding.GetString(buf, 0, buf.Length);
      System.String RNTRNTRNT_157 = s;
      IACSharpSensor.IACSharpSensor.SensorReached(938);
      return RNTRNTRNT_157;
    }
    static internal int ReadSignature(System.IO.Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(939);
      int x = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(940);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(941);
        x = _ReadFourBytes(s, "n/a");
      } catch (BadReadException) {
      }
      System.Int32 RNTRNTRNT_158 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(942);
      return RNTRNTRNT_158;
    }
    static internal int ReadEntrySignature(System.IO.Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(943);
      int x = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(944);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(945);
        x = _ReadFourBytes(s, "n/a");
        IACSharpSensor.IACSharpSensor.SensorReached(946);
        if (x == ZipConstants.ZipEntryDataDescriptorSignature) {
          IACSharpSensor.IACSharpSensor.SensorReached(947);
          s.Seek(12, SeekOrigin.Current);
          Workaround_Ladybug318918(s);
          x = _ReadFourBytes(s, "n/a");
          IACSharpSensor.IACSharpSensor.SensorReached(948);
          if (x != ZipConstants.ZipEntrySignature) {
            IACSharpSensor.IACSharpSensor.SensorReached(949);
            s.Seek(8, SeekOrigin.Current);
            Workaround_Ladybug318918(s);
            x = _ReadFourBytes(s, "n/a");
            IACSharpSensor.IACSharpSensor.SensorReached(950);
            if (x != ZipConstants.ZipEntrySignature) {
              IACSharpSensor.IACSharpSensor.SensorReached(951);
              s.Seek(-24, SeekOrigin.Current);
              Workaround_Ladybug318918(s);
              x = _ReadFourBytes(s, "n/a");
            }
          }
        }
      } catch (BadReadException) {
      }
      System.Int32 RNTRNTRNT_159 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(952);
      return RNTRNTRNT_159;
    }
    static internal int ReadInt(System.IO.Stream s)
    {
      System.Int32 RNTRNTRNT_160 = _ReadFourBytes(s, "Could not read block - no data!  (position 0x{0:X8})");
      IACSharpSensor.IACSharpSensor.SensorReached(953);
      return RNTRNTRNT_160;
    }
    private static int _ReadFourBytes(System.IO.Stream s, string message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(954);
      int n = 0;
      byte[] block = new byte[4];
      n = s.Read(block, 0, block.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(955);
      if (n != block.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(956);
        throw new BadReadException(String.Format(message, s.Position));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(957);
      int data = unchecked((((block[3] * 256 + block[2]) * 256) + block[1]) * 256 + block[0]);
      System.Int32 RNTRNTRNT_161 = data;
      IACSharpSensor.IACSharpSensor.SensorReached(958);
      return RNTRNTRNT_161;
    }
    static internal long FindSignature(System.IO.Stream stream, int SignatureToFind)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(959);
      long startingPosition = stream.Position;
      int BATCH_SIZE = 65536;
      byte[] targetBytes = new byte[4];
      targetBytes[0] = (byte)(SignatureToFind >> 24);
      targetBytes[1] = (byte)((SignatureToFind & 0xff0000) >> 16);
      targetBytes[2] = (byte)((SignatureToFind & 0xff00) >> 8);
      targetBytes[3] = (byte)(SignatureToFind & 0xff);
      byte[] batch = new byte[BATCH_SIZE];
      int n = 0;
      bool success = false;
      IACSharpSensor.IACSharpSensor.SensorReached(960);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(961);
        n = stream.Read(batch, 0, batch.Length);
        IACSharpSensor.IACSharpSensor.SensorReached(962);
        if (n != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(963);
          for (int i = 0; i < n; i++) {
            IACSharpSensor.IACSharpSensor.SensorReached(964);
            if (batch[i] == targetBytes[3]) {
              IACSharpSensor.IACSharpSensor.SensorReached(965);
              long curPosition = stream.Position;
              stream.Seek(i - n, System.IO.SeekOrigin.Current);
              Workaround_Ladybug318918(stream);
              int sig = ReadSignature(stream);
              success = (sig == SignatureToFind);
              IACSharpSensor.IACSharpSensor.SensorReached(966);
              if (!success) {
                IACSharpSensor.IACSharpSensor.SensorReached(967);
                stream.Seek(curPosition, System.IO.SeekOrigin.Begin);
                Workaround_Ladybug318918(stream);
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(968);
                break;
              }
            }
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(969);
          break;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(970);
        if (success) {
          IACSharpSensor.IACSharpSensor.SensorReached(971);
          break;
        }
      } while (true);
      IACSharpSensor.IACSharpSensor.SensorReached(972);
      if (!success) {
        IACSharpSensor.IACSharpSensor.SensorReached(973);
        stream.Seek(startingPosition, System.IO.SeekOrigin.Begin);
        Workaround_Ladybug318918(stream);
        System.Int64 RNTRNTRNT_162 = -1;
        IACSharpSensor.IACSharpSensor.SensorReached(974);
        return RNTRNTRNT_162;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(975);
      long bytesRead = (stream.Position - startingPosition) - 4;
      System.Int64 RNTRNTRNT_163 = bytesRead;
      IACSharpSensor.IACSharpSensor.SensorReached(976);
      return RNTRNTRNT_163;
    }
    static internal DateTime AdjustTime_Reverse(DateTime time)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(977);
      if (time.Kind == DateTimeKind.Utc) {
        DateTime RNTRNTRNT_164 = time;
        IACSharpSensor.IACSharpSensor.SensorReached(978);
        return RNTRNTRNT_164;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(979);
      DateTime adjusted = time;
      IACSharpSensor.IACSharpSensor.SensorReached(980);
      if (DateTime.Now.IsDaylightSavingTime() && !time.IsDaylightSavingTime()) {
        IACSharpSensor.IACSharpSensor.SensorReached(981);
        adjusted = time - new System.TimeSpan(1, 0, 0);
      } else if (!DateTime.Now.IsDaylightSavingTime() && time.IsDaylightSavingTime()) {
        IACSharpSensor.IACSharpSensor.SensorReached(982);
        adjusted = time + new System.TimeSpan(1, 0, 0);
      }
      DateTime RNTRNTRNT_165 = adjusted;
      IACSharpSensor.IACSharpSensor.SensorReached(983);
      return RNTRNTRNT_165;
    }
    static internal DateTime PackedToDateTime(Int32 packedDateTime)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(984);
      if (packedDateTime == 0xffff || packedDateTime == 0) {
        DateTime RNTRNTRNT_166 = new System.DateTime(1995, 1, 1, 0, 0, 0, 0);
        IACSharpSensor.IACSharpSensor.SensorReached(985);
        return RNTRNTRNT_166;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(986);
      Int16 packedTime = unchecked((Int16)(packedDateTime & 0xffff));
      Int16 packedDate = unchecked((Int16)((packedDateTime & 0xffff0000u) >> 16));
      int year = 1980 + ((packedDate & 0xfe00) >> 9);
      int month = (packedDate & 0x1e0) >> 5;
      int day = packedDate & 0x1f;
      int hour = (packedTime & 0xf800) >> 11;
      int minute = (packedTime & 0x7e0) >> 5;
      int second = (packedTime & 0x1f) * 2;
      IACSharpSensor.IACSharpSensor.SensorReached(987);
      if (second >= 60) {
        IACSharpSensor.IACSharpSensor.SensorReached(988);
        minute++;
        second = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(989);
      if (minute >= 60) {
        IACSharpSensor.IACSharpSensor.SensorReached(990);
        hour++;
        minute = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(991);
      if (hour >= 24) {
        IACSharpSensor.IACSharpSensor.SensorReached(992);
        day++;
        hour = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(993);
      DateTime d = System.DateTime.Now;
      bool success = false;
      IACSharpSensor.IACSharpSensor.SensorReached(994);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(995);
        d = new System.DateTime(year, month, day, hour, minute, second, 0);
        success = true;
      } catch (System.ArgumentOutOfRangeException) {
        IACSharpSensor.IACSharpSensor.SensorReached(996);
        if (year == 1980 && (month == 0 || day == 0)) {
          IACSharpSensor.IACSharpSensor.SensorReached(997);
          try {
            IACSharpSensor.IACSharpSensor.SensorReached(998);
            d = new System.DateTime(1980, 1, 1, hour, minute, second, 0);
            success = true;
          } catch (System.ArgumentOutOfRangeException) {
            IACSharpSensor.IACSharpSensor.SensorReached(999);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(1000);
              d = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
              success = true;
            } catch (System.ArgumentOutOfRangeException) {
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1001);
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1002);
          try {
            IACSharpSensor.IACSharpSensor.SensorReached(1003);
            while (year < 1980) {
              IACSharpSensor.IACSharpSensor.SensorReached(1004);
              year++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1005);
            while (year > 2030) {
              IACSharpSensor.IACSharpSensor.SensorReached(1006);
              year--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1007);
            while (month < 1) {
              IACSharpSensor.IACSharpSensor.SensorReached(1008);
              month++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1009);
            while (month > 12) {
              IACSharpSensor.IACSharpSensor.SensorReached(1010);
              month--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1011);
            while (day < 1) {
              IACSharpSensor.IACSharpSensor.SensorReached(1012);
              day++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1013);
            while (day > 28) {
              IACSharpSensor.IACSharpSensor.SensorReached(1014);
              day--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1015);
            while (minute < 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(1016);
              minute++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1017);
            while (minute > 59) {
              IACSharpSensor.IACSharpSensor.SensorReached(1018);
              minute--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1019);
            while (second < 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(1020);
              second++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1021);
            while (second > 59) {
              IACSharpSensor.IACSharpSensor.SensorReached(1022);
              second--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1023);
            d = new System.DateTime(year, month, day, hour, minute, second, 0);
            success = true;
          } catch (System.ArgumentOutOfRangeException) {
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1024);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1025);
      if (!success) {
        IACSharpSensor.IACSharpSensor.SensorReached(1026);
        string msg = String.Format("y({0}) m({1}) d({2}) h({3}) m({4}) s({5})", year, month, day, hour, minute, second);
        IACSharpSensor.IACSharpSensor.SensorReached(1027);
        throw new ZipException(String.Format("Bad date/time format in the zip file. ({0})", msg));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1028);
      d = DateTime.SpecifyKind(d, DateTimeKind.Local);
      DateTime RNTRNTRNT_167 = d;
      IACSharpSensor.IACSharpSensor.SensorReached(1029);
      return RNTRNTRNT_167;
    }
    static internal Int32 DateTimeToPacked(DateTime time)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1030);
      time = time.ToLocalTime();
      UInt16 packedDate = (UInt16)((time.Day & 0x1f) | ((time.Month << 5) & 0x1e0) | (((time.Year - 1980) << 9) & 0xfe00));
      UInt16 packedTime = (UInt16)((time.Second / 2 & 0x1f) | ((time.Minute << 5) & 0x7e0) | ((time.Hour << 11) & 0xf800));
      Int32 result = (Int32)(((UInt32)(packedDate << 16)) | packedTime);
      Int32 RNTRNTRNT_168 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(1031);
      return RNTRNTRNT_168;
    }
    public static void CreateAndOpenUniqueTempFile(string dir, out Stream fs, out string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1032);
      for (int i = 0; i < 3; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1033);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(1034);
          filename = Path.Combine(dir, InternalGetTempFileName());
          fs = new FileStream(filename, FileMode.CreateNew);
          IACSharpSensor.IACSharpSensor.SensorReached(1035);
          return;
        } catch (IOException) {
          IACSharpSensor.IACSharpSensor.SensorReached(1036);
          if (i == 2) {
            IACSharpSensor.IACSharpSensor.SensorReached(1037);
            throw;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1038);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1039);
      throw new IOException();
    }
    public static string InternalGetTempFileName()
    {
      System.String RNTRNTRNT_169 = "DotNetZip-" + Path.GetRandomFileName().Substring(0, 8) + ".tmp";
      IACSharpSensor.IACSharpSensor.SensorReached(1040);
      return RNTRNTRNT_169;
    }
    static internal int ReadWithRetry(System.IO.Stream s, byte[] buffer, int offset, int count, string FileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1041);
      int n = 0;
      bool done = false;
      int retries = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1042);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(1043);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(1044);
          n = s.Read(buffer, offset, count);
          done = true;
        } catch (System.IO.IOException ioexc1) {
          IACSharpSensor.IACSharpSensor.SensorReached(1045);
          var p = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);
          IACSharpSensor.IACSharpSensor.SensorReached(1046);
          if (p.IsUnrestricted()) {
            IACSharpSensor.IACSharpSensor.SensorReached(1047);
            uint hresult = _HRForException(ioexc1);
            IACSharpSensor.IACSharpSensor.SensorReached(1048);
            if (hresult != 0x80070021u) {
              IACSharpSensor.IACSharpSensor.SensorReached(1049);
              throw new System.IO.IOException(String.Format("Cannot read file {0}", FileName), ioexc1);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1050);
            retries++;
            IACSharpSensor.IACSharpSensor.SensorReached(1051);
            if (retries > 10) {
              IACSharpSensor.IACSharpSensor.SensorReached(1052);
              throw new System.IO.IOException(String.Format("Cannot read file {0}, at offset 0x{1:X8} after 10 retries", FileName, offset), ioexc1);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1053);
            System.Threading.Thread.Sleep(250 + retries * 550);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1054);
            throw;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1055);
        }
      } while (!done);
      System.Int32 RNTRNTRNT_170 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(1056);
      return RNTRNTRNT_170;
    }
    private static uint _HRForException(System.Exception ex1)
    {
      System.UInt32 RNTRNTRNT_171 = unchecked((uint)System.Runtime.InteropServices.Marshal.GetHRForException(ex1));
      IACSharpSensor.IACSharpSensor.SensorReached(1057);
      return RNTRNTRNT_171;
    }
  }
  public class CountingStream : System.IO.Stream
  {
    private System.IO.Stream _s;
    private Int64 _bytesWritten;
    private Int64 _bytesRead;
    private Int64 _initialOffset;
    public CountingStream(System.IO.Stream stream) : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1058);
      _s = stream;
      IACSharpSensor.IACSharpSensor.SensorReached(1059);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(1060);
        _initialOffset = _s.Position;
      } catch {
        IACSharpSensor.IACSharpSensor.SensorReached(1061);
        _initialOffset = 0L;
        IACSharpSensor.IACSharpSensor.SensorReached(1062);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1063);
    }
    public Stream WrappedStream {
      get {
        Stream RNTRNTRNT_172 = _s;
        IACSharpSensor.IACSharpSensor.SensorReached(1064);
        return RNTRNTRNT_172;
      }
    }
    public Int64 BytesWritten {
      get {
        Int64 RNTRNTRNT_173 = _bytesWritten;
        IACSharpSensor.IACSharpSensor.SensorReached(1065);
        return RNTRNTRNT_173;
      }
    }
    public Int64 BytesRead {
      get {
        Int64 RNTRNTRNT_174 = _bytesRead;
        IACSharpSensor.IACSharpSensor.SensorReached(1066);
        return RNTRNTRNT_174;
      }
    }
    public void Adjust(Int64 delta)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1067);
      _bytesWritten -= delta;
      IACSharpSensor.IACSharpSensor.SensorReached(1068);
      if (_bytesWritten < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1069);
        throw new InvalidOperationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1070);
      if (_s as CountingStream != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1071);
        ((CountingStream)_s).Adjust(delta);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1072);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1073);
      int n = _s.Read(buffer, offset, count);
      _bytesRead += n;
      System.Int32 RNTRNTRNT_175 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(1074);
      return RNTRNTRNT_175;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1075);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1076);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1077);
      _s.Write(buffer, offset, count);
      _bytesWritten += count;
      IACSharpSensor.IACSharpSensor.SensorReached(1078);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_176 = _s.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(1079);
        return RNTRNTRNT_176;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_177 = _s.CanSeek;
        IACSharpSensor.IACSharpSensor.SensorReached(1080);
        return RNTRNTRNT_177;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_178 = _s.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(1081);
        return RNTRNTRNT_178;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1082);
      _s.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(1083);
    }
    public override long Length {
      get {
        System.Int64 RNTRNTRNT_179 = _s.Length;
        IACSharpSensor.IACSharpSensor.SensorReached(1084);
        return RNTRNTRNT_179;
      }
    }
    public long ComputedPosition {
      get {
        System.Int64 RNTRNTRNT_180 = _initialOffset + _bytesWritten;
        IACSharpSensor.IACSharpSensor.SensorReached(1085);
        return RNTRNTRNT_180;
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_181 = _s.Position;
        IACSharpSensor.IACSharpSensor.SensorReached(1086);
        return RNTRNTRNT_181;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1087);
        _s.Seek(value, System.IO.SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_s);
        IACSharpSensor.IACSharpSensor.SensorReached(1088);
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      System.Int64 RNTRNTRNT_182 = _s.Seek(offset, origin);
      IACSharpSensor.IACSharpSensor.SensorReached(1089);
      return RNTRNTRNT_182;
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1090);
      _s.SetLength(value);
      IACSharpSensor.IACSharpSensor.SensorReached(1091);
    }
  }
}
