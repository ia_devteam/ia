using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public class ReadOptions
  {
    public EventHandler<ReadProgressEventArgs> ReadProgress { get; set; }
    public TextWriter StatusMessageWriter { get; set; }
    public System.Text.Encoding Encoding { get; set; }
  }
  public partial class ZipFile
  {
    public static ZipFile Read(string fileName)
    {
      ZipFile RNTRNTRNT_427 = ZipFile.Read(fileName, null, null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2991);
      return RNTRNTRNT_427;
    }
    public static ZipFile Read(string fileName, ReadOptions options)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2992);
      if (options == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2993);
        throw new ArgumentNullException("options");
      }
      ZipFile RNTRNTRNT_428 = Read(fileName, options.StatusMessageWriter, options.Encoding, options.ReadProgress);
      IACSharpSensor.IACSharpSensor.SensorReached(2994);
      return RNTRNTRNT_428;
    }
    private static ZipFile Read(string fileName, TextWriter statusMessageWriter, System.Text.Encoding encoding, EventHandler<ReadProgressEventArgs> readProgress)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2995);
      ZipFile zf = new ZipFile();
      zf.AlternateEncoding = encoding ?? DefaultEncoding;
      zf.AlternateEncodingUsage = ZipOption.Always;
      zf._StatusMessageTextWriter = statusMessageWriter;
      zf._name = fileName;
      IACSharpSensor.IACSharpSensor.SensorReached(2996);
      if (readProgress != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2997);
        zf.ReadProgress = readProgress;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2998);
      if (zf.Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(2999);
        zf._StatusMessageTextWriter.WriteLine("reading from {0}...", fileName);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3000);
      ReadIntoInstance(zf);
      zf._fileAlreadyExists = true;
      ZipFile RNTRNTRNT_429 = zf;
      IACSharpSensor.IACSharpSensor.SensorReached(3001);
      return RNTRNTRNT_429;
    }
    public static ZipFile Read(Stream zipStream)
    {
      ZipFile RNTRNTRNT_430 = Read(zipStream, null, null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(3002);
      return RNTRNTRNT_430;
    }
    public static ZipFile Read(Stream zipStream, ReadOptions options)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3003);
      if (options == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3004);
        throw new ArgumentNullException("options");
      }
      ZipFile RNTRNTRNT_431 = Read(zipStream, options.StatusMessageWriter, options.Encoding, options.ReadProgress);
      IACSharpSensor.IACSharpSensor.SensorReached(3005);
      return RNTRNTRNT_431;
    }
    private static ZipFile Read(Stream zipStream, TextWriter statusMessageWriter, System.Text.Encoding encoding, EventHandler<ReadProgressEventArgs> readProgress)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3006);
      if (zipStream == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3007);
        throw new ArgumentNullException("zipStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3008);
      ZipFile zf = new ZipFile();
      zf._StatusMessageTextWriter = statusMessageWriter;
      zf._alternateEncoding = encoding ?? ZipFile.DefaultEncoding;
      zf._alternateEncodingUsage = ZipOption.Always;
      IACSharpSensor.IACSharpSensor.SensorReached(3009);
      if (readProgress != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3010);
        zf.ReadProgress += readProgress;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3011);
      zf._readstream = (zipStream.Position == 0L) ? zipStream : new OffsetStream(zipStream);
      zf._ReadStreamIsOurs = false;
      IACSharpSensor.IACSharpSensor.SensorReached(3012);
      if (zf.Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(3013);
        zf._StatusMessageTextWriter.WriteLine("reading from stream...");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3014);
      ReadIntoInstance(zf);
      ZipFile RNTRNTRNT_432 = zf;
      IACSharpSensor.IACSharpSensor.SensorReached(3015);
      return RNTRNTRNT_432;
    }
    private static void ReadIntoInstance(ZipFile zf)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3016);
      Stream s = zf.ReadStream;
      IACSharpSensor.IACSharpSensor.SensorReached(3017);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3018);
        zf._readName = zf._name;
        IACSharpSensor.IACSharpSensor.SensorReached(3019);
        if (!s.CanSeek) {
          IACSharpSensor.IACSharpSensor.SensorReached(3020);
          ReadIntoInstance_Orig(zf);
          IACSharpSensor.IACSharpSensor.SensorReached(3021);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3022);
        zf.OnReadStarted();
        uint datum = ReadFirstFourBytes(s);
        IACSharpSensor.IACSharpSensor.SensorReached(3023);
        if (datum == ZipConstants.EndOfCentralDirectorySignature) {
          IACSharpSensor.IACSharpSensor.SensorReached(3024);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3025);
        int nTries = 0;
        bool success = false;
        long posn = s.Length - 64;
        long maxSeekback = Math.Max(s.Length - 0x4000, 10);
        IACSharpSensor.IACSharpSensor.SensorReached(3026);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(3027);
          if (posn < 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(3028);
            posn = 0;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3029);
          s.Seek(posn, SeekOrigin.Begin);
          long bytesRead = SharedUtilities.FindSignature(s, (int)ZipConstants.EndOfCentralDirectorySignature);
          IACSharpSensor.IACSharpSensor.SensorReached(3030);
          if (bytesRead != -1) {
            IACSharpSensor.IACSharpSensor.SensorReached(3031);
            success = true;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(3032);
            if (posn == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(3033);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3034);
            nTries++;
            posn -= (32 * (nTries + 1) * nTries);
          }
        } while (!success && posn > maxSeekback);
        IACSharpSensor.IACSharpSensor.SensorReached(3035);
        if (success) {
          IACSharpSensor.IACSharpSensor.SensorReached(3036);
          zf._locEndOfCDS = s.Position - 4;
          byte[] block = new byte[16];
          s.Read(block, 0, block.Length);
          zf._diskNumberWithCd = BitConverter.ToUInt16(block, 2);
          IACSharpSensor.IACSharpSensor.SensorReached(3037);
          if (zf._diskNumberWithCd == 0xffff) {
            IACSharpSensor.IACSharpSensor.SensorReached(3038);
            throw new ZipException("Spanned archives with more than 65534 segments are not supported at this time.");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3039);
          zf._diskNumberWithCd++;
          int i = 12;
          uint offset32 = (uint)BitConverter.ToUInt32(block, i);
          IACSharpSensor.IACSharpSensor.SensorReached(3040);
          if (offset32 == 0xffffffffu) {
            IACSharpSensor.IACSharpSensor.SensorReached(3041);
            Zip64SeekToCentralDirectory(zf);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(3042);
            zf._OffsetOfCentralDirectory = offset32;
            s.Seek(offset32, SeekOrigin.Begin);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3043);
          ReadCentralDirectory(zf);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(3044);
          s.Seek(0L, SeekOrigin.Begin);
          ReadIntoInstance_Orig(zf);
        }
      } catch (Exception ex1) {
        IACSharpSensor.IACSharpSensor.SensorReached(3045);
        if (zf._ReadStreamIsOurs && zf._readstream != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3046);
          try {
            IACSharpSensor.IACSharpSensor.SensorReached(3047);
            zf._readstream.Dispose();
            zf._readstream = null;
          } finally {
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3048);
        throw new ZipException("Cannot read that as a ZipFile", ex1);
        IACSharpSensor.IACSharpSensor.SensorReached(3049);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3050);
      zf._contentsChanged = false;
      IACSharpSensor.IACSharpSensor.SensorReached(3051);
    }
    private static void Zip64SeekToCentralDirectory(ZipFile zf)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3052);
      Stream s = zf.ReadStream;
      byte[] block = new byte[16];
      s.Seek(-40, SeekOrigin.Current);
      s.Read(block, 0, 16);
      Int64 offset64 = BitConverter.ToInt64(block, 8);
      zf._OffsetOfCentralDirectory = 0xffffffffu;
      zf._OffsetOfCentralDirectory64 = offset64;
      s.Seek(offset64, SeekOrigin.Begin);
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      IACSharpSensor.IACSharpSensor.SensorReached(3053);
      if (datum != ZipConstants.Zip64EndOfCentralDirectoryRecordSignature) {
        IACSharpSensor.IACSharpSensor.SensorReached(3054);
        throw new BadReadException(String.Format("  Bad signature (0x{0:X8}) looking for ZIP64 EoCD Record at position 0x{1:X8}", datum, s.Position));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3055);
      s.Read(block, 0, 8);
      Int64 Size = BitConverter.ToInt64(block, 0);
      block = new byte[Size];
      s.Read(block, 0, block.Length);
      offset64 = BitConverter.ToInt64(block, 36);
      s.Seek(offset64, SeekOrigin.Begin);
      IACSharpSensor.IACSharpSensor.SensorReached(3056);
    }
    private static uint ReadFirstFourBytes(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3057);
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      System.UInt32 RNTRNTRNT_433 = datum;
      IACSharpSensor.IACSharpSensor.SensorReached(3058);
      return RNTRNTRNT_433;
    }
    private static void ReadCentralDirectory(ZipFile zf)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3059);
      bool inputUsesZip64 = false;
      ZipEntry de;
      var previouslySeen = new Dictionary<String, object>();
      IACSharpSensor.IACSharpSensor.SensorReached(3060);
      while ((de = ZipEntry.ReadDirEntry(zf, previouslySeen)) != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3061);
        de.ResetDirEntry();
        zf.OnReadEntry(true, null);
        IACSharpSensor.IACSharpSensor.SensorReached(3062);
        if (zf.Verbose) {
          IACSharpSensor.IACSharpSensor.SensorReached(3063);
          zf.StatusMessageTextWriter.WriteLine("entry {0}", de.FileName);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3064);
        zf._entries.Add(de.FileName, de);
        IACSharpSensor.IACSharpSensor.SensorReached(3065);
        if (de._InputUsesZip64) {
          IACSharpSensor.IACSharpSensor.SensorReached(3066);
          inputUsesZip64 = true;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3067);
        previouslySeen.Add(de.FileName, null);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3068);
      if (inputUsesZip64) {
        IACSharpSensor.IACSharpSensor.SensorReached(3069);
        zf.UseZip64WhenSaving = Zip64Option.Always;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3070);
      if (zf._locEndOfCDS > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3071);
        zf.ReadStream.Seek(zf._locEndOfCDS, SeekOrigin.Begin);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3072);
      ReadCentralDirectoryFooter(zf);
      IACSharpSensor.IACSharpSensor.SensorReached(3073);
      if (zf.Verbose && !String.IsNullOrEmpty(zf.Comment)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3074);
        zf.StatusMessageTextWriter.WriteLine("Zip file Comment: {0}", zf.Comment);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3075);
      if (zf.Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(3076);
        zf.StatusMessageTextWriter.WriteLine("read in {0} entries.", zf._entries.Count);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3077);
      zf.OnReadCompleted();
      IACSharpSensor.IACSharpSensor.SensorReached(3078);
    }
    private static void ReadIntoInstance_Orig(ZipFile zf)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3079);
      zf.OnReadStarted();
      zf._entries = new System.Collections.Generic.Dictionary<String, ZipEntry>();
      ZipEntry e;
      IACSharpSensor.IACSharpSensor.SensorReached(3080);
      if (zf.Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(3081);
        if (zf.Name == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3082);
          zf.StatusMessageTextWriter.WriteLine("Reading zip from stream...");
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(3083);
          zf.StatusMessageTextWriter.WriteLine("Reading zip {0}...", zf.Name);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3084);
      bool firstEntry = true;
      ZipContainer zc = new ZipContainer(zf);
      IACSharpSensor.IACSharpSensor.SensorReached(3085);
      while ((e = ZipEntry.ReadEntry(zc, firstEntry)) != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3086);
        if (zf.Verbose) {
          IACSharpSensor.IACSharpSensor.SensorReached(3087);
          zf.StatusMessageTextWriter.WriteLine("  {0}", e.FileName);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3088);
        zf._entries.Add(e.FileName, e);
        firstEntry = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3089);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3090);
        ZipEntry de;
        var previouslySeen = new Dictionary<String, Object>();
        IACSharpSensor.IACSharpSensor.SensorReached(3091);
        while ((de = ZipEntry.ReadDirEntry(zf, previouslySeen)) != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3092);
          ZipEntry e1 = zf._entries[de.FileName];
          IACSharpSensor.IACSharpSensor.SensorReached(3093);
          if (e1 != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3094);
            e1._Comment = de.Comment;
            IACSharpSensor.IACSharpSensor.SensorReached(3095);
            if (de.IsDirectory) {
              IACSharpSensor.IACSharpSensor.SensorReached(3096);
              e1.MarkAsDirectory();
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3097);
          previouslySeen.Add(de.FileName, null);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3098);
        if (zf._locEndOfCDS > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3099);
          zf.ReadStream.Seek(zf._locEndOfCDS, SeekOrigin.Begin);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3100);
        ReadCentralDirectoryFooter(zf);
        IACSharpSensor.IACSharpSensor.SensorReached(3101);
        if (zf.Verbose && !String.IsNullOrEmpty(zf.Comment)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3102);
          zf.StatusMessageTextWriter.WriteLine("Zip file Comment: {0}", zf.Comment);
        }
      } catch (ZipException) {
      } catch (IOException) {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3103);
      zf.OnReadCompleted();
      IACSharpSensor.IACSharpSensor.SensorReached(3104);
    }
    private static void ReadCentralDirectoryFooter(ZipFile zf)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3105);
      Stream s = zf.ReadStream;
      int signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
      byte[] block = null;
      int j = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3106);
      if (signature == ZipConstants.Zip64EndOfCentralDirectoryRecordSignature) {
        IACSharpSensor.IACSharpSensor.SensorReached(3107);
        block = new byte[8 + 44];
        s.Read(block, 0, block.Length);
        Int64 DataSize = BitConverter.ToInt64(block, 0);
        IACSharpSensor.IACSharpSensor.SensorReached(3108);
        if (DataSize < 44) {
          IACSharpSensor.IACSharpSensor.SensorReached(3109);
          throw new ZipException("Bad size in the ZIP64 Central Directory.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3110);
        zf._versionMadeBy = BitConverter.ToUInt16(block, j);
        j += 2;
        zf._versionNeededToExtract = BitConverter.ToUInt16(block, j);
        j += 2;
        zf._diskNumberWithCd = BitConverter.ToUInt32(block, j);
        j += 2;
        block = new byte[DataSize - 44];
        s.Read(block, 0, block.Length);
        signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
        IACSharpSensor.IACSharpSensor.SensorReached(3111);
        if (signature != ZipConstants.Zip64EndOfCentralDirectoryLocatorSignature) {
          IACSharpSensor.IACSharpSensor.SensorReached(3112);
          throw new ZipException("Inconsistent metadata in the ZIP64 Central Directory.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3113);
        block = new byte[16];
        s.Read(block, 0, block.Length);
        signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3114);
      if (signature != ZipConstants.EndOfCentralDirectorySignature) {
        IACSharpSensor.IACSharpSensor.SensorReached(3115);
        s.Seek(-4, SeekOrigin.Current);
        IACSharpSensor.IACSharpSensor.SensorReached(3116);
        throw new BadReadException(String.Format("Bad signature ({0:X8}) at position 0x{1:X8}", signature, s.Position));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3117);
      block = new byte[16];
      zf.ReadStream.Read(block, 0, block.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(3118);
      if (zf._diskNumberWithCd == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3119);
        zf._diskNumberWithCd = BitConverter.ToUInt16(block, 2);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3120);
      ReadZipFileComment(zf);
      IACSharpSensor.IACSharpSensor.SensorReached(3121);
    }
    private static void ReadZipFileComment(ZipFile zf)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3122);
      byte[] block = new byte[2];
      zf.ReadStream.Read(block, 0, block.Length);
      Int16 commentLength = (short)(block[0] + block[1] * 256);
      IACSharpSensor.IACSharpSensor.SensorReached(3123);
      if (commentLength > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3124);
        block = new byte[commentLength];
        zf.ReadStream.Read(block, 0, block.Length);
        string s1 = zf.AlternateEncoding.GetString(block, 0, block.Length);
        zf.Comment = s1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3125);
    }
    public static bool IsZipFile(string fileName)
    {
      System.Boolean RNTRNTRNT_434 = IsZipFile(fileName, false);
      IACSharpSensor.IACSharpSensor.SensorReached(3126);
      return RNTRNTRNT_434;
    }
    public static bool IsZipFile(string fileName, bool testExtract)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3127);
      bool result = false;
      IACSharpSensor.IACSharpSensor.SensorReached(3128);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3129);
        if (!File.Exists(fileName)) {
          System.Boolean RNTRNTRNT_435 = false;
          IACSharpSensor.IACSharpSensor.SensorReached(3130);
          return RNTRNTRNT_435;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3131);
        using (var s = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3132);
          result = IsZipFile(s, testExtract);
        }
      } catch (IOException) {
      } catch (ZipException) {
      }
      System.Boolean RNTRNTRNT_436 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(3133);
      return RNTRNTRNT_436;
    }
    public static bool IsZipFile(Stream stream, bool testExtract)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3134);
      if (stream == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3135);
        throw new ArgumentNullException("stream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3136);
      bool result = false;
      IACSharpSensor.IACSharpSensor.SensorReached(3137);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3138);
        if (!stream.CanRead) {
          System.Boolean RNTRNTRNT_437 = false;
          IACSharpSensor.IACSharpSensor.SensorReached(3139);
          return RNTRNTRNT_437;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3140);
        var bitBucket = Stream.Null;
        IACSharpSensor.IACSharpSensor.SensorReached(3141);
        using (ZipFile zip1 = ZipFile.Read(stream, null, null, null)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3142);
          if (testExtract) {
            IACSharpSensor.IACSharpSensor.SensorReached(3143);
            foreach (var e in zip1) {
              IACSharpSensor.IACSharpSensor.SensorReached(3144);
              if (!e.IsDirectory) {
                IACSharpSensor.IACSharpSensor.SensorReached(3145);
                e.Extract(bitBucket);
              }
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3146);
        result = true;
      } catch (IOException) {
      } catch (ZipException) {
      }
      System.Boolean RNTRNTRNT_438 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(3147);
      return RNTRNTRNT_438;
    }
  }
}
