using System;
using System.IO;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Collections.Generic;
namespace Ionic
{
  internal enum LogicalConjunction
  {
    NONE,
    AND,
    OR,
    XOR
  }
  internal enum WhichTime
  {
    atime,
    mtime,
    ctime
  }
  internal enum ComparisonOperator
  {
    [Description(">")]
    GreaterThan,
    [Description(">=")]
    GreaterThanOrEqualTo,
    [Description("<")]
    LesserThan,
    [Description("<=")]
    LesserThanOrEqualTo,
    [Description("=")]
    EqualTo,
    [Description("!=")]
    NotEqualTo
  }
  internal abstract partial class SelectionCriterion
  {
    internal virtual bool Verbose { get; set; }
    internal abstract bool Evaluate(string filename);
    [System.Diagnostics.Conditional("SelectorTrace")]
    protected static void CriterionTrace(string format, params object[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(422);
    }
  }
  internal partial class SizeCriterion : SelectionCriterion
  {
    internal ComparisonOperator Operator;
    internal Int64 Size;
    public override String ToString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(423);
      StringBuilder sb = new StringBuilder();
      sb.Append("size ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(Size.ToString());
      String RNTRNTRNT_100 = sb.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(424);
      return RNTRNTRNT_100;
    }
    internal override bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(425);
      System.IO.FileInfo fi = new System.IO.FileInfo(filename);
      CriterionTrace("SizeCriterion::Evaluate('{0}' [{1}])", filename, this.ToString());
      System.Boolean RNTRNTRNT_101 = _Evaluate(fi.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(426);
      return RNTRNTRNT_101;
    }
    private bool _Evaluate(Int64 Length)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(427);
      bool result = false;
      IACSharpSensor.IACSharpSensor.SensorReached(428);
      switch (Operator) {
        case ComparisonOperator.GreaterThanOrEqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(429);
          result = Length >= Size;
          IACSharpSensor.IACSharpSensor.SensorReached(430);
          break;
        case ComparisonOperator.GreaterThan:
          IACSharpSensor.IACSharpSensor.SensorReached(431);
          result = Length > Size;
          IACSharpSensor.IACSharpSensor.SensorReached(432);
          break;
        case ComparisonOperator.LesserThanOrEqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(433);
          result = Length <= Size;
          IACSharpSensor.IACSharpSensor.SensorReached(434);
          break;
        case ComparisonOperator.LesserThan:
          IACSharpSensor.IACSharpSensor.SensorReached(435);
          result = Length < Size;
          IACSharpSensor.IACSharpSensor.SensorReached(436);
          break;
        case ComparisonOperator.EqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(437);
          result = Length == Size;
          IACSharpSensor.IACSharpSensor.SensorReached(438);
          break;
        case ComparisonOperator.NotEqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(439);
          result = Length != Size;
          IACSharpSensor.IACSharpSensor.SensorReached(440);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(441);
          throw new ArgumentException("Operator");
      }
      System.Boolean RNTRNTRNT_102 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(442);
      return RNTRNTRNT_102;
    }
  }
  internal partial class TimeCriterion : SelectionCriterion
  {
    internal ComparisonOperator Operator;
    internal WhichTime Which;
    internal DateTime Time;
    public override String ToString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(443);
      StringBuilder sb = new StringBuilder();
      sb.Append(Which.ToString()).Append(" ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(Time.ToString("yyyy-MM-dd-HH:mm:ss"));
      String RNTRNTRNT_103 = sb.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(444);
      return RNTRNTRNT_103;
    }
    internal override bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(445);
      DateTime x;
      IACSharpSensor.IACSharpSensor.SensorReached(446);
      switch (Which) {
        case WhichTime.atime:
          IACSharpSensor.IACSharpSensor.SensorReached(447);
          x = System.IO.File.GetLastAccessTime(filename).ToUniversalTime();
          IACSharpSensor.IACSharpSensor.SensorReached(448);
          break;
        case WhichTime.mtime:
          IACSharpSensor.IACSharpSensor.SensorReached(449);
          x = System.IO.File.GetLastWriteTime(filename).ToUniversalTime();
          IACSharpSensor.IACSharpSensor.SensorReached(450);
          break;
        case WhichTime.ctime:
          IACSharpSensor.IACSharpSensor.SensorReached(451);
          x = System.IO.File.GetCreationTime(filename).ToUniversalTime();
          IACSharpSensor.IACSharpSensor.SensorReached(452);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(453);
          throw new ArgumentException("Operator");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(454);
      CriterionTrace("TimeCriterion({0},{1})= {2}", filename, Which.ToString(), x);
      System.Boolean RNTRNTRNT_104 = _Evaluate(x);
      IACSharpSensor.IACSharpSensor.SensorReached(455);
      return RNTRNTRNT_104;
    }
    private bool _Evaluate(DateTime x)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(456);
      bool result = false;
      IACSharpSensor.IACSharpSensor.SensorReached(457);
      switch (Operator) {
        case ComparisonOperator.GreaterThanOrEqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(458);
          result = (x >= Time);
          IACSharpSensor.IACSharpSensor.SensorReached(459);
          break;
        case ComparisonOperator.GreaterThan:
          IACSharpSensor.IACSharpSensor.SensorReached(460);
          result = (x > Time);
          IACSharpSensor.IACSharpSensor.SensorReached(461);
          break;
        case ComparisonOperator.LesserThanOrEqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(462);
          result = (x <= Time);
          IACSharpSensor.IACSharpSensor.SensorReached(463);
          break;
        case ComparisonOperator.LesserThan:
          IACSharpSensor.IACSharpSensor.SensorReached(464);
          result = (x < Time);
          IACSharpSensor.IACSharpSensor.SensorReached(465);
          break;
        case ComparisonOperator.EqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(466);
          result = (x == Time);
          IACSharpSensor.IACSharpSensor.SensorReached(467);
          break;
        case ComparisonOperator.NotEqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(468);
          result = (x != Time);
          IACSharpSensor.IACSharpSensor.SensorReached(469);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(470);
          throw new ArgumentException("Operator");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(471);
      CriterionTrace("TimeCriterion: {0}", result);
      System.Boolean RNTRNTRNT_105 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(472);
      return RNTRNTRNT_105;
    }
  }
  internal partial class NameCriterion : SelectionCriterion
  {
    private Regex _re;
    private String _regexString;
    internal ComparisonOperator Operator;
    private string _MatchingFileSpec;
    internal virtual string MatchingFileSpec {
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(473);
        if (Directory.Exists(value)) {
          IACSharpSensor.IACSharpSensor.SensorReached(474);
          _MatchingFileSpec = ".\\" + value + "\\*.*";
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(475);
          _MatchingFileSpec = value;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(476);
        _regexString = "^" + Regex.Escape(_MatchingFileSpec).Replace("\\\\\\*\\.\\*", "\\\\([^\\.]+|.*\\.[^\\\\\\.]*)").Replace("\\.\\*", "\\.[^\\\\\\.]*").Replace("\\*", ".*").Replace("\\?", "[^\\\\\\.]") + "$";
        CriterionTrace("NameCriterion regexString({0})", _regexString);
        _re = new Regex(_regexString, RegexOptions.IgnoreCase);
        IACSharpSensor.IACSharpSensor.SensorReached(477);
      }
    }
    public override String ToString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(478);
      StringBuilder sb = new StringBuilder();
      sb.Append("name ").Append(EnumUtil.GetDescription(Operator)).Append(" '").Append(_MatchingFileSpec).Append("'");
      String RNTRNTRNT_106 = sb.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(479);
      return RNTRNTRNT_106;
    }
    internal override bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(480);
      CriterionTrace("NameCriterion::Evaluate('{0}' pattern[{1}])", filename, _MatchingFileSpec);
      System.Boolean RNTRNTRNT_107 = _Evaluate(filename);
      IACSharpSensor.IACSharpSensor.SensorReached(481);
      return RNTRNTRNT_107;
    }
    private bool _Evaluate(string fullpath)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(482);
      CriterionTrace("NameCriterion::Evaluate({0})", fullpath);
      String f = (_MatchingFileSpec.IndexOf('\\') == -1) ? System.IO.Path.GetFileName(fullpath) : fullpath;
      bool result = _re.IsMatch(f);
      IACSharpSensor.IACSharpSensor.SensorReached(483);
      if (Operator != ComparisonOperator.EqualTo) {
        IACSharpSensor.IACSharpSensor.SensorReached(484);
        result = !result;
      }
      System.Boolean RNTRNTRNT_108 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(485);
      return RNTRNTRNT_108;
    }
  }
  internal partial class TypeCriterion : SelectionCriterion
  {
    private char ObjectType;
    internal ComparisonOperator Operator;
    internal string AttributeString {
      get {
        System.String RNTRNTRNT_109 = ObjectType.ToString();
        IACSharpSensor.IACSharpSensor.SensorReached(486);
        return RNTRNTRNT_109;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(487);
        if (value.Length != 1 || (value[0] != 'D' && value[0] != 'F')) {
          IACSharpSensor.IACSharpSensor.SensorReached(488);
          throw new ArgumentException("Specify a single character: either D or F");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(489);
        ObjectType = value[0];
        IACSharpSensor.IACSharpSensor.SensorReached(490);
      }
    }
    public override String ToString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(491);
      StringBuilder sb = new StringBuilder();
      sb.Append("type ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(AttributeString);
      String RNTRNTRNT_110 = sb.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(492);
      return RNTRNTRNT_110;
    }
    internal override bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(493);
      CriterionTrace("TypeCriterion::Evaluate({0})", filename);
      bool result = (ObjectType == 'D') ? Directory.Exists(filename) : File.Exists(filename);
      IACSharpSensor.IACSharpSensor.SensorReached(494);
      if (Operator != ComparisonOperator.EqualTo) {
        IACSharpSensor.IACSharpSensor.SensorReached(495);
        result = !result;
      }
      System.Boolean RNTRNTRNT_111 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(496);
      return RNTRNTRNT_111;
    }
  }
  internal partial class AttributesCriterion : SelectionCriterion
  {
    private FileAttributes _Attributes;
    internal ComparisonOperator Operator;
    internal string AttributeString {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(497);
        string result = "";
        IACSharpSensor.IACSharpSensor.SensorReached(498);
        if ((_Attributes & FileAttributes.Hidden) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(499);
          result += "H";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(500);
        if ((_Attributes & FileAttributes.System) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(501);
          result += "S";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(502);
        if ((_Attributes & FileAttributes.ReadOnly) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(503);
          result += "R";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(504);
        if ((_Attributes & FileAttributes.Archive) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(505);
          result += "A";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(506);
        if ((_Attributes & FileAttributes.ReparsePoint) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(507);
          result += "L";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(508);
        if ((_Attributes & FileAttributes.NotContentIndexed) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(509);
          result += "I";
        }
        System.String RNTRNTRNT_112 = result;
        IACSharpSensor.IACSharpSensor.SensorReached(510);
        return RNTRNTRNT_112;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(511);
        _Attributes = FileAttributes.Normal;
        IACSharpSensor.IACSharpSensor.SensorReached(512);
        foreach (char c in value.ToUpper()) {
          IACSharpSensor.IACSharpSensor.SensorReached(513);
          switch (c) {
            case 'H':
              IACSharpSensor.IACSharpSensor.SensorReached(514);
              if ((_Attributes & FileAttributes.Hidden) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(515);
                throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(516);
              _Attributes |= FileAttributes.Hidden;
              IACSharpSensor.IACSharpSensor.SensorReached(517);
              break;
            case 'R':
              IACSharpSensor.IACSharpSensor.SensorReached(518);
              if ((_Attributes & FileAttributes.ReadOnly) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(519);
                throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(520);
              _Attributes |= FileAttributes.ReadOnly;
              IACSharpSensor.IACSharpSensor.SensorReached(521);
              break;
            case 'S':
              IACSharpSensor.IACSharpSensor.SensorReached(522);
              if ((_Attributes & FileAttributes.System) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(523);
                throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(524);
              _Attributes |= FileAttributes.System;
              IACSharpSensor.IACSharpSensor.SensorReached(525);
              break;
            case 'A':
              IACSharpSensor.IACSharpSensor.SensorReached(526);
              if ((_Attributes & FileAttributes.Archive) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(527);
                throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(528);
              _Attributes |= FileAttributes.Archive;
              IACSharpSensor.IACSharpSensor.SensorReached(529);
              break;
            case 'I':
              IACSharpSensor.IACSharpSensor.SensorReached(530);
              if ((_Attributes & FileAttributes.NotContentIndexed) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(531);
                throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(532);
              _Attributes |= FileAttributes.NotContentIndexed;
              IACSharpSensor.IACSharpSensor.SensorReached(533);
              break;
            case 'L':
              IACSharpSensor.IACSharpSensor.SensorReached(534);
              if ((_Attributes & FileAttributes.ReparsePoint) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(535);
                throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(536);
              _Attributes |= FileAttributes.ReparsePoint;
              IACSharpSensor.IACSharpSensor.SensorReached(537);
              break;
            default:
              IACSharpSensor.IACSharpSensor.SensorReached(538);
              throw new ArgumentException(value);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(539);
      }
    }
    public override String ToString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(540);
      StringBuilder sb = new StringBuilder();
      sb.Append("attributes ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(AttributeString);
      String RNTRNTRNT_113 = sb.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(541);
      return RNTRNTRNT_113;
    }
    private bool _EvaluateOne(FileAttributes fileAttrs, FileAttributes criterionAttrs)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(542);
      bool result = false;
      IACSharpSensor.IACSharpSensor.SensorReached(543);
      if ((_Attributes & criterionAttrs) == criterionAttrs) {
        IACSharpSensor.IACSharpSensor.SensorReached(544);
        result = ((fileAttrs & criterionAttrs) == criterionAttrs);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(545);
        result = true;
      }
      System.Boolean RNTRNTRNT_114 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(546);
      return RNTRNTRNT_114;
    }
    internal override bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(547);
      if (Directory.Exists(filename)) {
        System.Boolean RNTRNTRNT_115 = (Operator != ComparisonOperator.EqualTo);
        IACSharpSensor.IACSharpSensor.SensorReached(548);
        return RNTRNTRNT_115;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(549);
      FileAttributes fileAttrs = System.IO.File.GetAttributes(filename);
      System.Boolean RNTRNTRNT_116 = _Evaluate(fileAttrs);
      IACSharpSensor.IACSharpSensor.SensorReached(550);
      return RNTRNTRNT_116;
    }
    private bool _Evaluate(FileAttributes fileAttrs)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(551);
      bool result = _EvaluateOne(fileAttrs, FileAttributes.Hidden);
      IACSharpSensor.IACSharpSensor.SensorReached(552);
      if (result) {
        IACSharpSensor.IACSharpSensor.SensorReached(553);
        result = _EvaluateOne(fileAttrs, FileAttributes.System);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(554);
      if (result) {
        IACSharpSensor.IACSharpSensor.SensorReached(555);
        result = _EvaluateOne(fileAttrs, FileAttributes.ReadOnly);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(556);
      if (result) {
        IACSharpSensor.IACSharpSensor.SensorReached(557);
        result = _EvaluateOne(fileAttrs, FileAttributes.Archive);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(558);
      if (result) {
        IACSharpSensor.IACSharpSensor.SensorReached(559);
        result = _EvaluateOne(fileAttrs, FileAttributes.NotContentIndexed);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(560);
      if (result) {
        IACSharpSensor.IACSharpSensor.SensorReached(561);
        result = _EvaluateOne(fileAttrs, FileAttributes.ReparsePoint);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(562);
      if (Operator != ComparisonOperator.EqualTo) {
        IACSharpSensor.IACSharpSensor.SensorReached(563);
        result = !result;
      }
      System.Boolean RNTRNTRNT_117 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(564);
      return RNTRNTRNT_117;
    }
  }
  internal partial class CompoundCriterion : SelectionCriterion
  {
    internal LogicalConjunction Conjunction;
    internal SelectionCriterion Left;
    private SelectionCriterion _Right;
    internal SelectionCriterion Right {
      get {
        SelectionCriterion RNTRNTRNT_118 = _Right;
        IACSharpSensor.IACSharpSensor.SensorReached(565);
        return RNTRNTRNT_118;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(566);
        _Right = value;
        IACSharpSensor.IACSharpSensor.SensorReached(567);
        if (value == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(568);
          Conjunction = LogicalConjunction.NONE;
        } else if (Conjunction == LogicalConjunction.NONE) {
          IACSharpSensor.IACSharpSensor.SensorReached(569);
          Conjunction = LogicalConjunction.AND;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(570);
      }
    }
    internal override bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(571);
      bool result = Left.Evaluate(filename);
      IACSharpSensor.IACSharpSensor.SensorReached(572);
      switch (Conjunction) {
        case LogicalConjunction.AND:
          IACSharpSensor.IACSharpSensor.SensorReached(573);
          if (result) {
            IACSharpSensor.IACSharpSensor.SensorReached(574);
            result = Right.Evaluate(filename);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(575);
          break;
        case LogicalConjunction.OR:
          IACSharpSensor.IACSharpSensor.SensorReached(576);
          if (!result) {
            IACSharpSensor.IACSharpSensor.SensorReached(577);
            result = Right.Evaluate(filename);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(578);
          break;
        case LogicalConjunction.XOR:
          IACSharpSensor.IACSharpSensor.SensorReached(579);
          result ^= Right.Evaluate(filename);
          IACSharpSensor.IACSharpSensor.SensorReached(580);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(581);
          throw new ArgumentException("Conjunction");
      }
      System.Boolean RNTRNTRNT_119 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(582);
      return RNTRNTRNT_119;
    }
    public override String ToString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(583);
      StringBuilder sb = new StringBuilder();
      sb.Append("(").Append((Left != null) ? Left.ToString() : "null").Append(" ").Append(Conjunction.ToString()).Append(" ").Append((Right != null) ? Right.ToString() : "null").Append(")");
      String RNTRNTRNT_120 = sb.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(584);
      return RNTRNTRNT_120;
    }
  }
  public partial class FileSelector
  {
    internal SelectionCriterion _Criterion;
    public FileSelector(String selectionCriteria) : this(selectionCriteria, true)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(585);
    }
    public FileSelector(String selectionCriteria, bool traverseDirectoryReparsePoints)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(586);
      if (!String.IsNullOrEmpty(selectionCriteria)) {
        IACSharpSensor.IACSharpSensor.SensorReached(587);
        _Criterion = _ParseCriterion(selectionCriteria);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(588);
      TraverseReparsePoints = traverseDirectoryReparsePoints;
      IACSharpSensor.IACSharpSensor.SensorReached(589);
    }
    public String SelectionCriteria {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(590);
        if (_Criterion == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(591);
          return null;
        }
        String RNTRNTRNT_121 = _Criterion.ToString();
        IACSharpSensor.IACSharpSensor.SensorReached(592);
        return RNTRNTRNT_121;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(593);
        if (value == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(594);
          _Criterion = null;
        } else if (value.Trim() == "") {
          IACSharpSensor.IACSharpSensor.SensorReached(596);
          _Criterion = null;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(595);
          _Criterion = _ParseCriterion(value);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(597);
      }
    }
    public bool TraverseReparsePoints { get; set; }
    private enum ParseState
    {
      Start,
      OpenParen,
      CriterionDone,
      ConjunctionPending,
      Whitespace
    }
    private static class RegexAssertions
    {
      public static readonly String PrecededByOddNumberOfSingleQuotes = "(?<=(?:[^']*'[^']*')*'[^']*)";
      public static readonly String FollowedByOddNumberOfSingleQuotesAndLineEnd = "(?=[^']*'(?:[^']*'[^']*')*[^']*$)";
      public static readonly String PrecededByEvenNumberOfSingleQuotes = "(?<=(?:[^']*'[^']*')*[^']*)";
      public static readonly String FollowedByEvenNumberOfSingleQuotesAndLineEnd = "(?=(?:[^']*'[^']*')*[^']*$)";
    }
    private static string NormalizeCriteriaExpression(string source)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(598);
      string[][] prPairs = {
        new string[] {
          "([^']*)\\(\\(([^']+)",
          "$1( ($2"
        },
        new string[] {
          "(.)\\)\\)",
          "$1) )"
        },
        new string[] {
          "\\((\\S)",
          "( $1"
        },
        new string[] {
          "(\\S)\\)",
          "$1 )"
        },
        new string[] {
          "^\\)",
          " )"
        },
        new string[] {
          "(\\S)\\(",
          "$1 ("
        },
        new string[] {
          "\\)(\\S)",
          ") $1"
        },
        new string[] {
          "(=)('[^']*')",
          "$1 $2"
        },
        new string[] {
          "([^ !><])(>|<|!=|=)",
          "$1 $2"
        },
        new string[] {
          "(>|<|!=|=)([^ =])",
          "$1 $2"
        },
        new string[] {
          "/",
          "\\"
        }
      };
      string interim = source;
      IACSharpSensor.IACSharpSensor.SensorReached(599);
      for (int i = 0; i < prPairs.Length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(600);
        string pattern = RegexAssertions.PrecededByEvenNumberOfSingleQuotes + prPairs[i][0] + RegexAssertions.FollowedByEvenNumberOfSingleQuotesAndLineEnd;
        interim = Regex.Replace(interim, pattern, prPairs[i][1]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(601);
      var regexPattern = "/" + RegexAssertions.FollowedByOddNumberOfSingleQuotesAndLineEnd;
      interim = Regex.Replace(interim, regexPattern, "\\");
      regexPattern = " " + RegexAssertions.FollowedByOddNumberOfSingleQuotesAndLineEnd;
      System.String RNTRNTRNT_122 = Regex.Replace(interim, regexPattern, "\u0006");
      IACSharpSensor.IACSharpSensor.SensorReached(602);
      return RNTRNTRNT_122;
    }
    private static SelectionCriterion _ParseCriterion(String s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(603);
      if (s == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(604);
        return null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(605);
      s = NormalizeCriteriaExpression(s);
      IACSharpSensor.IACSharpSensor.SensorReached(606);
      if (s.IndexOf(" ") == -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(607);
        s = "name = " + s;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(608);
      string[] tokens = s.Trim().Split(' ', '\t');
      IACSharpSensor.IACSharpSensor.SensorReached(609);
      if (tokens.Length < 3) {
        IACSharpSensor.IACSharpSensor.SensorReached(610);
        throw new ArgumentException(s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(611);
      SelectionCriterion current = null;
      LogicalConjunction pendingConjunction = LogicalConjunction.NONE;
      ParseState state;
      var stateStack = new System.Collections.Generic.Stack<ParseState>();
      var critStack = new System.Collections.Generic.Stack<SelectionCriterion>();
      stateStack.Push(ParseState.Start);
      IACSharpSensor.IACSharpSensor.SensorReached(612);
      for (int i = 0; i < tokens.Length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(613);
        string tok1 = tokens[i].ToLower();
        IACSharpSensor.IACSharpSensor.SensorReached(614);
        switch (tok1) {
          case "and":
          case "xor":
          case "or":
            IACSharpSensor.IACSharpSensor.SensorReached(615);
            state = stateStack.Peek();
            IACSharpSensor.IACSharpSensor.SensorReached(616);
            if (state != ParseState.CriterionDone) {
              IACSharpSensor.IACSharpSensor.SensorReached(617);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(618);
            if (tokens.Length <= i + 3) {
              IACSharpSensor.IACSharpSensor.SensorReached(619);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(620);
            pendingConjunction = (LogicalConjunction)Enum.Parse(typeof(LogicalConjunction), tokens[i].ToUpper(), true);
            current = new CompoundCriterion {
              Left = current,
              Right = null,
              Conjunction = pendingConjunction
            };
            stateStack.Push(state);
            stateStack.Push(ParseState.ConjunctionPending);
            critStack.Push(current);
            IACSharpSensor.IACSharpSensor.SensorReached(621);
            break;
          case "(":
            IACSharpSensor.IACSharpSensor.SensorReached(622);
            state = stateStack.Peek();
            IACSharpSensor.IACSharpSensor.SensorReached(623);
            if (state != ParseState.Start && state != ParseState.ConjunctionPending && state != ParseState.OpenParen) {
              IACSharpSensor.IACSharpSensor.SensorReached(624);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(625);
            if (tokens.Length <= i + 4) {
              IACSharpSensor.IACSharpSensor.SensorReached(626);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(627);
            stateStack.Push(ParseState.OpenParen);
            IACSharpSensor.IACSharpSensor.SensorReached(628);
            break;
          case ")":
            IACSharpSensor.IACSharpSensor.SensorReached(629);
            state = stateStack.Pop();
            IACSharpSensor.IACSharpSensor.SensorReached(630);
            if (stateStack.Peek() != ParseState.OpenParen) {
              IACSharpSensor.IACSharpSensor.SensorReached(631);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(632);
            stateStack.Pop();
            stateStack.Push(ParseState.CriterionDone);
            IACSharpSensor.IACSharpSensor.SensorReached(633);
            break;
          case "atime":
          case "ctime":
          case "mtime":
            IACSharpSensor.IACSharpSensor.SensorReached(634);
            if (tokens.Length <= i + 2) {
              IACSharpSensor.IACSharpSensor.SensorReached(635);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(636);
            DateTime t;
            IACSharpSensor.IACSharpSensor.SensorReached(637);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(638);
              t = DateTime.ParseExact(tokens[i + 2], "yyyy-MM-dd-HH:mm:ss", null);
            } catch (FormatException) {
              IACSharpSensor.IACSharpSensor.SensorReached(639);
              try {
                IACSharpSensor.IACSharpSensor.SensorReached(640);
                t = DateTime.ParseExact(tokens[i + 2], "yyyy/MM/dd-HH:mm:ss", null);
              } catch (FormatException) {
                IACSharpSensor.IACSharpSensor.SensorReached(641);
                try {
                  IACSharpSensor.IACSharpSensor.SensorReached(642);
                  t = DateTime.ParseExact(tokens[i + 2], "yyyy/MM/dd", null);
                } catch (FormatException) {
                  IACSharpSensor.IACSharpSensor.SensorReached(643);
                  try {
                    IACSharpSensor.IACSharpSensor.SensorReached(644);
                    t = DateTime.ParseExact(tokens[i + 2], "MM/dd/yyyy", null);
                  } catch (FormatException) {
                    IACSharpSensor.IACSharpSensor.SensorReached(645);
                    t = DateTime.ParseExact(tokens[i + 2], "yyyy-MM-dd", null);
                    IACSharpSensor.IACSharpSensor.SensorReached(646);
                  }
                  IACSharpSensor.IACSharpSensor.SensorReached(647);
                }
                IACSharpSensor.IACSharpSensor.SensorReached(648);
              }
              IACSharpSensor.IACSharpSensor.SensorReached(649);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(650);
            t = DateTime.SpecifyKind(t, DateTimeKind.Local).ToUniversalTime();
            current = new TimeCriterion {
              Which = (WhichTime)Enum.Parse(typeof(WhichTime), tokens[i], true),
              Operator = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1]),
              Time = t
            };
            i += 2;
            stateStack.Push(ParseState.CriterionDone);
            IACSharpSensor.IACSharpSensor.SensorReached(651);
            break;
          case "length":
          case "size":
            IACSharpSensor.IACSharpSensor.SensorReached(652);
            if (tokens.Length <= i + 2) {
              IACSharpSensor.IACSharpSensor.SensorReached(653);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(654);
            Int64 sz = 0;
            string v = tokens[i + 2];
            IACSharpSensor.IACSharpSensor.SensorReached(655);
            if (v.ToUpper().EndsWith("K")) {
              IACSharpSensor.IACSharpSensor.SensorReached(656);
              sz = Int64.Parse(v.Substring(0, v.Length - 1)) * 1024;
            } else if (v.ToUpper().EndsWith("KB")) {
              IACSharpSensor.IACSharpSensor.SensorReached(662);
              sz = Int64.Parse(v.Substring(0, v.Length - 2)) * 1024;
            } else if (v.ToUpper().EndsWith("M")) {
              IACSharpSensor.IACSharpSensor.SensorReached(661);
              sz = Int64.Parse(v.Substring(0, v.Length - 1)) * 1024 * 1024;
            } else if (v.ToUpper().EndsWith("MB")) {
              IACSharpSensor.IACSharpSensor.SensorReached(660);
              sz = Int64.Parse(v.Substring(0, v.Length - 2)) * 1024 * 1024;
            } else if (v.ToUpper().EndsWith("G")) {
              IACSharpSensor.IACSharpSensor.SensorReached(659);
              sz = Int64.Parse(v.Substring(0, v.Length - 1)) * 1024 * 1024 * 1024;
            } else if (v.ToUpper().EndsWith("GB")) {
              IACSharpSensor.IACSharpSensor.SensorReached(658);
              sz = Int64.Parse(v.Substring(0, v.Length - 2)) * 1024 * 1024 * 1024;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(657);
              sz = Int64.Parse(tokens[i + 2]);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(663);
            current = new SizeCriterion {
              Size = sz,
              Operator = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1])
            };
            i += 2;
            stateStack.Push(ParseState.CriterionDone);
            IACSharpSensor.IACSharpSensor.SensorReached(664);
            break;
          case "filename":
          case "name":
            
            {
              IACSharpSensor.IACSharpSensor.SensorReached(665);
              if (tokens.Length <= i + 2) {
                IACSharpSensor.IACSharpSensor.SensorReached(666);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              IACSharpSensor.IACSharpSensor.SensorReached(667);
              ComparisonOperator c = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1]);
              IACSharpSensor.IACSharpSensor.SensorReached(668);
              if (c != ComparisonOperator.NotEqualTo && c != ComparisonOperator.EqualTo) {
                IACSharpSensor.IACSharpSensor.SensorReached(669);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              IACSharpSensor.IACSharpSensor.SensorReached(670);
              string m = tokens[i + 2];
              IACSharpSensor.IACSharpSensor.SensorReached(671);
              if (m.StartsWith("'") && m.EndsWith("'")) {
                IACSharpSensor.IACSharpSensor.SensorReached(672);
                m = m.Substring(1, m.Length - 2).Replace("\u0006", " ");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(673);
              current = new NameCriterion {
                MatchingFileSpec = m,
                Operator = c
              };
              i += 2;
              stateStack.Push(ParseState.CriterionDone);
              IACSharpSensor.IACSharpSensor.SensorReached(674);
              break;
            }

          case "attrs":
          case "attributes":
          case "type":
            
            {
              IACSharpSensor.IACSharpSensor.SensorReached(675);
              if (tokens.Length <= i + 2) {
                IACSharpSensor.IACSharpSensor.SensorReached(676);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              IACSharpSensor.IACSharpSensor.SensorReached(677);
              ComparisonOperator c = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1]);
              IACSharpSensor.IACSharpSensor.SensorReached(678);
              if (c != ComparisonOperator.NotEqualTo && c != ComparisonOperator.EqualTo) {
                IACSharpSensor.IACSharpSensor.SensorReached(679);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              IACSharpSensor.IACSharpSensor.SensorReached(680);
              current = (tok1 == "type") ? (SelectionCriterion)new TypeCriterion {
                AttributeString = tokens[i + 2],
                Operator = c
              } : (SelectionCriterion)new AttributesCriterion {
                AttributeString = tokens[i + 2],
                Operator = c
              };
              i += 2;
              stateStack.Push(ParseState.CriterionDone);
              IACSharpSensor.IACSharpSensor.SensorReached(681);
              break;
            }

          case "":
            IACSharpSensor.IACSharpSensor.SensorReached(682);
            stateStack.Push(ParseState.Whitespace);
            IACSharpSensor.IACSharpSensor.SensorReached(683);
            break;
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(684);
            throw new ArgumentException("'" + tokens[i] + "'");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(685);
        state = stateStack.Peek();
        IACSharpSensor.IACSharpSensor.SensorReached(686);
        if (state == ParseState.CriterionDone) {
          IACSharpSensor.IACSharpSensor.SensorReached(687);
          stateStack.Pop();
          IACSharpSensor.IACSharpSensor.SensorReached(688);
          if (stateStack.Peek() == ParseState.ConjunctionPending) {
            IACSharpSensor.IACSharpSensor.SensorReached(689);
            while (stateStack.Peek() == ParseState.ConjunctionPending) {
              IACSharpSensor.IACSharpSensor.SensorReached(690);
              var cc = critStack.Pop() as CompoundCriterion;
              cc.Right = current;
              current = cc;
              stateStack.Pop();
              state = stateStack.Pop();
              IACSharpSensor.IACSharpSensor.SensorReached(691);
              if (state != ParseState.CriterionDone) {
                IACSharpSensor.IACSharpSensor.SensorReached(692);
                throw new ArgumentException("??");
              }
            }
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(693);
            stateStack.Push(ParseState.CriterionDone);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(694);
        if (state == ParseState.Whitespace) {
          IACSharpSensor.IACSharpSensor.SensorReached(695);
          stateStack.Pop();
        }
      }
      SelectionCriterion RNTRNTRNT_123 = current;
      IACSharpSensor.IACSharpSensor.SensorReached(696);
      return RNTRNTRNT_123;
    }
    public override String ToString()
    {
      String RNTRNTRNT_124 = "FileSelector(" + _Criterion.ToString() + ")";
      IACSharpSensor.IACSharpSensor.SensorReached(697);
      return RNTRNTRNT_124;
    }
    private bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(698);
      SelectorTrace("Evaluate({0})", filename);
      bool result = _Criterion.Evaluate(filename);
      System.Boolean RNTRNTRNT_125 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(699);
      return RNTRNTRNT_125;
    }
    [System.Diagnostics.Conditional("SelectorTrace")]
    private void SelectorTrace(string format, params object[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(700);
      if (_Criterion != null && _Criterion.Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(701);
        System.Console.WriteLine(format, args);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(702);
    }
    public System.Collections.Generic.ICollection<String> SelectFiles(String directory)
    {
      System.Collections.Generic.ICollection<String> RNTRNTRNT_126 = SelectFiles(directory, false);
      IACSharpSensor.IACSharpSensor.SensorReached(703);
      return RNTRNTRNT_126;
    }
    public System.Collections.ObjectModel.ReadOnlyCollection<String> SelectFiles(String directory, bool recurseDirectories)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(704);
      if (_Criterion == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(705);
        throw new ArgumentException("SelectionCriteria has not been set");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(706);
      var list = new List<String>();
      IACSharpSensor.IACSharpSensor.SensorReached(707);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(708);
        if (Directory.Exists(directory)) {
          IACSharpSensor.IACSharpSensor.SensorReached(709);
          String[] filenames = Directory.GetFiles(directory);
          IACSharpSensor.IACSharpSensor.SensorReached(710);
          foreach (String filename in filenames) {
            IACSharpSensor.IACSharpSensor.SensorReached(711);
            if (Evaluate(filename)) {
              IACSharpSensor.IACSharpSensor.SensorReached(712);
              list.Add(filename);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(713);
          if (recurseDirectories) {
            IACSharpSensor.IACSharpSensor.SensorReached(714);
            String[] dirnames = Directory.GetDirectories(directory);
            IACSharpSensor.IACSharpSensor.SensorReached(715);
            foreach (String dir in dirnames) {
              IACSharpSensor.IACSharpSensor.SensorReached(716);
              if (this.TraverseReparsePoints || ((File.GetAttributes(dir) & FileAttributes.ReparsePoint) == 0)) {
                IACSharpSensor.IACSharpSensor.SensorReached(717);
                if (Evaluate(dir)) {
                  IACSharpSensor.IACSharpSensor.SensorReached(718);
                  list.Add(dir);
                }
                IACSharpSensor.IACSharpSensor.SensorReached(719);
                list.AddRange(this.SelectFiles(dir, recurseDirectories));
              }
            }
          }
        }
      } catch (System.UnauthorizedAccessException) {
      } catch (System.IO.IOException) {
      }
      System.Collections.ObjectModel.ReadOnlyCollection<String> RNTRNTRNT_127 = list.AsReadOnly();
      IACSharpSensor.IACSharpSensor.SensorReached(720);
      return RNTRNTRNT_127;
    }
  }
  internal sealed class EnumUtil
  {
    private EnumUtil()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(721);
    }
    static internal string GetDescription(System.Enum value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(722);
      FieldInfo fi = value.GetType().GetField(value.ToString());
      var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
      IACSharpSensor.IACSharpSensor.SensorReached(723);
      if (attributes.Length > 0) {
        System.String RNTRNTRNT_128 = attributes[0].Description;
        IACSharpSensor.IACSharpSensor.SensorReached(724);
        return RNTRNTRNT_128;
      } else {
        System.String RNTRNTRNT_129 = value.ToString();
        IACSharpSensor.IACSharpSensor.SensorReached(725);
        return RNTRNTRNT_129;
      }
    }
    static internal object Parse(Type enumType, string stringRepresentation)
    {
      System.Object RNTRNTRNT_130 = Parse(enumType, stringRepresentation, false);
      IACSharpSensor.IACSharpSensor.SensorReached(726);
      return RNTRNTRNT_130;
    }
    static internal object Parse(Type enumType, string stringRepresentation, bool ignoreCase)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(727);
      if (ignoreCase) {
        IACSharpSensor.IACSharpSensor.SensorReached(728);
        stringRepresentation = stringRepresentation.ToLower();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(729);
      foreach (System.Enum enumVal in System.Enum.GetValues(enumType)) {
        IACSharpSensor.IACSharpSensor.SensorReached(730);
        string description = GetDescription(enumVal);
        IACSharpSensor.IACSharpSensor.SensorReached(731);
        if (ignoreCase) {
          IACSharpSensor.IACSharpSensor.SensorReached(732);
          description = description.ToLower();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(733);
        if (description == stringRepresentation) {
          System.Object RNTRNTRNT_131 = enumVal;
          IACSharpSensor.IACSharpSensor.SensorReached(734);
          return RNTRNTRNT_131;
        }
      }
      System.Object RNTRNTRNT_132 = System.Enum.Parse(enumType, stringRepresentation, ignoreCase);
      IACSharpSensor.IACSharpSensor.SensorReached(735);
      return RNTRNTRNT_132;
    }
  }
}
