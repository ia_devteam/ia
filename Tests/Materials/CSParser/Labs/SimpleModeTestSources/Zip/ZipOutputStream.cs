using System;
using System.Threading;
using System.Collections.Generic;
using System.IO;
using Ionic.Zip;
namespace Ionic.Zip
{
  public class ZipOutputStream : Stream
  {
    public ZipOutputStream(Stream stream) : this(stream, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
    }
    public ZipOutputStream(String fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      Stream stream = File.Open(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
      _Init(stream, false, fileName);
      IACSharpSensor.IACSharpSensor.SensorReached(3);
    }
    public ZipOutputStream(Stream stream, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4);
      _Init(stream, leaveOpen, null);
      IACSharpSensor.IACSharpSensor.SensorReached(5);
    }
    private void _Init(Stream stream, bool leaveOpen, string name)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6);
      _outputStream = stream.CanRead ? stream : new CountingStream(stream);
      CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
      CompressionMethod = Ionic.Zip.CompressionMethod.Deflate;
      _encryption = EncryptionAlgorithm.None;
      _entriesWritten = new Dictionary<String, ZipEntry>(StringComparer.Ordinal);
      _zip64 = Zip64Option.Never;
      _leaveUnderlyingStreamOpen = leaveOpen;
      Strategy = Ionic.Zlib.CompressionStrategy.Default;
      _name = name ?? "(stream)";
      ParallelDeflateThreshold = -1L;
      IACSharpSensor.IACSharpSensor.SensorReached(7);
    }
    public override String ToString()
    {
      String RNTRNTRNT_1 = String.Format("ZipOutputStream::{0}(leaveOpen({1})))", _name, _leaveUnderlyingStreamOpen);
      IACSharpSensor.IACSharpSensor.SensorReached(8);
      return RNTRNTRNT_1;
    }
    public String Password {
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(9);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(10);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(11);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(12);
        _password = value;
        IACSharpSensor.IACSharpSensor.SensorReached(13);
        if (_password == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(14);
          _encryption = EncryptionAlgorithm.None;
        } else if (_encryption == EncryptionAlgorithm.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(15);
          _encryption = EncryptionAlgorithm.PkzipWeak;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(16);
      }
    }
    public EncryptionAlgorithm Encryption {
      get {
        EncryptionAlgorithm RNTRNTRNT_2 = _encryption;
        IACSharpSensor.IACSharpSensor.SensorReached(17);
        return RNTRNTRNT_2;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(18);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(19);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(20);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(21);
        if (value == EncryptionAlgorithm.Unsupported) {
          IACSharpSensor.IACSharpSensor.SensorReached(22);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(23);
          throw new InvalidOperationException("You may not set Encryption to that value.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(24);
        _encryption = value;
        IACSharpSensor.IACSharpSensor.SensorReached(25);
      }
    }
    public int CodecBufferSize { get; set; }
    public Ionic.Zlib.CompressionStrategy Strategy { get; set; }
    public ZipEntryTimestamp Timestamp {
      get {
        ZipEntryTimestamp RNTRNTRNT_3 = _timestamp;
        IACSharpSensor.IACSharpSensor.SensorReached(26);
        return RNTRNTRNT_3;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(27);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(28);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(29);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(30);
        _timestamp = value;
        IACSharpSensor.IACSharpSensor.SensorReached(31);
      }
    }
    public Ionic.Zlib.CompressionLevel CompressionLevel { get; set; }
    public Ionic.Zip.CompressionMethod CompressionMethod { get; set; }
    public string Comment {
      get {
        System.String RNTRNTRNT_4 = _comment;
        IACSharpSensor.IACSharpSensor.SensorReached(32);
        return RNTRNTRNT_4;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(33);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(34);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(35);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(36);
        _comment = value;
        IACSharpSensor.IACSharpSensor.SensorReached(37);
      }
    }
    public Zip64Option EnableZip64 {
      get {
        Zip64Option RNTRNTRNT_5 = _zip64;
        IACSharpSensor.IACSharpSensor.SensorReached(38);
        return RNTRNTRNT_5;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(39);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(40);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(41);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(42);
        _zip64 = value;
        IACSharpSensor.IACSharpSensor.SensorReached(43);
      }
    }
    public bool OutputUsedZip64 {
      get {
        System.Boolean RNTRNTRNT_6 = _anyEntriesUsedZip64 || _directoryNeededZip64;
        IACSharpSensor.IACSharpSensor.SensorReached(44);
        return RNTRNTRNT_6;
      }
    }
    public bool IgnoreCase {
      get {
        System.Boolean RNTRNTRNT_7 = !_DontIgnoreCase;
        IACSharpSensor.IACSharpSensor.SensorReached(45);
        return RNTRNTRNT_7;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(46);
        _DontIgnoreCase = !value;
        IACSharpSensor.IACSharpSensor.SensorReached(47);
      }
    }
    [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete. It will be removed in a future version of the library. Use AlternateEncoding and AlternateEncodingUsage instead.")]
    public bool UseUnicodeAsNecessary {
      get {
        System.Boolean RNTRNTRNT_8 = (_alternateEncoding == System.Text.Encoding.UTF8) && (AlternateEncodingUsage == ZipOption.AsNecessary);
        IACSharpSensor.IACSharpSensor.SensorReached(48);
        return RNTRNTRNT_8;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(49);
        if (value) {
          IACSharpSensor.IACSharpSensor.SensorReached(50);
          _alternateEncoding = System.Text.Encoding.UTF8;
          _alternateEncodingUsage = ZipOption.AsNecessary;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(51);
          _alternateEncoding = Ionic.Zip.ZipOutputStream.DefaultEncoding;
          _alternateEncodingUsage = ZipOption.Never;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(52);
      }
    }
    [Obsolete("use AlternateEncoding and AlternateEncodingUsage instead.")]
    public System.Text.Encoding ProvisionalAlternateEncoding {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(53);
        if (_alternateEncodingUsage == ZipOption.AsNecessary) {
          System.Text.Encoding RNTRNTRNT_9 = _alternateEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(54);
          return RNTRNTRNT_9;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(55);
        return null;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(56);
        _alternateEncoding = value;
        _alternateEncodingUsage = ZipOption.AsNecessary;
        IACSharpSensor.IACSharpSensor.SensorReached(57);
      }
    }
    public System.Text.Encoding AlternateEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_10 = _alternateEncoding;
        IACSharpSensor.IACSharpSensor.SensorReached(58);
        return RNTRNTRNT_10;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(59);
        _alternateEncoding = value;
        IACSharpSensor.IACSharpSensor.SensorReached(60);
      }
    }
    public ZipOption AlternateEncodingUsage {
      get {
        ZipOption RNTRNTRNT_11 = _alternateEncodingUsage;
        IACSharpSensor.IACSharpSensor.SensorReached(61);
        return RNTRNTRNT_11;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(62);
        _alternateEncodingUsage = value;
        IACSharpSensor.IACSharpSensor.SensorReached(63);
      }
    }
    public static System.Text.Encoding DefaultEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_12 = System.Text.Encoding.GetEncoding("IBM437");
        IACSharpSensor.IACSharpSensor.SensorReached(64);
        return RNTRNTRNT_12;
      }
    }
    public long ParallelDeflateThreshold {
      get {
        System.Int64 RNTRNTRNT_13 = _ParallelDeflateThreshold;
        IACSharpSensor.IACSharpSensor.SensorReached(65);
        return RNTRNTRNT_13;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(66);
        if ((value != 0) && (value != -1) && (value < 64 * 1024)) {
          IACSharpSensor.IACSharpSensor.SensorReached(67);
          throw new ArgumentOutOfRangeException("value must be greater than 64k, or 0, or -1");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(68);
        _ParallelDeflateThreshold = value;
        IACSharpSensor.IACSharpSensor.SensorReached(69);
      }
    }
    public int ParallelDeflateMaxBufferPairs {
      get {
        System.Int32 RNTRNTRNT_14 = _maxBufferPairs;
        IACSharpSensor.IACSharpSensor.SensorReached(70);
        return RNTRNTRNT_14;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(71);
        if (value < 4) {
          IACSharpSensor.IACSharpSensor.SensorReached(72);
          throw new ArgumentOutOfRangeException("ParallelDeflateMaxBufferPairs", "Value must be 4 or greater.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(73);
        _maxBufferPairs = value;
        IACSharpSensor.IACSharpSensor.SensorReached(74);
      }
    }
    private void InsureUniqueEntry(ZipEntry ze1)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(75);
      if (_entriesWritten.ContainsKey(ze1.FileName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(76);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(77);
        throw new ArgumentException(String.Format("The entry '{0}' already exists in the zip archive.", ze1.FileName));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(78);
    }
    internal Stream OutputStream {
      get {
        Stream RNTRNTRNT_15 = _outputStream;
        IACSharpSensor.IACSharpSensor.SensorReached(79);
        return RNTRNTRNT_15;
      }
    }
    internal String Name {
      get {
        String RNTRNTRNT_16 = _name;
        IACSharpSensor.IACSharpSensor.SensorReached(80);
        return RNTRNTRNT_16;
      }
    }
    public bool ContainsEntry(string name)
    {
      System.Boolean RNTRNTRNT_17 = _entriesWritten.ContainsKey(SharedUtilities.NormalizePathForUseInZipFile(name));
      IACSharpSensor.IACSharpSensor.SensorReached(81);
      return RNTRNTRNT_17;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(82);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(83);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(84);
        throw new System.InvalidOperationException("The stream has been closed.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(85);
      if (buffer == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(86);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(87);
        throw new System.ArgumentNullException("buffer");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(88);
      if (_currentEntry == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(89);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(90);
        throw new System.InvalidOperationException("You must call PutNextEntry() before calling Write().");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(91);
      if (_currentEntry.IsDirectory) {
        IACSharpSensor.IACSharpSensor.SensorReached(92);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(93);
        throw new System.InvalidOperationException("You cannot Write() data for an entry that is a directory.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(94);
      if (_needToWriteEntryHeader) {
        IACSharpSensor.IACSharpSensor.SensorReached(95);
        _InitiateCurrentEntry(false);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(96);
      if (count != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(97);
        _entryOutputStream.Write(buffer, offset, count);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(98);
    }
    public ZipEntry PutNextEntry(String entryName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(99);
      if (String.IsNullOrEmpty(entryName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(100);
        throw new ArgumentNullException("entryName");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(101);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(102);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(103);
        throw new System.InvalidOperationException("The stream has been closed.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(104);
      _FinishCurrentEntry();
      _currentEntry = ZipEntry.CreateForZipOutputStream(entryName);
      _currentEntry._container = new ZipContainer(this);
      _currentEntry._BitField |= 0x8;
      _currentEntry.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      _currentEntry.CompressionLevel = this.CompressionLevel;
      _currentEntry.CompressionMethod = this.CompressionMethod;
      _currentEntry.Password = _password;
      _currentEntry.Encryption = this.Encryption;
      _currentEntry.AlternateEncoding = this.AlternateEncoding;
      _currentEntry.AlternateEncodingUsage = this.AlternateEncodingUsage;
      IACSharpSensor.IACSharpSensor.SensorReached(105);
      if (entryName.EndsWith("/")) {
        IACSharpSensor.IACSharpSensor.SensorReached(106);
        _currentEntry.MarkAsDirectory();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(107);
      _currentEntry.EmitTimesInWindowsFormatWhenSaving = ((_timestamp & ZipEntryTimestamp.Windows) != 0);
      _currentEntry.EmitTimesInUnixFormatWhenSaving = ((_timestamp & ZipEntryTimestamp.Unix) != 0);
      InsureUniqueEntry(_currentEntry);
      _needToWriteEntryHeader = true;
      ZipEntry RNTRNTRNT_18 = _currentEntry;
      IACSharpSensor.IACSharpSensor.SensorReached(108);
      return RNTRNTRNT_18;
    }
    private void _InitiateCurrentEntry(bool finishing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(109);
      _entriesWritten.Add(_currentEntry.FileName, _currentEntry);
      _entryCount++;
      IACSharpSensor.IACSharpSensor.SensorReached(110);
      if (_entryCount > 65534 && _zip64 == Zip64Option.Never) {
        IACSharpSensor.IACSharpSensor.SensorReached(111);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(112);
        throw new System.InvalidOperationException("Too many entries. Consider setting ZipOutputStream.EnableZip64.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(113);
      _currentEntry.WriteHeader(_outputStream, finishing ? 99 : 0);
      _currentEntry.StoreRelativeOffset();
      IACSharpSensor.IACSharpSensor.SensorReached(114);
      if (!_currentEntry.IsDirectory) {
        IACSharpSensor.IACSharpSensor.SensorReached(115);
        _currentEntry.WriteSecurityMetadata(_outputStream);
        _currentEntry.PrepOutputStream(_outputStream, finishing ? 0 : -1, out _outputCounter, out _encryptor, out _deflater, out _entryOutputStream);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(116);
      _needToWriteEntryHeader = false;
      IACSharpSensor.IACSharpSensor.SensorReached(117);
    }
    private void _FinishCurrentEntry()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(118);
      if (_currentEntry != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(119);
        if (_needToWriteEntryHeader) {
          IACSharpSensor.IACSharpSensor.SensorReached(120);
          _InitiateCurrentEntry(true);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(121);
        _currentEntry.FinishOutputStream(_outputStream, _outputCounter, _encryptor, _deflater, _entryOutputStream);
        _currentEntry.PostProcessOutput(_outputStream);
        IACSharpSensor.IACSharpSensor.SensorReached(122);
        if (_currentEntry.OutputUsedZip64 != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(123);
          _anyEntriesUsedZip64 |= _currentEntry.OutputUsedZip64.Value;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(124);
        _outputCounter = null;
        _encryptor = _deflater = null;
        _entryOutputStream = null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(125);
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(126);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(127);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(128);
      if (disposing) {
        IACSharpSensor.IACSharpSensor.SensorReached(129);
        if (!_exceptionPending) {
          IACSharpSensor.IACSharpSensor.SensorReached(130);
          _FinishCurrentEntry();
          _directoryNeededZip64 = ZipOutput.WriteCentralDirectoryStructure(_outputStream, _entriesWritten.Values, 1, _zip64, Comment, new ZipContainer(this));
          Stream wrappedStream = null;
          CountingStream cs = _outputStream as CountingStream;
          IACSharpSensor.IACSharpSensor.SensorReached(131);
          if (cs != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(132);
            wrappedStream = cs.WrappedStream;
            cs.Dispose();
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(133);
            wrappedStream = _outputStream;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(134);
          if (!_leaveUnderlyingStreamOpen) {
            IACSharpSensor.IACSharpSensor.SensorReached(135);
            wrappedStream.Dispose();
          }
          IACSharpSensor.IACSharpSensor.SensorReached(136);
          _outputStream = null;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(137);
      _disposed = true;
      IACSharpSensor.IACSharpSensor.SensorReached(138);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_19 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(139);
        return RNTRNTRNT_19;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_20 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(140);
        return RNTRNTRNT_20;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_21 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(141);
        return RNTRNTRNT_21;
      }
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(142);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_22 = _outputStream.Position;
        IACSharpSensor.IACSharpSensor.SensorReached(143);
        return RNTRNTRNT_22;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(144);
        throw new NotSupportedException();
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(145);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(146);
      throw new NotSupportedException("Read");
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(147);
      throw new NotSupportedException("Seek");
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(148);
      throw new NotSupportedException();
    }
    private EncryptionAlgorithm _encryption;
    private ZipEntryTimestamp _timestamp;
    internal String _password;
    private String _comment;
    private Stream _outputStream;
    private ZipEntry _currentEntry;
    internal Zip64Option _zip64;
    private Dictionary<String, ZipEntry> _entriesWritten;
    private int _entryCount;
    private ZipOption _alternateEncodingUsage = ZipOption.Never;
    private System.Text.Encoding _alternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
    private bool _leaveUnderlyingStreamOpen;
    private bool _disposed;
    private bool _exceptionPending;
    private bool _anyEntriesUsedZip64, _directoryNeededZip64;
    private CountingStream _outputCounter;
    private Stream _encryptor;
    private Stream _deflater;
    private Ionic.Crc.CrcCalculatorStream _entryOutputStream;
    private bool _needToWriteEntryHeader;
    private string _name;
    private bool _DontIgnoreCase;
    internal Ionic.Zlib.ParallelDeflateOutputStream ParallelDeflater;
    private long _ParallelDeflateThreshold;
    private int _maxBufferPairs = 16;
  }
  internal class ZipContainer
  {
    private ZipFile _zf;
    private ZipOutputStream _zos;
    private ZipInputStream _zis;
    public ZipContainer(Object o)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(149);
      _zf = (o as ZipFile);
      _zos = (o as ZipOutputStream);
      _zis = (o as ZipInputStream);
      IACSharpSensor.IACSharpSensor.SensorReached(150);
    }
    public ZipFile ZipFile {
      get {
        ZipFile RNTRNTRNT_23 = _zf;
        IACSharpSensor.IACSharpSensor.SensorReached(151);
        return RNTRNTRNT_23;
      }
    }
    public ZipOutputStream ZipOutputStream {
      get {
        ZipOutputStream RNTRNTRNT_24 = _zos;
        IACSharpSensor.IACSharpSensor.SensorReached(152);
        return RNTRNTRNT_24;
      }
    }
    public string Name {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(153);
        if (_zf != null) {
          System.String RNTRNTRNT_25 = _zf.Name;
          IACSharpSensor.IACSharpSensor.SensorReached(154);
          return RNTRNTRNT_25;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(155);
        if (_zis != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(156);
          throw new NotSupportedException();
        }
        System.String RNTRNTRNT_26 = _zos.Name;
        IACSharpSensor.IACSharpSensor.SensorReached(157);
        return RNTRNTRNT_26;
      }
    }
    public string Password {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(158);
        if (_zf != null) {
          System.String RNTRNTRNT_27 = _zf._Password;
          IACSharpSensor.IACSharpSensor.SensorReached(159);
          return RNTRNTRNT_27;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(160);
        if (_zis != null) {
          System.String RNTRNTRNT_28 = _zis._Password;
          IACSharpSensor.IACSharpSensor.SensorReached(161);
          return RNTRNTRNT_28;
        }
        System.String RNTRNTRNT_29 = _zos._password;
        IACSharpSensor.IACSharpSensor.SensorReached(162);
        return RNTRNTRNT_29;
      }
    }
    public Zip64Option Zip64 {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(163);
        if (_zf != null) {
          Zip64Option RNTRNTRNT_30 = _zf._zip64;
          IACSharpSensor.IACSharpSensor.SensorReached(164);
          return RNTRNTRNT_30;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(165);
        if (_zis != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(166);
          throw new NotSupportedException();
        }
        Zip64Option RNTRNTRNT_31 = _zos._zip64;
        IACSharpSensor.IACSharpSensor.SensorReached(167);
        return RNTRNTRNT_31;
      }
    }
    public int BufferSize {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(168);
        if (_zf != null) {
          System.Int32 RNTRNTRNT_32 = _zf.BufferSize;
          IACSharpSensor.IACSharpSensor.SensorReached(169);
          return RNTRNTRNT_32;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(170);
        if (_zis != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(171);
          throw new NotSupportedException();
        }
        System.Int32 RNTRNTRNT_33 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(172);
        return RNTRNTRNT_33;
      }
    }
    public Ionic.Zlib.ParallelDeflateOutputStream ParallelDeflater {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(173);
        if (_zf != null) {
          Ionic.Zlib.ParallelDeflateOutputStream RNTRNTRNT_34 = _zf.ParallelDeflater;
          IACSharpSensor.IACSharpSensor.SensorReached(174);
          return RNTRNTRNT_34;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(175);
        if (_zis != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(176);
          return null;
        }
        Ionic.Zlib.ParallelDeflateOutputStream RNTRNTRNT_35 = _zos.ParallelDeflater;
        IACSharpSensor.IACSharpSensor.SensorReached(177);
        return RNTRNTRNT_35;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(178);
        if (_zf != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(179);
          _zf.ParallelDeflater = value;
        } else if (_zos != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(180);
          _zos.ParallelDeflater = value;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(181);
      }
    }
    public long ParallelDeflateThreshold {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(182);
        if (_zf != null) {
          System.Int64 RNTRNTRNT_36 = _zf.ParallelDeflateThreshold;
          IACSharpSensor.IACSharpSensor.SensorReached(183);
          return RNTRNTRNT_36;
        }
        System.Int64 RNTRNTRNT_37 = _zos.ParallelDeflateThreshold;
        IACSharpSensor.IACSharpSensor.SensorReached(184);
        return RNTRNTRNT_37;
      }
    }
    public int ParallelDeflateMaxBufferPairs {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(185);
        if (_zf != null) {
          System.Int32 RNTRNTRNT_38 = _zf.ParallelDeflateMaxBufferPairs;
          IACSharpSensor.IACSharpSensor.SensorReached(186);
          return RNTRNTRNT_38;
        }
        System.Int32 RNTRNTRNT_39 = _zos.ParallelDeflateMaxBufferPairs;
        IACSharpSensor.IACSharpSensor.SensorReached(187);
        return RNTRNTRNT_39;
      }
    }
    public int CodecBufferSize {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(188);
        if (_zf != null) {
          System.Int32 RNTRNTRNT_40 = _zf.CodecBufferSize;
          IACSharpSensor.IACSharpSensor.SensorReached(189);
          return RNTRNTRNT_40;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(190);
        if (_zis != null) {
          System.Int32 RNTRNTRNT_41 = _zis.CodecBufferSize;
          IACSharpSensor.IACSharpSensor.SensorReached(191);
          return RNTRNTRNT_41;
        }
        System.Int32 RNTRNTRNT_42 = _zos.CodecBufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(192);
        return RNTRNTRNT_42;
      }
    }
    public Ionic.Zlib.CompressionStrategy Strategy {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(193);
        if (_zf != null) {
          Ionic.Zlib.CompressionStrategy RNTRNTRNT_43 = _zf.Strategy;
          IACSharpSensor.IACSharpSensor.SensorReached(194);
          return RNTRNTRNT_43;
        }
        Ionic.Zlib.CompressionStrategy RNTRNTRNT_44 = _zos.Strategy;
        IACSharpSensor.IACSharpSensor.SensorReached(195);
        return RNTRNTRNT_44;
      }
    }
    public Zip64Option UseZip64WhenSaving {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(196);
        if (_zf != null) {
          Zip64Option RNTRNTRNT_45 = _zf.UseZip64WhenSaving;
          IACSharpSensor.IACSharpSensor.SensorReached(197);
          return RNTRNTRNT_45;
        }
        Zip64Option RNTRNTRNT_46 = _zos.EnableZip64;
        IACSharpSensor.IACSharpSensor.SensorReached(198);
        return RNTRNTRNT_46;
      }
    }
    public System.Text.Encoding AlternateEncoding {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(199);
        if (_zf != null) {
          System.Text.Encoding RNTRNTRNT_47 = _zf.AlternateEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(200);
          return RNTRNTRNT_47;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(201);
        if (_zos != null) {
          System.Text.Encoding RNTRNTRNT_48 = _zos.AlternateEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(202);
          return RNTRNTRNT_48;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(203);
        return null;
      }
    }
    public System.Text.Encoding DefaultEncoding {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(204);
        if (_zf != null) {
          System.Text.Encoding RNTRNTRNT_49 = ZipFile.DefaultEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(205);
          return RNTRNTRNT_49;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(206);
        if (_zos != null) {
          System.Text.Encoding RNTRNTRNT_50 = ZipOutputStream.DefaultEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(207);
          return RNTRNTRNT_50;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(208);
        return null;
      }
    }
    public ZipOption AlternateEncodingUsage {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(209);
        if (_zf != null) {
          ZipOption RNTRNTRNT_51 = _zf.AlternateEncodingUsage;
          IACSharpSensor.IACSharpSensor.SensorReached(210);
          return RNTRNTRNT_51;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(211);
        if (_zos != null) {
          ZipOption RNTRNTRNT_52 = _zos.AlternateEncodingUsage;
          IACSharpSensor.IACSharpSensor.SensorReached(212);
          return RNTRNTRNT_52;
        }
        ZipOption RNTRNTRNT_53 = ZipOption.Never;
        IACSharpSensor.IACSharpSensor.SensorReached(213);
        return RNTRNTRNT_53;
      }
    }
    public Stream ReadStream {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(214);
        if (_zf != null) {
          Stream RNTRNTRNT_54 = _zf.ReadStream;
          IACSharpSensor.IACSharpSensor.SensorReached(215);
          return RNTRNTRNT_54;
        }
        Stream RNTRNTRNT_55 = _zis.ReadStream;
        IACSharpSensor.IACSharpSensor.SensorReached(216);
        return RNTRNTRNT_55;
      }
    }
  }
}
