using System;
namespace Ionic.Zip
{
  internal class ZipCrypto
  {
    private ZipCrypto()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1092);
    }
    public static ZipCrypto ForWrite(string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1093);
      ZipCrypto z = new ZipCrypto();
      IACSharpSensor.IACSharpSensor.SensorReached(1094);
      if (password == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1095);
        throw new BadPasswordException("This entry requires a password.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1096);
      z.InitCipher(password);
      ZipCrypto RNTRNTRNT_183 = z;
      IACSharpSensor.IACSharpSensor.SensorReached(1097);
      return RNTRNTRNT_183;
    }
    public static ZipCrypto ForRead(string password, ZipEntry e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1098);
      System.IO.Stream s = e._archiveStream;
      e._WeakEncryptionHeader = new byte[12];
      byte[] eh = e._WeakEncryptionHeader;
      ZipCrypto z = new ZipCrypto();
      IACSharpSensor.IACSharpSensor.SensorReached(1099);
      if (password == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1100);
        throw new BadPasswordException("This entry requires a password.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1101);
      z.InitCipher(password);
      ZipEntry.ReadWeakEncryptionHeader(s, eh);
      byte[] DecryptedHeader = z.DecryptMessage(eh, eh.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(1102);
      if (DecryptedHeader[11] != (byte)((e._Crc32 >> 24) & 0xff)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1103);
        if ((e._BitField & 0x8) != 0x8) {
          IACSharpSensor.IACSharpSensor.SensorReached(1104);
          throw new BadPasswordException("The password did not match.");
        } else if (DecryptedHeader[11] != (byte)((e._TimeBlob >> 8) & 0xff)) {
          IACSharpSensor.IACSharpSensor.SensorReached(1105);
          throw new BadPasswordException("The password did not match.");
        }
      } else {
      }
      ZipCrypto RNTRNTRNT_184 = z;
      IACSharpSensor.IACSharpSensor.SensorReached(1106);
      return RNTRNTRNT_184;
    }
    private byte MagicByte {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1107);
        UInt16 t = (UInt16)((UInt16)(_Keys[2] & 0xffff) | 2);
        System.Byte RNTRNTRNT_185 = (byte)((t * (t ^ 1)) >> 8);
        IACSharpSensor.IACSharpSensor.SensorReached(1108);
        return RNTRNTRNT_185;
      }
    }
    public byte[] DecryptMessage(byte[] cipherText, int length)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1109);
      if (cipherText == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1110);
        throw new ArgumentNullException("cipherText");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1111);
      if (length > cipherText.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(1112);
        throw new ArgumentOutOfRangeException("length", "Bad length during Decryption: the length parameter must be smaller than or equal to the size of the destination array.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1113);
      byte[] plainText = new byte[length];
      IACSharpSensor.IACSharpSensor.SensorReached(1114);
      for (int i = 0; i < length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1115);
        byte C = (byte)(cipherText[i] ^ MagicByte);
        UpdateKeys(C);
        plainText[i] = C;
      }
      System.Byte[] RNTRNTRNT_186 = plainText;
      IACSharpSensor.IACSharpSensor.SensorReached(1116);
      return RNTRNTRNT_186;
    }
    public byte[] EncryptMessage(byte[] plainText, int length)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1117);
      if (plainText == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1118);
        throw new ArgumentNullException("plaintext");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1119);
      if (length > plainText.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(1120);
        throw new ArgumentOutOfRangeException("length", "Bad length during Encryption: The length parameter must be smaller than or equal to the size of the destination array.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1121);
      byte[] cipherText = new byte[length];
      IACSharpSensor.IACSharpSensor.SensorReached(1122);
      for (int i = 0; i < length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1123);
        byte C = plainText[i];
        cipherText[i] = (byte)(plainText[i] ^ MagicByte);
        UpdateKeys(C);
      }
      System.Byte[] RNTRNTRNT_187 = cipherText;
      IACSharpSensor.IACSharpSensor.SensorReached(1124);
      return RNTRNTRNT_187;
    }
    public void InitCipher(string passphrase)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1125);
      byte[] p = SharedUtilities.StringToByteArray(passphrase);
      IACSharpSensor.IACSharpSensor.SensorReached(1126);
      for (int i = 0; i < passphrase.Length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1127);
        UpdateKeys(p[i]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1128);
    }
    private void UpdateKeys(byte byteValue)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1129);
      _Keys[0] = (UInt32)crc32.ComputeCrc32((int)_Keys[0], byteValue);
      _Keys[1] = _Keys[1] + (byte)_Keys[0];
      _Keys[1] = _Keys[1] * 0x8088405 + 1;
      _Keys[2] = (UInt32)crc32.ComputeCrc32((int)_Keys[2], (byte)(_Keys[1] >> 24));
      IACSharpSensor.IACSharpSensor.SensorReached(1130);
    }
    private UInt32[] _Keys = {
      0x12345678,
      0x23456789,
      0x34567890
    };
    private Ionic.Crc.CRC32 crc32 = new Ionic.Crc.CRC32();
  }
  internal enum CryptoMode
  {
    Encrypt,
    Decrypt
  }
  internal class ZipCipherStream : System.IO.Stream
  {
    private ZipCrypto _cipher;
    private System.IO.Stream _s;
    private CryptoMode _mode;
    public ZipCipherStream(System.IO.Stream s, ZipCrypto cipher, CryptoMode mode) : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1131);
      _cipher = cipher;
      _s = s;
      _mode = mode;
      IACSharpSensor.IACSharpSensor.SensorReached(1132);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1133);
      if (_mode == CryptoMode.Encrypt) {
        IACSharpSensor.IACSharpSensor.SensorReached(1134);
        throw new NotSupportedException("This stream does not encrypt via Read()");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1135);
      if (buffer == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1136);
        throw new ArgumentNullException("buffer");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1137);
      byte[] db = new byte[count];
      int n = _s.Read(db, 0, count);
      byte[] decrypted = _cipher.DecryptMessage(db, n);
      IACSharpSensor.IACSharpSensor.SensorReached(1138);
      for (int i = 0; i < n; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1139);
        buffer[offset + i] = decrypted[i];
      }
      System.Int32 RNTRNTRNT_188 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(1140);
      return RNTRNTRNT_188;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1141);
      if (_mode == CryptoMode.Decrypt) {
        IACSharpSensor.IACSharpSensor.SensorReached(1142);
        throw new NotSupportedException("This stream does not Decrypt via Write()");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1143);
      if (buffer == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1144);
        throw new ArgumentNullException("buffer");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1145);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1146);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1147);
      byte[] plaintext = null;
      IACSharpSensor.IACSharpSensor.SensorReached(1148);
      if (offset != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1149);
        plaintext = new byte[count];
        IACSharpSensor.IACSharpSensor.SensorReached(1150);
        for (int i = 0; i < count; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(1151);
          plaintext[i] = buffer[offset + i];
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(1152);
        plaintext = buffer;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1153);
      byte[] encrypted = _cipher.EncryptMessage(plaintext, count);
      _s.Write(encrypted, 0, encrypted.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(1154);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_189 = (_mode == CryptoMode.Decrypt);
        IACSharpSensor.IACSharpSensor.SensorReached(1155);
        return RNTRNTRNT_189;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_190 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1156);
        return RNTRNTRNT_190;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_191 = (_mode == CryptoMode.Encrypt);
        IACSharpSensor.IACSharpSensor.SensorReached(1157);
        return RNTRNTRNT_191;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1158);
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1159);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1160);
        throw new NotSupportedException();
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1161);
        throw new NotSupportedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1162);
      throw new NotSupportedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1163);
      throw new NotSupportedException();
    }
  }
}
