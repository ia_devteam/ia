using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    public static bool CheckZip(string zipFileName)
    {
      System.Boolean RNTRNTRNT_367 = CheckZip(zipFileName, false, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2577);
      return RNTRNTRNT_367;
    }
    public static bool CheckZip(string zipFileName, bool fixIfNecessary, TextWriter writer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2578);
      ZipFile zip1 = null, zip2 = null;
      bool isOk = true;
      IACSharpSensor.IACSharpSensor.SensorReached(2579);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2580);
        zip1 = new ZipFile();
        zip1.FullScan = true;
        zip1.Initialize(zipFileName);
        zip2 = ZipFile.Read(zipFileName);
        IACSharpSensor.IACSharpSensor.SensorReached(2581);
        foreach (var e1 in zip1) {
          IACSharpSensor.IACSharpSensor.SensorReached(2582);
          foreach (var e2 in zip2) {
            IACSharpSensor.IACSharpSensor.SensorReached(2583);
            if (e1.FileName == e2.FileName) {
              IACSharpSensor.IACSharpSensor.SensorReached(2584);
              if (e1._RelativeOffsetOfLocalHeader != e2._RelativeOffsetOfLocalHeader) {
                IACSharpSensor.IACSharpSensor.SensorReached(2585);
                isOk = false;
                IACSharpSensor.IACSharpSensor.SensorReached(2586);
                if (writer != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(2587);
                  writer.WriteLine("{0}: mismatch in RelativeOffsetOfLocalHeader  (0x{1:X16} != 0x{2:X16})", e1.FileName, e1._RelativeOffsetOfLocalHeader, e2._RelativeOffsetOfLocalHeader);
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(2588);
              if (e1._CompressedSize != e2._CompressedSize) {
                IACSharpSensor.IACSharpSensor.SensorReached(2589);
                isOk = false;
                IACSharpSensor.IACSharpSensor.SensorReached(2590);
                if (writer != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(2591);
                  writer.WriteLine("{0}: mismatch in CompressedSize  (0x{1:X16} != 0x{2:X16})", e1.FileName, e1._CompressedSize, e2._CompressedSize);
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(2592);
              if (e1._UncompressedSize != e2._UncompressedSize) {
                IACSharpSensor.IACSharpSensor.SensorReached(2593);
                isOk = false;
                IACSharpSensor.IACSharpSensor.SensorReached(2594);
                if (writer != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(2595);
                  writer.WriteLine("{0}: mismatch in UncompressedSize  (0x{1:X16} != 0x{2:X16})", e1.FileName, e1._UncompressedSize, e2._UncompressedSize);
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(2596);
              if (e1.CompressionMethod != e2.CompressionMethod) {
                IACSharpSensor.IACSharpSensor.SensorReached(2597);
                isOk = false;
                IACSharpSensor.IACSharpSensor.SensorReached(2598);
                if (writer != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(2599);
                  writer.WriteLine("{0}: mismatch in CompressionMethod  (0x{1:X4} != 0x{2:X4})", e1.FileName, e1.CompressionMethod, e2.CompressionMethod);
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(2600);
              if (e1.Crc != e2.Crc) {
                IACSharpSensor.IACSharpSensor.SensorReached(2601);
                isOk = false;
                IACSharpSensor.IACSharpSensor.SensorReached(2602);
                if (writer != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(2603);
                  writer.WriteLine("{0}: mismatch in Crc32  (0x{1:X4} != 0x{2:X4})", e1.FileName, e1.Crc, e2.Crc);
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(2604);
              break;
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2605);
        zip2.Dispose();
        zip2 = null;
        IACSharpSensor.IACSharpSensor.SensorReached(2606);
        if (!isOk && fixIfNecessary) {
          IACSharpSensor.IACSharpSensor.SensorReached(2607);
          string newFileName = Path.GetFileNameWithoutExtension(zipFileName);
          newFileName = System.String.Format("{0}_fixed.zip", newFileName);
          zip1.Save(newFileName);
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(2608);
        if (zip1 != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2609);
          zip1.Dispose();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2610);
        if (zip2 != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2611);
          zip2.Dispose();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2612);
      }
      System.Boolean RNTRNTRNT_368 = isOk;
      IACSharpSensor.IACSharpSensor.SensorReached(2613);
      return RNTRNTRNT_368;
    }
    public static void FixZipDirectory(string zipFileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2614);
      using (var zip = new ZipFile()) {
        IACSharpSensor.IACSharpSensor.SensorReached(2615);
        zip.FullScan = true;
        zip.Initialize(zipFileName);
        zip.Save(zipFileName);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2616);
    }
    public static bool CheckZipPassword(string zipFileName, string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2617);
      bool success = false;
      IACSharpSensor.IACSharpSensor.SensorReached(2618);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2619);
        using (ZipFile zip1 = ZipFile.Read(zipFileName)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2620);
          foreach (var e in zip1) {
            IACSharpSensor.IACSharpSensor.SensorReached(2621);
            if (!e.IsDirectory && e.UsesEncryption) {
              IACSharpSensor.IACSharpSensor.SensorReached(2622);
              e.ExtractWithPassword(System.IO.Stream.Null, password);
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2623);
        success = true;
      } catch (Ionic.Zip.BadPasswordException) {
      }
      System.Boolean RNTRNTRNT_369 = success;
      IACSharpSensor.IACSharpSensor.SensorReached(2624);
      return RNTRNTRNT_369;
    }
    public string Info {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2625);
        var builder = new System.Text.StringBuilder();
        builder.Append(string.Format("          ZipFile: {0}\n", this.Name));
        IACSharpSensor.IACSharpSensor.SensorReached(2626);
        if (!string.IsNullOrEmpty(this._Comment)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2627);
          builder.Append(string.Format("          Comment: {0}\n", this._Comment));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2628);
        if (this._versionMadeBy != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2629);
          builder.Append(string.Format("  version made by: 0x{0:X4}\n", this._versionMadeBy));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2630);
        if (this._versionNeededToExtract != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2631);
          builder.Append(string.Format("needed to extract: 0x{0:X4}\n", this._versionNeededToExtract));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2632);
        builder.Append(string.Format("       uses ZIP64: {0}\n", this.InputUsesZip64));
        builder.Append(string.Format("     disk with CD: {0}\n", this._diskNumberWithCd));
        IACSharpSensor.IACSharpSensor.SensorReached(2633);
        if (this._OffsetOfCentralDirectory == 0xffffffffu) {
          IACSharpSensor.IACSharpSensor.SensorReached(2634);
          builder.Append(string.Format("      CD64 offset: 0x{0:X16}\n", this._OffsetOfCentralDirectory64));
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2635);
          builder.Append(string.Format("        CD offset: 0x{0:X8}\n", this._OffsetOfCentralDirectory));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2636);
        builder.Append("\n");
        IACSharpSensor.IACSharpSensor.SensorReached(2637);
        foreach (ZipEntry entry in this._entries.Values) {
          IACSharpSensor.IACSharpSensor.SensorReached(2638);
          builder.Append(entry.Info);
        }
        System.String RNTRNTRNT_370 = builder.ToString();
        IACSharpSensor.IACSharpSensor.SensorReached(2639);
        return RNTRNTRNT_370;
      }
    }
  }
}
