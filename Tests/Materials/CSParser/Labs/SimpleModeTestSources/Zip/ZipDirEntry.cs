using System;
using System.Collections.Generic;
namespace Ionic.Zip
{
  partial class ZipEntry
  {
    internal bool AttributesIndicateDirectory {
      get {
        System.Boolean RNTRNTRNT_222 = ((_InternalFileAttrs == 0) && ((_ExternalFileAttrs & 0x10) == 0x10));
        IACSharpSensor.IACSharpSensor.SensorReached(1242);
        return RNTRNTRNT_222;
      }
    }
    internal void ResetDirEntry()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1243);
      this.__FileDataPosition = -1;
      this._LengthOfHeader = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1244);
    }
    public string Info {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1245);
        var builder = new System.Text.StringBuilder();
        builder.Append(string.Format("          ZipEntry: {0}\n", this.FileName)).Append(string.Format("   Version Made By: {0}\n", this._VersionMadeBy)).Append(string.Format(" Needed to extract: {0}\n", this.VersionNeeded));
        IACSharpSensor.IACSharpSensor.SensorReached(1246);
        if (this._IsDirectory) {
          IACSharpSensor.IACSharpSensor.SensorReached(1247);
          builder.Append("        Entry type: directory\n");
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1248);
          builder.Append(string.Format("         File type: {0}\n", this._IsText ? "text" : "binary")).Append(string.Format("       Compression: {0}\n", this.CompressionMethod)).Append(string.Format("        Compressed: 0x{0:X}\n", this.CompressedSize)).Append(string.Format("      Uncompressed: 0x{0:X}\n", this.UncompressedSize)).Append(string.Format("             CRC32: 0x{0:X8}\n", this._Crc32));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1249);
        builder.Append(string.Format("       Disk Number: {0}\n", this._diskNumber));
        IACSharpSensor.IACSharpSensor.SensorReached(1250);
        if (this._RelativeOffsetOfLocalHeader > 0xffffffffu) {
          IACSharpSensor.IACSharpSensor.SensorReached(1251);
          builder.Append(string.Format("   Relative Offset: 0x{0:X16}\n", this._RelativeOffsetOfLocalHeader));
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1252);
          builder.Append(string.Format("   Relative Offset: 0x{0:X8}\n", this._RelativeOffsetOfLocalHeader));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1253);
        builder.Append(string.Format("         Bit Field: 0x{0:X4}\n", this._BitField)).Append(string.Format("        Encrypted?: {0}\n", this._sourceIsEncrypted)).Append(string.Format("          Timeblob: 0x{0:X8}\n", this._TimeBlob)).Append(string.Format("              Time: {0}\n", Ionic.Zip.SharedUtilities.PackedToDateTime(this._TimeBlob)));
        builder.Append(string.Format("         Is Zip64?: {0}\n", this._InputUsesZip64));
        IACSharpSensor.IACSharpSensor.SensorReached(1254);
        if (!string.IsNullOrEmpty(this._Comment)) {
          IACSharpSensor.IACSharpSensor.SensorReached(1255);
          builder.Append(string.Format("           Comment: {0}\n", this._Comment));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1256);
        builder.Append("\n");
        System.String RNTRNTRNT_223 = builder.ToString();
        IACSharpSensor.IACSharpSensor.SensorReached(1257);
        return RNTRNTRNT_223;
      }
    }
    private class CopyHelper
    {
      private static System.Text.RegularExpressions.Regex re = new System.Text.RegularExpressions.Regex(" \\(copy (\\d+)\\)$");
      private static int callCount = 0;
      static internal string AppendCopyToFileName(string f)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(1258);
        callCount++;
        IACSharpSensor.IACSharpSensor.SensorReached(1259);
        if (callCount > 25) {
          IACSharpSensor.IACSharpSensor.SensorReached(1260);
          throw new OverflowException("overflow while creating filename");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1261);
        int n = 1;
        int r = f.LastIndexOf(".");
        IACSharpSensor.IACSharpSensor.SensorReached(1262);
        if (r == -1) {
          IACSharpSensor.IACSharpSensor.SensorReached(1263);
          System.Text.RegularExpressions.Match m = re.Match(f);
          IACSharpSensor.IACSharpSensor.SensorReached(1264);
          if (m.Success) {
            IACSharpSensor.IACSharpSensor.SensorReached(1265);
            n = Int32.Parse(m.Groups[1].Value) + 1;
            string copy = String.Format(" (copy {0})", n);
            f = f.Substring(0, m.Index) + copy;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1266);
            string copy = String.Format(" (copy {0})", n);
            f = f + copy;
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1267);
          System.Text.RegularExpressions.Match m = re.Match(f.Substring(0, r));
          IACSharpSensor.IACSharpSensor.SensorReached(1268);
          if (m.Success) {
            IACSharpSensor.IACSharpSensor.SensorReached(1269);
            n = Int32.Parse(m.Groups[1].Value) + 1;
            string copy = String.Format(" (copy {0})", n);
            f = f.Substring(0, m.Index) + copy + f.Substring(r);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1270);
            string copy = String.Format(" (copy {0})", n);
            f = f.Substring(0, r) + copy + f.Substring(r);
          }
        }
        System.String RNTRNTRNT_224 = f;
        IACSharpSensor.IACSharpSensor.SensorReached(1271);
        return RNTRNTRNT_224;
      }
    }
    static internal ZipEntry ReadDirEntry(ZipFile zf, Dictionary<String, Object> previouslySeen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1272);
      System.IO.Stream s = zf.ReadStream;
      System.Text.Encoding expectedEncoding = (zf.AlternateEncodingUsage == ZipOption.Always) ? zf.AlternateEncoding : ZipFile.DefaultEncoding;
      int signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
      IACSharpSensor.IACSharpSensor.SensorReached(1273);
      if (IsNotValidZipDirEntrySig(signature)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1274);
        s.Seek(-4, System.IO.SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
        IACSharpSensor.IACSharpSensor.SensorReached(1275);
        if (signature != ZipConstants.EndOfCentralDirectorySignature && signature != ZipConstants.Zip64EndOfCentralDirectoryRecordSignature && signature != ZipConstants.ZipEntrySignature) {
          IACSharpSensor.IACSharpSensor.SensorReached(1276);
          throw new BadReadException(String.Format("  Bad signature (0x{0:X8}) at position 0x{1:X8}", signature, s.Position));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1277);
        return null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1278);
      int bytesRead = 42 + 4;
      byte[] block = new byte[42];
      int n = s.Read(block, 0, block.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(1279);
      if (n != block.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(1280);
        return null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1281);
      int i = 0;
      ZipEntry zde = new ZipEntry();
      zde.AlternateEncoding = expectedEncoding;
      zde._Source = ZipEntrySource.ZipFile;
      zde._container = new ZipContainer(zf);
      unchecked {
        IACSharpSensor.IACSharpSensor.SensorReached(1282);
        zde._VersionMadeBy = (short)(block[i++] + block[i++] * 256);
        zde._VersionNeeded = (short)(block[i++] + block[i++] * 256);
        zde._BitField = (short)(block[i++] + block[i++] * 256);
        zde._CompressionMethod = (Int16)(block[i++] + block[i++] * 256);
        zde._TimeBlob = block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256;
        zde._LastModified = Ionic.Zip.SharedUtilities.PackedToDateTime(zde._TimeBlob);
        zde._timestamp |= ZipEntryTimestamp.DOS;
        zde._Crc32 = block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256;
        zde._CompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
        zde._UncompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
      }
      zde._CompressionMethod_FromZipFile = zde._CompressionMethod;
      zde._filenameLength = (short)(block[i++] + block[i++] * 256);
      zde._extraFieldLength = (short)(block[i++] + block[i++] * 256);
      zde._commentLength = (short)(block[i++] + block[i++] * 256);
      zde._diskNumber = (UInt32)(block[i++] + block[i++] * 256);
      zde._InternalFileAttrs = (short)(block[i++] + block[i++] * 256);
      zde._ExternalFileAttrs = block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256;
      zde._RelativeOffsetOfLocalHeader = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
      zde.IsText = ((zde._InternalFileAttrs & 0x1) == 0x1);
      block = new byte[zde._filenameLength];
      n = s.Read(block, 0, block.Length);
      bytesRead += n;
      IACSharpSensor.IACSharpSensor.SensorReached(1283);
      if ((zde._BitField & 0x800) == 0x800) {
        IACSharpSensor.IACSharpSensor.SensorReached(1284);
        zde._FileNameInArchive = Ionic.Zip.SharedUtilities.Utf8StringFromBuffer(block);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(1285);
        zde._FileNameInArchive = Ionic.Zip.SharedUtilities.StringFromBuffer(block, expectedEncoding);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1286);
      while (previouslySeen.ContainsKey(zde._FileNameInArchive)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1287);
        zde._FileNameInArchive = CopyHelper.AppendCopyToFileName(zde._FileNameInArchive);
        zde._metadataChanged = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1288);
      if (zde.AttributesIndicateDirectory) {
        IACSharpSensor.IACSharpSensor.SensorReached(1289);
        zde.MarkAsDirectory();
      } else if (zde._FileNameInArchive.EndsWith("/")) {
        IACSharpSensor.IACSharpSensor.SensorReached(1290);
        zde.MarkAsDirectory();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1291);
      zde._CompressedFileDataSize = zde._CompressedSize;
      IACSharpSensor.IACSharpSensor.SensorReached(1292);
      if ((zde._BitField & 0x1) == 0x1) {
        IACSharpSensor.IACSharpSensor.SensorReached(1293);
        zde._Encryption_FromZipFile = zde._Encryption = EncryptionAlgorithm.PkzipWeak;
        zde._sourceIsEncrypted = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1294);
      if (zde._extraFieldLength > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1295);
        zde._InputUsesZip64 = (zde._CompressedSize == 0xffffffffu || zde._UncompressedSize == 0xffffffffu || zde._RelativeOffsetOfLocalHeader == 0xffffffffu);
        bytesRead += zde.ProcessExtraField(s, zde._extraFieldLength);
        zde._CompressedFileDataSize = zde._CompressedSize;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1296);
      if (zde._Encryption == EncryptionAlgorithm.PkzipWeak) {
        IACSharpSensor.IACSharpSensor.SensorReached(1297);
        zde._CompressedFileDataSize -= 12;
      } else if (zde.Encryption == EncryptionAlgorithm.WinZipAes128 || zde.Encryption == EncryptionAlgorithm.WinZipAes256) {
        IACSharpSensor.IACSharpSensor.SensorReached(1298);
        zde._CompressedFileDataSize = zde.CompressedSize - (ZipEntry.GetLengthOfCryptoHeaderBytes(zde.Encryption) + 10);
        zde._LengthOfTrailer = 10;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1299);
      if ((zde._BitField & 0x8) == 0x8) {
        IACSharpSensor.IACSharpSensor.SensorReached(1300);
        if (zde._InputUsesZip64) {
          IACSharpSensor.IACSharpSensor.SensorReached(1301);
          zde._LengthOfTrailer += 24;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1302);
          zde._LengthOfTrailer += 16;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1303);
      zde.AlternateEncoding = ((zde._BitField & 0x800) == 0x800) ? System.Text.Encoding.UTF8 : expectedEncoding;
      zde.AlternateEncodingUsage = ZipOption.Always;
      IACSharpSensor.IACSharpSensor.SensorReached(1304);
      if (zde._commentLength > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1305);
        block = new byte[zde._commentLength];
        n = s.Read(block, 0, block.Length);
        bytesRead += n;
        IACSharpSensor.IACSharpSensor.SensorReached(1306);
        if ((zde._BitField & 0x800) == 0x800) {
          IACSharpSensor.IACSharpSensor.SensorReached(1307);
          zde._Comment = Ionic.Zip.SharedUtilities.Utf8StringFromBuffer(block);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1308);
          zde._Comment = Ionic.Zip.SharedUtilities.StringFromBuffer(block, expectedEncoding);
        }
      }
      ZipEntry RNTRNTRNT_225 = zde;
      IACSharpSensor.IACSharpSensor.SensorReached(1309);
      return RNTRNTRNT_225;
    }
    static internal bool IsNotValidZipDirEntrySig(int signature)
    {
      System.Boolean RNTRNTRNT_226 = (signature != ZipConstants.ZipDirEntrySignature);
      IACSharpSensor.IACSharpSensor.SensorReached(1310);
      return RNTRNTRNT_226;
    }
    private Int16 _VersionMadeBy;
    private Int16 _InternalFileAttrs;
    private Int32 _ExternalFileAttrs;
    private Int16 _filenameLength;
    private Int16 _extraFieldLength;
    private Int16 _commentLength;
  }
}
