using System;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public enum SelfExtractorFlavor
  {
    ConsoleApplication = 0,
    WinFormsApplication
  }
  public class SelfExtractorSaveOptions
  {
    public SelfExtractorFlavor Flavor { get; set; }
    public String PostExtractCommandLine { get; set; }
    public String DefaultExtractDirectory { get; set; }
    public string IconFile { get; set; }
    public bool Quiet { get; set; }
    public Ionic.Zip.ExtractExistingFileAction ExtractExistingFile { get; set; }
    public bool RemoveUnpackedFilesAfterExecute { get; set; }
    public Version FileVersion { get; set; }
    public String ProductVersion { get; set; }
    public String Copyright { get; set; }
    public String Description { get; set; }
    public String ProductName { get; set; }
    public String SfxExeWindowTitle { get; set; }
    public string AdditionalCompilerSwitches { get; set; }
  }
  partial class ZipFile
  {
    class ExtractorSettings
    {
      public SelfExtractorFlavor Flavor;
      public List<string> ReferencedAssemblies;
      public List<string> CopyThroughResources;
      public List<string> ResourcesToCompile;
    }
    private static ExtractorSettings[] SettingsList = {
      new ExtractorSettings {
        Flavor = SelfExtractorFlavor.WinFormsApplication,
        ReferencedAssemblies = new List<string> {
          "System.dll",
          "System.Windows.Forms.dll",
          "System.Drawing.dll"
        },
        CopyThroughResources = new List<string> {
          "Ionic.Zip.WinFormsSelfExtractorStub.resources",
          "Ionic.Zip.Forms.PasswordDialog.resources",
          "Ionic.Zip.Forms.ZipContentsDialog.resources"
        },
        ResourcesToCompile = new List<string> {
          "WinFormsSelfExtractorStub.cs",
          "WinFormsSelfExtractorStub.Designer.cs",
          "PasswordDialog.cs",
          "PasswordDialog.Designer.cs",
          "ZipContentsDialog.cs",
          "ZipContentsDialog.Designer.cs",
          "FolderBrowserDialogEx.cs"
        }
      },
      new ExtractorSettings {
        Flavor = SelfExtractorFlavor.ConsoleApplication,
        ReferencedAssemblies = new List<string> { "System.dll" },
        CopyThroughResources = null,
        ResourcesToCompile = new List<string> { "CommandLineSelfExtractorStub.cs" }
      }
    };
    public void SaveSelfExtractor(string exeToGenerate, SelfExtractorFlavor flavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3427);
      SelfExtractorSaveOptions options = new SelfExtractorSaveOptions();
      options.Flavor = flavor;
      SaveSelfExtractor(exeToGenerate, options);
      IACSharpSensor.IACSharpSensor.SensorReached(3428);
    }
    public void SaveSelfExtractor(string exeToGenerate, SelfExtractorSaveOptions options)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3429);
      if (_name == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3430);
        _writestream = null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3431);
      _SavingSfx = true;
      _name = exeToGenerate;
      IACSharpSensor.IACSharpSensor.SensorReached(3432);
      if (Directory.Exists(_name)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3433);
        throw new ZipException("Bad Directory", new System.ArgumentException("That name specifies an existing directory. Please specify a filename.", "exeToGenerate"));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3434);
      _contentsChanged = true;
      _fileAlreadyExists = File.Exists(_name);
      _SaveSfxStub(exeToGenerate, options);
      Save();
      _SavingSfx = false;
      IACSharpSensor.IACSharpSensor.SensorReached(3435);
    }
    private static void ExtractResourceToFile(Assembly a, string resourceName, string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3436);
      int n = 0;
      byte[] bytes = new byte[1024];
      IACSharpSensor.IACSharpSensor.SensorReached(3437);
      using (Stream instream = a.GetManifestResourceStream(resourceName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3438);
        if (instream == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3439);
          throw new ZipException(String.Format("missing resource '{0}'", resourceName));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3440);
        using (FileStream outstream = File.OpenWrite(filename)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3441);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(3442);
            n = instream.Read(bytes, 0, bytes.Length);
            outstream.Write(bytes, 0, n);
          } while (n > 0);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3443);
    }
    private void _SaveSfxStub(string exeToGenerate, SelfExtractorSaveOptions options)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3444);
      string nameOfIconFile = null;
      string stubExe = null;
      string unpackedResourceDir = null;
      string tmpDir = null;
      IACSharpSensor.IACSharpSensor.SensorReached(3445);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3446);
        if (File.Exists(exeToGenerate)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3447);
          if (Verbose) {
            IACSharpSensor.IACSharpSensor.SensorReached(3448);
            StatusMessageTextWriter.WriteLine("The existing file ({0}) will be overwritten.", exeToGenerate);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3449);
        if (!exeToGenerate.EndsWith(".exe")) {
          IACSharpSensor.IACSharpSensor.SensorReached(3450);
          if (Verbose) {
            IACSharpSensor.IACSharpSensor.SensorReached(3451);
            StatusMessageTextWriter.WriteLine("Warning: The generated self-extracting file will not have an .exe extension.");
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3452);
        tmpDir = TempFileFolder ?? Path.GetDirectoryName(exeToGenerate);
        stubExe = GenerateTempPathname(tmpDir, "exe");
        Assembly a1 = typeof(ZipFile).Assembly;
        IACSharpSensor.IACSharpSensor.SensorReached(3453);
        using (var csharp = new Microsoft.CSharp.CSharpCodeProvider()) {
          IACSharpSensor.IACSharpSensor.SensorReached(3454);
          ExtractorSettings settings = null;
          IACSharpSensor.IACSharpSensor.SensorReached(3455);
          foreach (var x in SettingsList) {
            IACSharpSensor.IACSharpSensor.SensorReached(3456);
            if (x.Flavor == options.Flavor) {
              IACSharpSensor.IACSharpSensor.SensorReached(3457);
              settings = x;
              IACSharpSensor.IACSharpSensor.SensorReached(3458);
              break;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3459);
          if (settings == null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3460);
            throw new BadStateException(String.Format("While saving a Self-Extracting Zip, Cannot find that flavor ({0})?", options.Flavor));
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3461);
          var cp = new System.CodeDom.Compiler.CompilerParameters();
          cp.ReferencedAssemblies.Add(a1.Location);
          IACSharpSensor.IACSharpSensor.SensorReached(3462);
          if (settings.ReferencedAssemblies != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3463);
            foreach (string ra in settings.ReferencedAssemblies) {
              IACSharpSensor.IACSharpSensor.SensorReached(3464);
              cp.ReferencedAssemblies.Add(ra);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3465);
          cp.GenerateInMemory = false;
          cp.GenerateExecutable = true;
          cp.IncludeDebugInformation = false;
          cp.CompilerOptions = "";
          Assembly a2 = Assembly.GetExecutingAssembly();
          var sb = new System.Text.StringBuilder();
          string sourceFile = GenerateTempPathname(tmpDir, "cs");
          IACSharpSensor.IACSharpSensor.SensorReached(3466);
          using (ZipFile zip = ZipFile.Read(a2.GetManifestResourceStream("Ionic.Zip.Resources.ZippedResources.zip"))) {
            IACSharpSensor.IACSharpSensor.SensorReached(3467);
            unpackedResourceDir = GenerateTempPathname(tmpDir, "tmp");
            IACSharpSensor.IACSharpSensor.SensorReached(3468);
            if (String.IsNullOrEmpty(options.IconFile)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3469);
              System.IO.Directory.CreateDirectory(unpackedResourceDir);
              ZipEntry e = zip["zippedFile.ico"];
              IACSharpSensor.IACSharpSensor.SensorReached(3470);
              if ((e.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
                IACSharpSensor.IACSharpSensor.SensorReached(3471);
                e.Attributes ^= FileAttributes.ReadOnly;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(3472);
              e.Extract(unpackedResourceDir);
              nameOfIconFile = Path.Combine(unpackedResourceDir, "zippedFile.ico");
              cp.CompilerOptions += String.Format("/win32icon:\"{0}\"", nameOfIconFile);
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(3473);
              cp.CompilerOptions += String.Format("/win32icon:\"{0}\"", options.IconFile);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3474);
            cp.OutputAssembly = stubExe;
            IACSharpSensor.IACSharpSensor.SensorReached(3475);
            if (options.Flavor == SelfExtractorFlavor.WinFormsApplication) {
              IACSharpSensor.IACSharpSensor.SensorReached(3476);
              cp.CompilerOptions += " /target:winexe";
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3477);
            if (!String.IsNullOrEmpty(options.AdditionalCompilerSwitches)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3478);
              cp.CompilerOptions += " " + options.AdditionalCompilerSwitches;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3479);
            if (String.IsNullOrEmpty(cp.CompilerOptions)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3480);
              cp.CompilerOptions = null;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3481);
            if ((settings.CopyThroughResources != null) && (settings.CopyThroughResources.Count != 0)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3482);
              if (!Directory.Exists(unpackedResourceDir)) {
                IACSharpSensor.IACSharpSensor.SensorReached(3483);
                System.IO.Directory.CreateDirectory(unpackedResourceDir);
              }
              IACSharpSensor.IACSharpSensor.SensorReached(3484);
              foreach (string re in settings.CopyThroughResources) {
                IACSharpSensor.IACSharpSensor.SensorReached(3485);
                string filename = Path.Combine(unpackedResourceDir, re);
                ExtractResourceToFile(a2, re, filename);
                cp.EmbeddedResources.Add(filename);
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3486);
            cp.EmbeddedResources.Add(a1.Location);
            sb.Append("// " + Path.GetFileName(sourceFile) + "\n").Append("// --------------------------------------------\n//\n").Append("// This SFX source file was generated by DotNetZip ").Append(ZipFile.LibraryVersion.ToString()).Append("\n//         at ").Append(System.DateTime.Now.ToString("yyyy MMMM dd  HH:mm:ss")).Append("\n//\n// --------------------------------------------\n\n\n");
            IACSharpSensor.IACSharpSensor.SensorReached(3487);
            if (!String.IsNullOrEmpty(options.Description)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3488);
              sb.Append("[assembly: System.Reflection.AssemblyTitle(\"" + options.Description.Replace("\"", "") + "\")]\n");
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(3489);
              sb.Append("[assembly: System.Reflection.AssemblyTitle(\"DotNetZip SFX Archive\")]\n");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3490);
            if (!String.IsNullOrEmpty(options.ProductVersion)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3491);
              sb.Append("[assembly: System.Reflection.AssemblyInformationalVersion(\"" + options.ProductVersion.Replace("\"", "") + "\")]\n");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3492);
            string copyright = (String.IsNullOrEmpty(options.Copyright)) ? "Extractor: Copyright � Dino Chiesa 2008-2011" : options.Copyright.Replace("\"", "");
            IACSharpSensor.IACSharpSensor.SensorReached(3493);
            if (!String.IsNullOrEmpty(options.ProductName)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3494);
              sb.Append("[assembly: System.Reflection.AssemblyProduct(\"").Append(options.ProductName.Replace("\"", "")).Append("\")]\n");
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(3495);
              sb.Append("[assembly: System.Reflection.AssemblyProduct(\"DotNetZip\")]\n");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3496);
            sb.Append("[assembly: System.Reflection.AssemblyCopyright(\"" + copyright + "\")]\n").Append(String.Format("[assembly: System.Reflection.AssemblyVersion(\"{0}\")]\n", ZipFile.LibraryVersion.ToString()));
            IACSharpSensor.IACSharpSensor.SensorReached(3497);
            if (options.FileVersion != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(3498);
              sb.Append(String.Format("[assembly: System.Reflection.AssemblyFileVersion(\"{0}\")]\n", options.FileVersion.ToString()));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3499);
            sb.Append("\n\n\n");
            string extractLoc = options.DefaultExtractDirectory;
            IACSharpSensor.IACSharpSensor.SensorReached(3500);
            if (extractLoc != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(3501);
              extractLoc = extractLoc.Replace("\"", "").Replace("\\", "\\\\");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3502);
            string postExCmdLine = options.PostExtractCommandLine;
            IACSharpSensor.IACSharpSensor.SensorReached(3503);
            if (postExCmdLine != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(3504);
              postExCmdLine = postExCmdLine.Replace("\\", "\\\\");
              postExCmdLine = postExCmdLine.Replace("\"", "\\\"");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3505);
            foreach (string rc in settings.ResourcesToCompile) {
              IACSharpSensor.IACSharpSensor.SensorReached(3506);
              using (Stream s = zip[rc].OpenReader()) {
                IACSharpSensor.IACSharpSensor.SensorReached(3507);
                if (s == null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(3508);
                  throw new ZipException(String.Format("missing resource '{0}'", rc));
                }
                IACSharpSensor.IACSharpSensor.SensorReached(3509);
                using (StreamReader sr = new StreamReader(s)) {
                  IACSharpSensor.IACSharpSensor.SensorReached(3510);
                  while (sr.Peek() >= 0) {
                    IACSharpSensor.IACSharpSensor.SensorReached(3511);
                    string line = sr.ReadLine();
                    IACSharpSensor.IACSharpSensor.SensorReached(3512);
                    if (extractLoc != null) {
                      IACSharpSensor.IACSharpSensor.SensorReached(3513);
                      line = line.Replace("@@EXTRACTLOCATION", extractLoc);
                    }
                    IACSharpSensor.IACSharpSensor.SensorReached(3514);
                    line = line.Replace("@@REMOVE_AFTER_EXECUTE", options.RemoveUnpackedFilesAfterExecute.ToString());
                    line = line.Replace("@@QUIET", options.Quiet.ToString());
                    IACSharpSensor.IACSharpSensor.SensorReached(3515);
                    if (!String.IsNullOrEmpty(options.SfxExeWindowTitle)) {
                      IACSharpSensor.IACSharpSensor.SensorReached(3516);
                      line = line.Replace("@@SFX_EXE_WINDOW_TITLE", options.SfxExeWindowTitle);
                    }
                    IACSharpSensor.IACSharpSensor.SensorReached(3517);
                    line = line.Replace("@@EXTRACT_EXISTING_FILE", ((int)options.ExtractExistingFile).ToString());
                    IACSharpSensor.IACSharpSensor.SensorReached(3518);
                    if (postExCmdLine != null) {
                      IACSharpSensor.IACSharpSensor.SensorReached(3519);
                      line = line.Replace("@@POST_UNPACK_CMD_LINE", postExCmdLine);
                    }
                    IACSharpSensor.IACSharpSensor.SensorReached(3520);
                    sb.Append(line).Append("\n");
                  }
                }
                IACSharpSensor.IACSharpSensor.SensorReached(3521);
                sb.Append("\n\n");
              }
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3522);
          string LiteralSource = sb.ToString();
          var cr = csharp.CompileAssemblyFromSource(cp, LiteralSource);
          IACSharpSensor.IACSharpSensor.SensorReached(3523);
          if (cr == null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3524);
            throw new SfxGenerationException("Cannot compile the extraction logic!");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3525);
          if (Verbose) {
            IACSharpSensor.IACSharpSensor.SensorReached(3526);
            foreach (string output in cr.Output) {
              IACSharpSensor.IACSharpSensor.SensorReached(3527);
              StatusMessageTextWriter.WriteLine(output);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3528);
          if (cr.Errors.Count != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(3529);
            using (TextWriter tw = new StreamWriter(sourceFile)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3530);
              tw.Write(LiteralSource);
              tw.Write("\n\n\n// ------------------------------------------------------------------\n");
              tw.Write("// Errors during compilation: \n//\n");
              string p = Path.GetFileName(sourceFile);
              IACSharpSensor.IACSharpSensor.SensorReached(3531);
              foreach (System.CodeDom.Compiler.CompilerError error in cr.Errors) {
                IACSharpSensor.IACSharpSensor.SensorReached(3532);
                tw.Write(String.Format("//   {0}({1},{2}): {3} {4}: {5}\n//\n", p, error.Line, error.Column, error.IsWarning ? "Warning" : "error", error.ErrorNumber, error.ErrorText));
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3533);
            throw new SfxGenerationException(String.Format("Errors compiling the extraction logic!  {0}", sourceFile));
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3534);
          OnSaveEvent(ZipProgressEventType.Saving_AfterCompileSelfExtractor);
          IACSharpSensor.IACSharpSensor.SensorReached(3535);
          using (System.IO.Stream input = System.IO.File.OpenRead(stubExe)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3536);
            byte[] buffer = new byte[4000];
            int n = 1;
            IACSharpSensor.IACSharpSensor.SensorReached(3537);
            while (n != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(3538);
              n = input.Read(buffer, 0, buffer.Length);
              IACSharpSensor.IACSharpSensor.SensorReached(3539);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(3540);
                WriteStream.Write(buffer, 0, n);
              }
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3541);
        OnSaveEvent(ZipProgressEventType.Saving_AfterSaveTempArchive);
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(3542);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(3543);
          if (Directory.Exists(unpackedResourceDir)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3544);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(3545);
              Directory.Delete(unpackedResourceDir, true);
            } catch (System.IO.IOException exc1) {
              IACSharpSensor.IACSharpSensor.SensorReached(3546);
              StatusMessageTextWriter.WriteLine("Warning: Exception: {0}", exc1);
              IACSharpSensor.IACSharpSensor.SensorReached(3547);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3548);
          if (File.Exists(stubExe)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3549);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(3550);
              File.Delete(stubExe);
            } catch (System.IO.IOException exc1) {
              IACSharpSensor.IACSharpSensor.SensorReached(3551);
              StatusMessageTextWriter.WriteLine("Warning: Exception: {0}", exc1);
              IACSharpSensor.IACSharpSensor.SensorReached(3552);
            }
          }
        } catch (System.IO.IOException) {
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3553);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3554);
      return;
    }
    static internal string GenerateTempPathname(string dir, string extension)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3555);
      string candidate = null;
      String AppName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
      IACSharpSensor.IACSharpSensor.SensorReached(3556);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(3557);
        string uuid = System.Guid.NewGuid().ToString();
        string Name = String.Format("{0}-{1}-{2}.{3}", AppName, System.DateTime.Now.ToString("yyyyMMMdd-HHmmss"), uuid, extension);
        candidate = System.IO.Path.Combine(dir, Name);
      } while (System.IO.File.Exists(candidate) || System.IO.Directory.Exists(candidate));
      System.String RNTRNTRNT_465 = candidate;
      IACSharpSensor.IACSharpSensor.SensorReached(3558);
      return RNTRNTRNT_465;
    }
  }
}
