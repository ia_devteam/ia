using System;
using System.IO;
using RE = System.Text.RegularExpressions;
namespace Ionic.Zip
{
  public partial class ZipEntry
  {
    internal void WriteCentralDirectoryEntry(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1949);
      byte[] bytes = new byte[4096];
      int i = 0;
      bytes[i++] = (byte)(ZipConstants.ZipDirEntrySignature & 0xff);
      bytes[i++] = (byte)((ZipConstants.ZipDirEntrySignature & 0xff00) >> 8);
      bytes[i++] = (byte)((ZipConstants.ZipDirEntrySignature & 0xff0000) >> 16);
      bytes[i++] = (byte)((ZipConstants.ZipDirEntrySignature & 0xff000000u) >> 24);
      bytes[i++] = (byte)(_VersionMadeBy & 0xff);
      bytes[i++] = (byte)((_VersionMadeBy & 0xff00) >> 8);
      Int16 vNeeded = (Int16)(VersionNeeded != 0 ? VersionNeeded : 20);
      IACSharpSensor.IACSharpSensor.SensorReached(1950);
      if (_OutputUsesZip64 == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1951);
        _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1952);
      Int16 versionNeededToExtract = (Int16)(_OutputUsesZip64.Value ? 45 : vNeeded);
      IACSharpSensor.IACSharpSensor.SensorReached(1953);
      if (this.CompressionMethod == Ionic.Zip.CompressionMethod.BZip2) {
        IACSharpSensor.IACSharpSensor.SensorReached(1954);
        versionNeededToExtract = 46;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1955);
      bytes[i++] = (byte)(versionNeededToExtract & 0xff);
      bytes[i++] = (byte)((versionNeededToExtract & 0xff00) >> 8);
      bytes[i++] = (byte)(_BitField & 0xff);
      bytes[i++] = (byte)((_BitField & 0xff00) >> 8);
      bytes[i++] = (byte)(_CompressionMethod & 0xff);
      bytes[i++] = (byte)((_CompressionMethod & 0xff00) >> 8);
      IACSharpSensor.IACSharpSensor.SensorReached(1956);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        IACSharpSensor.IACSharpSensor.SensorReached(1957);
        i -= 2;
        bytes[i++] = 0x63;
        bytes[i++] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1958);
      bytes[i++] = (byte)(_TimeBlob & 0xff);
      bytes[i++] = (byte)((_TimeBlob & 0xff00) >> 8);
      bytes[i++] = (byte)((_TimeBlob & 0xff0000) >> 16);
      bytes[i++] = (byte)((_TimeBlob & 0xff000000u) >> 24);
      bytes[i++] = (byte)(_Crc32 & 0xff);
      bytes[i++] = (byte)((_Crc32 & 0xff00) >> 8);
      bytes[i++] = (byte)((_Crc32 & 0xff0000) >> 16);
      bytes[i++] = (byte)((_Crc32 & 0xff000000u) >> 24);
      int j = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1959);
      if (_OutputUsesZip64.Value) {
        IACSharpSensor.IACSharpSensor.SensorReached(1960);
        for (j = 0; j < 8; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(1961);
          bytes[i++] = 0xff;
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(1962);
        bytes[i++] = (byte)(_CompressedSize & 0xff);
        bytes[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
        bytes[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
        bytes[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
        bytes[i++] = (byte)(_UncompressedSize & 0xff);
        bytes[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
        bytes[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
        bytes[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1963);
      byte[] fileNameBytes = GetEncodedFileNameBytes();
      Int16 filenameLength = (Int16)fileNameBytes.Length;
      bytes[i++] = (byte)(filenameLength & 0xff);
      bytes[i++] = (byte)((filenameLength & 0xff00) >> 8);
      _presumeZip64 = _OutputUsesZip64.Value;
      _Extra = ConstructExtraField(true);
      Int16 extraFieldLength = (Int16)((_Extra == null) ? 0 : _Extra.Length);
      bytes[i++] = (byte)(extraFieldLength & 0xff);
      bytes[i++] = (byte)((extraFieldLength & 0xff00) >> 8);
      int commentLength = (_CommentBytes == null) ? 0 : _CommentBytes.Length;
      IACSharpSensor.IACSharpSensor.SensorReached(1964);
      if (commentLength + i > bytes.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(1965);
        commentLength = bytes.Length - i;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1966);
      bytes[i++] = (byte)(commentLength & 0xff);
      bytes[i++] = (byte)((commentLength & 0xff00) >> 8);
      bool segmented = (this._container.ZipFile != null) && (this._container.ZipFile.MaxOutputSegmentSize != 0);
      IACSharpSensor.IACSharpSensor.SensorReached(1967);
      if (segmented) {
        IACSharpSensor.IACSharpSensor.SensorReached(1968);
        bytes[i++] = (byte)(_diskNumber & 0xff);
        bytes[i++] = (byte)((_diskNumber & 0xff00) >> 8);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(1969);
        bytes[i++] = 0;
        bytes[i++] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1970);
      bytes[i++] = (byte)((_IsText) ? 1 : 0);
      bytes[i++] = 0;
      bytes[i++] = (byte)(_ExternalFileAttrs & 0xff);
      bytes[i++] = (byte)((_ExternalFileAttrs & 0xff00) >> 8);
      bytes[i++] = (byte)((_ExternalFileAttrs & 0xff0000) >> 16);
      bytes[i++] = (byte)((_ExternalFileAttrs & 0xff000000u) >> 24);
      IACSharpSensor.IACSharpSensor.SensorReached(1971);
      if (_RelativeOffsetOfLocalHeader > 0xffffffffL) {
        IACSharpSensor.IACSharpSensor.SensorReached(1972);
        bytes[i++] = 0xff;
        bytes[i++] = 0xff;
        bytes[i++] = 0xff;
        bytes[i++] = 0xff;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(1973);
        bytes[i++] = (byte)(_RelativeOffsetOfLocalHeader & 0xff);
        bytes[i++] = (byte)((_RelativeOffsetOfLocalHeader & 0xff00) >> 8);
        bytes[i++] = (byte)((_RelativeOffsetOfLocalHeader & 0xff0000) >> 16);
        bytes[i++] = (byte)((_RelativeOffsetOfLocalHeader & 0xff000000u) >> 24);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1974);
      Buffer.BlockCopy(fileNameBytes, 0, bytes, i, filenameLength);
      i += filenameLength;
      IACSharpSensor.IACSharpSensor.SensorReached(1975);
      if (_Extra != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1976);
        byte[] h = _Extra;
        int offx = 0;
        Buffer.BlockCopy(h, offx, bytes, i, extraFieldLength);
        i += extraFieldLength;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1977);
      if (commentLength != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1978);
        Buffer.BlockCopy(_CommentBytes, 0, bytes, i, commentLength);
        i += commentLength;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1979);
      s.Write(bytes, 0, i);
      IACSharpSensor.IACSharpSensor.SensorReached(1980);
    }
    private byte[] ConstructExtraField(bool forCentralDirectory)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1981);
      var listOfBlocks = new System.Collections.Generic.List<byte[]>();
      byte[] block;
      IACSharpSensor.IACSharpSensor.SensorReached(1982);
      if (_container.Zip64 == Zip64Option.Always || (_container.Zip64 == Zip64Option.AsNecessary && (!forCentralDirectory || _entryRequiresZip64.Value))) {
        IACSharpSensor.IACSharpSensor.SensorReached(1983);
        int sz = 4 + (forCentralDirectory ? 28 : 16);
        block = new byte[sz];
        int i = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(1984);
        if (_presumeZip64 || forCentralDirectory) {
          IACSharpSensor.IACSharpSensor.SensorReached(1985);
          block[i++] = 0x1;
          block[i++] = 0x0;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1986);
          block[i++] = 0x99;
          block[i++] = 0x99;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1987);
        block[i++] = (byte)(sz - 4);
        block[i++] = 0x0;
        Array.Copy(BitConverter.GetBytes(_UncompressedSize), 0, block, i, 8);
        i += 8;
        Array.Copy(BitConverter.GetBytes(_CompressedSize), 0, block, i, 8);
        i += 8;
        IACSharpSensor.IACSharpSensor.SensorReached(1988);
        if (forCentralDirectory) {
          IACSharpSensor.IACSharpSensor.SensorReached(1989);
          Array.Copy(BitConverter.GetBytes(_RelativeOffsetOfLocalHeader), 0, block, i, 8);
          i += 8;
          Array.Copy(BitConverter.GetBytes(0), 0, block, i, 4);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1990);
        listOfBlocks.Add(block);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1991);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        IACSharpSensor.IACSharpSensor.SensorReached(1992);
        block = new byte[4 + 7];
        int i = 0;
        block[i++] = 0x1;
        block[i++] = 0x99;
        block[i++] = 0x7;
        block[i++] = 0x0;
        block[i++] = 0x1;
        block[i++] = 0x0;
        block[i++] = 0x41;
        block[i++] = 0x45;
        int keystrength = GetKeyStrengthInBits(Encryption);
        IACSharpSensor.IACSharpSensor.SensorReached(1993);
        if (keystrength == 128) {
          IACSharpSensor.IACSharpSensor.SensorReached(1994);
          block[i] = 1;
        } else if (keystrength == 256) {
          IACSharpSensor.IACSharpSensor.SensorReached(1996);
          block[i] = 3;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1995);
          block[i] = 0xff;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1997);
        i++;
        block[i++] = (byte)(_CompressionMethod & 0xff);
        block[i++] = (byte)(_CompressionMethod & 0xff00);
        listOfBlocks.Add(block);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1998);
      if (_ntfsTimesAreSet && _emitNtfsTimes) {
        IACSharpSensor.IACSharpSensor.SensorReached(1999);
        block = new byte[32 + 4];
        int i = 0;
        block[i++] = 0xa;
        block[i++] = 0x0;
        block[i++] = 32;
        block[i++] = 0;
        i += 4;
        block[i++] = 0x1;
        block[i++] = 0x0;
        block[i++] = 24;
        block[i++] = 0;
        Int64 z = _Mtime.ToFileTime();
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 8);
        i += 8;
        z = _Atime.ToFileTime();
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 8);
        i += 8;
        z = _Ctime.ToFileTime();
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 8);
        i += 8;
        listOfBlocks.Add(block);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2000);
      if (_ntfsTimesAreSet && _emitUnixTimes) {
        IACSharpSensor.IACSharpSensor.SensorReached(2001);
        int len = 5 + 4;
        IACSharpSensor.IACSharpSensor.SensorReached(2002);
        if (!forCentralDirectory) {
          IACSharpSensor.IACSharpSensor.SensorReached(2003);
          len += 8;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2004);
        block = new byte[len];
        int i = 0;
        block[i++] = 0x55;
        block[i++] = 0x54;
        block[i++] = unchecked((byte)(len - 4));
        block[i++] = 0;
        block[i++] = 0x7;
        Int32 z = unchecked((int)((_Mtime - _unixEpoch).TotalSeconds));
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 4);
        i += 4;
        IACSharpSensor.IACSharpSensor.SensorReached(2005);
        if (!forCentralDirectory) {
          IACSharpSensor.IACSharpSensor.SensorReached(2006);
          z = unchecked((int)((_Atime - _unixEpoch).TotalSeconds));
          Array.Copy(BitConverter.GetBytes(z), 0, block, i, 4);
          i += 4;
          z = unchecked((int)((_Ctime - _unixEpoch).TotalSeconds));
          Array.Copy(BitConverter.GetBytes(z), 0, block, i, 4);
          i += 4;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2007);
        listOfBlocks.Add(block);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2008);
      byte[] aggregateBlock = null;
      IACSharpSensor.IACSharpSensor.SensorReached(2009);
      if (listOfBlocks.Count > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2010);
        int totalLength = 0;
        int i, current = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(2011);
        for (i = 0; i < listOfBlocks.Count; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(2012);
          totalLength += listOfBlocks[i].Length;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2013);
        aggregateBlock = new byte[totalLength];
        IACSharpSensor.IACSharpSensor.SensorReached(2014);
        for (i = 0; i < listOfBlocks.Count; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(2015);
          System.Array.Copy(listOfBlocks[i], 0, aggregateBlock, current, listOfBlocks[i].Length);
          current += listOfBlocks[i].Length;
        }
      }
      System.Byte[] RNTRNTRNT_314 = aggregateBlock;
      IACSharpSensor.IACSharpSensor.SensorReached(2016);
      return RNTRNTRNT_314;
    }
    private string NormalizeFileName()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2017);
      string SlashFixed = FileName.Replace("\\", "/");
      string s1 = null;
      IACSharpSensor.IACSharpSensor.SensorReached(2018);
      if ((_TrimVolumeFromFullyQualifiedPaths) && (FileName.Length >= 3) && (FileName[1] == ':') && (SlashFixed[2] == '/')) {
        IACSharpSensor.IACSharpSensor.SensorReached(2019);
        s1 = SlashFixed.Substring(3);
      } else if ((FileName.Length >= 4) && ((SlashFixed[0] == '/') && (SlashFixed[1] == '/'))) {
        IACSharpSensor.IACSharpSensor.SensorReached(2022);
        int n = SlashFixed.IndexOf('/', 2);
        IACSharpSensor.IACSharpSensor.SensorReached(2023);
        if (n == -1) {
          IACSharpSensor.IACSharpSensor.SensorReached(2024);
          throw new ArgumentException("The path for that entry appears to be badly formatted");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2025);
        s1 = SlashFixed.Substring(n + 1);
      } else if ((FileName.Length >= 3) && ((SlashFixed[0] == '.') && (SlashFixed[1] == '/'))) {
        IACSharpSensor.IACSharpSensor.SensorReached(2021);
        s1 = SlashFixed.Substring(2);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2020);
        s1 = SlashFixed;
      }
      System.String RNTRNTRNT_315 = s1;
      IACSharpSensor.IACSharpSensor.SensorReached(2026);
      return RNTRNTRNT_315;
    }
    private byte[] GetEncodedFileNameBytes()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2027);
      var s1 = NormalizeFileName();
      IACSharpSensor.IACSharpSensor.SensorReached(2028);
      switch (AlternateEncodingUsage) {
        case ZipOption.Always:
          IACSharpSensor.IACSharpSensor.SensorReached(2029);
          if (!(_Comment == null || _Comment.Length == 0)) {
            IACSharpSensor.IACSharpSensor.SensorReached(2030);
            _CommentBytes = AlternateEncoding.GetBytes(_Comment);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2031);
          _actualEncoding = AlternateEncoding;
          System.Byte[] RNTRNTRNT_316 = AlternateEncoding.GetBytes(s1);
          IACSharpSensor.IACSharpSensor.SensorReached(2032);
          return RNTRNTRNT_316;
        case ZipOption.Never:
          IACSharpSensor.IACSharpSensor.SensorReached(2033);
          if (!(_Comment == null || _Comment.Length == 0)) {
            IACSharpSensor.IACSharpSensor.SensorReached(2034);
            _CommentBytes = ibm437.GetBytes(_Comment);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2035);
          _actualEncoding = ibm437;
          System.Byte[] RNTRNTRNT_317 = ibm437.GetBytes(s1);
          IACSharpSensor.IACSharpSensor.SensorReached(2036);
          return RNTRNTRNT_317;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2037);
      byte[] result = ibm437.GetBytes(s1);
      string s2 = ibm437.GetString(result, 0, result.Length);
      _CommentBytes = null;
      IACSharpSensor.IACSharpSensor.SensorReached(2038);
      if (s2 != s1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2039);
        result = AlternateEncoding.GetBytes(s1);
        IACSharpSensor.IACSharpSensor.SensorReached(2040);
        if (_Comment != null && _Comment.Length != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2041);
          _CommentBytes = AlternateEncoding.GetBytes(_Comment);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2042);
        _actualEncoding = AlternateEncoding;
        System.Byte[] RNTRNTRNT_318 = result;
        IACSharpSensor.IACSharpSensor.SensorReached(2043);
        return RNTRNTRNT_318;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2044);
      _actualEncoding = ibm437;
      IACSharpSensor.IACSharpSensor.SensorReached(2045);
      if (_Comment == null || _Comment.Length == 0) {
        System.Byte[] RNTRNTRNT_319 = result;
        IACSharpSensor.IACSharpSensor.SensorReached(2046);
        return RNTRNTRNT_319;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2047);
      byte[] cbytes = ibm437.GetBytes(_Comment);
      string c2 = ibm437.GetString(cbytes, 0, cbytes.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(2048);
      if (c2 != Comment) {
        IACSharpSensor.IACSharpSensor.SensorReached(2049);
        result = AlternateEncoding.GetBytes(s1);
        _CommentBytes = AlternateEncoding.GetBytes(_Comment);
        _actualEncoding = AlternateEncoding;
        System.Byte[] RNTRNTRNT_320 = result;
        IACSharpSensor.IACSharpSensor.SensorReached(2050);
        return RNTRNTRNT_320;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2051);
      _CommentBytes = cbytes;
      System.Byte[] RNTRNTRNT_321 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(2052);
      return RNTRNTRNT_321;
    }
    private bool WantReadAgain()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2053);
      if (_UncompressedSize < 0x10) {
        System.Boolean RNTRNTRNT_322 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2054);
        return RNTRNTRNT_322;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2055);
      if (_CompressionMethod == 0x0) {
        System.Boolean RNTRNTRNT_323 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2056);
        return RNTRNTRNT_323;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2057);
      if (CompressionLevel == Ionic.Zlib.CompressionLevel.None) {
        System.Boolean RNTRNTRNT_324 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2058);
        return RNTRNTRNT_324;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2059);
      if (_CompressedSize < _UncompressedSize) {
        System.Boolean RNTRNTRNT_325 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2060);
        return RNTRNTRNT_325;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2061);
      if (this._Source == ZipEntrySource.Stream && !this._sourceStream.CanSeek) {
        System.Boolean RNTRNTRNT_326 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2062);
        return RNTRNTRNT_326;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2063);
      if (_aesCrypto_forWrite != null && (CompressedSize - _aesCrypto_forWrite.SizeOfEncryptionMetadata) <= UncompressedSize + 0x10) {
        System.Boolean RNTRNTRNT_327 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2064);
        return RNTRNTRNT_327;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2065);
      if (_zipCrypto_forWrite != null && (CompressedSize - 12) <= UncompressedSize) {
        System.Boolean RNTRNTRNT_328 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2066);
        return RNTRNTRNT_328;
      }
      System.Boolean RNTRNTRNT_329 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(2067);
      return RNTRNTRNT_329;
    }
    private void MaybeUnsetCompressionMethodForWriting(int cycle)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2068);
      if (cycle > 1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2069);
        _CompressionMethod = 0x0;
        IACSharpSensor.IACSharpSensor.SensorReached(2070);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2071);
      if (IsDirectory) {
        IACSharpSensor.IACSharpSensor.SensorReached(2072);
        _CompressionMethod = 0x0;
        IACSharpSensor.IACSharpSensor.SensorReached(2073);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2074);
      if (this._Source == ZipEntrySource.ZipFile) {
        IACSharpSensor.IACSharpSensor.SensorReached(2075);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2076);
      if (this._Source == ZipEntrySource.Stream) {
        IACSharpSensor.IACSharpSensor.SensorReached(2077);
        if (_sourceStream != null && _sourceStream.CanSeek) {
          IACSharpSensor.IACSharpSensor.SensorReached(2078);
          long fileLength = _sourceStream.Length;
          IACSharpSensor.IACSharpSensor.SensorReached(2079);
          if (fileLength == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(2080);
            _CompressionMethod = 0x0;
            IACSharpSensor.IACSharpSensor.SensorReached(2081);
            return;
          }
        }
      } else if ((this._Source == ZipEntrySource.FileSystem) && (SharedUtilities.GetFileLength(LocalFileName) == 0L)) {
        IACSharpSensor.IACSharpSensor.SensorReached(2082);
        _CompressionMethod = 0x0;
        IACSharpSensor.IACSharpSensor.SensorReached(2083);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2084);
      if (SetCompression != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2085);
        CompressionLevel = SetCompression(LocalFileName, _FileNameInArchive);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2086);
      if (CompressionLevel == (short)Ionic.Zlib.CompressionLevel.None && CompressionMethod == Ionic.Zip.CompressionMethod.Deflate) {
        IACSharpSensor.IACSharpSensor.SensorReached(2087);
        _CompressionMethod = 0x0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2088);
      return;
    }
    internal void WriteHeader(Stream s, int cycle)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2089);
      var counter = s as CountingStream;
      _future_ROLH = (counter != null) ? counter.ComputedPosition : s.Position;
      int j = 0, i = 0;
      byte[] block = new byte[30];
      block[i++] = (byte)(ZipConstants.ZipEntrySignature & 0xff);
      block[i++] = (byte)((ZipConstants.ZipEntrySignature & 0xff00) >> 8);
      block[i++] = (byte)((ZipConstants.ZipEntrySignature & 0xff0000) >> 16);
      block[i++] = (byte)((ZipConstants.ZipEntrySignature & 0xff000000u) >> 24);
      _presumeZip64 = (_container.Zip64 == Zip64Option.Always || (_container.Zip64 == Zip64Option.AsNecessary && !s.CanSeek));
      Int16 VersionNeededToExtract = (Int16)(_presumeZip64 ? 45 : 20);
      IACSharpSensor.IACSharpSensor.SensorReached(2090);
      if (this.CompressionMethod == Ionic.Zip.CompressionMethod.BZip2) {
        IACSharpSensor.IACSharpSensor.SensorReached(2091);
        VersionNeededToExtract = 46;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2092);
      block[i++] = (byte)(VersionNeededToExtract & 0xff);
      block[i++] = (byte)((VersionNeededToExtract & 0xff00) >> 8);
      byte[] fileNameBytes = GetEncodedFileNameBytes();
      Int16 filenameLength = (Int16)fileNameBytes.Length;
      IACSharpSensor.IACSharpSensor.SensorReached(2093);
      if (_Encryption == EncryptionAlgorithm.None) {
        IACSharpSensor.IACSharpSensor.SensorReached(2094);
        _BitField &= ~1;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2095);
        _BitField |= 1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2096);
      if (_actualEncoding.CodePage == System.Text.Encoding.UTF8.CodePage) {
        IACSharpSensor.IACSharpSensor.SensorReached(2097);
        _BitField |= 0x800;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2098);
      if (IsDirectory || cycle == 99) {
        IACSharpSensor.IACSharpSensor.SensorReached(2099);
        _BitField &= ~0x8;
        _BitField &= ~0x1;
        Encryption = EncryptionAlgorithm.None;
        Password = null;
      } else if (!s.CanSeek) {
        IACSharpSensor.IACSharpSensor.SensorReached(2100);
        _BitField |= 0x8;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2101);
      block[i++] = (byte)(_BitField & 0xff);
      block[i++] = (byte)((_BitField & 0xff00) >> 8);
      IACSharpSensor.IACSharpSensor.SensorReached(2102);
      if (this.__FileDataPosition == -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2103);
        _CompressedSize = 0;
        _crcCalculated = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2104);
      MaybeUnsetCompressionMethodForWriting(cycle);
      block[i++] = (byte)(_CompressionMethod & 0xff);
      block[i++] = (byte)((_CompressionMethod & 0xff00) >> 8);
      IACSharpSensor.IACSharpSensor.SensorReached(2105);
      if (cycle == 99) {
        IACSharpSensor.IACSharpSensor.SensorReached(2106);
        SetZip64Flags();
      } else if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        IACSharpSensor.IACSharpSensor.SensorReached(2107);
        i -= 2;
        block[i++] = 0x63;
        block[i++] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2108);
      _TimeBlob = Ionic.Zip.SharedUtilities.DateTimeToPacked(LastModified);
      block[i++] = (byte)(_TimeBlob & 0xff);
      block[i++] = (byte)((_TimeBlob & 0xff00) >> 8);
      block[i++] = (byte)((_TimeBlob & 0xff0000) >> 16);
      block[i++] = (byte)((_TimeBlob & 0xff000000u) >> 24);
      block[i++] = (byte)(_Crc32 & 0xff);
      block[i++] = (byte)((_Crc32 & 0xff00) >> 8);
      block[i++] = (byte)((_Crc32 & 0xff0000) >> 16);
      block[i++] = (byte)((_Crc32 & 0xff000000u) >> 24);
      IACSharpSensor.IACSharpSensor.SensorReached(2109);
      if (_presumeZip64) {
        IACSharpSensor.IACSharpSensor.SensorReached(2110);
        for (j = 0; j < 8; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(2111);
          block[i++] = 0xff;
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2112);
        block[i++] = (byte)(_CompressedSize & 0xff);
        block[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
        block[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
        block[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
        block[i++] = (byte)(_UncompressedSize & 0xff);
        block[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
        block[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
        block[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2113);
      block[i++] = (byte)(filenameLength & 0xff);
      block[i++] = (byte)((filenameLength & 0xff00) >> 8);
      _Extra = ConstructExtraField(false);
      Int16 extraFieldLength = (Int16)((_Extra == null) ? 0 : _Extra.Length);
      block[i++] = (byte)(extraFieldLength & 0xff);
      block[i++] = (byte)((extraFieldLength & 0xff00) >> 8);
      byte[] bytes = new byte[i + filenameLength + extraFieldLength];
      Buffer.BlockCopy(block, 0, bytes, 0, i);
      Buffer.BlockCopy(fileNameBytes, 0, bytes, i, fileNameBytes.Length);
      i += fileNameBytes.Length;
      IACSharpSensor.IACSharpSensor.SensorReached(2114);
      if (_Extra != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2115);
        Buffer.BlockCopy(_Extra, 0, bytes, i, _Extra.Length);
        i += _Extra.Length;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2116);
      _LengthOfHeader = i;
      var zss = s as ZipSegmentedStream;
      IACSharpSensor.IACSharpSensor.SensorReached(2117);
      if (zss != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2118);
        zss.ContiguousWrite = true;
        UInt32 requiredSegment = zss.ComputeSegment(i);
        IACSharpSensor.IACSharpSensor.SensorReached(2119);
        if (requiredSegment != zss.CurrentSegment) {
          IACSharpSensor.IACSharpSensor.SensorReached(2120);
          _future_ROLH = 0;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2121);
          _future_ROLH = zss.Position;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2122);
        _diskNumber = requiredSegment;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2123);
      if (_container.Zip64 == Zip64Option.Never && (uint)_RelativeOffsetOfLocalHeader >= 0xffffffffu) {
        IACSharpSensor.IACSharpSensor.SensorReached(2124);
        throw new ZipException("Offset within the zip archive exceeds 0xFFFFFFFF. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2125);
      s.Write(bytes, 0, i);
      IACSharpSensor.IACSharpSensor.SensorReached(2126);
      if (zss != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2127);
        zss.ContiguousWrite = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2128);
      _EntryHeader = bytes;
      IACSharpSensor.IACSharpSensor.SensorReached(2129);
    }
    private Int32 FigureCrc32()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2130);
      if (_crcCalculated == false) {
        IACSharpSensor.IACSharpSensor.SensorReached(2131);
        Stream input = null;
        IACSharpSensor.IACSharpSensor.SensorReached(2132);
        if (this._Source == ZipEntrySource.WriteDelegate) {
          IACSharpSensor.IACSharpSensor.SensorReached(2133);
          var output = new Ionic.Crc.CrcCalculatorStream(Stream.Null);
          this._WriteDelegate(this.FileName, output);
          _Crc32 = output.Crc;
        } else if (this._Source == ZipEntrySource.ZipFile) {
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2134);
          if (this._Source == ZipEntrySource.Stream) {
            IACSharpSensor.IACSharpSensor.SensorReached(2135);
            PrepSourceStream();
            input = this._sourceStream;
          } else if (this._Source == ZipEntrySource.JitStream) {
            IACSharpSensor.IACSharpSensor.SensorReached(2137);
            if (this._sourceStream == null) {
              IACSharpSensor.IACSharpSensor.SensorReached(2138);
              _sourceStream = this._OpenDelegate(this.FileName);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2139);
            PrepSourceStream();
            input = this._sourceStream;
          } else if (this._Source == ZipEntrySource.ZipOutputStream) {
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2136);
            input = File.Open(LocalFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2140);
          var crc32 = new Ionic.Crc.CRC32();
          _Crc32 = crc32.GetCrc32(input);
          IACSharpSensor.IACSharpSensor.SensorReached(2141);
          if (_sourceStream == null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2142);
            input.Dispose();
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2143);
        _crcCalculated = true;
      }
      Int32 RNTRNTRNT_330 = _Crc32;
      IACSharpSensor.IACSharpSensor.SensorReached(2144);
      return RNTRNTRNT_330;
    }
    private void PrepSourceStream()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2145);
      if (_sourceStream == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2146);
        throw new ZipException(String.Format("The input stream is null for entry '{0}'.", FileName));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2147);
      if (this._sourceStreamOriginalPosition != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2148);
        this._sourceStream.Position = this._sourceStreamOriginalPosition.Value;
      } else if (this._sourceStream.CanSeek) {
        IACSharpSensor.IACSharpSensor.SensorReached(2151);
        this._sourceStreamOriginalPosition = new Nullable<Int64>(this._sourceStream.Position);
      } else if (this.Encryption == EncryptionAlgorithm.PkzipWeak) {
        IACSharpSensor.IACSharpSensor.SensorReached(2149);
        if (this._Source != ZipEntrySource.ZipFile && ((this._BitField & 0x8) != 0x8)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2150);
          throw new ZipException("It is not possible to use PKZIP encryption on a non-seekable input stream");
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2152);
    }
    internal void CopyMetaData(ZipEntry source)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2153);
      this.__FileDataPosition = source.__FileDataPosition;
      this.CompressionMethod = source.CompressionMethod;
      this._CompressionMethod_FromZipFile = source._CompressionMethod_FromZipFile;
      this._CompressedFileDataSize = source._CompressedFileDataSize;
      this._UncompressedSize = source._UncompressedSize;
      this._BitField = source._BitField;
      this._Source = source._Source;
      this._LastModified = source._LastModified;
      this._Mtime = source._Mtime;
      this._Atime = source._Atime;
      this._Ctime = source._Ctime;
      this._ntfsTimesAreSet = source._ntfsTimesAreSet;
      this._emitUnixTimes = source._emitUnixTimes;
      this._emitNtfsTimes = source._emitNtfsTimes;
      IACSharpSensor.IACSharpSensor.SensorReached(2154);
    }
    private void OnWriteBlock(Int64 bytesXferred, Int64 totalBytesToXfer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2155);
      if (_container.ZipFile != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2156);
        _ioOperationCanceled = _container.ZipFile.OnSaveBlock(this, bytesXferred, totalBytesToXfer);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2157);
    }
    private void _WriteEntryData(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2158);
      Stream input = null;
      long fdp = -1L;
      IACSharpSensor.IACSharpSensor.SensorReached(2159);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2160);
        fdp = s.Position;
      } catch (Exception) {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2161);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2162);
        long fileLength = SetInputAndFigureFileLength(ref input);
        CountingStream entryCounter = new CountingStream(s);
        Stream encryptor;
        Stream compressor;
        IACSharpSensor.IACSharpSensor.SensorReached(2163);
        if (fileLength != 0L) {
          IACSharpSensor.IACSharpSensor.SensorReached(2164);
          encryptor = MaybeApplyEncryption(entryCounter);
          compressor = MaybeApplyCompression(encryptor, fileLength);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2165);
          encryptor = compressor = entryCounter;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2166);
        var output = new Ionic.Crc.CrcCalculatorStream(compressor, true);
        IACSharpSensor.IACSharpSensor.SensorReached(2167);
        if (this._Source == ZipEntrySource.WriteDelegate) {
          IACSharpSensor.IACSharpSensor.SensorReached(2168);
          this._WriteDelegate(this.FileName, output);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2169);
          byte[] buffer = new byte[BufferSize];
          int n;
          IACSharpSensor.IACSharpSensor.SensorReached(2170);
          while ((n = SharedUtilities.ReadWithRetry(input, buffer, 0, buffer.Length, FileName)) != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(2171);
            output.Write(buffer, 0, n);
            OnWriteBlock(output.TotalBytesSlurped, fileLength);
            IACSharpSensor.IACSharpSensor.SensorReached(2172);
            if (_ioOperationCanceled) {
              IACSharpSensor.IACSharpSensor.SensorReached(2173);
              break;
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2174);
        FinishOutputStream(s, entryCounter, encryptor, compressor, output);
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(2175);
        if (this._Source == ZipEntrySource.JitStream) {
          IACSharpSensor.IACSharpSensor.SensorReached(2176);
          if (this._CloseDelegate != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2177);
            this._CloseDelegate(this.FileName, input);
          }
        } else if ((input as FileStream) != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2178);
          input.Dispose();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2179);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2180);
      if (_ioOperationCanceled) {
        IACSharpSensor.IACSharpSensor.SensorReached(2181);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2182);
      this.__FileDataPosition = fdp;
      PostProcessOutput(s);
      IACSharpSensor.IACSharpSensor.SensorReached(2183);
    }
    private long SetInputAndFigureFileLength(ref Stream input)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2184);
      long fileLength = -1L;
      IACSharpSensor.IACSharpSensor.SensorReached(2185);
      if (this._Source == ZipEntrySource.Stream) {
        IACSharpSensor.IACSharpSensor.SensorReached(2186);
        PrepSourceStream();
        input = this._sourceStream;
        IACSharpSensor.IACSharpSensor.SensorReached(2187);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(2188);
          fileLength = this._sourceStream.Length;
        } catch (NotSupportedException) {
        }
      } else if (this._Source == ZipEntrySource.ZipFile) {
        IACSharpSensor.IACSharpSensor.SensorReached(2195);
        string pwd = (_Encryption_FromZipFile == EncryptionAlgorithm.None) ? null : (this._Password ?? this._container.Password);
        this._sourceStream = InternalOpenReader(pwd);
        PrepSourceStream();
        input = this._sourceStream;
        fileLength = this._sourceStream.Length;
      } else if (this._Source == ZipEntrySource.JitStream) {
        IACSharpSensor.IACSharpSensor.SensorReached(2190);
        if (this._sourceStream == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2191);
          _sourceStream = this._OpenDelegate(this.FileName);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2192);
        PrepSourceStream();
        input = this._sourceStream;
        IACSharpSensor.IACSharpSensor.SensorReached(2193);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(2194);
          fileLength = this._sourceStream.Length;
        } catch (NotSupportedException) {
        }
      } else if (this._Source == ZipEntrySource.FileSystem) {
        IACSharpSensor.IACSharpSensor.SensorReached(2189);
        FileShare fs = FileShare.ReadWrite;
        fs |= FileShare.Delete;
        input = File.Open(LocalFileName, FileMode.Open, FileAccess.Read, fs);
        fileLength = input.Length;
      }
      System.Int64 RNTRNTRNT_331 = fileLength;
      IACSharpSensor.IACSharpSensor.SensorReached(2196);
      return RNTRNTRNT_331;
    }
    internal void FinishOutputStream(Stream s, CountingStream entryCounter, Stream encryptor, Stream compressor, Ionic.Crc.CrcCalculatorStream output)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2197);
      if (output == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2198);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2199);
      output.Close();
      IACSharpSensor.IACSharpSensor.SensorReached(2200);
      if ((compressor as Ionic.Zlib.DeflateStream) != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2201);
        compressor.Close();
      } else if ((compressor as Ionic.BZip2.BZip2OutputStream) != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2204);
        compressor.Close();
      } else if ((compressor as Ionic.BZip2.ParallelBZip2OutputStream) != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2203);
        compressor.Close();
      } else if ((compressor as Ionic.Zlib.ParallelDeflateOutputStream) != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2202);
        compressor.Close();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2205);
      encryptor.Flush();
      encryptor.Close();
      _LengthOfTrailer = 0;
      _UncompressedSize = output.TotalBytesSlurped;
      WinZipAesCipherStream wzacs = encryptor as WinZipAesCipherStream;
      IACSharpSensor.IACSharpSensor.SensorReached(2206);
      if (wzacs != null && _UncompressedSize > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2207);
        s.Write(wzacs.FinalAuthentication, 0, 10);
        _LengthOfTrailer += 10;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2208);
      _CompressedFileDataSize = entryCounter.BytesWritten;
      _CompressedSize = _CompressedFileDataSize;
      _Crc32 = output.Crc;
      StoreRelativeOffset();
      IACSharpSensor.IACSharpSensor.SensorReached(2209);
    }
    internal void PostProcessOutput(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2210);
      var s1 = s as CountingStream;
      IACSharpSensor.IACSharpSensor.SensorReached(2211);
      if (_UncompressedSize == 0 && _CompressedSize == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2212);
        if (this._Source == ZipEntrySource.ZipOutputStream) {
          IACSharpSensor.IACSharpSensor.SensorReached(2213);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2214);
        if (_Password != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2215);
          int headerBytesToRetract = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(2216);
          if (Encryption == EncryptionAlgorithm.PkzipWeak) {
            IACSharpSensor.IACSharpSensor.SensorReached(2217);
            headerBytesToRetract = 12;
          } else if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
            IACSharpSensor.IACSharpSensor.SensorReached(2218);
            headerBytesToRetract = _aesCrypto_forWrite._Salt.Length + _aesCrypto_forWrite.GeneratedPV.Length;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2219);
          if (this._Source == ZipEntrySource.ZipOutputStream && !s.CanSeek) {
            IACSharpSensor.IACSharpSensor.SensorReached(2220);
            throw new ZipException("Zero bytes written, encryption in use, and non-seekable output.");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2221);
          if (Encryption != EncryptionAlgorithm.None) {
            IACSharpSensor.IACSharpSensor.SensorReached(2222);
            s.Seek(-1 * headerBytesToRetract, SeekOrigin.Current);
            s.SetLength(s.Position);
            Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
            IACSharpSensor.IACSharpSensor.SensorReached(2223);
            if (s1 != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(2224);
              s1.Adjust(headerBytesToRetract);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2225);
            _LengthOfHeader -= headerBytesToRetract;
            __FileDataPosition -= headerBytesToRetract;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2226);
          _Password = null;
          _BitField &= ~(0x1);
          int j = 6;
          _EntryHeader[j++] = (byte)(_BitField & 0xff);
          _EntryHeader[j++] = (byte)((_BitField & 0xff00) >> 8);
          IACSharpSensor.IACSharpSensor.SensorReached(2227);
          if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
            IACSharpSensor.IACSharpSensor.SensorReached(2228);
            Int16 fnLength = (short)(_EntryHeader[26] + _EntryHeader[27] * 256);
            int offx = 30 + fnLength;
            int aesIndex = FindExtraFieldSegment(_EntryHeader, offx, 0x9901);
            IACSharpSensor.IACSharpSensor.SensorReached(2229);
            if (aesIndex >= 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(2230);
              _EntryHeader[aesIndex++] = 0x99;
              _EntryHeader[aesIndex++] = 0x99;
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2231);
        CompressionMethod = 0;
        Encryption = EncryptionAlgorithm.None;
      } else if (_zipCrypto_forWrite != null || _aesCrypto_forWrite != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2232);
        if (Encryption == EncryptionAlgorithm.PkzipWeak) {
          IACSharpSensor.IACSharpSensor.SensorReached(2233);
          _CompressedSize += 12;
        } else if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
          IACSharpSensor.IACSharpSensor.SensorReached(2234);
          _CompressedSize += _aesCrypto_forWrite.SizeOfEncryptionMetadata;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2235);
      int i = 8;
      _EntryHeader[i++] = (byte)(_CompressionMethod & 0xff);
      _EntryHeader[i++] = (byte)((_CompressionMethod & 0xff00) >> 8);
      i = 14;
      _EntryHeader[i++] = (byte)(_Crc32 & 0xff);
      _EntryHeader[i++] = (byte)((_Crc32 & 0xff00) >> 8);
      _EntryHeader[i++] = (byte)((_Crc32 & 0xff0000) >> 16);
      _EntryHeader[i++] = (byte)((_Crc32 & 0xff000000u) >> 24);
      SetZip64Flags();
      Int16 filenameLength = (short)(_EntryHeader[26] + _EntryHeader[27] * 256);
      Int16 extraFieldLength = (short)(_EntryHeader[28] + _EntryHeader[29] * 256);
      IACSharpSensor.IACSharpSensor.SensorReached(2236);
      if (_OutputUsesZip64.Value) {
        IACSharpSensor.IACSharpSensor.SensorReached(2237);
        _EntryHeader[4] = (byte)(45 & 0xff);
        _EntryHeader[5] = 0x0;
        IACSharpSensor.IACSharpSensor.SensorReached(2238);
        for (int j = 0; j < 8; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(2239);
          _EntryHeader[i++] = 0xff;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2240);
        i = 30 + filenameLength;
        _EntryHeader[i++] = 0x1;
        _EntryHeader[i++] = 0x0;
        i += 2;
        Array.Copy(BitConverter.GetBytes(_UncompressedSize), 0, _EntryHeader, i, 8);
        i += 8;
        Array.Copy(BitConverter.GetBytes(_CompressedSize), 0, _EntryHeader, i, 8);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2241);
        _EntryHeader[4] = (byte)(20 & 0xff);
        _EntryHeader[5] = 0x0;
        i = 18;
        _EntryHeader[i++] = (byte)(_CompressedSize & 0xff);
        _EntryHeader[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
        _EntryHeader[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
        _EntryHeader[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
        _EntryHeader[i++] = (byte)(_UncompressedSize & 0xff);
        _EntryHeader[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
        _EntryHeader[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
        _EntryHeader[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
        IACSharpSensor.IACSharpSensor.SensorReached(2242);
        if (extraFieldLength != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2243);
          i = 30 + filenameLength;
          Int16 DataSize = (short)(_EntryHeader[i + 2] + _EntryHeader[i + 3] * 256);
          IACSharpSensor.IACSharpSensor.SensorReached(2244);
          if (DataSize == 16) {
            IACSharpSensor.IACSharpSensor.SensorReached(2245);
            _EntryHeader[i++] = 0x99;
            _EntryHeader[i++] = 0x99;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2246);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        IACSharpSensor.IACSharpSensor.SensorReached(2247);
        i = 8;
        _EntryHeader[i++] = 0x63;
        _EntryHeader[i++] = 0;
        i = 30 + filenameLength;
        IACSharpSensor.IACSharpSensor.SensorReached(2248);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(2249);
          UInt16 HeaderId = (UInt16)(_EntryHeader[i] + _EntryHeader[i + 1] * 256);
          Int16 DataSize = (short)(_EntryHeader[i + 2] + _EntryHeader[i + 3] * 256);
          IACSharpSensor.IACSharpSensor.SensorReached(2250);
          if (HeaderId != 0x9901) {
            IACSharpSensor.IACSharpSensor.SensorReached(2251);
            i += DataSize + 4;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2252);
            i += 9;
            _EntryHeader[i++] = (byte)(_CompressionMethod & 0xff);
            _EntryHeader[i++] = (byte)(_CompressionMethod & 0xff00);
          }
        } while (i < (extraFieldLength - 30 - filenameLength));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2253);
      if ((_BitField & 0x8) != 0x8 || (this._Source == ZipEntrySource.ZipOutputStream && s.CanSeek)) {
        IACSharpSensor.IACSharpSensor.SensorReached(2254);
        var zss = s as ZipSegmentedStream;
        IACSharpSensor.IACSharpSensor.SensorReached(2255);
        if (zss != null && _diskNumber != zss.CurrentSegment) {
          IACSharpSensor.IACSharpSensor.SensorReached(2256);
          using (Stream hseg = ZipSegmentedStream.ForUpdate(this._container.ZipFile.Name, _diskNumber)) {
            IACSharpSensor.IACSharpSensor.SensorReached(2257);
            hseg.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
            hseg.Write(_EntryHeader, 0, _EntryHeader.Length);
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2258);
          s.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
          s.Write(_EntryHeader, 0, _EntryHeader.Length);
          IACSharpSensor.IACSharpSensor.SensorReached(2259);
          if (s1 != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2260);
            s1.Adjust(_EntryHeader.Length);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2261);
          s.Seek(_CompressedSize, SeekOrigin.Current);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2262);
      if (((_BitField & 0x8) == 0x8) && !IsDirectory) {
        IACSharpSensor.IACSharpSensor.SensorReached(2263);
        byte[] Descriptor = new byte[16 + (_OutputUsesZip64.Value ? 8 : 0)];
        i = 0;
        Array.Copy(BitConverter.GetBytes(ZipConstants.ZipEntryDataDescriptorSignature), 0, Descriptor, i, 4);
        i += 4;
        Array.Copy(BitConverter.GetBytes(_Crc32), 0, Descriptor, i, 4);
        i += 4;
        IACSharpSensor.IACSharpSensor.SensorReached(2264);
        if (_OutputUsesZip64.Value) {
          IACSharpSensor.IACSharpSensor.SensorReached(2265);
          Array.Copy(BitConverter.GetBytes(_CompressedSize), 0, Descriptor, i, 8);
          i += 8;
          Array.Copy(BitConverter.GetBytes(_UncompressedSize), 0, Descriptor, i, 8);
          i += 8;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2266);
          Descriptor[i++] = (byte)(_CompressedSize & 0xff);
          Descriptor[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
          Descriptor[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
          Descriptor[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
          Descriptor[i++] = (byte)(_UncompressedSize & 0xff);
          Descriptor[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
          Descriptor[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
          Descriptor[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2267);
        s.Write(Descriptor, 0, Descriptor.Length);
        _LengthOfTrailer += Descriptor.Length;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2268);
    }
    private void SetZip64Flags()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2269);
      _entryRequiresZip64 = new Nullable<bool>(_CompressedSize >= 0xffffffffu || _UncompressedSize >= 0xffffffffu || _RelativeOffsetOfLocalHeader >= 0xffffffffu);
      IACSharpSensor.IACSharpSensor.SensorReached(2270);
      if (_container.Zip64 == Zip64Option.Never && _entryRequiresZip64.Value) {
        IACSharpSensor.IACSharpSensor.SensorReached(2271);
        throw new ZipException("Compressed or Uncompressed size, or offset exceeds the maximum value. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2272);
      _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always || _entryRequiresZip64.Value);
      IACSharpSensor.IACSharpSensor.SensorReached(2273);
    }
    internal void PrepOutputStream(Stream s, long streamLength, out CountingStream outputCounter, out Stream encryptor, out Stream compressor, out Ionic.Crc.CrcCalculatorStream output)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2274);
      TraceWriteLine("PrepOutputStream: e({0}) comp({1}) crypto({2}) zf({3})", FileName, CompressionLevel, Encryption, _container.Name);
      outputCounter = new CountingStream(s);
      IACSharpSensor.IACSharpSensor.SensorReached(2275);
      if (streamLength != 0L) {
        IACSharpSensor.IACSharpSensor.SensorReached(2276);
        encryptor = MaybeApplyEncryption(outputCounter);
        compressor = MaybeApplyCompression(encryptor, streamLength);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2277);
        encryptor = compressor = outputCounter;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2278);
      output = new Ionic.Crc.CrcCalculatorStream(compressor, true);
      IACSharpSensor.IACSharpSensor.SensorReached(2279);
    }
    private Stream MaybeApplyCompression(Stream s, long streamLength)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2280);
      if (_CompressionMethod == 0x8 && CompressionLevel != Ionic.Zlib.CompressionLevel.None) {
        IACSharpSensor.IACSharpSensor.SensorReached(2281);
        if (_container.ParallelDeflateThreshold == 0L || (streamLength > _container.ParallelDeflateThreshold && _container.ParallelDeflateThreshold > 0L)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2282);
          if (_container.ParallelDeflater == null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2283);
            _container.ParallelDeflater = new Ionic.Zlib.ParallelDeflateOutputStream(s, CompressionLevel, _container.Strategy, true);
            IACSharpSensor.IACSharpSensor.SensorReached(2284);
            if (_container.CodecBufferSize > 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(2285);
              _container.ParallelDeflater.BufferSize = _container.CodecBufferSize;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2286);
            if (_container.ParallelDeflateMaxBufferPairs > 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(2287);
              _container.ParallelDeflater.MaxBufferPairs = _container.ParallelDeflateMaxBufferPairs;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2288);
          Ionic.Zlib.ParallelDeflateOutputStream o1 = _container.ParallelDeflater;
          o1.Reset(s);
          Stream RNTRNTRNT_332 = o1;
          IACSharpSensor.IACSharpSensor.SensorReached(2289);
          return RNTRNTRNT_332;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2290);
        var o = new Ionic.Zlib.DeflateStream(s, Ionic.Zlib.CompressionMode.Compress, CompressionLevel, true);
        IACSharpSensor.IACSharpSensor.SensorReached(2291);
        if (_container.CodecBufferSize > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2292);
          o.BufferSize = _container.CodecBufferSize;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2293);
        o.Strategy = _container.Strategy;
        Stream RNTRNTRNT_333 = o;
        IACSharpSensor.IACSharpSensor.SensorReached(2294);
        return RNTRNTRNT_333;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2295);
      if (_CompressionMethod == 0xc) {
        IACSharpSensor.IACSharpSensor.SensorReached(2296);
        if (_container.ParallelDeflateThreshold == 0L || (streamLength > _container.ParallelDeflateThreshold && _container.ParallelDeflateThreshold > 0L)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2297);
          var o1 = new Ionic.BZip2.ParallelBZip2OutputStream(s, true);
          Stream RNTRNTRNT_334 = o1;
          IACSharpSensor.IACSharpSensor.SensorReached(2298);
          return RNTRNTRNT_334;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2299);
        var o = new Ionic.BZip2.BZip2OutputStream(s, true);
        Stream RNTRNTRNT_335 = o;
        IACSharpSensor.IACSharpSensor.SensorReached(2300);
        return RNTRNTRNT_335;
      }
      Stream RNTRNTRNT_336 = s;
      IACSharpSensor.IACSharpSensor.SensorReached(2301);
      return RNTRNTRNT_336;
    }
    private Stream MaybeApplyEncryption(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2302);
      if (Encryption == EncryptionAlgorithm.PkzipWeak) {
        IACSharpSensor.IACSharpSensor.SensorReached(2303);
        TraceWriteLine("MaybeApplyEncryption: e({0}) PKZIP", FileName);
        Stream RNTRNTRNT_337 = new ZipCipherStream(s, _zipCrypto_forWrite, CryptoMode.Encrypt);
        IACSharpSensor.IACSharpSensor.SensorReached(2304);
        return RNTRNTRNT_337;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2305);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        IACSharpSensor.IACSharpSensor.SensorReached(2306);
        TraceWriteLine("MaybeApplyEncryption: e({0}) AES", FileName);
        Stream RNTRNTRNT_338 = new WinZipAesCipherStream(s, _aesCrypto_forWrite, CryptoMode.Encrypt);
        IACSharpSensor.IACSharpSensor.SensorReached(2307);
        return RNTRNTRNT_338;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2308);
      TraceWriteLine("MaybeApplyEncryption: e({0}) None", FileName);
      Stream RNTRNTRNT_339 = s;
      IACSharpSensor.IACSharpSensor.SensorReached(2309);
      return RNTRNTRNT_339;
    }
    private void OnZipErrorWhileSaving(Exception e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2310);
      if (_container.ZipFile != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2311);
        _ioOperationCanceled = _container.ZipFile.OnZipErrorSaving(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2312);
    }
    internal void Write(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2313);
      var cs1 = s as CountingStream;
      var zss1 = s as ZipSegmentedStream;
      bool done = false;
      IACSharpSensor.IACSharpSensor.SensorReached(2314);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(2315);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(2316);
          if (_Source == ZipEntrySource.ZipFile && !_restreamRequiredOnSave) {
            IACSharpSensor.IACSharpSensor.SensorReached(2317);
            CopyThroughOneEntry(s);
            IACSharpSensor.IACSharpSensor.SensorReached(2318);
            return;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2319);
          if (IsDirectory) {
            IACSharpSensor.IACSharpSensor.SensorReached(2320);
            WriteHeader(s, 1);
            StoreRelativeOffset();
            _entryRequiresZip64 = new Nullable<bool>(_RelativeOffsetOfLocalHeader >= 0xffffffffu);
            _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always || _entryRequiresZip64.Value);
            IACSharpSensor.IACSharpSensor.SensorReached(2321);
            if (zss1 != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(2322);
              _diskNumber = zss1.CurrentSegment;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2323);
            return;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2324);
          bool readAgain = true;
          int nCycles = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(2325);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(2326);
            nCycles++;
            WriteHeader(s, nCycles);
            WriteSecurityMetadata(s);
            _WriteEntryData(s);
            _TotalEntrySize = _LengthOfHeader + _CompressedFileDataSize + _LengthOfTrailer;
            IACSharpSensor.IACSharpSensor.SensorReached(2327);
            if (nCycles > 1) {
              IACSharpSensor.IACSharpSensor.SensorReached(2328);
              readAgain = false;
            } else if (!s.CanSeek) {
              IACSharpSensor.IACSharpSensor.SensorReached(2330);
              readAgain = false;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(2329);
              readAgain = WantReadAgain();
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2331);
            if (readAgain) {
              IACSharpSensor.IACSharpSensor.SensorReached(2332);
              if (zss1 != null) {
                IACSharpSensor.IACSharpSensor.SensorReached(2333);
                zss1.TruncateBackward(_diskNumber, _RelativeOffsetOfLocalHeader);
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(2334);
                s.Seek(_RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
              }
              IACSharpSensor.IACSharpSensor.SensorReached(2335);
              s.SetLength(s.Position);
              IACSharpSensor.IACSharpSensor.SensorReached(2336);
              if (cs1 != null) {
                IACSharpSensor.IACSharpSensor.SensorReached(2337);
                cs1.Adjust(_TotalEntrySize);
              }
            }
          } while (readAgain);
          IACSharpSensor.IACSharpSensor.SensorReached(2338);
          _skippedDuringSave = false;
          done = true;
        } catch (System.Exception exc1) {
          IACSharpSensor.IACSharpSensor.SensorReached(2339);
          ZipErrorAction orig = this.ZipErrorAction;
          int loop = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(2340);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(2341);
            if (ZipErrorAction == ZipErrorAction.Throw) {
              IACSharpSensor.IACSharpSensor.SensorReached(2342);
              throw;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2343);
            if (ZipErrorAction == ZipErrorAction.Skip || ZipErrorAction == ZipErrorAction.Retry) {
              IACSharpSensor.IACSharpSensor.SensorReached(2344);
              long p1 = (cs1 != null) ? cs1.ComputedPosition : s.Position;
              long delta = p1 - _future_ROLH;
              IACSharpSensor.IACSharpSensor.SensorReached(2345);
              if (delta > 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(2346);
                s.Seek(delta, SeekOrigin.Current);
                long p2 = s.Position;
                s.SetLength(s.Position);
                IACSharpSensor.IACSharpSensor.SensorReached(2347);
                if (cs1 != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(2348);
                  cs1.Adjust(p1 - p2);
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(2349);
              if (ZipErrorAction == ZipErrorAction.Skip) {
                IACSharpSensor.IACSharpSensor.SensorReached(2350);
                WriteStatus("Skipping file {0} (exception: {1})", LocalFileName, exc1.ToString());
                _skippedDuringSave = true;
                done = true;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(2351);
                this.ZipErrorAction = orig;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(2352);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2353);
            if (loop > 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(2354);
              throw;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2355);
            if (ZipErrorAction == ZipErrorAction.InvokeErrorEvent) {
              IACSharpSensor.IACSharpSensor.SensorReached(2356);
              OnZipErrorWhileSaving(exc1);
              IACSharpSensor.IACSharpSensor.SensorReached(2357);
              if (_ioOperationCanceled) {
                IACSharpSensor.IACSharpSensor.SensorReached(2358);
                done = true;
                IACSharpSensor.IACSharpSensor.SensorReached(2359);
                break;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2360);
            loop++;
          } while (true);
          IACSharpSensor.IACSharpSensor.SensorReached(2361);
        }
      } while (!done);
      IACSharpSensor.IACSharpSensor.SensorReached(2362);
    }
    internal void StoreRelativeOffset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2363);
      _RelativeOffsetOfLocalHeader = _future_ROLH;
      IACSharpSensor.IACSharpSensor.SensorReached(2364);
    }
    internal void NotifySaveComplete()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2365);
      _Encryption_FromZipFile = _Encryption;
      _CompressionMethod_FromZipFile = _CompressionMethod;
      _restreamRequiredOnSave = false;
      _metadataChanged = false;
      _Source = ZipEntrySource.ZipFile;
      IACSharpSensor.IACSharpSensor.SensorReached(2366);
    }
    internal void WriteSecurityMetadata(Stream outstream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2367);
      if (Encryption == EncryptionAlgorithm.None) {
        IACSharpSensor.IACSharpSensor.SensorReached(2368);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2369);
      string pwd = this._Password;
      IACSharpSensor.IACSharpSensor.SensorReached(2370);
      if (this._Source == ZipEntrySource.ZipFile && pwd == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2371);
        pwd = this._container.Password;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2372);
      if (pwd == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2373);
        _zipCrypto_forWrite = null;
        _aesCrypto_forWrite = null;
        IACSharpSensor.IACSharpSensor.SensorReached(2374);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2375);
      TraceWriteLine("WriteSecurityMetadata: e({0}) crypto({1}) pw({2})", FileName, Encryption.ToString(), pwd);
      IACSharpSensor.IACSharpSensor.SensorReached(2376);
      if (Encryption == EncryptionAlgorithm.PkzipWeak) {
        IACSharpSensor.IACSharpSensor.SensorReached(2377);
        _zipCrypto_forWrite = ZipCrypto.ForWrite(pwd);
        var rnd = new System.Random();
        byte[] encryptionHeader = new byte[12];
        rnd.NextBytes(encryptionHeader);
        IACSharpSensor.IACSharpSensor.SensorReached(2378);
        if ((this._BitField & 0x8) == 0x8) {
          IACSharpSensor.IACSharpSensor.SensorReached(2379);
          _TimeBlob = Ionic.Zip.SharedUtilities.DateTimeToPacked(LastModified);
          encryptionHeader[11] = (byte)((this._TimeBlob >> 8) & 0xff);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2380);
          FigureCrc32();
          encryptionHeader[11] = (byte)((this._Crc32 >> 24) & 0xff);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2381);
        byte[] cipherText = _zipCrypto_forWrite.EncryptMessage(encryptionHeader, encryptionHeader.Length);
        outstream.Write(cipherText, 0, cipherText.Length);
        _LengthOfHeader += cipherText.Length;
      } else if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        IACSharpSensor.IACSharpSensor.SensorReached(2382);
        int keystrength = GetKeyStrengthInBits(Encryption);
        _aesCrypto_forWrite = WinZipAesCrypto.Generate(pwd, keystrength);
        outstream.Write(_aesCrypto_forWrite.Salt, 0, _aesCrypto_forWrite._Salt.Length);
        outstream.Write(_aesCrypto_forWrite.GeneratedPV, 0, _aesCrypto_forWrite.GeneratedPV.Length);
        _LengthOfHeader += _aesCrypto_forWrite._Salt.Length + _aesCrypto_forWrite.GeneratedPV.Length;
        TraceWriteLine("WriteSecurityMetadata: AES e({0}) keybits({1}) _LOH({2})", FileName, keystrength, _LengthOfHeader);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2383);
    }
    private void CopyThroughOneEntry(Stream outStream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2384);
      if (this.LengthOfHeader == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2385);
        throw new BadStateException("Bad header length.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2386);
      bool needRecompute = _metadataChanged || (this.ArchiveStream is ZipSegmentedStream) || (outStream is ZipSegmentedStream) || (_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Never) || (!_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Always);
      IACSharpSensor.IACSharpSensor.SensorReached(2387);
      if (needRecompute) {
        IACSharpSensor.IACSharpSensor.SensorReached(2388);
        CopyThroughWithRecompute(outStream);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2389);
        CopyThroughWithNoChange(outStream);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2390);
      _entryRequiresZip64 = new Nullable<bool>(_CompressedSize >= 0xffffffffu || _UncompressedSize >= 0xffffffffu || _RelativeOffsetOfLocalHeader >= 0xffffffffu);
      _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always || _entryRequiresZip64.Value);
      IACSharpSensor.IACSharpSensor.SensorReached(2391);
    }
    private void CopyThroughWithRecompute(Stream outstream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2392);
      int n;
      byte[] bytes = new byte[BufferSize];
      var input = new CountingStream(this.ArchiveStream);
      long origRelativeOffsetOfHeader = _RelativeOffsetOfLocalHeader;
      int origLengthOfHeader = LengthOfHeader;
      WriteHeader(outstream, 0);
      StoreRelativeOffset();
      IACSharpSensor.IACSharpSensor.SensorReached(2393);
      if (!this.FileName.EndsWith("/")) {
        IACSharpSensor.IACSharpSensor.SensorReached(2394);
        long pos = origRelativeOffsetOfHeader + origLengthOfHeader;
        int len = GetLengthOfCryptoHeaderBytes(_Encryption_FromZipFile);
        pos -= len;
        _LengthOfHeader += len;
        input.Seek(pos, SeekOrigin.Begin);
        long remaining = this._CompressedSize;
        IACSharpSensor.IACSharpSensor.SensorReached(2395);
        while (remaining > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2396);
          len = (remaining > bytes.Length) ? bytes.Length : (int)remaining;
          n = input.Read(bytes, 0, len);
          outstream.Write(bytes, 0, n);
          remaining -= n;
          OnWriteBlock(input.BytesRead, this._CompressedSize);
          IACSharpSensor.IACSharpSensor.SensorReached(2397);
          if (_ioOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(2398);
            break;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2399);
        if ((this._BitField & 0x8) == 0x8) {
          IACSharpSensor.IACSharpSensor.SensorReached(2400);
          int size = 16;
          IACSharpSensor.IACSharpSensor.SensorReached(2401);
          if (_InputUsesZip64) {
            IACSharpSensor.IACSharpSensor.SensorReached(2402);
            size += 8;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2403);
          byte[] Descriptor = new byte[size];
          input.Read(Descriptor, 0, size);
          IACSharpSensor.IACSharpSensor.SensorReached(2404);
          if (_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Never) {
            IACSharpSensor.IACSharpSensor.SensorReached(2405);
            outstream.Write(Descriptor, 0, 8);
            IACSharpSensor.IACSharpSensor.SensorReached(2406);
            if (_CompressedSize > 0xffffffffu) {
              IACSharpSensor.IACSharpSensor.SensorReached(2407);
              throw new InvalidOperationException("ZIP64 is required");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2408);
            outstream.Write(Descriptor, 8, 4);
            IACSharpSensor.IACSharpSensor.SensorReached(2409);
            if (_UncompressedSize > 0xffffffffu) {
              IACSharpSensor.IACSharpSensor.SensorReached(2410);
              throw new InvalidOperationException("ZIP64 is required");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2411);
            outstream.Write(Descriptor, 16, 4);
            _LengthOfTrailer -= 8;
          } else if (!_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Always) {
            IACSharpSensor.IACSharpSensor.SensorReached(2413);
            byte[] pad = new byte[4];
            outstream.Write(Descriptor, 0, 8);
            outstream.Write(Descriptor, 8, 4);
            outstream.Write(pad, 0, 4);
            outstream.Write(Descriptor, 12, 4);
            outstream.Write(pad, 0, 4);
            _LengthOfTrailer += 8;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2412);
            outstream.Write(Descriptor, 0, size);
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2414);
      _TotalEntrySize = _LengthOfHeader + _CompressedFileDataSize + _LengthOfTrailer;
      IACSharpSensor.IACSharpSensor.SensorReached(2415);
    }
    private void CopyThroughWithNoChange(Stream outstream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2416);
      int n;
      byte[] bytes = new byte[BufferSize];
      var input = new CountingStream(this.ArchiveStream);
      input.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
      IACSharpSensor.IACSharpSensor.SensorReached(2417);
      if (this._TotalEntrySize == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2418);
        this._TotalEntrySize = this._LengthOfHeader + this._CompressedFileDataSize + _LengthOfTrailer;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2419);
      var counter = outstream as CountingStream;
      _RelativeOffsetOfLocalHeader = (counter != null) ? counter.ComputedPosition : outstream.Position;
      long remaining = this._TotalEntrySize;
      IACSharpSensor.IACSharpSensor.SensorReached(2420);
      while (remaining > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2421);
        int len = (remaining > bytes.Length) ? bytes.Length : (int)remaining;
        n = input.Read(bytes, 0, len);
        outstream.Write(bytes, 0, n);
        remaining -= n;
        OnWriteBlock(input.BytesRead, this._TotalEntrySize);
        IACSharpSensor.IACSharpSensor.SensorReached(2422);
        if (_ioOperationCanceled) {
          IACSharpSensor.IACSharpSensor.SensorReached(2423);
          break;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2424);
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceWriteLine(string format, params object[] varParams)
    {
      lock (_outputLock) {
        IACSharpSensor.IACSharpSensor.SensorReached(2425);
        int tid = System.Threading.Thread.CurrentThread.GetHashCode();
        Console.ForegroundColor = (ConsoleColor)(tid % 8 + 8);
        Console.Write("{0:000} ZipEntry.Write ", tid);
        Console.WriteLine(format, varParams);
        Console.ResetColor();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2426);
    }
    private object _outputLock = new Object();
  }
}
