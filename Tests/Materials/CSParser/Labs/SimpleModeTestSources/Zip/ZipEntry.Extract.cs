using System;
using System.IO;
namespace Ionic.Zip
{
  public partial class ZipEntry
  {
    public void Extract()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1508);
      InternalExtract(".", null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(1509);
    }
    public void Extract(ExtractExistingFileAction extractExistingFile)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1510);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(".", null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(1511);
    }
    public void Extract(Stream stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1512);
      InternalExtract(null, stream, null);
      IACSharpSensor.IACSharpSensor.SensorReached(1513);
    }
    public void Extract(string baseDirectory)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1514);
      InternalExtract(baseDirectory, null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(1515);
    }
    public void Extract(string baseDirectory, ExtractExistingFileAction extractExistingFile)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1516);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(baseDirectory, null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(1517);
    }
    public void ExtractWithPassword(string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1518);
      InternalExtract(".", null, password);
      IACSharpSensor.IACSharpSensor.SensorReached(1519);
    }
    public void ExtractWithPassword(string baseDirectory, string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1520);
      InternalExtract(baseDirectory, null, password);
      IACSharpSensor.IACSharpSensor.SensorReached(1521);
    }
    public void ExtractWithPassword(ExtractExistingFileAction extractExistingFile, string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1522);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(".", null, password);
      IACSharpSensor.IACSharpSensor.SensorReached(1523);
    }
    public void ExtractWithPassword(string baseDirectory, ExtractExistingFileAction extractExistingFile, string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1524);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(baseDirectory, null, password);
      IACSharpSensor.IACSharpSensor.SensorReached(1525);
    }
    public void ExtractWithPassword(Stream stream, string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1526);
      InternalExtract(null, stream, password);
      IACSharpSensor.IACSharpSensor.SensorReached(1527);
    }
    public Ionic.Crc.CrcCalculatorStream OpenReader()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1528);
      if (_container.ZipFile == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1529);
        throw new InvalidOperationException("Use OpenReader() only with ZipFile.");
      }
      Ionic.Crc.CrcCalculatorStream RNTRNTRNT_279 = InternalOpenReader(this._Password ?? this._container.Password);
      IACSharpSensor.IACSharpSensor.SensorReached(1530);
      return RNTRNTRNT_279;
    }
    public Ionic.Crc.CrcCalculatorStream OpenReader(string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1531);
      if (_container.ZipFile == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1532);
        throw new InvalidOperationException("Use OpenReader() only with ZipFile.");
      }
      Ionic.Crc.CrcCalculatorStream RNTRNTRNT_280 = InternalOpenReader(password);
      IACSharpSensor.IACSharpSensor.SensorReached(1533);
      return RNTRNTRNT_280;
    }
    internal Ionic.Crc.CrcCalculatorStream InternalOpenReader(string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1534);
      ValidateCompression();
      ValidateEncryption();
      SetupCryptoForExtract(password);
      IACSharpSensor.IACSharpSensor.SensorReached(1535);
      if (this._Source != ZipEntrySource.ZipFile) {
        IACSharpSensor.IACSharpSensor.SensorReached(1536);
        throw new BadStateException("You must call ZipFile.Save before calling OpenReader");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1537);
      Int64 LeftToRead = (_CompressionMethod_FromZipFile == (short)CompressionMethod.None) ? this._CompressedFileDataSize : this.UncompressedSize;
      Stream input = this.ArchiveStream;
      this.ArchiveStream.Seek(this.FileDataPosition, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      _inputDecryptorStream = GetExtractDecryptor(input);
      Stream input3 = GetExtractDecompressor(_inputDecryptorStream);
      Ionic.Crc.CrcCalculatorStream RNTRNTRNT_281 = new Ionic.Crc.CrcCalculatorStream(input3, LeftToRead);
      IACSharpSensor.IACSharpSensor.SensorReached(1538);
      return RNTRNTRNT_281;
    }
    private void OnExtractProgress(Int64 bytesWritten, Int64 totalBytesToWrite)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1539);
      if (_container.ZipFile != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1540);
        _ioOperationCanceled = _container.ZipFile.OnExtractBlock(this, bytesWritten, totalBytesToWrite);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1541);
    }
    private void OnBeforeExtract(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1542);
      if (_container.ZipFile != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1543);
        if (!_container.ZipFile._inExtractAll) {
          IACSharpSensor.IACSharpSensor.SensorReached(1544);
          _ioOperationCanceled = _container.ZipFile.OnSingleEntryExtract(this, path, true);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1545);
    }
    private void OnAfterExtract(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1546);
      if (_container.ZipFile != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1547);
        if (!_container.ZipFile._inExtractAll) {
          IACSharpSensor.IACSharpSensor.SensorReached(1548);
          _container.ZipFile.OnSingleEntryExtract(this, path, false);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1549);
    }
    private void OnExtractExisting(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1550);
      if (_container.ZipFile != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1551);
        _ioOperationCanceled = _container.ZipFile.OnExtractExisting(this, path);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1552);
    }
    private static void ReallyDelete(string fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1553);
      if ((File.GetAttributes(fileName) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
        IACSharpSensor.IACSharpSensor.SensorReached(1554);
        File.SetAttributes(fileName, FileAttributes.Normal);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1555);
      File.Delete(fileName);
      IACSharpSensor.IACSharpSensor.SensorReached(1556);
    }
    private void WriteStatus(string format, params Object[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1557);
      if (_container.ZipFile != null && _container.ZipFile.Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(1558);
        _container.ZipFile.StatusMessageTextWriter.WriteLine(format, args);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1559);
    }
    private void InternalExtract(string baseDir, Stream outstream, string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1560);
      if (_container == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1561);
        throw new BadStateException("This entry is an orphan");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1562);
      if (_container.ZipFile == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1563);
        throw new InvalidOperationException("Use Extract() only with ZipFile.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1564);
      _container.ZipFile.Reset(false);
      IACSharpSensor.IACSharpSensor.SensorReached(1565);
      if (this._Source != ZipEntrySource.ZipFile) {
        IACSharpSensor.IACSharpSensor.SensorReached(1566);
        throw new BadStateException("You must call ZipFile.Save before calling any Extract method");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1567);
      OnBeforeExtract(baseDir);
      _ioOperationCanceled = false;
      string targetFileName = null;
      Stream output = null;
      bool fileExistsBeforeExtraction = false;
      bool checkLaterForResetDirTimes = false;
      IACSharpSensor.IACSharpSensor.SensorReached(1568);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(1569);
        ValidateCompression();
        ValidateEncryption();
        IACSharpSensor.IACSharpSensor.SensorReached(1570);
        if (ValidateOutput(baseDir, outstream, out targetFileName)) {
          IACSharpSensor.IACSharpSensor.SensorReached(1571);
          WriteStatus("extract dir {0}...", targetFileName);
          OnAfterExtract(baseDir);
          IACSharpSensor.IACSharpSensor.SensorReached(1572);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1573);
        if (targetFileName != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1574);
          if (File.Exists(targetFileName)) {
            IACSharpSensor.IACSharpSensor.SensorReached(1575);
            fileExistsBeforeExtraction = true;
            int rc = CheckExtractExistingFile(baseDir, targetFileName);
            IACSharpSensor.IACSharpSensor.SensorReached(1576);
            if (rc == 2) {
              IACSharpSensor.IACSharpSensor.SensorReached(1577);
              goto ExitTry;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1578);
            if (rc == 1) {
              IACSharpSensor.IACSharpSensor.SensorReached(1579);
              return;
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1580);
        string p = password ?? this._Password ?? this._container.Password;
        IACSharpSensor.IACSharpSensor.SensorReached(1581);
        if (_Encryption_FromZipFile != EncryptionAlgorithm.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(1582);
          if (p == null) {
            IACSharpSensor.IACSharpSensor.SensorReached(1583);
            throw new BadPasswordException();
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1584);
          SetupCryptoForExtract(p);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1585);
        if (targetFileName != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1586);
          WriteStatus("extract file {0}...", targetFileName);
          targetFileName += ".tmp";
          var dirName = Path.GetDirectoryName(targetFileName);
          IACSharpSensor.IACSharpSensor.SensorReached(1587);
          if (!Directory.Exists(dirName)) {
            IACSharpSensor.IACSharpSensor.SensorReached(1588);
            Directory.CreateDirectory(dirName);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1589);
            if (_container.ZipFile != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(1590);
              checkLaterForResetDirTimes = _container.ZipFile._inExtractAll;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1591);
          output = new FileStream(targetFileName, FileMode.CreateNew);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1592);
          WriteStatus("extract entry {0} to stream...", FileName);
          output = outstream;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1593);
        if (_ioOperationCanceled) {
          IACSharpSensor.IACSharpSensor.SensorReached(1594);
          goto ExitTry;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1595);
        Int32 ActualCrc32 = ExtractOne(output);
        IACSharpSensor.IACSharpSensor.SensorReached(1596);
        if (_ioOperationCanceled) {
          IACSharpSensor.IACSharpSensor.SensorReached(1597);
          goto ExitTry;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1598);
        VerifyCrcAfterExtract(ActualCrc32);
        IACSharpSensor.IACSharpSensor.SensorReached(1599);
        if (targetFileName != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1600);
          output.Close();
          output = null;
          string tmpName = targetFileName;
          string zombie = null;
          targetFileName = tmpName.Substring(0, tmpName.Length - 4);
          IACSharpSensor.IACSharpSensor.SensorReached(1601);
          if (fileExistsBeforeExtraction) {
            IACSharpSensor.IACSharpSensor.SensorReached(1602);
            zombie = targetFileName + ".PendingOverwrite";
            File.Move(targetFileName, zombie);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1603);
          File.Move(tmpName, targetFileName);
          _SetTimes(targetFileName, true);
          IACSharpSensor.IACSharpSensor.SensorReached(1604);
          if (zombie != null && File.Exists(zombie)) {
            IACSharpSensor.IACSharpSensor.SensorReached(1605);
            ReallyDelete(zombie);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1606);
          if (checkLaterForResetDirTimes) {
            IACSharpSensor.IACSharpSensor.SensorReached(1607);
            if (this.FileName.IndexOf('/') != -1) {
              IACSharpSensor.IACSharpSensor.SensorReached(1608);
              string dirname = Path.GetDirectoryName(this.FileName);
              IACSharpSensor.IACSharpSensor.SensorReached(1609);
              if (this._container.ZipFile[dirname] == null) {
                IACSharpSensor.IACSharpSensor.SensorReached(1610);
                _SetTimes(Path.GetDirectoryName(targetFileName), false);
              }
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1611);
          if ((_VersionMadeBy & 0xff00) == 0xa00 || (_VersionMadeBy & 0xff00) == 0x0) {
            IACSharpSensor.IACSharpSensor.SensorReached(1612);
            File.SetAttributes(targetFileName, (FileAttributes)_ExternalFileAttrs);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1613);
        OnAfterExtract(baseDir);
        ExitTry:
        ;
      } catch (Exception) {
        IACSharpSensor.IACSharpSensor.SensorReached(1614);
        _ioOperationCanceled = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1615);
        throw;
        IACSharpSensor.IACSharpSensor.SensorReached(1616);
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(1617);
        if (_ioOperationCanceled) {
          IACSharpSensor.IACSharpSensor.SensorReached(1618);
          if (targetFileName != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(1619);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(1620);
              if (output != null) {
                IACSharpSensor.IACSharpSensor.SensorReached(1621);
                output.Close();
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1622);
              if (File.Exists(targetFileName) && !fileExistsBeforeExtraction) {
                IACSharpSensor.IACSharpSensor.SensorReached(1623);
                File.Delete(targetFileName);
              }
            } finally {
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1624);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1625);
    }
    internal void VerifyCrcAfterExtract(Int32 actualCrc32)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1626);
      if (actualCrc32 != _Crc32) {
        IACSharpSensor.IACSharpSensor.SensorReached(1627);
        if ((Encryption != EncryptionAlgorithm.WinZipAes128 && Encryption != EncryptionAlgorithm.WinZipAes256) || _WinZipAesMethod != 0x2) {
          IACSharpSensor.IACSharpSensor.SensorReached(1628);
          throw new BadCrcException("CRC error: the file being extracted appears to be corrupted. " + String.Format("Expected 0x{0:X8}, Actual 0x{1:X8}", _Crc32, actualCrc32));
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1629);
      if (this.UncompressedSize == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1630);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1631);
      if (Encryption == EncryptionAlgorithm.WinZipAes128 || Encryption == EncryptionAlgorithm.WinZipAes256) {
        IACSharpSensor.IACSharpSensor.SensorReached(1632);
        WinZipAesCipherStream wzs = _inputDecryptorStream as WinZipAesCipherStream;
        _aesCrypto_forExtract.CalculatedMac = wzs.FinalAuthentication;
        _aesCrypto_forExtract.ReadAndVerifyMac(this.ArchiveStream);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1633);
    }
    private int CheckExtractExistingFile(string baseDir, string targetFileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1634);
      int loop = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1635);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(1636);
        switch (ExtractExistingFile) {
          case ExtractExistingFileAction.OverwriteSilently:
            IACSharpSensor.IACSharpSensor.SensorReached(1637);
            WriteStatus("the file {0} exists; will overwrite it...", targetFileName);
            System.Int32 RNTRNTRNT_282 = 0;
            IACSharpSensor.IACSharpSensor.SensorReached(1638);
            return RNTRNTRNT_282;
          case ExtractExistingFileAction.DoNotOverwrite:
            IACSharpSensor.IACSharpSensor.SensorReached(1639);
            WriteStatus("the file {0} exists; not extracting entry...", FileName);
            OnAfterExtract(baseDir);
            System.Int32 RNTRNTRNT_283 = 1;
            IACSharpSensor.IACSharpSensor.SensorReached(1640);
            return RNTRNTRNT_283;
          case ExtractExistingFileAction.InvokeExtractProgressEvent:
            IACSharpSensor.IACSharpSensor.SensorReached(1641);
            if (loop > 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(1642);
              throw new ZipException(String.Format("The file {0} already exists.", targetFileName));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1643);
            OnExtractExisting(baseDir);
            IACSharpSensor.IACSharpSensor.SensorReached(1644);
            if (_ioOperationCanceled) {
              System.Int32 RNTRNTRNT_284 = 2;
              IACSharpSensor.IACSharpSensor.SensorReached(1645);
              return RNTRNTRNT_284;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1646);
            break;
          case ExtractExistingFileAction.Throw:
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(1647);
            throw new ZipException(String.Format("The file {0} already exists.", targetFileName));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1648);
        loop++;
      } while (true);
      IACSharpSensor.IACSharpSensor.SensorReached(1649);
    }
    private void _CheckRead(int nbytes)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1650);
      if (nbytes == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1651);
        throw new BadReadException(String.Format("bad read of entry {0} from compressed archive.", this.FileName));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1652);
    }
    private Stream _inputDecryptorStream;
    private Int32 ExtractOne(Stream output)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1653);
      Int32 CrcResult = 0;
      Stream input = this.ArchiveStream;
      IACSharpSensor.IACSharpSensor.SensorReached(1654);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(1655);
        input.Seek(this.FileDataPosition, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(input);
        byte[] bytes = new byte[BufferSize];
        Int64 LeftToRead = (_CompressionMethod_FromZipFile != (short)CompressionMethod.None) ? this.UncompressedSize : this._CompressedFileDataSize;
        _inputDecryptorStream = GetExtractDecryptor(input);
        Stream input3 = GetExtractDecompressor(_inputDecryptorStream);
        Int64 bytesWritten = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(1656);
        using (var s1 = new Ionic.Crc.CrcCalculatorStream(input3)) {
          IACSharpSensor.IACSharpSensor.SensorReached(1657);
          while (LeftToRead > 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(1658);
            int len = (LeftToRead > bytes.Length) ? bytes.Length : (int)LeftToRead;
            int n = s1.Read(bytes, 0, len);
            _CheckRead(n);
            output.Write(bytes, 0, n);
            LeftToRead -= n;
            bytesWritten += n;
            OnExtractProgress(bytesWritten, UncompressedSize);
            IACSharpSensor.IACSharpSensor.SensorReached(1659);
            if (_ioOperationCanceled) {
              IACSharpSensor.IACSharpSensor.SensorReached(1660);
              break;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1661);
          CrcResult = s1.Crc;
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(1662);
        var zss = input as ZipSegmentedStream;
        IACSharpSensor.IACSharpSensor.SensorReached(1663);
        if (zss != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1664);
          zss.Dispose();
          _archiveStream = null;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1665);
      }
      Int32 RNTRNTRNT_285 = CrcResult;
      IACSharpSensor.IACSharpSensor.SensorReached(1666);
      return RNTRNTRNT_285;
    }
    internal Stream GetExtractDecompressor(Stream input2)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1667);
      switch (_CompressionMethod_FromZipFile) {
        case (short)CompressionMethod.None:
          Stream RNTRNTRNT_286 = input2;
          IACSharpSensor.IACSharpSensor.SensorReached(1668);
          return RNTRNTRNT_286;
        case (short)CompressionMethod.Deflate:
          Stream RNTRNTRNT_287 = new Ionic.Zlib.DeflateStream(input2, Ionic.Zlib.CompressionMode.Decompress, true);
          IACSharpSensor.IACSharpSensor.SensorReached(1669);
          return RNTRNTRNT_287;
        case (short)CompressionMethod.BZip2:
          Stream RNTRNTRNT_288 = new Ionic.BZip2.BZip2InputStream(input2, true);
          IACSharpSensor.IACSharpSensor.SensorReached(1670);
          return RNTRNTRNT_288;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1671);
      return null;
    }
    internal Stream GetExtractDecryptor(Stream input)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1672);
      Stream input2 = null;
      IACSharpSensor.IACSharpSensor.SensorReached(1673);
      if (_Encryption_FromZipFile == EncryptionAlgorithm.PkzipWeak) {
        IACSharpSensor.IACSharpSensor.SensorReached(1674);
        input2 = new ZipCipherStream(input, _zipCrypto_forExtract, CryptoMode.Decrypt);
      } else if (_Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes128 || _Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes256) {
        IACSharpSensor.IACSharpSensor.SensorReached(1676);
        input2 = new WinZipAesCipherStream(input, _aesCrypto_forExtract, _CompressedFileDataSize, CryptoMode.Decrypt);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(1675);
        input2 = input;
      }
      Stream RNTRNTRNT_289 = input2;
      IACSharpSensor.IACSharpSensor.SensorReached(1677);
      return RNTRNTRNT_289;
    }
    internal void _SetTimes(string fileOrDirectory, bool isFile)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1678);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(1679);
        if (_ntfsTimesAreSet) {
          IACSharpSensor.IACSharpSensor.SensorReached(1680);
          if (isFile) {
            IACSharpSensor.IACSharpSensor.SensorReached(1681);
            if (File.Exists(fileOrDirectory)) {
              IACSharpSensor.IACSharpSensor.SensorReached(1682);
              File.SetCreationTimeUtc(fileOrDirectory, _Ctime);
              File.SetLastAccessTimeUtc(fileOrDirectory, _Atime);
              File.SetLastWriteTimeUtc(fileOrDirectory, _Mtime);
            }
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1683);
            if (Directory.Exists(fileOrDirectory)) {
              IACSharpSensor.IACSharpSensor.SensorReached(1684);
              Directory.SetCreationTimeUtc(fileOrDirectory, _Ctime);
              Directory.SetLastAccessTimeUtc(fileOrDirectory, _Atime);
              Directory.SetLastWriteTimeUtc(fileOrDirectory, _Mtime);
            }
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1685);
          DateTime AdjustedLastModified = Ionic.Zip.SharedUtilities.AdjustTime_Reverse(LastModified);
          IACSharpSensor.IACSharpSensor.SensorReached(1686);
          if (isFile) {
            IACSharpSensor.IACSharpSensor.SensorReached(1687);
            File.SetLastWriteTime(fileOrDirectory, AdjustedLastModified);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1688);
            Directory.SetLastWriteTime(fileOrDirectory, AdjustedLastModified);
          }
        }
      } catch (System.IO.IOException ioexc1) {
        IACSharpSensor.IACSharpSensor.SensorReached(1689);
        WriteStatus("failed to set time on {0}: {1}", fileOrDirectory, ioexc1.Message);
        IACSharpSensor.IACSharpSensor.SensorReached(1690);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1691);
    }
    private string UnsupportedAlgorithm {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1692);
        string alg = String.Empty;
        IACSharpSensor.IACSharpSensor.SensorReached(1693);
        switch (_UnsupportedAlgorithmId) {
          case 0:
            IACSharpSensor.IACSharpSensor.SensorReached(1694);
            alg = "--";
            IACSharpSensor.IACSharpSensor.SensorReached(1695);
            break;
          case 0x6601:
            IACSharpSensor.IACSharpSensor.SensorReached(1696);
            alg = "DES";
            IACSharpSensor.IACSharpSensor.SensorReached(1697);
            break;
          case 0x6602:
            IACSharpSensor.IACSharpSensor.SensorReached(1698);
            alg = "RC2";
            IACSharpSensor.IACSharpSensor.SensorReached(1699);
            break;
          case 0x6603:
            IACSharpSensor.IACSharpSensor.SensorReached(1700);
            alg = "3DES-168";
            IACSharpSensor.IACSharpSensor.SensorReached(1701);
            break;
          case 0x6609:
            IACSharpSensor.IACSharpSensor.SensorReached(1702);
            alg = "3DES-112";
            IACSharpSensor.IACSharpSensor.SensorReached(1703);
            break;
          case 0x660e:
            IACSharpSensor.IACSharpSensor.SensorReached(1704);
            alg = "PKWare AES128";
            IACSharpSensor.IACSharpSensor.SensorReached(1705);
            break;
          case 0x660f:
            IACSharpSensor.IACSharpSensor.SensorReached(1706);
            alg = "PKWare AES192";
            IACSharpSensor.IACSharpSensor.SensorReached(1707);
            break;
          case 0x6610:
            IACSharpSensor.IACSharpSensor.SensorReached(1708);
            alg = "PKWare AES256";
            IACSharpSensor.IACSharpSensor.SensorReached(1709);
            break;
          case 0x6702:
            IACSharpSensor.IACSharpSensor.SensorReached(1710);
            alg = "RC2";
            IACSharpSensor.IACSharpSensor.SensorReached(1711);
            break;
          case 0x6720:
            IACSharpSensor.IACSharpSensor.SensorReached(1712);
            alg = "Blowfish";
            IACSharpSensor.IACSharpSensor.SensorReached(1713);
            break;
          case 0x6721:
            IACSharpSensor.IACSharpSensor.SensorReached(1714);
            alg = "Twofish";
            IACSharpSensor.IACSharpSensor.SensorReached(1715);
            break;
          case 0x6801:
            IACSharpSensor.IACSharpSensor.SensorReached(1716);
            alg = "RC4";
            IACSharpSensor.IACSharpSensor.SensorReached(1717);
            break;
          case 0xffff:
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(1718);
            alg = String.Format("Unknown (0x{0:X4})", _UnsupportedAlgorithmId);
            IACSharpSensor.IACSharpSensor.SensorReached(1719);
            break;
        }
        System.String RNTRNTRNT_290 = alg;
        IACSharpSensor.IACSharpSensor.SensorReached(1720);
        return RNTRNTRNT_290;
      }
    }
    private string UnsupportedCompressionMethod {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1721);
        string meth = String.Empty;
        IACSharpSensor.IACSharpSensor.SensorReached(1722);
        switch ((int)_CompressionMethod) {
          case 0:
            IACSharpSensor.IACSharpSensor.SensorReached(1723);
            meth = "Store";
            IACSharpSensor.IACSharpSensor.SensorReached(1724);
            break;
          case 1:
            IACSharpSensor.IACSharpSensor.SensorReached(1725);
            meth = "Shrink";
            IACSharpSensor.IACSharpSensor.SensorReached(1726);
            break;
          case 8:
            IACSharpSensor.IACSharpSensor.SensorReached(1727);
            meth = "DEFLATE";
            IACSharpSensor.IACSharpSensor.SensorReached(1728);
            break;
          case 9:
            IACSharpSensor.IACSharpSensor.SensorReached(1729);
            meth = "Deflate64";
            IACSharpSensor.IACSharpSensor.SensorReached(1730);
            break;
          case 12:
            IACSharpSensor.IACSharpSensor.SensorReached(1731);
            meth = "BZIP2";
            IACSharpSensor.IACSharpSensor.SensorReached(1732);
            break;
          case 14:
            IACSharpSensor.IACSharpSensor.SensorReached(1733);
            meth = "LZMA";
            IACSharpSensor.IACSharpSensor.SensorReached(1734);
            break;
          case 19:
            IACSharpSensor.IACSharpSensor.SensorReached(1735);
            meth = "LZ77";
            IACSharpSensor.IACSharpSensor.SensorReached(1736);
            break;
          case 98:
            IACSharpSensor.IACSharpSensor.SensorReached(1737);
            meth = "PPMd";
            IACSharpSensor.IACSharpSensor.SensorReached(1738);
            break;
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(1739);
            meth = String.Format("Unknown (0x{0:X4})", _CompressionMethod);
            IACSharpSensor.IACSharpSensor.SensorReached(1740);
            break;
        }
        System.String RNTRNTRNT_291 = meth;
        IACSharpSensor.IACSharpSensor.SensorReached(1741);
        return RNTRNTRNT_291;
      }
    }
    internal void ValidateEncryption()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1742);
      if (Encryption != EncryptionAlgorithm.PkzipWeak && Encryption != EncryptionAlgorithm.WinZipAes128 && Encryption != EncryptionAlgorithm.WinZipAes256 && Encryption != EncryptionAlgorithm.None) {
        IACSharpSensor.IACSharpSensor.SensorReached(1743);
        if (_UnsupportedAlgorithmId != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(1744);
          throw new ZipException(String.Format("Cannot extract: Entry {0} is encrypted with an algorithm not supported by DotNetZip: {1}", FileName, UnsupportedAlgorithm));
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1745);
          throw new ZipException(String.Format("Cannot extract: Entry {0} uses an unsupported encryption algorithm ({1:X2})", FileName, (int)Encryption));
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1746);
    }
    private void ValidateCompression()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1747);
      if ((_CompressionMethod_FromZipFile != (short)CompressionMethod.None) && (_CompressionMethod_FromZipFile != (short)CompressionMethod.Deflate) && (_CompressionMethod_FromZipFile != (short)CompressionMethod.BZip2)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1748);
        throw new ZipException(String.Format("Entry {0} uses an unsupported compression method (0x{1:X2}, {2})", FileName, _CompressionMethod_FromZipFile, UnsupportedCompressionMethod));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1749);
    }
    private void SetupCryptoForExtract(string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1750);
      if (_Encryption_FromZipFile == EncryptionAlgorithm.None) {
        IACSharpSensor.IACSharpSensor.SensorReached(1751);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1752);
      if (_Encryption_FromZipFile == EncryptionAlgorithm.PkzipWeak) {
        IACSharpSensor.IACSharpSensor.SensorReached(1753);
        if (password == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1754);
          throw new ZipException("Missing password.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1755);
        this.ArchiveStream.Seek(this.FileDataPosition - 12, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
        _zipCrypto_forExtract = ZipCrypto.ForRead(password, this);
      } else if (_Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes128 || _Encryption_FromZipFile == EncryptionAlgorithm.WinZipAes256) {
        IACSharpSensor.IACSharpSensor.SensorReached(1756);
        if (password == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1757);
          throw new ZipException("Missing password.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1758);
        if (_aesCrypto_forExtract != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1759);
          _aesCrypto_forExtract.Password = password;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1760);
          int sizeOfSaltAndPv = GetLengthOfCryptoHeaderBytes(_Encryption_FromZipFile);
          this.ArchiveStream.Seek(this.FileDataPosition - sizeOfSaltAndPv, SeekOrigin.Begin);
          Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
          int keystrength = GetKeyStrengthInBits(_Encryption_FromZipFile);
          _aesCrypto_forExtract = WinZipAesCrypto.ReadFromStream(password, keystrength, this.ArchiveStream);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1761);
    }
    private bool ValidateOutput(string basedir, Stream outstream, out string outFileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1762);
      if (basedir != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1763);
        string f = this.FileName.Replace("\\", "/");
        IACSharpSensor.IACSharpSensor.SensorReached(1764);
        if (f.IndexOf(':') == 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(1765);
          f = f.Substring(2);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1766);
        if (f.StartsWith("/")) {
          IACSharpSensor.IACSharpSensor.SensorReached(1767);
          f = f.Substring(1);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1768);
        if (_container.ZipFile.FlattenFoldersOnExtract) {
          IACSharpSensor.IACSharpSensor.SensorReached(1769);
          outFileName = Path.Combine(basedir, (f.IndexOf('/') != -1) ? Path.GetFileName(f) : f);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1770);
          outFileName = Path.Combine(basedir, f);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1771);
        outFileName = outFileName.Replace("/", "\\");
        IACSharpSensor.IACSharpSensor.SensorReached(1772);
        if ((IsDirectory) || (FileName.EndsWith("/"))) {
          IACSharpSensor.IACSharpSensor.SensorReached(1773);
          if (!Directory.Exists(outFileName)) {
            IACSharpSensor.IACSharpSensor.SensorReached(1774);
            Directory.CreateDirectory(outFileName);
            _SetTimes(outFileName, false);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1775);
            if (ExtractExistingFile == ExtractExistingFileAction.OverwriteSilently) {
              IACSharpSensor.IACSharpSensor.SensorReached(1776);
              _SetTimes(outFileName, false);
            }
          }
          System.Boolean RNTRNTRNT_292 = true;
          IACSharpSensor.IACSharpSensor.SensorReached(1777);
          return RNTRNTRNT_292;
        }
        System.Boolean RNTRNTRNT_293 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1778);
        return RNTRNTRNT_293;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1779);
      if (outstream != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1780);
        outFileName = null;
        IACSharpSensor.IACSharpSensor.SensorReached(1781);
        if ((IsDirectory) || (FileName.EndsWith("/"))) {
          System.Boolean RNTRNTRNT_294 = true;
          IACSharpSensor.IACSharpSensor.SensorReached(1782);
          return RNTRNTRNT_294;
        }
        System.Boolean RNTRNTRNT_295 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1783);
        return RNTRNTRNT_295;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1784);
      throw new ArgumentNullException("outstream");
    }
  }
}
