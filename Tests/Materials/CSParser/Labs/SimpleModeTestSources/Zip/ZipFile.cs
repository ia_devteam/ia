using System;
using System.IO;
using System.Collections.Generic;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Zip
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00005")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public partial class ZipFile : System.Collections.IEnumerable, System.Collections.Generic.IEnumerable<ZipEntry>, IDisposable
  {
    public bool FullScan { get; set; }
    public bool SortEntriesBeforeSaving { get; set; }
    public bool AddDirectoryWillTraverseReparsePoints { get; set; }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_371 = _BufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(2640);
        return RNTRNTRNT_371;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2641);
        _BufferSize = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2642);
      }
    }
    public int CodecBufferSize { get; set; }
    public bool FlattenFoldersOnExtract { get; set; }
    public Ionic.Zlib.CompressionStrategy Strategy {
      get {
        Ionic.Zlib.CompressionStrategy RNTRNTRNT_372 = _Strategy;
        IACSharpSensor.IACSharpSensor.SensorReached(2643);
        return RNTRNTRNT_372;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2644);
        _Strategy = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2645);
      }
    }
    public string Name {
      get {
        System.String RNTRNTRNT_373 = _name;
        IACSharpSensor.IACSharpSensor.SensorReached(2646);
        return RNTRNTRNT_373;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2647);
        _name = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2648);
      }
    }
    public Ionic.Zlib.CompressionLevel CompressionLevel { get; set; }
    public Ionic.Zip.CompressionMethod CompressionMethod {
      get {
        Ionic.Zip.CompressionMethod RNTRNTRNT_374 = _compressionMethod;
        IACSharpSensor.IACSharpSensor.SensorReached(2649);
        return RNTRNTRNT_374;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2650);
        _compressionMethod = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2651);
      }
    }
    public string Comment {
      get {
        System.String RNTRNTRNT_375 = _Comment;
        IACSharpSensor.IACSharpSensor.SensorReached(2652);
        return RNTRNTRNT_375;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2653);
        _Comment = value;
        _contentsChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(2654);
      }
    }
    public bool EmitTimesInWindowsFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_376 = _emitNtfsTimes;
        IACSharpSensor.IACSharpSensor.SensorReached(2655);
        return RNTRNTRNT_376;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2656);
        _emitNtfsTimes = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2657);
      }
    }
    public bool EmitTimesInUnixFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_377 = _emitUnixTimes;
        IACSharpSensor.IACSharpSensor.SensorReached(2658);
        return RNTRNTRNT_377;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2659);
        _emitUnixTimes = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2660);
      }
    }
    internal bool Verbose {
      get {
        System.Boolean RNTRNTRNT_378 = (_StatusMessageTextWriter != null);
        IACSharpSensor.IACSharpSensor.SensorReached(2661);
        return RNTRNTRNT_378;
      }
    }
    public bool ContainsEntry(string name)
    {
      System.Boolean RNTRNTRNT_379 = _entries.ContainsKey(SharedUtilities.NormalizePathForUseInZipFile(name));
      IACSharpSensor.IACSharpSensor.SensorReached(2662);
      return RNTRNTRNT_379;
    }
    public bool CaseSensitiveRetrieval {
      get {
        System.Boolean RNTRNTRNT_380 = _CaseSensitiveRetrieval;
        IACSharpSensor.IACSharpSensor.SensorReached(2663);
        return RNTRNTRNT_380;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2664);
        if (value != _CaseSensitiveRetrieval) {
          IACSharpSensor.IACSharpSensor.SensorReached(2665);
          _CaseSensitiveRetrieval = value;
          _initEntriesDictionary();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2666);
      }
    }
    [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete.  It will be removed in a future version of the library. Your applications should  use AlternateEncoding and AlternateEncodingUsage instead.")]
    public bool UseUnicodeAsNecessary {
      get {
        System.Boolean RNTRNTRNT_381 = (_alternateEncoding == System.Text.Encoding.GetEncoding("UTF-8")) && (_alternateEncodingUsage == ZipOption.AsNecessary);
        IACSharpSensor.IACSharpSensor.SensorReached(2667);
        return RNTRNTRNT_381;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2668);
        if (value) {
          IACSharpSensor.IACSharpSensor.SensorReached(2669);
          _alternateEncoding = System.Text.Encoding.GetEncoding("UTF-8");
          _alternateEncodingUsage = ZipOption.AsNecessary;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2670);
          _alternateEncoding = Ionic.Zip.ZipFile.DefaultEncoding;
          _alternateEncodingUsage = ZipOption.Never;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2671);
      }
    }
    public Zip64Option UseZip64WhenSaving {
      get {
        Zip64Option RNTRNTRNT_382 = _zip64;
        IACSharpSensor.IACSharpSensor.SensorReached(2672);
        return RNTRNTRNT_382;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2673);
        _zip64 = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2674);
      }
    }
    public Nullable<bool> RequiresZip64 {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2675);
        if (_entries.Count > 65534) {
          Nullable<System.Boolean> RNTRNTRNT_383 = new Nullable<bool>(true);
          IACSharpSensor.IACSharpSensor.SensorReached(2676);
          return RNTRNTRNT_383;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2677);
        if (!_hasBeenSaved || _contentsChanged) {
          IACSharpSensor.IACSharpSensor.SensorReached(2678);
          return null;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2679);
        foreach (ZipEntry e in _entries.Values) {
          IACSharpSensor.IACSharpSensor.SensorReached(2680);
          if (e.RequiresZip64.Value) {
            Nullable<System.Boolean> RNTRNTRNT_384 = new Nullable<bool>(true);
            IACSharpSensor.IACSharpSensor.SensorReached(2681);
            return RNTRNTRNT_384;
          }
        }
        Nullable<System.Boolean> RNTRNTRNT_385 = new Nullable<bool>(false);
        IACSharpSensor.IACSharpSensor.SensorReached(2682);
        return RNTRNTRNT_385;
      }
    }
    public Nullable<bool> OutputUsedZip64 {
      get {
        Nullable<System.Boolean> RNTRNTRNT_386 = _OutputUsesZip64;
        IACSharpSensor.IACSharpSensor.SensorReached(2683);
        return RNTRNTRNT_386;
      }
    }
    public Nullable<bool> InputUsesZip64 {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2684);
        if (_entries.Count > 65534) {
          Nullable<System.Boolean> RNTRNTRNT_387 = true;
          IACSharpSensor.IACSharpSensor.SensorReached(2685);
          return RNTRNTRNT_387;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2686);
        foreach (ZipEntry e in this) {
          IACSharpSensor.IACSharpSensor.SensorReached(2687);
          if (e.Source != ZipEntrySource.ZipFile) {
            IACSharpSensor.IACSharpSensor.SensorReached(2688);
            return null;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2689);
          if (e._InputUsesZip64) {
            Nullable<System.Boolean> RNTRNTRNT_388 = true;
            IACSharpSensor.IACSharpSensor.SensorReached(2690);
            return RNTRNTRNT_388;
          }
        }
        Nullable<System.Boolean> RNTRNTRNT_389 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2691);
        return RNTRNTRNT_389;
      }
    }
    [Obsolete("use AlternateEncoding instead.")]
    public System.Text.Encoding ProvisionalAlternateEncoding {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2692);
        if (_alternateEncodingUsage == ZipOption.AsNecessary) {
          System.Text.Encoding RNTRNTRNT_390 = _alternateEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(2693);
          return RNTRNTRNT_390;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2694);
        return null;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2695);
        _alternateEncoding = value;
        _alternateEncodingUsage = ZipOption.AsNecessary;
        IACSharpSensor.IACSharpSensor.SensorReached(2696);
      }
    }
    public System.Text.Encoding AlternateEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_391 = _alternateEncoding;
        IACSharpSensor.IACSharpSensor.SensorReached(2697);
        return RNTRNTRNT_391;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2698);
        _alternateEncoding = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2699);
      }
    }
    public ZipOption AlternateEncodingUsage {
      get {
        ZipOption RNTRNTRNT_392 = _alternateEncodingUsage;
        IACSharpSensor.IACSharpSensor.SensorReached(2700);
        return RNTRNTRNT_392;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2701);
        _alternateEncodingUsage = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2702);
      }
    }
    public static System.Text.Encoding DefaultEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_393 = _defaultEncoding;
        IACSharpSensor.IACSharpSensor.SensorReached(2703);
        return RNTRNTRNT_393;
      }
    }
    public TextWriter StatusMessageTextWriter {
      get {
        TextWriter RNTRNTRNT_394 = _StatusMessageTextWriter;
        IACSharpSensor.IACSharpSensor.SensorReached(2704);
        return RNTRNTRNT_394;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2705);
        _StatusMessageTextWriter = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2706);
      }
    }
    public String TempFileFolder {
      get {
        String RNTRNTRNT_395 = _TempFileFolder;
        IACSharpSensor.IACSharpSensor.SensorReached(2707);
        return RNTRNTRNT_395;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2708);
        _TempFileFolder = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2709);
        if (value == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2710);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2711);
        if (!Directory.Exists(value)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2712);
          throw new FileNotFoundException(String.Format("That directory ({0}) does not exist.", value));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2713);
      }
    }
    public String Password {
      private get {
        String RNTRNTRNT_396 = _Password;
        IACSharpSensor.IACSharpSensor.SensorReached(2714);
        return RNTRNTRNT_396;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2715);
        _Password = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2716);
        if (_Password == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2717);
          Encryption = EncryptionAlgorithm.None;
        } else if (Encryption == EncryptionAlgorithm.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(2718);
          Encryption = EncryptionAlgorithm.PkzipWeak;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2719);
      }
    }
    public ExtractExistingFileAction ExtractExistingFile { get; set; }
    public ZipErrorAction ZipErrorAction {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2720);
        if (ZipError != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2721);
          _zipErrorAction = ZipErrorAction.InvokeErrorEvent;
        }
        ZipErrorAction RNTRNTRNT_397 = _zipErrorAction;
        IACSharpSensor.IACSharpSensor.SensorReached(2722);
        return RNTRNTRNT_397;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2723);
        _zipErrorAction = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2724);
        if (_zipErrorAction != ZipErrorAction.InvokeErrorEvent && ZipError != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2725);
          ZipError = null;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2726);
      }
    }
    public EncryptionAlgorithm Encryption {
      get {
        EncryptionAlgorithm RNTRNTRNT_398 = _Encryption;
        IACSharpSensor.IACSharpSensor.SensorReached(2727);
        return RNTRNTRNT_398;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2728);
        if (value == EncryptionAlgorithm.Unsupported) {
          IACSharpSensor.IACSharpSensor.SensorReached(2729);
          throw new InvalidOperationException("You may not set Encryption to that value.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2730);
        _Encryption = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2731);
      }
    }
    public SetCompressionCallback SetCompression { get; set; }
    public Int32 MaxOutputSegmentSize {
      get {
        Int32 RNTRNTRNT_399 = _maxOutputSegmentSize;
        IACSharpSensor.IACSharpSensor.SensorReached(2732);
        return RNTRNTRNT_399;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2733);
        if (value < 65536 && value != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2734);
          throw new ZipException("The minimum acceptable segment size is 65536.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2735);
        _maxOutputSegmentSize = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2736);
      }
    }
    public Int32 NumberOfSegmentsForMostRecentSave {
      get {
        Int32 RNTRNTRNT_400 = unchecked((Int32)_numberOfSegmentsForMostRecentSave + 1);
        IACSharpSensor.IACSharpSensor.SensorReached(2737);
        return RNTRNTRNT_400;
      }
    }
    public long ParallelDeflateThreshold {
      get {
        System.Int64 RNTRNTRNT_401 = _ParallelDeflateThreshold;
        IACSharpSensor.IACSharpSensor.SensorReached(2738);
        return RNTRNTRNT_401;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2739);
        if ((value != 0) && (value != -1) && (value < 64 * 1024)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2740);
          throw new ArgumentOutOfRangeException("ParallelDeflateThreshold should be -1, 0, or > 65536");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2741);
        _ParallelDeflateThreshold = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2742);
      }
    }
    public int ParallelDeflateMaxBufferPairs {
      get {
        System.Int32 RNTRNTRNT_402 = _maxBufferPairs;
        IACSharpSensor.IACSharpSensor.SensorReached(2743);
        return RNTRNTRNT_402;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2744);
        if (value < 4) {
          IACSharpSensor.IACSharpSensor.SensorReached(2745);
          throw new ArgumentOutOfRangeException("ParallelDeflateMaxBufferPairs", "Value must be 4 or greater.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2746);
        _maxBufferPairs = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2747);
      }
    }
    public override String ToString()
    {
      String RNTRNTRNT_403 = String.Format("ZipFile::{0}", Name);
      IACSharpSensor.IACSharpSensor.SensorReached(2748);
      return RNTRNTRNT_403;
    }
    public static System.Version LibraryVersion {
      get {
        System.Version RNTRNTRNT_404 = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        IACSharpSensor.IACSharpSensor.SensorReached(2749);
        return RNTRNTRNT_404;
      }
    }
    internal void NotifyEntryChanged()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2750);
      _contentsChanged = true;
      IACSharpSensor.IACSharpSensor.SensorReached(2751);
    }
    internal Stream StreamForDiskNumber(uint diskNumber)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2752);
      if (diskNumber + 1 == this._diskNumberWithCd || (diskNumber == 0 && this._diskNumberWithCd == 0)) {
        Stream RNTRNTRNT_405 = this.ReadStream;
        IACSharpSensor.IACSharpSensor.SensorReached(2753);
        return RNTRNTRNT_405;
      }
      Stream RNTRNTRNT_406 = ZipSegmentedStream.ForReading(this._readName ?? this._name, diskNumber, _diskNumberWithCd);
      IACSharpSensor.IACSharpSensor.SensorReached(2754);
      return RNTRNTRNT_406;
    }
    internal void Reset(bool whileSaving)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2755);
      if (_JustSaved) {
        IACSharpSensor.IACSharpSensor.SensorReached(2756);
        using (ZipFile x = new ZipFile()) {
          IACSharpSensor.IACSharpSensor.SensorReached(2757);
          x._readName = x._name = whileSaving ? (this._readName ?? this._name) : this._name;
          x.AlternateEncoding = this.AlternateEncoding;
          x.AlternateEncodingUsage = this.AlternateEncodingUsage;
          ReadIntoInstance(x);
          IACSharpSensor.IACSharpSensor.SensorReached(2758);
          foreach (ZipEntry e1 in x) {
            IACSharpSensor.IACSharpSensor.SensorReached(2759);
            foreach (ZipEntry e2 in this) {
              IACSharpSensor.IACSharpSensor.SensorReached(2760);
              if (e1.FileName == e2.FileName) {
                IACSharpSensor.IACSharpSensor.SensorReached(2761);
                e2.CopyMetaData(e1);
                IACSharpSensor.IACSharpSensor.SensorReached(2762);
                break;
              }
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2763);
        _JustSaved = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2764);
    }
    public ZipFile(string fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2765);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2766);
        _InitInstance(fileName, null);
      } catch (Exception e1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2767);
        throw new ZipException(String.Format("Could not read {0} as a zip file", fileName), e1);
        IACSharpSensor.IACSharpSensor.SensorReached(2768);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2769);
    }
    public ZipFile(string fileName, System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2770);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2771);
        AlternateEncoding = encoding;
        AlternateEncodingUsage = ZipOption.Always;
        _InitInstance(fileName, null);
      } catch (Exception e1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2772);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
        IACSharpSensor.IACSharpSensor.SensorReached(2773);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2774);
    }
    public ZipFile()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2775);
      _InitInstance(null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2776);
    }
    public ZipFile(System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2777);
      AlternateEncoding = encoding;
      AlternateEncodingUsage = ZipOption.Always;
      _InitInstance(null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2778);
    }
    public ZipFile(string fileName, TextWriter statusMessageWriter)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2779);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2780);
        _InitInstance(fileName, statusMessageWriter);
      } catch (Exception e1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2781);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
        IACSharpSensor.IACSharpSensor.SensorReached(2782);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2783);
    }
    public ZipFile(string fileName, TextWriter statusMessageWriter, System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2784);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2785);
        AlternateEncoding = encoding;
        AlternateEncodingUsage = ZipOption.Always;
        _InitInstance(fileName, statusMessageWriter);
      } catch (Exception e1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2786);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
        IACSharpSensor.IACSharpSensor.SensorReached(2787);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2788);
    }
    public void Initialize(string fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2789);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2790);
        _InitInstance(fileName, null);
      } catch (Exception e1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2791);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
        IACSharpSensor.IACSharpSensor.SensorReached(2792);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2793);
    }
    private void _initEntriesDictionary()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2794);
      StringComparer sc = (CaseSensitiveRetrieval) ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase;
      _entries = (_entries == null) ? new Dictionary<String, ZipEntry>(sc) : new Dictionary<String, ZipEntry>(_entries, sc);
      IACSharpSensor.IACSharpSensor.SensorReached(2795);
    }
    private void _InitInstance(string zipFileName, TextWriter statusMessageWriter)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2796);
      _name = zipFileName;
      _StatusMessageTextWriter = statusMessageWriter;
      _contentsChanged = true;
      AddDirectoryWillTraverseReparsePoints = true;
      CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
      ParallelDeflateThreshold = 512 * 1024;
      _initEntriesDictionary();
      IACSharpSensor.IACSharpSensor.SensorReached(2797);
      if (File.Exists(_name)) {
        IACSharpSensor.IACSharpSensor.SensorReached(2798);
        if (FullScan) {
          IACSharpSensor.IACSharpSensor.SensorReached(2799);
          ReadIntoInstance_Orig(this);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2800);
          ReadIntoInstance(this);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2801);
        this._fileAlreadyExists = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2802);
      return;
    }
    private List<ZipEntry> ZipEntriesAsList {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2803);
        if (_zipEntriesAsList == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2804);
          _zipEntriesAsList = new List<ZipEntry>(_entries.Values);
        }
        List<ZipEntry> RNTRNTRNT_407 = _zipEntriesAsList;
        IACSharpSensor.IACSharpSensor.SensorReached(2805);
        return RNTRNTRNT_407;
      }
    }
    public ZipEntry this[int ix] {
      get {
        ZipEntry RNTRNTRNT_408 = ZipEntriesAsList[ix];
        IACSharpSensor.IACSharpSensor.SensorReached(2806);
        return RNTRNTRNT_408;
      }
    }
    public ZipEntry this[String fileName] {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2807);
        var key = SharedUtilities.NormalizePathForUseInZipFile(fileName);
        IACSharpSensor.IACSharpSensor.SensorReached(2808);
        if (_entries.ContainsKey(key)) {
          ZipEntry RNTRNTRNT_409 = _entries[key];
          IACSharpSensor.IACSharpSensor.SensorReached(2809);
          return RNTRNTRNT_409;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2810);
        key = key.Replace("/", "\\");
        IACSharpSensor.IACSharpSensor.SensorReached(2811);
        if (_entries.ContainsKey(key)) {
          ZipEntry RNTRNTRNT_410 = _entries[key];
          IACSharpSensor.IACSharpSensor.SensorReached(2812);
          return RNTRNTRNT_410;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2813);
        return null;
      }
    }
    public System.Collections.Generic.ICollection<String> EntryFileNames {
      get {
        System.Collections.Generic.ICollection<String> RNTRNTRNT_411 = _entries.Keys;
        IACSharpSensor.IACSharpSensor.SensorReached(2814);
        return RNTRNTRNT_411;
      }
    }
    public System.Collections.Generic.ICollection<ZipEntry> Entries {
      get {
        System.Collections.Generic.ICollection<ZipEntry> RNTRNTRNT_412 = _entries.Values;
        IACSharpSensor.IACSharpSensor.SensorReached(2815);
        return RNTRNTRNT_412;
      }
    }
    public System.Collections.Generic.ICollection<ZipEntry> EntriesSorted {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2816);
        var coll = new System.Collections.Generic.List<ZipEntry>();
        IACSharpSensor.IACSharpSensor.SensorReached(2817);
        foreach (var e in this.Entries) {
          IACSharpSensor.IACSharpSensor.SensorReached(2818);
          coll.Add(e);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2819);
        StringComparison sc = (CaseSensitiveRetrieval) ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase;
        coll.Sort((x, y) => { return String.Compare(x.FileName, y.FileName, sc); });
        System.Collections.Generic.ICollection<ZipEntry> RNTRNTRNT_413 = coll.AsReadOnly();
        IACSharpSensor.IACSharpSensor.SensorReached(2820);
        return RNTRNTRNT_413;
      }
    }
    public int Count {
      get {
        System.Int32 RNTRNTRNT_414 = _entries.Count;
        IACSharpSensor.IACSharpSensor.SensorReached(2821);
        return RNTRNTRNT_414;
      }
    }
    public void RemoveEntry(ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2822);
      if (entry == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2823);
        throw new ArgumentNullException("entry");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2824);
      _entries.Remove(SharedUtilities.NormalizePathForUseInZipFile(entry.FileName));
      _zipEntriesAsList = null;
      _contentsChanged = true;
      IACSharpSensor.IACSharpSensor.SensorReached(2825);
    }
    public void RemoveEntry(String fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2826);
      string modifiedName = ZipEntry.NameInArchive(fileName, null);
      ZipEntry e = this[modifiedName];
      IACSharpSensor.IACSharpSensor.SensorReached(2827);
      if (e == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2828);
        throw new ArgumentException("The entry you specified was not found in the zip archive.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2829);
      RemoveEntry(e);
      IACSharpSensor.IACSharpSensor.SensorReached(2830);
    }
    public void Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2831);
      Dispose(true);
      GC.SuppressFinalize(this);
      IACSharpSensor.IACSharpSensor.SensorReached(2832);
    }
    protected virtual void Dispose(bool disposeManagedResources)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2833);
      if (!this._disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(2834);
        if (disposeManagedResources) {
          IACSharpSensor.IACSharpSensor.SensorReached(2835);
          if (_ReadStreamIsOurs) {
            IACSharpSensor.IACSharpSensor.SensorReached(2836);
            if (_readstream != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(2837);
              _readstream.Dispose();
              _readstream = null;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2838);
          if ((_temporaryFileName != null) && (_name != null)) {
            IACSharpSensor.IACSharpSensor.SensorReached(2839);
            if (_writestream != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(2840);
              _writestream.Dispose();
              _writestream = null;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2841);
          if (this.ParallelDeflater != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2842);
            this.ParallelDeflater.Dispose();
            this.ParallelDeflater = null;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2843);
        this._disposed = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2844);
    }
    internal Stream ReadStream {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2845);
        if (_readstream == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2846);
          if (_readName != null || _name != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2847);
            _readstream = File.Open(_readName ?? _name, FileMode.Open, FileAccess.Read, FileShare.Read | FileShare.Write);
            _ReadStreamIsOurs = true;
          }
        }
        Stream RNTRNTRNT_415 = _readstream;
        IACSharpSensor.IACSharpSensor.SensorReached(2848);
        return RNTRNTRNT_415;
      }
    }
    private Stream WriteStream {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2849);
        if (_writestream != null) {
          Stream RNTRNTRNT_416 = _writestream;
          IACSharpSensor.IACSharpSensor.SensorReached(2850);
          return RNTRNTRNT_416;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2851);
        if (_name == null) {
          Stream RNTRNTRNT_417 = _writestream;
          IACSharpSensor.IACSharpSensor.SensorReached(2852);
          return RNTRNTRNT_417;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2853);
        if (_maxOutputSegmentSize != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2854);
          _writestream = ZipSegmentedStream.ForWriting(this._name, _maxOutputSegmentSize);
          Stream RNTRNTRNT_418 = _writestream;
          IACSharpSensor.IACSharpSensor.SensorReached(2855);
          return RNTRNTRNT_418;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2856);
        SharedUtilities.CreateAndOpenUniqueTempFile(TempFileFolder ?? Path.GetDirectoryName(_name), out _writestream, out _temporaryFileName);
        Stream RNTRNTRNT_419 = _writestream;
        IACSharpSensor.IACSharpSensor.SensorReached(2857);
        return RNTRNTRNT_419;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2858);
        if (value != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2859);
          throw new ZipException("Cannot set the stream to a non-null value.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2860);
        _writestream = null;
        IACSharpSensor.IACSharpSensor.SensorReached(2861);
      }
    }
    private TextWriter _StatusMessageTextWriter;
    private bool _CaseSensitiveRetrieval;
    private Stream _readstream;
    private Stream _writestream;
    private UInt16 _versionMadeBy;
    private UInt16 _versionNeededToExtract;
    private UInt32 _diskNumberWithCd;
    private Int32 _maxOutputSegmentSize;
    private UInt32 _numberOfSegmentsForMostRecentSave;
    private ZipErrorAction _zipErrorAction;
    private bool _disposed;
    private System.Collections.Generic.Dictionary<String, ZipEntry> _entries;
    private List<ZipEntry> _zipEntriesAsList;
    private string _name;
    private string _readName;
    private string _Comment;
    internal string _Password;
    private bool _emitNtfsTimes = true;
    private bool _emitUnixTimes;
    private Ionic.Zlib.CompressionStrategy _Strategy = Ionic.Zlib.CompressionStrategy.Default;
    private Ionic.Zip.CompressionMethod _compressionMethod = Ionic.Zip.CompressionMethod.Deflate;
    private bool _fileAlreadyExists;
    private string _temporaryFileName;
    private bool _contentsChanged;
    private bool _hasBeenSaved;
    private String _TempFileFolder;
    private bool _ReadStreamIsOurs = true;
    private object LOCK = new object();
    private bool _saveOperationCanceled;
    private bool _extractOperationCanceled;
    private bool _addOperationCanceled;
    private EncryptionAlgorithm _Encryption;
    private bool _JustSaved;
    private long _locEndOfCDS = -1;
    private uint _OffsetOfCentralDirectory;
    private Int64 _OffsetOfCentralDirectory64;
    private Nullable<bool> _OutputUsesZip64;
    internal bool _inExtractAll;
    private System.Text.Encoding _alternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
    private ZipOption _alternateEncodingUsage = ZipOption.Never;
    private static System.Text.Encoding _defaultEncoding = System.Text.Encoding.GetEncoding("IBM437");
    private int _BufferSize = BufferSizeDefault;
    internal Ionic.Zlib.ParallelDeflateOutputStream ParallelDeflater;
    private long _ParallelDeflateThreshold;
    private int _maxBufferPairs = 16;
    internal Zip64Option _zip64 = Zip64Option.Default;
    private bool _SavingSfx;
    public static readonly int BufferSizeDefault = 32768;
  }
  public enum Zip64Option
  {
    Default = 0,
    Never = 0,
    AsNecessary = 1,
    Always
  }
  public enum ZipOption
  {
    Default = 0,
    Never = 0,
    AsNecessary = 1,
    Always
  }
  enum AddOrUpdateAction
  {
    AddOnly = 0,
    AddOrUpdate
  }
}
