using Interop = System.Runtime.InteropServices;
namespace Ionic.Zip
{
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000F")]
  [System.Runtime.InteropServices.ComVisible(true)]
  [System.Runtime.InteropServices.ClassInterface(System.Runtime.InteropServices.ClassInterfaceType.AutoDispatch)]
  public class ComHelper
  {
    public bool IsZipFile(string filename)
    {
      System.Boolean RNTRNTRNT_88 = ZipFile.IsZipFile(filename);
      IACSharpSensor.IACSharpSensor.SensorReached(396);
      return RNTRNTRNT_88;
    }
    public bool IsZipFileWithExtract(string filename)
    {
      System.Boolean RNTRNTRNT_89 = ZipFile.IsZipFile(filename, true);
      IACSharpSensor.IACSharpSensor.SensorReached(397);
      return RNTRNTRNT_89;
    }
    public bool CheckZip(string filename)
    {
      System.Boolean RNTRNTRNT_90 = ZipFile.CheckZip(filename);
      IACSharpSensor.IACSharpSensor.SensorReached(398);
      return RNTRNTRNT_90;
    }
    public bool CheckZipPassword(string filename, string password)
    {
      System.Boolean RNTRNTRNT_91 = ZipFile.CheckZipPassword(filename, password);
      IACSharpSensor.IACSharpSensor.SensorReached(399);
      return RNTRNTRNT_91;
    }
    public void FixZipDirectory(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(400);
      ZipFile.FixZipDirectory(filename);
      IACSharpSensor.IACSharpSensor.SensorReached(401);
    }
    public string GetZipLibraryVersion()
    {
      System.String RNTRNTRNT_92 = ZipFile.LibraryVersion.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(402);
      return RNTRNTRNT_92;
    }
  }
}
