using System;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography;
namespace Ionic.Zip
{
  internal class WinZipAesCrypto
  {
    internal byte[] _Salt;
    internal byte[] _providedPv;
    internal byte[] _generatedPv;
    internal int _KeyStrengthInBits;
    private byte[] _MacInitializationVector;
    private byte[] _StoredMac;
    private byte[] _keyBytes;
    private Int16 PasswordVerificationStored;
    private Int16 PasswordVerificationGenerated;
    private int Rfc2898KeygenIterations = 1000;
    private string _Password;
    private bool _cryptoGenerated;
    private WinZipAesCrypto(string password, int KeyStrengthInBits)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(736);
      _Password = password;
      _KeyStrengthInBits = KeyStrengthInBits;
      IACSharpSensor.IACSharpSensor.SensorReached(737);
    }
    public static WinZipAesCrypto Generate(string password, int KeyStrengthInBits)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(738);
      WinZipAesCrypto c = new WinZipAesCrypto(password, KeyStrengthInBits);
      int saltSizeInBytes = c._KeyStrengthInBytes / 2;
      c._Salt = new byte[saltSizeInBytes];
      Random rnd = new Random();
      rnd.NextBytes(c._Salt);
      WinZipAesCrypto RNTRNTRNT_133 = c;
      IACSharpSensor.IACSharpSensor.SensorReached(739);
      return RNTRNTRNT_133;
    }
    public static WinZipAesCrypto ReadFromStream(string password, int KeyStrengthInBits, Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(740);
      WinZipAesCrypto c = new WinZipAesCrypto(password, KeyStrengthInBits);
      int saltSizeInBytes = c._KeyStrengthInBytes / 2;
      c._Salt = new byte[saltSizeInBytes];
      c._providedPv = new byte[2];
      s.Read(c._Salt, 0, c._Salt.Length);
      s.Read(c._providedPv, 0, c._providedPv.Length);
      c.PasswordVerificationStored = (Int16)(c._providedPv[0] + c._providedPv[1] * 256);
      IACSharpSensor.IACSharpSensor.SensorReached(741);
      if (password != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(742);
        c.PasswordVerificationGenerated = (Int16)(c.GeneratedPV[0] + c.GeneratedPV[1] * 256);
        IACSharpSensor.IACSharpSensor.SensorReached(743);
        if (c.PasswordVerificationGenerated != c.PasswordVerificationStored) {
          IACSharpSensor.IACSharpSensor.SensorReached(744);
          throw new BadPasswordException("bad password");
        }
      }
      WinZipAesCrypto RNTRNTRNT_134 = c;
      IACSharpSensor.IACSharpSensor.SensorReached(745);
      return RNTRNTRNT_134;
    }
    public byte[] GeneratedPV {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(746);
        if (!_cryptoGenerated) {
          IACSharpSensor.IACSharpSensor.SensorReached(747);
          _GenerateCryptoBytes();
        }
        System.Byte[] RNTRNTRNT_135 = _generatedPv;
        IACSharpSensor.IACSharpSensor.SensorReached(748);
        return RNTRNTRNT_135;
      }
    }
    public byte[] Salt {
      get {
        System.Byte[] RNTRNTRNT_136 = _Salt;
        IACSharpSensor.IACSharpSensor.SensorReached(749);
        return RNTRNTRNT_136;
      }
    }
    private int _KeyStrengthInBytes {
      get {
        System.Int32 RNTRNTRNT_137 = _KeyStrengthInBits / 8;
        IACSharpSensor.IACSharpSensor.SensorReached(750);
        return RNTRNTRNT_137;
      }
    }
    public int SizeOfEncryptionMetadata {
      get {
        System.Int32 RNTRNTRNT_138 = _KeyStrengthInBytes / 2 + 10 + 2;
        IACSharpSensor.IACSharpSensor.SensorReached(751);
        return RNTRNTRNT_138;
      }
    }
    public string Password {
      private get {
        System.String RNTRNTRNT_139 = _Password;
        IACSharpSensor.IACSharpSensor.SensorReached(752);
        return RNTRNTRNT_139;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(753);
        _Password = value;
        IACSharpSensor.IACSharpSensor.SensorReached(754);
        if (_Password != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(755);
          PasswordVerificationGenerated = (Int16)(GeneratedPV[0] + GeneratedPV[1] * 256);
          IACSharpSensor.IACSharpSensor.SensorReached(756);
          if (PasswordVerificationGenerated != PasswordVerificationStored) {
            IACSharpSensor.IACSharpSensor.SensorReached(757);
            throw new Ionic.Zip.BadPasswordException();
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(758);
      }
    }
    private void _GenerateCryptoBytes()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(759);
      System.Security.Cryptography.Rfc2898DeriveBytes rfc2898 = new System.Security.Cryptography.Rfc2898DeriveBytes(_Password, Salt, Rfc2898KeygenIterations);
      _keyBytes = rfc2898.GetBytes(_KeyStrengthInBytes);
      _MacInitializationVector = rfc2898.GetBytes(_KeyStrengthInBytes);
      _generatedPv = rfc2898.GetBytes(2);
      _cryptoGenerated = true;
      IACSharpSensor.IACSharpSensor.SensorReached(760);
    }
    public byte[] KeyBytes {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(761);
        if (!_cryptoGenerated) {
          IACSharpSensor.IACSharpSensor.SensorReached(762);
          _GenerateCryptoBytes();
        }
        System.Byte[] RNTRNTRNT_140 = _keyBytes;
        IACSharpSensor.IACSharpSensor.SensorReached(763);
        return RNTRNTRNT_140;
      }
    }
    public byte[] MacIv {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(764);
        if (!_cryptoGenerated) {
          IACSharpSensor.IACSharpSensor.SensorReached(765);
          _GenerateCryptoBytes();
        }
        System.Byte[] RNTRNTRNT_141 = _MacInitializationVector;
        IACSharpSensor.IACSharpSensor.SensorReached(766);
        return RNTRNTRNT_141;
      }
    }
    public byte[] CalculatedMac;
    public void ReadAndVerifyMac(System.IO.Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(767);
      bool invalid = false;
      _StoredMac = new byte[10];
      s.Read(_StoredMac, 0, _StoredMac.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(768);
      if (_StoredMac.Length != CalculatedMac.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(769);
        invalid = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(770);
      if (!invalid) {
        IACSharpSensor.IACSharpSensor.SensorReached(771);
        for (int i = 0; i < _StoredMac.Length; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(772);
          if (_StoredMac[i] != CalculatedMac[i]) {
            IACSharpSensor.IACSharpSensor.SensorReached(773);
            invalid = true;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(774);
      if (invalid) {
        IACSharpSensor.IACSharpSensor.SensorReached(775);
        throw new Ionic.Zip.BadStateException("The MAC does not match.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(776);
    }
  }
  internal class WinZipAesCipherStream : Stream
  {
    private WinZipAesCrypto _params;
    private System.IO.Stream _s;
    private CryptoMode _mode;
    private int _nonce;
    private bool _finalBlock;
    internal HMACSHA1 _mac;
    internal RijndaelManaged _aesCipher;
    internal ICryptoTransform _xform;
    private const int BLOCK_SIZE_IN_BYTES = 16;
    private byte[] counter = new byte[BLOCK_SIZE_IN_BYTES];
    private byte[] counterOut = new byte[BLOCK_SIZE_IN_BYTES];
    private long _length;
    private long _totalBytesXferred;
    private byte[] _PendingWriteBlock;
    private int _pendingCount;
    private byte[] _iobuf;
    internal WinZipAesCipherStream(System.IO.Stream s, WinZipAesCrypto cryptoParams, long length, CryptoMode mode) : this(s, cryptoParams, mode)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(777);
      _length = length;
      IACSharpSensor.IACSharpSensor.SensorReached(778);
    }
    internal WinZipAesCipherStream(System.IO.Stream s, WinZipAesCrypto cryptoParams, CryptoMode mode) : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(779);
      TraceOutput("-------------------------------------------------------");
      TraceOutput("Create {0:X8}", this.GetHashCode());
      _params = cryptoParams;
      _s = s;
      _mode = mode;
      _nonce = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(780);
      if (_params == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(781);
        throw new BadPasswordException("Supply a password to use AES encryption.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(782);
      int keySizeInBits = _params.KeyBytes.Length * 8;
      IACSharpSensor.IACSharpSensor.SensorReached(783);
      if (keySizeInBits != 256 && keySizeInBits != 128 && keySizeInBits != 192) {
        IACSharpSensor.IACSharpSensor.SensorReached(784);
        throw new ArgumentOutOfRangeException("keysize", "size of key must be 128, 192, or 256");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(785);
      _mac = new HMACSHA1(_params.MacIv);
      _aesCipher = new System.Security.Cryptography.RijndaelManaged();
      _aesCipher.BlockSize = 128;
      _aesCipher.KeySize = keySizeInBits;
      _aesCipher.Mode = CipherMode.ECB;
      _aesCipher.Padding = PaddingMode.None;
      byte[] iv = new byte[BLOCK_SIZE_IN_BYTES];
      _xform = _aesCipher.CreateEncryptor(_params.KeyBytes, iv);
      IACSharpSensor.IACSharpSensor.SensorReached(786);
      if (_mode == CryptoMode.Encrypt) {
        IACSharpSensor.IACSharpSensor.SensorReached(787);
        _iobuf = new byte[2048];
        _PendingWriteBlock = new byte[BLOCK_SIZE_IN_BYTES];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(788);
    }
    private void XorInPlace(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(789);
      for (int i = 0; i < count; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(790);
        buffer[offset + i] = (byte)(counterOut[i] ^ buffer[offset + i]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(791);
    }
    private void WriteTransformOneBlock(byte[] buffer, int offset)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(792);
      System.Array.Copy(BitConverter.GetBytes(_nonce++), 0, counter, 0, 4);
      _xform.TransformBlock(counter, 0, BLOCK_SIZE_IN_BYTES, counterOut, 0);
      XorInPlace(buffer, offset, BLOCK_SIZE_IN_BYTES);
      _mac.TransformBlock(buffer, offset, BLOCK_SIZE_IN_BYTES, null, 0);
      IACSharpSensor.IACSharpSensor.SensorReached(793);
    }
    private void WriteTransformBlocks(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(794);
      int posn = offset;
      int last = count + offset;
      IACSharpSensor.IACSharpSensor.SensorReached(795);
      while (posn < buffer.Length && posn < last) {
        IACSharpSensor.IACSharpSensor.SensorReached(796);
        WriteTransformOneBlock(buffer, posn);
        posn += BLOCK_SIZE_IN_BYTES;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(797);
    }
    private void WriteTransformFinalBlock()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(798);
      if (_pendingCount == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(799);
        throw new InvalidOperationException("No bytes available.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(800);
      if (_finalBlock) {
        IACSharpSensor.IACSharpSensor.SensorReached(801);
        throw new InvalidOperationException("The final block has already been transformed.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(802);
      System.Array.Copy(BitConverter.GetBytes(_nonce++), 0, counter, 0, 4);
      counterOut = _xform.TransformFinalBlock(counter, 0, BLOCK_SIZE_IN_BYTES);
      XorInPlace(_PendingWriteBlock, 0, _pendingCount);
      _mac.TransformFinalBlock(_PendingWriteBlock, 0, _pendingCount);
      _finalBlock = true;
      IACSharpSensor.IACSharpSensor.SensorReached(803);
    }
    private int ReadTransformOneBlock(byte[] buffer, int offset, int last)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(804);
      if (_finalBlock) {
        IACSharpSensor.IACSharpSensor.SensorReached(805);
        throw new NotSupportedException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(806);
      int bytesRemaining = last - offset;
      int bytesToRead = (bytesRemaining > BLOCK_SIZE_IN_BYTES) ? BLOCK_SIZE_IN_BYTES : bytesRemaining;
      System.Array.Copy(BitConverter.GetBytes(_nonce++), 0, counter, 0, 4);
      IACSharpSensor.IACSharpSensor.SensorReached(807);
      if ((bytesToRead == bytesRemaining) && (_length > 0) && (_totalBytesXferred + last == _length)) {
        IACSharpSensor.IACSharpSensor.SensorReached(808);
        _mac.TransformFinalBlock(buffer, offset, bytesToRead);
        counterOut = _xform.TransformFinalBlock(counter, 0, BLOCK_SIZE_IN_BYTES);
        _finalBlock = true;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(809);
        _mac.TransformBlock(buffer, offset, bytesToRead, null, 0);
        _xform.TransformBlock(counter, 0, BLOCK_SIZE_IN_BYTES, counterOut, 0);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(810);
      XorInPlace(buffer, offset, bytesToRead);
      System.Int32 RNTRNTRNT_142 = bytesToRead;
      IACSharpSensor.IACSharpSensor.SensorReached(811);
      return RNTRNTRNT_142;
    }
    private void ReadTransformBlocks(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(812);
      int posn = offset;
      int last = count + offset;
      IACSharpSensor.IACSharpSensor.SensorReached(813);
      while (posn < buffer.Length && posn < last) {
        IACSharpSensor.IACSharpSensor.SensorReached(814);
        int n = ReadTransformOneBlock(buffer, posn, last);
        posn += n;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(815);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(816);
      if (_mode == CryptoMode.Encrypt) {
        IACSharpSensor.IACSharpSensor.SensorReached(817);
        throw new NotSupportedException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(818);
      if (buffer == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(819);
        throw new ArgumentNullException("buffer");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(820);
      if (offset < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(821);
        throw new ArgumentOutOfRangeException("offset", "Must not be less than zero.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(822);
      if (count < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(823);
        throw new ArgumentOutOfRangeException("count", "Must not be less than zero.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(824);
      if (buffer.Length < offset + count) {
        IACSharpSensor.IACSharpSensor.SensorReached(825);
        throw new ArgumentException("The buffer is too small");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(826);
      int bytesToRead = count;
      IACSharpSensor.IACSharpSensor.SensorReached(827);
      if (_totalBytesXferred >= _length) {
        System.Int32 RNTRNTRNT_143 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(828);
        return RNTRNTRNT_143;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(829);
      long bytesRemaining = _length - _totalBytesXferred;
      IACSharpSensor.IACSharpSensor.SensorReached(830);
      if (bytesRemaining < count) {
        IACSharpSensor.IACSharpSensor.SensorReached(831);
        bytesToRead = (int)bytesRemaining;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(832);
      int n = _s.Read(buffer, offset, bytesToRead);
      ReadTransformBlocks(buffer, offset, bytesToRead);
      _totalBytesXferred += n;
      System.Int32 RNTRNTRNT_144 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(833);
      return RNTRNTRNT_144;
    }
    public byte[] FinalAuthentication {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(834);
        if (!_finalBlock) {
          IACSharpSensor.IACSharpSensor.SensorReached(835);
          if (_totalBytesXferred != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(836);
            throw new BadStateException("The final hash has not been computed.");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(837);
          byte[] b = {
            
          };
          _mac.ComputeHash(b);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(838);
        byte[] macBytes10 = new byte[10];
        System.Array.Copy(_mac.Hash, 0, macBytes10, 0, 10);
        System.Byte[] RNTRNTRNT_145 = macBytes10;
        IACSharpSensor.IACSharpSensor.SensorReached(839);
        return RNTRNTRNT_145;
      }
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(840);
      if (_finalBlock) {
        IACSharpSensor.IACSharpSensor.SensorReached(841);
        throw new InvalidOperationException("The final block has already been transformed.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(842);
      if (_mode == CryptoMode.Decrypt) {
        IACSharpSensor.IACSharpSensor.SensorReached(843);
        throw new NotSupportedException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(844);
      if (buffer == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(845);
        throw new ArgumentNullException("buffer");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(846);
      if (offset < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(847);
        throw new ArgumentOutOfRangeException("offset", "Must not be less than zero.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(848);
      if (count < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(849);
        throw new ArgumentOutOfRangeException("count", "Must not be less than zero.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(850);
      if (buffer.Length < offset + count) {
        IACSharpSensor.IACSharpSensor.SensorReached(851);
        throw new ArgumentException("The offset and count are too large");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(852);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(853);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(854);
      TraceOutput("Write off({0}) count({1})", offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(855);
      if (count + _pendingCount <= BLOCK_SIZE_IN_BYTES) {
        IACSharpSensor.IACSharpSensor.SensorReached(856);
        Buffer.BlockCopy(buffer, offset, _PendingWriteBlock, _pendingCount, count);
        _pendingCount += count;
        IACSharpSensor.IACSharpSensor.SensorReached(857);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(858);
      int bytesRemaining = count;
      int curOffset = offset;
      IACSharpSensor.IACSharpSensor.SensorReached(859);
      if (_pendingCount != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(860);
        int fillCount = BLOCK_SIZE_IN_BYTES - _pendingCount;
        IACSharpSensor.IACSharpSensor.SensorReached(861);
        if (fillCount > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(862);
          Buffer.BlockCopy(buffer, offset, _PendingWriteBlock, _pendingCount, fillCount);
          bytesRemaining -= fillCount;
          curOffset += fillCount;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(863);
        WriteTransformOneBlock(_PendingWriteBlock, 0);
        _s.Write(_PendingWriteBlock, 0, BLOCK_SIZE_IN_BYTES);
        _totalBytesXferred += BLOCK_SIZE_IN_BYTES;
        _pendingCount = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(864);
      int blocksToXform = (bytesRemaining - 1) / BLOCK_SIZE_IN_BYTES;
      _pendingCount = bytesRemaining - (blocksToXform * BLOCK_SIZE_IN_BYTES);
      Buffer.BlockCopy(buffer, curOffset + bytesRemaining - _pendingCount, _PendingWriteBlock, 0, _pendingCount);
      bytesRemaining -= _pendingCount;
      _totalBytesXferred += bytesRemaining;
      IACSharpSensor.IACSharpSensor.SensorReached(865);
      if (blocksToXform > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(866);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(867);
          int c = _iobuf.Length;
          IACSharpSensor.IACSharpSensor.SensorReached(868);
          if (c > bytesRemaining) {
            IACSharpSensor.IACSharpSensor.SensorReached(869);
            c = bytesRemaining;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(870);
          Buffer.BlockCopy(buffer, curOffset, _iobuf, 0, c);
          WriteTransformBlocks(_iobuf, 0, c);
          _s.Write(_iobuf, 0, c);
          bytesRemaining -= c;
          curOffset += c;
        } while (bytesRemaining > 0);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(871);
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(872);
      TraceOutput("Close {0:X8}", this.GetHashCode());
      IACSharpSensor.IACSharpSensor.SensorReached(873);
      if (_pendingCount > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(874);
        WriteTransformFinalBlock();
        _s.Write(_PendingWriteBlock, 0, _pendingCount);
        _totalBytesXferred += _pendingCount;
        _pendingCount = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(875);
      _s.Close();
      TraceOutput("-------------------------------------------------------");
      IACSharpSensor.IACSharpSensor.SensorReached(876);
    }
    public override bool CanRead {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(877);
        if (_mode != CryptoMode.Decrypt) {
          System.Boolean RNTRNTRNT_146 = false;
          IACSharpSensor.IACSharpSensor.SensorReached(878);
          return RNTRNTRNT_146;
        }
        System.Boolean RNTRNTRNT_147 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(879);
        return RNTRNTRNT_147;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_148 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(880);
        return RNTRNTRNT_148;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_149 = (_mode == CryptoMode.Encrypt);
        IACSharpSensor.IACSharpSensor.SensorReached(881);
        return RNTRNTRNT_149;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(882);
      _s.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(883);
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(884);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(885);
        throw new NotImplementedException();
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(886);
        throw new NotImplementedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(887);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(888);
      throw new NotImplementedException();
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceOutput(string format, params object[] varParams)
    {
      lock (_outputLock) {
        IACSharpSensor.IACSharpSensor.SensorReached(889);
        int tid = System.Threading.Thread.CurrentThread.GetHashCode();
        Console.ForegroundColor = (ConsoleColor)(tid % 8 + 8);
        Console.Write("{0:000} WZACS ", tid);
        Console.WriteLine(format, varParams);
        Console.ResetColor();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(890);
    }
    private object _outputLock = new Object();
  }
}
