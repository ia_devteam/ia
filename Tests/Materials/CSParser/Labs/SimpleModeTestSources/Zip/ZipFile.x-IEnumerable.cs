namespace Ionic.Zip
{
  public partial class ZipFile
  {
    public System.Collections.Generic.IEnumerator<ZipEntry> GetEnumerator()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3559);
      foreach (ZipEntry e in _entries.Values) {
        System.Collections.Generic.IEnumerator<ZipEntry> RNTRNTRNT_466 = e;
        IACSharpSensor.IACSharpSensor.SensorReached(3560);
        yield return RNTRNTRNT_466;
        IACSharpSensor.IACSharpSensor.SensorReached(3561);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3562);
    }
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      System.Collections.IEnumerator RNTRNTRNT_467 = GetEnumerator();
      IACSharpSensor.IACSharpSensor.SensorReached(3563);
      return RNTRNTRNT_467;
    }
    [System.Runtime.InteropServices.DispId(-4)]
    public System.Collections.IEnumerator GetNewEnum()
    {
      System.Collections.IEnumerator RNTRNTRNT_468 = GetEnumerator();
      IACSharpSensor.IACSharpSensor.SensorReached(3564);
      return RNTRNTRNT_468;
    }
  }
}
