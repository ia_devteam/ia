using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    public void ExtractAll(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2963);
      _InternalExtractAll(path, true);
      IACSharpSensor.IACSharpSensor.SensorReached(2964);
    }
    public void ExtractAll(string path, ExtractExistingFileAction extractExistingFile)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2965);
      ExtractExistingFile = extractExistingFile;
      _InternalExtractAll(path, true);
      IACSharpSensor.IACSharpSensor.SensorReached(2966);
    }
    private void _InternalExtractAll(string path, bool overrideExtractExistingProperty)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2967);
      bool header = Verbose;
      _inExtractAll = true;
      IACSharpSensor.IACSharpSensor.SensorReached(2968);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2969);
        OnExtractAllStarted(path);
        int n = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(2970);
        foreach (ZipEntry e in _entries.Values) {
          IACSharpSensor.IACSharpSensor.SensorReached(2971);
          if (header) {
            IACSharpSensor.IACSharpSensor.SensorReached(2972);
            StatusMessageTextWriter.WriteLine("\n{1,-22} {2,-8} {3,4}   {4,-8}  {0}", "Name", "Modified", "Size", "Ratio", "Packed");
            StatusMessageTextWriter.WriteLine(new System.String('-', 72));
            header = false;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2973);
          if (Verbose) {
            IACSharpSensor.IACSharpSensor.SensorReached(2974);
            StatusMessageTextWriter.WriteLine("{1,-22} {2,-8} {3,4:F0}%   {4,-8} {0}", e.FileName, e.LastModified.ToString("yyyy-MM-dd HH:mm:ss"), e.UncompressedSize, e.CompressionRatio, e.CompressedSize);
            IACSharpSensor.IACSharpSensor.SensorReached(2975);
            if (!String.IsNullOrEmpty(e.Comment)) {
              IACSharpSensor.IACSharpSensor.SensorReached(2976);
              StatusMessageTextWriter.WriteLine("  Comment: {0}", e.Comment);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2977);
          e.Password = _Password;
          OnExtractEntry(n, true, e, path);
          IACSharpSensor.IACSharpSensor.SensorReached(2978);
          if (overrideExtractExistingProperty) {
            IACSharpSensor.IACSharpSensor.SensorReached(2979);
            e.ExtractExistingFile = this.ExtractExistingFile;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2980);
          e.Extract(path);
          n++;
          OnExtractEntry(n, false, e, path);
          IACSharpSensor.IACSharpSensor.SensorReached(2981);
          if (_extractOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(2982);
            break;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2983);
        if (!_extractOperationCanceled) {
          IACSharpSensor.IACSharpSensor.SensorReached(2984);
          foreach (ZipEntry e in _entries.Values) {
            IACSharpSensor.IACSharpSensor.SensorReached(2985);
            if ((e.IsDirectory) || (e.FileName.EndsWith("/"))) {
              IACSharpSensor.IACSharpSensor.SensorReached(2986);
              string outputFile = (e.FileName.StartsWith("/")) ? Path.Combine(path, e.FileName.Substring(1)) : Path.Combine(path, e.FileName);
              e._SetTimes(outputFile, false);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2987);
          OnExtractAllCompleted(path);
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(2988);
        _inExtractAll = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2989);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2990);
    }
  }
}
