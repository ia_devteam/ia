using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    private void DeleteFileWithRetry(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3148);
      bool done = false;
      int nRetries = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(3149);
      for (int i = 0; i < nRetries && !done; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3150);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(3151);
          File.Delete(filename);
          done = true;
        } catch (System.UnauthorizedAccessException) {
          IACSharpSensor.IACSharpSensor.SensorReached(3152);
          Console.WriteLine("************************************************** Retry delete.");
          System.Threading.Thread.Sleep(200 + i * 200);
          IACSharpSensor.IACSharpSensor.SensorReached(3153);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3154);
    }
    public void Save()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3155);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3156);
        bool thisSaveUsedZip64 = false;
        _saveOperationCanceled = false;
        _numberOfSegmentsForMostRecentSave = 0;
        OnSaveStarted();
        IACSharpSensor.IACSharpSensor.SensorReached(3157);
        if (WriteStream == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3158);
          throw new BadStateException("You haven't specified where to save the zip.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3159);
        if (_name != null && _name.EndsWith(".exe") && !_SavingSfx) {
          IACSharpSensor.IACSharpSensor.SensorReached(3160);
          throw new BadStateException("You specified an EXE for a plain zip file.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3161);
        if (!_contentsChanged) {
          IACSharpSensor.IACSharpSensor.SensorReached(3162);
          OnSaveCompleted();
          IACSharpSensor.IACSharpSensor.SensorReached(3163);
          if (Verbose) {
            IACSharpSensor.IACSharpSensor.SensorReached(3164);
            StatusMessageTextWriter.WriteLine("No save is necessary....");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3165);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3166);
        Reset(true);
        IACSharpSensor.IACSharpSensor.SensorReached(3167);
        if (Verbose) {
          IACSharpSensor.IACSharpSensor.SensorReached(3168);
          StatusMessageTextWriter.WriteLine("saving....");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3169);
        if (_entries.Count >= 0xffff && _zip64 == Zip64Option.Never) {
          IACSharpSensor.IACSharpSensor.SensorReached(3170);
          throw new ZipException("The number of entries is 65535 or greater. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3171);
        int n = 0;
        ICollection<ZipEntry> c = (SortEntriesBeforeSaving) ? EntriesSorted : Entries;
        IACSharpSensor.IACSharpSensor.SensorReached(3172);
        foreach (ZipEntry e in c) {
          IACSharpSensor.IACSharpSensor.SensorReached(3173);
          OnSaveEntry(n, e, true);
          e.Write(WriteStream);
          IACSharpSensor.IACSharpSensor.SensorReached(3174);
          if (_saveOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(3175);
            break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3176);
          n++;
          OnSaveEntry(n, e, false);
          IACSharpSensor.IACSharpSensor.SensorReached(3177);
          if (_saveOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(3178);
            break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3179);
          if (e.IncludedInMostRecentSave) {
            IACSharpSensor.IACSharpSensor.SensorReached(3180);
            thisSaveUsedZip64 |= e.OutputUsedZip64.Value;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3181);
        if (_saveOperationCanceled) {
          IACSharpSensor.IACSharpSensor.SensorReached(3182);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3183);
        var zss = WriteStream as ZipSegmentedStream;
        _numberOfSegmentsForMostRecentSave = (zss != null) ? zss.CurrentSegment : 1;
        bool directoryNeededZip64 = ZipOutput.WriteCentralDirectoryStructure(WriteStream, c, _numberOfSegmentsForMostRecentSave, _zip64, Comment, new ZipContainer(this));
        OnSaveEvent(ZipProgressEventType.Saving_AfterSaveTempArchive);
        _hasBeenSaved = true;
        _contentsChanged = false;
        thisSaveUsedZip64 |= directoryNeededZip64;
        _OutputUsesZip64 = new Nullable<bool>(thisSaveUsedZip64);
        IACSharpSensor.IACSharpSensor.SensorReached(3184);
        if (_name != null && (_temporaryFileName != null || zss != null)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3185);
          WriteStream.Dispose();
          IACSharpSensor.IACSharpSensor.SensorReached(3186);
          if (_saveOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(3187);
            return;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3188);
          if (_fileAlreadyExists && this._readstream != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3189);
            this._readstream.Close();
            this._readstream = null;
            IACSharpSensor.IACSharpSensor.SensorReached(3190);
            foreach (var e in c) {
              IACSharpSensor.IACSharpSensor.SensorReached(3191);
              var zss1 = e._archiveStream as ZipSegmentedStream;
              IACSharpSensor.IACSharpSensor.SensorReached(3192);
              if (zss1 != null) {
                IACSharpSensor.IACSharpSensor.SensorReached(3193);
                zss1.Dispose();
              }
              IACSharpSensor.IACSharpSensor.SensorReached(3194);
              e._archiveStream = null;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3195);
          string tmpName = null;
          IACSharpSensor.IACSharpSensor.SensorReached(3196);
          if (File.Exists(_name)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3197);
            tmpName = _name + "." + Path.GetRandomFileName();
            IACSharpSensor.IACSharpSensor.SensorReached(3198);
            if (File.Exists(tmpName)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3199);
              DeleteFileWithRetry(tmpName);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3200);
            File.Move(_name, tmpName);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3201);
          OnSaveEvent(ZipProgressEventType.Saving_BeforeRenameTempArchive);
          File.Move((zss != null) ? zss.CurrentTempName : _temporaryFileName, _name);
          OnSaveEvent(ZipProgressEventType.Saving_AfterRenameTempArchive);
          IACSharpSensor.IACSharpSensor.SensorReached(3202);
          if (tmpName != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3203);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(3204);
              if (File.Exists(tmpName)) {
                IACSharpSensor.IACSharpSensor.SensorReached(3205);
                File.Delete(tmpName);
              }
            } catch {
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3206);
          _fileAlreadyExists = true;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3207);
        NotifyEntriesSaveComplete(c);
        OnSaveCompleted();
        _JustSaved = true;
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(3208);
        CleanupAfterSaveOperation();
        IACSharpSensor.IACSharpSensor.SensorReached(3209);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3210);
      return;
    }
    private static void NotifyEntriesSaveComplete(ICollection<ZipEntry> c)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3211);
      foreach (ZipEntry e in c) {
        IACSharpSensor.IACSharpSensor.SensorReached(3212);
        e.NotifySaveComplete();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3213);
    }
    private void RemoveTempFile()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3214);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3215);
        if (File.Exists(_temporaryFileName)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3216);
          File.Delete(_temporaryFileName);
        }
      } catch (IOException ex1) {
        IACSharpSensor.IACSharpSensor.SensorReached(3217);
        if (Verbose) {
          IACSharpSensor.IACSharpSensor.SensorReached(3218);
          StatusMessageTextWriter.WriteLine("ZipFile::Save: could not delete temp file: {0}.", ex1.Message);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3219);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3220);
    }
    private void CleanupAfterSaveOperation()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3221);
      if (_name != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3222);
        if (_writestream != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3223);
          try {
            IACSharpSensor.IACSharpSensor.SensorReached(3224);
            _writestream.Dispose();
          } catch (System.IO.IOException) {
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3225);
        _writestream = null;
        IACSharpSensor.IACSharpSensor.SensorReached(3226);
        if (_temporaryFileName != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3227);
          RemoveTempFile();
          _temporaryFileName = null;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3228);
    }
    public void Save(String fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3229);
      if (_name == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3230);
        _writestream = null;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(3231);
        _readName = _name;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3232);
      _name = fileName;
      IACSharpSensor.IACSharpSensor.SensorReached(3233);
      if (Directory.Exists(_name)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3234);
        throw new ZipException("Bad Directory", new System.ArgumentException("That name specifies an existing directory. Please specify a filename.", "fileName"));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3235);
      _contentsChanged = true;
      _fileAlreadyExists = File.Exists(_name);
      Save();
      IACSharpSensor.IACSharpSensor.SensorReached(3236);
    }
    public void Save(Stream outputStream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3237);
      if (outputStream == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3238);
        throw new ArgumentNullException("outputStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3239);
      if (!outputStream.CanWrite) {
        IACSharpSensor.IACSharpSensor.SensorReached(3240);
        throw new ArgumentException("Must be a writable stream.", "outputStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3241);
      _name = null;
      _writestream = new CountingStream(outputStream);
      _contentsChanged = true;
      _fileAlreadyExists = false;
      Save();
      IACSharpSensor.IACSharpSensor.SensorReached(3242);
    }
  }
  static internal class ZipOutput
  {
    public static bool WriteCentralDirectoryStructure(Stream s, ICollection<ZipEntry> entries, uint numSegments, Zip64Option zip64, String comment, ZipContainer container)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3243);
      var zss = s as ZipSegmentedStream;
      IACSharpSensor.IACSharpSensor.SensorReached(3244);
      if (zss != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3245);
        zss.ContiguousWrite = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3246);
      Int64 aLength = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3247);
      using (var ms = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(3248);
        foreach (ZipEntry e in entries) {
          IACSharpSensor.IACSharpSensor.SensorReached(3249);
          if (e.IncludedInMostRecentSave) {
            IACSharpSensor.IACSharpSensor.SensorReached(3250);
            e.WriteCentralDirectoryEntry(ms);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3251);
        var a = ms.ToArray();
        s.Write(a, 0, a.Length);
        aLength = a.Length;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3252);
      var output = s as CountingStream;
      long Finish = (output != null) ? output.ComputedPosition : s.Position;
      long Start = Finish - aLength;
      UInt32 startSegment = (zss != null) ? zss.CurrentSegment : 0;
      Int64 SizeOfCentralDirectory = Finish - Start;
      int countOfEntries = CountEntries(entries);
      bool needZip64CentralDirectory = zip64 == Zip64Option.Always || countOfEntries >= 0xffff || SizeOfCentralDirectory > 0xffffffffu || Start > 0xffffffffu;
      byte[] a2 = null;
      IACSharpSensor.IACSharpSensor.SensorReached(3253);
      if (needZip64CentralDirectory) {
        IACSharpSensor.IACSharpSensor.SensorReached(3254);
        if (zip64 == Zip64Option.Never) {
          IACSharpSensor.IACSharpSensor.SensorReached(3255);
          System.Diagnostics.StackFrame sf = new System.Diagnostics.StackFrame(1);
          IACSharpSensor.IACSharpSensor.SensorReached(3256);
          if (sf.GetMethod().DeclaringType == typeof(ZipFile)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3257);
            throw new ZipException("The archive requires a ZIP64 Central Directory. Consider setting the ZipFile.UseZip64WhenSaving property.");
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(3258);
            throw new ZipException("The archive requires a ZIP64 Central Directory. Consider setting the ZipOutputStream.EnableZip64 property.");
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3259);
        var a = GenZip64EndOfCentralDirectory(Start, Finish, countOfEntries, numSegments);
        a2 = GenCentralDirectoryFooter(Start, Finish, zip64, countOfEntries, comment, container);
        IACSharpSensor.IACSharpSensor.SensorReached(3260);
        if (startSegment != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3261);
          UInt32 thisSegment = zss.ComputeSegment(a.Length + a2.Length);
          int i = 16;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
          i += 4;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
          i = 60;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
          i += 4;
          i += 8;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3262);
        s.Write(a, 0, a.Length);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(3263);
        a2 = GenCentralDirectoryFooter(Start, Finish, zip64, countOfEntries, comment, container);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3264);
      if (startSegment != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3265);
        UInt16 thisSegment = (UInt16)zss.ComputeSegment(a2.Length);
        int i = 4;
        Array.Copy(BitConverter.GetBytes(thisSegment), 0, a2, i, 2);
        i += 2;
        Array.Copy(BitConverter.GetBytes(thisSegment), 0, a2, i, 2);
        i += 2;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3266);
      s.Write(a2, 0, a2.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(3267);
      if (zss != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3268);
        zss.ContiguousWrite = false;
      }
      System.Boolean RNTRNTRNT_439 = needZip64CentralDirectory;
      IACSharpSensor.IACSharpSensor.SensorReached(3269);
      return RNTRNTRNT_439;
    }
    private static System.Text.Encoding GetEncoding(ZipContainer container, string t)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3270);
      switch (container.AlternateEncodingUsage) {
        case ZipOption.Always:
          System.Text.Encoding RNTRNTRNT_440 = container.AlternateEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(3271);
          return RNTRNTRNT_440;
        case ZipOption.Never:
          System.Text.Encoding RNTRNTRNT_441 = container.DefaultEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(3272);
          return RNTRNTRNT_441;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3273);
      var e = container.DefaultEncoding;
      IACSharpSensor.IACSharpSensor.SensorReached(3274);
      if (t == null) {
        System.Text.Encoding RNTRNTRNT_442 = e;
        IACSharpSensor.IACSharpSensor.SensorReached(3275);
        return RNTRNTRNT_442;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3276);
      var bytes = e.GetBytes(t);
      var t2 = e.GetString(bytes, 0, bytes.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(3277);
      if (t2.Equals(t)) {
        System.Text.Encoding RNTRNTRNT_443 = e;
        IACSharpSensor.IACSharpSensor.SensorReached(3278);
        return RNTRNTRNT_443;
      }
      System.Text.Encoding RNTRNTRNT_444 = container.AlternateEncoding;
      IACSharpSensor.IACSharpSensor.SensorReached(3279);
      return RNTRNTRNT_444;
    }
    private static byte[] GenCentralDirectoryFooter(long StartOfCentralDirectory, long EndOfCentralDirectory, Zip64Option zip64, int entryCount, string comment, ZipContainer container)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3280);
      System.Text.Encoding encoding = GetEncoding(container, comment);
      int j = 0;
      int bufferLength = 22;
      byte[] block = null;
      Int16 commentLength = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3281);
      if ((comment != null) && (comment.Length != 0)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3282);
        block = encoding.GetBytes(comment);
        commentLength = (Int16)block.Length;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3283);
      bufferLength += commentLength;
      byte[] bytes = new byte[bufferLength];
      int i = 0;
      byte[] sig = BitConverter.GetBytes(ZipConstants.EndOfCentralDirectorySignature);
      Array.Copy(sig, 0, bytes, i, 4);
      i += 4;
      bytes[i++] = 0;
      bytes[i++] = 0;
      bytes[i++] = 0;
      bytes[i++] = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3284);
      if (entryCount >= 0xffff || zip64 == Zip64Option.Always) {
        IACSharpSensor.IACSharpSensor.SensorReached(3285);
        for (j = 0; j < 4; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(3286);
          bytes[i++] = 0xff;
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(3287);
        bytes[i++] = (byte)(entryCount & 0xff);
        bytes[i++] = (byte)((entryCount & 0xff00) >> 8);
        bytes[i++] = (byte)(entryCount & 0xff);
        bytes[i++] = (byte)((entryCount & 0xff00) >> 8);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3288);
      Int64 SizeOfCentralDirectory = EndOfCentralDirectory - StartOfCentralDirectory;
      IACSharpSensor.IACSharpSensor.SensorReached(3289);
      if (SizeOfCentralDirectory >= 0xffffffffu || StartOfCentralDirectory >= 0xffffffffu) {
        IACSharpSensor.IACSharpSensor.SensorReached(3290);
        for (j = 0; j < 8; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(3291);
          bytes[i++] = 0xff;
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(3292);
        bytes[i++] = (byte)(SizeOfCentralDirectory & 0xff);
        bytes[i++] = (byte)((SizeOfCentralDirectory & 0xff00) >> 8);
        bytes[i++] = (byte)((SizeOfCentralDirectory & 0xff0000) >> 16);
        bytes[i++] = (byte)((SizeOfCentralDirectory & 0xff000000u) >> 24);
        bytes[i++] = (byte)(StartOfCentralDirectory & 0xff);
        bytes[i++] = (byte)((StartOfCentralDirectory & 0xff00) >> 8);
        bytes[i++] = (byte)((StartOfCentralDirectory & 0xff0000) >> 16);
        bytes[i++] = (byte)((StartOfCentralDirectory & 0xff000000u) >> 24);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3293);
      if ((comment == null) || (comment.Length == 0)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3294);
        bytes[i++] = (byte)0;
        bytes[i++] = (byte)0;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(3295);
        if (commentLength + i + 2 > bytes.Length) {
          IACSharpSensor.IACSharpSensor.SensorReached(3296);
          commentLength = (Int16)(bytes.Length - i - 2);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3297);
        bytes[i++] = (byte)(commentLength & 0xff);
        bytes[i++] = (byte)((commentLength & 0xff00) >> 8);
        IACSharpSensor.IACSharpSensor.SensorReached(3298);
        if (commentLength != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3299);
          for (j = 0; (j < commentLength) && (i + j < bytes.Length); j++) {
            IACSharpSensor.IACSharpSensor.SensorReached(3300);
            bytes[i + j] = block[j];
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3301);
          i += j;
        }
      }
      System.Byte[] RNTRNTRNT_445 = bytes;
      IACSharpSensor.IACSharpSensor.SensorReached(3302);
      return RNTRNTRNT_445;
    }
    private static byte[] GenZip64EndOfCentralDirectory(long StartOfCentralDirectory, long EndOfCentralDirectory, int entryCount, uint numSegments)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3303);
      const int bufferLength = 12 + 44 + 20;
      byte[] bytes = new byte[bufferLength];
      int i = 0;
      byte[] sig = BitConverter.GetBytes(ZipConstants.Zip64EndOfCentralDirectoryRecordSignature);
      Array.Copy(sig, 0, bytes, i, 4);
      i += 4;
      long DataSize = 44;
      Array.Copy(BitConverter.GetBytes(DataSize), 0, bytes, i, 8);
      i += 8;
      bytes[i++] = 45;
      bytes[i++] = 0x0;
      bytes[i++] = 45;
      bytes[i++] = 0x0;
      IACSharpSensor.IACSharpSensor.SensorReached(3304);
      for (int j = 0; j < 8; j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3305);
        bytes[i++] = 0x0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3306);
      long numberOfEntries = entryCount;
      Array.Copy(BitConverter.GetBytes(numberOfEntries), 0, bytes, i, 8);
      i += 8;
      Array.Copy(BitConverter.GetBytes(numberOfEntries), 0, bytes, i, 8);
      i += 8;
      Int64 SizeofCentraldirectory = EndOfCentralDirectory - StartOfCentralDirectory;
      Array.Copy(BitConverter.GetBytes(SizeofCentraldirectory), 0, bytes, i, 8);
      i += 8;
      Array.Copy(BitConverter.GetBytes(StartOfCentralDirectory), 0, bytes, i, 8);
      i += 8;
      sig = BitConverter.GetBytes(ZipConstants.Zip64EndOfCentralDirectoryLocatorSignature);
      Array.Copy(sig, 0, bytes, i, 4);
      i += 4;
      uint x2 = (numSegments == 0) ? 0 : (uint)(numSegments - 1);
      Array.Copy(BitConverter.GetBytes(x2), 0, bytes, i, 4);
      i += 4;
      Array.Copy(BitConverter.GetBytes(EndOfCentralDirectory), 0, bytes, i, 8);
      i += 8;
      Array.Copy(BitConverter.GetBytes(numSegments), 0, bytes, i, 4);
      i += 4;
      System.Byte[] RNTRNTRNT_446 = bytes;
      IACSharpSensor.IACSharpSensor.SensorReached(3307);
      return RNTRNTRNT_446;
    }
    private static int CountEntries(ICollection<ZipEntry> _entries)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3308);
      int count = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3309);
      foreach (var entry in _entries) {
        IACSharpSensor.IACSharpSensor.SensorReached(3310);
        if (entry.IncludedInMostRecentSave) {
          IACSharpSensor.IACSharpSensor.SensorReached(3311);
          count++;
        }
      }
      System.Int32 RNTRNTRNT_447 = count;
      IACSharpSensor.IACSharpSensor.SensorReached(3312);
      return RNTRNTRNT_447;
    }
  }
}
