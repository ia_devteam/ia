namespace GotoStatement
{
  public class GotoStatement
  {
    GotoStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      int a = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      goto label2;
      label1:
      IACSharpSensor.IACSharpSensor.SensorReached(3);
      a++;
      IACSharpSensor.IACSharpSensor.SensorReached(4);
      if (a < 2) {
        IACSharpSensor.IACSharpSensor.SensorReached(5);
        goto label3;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6);
      a--;
      label2:
      a++;
      IACSharpSensor.IACSharpSensor.SensorReached(7);
      if (a < 2) {
        IACSharpSensor.IACSharpSensor.SensorReached(8);
        goto label1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(9);
      a--;
      label3:
      a++;
      IACSharpSensor.IACSharpSensor.SensorReached(10);
    }
  }
}
