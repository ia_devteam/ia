namespace WhileDoStatement
{
  public class WhileDoStatement
  {
    WhileDoStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      int a = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      while (a < 10) {
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        a++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4);
    }
  }
}
