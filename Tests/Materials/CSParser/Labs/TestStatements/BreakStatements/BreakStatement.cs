namespace TestStatements
{
  public class BreakStatement
  {
    BreakStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      int[] arr = {
        0,
        1,
        2
      };
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      foreach (int i in arr) {
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        int a = arr[i];
        IACSharpSensor.IACSharpSensor.SensorReached(4);
        break;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5);
    }
  }
}
