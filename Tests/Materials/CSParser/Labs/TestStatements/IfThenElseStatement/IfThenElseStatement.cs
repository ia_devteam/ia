namespace IfThenElseStatement
{
  public class IfThenElseStatement
  {
    IfThenElseStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      int a = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      if (a != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        a++;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4);
        a--;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5);
    }
  }
}
