namespace DoWhileStatement
{
  public class DoWhileStatement
  {
    DoWhileStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      int a = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        a++;
      } while (a < 10);
      IACSharpSensor.IACSharpSensor.SensorReached(4);
    }
  }
}
