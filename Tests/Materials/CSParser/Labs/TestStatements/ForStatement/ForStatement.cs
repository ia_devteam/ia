namespace ForStatement
{
  public class ForStatement
  {
    ForStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      int[] a = new int[10];
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      for (int i = 0; i < 10; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        a[i] = i;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4);
    }
  }
}
