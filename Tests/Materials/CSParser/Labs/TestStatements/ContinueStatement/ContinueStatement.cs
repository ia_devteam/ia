namespace ContinueStatement
{
  public class ContinueStatement
  {
    ContinueStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      for (int i = 0; i < 10; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(2);
        int a = i;
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        continue;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4);
    }
  }
}
