using System;
namespace ThrowStatement
{
  public class ThrowStatement
  {
    ThrowStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      string a = AppDomain.CurrentDomain.BaseDirectory;
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      throw new NotImplementedException();
    }
  }
}
