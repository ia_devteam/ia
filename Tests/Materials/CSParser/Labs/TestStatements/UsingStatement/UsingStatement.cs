using System.IO;
namespace UsingStatement
{
  public class UsingStatement
  {
    UsingStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      Stream stream = FileStream.Null;
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      using (StreamReader reader = new StreamReader(stream)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        stream = reader.BaseStream;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4);
    }
  }
}
