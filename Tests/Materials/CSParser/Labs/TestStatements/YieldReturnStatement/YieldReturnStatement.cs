using System.Collections.Generic;
namespace YieldReturnStatement
{
  public class YieldReturnStatement
  {
    IEnumerable<int> StatementYieldReturn()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      int a = 0;
      System.Int32 RNTRNTRNT_1 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      yield return RNTRNTRNT_1;
      IACSharpSensor.IACSharpSensor.SensorReached(3);
      a++;
      System.Int32 RNTRNTRNT_2 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(4);
      yield return RNTRNTRNT_2;
      IACSharpSensor.IACSharpSensor.SensorReached(5);
      a++;
      System.Int32 RNTRNTRNT_3 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(6);
      yield return RNTRNTRNT_3;
      IACSharpSensor.IACSharpSensor.SensorReached(7);
      IACSharpSensor.IACSharpSensor.SensorReached(8);
    }
  }
}
