namespace SwitchStatement
{
  public class SwitchStatement
  {
    SwitchStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      int a = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      switch (a) {
        case 0:
          
          {
            IACSharpSensor.IACSharpSensor.SensorReached(3);
            a = 10;
            IACSharpSensor.IACSharpSensor.SensorReached(4);
            break;
          }

        case 1:
          IACSharpSensor.IACSharpSensor.SensorReached(5);
          a = 11;
          IACSharpSensor.IACSharpSensor.SensorReached(6);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(7);
          a = 12;
          IACSharpSensor.IACSharpSensor.SensorReached(8);
          break;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(9);
    }
  }
}
