namespace ForeachStatement
{
  public class ForeachStatement
  {
    ForeachStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      int[] arr = {
        0,
        1,
        2
      };
      int[] a = new int[arr.Length];
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      foreach (int i in arr) {
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        a[i] = arr[i];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4);
    }
  }
}
