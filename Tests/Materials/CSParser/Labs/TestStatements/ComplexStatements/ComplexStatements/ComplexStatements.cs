using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ComplexStatements
{
  public class ComplexStatements
  {
    public ComplexStatements()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      List<int> list = new List<int>();
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      for (int i = 0; i < 10; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        list.Add(i);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4);
      int j = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5);
      while (list.Count != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6);
        if (list[j] % 2 == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(7);
          list.Remove(j);
          j++;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(8);
          try {
            IACSharpSensor.IACSharpSensor.SensorReached(9);
            foreach (int key in list) {
              IACSharpSensor.IACSharpSensor.SensorReached(10);
              switch (key) {
                case 2:
                case 4:
                case 6:
                  
                  {
                    IACSharpSensor.IACSharpSensor.SensorReached(11);
                    j++;
                    IACSharpSensor.IACSharpSensor.SensorReached(12);
                    break;
                  }

                case 0:
                case 8:
                  
                  {
                    IACSharpSensor.IACSharpSensor.SensorReached(13);
                    j--;
                    IACSharpSensor.IACSharpSensor.SensorReached(14);
                    break;
                  }

                default:
                  
                  {
                    IACSharpSensor.IACSharpSensor.SensorReached(15);
                    break;
                  }

              }
            }
          } catch {
            IACSharpSensor.IACSharpSensor.SensorReached(16);
            throw new Exception("Что-то пошло не так!");
            IACSharpSensor.IACSharpSensor.SensorReached(17);
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(18);
    }
  }
}
