namespace IfThenStatement
{
  public class IfThenStatement
  {
    IfThenStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      int a = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      if (a == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        a++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4);
    }
  }
}
