using System;
namespace IfThenStatement
{
  class Test
  {
    public Test()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5);
      int a = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(6);
      if (a != 0xffff) {
        IACSharpSensor.IACSharpSensor.SensorReached(7);
        throw new Exception("");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(8);
    }
  }
}
