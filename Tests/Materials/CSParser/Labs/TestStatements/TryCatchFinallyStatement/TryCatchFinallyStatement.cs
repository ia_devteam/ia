using System;
namespace TryCatchFinallyStatement
{
  public class TryCatchFinallyStatement
  {
    TryCatchFinallyStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      int a = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        a = 1;
      } catch (NullReferenceException) {
        IACSharpSensor.IACSharpSensor.SensorReached(4);
        a = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(5);
      } catch (OverflowException) {
        IACSharpSensor.IACSharpSensor.SensorReached(6);
        a = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(7);
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(8);
        a = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(9);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(10);
    }
  }
  public class TryCatchStatement
  {
    TryCatchStatement()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(11);
      int a = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(12);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(13);
        if (a == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(14);
          throw new Exception("");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(15);
        a++;
      } catch (Exception) {
        IACSharpSensor.IACSharpSensor.SensorReached(16);
        a++;
        IACSharpSensor.IACSharpSensor.SensorReached(17);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(18);
    }
  }
}
