using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace WindowsFormsApplication2
{
  static class Program
  {
    [STAThread()]
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(65);
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new Form1());
      IACSharpSensor.IACSharpSensor.SensorReached(66);
    }
  }
}
