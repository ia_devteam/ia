using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.IO;
namespace FindStringsInFiles
{
  class Program
  {
    class LinePosition
    {
      internal string File;
      internal List<ulong> Line = new List<ulong>();
    }
    [STAThreadAttribute()]
    static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      OpenFileDialog ofd = new OpenFileDialog { InitialDirectory = "d:\\123\\Work\\" };
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      while (ofd.ShowDialog() != DialogResult.OK) {
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        Console.WriteLine("\nНе выбран файл. Выйти? (y|n)");
        IACSharpSensor.IACSharpSensor.SensorReached(4);
        if (Console.ReadKey().KeyChar == 'y') {
          IACSharpSensor.IACSharpSensor.SensorReached(5);
          return;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6);
      FolderBrowserDialog fbd = new FolderBrowserDialog { SelectedPath = "d:\\123\\Work\\" };
      IACSharpSensor.IACSharpSensor.SensorReached(7);
      while (fbd.ShowDialog() != DialogResult.OK) {
        IACSharpSensor.IACSharpSensor.SensorReached(8);
        Console.WriteLine("\nНе выбрана директория. Выйти? (y|n)");
        IACSharpSensor.IACSharpSensor.SensorReached(9);
        if (Console.ReadKey().KeyChar == 'y') {
          IACSharpSensor.IACSharpSensor.SensorReached(10);
          return;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(11);
      ulong count = (ulong)Directory.EnumerateFiles(fbd.SelectedPath, "*.*", SearchOption.AllDirectories).Count();
      Console.WriteLine("\nПоиск...");
      Dictionary<string, List<LinePosition>> dict = new Dictionary<string, List<LinePosition>>();
      string reportPath = Path.Combine(Path.GetDirectoryName(ofd.FileName), "Reports");
      IACSharpSensor.IACSharpSensor.SensorReached(12);
      if (!Directory.Exists(reportPath)) {
        IACSharpSensor.IACSharpSensor.SensorReached(13);
        Directory.CreateDirectory(reportPath);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(14);
        DirectoryInfo di = new DirectoryInfo(reportPath);
        IACSharpSensor.IACSharpSensor.SensorReached(15);
        foreach (FileInfo file in di.GetFiles()) {
          IACSharpSensor.IACSharpSensor.SensorReached(16);
          file.Delete();
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(17);
      using (StreamReader reader = new StreamReader(ofd.FileName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(18);
        string signature;
        IACSharpSensor.IACSharpSensor.SensorReached(19);
        while ((signature = reader.ReadLine()) != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(20);
          Console.WriteLine("\n{0}...", signature);
          List<LinePosition> curSig = new List<LinePosition>();
          ulong curCount = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(21);
          foreach (string file in Directory.GetFiles(fbd.SelectedPath, "*.*", SearchOption.AllDirectories)) {
            IACSharpSensor.IACSharpSensor.SensorReached(22);
            curCount++;
            Console.Write(String.Format("\r{0,1:P3}", curCount / (double)count));
            LinePosition curFile = new LinePosition();
            curFile.File = file;
            IACSharpSensor.IACSharpSensor.SensorReached(23);
            using (StreamReader currentFile = new StreamReader(file)) {
              IACSharpSensor.IACSharpSensor.SensorReached(24);
              ulong counter = 0;
              string line;
              IACSharpSensor.IACSharpSensor.SensorReached(25);
              while ((line = currentFile.ReadLine()) != null) {
                IACSharpSensor.IACSharpSensor.SensorReached(26);
                counter++;
                IACSharpSensor.IACSharpSensor.SensorReached(27);
                if (line.IndexOf(signature) != -1) {
                  IACSharpSensor.IACSharpSensor.SensorReached(28);
                  curFile.Line.Add(counter);
                }
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(29);
            curSig.Add(curFile);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(30);
          dict.Add(signature, curSig);
          WriteReport(reportPath, signature, curSig);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(31);
    }
    private static void WriteReport(string reportPath, string signature, List<LinePosition> curSig)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(32);
      using (StreamWriter writer = new StreamWriter(Path.Combine(reportPath, signature + ".txt"))) {
        IACSharpSensor.IACSharpSensor.SensorReached(33);
        foreach (LinePosition lp in curSig) {
          IACSharpSensor.IACSharpSensor.SensorReached(34);
          foreach (ulong line in lp.Line) {
            IACSharpSensor.IACSharpSensor.SensorReached(35);
            writer.WriteLine(signature + "\t" + lp.File + "\t" + line);
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(36);
    }
  }
}
