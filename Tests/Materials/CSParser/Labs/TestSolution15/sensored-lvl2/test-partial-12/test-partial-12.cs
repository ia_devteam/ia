using System;
namespace Test1
{
  public partial class Foo
  {
    static internal System.Collections.IEnumerable E()
    {
      System.Collections.IEnumerable RNTRNTRNT_169 = "a";
      IACSharpSensor.IACSharpSensor.SensorReached(440);
      yield return RNTRNTRNT_169;
      IACSharpSensor.IACSharpSensor.SensorReached(441);
      IACSharpSensor.IACSharpSensor.SensorReached(442);
    }
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(443);
    foreach (string s in Test1.Foo.E()) {
      IACSharpSensor.IACSharpSensor.SensorReached(444);
      Console.WriteLine(s);
      IACSharpSensor.IACSharpSensor.SensorReached(445);
      if (s != "a") {
        System.Int32 RNTRNTRNT_170 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(446);
        return RNTRNTRNT_170;
      }
      System.Int32 RNTRNTRNT_171 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(447);
      return RNTRNTRNT_171;
    }
    System.Int32 RNTRNTRNT_172 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(448);
    return RNTRNTRNT_172;
  }
}
