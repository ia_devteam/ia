using System;
using System.Collections;
class X
{
  public event EventHandler Hook;
  public IEnumerator Pipeline()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(358);
    if (Hook == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(359);
      throw new Exception("error");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(360);
    Hook(this, EventArgs.Empty);
    Object RNTRNTRNT_135 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(361);
    yield return RNTRNTRNT_135;
    IACSharpSensor.IACSharpSensor.SensorReached(362);
    IACSharpSensor.IACSharpSensor.SensorReached(363);
  }
  static void M(object sender, EventArgs args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(364);
    Console.WriteLine("Hook invoked");
    IACSharpSensor.IACSharpSensor.SensorReached(365);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(366);
    X x = new X();
    x.Hook += M;
    IEnumerator y = x.Pipeline();
    y.MoveNext();
    IACSharpSensor.IACSharpSensor.SensorReached(367);
  }
}
