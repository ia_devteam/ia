public interface IExecutable
{
  void Execute();
  object Current { get; }
}
public class A : IExecutable
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(596);
  }
  public void Execute()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(597);
  }
  public object Current {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(598);
      return null;
    }
  }
}
