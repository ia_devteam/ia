public class A
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(600);
  }
  public virtual string Level {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(601);
      return null;
    }
  }
  public virtual void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(602);
  }
}
public class B : A
{
  public override string Level {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(603);
      return null;
    }
  }
  public override void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(604);
  }
}
