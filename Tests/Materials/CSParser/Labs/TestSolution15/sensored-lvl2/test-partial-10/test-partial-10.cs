using System;
namespace Test2
{
  public interface Base
  {
  }
  public partial class Foo : Base
  {
    public static int f = 10;
  }
  public partial class Foo : Base
  {
    public static int f2 = 9;
  }
}
namespace Test3
{
  public interface Base
  {
  }
  public partial struct Foo : Base
  {
    public static int f = 10;
  }
  public partial struct Foo : Base
  {
    public static int f2 = 9;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(415);
    if (Test2.Foo.f != 10) {
      System.Int32 RNTRNTRNT_157 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(416);
      return RNTRNTRNT_157;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(417);
    if (Test2.Foo.f2 != 9) {
      System.Int32 RNTRNTRNT_158 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(418);
      return RNTRNTRNT_158;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(419);
    if (Test3.Foo.f != 10) {
      System.Int32 RNTRNTRNT_159 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(420);
      return RNTRNTRNT_159;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(421);
    if (Test3.Foo.f2 != 9) {
      System.Int32 RNTRNTRNT_160 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(422);
      return RNTRNTRNT_160;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(423);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_161 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(424);
    return RNTRNTRNT_161;
  }
}
