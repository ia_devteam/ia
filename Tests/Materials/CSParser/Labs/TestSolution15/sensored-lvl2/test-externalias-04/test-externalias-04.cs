extern alias MyAssembly01;
extern alias MyAssembly02;
using System;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(54);
    if (MyAssembly01.Namespace1.Namespace2.MyClass2.StaticMethod() != 1) {
      System.Int32 RNTRNTRNT_36 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(55);
      return RNTRNTRNT_36;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(56);
    if (MyAssembly02.Namespace1.Namespace2.MyClass2.StaticMethod() != 2) {
      System.Int32 RNTRNTRNT_37 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(57);
      return RNTRNTRNT_37;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(58);
    if (new MyAssembly01.Namespace1.Namespace2.MyClass2().InstanceMethod() != 1) {
      System.Int32 RNTRNTRNT_38 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(59);
      return RNTRNTRNT_38;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(60);
    if (new MyAssembly02.Namespace1.Namespace2.MyClass2().InstanceMethod() != 2) {
      System.Int32 RNTRNTRNT_39 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(61);
      return RNTRNTRNT_39;
    }
    System.Int32 RNTRNTRNT_40 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    return RNTRNTRNT_40;
  }
}
