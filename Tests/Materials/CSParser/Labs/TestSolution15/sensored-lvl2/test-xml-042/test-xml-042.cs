using System;
namespace TestNamespace
{
  class TestClass
  {
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(581);
    }
    public void FunctionWithParameter(ref int number, out int num2)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(582);
      num2 = 0;
      number = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(583);
    }
  }
}
