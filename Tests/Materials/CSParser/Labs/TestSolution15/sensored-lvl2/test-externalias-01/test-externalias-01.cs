extern alias MyAssembly01;
extern alias MyAssembly02;
using System;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(14);
    if (MyAssembly01.GlobalClass.StaticMethod() != 1) {
      System.Int32 RNTRNTRNT_11 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(15);
      return RNTRNTRNT_11;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    if (MyAssembly02.GlobalClass.StaticMethod() != 2) {
      System.Int32 RNTRNTRNT_12 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(17);
      return RNTRNTRNT_12;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(18);
    if (new MyAssembly01.GlobalClass().InstanceMethod() != 1) {
      System.Int32 RNTRNTRNT_13 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(19);
      return RNTRNTRNT_13;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(20);
    if (new MyAssembly02.GlobalClass().InstanceMethod() != 2) {
      System.Int32 RNTRNTRNT_14 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(21);
      return RNTRNTRNT_14;
    }
    System.Int32 RNTRNTRNT_15 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(22);
    return RNTRNTRNT_15;
  }
}
