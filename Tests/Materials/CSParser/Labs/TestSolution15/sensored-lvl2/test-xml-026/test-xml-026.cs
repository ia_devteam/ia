namespace Testing
{
  public class Test
  {
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(548);
    }
    public void Foo(int i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(549);
    }
    public void Bar(int i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(550);
    }
    public void Baz(int i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(551);
    }
    public void Var(params int[] arr)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(552);
    }
  }
}
