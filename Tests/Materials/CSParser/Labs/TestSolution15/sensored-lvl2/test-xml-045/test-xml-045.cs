public class EntryPoint
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(589);
  }
  protected void Create(bool test)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(590);
    Define(true);
    IACSharpSensor.IACSharpSensor.SensorReached(591);
  }
  private void Define(bool test)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(592);
  }
  protected void Undefine(bool test)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(593);
  }
  protected void Remove()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(594);
  }
  public virtual void Destroy(bool test)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(595);
  }
}
