using System;
namespace Testing
{
  public class Test
  {
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(513);
    }
    public static void Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(514);
    }
    public static void Foo(long l, Test t, System.Collections.ArrayList al)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(515);
    }
    public static void Foo(params string[] param)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(516);
    }
  }
}
