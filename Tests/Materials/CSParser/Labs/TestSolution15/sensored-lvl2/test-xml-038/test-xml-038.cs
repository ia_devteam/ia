using System;
using System.Reflection;
using System.Xml;
public class Whatever
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(573);
    foreach (MemberInfo mi in typeof(XmlDocument).FindMembers(MemberTypes.All, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance, Type.FilterName, "NodeInserted")) {
      IACSharpSensor.IACSharpSensor.SensorReached(574);
      Console.WriteLine(mi.GetType());
    }
    IACSharpSensor.IACSharpSensor.SensorReached(575);
  }
}
