class X
{
  System.Collections.IEnumerable a()
  {
    lock (this) {
      System.Collections.IEnumerable RNTRNTRNT_136 = "a";
      IACSharpSensor.IACSharpSensor.SensorReached(368);
      yield return RNTRNTRNT_136;
      IACSharpSensor.IACSharpSensor.SensorReached(369);
      System.Collections.IEnumerable RNTRNTRNT_137 = "b";
      IACSharpSensor.IACSharpSensor.SensorReached(370);
      yield return RNTRNTRNT_137;
      IACSharpSensor.IACSharpSensor.SensorReached(371);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(372);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(373);
  }
}
