using System;
using System.Collections;
public class Foo : IDisposable
{
  public readonly int Data;
  public Foo(int data)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(268);
    this.Data = data;
    IACSharpSensor.IACSharpSensor.SensorReached(269);
  }
  public bool disposed;
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(270);
    disposed = true;
    IACSharpSensor.IACSharpSensor.SensorReached(271);
  }
}
class X
{
  public static IEnumerable Test(int a, int b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(272);
    Foo foo3, foo4;
    IACSharpSensor.IACSharpSensor.SensorReached(273);
    using (Foo foo1 = new Foo(a), foo2 = new Foo(b)) {
      IEnumerable RNTRNTRNT_108 = foo1.Data;
      IACSharpSensor.IACSharpSensor.SensorReached(274);
      yield return RNTRNTRNT_108;
      IACSharpSensor.IACSharpSensor.SensorReached(275);
      IEnumerable RNTRNTRNT_109 = foo2.Data;
      IACSharpSensor.IACSharpSensor.SensorReached(276);
      yield return RNTRNTRNT_109;
      IACSharpSensor.IACSharpSensor.SensorReached(277);
      foo3 = foo1;
      foo4 = foo2;
    }
    IEnumerable RNTRNTRNT_110 = foo3.disposed;
    IACSharpSensor.IACSharpSensor.SensorReached(278);
    yield return RNTRNTRNT_110;
    IACSharpSensor.IACSharpSensor.SensorReached(279);
    IEnumerable RNTRNTRNT_111 = foo4.disposed;
    IACSharpSensor.IACSharpSensor.SensorReached(280);
    yield return RNTRNTRNT_111;
    IACSharpSensor.IACSharpSensor.SensorReached(281);
    IACSharpSensor.IACSharpSensor.SensorReached(282);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    ArrayList list = new ArrayList();
    IACSharpSensor.IACSharpSensor.SensorReached(284);
    foreach (object data in Test(3, 5)) {
      IACSharpSensor.IACSharpSensor.SensorReached(285);
      list.Add(data);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    if (list.Count != 4) {
      System.Int32 RNTRNTRNT_112 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(287);
      return RNTRNTRNT_112;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(288);
    if ((int)list[0] != 3) {
      System.Int32 RNTRNTRNT_113 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(289);
      return RNTRNTRNT_113;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(290);
    if ((int)list[1] != 5) {
      System.Int32 RNTRNTRNT_114 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(291);
      return RNTRNTRNT_114;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(292);
    if (!(bool)list[2]) {
      System.Int32 RNTRNTRNT_115 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(293);
      return RNTRNTRNT_115;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(294);
    if (!(bool)list[3]) {
      System.Int32 RNTRNTRNT_116 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(295);
      return RNTRNTRNT_116;
    }
    System.Int32 RNTRNTRNT_117 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(296);
    return RNTRNTRNT_117;
  }
}
