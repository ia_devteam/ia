public partial class Foo
{
  public string myId;
}
public abstract partial class Foo
{
  public string Id {
    get {
      System.String RNTRNTRNT_156 = myId;
      IACSharpSensor.IACSharpSensor.SensorReached(410);
      return RNTRNTRNT_156;
    }
  }
}
public class Bar : Foo
{
  public Bar(string id)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(411);
    myId = id;
    IACSharpSensor.IACSharpSensor.SensorReached(412);
  }
}
public class PartialAbstractCompilationError
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    System.Console.WriteLine(typeof(Foo).IsAbstract);
    IACSharpSensor.IACSharpSensor.SensorReached(414);
  }
}
