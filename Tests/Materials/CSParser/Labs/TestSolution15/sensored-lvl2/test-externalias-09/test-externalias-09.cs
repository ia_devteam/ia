extern alias MyAssembly01;
using System;
using SameNamespace = MyAssembly01;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(79);
    SameNamespace.GlobalClass.StaticMethod();
    System.Int32 RNTRNTRNT_49 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(80);
    return RNTRNTRNT_49;
  }
}
