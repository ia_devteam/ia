using System;
using System.Collections;
struct S
{
  int j;
  public IEnumerable Get(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(168);
    Console.WriteLine("Sending: " + a);
    IEnumerable RNTRNTRNT_76 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(169);
    yield return RNTRNTRNT_76;
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    j = 10;
    Console.WriteLine("Sending: " + j);
    IEnumerable RNTRNTRNT_77 = j;
    IACSharpSensor.IACSharpSensor.SensorReached(171);
    yield return RNTRNTRNT_77;
    IACSharpSensor.IACSharpSensor.SensorReached(172);
    IACSharpSensor.IACSharpSensor.SensorReached(173);
  }
  public static IEnumerable GetS(int a)
  {
    IEnumerable RNTRNTRNT_78 = 100;
    IACSharpSensor.IACSharpSensor.SensorReached(174);
    yield return RNTRNTRNT_78;
    IACSharpSensor.IACSharpSensor.SensorReached(175);
    IEnumerable RNTRNTRNT_79 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(176);
    yield return RNTRNTRNT_79;
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    IEnumerable RNTRNTRNT_80 = 1000;
    IACSharpSensor.IACSharpSensor.SensorReached(178);
    yield return RNTRNTRNT_80;
    IACSharpSensor.IACSharpSensor.SensorReached(179);
    IACSharpSensor.IACSharpSensor.SensorReached(180);
  }
}
class X
{
  IEnumerable Get(int a)
  {
    IEnumerable RNTRNTRNT_81 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(181);
    yield return RNTRNTRNT_81;
    IACSharpSensor.IACSharpSensor.SensorReached(182);
    IEnumerable RNTRNTRNT_82 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(183);
    yield return RNTRNTRNT_82;
    IACSharpSensor.IACSharpSensor.SensorReached(184);
    IEnumerable RNTRNTRNT_83 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(185);
    yield return RNTRNTRNT_83;
    IACSharpSensor.IACSharpSensor.SensorReached(186);
    IACSharpSensor.IACSharpSensor.SensorReached(187);
  }
  static IEnumerable GetS(int a)
  {
    IEnumerable RNTRNTRNT_84 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(188);
    yield return RNTRNTRNT_84;
    IACSharpSensor.IACSharpSensor.SensorReached(189);
    IEnumerable RNTRNTRNT_85 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(190);
    yield return RNTRNTRNT_85;
    IACSharpSensor.IACSharpSensor.SensorReached(191);
    IEnumerable RNTRNTRNT_86 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(192);
    yield return RNTRNTRNT_86;
    IACSharpSensor.IACSharpSensor.SensorReached(193);
    IACSharpSensor.IACSharpSensor.SensorReached(194);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(195);
    X y = new X();
    int total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(196);
    foreach (int x in y.Get(5)) {
      IACSharpSensor.IACSharpSensor.SensorReached(197);
      total += x;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(198);
    if (total != 8) {
      System.Int32 RNTRNTRNT_87 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(199);
      return RNTRNTRNT_87;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(200);
    total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(201);
    foreach (int x in GetS(3)) {
      IACSharpSensor.IACSharpSensor.SensorReached(202);
      total += x;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    if (total != 7) {
      System.Int32 RNTRNTRNT_88 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(204);
      return RNTRNTRNT_88;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(205);
    S s = new S();
    total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    foreach (int x in s.Get(100)) {
      IACSharpSensor.IACSharpSensor.SensorReached(207);
      Console.WriteLine("Got: " + x);
      total += x;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(208);
    if (total != 110) {
      System.Int32 RNTRNTRNT_89 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(209);
      return RNTRNTRNT_89;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(210);
    total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(211);
    foreach (int x in S.GetS(1)) {
      IACSharpSensor.IACSharpSensor.SensorReached(212);
      total += x;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(213);
    if (total != 1101) {
      System.Int32 RNTRNTRNT_90 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(214);
      return RNTRNTRNT_90;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_91 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(216);
    return RNTRNTRNT_91;
  }
}
