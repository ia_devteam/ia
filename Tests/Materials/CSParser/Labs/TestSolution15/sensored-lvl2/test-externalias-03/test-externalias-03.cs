extern alias MyAssembly01;
extern alias MyAssembly02;
using System;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(45);
    if (MyAssembly01.Namespace1.MyClass1.MyNestedClass1.StaticMethod() != 1) {
      System.Int32 RNTRNTRNT_31 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(46);
      return RNTRNTRNT_31;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(47);
    if (MyAssembly02.Namespace1.MyClass1.MyNestedClass1.StaticMethod() != 2) {
      System.Int32 RNTRNTRNT_32 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(48);
      return RNTRNTRNT_32;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(49);
    if (new MyAssembly01.Namespace1.MyClass1.MyNestedClass1().InstanceMethod() != 1) {
      System.Int32 RNTRNTRNT_33 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(50);
      return RNTRNTRNT_33;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(51);
    if (new MyAssembly02.Namespace1.MyClass1.MyNestedClass1().InstanceMethod() != 2) {
      System.Int32 RNTRNTRNT_34 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(52);
      return RNTRNTRNT_34;
    }
    System.Int32 RNTRNTRNT_35 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    return RNTRNTRNT_35;
  }
}
