using System;
using System.Collections;
public class Test
{
  public IEnumerable Foo(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(217);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(218);
      try {
        IEnumerable RNTRNTRNT_92 = a;
        IACSharpSensor.IACSharpSensor.SensorReached(219);
        yield return RNTRNTRNT_92;
        IACSharpSensor.IACSharpSensor.SensorReached(220);
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(221);
        Console.WriteLine("Hello World");
        IACSharpSensor.IACSharpSensor.SensorReached(222);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(223);
      Console.WriteLine("Next block");
      IACSharpSensor.IACSharpSensor.SensorReached(224);
      try {
        IEnumerable RNTRNTRNT_93 = a * a;
        IACSharpSensor.IACSharpSensor.SensorReached(225);
        yield return RNTRNTRNT_93;
        IACSharpSensor.IACSharpSensor.SensorReached(226);
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(227);
        Console.WriteLine("Boston");
        IACSharpSensor.IACSharpSensor.SensorReached(228);
      }
    } finally {
      IACSharpSensor.IACSharpSensor.SensorReached(229);
      Console.WriteLine("Outer finally");
      IACSharpSensor.IACSharpSensor.SensorReached(230);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(231);
    Console.WriteLine("Outer block");
    IACSharpSensor.IACSharpSensor.SensorReached(232);
    yield break;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(233);
    Test test = new Test();
    ArrayList list = new ArrayList();
    IACSharpSensor.IACSharpSensor.SensorReached(234);
    foreach (object o in test.Foo(5)) {
      IACSharpSensor.IACSharpSensor.SensorReached(235);
      list.Add(o);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(236);
    if (list.Count != 2) {
      System.Int32 RNTRNTRNT_94 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(237);
      return RNTRNTRNT_94;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    if ((int)list[0] != 5) {
      System.Int32 RNTRNTRNT_95 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(239);
      return RNTRNTRNT_95;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(240);
    if ((int)list[1] != 25) {
      System.Int32 RNTRNTRNT_96 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(241);
      return RNTRNTRNT_96;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(242);
    IEnumerable a = test.Foo(5);
    IEnumerator b = a as IEnumerator;
    IACSharpSensor.IACSharpSensor.SensorReached(243);
    if (b != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(244);
      if (b.MoveNext()) {
        System.Int32 RNTRNTRNT_97 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(245);
        return RNTRNTRNT_97;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(246);
    IEnumerator c = a.GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(247);
    if (!c.MoveNext()) {
      System.Int32 RNTRNTRNT_98 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(248);
      return RNTRNTRNT_98;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(249);
    if ((int)c.Current != 5) {
      System.Int32 RNTRNTRNT_99 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(250);
      return RNTRNTRNT_99;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(251);
    if (!c.MoveNext()) {
      System.Int32 RNTRNTRNT_100 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(252);
      return RNTRNTRNT_100;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(253);
    if ((int)c.Current != 25) {
      System.Int32 RNTRNTRNT_101 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(254);
      return RNTRNTRNT_101;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(255);
    IEnumerator d = a.GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(256);
    if ((int)c.Current != 25) {
      System.Int32 RNTRNTRNT_102 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(257);
      return RNTRNTRNT_102;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(258);
    if (!d.MoveNext()) {
      System.Int32 RNTRNTRNT_103 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(259);
      return RNTRNTRNT_103;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(260);
    if ((int)c.Current != 25) {
      System.Int32 RNTRNTRNT_104 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(261);
      return RNTRNTRNT_104;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(262);
    if ((int)d.Current != 5) {
      System.Int32 RNTRNTRNT_105 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(263);
      return RNTRNTRNT_105;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(264);
    if (c.MoveNext()) {
      System.Int32 RNTRNTRNT_106 = 13;
      IACSharpSensor.IACSharpSensor.SensorReached(265);
      return RNTRNTRNT_106;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(266);
    ((IDisposable)a).Dispose();
    System.Int32 RNTRNTRNT_107 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(267);
    return RNTRNTRNT_107;
  }
}
