extern alias MyAssembly01;
extern alias MyAssembly02;
using System;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(36);
    if (MyAssembly01.Namespace1.MyClass1.StaticMethod() != 1) {
      System.Int32 RNTRNTRNT_26 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(37);
      return RNTRNTRNT_26;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(38);
    if (MyAssembly02.Namespace1.MyClass1.StaticMethod() != 2) {
      System.Int32 RNTRNTRNT_27 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(39);
      return RNTRNTRNT_27;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(40);
    if (new MyAssembly01.Namespace1.MyClass1().InstanceMethod() != 1) {
      System.Int32 RNTRNTRNT_28 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(41);
      return RNTRNTRNT_28;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    if (new MyAssembly02.Namespace1.MyClass1().InstanceMethod() != 2) {
      System.Int32 RNTRNTRNT_29 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(43);
      return RNTRNTRNT_29;
    }
    System.Int32 RNTRNTRNT_30 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(44);
    return RNTRNTRNT_30;
  }
}
