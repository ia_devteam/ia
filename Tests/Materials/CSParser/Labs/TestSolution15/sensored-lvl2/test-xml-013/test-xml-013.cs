using System;
namespace Testing
{
  public class Test
  {
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(500);
    }
    public string PublicProperty {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(501);
        return null;
      }
      set { IACSharpSensor.IACSharpSensor.SensorReached(502); }
    }
    public string PublicProperty2 {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(503);
        return null;
      }
    }
    public string PublicProperty3 {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(504);
        return null;
      }
      set { IACSharpSensor.IACSharpSensor.SensorReached(505); }
    }
  }
}
