namespace Foo
{
  public partial class X
  {
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(455);
    }
  }
}
namespace Foo
{
  using System;
  using System.Collections;
  public partial class X
  {
    public static IEnumerable Attempts2()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(456);
      AttributeTargets t = AttributeTargets.All;
      IACSharpSensor.IACSharpSensor.SensorReached(457);
      yield break;
    }
    public static IEnumerable Attempts {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(458);
        AttributeTargets t = AttributeTargets.All;
        IACSharpSensor.IACSharpSensor.SensorReached(459);
        yield break;
      }
    }
    public IEnumerable this[int i] {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(460);
        AttributeTargets t = AttributeTargets.All;
        IACSharpSensor.IACSharpSensor.SensorReached(461);
        yield break;
      }
    }
  }
}
