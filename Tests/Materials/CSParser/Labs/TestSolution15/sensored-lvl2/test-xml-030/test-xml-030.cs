using System;
class Test
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(560);
  }
  public void foo2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(561);
  }
  public void foo3(string line, params object[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(562);
  }
}
