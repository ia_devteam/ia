using System;
namespace NS
{
  extern alias MyAssembly01;
  public class MyClass
  {
    public static int GetInt()
    {
      System.Int32 RNTRNTRNT_41 = MyAssembly01.GlobalClass.StaticMethod();
      IACSharpSensor.IACSharpSensor.SensorReached(66);
      return RNTRNTRNT_41;
    }
  }
}
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(67);
    if (NS.MyClass.GetInt() != 1) {
      System.Int32 RNTRNTRNT_42 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(68);
      return RNTRNTRNT_42;
    }
    System.Int32 RNTRNTRNT_43 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(69);
    return RNTRNTRNT_43;
  }
}
