public partial class Test
{
  public readonly Foo TheFoo;
  public Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(382);
    this.TheFoo = new Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(383);
  }
  public partial interface IFoo
  {
    int Hello(Test foo);
  }
  public int TestFoo()
  {
    System.Int32 RNTRNTRNT_143 = TheFoo.Hello(this);
    IACSharpSensor.IACSharpSensor.SensorReached(384);
    return RNTRNTRNT_143;
  }
}
public partial class Test
{
  public partial class Foo : IFoo
  {
    int IFoo.Hello(Test test)
    {
      System.Int32 RNTRNTRNT_144 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(385);
      return RNTRNTRNT_144;
    }
    public int Hello(Test test)
    {
      System.Int32 RNTRNTRNT_145 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(386);
      return RNTRNTRNT_145;
    }
  }
  public int TestIFoo(IFoo foo)
  {
    System.Int32 RNTRNTRNT_146 = foo.Hello(this);
    IACSharpSensor.IACSharpSensor.SensorReached(387);
    return RNTRNTRNT_146;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(388);
    Test test = new Test();
    IACSharpSensor.IACSharpSensor.SensorReached(389);
    if (test.TestFoo() != 1) {
      System.Int32 RNTRNTRNT_147 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(390);
      return RNTRNTRNT_147;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(391);
    if (test.TestIFoo(test.TheFoo) != 2) {
      System.Int32 RNTRNTRNT_148 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(392);
      return RNTRNTRNT_148;
    }
    System.Int32 RNTRNTRNT_149 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(393);
    return RNTRNTRNT_149;
  }
}
