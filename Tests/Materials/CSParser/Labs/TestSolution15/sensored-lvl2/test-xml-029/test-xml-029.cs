using System;
class Test1
{
  void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(558);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(559);
  }
}
public interface ITest2
{
  void Foo();
  long Bar { get; }
  event EventHandler EventRaised;
}
