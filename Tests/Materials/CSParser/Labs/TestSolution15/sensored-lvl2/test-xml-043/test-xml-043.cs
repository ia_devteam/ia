public class EntryPoint
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(584);
  }
  private class A
  {
    public virtual void Decide(int a)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(585);
    }
  }
  private class B : A
  {
    public override void Decide(int a)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(586);
    }
  }
}
