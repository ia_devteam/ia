namespace N
{
  partial class Foo
  {
  }
}
namespace N
{
  using System;
  partial class Foo
  {
    public Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(470);
      Console.Write("Hello, world.\n");
      IACSharpSensor.IACSharpSensor.SensorReached(471);
    }
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(472);
    }
  }
}
