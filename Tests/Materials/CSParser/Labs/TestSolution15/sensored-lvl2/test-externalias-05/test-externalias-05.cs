extern alias MyAssembly01;
extern alias MyAssembly02;
using System;
public class Test
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(63);
    MyAssembly01.GlobalClass.JustForFirst();
    MyAssembly02.GlobalClass.JustForSecond();
    MyAssembly01.Namespace1.MyClass1.JustForFirst();
    MyAssembly02.Namespace1.MyClass1.JustForSecond();
    IACSharpSensor.IACSharpSensor.SensorReached(64);
  }
}
