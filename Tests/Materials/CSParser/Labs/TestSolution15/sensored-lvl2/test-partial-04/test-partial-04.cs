namespace A
{
  interface IFoo
  {
    void Hello(IFoo foo);
  }
}
namespace B
{
  partial class Test
  {
  }
}
namespace B
{
  using A;
  partial class Test : IFoo
  {
    void IFoo.Hello(IFoo foo)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(394);
    }
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(395);
  }
}
