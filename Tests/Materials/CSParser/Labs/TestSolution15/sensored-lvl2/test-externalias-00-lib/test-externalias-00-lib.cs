using System;
public class GlobalClass
{
  public int InstanceMethod()
  {
    System.Int32 RNTRNTRNT_1 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1);
    return RNTRNTRNT_1;
  }
  public static int StaticMethod()
  {
    System.Int32 RNTRNTRNT_2 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(2);
    return RNTRNTRNT_2;
  }
  public static void JustForFirst()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(3);
  }
}
namespace Namespace1
{
  public class MyClass1
  {
    public int InstanceMethod()
    {
      System.Int32 RNTRNTRNT_3 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(4);
      return RNTRNTRNT_3;
    }
    public static int StaticMethod()
    {
      System.Int32 RNTRNTRNT_4 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(5);
      return RNTRNTRNT_4;
    }
    public class MyNestedClass1
    {
      public int InstanceMethod()
      {
        System.Int32 RNTRNTRNT_5 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(6);
        return RNTRNTRNT_5;
      }
      public static int StaticMethod()
      {
        System.Int32 RNTRNTRNT_6 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(7);
        return RNTRNTRNT_6;
      }
    }
    public static void JustForFirst()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(8);
    }
  }
  namespace Namespace2
  {
    public class MyClass2
    {
      public class MyNestedClass2
      {
        public int InstanceMethod()
        {
          System.Int32 RNTRNTRNT_7 = 1;
          IACSharpSensor.IACSharpSensor.SensorReached(9);
          return RNTRNTRNT_7;
        }
        public static int StaticMethod()
        {
          System.Int32 RNTRNTRNT_8 = 1;
          IACSharpSensor.IACSharpSensor.SensorReached(10);
          return RNTRNTRNT_8;
        }
      }
      public int InstanceMethod()
      {
        System.Int32 RNTRNTRNT_9 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(11);
        return RNTRNTRNT_9;
      }
      public static int StaticMethod()
      {
        System.Int32 RNTRNTRNT_10 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(12);
        return RNTRNTRNT_10;
      }
      public static void JustForFirst()
      {
        IACSharpSensor.IACSharpSensor.SensorReached(13);
      }
    }
  }
}
