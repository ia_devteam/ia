using System;
using System.Collections;
class X
{
  static IEnumerable GetIt(int[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(120);
    foreach (int a in args) {
      IEnumerable RNTRNTRNT_64 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(121);
      yield return RNTRNTRNT_64;
      IACSharpSensor.IACSharpSensor.SensorReached(122);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(123);
  }
  static IEnumerable GetMulti(int[,] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(124);
    foreach (int a in args) {
      IEnumerable RNTRNTRNT_65 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(125);
      yield return RNTRNTRNT_65;
      IACSharpSensor.IACSharpSensor.SensorReached(126);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(127);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(128);
    int total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(129);
    foreach (int i in GetIt(new int[] {
      1,
      2,
      3
    })) {
      IACSharpSensor.IACSharpSensor.SensorReached(130);
      Console.WriteLine("Got: " + i);
      total += i;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(131);
    if (total != 6) {
      System.Int32 RNTRNTRNT_66 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(132);
      return RNTRNTRNT_66;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(133);
    total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(134);
    foreach (int i in GetMulti(new int[,] {
      {
        10,
        20
      },
      {
        30,
        40
      }
    })) {
      IACSharpSensor.IACSharpSensor.SensorReached(135);
      Console.WriteLine("Got: " + i);
      total += i;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(136);
    if (total != 100) {
      System.Int32 RNTRNTRNT_67 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(137);
      return RNTRNTRNT_67;
    }
    System.Int32 RNTRNTRNT_68 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(138);
    return RNTRNTRNT_68;
  }
}
