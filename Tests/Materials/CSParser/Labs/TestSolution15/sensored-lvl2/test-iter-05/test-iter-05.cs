using System;
using System.Collections;
class X
{
  static IEnumerable GetIt()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(147);
    List l = new List(3);
    l.Add(1);
    l.Add(2);
    l.Add(3);
    IACSharpSensor.IACSharpSensor.SensorReached(148);
    foreach (int i in l) {
      IEnumerable RNTRNTRNT_70 = i;
      IACSharpSensor.IACSharpSensor.SensorReached(149);
      yield return RNTRNTRNT_70;
      IACSharpSensor.IACSharpSensor.SensorReached(150);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(151);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(152);
    int total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(153);
    foreach (int i in GetIt()) {
      IACSharpSensor.IACSharpSensor.SensorReached(154);
      Console.WriteLine("Got: " + i);
      total += i;
    }
    System.Int32 RNTRNTRNT_71 = total == 6 ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(155);
    return RNTRNTRNT_71;
  }
}
public class List : IEnumerable
{
  int pos = 0;
  int[] items;
  public List(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(156);
    items = new int[i];
    IACSharpSensor.IACSharpSensor.SensorReached(157);
  }
  public void Add(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(158);
    items[pos++] = value;
    IACSharpSensor.IACSharpSensor.SensorReached(159);
  }
  public MyEnumerator GetEnumerator()
  {
    MyEnumerator RNTRNTRNT_72 = new MyEnumerator(this);
    IACSharpSensor.IACSharpSensor.SensorReached(160);
    return RNTRNTRNT_72;
  }
  IEnumerator IEnumerable.GetEnumerator()
  {
    IEnumerator RNTRNTRNT_73 = GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(161);
    return RNTRNTRNT_73;
  }
  public struct MyEnumerator : IEnumerator
  {
    List l;
    int p;
    public MyEnumerator(List l)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(162);
      this.l = l;
      p = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(163);
    }
    public object Current {
      get {
        System.Object RNTRNTRNT_74 = l.items[p];
        IACSharpSensor.IACSharpSensor.SensorReached(164);
        return RNTRNTRNT_74;
      }
    }
    public bool MoveNext()
    {
      System.Boolean RNTRNTRNT_75 = ++p < l.pos;
      IACSharpSensor.IACSharpSensor.SensorReached(165);
      return RNTRNTRNT_75;
    }
    public void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(166);
      p = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(167);
    }
  }
}
