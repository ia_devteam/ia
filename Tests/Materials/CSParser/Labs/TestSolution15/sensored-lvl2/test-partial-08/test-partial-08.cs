public partial class Foo
{
  public string myId;
}
public sealed partial class Foo
{
  public string Id {
    get {
      System.String RNTRNTRNT_153 = myId;
      IACSharpSensor.IACSharpSensor.SensorReached(405);
      return RNTRNTRNT_153;
    }
  }
}
public class PartialAbstractCompilationError
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(406);
    if (typeof(Foo).IsAbstract || !typeof(Foo).IsSealed) {
      System.Int32 RNTRNTRNT_154 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(407);
      return RNTRNTRNT_154;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(408);
    System.Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_155 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(409);
    return RNTRNTRNT_155;
  }
}
