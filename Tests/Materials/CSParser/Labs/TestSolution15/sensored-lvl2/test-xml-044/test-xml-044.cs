using System.Xml;
public class EntryPoint
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(587);
  }
  internal class A
  {
    public virtual void Decide(int a)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(588);
    }
  }
  internal class B : A
  {
  }
}
