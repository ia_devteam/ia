using System;
using System.Collections;
class X
{
  static IEnumerator GetIt()
  {
    Object RNTRNTRNT_50 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(81);
    yield return RNTRNTRNT_50;
    IACSharpSensor.IACSharpSensor.SensorReached(82);
    Object RNTRNTRNT_51 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(83);
    yield return RNTRNTRNT_51;
    IACSharpSensor.IACSharpSensor.SensorReached(84);
    Object RNTRNTRNT_52 = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(85);
    yield return RNTRNTRNT_52;
    IACSharpSensor.IACSharpSensor.SensorReached(86);
    IACSharpSensor.IACSharpSensor.SensorReached(87);
  }
  static IEnumerable GetIt2()
  {
    IEnumerable RNTRNTRNT_53 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(88);
    yield return RNTRNTRNT_53;
    IACSharpSensor.IACSharpSensor.SensorReached(89);
    IEnumerable RNTRNTRNT_54 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(90);
    yield return RNTRNTRNT_54;
    IACSharpSensor.IACSharpSensor.SensorReached(91);
    IEnumerable RNTRNTRNT_55 = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(92);
    yield return RNTRNTRNT_55;
    IACSharpSensor.IACSharpSensor.SensorReached(93);
    IACSharpSensor.IACSharpSensor.SensorReached(94);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(95);
    IEnumerator e = GetIt();
    int total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(96);
    while (e.MoveNext()) {
      IACSharpSensor.IACSharpSensor.SensorReached(97);
      Console.WriteLine("Value=" + e.Current);
      total += (int)e.Current;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(98);
    if (total != 6) {
      System.Int32 RNTRNTRNT_56 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(99);
      return RNTRNTRNT_56;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(100);
    total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(101);
    foreach (int i in GetIt2()) {
      IACSharpSensor.IACSharpSensor.SensorReached(102);
      Console.WriteLine("Value=" + i);
      total += i;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(103);
    if (total != 6) {
      System.Int32 RNTRNTRNT_57 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(104);
      return RNTRNTRNT_57;
    }
    System.Int32 RNTRNTRNT_58 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(105);
    return RNTRNTRNT_58;
  }
}
