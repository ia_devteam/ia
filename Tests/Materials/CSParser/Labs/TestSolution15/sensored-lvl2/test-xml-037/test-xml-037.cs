using System;
using System.Reflection;
public class Whatever
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(570);
    foreach (MemberInfo mi in typeof(AppDomain).FindMembers(MemberTypes.All, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance, Type.FilterName, "AssemblyResolve")) {
      IACSharpSensor.IACSharpSensor.SensorReached(571);
      Console.WriteLine(mi.GetType());
    }
    IACSharpSensor.IACSharpSensor.SensorReached(572);
  }
}
