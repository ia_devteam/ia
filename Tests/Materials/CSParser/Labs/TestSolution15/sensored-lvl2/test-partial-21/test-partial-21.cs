namespace Mono.Sms
{
  partial class Main
  {
  }
}
namespace Mono.Sms
{
  using Mono.Sms.Core;
  public partial class Main
  {
    public void Test()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(482);
      Contacts frm = new Contacts();
      frm.ContactsEventHandler += delegate() { Agenda.AddContact(); };
      IACSharpSensor.IACSharpSensor.SensorReached(483);
    }
  }
  public partial class Contacts
  {
    public void Test()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(484);
      ContactsEventHandler();
      IACSharpSensor.IACSharpSensor.SensorReached(485);
    }
    public delegate void ContactsHandler();
    public event ContactsHandler ContactsEventHandler;
  }
}
namespace Mono.Sms.Core
{
  public class Agenda
  {
    public static void AddContact()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(486);
    }
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(487);
    }
  }
}
