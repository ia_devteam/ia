using System;
using System.Collections;
class X
{
  static IEnumerable GetRange(int start, int end)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(139);
    for (int i = start; i < end; i++) {
      IEnumerable RNTRNTRNT_69 = i;
      IACSharpSensor.IACSharpSensor.SensorReached(140);
      yield return RNTRNTRNT_69;
      IACSharpSensor.IACSharpSensor.SensorReached(141);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(142);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(143);
    Console.WriteLine("GetRange 10..20");
    IACSharpSensor.IACSharpSensor.SensorReached(144);
    foreach (int i in GetRange(10, 20)) {
      IACSharpSensor.IACSharpSensor.SensorReached(145);
      Console.WriteLine("i=" + i);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(146);
  }
}
