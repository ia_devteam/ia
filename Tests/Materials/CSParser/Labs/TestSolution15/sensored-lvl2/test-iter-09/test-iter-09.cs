using System;
using System.Collections;
public class Tester
{
  string[] ABC = {
    "A",
    "B",
    "C"
  };
  string[,] EFGH = {
    {
      "E",
      "F"
    },
    {
      "G",
      "H"
    }
  };
  ArrayList al = new ArrayList();
  public Tester()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(297);
    al.Add("J");
    al.Add("K");
    IACSharpSensor.IACSharpSensor.SensorReached(298);
  }
  public System.Collections.IEnumerator GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(299);
    foreach (string s in ABC) {
      IACSharpSensor.IACSharpSensor.SensorReached(300);
      if (s == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(301);
        throw new Exception();
      } else {
        System.Collections.IEnumerator RNTRNTRNT_118 = s;
        IACSharpSensor.IACSharpSensor.SensorReached(302);
        yield return RNTRNTRNT_118;
        IACSharpSensor.IACSharpSensor.SensorReached(303);
      }
    }
    System.Collections.IEnumerator RNTRNTRNT_119 = "D";
    IACSharpSensor.IACSharpSensor.SensorReached(304);
    yield return RNTRNTRNT_119;
    IACSharpSensor.IACSharpSensor.SensorReached(305);
    IACSharpSensor.IACSharpSensor.SensorReached(306);
    foreach (string s in EFGH) {
      IACSharpSensor.IACSharpSensor.SensorReached(307);
      if (s == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(308);
        throw new Exception();
      } else {
        System.Collections.IEnumerator RNTRNTRNT_120 = s;
        IACSharpSensor.IACSharpSensor.SensorReached(309);
        yield return RNTRNTRNT_120;
        IACSharpSensor.IACSharpSensor.SensorReached(310);
      }
    }
    System.Collections.IEnumerator RNTRNTRNT_121 = "I";
    IACSharpSensor.IACSharpSensor.SensorReached(311);
    yield return RNTRNTRNT_121;
    IACSharpSensor.IACSharpSensor.SensorReached(312);
    IACSharpSensor.IACSharpSensor.SensorReached(313);
    foreach (string s in al) {
      IACSharpSensor.IACSharpSensor.SensorReached(314);
      if (s == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(315);
        throw new Exception();
      } else {
        System.Collections.IEnumerator RNTRNTRNT_122 = s;
        IACSharpSensor.IACSharpSensor.SensorReached(316);
        yield return RNTRNTRNT_122;
        IACSharpSensor.IACSharpSensor.SensorReached(317);
      }
    }
    System.Collections.IEnumerator RNTRNTRNT_123 = "L";
    IACSharpSensor.IACSharpSensor.SensorReached(318);
    yield return RNTRNTRNT_123;
    IACSharpSensor.IACSharpSensor.SensorReached(319);
    IACSharpSensor.IACSharpSensor.SensorReached(320);
  }
}
class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(321);
    Tester tester = new Tester();
    string[] list = {
      "A",
      "B",
      "C",
      "D",
      "E",
      "F",
      "G",
      "H",
      "I",
      "J",
      "K",
      "L"
    };
    int top = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(322);
    foreach (string s in tester) {
      IACSharpSensor.IACSharpSensor.SensorReached(323);
      if (s != list[top]) {
        IACSharpSensor.IACSharpSensor.SensorReached(324);
        Console.WriteLine("Failure, got {0} expected {1}", s, list[top]);
        System.Int32 RNTRNTRNT_124 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(325);
        return RNTRNTRNT_124;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(326);
      top++;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(327);
    if (top != list.Length) {
      IACSharpSensor.IACSharpSensor.SensorReached(328);
      Console.WriteLine("Failure, expected {0} got {1}", list.Length, top);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(329);
    Console.WriteLine("Success");
    System.Int32 RNTRNTRNT_125 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(330);
    return RNTRNTRNT_125;
  }
}
