using System;
namespace Testing
{
  public class Test2
  {
    public static void Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(528);
    }
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(529);
    }
  }
  public struct StructTest
  {
  }
  public interface InterfaceTest
  {
  }
  public enum EnumTest
  {
    Foo,
    Bar
  }
  public class Dummy
  {
  }
  public delegate void MyDelegate(object o, EventArgs e);
  public class Test
  {
    const string Constant = "CONSTANT STRING";
    public string BadPublicField;
    private string PrivateField;
    public string PublicProperty {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(530);
        return null;
      }
    }
    private string PrivateProperty {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(531);
        return null;
      }
      set { IACSharpSensor.IACSharpSensor.SensorReached(532); }
    }
    int x;
    public event EventHandler MyEvent;
    int y;
    public static void Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(533);
    }
    public static void Foo(long l, Test t, System.Collections.ArrayList al)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(534);
    }
    public string this[int i] {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(535);
        return null;
      }
    }
    public string this[int i, Test t] {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(536);
        return null;
      }
    }
    public static bool operator !(Test t)
    {
      System.Boolean RNTRNTRNT_185 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(537);
      return RNTRNTRNT_185;
    }
    public static int operator +(Test t, int b)
    {
      System.Int32 RNTRNTRNT_186 = b;
      IACSharpSensor.IACSharpSensor.SensorReached(538);
      return RNTRNTRNT_186;
    }
    ~Test()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(539);
    }
    public Test()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(540);
    }
    public Test(string arg, string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(541);
    }
    public class InternalClass
    {
    }
    public struct InternalStruct
    {
    }
  }
}
