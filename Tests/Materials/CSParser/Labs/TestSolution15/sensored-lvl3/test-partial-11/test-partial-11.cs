using System;
using System.Reflection;
static partial class StaticClass
{
  public static string Name()
  {
    System.String RNTRNTRNT_162 = "OK";
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    return RNTRNTRNT_162;
  }
}
partial class StaticClass2
{
}
static partial class StaticClass2
{
}
public class MainClass
{
  static bool IsStatic(Type t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(284);
    Type type = typeof(StaticClass);
    if (!type.IsAbstract || !type.IsSealed) {
      Console.WriteLine("Is not abstract sealed");
      System.Boolean RNTRNTRNT_163 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(285);
      return RNTRNTRNT_163;
    }
    if (type.GetConstructors().Length > 0) {
      Console.WriteLine("Has constructor");
      System.Boolean RNTRNTRNT_164 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(286);
      return RNTRNTRNT_164;
    }
    System.Boolean RNTRNTRNT_165 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(287);
    return RNTRNTRNT_165;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(288);
    if (!IsStatic(typeof(StaticClass))) {
      System.Int32 RNTRNTRNT_166 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(289);
      return RNTRNTRNT_166;
    }
    if (!IsStatic(typeof(StaticClass2))) {
      System.Int32 RNTRNTRNT_167 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(290);
      return RNTRNTRNT_167;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_168 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(291);
    return RNTRNTRNT_168;
  }
}
