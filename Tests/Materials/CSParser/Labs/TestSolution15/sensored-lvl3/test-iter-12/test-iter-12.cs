class X
{
  System.Collections.IEnumerable a()
  {
    lock (this) {
      System.Collections.IEnumerable RNTRNTRNT_136 = "a";
      IACSharpSensor.IACSharpSensor.SensorReached(243);
      yield return RNTRNTRNT_136;
      IACSharpSensor.IACSharpSensor.SensorReached(244);
      System.Collections.IEnumerable RNTRNTRNT_137 = "b";
      IACSharpSensor.IACSharpSensor.SensorReached(245);
      yield return RNTRNTRNT_137;
      IACSharpSensor.IACSharpSensor.SensorReached(246);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(247);
  }
  static void Main()
  {
  }
}
