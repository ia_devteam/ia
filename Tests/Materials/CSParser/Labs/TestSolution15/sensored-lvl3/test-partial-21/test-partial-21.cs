namespace Mono.Sms
{
  partial class Main
  {
  }
}
namespace Mono.Sms
{
  using Mono.Sms.Core;
  public partial class Main
  {
    public void Test()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(323);
      Contacts frm = new Contacts();
      frm.ContactsEventHandler += delegate() { Agenda.AddContact(); };
      IACSharpSensor.IACSharpSensor.SensorReached(324);
    }
  }
  public partial class Contacts
  {
    public void Test()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(325);
      ContactsEventHandler();
      IACSharpSensor.IACSharpSensor.SensorReached(326);
    }
    public delegate void ContactsHandler();
    public event ContactsHandler ContactsEventHandler;
  }
}
namespace Mono.Sms.Core
{
  public class Agenda
  {
    public static void AddContact()
    {
    }
    public static void Main()
    {
    }
  }
}
