namespace Foo
{
  public class Hello
  {
    public static int World = 8;
  }
}
namespace Bar
{
  public class Hello
  {
    public static int World = 9;
  }
}
namespace X
{
  using Foo;
  public partial class Test
  {
    public static int FooWorld()
    {
      System.Int32 RNTRNTRNT_138 = Hello.World;
      IACSharpSensor.IACSharpSensor.SensorReached(248);
      return RNTRNTRNT_138;
    }
  }
}
namespace X
{
  using Bar;
  public partial class Test
  {
    public static int BarWorld()
    {
      System.Int32 RNTRNTRNT_139 = Hello.World;
      IACSharpSensor.IACSharpSensor.SensorReached(249);
      return RNTRNTRNT_139;
    }
  }
}
class Y
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(250);
    if (X.Test.FooWorld() != 8) {
      System.Int32 RNTRNTRNT_140 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(251);
      return RNTRNTRNT_140;
    }
    if (X.Test.BarWorld() != 9) {
      System.Int32 RNTRNTRNT_141 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(252);
      return RNTRNTRNT_141;
    }
    System.Int32 RNTRNTRNT_142 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(253);
    return RNTRNTRNT_142;
  }
}
