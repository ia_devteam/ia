public class EntryPoint
{
  static void Main()
  {
  }
}
public interface IA
{
  string Name { get; }
  string Execute();
}
public interface IB : IA
{
  new int Name { get; }
  new int Execute();
}
public class A
{
  public string Name {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(361);
      return null;
    }
  }
  public string Execute()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(362);
    return null;
  }
}
public class B : A
{
  public new int Name {
    get {
      System.Int32 RNTRNTRNT_191 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(363);
      return RNTRNTRNT_191;
    }
  }
  public new int Execute()
  {
    System.Int32 RNTRNTRNT_192 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(364);
    return RNTRNTRNT_192;
  }
}
