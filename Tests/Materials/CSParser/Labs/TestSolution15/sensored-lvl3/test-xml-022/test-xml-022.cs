using System;
namespace Testing
{
  public class Test2
  {
    public static void Foo()
    {
    }
    public static void Main()
    {
    }
  }
  public struct StructTest
  {
  }
  public interface InterfaceTest
  {
  }
  public enum EnumTest
  {
    Foo,
    Bar
  }
  public class Dummy
  {
  }
  public delegate void MyDelegate(object o, EventArgs e);
  public class Test
  {
    const string Constant = "CONSTANT STRING";
    public string BadPublicField;
    private string PrivateField;
    public string PublicProperty {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(337);
        return null;
      }
    }
    private string PrivateProperty {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(338);
        return null;
      }
      set { }
    }
    int x;
    public event EventHandler MyEvent;
    int y;
    public static void Foo()
    {
    }
    public static void Foo(long l, Test t, System.Collections.ArrayList al)
    {
    }
    public string this[int i] {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(339);
        return null;
      }
    }
    public string this[int i, Test t] {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(340);
        return null;
      }
    }
    public static bool operator !(Test t)
    {
      System.Boolean RNTRNTRNT_185 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(341);
      return RNTRNTRNT_185;
    }
    public static int operator +(Test t, int b)
    {
      System.Int32 RNTRNTRNT_186 = b;
      IACSharpSensor.IACSharpSensor.SensorReached(342);
      return RNTRNTRNT_186;
    }
    ~Test()
    {
    }
    public Test()
    {
    }
    public Test(string arg, string[] args)
    {
    }
    public class InternalClass
    {
    }
    public struct InternalStruct
    {
    }
  }
}
