using System;
using System.Reflection;
using System.Xml;
public class Whatever
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(350);
    foreach (MemberInfo mi in typeof(XmlDocument).FindMembers(MemberTypes.All, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance, Type.FilterName, "NodeInserted")) {
      Console.WriteLine(mi.GetType());
    }
    IACSharpSensor.IACSharpSensor.SensorReached(351);
  }
}
