public partial class Foo
{
  public string myId;
}
public sealed partial class Foo
{
  public string Id {
    get {
      System.String RNTRNTRNT_153 = myId;
      IACSharpSensor.IACSharpSensor.SensorReached(268);
      return RNTRNTRNT_153;
    }
  }
}
public class PartialAbstractCompilationError
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(269);
    if (typeof(Foo).IsAbstract || !typeof(Foo).IsSealed) {
      System.Int32 RNTRNTRNT_154 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(270);
      return RNTRNTRNT_154;
    }
    System.Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_155 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(271);
    return RNTRNTRNT_155;
  }
}
