namespace Foo
{
  public partial class X
  {
    public static void Main()
    {
    }
  }
}
namespace Foo
{
  using System;
  using System.Collections;
  public partial class X
  {
    public static IEnumerable Attempts2()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(303);
      AttributeTargets t = AttributeTargets.All;
      IACSharpSensor.IACSharpSensor.SensorReached(304);
      yield break;
    }
    public static IEnumerable Attempts {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(305);
        AttributeTargets t = AttributeTargets.All;
        IACSharpSensor.IACSharpSensor.SensorReached(306);
        yield break;
      }
    }
    public IEnumerable this[int i] {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(307);
        AttributeTargets t = AttributeTargets.All;
        IACSharpSensor.IACSharpSensor.SensorReached(308);
        yield break;
      }
    }
  }
}
