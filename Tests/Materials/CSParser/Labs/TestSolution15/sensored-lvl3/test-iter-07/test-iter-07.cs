using System;
using System.Collections;
public class Test
{
  public IEnumerable Foo(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(156);
    try {
      try {
        IEnumerable RNTRNTRNT_92 = a;
        IACSharpSensor.IACSharpSensor.SensorReached(157);
        yield return RNTRNTRNT_92;
        IACSharpSensor.IACSharpSensor.SensorReached(158);
      } finally {
        Console.WriteLine("Hello World");
      }
      Console.WriteLine("Next block");
      try {
        IEnumerable RNTRNTRNT_93 = a * a;
        IACSharpSensor.IACSharpSensor.SensorReached(159);
        yield return RNTRNTRNT_93;
        IACSharpSensor.IACSharpSensor.SensorReached(160);
      } finally {
        Console.WriteLine("Boston");
      }
    } finally {
      Console.WriteLine("Outer finally");
    }
    Console.WriteLine("Outer block");
    IACSharpSensor.IACSharpSensor.SensorReached(161);
    yield break;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    Test test = new Test();
    ArrayList list = new ArrayList();
    foreach (object o in test.Foo(5)) {
      list.Add(o);
    }
    if (list.Count != 2) {
      System.Int32 RNTRNTRNT_94 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(163);
      return RNTRNTRNT_94;
    }
    if ((int)list[0] != 5) {
      System.Int32 RNTRNTRNT_95 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(164);
      return RNTRNTRNT_95;
    }
    if ((int)list[1] != 25) {
      System.Int32 RNTRNTRNT_96 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(165);
      return RNTRNTRNT_96;
    }
    IEnumerable a = test.Foo(5);
    IEnumerator b = a as IEnumerator;
    if (b != null) {
      if (b.MoveNext()) {
        System.Int32 RNTRNTRNT_97 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(166);
        return RNTRNTRNT_97;
      }
    }
    IEnumerator c = a.GetEnumerator();
    if (!c.MoveNext()) {
      System.Int32 RNTRNTRNT_98 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(167);
      return RNTRNTRNT_98;
    }
    if ((int)c.Current != 5) {
      System.Int32 RNTRNTRNT_99 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(168);
      return RNTRNTRNT_99;
    }
    if (!c.MoveNext()) {
      System.Int32 RNTRNTRNT_100 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(169);
      return RNTRNTRNT_100;
    }
    if ((int)c.Current != 25) {
      System.Int32 RNTRNTRNT_101 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(170);
      return RNTRNTRNT_101;
    }
    IEnumerator d = a.GetEnumerator();
    if ((int)c.Current != 25) {
      System.Int32 RNTRNTRNT_102 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(171);
      return RNTRNTRNT_102;
    }
    if (!d.MoveNext()) {
      System.Int32 RNTRNTRNT_103 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(172);
      return RNTRNTRNT_103;
    }
    if ((int)c.Current != 25) {
      System.Int32 RNTRNTRNT_104 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(173);
      return RNTRNTRNT_104;
    }
    if ((int)d.Current != 5) {
      System.Int32 RNTRNTRNT_105 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(174);
      return RNTRNTRNT_105;
    }
    if (c.MoveNext()) {
      System.Int32 RNTRNTRNT_106 = 13;
      IACSharpSensor.IACSharpSensor.SensorReached(175);
      return RNTRNTRNT_106;
    }
    ((IDisposable)a).Dispose();
    System.Int32 RNTRNTRNT_107 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(176);
    return RNTRNTRNT_107;
  }
}
