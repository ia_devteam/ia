using System;
using System.Collections;
class X
{
  static IEnumerable GetRange(int start, int end)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(99);
    for (int i = start; i < end; i++) {
      IEnumerable RNTRNTRNT_69 = i;
      IACSharpSensor.IACSharpSensor.SensorReached(100);
      yield return RNTRNTRNT_69;
      IACSharpSensor.IACSharpSensor.SensorReached(101);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(102);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(103);
    Console.WriteLine("GetRange 10..20");
    foreach (int i in GetRange(10, 20)) {
      Console.WriteLine("i=" + i);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(104);
  }
}
