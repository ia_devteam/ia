extern alias MyAssembly01;
using System;
using SameNamespace = MyAssembly01;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(57);
    SameNamespace.GlobalClass.StaticMethod();
    System.Int32 RNTRNTRNT_49 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(58);
    return RNTRNTRNT_49;
  }
}
