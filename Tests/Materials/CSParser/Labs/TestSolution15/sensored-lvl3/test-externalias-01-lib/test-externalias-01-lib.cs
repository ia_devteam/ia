using System;
public class GlobalClass
{
  public int InstanceMethod()
  {
    System.Int32 RNTRNTRNT_16 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(17);
    return RNTRNTRNT_16;
  }
  public static int StaticMethod()
  {
    System.Int32 RNTRNTRNT_17 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(18);
    return RNTRNTRNT_17;
  }
  public static void JustForSecond()
  {
  }
}
namespace Namespace1
{
  public class MyClass1
  {
    public int InstanceMethod()
    {
      System.Int32 RNTRNTRNT_18 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(19);
      return RNTRNTRNT_18;
    }
    public static int StaticMethod()
    {
      System.Int32 RNTRNTRNT_19 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(20);
      return RNTRNTRNT_19;
    }
    public class MyNestedClass1
    {
      public int InstanceMethod()
      {
        System.Int32 RNTRNTRNT_20 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(21);
        return RNTRNTRNT_20;
      }
      public static int StaticMethod()
      {
        System.Int32 RNTRNTRNT_21 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(22);
        return RNTRNTRNT_21;
      }
    }
    public static void JustForSecond()
    {
    }
  }
  namespace Namespace2
  {
    public class MyClass2
    {
      public class MyNestedClass2
      {
        public int InstanceMethod()
        {
          System.Int32 RNTRNTRNT_22 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(23);
          return RNTRNTRNT_22;
        }
        public static int StaticMethod()
        {
          System.Int32 RNTRNTRNT_23 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(24);
          return RNTRNTRNT_23;
        }
      }
      public int InstanceMethod()
      {
        System.Int32 RNTRNTRNT_24 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(25);
        return RNTRNTRNT_24;
      }
      public static int StaticMethod()
      {
        System.Int32 RNTRNTRNT_25 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(26);
        return RNTRNTRNT_25;
      }
      public static void JustForFirst()
      {
      }
    }
  }
}
