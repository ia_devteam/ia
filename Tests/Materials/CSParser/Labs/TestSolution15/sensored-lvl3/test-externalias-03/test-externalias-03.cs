extern alias MyAssembly01;
extern alias MyAssembly02;
using System;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(33);
    if (MyAssembly01.Namespace1.MyClass1.MyNestedClass1.StaticMethod() != 1) {
      System.Int32 RNTRNTRNT_31 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(34);
      return RNTRNTRNT_31;
    }
    if (MyAssembly02.Namespace1.MyClass1.MyNestedClass1.StaticMethod() != 2) {
      System.Int32 RNTRNTRNT_32 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(35);
      return RNTRNTRNT_32;
    }
    if (new MyAssembly01.Namespace1.MyClass1.MyNestedClass1().InstanceMethod() != 1) {
      System.Int32 RNTRNTRNT_33 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(36);
      return RNTRNTRNT_33;
    }
    if (new MyAssembly02.Namespace1.MyClass1.MyNestedClass1().InstanceMethod() != 2) {
      System.Int32 RNTRNTRNT_34 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(37);
      return RNTRNTRNT_34;
    }
    System.Int32 RNTRNTRNT_35 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(38);
    return RNTRNTRNT_35;
  }
}
