using System;
namespace Test2
{
  public interface Base
  {
  }
  public partial class Foo : Base
  {
    public static int f = 10;
  }
  public partial class Foo : Base
  {
    public static int f2 = 9;
  }
}
namespace Test3
{
  public interface Base
  {
  }
  public partial struct Foo : Base
  {
    public static int f = 10;
  }
  public partial struct Foo : Base
  {
    public static int f2 = 9;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(277);
    if (Test2.Foo.f != 10) {
      System.Int32 RNTRNTRNT_157 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(278);
      return RNTRNTRNT_157;
    }
    if (Test2.Foo.f2 != 9) {
      System.Int32 RNTRNTRNT_158 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(279);
      return RNTRNTRNT_158;
    }
    if (Test3.Foo.f != 10) {
      System.Int32 RNTRNTRNT_159 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(280);
      return RNTRNTRNT_159;
    }
    if (Test3.Foo.f2 != 9) {
      System.Int32 RNTRNTRNT_160 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(281);
      return RNTRNTRNT_160;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_161 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(282);
    return RNTRNTRNT_161;
  }
}
