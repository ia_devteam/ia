extern alias MyAssembly01;
extern alias MyAssembly02;
using System;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(39);
    if (MyAssembly01.Namespace1.Namespace2.MyClass2.StaticMethod() != 1) {
      System.Int32 RNTRNTRNT_36 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(40);
      return RNTRNTRNT_36;
    }
    if (MyAssembly02.Namespace1.Namespace2.MyClass2.StaticMethod() != 2) {
      System.Int32 RNTRNTRNT_37 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(41);
      return RNTRNTRNT_37;
    }
    if (new MyAssembly01.Namespace1.Namespace2.MyClass2().InstanceMethod() != 1) {
      System.Int32 RNTRNTRNT_38 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(42);
      return RNTRNTRNT_38;
    }
    if (new MyAssembly02.Namespace1.Namespace2.MyClass2().InstanceMethod() != 2) {
      System.Int32 RNTRNTRNT_39 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(43);
      return RNTRNTRNT_39;
    }
    System.Int32 RNTRNTRNT_40 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(44);
    return RNTRNTRNT_40;
  }
}
