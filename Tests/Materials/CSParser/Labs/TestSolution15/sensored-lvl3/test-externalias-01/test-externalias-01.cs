extern alias MyAssembly01;
extern alias MyAssembly02;
using System;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(11);
    if (MyAssembly01.GlobalClass.StaticMethod() != 1) {
      System.Int32 RNTRNTRNT_11 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(12);
      return RNTRNTRNT_11;
    }
    if (MyAssembly02.GlobalClass.StaticMethod() != 2) {
      System.Int32 RNTRNTRNT_12 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(13);
      return RNTRNTRNT_12;
    }
    if (new MyAssembly01.GlobalClass().InstanceMethod() != 1) {
      System.Int32 RNTRNTRNT_13 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(14);
      return RNTRNTRNT_13;
    }
    if (new MyAssembly02.GlobalClass().InstanceMethod() != 2) {
      System.Int32 RNTRNTRNT_14 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(15);
      return RNTRNTRNT_14;
    }
    System.Int32 RNTRNTRNT_15 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    return RNTRNTRNT_15;
  }
}
