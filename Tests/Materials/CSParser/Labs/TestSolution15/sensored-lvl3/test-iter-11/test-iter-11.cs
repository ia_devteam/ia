using System;
using System.Collections;
class X
{
  public event EventHandler Hook;
  public IEnumerator Pipeline()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(235);
    if (Hook == null) {
      throw new Exception("error");
    }
    Hook(this, EventArgs.Empty);
    Object RNTRNTRNT_135 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(236);
    yield return RNTRNTRNT_135;
    IACSharpSensor.IACSharpSensor.SensorReached(237);
    IACSharpSensor.IACSharpSensor.SensorReached(238);
  }
  static void M(object sender, EventArgs args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(239);
    Console.WriteLine("Hook invoked");
    IACSharpSensor.IACSharpSensor.SensorReached(240);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(241);
    X x = new X();
    x.Hook += M;
    IEnumerator y = x.Pipeline();
    y.MoveNext();
    IACSharpSensor.IACSharpSensor.SensorReached(242);
  }
}
