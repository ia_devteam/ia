using System;
namespace NS
{
  extern alias MyAssembly01;
  public class MyClass
  {
    public static int GetInt()
    {
      System.Int32 RNTRNTRNT_41 = MyAssembly01.GlobalClass.StaticMethod();
      IACSharpSensor.IACSharpSensor.SensorReached(47);
      return RNTRNTRNT_41;
    }
  }
}
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(48);
    if (NS.MyClass.GetInt() != 1) {
      System.Int32 RNTRNTRNT_42 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(49);
      return RNTRNTRNT_42;
    }
    System.Int32 RNTRNTRNT_43 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(50);
    return RNTRNTRNT_43;
  }
}
