using System;
using System.Collections;
class X
{
  static IEnumerator GetIt()
  {
    Object RNTRNTRNT_50 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(59);
    yield return RNTRNTRNT_50;
    IACSharpSensor.IACSharpSensor.SensorReached(60);
    Object RNTRNTRNT_51 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(61);
    yield return RNTRNTRNT_51;
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    Object RNTRNTRNT_52 = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(63);
    yield return RNTRNTRNT_52;
    IACSharpSensor.IACSharpSensor.SensorReached(64);
    IACSharpSensor.IACSharpSensor.SensorReached(65);
  }
  static IEnumerable GetIt2()
  {
    IEnumerable RNTRNTRNT_53 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(66);
    yield return RNTRNTRNT_53;
    IACSharpSensor.IACSharpSensor.SensorReached(67);
    IEnumerable RNTRNTRNT_54 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    yield return RNTRNTRNT_54;
    IACSharpSensor.IACSharpSensor.SensorReached(69);
    IEnumerable RNTRNTRNT_55 = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(70);
    yield return RNTRNTRNT_55;
    IACSharpSensor.IACSharpSensor.SensorReached(71);
    IACSharpSensor.IACSharpSensor.SensorReached(72);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(73);
    IEnumerator e = GetIt();
    int total = 0;
    while (e.MoveNext()) {
      Console.WriteLine("Value=" + e.Current);
      total += (int)e.Current;
    }
    if (total != 6) {
      System.Int32 RNTRNTRNT_56 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(74);
      return RNTRNTRNT_56;
    }
    total = 0;
    foreach (int i in GetIt2()) {
      Console.WriteLine("Value=" + i);
      total += i;
    }
    if (total != 6) {
      System.Int32 RNTRNTRNT_57 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(75);
      return RNTRNTRNT_57;
    }
    System.Int32 RNTRNTRNT_58 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(76);
    return RNTRNTRNT_58;
  }
}
