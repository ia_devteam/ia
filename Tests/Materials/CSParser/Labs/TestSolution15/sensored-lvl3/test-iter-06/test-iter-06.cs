using System;
using System.Collections;
struct S
{
  int j;
  public IEnumerable Get(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(123);
    Console.WriteLine("Sending: " + a);
    IEnumerable RNTRNTRNT_76 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(124);
    yield return RNTRNTRNT_76;
    IACSharpSensor.IACSharpSensor.SensorReached(125);
    j = 10;
    Console.WriteLine("Sending: " + j);
    IEnumerable RNTRNTRNT_77 = j;
    IACSharpSensor.IACSharpSensor.SensorReached(126);
    yield return RNTRNTRNT_77;
    IACSharpSensor.IACSharpSensor.SensorReached(127);
    IACSharpSensor.IACSharpSensor.SensorReached(128);
  }
  public static IEnumerable GetS(int a)
  {
    IEnumerable RNTRNTRNT_78 = 100;
    IACSharpSensor.IACSharpSensor.SensorReached(129);
    yield return RNTRNTRNT_78;
    IACSharpSensor.IACSharpSensor.SensorReached(130);
    IEnumerable RNTRNTRNT_79 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(131);
    yield return RNTRNTRNT_79;
    IACSharpSensor.IACSharpSensor.SensorReached(132);
    IEnumerable RNTRNTRNT_80 = 1000;
    IACSharpSensor.IACSharpSensor.SensorReached(133);
    yield return RNTRNTRNT_80;
    IACSharpSensor.IACSharpSensor.SensorReached(134);
    IACSharpSensor.IACSharpSensor.SensorReached(135);
  }
}
class X
{
  IEnumerable Get(int a)
  {
    IEnumerable RNTRNTRNT_81 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(136);
    yield return RNTRNTRNT_81;
    IACSharpSensor.IACSharpSensor.SensorReached(137);
    IEnumerable RNTRNTRNT_82 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(138);
    yield return RNTRNTRNT_82;
    IACSharpSensor.IACSharpSensor.SensorReached(139);
    IEnumerable RNTRNTRNT_83 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(140);
    yield return RNTRNTRNT_83;
    IACSharpSensor.IACSharpSensor.SensorReached(141);
    IACSharpSensor.IACSharpSensor.SensorReached(142);
  }
  static IEnumerable GetS(int a)
  {
    IEnumerable RNTRNTRNT_84 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(143);
    yield return RNTRNTRNT_84;
    IACSharpSensor.IACSharpSensor.SensorReached(144);
    IEnumerable RNTRNTRNT_85 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(145);
    yield return RNTRNTRNT_85;
    IACSharpSensor.IACSharpSensor.SensorReached(146);
    IEnumerable RNTRNTRNT_86 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(147);
    yield return RNTRNTRNT_86;
    IACSharpSensor.IACSharpSensor.SensorReached(148);
    IACSharpSensor.IACSharpSensor.SensorReached(149);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(150);
    X y = new X();
    int total = 0;
    foreach (int x in y.Get(5)) {
      total += x;
    }
    if (total != 8) {
      System.Int32 RNTRNTRNT_87 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(151);
      return RNTRNTRNT_87;
    }
    total = 0;
    foreach (int x in GetS(3)) {
      total += x;
    }
    if (total != 7) {
      System.Int32 RNTRNTRNT_88 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(152);
      return RNTRNTRNT_88;
    }
    S s = new S();
    total = 0;
    foreach (int x in s.Get(100)) {
      Console.WriteLine("Got: " + x);
      total += x;
    }
    if (total != 110) {
      System.Int32 RNTRNTRNT_89 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(153);
      return RNTRNTRNT_89;
    }
    total = 0;
    foreach (int x in S.GetS(1)) {
      total += x;
    }
    if (total != 1101) {
      System.Int32 RNTRNTRNT_90 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(154);
      return RNTRNTRNT_90;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_91 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(155);
    return RNTRNTRNT_91;
  }
}
