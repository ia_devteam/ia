using System;
using System.Reflection;
using System.Runtime.InteropServices;
[Test2()]
public partial class Test
{
}
[AttributeUsage(AttributeTargets.Struct)]
public partial class TestAttribute : Attribute
{
}
[AttributeUsage(AttributeTargets.All)]
public partial class Test2Attribute : Attribute
{
}
[TestAttribute()]
public struct Test_2
{
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(264);
    if (Attribute.GetCustomAttributes(typeof(Test)).Length != 1) {
      System.Int32 RNTRNTRNT_150 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(265);
      return RNTRNTRNT_150;
    }
    if (Attribute.GetCustomAttributes(typeof(Test_2)).Length != 1) {
      System.Int32 RNTRNTRNT_151 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(266);
      return RNTRNTRNT_151;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_152 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(267);
    return RNTRNTRNT_152;
  }
}
