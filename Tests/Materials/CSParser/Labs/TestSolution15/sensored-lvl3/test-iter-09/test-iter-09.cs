using System;
using System.Collections;
public class Tester
{
  string[] ABC = {
    "A",
    "B",
    "C"
  };
  string[,] EFGH = {
    {
      "E",
      "F"
    },
    {
      "G",
      "H"
    }
  };
  ArrayList al = new ArrayList();
  public Tester()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(198);
    al.Add("J");
    al.Add("K");
    IACSharpSensor.IACSharpSensor.SensorReached(199);
  }
  public System.Collections.IEnumerator GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(200);
    foreach (string s in ABC) {
      if (s == null) {
        throw new Exception();
      } else {
        System.Collections.IEnumerator RNTRNTRNT_118 = s;
        IACSharpSensor.IACSharpSensor.SensorReached(201);
        yield return RNTRNTRNT_118;
        IACSharpSensor.IACSharpSensor.SensorReached(202);
      }
    }
    System.Collections.IEnumerator RNTRNTRNT_119 = "D";
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    yield return RNTRNTRNT_119;
    IACSharpSensor.IACSharpSensor.SensorReached(204);
    foreach (string s in EFGH) {
      if (s == null) {
        throw new Exception();
      } else {
        System.Collections.IEnumerator RNTRNTRNT_120 = s;
        IACSharpSensor.IACSharpSensor.SensorReached(205);
        yield return RNTRNTRNT_120;
        IACSharpSensor.IACSharpSensor.SensorReached(206);
      }
    }
    System.Collections.IEnumerator RNTRNTRNT_121 = "I";
    IACSharpSensor.IACSharpSensor.SensorReached(207);
    yield return RNTRNTRNT_121;
    IACSharpSensor.IACSharpSensor.SensorReached(208);
    foreach (string s in al) {
      if (s == null) {
        throw new Exception();
      } else {
        System.Collections.IEnumerator RNTRNTRNT_122 = s;
        IACSharpSensor.IACSharpSensor.SensorReached(209);
        yield return RNTRNTRNT_122;
        IACSharpSensor.IACSharpSensor.SensorReached(210);
      }
    }
    System.Collections.IEnumerator RNTRNTRNT_123 = "L";
    IACSharpSensor.IACSharpSensor.SensorReached(211);
    yield return RNTRNTRNT_123;
    IACSharpSensor.IACSharpSensor.SensorReached(212);
    IACSharpSensor.IACSharpSensor.SensorReached(213);
  }
}
class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(214);
    Tester tester = new Tester();
    string[] list = {
      "A",
      "B",
      "C",
      "D",
      "E",
      "F",
      "G",
      "H",
      "I",
      "J",
      "K",
      "L"
    };
    int top = 0;
    foreach (string s in tester) {
      if (s != list[top]) {
        Console.WriteLine("Failure, got {0} expected {1}", s, list[top]);
        System.Int32 RNTRNTRNT_124 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(215);
        return RNTRNTRNT_124;
      }
      top++;
    }
    if (top != list.Length) {
      Console.WriteLine("Failure, expected {0} got {1}", list.Length, top);
    }
    Console.WriteLine("Success");
    System.Int32 RNTRNTRNT_125 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(216);
    return RNTRNTRNT_125;
  }
}
