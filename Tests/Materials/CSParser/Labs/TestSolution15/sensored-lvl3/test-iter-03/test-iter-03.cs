using System;
using System.Collections;
class X
{
  static IEnumerable GetIt(int[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(87);
    foreach (int a in args) {
      IEnumerable RNTRNTRNT_64 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(88);
      yield return RNTRNTRNT_64;
      IACSharpSensor.IACSharpSensor.SensorReached(89);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(90);
  }
  static IEnumerable GetMulti(int[,] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(91);
    foreach (int a in args) {
      IEnumerable RNTRNTRNT_65 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(92);
      yield return RNTRNTRNT_65;
      IACSharpSensor.IACSharpSensor.SensorReached(93);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(94);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(95);
    int total = 0;
    foreach (int i in GetIt(new int[] {
      1,
      2,
      3
    })) {
      Console.WriteLine("Got: " + i);
      total += i;
    }
    if (total != 6) {
      System.Int32 RNTRNTRNT_66 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(96);
      return RNTRNTRNT_66;
    }
    total = 0;
    foreach (int i in GetMulti(new int[,] {
      {
        10,
        20
      },
      {
        30,
        40
      }
    })) {
      Console.WriteLine("Got: " + i);
      total += i;
    }
    if (total != 100) {
      System.Int32 RNTRNTRNT_67 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(97);
      return RNTRNTRNT_67;
    }
    System.Int32 RNTRNTRNT_68 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(98);
    return RNTRNTRNT_68;
  }
}
