using System;
partial class A
{
}
partial class A
{
  public static int F = 3;
}
partial class B
{
  public static int F = 4;
}
partial class B
{
}
public class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(319);
    if (A.F != 3) {
      System.Int32 RNTRNTRNT_180 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(320);
      return RNTRNTRNT_180;
    }
    if (B.F != 4) {
      System.Int32 RNTRNTRNT_181 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(321);
      return RNTRNTRNT_181;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_182 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(322);
    return RNTRNTRNT_182;
  }
}
