public class A
{
  static void Main()
  {
  }
  public virtual string Level {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(358);
      return null;
    }
  }
  public virtual void Test()
  {
  }
}
public class B : A
{
  public override string Level {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(359);
      return null;
    }
  }
  public override void Test()
  {
  }
}
