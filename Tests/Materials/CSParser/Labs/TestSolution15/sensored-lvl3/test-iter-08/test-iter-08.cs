using System;
using System.Collections;
public class Foo : IDisposable
{
  public readonly int Data;
  public Foo(int data)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    this.Data = data;
    IACSharpSensor.IACSharpSensor.SensorReached(178);
  }
  public bool disposed;
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(179);
    disposed = true;
    IACSharpSensor.IACSharpSensor.SensorReached(180);
  }
}
class X
{
  public static IEnumerable Test(int a, int b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(181);
    Foo foo3, foo4;
    using (Foo foo1 = new Foo(a), foo2 = new Foo(b)) {
      IEnumerable RNTRNTRNT_108 = foo1.Data;
      IACSharpSensor.IACSharpSensor.SensorReached(182);
      yield return RNTRNTRNT_108;
      IACSharpSensor.IACSharpSensor.SensorReached(183);
      IEnumerable RNTRNTRNT_109 = foo2.Data;
      IACSharpSensor.IACSharpSensor.SensorReached(184);
      yield return RNTRNTRNT_109;
      IACSharpSensor.IACSharpSensor.SensorReached(185);
      foo3 = foo1;
      foo4 = foo2;
    }
    IEnumerable RNTRNTRNT_110 = foo3.disposed;
    IACSharpSensor.IACSharpSensor.SensorReached(186);
    yield return RNTRNTRNT_110;
    IACSharpSensor.IACSharpSensor.SensorReached(187);
    IEnumerable RNTRNTRNT_111 = foo4.disposed;
    IACSharpSensor.IACSharpSensor.SensorReached(188);
    yield return RNTRNTRNT_111;
    IACSharpSensor.IACSharpSensor.SensorReached(189);
    IACSharpSensor.IACSharpSensor.SensorReached(190);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(191);
    ArrayList list = new ArrayList();
    foreach (object data in Test(3, 5)) {
      list.Add(data);
    }
    if (list.Count != 4) {
      System.Int32 RNTRNTRNT_112 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(192);
      return RNTRNTRNT_112;
    }
    if ((int)list[0] != 3) {
      System.Int32 RNTRNTRNT_113 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(193);
      return RNTRNTRNT_113;
    }
    if ((int)list[1] != 5) {
      System.Int32 RNTRNTRNT_114 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(194);
      return RNTRNTRNT_114;
    }
    if (!(bool)list[2]) {
      System.Int32 RNTRNTRNT_115 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(195);
      return RNTRNTRNT_115;
    }
    if (!(bool)list[3]) {
      System.Int32 RNTRNTRNT_116 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(196);
      return RNTRNTRNT_116;
    }
    System.Int32 RNTRNTRNT_117 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(197);
    return RNTRNTRNT_117;
  }
}
