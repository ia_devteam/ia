extern alias MyAssembly01;
extern alias MyAssembly02;
using System;
using MyAssembly02.Namespace1;
using MyAssembly02;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(51);
    if (MyClass1.StaticMethod() != 2) {
      System.Int32 RNTRNTRNT_44 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(52);
      return RNTRNTRNT_44;
    }
    if (GlobalClass.StaticMethod() != 2) {
      System.Int32 RNTRNTRNT_45 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(53);
      return RNTRNTRNT_45;
    }
    if (MyAssembly01.GlobalClass.StaticMethod() != 1) {
      System.Int32 RNTRNTRNT_46 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      return RNTRNTRNT_46;
    }
    if (MyAssembly02.GlobalClass.StaticMethod() != 2) {
      System.Int32 RNTRNTRNT_47 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(55);
      return RNTRNTRNT_47;
    }
    System.Int32 RNTRNTRNT_48 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(56);
    return RNTRNTRNT_48;
  }
}
