using System;
class MyClass
{
  delegate bool IsAnything(Char c);
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(80);
    IsAnything validDigit;
    validDigit = new IsAnything(Char.IsDigit);
    System.Int32 RNTRNTRNT_39 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(81);
    return RNTRNTRNT_39;
  }
}
