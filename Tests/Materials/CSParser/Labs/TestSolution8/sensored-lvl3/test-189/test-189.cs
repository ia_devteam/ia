using System;
class MyTest
{
  public static int Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(813);
    if (m(1, 2) != 0) {
      System.Int32 RNTRNTRNT_432 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(814);
      return RNTRNTRNT_432;
    }
    MonoTest2 test = new MonoTest2();
    if (test.method1("some message", "some string") != 0) {
      System.Int32 RNTRNTRNT_433 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(815);
      return RNTRNTRNT_433;
    }
    System.Int32 RNTRNTRNT_434 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(816);
    return RNTRNTRNT_434;
  }
  public static int m(int a, double b)
  {
    System.Int32 RNTRNTRNT_435 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(817);
    return RNTRNTRNT_435;
  }
  public static int m(int x0, params int[] xr)
  {
    System.Int32 RNTRNTRNT_436 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(818);
    return RNTRNTRNT_436;
  }
}
public class MonoTest
{
  public virtual int method1(string message, params object[] args)
  {
    System.Int32 RNTRNTRNT_437 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(819);
    return RNTRNTRNT_437;
  }
  public void testmethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(820);
    method1("some message", "some string");
    IACSharpSensor.IACSharpSensor.SensorReached(821);
  }
}
public class MonoTest2 : MonoTest
{
  public override int method1(string message, params object[] args)
  {
    System.Int32 RNTRNTRNT_438 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(822);
    return RNTRNTRNT_438;
  }
  public void testmethod2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(823);
    method1("some message ", "some string");
    IACSharpSensor.IACSharpSensor.SensorReached(824);
  }
}
