using System;
using System.Reflection;
namespace Test
{
  public class MyAttribute : Attribute
  {
    public string val;
    public MyAttribute(string stuff)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(498);
      System.Console.WriteLine(stuff);
      val = stuff;
      IACSharpSensor.IACSharpSensor.SensorReached(499);
    }
  }
  public interface ITest
  {
    string TestProperty {
      [My("testifaceproperty")]
      get;
    }
  }
  [My("testclass")]
  public class Test
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(500);
      System.Reflection.MemberInfo info = typeof(Test);
      object[] attributes = info.GetCustomAttributes(false);
      for (int i = 0; i < attributes.Length; i++) {
        System.Console.WriteLine(attributes[i]);
      }
      if (attributes.Length != 1) {
        System.Int32 RNTRNTRNT_260 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(501);
        return RNTRNTRNT_260;
      }
      MyAttribute attr = (MyAttribute)attributes[0];
      if (attr.val != "testclass") {
        System.Int32 RNTRNTRNT_261 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(502);
        return RNTRNTRNT_261;
      }
      info = typeof(ITest).GetMethod("get_TestProperty");
      attributes = info.GetCustomAttributes(false);
      for (int i = 0; i < attributes.Length; i++) {
        System.Console.WriteLine(attributes[i]);
      }
      if (attributes.Length != 1) {
        System.Int32 RNTRNTRNT_262 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(503);
        return RNTRNTRNT_262;
      }
      attr = (MyAttribute)attributes[0];
      if (attr.val != "testifaceproperty") {
        System.Int32 RNTRNTRNT_263 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(504);
        return RNTRNTRNT_263;
      }
      System.Int32 RNTRNTRNT_264 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(505);
      return RNTRNTRNT_264;
    }
  }
}
