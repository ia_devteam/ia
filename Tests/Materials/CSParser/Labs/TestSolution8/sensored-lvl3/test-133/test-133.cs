using System;
public struct S
{
  public int a, b;
}
class T
{
  enum OpCode : ushort
  {
    False
  }
  enum OpFlags : ushort
  {
    None
  }
  static void DecodeOp(ushort word, out OpCode op, out OpFlags flags)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(185);
    op = (OpCode)(word & 0xff);
    flags = (OpFlags)(word & 0xff00);
    IACSharpSensor.IACSharpSensor.SensorReached(186);
  }
  static void get_struct(out S s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(187);
    S ss;
    ss.a = 1;
    ss.b = 2;
    s = ss;
    IACSharpSensor.IACSharpSensor.SensorReached(188);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(189);
    OpCode op;
    OpFlags flags;
    S s;
    DecodeOp((ushort)0x203, out op, out flags);
    if (op != (OpCode)0x3) {
      System.Int32 RNTRNTRNT_95 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(190);
      return RNTRNTRNT_95;
    }
    if (flags != (OpFlags)0x200) {
      System.Int32 RNTRNTRNT_96 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(191);
      return RNTRNTRNT_96;
    }
    get_struct(out s);
    if (s.a != 1) {
      System.Int32 RNTRNTRNT_97 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(192);
      return RNTRNTRNT_97;
    }
    if (s.b != 2) {
      System.Int32 RNTRNTRNT_98 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(193);
      return RNTRNTRNT_98;
    }
    System.Int32 RNTRNTRNT_99 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(194);
    return RNTRNTRNT_99;
  }
}
