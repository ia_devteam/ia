using System;
public interface Interface
{
  int X { get; }
}
public struct Struct : Interface
{
  public Struct(int x)
  {
  }
  public int X {
    get {
      System.Int32 RNTRNTRNT_418 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(783);
      return RNTRNTRNT_418;
    }
  }
}
public class User
{
  public User(Interface iface)
  {
  }
}
public class Test
{
  User t;
  Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(784);
    t = new User(new Struct(5));
    IACSharpSensor.IACSharpSensor.SensorReached(785);
  }
  User t2 = new User(new Struct(251));
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(786);
    Test tt = new Test();
    System.Int32 RNTRNTRNT_419 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(787);
    return RNTRNTRNT_419;
  }
}
