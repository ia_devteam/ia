using System;
using System.Reflection;
[AttributeUsage(AttributeTargets.All)]
public class My : Attribute
{
  public object o;
  public My(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(506);
    this.o = o;
    IACSharpSensor.IACSharpSensor.SensorReached(507);
  }
  [My(TypeCode.Empty)]
  public class Test
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(508);
      System.Reflection.MemberInfo info = typeof(Test);
      object[] attributes = info.GetCustomAttributes(false);
      for (int i = 0; i < attributes.Length; i++) {
        System.Console.WriteLine(attributes[i]);
      }
      if (attributes.Length != 1) {
        System.Int32 RNTRNTRNT_265 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(509);
        return RNTRNTRNT_265;
      }
      My attr = (My)attributes[0];
      if ((TypeCode)attr.o != TypeCode.Empty) {
        System.Int32 RNTRNTRNT_266 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(510);
        return RNTRNTRNT_266;
      }
      System.Int32 RNTRNTRNT_267 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(511);
      return RNTRNTRNT_267;
    }
  }
}
