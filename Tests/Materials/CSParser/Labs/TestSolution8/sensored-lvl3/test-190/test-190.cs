class A
{
  private int foo = 0;
  class B : A
  {
    void Test()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(825);
      foo = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(826);
    }
  }
  class C
  {
    void Test(A a)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(827);
      a.foo = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(828);
    }
  }
  static void Main()
  {
  }
}
