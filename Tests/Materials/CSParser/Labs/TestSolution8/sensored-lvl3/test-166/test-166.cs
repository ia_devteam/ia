using System;
interface ITargetInfo
{
  int TargetIntegerSize { get; }
}
interface ITargetMemoryAccess : ITargetInfo
{
}
interface IInferior : ITargetMemoryAccess
{
}
interface ITest
{
  int this[int index] { get; }
}
class Test : ITest
{
  public int this[int index] {
    get {
      System.Int32 RNTRNTRNT_288 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(568);
      return RNTRNTRNT_288;
    }
  }
  int ITest.this[int index] {
    get {
      System.Int32 RNTRNTRNT_289 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(569);
      return RNTRNTRNT_289;
    }
  }
}
class D : IInferior
{
  public int TargetIntegerSize {
    get {
      System.Int32 RNTRNTRNT_290 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(570);
      return RNTRNTRNT_290;
    }
  }
  int Hello(IInferior inferior)
  {
    System.Int32 RNTRNTRNT_291 = inferior.TargetIntegerSize;
    IACSharpSensor.IACSharpSensor.SensorReached(571);
    return RNTRNTRNT_291;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(572);
    D d = new D();
    if (d.Hello(d) != 5) {
      System.Int32 RNTRNTRNT_292 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(573);
      return RNTRNTRNT_292;
    }
    Test test = new Test();
    ITest itest = test;
    if (test[0] != 5) {
      System.Int32 RNTRNTRNT_293 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(574);
      return RNTRNTRNT_293;
    }
    if (itest[0] != 8) {
      System.Int32 RNTRNTRNT_294 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(575);
      return RNTRNTRNT_294;
    }
    System.Int32 RNTRNTRNT_295 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(576);
    return RNTRNTRNT_295;
  }
}
