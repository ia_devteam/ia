using System;
using System.Collections;
public class X
{
  public static int Main()
  {
    System.Int32 RNTRNTRNT_220 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(391);
    return RNTRNTRNT_220;
  }
  public static void test1(out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(392);
    throw new NotSupportedException();
  }
  public static void test2(int a, out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(393);
    while (a > 0) {
      if (a == 5) {
        continue;
      }
      Console.WriteLine(a);
    }
    f = 8.53f;
    IACSharpSensor.IACSharpSensor.SensorReached(394);
  }
  public static void test3(long[] b, int c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(395);
    ICollection a;
    if (b == null) {
      throw new ArgumentException();
    } else {
      a = (ICollection)b;
    }
    Console.WriteLine(a);
    IACSharpSensor.IACSharpSensor.SensorReached(396);
  }
  public static int test4(int b, out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(397);
    long a;
    Console.WriteLine("Hello World");
    a = 5;
    goto World;
    World:
    Console.WriteLine(a);
    f = 8.53f;
    System.Int32 RNTRNTRNT_221 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(398);
    return RNTRNTRNT_221;
  }
  public static int test5(out float f, long d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(399);
    int a;
    long b = 8;
    try {
      f = 8.53f;
      if (d == 500) {
        System.Int32 RNTRNTRNT_222 = 9;
        IACSharpSensor.IACSharpSensor.SensorReached(400);
        return RNTRNTRNT_222;
      }
      a = 5;
    } catch (NotSupportedException e) {
      a = 9;
    } catch (Exception e) {
      System.Int32 RNTRNTRNT_223 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(401);
      return RNTRNTRNT_223;
    } finally {
      f = 9.234f;
    }
    System.Int32 RNTRNTRNT_224 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(402);
    return RNTRNTRNT_224;
  }
  public static int test6(out float f)
  {
    System.Int32 RNTRNTRNT_225 = test5(out f, 50);
    IACSharpSensor.IACSharpSensor.SensorReached(403);
    return RNTRNTRNT_225;
  }
  public static long test7(int[] a, int stop)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(404);
    long b = 0;
    foreach (int i in a) {
      b += i;
    }
    for (int i = 1; i < stop; i++) {
      b *= i;
    }
    System.Int64 RNTRNTRNT_226 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(405);
    return RNTRNTRNT_226;
  }
  public static long test8(int stop)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(406);
    int i;
    long b;
    for (i = 1; (b = stop) > 3; i++) {
      stop--;
      b += i;
    }
    System.Int64 RNTRNTRNT_227 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    return RNTRNTRNT_227;
  }
  public static long test9(int stop)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(408);
    long b;
    while ((b = stop) > 3) {
      stop--;
      b += stop;
    }
    System.Int64 RNTRNTRNT_228 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(409);
    return RNTRNTRNT_228;
  }
  public static void test10(int a, out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(410);
    if (a == 5) {
      f = 8.53f;
      IACSharpSensor.IACSharpSensor.SensorReached(411);
      return;
    }
    f = 9.0f;
    IACSharpSensor.IACSharpSensor.SensorReached(412);
  }
  public static long test11(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    long b;
    switch (a) {
      case 5:
        b = 1;
        break;
      case 9:
        b = 3;
        break;
      default:
        System.Int64 RNTRNTRNT_229 = 9;
        IACSharpSensor.IACSharpSensor.SensorReached(414);
        return RNTRNTRNT_229;
    }
    System.Int64 RNTRNTRNT_230 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(415);
    return RNTRNTRNT_230;
  }
  public static void test12(out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(416);
    try {
      f = 9.0f;
    } catch {
      throw new NotSupportedException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(417);
  }
  public static void test13(int a, out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(418);
    do {
      if (a == 8) {
        f = 8.5f;
        IACSharpSensor.IACSharpSensor.SensorReached(419);
        return;
      }
    } while (false);
    f = 1.3f;
    IACSharpSensor.IACSharpSensor.SensorReached(420);
    return;
  }
  public static long test14(int a, out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(421);
    long b;
    switch (a) {
      case 1:
        goto case 2;
      case 2:
        f = 9.53f;
        System.Int64 RNTRNTRNT_231 = 9;
        IACSharpSensor.IACSharpSensor.SensorReached(422);
        return RNTRNTRNT_231;
      case 3:
        goto default;
      default:
        b = 10;
        break;
    }
    f = 10.0f;
    System.Int64 RNTRNTRNT_232 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(423);
    return RNTRNTRNT_232;
  }
  public static int test15(int b, out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(424);
    long a;
    Console.WriteLine("Hello World");
    a = 5;
    f = 8.53f;
    goto World;
    World:
    Console.WriteLine(a);
    System.Int32 RNTRNTRNT_233 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(425);
    return RNTRNTRNT_233;
  }
  public static void test16()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(426);
    int value;
    for (int i = 0; i < 5; ++i) {
      if (i == 0) {
        continue;
      } else if (i == 1) {
        value = 2;
      } else {
        value = 0;
      }
      if (value > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(427);
        return;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(428);
  }
  public static void test17()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(429);
    int value;
    long charCount = 9;
    long testit = 5;
    while (charCount > 0) {
      --charCount;
      if (testit == 8) {
        if (testit == 9) {
          throw new Exception();
        }
        continue;
      } else {
        value = 0;
      }
      Console.WriteLine(value);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(430);
  }
  static void test18(int a, out int f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(431);
    try {
      if (a == 5) {
        throw new Exception();
      }
      f = 9;
    } catch (IndexOutOfRangeException) {
      throw new FormatException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(432);
  }
  static int test19()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(433);
    int res;
    int a = Environment.NewLine.Length;
    int fin = 0;
    try {
      res = 10 / a;
      throw new NotImplementedException();
    } catch (NotImplementedException e) {
      fin = 2;
      throw new NotImplementedException();
    } finally {
      fin = 1;
    }
    System.Int32 RNTRNTRNT_234 = fin;
    IACSharpSensor.IACSharpSensor.SensorReached(434);
    return RNTRNTRNT_234;
  }
  static int test20()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(435);
    try {
      System.Int32 RNTRNTRNT_235 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(436);
      return RNTRNTRNT_235;
    } catch (Exception) {
      throw;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(437);
  }
  static int test21()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(438);
    int res;
    try {
      res = 4;
      System.Int32 RNTRNTRNT_236 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(439);
      return RNTRNTRNT_236;
    } catch (DivideByZeroException) {
      res = 33;
    } finally {
    }
    System.Int32 RNTRNTRNT_237 = res;
    IACSharpSensor.IACSharpSensor.SensorReached(440);
    return RNTRNTRNT_237;
  }
  static int test22()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(441);
    int res;
    try {
      res = 4;
      System.Int32 RNTRNTRNT_238 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(442);
      return RNTRNTRNT_238;
    } catch (DivideByZeroException) {
      res = 33;
    }
    System.Int32 RNTRNTRNT_239 = res;
    IACSharpSensor.IACSharpSensor.SensorReached(443);
    return RNTRNTRNT_239;
  }
  static int test23(object obj, int a, out bool test)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(444);
    if (obj == null) {
      throw new ArgumentNullException();
    }
    if (a == 5) {
      test = false;
      System.Int32 RNTRNTRNT_240 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(445);
      return RNTRNTRNT_240;
    } else {
      test = true;
      System.Int32 RNTRNTRNT_241 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(446);
      return RNTRNTRNT_241;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(447);
  }
  static long test24(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(448);
    long b;
    switch (a) {
      case 0:
        System.Int64 RNTRNTRNT_242 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(449);
        return RNTRNTRNT_242;
    }
    if (a > 2) {
      if (a == 5) {
        b = 4;
      } else if (a == 6) {
        b = 5;
      } else {
        System.Int64 RNTRNTRNT_243 = 7;
        IACSharpSensor.IACSharpSensor.SensorReached(450);
        return RNTRNTRNT_243;
      }
      Console.WriteLine(b);
      System.Int64 RNTRNTRNT_244 = b;
      IACSharpSensor.IACSharpSensor.SensorReached(451);
      return RNTRNTRNT_244;
    }
    System.Int64 RNTRNTRNT_245 = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(452);
    return RNTRNTRNT_245;
  }
  static long test25(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(453);
    long b, c;
    try {
      b = 5;
    } catch (NotSupportedException) {
      throw new InvalidOperationException();
    }
    try {
      c = 5;
    } catch {
      throw new InvalidOperationException();
    }
    System.Int64 RNTRNTRNT_246 = b + c;
    IACSharpSensor.IACSharpSensor.SensorReached(454);
    return RNTRNTRNT_246;
  }
  static void test26()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(455);
    int j;
    for (int i = 0; i < 10; i = j) {
      j = i + 1;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(456);
  }
  static int test27()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(457);
    while (true) {
      break;
      while (true) {
        Console.WriteLine("Test");
      }
    }
    System.Int32 RNTRNTRNT_247 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(458);
    return RNTRNTRNT_247;
  }
  static void test28(out object value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(459);
    if (true) {
      try {
        value = null;
        IACSharpSensor.IACSharpSensor.SensorReached(460);
        return;
      } catch {
      }
    }
    value = null;
    IACSharpSensor.IACSharpSensor.SensorReached(461);
  }
  static bool test29(out int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(462);
    try {
      a = 0;
      System.Boolean RNTRNTRNT_248 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(463);
      return RNTRNTRNT_248;
    } catch (System.Exception) {
      a = -1;
      System.Boolean RNTRNTRNT_249 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(464);
      return RNTRNTRNT_249;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(465);
  }
  public string test30(out string outparam)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(466);
    try {
      if (true) {
        outparam = "";
        System.String RNTRNTRNT_250 = "";
        IACSharpSensor.IACSharpSensor.SensorReached(467);
        return RNTRNTRNT_250;
      }
    } catch {
    }
    outparam = null;
    IACSharpSensor.IACSharpSensor.SensorReached(468);
    return null;
  }
  public string test31(int blah)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(469);
    switch (blah) {
      case 1:
        System.String RNTRNTRNT_251 = ("foo");
        IACSharpSensor.IACSharpSensor.SensorReached(470);
        return RNTRNTRNT_251;
        break;
      case 2:
        System.String RNTRNTRNT_252 = ("bar");
        IACSharpSensor.IACSharpSensor.SensorReached(471);
        return RNTRNTRNT_252;
        break;
      case 3:
        System.String RNTRNTRNT_253 = ("baz");
        IACSharpSensor.IACSharpSensor.SensorReached(472);
        return RNTRNTRNT_253;
        break;
      default:
        throw new ArgumentException("Value 0x" + blah.ToString("x4") + " is not supported.");
    }
  }
  public void test32()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(473);
    while (true) {
      System.Threading.Thread.Sleep(1);
    }
    Console.WriteLine("Hello");
    IACSharpSensor.IACSharpSensor.SensorReached(474);
  }
  public int test33()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(475);
    int i = 0;
    System.Int32 RNTRNTRNT_254 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(476);
    return RNTRNTRNT_254;
    if (i == 0) {
      System.Int32 RNTRNTRNT_255 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(477);
      return RNTRNTRNT_255;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(478);
  }
  public void test34()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(479);
    int y, x = 3;
    if (x > 3) {
      y = 3;
      goto end;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(480);
    return;
    end:
    x = y;
    IACSharpSensor.IACSharpSensor.SensorReached(481);
  }
  public static void test35(int a, bool test)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(482);
    switch (a) {
      case 3:
        if (test) {
          break;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(483);
        return;
      default:
        IACSharpSensor.IACSharpSensor.SensorReached(484);
        return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(485);
  }
  public static void test36()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(486);
    string myVar;
    int counter = 0;
    while (true) {
      if (counter < 3) {
        counter++;
      } else {
        myVar = "assigned";
        break;
      }
    }
    Console.WriteLine(myVar);
    IACSharpSensor.IACSharpSensor.SensorReached(487);
  }
  public static void test37()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(488);
    int x = 0;
    int y = 0;
    switch (x) {
      case 0:
        switch (y) {
          case 0:
            goto k_0;
          default:
            throw new Exception();
        }
    }
    k_0:
    ;
    IACSharpSensor.IACSharpSensor.SensorReached(489);
  }
  public static int test38()
  {
    System.Int32 RNTRNTRNT_256 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(490);
    return RNTRNTRNT_256;
    foo:
    ;
    IACSharpSensor.IACSharpSensor.SensorReached(491);
  }
  static int test40(int stop)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(492);
    int service;
    int pos = 0;
    do {
      service = 1;
      break;
    } while (pos < stop);
    System.Int32 RNTRNTRNT_257 = service;
    IACSharpSensor.IACSharpSensor.SensorReached(493);
    return RNTRNTRNT_257;
  }
}
