using System;
class Base
{
  int value;
  public int Value {
    get {
      System.Int32 RNTRNTRNT_364 = value;
      IACSharpSensor.IACSharpSensor.SensorReached(664);
      return RNTRNTRNT_364;
    }
  }
  protected Base(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(665);
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(666);
  }
}
class A : Base
{
  public A(int value) : base(1)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(667);
    Console.WriteLine("Int");
    IACSharpSensor.IACSharpSensor.SensorReached(668);
  }
  public A(uint value) : base(2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(669);
    Console.WriteLine("UInt");
    IACSharpSensor.IACSharpSensor.SensorReached(670);
  }
}
class B : Base
{
  public B(long value) : base(3)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(671);
    Console.WriteLine("Long");
    IACSharpSensor.IACSharpSensor.SensorReached(672);
  }
  public B(ulong value) : base(4)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(673);
    Console.WriteLine("ULong");
    IACSharpSensor.IACSharpSensor.SensorReached(674);
  }
}
class C : Base
{
  public C(short value) : base(5)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(675);
    Console.WriteLine("Short");
    IACSharpSensor.IACSharpSensor.SensorReached(676);
  }
  public C(ushort value) : base(6)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(677);
    Console.WriteLine("UShort");
    IACSharpSensor.IACSharpSensor.SensorReached(678);
  }
}
class D : Base
{
  public D(sbyte value) : base(7)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(679);
    Console.WriteLine("SByte");
    IACSharpSensor.IACSharpSensor.SensorReached(680);
  }
  public D(byte value) : base(8)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(681);
    Console.WriteLine("Byte");
    IACSharpSensor.IACSharpSensor.SensorReached(682);
  }
}
class E : Base
{
  public E(long value) : base(9)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(683);
    Console.WriteLine("Long");
    IACSharpSensor.IACSharpSensor.SensorReached(684);
  }
  public E(E e) : base(10)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(685);
    Console.WriteLine("E");
    IACSharpSensor.IACSharpSensor.SensorReached(686);
  }
  public static implicit operator E(long value)
  {
    E RNTRNTRNT_365 = (new E(value));
    IACSharpSensor.IACSharpSensor.SensorReached(687);
    return RNTRNTRNT_365;
  }
}
class F : Base
{
  public F(int value) : base(11)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(688);
    Console.WriteLine("Int");
    IACSharpSensor.IACSharpSensor.SensorReached(689);
  }
  public F(F f) : base(12)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(690);
    Console.WriteLine("F");
    IACSharpSensor.IACSharpSensor.SensorReached(691);
  }
  public static implicit operator F(int value)
  {
    F RNTRNTRNT_366 = (new F(value));
    IACSharpSensor.IACSharpSensor.SensorReached(692);
    return RNTRNTRNT_366;
  }
}
class X
{
  static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(697);
    {
      A a = new A(4);
      if (a.Value != 1) {
        System.Int32 RNTRNTRNT_367 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(693);
        return RNTRNTRNT_367;
      }
      B b = new B(4);
      if (b.Value != 3) {
        System.Int32 RNTRNTRNT_368 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(694);
        return RNTRNTRNT_368;
      }
      C c = new C(4);
      if (c.Value != 5) {
        System.Int32 RNTRNTRNT_369 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(695);
        return RNTRNTRNT_369;
      }
      D d = new D(4);
      if (d.Value != 7) {
        System.Int32 RNTRNTRNT_370 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(696);
        return RNTRNTRNT_370;
      }
    }
    {
      A a = new A(4u);
      if (a.Value != 2) {
        System.Int32 RNTRNTRNT_371 = 5;
        IACSharpSensor.IACSharpSensor.SensorReached(698);
        return RNTRNTRNT_371;
      }
      B b = new B(4u);
      if (b.Value != 3) {
        System.Int32 RNTRNTRNT_372 = 6;
        IACSharpSensor.IACSharpSensor.SensorReached(699);
        return RNTRNTRNT_372;
      }
      C c = new C(4);
      if (c.Value != 5) {
        System.Int32 RNTRNTRNT_373 = 7;
        IACSharpSensor.IACSharpSensor.SensorReached(700);
        return RNTRNTRNT_373;
      }
      D d = new D(4);
      if (d.Value != 7) {
        System.Int32 RNTRNTRNT_374 = 8;
        IACSharpSensor.IACSharpSensor.SensorReached(701);
        return RNTRNTRNT_374;
      }
    }
    {
      E e = new E(4);
      if (e.Value != 9) {
        System.Int32 RNTRNTRNT_375 = 9;
        IACSharpSensor.IACSharpSensor.SensorReached(702);
        return RNTRNTRNT_375;
      }
      F f = new F(4);
      if (f.Value != 11) {
        System.Int32 RNTRNTRNT_376 = 10;
        IACSharpSensor.IACSharpSensor.SensorReached(703);
        return RNTRNTRNT_376;
      }
    }
    System.Int32 RNTRNTRNT_377 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(704);
    return RNTRNTRNT_377;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(705);
    int result = Test();
    Console.WriteLine("RESULT: {0}", result);
    System.Int32 RNTRNTRNT_378 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(706);
    return RNTRNTRNT_378;
  }
}
