interface IA
{
  void A();
}
interface IB : IA
{
  void B();
}
interface IC : IA, IB
{
  void C();
}
interface ID : IC
{
}
class AA : IC
{
  bool a, b, c;
  public void A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(195);
    a = true;
    IACSharpSensor.IACSharpSensor.SensorReached(196);
  }
  public void B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(197);
    b = true;
    IACSharpSensor.IACSharpSensor.SensorReached(198);
  }
  public void C()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(199);
    c = true;
    IACSharpSensor.IACSharpSensor.SensorReached(200);
  }
  public bool OK {
    get {
      System.Boolean RNTRNTRNT_100 = a && b && c;
      IACSharpSensor.IACSharpSensor.SensorReached(201);
      return RNTRNTRNT_100;
    }
  }
}
class BB : ID
{
  bool a, b, c;
  public void A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(202);
    a = true;
    System.Console.WriteLine("A");
    IACSharpSensor.IACSharpSensor.SensorReached(203);
  }
  public void B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(204);
    b = true;
    IACSharpSensor.IACSharpSensor.SensorReached(205);
  }
  public void C()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    c = true;
    IACSharpSensor.IACSharpSensor.SensorReached(207);
  }
  public bool OK {
    get {
      System.Boolean RNTRNTRNT_101 = a && b && c;
      IACSharpSensor.IACSharpSensor.SensorReached(208);
      return RNTRNTRNT_101;
    }
  }
}
class T : IB
{
  public void A()
  {
  }
  public void B()
  {
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(209);
    BB bb = new BB();
    bb.A();
    bb.B();
    bb.C();
    if (!bb.OK) {
      System.Int32 RNTRNTRNT_102 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(210);
      return RNTRNTRNT_102;
    }
    AA aa = new AA();
    aa.A();
    aa.B();
    aa.C();
    if (!aa.OK) {
      System.Int32 RNTRNTRNT_103 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(211);
      return RNTRNTRNT_103;
    }
    System.Int32 RNTRNTRNT_104 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(212);
    return RNTRNTRNT_104;
  }
}
