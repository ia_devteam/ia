using System;
public interface IDiagnostic
{
  void Stop();
}
public interface IAutomobile
{
  void Stop();
}
public class MyCar : IAutomobile, IDiagnostic
{
  public bool diag_stop, car_stop, auto_stop;
  void IDiagnostic.Stop()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(218);
    diag_stop = true;
    IACSharpSensor.IACSharpSensor.SensorReached(219);
  }
  public void Stop()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    car_stop = true;
    IAutomobile self = (IAutomobile)this;
    self.Stop();
    IACSharpSensor.IACSharpSensor.SensorReached(221);
  }
  void IAutomobile.Stop()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(222);
    auto_stop = true;
    IACSharpSensor.IACSharpSensor.SensorReached(223);
  }
}
class TestConflict
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(224);
    MyCar car1 = new MyCar();
    car1.Stop();
    IDiagnostic car2 = new MyCar();
    car2.Stop();
    IAutomobile car3 = new MyCar();
    car3.Stop();
    if (!car1.car_stop) {
      System.Int32 RNTRNTRNT_109 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(225);
      return RNTRNTRNT_109;
    }
    if (car1.diag_stop) {
      System.Int32 RNTRNTRNT_110 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(226);
      return RNTRNTRNT_110;
    }
    Console.WriteLine("ok");
    System.Int32 RNTRNTRNT_111 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(227);
    return RNTRNTRNT_111;
  }
}
