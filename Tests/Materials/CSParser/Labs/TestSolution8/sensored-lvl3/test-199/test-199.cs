public class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(857);
    N1.Foo.Foo2 foo2 = new N1.Foo.Foo2();
    if (foo2.Talk() != 1) {
      System.Int32 RNTRNTRNT_452 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(858);
      return RNTRNTRNT_452;
    }
    System.Int32 RNTRNTRNT_453 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(859);
    return RNTRNTRNT_453;
  }
}
namespace N1
{
  public class Foo : N2.Bar
  {
    public class Foo2 : Bar2
    {
    }
  }
  public class Bar2
  {
    public int Talk()
    {
      System.Int32 RNTRNTRNT_454 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(860);
      return RNTRNTRNT_454;
    }
  }
}
namespace N2
{
  public class Bar
  {
    private class Bar2
    {
      public int Talk()
      {
        System.Int32 RNTRNTRNT_455 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(861);
        return RNTRNTRNT_455;
      }
    }
  }
}
