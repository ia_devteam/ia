using System;
namespace test
{
  public class test
  {
    static int test_method(int vv)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(57);
      byte b = 45;
      b |= (byte)(vv << 1);
      System.Int32 RNTRNTRNT_29 = b;
      IACSharpSensor.IACSharpSensor.SensorReached(58);
      return RNTRNTRNT_29;
    }
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(59);
      if (test_method(1) != 47) {
        System.Int32 RNTRNTRNT_30 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(60);
        return RNTRNTRNT_30;
      }
      System.Int32 RNTRNTRNT_31 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(61);
      return RNTRNTRNT_31;
    }
  }
}
