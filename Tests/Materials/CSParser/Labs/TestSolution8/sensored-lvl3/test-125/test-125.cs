using System;
public class Fraction
{
  public Fraction(int numerator, int denominator)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(132);
    Console.WriteLine("In Fraction Constructor(int, int)");
    this.numerator = numerator;
    this.denominator = denominator;
    IACSharpSensor.IACSharpSensor.SensorReached(133);
  }
  public Fraction(int wholeNumber)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(134);
    Console.WriteLine("In Fraction Constructor(int)");
    numerator = wholeNumber;
    denominator = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(135);
  }
  public static implicit operator Fraction(int theInt)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(136);
    Console.WriteLine("In implicit conversion to Fraction");
    Fraction RNTRNTRNT_67 = new Fraction(theInt);
    IACSharpSensor.IACSharpSensor.SensorReached(137);
    return RNTRNTRNT_67;
  }
  public static explicit operator int(Fraction theFraction)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(138);
    Console.WriteLine("In explicit conversion to int");
    System.Int32 RNTRNTRNT_68 = theFraction.numerator / theFraction.denominator;
    IACSharpSensor.IACSharpSensor.SensorReached(139);
    return RNTRNTRNT_68;
  }
  public static bool operator ==(Fraction lhs, Fraction rhs)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(140);
    Console.WriteLine("In operator ==");
    if (lhs.denominator == rhs.denominator && lhs.numerator == rhs.numerator) {
      System.Boolean RNTRNTRNT_69 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(141);
      return RNTRNTRNT_69;
    }
    System.Boolean RNTRNTRNT_70 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(142);
    return RNTRNTRNT_70;
  }
  public static bool operator !=(Fraction lhs, Fraction rhs)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(143);
    Console.WriteLine("In operator !=");
    System.Boolean RNTRNTRNT_71 = !(lhs == rhs);
    IACSharpSensor.IACSharpSensor.SensorReached(144);
    return RNTRNTRNT_71;
  }
  public override bool Equals(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(145);
    Console.WriteLine("In method Equals");
    if (!(o is Fraction)) {
      System.Boolean RNTRNTRNT_72 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(146);
      return RNTRNTRNT_72;
    }
    System.Boolean RNTRNTRNT_73 = this == (Fraction)o;
    IACSharpSensor.IACSharpSensor.SensorReached(147);
    return RNTRNTRNT_73;
  }
  public static Fraction operator +(Fraction lhs, Fraction rhs)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(148);
    Console.WriteLine("In operator +");
    if (lhs.denominator == rhs.denominator) {
      Fraction RNTRNTRNT_74 = new Fraction(lhs.numerator + rhs.numerator, lhs.denominator);
      IACSharpSensor.IACSharpSensor.SensorReached(149);
      return RNTRNTRNT_74;
    }
    int firstProduct = lhs.numerator * rhs.denominator;
    int secondProduct = lhs.denominator * rhs.numerator;
    Fraction RNTRNTRNT_75 = new Fraction(firstProduct + secondProduct, lhs.denominator * rhs.denominator);
    IACSharpSensor.IACSharpSensor.SensorReached(150);
    return RNTRNTRNT_75;
  }
  public override string ToString()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(151);
    String s = numerator.ToString() + "/" + denominator.ToString();
    System.String RNTRNTRNT_76 = s;
    IACSharpSensor.IACSharpSensor.SensorReached(152);
    return RNTRNTRNT_76;
  }
  private int numerator;
  private int denominator;
}
public class Tester
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(153);
    Fraction f1 = new Fraction(3, 4);
    Console.WriteLine("f1: {0}", f1.ToString());
    Fraction f2 = new Fraction(2, 4);
    Console.WriteLine("f2: {0}", f2.ToString());
    Fraction f3 = f1 + f2;
    Console.WriteLine("f1 + f2 = f3: {0}", f3.ToString());
    Fraction f4 = f3 + 5;
    Console.WriteLine("f3 + 5 = f4: {0}", f4.ToString());
    Fraction f5 = new Fraction(2, 4);
    if (f5 == f2) {
      Console.WriteLine("f5: {0} == f2: {1}", f5.ToString(), f2.ToString());
    }
    System.Int32 RNTRNTRNT_77 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(154);
    return RNTRNTRNT_77;
  }
}
