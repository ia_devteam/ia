using System;
using System.Collections;
public interface IFoo
{
}
public class Blah : IFoo
{
  Hashtable table;
  public Blah()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(89);
    table = new Hashtable();
    IACSharpSensor.IACSharpSensor.SensorReached(90);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(91);
    Blah b = new Blah();
    b.table.Add("Ravi", (IFoo)b);
    System.Int32 RNTRNTRNT_44 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(92);
    return RNTRNTRNT_44;
  }
}
