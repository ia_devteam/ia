using System;
struct A
{
  public int a;
  private long b;
  private float c;
  public A(int foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(520);
    a = foo;
    b = 8;
    c = 9.0f;
    IACSharpSensor.IACSharpSensor.SensorReached(521);
  }
}
struct B
{
  public int a;
}
struct C
{
  public long b;
  public C(long foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(522);
    b = foo;
    IACSharpSensor.IACSharpSensor.SensorReached(523);
  }
  public C(string foo) : this(500)
  {
  }
}
struct D
{
  public int foo;
}
struct E
{
  public D d;
  public bool e;
  public E(int foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(524);
    this.e = true;
    this.d.foo = 9;
    IACSharpSensor.IACSharpSensor.SensorReached(525);
  }
}
struct F
{
  public E e;
  public float f;
}
class X
{
  static void test_output(A x)
  {
  }
  static void test_output(B y)
  {
  }
  static void test_output(E e)
  {
  }
  static void test_output(F f)
  {
  }
  static void test1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(526);
    A x;
    x.a = 5;
    Console.WriteLine(x.a);
    IACSharpSensor.IACSharpSensor.SensorReached(527);
  }
  static void test2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(528);
    B y;
    y.a = 5;
    Console.WriteLine(y.a);
    Console.WriteLine(y);
    IACSharpSensor.IACSharpSensor.SensorReached(529);
  }
  static void test3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(530);
    A x = new A(85);
    Console.WriteLine(x);
    IACSharpSensor.IACSharpSensor.SensorReached(531);
  }
  static void test4(A x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(532);
    x.a = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(533);
  }
  static void test5(out A x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(534);
    x = new A(85);
    IACSharpSensor.IACSharpSensor.SensorReached(535);
  }
  static void test6(out B y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(536);
    y.a = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(537);
  }
  static void test7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(538);
    E e;
    e.e = true;
    e.d.foo = 5;
    test_output(e);
    IACSharpSensor.IACSharpSensor.SensorReached(539);
  }
  static void test8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(540);
    F f;
    f.e.e = true;
    f.e.d.foo = 5;
    f.f = 3.14f;
    test_output(f);
    IACSharpSensor.IACSharpSensor.SensorReached(541);
  }
  static void test9()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(542);
    E e = new E(5);
    Console.WriteLine(e.d.foo);
    IACSharpSensor.IACSharpSensor.SensorReached(543);
  }
  static void test10()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(544);
    F f;
    f.e = new E(10);
    Console.WriteLine(f.e.d.foo);
    Console.WriteLine(f.e.d);
    f.f = 3.14f;
    Console.WriteLine(f);
    IACSharpSensor.IACSharpSensor.SensorReached(545);
  }
  public static int Main()
  {
    System.Int32 RNTRNTRNT_273 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(546);
    return RNTRNTRNT_273;
  }
}
