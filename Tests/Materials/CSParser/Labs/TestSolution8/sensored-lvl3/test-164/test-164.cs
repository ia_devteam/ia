using System;
class X
{
  protected virtual int Foo()
  {
    System.Int32 RNTRNTRNT_278 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(552);
    return RNTRNTRNT_278;
  }
  protected delegate int FooDelegate();
  protected FooDelegate foo;
  protected X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(553);
    foo = new FooDelegate(Foo);
    IACSharpSensor.IACSharpSensor.SensorReached(554);
  }
}
class Y : X
{
  protected Y() : base()
  {
  }
  protected override int Foo()
  {
    System.Int32 RNTRNTRNT_279 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(555);
    return RNTRNTRNT_279;
  }
  int Hello()
  {
    System.Int32 RNTRNTRNT_280 = foo();
    IACSharpSensor.IACSharpSensor.SensorReached(556);
    return RNTRNTRNT_280;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(557);
    Y y = new Y();
    int result = y.Hello();
    if (result == 2) {
      Console.WriteLine("OK");
    } else {
      Console.WriteLine("NOT OK");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(558);
  }
}
