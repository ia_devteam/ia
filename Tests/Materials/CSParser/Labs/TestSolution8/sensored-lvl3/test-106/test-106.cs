using System;
using System.Threading;
using System.Runtime.InteropServices;
class Test
{
  delegate int SimpleDelegate(int a);
  static int cb_state = 0;
  static int F(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(37);
    Console.WriteLine("Test.F from delegate: " + a);
    throw new NotImplementedException();
  }
  static void async_callback(IAsyncResult ar)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(38);
    Console.WriteLine("Async Callback " + ar.AsyncState);
    cb_state = 1;
    throw new NotImplementedException();
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(39);
    SimpleDelegate d = new SimpleDelegate(F);
    AsyncCallback ac = new AsyncCallback(async_callback);
    string state1 = "STATE1";
    int res = 0;
    IAsyncResult ar1 = d.BeginInvoke(1, ac, state1);
    ar1.AsyncWaitHandle.WaitOne();
    try {
      res = d.EndInvoke(ar1);
    } catch (NotImplementedException) {
      res = 1;
      Console.WriteLine("received exception ... OK");
    }
    while (cb_state == 0) {
      Thread.Sleep(0);
    }
    if (cb_state != 1) {
      System.Int32 RNTRNTRNT_18 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(40);
      return RNTRNTRNT_18;
    }
    if (res != 1) {
      System.Int32 RNTRNTRNT_19 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(41);
      return RNTRNTRNT_19;
    }
    System.Int32 RNTRNTRNT_20 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    return RNTRNTRNT_20;
  }
}
