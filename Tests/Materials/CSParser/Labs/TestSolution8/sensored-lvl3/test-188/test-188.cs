using System;
public class Enumerator
{
  int counter;
  public Enumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(802);
    counter = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(803);
  }
  public bool MoveNext()
  {
    System.Boolean RNTRNTRNT_428 = (counter-- > 0);
    IACSharpSensor.IACSharpSensor.SensorReached(804);
    return RNTRNTRNT_428;
  }
  public char Current {
    get {
      System.Char RNTRNTRNT_429 = 'a';
      IACSharpSensor.IACSharpSensor.SensorReached(805);
      return RNTRNTRNT_429;
    }
  }
}
class RealEnumerator : Enumerator, IDisposable
{
  Coll c;
  public RealEnumerator(Coll c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(806);
    this.c = c;
    IACSharpSensor.IACSharpSensor.SensorReached(807);
  }
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(808);
    c.disposed = true;
    IACSharpSensor.IACSharpSensor.SensorReached(809);
  }
}
public class Coll
{
  public bool disposed;
  public Enumerator GetEnumerator()
  {
    Enumerator RNTRNTRNT_430 = new RealEnumerator(this);
    IACSharpSensor.IACSharpSensor.SensorReached(810);
    return RNTRNTRNT_430;
  }
}
class Test
{
  public static int Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(811);
    Coll coll = new Coll();
    foreach (char c in coll) {
    }
    System.Int32 RNTRNTRNT_431 = (coll.disposed ? 0 : 1);
    IACSharpSensor.IACSharpSensor.SensorReached(812);
    return RNTRNTRNT_431;
  }
}
