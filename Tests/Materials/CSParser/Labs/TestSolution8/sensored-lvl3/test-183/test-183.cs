using System;
class Test
{
  delegate int D(int x, out int y);
  static int M(int x, out int y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(777);
    y = x + 2;
    System.Int32 RNTRNTRNT_414 = ++x;
    IACSharpSensor.IACSharpSensor.SensorReached(778);
    return RNTRNTRNT_414;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(779);
    int x = 1;
    int y = 0;
    D del = new D(M);
    IAsyncResult ar = del.BeginInvoke(x, out y, null, null);
    if (del.EndInvoke(out y, ar) != 2) {
      System.Int32 RNTRNTRNT_415 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(780);
      return RNTRNTRNT_415;
    }
    if (y != 3) {
      System.Int32 RNTRNTRNT_416 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(781);
      return RNTRNTRNT_416;
    }
    Console.WriteLine("Test ok");
    System.Int32 RNTRNTRNT_417 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(782);
    return RNTRNTRNT_417;
  }
}
