class B
{
  public S s;
}
class S
{
  public int a;
}
class T
{
  static B foo;
  static int blah(object arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(515);
    B look = (B)arg;
    foo.s.a = 9;
    look.s.a = foo.s.a;
    System.Int32 RNTRNTRNT_269 = look.s.a;
    IACSharpSensor.IACSharpSensor.SensorReached(516);
    return RNTRNTRNT_269;
  }
  static int Main()
  {
    System.Int32 RNTRNTRNT_270 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(517);
    return RNTRNTRNT_270;
  }
}
