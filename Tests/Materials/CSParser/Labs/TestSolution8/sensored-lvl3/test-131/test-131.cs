using System;
public class SimpleAttribute : Attribute
{
  string n;
  public SimpleAttribute(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(178);
    n = name;
    IACSharpSensor.IACSharpSensor.SensorReached(179);
  }
}
public class Blah
{
  public enum Foo
  {
    A,
    [Simple("second")]
    B,
    C
  }
  public static int Main()
  {
    System.Int32 RNTRNTRNT_91 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(180);
    return RNTRNTRNT_91;
  }
}
