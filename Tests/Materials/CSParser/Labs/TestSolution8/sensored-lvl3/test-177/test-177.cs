using System;
using System.Reflection;
public class MethodAttribute : Attribute
{
}
public class ReturnAttribute : Attribute
{
}
public class Test
{
  [Method()]
  [return: Return()]
  public void Method()
  {
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(736);
    Type t = typeof(Test);
    MethodInfo mi = t.GetMethod("Method");
    ICustomAttributeProvider cap = mi.ReturnTypeCustomAttributes;
    if (cap != null) {
      System.Int32 RNTRNTRNT_393 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(737);
      return RNTRNTRNT_393;
    } else {
      System.Int32 RNTRNTRNT_394 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(738);
      return RNTRNTRNT_394;
    }
  }
}
