using System;
class t
{
  void a()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(124);
    int b;
    try {
      b = 1;
    } catch {
      b = 2;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(125);
  }
  void b()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(126);
    int a;
    try {
      a = 1;
    } catch (Exception) {
      a = 2;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(127);
  }
  void c()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(128);
    int a;
    try {
      a = 2;
    } catch (Exception e) {
      a = 0x3;
    } catch {
      a = 0x1;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(129);
  }
  void d()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(130);
    int a;
    try {
      a = 2;
    } catch (Exception e) {
      a = 0x3;
    } catch {
      a = 0x1;
    } finally {
      a = 111;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(131);
  }
  public static void Main()
  {
  }
}
