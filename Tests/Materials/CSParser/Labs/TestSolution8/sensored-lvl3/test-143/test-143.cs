using System;
struct MonoEnumInfo
{
  int val;
  void stuff()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(272);
    val = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(273);
  }
  static int GetInfo(out MonoEnumInfo info)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(274);
    info = new MonoEnumInfo();
    info.stuff();
    System.Int32 RNTRNTRNT_133 = info.val;
    IACSharpSensor.IACSharpSensor.SensorReached(275);
    return RNTRNTRNT_133;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(276);
    MonoEnumInfo m;
    if (GetInfo(out m) != 1) {
      System.Int32 RNTRNTRNT_134 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(277);
      return RNTRNTRNT_134;
    }
    if (m.val != 1) {
      System.Int32 RNTRNTRNT_135 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(278);
      return RNTRNTRNT_135;
    }
    System.Int32 RNTRNTRNT_136 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(279);
    return RNTRNTRNT_136;
  }
}
