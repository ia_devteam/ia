using System.ComponentModel;
using System.Reflection;
public class BrowsableClass
{
  [EditorBrowsable(EditorBrowsableState.Always)]
  public static BrowsableClass operator ++(BrowsableClass a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(739);
    return null;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(740);
    BrowsableClass c = new BrowsableClass();
    MethodInfo mi = c.GetType().GetMethod("op_Increment");
    object[] attributes = mi.GetCustomAttributes(typeof(EditorBrowsableAttribute), false);
    if (attributes.Length != 1) {
      System.Int32 RNTRNTRNT_395 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(741);
      return RNTRNTRNT_395;
    }
    System.Int32 RNTRNTRNT_396 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(742);
    return RNTRNTRNT_396;
  }
}
