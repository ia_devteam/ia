class A
{
  double d1, d2;
  public double this[double x] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(743);
      if (d1 == x) {
        System.Double RNTRNTRNT_397 = d2;
        IACSharpSensor.IACSharpSensor.SensorReached(744);
        return RNTRNTRNT_397;
      }
      System.Double RNTRNTRNT_398 = 0.0;
      IACSharpSensor.IACSharpSensor.SensorReached(745);
      return RNTRNTRNT_398;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(746);
      d1 = x;
      d2 = value;
      IACSharpSensor.IACSharpSensor.SensorReached(747);
    }
  }
}
class B : A
{
  double d1, d2;
  public new double this[double x] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(748);
      if (d1 == x) {
        System.Double RNTRNTRNT_399 = d2;
        IACSharpSensor.IACSharpSensor.SensorReached(749);
        return RNTRNTRNT_399;
      }
      System.Double RNTRNTRNT_400 = 0.0;
      IACSharpSensor.IACSharpSensor.SensorReached(750);
      return RNTRNTRNT_400;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(751);
      d1 = x;
      d2 = value;
      IACSharpSensor.IACSharpSensor.SensorReached(752);
    }
  }
}
class C : B
{
  string s1, s2;
  int i1, i2;
  public string this[string x] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(753);
      if (s1 == x) {
        System.String RNTRNTRNT_401 = s2;
        IACSharpSensor.IACSharpSensor.SensorReached(754);
        return RNTRNTRNT_401;
      }
      System.String RNTRNTRNT_402 = "";
      IACSharpSensor.IACSharpSensor.SensorReached(755);
      return RNTRNTRNT_402;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(756);
      s1 = x;
      s2 = value;
      IACSharpSensor.IACSharpSensor.SensorReached(757);
    }
  }
  public int this[int x] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(758);
      if (i1 == x) {
        System.Int32 RNTRNTRNT_403 = i2;
        IACSharpSensor.IACSharpSensor.SensorReached(759);
        return RNTRNTRNT_403;
      }
      System.Int32 RNTRNTRNT_404 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(760);
      return RNTRNTRNT_404;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(761);
      i1 = x;
      i2 = value;
      IACSharpSensor.IACSharpSensor.SensorReached(762);
    }
  }
}
struct EntryPoint
{
  public static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(763);
    C test = new C();
    test[333.333] = 444.444;
    if (test[333.333] != 444.444) {
      System.Int32 RNTRNTRNT_405 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(764);
      return RNTRNTRNT_405;
    }
    test["a string"] = "another string";
    if (test["a string"] != "another string") {
      System.Int32 RNTRNTRNT_406 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(765);
      return RNTRNTRNT_406;
    }
    test[111] = 222;
    if (test[111] != 222) {
      System.Int32 RNTRNTRNT_407 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(766);
      return RNTRNTRNT_407;
    }
    System.Console.WriteLine("Passes");
    System.Int32 RNTRNTRNT_408 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(767);
    return RNTRNTRNT_408;
  }
}
