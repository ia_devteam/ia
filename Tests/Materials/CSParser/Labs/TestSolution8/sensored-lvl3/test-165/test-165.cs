using System;
public class Testing
{
  enum Fruit
  {
    Apple,
    Banana,
    Cherry
  }
  static int TestEnumInit(Fruit f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(559);
    Fruit[] testedFruits = { f };
    if (f != Fruit.Apple) {
      System.Int32 RNTRNTRNT_281 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(560);
      return RNTRNTRNT_281;
    }
    System.Int32 RNTRNTRNT_282 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(561);
    return RNTRNTRNT_282;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(562);
    Fruit[] pieFillings = {
      Fruit.Apple,
      Fruit.Banana,
      Fruit.Cherry
    };
    if (pieFillings[0] != Fruit.Apple) {
      System.Int32 RNTRNTRNT_283 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(563);
      return RNTRNTRNT_283;
    }
    if (pieFillings[1] != Fruit.Banana) {
      System.Int32 RNTRNTRNT_284 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(564);
      return RNTRNTRNT_284;
    }
    if (pieFillings[2] != Fruit.Cherry) {
      System.Int32 RNTRNTRNT_285 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(565);
      return RNTRNTRNT_285;
    }
    if (TestEnumInit(Fruit.Apple) != 0) {
      System.Int32 RNTRNTRNT_286 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(566);
      return RNTRNTRNT_286;
    }
    System.Int32 RNTRNTRNT_287 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(567);
    return RNTRNTRNT_287;
  }
}
