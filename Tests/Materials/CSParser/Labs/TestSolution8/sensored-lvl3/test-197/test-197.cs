using System;
public interface A
{
  void Foo();
}
public interface B : A
{
}
public abstract class X : A
{
  public abstract void Foo();
}
public abstract class Y : X, B
{
}
public class Z : Y
{
  public override void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(851);
    Console.WriteLine("Hello World!");
    IACSharpSensor.IACSharpSensor.SensorReached(852);
  }
}
class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(853);
    Z z = new Z();
    A a = z;
    a.Foo();
    System.Int32 RNTRNTRNT_449 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(854);
    return RNTRNTRNT_449;
  }
}
