using System;
using System.Reflection;
[assembly: AssemblyTitle("Foo")]
[assembly: AssemblyVersion("1.0.2")]
namespace N1
{
  [AttributeUsage(AttributeTargets.All)]
  public class MineAttribute : Attribute
  {
    public string name;
    public MineAttribute(string s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(15);
      name = s;
      IACSharpSensor.IACSharpSensor.SensorReached(16);
    }
  }
  [AttributeUsage(AttributeTargets.ReturnValue)]
  public class ReturnAttribute : Attribute
  {
    public string name;
    public ReturnAttribute(string s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(17);
      name = s;
      IACSharpSensor.IACSharpSensor.SensorReached(18);
    }
  }
  public interface TestInterface
  {
    void Hello(    [Mine("param")]
int i);
  }
  public class Foo
  {
    int i;
    [Mine("Foo")]
    [return: Return("Bar")]
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(19);
      Type t = typeof(Foo);
      foreach (MemberInfo m in t.GetMembers()) {
        if (m.Name == "Main") {
          MethodInfo mb = (MethodInfo)m;
          ICustomAttributeProvider p = mb.ReturnTypeCustomAttributes;
          object[] ret_attrs = p.GetCustomAttributes(false);
          if (ret_attrs.Length != 1) {
            Console.WriteLine("Got more than one return attribute");
            System.Int32 RNTRNTRNT_6 = 1;
            IACSharpSensor.IACSharpSensor.SensorReached(20);
            return RNTRNTRNT_6;
          }
          if (!(ret_attrs[0] is ReturnAttribute)) {
            Console.WriteLine("Dit not get a MineAttribute");
            System.Int32 RNTRNTRNT_7 = 2;
            IACSharpSensor.IACSharpSensor.SensorReached(21);
            return RNTRNTRNT_7;
          }
          ReturnAttribute ma = (ReturnAttribute)ret_attrs[0];
          if (ma.name != "Bar") {
            Console.WriteLine("The return attribute is not Bar");
            System.Int32 RNTRNTRNT_8 = 2;
            IACSharpSensor.IACSharpSensor.SensorReached(22);
            return RNTRNTRNT_8;
          }
        }
      }
      Type ifType = typeof(TestInterface);
      MethodInfo method = ifType.GetMethod("Hello", BindingFlags.Public | BindingFlags.Instance);
      ParameterInfo[] parameters = method.GetParameters();
      ParameterInfo param = parameters[0];
      object[] testAttrs = param.GetCustomAttributes(true);
      if (testAttrs.Length != 1) {
        System.Int32 RNTRNTRNT_9 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(23);
        return RNTRNTRNT_9;
      }
      System.Int32 RNTRNTRNT_10 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(24);
      return RNTRNTRNT_10;
    }
  }
}
