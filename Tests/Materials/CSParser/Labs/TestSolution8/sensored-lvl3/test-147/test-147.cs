using System;
public class X
{
  public long Value = 5;
  public static long StaticValue = 6;
  public static X Foo()
  {
    X RNTRNTRNT_139 = new X();
    IACSharpSensor.IACSharpSensor.SensorReached(285);
    return RNTRNTRNT_139;
  }
  public static X Bar()
  {
    X RNTRNTRNT_140 = Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    return RNTRNTRNT_140;
  }
  public X Baz()
  {
    X RNTRNTRNT_141 = Bar();
    IACSharpSensor.IACSharpSensor.SensorReached(287);
    return RNTRNTRNT_141;
  }
  public uint Property {
    get {
      System.UInt32 RNTRNTRNT_142 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(288);
      return RNTRNTRNT_142;
    }
  }
  public static uint StaticProperty {
    get {
      System.UInt32 RNTRNTRNT_143 = 20;
      IACSharpSensor.IACSharpSensor.SensorReached(289);
      return RNTRNTRNT_143;
    }
  }
  public int this[int index] {
    get {
      System.Int32 RNTRNTRNT_144 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(290);
      return RNTRNTRNT_144;
    }
  }
}
public class Y : X
{
  public new long Value = 8;
  public static new long StaticValue = 9;
  public static new Y Foo()
  {
    Y RNTRNTRNT_145 = new Y();
    IACSharpSensor.IACSharpSensor.SensorReached(291);
    return RNTRNTRNT_145;
  }
  public static new Y Bar()
  {
    Y RNTRNTRNT_146 = Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(292);
    return RNTRNTRNT_146;
  }
  public new Y Baz()
  {
    Y RNTRNTRNT_147 = Bar();
    IACSharpSensor.IACSharpSensor.SensorReached(293);
    return RNTRNTRNT_147;
  }
  public new uint Property {
    get {
      System.UInt32 RNTRNTRNT_148 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(294);
      return RNTRNTRNT_148;
    }
  }
  public static new uint StaticProperty {
    get {
      System.UInt32 RNTRNTRNT_149 = 21;
      IACSharpSensor.IACSharpSensor.SensorReached(295);
      return RNTRNTRNT_149;
    }
  }
  public new int this[int index] {
    get {
      System.Int32 RNTRNTRNT_150 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(296);
      return RNTRNTRNT_150;
    }
  }
}
public class Z : Y
{
  public int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(297);
    if (Property != 4) {
      System.Int32 RNTRNTRNT_151 = 20;
      IACSharpSensor.IACSharpSensor.SensorReached(298);
      return RNTRNTRNT_151;
    }
    if (StaticProperty != 21) {
      System.Int32 RNTRNTRNT_152 = 21;
      IACSharpSensor.IACSharpSensor.SensorReached(299);
      return RNTRNTRNT_152;
    }
    if (((X)this).Property != 3) {
      System.Int32 RNTRNTRNT_153 = 22;
      IACSharpSensor.IACSharpSensor.SensorReached(300);
      return RNTRNTRNT_153;
    }
    if (X.StaticProperty != 20) {
      System.Int32 RNTRNTRNT_154 = 23;
      IACSharpSensor.IACSharpSensor.SensorReached(301);
      return RNTRNTRNT_154;
    }
    if (this[5] != 2) {
      System.Int32 RNTRNTRNT_155 = 24;
      IACSharpSensor.IACSharpSensor.SensorReached(302);
      return RNTRNTRNT_155;
    }
    if (((X)this)[6] != 1) {
      System.Int32 RNTRNTRNT_156 = 25;
      IACSharpSensor.IACSharpSensor.SensorReached(303);
      return RNTRNTRNT_156;
    }
    System.Int32 RNTRNTRNT_157 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(304);
    return RNTRNTRNT_157;
  }
}
public class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(305);
    Y y = new Y();
    X a, b, c, d;
    a = Y.Bar();
    if (!(a is Y)) {
      System.Int32 RNTRNTRNT_158 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(306);
      return RNTRNTRNT_158;
    }
    b = y.Baz();
    if (!(b is Y)) {
      System.Int32 RNTRNTRNT_159 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(307);
      return RNTRNTRNT_159;
    }
    c = X.Bar();
    if (c is Y) {
      System.Int32 RNTRNTRNT_160 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(308);
      return RNTRNTRNT_160;
    }
    d = ((X)y).Baz();
    if (d is Y) {
      System.Int32 RNTRNTRNT_161 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(309);
      return RNTRNTRNT_161;
    }
    if (y.Value != 8) {
      System.Int32 RNTRNTRNT_162 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(310);
      return RNTRNTRNT_162;
    }
    if (((X)y).Value != 5) {
      System.Int32 RNTRNTRNT_163 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(311);
      return RNTRNTRNT_163;
    }
    if (Y.StaticValue != 9) {
      System.Int32 RNTRNTRNT_164 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(312);
      return RNTRNTRNT_164;
    }
    if (X.StaticValue != 6) {
      System.Int32 RNTRNTRNT_165 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(313);
      return RNTRNTRNT_165;
    }
    if (y.Property != 4) {
      System.Int32 RNTRNTRNT_166 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(314);
      return RNTRNTRNT_166;
    }
    if (((X)y).Property != 3) {
      System.Int32 RNTRNTRNT_167 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(315);
      return RNTRNTRNT_167;
    }
    if (y[5] != 2) {
      System.Int32 RNTRNTRNT_168 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(316);
      return RNTRNTRNT_168;
    }
    if (((X)y)[7] != 1) {
      System.Int32 RNTRNTRNT_169 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(317);
      return RNTRNTRNT_169;
    }
    if (X.StaticProperty != 20) {
      System.Int32 RNTRNTRNT_170 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(318);
      return RNTRNTRNT_170;
    }
    if (Y.StaticProperty != 21) {
      System.Int32 RNTRNTRNT_171 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(319);
      return RNTRNTRNT_171;
    }
    Z z = new Z();
    System.Int32 RNTRNTRNT_172 = z.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(320);
    return RNTRNTRNT_172;
  }
}
