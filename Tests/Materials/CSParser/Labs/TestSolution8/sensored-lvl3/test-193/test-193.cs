using System;
class A
{
  static int Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(830);
    switch (i) {
      case 1:
        Console.WriteLine("1");
        if (i > 0) {
          goto LBL4;
        }
        Console.WriteLine("2");
        break;
      case 3:
        Console.WriteLine("3");
        LBL4:
        Console.WriteLine("4");
        System.Int32 RNTRNTRNT_439 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(831);
        return RNTRNTRNT_439;
    }
    System.Int32 RNTRNTRNT_440 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(832);
    return RNTRNTRNT_440;
  }
  static int Main()
  {
    System.Int32 RNTRNTRNT_441 = Test(1);
    IACSharpSensor.IACSharpSensor.SensorReached(833);
    return RNTRNTRNT_441;
  }
}
