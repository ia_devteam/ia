using System;
class X
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(252);
    if (!Test1()) {
      System.Int32 RNTRNTRNT_122 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(253);
      return RNTRNTRNT_122;
    }
    if (!Test2()) {
      System.Int32 RNTRNTRNT_123 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(254);
      return RNTRNTRNT_123;
    }
    if (!Test3()) {
      System.Int32 RNTRNTRNT_124 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(255);
      return RNTRNTRNT_124;
    }
    System.Int32 RNTRNTRNT_125 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(256);
    return RNTRNTRNT_125;
  }
  static bool Test1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(257);
    byte num1 = 105;
    byte num2 = 150;
    try {
      checked {
        byte sum = (byte)(num1 - num2);
      }
      System.Boolean RNTRNTRNT_126 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(258);
      return RNTRNTRNT_126;
    } catch (OverflowException) {
      System.Boolean RNTRNTRNT_127 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(259);
      return RNTRNTRNT_127;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(260);
  }
  static bool Test2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(261);
    long l = long.MinValue;
    try {
      checked {
        l = -l;
      }
      System.Boolean RNTRNTRNT_128 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(262);
      return RNTRNTRNT_128;
    } catch (OverflowException) {
      System.Boolean RNTRNTRNT_129 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(263);
      return RNTRNTRNT_129;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(264);
  }
  static bool Test3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(265);
    int i = int.MinValue;
    try {
      checked {
        i = -i;
      }
      System.Boolean RNTRNTRNT_130 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(266);
      return RNTRNTRNT_130;
    } catch (OverflowException) {
      System.Boolean RNTRNTRNT_131 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(267);
      return RNTRNTRNT_131;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(268);
  }
}
