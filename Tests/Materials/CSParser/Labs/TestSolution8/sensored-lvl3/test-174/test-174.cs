using System;
using System.Collections;
class ProtectedAccessToPropertyOnChild : Hashtable
{
  ProtectedAccessToPropertyOnChild()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(707);
    comparer = null;
    IACSharpSensor.IACSharpSensor.SensorReached(708);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(709);
    TestAccessToProtectedOnChildInstanceFromParent t = new TestAccessToProtectedOnChildInstanceFromParent();
    if (t.Test() != 0) {
      System.Int32 RNTRNTRNT_379 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(710);
      return RNTRNTRNT_379;
    }
    System.Int32 RNTRNTRNT_380 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(711);
    return RNTRNTRNT_380;
  }
}
public class TestAccessToPrivateMemberInParentClass
{
  double[][] data;
  int rows;
  int columns;
  public TestAccessToPrivateMemberInParentClass()
  {
  }
  double[][] Array {
    get {
      System.Double[][] RNTRNTRNT_381 = data;
      IACSharpSensor.IACSharpSensor.SensorReached(712);
      return RNTRNTRNT_381;
    }
  }
  class CholeskyDecomposition
  {
    TestAccessToPrivateMemberInParentClass L;
    bool isSymmetric;
    bool isPositiveDefinite;
    public CholeskyDecomposition(TestAccessToPrivateMemberInParentClass A)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(713);
      L = new TestAccessToPrivateMemberInParentClass();
      double[][] a = A.Array;
      double[][] l = L.Array;
      IACSharpSensor.IACSharpSensor.SensorReached(714);
    }
  }
}
public class TestAccessToProtectedOnChildInstanceFromParent
{
  class Parent
  {
    protected int a;
    static int x;
    protected Parent()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(715);
      a = x++;
      IACSharpSensor.IACSharpSensor.SensorReached(716);
    }
    public int TestAccessToProtected(Child c)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(717);
      if (c.a == 0) {
        System.Int32 RNTRNTRNT_382 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(718);
        return RNTRNTRNT_382;
      } else {
        System.Int32 RNTRNTRNT_383 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(719);
        return RNTRNTRNT_383;
      }
    }
  }
  class Child : Parent
  {
  }
  Child c, d;
  public TestAccessToProtectedOnChildInstanceFromParent()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(720);
    c = new Child();
    d = new Child();
    IACSharpSensor.IACSharpSensor.SensorReached(721);
  }
  public int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(722);
    if (d.TestAccessToProtected(c) == 1) {
      System.Int32 RNTRNTRNT_384 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(723);
      return RNTRNTRNT_384;
    }
    System.Int32 RNTRNTRNT_385 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(724);
    return RNTRNTRNT_385;
  }
}
