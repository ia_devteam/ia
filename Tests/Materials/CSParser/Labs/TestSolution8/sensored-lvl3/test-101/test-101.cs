using System;
using System.Reflection;
namespace Test
{
  public class MyAttribute : Attribute
  {
    public string val;
    public MyAttribute(string stuff)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(7);
      System.Console.WriteLine(stuff);
      val = stuff;
      IACSharpSensor.IACSharpSensor.SensorReached(8);
    }
  }
  public class My2Attribute : MyAttribute
  {
    public int ival;
    public My2Attribute(string stuff, int blah) : base(stuff)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(9);
      System.Console.WriteLine("ctor with int val" + stuff);
      ival = blah;
      IACSharpSensor.IACSharpSensor.SensorReached(10);
    }
  }
  [Flags()]
  enum X
  {
    A,
    B
  }
  [My("testclass")]
  [My2("testclass", 22)]
  public class Test
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(11);
      System.Reflection.MemberInfo info = typeof(Test);
      object[] attributes = info.GetCustomAttributes(false);
      for (int i = 0; i < attributes.Length; i++) {
        System.Console.WriteLine(attributes[i]);
      }
      if (attributes.Length != 2) {
        System.Int32 RNTRNTRNT_3 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(12);
        return RNTRNTRNT_3;
      }
      MyAttribute attr = (MyAttribute)attributes[0];
      if (attr.val != "testclass") {
        System.Int32 RNTRNTRNT_4 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(13);
        return RNTRNTRNT_4;
      }
      System.Int32 RNTRNTRNT_5 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(14);
      return RNTRNTRNT_5;
    }
  }
}
