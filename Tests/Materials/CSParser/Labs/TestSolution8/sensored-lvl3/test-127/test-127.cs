using System;
enum Test
{
  A,
  B,
  C
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(158);
    Test test = Test.A;
    if (!Test.IsDefined(typeof(Test), test)) {
      System.Int32 RNTRNTRNT_80 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(159);
      return RNTRNTRNT_80;
    }
    System.Int32 RNTRNTRNT_81 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(160);
    return RNTRNTRNT_81;
  }
}
