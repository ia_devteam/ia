struct T
{
  int val;
  void one()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    two(this);
    IACSharpSensor.IACSharpSensor.SensorReached(239);
  }
  void two(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(240);
    this = t;
    IACSharpSensor.IACSharpSensor.SensorReached(241);
  }
  void three(ref T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(242);
    two(t);
    IACSharpSensor.IACSharpSensor.SensorReached(243);
  }
  public override int GetHashCode()
  {
    System.Int32 RNTRNTRNT_118 = val.GetHashCode();
    IACSharpSensor.IACSharpSensor.SensorReached(244);
    return RNTRNTRNT_118;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    T t = new T();
    t.one();
    t.GetHashCode();
    System.Int32 RNTRNTRNT_119 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(246);
    return RNTRNTRNT_119;
  }
}
