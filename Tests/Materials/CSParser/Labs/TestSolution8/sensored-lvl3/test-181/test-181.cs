using System;
using System.Reflection;
using System.Runtime.CompilerServices;
class Test
{
  [MethodImplAttribute(MethodImplOptions.Synchronized)]
  public void test()
  {
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(770);
    MethodImplAttributes iflags = typeof(Test).GetMethod("test").GetMethodImplementationFlags();
    System.Int32 RNTRNTRNT_409 = ((iflags & MethodImplAttributes.Synchronized) != 0 ? 0 : 1);
    IACSharpSensor.IACSharpSensor.SensorReached(771);
    return RNTRNTRNT_409;
  }
}
