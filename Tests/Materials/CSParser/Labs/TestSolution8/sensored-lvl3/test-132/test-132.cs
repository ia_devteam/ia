using System.Reflection;
class T
{
  protected internal string s;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(181);
    FieldInfo f = typeof(T).GetField("s", BindingFlags.NonPublic | BindingFlags.Instance);
    if (f == null) {
      System.Int32 RNTRNTRNT_92 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(182);
      return RNTRNTRNT_92;
    }
    FieldAttributes attrs = f.Attributes;
    if ((attrs & FieldAttributes.FieldAccessMask) != FieldAttributes.FamORAssem) {
      System.Int32 RNTRNTRNT_93 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(183);
      return RNTRNTRNT_93;
    }
    System.Int32 RNTRNTRNT_94 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(184);
    return RNTRNTRNT_94;
  }
}
