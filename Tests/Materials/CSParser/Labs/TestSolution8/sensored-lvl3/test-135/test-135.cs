using System;
using System.Reflection;
interface IA
{
  void doh();
}
interface IB
{
  IA Prop { get; }
}
class A : IA
{
  public void doh()
  {
  }
}
class T : IB
{
  IA IB.Prop {
    get {
      IA RNTRNTRNT_105 = new A();
      IACSharpSensor.IACSharpSensor.SensorReached(213);
      return RNTRNTRNT_105;
    }
  }
  public A Prop {
    get {
      A RNTRNTRNT_106 = new A();
      IACSharpSensor.IACSharpSensor.SensorReached(214);
      return RNTRNTRNT_106;
    }
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    PropertyInfo[] p = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
    if (p == null || p.Length != 2) {
      System.Int32 RNTRNTRNT_107 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(216);
      return RNTRNTRNT_107;
    }
    System.Int32 RNTRNTRNT_108 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(217);
    return RNTRNTRNT_108;
  }
}
