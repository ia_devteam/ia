using System;
using System.Collections;
using System.Runtime.CompilerServices;
public interface X
{
  [IndexerName("Foo")]
  int this[int a] { get; }
}
public class Y : X
{
  int X.this[int a] {
    get {
      System.Int32 RNTRNTRNT_173 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(321);
      return RNTRNTRNT_173;
    }
  }
  [IndexerName("Bar")]
  public int this[int a] {
    get {
      System.Int32 RNTRNTRNT_174 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(322);
      return RNTRNTRNT_174;
    }
  }
  [IndexerName("Bar")]
  public long this[double a] {
    get {
      System.Int64 RNTRNTRNT_175 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(323);
      return RNTRNTRNT_175;
    }
  }
}
public class Z : Y
{
  [IndexerName("Whatever")]
  public new long this[double a] {
    get {
      System.Int64 RNTRNTRNT_176 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(324);
      return RNTRNTRNT_176;
    }
  }
  [IndexerName("Whatever")]
  public float this[long a, int b] {
    get {
      System.Single RNTRNTRNT_177 = a / b;
      IACSharpSensor.IACSharpSensor.SensorReached(325);
      return RNTRNTRNT_177;
    }
  }
  public int InstanceTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(326);
    double index = 5;
    Console.WriteLine("INSTANCE TEST");
    if (this[index] != 4) {
      System.Int32 RNTRNTRNT_178 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(327);
      return RNTRNTRNT_178;
    }
    if (base[index] != 3) {
      System.Int32 RNTRNTRNT_179 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(328);
      return RNTRNTRNT_179;
    }
    System.Int32 RNTRNTRNT_180 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(329);
    return RNTRNTRNT_180;
  }
  public static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(330);
    Z z = new Z();
    X x = (X)z;
    Y y = (Y)z;
    Console.WriteLine(z[1]);
    Console.WriteLine(y[2]);
    Console.WriteLine(x[3]);
    if (z[1] != 4) {
      System.Int32 RNTRNTRNT_181 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(331);
      return RNTRNTRNT_181;
    }
    if (y[1] != 2) {
      System.Int32 RNTRNTRNT_182 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(332);
      return RNTRNTRNT_182;
    }
    if (x[1] != 1) {
      System.Int32 RNTRNTRNT_183 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(333);
      return RNTRNTRNT_183;
    }
    double index = 5;
    Console.WriteLine(z[index]);
    Console.WriteLine(y[index]);
    if (z[index] != 4) {
      System.Int32 RNTRNTRNT_184 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(334);
      return RNTRNTRNT_184;
    }
    if (y[index] != 3) {
      System.Int32 RNTRNTRNT_185 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(335);
      return RNTRNTRNT_185;
    }
    int retval = z.InstanceTest();
    if (retval != 0) {
      System.Int32 RNTRNTRNT_186 = retval;
      IACSharpSensor.IACSharpSensor.SensorReached(336);
      return RNTRNTRNT_186;
    }
    B b = new B();
    if (b[4] != 16) {
      System.Int32 RNTRNTRNT_187 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(337);
      return RNTRNTRNT_187;
    }
    if (b[3, 5] != 15) {
      System.Int32 RNTRNTRNT_188 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(338);
      return RNTRNTRNT_188;
    }
    D d = new D();
    if (d[4] != 16) {
      System.Int32 RNTRNTRNT_189 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(339);
      return RNTRNTRNT_189;
    }
    if (d[3, 5] != 15) {
      System.Int32 RNTRNTRNT_190 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(340);
      return RNTRNTRNT_190;
    }
    ChildList xd = new ChildList();
    xd.Add(0);
    if (0 != (int)xd[0]) {
      System.Int32 RNTRNTRNT_191 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(341);
      return RNTRNTRNT_191;
    }
    xd.Test();
    if (1 != (int)xd[0]) {
      System.Int32 RNTRNTRNT_192 = 13;
      IACSharpSensor.IACSharpSensor.SensorReached(342);
      return RNTRNTRNT_192;
    }
    System.Int32 RNTRNTRNT_193 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(343);
    return RNTRNTRNT_193;
  }
  class MyArray : ArrayList
  {
    public override object this[int index] {
      get {
        System.Object RNTRNTRNT_194 = base[index];
        IACSharpSensor.IACSharpSensor.SensorReached(344);
        return RNTRNTRNT_194;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(345);
        base[index] = value;
        IACSharpSensor.IACSharpSensor.SensorReached(346);
      }
    }
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(347);
    int result = Test();
    Console.WriteLine("RESULT: " + result);
    E e = new E();
    e.g = "monkey";
    MyArray arr = new MyArray();
    arr.Add("String value");
    if (arr[0].ToString() != "String value") {
      System.Int32 RNTRNTRNT_195 = 100;
      IACSharpSensor.IACSharpSensor.SensorReached(348);
      return RNTRNTRNT_195;
    }
    System.Int32 RNTRNTRNT_196 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(349);
    return RNTRNTRNT_196;
  }
}
public class A
{
  [IndexerName("Monkey")]
  public int this[int value] {
    get {
      System.Int32 RNTRNTRNT_197 = value * 4;
      IACSharpSensor.IACSharpSensor.SensorReached(350);
      return RNTRNTRNT_197;
    }
  }
}
public class B : A
{
  public long this[long a, int value] {
    get {
      System.Int64 RNTRNTRNT_198 = a * value;
      IACSharpSensor.IACSharpSensor.SensorReached(351);
      return RNTRNTRNT_198;
    }
  }
}
public class C
{
  public int this[int value] {
    get {
      System.Int32 RNTRNTRNT_199 = value * 4;
      IACSharpSensor.IACSharpSensor.SensorReached(352);
      return RNTRNTRNT_199;
    }
  }
}
public class D : C
{
  public long this[long a, int value] {
    get {
      System.Int64 RNTRNTRNT_200 = a * value;
      IACSharpSensor.IACSharpSensor.SensorReached(353);
      return RNTRNTRNT_200;
    }
  }
}
public class E
{
  public virtual string g {
    get {
      System.String RNTRNTRNT_201 = "g";
      IACSharpSensor.IACSharpSensor.SensorReached(354);
      return RNTRNTRNT_201;
    }
    set { }
  }
}
public class F : E
{
  public override string g {
    get {
      System.String RNTRNTRNT_202 = "h";
      IACSharpSensor.IACSharpSensor.SensorReached(355);
      return RNTRNTRNT_202;
    }
  }
}
public class DisposableNotifyList : ArrayList
{
}
public class ChildList : DisposableNotifyList
{
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(356);
    base[0] = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(357);
  }
}
