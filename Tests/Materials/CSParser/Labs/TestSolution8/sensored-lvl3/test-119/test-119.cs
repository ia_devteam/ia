class Value
{
  public static explicit operator int(Value val)
  {
    System.Int32 RNTRNTRNT_46 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(94);
    return RNTRNTRNT_46;
  }
  public static explicit operator MyObject(Value val)
  {
    MyObject RNTRNTRNT_47 = new MyObject(1);
    IACSharpSensor.IACSharpSensor.SensorReached(95);
    return RNTRNTRNT_47;
  }
  public static explicit operator uint(Value val)
  {
    System.UInt32 RNTRNTRNT_48 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(96);
    return RNTRNTRNT_48;
  }
}
class MyObject
{
  public MyObject(int i)
  {
  }
}
class Derived : MyObject
{
  public Derived(int i) : base(i)
  {
  }
  Derived Blah()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(97);
    Value val = new Value();
    Derived RNTRNTRNT_49 = (Derived)val;
    IACSharpSensor.IACSharpSensor.SensorReached(98);
    return RNTRNTRNT_49;
  }
}
class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(99);
    Value v = new Value();
    v = null;
    try {
      Derived d = (Derived)v;
    } catch {
    }
    System.Int32 RNTRNTRNT_50 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(100);
    return RNTRNTRNT_50;
  }
}
