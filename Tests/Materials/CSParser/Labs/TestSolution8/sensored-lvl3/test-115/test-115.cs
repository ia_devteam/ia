using System;
class A
{
  public static explicit operator X(A foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(82);
    X myX = new X();
    X RNTRNTRNT_40 = myX;
    IACSharpSensor.IACSharpSensor.SensorReached(83);
    return RNTRNTRNT_40;
  }
}
class X
{
}
class Y : X
{
}
class blah
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(84);
    A testA = new A();
    X testX = (X)testA;
    try {
      Y testY = (Y)testA;
    } catch (InvalidCastException) {
      System.Int32 RNTRNTRNT_41 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(85);
      return RNTRNTRNT_41;
    }
    System.Int32 RNTRNTRNT_42 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(86);
    return RNTRNTRNT_42;
  }
}
