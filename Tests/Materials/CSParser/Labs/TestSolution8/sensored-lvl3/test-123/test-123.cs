class X
{
  static object get_non_null()
  {
    System.Object RNTRNTRNT_58 = new X();
    IACSharpSensor.IACSharpSensor.SensorReached(113);
    return RNTRNTRNT_58;
  }
  static object get_null()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(114);
    return null;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(115);
    int a = 5;
    object o;
    decimal d = 0m;
    if (!(get_non_null() is object)) {
      System.Int32 RNTRNTRNT_59 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(116);
      return RNTRNTRNT_59;
    }
    if (get_null() is object) {
      System.Int32 RNTRNTRNT_60 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(117);
      return RNTRNTRNT_60;
    }
    if (!(a is object)) {
      System.Int32 RNTRNTRNT_61 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(118);
      return RNTRNTRNT_61;
    }
    if (null is object) {
      System.Int32 RNTRNTRNT_62 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(119);
      return RNTRNTRNT_62;
    }
    o = a;
    if (!(o is int)) {
      System.Int32 RNTRNTRNT_63 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(120);
      return RNTRNTRNT_63;
    }
    if (d is int) {
      System.Int32 RNTRNTRNT_64 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(121);
      return RNTRNTRNT_64;
    }
    object oi = 1;
    if (!(oi is int)) {
      System.Int32 RNTRNTRNT_65 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(122);
      return RNTRNTRNT_65;
    }
    System.Console.WriteLine("Is tests pass");
    System.Int32 RNTRNTRNT_66 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(123);
    return RNTRNTRNT_66;
  }
}
