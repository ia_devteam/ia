using System;
struct RVA
{
  public uint value;
  public RVA(uint val)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(725);
    value = val;
    IACSharpSensor.IACSharpSensor.SensorReached(726);
  }
  public static implicit operator RVA(uint val)
  {
    RVA RNTRNTRNT_386 = new RVA(val);
    IACSharpSensor.IACSharpSensor.SensorReached(727);
    return RNTRNTRNT_386;
  }
  public static implicit operator uint(RVA rva)
  {
    System.UInt32 RNTRNTRNT_387 = rva.value;
    IACSharpSensor.IACSharpSensor.SensorReached(728);
    return RNTRNTRNT_387;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(729);
    RVA a = 10;
    RVA b = 20;
    if (a > b) {
      System.Int32 RNTRNTRNT_388 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(730);
      return RNTRNTRNT_388;
    }
    if (a + b != 30) {
      System.Int32 RNTRNTRNT_389 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(731);
      return RNTRNTRNT_389;
    }
    System.Int32 RNTRNTRNT_390 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(732);
    return RNTRNTRNT_390;
  }
}
