using System;
class X
{
  static int Test(params Foo[] foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(577);
    if (foo.Length != 1) {
      System.Int32 RNTRNTRNT_296 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(578);
      return RNTRNTRNT_296;
    }
    if (foo[0] != Foo.A) {
      System.Int32 RNTRNTRNT_297 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(579);
      return RNTRNTRNT_297;
    }
    System.Int32 RNTRNTRNT_298 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(580);
    return RNTRNTRNT_298;
  }
  enum Foo
  {
    A,
    B
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(581);
    int v = Test(Foo.A);
    if (v != 0) {
      System.Int32 RNTRNTRNT_299 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(582);
      return RNTRNTRNT_299;
    }
    MyEnum[] arr = new MyEnum[2];
    arr[0] = MyEnum.c;
    if (arr[0] != MyEnum.c) {
      System.Int32 RNTRNTRNT_300 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(583);
      return RNTRNTRNT_300;
    }
    System.Int32 RNTRNTRNT_301 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(584);
    return RNTRNTRNT_301;
  }
  enum MyEnum
  {
    a,
    b,
    c
  }
}
