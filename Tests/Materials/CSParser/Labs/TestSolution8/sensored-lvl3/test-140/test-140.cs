using System;
public class BreakTest
{
  static int ok = 0;
  public static void B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(247);
    ok++;
    while (true) {
      ok++;
      break;
    }
    ok++;
    IACSharpSensor.IACSharpSensor.SensorReached(248);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(249);
    B();
    if (ok != 3) {
      System.Int32 RNTRNTRNT_120 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(250);
      return RNTRNTRNT_120;
    }
    System.Int32 RNTRNTRNT_121 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(251);
    return RNTRNTRNT_121;
  }
}
