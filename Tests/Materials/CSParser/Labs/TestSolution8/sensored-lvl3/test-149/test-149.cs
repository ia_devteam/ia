using System;
public delegate long MyDelegate(int a);
public interface X
{
  event EventHandler Foo;
  event MyDelegate TestEvent;
}
public class Y : X
{
  static int a = 0;
  event EventHandler X.Foo {
    add { }
    remove { }
  }
  public event EventHandler Foo;
  public event MyDelegate TestEvent;
  public int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(358);
    X x = this;
    Foo += new EventHandler(callback1);
    TestEvent += new MyDelegate(callback2);
    x.Foo += new EventHandler(callback3);
    if (a != 0) {
      System.Int32 RNTRNTRNT_203 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(359);
      return RNTRNTRNT_203;
    }
    Foo(this, new EventArgs());
    if (a != 1) {
      System.Int32 RNTRNTRNT_204 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(360);
      return RNTRNTRNT_204;
    }
    if (TestEvent(2) != 4) {
      System.Int32 RNTRNTRNT_205 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(361);
      return RNTRNTRNT_205;
    }
    if (a != 2) {
      System.Int32 RNTRNTRNT_206 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(362);
      return RNTRNTRNT_206;
    }
    System.Int32 RNTRNTRNT_207 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(363);
    return RNTRNTRNT_207;
  }
  private static void callback1(object sender, EventArgs e)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(364);
    a = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(365);
  }
  private static long callback2(int b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(366);
    a = b;
    System.Int64 RNTRNTRNT_208 = a * a;
    IACSharpSensor.IACSharpSensor.SensorReached(367);
    return RNTRNTRNT_208;
  }
  private static void callback3(object sender, EventArgs e)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    a = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(369);
  }
}
public class Z : Y
{
  public delegate int SomeEventHandler();
  public static event SomeEventHandler BuildStarted;
  static int a()
  {
    System.Int32 RNTRNTRNT_209 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(370);
    return RNTRNTRNT_209;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(371);
    Z z = new Z();
    int result = z.Test();
    if (result != 0) {
      System.Int32 RNTRNTRNT_210 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(372);
      return RNTRNTRNT_210;
    }
    if (BuildStarted != null) {
      BuildStarted();
    }
    BuildStarted = new SomeEventHandler(a);
    if (BuildStarted() != 1) {
      System.Int32 RNTRNTRNT_211 = 50;
      IACSharpSensor.IACSharpSensor.SensorReached(373);
      return RNTRNTRNT_211;
    }
    System.Int32 RNTRNTRNT_212 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(374);
    return RNTRNTRNT_212;
  }
}
public class Static
{
  public static event EventHandler Test;
  public void Fire()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(375);
    if (Test != null) {
      Test(null, null);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(376);
  }
}
