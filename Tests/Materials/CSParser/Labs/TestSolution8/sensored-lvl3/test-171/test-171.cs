using System;
namespace IntPtr_Conv
{
  struct FooStruct
  {
    int x;
  }
  class Class1
  {
    static int Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(611);
      IntPtr p = IntPtr.Zero;
      unsafe {
        FooStruct* s = (FooStruct*)(p);
      }
      System.Int32 RNTRNTRNT_318 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(612);
      return RNTRNTRNT_318;
    }
  }
}
