using System;
public class TestClass : TestBaseClass
{
  public TestClass(EventHandler hndlr) : base()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(269);
    Blah += hndlr;
    IACSharpSensor.IACSharpSensor.SensorReached(270);
  }
  public static int Main()
  {
    System.Int32 RNTRNTRNT_132 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(271);
    return RNTRNTRNT_132;
  }
}
public class TestBaseClass
{
  public event EventHandler Blah;
}
