using System;
interface A
{
  void X();
}
interface B
{
  void X();
}
class C : A, B
{
  int var;
  public void X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(228);
    var++;
    IACSharpSensor.IACSharpSensor.SensorReached(229);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(230);
    C c = new C();
    A a = c;
    B b = c;
    if (c.var != 0) {
      System.Int32 RNTRNTRNT_112 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(231);
      return RNTRNTRNT_112;
    }
    a.X();
    if (c.var != 1) {
      System.Int32 RNTRNTRNT_113 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(232);
      return RNTRNTRNT_113;
    }
    b.X();
    if (c.var != 2) {
      System.Int32 RNTRNTRNT_114 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(233);
      return RNTRNTRNT_114;
    }
    c.X();
    if (c.var != 3) {
      System.Int32 RNTRNTRNT_115 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(234);
      return RNTRNTRNT_115;
    }
    Console.WriteLine("Test passes");
    System.Int32 RNTRNTRNT_116 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(235);
    return RNTRNTRNT_116;
  }
}
