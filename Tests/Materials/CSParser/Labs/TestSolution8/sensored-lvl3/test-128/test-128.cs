using System;
using System.Reflection;
public class SimpleAttribute : Attribute
{
  string n;
  public SimpleAttribute(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(161);
    n = name;
    IACSharpSensor.IACSharpSensor.SensorReached(162);
  }
}
[AttributeUsage(AttributeTargets.All)]
public class MineAttribute : Attribute
{
  public MineAttribute(params Type[] t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(163);
    types = t;
    IACSharpSensor.IACSharpSensor.SensorReached(164);
  }
  public Type[] types;
}
[Mine(typeof(int), typeof(string), typeof(object[]))]
public class Foo
{
  public static int MM()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(165);
    object[] attrs = typeof(Foo).GetCustomAttributes(typeof(MineAttribute), true);
    MineAttribute ma = (MineAttribute)attrs[0];
    if (ma.types[0] != typeof(int)) {
      Console.WriteLine("failed");
      System.Int32 RNTRNTRNT_82 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(166);
      return RNTRNTRNT_82;
    }
    if (ma.types[1] != typeof(string)) {
      Console.WriteLine("failed");
      System.Int32 RNTRNTRNT_83 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(167);
      return RNTRNTRNT_83;
    }
    if (ma.types[2] != typeof(object[])) {
      Console.WriteLine("failed");
      System.Int32 RNTRNTRNT_84 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(168);
      return RNTRNTRNT_84;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_85 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(169);
    return RNTRNTRNT_85;
  }
}
public class Blah
{
  int i;
  public int Value {
    [Simple("Foo!")]
    get {
      System.Int32 RNTRNTRNT_86 = i;
      IACSharpSensor.IACSharpSensor.SensorReached(170);
      return RNTRNTRNT_86;
    }
    [Simple("Bar !")]
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(171);
      i = value;
      IACSharpSensor.IACSharpSensor.SensorReached(172);
    }
  }
  [Simple((string)null)]
  int Another()
  {
    System.Int32 RNTRNTRNT_87 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(173);
    return RNTRNTRNT_87;
  }
  public static int Main()
  {
    System.Int32 RNTRNTRNT_88 = Foo.MM();
    IACSharpSensor.IACSharpSensor.SensorReached(174);
    return RNTRNTRNT_88;
  }
}
