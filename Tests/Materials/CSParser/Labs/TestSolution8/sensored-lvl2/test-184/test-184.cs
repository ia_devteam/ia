using System;
public interface Interface
{
  int X { get; }
}
public struct Struct : Interface
{
  public Struct(int x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1399);
  }
  public int X {
    get {
      System.Int32 RNTRNTRNT_418 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1400);
      return RNTRNTRNT_418;
    }
  }
}
public class User
{
  public User(Interface iface)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1401);
  }
}
public class Test
{
  User t;
  Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1402);
    t = new User(new Struct(5));
    IACSharpSensor.IACSharpSensor.SensorReached(1403);
  }
  User t2 = new User(new Struct(251));
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1404);
    Test tt = new Test();
    System.Int32 RNTRNTRNT_419 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1405);
    return RNTRNTRNT_419;
  }
}
