interface IA
{
  void A();
}
interface IB : IA
{
  void B();
}
interface IC : IA, IB
{
  void C();
}
interface ID : IC
{
}
class AA : IC
{
  bool a, b, c;
  public void A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(339);
    a = true;
    IACSharpSensor.IACSharpSensor.SensorReached(340);
  }
  public void B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(341);
    b = true;
    IACSharpSensor.IACSharpSensor.SensorReached(342);
  }
  public void C()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(343);
    c = true;
    IACSharpSensor.IACSharpSensor.SensorReached(344);
  }
  public bool OK {
    get {
      System.Boolean RNTRNTRNT_100 = a && b && c;
      IACSharpSensor.IACSharpSensor.SensorReached(345);
      return RNTRNTRNT_100;
    }
  }
}
class BB : ID
{
  bool a, b, c;
  public void A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(346);
    a = true;
    System.Console.WriteLine("A");
    IACSharpSensor.IACSharpSensor.SensorReached(347);
  }
  public void B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(348);
    b = true;
    IACSharpSensor.IACSharpSensor.SensorReached(349);
  }
  public void C()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(350);
    c = true;
    IACSharpSensor.IACSharpSensor.SensorReached(351);
  }
  public bool OK {
    get {
      System.Boolean RNTRNTRNT_101 = a && b && c;
      IACSharpSensor.IACSharpSensor.SensorReached(352);
      return RNTRNTRNT_101;
    }
  }
}
class T : IB
{
  public void A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(353);
  }
  public void B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(354);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(355);
    BB bb = new BB();
    bb.A();
    bb.B();
    bb.C();
    IACSharpSensor.IACSharpSensor.SensorReached(356);
    if (!bb.OK) {
      System.Int32 RNTRNTRNT_102 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(357);
      return RNTRNTRNT_102;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(358);
    AA aa = new AA();
    aa.A();
    aa.B();
    aa.C();
    IACSharpSensor.IACSharpSensor.SensorReached(359);
    if (!aa.OK) {
      System.Int32 RNTRNTRNT_103 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(360);
      return RNTRNTRNT_103;
    }
    System.Int32 RNTRNTRNT_104 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(361);
    return RNTRNTRNT_104;
  }
}
