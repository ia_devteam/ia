using System;
class A
{
  static int Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1459);
    switch (i) {
      case 1:
        IACSharpSensor.IACSharpSensor.SensorReached(1460);
        Console.WriteLine("1");
        IACSharpSensor.IACSharpSensor.SensorReached(1461);
        if (i > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(1462);
          goto LBL4;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1463);
        Console.WriteLine("2");
        IACSharpSensor.IACSharpSensor.SensorReached(1464);
        break;
      case 3:
        IACSharpSensor.IACSharpSensor.SensorReached(1465);
        Console.WriteLine("3");
        LBL4:
        Console.WriteLine("4");
        System.Int32 RNTRNTRNT_439 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(1466);
        return RNTRNTRNT_439;
    }
    System.Int32 RNTRNTRNT_440 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1467);
    return RNTRNTRNT_440;
  }
  static int Main()
  {
    System.Int32 RNTRNTRNT_441 = Test(1);
    IACSharpSensor.IACSharpSensor.SensorReached(1468);
    return RNTRNTRNT_441;
  }
}
