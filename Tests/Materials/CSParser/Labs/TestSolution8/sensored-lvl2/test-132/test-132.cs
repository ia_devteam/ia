using System.Reflection;
class T
{
  protected internal string s;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(317);
    FieldInfo f = typeof(T).GetField("s", BindingFlags.NonPublic | BindingFlags.Instance);
    IACSharpSensor.IACSharpSensor.SensorReached(318);
    if (f == null) {
      System.Int32 RNTRNTRNT_92 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(319);
      return RNTRNTRNT_92;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(320);
    FieldAttributes attrs = f.Attributes;
    IACSharpSensor.IACSharpSensor.SensorReached(321);
    if ((attrs & FieldAttributes.FieldAccessMask) != FieldAttributes.FamORAssem) {
      System.Int32 RNTRNTRNT_93 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(322);
      return RNTRNTRNT_93;
    }
    System.Int32 RNTRNTRNT_94 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(323);
    return RNTRNTRNT_94;
  }
}
