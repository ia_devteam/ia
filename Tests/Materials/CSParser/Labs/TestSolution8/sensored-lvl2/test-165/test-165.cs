using System;
public class Testing
{
  enum Fruit
  {
    Apple,
    Banana,
    Cherry
  }
  static int TestEnumInit(Fruit f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1031);
    Fruit[] testedFruits = { f };
    IACSharpSensor.IACSharpSensor.SensorReached(1032);
    if (f != Fruit.Apple) {
      System.Int32 RNTRNTRNT_281 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1033);
      return RNTRNTRNT_281;
    }
    System.Int32 RNTRNTRNT_282 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1034);
    return RNTRNTRNT_282;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1035);
    Fruit[] pieFillings = {
      Fruit.Apple,
      Fruit.Banana,
      Fruit.Cherry
    };
    IACSharpSensor.IACSharpSensor.SensorReached(1036);
    if (pieFillings[0] != Fruit.Apple) {
      System.Int32 RNTRNTRNT_283 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1037);
      return RNTRNTRNT_283;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1038);
    if (pieFillings[1] != Fruit.Banana) {
      System.Int32 RNTRNTRNT_284 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1039);
      return RNTRNTRNT_284;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1040);
    if (pieFillings[2] != Fruit.Cherry) {
      System.Int32 RNTRNTRNT_285 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1041);
      return RNTRNTRNT_285;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1042);
    if (TestEnumInit(Fruit.Apple) != 0) {
      System.Int32 RNTRNTRNT_286 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1043);
      return RNTRNTRNT_286;
    }
    System.Int32 RNTRNTRNT_287 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1044);
    return RNTRNTRNT_287;
  }
}
