using System;
class Test
{
  delegate int D(int x, out int y);
  static int M(int x, out int y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1390);
    y = x + 2;
    System.Int32 RNTRNTRNT_414 = ++x;
    IACSharpSensor.IACSharpSensor.SensorReached(1391);
    return RNTRNTRNT_414;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1392);
    int x = 1;
    int y = 0;
    D del = new D(M);
    IAsyncResult ar = del.BeginInvoke(x, out y, null, null);
    IACSharpSensor.IACSharpSensor.SensorReached(1393);
    if (del.EndInvoke(out y, ar) != 2) {
      System.Int32 RNTRNTRNT_415 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1394);
      return RNTRNTRNT_415;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1395);
    if (y != 3) {
      System.Int32 RNTRNTRNT_416 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1396);
      return RNTRNTRNT_416;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1397);
    Console.WriteLine("Test ok");
    System.Int32 RNTRNTRNT_417 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1398);
    return RNTRNTRNT_417;
  }
}
