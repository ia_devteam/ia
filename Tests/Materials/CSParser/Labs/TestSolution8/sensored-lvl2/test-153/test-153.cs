using System;
using System.Text;
using System.Diagnostics;
class Z
{
  public static void Test2(string message, params object[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(678);
  }
  public static void Test(string message, params object[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(679);
    Test2(message, args);
    IACSharpSensor.IACSharpSensor.SensorReached(680);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(681);
    Test("TEST");
    Test("Foo", 8);
    Test("Foo", 8, 9, "Hello");
    System.Int32 RNTRNTRNT_219 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(682);
    return RNTRNTRNT_219;
  }
}
