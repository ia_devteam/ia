using System;
using System.Threading;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
class Test
{
  delegate int SimpleDelegate(int a);
  static int cb_state = 0;
  static int F(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(88);
    Console.WriteLine("Test.F from delegate: " + a);
    IACSharpSensor.IACSharpSensor.SensorReached(89);
    throw new NotImplementedException();
  }
  static void async_callback(IAsyncResult ar)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(90);
    AsyncResult ares = (AsyncResult)ar;
    AsyncCallback ac = new AsyncCallback(async_callback);
    Console.WriteLine("Async Callback " + ar.AsyncState);
    cb_state++;
    SimpleDelegate d = (SimpleDelegate)ares.AsyncDelegate;
    IACSharpSensor.IACSharpSensor.SensorReached(91);
    if (cb_state < 5) {
      IACSharpSensor.IACSharpSensor.SensorReached(92);
      d.BeginInvoke(cb_state, ac, cb_state);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(93);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(94);
    SimpleDelegate d = new SimpleDelegate(F);
    AsyncCallback ac = new AsyncCallback(async_callback);
    IAsyncResult ar1 = d.BeginInvoke(cb_state, ac, cb_state);
    ar1.AsyncWaitHandle.WaitOne();
    IACSharpSensor.IACSharpSensor.SensorReached(95);
    while (cb_state < 5) {
      IACSharpSensor.IACSharpSensor.SensorReached(96);
      Thread.Sleep(200);
    }
    System.Int32 RNTRNTRNT_21 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(97);
    return RNTRNTRNT_21;
  }
}
