class A
{
  double d1, d2;
  public double this[double x] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(1348);
      if (d1 == x) {
        System.Double RNTRNTRNT_397 = d2;
        IACSharpSensor.IACSharpSensor.SensorReached(1349);
        return RNTRNTRNT_397;
      }
      System.Double RNTRNTRNT_398 = 0.0;
      IACSharpSensor.IACSharpSensor.SensorReached(1350);
      return RNTRNTRNT_398;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1351);
      d1 = x;
      d2 = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1352);
    }
  }
}
class B : A
{
  double d1, d2;
  public new double this[double x] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(1353);
      if (d1 == x) {
        System.Double RNTRNTRNT_399 = d2;
        IACSharpSensor.IACSharpSensor.SensorReached(1354);
        return RNTRNTRNT_399;
      }
      System.Double RNTRNTRNT_400 = 0.0;
      IACSharpSensor.IACSharpSensor.SensorReached(1355);
      return RNTRNTRNT_400;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1356);
      d1 = x;
      d2 = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1357);
    }
  }
}
class C : B
{
  string s1, s2;
  int i1, i2;
  public string this[string x] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(1358);
      if (s1 == x) {
        System.String RNTRNTRNT_401 = s2;
        IACSharpSensor.IACSharpSensor.SensorReached(1359);
        return RNTRNTRNT_401;
      }
      System.String RNTRNTRNT_402 = "";
      IACSharpSensor.IACSharpSensor.SensorReached(1360);
      return RNTRNTRNT_402;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1361);
      s1 = x;
      s2 = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1362);
    }
  }
  public int this[int x] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(1363);
      if (i1 == x) {
        System.Int32 RNTRNTRNT_403 = i2;
        IACSharpSensor.IACSharpSensor.SensorReached(1364);
        return RNTRNTRNT_403;
      }
      System.Int32 RNTRNTRNT_404 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1365);
      return RNTRNTRNT_404;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1366);
      i1 = x;
      i2 = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1367);
    }
  }
}
struct EntryPoint
{
  public static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1368);
    C test = new C();
    test[333.333] = 444.444;
    IACSharpSensor.IACSharpSensor.SensorReached(1369);
    if (test[333.333] != 444.444) {
      System.Int32 RNTRNTRNT_405 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1370);
      return RNTRNTRNT_405;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1371);
    test["a string"] = "another string";
    IACSharpSensor.IACSharpSensor.SensorReached(1372);
    if (test["a string"] != "another string") {
      System.Int32 RNTRNTRNT_406 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1373);
      return RNTRNTRNT_406;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1374);
    test[111] = 222;
    IACSharpSensor.IACSharpSensor.SensorReached(1375);
    if (test[111] != 222) {
      System.Int32 RNTRNTRNT_407 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1376);
      return RNTRNTRNT_407;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1377);
    System.Console.WriteLine("Passes");
    System.Int32 RNTRNTRNT_408 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1378);
    return RNTRNTRNT_408;
  }
}
