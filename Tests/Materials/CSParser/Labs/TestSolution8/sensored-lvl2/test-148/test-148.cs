using System;
using System.Collections;
using System.Runtime.CompilerServices;
public interface X
{
  [IndexerName("Foo")]
  int this[int a] { get; }
}
public class Y : X
{
  int X.this[int a] {
    get {
      System.Int32 RNTRNTRNT_173 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(567);
      return RNTRNTRNT_173;
    }
  }
  [IndexerName("Bar")]
  public int this[int a] {
    get {
      System.Int32 RNTRNTRNT_174 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(568);
      return RNTRNTRNT_174;
    }
  }
  [IndexerName("Bar")]
  public long this[double a] {
    get {
      System.Int64 RNTRNTRNT_175 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(569);
      return RNTRNTRNT_175;
    }
  }
}
public class Z : Y
{
  [IndexerName("Whatever")]
  public new long this[double a] {
    get {
      System.Int64 RNTRNTRNT_176 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(570);
      return RNTRNTRNT_176;
    }
  }
  [IndexerName("Whatever")]
  public float this[long a, int b] {
    get {
      System.Single RNTRNTRNT_177 = a / b;
      IACSharpSensor.IACSharpSensor.SensorReached(571);
      return RNTRNTRNT_177;
    }
  }
  public int InstanceTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(572);
    double index = 5;
    Console.WriteLine("INSTANCE TEST");
    IACSharpSensor.IACSharpSensor.SensorReached(573);
    if (this[index] != 4) {
      System.Int32 RNTRNTRNT_178 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(574);
      return RNTRNTRNT_178;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(575);
    if (base[index] != 3) {
      System.Int32 RNTRNTRNT_179 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(576);
      return RNTRNTRNT_179;
    }
    System.Int32 RNTRNTRNT_180 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(577);
    return RNTRNTRNT_180;
  }
  public static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(578);
    Z z = new Z();
    X x = (X)z;
    Y y = (Y)z;
    Console.WriteLine(z[1]);
    Console.WriteLine(y[2]);
    Console.WriteLine(x[3]);
    IACSharpSensor.IACSharpSensor.SensorReached(579);
    if (z[1] != 4) {
      System.Int32 RNTRNTRNT_181 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(580);
      return RNTRNTRNT_181;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(581);
    if (y[1] != 2) {
      System.Int32 RNTRNTRNT_182 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(582);
      return RNTRNTRNT_182;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(583);
    if (x[1] != 1) {
      System.Int32 RNTRNTRNT_183 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(584);
      return RNTRNTRNT_183;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(585);
    double index = 5;
    Console.WriteLine(z[index]);
    Console.WriteLine(y[index]);
    IACSharpSensor.IACSharpSensor.SensorReached(586);
    if (z[index] != 4) {
      System.Int32 RNTRNTRNT_184 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(587);
      return RNTRNTRNT_184;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(588);
    if (y[index] != 3) {
      System.Int32 RNTRNTRNT_185 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(589);
      return RNTRNTRNT_185;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(590);
    int retval = z.InstanceTest();
    IACSharpSensor.IACSharpSensor.SensorReached(591);
    if (retval != 0) {
      System.Int32 RNTRNTRNT_186 = retval;
      IACSharpSensor.IACSharpSensor.SensorReached(592);
      return RNTRNTRNT_186;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(593);
    B b = new B();
    IACSharpSensor.IACSharpSensor.SensorReached(594);
    if (b[4] != 16) {
      System.Int32 RNTRNTRNT_187 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(595);
      return RNTRNTRNT_187;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(596);
    if (b[3, 5] != 15) {
      System.Int32 RNTRNTRNT_188 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(597);
      return RNTRNTRNT_188;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(598);
    D d = new D();
    IACSharpSensor.IACSharpSensor.SensorReached(599);
    if (d[4] != 16) {
      System.Int32 RNTRNTRNT_189 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(600);
      return RNTRNTRNT_189;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(601);
    if (d[3, 5] != 15) {
      System.Int32 RNTRNTRNT_190 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(602);
      return RNTRNTRNT_190;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(603);
    ChildList xd = new ChildList();
    xd.Add(0);
    IACSharpSensor.IACSharpSensor.SensorReached(604);
    if (0 != (int)xd[0]) {
      System.Int32 RNTRNTRNT_191 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(605);
      return RNTRNTRNT_191;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(606);
    xd.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(607);
    if (1 != (int)xd[0]) {
      System.Int32 RNTRNTRNT_192 = 13;
      IACSharpSensor.IACSharpSensor.SensorReached(608);
      return RNTRNTRNT_192;
    }
    System.Int32 RNTRNTRNT_193 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(609);
    return RNTRNTRNT_193;
  }
  class MyArray : ArrayList
  {
    public override object this[int index] {
      get {
        System.Object RNTRNTRNT_194 = base[index];
        IACSharpSensor.IACSharpSensor.SensorReached(610);
        return RNTRNTRNT_194;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(611);
        base[index] = value;
        IACSharpSensor.IACSharpSensor.SensorReached(612);
      }
    }
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(613);
    int result = Test();
    Console.WriteLine("RESULT: " + result);
    E e = new E();
    e.g = "monkey";
    MyArray arr = new MyArray();
    arr.Add("String value");
    IACSharpSensor.IACSharpSensor.SensorReached(614);
    if (arr[0].ToString() != "String value") {
      System.Int32 RNTRNTRNT_195 = 100;
      IACSharpSensor.IACSharpSensor.SensorReached(615);
      return RNTRNTRNT_195;
    }
    System.Int32 RNTRNTRNT_196 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(616);
    return RNTRNTRNT_196;
  }
}
public class A
{
  [IndexerName("Monkey")]
  public int this[int value] {
    get {
      System.Int32 RNTRNTRNT_197 = value * 4;
      IACSharpSensor.IACSharpSensor.SensorReached(617);
      return RNTRNTRNT_197;
    }
  }
}
public class B : A
{
  public long this[long a, int value] {
    get {
      System.Int64 RNTRNTRNT_198 = a * value;
      IACSharpSensor.IACSharpSensor.SensorReached(618);
      return RNTRNTRNT_198;
    }
  }
}
public class C
{
  public int this[int value] {
    get {
      System.Int32 RNTRNTRNT_199 = value * 4;
      IACSharpSensor.IACSharpSensor.SensorReached(619);
      return RNTRNTRNT_199;
    }
  }
}
public class D : C
{
  public long this[long a, int value] {
    get {
      System.Int64 RNTRNTRNT_200 = a * value;
      IACSharpSensor.IACSharpSensor.SensorReached(620);
      return RNTRNTRNT_200;
    }
  }
}
public class E
{
  public virtual string g {
    get {
      System.String RNTRNTRNT_201 = "g";
      IACSharpSensor.IACSharpSensor.SensorReached(621);
      return RNTRNTRNT_201;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(622); }
  }
}
public class F : E
{
  public override string g {
    get {
      System.String RNTRNTRNT_202 = "h";
      IACSharpSensor.IACSharpSensor.SensorReached(623);
      return RNTRNTRNT_202;
    }
  }
}
public class DisposableNotifyList : ArrayList
{
}
public class ChildList : DisposableNotifyList
{
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(624);
    base[0] = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(625);
  }
}
