using System;
struct MonoEnumInfo
{
  int val;
  void stuff()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(453);
    val = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(454);
  }
  static int GetInfo(out MonoEnumInfo info)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(455);
    info = new MonoEnumInfo();
    info.stuff();
    System.Int32 RNTRNTRNT_133 = info.val;
    IACSharpSensor.IACSharpSensor.SensorReached(456);
    return RNTRNTRNT_133;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(457);
    MonoEnumInfo m;
    IACSharpSensor.IACSharpSensor.SensorReached(458);
    if (GetInfo(out m) != 1) {
      System.Int32 RNTRNTRNT_134 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(459);
      return RNTRNTRNT_134;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(460);
    if (m.val != 1) {
      System.Int32 RNTRNTRNT_135 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(461);
      return RNTRNTRNT_135;
    }
    System.Int32 RNTRNTRNT_136 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(462);
    return RNTRNTRNT_136;
  }
}
