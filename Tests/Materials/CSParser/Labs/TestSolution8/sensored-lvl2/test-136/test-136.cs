using System;
public interface IDiagnostic
{
  void Stop();
}
public interface IAutomobile
{
  void Stop();
}
public class MyCar : IAutomobile, IDiagnostic
{
  public bool diag_stop, car_stop, auto_stop;
  void IDiagnostic.Stop()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(369);
    diag_stop = true;
    IACSharpSensor.IACSharpSensor.SensorReached(370);
  }
  public void Stop()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(371);
    car_stop = true;
    IAutomobile self = (IAutomobile)this;
    self.Stop();
    IACSharpSensor.IACSharpSensor.SensorReached(372);
  }
  void IAutomobile.Stop()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(373);
    auto_stop = true;
    IACSharpSensor.IACSharpSensor.SensorReached(374);
  }
}
class TestConflict
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(375);
    MyCar car1 = new MyCar();
    car1.Stop();
    IDiagnostic car2 = new MyCar();
    car2.Stop();
    IAutomobile car3 = new MyCar();
    car3.Stop();
    IACSharpSensor.IACSharpSensor.SensorReached(376);
    if (!car1.car_stop) {
      System.Int32 RNTRNTRNT_109 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(377);
      return RNTRNTRNT_109;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(378);
    if (car1.diag_stop) {
      System.Int32 RNTRNTRNT_110 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(379);
      return RNTRNTRNT_110;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(380);
    Console.WriteLine("ok");
    System.Int32 RNTRNTRNT_111 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(381);
    return RNTRNTRNT_111;
  }
}
