using System;
class X
{
  protected virtual int Foo()
  {
    System.Int32 RNTRNTRNT_278 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1020);
    return RNTRNTRNT_278;
  }
  protected delegate int FooDelegate();
  protected FooDelegate foo;
  protected X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1021);
    foo = new FooDelegate(Foo);
    IACSharpSensor.IACSharpSensor.SensorReached(1022);
  }
}
class Y : X
{
  protected Y() : base()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1023);
  }
  protected override int Foo()
  {
    System.Int32 RNTRNTRNT_279 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(1024);
    return RNTRNTRNT_279;
  }
  int Hello()
  {
    System.Int32 RNTRNTRNT_280 = foo();
    IACSharpSensor.IACSharpSensor.SensorReached(1025);
    return RNTRNTRNT_280;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1026);
    Y y = new Y();
    int result = y.Hello();
    IACSharpSensor.IACSharpSensor.SensorReached(1027);
    if (result == 2) {
      IACSharpSensor.IACSharpSensor.SensorReached(1028);
      Console.WriteLine("OK");
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(1029);
      Console.WriteLine("NOT OK");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1030);
  }
}
