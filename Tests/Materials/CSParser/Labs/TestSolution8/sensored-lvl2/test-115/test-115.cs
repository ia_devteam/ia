using System;
class A
{
  public static explicit operator X(A foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(150);
    X myX = new X();
    X RNTRNTRNT_40 = myX;
    IACSharpSensor.IACSharpSensor.SensorReached(151);
    return RNTRNTRNT_40;
  }
}
class X
{
}
class Y : X
{
}
class blah
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(152);
    A testA = new A();
    X testX = (X)testA;
    IACSharpSensor.IACSharpSensor.SensorReached(153);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(154);
      Y testY = (Y)testA;
    } catch (InvalidCastException) {
      System.Int32 RNTRNTRNT_41 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(155);
      return RNTRNTRNT_41;
      IACSharpSensor.IACSharpSensor.SensorReached(156);
    }
    System.Int32 RNTRNTRNT_42 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(157);
    return RNTRNTRNT_42;
  }
}
