using System;
struct A
{
  public int a;
  private long b;
  private float c;
  public A(int foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(981);
    a = foo;
    b = 8;
    c = 9.0f;
    IACSharpSensor.IACSharpSensor.SensorReached(982);
  }
}
struct B
{
  public int a;
}
struct C
{
  public long b;
  public C(long foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(983);
    b = foo;
    IACSharpSensor.IACSharpSensor.SensorReached(984);
  }
  public C(string foo) : this(500)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(985);
  }
}
struct D
{
  public int foo;
}
struct E
{
  public D d;
  public bool e;
  public E(int foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(986);
    this.e = true;
    this.d.foo = 9;
    IACSharpSensor.IACSharpSensor.SensorReached(987);
  }
}
struct F
{
  public E e;
  public float f;
}
class X
{
  static void test_output(A x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(988);
  }
  static void test_output(B y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(989);
  }
  static void test_output(E e)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(990);
  }
  static void test_output(F f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(991);
  }
  static void test1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(992);
    A x;
    x.a = 5;
    Console.WriteLine(x.a);
    IACSharpSensor.IACSharpSensor.SensorReached(993);
  }
  static void test2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(994);
    B y;
    y.a = 5;
    Console.WriteLine(y.a);
    Console.WriteLine(y);
    IACSharpSensor.IACSharpSensor.SensorReached(995);
  }
  static void test3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(996);
    A x = new A(85);
    Console.WriteLine(x);
    IACSharpSensor.IACSharpSensor.SensorReached(997);
  }
  static void test4(A x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(998);
    x.a = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(999);
  }
  static void test5(out A x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1000);
    x = new A(85);
    IACSharpSensor.IACSharpSensor.SensorReached(1001);
  }
  static void test6(out B y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1002);
    y.a = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1003);
  }
  static void test7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1004);
    E e;
    e.e = true;
    e.d.foo = 5;
    test_output(e);
    IACSharpSensor.IACSharpSensor.SensorReached(1005);
  }
  static void test8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1006);
    F f;
    f.e.e = true;
    f.e.d.foo = 5;
    f.f = 3.14f;
    test_output(f);
    IACSharpSensor.IACSharpSensor.SensorReached(1007);
  }
  static void test9()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1008);
    E e = new E(5);
    Console.WriteLine(e.d.foo);
    IACSharpSensor.IACSharpSensor.SensorReached(1009);
  }
  static void test10()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1010);
    F f;
    f.e = new E(10);
    Console.WriteLine(f.e.d.foo);
    Console.WriteLine(f.e.d);
    f.f = 3.14f;
    Console.WriteLine(f);
    IACSharpSensor.IACSharpSensor.SensorReached(1011);
  }
  public static int Main()
  {
    System.Int32 RNTRNTRNT_273 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1012);
    return RNTRNTRNT_273;
  }
}
