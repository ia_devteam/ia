using System;
public interface A
{
  void Foo();
}
public interface B : A
{
}
public abstract class X : A
{
  public abstract void Foo();
}
public abstract class Y : X, B
{
}
public class Z : Y
{
  public override void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1493);
    Console.WriteLine("Hello World!");
    IACSharpSensor.IACSharpSensor.SensorReached(1494);
  }
}
class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1495);
    Z z = new Z();
    A a = z;
    a.Foo();
    System.Int32 RNTRNTRNT_449 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1496);
    return RNTRNTRNT_449;
  }
}
