using System;
using System.Threading;
using System.Runtime.InteropServices;
class Test
{
  delegate int SimpleDelegate(int a);
  static int F(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(47);
    Console.WriteLine("Test.F from delegate: " + a);
    Thread.Sleep(200);
    System.Int32 RNTRNTRNT_15 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(48);
    return RNTRNTRNT_15;
  }
  static void async_callback(IAsyncResult ar)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(49);
    Console.WriteLine("Async Callback " + ar.AsyncState);
    IACSharpSensor.IACSharpSensor.SensorReached(50);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(51);
    SimpleDelegate d = new SimpleDelegate(F);
    AsyncCallback ac = new AsyncCallback(async_callback);
    string state1 = "STATE1";
    string state2 = "STATE2";
    string state3 = "STATE3";
    string state4 = "STATE4";
    int fin = 0;
    IAsyncResult ar1 = d.BeginInvoke(1, ac, state1);
    IAsyncResult ar2 = d.BeginInvoke(2, ac, state2);
    IAsyncResult ar3 = d.BeginInvoke(3, ac, state3);
    IAsyncResult ar4 = d.BeginInvoke(4, ac, state4);
    int res = d.EndInvoke(ar1);
    Console.WriteLine("Result = " + res);
    IACSharpSensor.IACSharpSensor.SensorReached(52);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(53);
      d.EndInvoke(ar1);
    } catch (InvalidOperationException) {
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      Console.WriteLine("cant execute EndInvoke twice ... OK");
      IACSharpSensor.IACSharpSensor.SensorReached(55);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(56);
    ar1.AsyncWaitHandle.WaitOne();
    IACSharpSensor.IACSharpSensor.SensorReached(57);
    if (ar1.IsCompleted) {
      IACSharpSensor.IACSharpSensor.SensorReached(58);
      fin++;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(59);
    Console.WriteLine("completed1: " + ar1.IsCompleted);
    ar2.AsyncWaitHandle.WaitOne();
    IACSharpSensor.IACSharpSensor.SensorReached(60);
    if (ar2.IsCompleted) {
      IACSharpSensor.IACSharpSensor.SensorReached(61);
      fin++;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    Console.WriteLine("completed2: " + ar2.IsCompleted);
    ar3.AsyncWaitHandle.WaitOne();
    IACSharpSensor.IACSharpSensor.SensorReached(63);
    if (ar3.IsCompleted) {
      IACSharpSensor.IACSharpSensor.SensorReached(64);
      fin++;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(65);
    Console.WriteLine("completed3: " + ar3.IsCompleted);
    ar4.AsyncWaitHandle.WaitOne();
    IACSharpSensor.IACSharpSensor.SensorReached(66);
    if (ar4.IsCompleted) {
      IACSharpSensor.IACSharpSensor.SensorReached(67);
      fin++;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    Console.WriteLine("completed4: " + ar4.IsCompleted);
    IACSharpSensor.IACSharpSensor.SensorReached(69);
    if (fin != 4) {
      System.Int32 RNTRNTRNT_16 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(70);
      return RNTRNTRNT_16;
    }
    System.Int32 RNTRNTRNT_17 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(71);
    return RNTRNTRNT_17;
  }
}
