using System;
enum Test
{
  A,
  B,
  C
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    Test test = Test.A;
    IACSharpSensor.IACSharpSensor.SensorReached(287);
    if (!Test.IsDefined(typeof(Test), test)) {
      System.Int32 RNTRNTRNT_80 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(288);
      return RNTRNTRNT_80;
    }
    System.Int32 RNTRNTRNT_81 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(289);
    return RNTRNTRNT_81;
  }
}
