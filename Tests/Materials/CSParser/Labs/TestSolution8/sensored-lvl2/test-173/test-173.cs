using System;
class Base
{
  int value;
  public int Value {
    get {
      System.Int32 RNTRNTRNT_364 = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1238);
      return RNTRNTRNT_364;
    }
  }
  protected Base(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1239);
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(1240);
  }
}
class A : Base
{
  public A(int value) : base(1)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1241);
    Console.WriteLine("Int");
    IACSharpSensor.IACSharpSensor.SensorReached(1242);
  }
  public A(uint value) : base(2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1243);
    Console.WriteLine("UInt");
    IACSharpSensor.IACSharpSensor.SensorReached(1244);
  }
}
class B : Base
{
  public B(long value) : base(3)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1245);
    Console.WriteLine("Long");
    IACSharpSensor.IACSharpSensor.SensorReached(1246);
  }
  public B(ulong value) : base(4)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1247);
    Console.WriteLine("ULong");
    IACSharpSensor.IACSharpSensor.SensorReached(1248);
  }
}
class C : Base
{
  public C(short value) : base(5)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1249);
    Console.WriteLine("Short");
    IACSharpSensor.IACSharpSensor.SensorReached(1250);
  }
  public C(ushort value) : base(6)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1251);
    Console.WriteLine("UShort");
    IACSharpSensor.IACSharpSensor.SensorReached(1252);
  }
}
class D : Base
{
  public D(sbyte value) : base(7)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1253);
    Console.WriteLine("SByte");
    IACSharpSensor.IACSharpSensor.SensorReached(1254);
  }
  public D(byte value) : base(8)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1255);
    Console.WriteLine("Byte");
    IACSharpSensor.IACSharpSensor.SensorReached(1256);
  }
}
class E : Base
{
  public E(long value) : base(9)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1257);
    Console.WriteLine("Long");
    IACSharpSensor.IACSharpSensor.SensorReached(1258);
  }
  public E(E e) : base(10)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1259);
    Console.WriteLine("E");
    IACSharpSensor.IACSharpSensor.SensorReached(1260);
  }
  public static implicit operator E(long value)
  {
    E RNTRNTRNT_365 = (new E(value));
    IACSharpSensor.IACSharpSensor.SensorReached(1261);
    return RNTRNTRNT_365;
  }
}
class F : Base
{
  public F(int value) : base(11)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1262);
    Console.WriteLine("Int");
    IACSharpSensor.IACSharpSensor.SensorReached(1263);
  }
  public F(F f) : base(12)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1264);
    Console.WriteLine("F");
    IACSharpSensor.IACSharpSensor.SensorReached(1265);
  }
  public static implicit operator F(int value)
  {
    F RNTRNTRNT_366 = (new F(value));
    IACSharpSensor.IACSharpSensor.SensorReached(1266);
    return RNTRNTRNT_366;
  }
}
class X
{
  static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1279);
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1267);
      A a = new A(4);
      IACSharpSensor.IACSharpSensor.SensorReached(1268);
      if (a.Value != 1) {
        System.Int32 RNTRNTRNT_367 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1269);
        return RNTRNTRNT_367;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1270);
      B b = new B(4);
      IACSharpSensor.IACSharpSensor.SensorReached(1271);
      if (b.Value != 3) {
        System.Int32 RNTRNTRNT_368 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(1272);
        return RNTRNTRNT_368;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1273);
      C c = new C(4);
      IACSharpSensor.IACSharpSensor.SensorReached(1274);
      if (c.Value != 5) {
        System.Int32 RNTRNTRNT_369 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(1275);
        return RNTRNTRNT_369;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1276);
      D d = new D(4);
      IACSharpSensor.IACSharpSensor.SensorReached(1277);
      if (d.Value != 7) {
        System.Int32 RNTRNTRNT_370 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(1278);
        return RNTRNTRNT_370;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1292);
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1280);
      A a = new A(4u);
      IACSharpSensor.IACSharpSensor.SensorReached(1281);
      if (a.Value != 2) {
        System.Int32 RNTRNTRNT_371 = 5;
        IACSharpSensor.IACSharpSensor.SensorReached(1282);
        return RNTRNTRNT_371;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1283);
      B b = new B(4u);
      IACSharpSensor.IACSharpSensor.SensorReached(1284);
      if (b.Value != 3) {
        System.Int32 RNTRNTRNT_372 = 6;
        IACSharpSensor.IACSharpSensor.SensorReached(1285);
        return RNTRNTRNT_372;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1286);
      C c = new C(4);
      IACSharpSensor.IACSharpSensor.SensorReached(1287);
      if (c.Value != 5) {
        System.Int32 RNTRNTRNT_373 = 7;
        IACSharpSensor.IACSharpSensor.SensorReached(1288);
        return RNTRNTRNT_373;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1289);
      D d = new D(4);
      IACSharpSensor.IACSharpSensor.SensorReached(1290);
      if (d.Value != 7) {
        System.Int32 RNTRNTRNT_374 = 8;
        IACSharpSensor.IACSharpSensor.SensorReached(1291);
        return RNTRNTRNT_374;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1299);
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1293);
      E e = new E(4);
      IACSharpSensor.IACSharpSensor.SensorReached(1294);
      if (e.Value != 9) {
        System.Int32 RNTRNTRNT_375 = 9;
        IACSharpSensor.IACSharpSensor.SensorReached(1295);
        return RNTRNTRNT_375;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1296);
      F f = new F(4);
      IACSharpSensor.IACSharpSensor.SensorReached(1297);
      if (f.Value != 11) {
        System.Int32 RNTRNTRNT_376 = 10;
        IACSharpSensor.IACSharpSensor.SensorReached(1298);
        return RNTRNTRNT_376;
      }
    }
    System.Int32 RNTRNTRNT_377 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1300);
    return RNTRNTRNT_377;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1301);
    int result = Test();
    Console.WriteLine("RESULT: {0}", result);
    System.Int32 RNTRNTRNT_378 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(1302);
    return RNTRNTRNT_378;
  }
}
