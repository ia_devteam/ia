using System;
class X
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(419);
    if (!Test1()) {
      System.Int32 RNTRNTRNT_122 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(420);
      return RNTRNTRNT_122;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(421);
    if (!Test2()) {
      System.Int32 RNTRNTRNT_123 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(422);
      return RNTRNTRNT_123;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(423);
    if (!Test3()) {
      System.Int32 RNTRNTRNT_124 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(424);
      return RNTRNTRNT_124;
    }
    System.Int32 RNTRNTRNT_125 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(425);
    return RNTRNTRNT_125;
  }
  static bool Test1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(426);
    byte num1 = 105;
    byte num2 = 150;
    IACSharpSensor.IACSharpSensor.SensorReached(427);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(428);
      checked {
        IACSharpSensor.IACSharpSensor.SensorReached(429);
        byte sum = (byte)(num1 - num2);
      }
      System.Boolean RNTRNTRNT_126 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(430);
      return RNTRNTRNT_126;
    } catch (OverflowException) {
      System.Boolean RNTRNTRNT_127 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(431);
      return RNTRNTRNT_127;
      IACSharpSensor.IACSharpSensor.SensorReached(432);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(433);
  }
  static bool Test2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(434);
    long l = long.MinValue;
    IACSharpSensor.IACSharpSensor.SensorReached(435);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(436);
      checked {
        IACSharpSensor.IACSharpSensor.SensorReached(437);
        l = -l;
      }
      System.Boolean RNTRNTRNT_128 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(438);
      return RNTRNTRNT_128;
    } catch (OverflowException) {
      System.Boolean RNTRNTRNT_129 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(439);
      return RNTRNTRNT_129;
      IACSharpSensor.IACSharpSensor.SensorReached(440);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(441);
  }
  static bool Test3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(442);
    int i = int.MinValue;
    IACSharpSensor.IACSharpSensor.SensorReached(443);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(444);
      checked {
        IACSharpSensor.IACSharpSensor.SensorReached(445);
        i = -i;
      }
      System.Boolean RNTRNTRNT_130 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(446);
      return RNTRNTRNT_130;
    } catch (OverflowException) {
      System.Boolean RNTRNTRNT_131 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(447);
      return RNTRNTRNT_131;
      IACSharpSensor.IACSharpSensor.SensorReached(448);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(449);
  }
}
