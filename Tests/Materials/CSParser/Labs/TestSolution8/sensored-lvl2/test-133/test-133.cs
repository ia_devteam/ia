using System;
public struct S
{
  public int a, b;
}
class T
{
  enum OpCode : ushort
  {
    False
  }
  enum OpFlags : ushort
  {
    None
  }
  static void DecodeOp(ushort word, out OpCode op, out OpFlags flags)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(324);
    op = (OpCode)(word & 0xff);
    flags = (OpFlags)(word & 0xff00);
    IACSharpSensor.IACSharpSensor.SensorReached(325);
  }
  static void get_struct(out S s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(326);
    S ss;
    ss.a = 1;
    ss.b = 2;
    s = ss;
    IACSharpSensor.IACSharpSensor.SensorReached(327);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(328);
    OpCode op;
    OpFlags flags;
    S s;
    DecodeOp((ushort)0x203, out op, out flags);
    IACSharpSensor.IACSharpSensor.SensorReached(329);
    if (op != (OpCode)0x3) {
      System.Int32 RNTRNTRNT_95 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(330);
      return RNTRNTRNT_95;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(331);
    if (flags != (OpFlags)0x200) {
      System.Int32 RNTRNTRNT_96 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(332);
      return RNTRNTRNT_96;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(333);
    get_struct(out s);
    IACSharpSensor.IACSharpSensor.SensorReached(334);
    if (s.a != 1) {
      System.Int32 RNTRNTRNT_97 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(335);
      return RNTRNTRNT_97;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(336);
    if (s.b != 2) {
      System.Int32 RNTRNTRNT_98 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(337);
      return RNTRNTRNT_98;
    }
    System.Int32 RNTRNTRNT_99 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(338);
    return RNTRNTRNT_99;
  }
}
