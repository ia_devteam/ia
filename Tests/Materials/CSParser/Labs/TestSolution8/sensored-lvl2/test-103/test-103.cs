abstract class A
{
  protected abstract int Foo();
}
class B : A
{
  protected override int Foo()
  {
    System.Int32 RNTRNTRNT_11 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    return RNTRNTRNT_11;
  }
  public int M()
  {
    System.Int32 RNTRNTRNT_12 = Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(43);
    return RNTRNTRNT_12;
  }
}
class Test
{
  public static int Main()
  {
    System.Int32 RNTRNTRNT_13 = new B().M();
    IACSharpSensor.IACSharpSensor.SensorReached(44);
    return RNTRNTRNT_13;
  }
}
