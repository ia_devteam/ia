using System;
public class BreakTest
{
  static int ok = 0;
  public static void B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(409);
    ok++;
    IACSharpSensor.IACSharpSensor.SensorReached(410);
    while (true) {
      IACSharpSensor.IACSharpSensor.SensorReached(411);
      ok++;
      IACSharpSensor.IACSharpSensor.SensorReached(412);
      break;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    ok++;
    IACSharpSensor.IACSharpSensor.SensorReached(414);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(415);
    B();
    IACSharpSensor.IACSharpSensor.SensorReached(416);
    if (ok != 3) {
      System.Int32 RNTRNTRNT_120 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(417);
      return RNTRNTRNT_120;
    }
    System.Int32 RNTRNTRNT_121 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(418);
    return RNTRNTRNT_121;
  }
}
