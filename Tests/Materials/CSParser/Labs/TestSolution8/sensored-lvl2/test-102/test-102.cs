using System;
using System.Reflection;
[assembly: AssemblyTitle("Foo")]
[assembly: AssemblyVersion("1.0.2")]
namespace N1
{
  [AttributeUsage(AttributeTargets.All)]
  public class MineAttribute : Attribute
  {
    public string name;
    public MineAttribute(string s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(20);
      name = s;
      IACSharpSensor.IACSharpSensor.SensorReached(21);
    }
  }
  [AttributeUsage(AttributeTargets.ReturnValue)]
  public class ReturnAttribute : Attribute
  {
    public string name;
    public ReturnAttribute(string s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(22);
      name = s;
      IACSharpSensor.IACSharpSensor.SensorReached(23);
    }
  }
  public interface TestInterface
  {
    void Hello(    [Mine("param")]
int i);
  }
  public class Foo
  {
    int i;
    [Mine("Foo")]
    [return: Return("Bar")]
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(24);
      Type t = typeof(Foo);
      IACSharpSensor.IACSharpSensor.SensorReached(25);
      foreach (MemberInfo m in t.GetMembers()) {
        IACSharpSensor.IACSharpSensor.SensorReached(26);
        if (m.Name == "Main") {
          IACSharpSensor.IACSharpSensor.SensorReached(27);
          MethodInfo mb = (MethodInfo)m;
          ICustomAttributeProvider p = mb.ReturnTypeCustomAttributes;
          object[] ret_attrs = p.GetCustomAttributes(false);
          IACSharpSensor.IACSharpSensor.SensorReached(28);
          if (ret_attrs.Length != 1) {
            IACSharpSensor.IACSharpSensor.SensorReached(29);
            Console.WriteLine("Got more than one return attribute");
            System.Int32 RNTRNTRNT_6 = 1;
            IACSharpSensor.IACSharpSensor.SensorReached(30);
            return RNTRNTRNT_6;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(31);
          if (!(ret_attrs[0] is ReturnAttribute)) {
            IACSharpSensor.IACSharpSensor.SensorReached(32);
            Console.WriteLine("Dit not get a MineAttribute");
            System.Int32 RNTRNTRNT_7 = 2;
            IACSharpSensor.IACSharpSensor.SensorReached(33);
            return RNTRNTRNT_7;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(34);
          ReturnAttribute ma = (ReturnAttribute)ret_attrs[0];
          IACSharpSensor.IACSharpSensor.SensorReached(35);
          if (ma.name != "Bar") {
            IACSharpSensor.IACSharpSensor.SensorReached(36);
            Console.WriteLine("The return attribute is not Bar");
            System.Int32 RNTRNTRNT_8 = 2;
            IACSharpSensor.IACSharpSensor.SensorReached(37);
            return RNTRNTRNT_8;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(38);
      Type ifType = typeof(TestInterface);
      MethodInfo method = ifType.GetMethod("Hello", BindingFlags.Public | BindingFlags.Instance);
      ParameterInfo[] parameters = method.GetParameters();
      ParameterInfo param = parameters[0];
      object[] testAttrs = param.GetCustomAttributes(true);
      IACSharpSensor.IACSharpSensor.SensorReached(39);
      if (testAttrs.Length != 1) {
        System.Int32 RNTRNTRNT_9 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(40);
        return RNTRNTRNT_9;
      }
      System.Int32 RNTRNTRNT_10 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(41);
      return RNTRNTRNT_10;
    }
  }
}
