class A
{
  private int foo = 0;
  class B : A
  {
    void Test()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1451);
      foo = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1452);
    }
  }
  class C
  {
    void Test(A a)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1453);
      a.foo = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1454);
    }
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1455);
  }
}
