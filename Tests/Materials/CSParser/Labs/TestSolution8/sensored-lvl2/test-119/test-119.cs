class Value
{
  public static explicit operator int(Value val)
  {
    System.Int32 RNTRNTRNT_46 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(165);
    return RNTRNTRNT_46;
  }
  public static explicit operator MyObject(Value val)
  {
    MyObject RNTRNTRNT_47 = new MyObject(1);
    IACSharpSensor.IACSharpSensor.SensorReached(166);
    return RNTRNTRNT_47;
  }
  public static explicit operator uint(Value val)
  {
    System.UInt32 RNTRNTRNT_48 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(167);
    return RNTRNTRNT_48;
  }
}
class MyObject
{
  public MyObject(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(168);
  }
}
class Derived : MyObject
{
  public Derived(int i) : base(i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(169);
  }
  Derived Blah()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    Value val = new Value();
    Derived RNTRNTRNT_49 = (Derived)val;
    IACSharpSensor.IACSharpSensor.SensorReached(171);
    return RNTRNTRNT_49;
  }
}
class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(172);
    Value v = new Value();
    v = null;
    IACSharpSensor.IACSharpSensor.SensorReached(173);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(174);
      Derived d = (Derived)v;
    } catch {
    }
    System.Int32 RNTRNTRNT_50 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(175);
    return RNTRNTRNT_50;
  }
}
