using System;
class X
{
  static int Test(params Foo[] foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1058);
    if (foo.Length != 1) {
      System.Int32 RNTRNTRNT_296 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1059);
      return RNTRNTRNT_296;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1060);
    if (foo[0] != Foo.A) {
      System.Int32 RNTRNTRNT_297 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1061);
      return RNTRNTRNT_297;
    }
    System.Int32 RNTRNTRNT_298 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1062);
    return RNTRNTRNT_298;
  }
  enum Foo
  {
    A,
    B
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1063);
    int v = Test(Foo.A);
    IACSharpSensor.IACSharpSensor.SensorReached(1064);
    if (v != 0) {
      System.Int32 RNTRNTRNT_299 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(1065);
      return RNTRNTRNT_299;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1066);
    MyEnum[] arr = new MyEnum[2];
    arr[0] = MyEnum.c;
    IACSharpSensor.IACSharpSensor.SensorReached(1067);
    if (arr[0] != MyEnum.c) {
      System.Int32 RNTRNTRNT_300 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1068);
      return RNTRNTRNT_300;
    }
    System.Int32 RNTRNTRNT_301 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1069);
    return RNTRNTRNT_301;
  }
  enum MyEnum
  {
    a,
    b,
    c
  }
}
