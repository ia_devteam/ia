using System;
using System.Reflection;
interface IA
{
  void doh();
}
interface IB
{
  IA Prop { get; }
}
class A : IA
{
  public void doh()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(362);
  }
}
class T : IB
{
  IA IB.Prop {
    get {
      IA RNTRNTRNT_105 = new A();
      IACSharpSensor.IACSharpSensor.SensorReached(363);
      return RNTRNTRNT_105;
    }
  }
  public A Prop {
    get {
      A RNTRNTRNT_106 = new A();
      IACSharpSensor.IACSharpSensor.SensorReached(364);
      return RNTRNTRNT_106;
    }
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(365);
    PropertyInfo[] p = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
    IACSharpSensor.IACSharpSensor.SensorReached(366);
    if (p == null || p.Length != 2) {
      System.Int32 RNTRNTRNT_107 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(367);
      return RNTRNTRNT_107;
    }
    System.Int32 RNTRNTRNT_108 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    return RNTRNTRNT_108;
  }
}
