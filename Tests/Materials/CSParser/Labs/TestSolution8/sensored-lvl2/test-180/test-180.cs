using System;
public class Testing
{
  public enum Fruit
  {
    Apple,
    Orange
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1379);
    Console.WriteLine(Convert.ToInt64(Fruit.Orange as Enum));
    IACSharpSensor.IACSharpSensor.SensorReached(1380);
  }
}
