using System;
using System.Reflection;
public class MethodAttribute : Attribute
{
}
public class ReturnAttribute : Attribute
{
}
public class Test
{
  [Method()]
  [return: Return()]
  public void Method()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1338);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1339);
    Type t = typeof(Test);
    MethodInfo mi = t.GetMethod("Method");
    ICustomAttributeProvider cap = mi.ReturnTypeCustomAttributes;
    IACSharpSensor.IACSharpSensor.SensorReached(1340);
    if (cap != null) {
      System.Int32 RNTRNTRNT_393 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1341);
      return RNTRNTRNT_393;
    } else {
      System.Int32 RNTRNTRNT_394 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1342);
      return RNTRNTRNT_394;
    }
  }
}
