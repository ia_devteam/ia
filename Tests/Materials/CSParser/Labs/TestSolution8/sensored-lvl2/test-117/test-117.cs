using System;
using System.Collections;
public interface IFoo
{
}
public class Blah : IFoo
{
  Hashtable table;
  public Blah()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(160);
    table = new Hashtable();
    IACSharpSensor.IACSharpSensor.SensorReached(161);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    Blah b = new Blah();
    b.table.Add("Ravi", (IFoo)b);
    System.Int32 RNTRNTRNT_44 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(163);
    return RNTRNTRNT_44;
  }
}
