struct T
{
  int val;
  void one()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(400);
    two(this);
    IACSharpSensor.IACSharpSensor.SensorReached(401);
  }
  void two(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(402);
    this = t;
    IACSharpSensor.IACSharpSensor.SensorReached(403);
  }
  void three(ref T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(404);
    two(t);
    IACSharpSensor.IACSharpSensor.SensorReached(405);
  }
  public override int GetHashCode()
  {
    System.Int32 RNTRNTRNT_118 = val.GetHashCode();
    IACSharpSensor.IACSharpSensor.SensorReached(406);
    return RNTRNTRNT_118;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    T t = new T();
    t.one();
    t.GetHashCode();
    System.Int32 RNTRNTRNT_119 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(408);
    return RNTRNTRNT_119;
  }
}
