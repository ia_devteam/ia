using System;
public class Enumerator
{
  int counter;
  public Enumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1425);
    counter = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(1426);
  }
  public bool MoveNext()
  {
    System.Boolean RNTRNTRNT_428 = (counter-- > 0);
    IACSharpSensor.IACSharpSensor.SensorReached(1427);
    return RNTRNTRNT_428;
  }
  public char Current {
    get {
      System.Char RNTRNTRNT_429 = 'a';
      IACSharpSensor.IACSharpSensor.SensorReached(1428);
      return RNTRNTRNT_429;
    }
  }
}
class RealEnumerator : Enumerator, IDisposable
{
  Coll c;
  public RealEnumerator(Coll c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1429);
    this.c = c;
    IACSharpSensor.IACSharpSensor.SensorReached(1430);
  }
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1431);
    c.disposed = true;
    IACSharpSensor.IACSharpSensor.SensorReached(1432);
  }
}
public class Coll
{
  public bool disposed;
  public Enumerator GetEnumerator()
  {
    Enumerator RNTRNTRNT_430 = new RealEnumerator(this);
    IACSharpSensor.IACSharpSensor.SensorReached(1433);
    return RNTRNTRNT_430;
  }
}
class Test
{
  public static int Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1434);
    Coll coll = new Coll();
    IACSharpSensor.IACSharpSensor.SensorReached(1435);
    foreach (char c in coll) {
    }
    System.Int32 RNTRNTRNT_431 = (coll.disposed ? 0 : 1);
    IACSharpSensor.IACSharpSensor.SensorReached(1436);
    return RNTRNTRNT_431;
  }
}
