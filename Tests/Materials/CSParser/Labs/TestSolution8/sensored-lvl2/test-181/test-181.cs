using System;
using System.Reflection;
using System.Runtime.CompilerServices;
class Test
{
  [MethodImplAttribute(MethodImplOptions.Synchronized)]
  public void test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1381);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1382);
    MethodImplAttributes iflags = typeof(Test).GetMethod("test").GetMethodImplementationFlags();
    System.Int32 RNTRNTRNT_409 = ((iflags & MethodImplAttributes.Synchronized) != 0 ? 0 : 1);
    IACSharpSensor.IACSharpSensor.SensorReached(1383);
    return RNTRNTRNT_409;
  }
}
