using System;
public class X
{
  public long Value = 5;
  public static long StaticValue = 6;
  public static X Foo()
  {
    X RNTRNTRNT_139 = new X();
    IACSharpSensor.IACSharpSensor.SensorReached(508);
    return RNTRNTRNT_139;
  }
  public static X Bar()
  {
    X RNTRNTRNT_140 = Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(509);
    return RNTRNTRNT_140;
  }
  public X Baz()
  {
    X RNTRNTRNT_141 = Bar();
    IACSharpSensor.IACSharpSensor.SensorReached(510);
    return RNTRNTRNT_141;
  }
  public uint Property {
    get {
      System.UInt32 RNTRNTRNT_142 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(511);
      return RNTRNTRNT_142;
    }
  }
  public static uint StaticProperty {
    get {
      System.UInt32 RNTRNTRNT_143 = 20;
      IACSharpSensor.IACSharpSensor.SensorReached(512);
      return RNTRNTRNT_143;
    }
  }
  public int this[int index] {
    get {
      System.Int32 RNTRNTRNT_144 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(513);
      return RNTRNTRNT_144;
    }
  }
}
public class Y : X
{
  public new long Value = 8;
  public static new long StaticValue = 9;
  public static new Y Foo()
  {
    Y RNTRNTRNT_145 = new Y();
    IACSharpSensor.IACSharpSensor.SensorReached(514);
    return RNTRNTRNT_145;
  }
  public static new Y Bar()
  {
    Y RNTRNTRNT_146 = Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(515);
    return RNTRNTRNT_146;
  }
  public new Y Baz()
  {
    Y RNTRNTRNT_147 = Bar();
    IACSharpSensor.IACSharpSensor.SensorReached(516);
    return RNTRNTRNT_147;
  }
  public new uint Property {
    get {
      System.UInt32 RNTRNTRNT_148 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(517);
      return RNTRNTRNT_148;
    }
  }
  public static new uint StaticProperty {
    get {
      System.UInt32 RNTRNTRNT_149 = 21;
      IACSharpSensor.IACSharpSensor.SensorReached(518);
      return RNTRNTRNT_149;
    }
  }
  public new int this[int index] {
    get {
      System.Int32 RNTRNTRNT_150 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(519);
      return RNTRNTRNT_150;
    }
  }
}
public class Z : Y
{
  public int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(520);
    if (Property != 4) {
      System.Int32 RNTRNTRNT_151 = 20;
      IACSharpSensor.IACSharpSensor.SensorReached(521);
      return RNTRNTRNT_151;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(522);
    if (StaticProperty != 21) {
      System.Int32 RNTRNTRNT_152 = 21;
      IACSharpSensor.IACSharpSensor.SensorReached(523);
      return RNTRNTRNT_152;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(524);
    if (((X)this).Property != 3) {
      System.Int32 RNTRNTRNT_153 = 22;
      IACSharpSensor.IACSharpSensor.SensorReached(525);
      return RNTRNTRNT_153;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(526);
    if (X.StaticProperty != 20) {
      System.Int32 RNTRNTRNT_154 = 23;
      IACSharpSensor.IACSharpSensor.SensorReached(527);
      return RNTRNTRNT_154;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(528);
    if (this[5] != 2) {
      System.Int32 RNTRNTRNT_155 = 24;
      IACSharpSensor.IACSharpSensor.SensorReached(529);
      return RNTRNTRNT_155;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(530);
    if (((X)this)[6] != 1) {
      System.Int32 RNTRNTRNT_156 = 25;
      IACSharpSensor.IACSharpSensor.SensorReached(531);
      return RNTRNTRNT_156;
    }
    System.Int32 RNTRNTRNT_157 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(532);
    return RNTRNTRNT_157;
  }
}
public class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(533);
    Y y = new Y();
    X a, b, c, d;
    a = Y.Bar();
    IACSharpSensor.IACSharpSensor.SensorReached(534);
    if (!(a is Y)) {
      System.Int32 RNTRNTRNT_158 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(535);
      return RNTRNTRNT_158;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(536);
    b = y.Baz();
    IACSharpSensor.IACSharpSensor.SensorReached(537);
    if (!(b is Y)) {
      System.Int32 RNTRNTRNT_159 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(538);
      return RNTRNTRNT_159;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(539);
    c = X.Bar();
    IACSharpSensor.IACSharpSensor.SensorReached(540);
    if (c is Y) {
      System.Int32 RNTRNTRNT_160 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(541);
      return RNTRNTRNT_160;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(542);
    d = ((X)y).Baz();
    IACSharpSensor.IACSharpSensor.SensorReached(543);
    if (d is Y) {
      System.Int32 RNTRNTRNT_161 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(544);
      return RNTRNTRNT_161;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(545);
    if (y.Value != 8) {
      System.Int32 RNTRNTRNT_162 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(546);
      return RNTRNTRNT_162;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(547);
    if (((X)y).Value != 5) {
      System.Int32 RNTRNTRNT_163 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(548);
      return RNTRNTRNT_163;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(549);
    if (Y.StaticValue != 9) {
      System.Int32 RNTRNTRNT_164 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(550);
      return RNTRNTRNT_164;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(551);
    if (X.StaticValue != 6) {
      System.Int32 RNTRNTRNT_165 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(552);
      return RNTRNTRNT_165;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(553);
    if (y.Property != 4) {
      System.Int32 RNTRNTRNT_166 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(554);
      return RNTRNTRNT_166;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(555);
    if (((X)y).Property != 3) {
      System.Int32 RNTRNTRNT_167 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(556);
      return RNTRNTRNT_167;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(557);
    if (y[5] != 2) {
      System.Int32 RNTRNTRNT_168 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(558);
      return RNTRNTRNT_168;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(559);
    if (((X)y)[7] != 1) {
      System.Int32 RNTRNTRNT_169 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(560);
      return RNTRNTRNT_169;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(561);
    if (X.StaticProperty != 20) {
      System.Int32 RNTRNTRNT_170 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(562);
      return RNTRNTRNT_170;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(563);
    if (Y.StaticProperty != 21) {
      System.Int32 RNTRNTRNT_171 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(564);
      return RNTRNTRNT_171;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(565);
    Z z = new Z();
    System.Int32 RNTRNTRNT_172 = z.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(566);
    return RNTRNTRNT_172;
  }
}
