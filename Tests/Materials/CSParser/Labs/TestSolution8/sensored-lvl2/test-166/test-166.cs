using System;
interface ITargetInfo
{
  int TargetIntegerSize { get; }
}
interface ITargetMemoryAccess : ITargetInfo
{
}
interface IInferior : ITargetMemoryAccess
{
}
interface ITest
{
  int this[int index] { get; }
}
class Test : ITest
{
  public int this[int index] {
    get {
      System.Int32 RNTRNTRNT_288 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(1045);
      return RNTRNTRNT_288;
    }
  }
  int ITest.this[int index] {
    get {
      System.Int32 RNTRNTRNT_289 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(1046);
      return RNTRNTRNT_289;
    }
  }
}
class D : IInferior
{
  public int TargetIntegerSize {
    get {
      System.Int32 RNTRNTRNT_290 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(1047);
      return RNTRNTRNT_290;
    }
  }
  int Hello(IInferior inferior)
  {
    System.Int32 RNTRNTRNT_291 = inferior.TargetIntegerSize;
    IACSharpSensor.IACSharpSensor.SensorReached(1048);
    return RNTRNTRNT_291;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1049);
    D d = new D();
    IACSharpSensor.IACSharpSensor.SensorReached(1050);
    if (d.Hello(d) != 5) {
      System.Int32 RNTRNTRNT_292 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1051);
      return RNTRNTRNT_292;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1052);
    Test test = new Test();
    ITest itest = test;
    IACSharpSensor.IACSharpSensor.SensorReached(1053);
    if (test[0] != 5) {
      System.Int32 RNTRNTRNT_293 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1054);
      return RNTRNTRNT_293;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1055);
    if (itest[0] != 8) {
      System.Int32 RNTRNTRNT_294 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1056);
      return RNTRNTRNT_294;
    }
    System.Int32 RNTRNTRNT_295 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1057);
    return RNTRNTRNT_295;
  }
}
