using System;
using System.Collections;
class ProtectedAccessToPropertyOnChild : Hashtable
{
  ProtectedAccessToPropertyOnChild()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1303);
    comparer = null;
    IACSharpSensor.IACSharpSensor.SensorReached(1304);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1305);
    TestAccessToProtectedOnChildInstanceFromParent t = new TestAccessToProtectedOnChildInstanceFromParent();
    IACSharpSensor.IACSharpSensor.SensorReached(1306);
    if (t.Test() != 0) {
      System.Int32 RNTRNTRNT_379 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1307);
      return RNTRNTRNT_379;
    }
    System.Int32 RNTRNTRNT_380 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1308);
    return RNTRNTRNT_380;
  }
}
public class TestAccessToPrivateMemberInParentClass
{
  double[][] data;
  int rows;
  int columns;
  public TestAccessToPrivateMemberInParentClass()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1309);
  }
  double[][] Array {
    get {
      System.Double[][] RNTRNTRNT_381 = data;
      IACSharpSensor.IACSharpSensor.SensorReached(1310);
      return RNTRNTRNT_381;
    }
  }
  class CholeskyDecomposition
  {
    TestAccessToPrivateMemberInParentClass L;
    bool isSymmetric;
    bool isPositiveDefinite;
    public CholeskyDecomposition(TestAccessToPrivateMemberInParentClass A)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1311);
      L = new TestAccessToPrivateMemberInParentClass();
      double[][] a = A.Array;
      double[][] l = L.Array;
      IACSharpSensor.IACSharpSensor.SensorReached(1312);
    }
  }
}
public class TestAccessToProtectedOnChildInstanceFromParent
{
  class Parent
  {
    protected int a;
    static int x;
    protected Parent()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1313);
      a = x++;
      IACSharpSensor.IACSharpSensor.SensorReached(1314);
    }
    public int TestAccessToProtected(Child c)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1315);
      if (c.a == 0) {
        System.Int32 RNTRNTRNT_382 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1316);
        return RNTRNTRNT_382;
      } else {
        System.Int32 RNTRNTRNT_383 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(1317);
        return RNTRNTRNT_383;
      }
    }
  }
  class Child : Parent
  {
  }
  Child c, d;
  public TestAccessToProtectedOnChildInstanceFromParent()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1318);
    c = new Child();
    d = new Child();
    IACSharpSensor.IACSharpSensor.SensorReached(1319);
  }
  public int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1320);
    if (d.TestAccessToProtected(c) == 1) {
      System.Int32 RNTRNTRNT_384 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1321);
      return RNTRNTRNT_384;
    }
    System.Int32 RNTRNTRNT_385 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1322);
    return RNTRNTRNT_385;
  }
}
