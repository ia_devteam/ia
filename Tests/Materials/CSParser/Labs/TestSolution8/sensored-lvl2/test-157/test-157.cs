using System;
using System.Reflection;
namespace Test
{
  public class MyAttribute : Attribute
  {
    public string val;
    public MyAttribute(string stuff)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(940);
      System.Console.WriteLine(stuff);
      val = stuff;
      IACSharpSensor.IACSharpSensor.SensorReached(941);
    }
  }
  public interface ITest
  {
    string TestProperty {
      [My("testifaceproperty")]
      get;
    }
  }
  [My("testclass")]
  public class Test
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(942);
      System.Reflection.MemberInfo info = typeof(Test);
      object[] attributes = info.GetCustomAttributes(false);
      IACSharpSensor.IACSharpSensor.SensorReached(943);
      for (int i = 0; i < attributes.Length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(944);
        System.Console.WriteLine(attributes[i]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(945);
      if (attributes.Length != 1) {
        System.Int32 RNTRNTRNT_260 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(946);
        return RNTRNTRNT_260;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(947);
      MyAttribute attr = (MyAttribute)attributes[0];
      IACSharpSensor.IACSharpSensor.SensorReached(948);
      if (attr.val != "testclass") {
        System.Int32 RNTRNTRNT_261 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(949);
        return RNTRNTRNT_261;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(950);
      info = typeof(ITest).GetMethod("get_TestProperty");
      attributes = info.GetCustomAttributes(false);
      IACSharpSensor.IACSharpSensor.SensorReached(951);
      for (int i = 0; i < attributes.Length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(952);
        System.Console.WriteLine(attributes[i]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(953);
      if (attributes.Length != 1) {
        System.Int32 RNTRNTRNT_262 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(954);
        return RNTRNTRNT_262;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(955);
      attr = (MyAttribute)attributes[0];
      IACSharpSensor.IACSharpSensor.SensorReached(956);
      if (attr.val != "testifaceproperty") {
        System.Int32 RNTRNTRNT_263 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(957);
        return RNTRNTRNT_263;
      }
      System.Int32 RNTRNTRNT_264 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(958);
      return RNTRNTRNT_264;
    }
  }
}
