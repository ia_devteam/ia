using System;
using System.Collections;
public class X
{
  public static int Main()
  {
    System.Int32 RNTRNTRNT_220 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(683);
    return RNTRNTRNT_220;
  }
  public static void test1(out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(684);
    throw new NotSupportedException();
  }
  public static void test2(int a, out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(685);
    while (a > 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(686);
      if (a == 5) {
        IACSharpSensor.IACSharpSensor.SensorReached(687);
        continue;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(688);
      Console.WriteLine(a);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(689);
    f = 8.53f;
    IACSharpSensor.IACSharpSensor.SensorReached(690);
  }
  public static void test3(long[] b, int c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(691);
    ICollection a;
    IACSharpSensor.IACSharpSensor.SensorReached(692);
    if (b == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(693);
      throw new ArgumentException();
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(694);
      a = (ICollection)b;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(695);
    Console.WriteLine(a);
    IACSharpSensor.IACSharpSensor.SensorReached(696);
  }
  public static int test4(int b, out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(697);
    long a;
    Console.WriteLine("Hello World");
    a = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(698);
    goto World;
    World:
    IACSharpSensor.IACSharpSensor.SensorReached(699);
    Console.WriteLine(a);
    f = 8.53f;
    System.Int32 RNTRNTRNT_221 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(700);
    return RNTRNTRNT_221;
  }
  public static int test5(out float f, long d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(701);
    int a;
    long b = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(702);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(703);
      f = 8.53f;
      IACSharpSensor.IACSharpSensor.SensorReached(704);
      if (d == 500) {
        System.Int32 RNTRNTRNT_222 = 9;
        IACSharpSensor.IACSharpSensor.SensorReached(705);
        return RNTRNTRNT_222;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(706);
      a = 5;
    } catch (NotSupportedException e) {
      IACSharpSensor.IACSharpSensor.SensorReached(707);
      a = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(708);
    } catch (Exception e) {
      System.Int32 RNTRNTRNT_223 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(709);
      return RNTRNTRNT_223;
      IACSharpSensor.IACSharpSensor.SensorReached(710);
    } finally {
      IACSharpSensor.IACSharpSensor.SensorReached(711);
      f = 9.234f;
      IACSharpSensor.IACSharpSensor.SensorReached(712);
    }
    System.Int32 RNTRNTRNT_224 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(713);
    return RNTRNTRNT_224;
  }
  public static int test6(out float f)
  {
    System.Int32 RNTRNTRNT_225 = test5(out f, 50);
    IACSharpSensor.IACSharpSensor.SensorReached(714);
    return RNTRNTRNT_225;
  }
  public static long test7(int[] a, int stop)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(715);
    long b = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(716);
    foreach (int i in a) {
      IACSharpSensor.IACSharpSensor.SensorReached(717);
      b += i;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(718);
    for (int i = 1; i < stop; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(719);
      b *= i;
    }
    System.Int64 RNTRNTRNT_226 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(720);
    return RNTRNTRNT_226;
  }
  public static long test8(int stop)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(721);
    int i;
    long b;
    IACSharpSensor.IACSharpSensor.SensorReached(722);
    for (i = 1; (b = stop) > 3; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(723);
      stop--;
      b += i;
    }
    System.Int64 RNTRNTRNT_227 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(724);
    return RNTRNTRNT_227;
  }
  public static long test9(int stop)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(725);
    long b;
    IACSharpSensor.IACSharpSensor.SensorReached(726);
    while ((b = stop) > 3) {
      IACSharpSensor.IACSharpSensor.SensorReached(727);
      stop--;
      b += stop;
    }
    System.Int64 RNTRNTRNT_228 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(728);
    return RNTRNTRNT_228;
  }
  public static void test10(int a, out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(729);
    if (a == 5) {
      IACSharpSensor.IACSharpSensor.SensorReached(730);
      f = 8.53f;
      IACSharpSensor.IACSharpSensor.SensorReached(731);
      return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(732);
    f = 9.0f;
    IACSharpSensor.IACSharpSensor.SensorReached(733);
  }
  public static long test11(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(734);
    long b;
    IACSharpSensor.IACSharpSensor.SensorReached(735);
    switch (a) {
      case 5:
        IACSharpSensor.IACSharpSensor.SensorReached(736);
        b = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(737);
        break;
      case 9:
        IACSharpSensor.IACSharpSensor.SensorReached(738);
        b = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(739);
        break;
      default:
        System.Int64 RNTRNTRNT_229 = 9;
        IACSharpSensor.IACSharpSensor.SensorReached(740);
        return RNTRNTRNT_229;
    }
    System.Int64 RNTRNTRNT_230 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(741);
    return RNTRNTRNT_230;
  }
  public static void test12(out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(742);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(743);
      f = 9.0f;
    } catch {
      IACSharpSensor.IACSharpSensor.SensorReached(744);
      throw new NotSupportedException();
      IACSharpSensor.IACSharpSensor.SensorReached(745);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(746);
  }
  public static void test13(int a, out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(747);
    do {
      IACSharpSensor.IACSharpSensor.SensorReached(748);
      if (a == 8) {
        IACSharpSensor.IACSharpSensor.SensorReached(749);
        f = 8.5f;
        IACSharpSensor.IACSharpSensor.SensorReached(750);
        return;
      }
    } while (false);
    IACSharpSensor.IACSharpSensor.SensorReached(751);
    f = 1.3f;
    IACSharpSensor.IACSharpSensor.SensorReached(752);
    return;
  }
  public static long test14(int a, out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(753);
    long b;
    IACSharpSensor.IACSharpSensor.SensorReached(754);
    switch (a) {
      case 1:
        IACSharpSensor.IACSharpSensor.SensorReached(755);
        goto case 2;
      case 2:
        IACSharpSensor.IACSharpSensor.SensorReached(756);
        f = 9.53f;
        System.Int64 RNTRNTRNT_231 = 9;
        IACSharpSensor.IACSharpSensor.SensorReached(757);
        return RNTRNTRNT_231;
      case 3:
        IACSharpSensor.IACSharpSensor.SensorReached(758);
        goto default;
      default:
        IACSharpSensor.IACSharpSensor.SensorReached(759);
        b = 10;
        IACSharpSensor.IACSharpSensor.SensorReached(760);
        break;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(761);
    f = 10.0f;
    System.Int64 RNTRNTRNT_232 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(762);
    return RNTRNTRNT_232;
  }
  public static int test15(int b, out float f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(763);
    long a;
    Console.WriteLine("Hello World");
    a = 5;
    f = 8.53f;
    IACSharpSensor.IACSharpSensor.SensorReached(764);
    goto World;
    World:
    IACSharpSensor.IACSharpSensor.SensorReached(765);
    Console.WriteLine(a);
    System.Int32 RNTRNTRNT_233 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(766);
    return RNTRNTRNT_233;
  }
  public static void test16()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(767);
    int value;
    IACSharpSensor.IACSharpSensor.SensorReached(768);
    for (int i = 0; i < 5; ++i) {
      IACSharpSensor.IACSharpSensor.SensorReached(769);
      if (i == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(770);
        continue;
      } else if (i == 1) {
        IACSharpSensor.IACSharpSensor.SensorReached(772);
        value = 2;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(771);
        value = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(773);
      if (value > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(774);
        return;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(775);
  }
  public static void test17()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(776);
    int value;
    long charCount = 9;
    long testit = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(777);
    while (charCount > 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(778);
      --charCount;
      IACSharpSensor.IACSharpSensor.SensorReached(779);
      if (testit == 8) {
        IACSharpSensor.IACSharpSensor.SensorReached(780);
        if (testit == 9) {
          IACSharpSensor.IACSharpSensor.SensorReached(781);
          throw new Exception();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(782);
        continue;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(783);
        value = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(784);
      Console.WriteLine(value);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(785);
  }
  static void test18(int a, out int f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(786);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(787);
      if (a == 5) {
        IACSharpSensor.IACSharpSensor.SensorReached(788);
        throw new Exception();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(789);
      f = 9;
    } catch (IndexOutOfRangeException) {
      IACSharpSensor.IACSharpSensor.SensorReached(790);
      throw new FormatException();
      IACSharpSensor.IACSharpSensor.SensorReached(791);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(792);
  }
  static int test19()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(793);
    int res;
    int a = Environment.NewLine.Length;
    int fin = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(794);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(795);
      res = 10 / a;
      IACSharpSensor.IACSharpSensor.SensorReached(796);
      throw new NotImplementedException();
    } catch (NotImplementedException e) {
      IACSharpSensor.IACSharpSensor.SensorReached(797);
      fin = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(798);
      throw new NotImplementedException();
      IACSharpSensor.IACSharpSensor.SensorReached(799);
    } finally {
      IACSharpSensor.IACSharpSensor.SensorReached(800);
      fin = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(801);
    }
    System.Int32 RNTRNTRNT_234 = fin;
    IACSharpSensor.IACSharpSensor.SensorReached(802);
    return RNTRNTRNT_234;
  }
  static int test20()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(803);
    try {
      System.Int32 RNTRNTRNT_235 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(804);
      return RNTRNTRNT_235;
    } catch (Exception) {
      IACSharpSensor.IACSharpSensor.SensorReached(805);
      throw;
      IACSharpSensor.IACSharpSensor.SensorReached(806);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(807);
  }
  static int test21()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(808);
    int res;
    IACSharpSensor.IACSharpSensor.SensorReached(809);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(810);
      res = 4;
      System.Int32 RNTRNTRNT_236 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(811);
      return RNTRNTRNT_236;
    } catch (DivideByZeroException) {
      IACSharpSensor.IACSharpSensor.SensorReached(812);
      res = 33;
      IACSharpSensor.IACSharpSensor.SensorReached(813);
    } finally {
    }
    System.Int32 RNTRNTRNT_237 = res;
    IACSharpSensor.IACSharpSensor.SensorReached(814);
    return RNTRNTRNT_237;
  }
  static int test22()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(815);
    int res;
    IACSharpSensor.IACSharpSensor.SensorReached(816);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(817);
      res = 4;
      System.Int32 RNTRNTRNT_238 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(818);
      return RNTRNTRNT_238;
    } catch (DivideByZeroException) {
      IACSharpSensor.IACSharpSensor.SensorReached(819);
      res = 33;
      IACSharpSensor.IACSharpSensor.SensorReached(820);
    }
    System.Int32 RNTRNTRNT_239 = res;
    IACSharpSensor.IACSharpSensor.SensorReached(821);
    return RNTRNTRNT_239;
  }
  static int test23(object obj, int a, out bool test)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(822);
    if (obj == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(823);
      throw new ArgumentNullException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(824);
    if (a == 5) {
      IACSharpSensor.IACSharpSensor.SensorReached(825);
      test = false;
      System.Int32 RNTRNTRNT_240 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(826);
      return RNTRNTRNT_240;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(827);
      test = true;
      System.Int32 RNTRNTRNT_241 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(828);
      return RNTRNTRNT_241;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(829);
  }
  static long test24(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(830);
    long b;
    IACSharpSensor.IACSharpSensor.SensorReached(831);
    switch (a) {
      case 0:
        System.Int64 RNTRNTRNT_242 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(832);
        return RNTRNTRNT_242;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(833);
    if (a > 2) {
      IACSharpSensor.IACSharpSensor.SensorReached(834);
      if (a == 5) {
        IACSharpSensor.IACSharpSensor.SensorReached(835);
        b = 4;
      } else if (a == 6) {
        IACSharpSensor.IACSharpSensor.SensorReached(837);
        b = 5;
      } else {
        System.Int64 RNTRNTRNT_243 = 7;
        IACSharpSensor.IACSharpSensor.SensorReached(836);
        return RNTRNTRNT_243;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(838);
      Console.WriteLine(b);
      System.Int64 RNTRNTRNT_244 = b;
      IACSharpSensor.IACSharpSensor.SensorReached(839);
      return RNTRNTRNT_244;
    }
    System.Int64 RNTRNTRNT_245 = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(840);
    return RNTRNTRNT_245;
  }
  static long test25(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(841);
    long b, c;
    IACSharpSensor.IACSharpSensor.SensorReached(842);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(843);
      b = 5;
    } catch (NotSupportedException) {
      IACSharpSensor.IACSharpSensor.SensorReached(844);
      throw new InvalidOperationException();
      IACSharpSensor.IACSharpSensor.SensorReached(845);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(846);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(847);
      c = 5;
    } catch {
      IACSharpSensor.IACSharpSensor.SensorReached(848);
      throw new InvalidOperationException();
      IACSharpSensor.IACSharpSensor.SensorReached(849);
    }
    System.Int64 RNTRNTRNT_246 = b + c;
    IACSharpSensor.IACSharpSensor.SensorReached(850);
    return RNTRNTRNT_246;
  }
  static void test26()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(851);
    int j;
    IACSharpSensor.IACSharpSensor.SensorReached(852);
    for (int i = 0; i < 10; i = j) {
      IACSharpSensor.IACSharpSensor.SensorReached(853);
      j = i + 1;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(854);
  }
  static int test27()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(855);
    while (true) {
      IACSharpSensor.IACSharpSensor.SensorReached(856);
      break;
      IACSharpSensor.IACSharpSensor.SensorReached(857);
      while (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(858);
        Console.WriteLine("Test");
      }
    }
    System.Int32 RNTRNTRNT_247 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(859);
    return RNTRNTRNT_247;
  }
  static void test28(out object value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(860);
    if (true) {
      IACSharpSensor.IACSharpSensor.SensorReached(861);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(862);
        value = null;
        IACSharpSensor.IACSharpSensor.SensorReached(863);
        return;
      } catch {
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(864);
    value = null;
    IACSharpSensor.IACSharpSensor.SensorReached(865);
  }
  static bool test29(out int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(866);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(867);
      a = 0;
      System.Boolean RNTRNTRNT_248 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(868);
      return RNTRNTRNT_248;
    } catch (System.Exception) {
      IACSharpSensor.IACSharpSensor.SensorReached(869);
      a = -1;
      System.Boolean RNTRNTRNT_249 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(870);
      return RNTRNTRNT_249;
      IACSharpSensor.IACSharpSensor.SensorReached(871);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(872);
  }
  public string test30(out string outparam)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(873);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(874);
      if (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(875);
        outparam = "";
        System.String RNTRNTRNT_250 = "";
        IACSharpSensor.IACSharpSensor.SensorReached(876);
        return RNTRNTRNT_250;
      }
    } catch {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(877);
    outparam = null;
    IACSharpSensor.IACSharpSensor.SensorReached(878);
    return null;
  }
  public string test31(int blah)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(879);
    switch (blah) {
      case 1:
        System.String RNTRNTRNT_251 = ("foo");
        IACSharpSensor.IACSharpSensor.SensorReached(880);
        return RNTRNTRNT_251;
        IACSharpSensor.IACSharpSensor.SensorReached(881);
        break;
      case 2:
        System.String RNTRNTRNT_252 = ("bar");
        IACSharpSensor.IACSharpSensor.SensorReached(882);
        return RNTRNTRNT_252;
        IACSharpSensor.IACSharpSensor.SensorReached(883);
        break;
      case 3:
        System.String RNTRNTRNT_253 = ("baz");
        IACSharpSensor.IACSharpSensor.SensorReached(884);
        return RNTRNTRNT_253;
        IACSharpSensor.IACSharpSensor.SensorReached(885);
        break;
      default:
        IACSharpSensor.IACSharpSensor.SensorReached(886);
        throw new ArgumentException("Value 0x" + blah.ToString("x4") + " is not supported.");
    }
  }
  public void test32()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(887);
    while (true) {
      IACSharpSensor.IACSharpSensor.SensorReached(888);
      System.Threading.Thread.Sleep(1);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(889);
    Console.WriteLine("Hello");
    IACSharpSensor.IACSharpSensor.SensorReached(890);
  }
  public int test33()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(891);
    int i = 0;
    System.Int32 RNTRNTRNT_254 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(892);
    return RNTRNTRNT_254;
    IACSharpSensor.IACSharpSensor.SensorReached(893);
    if (i == 0) {
      System.Int32 RNTRNTRNT_255 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(894);
      return RNTRNTRNT_255;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(895);
  }
  public void test34()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(896);
    int y, x = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(897);
    if (x > 3) {
      IACSharpSensor.IACSharpSensor.SensorReached(898);
      y = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(899);
      goto end;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(900);
    return;
    end:
    IACSharpSensor.IACSharpSensor.SensorReached(901);
    x = y;
    IACSharpSensor.IACSharpSensor.SensorReached(902);
  }
  public static void test35(int a, bool test)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(903);
    switch (a) {
      case 3:
        IACSharpSensor.IACSharpSensor.SensorReached(904);
        if (test) {
          IACSharpSensor.IACSharpSensor.SensorReached(905);
          break;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(906);
        return;
      default:
        IACSharpSensor.IACSharpSensor.SensorReached(907);
        return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(908);
  }
  public static void test36()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(909);
    string myVar;
    int counter = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(910);
    while (true) {
      IACSharpSensor.IACSharpSensor.SensorReached(911);
      if (counter < 3) {
        IACSharpSensor.IACSharpSensor.SensorReached(912);
        counter++;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(913);
        myVar = "assigned";
        IACSharpSensor.IACSharpSensor.SensorReached(914);
        break;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(915);
    Console.WriteLine(myVar);
    IACSharpSensor.IACSharpSensor.SensorReached(916);
  }
  public static void test37()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(917);
    int x = 0;
    int y = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(918);
    switch (x) {
      case 0:
        IACSharpSensor.IACSharpSensor.SensorReached(919);
        switch (y) {
          case 0:
            IACSharpSensor.IACSharpSensor.SensorReached(920);
            goto k_0;
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(921);
            throw new Exception();
        }
    }
    k_0:
    ;
    IACSharpSensor.IACSharpSensor.SensorReached(922);
  }
  public static int test38()
  {
    System.Int32 RNTRNTRNT_256 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(923);
    return RNTRNTRNT_256;
    foo:
    ;
    IACSharpSensor.IACSharpSensor.SensorReached(924);
  }
  static int test40(int stop)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(925);
    int service;
    int pos = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(926);
    do {
      IACSharpSensor.IACSharpSensor.SensorReached(927);
      service = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(928);
      break;
    } while (pos < stop);
    System.Int32 RNTRNTRNT_257 = service;
    IACSharpSensor.IACSharpSensor.SensorReached(929);
    return RNTRNTRNT_257;
  }
}
