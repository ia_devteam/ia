class X
{
  static object get_non_null()
  {
    System.Object RNTRNTRNT_58 = new X();
    IACSharpSensor.IACSharpSensor.SensorReached(200);
    return RNTRNTRNT_58;
  }
  static object get_null()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(201);
    return null;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(202);
    int a = 5;
    object o;
    decimal d = 0m;
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    if (!(get_non_null() is object)) {
      System.Int32 RNTRNTRNT_59 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(204);
      return RNTRNTRNT_59;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(205);
    if (get_null() is object) {
      System.Int32 RNTRNTRNT_60 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(206);
      return RNTRNTRNT_60;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(207);
    if (!(a is object)) {
      System.Int32 RNTRNTRNT_61 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(208);
      return RNTRNTRNT_61;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(209);
    if (null is object) {
      System.Int32 RNTRNTRNT_62 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(210);
      return RNTRNTRNT_62;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(211);
    o = a;
    IACSharpSensor.IACSharpSensor.SensorReached(212);
    if (!(o is int)) {
      System.Int32 RNTRNTRNT_63 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(213);
      return RNTRNTRNT_63;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(214);
    if (d is int) {
      System.Int32 RNTRNTRNT_64 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(215);
      return RNTRNTRNT_64;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(216);
    object oi = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(217);
    if (!(oi is int)) {
      System.Int32 RNTRNTRNT_65 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(218);
      return RNTRNTRNT_65;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(219);
    System.Console.WriteLine("Is tests pass");
    System.Int32 RNTRNTRNT_66 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    return RNTRNTRNT_66;
  }
}
