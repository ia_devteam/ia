using System;
interface A
{
  void X();
}
interface B
{
  void X();
}
class C : A, B
{
  int var;
  public void X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(382);
    var++;
    IACSharpSensor.IACSharpSensor.SensorReached(383);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(384);
    C c = new C();
    A a = c;
    B b = c;
    IACSharpSensor.IACSharpSensor.SensorReached(385);
    if (c.var != 0) {
      System.Int32 RNTRNTRNT_112 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(386);
      return RNTRNTRNT_112;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(387);
    a.X();
    IACSharpSensor.IACSharpSensor.SensorReached(388);
    if (c.var != 1) {
      System.Int32 RNTRNTRNT_113 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(389);
      return RNTRNTRNT_113;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(390);
    b.X();
    IACSharpSensor.IACSharpSensor.SensorReached(391);
    if (c.var != 2) {
      System.Int32 RNTRNTRNT_114 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(392);
      return RNTRNTRNT_114;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(393);
    c.X();
    IACSharpSensor.IACSharpSensor.SensorReached(394);
    if (c.var != 3) {
      System.Int32 RNTRNTRNT_115 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(395);
      return RNTRNTRNT_115;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(396);
    Console.WriteLine("Test passes");
    System.Int32 RNTRNTRNT_116 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(397);
    return RNTRNTRNT_116;
  }
}
