using System;
class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(930);
    Console.WriteLine("test");
    TestClass tst = new TestClass();
    tst.test("test");
    TestInterface ti = (TestInterface)tst;
    ti.test("test");
    System.Int32 RNTRNTRNT_258 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(931);
    return RNTRNTRNT_258;
  }
  public interface TestInterface
  {
    string test(string name);
  }
  public class TestClass : TestInterface
  {
    public string test(string name)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(932);
      Console.WriteLine("test2");
      System.String RNTRNTRNT_259 = name + " testar";
      IACSharpSensor.IACSharpSensor.SensorReached(933);
      return RNTRNTRNT_259;
    }
  }
}
