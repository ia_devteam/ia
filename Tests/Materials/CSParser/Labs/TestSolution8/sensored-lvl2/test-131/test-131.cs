using System;
public class SimpleAttribute : Attribute
{
  string n;
  public SimpleAttribute(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(314);
    n = name;
    IACSharpSensor.IACSharpSensor.SensorReached(315);
  }
}
public class Blah
{
  public enum Foo
  {
    A,
    [Simple("second")]
    B,
    C
  }
  public static int Main()
  {
    System.Int32 RNTRNTRNT_91 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(316);
    return RNTRNTRNT_91;
  }
}
