using System;
using System.Reflection;
[AttributeUsage(AttributeTargets.All)]
public class My : Attribute
{
  public object o;
  public My(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(959);
    this.o = o;
    IACSharpSensor.IACSharpSensor.SensorReached(960);
  }
  [My(TypeCode.Empty)]
  public class Test
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(961);
      System.Reflection.MemberInfo info = typeof(Test);
      object[] attributes = info.GetCustomAttributes(false);
      IACSharpSensor.IACSharpSensor.SensorReached(962);
      for (int i = 0; i < attributes.Length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(963);
        System.Console.WriteLine(attributes[i]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(964);
      if (attributes.Length != 1) {
        System.Int32 RNTRNTRNT_265 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(965);
        return RNTRNTRNT_265;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(966);
      My attr = (My)attributes[0];
      IACSharpSensor.IACSharpSensor.SensorReached(967);
      if ((TypeCode)attr.o != TypeCode.Empty) {
        System.Int32 RNTRNTRNT_266 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(968);
        return RNTRNTRNT_266;
      }
      System.Int32 RNTRNTRNT_267 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(969);
      return RNTRNTRNT_267;
    }
  }
}
