using System;
using System.Reflection;
using System.Runtime.InteropServices;
[StructLayout(LayoutKind.Explicit, Size = 32, CharSet = CharSet.Unicode)]
struct MyUnicode
{
  [FieldOffset(0)]
  public float fh_float;
  [FieldOffset(0)]
  public int fh_int;
}
[StructLayout(LayoutKind.Explicit, Size = 32, CharSet = CharSet.Ansi)]
struct MyAnsi
{
  [FieldOffset(0)]
  public float fh_float;
  [FieldOffset(0)]
  public int fh_int;
}
[StructLayout(LayoutKind.Explicit, Size = 32, CharSet = CharSet.Auto)]
struct MyAuto
{
  [FieldOffset(0)]
  public float fh_float;
  [FieldOffset(0)]
  public int fh_int;
}
class test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(176);
    int errors = 0;
    Type t = typeof(MyUnicode);
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    if ((t.Attributes & TypeAttributes.StringFormatMask) != TypeAttributes.UnicodeClass) {
      IACSharpSensor.IACSharpSensor.SensorReached(178);
      Console.WriteLine("Class MyUnicode does not have Unicode bit set");
      errors += 1;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(179);
    t = typeof(MyAuto);
    IACSharpSensor.IACSharpSensor.SensorReached(180);
    if ((t.Attributes & TypeAttributes.StringFormatMask) != TypeAttributes.AutoClass) {
      IACSharpSensor.IACSharpSensor.SensorReached(181);
      Console.WriteLine("Class MyAuto does not have Auto bit set");
      errors += 2;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(182);
    t = typeof(MyAnsi);
    IACSharpSensor.IACSharpSensor.SensorReached(183);
    if ((t.Attributes & TypeAttributes.StringFormatMask) != TypeAttributes.AnsiClass) {
      IACSharpSensor.IACSharpSensor.SensorReached(184);
      Console.WriteLine("Class MyUnicode does not have Ansi bit set");
      errors += 4;
    }
    System.Int32 RNTRNTRNT_51 = errors;
    IACSharpSensor.IACSharpSensor.SensorReached(185);
    return RNTRNTRNT_51;
  }
}
