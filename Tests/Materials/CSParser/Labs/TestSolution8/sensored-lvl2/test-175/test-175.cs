using System;
struct RVA
{
  public uint value;
  public RVA(uint val)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1323);
    value = val;
    IACSharpSensor.IACSharpSensor.SensorReached(1324);
  }
  public static implicit operator RVA(uint val)
  {
    RVA RNTRNTRNT_386 = new RVA(val);
    IACSharpSensor.IACSharpSensor.SensorReached(1325);
    return RNTRNTRNT_386;
  }
  public static implicit operator uint(RVA rva)
  {
    System.UInt32 RNTRNTRNT_387 = rva.value;
    IACSharpSensor.IACSharpSensor.SensorReached(1326);
    return RNTRNTRNT_387;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1327);
    RVA a = 10;
    RVA b = 20;
    IACSharpSensor.IACSharpSensor.SensorReached(1328);
    if (a > b) {
      System.Int32 RNTRNTRNT_388 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1329);
      return RNTRNTRNT_388;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1330);
    if (a + b != 30) {
      System.Int32 RNTRNTRNT_389 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1331);
      return RNTRNTRNT_389;
    }
    System.Int32 RNTRNTRNT_390 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1332);
    return RNTRNTRNT_390;
  }
}
