using System;
using System.Threading;
using System.Runtime.InteropServices;
class Test
{
  delegate int SimpleDelegate(int a);
  static int cb_state = 0;
  static int F(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(72);
    Console.WriteLine("Test.F from delegate: " + a);
    IACSharpSensor.IACSharpSensor.SensorReached(73);
    throw new NotImplementedException();
  }
  static void async_callback(IAsyncResult ar)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(74);
    Console.WriteLine("Async Callback " + ar.AsyncState);
    cb_state = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(75);
    throw new NotImplementedException();
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(76);
    SimpleDelegate d = new SimpleDelegate(F);
    AsyncCallback ac = new AsyncCallback(async_callback);
    string state1 = "STATE1";
    int res = 0;
    IAsyncResult ar1 = d.BeginInvoke(1, ac, state1);
    ar1.AsyncWaitHandle.WaitOne();
    IACSharpSensor.IACSharpSensor.SensorReached(77);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(78);
      res = d.EndInvoke(ar1);
    } catch (NotImplementedException) {
      IACSharpSensor.IACSharpSensor.SensorReached(79);
      res = 1;
      Console.WriteLine("received exception ... OK");
      IACSharpSensor.IACSharpSensor.SensorReached(80);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(81);
    while (cb_state == 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(82);
      Thread.Sleep(0);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(83);
    if (cb_state != 1) {
      System.Int32 RNTRNTRNT_18 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(84);
      return RNTRNTRNT_18;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(85);
    if (res != 1) {
      System.Int32 RNTRNTRNT_19 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(86);
      return RNTRNTRNT_19;
    }
    System.Int32 RNTRNTRNT_20 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(87);
    return RNTRNTRNT_20;
  }
}
