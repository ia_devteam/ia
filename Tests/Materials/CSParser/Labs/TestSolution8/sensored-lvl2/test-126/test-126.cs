using System;
interface Iface
{
  void Method();
}
class X : Iface
{
  void Iface.Method()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(281);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(282);
    X x = new X();
    Iface f = x;
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    if (f.ToString() != "X") {
      System.Int32 RNTRNTRNT_78 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(284);
      return RNTRNTRNT_78;
    }
    System.Int32 RNTRNTRNT_79 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(285);
    return RNTRNTRNT_79;
  }
}
