using System;
using System.Reflection;
public class SimpleAttribute : Attribute
{
  string n;
  public SimpleAttribute(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(290);
    n = name;
    IACSharpSensor.IACSharpSensor.SensorReached(291);
  }
}
[AttributeUsage(AttributeTargets.All)]
public class MineAttribute : Attribute
{
  public MineAttribute(params Type[] t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(292);
    types = t;
    IACSharpSensor.IACSharpSensor.SensorReached(293);
  }
  public Type[] types;
}
[Mine(typeof(int), typeof(string), typeof(object[]))]
public class Foo
{
  public static int MM()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(294);
    object[] attrs = typeof(Foo).GetCustomAttributes(typeof(MineAttribute), true);
    MineAttribute ma = (MineAttribute)attrs[0];
    IACSharpSensor.IACSharpSensor.SensorReached(295);
    if (ma.types[0] != typeof(int)) {
      IACSharpSensor.IACSharpSensor.SensorReached(296);
      Console.WriteLine("failed");
      System.Int32 RNTRNTRNT_82 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(297);
      return RNTRNTRNT_82;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(298);
    if (ma.types[1] != typeof(string)) {
      IACSharpSensor.IACSharpSensor.SensorReached(299);
      Console.WriteLine("failed");
      System.Int32 RNTRNTRNT_83 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(300);
      return RNTRNTRNT_83;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(301);
    if (ma.types[2] != typeof(object[])) {
      IACSharpSensor.IACSharpSensor.SensorReached(302);
      Console.WriteLine("failed");
      System.Int32 RNTRNTRNT_84 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(303);
      return RNTRNTRNT_84;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(304);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_85 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(305);
    return RNTRNTRNT_85;
  }
}
public class Blah
{
  int i;
  public int Value {
    [Simple("Foo!")]
    get {
      System.Int32 RNTRNTRNT_86 = i;
      IACSharpSensor.IACSharpSensor.SensorReached(306);
      return RNTRNTRNT_86;
    }
    [Simple("Bar !")]
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(307);
      i = value;
      IACSharpSensor.IACSharpSensor.SensorReached(308);
    }
  }
  [Simple((string)null)]
  int Another()
  {
    System.Int32 RNTRNTRNT_87 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(309);
    return RNTRNTRNT_87;
  }
  public static int Main()
  {
    System.Int32 RNTRNTRNT_88 = Foo.MM();
    IACSharpSensor.IACSharpSensor.SensorReached(310);
    return RNTRNTRNT_88;
  }
}
