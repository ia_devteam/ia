using System;
public delegate long MyDelegate(int a);
public interface X
{
  event EventHandler Foo;
  event MyDelegate TestEvent;
}
public class Y : X
{
  static int a = 0;
  event EventHandler X.Foo {
    add { }
    remove { }
  }
  public event EventHandler Foo;
  public event MyDelegate TestEvent;
  public int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(626);
    X x = this;
    Foo += new EventHandler(callback1);
    TestEvent += new MyDelegate(callback2);
    x.Foo += new EventHandler(callback3);
    IACSharpSensor.IACSharpSensor.SensorReached(627);
    if (a != 0) {
      System.Int32 RNTRNTRNT_203 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(628);
      return RNTRNTRNT_203;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(629);
    Foo(this, new EventArgs());
    IACSharpSensor.IACSharpSensor.SensorReached(630);
    if (a != 1) {
      System.Int32 RNTRNTRNT_204 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(631);
      return RNTRNTRNT_204;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(632);
    if (TestEvent(2) != 4) {
      System.Int32 RNTRNTRNT_205 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(633);
      return RNTRNTRNT_205;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(634);
    if (a != 2) {
      System.Int32 RNTRNTRNT_206 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(635);
      return RNTRNTRNT_206;
    }
    System.Int32 RNTRNTRNT_207 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(636);
    return RNTRNTRNT_207;
  }
  private static void callback1(object sender, EventArgs e)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(637);
    a = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(638);
  }
  private static long callback2(int b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(639);
    a = b;
    System.Int64 RNTRNTRNT_208 = a * a;
    IACSharpSensor.IACSharpSensor.SensorReached(640);
    return RNTRNTRNT_208;
  }
  private static void callback3(object sender, EventArgs e)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(641);
    a = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(642);
  }
}
public class Z : Y
{
  public delegate int SomeEventHandler();
  public static event SomeEventHandler BuildStarted;
  static int a()
  {
    System.Int32 RNTRNTRNT_209 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(643);
    return RNTRNTRNT_209;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(644);
    Z z = new Z();
    int result = z.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(645);
    if (result != 0) {
      System.Int32 RNTRNTRNT_210 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(646);
      return RNTRNTRNT_210;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(647);
    if (BuildStarted != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(648);
      BuildStarted();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(649);
    BuildStarted = new SomeEventHandler(a);
    IACSharpSensor.IACSharpSensor.SensorReached(650);
    if (BuildStarted() != 1) {
      System.Int32 RNTRNTRNT_211 = 50;
      IACSharpSensor.IACSharpSensor.SensorReached(651);
      return RNTRNTRNT_211;
    }
    System.Int32 RNTRNTRNT_212 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(652);
    return RNTRNTRNT_212;
  }
}
public class Static
{
  public static event EventHandler Test;
  public void Fire()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(653);
    if (Test != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(654);
      Test(null, null);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(655);
  }
}
