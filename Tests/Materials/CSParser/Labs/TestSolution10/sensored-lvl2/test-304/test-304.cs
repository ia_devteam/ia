using System;
using System.Collections;
using C = A.D;
class A
{
  internal class D
  {
  }
  public class B
  {
    class C
    {
    }
    public B()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(66);
      string error = "";
      IACSharpSensor.IACSharpSensor.SensorReached(67);
      if (typeof(C) != typeof(A.B.C)) {
        IACSharpSensor.IACSharpSensor.SensorReached(68);
        error += " 'typeof' keyword,";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(69);
      object o0 = new C();
      IACSharpSensor.IACSharpSensor.SensorReached(70);
      if (o0.GetType() != typeof(A.B.C)) {
        IACSharpSensor.IACSharpSensor.SensorReached(71);
        error += " 'new' keyword,";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(72);
      C o1 = new C();
      IACSharpSensor.IACSharpSensor.SensorReached(73);
      if (o1.GetType() != typeof(A.B.C)) {
        IACSharpSensor.IACSharpSensor.SensorReached(74);
        error += " type declaration,";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(75);
      object o2 = new A.B.C();
      IACSharpSensor.IACSharpSensor.SensorReached(76);
      if (!(o2 is C)) {
        IACSharpSensor.IACSharpSensor.SensorReached(77);
        error += " 'is' keyword,";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(78);
      object o3 = o2 as C;
      IACSharpSensor.IACSharpSensor.SensorReached(79);
      if (o3 == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(80);
        error += " 'as' keyword,";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(81);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(82);
        object o4 = (C)o2;
      } catch {
        IACSharpSensor.IACSharpSensor.SensorReached(83);
        error += " type cast,";
        IACSharpSensor.IACSharpSensor.SensorReached(84);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(85);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(86);
        object o5 = (C)(o2);
      } catch {
        IACSharpSensor.IACSharpSensor.SensorReached(87);
        error += " invocation-or-cast,";
        IACSharpSensor.IACSharpSensor.SensorReached(88);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(89);
      object o6 = new C[1];
      IACSharpSensor.IACSharpSensor.SensorReached(90);
      if (o6.GetType().GetElementType() != typeof(A.B.C)) {
        IACSharpSensor.IACSharpSensor.SensorReached(91);
        error += " array creation,";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(92);
      if (typeof(C[]).GetElementType() != typeof(A.B.C)) {
        IACSharpSensor.IACSharpSensor.SensorReached(93);
        error += " composed cast (array),";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(94);
      ArrayList a = new ArrayList();
      a.Add(new A.B.C());
      IACSharpSensor.IACSharpSensor.SensorReached(95);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(96);
        foreach (C c in a) {
        }
      } catch {
        IACSharpSensor.IACSharpSensor.SensorReached(97);
        error += " 'foreach' statement,";
        IACSharpSensor.IACSharpSensor.SensorReached(98);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(99);
      if (error.Length != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(100);
        throw new Exception("The following couldn't resolve C as A+B+C:" + error);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(101);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(102);
    object o = new A.B();
    IACSharpSensor.IACSharpSensor.SensorReached(103);
  }
}
