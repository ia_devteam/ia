using System;
struct X : IDisposable
{
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(276);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(277);
    X x = new X();
    IACSharpSensor.IACSharpSensor.SensorReached(278);
    using (x) {
      ;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(279);
  }
}
