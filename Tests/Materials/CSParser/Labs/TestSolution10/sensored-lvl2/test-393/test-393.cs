using System;
public class A
{
  protected string name;
  public A(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(589);
    this.name = "A::" + name;
    IACSharpSensor.IACSharpSensor.SensorReached(590);
  }
  public A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(591);
  }
  public string Name {
    get {
      System.String RNTRNTRNT_122 = name;
      IACSharpSensor.IACSharpSensor.SensorReached(592);
      return RNTRNTRNT_122;
    }
  }
}
public class B : A
{
  public B(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(593);
    this.name = "B::" + name;
    IACSharpSensor.IACSharpSensor.SensorReached(594);
  }
  public B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(595);
  }
}
public class C : B
{
  public C(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(596);
    this.name = "C::" + name;
    IACSharpSensor.IACSharpSensor.SensorReached(597);
  }
}
public class Tester
{
  delegate A MethodHandler(string name);
  static A MethodSampleA(string name)
  {
    A RNTRNTRNT_123 = new A(name);
    IACSharpSensor.IACSharpSensor.SensorReached(598);
    return RNTRNTRNT_123;
  }
  static B MethodSampleB(string name)
  {
    B RNTRNTRNT_124 = new B(name);
    IACSharpSensor.IACSharpSensor.SensorReached(599);
    return RNTRNTRNT_124;
  }
  static C MethodSampleC(string name)
  {
    C RNTRNTRNT_125 = new C(name);
    IACSharpSensor.IACSharpSensor.SensorReached(600);
    return RNTRNTRNT_125;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(601);
    MethodHandler a = MethodSampleA;
    MethodHandler b = MethodSampleB;
    MethodHandler c = MethodSampleC;
    A instance1 = a("Hello");
    A instance2 = b("World");
    A instance3 = c("!");
    Console.WriteLine(instance1.Name);
    Console.WriteLine(instance2.Name);
    Console.WriteLine(instance3.Name);
    IACSharpSensor.IACSharpSensor.SensorReached(602);
  }
}
