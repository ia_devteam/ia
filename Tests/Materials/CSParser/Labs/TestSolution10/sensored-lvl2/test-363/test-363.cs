public class Location
{
  public static readonly Location UnknownLocation = new Location();
  private Location()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(413);
  }
}
public abstract class Element
{
  private Location _location = Location.UnknownLocation;
  protected virtual Location Location {
    get {
      Location RNTRNTRNT_72 = _location;
      IACSharpSensor.IACSharpSensor.SensorReached(414);
      return RNTRNTRNT_72;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(415);
      _location = value;
      IACSharpSensor.IACSharpSensor.SensorReached(416);
    }
  }
}
public class T
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(417);
  }
}
