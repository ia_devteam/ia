using System;
public class Foo
{
  [Obsolete()]
  public void Something()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(324);
  }
}
public class Bar : Foo
{
  public new void Something()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(325);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(326);
  }
}
