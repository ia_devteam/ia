using System;
class TestVararg
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(690);
    int result = Vararg.AddABunchOfInts(__arglist(2, 3, 4));
    Console.WriteLine("Answer: {0}", result);
    IACSharpSensor.IACSharpSensor.SensorReached(691);
    if (result != 9) {
      System.Int32 RNTRNTRNT_152 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(692);
      return RNTRNTRNT_152;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(693);
    result = Vararg.AddASecondBunchOfInts(16, __arglist(2, 3, 4));
    Console.WriteLine("Answer: {0}", result);
    IACSharpSensor.IACSharpSensor.SensorReached(694);
    if (result != 9) {
      System.Int32 RNTRNTRNT_153 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(695);
      return RNTRNTRNT_153;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(696);
    Vararg s = new Vararg();
    result = s.InstAddABunchOfInts(__arglist(2, 3, 4, 5));
    Console.WriteLine("Answer: {0}", result);
    IACSharpSensor.IACSharpSensor.SensorReached(697);
    if (result != 14) {
      System.Int32 RNTRNTRNT_154 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(698);
      return RNTRNTRNT_154;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(699);
    result = s.InstAddASecondBunchOfInts(16, __arglist(2, 3, 4, 5, 6));
    Console.WriteLine("Answer: {0}", result);
    IACSharpSensor.IACSharpSensor.SensorReached(700);
    if (result != 20) {
      System.Int32 RNTRNTRNT_155 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(701);
      return RNTRNTRNT_155;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(702);
    result = s.InstVtAddABunchOfInts(__arglist(2, 3, 4, 5)).res;
    Console.WriteLine("Answer: {0}", result);
    IACSharpSensor.IACSharpSensor.SensorReached(703);
    if (result != 14) {
      System.Int32 RNTRNTRNT_156 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(704);
      return RNTRNTRNT_156;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(705);
    result = s.InstVtAddASecondBunchOfInts(16, __arglist(2, 3, 4, 5, 6)).res;
    Console.WriteLine("Answer: {0}", result);
    IACSharpSensor.IACSharpSensor.SensorReached(706);
    if (result != 20) {
      System.Int32 RNTRNTRNT_157 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(707);
      return RNTRNTRNT_157;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(708);
    result = Vararg.VtAddABunchOfInts(__arglist(2, 3, 4, 5, 1)).res;
    Console.WriteLine("Answer: {0}", result);
    IACSharpSensor.IACSharpSensor.SensorReached(709);
    if (result != 15) {
      System.Int32 RNTRNTRNT_158 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(710);
      return RNTRNTRNT_158;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(711);
    result = Vararg.VtAddASecondBunchOfInts(16, __arglist(2, 3, 4, 5, 6, 1)).res;
    Console.WriteLine("Answer: {0}", result);
    IACSharpSensor.IACSharpSensor.SensorReached(712);
    if (result != 21) {
      System.Int32 RNTRNTRNT_159 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(713);
      return RNTRNTRNT_159;
    }
    System.Int32 RNTRNTRNT_160 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(714);
    return RNTRNTRNT_160;
  }
}
