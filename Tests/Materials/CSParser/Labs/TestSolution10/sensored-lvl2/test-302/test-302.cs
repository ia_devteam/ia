using System;
class A
{
  class C : Exception
  {
  }
  public class B
  {
    class C : Exception
    {
    }
    public B()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(55);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(56);
        throw new A.B.C();
      } catch (C e) {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(57);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(58);
    object o = new A.B();
    IACSharpSensor.IACSharpSensor.SensorReached(59);
  }
}
