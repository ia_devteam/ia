using System;
public class A
{
  public void DoStuff()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(290);
    Console.WriteLine("stuff");
    IACSharpSensor.IACSharpSensor.SensorReached(291);
  }
}
public struct B
{
  public bool Val {
    get {
      System.Boolean RNTRNTRNT_53 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(292);
      return RNTRNTRNT_53;
    }
  }
}
public class T : MarshalByRefObject
{
  static internal A a = new A();
  public static B b;
}
public class Driver
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(293);
    T.a.DoStuff();
    bool b = T.b.Val;
    IACSharpSensor.IACSharpSensor.SensorReached(294);
  }
}
