using System;
class A
{
  class C : IDisposable
  {
    void IDisposable.Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      throw new Exception("'using' and 'new' didn't resolve C as A+B+C");
    }
  }
  public class B
  {
    class C : IDisposable
    {
      void IDisposable.Dispose()
      {
        IACSharpSensor.IACSharpSensor.SensorReached(61);
      }
    }
    public B()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(62);
      using (C c = new C()) {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(63);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(64);
    object o = new A.B();
    IACSharpSensor.IACSharpSensor.SensorReached(65);
  }
}
