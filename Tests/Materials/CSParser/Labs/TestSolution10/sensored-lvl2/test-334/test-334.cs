namespace Test
{
  using Bar = Foo.Store.Directory;
  namespace Foo
  {
    namespace Index
    {
      public class CompoundFileReader : Bar
      {
        static void Main()
        {
          IACSharpSensor.IACSharpSensor.SensorReached(327);
        }
      }
    }
  }
}
namespace Test
{
  namespace Foo
  {
    namespace Store
    {
      public class Directory
      {
      }
    }
  }
}
