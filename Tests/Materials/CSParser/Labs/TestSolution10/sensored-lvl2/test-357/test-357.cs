namespace SD
{
  public class Sd
  {
    public static void F(bool b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(392);
    }
  }
}
namespace Foo
{
  using SD;
  partial class Bar
  {
    delegate void f_t(bool b);
    f_t f = new f_t(Sd.F);
  }
}
namespace Foo
{
  partial class Bar
  {
    public Bar()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(393);
    }
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(394);
      if (new Bar().f == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(395);
        throw new System.Exception("Didn't resolve Sd.F?");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(396);
    }
  }
}
