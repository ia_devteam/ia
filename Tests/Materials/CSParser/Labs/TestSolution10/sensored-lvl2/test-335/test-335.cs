class X
{
  delegate void B(int a, int b);
  static void A(int a, int b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(328);
  }
  delegate void D(out int a);
  static void C(out int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(329);
    a = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(330);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(331);
    (new B(A))(1, 2);
    int x = 0;
    (new D(C))(out x);
    IACSharpSensor.IACSharpSensor.SensorReached(332);
    if (x != 5) {
      IACSharpSensor.IACSharpSensor.SensorReached(333);
      throw new System.Exception("The value of x is " + x);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(334);
  }
}
