public interface Node
{
  int GetStat();
}
public interface FileNode : Node
{
  int NotUsed();
}
public class GenericNode : Node
{
  public virtual int GetStat()
  {
    System.Int32 RNTRNTRNT_95 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(482);
    return RNTRNTRNT_95;
  }
}
public class GenericFileNode : GenericNode, FileNode
{
  public virtual int NotUsed()
  {
    System.Int32 RNTRNTRNT_96 = -1;
    IACSharpSensor.IACSharpSensor.SensorReached(483);
    return RNTRNTRNT_96;
  }
}
public class WorkingTest : GenericFileNode, FileNode
{
  public override int GetStat()
  {
    System.Int32 RNTRNTRNT_97 = 42;
    IACSharpSensor.IACSharpSensor.SensorReached(484);
    return RNTRNTRNT_97;
  }
}
public class FailingTest : GenericFileNode
{
  public override int GetStat()
  {
    System.Int32 RNTRNTRNT_98 = 42;
    IACSharpSensor.IACSharpSensor.SensorReached(485);
    return RNTRNTRNT_98;
  }
}
public class TestWrapper
{
  static bool Test(Node inst, string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(486);
    if (inst.GetStat() == 42) {
      IACSharpSensor.IACSharpSensor.SensorReached(487);
      System.Console.WriteLine("{0} -- Passed", name);
      System.Boolean RNTRNTRNT_99 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(488);
      return RNTRNTRNT_99;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(489);
      System.Console.WriteLine("{0} -- FAILED", name);
      System.Boolean RNTRNTRNT_100 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(490);
      return RNTRNTRNT_100;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(491);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(492);
    if (Test(new WorkingTest(), "WorkingTest") && Test(new FailingTest(), "FailingTest")) {
      System.Int32 RNTRNTRNT_101 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(493);
      return RNTRNTRNT_101;
    } else {
      System.Int32 RNTRNTRNT_102 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(494);
      return RNTRNTRNT_102;
    }
  }
}
