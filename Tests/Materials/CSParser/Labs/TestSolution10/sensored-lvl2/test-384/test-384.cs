using System;
class X
{
  static int Foo = 10;
  static void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(541);
    while (true) {
      IACSharpSensor.IACSharpSensor.SensorReached(542);
      if (Foo == 1) {
        IACSharpSensor.IACSharpSensor.SensorReached(543);
        throw new Exception("Error Test");
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(544);
        break;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(545);
    Foo = 20;
    IACSharpSensor.IACSharpSensor.SensorReached(546);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(547);
    Test();
    IACSharpSensor.IACSharpSensor.SensorReached(548);
    if (Foo != 20) {
      System.Int32 RNTRNTRNT_111 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(549);
      return RNTRNTRNT_111;
    }
    System.Int32 RNTRNTRNT_112 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(550);
    return RNTRNTRNT_112;
  }
}
