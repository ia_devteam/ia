using System;
using System.Runtime.InteropServices;
using S = A.T;
class A
{
  [StructLayout(LayoutKind.Sequential)]
  public struct T
  {
    int x;
  }
  public class B
  {
    [StructLayout(LayoutKind.Sequential)]
    struct S
    {
      int x;
      int y;
    }
    S s;
    public B()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(104);
      string error = "";
      unsafe {
        IACSharpSensor.IACSharpSensor.SensorReached(105);
        if (typeof(S*).GetElementType() != typeof(A.B.S)) {
          IACSharpSensor.IACSharpSensor.SensorReached(106);
          error += " composed cast (pointer),";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(107);
        if (sizeof(S) != sizeof(A.B.S)) {
          IACSharpSensor.IACSharpSensor.SensorReached(108);
          error += " sizeof,";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(109);
        S* p1 = stackalloc S[1];
        IACSharpSensor.IACSharpSensor.SensorReached(110);
        if ((*p1).GetType() != typeof(A.B.S)) {
          IACSharpSensor.IACSharpSensor.SensorReached(111);
          error += " local declaration, 'stackalloc' keyword,";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(112);
        fixed (S* p2 = &s) {
          IACSharpSensor.IACSharpSensor.SensorReached(113);
          if ((*p2).GetType() != typeof(A.B.S)) {
            IACSharpSensor.IACSharpSensor.SensorReached(114);
            error += " class declaration, 'fixed' statement,";
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(115);
      if (error.Length != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(116);
        throw new Exception("The following couldn't resolve S as A+B+S:" + error);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(117);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(118);
    object o = new A.B();
    IACSharpSensor.IACSharpSensor.SensorReached(119);
  }
}
