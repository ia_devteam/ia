using System;
using System.Collections;
class A
{
  class C
  {
  }
  public class B
  {
    class C
    {
    }
    public B()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      string error = "";
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      if (typeof(C) != typeof(A.B.C)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        error += " 'typeof' keyword,";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4);
      object o0 = new C();
      IACSharpSensor.IACSharpSensor.SensorReached(5);
      if (o0.GetType() != typeof(A.B.C)) {
        IACSharpSensor.IACSharpSensor.SensorReached(6);
        error += " 'new' keyword,";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(7);
      C o1 = new C();
      IACSharpSensor.IACSharpSensor.SensorReached(8);
      if (o1.GetType() != typeof(A.B.C)) {
        IACSharpSensor.IACSharpSensor.SensorReached(9);
        error += " local declaration,";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(10);
      object o2 = new A.B.C();
      IACSharpSensor.IACSharpSensor.SensorReached(11);
      if (!(o2 is C)) {
        IACSharpSensor.IACSharpSensor.SensorReached(12);
        error += " 'is' keyword,";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(13);
      object o3 = o2 as C;
      IACSharpSensor.IACSharpSensor.SensorReached(14);
      if (o3 == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(15);
        error += " 'as' keyword,";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(16);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(17);
        object o4 = (C)o2;
      } catch {
        IACSharpSensor.IACSharpSensor.SensorReached(18);
        error += " type cast,";
        IACSharpSensor.IACSharpSensor.SensorReached(19);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(20);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(21);
        object o5 = (C)(o2);
      } catch {
        IACSharpSensor.IACSharpSensor.SensorReached(22);
        error += " invocation-or-cast,";
        IACSharpSensor.IACSharpSensor.SensorReached(23);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(24);
      object o6 = new C[1];
      IACSharpSensor.IACSharpSensor.SensorReached(25);
      if (o6.GetType().GetElementType() != typeof(A.B.C)) {
        IACSharpSensor.IACSharpSensor.SensorReached(26);
        error += " array creation,";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(27);
      if (typeof(C[]).GetElementType() != typeof(A.B.C)) {
        IACSharpSensor.IACSharpSensor.SensorReached(28);
        error += " composed cast (array),";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(29);
      ArrayList a = new ArrayList();
      a.Add(new A.B.C());
      IACSharpSensor.IACSharpSensor.SensorReached(30);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(31);
        foreach (C c in a) {
        }
      } catch {
        IACSharpSensor.IACSharpSensor.SensorReached(32);
        error += " 'foreach' statement,";
        IACSharpSensor.IACSharpSensor.SensorReached(33);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(34);
      if (error.Length != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(35);
        throw new Exception("The following couldn't resolve C as A+B+C:" + error);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(36);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(37);
    object o = new A.B();
    IACSharpSensor.IACSharpSensor.SensorReached(38);
  }
}
