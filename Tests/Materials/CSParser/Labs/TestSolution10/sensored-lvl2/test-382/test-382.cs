using System;
using System.Reflection;
class Dec
{
  public const decimal MinValue = -79228162514264337593543950335m;
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(531);
    System.Console.WriteLine("Compiler said value is {0}", MinValue);
    FieldInfo fi = typeof(Dec).GetField("MinValue");
    Decimal d = (Decimal)fi.GetValue(fi);
    System.Console.WriteLine("Reflection said value is {0}", d);
    IACSharpSensor.IACSharpSensor.SensorReached(532);
    if (d != MinValue) {
      IACSharpSensor.IACSharpSensor.SensorReached(533);
      throw new Exception("decimal constant not initialized");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(534);
  }
}
