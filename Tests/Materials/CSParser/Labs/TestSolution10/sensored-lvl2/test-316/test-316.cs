using System;
interface IA
{
  int Add(int i);
}
interface IB
{
  int Add(int i);
}
interface IC : IA, IB
{
}
interface IE : ICloneable, IDisposable
{
  void doom();
}
class D : IC, IB
{
  int IA.Add(int i)
  {
    System.Int32 RNTRNTRNT_36 = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(227);
    return RNTRNTRNT_36;
  }
  int IB.Add(int i)
  {
    System.Int32 RNTRNTRNT_37 = 6;
    IACSharpSensor.IACSharpSensor.SensorReached(228);
    return RNTRNTRNT_37;
  }
}
class E : IE, IC
{
  public E()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(229);
  }
  public void doom()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(230);
    return;
  }
  public Object Clone()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(231);
    return null;
  }
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(232);
  }
  int IA.Add(int i)
  {
    System.Int32 RNTRNTRNT_38 = 7;
    IACSharpSensor.IACSharpSensor.SensorReached(233);
    return RNTRNTRNT_38;
  }
  int IB.Add(int i)
  {
    System.Int32 RNTRNTRNT_39 = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(234);
    return RNTRNTRNT_39;
  }
}
class C
{
  static int Test(IC n)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(235);
    IA a = (IA)n;
    IACSharpSensor.IACSharpSensor.SensorReached(236);
    if (a.Add(0) != 5) {
      System.Int32 RNTRNTRNT_40 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(237);
      return RNTRNTRNT_40;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    if (((IA)n).Add(0) != 5) {
      System.Int32 RNTRNTRNT_41 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(239);
      return RNTRNTRNT_41;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(240);
    if (((IB)n).Add(0) != 6) {
      System.Int32 RNTRNTRNT_42 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(241);
      return RNTRNTRNT_42;
    }
    System.Int32 RNTRNTRNT_43 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(242);
    return RNTRNTRNT_43;
  }
  static void Test2(IE ie)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(243);
    ie.doom();
    ie.Clone();
    ie.Dispose();
    IACSharpSensor.IACSharpSensor.SensorReached(244);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    D d = new D();
    E e = new E();
    Test(e);
    Test2(e);
    System.Int32 RNTRNTRNT_44 = Test(d);
    IACSharpSensor.IACSharpSensor.SensorReached(246);
    return RNTRNTRNT_44;
  }
}
