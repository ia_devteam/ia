using System.Reflection;
using System;
using A;
namespace A
{
  interface B
  {
    void METHOD();
  }
}
class D : B
{
  void B.METHOD()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(204);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(205);
    MethodInfo[] mi = typeof(D).GetMethods(BindingFlags.Instance | BindingFlags.NonPublic);
    MethodInfo m = null;
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    foreach (MethodInfo j in mi) {
      IACSharpSensor.IACSharpSensor.SensorReached(207);
      if (j.Name.IndexOf("METHOD") != -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(208);
        m = j;
        IACSharpSensor.IACSharpSensor.SensorReached(209);
        break;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(210);
    if (m == null) {
      System.Int32 RNTRNTRNT_27 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(211);
      return RNTRNTRNT_27;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(212);
    if (m.Name != "A.B.METHOD") {
      IACSharpSensor.IACSharpSensor.SensorReached(213);
      Console.WriteLine("Incorrect method name, expecting: {0} got {1}", "A.B.METHOD", m.Name);
      System.Int32 RNTRNTRNT_28 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(214);
      return RNTRNTRNT_28;
    }
    System.Int32 RNTRNTRNT_29 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    return RNTRNTRNT_29;
  }
}
