using C = System.Console;
class Test
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(495);
    switch (1) {
      default:
        IACSharpSensor.IACSharpSensor.SensorReached(496);
        switch (2) {
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(497);
            int flag = 1;
            IACSharpSensor.IACSharpSensor.SensorReached(498);
            if (flag == 1) {
              IACSharpSensor.IACSharpSensor.SensorReached(499);
              C.WriteLine("**** This one is expected");
              IACSharpSensor.IACSharpSensor.SensorReached(500);
              break;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(501);
              goto lbl;
            }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(502);
        break;
        lbl:
        IACSharpSensor.IACSharpSensor.SensorReached(503);
        C.WriteLine("**** THIS SHOULD NOT APPEAR, since break-1 was supposed to fire ***");
        IACSharpSensor.IACSharpSensor.SensorReached(504);
        break;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(505);
  }
}
