using System;
public class A
{
  protected string name;
  public A(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(603);
    this.name = "A::" + name;
    IACSharpSensor.IACSharpSensor.SensorReached(604);
  }
  public A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(605);
  }
  public string Name {
    get {
      System.String RNTRNTRNT_126 = name;
      IACSharpSensor.IACSharpSensor.SensorReached(606);
      return RNTRNTRNT_126;
    }
  }
}
public class B : A
{
  public B(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(607);
    this.name = "B::" + name;
    IACSharpSensor.IACSharpSensor.SensorReached(608);
  }
  public B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(609);
  }
}
public class C : B
{
  string value;
  public C(string name, string value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(610);
    this.name = "C::" + name;
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(611);
  }
  public string Value {
    get {
      System.String RNTRNTRNT_127 = value;
      IACSharpSensor.IACSharpSensor.SensorReached(612);
      return RNTRNTRNT_127;
    }
  }
}
public class Tester
{
  delegate string MethodHandler(C c);
  static string MethodSampleA(A value)
  {
    System.String RNTRNTRNT_128 = value.Name;
    IACSharpSensor.IACSharpSensor.SensorReached(613);
    return RNTRNTRNT_128;
  }
  static string MethodSampleB(B value)
  {
    System.String RNTRNTRNT_129 = value.Name;
    IACSharpSensor.IACSharpSensor.SensorReached(614);
    return RNTRNTRNT_129;
  }
  static string MethodSampleC(C value)
  {
    System.String RNTRNTRNT_130 = value.Name + " " + value.Value;
    IACSharpSensor.IACSharpSensor.SensorReached(615);
    return RNTRNTRNT_130;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(616);
    MethodHandler da = MethodSampleA;
    MethodHandler db = MethodSampleB;
    MethodHandler dc = MethodSampleC;
    C a = new C("Hello", "hello");
    C b = new C("World", "world");
    C c = new C("!", "!!!");
    Console.WriteLine(da(a));
    Console.WriteLine(db(b));
    Console.WriteLine(dc(c));
    IACSharpSensor.IACSharpSensor.SensorReached(617);
  }
}
