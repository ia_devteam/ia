using System;
using System.Reflection;
public interface ITest
{
  event EventHandler DocBuildingStep;
}
class X
{
  static int Main()
  {
    System.Int32 RNTRNTRNT_84 = typeof(ITest).GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Length;
    IACSharpSensor.IACSharpSensor.SensorReached(450);
    return RNTRNTRNT_84;
  }
}
