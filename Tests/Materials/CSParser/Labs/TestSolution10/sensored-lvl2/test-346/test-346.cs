using System;
namespace TestMethods
{
  class Class1
  {
    static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(366);
      int test_int = 1;
      TestClass testClass = new TestClass();
      test_int *= testClass.AddItem(new TestParam());
      test_int *= testClass.AddItem(new ParamClass());
      int base_int = 1;
      BaseClass baseClass = testClass as BaseClass;
      base_int *= baseClass.AddItem(new TestParam());
      base_int *= baseClass.AddItem(new ParamClass());
      System.Int32 RNTRNTRNT_64 = (test_int == 4 && base_int == 9) ? 0 : 1;
      IACSharpSensor.IACSharpSensor.SensorReached(367);
      return RNTRNTRNT_64;
    }
  }
  public class ParamClass
  {
  }
  public class TestParam : ParamClass
  {
  }
  public abstract class BaseClass
  {
    public abstract int AddItem(ParamClass val);
  }
  public class TestClass : BaseClass
  {
    public int AddItem(object val)
    {
      System.Int32 RNTRNTRNT_65 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(368);
      return RNTRNTRNT_65;
    }
    public override int AddItem(ParamClass val)
    {
      System.Int32 RNTRNTRNT_66 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(369);
      return RNTRNTRNT_66;
    }
  }
}
