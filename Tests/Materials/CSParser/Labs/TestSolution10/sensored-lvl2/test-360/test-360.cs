public class Application
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(400);
    if (true) {
      IACSharpSensor.IACSharpSensor.SensorReached(401);
      string thisWorks = "nice";
      System.Console.WriteLine(thisWorks);
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(402);
      string thisDoesnt = "not so";
      System.Console.WriteLine(thisDoesnt);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(403);
  }
}
