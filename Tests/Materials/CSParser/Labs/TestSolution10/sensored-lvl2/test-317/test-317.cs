interface IFoo
{
  void Test(int t);
}
interface IBar : IFoo
{
  new int Test(int t);
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(247);
  }
}
