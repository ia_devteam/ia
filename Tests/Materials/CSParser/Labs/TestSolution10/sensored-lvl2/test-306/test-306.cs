using System;
using C = A.D;
class A
{
  protected internal class D : Exception
  {
  }
  public class B
  {
    class C : Exception
    {
    }
    public B()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(120);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(121);
        throw new A.B.C();
      } catch (C e) {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(122);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(123);
    object o = new A.B();
    IACSharpSensor.IACSharpSensor.SensorReached(124);
  }
}
