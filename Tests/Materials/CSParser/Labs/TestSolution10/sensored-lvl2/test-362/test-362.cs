class C
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(404);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(405);
      Test();
    } catch {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(406);
  }
  static void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(408);
      throw new System.ArgumentException();
    } catch {
      IACSharpSensor.IACSharpSensor.SensorReached(409);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(410);
        throw;
      } finally {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(411);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(412);
  }
}
