using System;
public class Tester
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(674);
    A a = new A(8);
    B b = new B(9);
    b.SetCount(10);
    Console.WriteLine("b.Count should be 9: {0}", b.Count);
    Console.WriteLine("b [{0}] should return {0}: {1}", 10, b[10]);
    Console.WriteLine("a.Message : {0}", a.Message);
    b.Message = "";
    Console.WriteLine("b.Messasge : {0}", b.Message);
    IACSharpSensor.IACSharpSensor.SensorReached(675);
  }
}
public class A
{
  protected int count;
  public A(int count)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(676);
    this.count = count;
    IACSharpSensor.IACSharpSensor.SensorReached(677);
  }
  public virtual int Count {
    get {
      System.Int32 RNTRNTRNT_147 = count;
      IACSharpSensor.IACSharpSensor.SensorReached(678);
      return RNTRNTRNT_147;
    }
    protected set {
      IACSharpSensor.IACSharpSensor.SensorReached(679);
      count = value;
      IACSharpSensor.IACSharpSensor.SensorReached(680);
    }
  }
  public virtual int this[int index] {
    get {
      System.Int32 RNTRNTRNT_148 = index;
      IACSharpSensor.IACSharpSensor.SensorReached(681);
      return RNTRNTRNT_148;
    }
  }
  public virtual string Message {
    get {
      System.String RNTRNTRNT_149 = "Hello Mono";
      IACSharpSensor.IACSharpSensor.SensorReached(682);
      return RNTRNTRNT_149;
    }
  }
}
public class B : A
{
  public B(int count) : base(count)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(683);
  }
  public override int Count {
    protected set { IACSharpSensor.IACSharpSensor.SensorReached(684); }
  }
  public void SetCount(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(685);
    Count = value;
    IACSharpSensor.IACSharpSensor.SensorReached(686);
  }
  public override int this[int index] {
    get {
      System.Int32 RNTRNTRNT_150 = base[index];
      IACSharpSensor.IACSharpSensor.SensorReached(687);
      return RNTRNTRNT_150;
    }
  }
  public new string Message {
    get {
      System.String RNTRNTRNT_151 = "Hello Mono (2)";
      IACSharpSensor.IACSharpSensor.SensorReached(688);
      return RNTRNTRNT_151;
    }
    internal set { IACSharpSensor.IACSharpSensor.SensorReached(689); }
  }
}
