public class X
{
  public X(out bool hello)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(478);
    hello = true;
    IACSharpSensor.IACSharpSensor.SensorReached(479);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(480);
  }
}
public class Y : X
{
  public Y(out bool hello) : base(out hello)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(481);
  }
}
