namespace FLMID.Bugs.BoolOne
{
  public interface IB
  {
    void Add(bool v1, bool v2, uint v3, bool v4);
  }
  public class A
  {
    public static bool ok;
    public void Add(bool v1, bool v2, uint v3, bool v4)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(347);
      ok = v4;
      IACSharpSensor.IACSharpSensor.SensorReached(348);
    }
  }
  public class B : A, IB
  {
  }
  public class Test
  {
    public static int Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(349);
      IB aux = new B();
      aux.Add(false, false, 0, true);
      System.Int32 RNTRNTRNT_63 = A.ok ? 0 : 1;
      IACSharpSensor.IACSharpSensor.SensorReached(350);
      return RNTRNTRNT_63;
    }
  }
}
