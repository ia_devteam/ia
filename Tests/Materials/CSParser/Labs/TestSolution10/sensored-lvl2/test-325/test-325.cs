using System;
public class RequestAttribute : Attribute
{
  public RequestAttribute(string a, string b, params string[] c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(295);
  }
}
public class MyClass
{
  [Request("somereq", "result")]
  public static int SomeRequest()
  {
    System.Int32 RNTRNTRNT_54 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(296);
    return RNTRNTRNT_54;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(297);
    SomeRequest();
    IACSharpSensor.IACSharpSensor.SensorReached(298);
  }
}
