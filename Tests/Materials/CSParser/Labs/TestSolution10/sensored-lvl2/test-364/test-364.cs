using System;
class X
{
  delegate int Foo();
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(418);
    int x = t1(1);
    IACSharpSensor.IACSharpSensor.SensorReached(419);
    if (x != 1) {
      System.Int32 RNTRNTRNT_73 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(420);
      return RNTRNTRNT_73;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(421);
    x = t2(2);
    IACSharpSensor.IACSharpSensor.SensorReached(422);
    if (x != 3) {
      System.Int32 RNTRNTRNT_74 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(423);
      return RNTRNTRNT_74;
    }
    System.Int32 RNTRNTRNT_75 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(424);
    return RNTRNTRNT_75;
  }
  static int t1(int p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(425);
    Foo f = delegate {
      System.Int32 RNTRNTRNT_76 = p;
      IACSharpSensor.IACSharpSensor.SensorReached(426);
      return RNTRNTRNT_76;
    };
    System.Int32 RNTRNTRNT_77 = f();
    IACSharpSensor.IACSharpSensor.SensorReached(427);
    return RNTRNTRNT_77;
  }
  static int t2(int p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(428);
    p++;
    Foo f = delegate {
      System.Int32 RNTRNTRNT_78 = p;
      IACSharpSensor.IACSharpSensor.SensorReached(429);
      return RNTRNTRNT_78;
    };
    System.Int32 RNTRNTRNT_79 = f();
    IACSharpSensor.IACSharpSensor.SensorReached(430);
    return RNTRNTRNT_79;
  }
  static void Main2(string[] argv)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(431);
    Console.WriteLine("Test");
    Delegable db = new Delegable();
    IACSharpSensor.IACSharpSensor.SensorReached(432);
    if (argv.Length > 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(433);
      db.MyDelegate += delegate(object o, EventArgs args) {
        Console.WriteLine("{0}", argv);
        Console.WriteLine("{0}", db);
      };
    }
    IACSharpSensor.IACSharpSensor.SensorReached(434);
  }
}
class Delegable
{
  public event EventHandler MyDelegate;
}
