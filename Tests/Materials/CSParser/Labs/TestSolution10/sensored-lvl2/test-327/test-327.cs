class X2
{
}
namespace A
{
  enum X1
  {
    x1
  }
  enum X2
  {
    x2
  }
}
namespace A.B
{
  using Y1 = X1;
  using Y2 = X2;
  class Tester
  {
    static internal Y1 y1 = Y1.x1;
    static internal Y2 y2 = Y2.x2;
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(303);
    }
  }
}
