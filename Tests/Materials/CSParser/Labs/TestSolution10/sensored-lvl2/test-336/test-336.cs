using System;
public delegate void Foo();
public delegate void Bar(int x);
class X
{
  public X(Foo foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(335);
  }
  public X(Bar bar)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(336);
  }
  static void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(337);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(338);
    X x = new X(Test);
    IACSharpSensor.IACSharpSensor.SensorReached(339);
  }
}
