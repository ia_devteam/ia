using System;
public delegate double Mapper(int item);
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(299);
    Mapper mapper = new Mapper(delegate(int i) { return i * 12; });
    IACSharpSensor.IACSharpSensor.SensorReached(300);
    if (mapper(3) == 36) {
      System.Int32 RNTRNTRNT_55 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(301);
      return RNTRNTRNT_55;
    }
    System.Int32 RNTRNTRNT_56 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(302);
    return RNTRNTRNT_56;
  }
}
