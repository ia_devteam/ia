using System;
using System.Reflection;
[AttributeUsage(AttributeTargets.Class)]
public class TypeCheckAttribute : Attribute
{
  public TypeCheckAttribute()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(639);
  }
}
[AttributeUsage(AttributeTargets.Property)]
public class PropertyCheckAttribute : Attribute
{
  public PropertyCheckAttribute()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(640);
  }
}
[AttributeUsage(AttributeTargets.Method)]
public class AccessorCheckAttribute : Attribute
{
  MethodAttributes flags;
  public AccessorCheckAttribute(MethodAttributes flags)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(641);
    this.flags = flags;
    IACSharpSensor.IACSharpSensor.SensorReached(642);
  }
  public MethodAttributes Attributes {
    get {
      MethodAttributes RNTRNTRNT_139 = flags;
      IACSharpSensor.IACSharpSensor.SensorReached(643);
      return RNTRNTRNT_139;
    }
  }
}
public class Test
{
  public static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(644);
    Type t = typeof(A);
    IACSharpSensor.IACSharpSensor.SensorReached(645);
    foreach (PropertyInfo pi in t.GetProperties()) {
      IACSharpSensor.IACSharpSensor.SensorReached(646);
      object[] attrs = pi.GetCustomAttributes(typeof(PropertyCheckAttribute), true);
      IACSharpSensor.IACSharpSensor.SensorReached(647);
      if (attrs == null) {
        System.Int32 RNTRNTRNT_140 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(648);
        return RNTRNTRNT_140;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(649);
      MethodInfo get_accessor, set_accessor;
      get_accessor = pi.GetGetMethod(true);
      set_accessor = pi.GetSetMethod(true);
      IACSharpSensor.IACSharpSensor.SensorReached(650);
      if (get_accessor != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(651);
        CheckFlags(pi, get_accessor);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(652);
      if (set_accessor != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(653);
        CheckFlags(pi, set_accessor);
      }
    }
    System.Int32 RNTRNTRNT_141 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(654);
    return RNTRNTRNT_141;
  }
  static void CheckFlags(PropertyInfo pi, MethodInfo accessor)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(655);
    object[] attrs = accessor.GetCustomAttributes(typeof(AccessorCheckAttribute), true);
    IACSharpSensor.IACSharpSensor.SensorReached(656);
    if (attrs == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(657);
      return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(658);
    AccessorCheckAttribute accessor_attr = (AccessorCheckAttribute)attrs[0];
    MethodAttributes accessor_flags = accessor.Attributes;
    IACSharpSensor.IACSharpSensor.SensorReached(659);
    if ((accessor_flags & accessor_attr.Attributes) == accessor_attr.Attributes) {
      IACSharpSensor.IACSharpSensor.SensorReached(660);
      Console.WriteLine("Test for {0}.{1} PASSED", pi.Name, accessor.Name);
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(661);
      string message = String.Format("Test for {0}.{1} INCORRECT: MethodAttributes should be {2}, but are {3}", pi.Name, accessor.Name, accessor_attr.Attributes, accessor_flags);
      IACSharpSensor.IACSharpSensor.SensorReached(662);
      throw new Exception(message);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(663);
  }
}
[TypeCheck()]
public class A
{
  const MethodAttributes flags = MethodAttributes.HideBySig | MethodAttributes.SpecialName;
  [PropertyCheck()]
  public int Value1 {
    [AccessorCheck(flags | MethodAttributes.Public)]
    get {
      System.Int32 RNTRNTRNT_142 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(664);
      return RNTRNTRNT_142;
    }
    [AccessorCheck(flags | MethodAttributes.Public)]
    set { IACSharpSensor.IACSharpSensor.SensorReached(665); }
  }
  [PropertyCheck()]
  public int Value2 {
    [AccessorCheck(flags | MethodAttributes.Public)]
    get {
      System.Int32 RNTRNTRNT_143 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(666);
      return RNTRNTRNT_143;
    }
    [AccessorCheck(flags | MethodAttributes.FamORAssem)]
    protected internal set { IACSharpSensor.IACSharpSensor.SensorReached(667); }
  }
  [PropertyCheck()]
  public int Value3 {
    [AccessorCheck(flags | MethodAttributes.Public)]
    get {
      System.Int32 RNTRNTRNT_144 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(668);
      return RNTRNTRNT_144;
    }
    [AccessorCheck(flags | MethodAttributes.Family)]
    protected set { IACSharpSensor.IACSharpSensor.SensorReached(669); }
  }
  [PropertyCheck()]
  public int Value4 {
    [AccessorCheck(flags | MethodAttributes.Assembly)]
    internal get {
      System.Int32 RNTRNTRNT_145 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(670);
      return RNTRNTRNT_145;
    }
    [AccessorCheck(flags | MethodAttributes.Public)]
    set { IACSharpSensor.IACSharpSensor.SensorReached(671); }
  }
  [PropertyCheck()]
  public int Value5 {
    [AccessorCheck(flags | MethodAttributes.Public)]
    get {
      System.Int32 RNTRNTRNT_146 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(672);
      return RNTRNTRNT_146;
    }
    [AccessorCheck(flags | MethodAttributes.Private)]
    private set { IACSharpSensor.IACSharpSensor.SensorReached(673); }
  }
}
