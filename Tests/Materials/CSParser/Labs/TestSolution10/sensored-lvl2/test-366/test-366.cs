using System;
using System.Reflection;
using System.Runtime.InteropServices;
[StructLayout(LayoutKind.Explicit)]
struct foo2
{
}
class C
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(437);
    foo2 f = new foo2();
    Type fit = typeof(FieldInfo);
    MethodInfo gfo = fit.GetMethod("GetFieldOffset", BindingFlags.Instance | BindingFlags.NonPublic);
    IACSharpSensor.IACSharpSensor.SensorReached(438);
    if (gfo == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(439);
      Console.WriteLine("PASS: On MS runtime, Test OK");
      System.Int32 RNTRNTRNT_80 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(440);
      return RNTRNTRNT_80;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(441);
    Type t = typeof(foo2);
    FieldInfo fi = t.GetField("$PRIVATE$", BindingFlags.Instance | BindingFlags.NonPublic);
    object res = gfo.Invoke(fi, null);
    IACSharpSensor.IACSharpSensor.SensorReached(442);
    if (res.GetType() != typeof(Int32)) {
      System.Int32 RNTRNTRNT_81 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(443);
      return RNTRNTRNT_81;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(444);
    int r = (int)res;
    IACSharpSensor.IACSharpSensor.SensorReached(445);
    if (r != 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(446);
      Console.WriteLine("FAIL: Offset is not zero");
      System.Int32 RNTRNTRNT_82 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(447);
      return RNTRNTRNT_82;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(448);
    Console.WriteLine("PASS: Test passes on Mono");
    System.Int32 RNTRNTRNT_83 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(449);
    return RNTRNTRNT_83;
  }
}
