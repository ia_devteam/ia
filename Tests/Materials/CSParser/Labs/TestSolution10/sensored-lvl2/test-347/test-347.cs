namespace Whatever.Core
{
  public class Project
  {
  }
  public class A
  {
    public Project Project {
      get {
        Project RNTRNTRNT_67 = new Project();
        IACSharpSensor.IACSharpSensor.SensorReached(370);
        return RNTRNTRNT_67;
      }
    }
  }
}
namespace SomethingElse.Core
{
  public class Project
  {
  }
}
namespace Whatever.App
{
  using Whatever.Core;
  using SomethingElse.Core;
  public class B : A
  {
    public string Execute()
    {
      System.String RNTRNTRNT_68 = Project.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(371);
      return RNTRNTRNT_68;
    }
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(372);
      new B().Execute();
      IACSharpSensor.IACSharpSensor.SensorReached(373);
    }
  }
}
