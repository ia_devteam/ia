using System;
using System.Security;
using System.Security.Permissions;
public class Program
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(131);
    SecurityAction a = SecurityAction.Demand;
    IACSharpSensor.IACSharpSensor.SensorReached(132);
    switch (a) {
      case (SecurityAction)13:
      case SecurityAction.Demand:
        IACSharpSensor.IACSharpSensor.SensorReached(133);
        Console.WriteLine("ok");
        IACSharpSensor.IACSharpSensor.SensorReached(134);
        break;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(135);
  }
}
