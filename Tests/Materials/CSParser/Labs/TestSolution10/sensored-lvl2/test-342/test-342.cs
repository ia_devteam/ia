using System;
class A
{
  public virtual void Foo(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(352);
  }
  public virtual void Foo(double d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(353);
    throw new Exception("Shouldn't be invoked");
  }
}
class B : A
{
  public override void Foo(double d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(354);
    throw new Exception("Overload resolution failed");
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(355);
    new B().Foo(1);
    IACSharpSensor.IACSharpSensor.SensorReached(356);
  }
}
