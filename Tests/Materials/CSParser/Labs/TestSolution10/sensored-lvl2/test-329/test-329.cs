using System;
[X(null)]
class X : Attribute
{
  int ID;
  public X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(305);
  }
  public X(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(306);
    if (o == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(307);
      ID = 55;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(308);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(309);
    object[] attrs = typeof(X).GetCustomAttributes(typeof(X), false);
    IACSharpSensor.IACSharpSensor.SensorReached(310);
    if (attrs.Length != 1) {
      System.Int32 RNTRNTRNT_57 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(311);
      return RNTRNTRNT_57;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(312);
    X x = attrs[0] as X;
    IACSharpSensor.IACSharpSensor.SensorReached(313);
    if (x.ID != 55) {
      System.Int32 RNTRNTRNT_58 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(314);
      return RNTRNTRNT_58;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(315);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_59 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(316);
    return RNTRNTRNT_59;
  }
}
