class C
{
  class O : M
  {
    public override void Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(571);
    }
  }
  class N
  {
    public virtual void Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(572);
    }
  }
  class M : N
  {
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(573);
  }
}
