class T
{
  unsafe private byte* ptr;
  unsafe internal byte* Ptr {
    get {
      System.Byte* RNTRNTRNT_60 = ptr;
      IACSharpSensor.IACSharpSensor.SensorReached(319);
      return RNTRNTRNT_60;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(320);
      ptr = value;
      IACSharpSensor.IACSharpSensor.SensorReached(321);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(322);
  }
}
