using System;
using System.Security;
using System.Security.Permissions;
[assembly: SecurityPermission(SecurityAction.RequestMinimum, Execution = true)]
[assembly: SecurityPermission(SecurityAction.RequestOptional, Unrestricted = true)]
[assembly: SecurityPermission(SecurityAction.RequestRefuse, SkipVerification = true)]
[SecurityPermission(SecurityAction.LinkDemand, ControlPrincipal = true)]
struct LinkDemandStruct
{
  internal string Info;
}
[SecurityPermission(SecurityAction.Demand, ControlAppDomain = true)]
public class Program
{
  private static string _message = "Hello Mono!";
  private LinkDemandStruct info;
  [SecurityPermission(SecurityAction.InheritanceDemand, ControlAppDomain = true)]
  public Program()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(144);
    info = new LinkDemandStruct();
    info.Info = ":-)";
    IACSharpSensor.IACSharpSensor.SensorReached(145);
  }
  public static string Message {
    [SecurityPermission(SecurityAction.PermitOnly, ControlEvidence = true)]
    get {
      System.String RNTRNTRNT_3 = _message;
      IACSharpSensor.IACSharpSensor.SensorReached(146);
      return RNTRNTRNT_3;
    }
    [SecurityPermission(SecurityAction.Assert, ControlThread = true)]
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(147);
      _message = value;
      IACSharpSensor.IACSharpSensor.SensorReached(148);
    }
  }
  [SecurityPermission(SecurityAction.Deny, UnmanagedCode = true)]
  private bool DenyMethod()
  {
    System.Boolean RNTRNTRNT_4 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(149);
    return RNTRNTRNT_4;
  }
  [SiteIdentityPermission(SecurityAction.PermitOnly)]
  [PermissionSet(SecurityAction.PermitOnly, Unrestricted = true)]
  [PermissionSet(SecurityAction.PermitOnly, Unrestricted = false)]
  public void Test2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(150);
  }
  [PermissionSet(SecurityAction.PermitOnly, Unrestricted = true)]
  [PermissionSet(SecurityAction.PermitOnly, Unrestricted = false)]
  public void Test3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(151);
  }
  [EnvironmentPermission(SecurityAction.Demand, Unrestricted = true)]
  public void Test4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(152);
  }
  [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlEvidence, UnmanagedCode = true)]
  [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.AllFlags, UnmanagedCode = true)]
  public static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(153);
    Type program = typeof(Program);
    IACSharpSensor.IACSharpSensor.SensorReached(154);
    if (program.GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_5 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(155);
      return RNTRNTRNT_5;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(156);
    if (program.GetConstructor(System.Type.EmptyTypes).GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_6 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(157);
      return RNTRNTRNT_6;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(158);
    if (program.GetProperty("Message").GetSetMethod().GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_7 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(159);
      return RNTRNTRNT_7;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(160);
    if (program.GetMethod("Main").GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_8 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(161);
      return RNTRNTRNT_8;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    if (program.GetMethod("Test2").GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_9 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(163);
      return RNTRNTRNT_9;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(164);
    Type test2 = typeof(Test2);
    IACSharpSensor.IACSharpSensor.SensorReached(165);
    if (test2.GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_10 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(166);
      return RNTRNTRNT_10;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(167);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_11 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(168);
    return RNTRNTRNT_11;
  }
}
[SecurityPermission(SecurityAction.Demand, ControlAppDomain = true)]
public partial class Test2
{
}
[SecurityPermission(SecurityAction.Demand, ControlAppDomain = true)]
public partial class Test2
{
}
