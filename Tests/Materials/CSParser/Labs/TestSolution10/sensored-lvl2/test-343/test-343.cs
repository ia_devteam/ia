using System;
class X
{
  static void Concat(string s1, string s2, string s3)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(357);
  }
  static void Concat(params string[] ss)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(358);
    throw new Exception("Overload resolution failed");
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(359);
    Concat("a", "b", "c");
    IACSharpSensor.IACSharpSensor.SensorReached(360);
  }
}
