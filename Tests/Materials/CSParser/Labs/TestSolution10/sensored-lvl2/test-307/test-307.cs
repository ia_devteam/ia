using System;
using C = A.D;
public class A
{
  public class D : IDisposable
  {
    void IDisposable.Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(125);
      throw new Exception("'using' and 'new' didn't resolve C as A+B+C");
    }
  }
  public class B
  {
    class C : IDisposable
    {
      void IDisposable.Dispose()
      {
        IACSharpSensor.IACSharpSensor.SensorReached(126);
      }
    }
    public B()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(127);
      using (C c = new C()) {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(128);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(129);
    object o = new A.B();
    IACSharpSensor.IACSharpSensor.SensorReached(130);
  }
}
