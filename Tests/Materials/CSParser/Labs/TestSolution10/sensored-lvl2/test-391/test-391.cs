class C
{
  void Foo(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(574);
  }
  void Foo(ref int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(575);
  }
  void Bar(out bool b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(576);
    b = false;
    IACSharpSensor.IACSharpSensor.SensorReached(577);
  }
  void Bar(bool b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(578);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(579);
  }
}
