using System;
using System.Runtime.InteropServices;
class A
{
  [StructLayout(LayoutKind.Sequential)]
  struct S
  {
    int x;
  }
  public class B
  {
    [StructLayout(LayoutKind.Sequential)]
    struct S
    {
      int x;
      int y;
    }
    S s;
    public B()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(39);
      string error = "";
      unsafe {
        IACSharpSensor.IACSharpSensor.SensorReached(40);
        if (typeof(S*).GetElementType() != typeof(A.B.S)) {
          IACSharpSensor.IACSharpSensor.SensorReached(41);
          error += " composed cast (pointer),";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(42);
        if (sizeof(S) != sizeof(A.B.S)) {
          IACSharpSensor.IACSharpSensor.SensorReached(43);
          error += " sizeof,";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(44);
        S* p1 = stackalloc S[1];
        IACSharpSensor.IACSharpSensor.SensorReached(45);
        if ((*p1).GetType() != typeof(A.B.S)) {
          IACSharpSensor.IACSharpSensor.SensorReached(46);
          error += " local declaration, 'stackalloc' keyword,";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(47);
        fixed (S* p2 = &s) {
          IACSharpSensor.IACSharpSensor.SensorReached(48);
          if ((*p2).GetType() != typeof(A.B.S)) {
            IACSharpSensor.IACSharpSensor.SensorReached(49);
            error += " class declaration, 'fixed' statement,";
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(50);
      if (error.Length != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(51);
        throw new Exception("The following couldn't resolve S as A+B+S:" + error);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(52);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    object o = new A.B();
    IACSharpSensor.IACSharpSensor.SensorReached(54);
  }
}
