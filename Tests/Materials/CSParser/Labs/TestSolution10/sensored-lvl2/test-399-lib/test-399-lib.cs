using System;
public struct Result
{
  public int res;
  double duh;
  long bah;
  public Result(int val)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(715);
    res = val;
    bah = val;
    duh = val;
    IACSharpSensor.IACSharpSensor.SensorReached(716);
  }
}
public class Vararg
{
  public static int AddABunchOfInts(__arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(717);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    IACSharpSensor.IACSharpSensor.SensorReached(718);
    for (int i = 0; i < argCount; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(719);
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    System.Int32 RNTRNTRNT_161 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(720);
    return RNTRNTRNT_161;
  }
  public static int AddASecondBunchOfInts(int a, __arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(721);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    IACSharpSensor.IACSharpSensor.SensorReached(722);
    for (int i = 0; i < argCount; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(723);
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    System.Int32 RNTRNTRNT_162 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(724);
    return RNTRNTRNT_162;
  }
  public static Result VtAddABunchOfInts(__arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(725);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    IACSharpSensor.IACSharpSensor.SensorReached(726);
    for (int i = 0; i < argCount; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(727);
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    Result RNTRNTRNT_163 = new Result(result);
    IACSharpSensor.IACSharpSensor.SensorReached(728);
    return RNTRNTRNT_163;
  }
  public static Result VtAddASecondBunchOfInts(int a, __arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(729);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    IACSharpSensor.IACSharpSensor.SensorReached(730);
    for (int i = 0; i < argCount; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(731);
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    Result RNTRNTRNT_164 = new Result(result);
    IACSharpSensor.IACSharpSensor.SensorReached(732);
    return RNTRNTRNT_164;
  }
  public int InstAddABunchOfInts(__arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(733);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    IACSharpSensor.IACSharpSensor.SensorReached(734);
    for (int i = 0; i < argCount; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(735);
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    System.Int32 RNTRNTRNT_165 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(736);
    return RNTRNTRNT_165;
  }
  public int InstAddASecondBunchOfInts(int a, __arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(737);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    IACSharpSensor.IACSharpSensor.SensorReached(738);
    for (int i = 0; i < argCount; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(739);
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    System.Int32 RNTRNTRNT_166 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(740);
    return RNTRNTRNT_166;
  }
  public Result InstVtAddABunchOfInts(__arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(741);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    IACSharpSensor.IACSharpSensor.SensorReached(742);
    for (int i = 0; i < argCount; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(743);
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    Result RNTRNTRNT_167 = new Result(result);
    IACSharpSensor.IACSharpSensor.SensorReached(744);
    return RNTRNTRNT_167;
  }
  public Result InstVtAddASecondBunchOfInts(int a, __arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(745);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    IACSharpSensor.IACSharpSensor.SensorReached(746);
    for (int i = 0; i < argCount; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(747);
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    Result RNTRNTRNT_168 = new Result(result);
    IACSharpSensor.IACSharpSensor.SensorReached(748);
    return RNTRNTRNT_168;
  }
}
