class Y
{
  byte b;
  public static implicit operator int(Y i)
  {
    System.Int32 RNTRNTRNT_52 = i.b;
    IACSharpSensor.IACSharpSensor.SensorReached(280);
    return RNTRNTRNT_52;
  }
  public Y(byte b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(281);
    this.b = b;
    IACSharpSensor.IACSharpSensor.SensorReached(282);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    Y y = new Y(1);
    IACSharpSensor.IACSharpSensor.SensorReached(284);
    switch (y) {
      case 0:
        IACSharpSensor.IACSharpSensor.SensorReached(285);
        break;
      case 1:
        IACSharpSensor.IACSharpSensor.SensorReached(286);
        break;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(287);
    int a = y;
    IACSharpSensor.IACSharpSensor.SensorReached(288);
  }
}
