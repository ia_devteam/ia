using System;
using System.ComponentModel;
public class Ev
{
  object disposedEvent = new object();
  EventHandlerList Events = new EventHandlerList();
  public event EventHandler Disposed {
    add { Events.AddHandler(disposedEvent, value); }
    remove { Events.RemoveHandler(disposedEvent, value); }
  }
  public void OnClick(EventArgs e)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(248);
    EventHandler clickEventDelegate = (EventHandler)Events[disposedEvent];
    IACSharpSensor.IACSharpSensor.SensorReached(249);
    if (clickEventDelegate != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(250);
      clickEventDelegate(this, e);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(251);
  }
}
public interface EventInterface
{
  event EventHandler Event;
}
class Foo : EventInterface
{
  event EventHandler EventInterface.Event {
    add { }
    remove { }
  }
}
public class C
{
  public static void my_from_fixed(out int val)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(252);
    val = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(253);
  }
  public static void month_from_fixed(int date)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(254);
    int year;
    my_from_fixed(out year);
    IACSharpSensor.IACSharpSensor.SensorReached(255);
  }
  static internal int CreateFromString(int arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(256);
    int major = 0;
    int number = 5;
    major = number;
    number = -1;
    System.Int32 RNTRNTRNT_45 = major;
    IACSharpSensor.IACSharpSensor.SensorReached(257);
    return RNTRNTRNT_45;
  }
  unsafe public double* GetValue(double value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(258);
    double d = value;
    System.Double* RNTRNTRNT_46 = &d;
    IACSharpSensor.IACSharpSensor.SensorReached(259);
    return RNTRNTRNT_46;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(260);
  }
}
