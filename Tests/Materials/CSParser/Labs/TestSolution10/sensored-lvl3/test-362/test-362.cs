class C
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(201);
    try {
      Test();
    } catch {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(202);
  }
  static void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    try {
      throw new System.ArgumentException();
    } catch {
      try {
        throw;
      } finally {
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(204);
  }
}
