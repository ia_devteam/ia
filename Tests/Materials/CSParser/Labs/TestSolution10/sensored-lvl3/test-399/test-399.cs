using System;
class TestVararg
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(381);
    int result = Vararg.AddABunchOfInts(__arglist(2, 3, 4));
    Console.WriteLine("Answer: {0}", result);
    if (result != 9) {
      System.Int32 RNTRNTRNT_152 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(382);
      return RNTRNTRNT_152;
    }
    result = Vararg.AddASecondBunchOfInts(16, __arglist(2, 3, 4));
    Console.WriteLine("Answer: {0}", result);
    if (result != 9) {
      System.Int32 RNTRNTRNT_153 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(383);
      return RNTRNTRNT_153;
    }
    Vararg s = new Vararg();
    result = s.InstAddABunchOfInts(__arglist(2, 3, 4, 5));
    Console.WriteLine("Answer: {0}", result);
    if (result != 14) {
      System.Int32 RNTRNTRNT_154 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(384);
      return RNTRNTRNT_154;
    }
    result = s.InstAddASecondBunchOfInts(16, __arglist(2, 3, 4, 5, 6));
    Console.WriteLine("Answer: {0}", result);
    if (result != 20) {
      System.Int32 RNTRNTRNT_155 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(385);
      return RNTRNTRNT_155;
    }
    result = s.InstVtAddABunchOfInts(__arglist(2, 3, 4, 5)).res;
    Console.WriteLine("Answer: {0}", result);
    if (result != 14) {
      System.Int32 RNTRNTRNT_156 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(386);
      return RNTRNTRNT_156;
    }
    result = s.InstVtAddASecondBunchOfInts(16, __arglist(2, 3, 4, 5, 6)).res;
    Console.WriteLine("Answer: {0}", result);
    if (result != 20) {
      System.Int32 RNTRNTRNT_157 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(387);
      return RNTRNTRNT_157;
    }
    result = Vararg.VtAddABunchOfInts(__arglist(2, 3, 4, 5, 1)).res;
    Console.WriteLine("Answer: {0}", result);
    if (result != 15) {
      System.Int32 RNTRNTRNT_158 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(388);
      return RNTRNTRNT_158;
    }
    result = Vararg.VtAddASecondBunchOfInts(16, __arglist(2, 3, 4, 5, 6, 1)).res;
    Console.WriteLine("Answer: {0}", result);
    if (result != 21) {
      System.Int32 RNTRNTRNT_159 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(389);
      return RNTRNTRNT_159;
    }
    System.Int32 RNTRNTRNT_160 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(390);
    return RNTRNTRNT_160;
  }
}
