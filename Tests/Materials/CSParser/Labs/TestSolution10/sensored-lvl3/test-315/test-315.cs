using System;
public interface IDirectedEnumerable
{
  IDirectedEnumerable Backwards();
}
public interface IDirectedCollectionValue : IDirectedEnumerable
{
  new IDirectedCollectionValue Backwards();
}
public class GuardedCollectionValue : IDirectedCollectionValue
{
  IDirectedEnumerable IDirectedEnumerable.Backwards()
  {
    IDirectedEnumerable RNTRNTRNT_33 = this;
    IACSharpSensor.IACSharpSensor.SensorReached(89);
    return RNTRNTRNT_33;
  }
  public IDirectedCollectionValue Backwards()
  {
    IDirectedCollectionValue RNTRNTRNT_34 = this;
    IACSharpSensor.IACSharpSensor.SensorReached(90);
    return RNTRNTRNT_34;
  }
}
public interface ISequenced : IDirectedCollectionValue
{
}
public class GuardedSequenced
{
  ISequenced sequenced;
  public IDirectedCollectionValue Test()
  {
    IDirectedCollectionValue RNTRNTRNT_35 = sequenced.Backwards();
    IACSharpSensor.IACSharpSensor.SensorReached(91);
    return RNTRNTRNT_35;
  }
}
class X
{
  static void Main()
  {
  }
}
