using System;
interface IA
{
  int Add(int i);
}
interface IB
{
  int Add(int i);
}
interface IC : IA, IB
{
}
interface IE : ICloneable, IDisposable
{
  void doom();
}
class D : IC, IB
{
  int IA.Add(int i)
  {
    System.Int32 RNTRNTRNT_36 = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(92);
    return RNTRNTRNT_36;
  }
  int IB.Add(int i)
  {
    System.Int32 RNTRNTRNT_37 = 6;
    IACSharpSensor.IACSharpSensor.SensorReached(93);
    return RNTRNTRNT_37;
  }
}
class E : IE, IC
{
  public E()
  {
  }
  public void doom()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(94);
    return;
  }
  public Object Clone()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(95);
    return null;
  }
  public void Dispose()
  {
  }
  int IA.Add(int i)
  {
    System.Int32 RNTRNTRNT_38 = 7;
    IACSharpSensor.IACSharpSensor.SensorReached(96);
    return RNTRNTRNT_38;
  }
  int IB.Add(int i)
  {
    System.Int32 RNTRNTRNT_39 = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(97);
    return RNTRNTRNT_39;
  }
}
class C
{
  static int Test(IC n)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(98);
    IA a = (IA)n;
    if (a.Add(0) != 5) {
      System.Int32 RNTRNTRNT_40 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(99);
      return RNTRNTRNT_40;
    }
    if (((IA)n).Add(0) != 5) {
      System.Int32 RNTRNTRNT_41 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(100);
      return RNTRNTRNT_41;
    }
    if (((IB)n).Add(0) != 6) {
      System.Int32 RNTRNTRNT_42 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(101);
      return RNTRNTRNT_42;
    }
    System.Int32 RNTRNTRNT_43 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(102);
    return RNTRNTRNT_43;
  }
  static void Test2(IE ie)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(103);
    ie.doom();
    ie.Clone();
    ie.Dispose();
    IACSharpSensor.IACSharpSensor.SensorReached(104);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(105);
    D d = new D();
    E e = new E();
    Test(e);
    Test2(e);
    System.Int32 RNTRNTRNT_44 = Test(d);
    IACSharpSensor.IACSharpSensor.SensorReached(106);
    return RNTRNTRNT_44;
  }
}
