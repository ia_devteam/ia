namespace X
{
  enum Z
  {
    x
  }
}
namespace A
{
  using Y = X;
  namespace B
  {
    using Y;
    class Tester
    {
      static internal Z z = Z.x;
      static void Main()
      {
      }
    }
  }
}
