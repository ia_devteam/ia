using System;
struct PointF
{
  public float fa, fb;
  public PointF(float a, float b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(55);
    fa = a;
    fb = b;
    Console.WriteLine("PointF created {0} and {1}", fa, fb);
    IACSharpSensor.IACSharpSensor.SensorReached(56);
  }
}
struct Point
{
  int ia, ib;
  public static implicit operator PointF(Point pt)
  {
    PointF RNTRNTRNT_12 = new PointF(pt.ia, pt.ib);
    IACSharpSensor.IACSharpSensor.SensorReached(57);
    return RNTRNTRNT_12;
  }
  public Point(int a, int b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(58);
    Console.WriteLine("Initialized with {0} and {1}", a, b);
    ia = a;
    ib = b;
    IACSharpSensor.IACSharpSensor.SensorReached(59);
  }
}
class X
{
  static bool ok = false;
  PointF field;
  static bool Method(PointF f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(60);
    Console.WriteLine("Method with PointF arg: {0} {1}", f.fa, f.fb);
    if (f.fa != 100 || f.fb != 200) {
      System.Boolean RNTRNTRNT_13 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(61);
      return RNTRNTRNT_13;
    }
    System.Boolean RNTRNTRNT_14 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    return RNTRNTRNT_14;
  }
  static bool Call_constructor_and_implicit()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(63);
    ok = false;
    System.Boolean RNTRNTRNT_15 = Method(new Point(100, 200));
    IACSharpSensor.IACSharpSensor.SensorReached(64);
    return RNTRNTRNT_15;
  }
  static bool Init_with_implicit_conv()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(65);
    PointF p = new Point(1, 100);
    if (p.fa == 1 && p.fb == 100) {
      System.Boolean RNTRNTRNT_16 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(66);
      return RNTRNTRNT_16;
    }
    System.Boolean RNTRNTRNT_17 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(67);
    return RNTRNTRNT_17;
  }
  static bool Init_ValueType()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    Point p = new Point(100, 200);
    System.Boolean RNTRNTRNT_18 = Method(p);
    IACSharpSensor.IACSharpSensor.SensorReached(69);
    return RNTRNTRNT_18;
  }
  static bool InstanceAssignTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(70);
    X x = new X();
    x.field = new Point(100, 200);
    if (x.field.fa != 100 || x.field.fb != 200) {
      System.Boolean RNTRNTRNT_19 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(71);
      return RNTRNTRNT_19;
    }
    System.Boolean RNTRNTRNT_20 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(72);
    return RNTRNTRNT_20;
  }
  static int T()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(73);
    if (!Init_with_implicit_conv()) {
      System.Int32 RNTRNTRNT_21 = 100;
      IACSharpSensor.IACSharpSensor.SensorReached(74);
      return RNTRNTRNT_21;
    }
    if (!Call_constructor_and_implicit()) {
      System.Int32 RNTRNTRNT_22 = 101;
      IACSharpSensor.IACSharpSensor.SensorReached(75);
      return RNTRNTRNT_22;
    }
    if (!Init_ValueType()) {
      System.Int32 RNTRNTRNT_23 = 102;
      IACSharpSensor.IACSharpSensor.SensorReached(76);
      return RNTRNTRNT_23;
    }
    if (!InstanceAssignTest()) {
      System.Int32 RNTRNTRNT_24 = 103;
      IACSharpSensor.IACSharpSensor.SensorReached(77);
      return RNTRNTRNT_24;
    }
    System.Int32 RNTRNTRNT_25 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(78);
    return RNTRNTRNT_25;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(79);
    int t = T();
    if (t != 0) {
      Console.WriteLine("Failed on test: " + t);
    }
    Console.WriteLine("Succeed");
    System.Int32 RNTRNTRNT_26 = t;
    IACSharpSensor.IACSharpSensor.SensorReached(80);
    return RNTRNTRNT_26;
  }
}
