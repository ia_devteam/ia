using System.Reflection;
using System;
using A;
namespace A
{
  interface B
  {
    void METHOD();
  }
}
class D : B
{
  void B.METHOD()
  {
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(81);
    MethodInfo[] mi = typeof(D).GetMethods(BindingFlags.Instance | BindingFlags.NonPublic);
    MethodInfo m = null;
    foreach (MethodInfo j in mi) {
      if (j.Name.IndexOf("METHOD") != -1) {
        m = j;
        break;
      }
    }
    if (m == null) {
      System.Int32 RNTRNTRNT_27 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(82);
      return RNTRNTRNT_27;
    }
    if (m.Name != "A.B.METHOD") {
      Console.WriteLine("Incorrect method name, expecting: {0} got {1}", "A.B.METHOD", m.Name);
      System.Int32 RNTRNTRNT_28 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(83);
      return RNTRNTRNT_28;
    }
    System.Int32 RNTRNTRNT_29 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(84);
    return RNTRNTRNT_29;
  }
}
