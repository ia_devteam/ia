using System;
public struct Result
{
  public int res;
  double duh;
  long bah;
  public Result(int val)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(391);
    res = val;
    bah = val;
    duh = val;
    IACSharpSensor.IACSharpSensor.SensorReached(392);
  }
}
public class Vararg
{
  public static int AddABunchOfInts(__arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(393);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    System.Int32 RNTRNTRNT_161 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(394);
    return RNTRNTRNT_161;
  }
  public static int AddASecondBunchOfInts(int a, __arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(395);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    System.Int32 RNTRNTRNT_162 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(396);
    return RNTRNTRNT_162;
  }
  public static Result VtAddABunchOfInts(__arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(397);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    Result RNTRNTRNT_163 = new Result(result);
    IACSharpSensor.IACSharpSensor.SensorReached(398);
    return RNTRNTRNT_163;
  }
  public static Result VtAddASecondBunchOfInts(int a, __arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(399);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    Result RNTRNTRNT_164 = new Result(result);
    IACSharpSensor.IACSharpSensor.SensorReached(400);
    return RNTRNTRNT_164;
  }
  public int InstAddABunchOfInts(__arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(401);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    System.Int32 RNTRNTRNT_165 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(402);
    return RNTRNTRNT_165;
  }
  public int InstAddASecondBunchOfInts(int a, __arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(403);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    System.Int32 RNTRNTRNT_166 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(404);
    return RNTRNTRNT_166;
  }
  public Result InstVtAddABunchOfInts(__arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(405);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    Result RNTRNTRNT_167 = new Result(result);
    IACSharpSensor.IACSharpSensor.SensorReached(406);
    return RNTRNTRNT_167;
  }
  public Result InstVtAddASecondBunchOfInts(int a, __arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    Result RNTRNTRNT_168 = new Result(result);
    IACSharpSensor.IACSharpSensor.SensorReached(408);
    return RNTRNTRNT_168;
  }
}
