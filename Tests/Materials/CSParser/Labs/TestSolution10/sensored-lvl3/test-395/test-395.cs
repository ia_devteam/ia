using System;
public class A
{
  protected string name;
  public A(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(335);
    this.name = "A::" + name;
    IACSharpSensor.IACSharpSensor.SensorReached(336);
  }
  public A()
  {
  }
  public string Name {
    get {
      System.String RNTRNTRNT_131 = name;
      IACSharpSensor.IACSharpSensor.SensorReached(337);
      return RNTRNTRNT_131;
    }
  }
}
public class B : A
{
  public B(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(338);
    this.name = "B::" + name;
    IACSharpSensor.IACSharpSensor.SensorReached(339);
  }
  public B()
  {
  }
}
public class C : B
{
  string value;
  public C(string name, string value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(340);
    this.name = "C::" + name;
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(341);
  }
  public string Value {
    get {
      System.String RNTRNTRNT_132 = value;
      IACSharpSensor.IACSharpSensor.SensorReached(342);
      return RNTRNTRNT_132;
    }
  }
}
public class Tester
{
  delegate void MethodHandler(C c1, C c2, C c3);
  static void MethodSample(B b, A a, C c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(343);
    Console.WriteLine("b = {0}", b.Name);
    Console.WriteLine("a = {0}", a.Name);
    Console.WriteLine("c = {0}, {1}", c.Name, c.Value);
    IACSharpSensor.IACSharpSensor.SensorReached(344);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(345);
    MethodHandler mh = MethodSample;
    C a = new C("Hello", "hello");
    C b = new C("World", "world");
    C c = new C("!", "!!!");
    mh(b, a, c);
    IACSharpSensor.IACSharpSensor.SensorReached(346);
  }
}
