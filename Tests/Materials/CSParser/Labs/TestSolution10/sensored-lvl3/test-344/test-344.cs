using System;
delegate void Y();
class X
{
  public event Y y;
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(175);
    X x = new X();
    x.Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(176);
  }
  int a;
  void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    int x = 1;
    y += delegate {
      Console.WriteLine(x);
      Console.WriteLine(this.GetType());
    };
    y();
    IACSharpSensor.IACSharpSensor.SensorReached(178);
  }
}
