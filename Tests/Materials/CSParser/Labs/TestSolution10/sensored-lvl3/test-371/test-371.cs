public class X
{
  public X(out bool hello)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    hello = true;
    IACSharpSensor.IACSharpSensor.SensorReached(246);
  }
  static void Main()
  {
  }
}
public class Y : X
{
  public Y(out bool hello) : base(out hello)
  {
  }
}
