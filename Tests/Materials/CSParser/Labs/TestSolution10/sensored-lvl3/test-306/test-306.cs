using System;
using C = A.D;
class A
{
  protected internal class D : Exception
  {
  }
  public class B
  {
    class C : Exception
    {
    }
    public B()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(26);
      try {
        throw new A.B.C();
      } catch (C e) {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(27);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(28);
    object o = new A.B();
    IACSharpSensor.IACSharpSensor.SensorReached(29);
  }
}
