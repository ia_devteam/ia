using System;
class A
{
  class C : IDisposable
  {
    void IDisposable.Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(13);
      throw new Exception("'using' and 'new' didn't resolve C as A+B+C");
    }
  }
  public class B
  {
    class C : IDisposable
    {
      void IDisposable.Dispose()
      {
      }
    }
    public B()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(14);
      using (C c = new C()) {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(15);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    object o = new A.B();
    IACSharpSensor.IACSharpSensor.SensorReached(17);
  }
}
