using System;
using System.Reflection;
enum E
{
}
delegate void D();
class C
{
  public C()
  {
  }
  int i = new int();
  int i2 = 1 - 1;
  double d = new double();
  char c = new char();
  bool b = new bool();
  decimal dec2 = new decimal();
  object o = null;
  ValueType BoolVal = (ValueType)null;
  E e = new E();
  event D Ev1 = null;
  int[] a_i = null;
  object[] a_o = null;
  ValueType[] a_v = null;
}
class X
{
  public static event D Ev1 = null;
  public static event D Ev2 = null;
  protected static string temp = null, real_temp = null;
}
class X2
{
  static int i = 5;
}
class Test
{
  static int a = b = 5;
  static int b = 0;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(187);
    if (a != 5 || b != 0) {
      System.Int32 RNTRNTRNT_69 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(188);
      return RNTRNTRNT_69;
    }
    if ((typeof(X2).Attributes & TypeAttributes.BeforeFieldInit) == 0) {
      System.Int32 RNTRNTRNT_70 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(189);
      return RNTRNTRNT_70;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_71 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(190);
    return RNTRNTRNT_71;
  }
}
