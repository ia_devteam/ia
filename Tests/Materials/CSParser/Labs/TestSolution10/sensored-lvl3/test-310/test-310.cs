namespace test
{
  interface IIntf1
  {
    string GetType(int index);
  }
  interface IIntf2 : IIntf1
  {
    bool IsDone();
  }
  class Impl : IIntf2
  {
    public string GetType(int index)
    {
      System.String RNTRNTRNT_1 = "none";
      IACSharpSensor.IACSharpSensor.SensorReached(37);
      return RNTRNTRNT_1;
    }
    public bool IsDone()
    {
      System.Boolean RNTRNTRNT_2 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(38);
      return RNTRNTRNT_2;
    }
  }
  class myclass
  {
    public static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(39);
      IIntf1 intf = new Impl();
      IIntf2 intf2 = intf as IIntf2;
      if (intf2 != null) {
        string str = intf2.GetType(0);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(40);
    }
  }
}
