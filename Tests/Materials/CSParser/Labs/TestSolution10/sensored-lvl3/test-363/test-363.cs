public class Location
{
  public static readonly Location UnknownLocation = new Location();
  private Location()
  {
  }
}
public abstract class Element
{
  private Location _location = Location.UnknownLocation;
  protected virtual Location Location {
    get {
      Location RNTRNTRNT_72 = _location;
      IACSharpSensor.IACSharpSensor.SensorReached(205);
      return RNTRNTRNT_72;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(206);
      _location = value;
      IACSharpSensor.IACSharpSensor.SensorReached(207);
    }
  }
}
public class T
{
  public static void Main()
  {
  }
}
