using System;
class A
{
  public virtual void Foo(int i)
  {
  }
  public virtual void Foo(double d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(168);
    throw new Exception("Shouldn't be invoked");
  }
}
class B : A
{
  public override void Foo(double d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(169);
    throw new Exception("Overload resolution failed");
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    new B().Foo(1);
    IACSharpSensor.IACSharpSensor.SensorReached(171);
  }
}
