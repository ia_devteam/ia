using System;
public delegate double Mapper(int item);
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(140);
    Mapper mapper = new Mapper(delegate(int i) { return i * 12; });
    if (mapper(3) == 36) {
      System.Int32 RNTRNTRNT_55 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(141);
      return RNTRNTRNT_55;
    }
    System.Int32 RNTRNTRNT_56 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(142);
    return RNTRNTRNT_56;
  }
}
