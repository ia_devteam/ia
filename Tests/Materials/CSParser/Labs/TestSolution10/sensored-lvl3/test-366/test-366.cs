using System;
using System.Reflection;
using System.Runtime.InteropServices;
[StructLayout(LayoutKind.Explicit)]
struct foo2
{
}
class C
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    foo2 f = new foo2();
    Type fit = typeof(FieldInfo);
    MethodInfo gfo = fit.GetMethod("GetFieldOffset", BindingFlags.Instance | BindingFlags.NonPublic);
    if (gfo == null) {
      Console.WriteLine("PASS: On MS runtime, Test OK");
      System.Int32 RNTRNTRNT_80 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(221);
      return RNTRNTRNT_80;
    }
    Type t = typeof(foo2);
    FieldInfo fi = t.GetField("$PRIVATE$", BindingFlags.Instance | BindingFlags.NonPublic);
    object res = gfo.Invoke(fi, null);
    if (res.GetType() != typeof(Int32)) {
      System.Int32 RNTRNTRNT_81 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(222);
      return RNTRNTRNT_81;
    }
    int r = (int)res;
    if (r != 0) {
      Console.WriteLine("FAIL: Offset is not zero");
      System.Int32 RNTRNTRNT_82 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(223);
      return RNTRNTRNT_82;
    }
    Console.WriteLine("PASS: Test passes on Mono");
    System.Int32 RNTRNTRNT_83 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(224);
    return RNTRNTRNT_83;
  }
}
