using System;
class X
{
  static int Foo = 10;
  static void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(282);
    while (true) {
      if (Foo == 1) {
        throw new Exception("Error Test");
      } else {
        break;
      }
    }
    Foo = 20;
    IACSharpSensor.IACSharpSensor.SensorReached(283);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(284);
    Test();
    if (Foo != 20) {
      System.Int32 RNTRNTRNT_111 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(285);
      return RNTRNTRNT_111;
    }
    System.Int32 RNTRNTRNT_112 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    return RNTRNTRNT_112;
  }
}
