using System;
class X
{
  delegate int Foo();
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(208);
    int x = t1(1);
    if (x != 1) {
      System.Int32 RNTRNTRNT_73 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(209);
      return RNTRNTRNT_73;
    }
    x = t2(2);
    if (x != 3) {
      System.Int32 RNTRNTRNT_74 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(210);
      return RNTRNTRNT_74;
    }
    System.Int32 RNTRNTRNT_75 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(211);
    return RNTRNTRNT_75;
  }
  static int t1(int p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(212);
    Foo f = delegate {
      System.Int32 RNTRNTRNT_76 = p;
      IACSharpSensor.IACSharpSensor.SensorReached(213);
      return RNTRNTRNT_76;
    };
    System.Int32 RNTRNTRNT_77 = f();
    IACSharpSensor.IACSharpSensor.SensorReached(214);
    return RNTRNTRNT_77;
  }
  static int t2(int p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    p++;
    Foo f = delegate {
      System.Int32 RNTRNTRNT_78 = p;
      IACSharpSensor.IACSharpSensor.SensorReached(216);
      return RNTRNTRNT_78;
    };
    System.Int32 RNTRNTRNT_79 = f();
    IACSharpSensor.IACSharpSensor.SensorReached(217);
    return RNTRNTRNT_79;
  }
  static void Main2(string[] argv)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(218);
    Console.WriteLine("Test");
    Delegable db = new Delegable();
    if (argv.Length > 1) {
      db.MyDelegate += delegate(object o, EventArgs args) {
        Console.WriteLine("{0}", argv);
        Console.WriteLine("{0}", db);
      };
    }
    IACSharpSensor.IACSharpSensor.SensorReached(219);
  }
}
class Delegable
{
  public event EventHandler MyDelegate;
}
