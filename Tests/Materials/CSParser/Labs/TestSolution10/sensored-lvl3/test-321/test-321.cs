using System;
struct X : IDisposable
{
  public void Dispose()
  {
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(125);
    X x = new X();
    using (x) {
      ;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(126);
  }
}
