using System;
public class A
{
  protected string name;
  public A(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(322);
    this.name = "A::" + name;
    IACSharpSensor.IACSharpSensor.SensorReached(323);
  }
  public A()
  {
  }
  public string Name {
    get {
      System.String RNTRNTRNT_126 = name;
      IACSharpSensor.IACSharpSensor.SensorReached(324);
      return RNTRNTRNT_126;
    }
  }
}
public class B : A
{
  public B(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(325);
    this.name = "B::" + name;
    IACSharpSensor.IACSharpSensor.SensorReached(326);
  }
  public B()
  {
  }
}
public class C : B
{
  string value;
  public C(string name, string value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(327);
    this.name = "C::" + name;
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(328);
  }
  public string Value {
    get {
      System.String RNTRNTRNT_127 = value;
      IACSharpSensor.IACSharpSensor.SensorReached(329);
      return RNTRNTRNT_127;
    }
  }
}
public class Tester
{
  delegate string MethodHandler(C c);
  static string MethodSampleA(A value)
  {
    System.String RNTRNTRNT_128 = value.Name;
    IACSharpSensor.IACSharpSensor.SensorReached(330);
    return RNTRNTRNT_128;
  }
  static string MethodSampleB(B value)
  {
    System.String RNTRNTRNT_129 = value.Name;
    IACSharpSensor.IACSharpSensor.SensorReached(331);
    return RNTRNTRNT_129;
  }
  static string MethodSampleC(C value)
  {
    System.String RNTRNTRNT_130 = value.Name + " " + value.Value;
    IACSharpSensor.IACSharpSensor.SensorReached(332);
    return RNTRNTRNT_130;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(333);
    MethodHandler da = MethodSampleA;
    MethodHandler db = MethodSampleB;
    MethodHandler dc = MethodSampleC;
    C a = new C("Hello", "hello");
    C b = new C("World", "world");
    C c = new C("!", "!!!");
    Console.WriteLine(da(a));
    Console.WriteLine(db(b));
    Console.WriteLine(dc(c));
    IACSharpSensor.IACSharpSensor.SensorReached(334);
  }
}
