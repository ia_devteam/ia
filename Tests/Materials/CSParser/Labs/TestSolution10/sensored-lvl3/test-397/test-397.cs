using System;
using System.Reflection;
[AttributeUsage(AttributeTargets.Class)]
public class TypeCheckAttribute : Attribute
{
  public TypeCheckAttribute()
  {
  }
}
[AttributeUsage(AttributeTargets.Property)]
public class PropertyCheckAttribute : Attribute
{
  public PropertyCheckAttribute()
  {
  }
}
[AttributeUsage(AttributeTargets.Method)]
public class AccessorCheckAttribute : Attribute
{
  MethodAttributes flags;
  public AccessorCheckAttribute(MethodAttributes flags)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(354);
    this.flags = flags;
    IACSharpSensor.IACSharpSensor.SensorReached(355);
  }
  public MethodAttributes Attributes {
    get {
      MethodAttributes RNTRNTRNT_139 = flags;
      IACSharpSensor.IACSharpSensor.SensorReached(356);
      return RNTRNTRNT_139;
    }
  }
}
public class Test
{
  public static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(357);
    Type t = typeof(A);
    foreach (PropertyInfo pi in t.GetProperties()) {
      object[] attrs = pi.GetCustomAttributes(typeof(PropertyCheckAttribute), true);
      if (attrs == null) {
        System.Int32 RNTRNTRNT_140 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(358);
        return RNTRNTRNT_140;
      }
      MethodInfo get_accessor, set_accessor;
      get_accessor = pi.GetGetMethod(true);
      set_accessor = pi.GetSetMethod(true);
      if (get_accessor != null) {
        CheckFlags(pi, get_accessor);
      }
      if (set_accessor != null) {
        CheckFlags(pi, set_accessor);
      }
    }
    System.Int32 RNTRNTRNT_141 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(359);
    return RNTRNTRNT_141;
  }
  static void CheckFlags(PropertyInfo pi, MethodInfo accessor)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(360);
    object[] attrs = accessor.GetCustomAttributes(typeof(AccessorCheckAttribute), true);
    if (attrs == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(361);
      return;
    }
    AccessorCheckAttribute accessor_attr = (AccessorCheckAttribute)attrs[0];
    MethodAttributes accessor_flags = accessor.Attributes;
    if ((accessor_flags & accessor_attr.Attributes) == accessor_attr.Attributes) {
      Console.WriteLine("Test for {0}.{1} PASSED", pi.Name, accessor.Name);
    } else {
      string message = String.Format("Test for {0}.{1} INCORRECT: MethodAttributes should be {2}, but are {3}", pi.Name, accessor.Name, accessor_attr.Attributes, accessor_flags);
      throw new Exception(message);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(362);
  }
}
[TypeCheck()]
public class A
{
  const MethodAttributes flags = MethodAttributes.HideBySig | MethodAttributes.SpecialName;
  [PropertyCheck()]
  public int Value1 {
    [AccessorCheck(flags | MethodAttributes.Public)]
    get {
      System.Int32 RNTRNTRNT_142 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(363);
      return RNTRNTRNT_142;
    }
    [AccessorCheck(flags | MethodAttributes.Public)]
    set { }
  }
  [PropertyCheck()]
  public int Value2 {
    [AccessorCheck(flags | MethodAttributes.Public)]
    get {
      System.Int32 RNTRNTRNT_143 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(364);
      return RNTRNTRNT_143;
    }
    [AccessorCheck(flags | MethodAttributes.FamORAssem)]
    protected internal set { }
  }
  [PropertyCheck()]
  public int Value3 {
    [AccessorCheck(flags | MethodAttributes.Public)]
    get {
      System.Int32 RNTRNTRNT_144 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(365);
      return RNTRNTRNT_144;
    }
    [AccessorCheck(flags | MethodAttributes.Family)]
    protected set { }
  }
  [PropertyCheck()]
  public int Value4 {
    [AccessorCheck(flags | MethodAttributes.Assembly)]
    internal get {
      System.Int32 RNTRNTRNT_145 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(366);
      return RNTRNTRNT_145;
    }
    [AccessorCheck(flags | MethodAttributes.Public)]
    set { }
  }
  [PropertyCheck()]
  public int Value5 {
    [AccessorCheck(flags | MethodAttributes.Public)]
    get {
      System.Int32 RNTRNTRNT_146 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(367);
      return RNTRNTRNT_146;
    }
    [AccessorCheck(flags | MethodAttributes.Private)]
    private set { }
  }
}
