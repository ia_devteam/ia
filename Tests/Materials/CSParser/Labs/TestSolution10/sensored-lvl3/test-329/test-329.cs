using System;
[X(null)]
class X : Attribute
{
  int ID;
  public X()
  {
  }
  public X(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(143);
    if (o == null) {
      ID = 55;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(144);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(145);
    object[] attrs = typeof(X).GetCustomAttributes(typeof(X), false);
    if (attrs.Length != 1) {
      System.Int32 RNTRNTRNT_57 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(146);
      return RNTRNTRNT_57;
    }
    X x = attrs[0] as X;
    if (x.ID != 55) {
      System.Int32 RNTRNTRNT_58 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(147);
      return RNTRNTRNT_58;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_59 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(148);
    return RNTRNTRNT_59;
  }
}
