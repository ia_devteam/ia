using System;
using System.Runtime.CompilerServices;
public class ConstFields
{
  public const decimal ConstDecimal1 = 314159265358979323846m;
  public static readonly decimal ConstDecimal2 = -314159265358979323846m;
  public const decimal ConstDecimal3 = -3;
  public const decimal ConstDecimal4 = 0;
  public const decimal MaxValue = 79228162514264337593543950335m;
  static readonly Decimal MaxValueDiv10 = MaxValue / 10;
  static decimal DecimalValue = -90;
  const decimal SmallConstValue = 0.02m;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(117);
    Type t = typeof(ConstFields);
    DecimalConstantAttribute a = (DecimalConstantAttribute)t.GetField("ConstDecimal3").GetCustomAttributes(typeof(DecimalConstantAttribute), false)[0];
    if (a.Value != ConstDecimal3) {
      System.Int32 RNTRNTRNT_47 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(118);
      return RNTRNTRNT_47;
    }
    a = (DecimalConstantAttribute)t.GetField("ConstDecimal1").GetCustomAttributes(typeof(DecimalConstantAttribute), false)[0];
    if (a.Value != 314159265358979323846m) {
      System.Int32 RNTRNTRNT_48 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(119);
      return RNTRNTRNT_48;
    }
    if (ConstDecimal1 != (-1) * ConstDecimal2) {
      System.Int32 RNTRNTRNT_49 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(120);
      return RNTRNTRNT_49;
    }
    if (!(SmallConstValue < 1 && SmallConstValue > 0)) {
      System.Int32 RNTRNTRNT_50 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(121);
      return RNTRNTRNT_50;
    }
    Console.WriteLine(C.D);
    Console.WriteLine(Decimal.One);
    Console.WriteLine(DecimalValue);
    Console.WriteLine(Decimal.MaxValue);
    Console.WriteLine("Success");
    System.Int32 RNTRNTRNT_51 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(122);
    return RNTRNTRNT_51;
  }
}
