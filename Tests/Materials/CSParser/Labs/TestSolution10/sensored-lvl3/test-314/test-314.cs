class X
{
  static string a = "static string";
  string b = a + "string";
  X()
  {
  }
  X(int x)
  {
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(85);
    if (new X().b != "static stringstring") {
      System.Int32 RNTRNTRNT_30 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(86);
      return RNTRNTRNT_30;
    }
    if (new X(1).b != "static stringstring") {
      System.Int32 RNTRNTRNT_31 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(87);
      return RNTRNTRNT_31;
    }
    System.Int32 RNTRNTRNT_32 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(88);
    return RNTRNTRNT_32;
  }
}
