using System;
public delegate void Foo();
public delegate void Bar(int x);
class X
{
  public X(Foo foo)
  {
  }
  public X(Bar bar)
  {
  }
  static void Test()
  {
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(158);
    X x = new X(Test);
    IACSharpSensor.IACSharpSensor.SensorReached(159);
  }
}
