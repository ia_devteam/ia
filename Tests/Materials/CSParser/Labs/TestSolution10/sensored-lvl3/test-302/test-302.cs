using System;
class A
{
  class C : Exception
  {
  }
  public class B
  {
    class C : Exception
    {
    }
    public B()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(9);
      try {
        throw new A.B.C();
      } catch (C e) {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(10);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(11);
    object o = new A.B();
    IACSharpSensor.IACSharpSensor.SensorReached(12);
  }
}
