using System;
public class X
{
  public readonly int Data;
  public X testme(out int x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(276);
    x = 1;
    X RNTRNTRNT_110 = this;
    IACSharpSensor.IACSharpSensor.SensorReached(277);
    return RNTRNTRNT_110;
  }
  public X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(278);
    int x, y;
    y = this.testme(out x).Data;
    Console.WriteLine("X is {0}", x);
    IACSharpSensor.IACSharpSensor.SensorReached(279);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(280);
    X x = new X();
    IACSharpSensor.IACSharpSensor.SensorReached(281);
  }
}
