using System;
using C = A.D;
public class A
{
  public class D : IDisposable
  {
    void IDisposable.Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(30);
      throw new Exception("'using' and 'new' didn't resolve C as A+B+C");
    }
  }
  public class B
  {
    class C : IDisposable
    {
      void IDisposable.Dispose()
      {
      }
    }
    public B()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(31);
      using (C c = new C()) {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(32);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(33);
    object o = new A.B();
    IACSharpSensor.IACSharpSensor.SensorReached(34);
  }
}
