using System;
public class Tester
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    A a = new A(8);
    B b = new B(9);
    b.SetCount(10);
    Console.WriteLine("b.Count should be 9: {0}", b.Count);
    Console.WriteLine("b [{0}] should return {0}: {1}", 10, b[10]);
    Console.WriteLine("a.Message : {0}", a.Message);
    b.Message = "";
    Console.WriteLine("b.Messasge : {0}", b.Message);
    IACSharpSensor.IACSharpSensor.SensorReached(369);
  }
}
public class A
{
  protected int count;
  public A(int count)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(370);
    this.count = count;
    IACSharpSensor.IACSharpSensor.SensorReached(371);
  }
  public virtual int Count {
    get {
      System.Int32 RNTRNTRNT_147 = count;
      IACSharpSensor.IACSharpSensor.SensorReached(372);
      return RNTRNTRNT_147;
    }
    protected set {
      IACSharpSensor.IACSharpSensor.SensorReached(373);
      count = value;
      IACSharpSensor.IACSharpSensor.SensorReached(374);
    }
  }
  public virtual int this[int index] {
    get {
      System.Int32 RNTRNTRNT_148 = index;
      IACSharpSensor.IACSharpSensor.SensorReached(375);
      return RNTRNTRNT_148;
    }
  }
  public virtual string Message {
    get {
      System.String RNTRNTRNT_149 = "Hello Mono";
      IACSharpSensor.IACSharpSensor.SensorReached(376);
      return RNTRNTRNT_149;
    }
  }
}
public class B : A
{
  public B(int count) : base(count)
  {
  }
  public override int Count {
    protected set { }
  }
  public void SetCount(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(377);
    Count = value;
    IACSharpSensor.IACSharpSensor.SensorReached(378);
  }
  public override int this[int index] {
    get {
      System.Int32 RNTRNTRNT_150 = base[index];
      IACSharpSensor.IACSharpSensor.SensorReached(379);
      return RNTRNTRNT_150;
    }
  }
  public new string Message {
    get {
      System.String RNTRNTRNT_151 = "Hello Mono (2)";
      IACSharpSensor.IACSharpSensor.SensorReached(380);
      return RNTRNTRNT_151;
    }
    internal set { }
  }
}
