using System;
using System.Security;
using System.Security.Permissions;
[assembly: SecurityPermission(SecurityAction.RequestMinimum, Execution = true)]
[assembly: SecurityPermission(SecurityAction.RequestOptional, Unrestricted = true)]
[assembly: SecurityPermission(SecurityAction.RequestRefuse, SkipVerification = true)]
[SecurityPermission(SecurityAction.LinkDemand, ControlPrincipal = true)]
struct LinkDemandStruct
{
  internal string Info;
}
[SecurityPermission(SecurityAction.Demand, ControlAppDomain = true)]
public class Program
{
  private static string _message = "Hello Mono!";
  private LinkDemandStruct info;
  [SecurityPermission(SecurityAction.InheritanceDemand, ControlAppDomain = true)]
  public Program()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(41);
    info = new LinkDemandStruct();
    info.Info = ":-)";
    IACSharpSensor.IACSharpSensor.SensorReached(42);
  }
  public static string Message {
    [SecurityPermission(SecurityAction.PermitOnly, ControlEvidence = true)]
    get {
      System.String RNTRNTRNT_3 = _message;
      IACSharpSensor.IACSharpSensor.SensorReached(43);
      return RNTRNTRNT_3;
    }
    [SecurityPermission(SecurityAction.Assert, ControlThread = true)]
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(44);
      _message = value;
      IACSharpSensor.IACSharpSensor.SensorReached(45);
    }
  }
  [SecurityPermission(SecurityAction.Deny, UnmanagedCode = true)]
  private bool DenyMethod()
  {
    System.Boolean RNTRNTRNT_4 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(46);
    return RNTRNTRNT_4;
  }
  [SiteIdentityPermission(SecurityAction.PermitOnly)]
  [PermissionSet(SecurityAction.PermitOnly, Unrestricted = true)]
  [PermissionSet(SecurityAction.PermitOnly, Unrestricted = false)]
  public void Test2()
  {
  }
  [PermissionSet(SecurityAction.PermitOnly, Unrestricted = true)]
  [PermissionSet(SecurityAction.PermitOnly, Unrestricted = false)]
  public void Test3()
  {
  }
  [EnvironmentPermission(SecurityAction.Demand, Unrestricted = true)]
  public void Test4()
  {
  }
  [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlEvidence, UnmanagedCode = true)]
  [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.AllFlags, UnmanagedCode = true)]
  public static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(47);
    Type program = typeof(Program);
    if (program.GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_5 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(48);
      return RNTRNTRNT_5;
    }
    if (program.GetConstructor(System.Type.EmptyTypes).GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_6 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(49);
      return RNTRNTRNT_6;
    }
    if (program.GetProperty("Message").GetSetMethod().GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_7 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(50);
      return RNTRNTRNT_7;
    }
    if (program.GetMethod("Main").GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_8 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(51);
      return RNTRNTRNT_8;
    }
    if (program.GetMethod("Test2").GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_9 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(52);
      return RNTRNTRNT_9;
    }
    Type test2 = typeof(Test2);
    if (test2.GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_10 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(53);
      return RNTRNTRNT_10;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_11 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(54);
    return RNTRNTRNT_11;
  }
}
[SecurityPermission(SecurityAction.Demand, ControlAppDomain = true)]
public partial class Test2
{
}
[SecurityPermission(SecurityAction.Demand, ControlAppDomain = true)]
public partial class Test2
{
}
