class Y
{
  byte b;
  public static implicit operator int(Y i)
  {
    System.Int32 RNTRNTRNT_52 = i.b;
    IACSharpSensor.IACSharpSensor.SensorReached(127);
    return RNTRNTRNT_52;
  }
  public Y(byte b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(128);
    this.b = b;
    IACSharpSensor.IACSharpSensor.SensorReached(129);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(130);
    Y y = new Y(1);
    switch (y) {
      case 0:
        break;
      case 1:
        break;
    }
    int a = y;
    IACSharpSensor.IACSharpSensor.SensorReached(131);
  }
}
