using System;
public class Foo<T>
{
  public readonly T Item;
  public Foo(T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(631);
    this.Item = item;
    IACSharpSensor.IACSharpSensor.SensorReached(632);
  }
  static void maketreer(out Node rest)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(633);
    rest = new Node();
    IACSharpSensor.IACSharpSensor.SensorReached(634);
  }
  class Node
  {
  }
  public void Hello<U>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(635);
    Foo<U>.Node node;
    Foo<U>.maketreer(out node);
    IACSharpSensor.IACSharpSensor.SensorReached(636);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(637);
  }
}
