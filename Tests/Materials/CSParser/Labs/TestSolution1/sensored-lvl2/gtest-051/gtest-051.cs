using System;
public class Foo<T> where T : A
{
  public void Test(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(350);
    Console.WriteLine(t);
    Console.WriteLine(t.GetType());
    t.Hello();
    IACSharpSensor.IACSharpSensor.SensorReached(351);
  }
}
public class A
{
  public void Hello()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(352);
    Console.WriteLine("Hello World");
    IACSharpSensor.IACSharpSensor.SensorReached(353);
  }
}
public class B : A
{
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(354);
    Foo<B> foo = new Foo<B>();
    foo.Test(new B());
    IACSharpSensor.IACSharpSensor.SensorReached(355);
  }
}
