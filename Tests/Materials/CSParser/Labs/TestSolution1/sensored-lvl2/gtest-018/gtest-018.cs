public class Stack
{
  public Stack()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(110);
  }
  public void Hello<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(111);
  }
}
public class X
{
  public static void Foo(Stack stack)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(112);
    stack.Hello<string>("Hello World");
    IACSharpSensor.IACSharpSensor.SensorReached(113);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(114);
    Stack stack = new Stack();
    Foo(stack);
    IACSharpSensor.IACSharpSensor.SensorReached(115);
  }
}
