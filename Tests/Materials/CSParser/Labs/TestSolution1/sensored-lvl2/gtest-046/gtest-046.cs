using System;
delegate void Test<T>(T t);
class Foo<T>
{
  public event Test<T> MyEvent;
  public void Hello(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(319);
    if (MyEvent != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(320);
      MyEvent(t);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(321);
  }
}
class X
{
  static void do_hello(string hello)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(322);
    Console.WriteLine("Hello: {0}", hello);
    IACSharpSensor.IACSharpSensor.SensorReached(323);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(324);
    Foo<string> foo = new Foo<string>();
    foo.MyEvent += new Test<string>(do_hello);
    foo.Hello("Boston");
    IACSharpSensor.IACSharpSensor.SensorReached(325);
  }
}
