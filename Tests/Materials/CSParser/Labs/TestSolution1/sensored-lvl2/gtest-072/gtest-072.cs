using System;
public class Log<T>
{
  private const int SIZE = 5;
  private static int instanceCount = 0;
  private int count = 0;
  private T[] log = new T[SIZE];
  public Log()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(501);
    instanceCount++;
    IACSharpSensor.IACSharpSensor.SensorReached(502);
  }
  public static int InstanceCount {
    get {
      System.Int32 RNTRNTRNT_72 = instanceCount;
      IACSharpSensor.IACSharpSensor.SensorReached(503);
      return RNTRNTRNT_72;
    }
  }
  public void Add(T msg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(504);
    log[count++ % SIZE] = msg;
    IACSharpSensor.IACSharpSensor.SensorReached(505);
  }
  public int Count {
    get {
      System.Int32 RNTRNTRNT_73 = count;
      IACSharpSensor.IACSharpSensor.SensorReached(506);
      return RNTRNTRNT_73;
    }
  }
  public T Last {
    get {
      T RNTRNTRNT_74 = count == 0 ? default(T) : log[(count - 1) % SIZE];
      IACSharpSensor.IACSharpSensor.SensorReached(507);
      return RNTRNTRNT_74;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(508);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(509);
        log[count++] = value;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(510);
        log[(count - 1) % SIZE] = value;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(511);
    }
  }
  public T[] All {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(512);
      int size = Math.Min(count, SIZE);
      T[] res = new T[size];
      IACSharpSensor.IACSharpSensor.SensorReached(513);
      for (int i = 0; i < size; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(514);
        res[i] = log[(count - size + i) % SIZE];
      }
      T[] RNTRNTRNT_75 = res;
      IACSharpSensor.IACSharpSensor.SensorReached(515);
      return RNTRNTRNT_75;
    }
  }
}
class TestLog
{
  class MyTest
  {
    public static void Main(String[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(516);
      Log<String> log1 = new Log<String>();
      log1.Add("Reboot");
      log1.Add("Coffee");
      Log<DateTime> log2 = new Log<DateTime>();
      log2.Add(DateTime.Now);
      log2.Add(DateTime.Now.AddHours(1));
      DateTime[] dts = log2.All;
      IACSharpSensor.IACSharpSensor.SensorReached(517);
      foreach (String s in log1.All) {
        IACSharpSensor.IACSharpSensor.SensorReached(518);
        Console.Write("{0}   ", s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(519);
      Console.WriteLine();
      IACSharpSensor.IACSharpSensor.SensorReached(520);
      foreach (DateTime dt in dts) {
        IACSharpSensor.IACSharpSensor.SensorReached(521);
        Console.Write("{0}   ", dt);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(522);
      Console.WriteLine();
      TestPairLog();
      IACSharpSensor.IACSharpSensor.SensorReached(523);
    }
    public static void TestPairLog()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(524);
      Log<Pair<DateTime, String>> log = new Log<Pair<DateTime, String>>();
      log.Add(new Pair<DateTime, String>(DateTime.Now, "Tea leaves"));
      log.Add(new Pair<DateTime, String>(DateTime.Now.AddMinutes(2), "Hot water"));
      log.Add(new Pair<DateTime, String>(DateTime.Now.AddMinutes(7), "Ready"));
      Pair<DateTime, String>[] allMsgs = log.All;
      IACSharpSensor.IACSharpSensor.SensorReached(525);
      foreach (Pair<DateTime, String> p in allMsgs) {
        IACSharpSensor.IACSharpSensor.SensorReached(526);
        Console.WriteLine("At {0}: {1}", p.Fst, p.Snd);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(527);
    }
  }
}
public struct Pair<T, U>
{
  public readonly T Fst;
  public readonly U Snd;
  public Pair(T fst, U snd)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(528);
    this.Fst = fst;
    this.Snd = snd;
    IACSharpSensor.IACSharpSensor.SensorReached(529);
  }
}
