class Foo<T>
{
  public void Hello()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(138);
  }
  public void World(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(139);
    Hello();
    IACSharpSensor.IACSharpSensor.SensorReached(140);
  }
}
class Bar : Foo<int>
{
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(141);
    Hello();
    World(4);
    IACSharpSensor.IACSharpSensor.SensorReached(142);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(143);
    Bar bar = new Bar();
    bar.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(144);
  }
}
