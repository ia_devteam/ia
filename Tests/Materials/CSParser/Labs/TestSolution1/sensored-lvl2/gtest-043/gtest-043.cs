using System;
public class Test<T>
{
  public static int Count;
  public void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(270);
    Count++;
    IACSharpSensor.IACSharpSensor.SensorReached(271);
  }
  public int GetCount()
  {
    System.Int32 RNTRNTRNT_19 = Count;
    IACSharpSensor.IACSharpSensor.SensorReached(272);
    return RNTRNTRNT_19;
  }
}
class X
{
  static int DoTheTest<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(273);
    Test<T> test = new Test<T>();
    test.Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(274);
    if (test.GetCount() != 1) {
      System.Int32 RNTRNTRNT_20 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(275);
      return RNTRNTRNT_20;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(276);
    if (Test<T>.Count != 1) {
      System.Int32 RNTRNTRNT_21 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(277);
      return RNTRNTRNT_21;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(278);
    test.Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(279);
    if (test.GetCount() != 2) {
      System.Int32 RNTRNTRNT_22 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(280);
      return RNTRNTRNT_22;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(281);
    if (Test<T>.Count != 2) {
      System.Int32 RNTRNTRNT_23 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(282);
      return RNTRNTRNT_23;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    test.Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(284);
    if (test.GetCount() != 3) {
      System.Int32 RNTRNTRNT_24 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(285);
      return RNTRNTRNT_24;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    if (Test<T>.Count != 3) {
      System.Int32 RNTRNTRNT_25 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(287);
      return RNTRNTRNT_25;
    }
    System.Int32 RNTRNTRNT_26 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(288);
    return RNTRNTRNT_26;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(289);
    int result = DoTheTest<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(290);
    if (result != 0) {
      System.Int32 RNTRNTRNT_27 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(291);
      return RNTRNTRNT_27;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(292);
    result = DoTheTest<long>() + 10;
    IACSharpSensor.IACSharpSensor.SensorReached(293);
    if (result != 10) {
      System.Int32 RNTRNTRNT_28 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(294);
      return RNTRNTRNT_28;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(295);
    Test<int>.Count = 0;
    ++Test<long>.Count;
    result = DoTheTest<int>() + 20;
    IACSharpSensor.IACSharpSensor.SensorReached(296);
    if (result != 20) {
      System.Int32 RNTRNTRNT_29 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(297);
      return RNTRNTRNT_29;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(298);
    if (Test<int>.Count != 3) {
      System.Int32 RNTRNTRNT_30 = 31;
      IACSharpSensor.IACSharpSensor.SensorReached(299);
      return RNTRNTRNT_30;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(300);
    if (Test<long>.Count != 4) {
      System.Int32 RNTRNTRNT_31 = 32;
      IACSharpSensor.IACSharpSensor.SensorReached(301);
      return RNTRNTRNT_31;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(302);
    Test<float>.Count = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(303);
    if (Test<int>.Count != 3) {
      System.Int32 RNTRNTRNT_32 = 33;
      IACSharpSensor.IACSharpSensor.SensorReached(304);
      return RNTRNTRNT_32;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(305);
    if (Test<long>.Count != 4) {
      System.Int32 RNTRNTRNT_33 = 34;
      IACSharpSensor.IACSharpSensor.SensorReached(306);
      return RNTRNTRNT_33;
    }
    System.Int32 RNTRNTRNT_34 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(307);
    return RNTRNTRNT_34;
  }
}
