class Test
{
  public delegate int Foo<T>(T t, T u);
  public void Hello<U>(Foo<U> foo, U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(485);
  }
}
class X
{
  static int Add(int a, int b)
  {
    System.Int32 RNTRNTRNT_67 = a + b;
    IACSharpSensor.IACSharpSensor.SensorReached(486);
    return RNTRNTRNT_67;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(487);
    Test test = new Test();
    test.Hello<int>(new Test.Foo<int>(Add), 5);
    IACSharpSensor.IACSharpSensor.SensorReached(488);
  }
}
