public class Foo<T>
{
  public void Hello(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(200);
  }
}
public class Bar<T, U> : Foo<U>
{
  public void Test(T t, U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(201);
  }
}
