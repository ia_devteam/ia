using System;
interface I
{
  void Hello();
}
class J
{
  public void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(77);
    Console.WriteLine("Foo!");
    IACSharpSensor.IACSharpSensor.SensorReached(78);
  }
}
class Stack<T> where T : J, I
{
  public void Test(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(79);
    t.Hello();
    t.Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(80);
  }
}
class Test
{
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(81);
  }
}
