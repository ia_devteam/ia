namespace Martin
{
  public class Test<T>
  {
    public static int Foo()
    {
      System.Int32 RNTRNTRNT_70 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(494);
      return RNTRNTRNT_70;
    }
  }
}
class Foo<T>
{
  public Foo(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(495);
  }
  public Foo() : this(Martin.Test<T>.Foo())
  {
    IACSharpSensor.IACSharpSensor.SensorReached(496);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(497);
  }
}
