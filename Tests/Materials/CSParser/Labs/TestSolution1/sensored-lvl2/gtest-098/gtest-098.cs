public class Foo : IFoo
{
  void IFoo<X>.Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(656);
  }
  void IFoo<Y, Z>.Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(657);
  }
}
public class Bar<X, Y, Z> : IBar<X>, IBar<Y, Z>
{
  void IBar<X>.Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(658);
  }
  void IBar<Y, Z>.Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(659);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(660);
  }
}
