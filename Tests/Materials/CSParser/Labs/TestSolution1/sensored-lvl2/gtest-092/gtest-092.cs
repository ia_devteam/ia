using System;
public class Element<T>
{
  public readonly T Item;
  public Element(T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(622);
    this.Item = item;
    IACSharpSensor.IACSharpSensor.SensorReached(623);
  }
  public void GetItem(out T retval)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(624);
    retval = Item;
    IACSharpSensor.IACSharpSensor.SensorReached(625);
  }
  public T GetItem(int a, ref T data)
  {
    T RNTRNTRNT_97 = Item;
    IACSharpSensor.IACSharpSensor.SensorReached(626);
    return RNTRNTRNT_97;
  }
  public void SetItem(T data)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(627);
  }
}
public class Foo<T>
{
  Element<Node> element;
  public Node Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(628);
    Node node = element.Item;
    element.GetItem(out node);
    element.SetItem(node);
    Node RNTRNTRNT_98 = element.GetItem(3, ref node);
    IACSharpSensor.IACSharpSensor.SensorReached(629);
    return RNTRNTRNT_98;
  }
  public class Node
  {
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(630);
  }
}
