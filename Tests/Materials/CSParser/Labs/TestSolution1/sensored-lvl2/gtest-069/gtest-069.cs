public interface IFoo
{
  int GetHashCode();
}
public interface IFoo<T>
{
  int GetHashCode();
}
public class Test<T>
{
  public int Foo(IFoo<T> foo)
  {
    System.Int32 RNTRNTRNT_68 = foo.GetHashCode();
    IACSharpSensor.IACSharpSensor.SensorReached(491);
    return RNTRNTRNT_68;
  }
  public int Foo(IFoo foo)
  {
    System.Int32 RNTRNTRNT_69 = foo.GetHashCode();
    IACSharpSensor.IACSharpSensor.SensorReached(492);
    return RNTRNTRNT_69;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(493);
  }
}
