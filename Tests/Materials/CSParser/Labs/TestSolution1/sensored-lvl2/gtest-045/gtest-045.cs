class Test<A, B>
{
  public void Foo<U>(U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(313);
  }
  public void Foo<V>(V[] v, V w)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(314);
  }
  public void Hello<V, W>(V v, W w, Test<V, W> x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(315);
  }
  public void ArrayMethod<V>(params V[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(316);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(317);
    Test<float, int> test = new Test<float, int>();
    test.Foo("Hello World");
    test.Foo(new long[] {
      3,
      4,
      5
    }, 9L);
    test.Hello(3.14f, 9, test);
    test.ArrayMethod(3.14f, (float)9 / 3);
    IACSharpSensor.IACSharpSensor.SensorReached(318);
  }
}
