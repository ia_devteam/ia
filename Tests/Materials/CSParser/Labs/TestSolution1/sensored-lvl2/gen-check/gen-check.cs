using System;
class Stress
{
  static string[] types = {
    "int",
    "uint",
    "short",
    "ushort",
    "long",
    "ulong",
    "sbyte",
    "byte",
    "char"
  };
  static void w(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(32);
    Console.Write(s);
    IACSharpSensor.IACSharpSensor.SensorReached(33);
  }
  static void wl(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(34);
    Console.WriteLine(s);
    IACSharpSensor.IACSharpSensor.SensorReached(35);
  }
  static void generate_receptors()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(36);
    foreach (string t in types) {
      IACSharpSensor.IACSharpSensor.SensorReached(37);
      w("\tstatic void receive_" + t + " (" + t + " a)\n\t{\n");
      w("\t\tConsole.Write (\"        \");\n");
      w("\t\tConsole.WriteLine (a);\n");
      w("\t}\n\n");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(38);
  }
  static void call(string type, string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(39);
    w("\t\treceive_" + type + " (checked ((" + type + ") var ));\n");
    IACSharpSensor.IACSharpSensor.SensorReached(40);
  }
  static void generate_emision()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(41);
    foreach (string type in types) {
      IACSharpSensor.IACSharpSensor.SensorReached(42);
      w("\tstatic void probe_" + type + "()\n\t{\n");
      IACSharpSensor.IACSharpSensor.SensorReached(43);
      if (type == "char") {
        IACSharpSensor.IACSharpSensor.SensorReached(44);
        w("\t\t" + type + " var = (char) 0;");
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(45);
        w("\t\t" + type + " var = 0;");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(46);
      wl("");
      IACSharpSensor.IACSharpSensor.SensorReached(47);
      foreach (string t in types) {
        IACSharpSensor.IACSharpSensor.SensorReached(48);
        call(t, "var");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(49);
      w("\t}\n\n");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(50);
  }
  static void generate_main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(51);
    wl("\tstatic void Main ()\n\t{");
    IACSharpSensor.IACSharpSensor.SensorReached(52);
    foreach (string t in types) {
      IACSharpSensor.IACSharpSensor.SensorReached(53);
      w("\t\tprobe_" + t + " ();\n");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(54);
    wl("\t}");
    IACSharpSensor.IACSharpSensor.SensorReached(55);
  }
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(56);
    wl("using System;\nclass Test {\n");
    generate_receptors();
    generate_emision();
    generate_main();
    wl("}\n");
    IACSharpSensor.IACSharpSensor.SensorReached(57);
  }
}
