public class X
{
  public static void Foo(Stack stack)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(104);
    stack.Hello<string>("Hello World");
    IACSharpSensor.IACSharpSensor.SensorReached(105);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(106);
    Stack stack = new Stack();
    Foo(stack);
    IACSharpSensor.IACSharpSensor.SensorReached(107);
  }
}
