using System;
public class Log<T>
{
  private const int SIZE = 5;
  private static int instanceCount = 0;
  private int count = 0;
  private T[] log = new T[SIZE];
  public Log()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(406);
    instanceCount++;
    IACSharpSensor.IACSharpSensor.SensorReached(407);
  }
  public static int InstanceCount {
    get {
      System.Int32 RNTRNTRNT_46 = instanceCount;
      IACSharpSensor.IACSharpSensor.SensorReached(408);
      return RNTRNTRNT_46;
    }
  }
  public void Add(T msg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(409);
    log[count++ % SIZE] = msg;
    IACSharpSensor.IACSharpSensor.SensorReached(410);
  }
  public int Count {
    get {
      System.Int32 RNTRNTRNT_47 = count;
      IACSharpSensor.IACSharpSensor.SensorReached(411);
      return RNTRNTRNT_47;
    }
  }
  public T Last {
    get {
      T RNTRNTRNT_48 = count == 0 ? default(T) : log[(count - 1) % SIZE];
      IACSharpSensor.IACSharpSensor.SensorReached(412);
      return RNTRNTRNT_48;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(413);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(414);
        log[count++] = value;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(415);
        log[(count - 1) % SIZE] = value;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(416);
    }
  }
  public T[] All {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(417);
      int size = Math.Min(count, SIZE);
      T[] res = new T[size];
      IACSharpSensor.IACSharpSensor.SensorReached(418);
      for (int i = 0; i < size; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(419);
        res[i] = log[(count - size + i) % SIZE];
      }
      T[] RNTRNTRNT_49 = res;
      IACSharpSensor.IACSharpSensor.SensorReached(420);
      return RNTRNTRNT_49;
    }
  }
}
class TestLog
{
  class MyTest
  {
    public static void Main(String[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(421);
      Log<String> log1 = new Log<String>();
      log1.Add("Reboot");
      log1.Add("Coffee");
      Log<DateTime> log2 = new Log<DateTime>();
      log2.Add(DateTime.Now);
      log2.Add(DateTime.Now.AddHours(1));
      DateTime[] dts = log2.All;
      IACSharpSensor.IACSharpSensor.SensorReached(422);
      foreach (String s in log1.All) {
        IACSharpSensor.IACSharpSensor.SensorReached(423);
        Console.Write("{0}   ", s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(424);
      Console.WriteLine();
      IACSharpSensor.IACSharpSensor.SensorReached(425);
      foreach (DateTime dt in dts) {
        IACSharpSensor.IACSharpSensor.SensorReached(426);
        Console.Write("{0}   ", dt);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(427);
      Console.WriteLine();
      IACSharpSensor.IACSharpSensor.SensorReached(428);
    }
  }
}
