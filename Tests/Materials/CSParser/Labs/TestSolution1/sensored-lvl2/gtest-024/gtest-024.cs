class Foo<T>
{
  public Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(151);
  }
  public void Hello(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(152);
    Whatever(t);
    IACSharpSensor.IACSharpSensor.SensorReached(153);
  }
  public void Whatever(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(154);
    System.Console.WriteLine(o.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(155);
  }
}
class X
{
  static void Test(Foo<int> foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(156);
    foo.Hello(4);
    IACSharpSensor.IACSharpSensor.SensorReached(157);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(158);
    Foo<int> foo = new Foo<int>();
    Test(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(159);
  }
}
