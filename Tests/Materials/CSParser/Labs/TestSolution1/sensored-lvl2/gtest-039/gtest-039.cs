using System;
interface IMonkey<T>
{
  T Jump();
}
class Zoo<T>
{
  T t;
  public Zoo(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(234);
    this.t = t;
    IACSharpSensor.IACSharpSensor.SensorReached(235);
  }
  public T Name {
    get {
      T RNTRNTRNT_9 = t;
      IACSharpSensor.IACSharpSensor.SensorReached(236);
      return RNTRNTRNT_9;
    }
  }
  public IMonkey<U> GetTheMonkey<U>(U u)
  {
    IMonkey<U> RNTRNTRNT_10 = new Monkey<T, U>(this, u);
    IACSharpSensor.IACSharpSensor.SensorReached(237);
    return RNTRNTRNT_10;
  }
  public class Monkey<V, W> : IMonkey<W>
  {
    public readonly Zoo<V> Zoo;
    public readonly W Data;
    public Monkey(Zoo<V> zoo, W data)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(238);
      this.Zoo = zoo;
      this.Data = data;
      IACSharpSensor.IACSharpSensor.SensorReached(239);
    }
    public W Jump()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(240);
      Console.WriteLine("Monkey {0} from {1} jumping!", Data, Zoo.Name);
      W RNTRNTRNT_11 = Data;
      IACSharpSensor.IACSharpSensor.SensorReached(241);
      return RNTRNTRNT_11;
    }
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(242);
    Zoo<string> zoo = new Zoo<string>("Boston");
    IMonkey<float> monkey = zoo.GetTheMonkey<float>(3.14f);
    monkey.Jump();
    IACSharpSensor.IACSharpSensor.SensorReached(243);
  }
}
