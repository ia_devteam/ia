using System;
class Foo<T>
{
}
class Test
{
  static void Hello<T>(Foo<T>[] foo, int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(643);
    Foo<T> element = foo[0];
    Console.WriteLine(element);
    IACSharpSensor.IACSharpSensor.SensorReached(644);
    if (i > 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(645);
      Hello<T>(foo, i - 1);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(646);
  }
  public static void Quicksort<U>(Foo<U>[] arr)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(647);
    Hello<U>(arr, 1);
    IACSharpSensor.IACSharpSensor.SensorReached(648);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(649);
    Foo<int>[] foo = new Foo<int>[1];
    foo[0] = new Foo<int>();
    Quicksort(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(650);
  }
}
