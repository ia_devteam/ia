using System;
public struct KeyValuePair<K, V>
{
  public KeyValuePair(K k, V v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(608);
  }
  public KeyValuePair(K k)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(609);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(610);
    new KeyValuePair<int, long>();
    IACSharpSensor.IACSharpSensor.SensorReached(611);
  }
}
