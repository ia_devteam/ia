class Foo
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(434);
  }
}
class Foo<T>
{
  static Foo<T> x;
  static Foo<T> Blah {
    get {
      Foo<T> RNTRNTRNT_51 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(435);
      return RNTRNTRNT_51;
    }
  }
}
