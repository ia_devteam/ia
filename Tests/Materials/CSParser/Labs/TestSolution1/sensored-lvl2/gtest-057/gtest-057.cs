using System;
interface IHello<T>
{
  void Print(T t);
}
interface Foo
{
  IHello<U> Test<U>();
}
class Hello<T> : IHello<T>, Foo
{
  public void Print(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(429);
    Console.WriteLine("Hello: {0}", t);
    IACSharpSensor.IACSharpSensor.SensorReached(430);
  }
  public IHello<U> Test<U>()
  {
    IHello<U> RNTRNTRNT_50 = new Hello<U>();
    IACSharpSensor.IACSharpSensor.SensorReached(431);
    return RNTRNTRNT_50;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(432);
    Hello<int> hello = new Hello<int>();
    hello.Print(5);
    hello.Test<float>().Print(3.14f);
    IHello<string> foo = hello.Test<string>();
    foo.Print("World");
    IACSharpSensor.IACSharpSensor.SensorReached(433);
  }
}
