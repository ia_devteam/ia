using System;
public interface ICounter
{
  void Increment();
}
namespace ValueTypeCounters
{
  public struct SimpleCounter : ICounter
  {
    public int Value;
    public void Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(362);
      Value += 2;
      IACSharpSensor.IACSharpSensor.SensorReached(363);
    }
  }
  public struct PrintingCounter : ICounter
  {
    public int Value;
    public override string ToString()
    {
      System.String RNTRNTRNT_38 = Value.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(364);
      return RNTRNTRNT_38;
    }
    public void Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(365);
      Value += 2;
      IACSharpSensor.IACSharpSensor.SensorReached(366);
    }
  }
  public struct ExplicitCounter : ICounter
  {
    public int Value;
    public override string ToString()
    {
      System.String RNTRNTRNT_39 = Value.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(367);
      return RNTRNTRNT_39;
    }
    void ICounter.Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(368);
      Value++;
      IACSharpSensor.IACSharpSensor.SensorReached(369);
    }
  }
  public struct InterfaceCounter : ICounter
  {
    public int Value;
    public override string ToString()
    {
      System.String RNTRNTRNT_40 = Value.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(370);
      return RNTRNTRNT_40;
    }
    void ICounter.Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(371);
      Value++;
      IACSharpSensor.IACSharpSensor.SensorReached(372);
    }
    public void Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(373);
      Value += 2;
      IACSharpSensor.IACSharpSensor.SensorReached(374);
    }
  }
}
namespace ReferenceTypeCounters
{
  public class SimpleCounter : ICounter
  {
    public int Value;
    public void Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(375);
      Value += 2;
      IACSharpSensor.IACSharpSensor.SensorReached(376);
    }
  }
  public class PrintingCounter : ICounter
  {
    public int Value;
    public override string ToString()
    {
      System.String RNTRNTRNT_41 = Value.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(377);
      return RNTRNTRNT_41;
    }
    public void Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(378);
      Value += 2;
      IACSharpSensor.IACSharpSensor.SensorReached(379);
    }
  }
  public class ExplicitCounter : ICounter
  {
    public int Value;
    public override string ToString()
    {
      System.String RNTRNTRNT_42 = Value.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(380);
      return RNTRNTRNT_42;
    }
    void ICounter.Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(381);
      Value++;
      IACSharpSensor.IACSharpSensor.SensorReached(382);
    }
  }
  public class InterfaceCounter : ICounter
  {
    public int Value;
    public override string ToString()
    {
      System.String RNTRNTRNT_43 = Value.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(383);
      return RNTRNTRNT_43;
    }
    void ICounter.Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(384);
      Value++;
      IACSharpSensor.IACSharpSensor.SensorReached(385);
    }
    public void Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(386);
      Value += 2;
      IACSharpSensor.IACSharpSensor.SensorReached(387);
    }
  }
}
namespace Test
{
  using V = ValueTypeCounters;
  using R = ReferenceTypeCounters;
  public class Test<T> where T : ICounter
  {
    public static void Foo(T x)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(388);
      Console.WriteLine(x.ToString());
      x.Increment();
      Console.WriteLine(x.ToString());
      IACSharpSensor.IACSharpSensor.SensorReached(389);
    }
  }
  public class X
  {
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(390);
      Test<V.SimpleCounter>.Foo(new V.SimpleCounter());
      Test<V.PrintingCounter>.Foo(new V.PrintingCounter());
      Test<V.ExplicitCounter>.Foo(new V.ExplicitCounter());
      Test<V.InterfaceCounter>.Foo(new V.InterfaceCounter());
      Test<R.SimpleCounter>.Foo(new R.SimpleCounter());
      Test<R.PrintingCounter>.Foo(new R.PrintingCounter());
      Test<R.ExplicitCounter>.Foo(new R.ExplicitCounter());
      Test<R.InterfaceCounter>.Foo(new R.InterfaceCounter());
      IACSharpSensor.IACSharpSensor.SensorReached(391);
    }
  }
}
