class Stack<T>
{
  public void Hello(int a, params T[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(266);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(267);
    Stack<string> stack = new Stack<string>();
    stack.Hello(1, "Hello", "World");
    IACSharpSensor.IACSharpSensor.SensorReached(268);
  }
}
