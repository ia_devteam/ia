class Foo
{
  public Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(160);
  }
  public void Hello<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(161);
    Whatever(t);
    IACSharpSensor.IACSharpSensor.SensorReached(162);
  }
  public void Whatever(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(163);
    System.Console.WriteLine(o.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(164);
  }
}
class X
{
  static void Test(Foo foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(165);
    foo.Hello<int>(531);
    IACSharpSensor.IACSharpSensor.SensorReached(166);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(167);
    Foo foo = new Foo();
    Test(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(168);
  }
}
