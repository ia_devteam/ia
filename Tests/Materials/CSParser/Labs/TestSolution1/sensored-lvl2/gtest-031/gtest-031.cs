public class X
{
  public static void Test(Bar<int, string> bar)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(197);
    bar.Hello("Test");
    bar.Test(7, "Hello");
    IACSharpSensor.IACSharpSensor.SensorReached(198);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(199);
  }
}
