public interface IFoo
{
  void Test<T>();
  void Test<U, V>();
}
public class Foo : IFoo
{
  void IFoo<X>.Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(651);
  }
  void IFoo<Y, Z>.Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(652);
  }
}
public interface IBar<T>
{
  void Test();
}
public interface IBar<U, V>
{
  void Test();
}
public class Bar<X, Y, Z> : IBar<X>, IBar<Y, Z>
{
  void IBar<X>.Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(653);
  }
  void IBar<Y, Z>.Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(654);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(655);
  }
}
