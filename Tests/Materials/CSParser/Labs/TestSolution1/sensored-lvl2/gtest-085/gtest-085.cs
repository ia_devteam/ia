using System;
public interface IFoo<S>
{
}
public class ArrayList<T>
{
  public virtual int InsertAll(IFoo<T> foo)
  {
    System.Int32 RNTRNTRNT_87 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(595);
    return RNTRNTRNT_87;
  }
  public virtual int InsertAll<U>(IFoo<U> foo) where U : T
  {
    System.Int32 RNTRNTRNT_88 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(596);
    return RNTRNTRNT_88;
  }
  public virtual int AddAll(IFoo<T> foo)
  {
    System.Int32 RNTRNTRNT_89 = InsertAll(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(597);
    return RNTRNTRNT_89;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(598);
    ArrayList<int> list = new ArrayList<int>();
    System.Int32 RNTRNTRNT_90 = list.AddAll(null);
    IACSharpSensor.IACSharpSensor.SensorReached(599);
    return RNTRNTRNT_90;
  }
}
