class Foo<T>
{
  public void Hello()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(145);
  }
  public void World(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(146);
    Hello();
    IACSharpSensor.IACSharpSensor.SensorReached(147);
  }
}
class Bar : Foo<Bar>
{
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(148);
    Hello();
    World(this);
    IACSharpSensor.IACSharpSensor.SensorReached(149);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(150);
  }
}
