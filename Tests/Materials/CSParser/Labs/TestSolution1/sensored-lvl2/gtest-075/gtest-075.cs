public interface IExtensible<T>
{
  void AddAll<U>(U item) where U : T;
}
public class ArrayList<T> : IExtensible<T>
{
  void IExtensible<T>.AddAll(U item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(552);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(553);
  }
}
