using System;
public delegate B Test<A, B>(A a);
public class Foo<T>
{
  T t;
  public Foo(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(442);
    this.t = t;
    IACSharpSensor.IACSharpSensor.SensorReached(443);
  }
  public U Method<U>(Test<T, U> test)
  {
    U RNTRNTRNT_53 = test(t);
    IACSharpSensor.IACSharpSensor.SensorReached(444);
    return RNTRNTRNT_53;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(445);
    Test<double, int> test = new Test<double, int>(Math.Sign);
    Foo<double> foo = new Foo<double>(Math.PI);
    Console.WriteLine(foo.Method<int>(test));
    string s = foo.Method<string>(delegate(double d) { return "s" + d; });
    Console.WriteLine(s);
    IACSharpSensor.IACSharpSensor.SensorReached(446);
  }
}
