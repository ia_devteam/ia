public class Stack<S>
{
  public Stack(S s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(95);
  }
  public void Push(S s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(96);
  }
}
public class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(97);
    Stack<int> s1 = new Stack<int>(3);
    s1.Push(4);
    Stack<string> s2 = new Stack<string>("Hello");
    s2.Push("Test");
    IACSharpSensor.IACSharpSensor.SensorReached(98);
  }
}
