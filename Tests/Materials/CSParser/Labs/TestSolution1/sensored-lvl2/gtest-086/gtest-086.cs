public interface IFoo<S>
{
}
public class ArrayList<T>
{
  public virtual int InsertAll(IFoo<T> foo)
  {
    System.Int32 RNTRNTRNT_91 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(600);
    return RNTRNTRNT_91;
  }
  public virtual int InsertAll<U>(IFoo<U> foo) where U : T
  {
    System.Int32 RNTRNTRNT_92 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(601);
    return RNTRNTRNT_92;
  }
  public virtual int AddAll(IFoo<T> foo)
  {
    System.Int32 RNTRNTRNT_93 = InsertAll(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(602);
    return RNTRNTRNT_93;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(603);
  }
}
