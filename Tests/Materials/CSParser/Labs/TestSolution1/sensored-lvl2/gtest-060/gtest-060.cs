using System;
interface IFoo
{
  MyList<U> Map<U>();
}
class MyList<T>
{
  public void Hello(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(437);
    Console.WriteLine(t);
    IACSharpSensor.IACSharpSensor.SensorReached(438);
  }
}
class Foo : IFoo
{
  public MyList<T> Map<T>()
  {
    MyList<T> RNTRNTRNT_52 = new MyList<T>();
    IACSharpSensor.IACSharpSensor.SensorReached(439);
    return RNTRNTRNT_52;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(440);
    Foo foo = new Foo();
    MyList<int> list = foo.Map<int>();
    list.Hello(9);
    IACSharpSensor.IACSharpSensor.SensorReached(441);
  }
}
