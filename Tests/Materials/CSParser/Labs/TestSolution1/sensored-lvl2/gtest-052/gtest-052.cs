using System;
public class Foo<T> where T : new()
{
  public T Create()
  {
    T RNTRNTRNT_37 = new T();
    IACSharpSensor.IACSharpSensor.SensorReached(356);
    return RNTRNTRNT_37;
  }
}
class X
{
  public X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(357);
  }
  void Hello()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(358);
    Console.WriteLine("Hello World");
    IACSharpSensor.IACSharpSensor.SensorReached(359);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(360);
    Foo<X> foo = new Foo<X>();
    foo.Create().Hello();
    IACSharpSensor.IACSharpSensor.SensorReached(361);
  }
}
