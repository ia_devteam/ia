namespace C5
{
  public class HashedArrayList<T>
  {
    public void Test()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(604);
      new HashSet<KeyValuePair<T, int>>(new KeyValuePairHasher<T, int>());
      IACSharpSensor.IACSharpSensor.SensorReached(605);
    }
  }
  public class HashSet<T>
  {
    public HashSet(IHasher<T> itemhasher)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(606);
    }
  }
  public interface IHasher<T>
  {
  }
  public struct KeyValuePair<K, V>
  {
  }
  public sealed class KeyValuePairHasher<K, V> : IHasher<KeyValuePair<K, V>>
  {
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(607);
  }
}
