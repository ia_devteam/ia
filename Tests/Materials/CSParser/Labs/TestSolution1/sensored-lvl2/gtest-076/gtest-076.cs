using System;
struct Foo<T>
{
  public T Data;
  public Foo(T data)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(554);
    this.Data = data;
    IACSharpSensor.IACSharpSensor.SensorReached(555);
  }
}
class Test<T>
{
  public Foo<T> GetFoo(T data)
  {
    Foo<T> RNTRNTRNT_82 = new Foo<T>(data);
    IACSharpSensor.IACSharpSensor.SensorReached(556);
    return RNTRNTRNT_82;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(557);
    Test<long> test = new Test<long>();
    Foo<long> foo = test.GetFoo(0x800);
    IACSharpSensor.IACSharpSensor.SensorReached(558);
    if (foo.Data != 0x800) {
      System.Int32 RNTRNTRNT_83 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(559);
      return RNTRNTRNT_83;
    }
    System.Int32 RNTRNTRNT_84 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(560);
    return RNTRNTRNT_84;
  }
}
