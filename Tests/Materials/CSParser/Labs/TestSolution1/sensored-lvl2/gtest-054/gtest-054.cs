using System;
interface Foo
{
  void Hello();
}
class A
{
}
class B : A, Foo
{
  public void Hello()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(392);
  }
  public static implicit operator C(B b)
  {
    C RNTRNTRNT_44 = new C();
    IACSharpSensor.IACSharpSensor.SensorReached(393);
    return RNTRNTRNT_44;
  }
}
class C
{
  public static explicit operator B(C c)
  {
    B RNTRNTRNT_45 = new B();
    IACSharpSensor.IACSharpSensor.SensorReached(394);
    return RNTRNTRNT_45;
  }
}
class Test
{
  static void Simple<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(395);
    object o = t;
    t = (T)o;
    Foo foo = (Foo)t;
    t = (T)foo;
    IACSharpSensor.IACSharpSensor.SensorReached(396);
  }
  static void Interface<T>(T t) where T : Foo
  {
    IACSharpSensor.IACSharpSensor.SensorReached(397);
    Foo foo = t;
    IACSharpSensor.IACSharpSensor.SensorReached(398);
  }
  static void Class<T>(T t) where T : B
  {
    IACSharpSensor.IACSharpSensor.SensorReached(399);
    B b = t;
    A a = t;
    Foo foo = t;
    t = (T)b;
    t = (T)a;
    t = (T)foo;
    C c = t;
    t = (T)c;
    IACSharpSensor.IACSharpSensor.SensorReached(400);
  }
  static void Array<T>(T[] t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(401);
    object o = t;
    Array a = t;
    t = (T[])o;
    t = (T[])a;
    IACSharpSensor.IACSharpSensor.SensorReached(402);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(403);
  }
}
