public interface IFoo<X>
{
}
public class Test
{
  public void Hello<T>(IFoo<T> foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(578);
    InsertAll(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(579);
  }
  public void InsertAll<U>(IFoo<U> foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(580);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(581);
  }
}
