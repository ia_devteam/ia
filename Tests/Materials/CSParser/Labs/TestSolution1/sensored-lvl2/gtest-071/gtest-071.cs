using System;
class Foo<T>
{
  public T Test<U>(U u) where U : T
  {
    T RNTRNTRNT_71 = u;
    IACSharpSensor.IACSharpSensor.SensorReached(498);
    return RNTRNTRNT_71;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(499);
    Foo<X> foo = new Foo<X>();
    Y y = new Y();
    X x = foo.Test<Y>(y);
    IACSharpSensor.IACSharpSensor.SensorReached(500);
  }
}
class Y : X
{
}
