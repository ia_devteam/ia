using System;
class Test<T>
{
  public void Foo(T t, out int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(612);
    a = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(613);
  }
  public void Hello(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(614);
    int a;
    Foo(t, out a);
    IACSharpSensor.IACSharpSensor.SensorReached(615);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(616);
  }
}
