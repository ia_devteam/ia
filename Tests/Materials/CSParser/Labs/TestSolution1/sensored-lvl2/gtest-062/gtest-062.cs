using System.Collections.Generic;
class X
{
  public IEnumerable<int> Test(int a, long b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(447);
    while (a < b) {
      IACSharpSensor.IACSharpSensor.SensorReached(448);
      a++;
      System.Int32 RNTRNTRNT_54 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(449);
      yield return RNTRNTRNT_54;
      IACSharpSensor.IACSharpSensor.SensorReached(450);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(451);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(452);
    X x = new X();
    int sum = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(453);
    foreach (int i in x.Test(3, 8L)) {
      IACSharpSensor.IACSharpSensor.SensorReached(454);
      sum += i;
    }
    System.Int32 RNTRNTRNT_55 = sum == 30 ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(455);
    return RNTRNTRNT_55;
  }
}
