using System;
using System.Collections;
using System.Collections.Generic;
class MyList<T> : IEnumerable<T>
{
  public IEnumerator<T> GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(530);
    yield break;
  }
  IEnumerator IEnumerable.GetEnumerator()
  {
    IEnumerator RNTRNTRNT_76 = GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(531);
    return RNTRNTRNT_76;
  }
}
struct Foo<T>
{
  public readonly T Data;
  public Foo(T data)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(532);
    this.Data = data;
    IACSharpSensor.IACSharpSensor.SensorReached(533);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(534);
    MyList<Foo<int>> list = new MyList<Foo<int>>();
    IACSharpSensor.IACSharpSensor.SensorReached(535);
    foreach (Foo<int> foo in list) {
      ;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(536);
  }
}
