using System;
class Foo<T>
{
  T[] t;
  public Foo(int n)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(121);
    t = new T[n];
    IACSharpSensor.IACSharpSensor.SensorReached(122);
    for (int i = 0; i < n; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(123);
      t[i] = default(T);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(124);
  }
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(125);
    X.Print(t[0]);
    IACSharpSensor.IACSharpSensor.SensorReached(126);
  }
}
class Bar<T>
{
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(127);
    X.Print(default(X));
    X.Print(default(T));
    X.Print(default(S));
    IACSharpSensor.IACSharpSensor.SensorReached(128);
  }
}
struct S
{
  public readonly string Hello;
  S(string hello)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(129);
    this.Hello = hello;
    IACSharpSensor.IACSharpSensor.SensorReached(130);
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_3 = String.Format("S({0})", Hello);
    IACSharpSensor.IACSharpSensor.SensorReached(131);
    return RNTRNTRNT_3;
  }
}
class X
{
  public static void Print(object obj)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(132);
    if (obj == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(133);
      Console.WriteLine("NULL");
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(134);
      Console.WriteLine("OBJECT: {0} {1}", obj, obj.GetType());
    }
    IACSharpSensor.IACSharpSensor.SensorReached(135);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(136);
    Foo<string> a = new Foo<string>(4);
    a.Test();
    Bar<int> b = new Bar<int>();
    b.Test();
    Bar<X> c = new Bar<X>();
    c.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(137);
  }
}
