class TheBase
{
  public void BaseFunc()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(172);
  }
}
class Stack<S> : TheBase
{
  public void Hello(S s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(173);
  }
}
class Test<T> : Stack<T>
{
  public void Foo(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(174);
  }
}
class X
{
  Test<int> test;
  void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(175);
    test.Foo(4);
    test.Hello(3);
    test.BaseFunc();
    IACSharpSensor.IACSharpSensor.SensorReached(176);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(177);
  }
}
