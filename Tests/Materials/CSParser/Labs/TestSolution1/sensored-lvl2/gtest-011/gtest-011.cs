class Stack<S>
{
  public void Hello(S s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(82);
  }
}
class X
{
  Stack<int> stack;
  void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(83);
    stack.Hello(3);
    IACSharpSensor.IACSharpSensor.SensorReached(84);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(85);
  }
}
