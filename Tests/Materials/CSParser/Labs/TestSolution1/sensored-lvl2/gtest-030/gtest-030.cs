class Foo<T>
{
  public Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(189);
  }
  public void Hello(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(190);
  }
}
class Bar<T, U> : Foo<U>
{
  public Bar()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(191);
  }
  public void Test(T t, U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(192);
  }
}
class X
{
  static void Test(Bar<int, string> bar)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(193);
    bar.Hello("Test");
    bar.Test(7, "Hello");
    IACSharpSensor.IACSharpSensor.SensorReached(194);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(195);
    Bar<int, string> bar = new Bar<int, string>();
    Test(bar);
    IACSharpSensor.IACSharpSensor.SensorReached(196);
  }
}
