public class Foo<T, U>
{
  public void Hello(Foo<T, U> foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(214);
  }
  public virtual void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    Hello(this);
    IACSharpSensor.IACSharpSensor.SensorReached(216);
  }
}
public class Bar<T> : Foo<T, long>
{
  public void Test(Foo<T, long> foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(217);
    Hello(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(218);
  }
}
public class Baz<T> : Foo<T, string>
{
  public override void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(219);
    Hello(this);
    IACSharpSensor.IACSharpSensor.SensorReached(220);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(221);
  }
}
