class Stack<T>
{
  T t;
  public Stack(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(178);
    this.t = t;
    IACSharpSensor.IACSharpSensor.SensorReached(179);
  }
  public object Test()
  {
    System.Object RNTRNTRNT_4 = t;
    IACSharpSensor.IACSharpSensor.SensorReached(180);
    return RNTRNTRNT_4;
  }
}
class X
{
  public static object Test(Stack<int> stack)
  {
    System.Object RNTRNTRNT_5 = stack.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(181);
    return RNTRNTRNT_5;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(182);
    Stack<int> stack = new Stack<int>(9);
    System.Console.WriteLine(Test(stack));
    IACSharpSensor.IACSharpSensor.SensorReached(183);
  }
}
