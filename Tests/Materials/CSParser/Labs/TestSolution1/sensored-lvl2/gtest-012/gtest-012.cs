class Stack<S>
{
  public void Hello(S s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(86);
  }
}
class Test<T> : Stack<T>
{
  public void Foo(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(87);
  }
}
class X
{
  Test<int> test;
  void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(88);
    test.Foo(4);
    test.Hello(3);
    IACSharpSensor.IACSharpSensor.SensorReached(89);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(90);
  }
}
