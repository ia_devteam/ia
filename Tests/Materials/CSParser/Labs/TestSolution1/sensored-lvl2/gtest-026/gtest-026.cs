class Foo<T>
{
  public T Hello;
  public Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(169);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    Foo<int> foo = new Foo<int>();
    foo.Hello = 9;
    IACSharpSensor.IACSharpSensor.SensorReached(171);
  }
}
