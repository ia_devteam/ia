using System;
class Stress
{
  static string mode = "unchecked";
  static string[] types = {
    "int",
    "uint",
    "short",
    "ushort",
    "long",
    "ulong",
    "sbyte",
    "byte",
    "char"
  };
  static void w(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
    Console.Write(s);
    IACSharpSensor.IACSharpSensor.SensorReached(2);
  }
  static void wl(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(3);
    Console.WriteLine(s);
    IACSharpSensor.IACSharpSensor.SensorReached(4);
  }
  static void generate_receptors()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(5);
    foreach (string t in types) {
      IACSharpSensor.IACSharpSensor.SensorReached(6);
      w("\tstatic void receive_" + t + " (" + t + " a)\n\t{\n");
      w("\t\tConsole.Write (\"        \");\n");
      w("\t\tConsole.WriteLine (a);\n");
      w("\t}\n\n");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(7);
  }
  static void var(string type, string name, string init)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(8);
    w("\t\t" + type + " " + name + " = (" + type + ") " + init + ";\n");
    IACSharpSensor.IACSharpSensor.SensorReached(9);
  }
  static void call(string type, string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(10);
    w("\t\treceive_" + type + " (" + mode + "((" + type + ") " + name + "));\n");
    IACSharpSensor.IACSharpSensor.SensorReached(11);
  }
  static void generate_emision()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(12);
    foreach (string type in types) {
      IACSharpSensor.IACSharpSensor.SensorReached(13);
      w("\tstatic void probe_" + type + "()\n\t{\n");
      var(type, "zero", "0");
      var(type, "min", type + ".MinValue");
      var(type, "max", type + ".MaxValue");
      wl("");
      wl("\t\tConsole.WriteLine (\"Testing: " + type + "\");\n");
      IACSharpSensor.IACSharpSensor.SensorReached(14);
      foreach (string t in types) {
        IACSharpSensor.IACSharpSensor.SensorReached(15);
        wl("\t\tConsole.WriteLine (\"   arg: " + t + " (" + type + ")\");\n");
        call(t, "zero");
        call(t, "min");
        call(t, "max");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(16);
      w("\t}\n\n");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(17);
  }
  static void generate_main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(18);
    wl("\tstatic void Main ()\n\t{");
    IACSharpSensor.IACSharpSensor.SensorReached(19);
    foreach (string t in types) {
      IACSharpSensor.IACSharpSensor.SensorReached(20);
      w("\t\tprobe_" + t + " ();\n");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(21);
    wl("\t}");
    IACSharpSensor.IACSharpSensor.SensorReached(22);
  }
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(23);
    foreach (string arg in args) {
      IACSharpSensor.IACSharpSensor.SensorReached(24);
      if (arg == "-h" || arg == "--help") {
        IACSharpSensor.IACSharpSensor.SensorReached(25);
        Console.WriteLine("-h, --help     Shows help");
        Console.WriteLine("-c, --checked  Generate checked contexts");
        IACSharpSensor.IACSharpSensor.SensorReached(26);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(27);
      if (arg == "--checked" || arg == "-c") {
        IACSharpSensor.IACSharpSensor.SensorReached(28);
        mode = "checked";
        IACSharpSensor.IACSharpSensor.SensorReached(29);
        continue;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(30);
    wl("using System;\nclass Test {\n");
    generate_receptors();
    generate_emision();
    generate_main();
    wl("}\n");
    IACSharpSensor.IACSharpSensor.SensorReached(31);
  }
}
