using System;
public interface IDirectedEnumerable<T>
{
  IDirectedEnumerable<T> Backwards();
}
public interface IDirectedCollectionValue<T> : IDirectedEnumerable<T>
{
  new IDirectedCollectionValue<T> Backwards();
}
public class GuardedCollectionValue<T> : IDirectedCollectionValue<T>
{
  IDirectedEnumerable<T> IDirectedEnumerable<T>.Backwards()
  {
    IDirectedEnumerable<T> RNTRNTRNT_99 = this;
    IACSharpSensor.IACSharpSensor.SensorReached(639);
    return RNTRNTRNT_99;
  }
  public IDirectedCollectionValue<T> Backwards()
  {
    IDirectedCollectionValue<T> RNTRNTRNT_100 = this;
    IACSharpSensor.IACSharpSensor.SensorReached(640);
    return RNTRNTRNT_100;
  }
}
public interface ISequenced<T> : IDirectedCollectionValue<T>
{
}
public class GuardedSequenced<T>
{
  ISequenced<T> sequenced;
  public IDirectedCollectionValue<T> Test()
  {
    IDirectedCollectionValue<T> RNTRNTRNT_101 = sequenced.Backwards();
    IACSharpSensor.IACSharpSensor.SensorReached(641);
    return RNTRNTRNT_101;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(642);
  }
}
