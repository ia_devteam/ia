using System;
public struct Foo<T>
{
  public T Data, Data2;
  public Foo(T a, T b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(537);
    this.Data = a;
    this.Data2 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(538);
  }
}
public class Test<T>
{
  public T Data, Data2;
  public Test(T a, T b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(539);
    this.Data = a;
    this.Data2 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(540);
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(541);
    Foo<long> foo = new Foo<long>(3, 5);
    IACSharpSensor.IACSharpSensor.SensorReached(542);
    if (foo.Data != 3) {
      System.Int32 RNTRNTRNT_77 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(543);
      return RNTRNTRNT_77;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(544);
    if (foo.Data2 != 5) {
      System.Int32 RNTRNTRNT_78 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(545);
      return RNTRNTRNT_78;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(546);
    Test<long> test = new Test<long>(3, 5);
    IACSharpSensor.IACSharpSensor.SensorReached(547);
    if (test.Data != 3) {
      System.Int32 RNTRNTRNT_79 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(548);
      return RNTRNTRNT_79;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(549);
    if (test.Data2 != 5) {
      System.Int32 RNTRNTRNT_80 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(550);
      return RNTRNTRNT_80;
    }
    System.Int32 RNTRNTRNT_81 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(551);
    return RNTRNTRNT_81;
  }
}
