namespace A
{
  public interface IExtensible<T>
  {
    void AddAll<U>(U u) where U : T;
  }
  public class ArrayList<T> : IExtensible<T>
  {
    void IExtensible<T>.AddAll(U u)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(568);
      InsertAll(u);
      IACSharpSensor.IACSharpSensor.SensorReached(569);
    }
    void InsertAll(T t)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(570);
    }
  }
}
namespace B
{
  public interface IExtensible<S, T>
  {
    void AddAll<U>(U t) where U : S;
  }
  public class ArrayList<X, Y> : IExtensible<Y, X>
  {
    public void AddAll<Z>(Z z) where Z : Y
    {
      IACSharpSensor.IACSharpSensor.SensorReached(571);
      InsertAll(z);
      IACSharpSensor.IACSharpSensor.SensorReached(572);
    }
    void InsertAll(Y y)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(573);
    }
  }
}
namespace C
{
  public interface IExtensible<S>
  {
    void AddAll<T>(T t) where T : S;
  }
  public class Foo<U>
  {
  }
  public class ArrayList<X> : IExtensible<Foo<X>>
  {
    public void AddAll<Y>(Y y) where Y : Foo<X>
    {
      IACSharpSensor.IACSharpSensor.SensorReached(574);
      InsertAll(y);
      IACSharpSensor.IACSharpSensor.SensorReached(575);
    }
    void InsertAll(Foo<X> foo)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(576);
    }
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(577);
  }
}
