class X<T>
{
  void A(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(334);
  }
  void A(T[] t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(335);
  }
  void A(T[,] t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(336);
  }
  void A(T[][] t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(337);
  }
  void B(T[] t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(338);
  }
  void B(int t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(339);
  }
  void C(T[] t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(340);
  }
  void C(T[,] t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(341);
  }
  void C(int[,,] t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(342);
  }
  void D(int x, T y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(343);
  }
  void D(T x, long y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(344);
  }
}
class Foo
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(345);
  }
}
