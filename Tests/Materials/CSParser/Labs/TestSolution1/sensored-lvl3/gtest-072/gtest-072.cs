using System;
public class Log<T>
{
  private const int SIZE = 5;
  private static int instanceCount = 0;
  private int count = 0;
  private T[] log = new T[SIZE];
  public Log()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(350);
    instanceCount++;
    IACSharpSensor.IACSharpSensor.SensorReached(351);
  }
  public static int InstanceCount {
    get {
      System.Int32 RNTRNTRNT_72 = instanceCount;
      IACSharpSensor.IACSharpSensor.SensorReached(352);
      return RNTRNTRNT_72;
    }
  }
  public void Add(T msg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(353);
    log[count++ % SIZE] = msg;
    IACSharpSensor.IACSharpSensor.SensorReached(354);
  }
  public int Count {
    get {
      System.Int32 RNTRNTRNT_73 = count;
      IACSharpSensor.IACSharpSensor.SensorReached(355);
      return RNTRNTRNT_73;
    }
  }
  public T Last {
    get {
      T RNTRNTRNT_74 = count == 0 ? default(T) : log[(count - 1) % SIZE];
      IACSharpSensor.IACSharpSensor.SensorReached(356);
      return RNTRNTRNT_74;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(357);
      if (count == 0) {
        log[count++] = value;
      } else {
        log[(count - 1) % SIZE] = value;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(358);
    }
  }
  public T[] All {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(359);
      int size = Math.Min(count, SIZE);
      T[] res = new T[size];
      for (int i = 0; i < size; i++) {
        res[i] = log[(count - size + i) % SIZE];
      }
      T[] RNTRNTRNT_75 = res;
      IACSharpSensor.IACSharpSensor.SensorReached(360);
      return RNTRNTRNT_75;
    }
  }
}
class TestLog
{
  class MyTest
  {
    public static void Main(String[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(361);
      Log<String> log1 = new Log<String>();
      log1.Add("Reboot");
      log1.Add("Coffee");
      Log<DateTime> log2 = new Log<DateTime>();
      log2.Add(DateTime.Now);
      log2.Add(DateTime.Now.AddHours(1));
      DateTime[] dts = log2.All;
      foreach (String s in log1.All) {
        Console.Write("{0}   ", s);
      }
      Console.WriteLine();
      foreach (DateTime dt in dts) {
        Console.Write("{0}   ", dt);
      }
      Console.WriteLine();
      TestPairLog();
      IACSharpSensor.IACSharpSensor.SensorReached(362);
    }
    public static void TestPairLog()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(363);
      Log<Pair<DateTime, String>> log = new Log<Pair<DateTime, String>>();
      log.Add(new Pair<DateTime, String>(DateTime.Now, "Tea leaves"));
      log.Add(new Pair<DateTime, String>(DateTime.Now.AddMinutes(2), "Hot water"));
      log.Add(new Pair<DateTime, String>(DateTime.Now.AddMinutes(7), "Ready"));
      Pair<DateTime, String>[] allMsgs = log.All;
      foreach (Pair<DateTime, String> p in allMsgs) {
        Console.WriteLine("At {0}: {1}", p.Fst, p.Snd);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(364);
    }
  }
}
public struct Pair<T, U>
{
  public readonly T Fst;
  public readonly U Snd;
  public Pair(T fst, U snd)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(365);
    this.Fst = fst;
    this.Snd = snd;
    IACSharpSensor.IACSharpSensor.SensorReached(366);
  }
}
