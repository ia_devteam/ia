public interface IFoo<X>
{
}
public class Test
{
  public void Hello<T>(IFoo<T> foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(401);
    InsertAll(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(402);
  }
  public void InsertAll<U>(IFoo<U> foo)
  {
  }
}
class X
{
  static void Main()
  {
  }
}
