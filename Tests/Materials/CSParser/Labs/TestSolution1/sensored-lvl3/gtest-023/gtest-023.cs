class Foo<T>
{
  public void Hello()
  {
  }
  public void World(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(85);
    Hello();
    IACSharpSensor.IACSharpSensor.SensorReached(86);
  }
}
class Bar : Foo<Bar>
{
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(87);
    Hello();
    World(this);
    IACSharpSensor.IACSharpSensor.SensorReached(88);
  }
}
class X
{
  static void Main()
  {
  }
}
