public class Foo<T, U>
{
  public void Hello(Foo<T, U> foo)
  {
  }
  public virtual void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(132);
    Hello(this);
    IACSharpSensor.IACSharpSensor.SensorReached(133);
  }
}
public class Bar<T> : Foo<T, long>
{
  public void Test(Foo<T, long> foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(134);
    Hello(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(135);
  }
}
public class Baz<T> : Foo<T, string>
{
  public override void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(136);
    Hello(this);
    IACSharpSensor.IACSharpSensor.SensorReached(137);
  }
}
class X
{
  static void Main()
  {
  }
}
