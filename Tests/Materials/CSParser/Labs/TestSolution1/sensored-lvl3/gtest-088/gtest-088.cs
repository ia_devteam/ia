using System;
public struct KeyValuePair<K, V>
{
  public KeyValuePair(K k, V v)
  {
  }
  public KeyValuePair(K k)
  {
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(421);
    new KeyValuePair<int, long>();
    IACSharpSensor.IACSharpSensor.SensorReached(422);
  }
}
