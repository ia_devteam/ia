public class Stack
{
  public Stack()
  {
  }
  public void Hello<T>(T t)
  {
  }
}
public class X
{
  public static void Foo(Stack stack)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(60);
    stack.Hello<string>("Hello World");
    IACSharpSensor.IACSharpSensor.SensorReached(61);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    Stack stack = new Stack();
    Foo(stack);
    IACSharpSensor.IACSharpSensor.SensorReached(63);
  }
}
