using System;
public struct Foo<T>
{
  public T Data, Data2;
  public Foo(T a, T b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(373);
    this.Data = a;
    this.Data2 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(374);
  }
}
public class Test<T>
{
  public T Data, Data2;
  public Test(T a, T b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(375);
    this.Data = a;
    this.Data2 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(376);
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(377);
    Foo<long> foo = new Foo<long>(3, 5);
    if (foo.Data != 3) {
      System.Int32 RNTRNTRNT_77 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(378);
      return RNTRNTRNT_77;
    }
    if (foo.Data2 != 5) {
      System.Int32 RNTRNTRNT_78 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(379);
      return RNTRNTRNT_78;
    }
    Test<long> test = new Test<long>(3, 5);
    if (test.Data != 3) {
      System.Int32 RNTRNTRNT_79 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(380);
      return RNTRNTRNT_79;
    }
    if (test.Data2 != 5) {
      System.Int32 RNTRNTRNT_80 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(381);
      return RNTRNTRNT_80;
    }
    System.Int32 RNTRNTRNT_81 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(382);
    return RNTRNTRNT_81;
  }
}
