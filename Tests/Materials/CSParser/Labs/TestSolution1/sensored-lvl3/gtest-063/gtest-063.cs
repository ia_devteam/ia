using System;
public class Test
{
  public static int IndexOf(Array array, object value)
  {
    System.Int32 RNTRNTRNT_56 = IndexOf(array, value, 0, array.Length);
    IACSharpSensor.IACSharpSensor.SensorReached(316);
    return RNTRNTRNT_56;
  }
  public static int IndexOf(Array array, object value, int startIndex, int count)
  {
    System.Int32 RNTRNTRNT_57 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(317);
    return RNTRNTRNT_57;
  }
  public static int IndexOf<T>(T[] array, T value, int startIndex, int count)
  {
    System.Int32 RNTRNTRNT_58 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(318);
    return RNTRNTRNT_58;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(319);
    Test test = new Test();
    string[] array = new string[] { "Hello" };
    int result = Test.IndexOf(array, array);
    if (result != 2) {
      System.Int32 RNTRNTRNT_59 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(320);
      return RNTRNTRNT_59;
    }
    string hello = "Hello World";
    result = Test.IndexOf(array, hello, 1, 2);
    if (result != 1) {
      System.Int32 RNTRNTRNT_60 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(321);
      return RNTRNTRNT_60;
    }
    System.Int32 RNTRNTRNT_61 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(322);
    return RNTRNTRNT_61;
  }
}
