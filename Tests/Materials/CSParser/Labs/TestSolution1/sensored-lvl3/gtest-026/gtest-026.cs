class Foo<T>
{
  public T Hello;
  public Foo()
  {
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(105);
    Foo<int> foo = new Foo<int>();
    foo.Hello = 9;
    IACSharpSensor.IACSharpSensor.SensorReached(106);
  }
}
