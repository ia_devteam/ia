class Foo<T>
{
  public Foo()
  {
  }
  public void Hello(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(89);
    Whatever(t);
    IACSharpSensor.IACSharpSensor.SensorReached(90);
  }
  public void Whatever(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(91);
    System.Console.WriteLine(o.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(92);
  }
}
class X
{
  static void Test(Foo<int> foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(93);
    foo.Hello(4);
    IACSharpSensor.IACSharpSensor.SensorReached(94);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(95);
    Foo<int> foo = new Foo<int>();
    Test(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(96);
  }
}
