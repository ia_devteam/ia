using System;
class Queue<T>
{
  public Queue(T first, T second)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(138);
    head = new Node<T>(null, second);
    head = new Node<T>(head, first);
    IACSharpSensor.IACSharpSensor.SensorReached(139);
  }
  protected Node<T> head;
  protected Node<T> GetFoo()
  {
    Node<T> RNTRNTRNT_7 = head;
    IACSharpSensor.IACSharpSensor.SensorReached(140);
    return RNTRNTRNT_7;
  }
  protected Node<T> Foo {
    get {
      Node<T> RNTRNTRNT_8 = GetFoo();
      IACSharpSensor.IACSharpSensor.SensorReached(141);
      return RNTRNTRNT_8;
    }
  }
  protected void Test(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(142);
    Console.WriteLine(t);
    IACSharpSensor.IACSharpSensor.SensorReached(143);
  }
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(144);
    Test(head.Item);
    Test(head.Next.Item);
    Test(GetFoo().Item);
    Test(Foo.Item);
    IACSharpSensor.IACSharpSensor.SensorReached(145);
  }
  protected class Node<U>
  {
    public readonly U Item;
    public readonly Node<U> Next;
    public Node(Node<U> next, U item)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(146);
      this.Next = next;
      this.Item = item;
      IACSharpSensor.IACSharpSensor.SensorReached(147);
    }
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(148);
    Queue<int> queue = new Queue<int>(5, 9);
    queue.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(149);
  }
}
