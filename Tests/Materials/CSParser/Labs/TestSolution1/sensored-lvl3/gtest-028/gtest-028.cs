class Stack<T>
{
  T t;
  public Stack(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(109);
    this.t = t;
    IACSharpSensor.IACSharpSensor.SensorReached(110);
  }
  public object Test()
  {
    System.Object RNTRNTRNT_4 = t;
    IACSharpSensor.IACSharpSensor.SensorReached(111);
    return RNTRNTRNT_4;
  }
}
class X
{
  public static object Test(Stack<int> stack)
  {
    System.Object RNTRNTRNT_5 = stack.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(112);
    return RNTRNTRNT_5;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(113);
    Stack<int> stack = new Stack<int>(9);
    System.Console.WriteLine(Test(stack));
    IACSharpSensor.IACSharpSensor.SensorReached(114);
  }
}
