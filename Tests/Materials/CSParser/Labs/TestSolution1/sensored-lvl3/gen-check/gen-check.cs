using System;
class Stress
{
  static string[] types = {
    "int",
    "uint",
    "short",
    "ushort",
    "long",
    "ulong",
    "sbyte",
    "byte",
    "char"
  };
  static void w(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(18);
    Console.Write(s);
    IACSharpSensor.IACSharpSensor.SensorReached(19);
  }
  static void wl(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(20);
    Console.WriteLine(s);
    IACSharpSensor.IACSharpSensor.SensorReached(21);
  }
  static void generate_receptors()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(22);
    foreach (string t in types) {
      w("\tstatic void receive_" + t + " (" + t + " a)\n\t{\n");
      w("\t\tConsole.Write (\"        \");\n");
      w("\t\tConsole.WriteLine (a);\n");
      w("\t}\n\n");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(23);
  }
  static void call(string type, string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(24);
    w("\t\treceive_" + type + " (checked ((" + type + ") var ));\n");
    IACSharpSensor.IACSharpSensor.SensorReached(25);
  }
  static void generate_emision()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(26);
    foreach (string type in types) {
      w("\tstatic void probe_" + type + "()\n\t{\n");
      if (type == "char") {
        w("\t\t" + type + " var = (char) 0;");
      } else {
        w("\t\t" + type + " var = 0;");
      }
      wl("");
      foreach (string t in types) {
        call(t, "var");
      }
      w("\t}\n\n");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(27);
  }
  static void generate_main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(28);
    wl("\tstatic void Main ()\n\t{");
    foreach (string t in types) {
      w("\t\tprobe_" + t + " ();\n");
    }
    wl("\t}");
    IACSharpSensor.IACSharpSensor.SensorReached(29);
  }
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(30);
    wl("using System;\nclass Test {\n");
    generate_receptors();
    generate_emision();
    generate_main();
    wl("}\n");
    IACSharpSensor.IACSharpSensor.SensorReached(31);
  }
}
