using System;
public class Foo<T> where T : A
{
  public void Test(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(228);
    Console.WriteLine(t);
    Console.WriteLine(t.GetType());
    t.Hello();
    IACSharpSensor.IACSharpSensor.SensorReached(229);
  }
}
public class A
{
  public void Hello()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(230);
    Console.WriteLine("Hello World");
    IACSharpSensor.IACSharpSensor.SensorReached(231);
  }
}
public class B : A
{
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(232);
    Foo<B> foo = new Foo<B>();
    foo.Test(new B());
    IACSharpSensor.IACSharpSensor.SensorReached(233);
  }
}
