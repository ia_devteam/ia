interface Foo<R, S>
{
  void Hello(R r, S s);
}
interface Bar<T, U, V> : Foo<V, float>
{
  void Test(T t, U u, V v);
}
class X
{
  static void Test(Bar<long, int, string> bar)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(128);
    bar.Hello("Test", 3.14f);
    bar.Test(512, 7, "Hello");
    IACSharpSensor.IACSharpSensor.SensorReached(129);
  }
  static void Main()
  {
  }
}
