using System;
class Stress
{
  static string mode = "unchecked";
  static string[] types = {
    "int",
    "uint",
    "short",
    "ushort",
    "long",
    "ulong",
    "sbyte",
    "byte",
    "char"
  };
  static void w(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
    Console.Write(s);
    IACSharpSensor.IACSharpSensor.SensorReached(2);
  }
  static void wl(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(3);
    Console.WriteLine(s);
    IACSharpSensor.IACSharpSensor.SensorReached(4);
  }
  static void generate_receptors()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(5);
    foreach (string t in types) {
      w("\tstatic void receive_" + t + " (" + t + " a)\n\t{\n");
      w("\t\tConsole.Write (\"        \");\n");
      w("\t\tConsole.WriteLine (a);\n");
      w("\t}\n\n");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(6);
  }
  static void var(string type, string name, string init)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(7);
    w("\t\t" + type + " " + name + " = (" + type + ") " + init + ";\n");
    IACSharpSensor.IACSharpSensor.SensorReached(8);
  }
  static void call(string type, string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(9);
    w("\t\treceive_" + type + " (" + mode + "((" + type + ") " + name + "));\n");
    IACSharpSensor.IACSharpSensor.SensorReached(10);
  }
  static void generate_emision()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(11);
    foreach (string type in types) {
      w("\tstatic void probe_" + type + "()\n\t{\n");
      var(type, "zero", "0");
      var(type, "min", type + ".MinValue");
      var(type, "max", type + ".MaxValue");
      wl("");
      wl("\t\tConsole.WriteLine (\"Testing: " + type + "\");\n");
      foreach (string t in types) {
        wl("\t\tConsole.WriteLine (\"   arg: " + t + " (" + type + ")\");\n");
        call(t, "zero");
        call(t, "min");
        call(t, "max");
      }
      w("\t}\n\n");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(12);
  }
  static void generate_main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(13);
    wl("\tstatic void Main ()\n\t{");
    foreach (string t in types) {
      w("\t\tprobe_" + t + " ();\n");
    }
    wl("\t}");
    IACSharpSensor.IACSharpSensor.SensorReached(14);
  }
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(15);
    foreach (string arg in args) {
      if (arg == "-h" || arg == "--help") {
        Console.WriteLine("-h, --help     Shows help");
        Console.WriteLine("-c, --checked  Generate checked contexts");
        IACSharpSensor.IACSharpSensor.SensorReached(16);
        return;
      }
      if (arg == "--checked" || arg == "-c") {
        mode = "checked";
        continue;
      }
    }
    wl("using System;\nclass Test {\n");
    generate_receptors();
    generate_emision();
    generate_main();
    wl("}\n");
    IACSharpSensor.IACSharpSensor.SensorReached(17);
  }
}
