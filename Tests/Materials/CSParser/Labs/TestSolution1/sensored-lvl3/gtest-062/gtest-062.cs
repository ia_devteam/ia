using System.Collections.Generic;
class X
{
  public IEnumerable<int> Test(int a, long b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(310);
    while (a < b) {
      a++;
      System.Int32 RNTRNTRNT_54 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(311);
      yield return RNTRNTRNT_54;
      IACSharpSensor.IACSharpSensor.SensorReached(312);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(313);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(314);
    X x = new X();
    int sum = 0;
    foreach (int i in x.Test(3, 8L)) {
      sum += i;
    }
    System.Int32 RNTRNTRNT_55 = sum == 30 ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(315);
    return RNTRNTRNT_55;
  }
}
