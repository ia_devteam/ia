interface Foo<S>
{
  void Hello(S s);
}
interface Bar<T, U> : Foo<U>
{
  void Test(T t, U u);
}
class X
{
  static void Test(Bar<int, string> bar)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(126);
    bar.Hello("Test");
    bar.Test(7, "Hello");
    IACSharpSensor.IACSharpSensor.SensorReached(127);
  }
  static void Main()
  {
  }
}
