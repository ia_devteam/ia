using System;
class Foo<T>
{
  T[] t;
  public Foo(int n)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(66);
    t = new T[n];
    for (int i = 0; i < n; i++) {
      t[i] = default(T);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(67);
  }
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    X.Print(t[0]);
    IACSharpSensor.IACSharpSensor.SensorReached(69);
  }
}
class Bar<T>
{
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(70);
    X.Print(default(X));
    X.Print(default(T));
    X.Print(default(S));
    IACSharpSensor.IACSharpSensor.SensorReached(71);
  }
}
struct S
{
  public readonly string Hello;
  S(string hello)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(72);
    this.Hello = hello;
    IACSharpSensor.IACSharpSensor.SensorReached(73);
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_3 = String.Format("S({0})", Hello);
    IACSharpSensor.IACSharpSensor.SensorReached(74);
    return RNTRNTRNT_3;
  }
}
class X
{
  public static void Print(object obj)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(75);
    if (obj == null) {
      Console.WriteLine("NULL");
    } else {
      Console.WriteLine("OBJECT: {0} {1}", obj, obj.GetType());
    }
    IACSharpSensor.IACSharpSensor.SensorReached(76);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(77);
    Foo<string> a = new Foo<string>(4);
    a.Test();
    Bar<int> b = new Bar<int>();
    b.Test();
    Bar<X> c = new Bar<X>();
    c.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(78);
  }
}
