class Foo
{
  public Foo()
  {
  }
  public void Hello<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(97);
    Whatever(t);
    IACSharpSensor.IACSharpSensor.SensorReached(98);
  }
  public void Whatever(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(99);
    System.Console.WriteLine(o.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(100);
  }
}
class X
{
  static void Test(Foo foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(101);
    foo.Hello<int>(531);
    IACSharpSensor.IACSharpSensor.SensorReached(102);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(103);
    Foo foo = new Foo();
    Test(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(104);
  }
}
