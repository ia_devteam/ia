using System;
using System.Collections.Generic;
struct ComparablePair<T, U> : IComparable<ComparablePair<T, U>> where T : IComparable<T> where U : IComparable<U>
{
  public readonly T Fst;
  public readonly U Snd;
  public ComparablePair(T fst, U snd)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(327);
    Fst = fst;
    Snd = snd;
    IACSharpSensor.IACSharpSensor.SensorReached(328);
  }
  public int CompareTo(ComparablePair<T, U> that)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(329);
    int firstCmp = this.Fst.CompareTo(that.Fst);
    System.Int32 RNTRNTRNT_64 = firstCmp != 0 ? firstCmp : this.Snd.CompareTo(that.Snd);
    IACSharpSensor.IACSharpSensor.SensorReached(330);
    return RNTRNTRNT_64;
  }
  public bool Equals(ComparablePair<T, U> that)
  {
    System.Boolean RNTRNTRNT_65 = this.Fst.Equals(that.Fst) && this.Snd.Equals(that.Snd);
    IACSharpSensor.IACSharpSensor.SensorReached(331);
    return RNTRNTRNT_65;
  }
  public override String ToString()
  {
    String RNTRNTRNT_66 = "(" + Fst + ", " + Snd + ")";
    IACSharpSensor.IACSharpSensor.SensorReached(332);
    return RNTRNTRNT_66;
  }
}
class MyTest
{
  static void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(333);
    new ComparablePair<string, int>("Brazil", 2002);
    IACSharpSensor.IACSharpSensor.SensorReached(334);
  }
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(335);
    List<ComparablePair<string, int>> lst = new List<ComparablePair<string, int>>();
    lst.Add(new ComparablePair<String, int>("Brazil", 2002));
    lst.Add(new ComparablePair<String, int>("Italy", 1982));
    lst.Add(new ComparablePair<String, int>("Argentina", 1978));
    lst.Add(new ComparablePair<String, int>("Argentina", 1986));
    lst.Add(new ComparablePair<String, int>("Germany", 1990));
    lst.Add(new ComparablePair<String, int>("Brazil", 1994));
    lst.Add(new ComparablePair<String, int>("France", 1998));
    foreach (ComparablePair<String, int> pair in lst) {
      Console.WriteLine(pair);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(336);
  }
}
