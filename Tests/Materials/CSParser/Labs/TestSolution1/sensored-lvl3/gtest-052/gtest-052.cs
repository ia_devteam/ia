using System;
public class Foo<T> where T : new()
{
  public T Create()
  {
    T RNTRNTRNT_37 = new T();
    IACSharpSensor.IACSharpSensor.SensorReached(234);
    return RNTRNTRNT_37;
  }
}
class X
{
  public X()
  {
  }
  void Hello()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(235);
    Console.WriteLine("Hello World");
    IACSharpSensor.IACSharpSensor.SensorReached(236);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(237);
    Foo<X> foo = new Foo<X>();
    foo.Create().Hello();
    IACSharpSensor.IACSharpSensor.SensorReached(238);
  }
}
