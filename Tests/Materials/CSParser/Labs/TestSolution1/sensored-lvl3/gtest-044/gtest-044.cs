using System;
class X<T>
{
  public int Count;
  public X(int count)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    this.Count = count;
    IACSharpSensor.IACSharpSensor.SensorReached(204);
  }
  public static X<T> operator ++(X<T> operand)
  {
    X<T> RNTRNTRNT_35 = new X<T>(operand.Count + 1);
    IACSharpSensor.IACSharpSensor.SensorReached(205);
    return RNTRNTRNT_35;
  }
}
class Test
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    X<long> x = new X<long>(5);
    Console.WriteLine(x.Count);
    x++;
    Console.WriteLine(x.Count);
    IACSharpSensor.IACSharpSensor.SensorReached(207);
  }
}
