public interface INode<T>
{
  void Hello(T t);
}
public class Stack<T>
{
  public T TheData;
  public readonly Foo<T> TheFoo;
  public Stack(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(160);
    this.TheData = t;
    this.TheFoo = new Foo<T>(t);
    IACSharpSensor.IACSharpSensor.SensorReached(161);
  }
  public INode<T> GetNode()
  {
    INode<T> RNTRNTRNT_12 = new Node(this);
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    return RNTRNTRNT_12;
  }
  public Foo<T> GetFoo(T t)
  {
    Foo<T> RNTRNTRNT_13 = new Foo<T>(t);
    IACSharpSensor.IACSharpSensor.SensorReached(163);
    return RNTRNTRNT_13;
  }
  public Bar<T> GetBar(T t)
  {
    Bar<T> RNTRNTRNT_14 = new Bar<T>(t);
    IACSharpSensor.IACSharpSensor.SensorReached(164);
    return RNTRNTRNT_14;
  }
  protected class Node : INode<T>
  {
    public readonly Stack<T> Stack;
    public Node(Stack<T> stack)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(165);
      this.Stack = stack;
      IACSharpSensor.IACSharpSensor.SensorReached(166);
    }
    public void Hello(T t)
    {
    }
  }
  public class Foo<T>
  {
    public readonly T Data;
    public Bar<T> GetBar()
    {
      Bar<T> RNTRNTRNT_15 = new Bar<T>(Data);
      IACSharpSensor.IACSharpSensor.SensorReached(167);
      return RNTRNTRNT_15;
    }
    public Foo(T t)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(168);
      this.Data = t;
      IACSharpSensor.IACSharpSensor.SensorReached(169);
    }
  }
  public class Bar<U>
  {
    public readonly U Data;
    public Bar(U u)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(170);
      this.Data = u;
      IACSharpSensor.IACSharpSensor.SensorReached(171);
    }
    public Foo<T> GetFoo(Stack<T> stack)
    {
      Foo<T> RNTRNTRNT_16 = stack.TheFoo;
      IACSharpSensor.IACSharpSensor.SensorReached(172);
      return RNTRNTRNT_16;
    }
    public class Baz<V>
    {
      public readonly V Data;
      public Foo<T> GetFoo(Stack<T> stack)
      {
        Foo<T> RNTRNTRNT_17 = new Foo<T>(stack.TheData);
        IACSharpSensor.IACSharpSensor.SensorReached(173);
        return RNTRNTRNT_17;
      }
      public Bar<V> GetBar()
      {
        Bar<V> RNTRNTRNT_18 = new Bar<V>(Data);
        IACSharpSensor.IACSharpSensor.SensorReached(174);
        return RNTRNTRNT_18;
      }
      public Baz(V v)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(175);
        this.Data = v;
        IACSharpSensor.IACSharpSensor.SensorReached(176);
      }
    }
  }
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    Stack<T>.Foo<T> foo1 = GetFoo(TheData);
    Foo<T> foo2 = GetFoo(TheData);
    Stack<long>.Foo<T> foo3 = new Stack<long>.Foo<T>(TheData);
    Stack<long>.Foo<float> foo4 = new Stack<long>.Foo<float>(3.14f);
    Foo<double> foo5 = new Foo<double>(3.14);
    IACSharpSensor.IACSharpSensor.SensorReached(178);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(179);
    Stack<int> stack = new Stack<int>(1);
    INode<int> node = stack.GetNode();
    Stack<int>.Foo<int> foo = stack.GetFoo(7);
    Stack<int>.Bar<int> bar = stack.GetBar(8);
    IACSharpSensor.IACSharpSensor.SensorReached(180);
  }
}
