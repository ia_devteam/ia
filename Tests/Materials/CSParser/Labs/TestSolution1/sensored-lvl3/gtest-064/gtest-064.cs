using System;
public interface IHello<T>
{
}
public interface IFoo<T>
{
  IHello<T> GetHello();
}
public interface IBar<T> : IFoo<T>
{
}
public class Foo<T> : IBar<T>, IFoo<T>
{
  public Hello GetHello()
  {
    Hello RNTRNTRNT_62 = new Hello(this);
    IACSharpSensor.IACSharpSensor.SensorReached(323);
    return RNTRNTRNT_62;
  }
  IHello<T> IFoo<T>.GetHello()
  {
    IHello<T> RNTRNTRNT_63 = new Hello(this);
    IACSharpSensor.IACSharpSensor.SensorReached(324);
    return RNTRNTRNT_63;
  }
  public class Hello : IHello<T>
  {
    public readonly Foo<T> Foo;
    public Hello(Foo<T> foo)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(325);
      this.Foo = foo;
      IACSharpSensor.IACSharpSensor.SensorReached(326);
    }
  }
}
class X
{
  static void Main()
  {
  }
}
