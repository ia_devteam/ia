using System;
class Test<T>
{
  public void Foo(T t, out int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(423);
    a = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(424);
  }
  public void Hello(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(425);
    int a;
    Foo(t, out a);
    IACSharpSensor.IACSharpSensor.SensorReached(426);
  }
}
class X
{
  static void Main()
  {
  }
}
