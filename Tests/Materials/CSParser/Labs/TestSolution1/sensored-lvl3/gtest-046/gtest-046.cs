using System;
delegate void Test<T>(T t);
class Foo<T>
{
  public event Test<T> MyEvent;
  public void Hello(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(210);
    if (MyEvent != null) {
      MyEvent(t);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(211);
  }
}
class X
{
  static void do_hello(string hello)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(212);
    Console.WriteLine("Hello: {0}", hello);
    IACSharpSensor.IACSharpSensor.SensorReached(213);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(214);
    Foo<string> foo = new Foo<string>();
    foo.MyEvent += new Test<string>(do_hello);
    foo.Hello("Boston");
    IACSharpSensor.IACSharpSensor.SensorReached(215);
  }
}
