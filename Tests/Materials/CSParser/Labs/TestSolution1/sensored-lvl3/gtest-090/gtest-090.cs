using System;
public abstract class Foo<T>
{
  public virtual T InsertAll<U>(U u) where U : T
  {
    T RNTRNTRNT_94 = u;
    IACSharpSensor.IACSharpSensor.SensorReached(427);
    return RNTRNTRNT_94;
  }
}
public class Bar<T> : Foo<T>
{
  public override T InsertAll<U>(U u)
  {
    T RNTRNTRNT_95 = u;
    IACSharpSensor.IACSharpSensor.SensorReached(428);
    return RNTRNTRNT_95;
  }
}
class X
{
  static void Main()
  {
  }
}
