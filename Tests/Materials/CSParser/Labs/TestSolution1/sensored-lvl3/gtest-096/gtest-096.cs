using System;
class Foo<T>
{
}
class Test
{
  static void Hello<T>(Foo<T>[] foo, int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(446);
    Foo<T> element = foo[0];
    Console.WriteLine(element);
    if (i > 0) {
      Hello<T>(foo, i - 1);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(447);
  }
  public static void Quicksort<U>(Foo<U>[] arr)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(448);
    Hello<U>(arr, 1);
    IACSharpSensor.IACSharpSensor.SensorReached(449);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(450);
    Foo<int>[] foo = new Foo<int>[1];
    foo[0] = new Foo<int>();
    Quicksort(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(451);
  }
}
