namespace N1
{
  class A<T>
  {
    public class B
    {
    }
    public class C<U>
    {
    }
  }
  class C
  {
  }
}
namespace N2
{
  using Y = N1.A<int>;
  class X
  {
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(279);
      Y y = new Y();
      Y.B b = new Y.B();
      Y.C<long> c = new Y.C<long>();
      IACSharpSensor.IACSharpSensor.SensorReached(280);
    }
  }
}
