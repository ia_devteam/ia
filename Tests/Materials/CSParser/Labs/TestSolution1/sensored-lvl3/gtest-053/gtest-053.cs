using System;
public interface ICounter
{
  void Increment();
}
namespace ValueTypeCounters
{
  public struct SimpleCounter : ICounter
  {
    public int Value;
    public void Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(239);
      Value += 2;
      IACSharpSensor.IACSharpSensor.SensorReached(240);
    }
  }
  public struct PrintingCounter : ICounter
  {
    public int Value;
    public override string ToString()
    {
      System.String RNTRNTRNT_38 = Value.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(241);
      return RNTRNTRNT_38;
    }
    public void Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(242);
      Value += 2;
      IACSharpSensor.IACSharpSensor.SensorReached(243);
    }
  }
  public struct ExplicitCounter : ICounter
  {
    public int Value;
    public override string ToString()
    {
      System.String RNTRNTRNT_39 = Value.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(244);
      return RNTRNTRNT_39;
    }
    void ICounter.Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(245);
      Value++;
      IACSharpSensor.IACSharpSensor.SensorReached(246);
    }
  }
  public struct InterfaceCounter : ICounter
  {
    public int Value;
    public override string ToString()
    {
      System.String RNTRNTRNT_40 = Value.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(247);
      return RNTRNTRNT_40;
    }
    void ICounter.Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(248);
      Value++;
      IACSharpSensor.IACSharpSensor.SensorReached(249);
    }
    public void Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(250);
      Value += 2;
      IACSharpSensor.IACSharpSensor.SensorReached(251);
    }
  }
}
namespace ReferenceTypeCounters
{
  public class SimpleCounter : ICounter
  {
    public int Value;
    public void Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(252);
      Value += 2;
      IACSharpSensor.IACSharpSensor.SensorReached(253);
    }
  }
  public class PrintingCounter : ICounter
  {
    public int Value;
    public override string ToString()
    {
      System.String RNTRNTRNT_41 = Value.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(254);
      return RNTRNTRNT_41;
    }
    public void Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(255);
      Value += 2;
      IACSharpSensor.IACSharpSensor.SensorReached(256);
    }
  }
  public class ExplicitCounter : ICounter
  {
    public int Value;
    public override string ToString()
    {
      System.String RNTRNTRNT_42 = Value.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(257);
      return RNTRNTRNT_42;
    }
    void ICounter.Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(258);
      Value++;
      IACSharpSensor.IACSharpSensor.SensorReached(259);
    }
  }
  public class InterfaceCounter : ICounter
  {
    public int Value;
    public override string ToString()
    {
      System.String RNTRNTRNT_43 = Value.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(260);
      return RNTRNTRNT_43;
    }
    void ICounter.Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(261);
      Value++;
      IACSharpSensor.IACSharpSensor.SensorReached(262);
    }
    public void Increment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(263);
      Value += 2;
      IACSharpSensor.IACSharpSensor.SensorReached(264);
    }
  }
}
namespace Test
{
  using V = ValueTypeCounters;
  using R = ReferenceTypeCounters;
  public class Test<T> where T : ICounter
  {
    public static void Foo(T x)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(265);
      Console.WriteLine(x.ToString());
      x.Increment();
      Console.WriteLine(x.ToString());
      IACSharpSensor.IACSharpSensor.SensorReached(266);
    }
  }
  public class X
  {
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(267);
      Test<V.SimpleCounter>.Foo(new V.SimpleCounter());
      Test<V.PrintingCounter>.Foo(new V.PrintingCounter());
      Test<V.ExplicitCounter>.Foo(new V.ExplicitCounter());
      Test<V.InterfaceCounter>.Foo(new V.InterfaceCounter());
      Test<R.SimpleCounter>.Foo(new R.SimpleCounter());
      Test<R.PrintingCounter>.Foo(new R.PrintingCounter());
      Test<R.ExplicitCounter>.Foo(new R.ExplicitCounter());
      Test<R.InterfaceCounter>.Foo(new R.InterfaceCounter());
      IACSharpSensor.IACSharpSensor.SensorReached(268);
    }
  }
}
