class Foo<T>
{
  public void Hello()
  {
  }
  public void World(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(79);
    Hello();
    IACSharpSensor.IACSharpSensor.SensorReached(80);
  }
}
class Bar : Foo<int>
{
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(81);
    Hello();
    World(4);
    IACSharpSensor.IACSharpSensor.SensorReached(82);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(83);
    Bar bar = new Bar();
    bar.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(84);
  }
}
