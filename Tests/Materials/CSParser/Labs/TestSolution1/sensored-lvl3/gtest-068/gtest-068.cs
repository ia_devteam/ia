namespace Martin
{
  public class Foo<T>
  {
  }
}
namespace Baulig
{
  using M = Martin;
  class X
  {
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(342);
      M.Foo<int> foo;
      IACSharpSensor.IACSharpSensor.SensorReached(343);
    }
  }
}
