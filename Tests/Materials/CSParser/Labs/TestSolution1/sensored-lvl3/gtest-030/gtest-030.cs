class Foo<T>
{
  public Foo()
  {
  }
  public void Hello(T t)
  {
  }
}
class Bar<T, U> : Foo<U>
{
  public Bar()
  {
  }
  public void Test(T t, U u)
  {
  }
}
class X
{
  static void Test(Bar<int, string> bar)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(120);
    bar.Hello("Test");
    bar.Test(7, "Hello");
    IACSharpSensor.IACSharpSensor.SensorReached(121);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(122);
    Bar<int, string> bar = new Bar<int, string>();
    Test(bar);
    IACSharpSensor.IACSharpSensor.SensorReached(123);
  }
}
