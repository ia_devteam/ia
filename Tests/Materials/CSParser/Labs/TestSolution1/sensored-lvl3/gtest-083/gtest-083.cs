public class CollectionValueBase<T>
{
  public virtual T[] ToArray()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    return null;
  }
}
public class CollectionBase<T> : CollectionValueBase<T>
{
}
public class SequencedBase<T> : CollectionBase<T>
{
}
public class ArrayBase<T> : SequencedBase<T>
{
  public override T[] ToArray()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(408);
    return null;
  }
}
class X
{
  static void Main()
  {
  }
}
