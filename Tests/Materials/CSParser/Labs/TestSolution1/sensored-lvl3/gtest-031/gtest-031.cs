public class X
{
  public static void Test(Bar<int, string> bar)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(124);
    bar.Hello("Test");
    bar.Test(7, "Hello");
    IACSharpSensor.IACSharpSensor.SensorReached(125);
  }
  static void Main()
  {
  }
}
