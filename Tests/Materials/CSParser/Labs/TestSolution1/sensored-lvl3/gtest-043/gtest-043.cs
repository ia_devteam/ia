using System;
public class Test<T>
{
  public static int Count;
  public void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(183);
    Count++;
    IACSharpSensor.IACSharpSensor.SensorReached(184);
  }
  public int GetCount()
  {
    System.Int32 RNTRNTRNT_19 = Count;
    IACSharpSensor.IACSharpSensor.SensorReached(185);
    return RNTRNTRNT_19;
  }
}
class X
{
  static int DoTheTest<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(186);
    Test<T> test = new Test<T>();
    test.Foo();
    if (test.GetCount() != 1) {
      System.Int32 RNTRNTRNT_20 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(187);
      return RNTRNTRNT_20;
    }
    if (Test<T>.Count != 1) {
      System.Int32 RNTRNTRNT_21 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(188);
      return RNTRNTRNT_21;
    }
    test.Foo();
    if (test.GetCount() != 2) {
      System.Int32 RNTRNTRNT_22 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(189);
      return RNTRNTRNT_22;
    }
    if (Test<T>.Count != 2) {
      System.Int32 RNTRNTRNT_23 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(190);
      return RNTRNTRNT_23;
    }
    test.Foo();
    if (test.GetCount() != 3) {
      System.Int32 RNTRNTRNT_24 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(191);
      return RNTRNTRNT_24;
    }
    if (Test<T>.Count != 3) {
      System.Int32 RNTRNTRNT_25 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(192);
      return RNTRNTRNT_25;
    }
    System.Int32 RNTRNTRNT_26 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(193);
    return RNTRNTRNT_26;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(194);
    int result = DoTheTest<int>();
    if (result != 0) {
      System.Int32 RNTRNTRNT_27 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(195);
      return RNTRNTRNT_27;
    }
    result = DoTheTest<long>() + 10;
    if (result != 10) {
      System.Int32 RNTRNTRNT_28 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(196);
      return RNTRNTRNT_28;
    }
    Test<int>.Count = 0;
    ++Test<long>.Count;
    result = DoTheTest<int>() + 20;
    if (result != 20) {
      System.Int32 RNTRNTRNT_29 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(197);
      return RNTRNTRNT_29;
    }
    if (Test<int>.Count != 3) {
      System.Int32 RNTRNTRNT_30 = 31;
      IACSharpSensor.IACSharpSensor.SensorReached(198);
      return RNTRNTRNT_30;
    }
    if (Test<long>.Count != 4) {
      System.Int32 RNTRNTRNT_31 = 32;
      IACSharpSensor.IACSharpSensor.SensorReached(199);
      return RNTRNTRNT_31;
    }
    Test<float>.Count = 5;
    if (Test<int>.Count != 3) {
      System.Int32 RNTRNTRNT_32 = 33;
      IACSharpSensor.IACSharpSensor.SensorReached(200);
      return RNTRNTRNT_32;
    }
    if (Test<long>.Count != 4) {
      System.Int32 RNTRNTRNT_33 = 34;
      IACSharpSensor.IACSharpSensor.SensorReached(201);
      return RNTRNTRNT_33;
    }
    System.Int32 RNTRNTRNT_34 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(202);
    return RNTRNTRNT_34;
  }
}
