using System;
struct Foo<T>
{
  public T Data;
  public Foo(T data)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(383);
    this.Data = data;
    IACSharpSensor.IACSharpSensor.SensorReached(384);
  }
}
class Test<T>
{
  public Foo<T> GetFoo(T data)
  {
    Foo<T> RNTRNTRNT_82 = new Foo<T>(data);
    IACSharpSensor.IACSharpSensor.SensorReached(385);
    return RNTRNTRNT_82;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(386);
    Test<long> test = new Test<long>();
    Foo<long> foo = test.GetFoo(0x800);
    if (foo.Data != 0x800) {
      System.Int32 RNTRNTRNT_83 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(387);
      return RNTRNTRNT_83;
    }
    System.Int32 RNTRNTRNT_84 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(388);
    return RNTRNTRNT_84;
  }
}
