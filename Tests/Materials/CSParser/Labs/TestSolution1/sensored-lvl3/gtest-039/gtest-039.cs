using System;
interface IMonkey<T>
{
  T Jump();
}
class Zoo<T>
{
  T t;
  public Zoo(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(150);
    this.t = t;
    IACSharpSensor.IACSharpSensor.SensorReached(151);
  }
  public T Name {
    get {
      T RNTRNTRNT_9 = t;
      IACSharpSensor.IACSharpSensor.SensorReached(152);
      return RNTRNTRNT_9;
    }
  }
  public IMonkey<U> GetTheMonkey<U>(U u)
  {
    IMonkey<U> RNTRNTRNT_10 = new Monkey<T, U>(this, u);
    IACSharpSensor.IACSharpSensor.SensorReached(153);
    return RNTRNTRNT_10;
  }
  public class Monkey<V, W> : IMonkey<W>
  {
    public readonly Zoo<V> Zoo;
    public readonly W Data;
    public Monkey(Zoo<V> zoo, W data)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(154);
      this.Zoo = zoo;
      this.Data = data;
      IACSharpSensor.IACSharpSensor.SensorReached(155);
    }
    public W Jump()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(156);
      Console.WriteLine("Monkey {0} from {1} jumping!", Data, Zoo.Name);
      W RNTRNTRNT_11 = Data;
      IACSharpSensor.IACSharpSensor.SensorReached(157);
      return RNTRNTRNT_11;
    }
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(158);
    Zoo<string> zoo = new Zoo<string>("Boston");
    IMonkey<float> monkey = zoo.GetTheMonkey<float>(3.14f);
    monkey.Jump();
    IACSharpSensor.IACSharpSensor.SensorReached(159);
  }
}
