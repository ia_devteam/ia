public class Foo : IFoo
{
  void IFoo<X>.Test()
  {
  }
  void IFoo<Y, Z>.Test()
  {
  }
}
public class Bar<X, Y, Z> : IBar<X>, IBar<Y, Z>
{
  void IBar<X>.Test()
  {
  }
  void IBar<Y, Z>.Test()
  {
  }
}
class X
{
  static void Main()
  {
  }
}
