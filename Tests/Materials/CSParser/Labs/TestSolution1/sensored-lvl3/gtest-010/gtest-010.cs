using System;
interface I
{
  void Hello();
}
class J
{
  public void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    Console.WriteLine("Foo!");
    IACSharpSensor.IACSharpSensor.SensorReached(43);
  }
}
class Stack<T> where T : J, I
{
  public void Test(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(44);
    t.Hello();
    t.Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(45);
  }
}
class Test
{
}
class X
{
  static void Main()
  {
  }
}
