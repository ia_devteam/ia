class Test
{
  public delegate int Foo<T>(T t, T u);
  public void Hello<U>(Foo<U> foo, U u)
  {
  }
}
class X
{
  static int Add(int a, int b)
  {
    System.Int32 RNTRNTRNT_67 = a + b;
    IACSharpSensor.IACSharpSensor.SensorReached(339);
    return RNTRNTRNT_67;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(340);
    Test test = new Test();
    test.Hello<int>(new Test.Foo<int>(Add), 5);
    IACSharpSensor.IACSharpSensor.SensorReached(341);
  }
}
