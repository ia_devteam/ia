using System;
public class Element<T>
{
  public readonly T Item;
  public Element(T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(430);
    this.Item = item;
    IACSharpSensor.IACSharpSensor.SensorReached(431);
  }
  public void GetItem(out T retval)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(432);
    retval = Item;
    IACSharpSensor.IACSharpSensor.SensorReached(433);
  }
  public T GetItem(int a, ref T data)
  {
    T RNTRNTRNT_97 = Item;
    IACSharpSensor.IACSharpSensor.SensorReached(434);
    return RNTRNTRNT_97;
  }
  public void SetItem(T data)
  {
  }
}
public class Foo<T>
{
  Element<Node> element;
  public Node Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(435);
    Node node = element.Item;
    element.GetItem(out node);
    element.SetItem(node);
    Node RNTRNTRNT_98 = element.GetItem(3, ref node);
    IACSharpSensor.IACSharpSensor.SensorReached(436);
    return RNTRNTRNT_98;
  }
  public class Node
  {
  }
}
class X
{
  static void Main()
  {
  }
}
