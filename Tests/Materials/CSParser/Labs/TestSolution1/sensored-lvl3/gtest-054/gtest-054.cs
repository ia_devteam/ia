using System;
interface Foo
{
  void Hello();
}
class A
{
}
class B : A, Foo
{
  public void Hello()
  {
  }
  public static implicit operator C(B b)
  {
    C RNTRNTRNT_44 = new C();
    IACSharpSensor.IACSharpSensor.SensorReached(269);
    return RNTRNTRNT_44;
  }
}
class C
{
  public static explicit operator B(C c)
  {
    B RNTRNTRNT_45 = new B();
    IACSharpSensor.IACSharpSensor.SensorReached(270);
    return RNTRNTRNT_45;
  }
}
class Test
{
  static void Simple<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(271);
    object o = t;
    t = (T)o;
    Foo foo = (Foo)t;
    t = (T)foo;
    IACSharpSensor.IACSharpSensor.SensorReached(272);
  }
  static void Interface<T>(T t) where T : Foo
  {
    IACSharpSensor.IACSharpSensor.SensorReached(273);
    Foo foo = t;
    IACSharpSensor.IACSharpSensor.SensorReached(274);
  }
  static void Class<T>(T t) where T : B
  {
    IACSharpSensor.IACSharpSensor.SensorReached(275);
    B b = t;
    A a = t;
    Foo foo = t;
    t = (T)b;
    t = (T)a;
    t = (T)foo;
    C c = t;
    t = (T)c;
    IACSharpSensor.IACSharpSensor.SensorReached(276);
  }
  static void Array<T>(T[] t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(277);
    object o = t;
    Array a = t;
    t = (T[])o;
    t = (T[])a;
    IACSharpSensor.IACSharpSensor.SensorReached(278);
  }
  static void Main()
  {
  }
}
