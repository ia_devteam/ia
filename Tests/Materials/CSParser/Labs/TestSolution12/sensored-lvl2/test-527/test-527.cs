using System;
class Repro
{
  private int[] stack = new int[1];
  private int cc;
  public int fc;
  private int sp;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(261);
    Repro r = new Repro();
    r.foo();
    Console.WriteLine(r.stack[0]);
    System.Int32 RNTRNTRNT_38 = r.stack[0] == 42 ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(262);
    return RNTRNTRNT_38;
  }
  public void foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(263);
    fc = cc = bar();
    fc = stack[sp++] = cc;
    IACSharpSensor.IACSharpSensor.SensorReached(264);
  }
  private int bar()
  {
    System.Int32 RNTRNTRNT_39 = 42;
    IACSharpSensor.IACSharpSensor.SensorReached(265);
    return RNTRNTRNT_39;
  }
}
