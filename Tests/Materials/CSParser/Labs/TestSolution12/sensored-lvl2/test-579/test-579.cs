using System;
using System.Reflection;
class X
{
  delegate object test(MethodInfo x);
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(480);
    DoCall(delegate(MethodInfo @from) { return @from.Invoke(null, new object[] { @from }); });
    IACSharpSensor.IACSharpSensor.SensorReached(481);
  }
  static void DoCall(test t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(482);
  }
}
