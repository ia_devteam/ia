using System;
public class TestParams
{
  object this[params string[] idx] {
    get {
      System.Object RNTRNTRNT_97 = idx[0];
      IACSharpSensor.IACSharpSensor.SensorReached(522);
      return RNTRNTRNT_97;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(523);
      Console.WriteLine(value);
      IACSharpSensor.IACSharpSensor.SensorReached(524);
      if ((string)value != "A(B)") {
        IACSharpSensor.IACSharpSensor.SensorReached(525);
        throw new ApplicationException(value.ToString());
      }
      IACSharpSensor.IACSharpSensor.SensorReached(526);
    }
  }
  public void TestMethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(527);
    this["A"] += "(" + this["B"] + ")";
    this[new string[] { "A" }] += "(" + this["B"] + ")";
    IACSharpSensor.IACSharpSensor.SensorReached(528);
  }
}
public class TestNonParams
{
  object this[string idx] {
    get {
      System.Object RNTRNTRNT_98 = idx;
      IACSharpSensor.IACSharpSensor.SensorReached(529);
      return RNTRNTRNT_98;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(530);
      Console.WriteLine(value);
      IACSharpSensor.IACSharpSensor.SensorReached(531);
      if ((string)value != "A(B)") {
        IACSharpSensor.IACSharpSensor.SensorReached(532);
        throw new ApplicationException(value.ToString());
      }
      IACSharpSensor.IACSharpSensor.SensorReached(533);
    }
  }
  public void TestMethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(534);
    this["A"] += "(" + this["B"] + ")";
    IACSharpSensor.IACSharpSensor.SensorReached(535);
  }
}
public class M
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(536);
    new TestNonParams().TestMethod();
    new TestParams().TestMethod();
    System.Int32 RNTRNTRNT_99 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(537);
    return RNTRNTRNT_99;
  }
}
