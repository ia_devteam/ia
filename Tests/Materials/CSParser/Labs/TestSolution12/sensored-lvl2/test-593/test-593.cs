using System;
interface I
{
  void Finalize();
}
class MainClass
{
  void Foo(I i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(646);
    i.Finalize();
    IACSharpSensor.IACSharpSensor.SensorReached(647);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(648);
  }
}
