public delegate void FooHandler();
public static class Test
{
  private static void OnFooTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(287);
  }
  public static event FooHandler Foo;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(288);
    FooHandler foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(289);
      Foo += OnFooTest;
    };
    IACSharpSensor.IACSharpSensor.SensorReached(290);
  }
}
