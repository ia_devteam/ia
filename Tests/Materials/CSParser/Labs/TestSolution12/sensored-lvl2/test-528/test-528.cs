using System;
namespace MicroFocus.MONO.Bugs
{
  public class Odd
  {
    private static readonly Type[] _argTypes = { typeof(object[]) };
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(266);
    }
  }
}
