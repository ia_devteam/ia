using System;
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(280);
    UIntPtr a = (UIntPtr)1;
    sbyte _sbyte = (sbyte)a;
    short _short = (short)a;
    int _int = (int)a;
    IntPtr _intptr = (IntPtr)1;
    ulong _ulong = (ulong)_intptr;
    UIntPtr _uptr = (UIntPtr)_sbyte;
    _uptr = (UIntPtr)_short;
    _uptr = (UIntPtr)_int;
    IACSharpSensor.IACSharpSensor.SensorReached(281);
  }
  static void Compile()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(282);
    IntPtr a = (IntPtr)1;
    M(a);
    IACSharpSensor.IACSharpSensor.SensorReached(283);
  }
  static void M(long l)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(284);
  }
  static void M(UInt64 l)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(285);
  }
  static void M(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(286);
  }
}
