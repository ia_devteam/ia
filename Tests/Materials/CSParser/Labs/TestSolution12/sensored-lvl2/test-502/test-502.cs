class Base
{
  public int Property {
    get {
      System.Int32 RNTRNTRNT_3 = 42;
      IACSharpSensor.IACSharpSensor.SensorReached(17);
      return RNTRNTRNT_3;
    }
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(18);
  }
}
class Derived : Base
{
  public int get_Property()
  {
    System.Int32 RNTRNTRNT_4 = 42;
    IACSharpSensor.IACSharpSensor.SensorReached(19);
    return RNTRNTRNT_4;
  }
}
class BaseClass
{
  protected virtual int Value {
    get {
      System.Int32 RNTRNTRNT_5 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(20);
      return RNTRNTRNT_5;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(21); }
  }
}
abstract class DerivedClass : BaseClass
{
  protected int get_Value()
  {
    System.Int32 RNTRNTRNT_6 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(22);
    return RNTRNTRNT_6;
  }
}
class ErrorClass : DerivedClass
{
  protected override int Value {
    get {
      System.Int32 RNTRNTRNT_7 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(23);
      return RNTRNTRNT_7;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(24); }
  }
}
