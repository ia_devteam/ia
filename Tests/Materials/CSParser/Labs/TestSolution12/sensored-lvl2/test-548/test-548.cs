using System;
namespace Bugs
{
  class Bug0
  {
    struct MyBoolean
    {
      private bool value;
      public MyBoolean(bool value)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(351);
        this.value = value;
        IACSharpSensor.IACSharpSensor.SensorReached(352);
      }
      public static implicit operator MyBoolean(bool value)
      {
        MyBoolean RNTRNTRNT_53 = new MyBoolean(value);
        IACSharpSensor.IACSharpSensor.SensorReached(353);
        return RNTRNTRNT_53;
      }
      public static implicit operator bool(MyBoolean b)
      {
        System.Boolean RNTRNTRNT_54 = b.value;
        IACSharpSensor.IACSharpSensor.SensorReached(354);
        return RNTRNTRNT_54;
      }
    }
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(355);
      MyBoolean b = true;
      IACSharpSensor.IACSharpSensor.SensorReached(356);
      if (true && b) {
        System.Int32 RNTRNTRNT_55 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(357);
        return RNTRNTRNT_55;
      } else {
        System.Int32 RNTRNTRNT_56 = 100;
        IACSharpSensor.IACSharpSensor.SensorReached(358);
        return RNTRNTRNT_56;
      }
    }
  }
}
