public class Test
{
  private C _vssItem;
  public string Spec {
    get {
      System.String RNTRNTRNT_72 = _vssItem.Spec;
      IACSharpSensor.IACSharpSensor.SensorReached(420);
      return RNTRNTRNT_72;
    }
  }
  void Foo(C c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(421);
    c.Checkout();
    IACSharpSensor.IACSharpSensor.SensorReached(422);
  }
  void Foo2(CC cc)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(423);
    cc.Spec = "aa";
    IACSharpSensor.IACSharpSensor.SensorReached(424);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(425);
  }
}
interface A
{
  void Checkout();
  string Spec { get; }
}
interface B : A
{
  new void Checkout();
  new string Spec { get; }
}
interface C : B
{
}
class CA
{
  public string Spec {
    set { IACSharpSensor.IACSharpSensor.SensorReached(426); }
  }
}
class CB : CA
{
  public new string Spec {
    set { IACSharpSensor.IACSharpSensor.SensorReached(427); }
  }
}
class CC : CB
{
}
