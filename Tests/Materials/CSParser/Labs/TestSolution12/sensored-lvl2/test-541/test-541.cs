delegate void TestFunc(int val);
class A
{
  public A(TestFunc func)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(317);
    func(0);
    IACSharpSensor.IACSharpSensor.SensorReached(318);
  }
}
class TestClass
{
  static int i = 1;
  static readonly A a = new A(delegate(int a) { i = a; });
  static int Main()
  {
    System.Int32 RNTRNTRNT_47 = i;
    IACSharpSensor.IACSharpSensor.SensorReached(319);
    return RNTRNTRNT_47;
  }
}
