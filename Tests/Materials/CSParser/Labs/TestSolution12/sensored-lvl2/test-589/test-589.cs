using System;
using TestNamespace;
namespace TestNamespace
{
  public class TestClass
  {
    public static void HelloWorld()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(570);
    }
  }
}
class SuperClass
{
  TestClass tc = null;
  public TestClass TestClass {
    private get {
      TestClass RNTRNTRNT_112 = tc;
      IACSharpSensor.IACSharpSensor.SensorReached(571);
      return RNTRNTRNT_112;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(572); }
  }
}
class SubClass : SuperClass
{
  public SubClass()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(573);
    TestClass.HelloWorld();
    IACSharpSensor.IACSharpSensor.SensorReached(574);
  }
}
class App
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(575);
    SubClass sc = new SubClass();
    IACSharpSensor.IACSharpSensor.SensorReached(576);
  }
}
