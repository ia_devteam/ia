delegate IInterface testDelegate(concrete x);
interface IInterface
{
}
class concrete : IInterface
{
}
class Program
{
  private concrete getConcrete(IInterface z)
  {
    concrete RNTRNTRNT_41 = new concrete();
    IACSharpSensor.IACSharpSensor.SensorReached(297);
    return RNTRNTRNT_41;
  }
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(298);
    Program p = new Program();
    testDelegate x = new testDelegate(p.getConcrete);
    IACSharpSensor.IACSharpSensor.SensorReached(299);
  }
}
