using System;
using System.Reflection;
using System.Runtime.InteropServices;
namespace preservesig_test
{
  class Class1
  {
    static int Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(428);
      MethodInfo dofoo = typeof(TestClass).GetMethod("DoFoo");
      IACSharpSensor.IACSharpSensor.SensorReached(429);
      if ((dofoo.GetMethodImplementationFlags() & MethodImplAttributes.PreserveSig) == 0) {
        System.Int32 RNTRNTRNT_73 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(430);
        return RNTRNTRNT_73;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(431);
      dofoo = typeof(TestClass).GetProperty("Foo").GetGetMethod();
      IACSharpSensor.IACSharpSensor.SensorReached(432);
      if ((dofoo.GetMethodImplementationFlags() & MethodImplAttributes.PreserveSig) == 0) {
        System.Int32 RNTRNTRNT_74 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(433);
        return RNTRNTRNT_74;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(434);
      dofoo = typeof(TestClass).GetEvent("e").GetAddMethod(true);
      IACSharpSensor.IACSharpSensor.SensorReached(435);
      if ((dofoo.GetMethodImplementationFlags() & MethodImplAttributes.PreserveSig) == 0) {
        System.Int32 RNTRNTRNT_75 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(436);
        return RNTRNTRNT_75;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(437);
      Console.WriteLine("Has PreserveSig");
      System.Int32 RNTRNTRNT_76 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(438);
      return RNTRNTRNT_76;
    }
  }
  public class TestClass
  {
    public delegate void D();
    [method: PreserveSig()]
    public event D e;
    [PreserveSig()]
    public int DoFoo()
    {
      System.Int32 RNTRNTRNT_77 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(439);
      return RNTRNTRNT_77;
    }
    public int Foo {
      [PreserveSig()]
      get {
        System.Int32 RNTRNTRNT_78 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(440);
        return RNTRNTRNT_78;
      }
    }
  }
}
