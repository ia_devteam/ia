using System;
class OutputParam
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    int a;
    Method(out a);
    Console.WriteLine(a);
    IACSharpSensor.IACSharpSensor.SensorReached(63);
  }
  public static void Method(out int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(64);
    int b;
    IACSharpSensor.IACSharpSensor.SensorReached(65);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(66);
      b = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(67);
      return;
    } finally {
      IACSharpSensor.IACSharpSensor.SensorReached(68);
      a = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(69);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(70);
  }
}
