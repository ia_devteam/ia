using System;
using System.Reflection;
[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
class SomeCustomAttribute : Attribute
{
  public SomeCustomAttribute()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
  }
}
class MainClass
{
  [SomeCustomAttribute()]
  public int a;
  [SomeCustomAttribute()]
  public int x, y;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2);
    Type t = typeof(MainClass);
    FieldInfo[] fia = t.GetFields();
    IACSharpSensor.IACSharpSensor.SensorReached(3);
    foreach (FieldInfo fi in fia) {
      IACSharpSensor.IACSharpSensor.SensorReached(4);
      object[] ca = fi.GetCustomAttributes(typeof(SomeCustomAttribute), false);
      System.Console.WriteLine("Field: {0} [{1}]", fi.Name, ca.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(5);
      if (ca.Length != 1) {
        System.Int32 RNTRNTRNT_1 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(6);
        return RNTRNTRNT_1;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(7);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_2 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(8);
    return RNTRNTRNT_2;
  }
}
