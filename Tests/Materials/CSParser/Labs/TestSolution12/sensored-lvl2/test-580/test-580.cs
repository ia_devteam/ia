using System;
public class Bla
{
  public static void BuildNode(ref string label)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(483);
    string s = "a";
    label += s + s + s + s;
    IACSharpSensor.IACSharpSensor.SensorReached(484);
  }
  public static void BuildNode(ref string[] label)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(485);
    string s = "a";
    int idx = 0;
    label[idx++] += s + s + s + s;
    IACSharpSensor.IACSharpSensor.SensorReached(486);
  }
  public static void BuildNode_B(ref object label)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(487);
    string s = "b";
    label += s + s;
    IACSharpSensor.IACSharpSensor.SensorReached(488);
  }
  public static string BuildNode_C(ref string label)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(489);
    string[] a = new string[2];
    int i = 0;
    a[0] = "a";
    string s = "b";
    a[i++] += label + s + s + s;
    System.String RNTRNTRNT_87 = a[i - 1];
    IACSharpSensor.IACSharpSensor.SensorReached(490);
    return RNTRNTRNT_87;
  }
  public static string BuildNode_D()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(491);
    System.Collections.ArrayList values = new System.Collections.ArrayList();
    IACSharpSensor.IACSharpSensor.SensorReached(492);
    for (int i = 0; i < 6; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(493);
      values.Add(i);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(494);
    string[] strs = new string[values.Count];
    int idx = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(495);
    foreach (int val in values) {
      IACSharpSensor.IACSharpSensor.SensorReached(496);
      strs[idx] = "Value:";
      strs[idx++] += val.ToString();
    }
    System.String RNTRNTRNT_88 = strs[5];
    IACSharpSensor.IACSharpSensor.SensorReached(497);
    return RNTRNTRNT_88;
  }
  public static void BuildNode_E(ref string[,] label)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(498);
    string s = "a";
    int idx = 0;
    label = new string[1, 1];
    label[idx++, idx - 1] += s + s + s + s;
    IACSharpSensor.IACSharpSensor.SensorReached(499);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(500);
    String str = "test";
    BuildNode(ref str);
    Console.WriteLine(str);
    IACSharpSensor.IACSharpSensor.SensorReached(501);
    if (str != "testaaaa") {
      System.Int32 RNTRNTRNT_89 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(502);
      return RNTRNTRNT_89;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(503);
    object ostr = "test";
    BuildNode_B(ref ostr);
    Console.WriteLine(ostr);
    IACSharpSensor.IACSharpSensor.SensorReached(504);
    if (ostr.ToString() != "testbb") {
      System.Int32 RNTRNTRNT_90 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(505);
      return RNTRNTRNT_90;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(506);
    str = "test";
    string res = BuildNode_C(ref str);
    Console.WriteLine(str);
    IACSharpSensor.IACSharpSensor.SensorReached(507);
    if (str != "test") {
      System.Int32 RNTRNTRNT_91 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(508);
      return RNTRNTRNT_91;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(509);
    Console.WriteLine(res);
    IACSharpSensor.IACSharpSensor.SensorReached(510);
    if (res != "atestbbb") {
      System.Int32 RNTRNTRNT_92 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(511);
      return RNTRNTRNT_92;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(512);
    string[] sa = new string[1];
    BuildNode(ref sa);
    Console.WriteLine(sa[0]);
    IACSharpSensor.IACSharpSensor.SensorReached(513);
    if (sa[0] != "aaaa") {
      System.Int32 RNTRNTRNT_93 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(514);
      return RNTRNTRNT_93;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(515);
    str = BuildNode_D();
    Console.WriteLine(str);
    IACSharpSensor.IACSharpSensor.SensorReached(516);
    if (str != "Value:5") {
      System.Int32 RNTRNTRNT_94 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(517);
      return RNTRNTRNT_94;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(518);
    string[,] sa2 = null;
    BuildNode_E(ref sa2);
    Console.WriteLine(sa2[0, 0]);
    IACSharpSensor.IACSharpSensor.SensorReached(519);
    if (sa2[0, 0] != "aaaa") {
      System.Int32 RNTRNTRNT_95 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(520);
      return RNTRNTRNT_95;
    }
    System.Int32 RNTRNTRNT_96 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(521);
    return RNTRNTRNT_96;
  }
}
