using System;
using System.Reflection;
namespace NewslotVirtualFinal
{
  internal interface INewslotVirtualFinal
  {
    void SomeMethod();
    void SomeMethod2();
  }
  internal class NewslotVirtualFinal : INewslotVirtualFinal
  {
    private NewslotVirtualFinal()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(51);
    }
    public void SomeMethod()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(52);
    }
    public virtual void SomeMethod2()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(53);
    }
  }
  class C
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      Type t = typeof(NewslotVirtualFinal);
      MethodInfo mi = t.GetMethod("SomeMethod");
      IACSharpSensor.IACSharpSensor.SensorReached(55);
      if (mi.Attributes != (MethodAttributes.PrivateScope | MethodAttributes.Public | MethodAttributes.Final | MethodAttributes.Virtual | MethodAttributes.HideBySig | MethodAttributes.VtableLayoutMask)) {
        System.Int32 RNTRNTRNT_12 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(56);
        return RNTRNTRNT_12;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(57);
      mi = t.GetMethod("SomeMethod2");
      IACSharpSensor.IACSharpSensor.SensorReached(58);
      if (mi.Attributes != (MethodAttributes.PrivateScope | MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.HideBySig | MethodAttributes.VtableLayoutMask)) {
        System.Int32 RNTRNTRNT_13 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(59);
        return RNTRNTRNT_13;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      Console.WriteLine("OK");
      System.Int32 RNTRNTRNT_14 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(61);
      return RNTRNTRNT_14;
    }
  }
}
