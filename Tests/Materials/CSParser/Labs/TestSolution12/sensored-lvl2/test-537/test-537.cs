using System;
class Base
{
  protected string H {
    get {
      System.String RNTRNTRNT_40 = "Base.H";
      IACSharpSensor.IACSharpSensor.SensorReached(291);
      return RNTRNTRNT_40;
    }
  }
}
class X
{
  class Derived : Base
  {
    public class Nested : Base
    {
      public void G()
      {
        IACSharpSensor.IACSharpSensor.SensorReached(292);
        Derived[] d = new Derived[0];
        Console.WriteLine(d[0].H);
        IACSharpSensor.IACSharpSensor.SensorReached(293);
      }
    }
  }
}
class Derived : Base
{
  public class Nested : Base
  {
    public void G()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(294);
      Derived d = new Derived();
      Console.WriteLine(d.H);
      IACSharpSensor.IACSharpSensor.SensorReached(295);
    }
  }
}
class Test
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(296);
  }
}
