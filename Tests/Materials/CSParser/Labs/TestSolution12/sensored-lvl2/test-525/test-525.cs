using System;
class X
{
  ~X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(234);
    Console.WriteLine("DESTRUCTOR!");
    IACSharpSensor.IACSharpSensor.SensorReached(235);
  }
  public static int Test1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(236);
    try {
      System.Int32 RNTRNTRNT_36 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(237);
      return RNTRNTRNT_36;
    } catch (Exception) {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    System.Console.WriteLine("Shouldn't get here");
    System.Int32 RNTRNTRNT_37 = 9;
    IACSharpSensor.IACSharpSensor.SensorReached(239);
    return RNTRNTRNT_37;
  }
  public static void Test2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(240);
    int[] vars = {
      3,
      4,
      5
    };
    IACSharpSensor.IACSharpSensor.SensorReached(241);
    foreach (int a in vars) {
      IACSharpSensor.IACSharpSensor.SensorReached(242);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(243);
        continue;
      } catch (Exception) {
        IACSharpSensor.IACSharpSensor.SensorReached(244);
        break;
        IACSharpSensor.IACSharpSensor.SensorReached(245);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(246);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(247);
    Test1();
    Test2();
    IACSharpSensor.IACSharpSensor.SensorReached(248);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(249);
      return;
    } catch (Exception) {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(250);
    System.Console.WriteLine("Shouldn't get here");
    IACSharpSensor.IACSharpSensor.SensorReached(251);
    return;
  }
}
