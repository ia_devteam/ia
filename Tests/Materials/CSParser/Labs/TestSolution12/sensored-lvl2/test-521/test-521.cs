using System;
public class Tests
{
  public delegate void CallTargetWithContextN(object o, params object[] args);
  public static void CallWithContextN(object o, object[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(194);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(195);
    object o = new CallTargetWithContextN(CallWithContextN);
    IACSharpSensor.IACSharpSensor.SensorReached(196);
  }
}
