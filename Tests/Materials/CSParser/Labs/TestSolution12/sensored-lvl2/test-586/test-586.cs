class Program
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(548);
    MyColor[] c = new MyColor[1];
    c[0] += new MyColor(1.3f);
    c[0] += new MyColor(1.5f);
    IACSharpSensor.IACSharpSensor.SensorReached(549);
    if (c[0].Value != 2.8f) {
      System.Int32 RNTRNTRNT_103 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(550);
      return RNTRNTRNT_103;
    }
    System.Int32 RNTRNTRNT_104 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(551);
    return RNTRNTRNT_104;
  }
  public struct MyColor
  {
    private float _value;
    public MyColor(float value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(552);
      _value = value;
      IACSharpSensor.IACSharpSensor.SensorReached(553);
    }
    public float Value {
      get {
        System.Single RNTRNTRNT_105 = _value;
        IACSharpSensor.IACSharpSensor.SensorReached(554);
        return RNTRNTRNT_105;
      }
    }
    public static MyColor operator +(MyColor a, MyColor b)
    {
      MyColor RNTRNTRNT_106 = new MyColor(a._value + b._value);
      IACSharpSensor.IACSharpSensor.SensorReached(555);
      return RNTRNTRNT_106;
    }
  }
}
