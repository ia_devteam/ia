using System;
class Test
{
  const bool bt = (false == false) & (false != true) & (true != false) & (true == true);
  const bool bf = (false != false) | (false == true) | (true == false) | (true != true);
  static void True(bool b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(272);
    False(!b);
    IACSharpSensor.IACSharpSensor.SensorReached(273);
  }
  static void False(bool b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(274);
    if (b) {
      IACSharpSensor.IACSharpSensor.SensorReached(275);
      throw new System.Exception();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(276);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(277);
    True(false == false);
    False(false == true);
    False(true == false);
    True(true == true);
    False(false != false);
    True(false != true);
    True(true != false);
    False(true != true);
    True(bt);
    False(bf);
    IACSharpSensor.IACSharpSensor.SensorReached(278);
  }
}
