public class TestClass
{
  delegate void OneDelegate(int i);
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(393);
    OneDelegate d = new OneDelegate(TestMethod);
    d.Invoke(1);
    IACSharpSensor.IACSharpSensor.SensorReached(394);
  }
  public static void TestMethod(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(395);
  }
}
