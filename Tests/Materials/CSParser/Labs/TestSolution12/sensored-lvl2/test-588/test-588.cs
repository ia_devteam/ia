using System;
using TestNamespace;
namespace TestNamespace
{
  public class TestClass
  {
    public static void HelloWorld()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(564);
    }
  }
}
class SuperClass
{
  TestClass tc = null;
  TestClass TestClass {
    get {
      TestClass RNTRNTRNT_111 = tc;
      IACSharpSensor.IACSharpSensor.SensorReached(565);
      return RNTRNTRNT_111;
    }
  }
}
class SubClass : SuperClass
{
  public SubClass()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(566);
    TestClass.HelloWorld();
    IACSharpSensor.IACSharpSensor.SensorReached(567);
  }
}
class App
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(568);
    SubClass sc = new SubClass();
    IACSharpSensor.IACSharpSensor.SensorReached(569);
  }
}
