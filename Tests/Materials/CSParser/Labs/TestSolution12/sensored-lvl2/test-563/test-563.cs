using System;
using System.Collections;
public class Test
{
  internal object TestMethod(TestCollection t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(414);
    foreach (object x in t) {
      System.Object RNTRNTRNT_70 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(415);
      return RNTRNTRNT_70;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(416);
    return null;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(417);
  }
}
interface ITestCollection : IEnumerable
{
  new IEnumerator GetEnumerator();
}
interface TestCollection : ITestCollection
{
}
