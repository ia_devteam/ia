using System;
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(577);
    X x = new X();
    System.Int32 RNTRNTRNT_113 = x.Do("a", "b", "c");
    IACSharpSensor.IACSharpSensor.SensorReached(578);
    return RNTRNTRNT_113;
  }
  string str = "start";
  string Foo()
  {
    System.String RNTRNTRNT_114 = "s";
    IACSharpSensor.IACSharpSensor.SensorReached(579);
    return RNTRNTRNT_114;
  }
  string Prop {
    get {
      System.String RNTRNTRNT_115 = str;
      IACSharpSensor.IACSharpSensor.SensorReached(580);
      return RNTRNTRNT_115;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(581);
      str = value;
      IACSharpSensor.IACSharpSensor.SensorReached(582);
    }
  }
  string this[int i] {
    get {
      System.String RNTRNTRNT_116 = str;
      IACSharpSensor.IACSharpSensor.SensorReached(583);
      return RNTRNTRNT_116;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(584);
      str = value;
      IACSharpSensor.IACSharpSensor.SensorReached(585);
    }
  }
  int Do(string a, string b, string c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(586);
    str += Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(587);
    if (str != "starts") {
      System.Int32 RNTRNTRNT_117 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(588);
      return RNTRNTRNT_117;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(589);
    str += a + "," + b + "," + c;
    IACSharpSensor.IACSharpSensor.SensorReached(590);
    if (str != "startsa,b,c") {
      System.Int32 RNTRNTRNT_118 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(591);
      return RNTRNTRNT_118;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(592);
    Prop += a;
    IACSharpSensor.IACSharpSensor.SensorReached(593);
    if (str != "startsa,b,ca") {
      System.Int32 RNTRNTRNT_119 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(594);
      return RNTRNTRNT_119;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(595);
    Prop += a + "," + b + "," + c;
    IACSharpSensor.IACSharpSensor.SensorReached(596);
    if (str != "startsa,b,caa,b,c") {
      System.Int32 RNTRNTRNT_120 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(597);
      return RNTRNTRNT_120;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(598);
    this[0] += a + "," + b + "," + c;
    IACSharpSensor.IACSharpSensor.SensorReached(599);
    if (str != "startsa,b,caa,b,ca,b,c") {
      System.Int32 RNTRNTRNT_121 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(600);
      return RNTRNTRNT_121;
    }
    System.Int32 RNTRNTRNT_122 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(601);
    return RNTRNTRNT_122;
  }
}
