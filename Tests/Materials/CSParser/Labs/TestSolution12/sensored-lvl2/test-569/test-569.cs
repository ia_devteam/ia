using System;
[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
public class MyAttribute : Attribute
{
}
public class SubAttribute : MyAttribute
{
}
public class test
{
  [SubAttribute()]
  [SubAttribute()]
  public void method()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(442);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(443);
  }
}
