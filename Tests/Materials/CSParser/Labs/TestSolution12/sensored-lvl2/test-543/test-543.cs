using System;
class BetterMethod
{
  public int this[params bool[] args] {
    get {
      System.Int32 RNTRNTRNT_50 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(325);
      return RNTRNTRNT_50;
    }
  }
  public string this[bool a, object b] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(326);
      throw new NotImplementedException();
    }
  }
}
class MainClass
{
  public int this[int expectedLength, params string[] items] {
    get {
      System.Int32 RNTRNTRNT_51 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(327);
      return RNTRNTRNT_51;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(328);
      if (expectedLength != items.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(329);
        throw new ArgumentException(expectedLength + " != " + items.Length);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(330);
    }
  }
  public object this[int expectedLength, params object[] items] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(331);
      return null;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(332);
      if (expectedLength != items.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(333);
        throw new ArgumentException(expectedLength + " != " + items.Length);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(334);
    }
  }
  public bool this[int expectedLength, bool isNull, params object[] items] {
    get {
      System.Boolean RNTRNTRNT_52 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(335);
      return RNTRNTRNT_52;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(336);
      if (expectedLength != items.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(337);
        throw new ArgumentException(expectedLength + " != " + items.Length);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(338);
    }
  }
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(339);
    MainClass t = new MainClass();
    t[2, "foo", "doo"] = 2;
    t[3, new object[3]] = null;
    t[2, new int[] { 1 }, new string[] {
      "c",
      "b",
      "a"
    }] = null;
    t[0, true] = t[0, true];
    t[1, false, "foo"] = t[0, false, "foo", "doo"];
    ExternClass e = new ExternClass();
    e["a", "b", "b"] = false;
    BetterMethod bm = new BetterMethod();
    Console.WriteLine(bm[true, false]);
    IACSharpSensor.IACSharpSensor.SensorReached(340);
  }
}
