class A
{
  public static implicit operator byte(A mask)
  {
    System.Byte RNTRNTRNT_42 = 22;
    IACSharpSensor.IACSharpSensor.SensorReached(300);
    return RNTRNTRNT_42;
  }
}
public class Constraint
{
  const A lm = null;
  enum E1 : int
  {
    A
  }
  enum E2 : byte
  {
    A
  }
  public static Constraint operator !(Constraint m)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(301);
    return null;
  }
  public static Constraint operator +(Constraint m)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(302);
    return null;
  }
  public static Constraint operator ~(Constraint m)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(303);
    return null;
  }
  public static Constraint operator -(Constraint m)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(304);
    return null;
  }
  static void Foo(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(305);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(306);
    Foo(!(Constraint)null);
    Foo(~(Constraint)null);
    Foo(+(Constraint)null);
    Foo(-(Constraint)null);
    const byte b1 = +0;
    const byte b2 = +b1;
    const byte b3 = (byte)0;
    const int a = -2147483648;
    const long l = -9223372036854775808L;
    const long l2 = -uint.MaxValue;
    const E1 e = (E1)~E2.A;
    unchecked {
      IACSharpSensor.IACSharpSensor.SensorReached(307);
      if (-int.MinValue != int.MinValue) {
        System.Int32 RNTRNTRNT_43 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(308);
        return RNTRNTRNT_43;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(309);
    int b = -lm;
    IACSharpSensor.IACSharpSensor.SensorReached(310);
    if (b != -22) {
      System.Int32 RNTRNTRNT_44 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(311);
      return RNTRNTRNT_44;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(312);
    uint ua = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(313);
    if (-ua != -2) {
      System.Int32 RNTRNTRNT_45 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(314);
      return RNTRNTRNT_45;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(315);
    System.Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_46 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(316);
    return RNTRNTRNT_46;
  }
}
