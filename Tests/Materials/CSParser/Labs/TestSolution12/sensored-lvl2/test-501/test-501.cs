using System;
public class Tests
{
  public delegate bool FilterStackFrame(object o);
  public static void DumpException(FilterStackFrame fsf)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(9);
  }
  public static void foo(out bool continueInteraction)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(10);
    continueInteraction = false;
    IACSharpSensor.IACSharpSensor.SensorReached(11);
    try {
    } catch (Exception ex) {
      IACSharpSensor.IACSharpSensor.SensorReached(12);
      DumpException(delegate(object o) { return true; });
      IACSharpSensor.IACSharpSensor.SensorReached(13);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(14);
  }
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(15);
    bool b;
    foo(out b);
    IACSharpSensor.IACSharpSensor.SensorReached(16);
  }
}
