using System;
class FakeInt
{
  private long _value;
  public FakeInt(long val)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(188);
    _value = val;
    IACSharpSensor.IACSharpSensor.SensorReached(189);
  }
  public static implicit operator long(FakeInt self)
  {
    System.Int64 RNTRNTRNT_33 = self._value;
    IACSharpSensor.IACSharpSensor.SensorReached(190);
    return RNTRNTRNT_33;
  }
}
class MainClass
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(191);
    if (new FakeInt(42) != 42) {
      IACSharpSensor.IACSharpSensor.SensorReached(192);
      throw new Exception();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(193);
  }
}
