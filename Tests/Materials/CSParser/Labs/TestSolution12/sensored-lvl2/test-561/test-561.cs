abstract class A : I
{
  protected abstract void M();
  void I.M()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(407);
  }
}
interface I
{
  void M();
}
class C : A, I
{
  protected override void M()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(408);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(409);
  }
}
