using System;
public struct SymbolId
{
}
public interface IAttributesCollection
{
  object this[SymbolId name] { get; set; }
}
class AttributesCollection : IAttributesCollection
{
  public object this[SymbolId name] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(650);
      return null;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(651); }
  }
}
class Program
{
  public static object SetDictionaryValue(object self, SymbolId name, object value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(652);
    IAttributesCollection dict = new AttributesCollection();
    System.Object RNTRNTRNT_133 = dict[name] = value;
    IACSharpSensor.IACSharpSensor.SensorReached(653);
    return RNTRNTRNT_133;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(654);
    SetDictionaryValue(null, new SymbolId(), 1);
    IACSharpSensor.IACSharpSensor.SensorReached(655);
  }
}
