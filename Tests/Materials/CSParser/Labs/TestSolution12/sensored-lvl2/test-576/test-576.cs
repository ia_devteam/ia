class Foo
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(469);
    int a = 0;
    int b = 5;
    a += -b;
    IACSharpSensor.IACSharpSensor.SensorReached(470);
    if (a != -5) {
      IACSharpSensor.IACSharpSensor.SensorReached(471);
      throw new System.Exception();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(472);
  }
}
