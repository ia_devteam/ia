using System;
interface IFoo
{
}
class Bar
{
}
class Program
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(444);
    IFoo foo = null;
    IACSharpSensor.IACSharpSensor.SensorReached(445);
    if (foo is IFoo) {
      IACSharpSensor.IACSharpSensor.SensorReached(446);
      Console.WriteLine("got an IFoo");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(447);
    Bar bar = null;
    IACSharpSensor.IACSharpSensor.SensorReached(448);
    if (bar is Bar) {
      IACSharpSensor.IACSharpSensor.SensorReached(449);
      Console.WriteLine("got a bar");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(450);
  }
}
