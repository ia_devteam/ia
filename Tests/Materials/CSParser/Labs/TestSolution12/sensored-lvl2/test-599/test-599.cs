using System;
namespace Test
{
  using Text = System.Text;
  using Str = System.String;
  public class String
  {
    string s;
    public String(string s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(668);
      this.s = s;
      IACSharpSensor.IACSharpSensor.SensorReached(669);
    }
    public static implicit operator String(string s1)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(670);
      if (s1 == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(671);
        return null;
      }
      String RNTRNTRNT_136 = new String(s1);
      IACSharpSensor.IACSharpSensor.SensorReached(672);
      return RNTRNTRNT_136;
    }
    public static implicit operator Str(String s1)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(673);
      if (s1 == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(674);
        return null;
      }
      Str RNTRNTRNT_137 = s1.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(675);
      return RNTRNTRNT_137;
    }
  }
}
namespace TestCompiler
{
  using String = Test.String;
  class MainClass
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(676);
      String a = "a";
      int i = 1;
      a += i;
      string s = i + a;
      Console.WriteLine(s);
      IACSharpSensor.IACSharpSensor.SensorReached(677);
      if (s != "1Test.String") {
        System.Int32 RNTRNTRNT_138 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(678);
        return RNTRNTRNT_138;
      }
      System.Int32 RNTRNTRNT_139 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(679);
      return RNTRNTRNT_139;
    }
  }
}
