using System;
namespace Bugs
{
  class Bug2
  {
    struct MyByte
    {
      private byte value;
      public MyByte(byte value)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(397);
        this.value = value;
        IACSharpSensor.IACSharpSensor.SensorReached(398);
      }
      public static implicit operator MyByte(byte value)
      {
        MyByte RNTRNTRNT_64 = new MyByte(value);
        IACSharpSensor.IACSharpSensor.SensorReached(399);
        return RNTRNTRNT_64;
      }
      public static implicit operator byte(MyByte b)
      {
        System.Byte RNTRNTRNT_65 = b.value;
        IACSharpSensor.IACSharpSensor.SensorReached(400);
        return RNTRNTRNT_65;
      }
    }
    struct MyInt
    {
      private int value;
      public MyInt(int value)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(401);
        this.value = value;
        IACSharpSensor.IACSharpSensor.SensorReached(402);
      }
      public static implicit operator MyInt(int value)
      {
        MyInt RNTRNTRNT_66 = new MyInt(value);
        IACSharpSensor.IACSharpSensor.SensorReached(403);
        return RNTRNTRNT_66;
      }
      public static implicit operator int(MyInt b)
      {
        System.Int32 RNTRNTRNT_67 = b.value;
        IACSharpSensor.IACSharpSensor.SensorReached(404);
        return RNTRNTRNT_67;
      }
    }
    public static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(405);
      MyByte b = 255;
      b += 255;
      Console.WriteLine(b);
      MyInt i = 3;
      i &= (4 + i);
      Console.WriteLine(i);
      IACSharpSensor.IACSharpSensor.SensorReached(406);
    }
  }
}
