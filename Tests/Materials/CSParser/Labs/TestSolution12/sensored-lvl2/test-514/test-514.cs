using System;
class X
{
  public static void HandleConflict(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(138);
    if (a != 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(139);
      goto throwException;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(140);
    if (a != 2) {
      IACSharpSensor.IACSharpSensor.SensorReached(141);
      goto throwException;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(142);
    return;
    throwException:
    IACSharpSensor.IACSharpSensor.SensorReached(143);
    throw new Exception();
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(144);
    int ret = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(145);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(146);
      HandleConflict(1);
    } catch {
      IACSharpSensor.IACSharpSensor.SensorReached(147);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(148);
        HandleConflict(2);
      } catch {
        IACSharpSensor.IACSharpSensor.SensorReached(149);
        ret = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(150);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(151);
    }
    System.Int32 RNTRNTRNT_28 = ret;
    IACSharpSensor.IACSharpSensor.SensorReached(152);
    return RNTRNTRNT_28;
  }
}
