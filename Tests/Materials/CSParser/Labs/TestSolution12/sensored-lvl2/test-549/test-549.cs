public delegate object Get(Do d);
public class Do
{
  public void Register(Get g)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(359);
  }
  public void Register(object g)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(360);
  }
  static object MyGet(Do d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(361);
    return null;
  }
  public void X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(362);
    Register(Do.MyGet);
    IACSharpSensor.IACSharpSensor.SensorReached(363);
  }
}
public class User
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(364);
  }
}
