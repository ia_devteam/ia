using System;
public class Foo
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(95);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(96);
      f();
    } catch {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(97);
  }
  static void f()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(98);
    throw new Exception();
    IACSharpSensor.IACSharpSensor.SensorReached(99);
    string hi = "";
    IACSharpSensor.IACSharpSensor.SensorReached(100);
    try {
    } finally {
      IACSharpSensor.IACSharpSensor.SensorReached(101);
      Console.WriteLine("hi = {0}", hi);
      IACSharpSensor.IACSharpSensor.SensorReached(102);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(103);
  }
}
