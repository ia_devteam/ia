using System;
using System.Reflection;
using System.Runtime.InteropServices;
class Program
{
  [DllImport("foo.dll")]
  public static extern void printf(string format, __arglist __arglist);
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(410);
    if (typeof(Program).GetMethod("printf").CallingConvention != CallingConventions.VarArgs) {
      System.Int32 RNTRNTRNT_68 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(411);
      return RNTRNTRNT_68;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(412);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_69 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    return RNTRNTRNT_69;
  }
}
