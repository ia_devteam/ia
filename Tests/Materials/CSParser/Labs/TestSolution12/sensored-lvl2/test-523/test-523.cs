using System;
class T
{
  int stuff()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(198);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(199);
      throw new Exception();
    } finally {
      IACSharpSensor.IACSharpSensor.SensorReached(200);
      stuff_finally();
      IACSharpSensor.IACSharpSensor.SensorReached(201);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(202);
  }
  int stuff2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(204);
      throw new Exception();
    } catch {
      IACSharpSensor.IACSharpSensor.SensorReached(205);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(206);
        throw new Exception();
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(207);
        stuff_finally();
        IACSharpSensor.IACSharpSensor.SensorReached(208);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(209);
    } finally {
      IACSharpSensor.IACSharpSensor.SensorReached(210);
      stuff_finally();
      IACSharpSensor.IACSharpSensor.SensorReached(211);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(212);
  }
  int stuff3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(213);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(214);
      throw new Exception();
    } catch {
      IACSharpSensor.IACSharpSensor.SensorReached(215);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(216);
        throw new Exception();
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(217);
        stuff_finally();
        IACSharpSensor.IACSharpSensor.SensorReached(218);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(219);
    } finally {
      IACSharpSensor.IACSharpSensor.SensorReached(220);
      stuff_finally();
      IACSharpSensor.IACSharpSensor.SensorReached(221);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(222);
  }
  void stuff4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(223);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(224);
      throw new Exception();
    } catch {
      IACSharpSensor.IACSharpSensor.SensorReached(225);
      throw;
      IACSharpSensor.IACSharpSensor.SensorReached(226);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(227);
  }
  void stuff_finally()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(228);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(229);
  }
}
