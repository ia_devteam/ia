using System;
namespace Bug
{
  unsafe struct Demo
  {
    fixed bool test[4];
    bool Fixed()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(602);
      fixed (bool* data_ptr = test) {
        System.Boolean RNTRNTRNT_123 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(603);
        return RNTRNTRNT_123;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(604);
    }
    static bool Foo(int[] data)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(605);
      fixed (int* data_ptr = data) {
        System.Boolean RNTRNTRNT_124 = data_ptr == null ? true : false;
        IACSharpSensor.IACSharpSensor.SensorReached(606);
        return RNTRNTRNT_124;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(607);
    }
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(608);
      if (!Foo(null)) {
        System.Int32 RNTRNTRNT_125 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(609);
        return RNTRNTRNT_125;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(610);
      if (!Foo(new int[0])) {
        System.Int32 RNTRNTRNT_126 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(611);
        return RNTRNTRNT_126;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(612);
      if (!new Demo().Fixed()) {
        System.Int32 RNTRNTRNT_127 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(613);
        return RNTRNTRNT_127;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(614);
      Console.WriteLine("OK");
      System.Int32 RNTRNTRNT_128 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(615);
      return RNTRNTRNT_128;
    }
  }
}
