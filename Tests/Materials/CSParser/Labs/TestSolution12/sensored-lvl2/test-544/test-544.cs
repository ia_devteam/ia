enum ByteEnum : byte
{
  One = 1,
  Two = 2
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(342);
    ByteEnum b = ByteEnum.One;
    IACSharpSensor.IACSharpSensor.SensorReached(343);
    switch (b) {
      case ByteEnum.One:
        IACSharpSensor.IACSharpSensor.SensorReached(344);
        return;
      case ByteEnum.One | ByteEnum.Two:
        IACSharpSensor.IACSharpSensor.SensorReached(345);
        return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(346);
  }
}
