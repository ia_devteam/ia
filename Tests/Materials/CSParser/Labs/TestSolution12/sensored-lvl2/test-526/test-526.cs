using System;
interface IList
{
  int Count();
}
interface ICounter
{
  int Count { set; }
}
interface IListCounter : IList, ICounter
{
}
interface IA
{
  int Value();
}
interface IB : IA
{
  new int Value { get; }
}
interface IC : IB
{
  new int Value { get; }
}
interface IBB : IList, ICounter
{
}
interface ICC : IBB
{
}
interface IM1
{
  void Add(int arg);
}
interface IM2 : IM1
{
  int Add(int arg, bool now);
}
class Test
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(252);
  }
  static void Foo(IListCounter t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(253);
    t.Count();
    IACSharpSensor.IACSharpSensor.SensorReached(254);
  }
  void Foo2(IC b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(255);
    int i = b.Value;
    IACSharpSensor.IACSharpSensor.SensorReached(256);
  }
  void Foo3(ICC c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(257);
    c.Count();
    IACSharpSensor.IACSharpSensor.SensorReached(258);
  }
  void Foo4(IM2 im2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(259);
    im2.Add(2);
    IACSharpSensor.IACSharpSensor.SensorReached(260);
  }
}
