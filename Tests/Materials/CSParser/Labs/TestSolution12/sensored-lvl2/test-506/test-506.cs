using System;
namespace ProtectedSetter
{
  public abstract class BaseClass
  {
    public abstract string Name { get; internal set; }
  }
  public class DerivedClass : BaseClass
  {
    public override String Name {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(48);
        return null;
      }
      internal set { IACSharpSensor.IACSharpSensor.SensorReached(49); }
    }
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(50);
    }
  }
}
