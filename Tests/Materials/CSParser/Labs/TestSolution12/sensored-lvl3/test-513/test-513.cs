using System;
using System.IO;
using System.Reflection;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(45);
    Assembly a = Assembly.GetExecutingAssembly();
    string[] resourceNames = a.GetManifestResourceNames();
    if (resourceNames.Length != 3) {
      System.Int32 RNTRNTRNT_15 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(46);
      return RNTRNTRNT_15;
    }
    if (resourceNames[0] != "test-513.cs") {
      System.Int32 RNTRNTRNT_16 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(47);
      return RNTRNTRNT_16;
    }
    if (resourceNames[1] != "test-512.cs") {
      System.Int32 RNTRNTRNT_17 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(48);
      return RNTRNTRNT_17;
    }
    if (resourceNames[2] != "test") {
      System.Int32 RNTRNTRNT_18 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(49);
      return RNTRNTRNT_18;
    }
    FileStream f = a.GetFile("test-513.cs");
    if (f == null) {
      System.Int32 RNTRNTRNT_19 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(50);
      return RNTRNTRNT_19;
    }
    f = a.GetFile("test-512.cs");
    if (f == null) {
      System.Int32 RNTRNTRNT_20 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(51);
      return RNTRNTRNT_20;
    }
    f = a.GetFile("test-511.cs");
    if (f == null) {
      System.Int32 RNTRNTRNT_21 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(52);
      return RNTRNTRNT_21;
    }
    f = a.GetFile("test");
    if (f != null) {
      System.Int32 RNTRNTRNT_22 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(53);
      return RNTRNTRNT_22;
    }
    Stream s = a.GetManifestResourceStream("test-513.cs");
    if (s == null) {
      System.Int32 RNTRNTRNT_23 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      return RNTRNTRNT_23;
    }
    s = a.GetManifestResourceStream("test-512.cs");
    if (s == null) {
      System.Int32 RNTRNTRNT_24 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(55);
      return RNTRNTRNT_24;
    }
    s = a.GetManifestResourceStream("test");
    if (s == null) {
      System.Int32 RNTRNTRNT_25 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(56);
      return RNTRNTRNT_25;
    }
    s = a.GetManifestResourceStream("test-511.cs");
    if (s != null) {
      System.Int32 RNTRNTRNT_26 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(57);
      return RNTRNTRNT_26;
    }
    System.Int32 RNTRNTRNT_27 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(58);
    return RNTRNTRNT_27;
  }
}
