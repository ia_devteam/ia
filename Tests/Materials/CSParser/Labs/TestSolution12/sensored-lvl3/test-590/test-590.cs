using System;
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(317);
    X x = new X();
    System.Int32 RNTRNTRNT_113 = x.Do("a", "b", "c");
    IACSharpSensor.IACSharpSensor.SensorReached(318);
    return RNTRNTRNT_113;
  }
  string str = "start";
  string Foo()
  {
    System.String RNTRNTRNT_114 = "s";
    IACSharpSensor.IACSharpSensor.SensorReached(319);
    return RNTRNTRNT_114;
  }
  string Prop {
    get {
      System.String RNTRNTRNT_115 = str;
      IACSharpSensor.IACSharpSensor.SensorReached(320);
      return RNTRNTRNT_115;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(321);
      str = value;
      IACSharpSensor.IACSharpSensor.SensorReached(322);
    }
  }
  string this[int i] {
    get {
      System.String RNTRNTRNT_116 = str;
      IACSharpSensor.IACSharpSensor.SensorReached(323);
      return RNTRNTRNT_116;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(324);
      str = value;
      IACSharpSensor.IACSharpSensor.SensorReached(325);
    }
  }
  int Do(string a, string b, string c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(326);
    str += Foo();
    if (str != "starts") {
      System.Int32 RNTRNTRNT_117 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(327);
      return RNTRNTRNT_117;
    }
    str += a + "," + b + "," + c;
    if (str != "startsa,b,c") {
      System.Int32 RNTRNTRNT_118 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(328);
      return RNTRNTRNT_118;
    }
    Prop += a;
    if (str != "startsa,b,ca") {
      System.Int32 RNTRNTRNT_119 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(329);
      return RNTRNTRNT_119;
    }
    Prop += a + "," + b + "," + c;
    if (str != "startsa,b,caa,b,c") {
      System.Int32 RNTRNTRNT_120 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(330);
      return RNTRNTRNT_120;
    }
    this[0] += a + "," + b + "," + c;
    if (str != "startsa,b,caa,b,ca,b,c") {
      System.Int32 RNTRNTRNT_121 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(331);
      return RNTRNTRNT_121;
    }
    System.Int32 RNTRNTRNT_122 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(332);
    return RNTRNTRNT_122;
  }
}
