using System;
class T
{
  int stuff()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(84);
    try {
      throw new Exception();
    } finally {
      stuff_finally();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(85);
  }
  int stuff2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(86);
    try {
      throw new Exception();
    } catch {
      try {
        throw new Exception();
      } finally {
        stuff_finally();
      }
    } finally {
      stuff_finally();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(87);
  }
  int stuff3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(88);
    try {
      throw new Exception();
    } catch {
      try {
        throw new Exception();
      } finally {
        stuff_finally();
      }
    } finally {
      stuff_finally();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(89);
  }
  void stuff4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(90);
    try {
      throw new Exception();
    } catch {
      throw;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(91);
  }
  void stuff_finally()
  {
  }
  static void Main()
  {
  }
}
