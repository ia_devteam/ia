class Foo
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(69);
    int ret = 1;
    try {
      goto done;
    } finally {
      ret = 0;
    }
    done:
    System.Int32 RNTRNTRNT_30 = ret;
    IACSharpSensor.IACSharpSensor.SensorReached(70);
    return RNTRNTRNT_30;
  }
}
