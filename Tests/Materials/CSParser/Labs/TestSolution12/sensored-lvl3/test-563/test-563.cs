using System;
using System.Collections;
public class Test
{
  internal object TestMethod(TestCollection t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    foreach (object x in t) {
      System.Object RNTRNTRNT_70 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(216);
      return RNTRNTRNT_70;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(217);
    return null;
  }
  public static void Main()
  {
  }
}
interface ITestCollection : IEnumerable
{
  new IEnumerator GetEnumerator();
}
interface TestCollection : ITestCollection
{
}
