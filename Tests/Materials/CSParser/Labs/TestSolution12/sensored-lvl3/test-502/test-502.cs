class Base
{
  public int Property {
    get {
      System.Int32 RNTRNTRNT_3 = 42;
      IACSharpSensor.IACSharpSensor.SensorReached(8);
      return RNTRNTRNT_3;
    }
  }
  static void Main()
  {
  }
}
class Derived : Base
{
  public int get_Property()
  {
    System.Int32 RNTRNTRNT_4 = 42;
    IACSharpSensor.IACSharpSensor.SensorReached(9);
    return RNTRNTRNT_4;
  }
}
class BaseClass
{
  protected virtual int Value {
    get {
      System.Int32 RNTRNTRNT_5 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(10);
      return RNTRNTRNT_5;
    }
    set { }
  }
}
abstract class DerivedClass : BaseClass
{
  protected int get_Value()
  {
    System.Int32 RNTRNTRNT_6 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(11);
    return RNTRNTRNT_6;
  }
}
class ErrorClass : DerivedClass
{
  protected override int Value {
    get {
      System.Int32 RNTRNTRNT_7 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(12);
      return RNTRNTRNT_7;
    }
    set { }
  }
}
