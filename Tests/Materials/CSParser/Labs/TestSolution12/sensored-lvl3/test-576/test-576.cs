class Foo
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    int a = 0;
    int b = 5;
    a += -b;
    if (a != -5) {
      throw new System.Exception();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(246);
  }
}
