using System;
using System.Reflection;
using System.Runtime.InteropServices;
namespace preservesig_test
{
  class Class1
  {
    static int Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(224);
      MethodInfo dofoo = typeof(TestClass).GetMethod("DoFoo");
      if ((dofoo.GetMethodImplementationFlags() & MethodImplAttributes.PreserveSig) == 0) {
        System.Int32 RNTRNTRNT_73 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(225);
        return RNTRNTRNT_73;
      }
      dofoo = typeof(TestClass).GetProperty("Foo").GetGetMethod();
      if ((dofoo.GetMethodImplementationFlags() & MethodImplAttributes.PreserveSig) == 0) {
        System.Int32 RNTRNTRNT_74 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(226);
        return RNTRNTRNT_74;
      }
      dofoo = typeof(TestClass).GetEvent("e").GetAddMethod(true);
      if ((dofoo.GetMethodImplementationFlags() & MethodImplAttributes.PreserveSig) == 0) {
        System.Int32 RNTRNTRNT_75 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(227);
        return RNTRNTRNT_75;
      }
      Console.WriteLine("Has PreserveSig");
      System.Int32 RNTRNTRNT_76 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(228);
      return RNTRNTRNT_76;
    }
  }
  public class TestClass
  {
    public delegate void D();
    [method: PreserveSig()]
    public event D e;
    [PreserveSig()]
    public int DoFoo()
    {
      System.Int32 RNTRNTRNT_77 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(229);
      return RNTRNTRNT_77;
    }
    public int Foo {
      [PreserveSig()]
      get {
        System.Int32 RNTRNTRNT_78 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(230);
        return RNTRNTRNT_78;
      }
    }
  }
}
