using System;
using TestNamespace;
namespace TestNamespace
{
  public class TestClass
  {
    public static void HelloWorld()
    {
    }
  }
}
class SuperClass
{
  TestClass tc = null;
  TestClass TestClass {
    get {
      TestClass RNTRNTRNT_111 = tc;
      IACSharpSensor.IACSharpSensor.SensorReached(307);
      return RNTRNTRNT_111;
    }
  }
}
class SubClass : SuperClass
{
  public SubClass()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(308);
    TestClass.HelloWorld();
    IACSharpSensor.IACSharpSensor.SensorReached(309);
  }
}
class App
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(310);
    SubClass sc = new SubClass();
    IACSharpSensor.IACSharpSensor.SensorReached(311);
  }
}
