using System;
public class Tests
{
  public delegate void CallTargetWithContextN(object o, params object[] args);
  public static void CallWithContextN(object o, object[] args)
  {
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(82);
    object o = new CallTargetWithContextN(CallWithContextN);
    IACSharpSensor.IACSharpSensor.SensorReached(83);
  }
}
