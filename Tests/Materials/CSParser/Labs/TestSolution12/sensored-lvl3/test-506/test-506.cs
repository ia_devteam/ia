using System;
namespace ProtectedSetter
{
  public abstract class BaseClass
  {
    public abstract string Name { get; internal set; }
  }
  public class DerivedClass : BaseClass
  {
    public override String Name {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(23);
        return null;
      }
      internal set { }
    }
    static void Main()
    {
    }
  }
}
