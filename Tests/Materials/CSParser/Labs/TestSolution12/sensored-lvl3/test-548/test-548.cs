using System;
namespace Bugs
{
  class Bug0
  {
    struct MyBoolean
    {
      private bool value;
      public MyBoolean(bool value)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(174);
        this.value = value;
        IACSharpSensor.IACSharpSensor.SensorReached(175);
      }
      public static implicit operator MyBoolean(bool value)
      {
        MyBoolean RNTRNTRNT_53 = new MyBoolean(value);
        IACSharpSensor.IACSharpSensor.SensorReached(176);
        return RNTRNTRNT_53;
      }
      public static implicit operator bool(MyBoolean b)
      {
        System.Boolean RNTRNTRNT_54 = b.value;
        IACSharpSensor.IACSharpSensor.SensorReached(177);
        return RNTRNTRNT_54;
      }
    }
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(178);
      MyBoolean b = true;
      if (true && b) {
        System.Int32 RNTRNTRNT_55 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(179);
        return RNTRNTRNT_55;
      } else {
        System.Int32 RNTRNTRNT_56 = 100;
        IACSharpSensor.IACSharpSensor.SensorReached(180);
        return RNTRNTRNT_56;
      }
    }
  }
}
