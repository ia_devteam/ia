using System;
interface IList
{
  int Count();
}
interface ICounter
{
  int Count { set; }
}
interface IListCounter : IList, ICounter
{
}
interface IA
{
  int Value();
}
interface IB : IA
{
  new int Value { get; }
}
interface IC : IB
{
  new int Value { get; }
}
interface IBB : IList, ICounter
{
}
interface ICC : IBB
{
}
interface IM1
{
  void Add(int arg);
}
interface IM2 : IM1
{
  int Add(int arg, bool now);
}
class Test
{
  static void Main()
  {
  }
  static void Foo(IListCounter t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(105);
    t.Count();
    IACSharpSensor.IACSharpSensor.SensorReached(106);
  }
  void Foo2(IC b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(107);
    int i = b.Value;
    IACSharpSensor.IACSharpSensor.SensorReached(108);
  }
  void Foo3(ICC c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(109);
    c.Count();
    IACSharpSensor.IACSharpSensor.SensorReached(110);
  }
  void Foo4(IM2 im2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(111);
    im2.Add(2);
    IACSharpSensor.IACSharpSensor.SensorReached(112);
  }
}
