class A
{
  public static implicit operator byte(A mask)
  {
    System.Byte RNTRNTRNT_42 = 22;
    IACSharpSensor.IACSharpSensor.SensorReached(140);
    return RNTRNTRNT_42;
  }
}
public class Constraint
{
  const A lm = null;
  enum E1 : int
  {
    A
  }
  enum E2 : byte
  {
    A
  }
  public static Constraint operator !(Constraint m)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(141);
    return null;
  }
  public static Constraint operator +(Constraint m)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(142);
    return null;
  }
  public static Constraint operator ~(Constraint m)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(143);
    return null;
  }
  public static Constraint operator -(Constraint m)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(144);
    return null;
  }
  static void Foo(object o)
  {
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(145);
    Foo(!(Constraint)null);
    Foo(~(Constraint)null);
    Foo(+(Constraint)null);
    Foo(-(Constraint)null);
    const byte b1 = +0;
    const byte b2 = +b1;
    const byte b3 = (byte)0;
    const int a = -2147483648;
    const long l = -9223372036854775808L;
    const long l2 = -uint.MaxValue;
    const E1 e = (E1)~E2.A;
    unchecked {
      if (-int.MinValue != int.MinValue) {
        System.Int32 RNTRNTRNT_43 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(146);
        return RNTRNTRNT_43;
      }
    }
    int b = -lm;
    if (b != -22) {
      System.Int32 RNTRNTRNT_44 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(147);
      return RNTRNTRNT_44;
    }
    uint ua = 2;
    if (-ua != -2) {
      System.Int32 RNTRNTRNT_45 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(148);
      return RNTRNTRNT_45;
    }
    System.Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_46 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(149);
    return RNTRNTRNT_46;
  }
}
