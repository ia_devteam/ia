using System;
interface IFoo
{
}
class Bar
{
}
class Program
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(231);
    IFoo foo = null;
    if (foo is IFoo) {
      Console.WriteLine("got an IFoo");
    }
    Bar bar = null;
    if (bar is Bar) {
      Console.WriteLine("got a bar");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(232);
  }
}
