namespace TestNS
{
  public interface IHoge
  {
  }
  public class Foo
  {
  }
  public class XElement : Element
  {
    public new Bar Document {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(363);
        return null;
      }
    }
    public object CrashHere {
      get {
        System.Object RNTRNTRNT_134 = (Document.Root == this) ? null : "";
        IACSharpSensor.IACSharpSensor.SensorReached(364);
        return RNTRNTRNT_134;
      }
    }
  }
  public class Element
  {
    public Foo Document {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(365);
        return null;
      }
    }
  }
  public class Bar
  {
    public IHoge Root {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(366);
        return null;
      }
    }
  }
  public class C
  {
    public static void Main()
    {
    }
  }
}
