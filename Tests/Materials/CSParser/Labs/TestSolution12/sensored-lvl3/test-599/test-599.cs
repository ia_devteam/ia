using System;
namespace Test
{
  using Text = System.Text;
  using Str = System.String;
  public class String
  {
    string s;
    public String(string s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(370);
      this.s = s;
      IACSharpSensor.IACSharpSensor.SensorReached(371);
    }
    public static implicit operator String(string s1)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(372);
      if (s1 == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(373);
        return null;
      }
      String RNTRNTRNT_136 = new String(s1);
      IACSharpSensor.IACSharpSensor.SensorReached(374);
      return RNTRNTRNT_136;
    }
    public static implicit operator Str(String s1)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(375);
      if (s1 == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(376);
        return null;
      }
      Str RNTRNTRNT_137 = s1.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(377);
      return RNTRNTRNT_137;
    }
  }
}
namespace TestCompiler
{
  using String = Test.String;
  class MainClass
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(378);
      String a = "a";
      int i = 1;
      a += i;
      string s = i + a;
      Console.WriteLine(s);
      if (s != "1Test.String") {
        System.Int32 RNTRNTRNT_138 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(379);
        return RNTRNTRNT_138;
      }
      System.Int32 RNTRNTRNT_139 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(380);
      return RNTRNTRNT_139;
    }
  }
}
