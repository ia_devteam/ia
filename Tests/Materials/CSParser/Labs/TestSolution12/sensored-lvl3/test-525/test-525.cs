using System;
class X
{
  ~X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(95);
    Console.WriteLine("DESTRUCTOR!");
    IACSharpSensor.IACSharpSensor.SensorReached(96);
  }
  public static int Test1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(97);
    try {
      System.Int32 RNTRNTRNT_36 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(98);
      return RNTRNTRNT_36;
    } catch (Exception) {
    }
    System.Console.WriteLine("Shouldn't get here");
    System.Int32 RNTRNTRNT_37 = 9;
    IACSharpSensor.IACSharpSensor.SensorReached(99);
    return RNTRNTRNT_37;
  }
  public static void Test2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(100);
    int[] vars = {
      3,
      4,
      5
    };
    foreach (int a in vars) {
      try {
        continue;
      } catch (Exception) {
        break;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(101);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(102);
    Test1();
    Test2();
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(103);
      return;
    } catch (Exception) {
    }
    System.Console.WriteLine("Shouldn't get here");
    IACSharpSensor.IACSharpSensor.SensorReached(104);
    return;
  }
}
