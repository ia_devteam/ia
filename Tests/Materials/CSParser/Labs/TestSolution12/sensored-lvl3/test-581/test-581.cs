using System;
public class TestParams
{
  object this[params string[] idx] {
    get {
      System.Object RNTRNTRNT_97 = idx[0];
      IACSharpSensor.IACSharpSensor.SensorReached(274);
      return RNTRNTRNT_97;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(275);
      Console.WriteLine(value);
      if ((string)value != "A(B)") {
        throw new ApplicationException(value.ToString());
      }
      IACSharpSensor.IACSharpSensor.SensorReached(276);
    }
  }
  public void TestMethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(277);
    this["A"] += "(" + this["B"] + ")";
    this[new string[] { "A" }] += "(" + this["B"] + ")";
    IACSharpSensor.IACSharpSensor.SensorReached(278);
  }
}
public class TestNonParams
{
  object this[string idx] {
    get {
      System.Object RNTRNTRNT_98 = idx;
      IACSharpSensor.IACSharpSensor.SensorReached(279);
      return RNTRNTRNT_98;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(280);
      Console.WriteLine(value);
      if ((string)value != "A(B)") {
        throw new ApplicationException(value.ToString());
      }
      IACSharpSensor.IACSharpSensor.SensorReached(281);
    }
  }
  public void TestMethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(282);
    this["A"] += "(" + this["B"] + ")";
    IACSharpSensor.IACSharpSensor.SensorReached(283);
  }
}
public class M
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(284);
    new TestNonParams().TestMethod();
    new TestParams().TestMethod();
    System.Int32 RNTRNTRNT_99 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(285);
    return RNTRNTRNT_99;
  }
}
