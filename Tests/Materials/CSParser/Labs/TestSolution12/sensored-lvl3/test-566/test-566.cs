public class Test
{
  private C _vssItem;
  public string Spec {
    get {
      System.String RNTRNTRNT_72 = _vssItem.Spec;
      IACSharpSensor.IACSharpSensor.SensorReached(219);
      return RNTRNTRNT_72;
    }
  }
  void Foo(C c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    c.Checkout();
    IACSharpSensor.IACSharpSensor.SensorReached(221);
  }
  void Foo2(CC cc)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(222);
    cc.Spec = "aa";
    IACSharpSensor.IACSharpSensor.SensorReached(223);
  }
  public static void Main()
  {
  }
}
interface A
{
  void Checkout();
  string Spec { get; }
}
interface B : A
{
  new void Checkout();
  new string Spec { get; }
}
interface C : B
{
}
class CA
{
  public string Spec {
    set { }
  }
}
class CB : CA
{
  public new string Spec {
    set { }
  }
}
class CC : CB
{
}
