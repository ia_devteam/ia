enum ByteEnum : byte
{
  One = 1,
  Two = 2
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    ByteEnum b = ByteEnum.One;
    switch (b) {
      case ByteEnum.One:
        IACSharpSensor.IACSharpSensor.SensorReached(171);
        return;
      case ByteEnum.One | ByteEnum.Two:
        IACSharpSensor.IACSharpSensor.SensorReached(172);
        return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(173);
  }
}
