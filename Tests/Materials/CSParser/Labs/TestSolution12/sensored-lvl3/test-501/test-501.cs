using System;
public class Tests
{
  public delegate bool FilterStackFrame(object o);
  public static void DumpException(FilterStackFrame fsf)
  {
  }
  public static void foo(out bool continueInteraction)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(4);
    continueInteraction = false;
    try {
    } catch (Exception ex) {
      DumpException(delegate(object o) { return true; });
    }
    IACSharpSensor.IACSharpSensor.SensorReached(5);
  }
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(6);
    bool b;
    foo(out b);
    IACSharpSensor.IACSharpSensor.SensorReached(7);
  }
}
