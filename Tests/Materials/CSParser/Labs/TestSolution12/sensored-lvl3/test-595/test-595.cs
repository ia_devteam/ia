using System;
public struct SymbolId
{
}
public interface IAttributesCollection
{
  object this[SymbolId name] { get; set; }
}
class AttributesCollection : IAttributesCollection
{
  public object this[SymbolId name] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(356);
      return null;
    }
    set { }
  }
}
class Program
{
  public static object SetDictionaryValue(object self, SymbolId name, object value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(357);
    IAttributesCollection dict = new AttributesCollection();
    System.Object RNTRNTRNT_133 = dict[name] = value;
    IACSharpSensor.IACSharpSensor.SensorReached(358);
    return RNTRNTRNT_133;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(359);
    SetDictionaryValue(null, new SymbolId(), 1);
    IACSharpSensor.IACSharpSensor.SensorReached(360);
  }
}
