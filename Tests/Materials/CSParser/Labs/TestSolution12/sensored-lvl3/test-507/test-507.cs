using System;
using System.Reflection;
namespace NewslotVirtualFinal
{
  internal interface INewslotVirtualFinal
  {
    void SomeMethod();
    void SomeMethod2();
  }
  internal class NewslotVirtualFinal : INewslotVirtualFinal
  {
    private NewslotVirtualFinal()
    {
    }
    public void SomeMethod()
    {
    }
    public virtual void SomeMethod2()
    {
    }
  }
  class C
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(24);
      Type t = typeof(NewslotVirtualFinal);
      MethodInfo mi = t.GetMethod("SomeMethod");
      if (mi.Attributes != (MethodAttributes.PrivateScope | MethodAttributes.Public | MethodAttributes.Final | MethodAttributes.Virtual | MethodAttributes.HideBySig | MethodAttributes.VtableLayoutMask)) {
        System.Int32 RNTRNTRNT_12 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(25);
        return RNTRNTRNT_12;
      }
      mi = t.GetMethod("SomeMethod2");
      if (mi.Attributes != (MethodAttributes.PrivateScope | MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.HideBySig | MethodAttributes.VtableLayoutMask)) {
        System.Int32 RNTRNTRNT_13 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(26);
        return RNTRNTRNT_13;
      }
      Console.WriteLine("OK");
      System.Int32 RNTRNTRNT_14 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(27);
      return RNTRNTRNT_14;
    }
  }
}
