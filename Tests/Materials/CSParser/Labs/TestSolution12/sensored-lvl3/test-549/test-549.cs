public delegate object Get(Do d);
public class Do
{
  public void Register(Get g)
  {
  }
  public void Register(object g)
  {
  }
  static object MyGet(Do d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(181);
    return null;
  }
  public void X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(182);
    Register(Do.MyGet);
    IACSharpSensor.IACSharpSensor.SensorReached(183);
  }
}
public class User
{
  static void Main()
  {
  }
}
