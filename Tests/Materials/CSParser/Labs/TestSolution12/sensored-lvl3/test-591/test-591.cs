using System;
namespace Bug
{
  unsafe struct Demo
  {
    fixed bool test[4];
    bool Fixed()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(333);
      fixed (bool* data_ptr = test) {
        System.Boolean RNTRNTRNT_123 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(334);
        return RNTRNTRNT_123;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(335);
    }
    static bool Foo(int[] data)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(336);
      fixed (int* data_ptr = data) {
        System.Boolean RNTRNTRNT_124 = data_ptr == null ? true : false;
        IACSharpSensor.IACSharpSensor.SensorReached(337);
        return RNTRNTRNT_124;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(338);
    }
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(339);
      if (!Foo(null)) {
        System.Int32 RNTRNTRNT_125 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(340);
        return RNTRNTRNT_125;
      }
      if (!Foo(new int[0])) {
        System.Int32 RNTRNTRNT_126 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(341);
        return RNTRNTRNT_126;
      }
      if (!new Demo().Fixed()) {
        System.Int32 RNTRNTRNT_127 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(342);
        return RNTRNTRNT_127;
      }
      Console.WriteLine("OK");
      System.Int32 RNTRNTRNT_128 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(343);
      return RNTRNTRNT_128;
    }
  }
}
