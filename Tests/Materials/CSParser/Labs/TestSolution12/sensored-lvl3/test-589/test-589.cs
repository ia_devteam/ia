using System;
using TestNamespace;
namespace TestNamespace
{
  public class TestClass
  {
    public static void HelloWorld()
    {
    }
  }
}
class SuperClass
{
  TestClass tc = null;
  public TestClass TestClass {
    private get {
      TestClass RNTRNTRNT_112 = tc;
      IACSharpSensor.IACSharpSensor.SensorReached(312);
      return RNTRNTRNT_112;
    }
    set { }
  }
}
class SubClass : SuperClass
{
  public SubClass()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(313);
    TestClass.HelloWorld();
    IACSharpSensor.IACSharpSensor.SensorReached(314);
  }
}
class App
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(315);
    SubClass sc = new SubClass();
    IACSharpSensor.IACSharpSensor.SensorReached(316);
  }
}
