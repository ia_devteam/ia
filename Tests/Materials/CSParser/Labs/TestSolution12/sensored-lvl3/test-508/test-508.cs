using System;
class OutputParam
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(28);
    int a;
    Method(out a);
    Console.WriteLine(a);
    IACSharpSensor.IACSharpSensor.SensorReached(29);
  }
  public static void Method(out int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(30);
    int b;
    try {
      b = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(31);
      return;
    } finally {
      a = 6;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(32);
  }
}
