using System;
interface I
{
  void Finalize();
}
class MainClass
{
  void Foo(I i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(354);
    i.Finalize();
    IACSharpSensor.IACSharpSensor.SensorReached(355);
  }
  public static void Main()
  {
  }
}
