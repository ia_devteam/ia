public class TestClass
{
  delegate void OneDelegate(int i);
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(200);
    OneDelegate d = new OneDelegate(TestMethod);
    d.Invoke(1);
    IACSharpSensor.IACSharpSensor.SensorReached(201);
  }
  public static void TestMethod(int i)
  {
  }
}
