using System;
using System.Reflection;
[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
class SomeCustomAttribute : Attribute
{
  public SomeCustomAttribute()
  {
  }
}
class MainClass
{
  [SomeCustomAttribute()]
  public int a;
  [SomeCustomAttribute()]
  public int x, y;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
    Type t = typeof(MainClass);
    FieldInfo[] fia = t.GetFields();
    foreach (FieldInfo fi in fia) {
      object[] ca = fi.GetCustomAttributes(typeof(SomeCustomAttribute), false);
      System.Console.WriteLine("Field: {0} [{1}]", fi.Name, ca.Length);
      if (ca.Length != 1) {
        System.Int32 RNTRNTRNT_1 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(2);
        return RNTRNTRNT_1;
      }
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_2 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(3);
    return RNTRNTRNT_2;
  }
}
