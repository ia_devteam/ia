using System;
class Test
{
  const bool bt = (false == false) & (false != true) & (true != false) & (true == true);
  const bool bf = (false != false) | (false == true) | (true == false) | (true != true);
  static void True(bool b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(120);
    False(!b);
    IACSharpSensor.IACSharpSensor.SensorReached(121);
  }
  static void False(bool b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(122);
    if (b) {
      throw new System.Exception();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(123);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(124);
    True(false == false);
    False(false == true);
    False(true == false);
    True(true == true);
    False(false != false);
    True(false != true);
    True(true != false);
    False(true != true);
    True(bt);
    False(bf);
    IACSharpSensor.IACSharpSensor.SensorReached(125);
  }
}
