using System;
class X
{
  public static void HandleConflict(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(59);
    if (a != 1) {
      goto throwException;
    }
    if (a != 2) {
      goto throwException;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(60);
    return;
    throwException:
    throw new Exception();
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(61);
    int ret = 1;
    try {
      HandleConflict(1);
    } catch {
      try {
        HandleConflict(2);
      } catch {
        ret = 0;
      }
    }
    System.Int32 RNTRNTRNT_28 = ret;
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    return RNTRNTRNT_28;
  }
}
