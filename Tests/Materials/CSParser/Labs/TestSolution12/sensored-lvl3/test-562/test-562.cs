using System;
using System.Reflection;
using System.Runtime.InteropServices;
class Program
{
  [DllImport("foo.dll")]
  public static extern void printf(string format, __arglist __arglist);
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(212);
    if (typeof(Program).GetMethod("printf").CallingConvention != CallingConventions.VarArgs) {
      System.Int32 RNTRNTRNT_68 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(213);
      return RNTRNTRNT_68;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_69 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(214);
    return RNTRNTRNT_69;
  }
}
