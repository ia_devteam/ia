using System;
public class Bla
{
  public static void BuildNode(ref string label)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(253);
    string s = "a";
    label += s + s + s + s;
    IACSharpSensor.IACSharpSensor.SensorReached(254);
  }
  public static void BuildNode(ref string[] label)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(255);
    string s = "a";
    int idx = 0;
    label[idx++] += s + s + s + s;
    IACSharpSensor.IACSharpSensor.SensorReached(256);
  }
  public static void BuildNode_B(ref object label)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(257);
    string s = "b";
    label += s + s;
    IACSharpSensor.IACSharpSensor.SensorReached(258);
  }
  public static string BuildNode_C(ref string label)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(259);
    string[] a = new string[2];
    int i = 0;
    a[0] = "a";
    string s = "b";
    a[i++] += label + s + s + s;
    System.String RNTRNTRNT_87 = a[i - 1];
    IACSharpSensor.IACSharpSensor.SensorReached(260);
    return RNTRNTRNT_87;
  }
  public static string BuildNode_D()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(261);
    System.Collections.ArrayList values = new System.Collections.ArrayList();
    for (int i = 0; i < 6; i++) {
      values.Add(i);
    }
    string[] strs = new string[values.Count];
    int idx = 0;
    foreach (int val in values) {
      strs[idx] = "Value:";
      strs[idx++] += val.ToString();
    }
    System.String RNTRNTRNT_88 = strs[5];
    IACSharpSensor.IACSharpSensor.SensorReached(262);
    return RNTRNTRNT_88;
  }
  public static void BuildNode_E(ref string[,] label)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(263);
    string s = "a";
    int idx = 0;
    label = new string[1, 1];
    label[idx++, idx - 1] += s + s + s + s;
    IACSharpSensor.IACSharpSensor.SensorReached(264);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(265);
    String str = "test";
    BuildNode(ref str);
    Console.WriteLine(str);
    if (str != "testaaaa") {
      System.Int32 RNTRNTRNT_89 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(266);
      return RNTRNTRNT_89;
    }
    object ostr = "test";
    BuildNode_B(ref ostr);
    Console.WriteLine(ostr);
    if (ostr.ToString() != "testbb") {
      System.Int32 RNTRNTRNT_90 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(267);
      return RNTRNTRNT_90;
    }
    str = "test";
    string res = BuildNode_C(ref str);
    Console.WriteLine(str);
    if (str != "test") {
      System.Int32 RNTRNTRNT_91 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(268);
      return RNTRNTRNT_91;
    }
    Console.WriteLine(res);
    if (res != "atestbbb") {
      System.Int32 RNTRNTRNT_92 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(269);
      return RNTRNTRNT_92;
    }
    string[] sa = new string[1];
    BuildNode(ref sa);
    Console.WriteLine(sa[0]);
    if (sa[0] != "aaaa") {
      System.Int32 RNTRNTRNT_93 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(270);
      return RNTRNTRNT_93;
    }
    str = BuildNode_D();
    Console.WriteLine(str);
    if (str != "Value:5") {
      System.Int32 RNTRNTRNT_94 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(271);
      return RNTRNTRNT_94;
    }
    string[,] sa2 = null;
    BuildNode_E(ref sa2);
    Console.WriteLine(sa2[0, 0]);
    if (sa2[0, 0] != "aaaa") {
      System.Int32 RNTRNTRNT_95 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(272);
      return RNTRNTRNT_95;
    }
    System.Int32 RNTRNTRNT_96 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(273);
    return RNTRNTRNT_96;
  }
}
