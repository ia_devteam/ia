public delegate void FooHandler();
public static class Test
{
  private static void OnFooTest()
  {
  }
  public static event FooHandler Foo;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(130);
    FooHandler foo = delegate { Foo += OnFooTest; };
    IACSharpSensor.IACSharpSensor.SensorReached(131);
  }
}
