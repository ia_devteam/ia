using System;
using System.Reflection;
class X
{
  delegate object test(MethodInfo x);
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(251);
    DoCall(delegate(MethodInfo @from) { return @from.Invoke(null, new object[] { @from }); });
    IACSharpSensor.IACSharpSensor.SensorReached(252);
  }
  static void DoCall(test t)
  {
  }
}
