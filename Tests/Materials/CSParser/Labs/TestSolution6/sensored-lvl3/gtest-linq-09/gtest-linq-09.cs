using System;
using System.Collections.Generic;
using System.Linq;
class Data
{
  public int Key;
  public string Value;
}
class Join
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(94);
    Data[] d1 = new Data[] { new Data {
      Key = 1,
      Value = "First"
    } };
    Data[] d2 = new Data[] {
      new Data {
        Key = 1,
        Value = "Second"
      },
      new Data {
        Key = 1,
        Value = "Third"
      }
    };
    var e = from a in d1
      join b in d2 on a.Key equals b.Key
      select new { Result = a.Value + b.Value };
    var res = e.ToList();
    if (res.Count != 2) {
      System.Int32 RNTRNTRNT_53 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(95);
      return RNTRNTRNT_53;
    }
    if (res[0].Result != "FirstSecond") {
      System.Int32 RNTRNTRNT_54 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(96);
      return RNTRNTRNT_54;
    }
    if (res[1].Result != "FirstThird") {
      System.Int32 RNTRNTRNT_55 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(97);
      return RNTRNTRNT_55;
    }
    e = from a in d1
      join b in d2 on a.Key equals b.Key
      where b.Value == "Second"
      select new { Result = a.Value + b.Value };
    res = e.ToList();
    if (res.Count != 1) {
      System.Int32 RNTRNTRNT_56 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(98);
      return RNTRNTRNT_56;
    }
    if (res[0].Result != "FirstSecond") {
      System.Int32 RNTRNTRNT_57 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(99);
      return RNTRNTRNT_57;
    }
    e = from a in d1
      join b in d2 on a.Key equals b.Key
      select new { Result = a.Value + b.Value };
    res = e.ToList();
    if (res.Count != 2) {
      System.Int32 RNTRNTRNT_58 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(100);
      return RNTRNTRNT_58;
    }
    if (res[0].Result != "FirstSecond") {
      System.Int32 RNTRNTRNT_59 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(101);
      return RNTRNTRNT_59;
    }
    if (res[1].Result != "FirstThird") {
      System.Int32 RNTRNTRNT_60 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(102);
      return RNTRNTRNT_60;
    }
    var e2 = from a in d1
      join b in d2 on a.Key equals b.Key
      group b by a.Key;
    var res2 = e2.ToList();
    if (res2.Count != 1) {
      System.Int32 RNTRNTRNT_61 = 20;
      IACSharpSensor.IACSharpSensor.SensorReached(103);
      return RNTRNTRNT_61;
    }
    if (res2[0].Key != 1) {
      System.Int32 RNTRNTRNT_62 = 21;
      IACSharpSensor.IACSharpSensor.SensorReached(104);
      return RNTRNTRNT_62;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_63 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(105);
    return RNTRNTRNT_63;
  }
}
