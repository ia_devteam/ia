using System;
class TestUnary
{
  static void Foo(Action<int> a)
  {
  }
  static void Bar()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(253);
    Foo(str => ++str);
    IACSharpSensor.IACSharpSensor.SensorReached(254);
  }
}
class Program
{
  static void Foo(Action<string> a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(255);
    a("action");
    IACSharpSensor.IACSharpSensor.SensorReached(256);
  }
  static T Foo<T>(Func<string, T> f)
  {
    T RNTRNTRNT_140 = f("function");
    IACSharpSensor.IACSharpSensor.SensorReached(257);
    return RNTRNTRNT_140;
  }
  static string Bar()
  {
    System.String RNTRNTRNT_141 = Foo(str => str.ToLower());
    IACSharpSensor.IACSharpSensor.SensorReached(258);
    return RNTRNTRNT_141;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(259);
    var str = Foo(s => s);
    Console.WriteLine(str);
    Foo(s => Console.WriteLine(s));
    IACSharpSensor.IACSharpSensor.SensorReached(260);
  }
}
