using System;
class Program
{
  static void Foo(Action<string> a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(282);
    a("action");
    IACSharpSensor.IACSharpSensor.SensorReached(283);
  }
  static T Foo<T>(Func<string, T> f)
  {
    T RNTRNTRNT_147 = f("function");
    IACSharpSensor.IACSharpSensor.SensorReached(284);
    return RNTRNTRNT_147;
  }
  static string Bar()
  {
    System.String RNTRNTRNT_148 = Foo(str => str.ToLower());
    IACSharpSensor.IACSharpSensor.SensorReached(285);
    return RNTRNTRNT_148;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    var str = Foo(s => s);
    Console.WriteLine(str);
    if (str != "function") {
      System.Int32 RNTRNTRNT_149 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(287);
      return RNTRNTRNT_149;
    }
    Foo(s => Console.WriteLine(s));
    System.Int32 RNTRNTRNT_150 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(288);
    return RNTRNTRNT_150;
  }
}
