partial class C
{
  int i;
  partial void Partial_A();
  partial void Partial_A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(138);
    i += 1;
    IACSharpSensor.IACSharpSensor.SensorReached(139);
  }
  partial void Partial_B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(140);
    i += 3;
    IACSharpSensor.IACSharpSensor.SensorReached(141);
  }
  partial void Partial_B();
  static byte s;
  static partial void Partial_S()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(142);
    s += 5;
    IACSharpSensor.IACSharpSensor.SensorReached(143);
  }
  static partial void Partial_S();
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(144);
    C c = new C();
    c.Partial_A();
    c.Partial_B();
    if (c.i != 4) {
      System.Int32 RNTRNTRNT_79 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(145);
      return RNTRNTRNT_79;
    }
    Partial_S();
    if (s != 5) {
      System.Int32 RNTRNTRNT_80 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(146);
      return RNTRNTRNT_80;
    }
    System.Int32 RNTRNTRNT_81 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(147);
    return RNTRNTRNT_81;
  }
}
