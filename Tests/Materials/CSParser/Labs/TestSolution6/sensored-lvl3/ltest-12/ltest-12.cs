using System;
public delegate void Func<TA>(TA arg0);
class Demo
{
  static void F<T>(T[] values, T value, Func<T> f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(248);
    Console.WriteLine(values[0]);
    f(value);
    Console.WriteLine(values[0]);
    IACSharpSensor.IACSharpSensor.SensorReached(249);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(250);
    int[] a = new int[] { 10 };
    F(a, 5, i => a[0] = i);
    if (a[0] != 5) {
      System.Int32 RNTRNTRNT_138 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(251);
      return RNTRNTRNT_138;
    }
    System.Int32 RNTRNTRNT_139 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(252);
    return RNTRNTRNT_139;
  }
}
