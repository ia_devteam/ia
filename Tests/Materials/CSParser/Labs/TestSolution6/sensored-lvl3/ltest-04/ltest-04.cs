using System;
public delegate TResult Func<TArg0, TResult>(TArg0 arg0);
class Demo
{
  static Z F<X, Y, Z>(X value, Func<X, Y> f1, Func<Y, Z> f2)
  {
    Z RNTRNTRNT_114 = f2(f1(value));
    IACSharpSensor.IACSharpSensor.SensorReached(205);
    return RNTRNTRNT_114;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    double d = F("1:15:30", s => TimeSpan.Parse(s), t => t.TotalSeconds);
    if (d < 4529 || d > 4531) {
      System.Int32 RNTRNTRNT_115 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(207);
      return RNTRNTRNT_115;
    }
    System.Int32 RNTRNTRNT_116 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(208);
    return RNTRNTRNT_116;
  }
}
