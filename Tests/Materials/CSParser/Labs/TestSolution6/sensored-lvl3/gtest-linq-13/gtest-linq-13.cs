using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
class Program
{
  static void Main()
  {
  }
  static void Foo(TypeDefinition type)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(123);
    var res = from meth in type.Methods
      select meth;
    IACSharpSensor.IACSharpSensor.SensorReached(124);
  }
}
interface IFoo
{
}
static class Extension
{
  public static IEnumerable<T> Cast<T>(this IFoo i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(125);
    return null;
  }
}
public class MethodDefinition
{
}
public class TypeDefinition
{
  public MethodDefinitionCollection Methods {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(126);
      return null;
    }
    set { }
  }
}
public class MethodDefinitionCollection : CollectionBase
{
}
