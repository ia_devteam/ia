using System;
public class Crasher
{
  public static void Crash()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(277);
    double[] array = new double[1];
    Do(() =>
    {
      int col = 1;
      array[col] += array[col];
    });
    IACSharpSensor.IACSharpSensor.SensorReached(278);
  }
  static void Do(Action action)
  {
  }
  public static void Main()
  {
  }
}
