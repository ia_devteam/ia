using System;
public class MyClass : IDisposable
{
  private string s;
  public MyClass(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(171);
    this.s = s;
    IACSharpSensor.IACSharpSensor.SensorReached(172);
  }
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(173);
    s = "";
    IACSharpSensor.IACSharpSensor.SensorReached(174);
  }
}
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(175);
    using (var v = new MyClass("foo")) {
      if (v.GetType() != typeof(MyClass)) {
        System.Int32 RNTRNTRNT_101 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(176);
        return RNTRNTRNT_101;
      }
    }
    System.Int32 RNTRNTRNT_102 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    return RNTRNTRNT_102;
  }
}
