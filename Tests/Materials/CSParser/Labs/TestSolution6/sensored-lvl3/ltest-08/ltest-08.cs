using System;
using System.Linq;
using System.Collections.Generic;
public class C
{
  static void Test<T, R>(Func<T, R> d)
  {
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(234);
    Test((int x) => { return x + 1; });
    int[] source = new int[] {
      2,
      1,
      0
    };
    IEnumerable<int> e = source.Where(i => i == 0).Select(i => i + 1);
    if (e.ToList()[0] != 1) {
      System.Int32 RNTRNTRNT_134 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(235);
      return RNTRNTRNT_134;
    }
    e = source.Where((int i) => i == 0).Select((int i) => i + 1);
    if (e.ToList()[0] != 1) {
      System.Int32 RNTRNTRNT_135 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(236);
      return RNTRNTRNT_135;
    }
    e = source.Where(delegate(int i) { return i == 0; }).Select(delegate(int i) { return i + 1; });
    if (e.ToList()[0] != 1) {
      System.Int32 RNTRNTRNT_136 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(237);
      return RNTRNTRNT_136;
    }
    System.Int32 RNTRNTRNT_137 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    return RNTRNTRNT_137;
  }
}
