using System;
using System.Collections.Generic;
static class Enumerable
{
  public static int Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
  {
    System.Int32 RNTRNTRNT_142 = Sum<TSource, int>(source, (a, b) => a + selector(b));
    IACSharpSensor.IACSharpSensor.SensorReached(263);
    return RNTRNTRNT_142;
  }
  static TR Sum<TA, TR>(this IEnumerable<TA> source, Func<TR, TA, TR> selector)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(264);
    if (source == null) {
      throw new ArgumentNullException("source");
    }
    if (selector == null) {
      throw new ArgumentNullException("selector");
    }
    TR total = default(TR);
    int counter = 0;
    foreach (var element in source) {
      total = selector(total, element);
      ++counter;
    }
    if (counter == 0) {
      throw new InvalidOperationException();
    }
    TR RNTRNTRNT_143 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(265);
    return RNTRNTRNT_143;
  }
}
class Repro
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(266);
    var sum = new[] {
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7"
    }.Sum(s => int.Parse(s));
    if (sum != 28) {
      System.Int32 RNTRNTRNT_144 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(267);
      return RNTRNTRNT_144;
    }
    Console.WriteLine(sum);
    System.Int32 RNTRNTRNT_145 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(268);
    return RNTRNTRNT_145;
  }
}
