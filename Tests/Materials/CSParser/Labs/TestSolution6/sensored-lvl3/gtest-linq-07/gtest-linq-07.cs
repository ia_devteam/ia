using System;
using System.Collections.Generic;
using System.Linq;
class SelectMany
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(76);
    int[] a1 = new int[] {
      0,
      1
    };
    string[] a2 = new string[] {
      "10",
      "11"
    };
    int id = 0;
    var e = from i1 in a1
      from i2 in a2
      select new {
        i1,
        i2
      };
    foreach (var item in e) {
      Console.WriteLine(item);
      if (item.i1 != id / 2) {
        System.Int32 RNTRNTRNT_41 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(77);
        return RNTRNTRNT_41;
      }
      if (id % 2 == 0) {
        if (item.i2 != "10") {
          System.Int32 RNTRNTRNT_42 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(78);
          return RNTRNTRNT_42;
        }
      }
      ++id;
    }
    var e2 = from i1 in a1
      where i1 > 0
      from i2 in a2
      from i3 in a1
      orderby i3
      select new {
        pp = 9,
        i1,
        i3
      };
    id = 0;
    foreach (var item in e2) {
      Console.WriteLine(item);
      if (item.i1 != 1) {
        System.Int32 RNTRNTRNT_43 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(79);
        return RNTRNTRNT_43;
      }
      if (id / 2 != item.i3) {
        System.Int32 RNTRNTRNT_44 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(80);
        return RNTRNTRNT_44;
      }
      ++id;
    }
    var e3 = from i1 in a1
      from i2 in a2
      select new {
        i1,
        i2
      };
    id = 0;
    foreach (var item in e3) {
      Console.WriteLine(item);
      if (item.i1 != id / 2) {
        System.Int32 RNTRNTRNT_45 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(81);
        return RNTRNTRNT_45;
      }
      if (id % 2 == 0) {
        if (item.i2 != "10") {
          System.Int32 RNTRNTRNT_46 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(82);
          return RNTRNTRNT_46;
        }
      }
      ++id;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_47 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(83);
    return RNTRNTRNT_47;
  }
}
