using System;
class TestA
{
  public string value;
  public TestA(string value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(84);
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(85);
  }
  public string Select<U>(Func<TestA, U> f)
  {
    System.String RNTRNTRNT_48 = value;
    IACSharpSensor.IACSharpSensor.SensorReached(86);
    return RNTRNTRNT_48;
  }
}
static class TestB
{
  public static TestA Where(this TestA a, Func<TestA, bool> predicate)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(87);
    if (predicate(a)) {
      TestA RNTRNTRNT_49 = new TestA("where used");
      IACSharpSensor.IACSharpSensor.SensorReached(88);
      return RNTRNTRNT_49;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(89);
    return null;
  }
}
public class CustomQueryExpressionPattern
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(90);
    var v = new TestA("Oh yes");
    string foo = from a in v
      select a;
    if (foo != "Oh yes") {
      System.Int32 RNTRNTRNT_50 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(91);
      return RNTRNTRNT_50;
    }
    v = new TestA("where?");
    v = from a in v
      where a.value != "wrong"
      select a;
    if (v.value != "where used") {
      System.Int32 RNTRNTRNT_51 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(92);
      return RNTRNTRNT_51;
    }
    Console.WriteLine(v.value.ToString());
    System.Int32 RNTRNTRNT_52 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(93);
    return RNTRNTRNT_52;
  }
}
