using System;
using System.Collections.Generic;
using System.Linq;
class Let
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(72);
    int[] int_array = new int[] {
      0,
      1
    };
    IEnumerable<int> e;
    int pos;
    e = from i in int_array
      let u = i * 2
      select u;
    pos = 0;
    foreach (int actual in e) {
      Console.WriteLine(actual);
      if (int_array[pos++] * 2 != actual) {
        System.Int32 RNTRNTRNT_38 = pos;
        IACSharpSensor.IACSharpSensor.SensorReached(73);
        return RNTRNTRNT_38;
      }
    }
    e = from i in int_array
      let u = i * 2
      let v = u * 3
      where u != 0
      select v;
    pos = 1;
    foreach (int actual in e) {
      Console.WriteLine(actual);
      if (int_array[pos++] * 6 != actual) {
        System.Int32 RNTRNTRNT_39 = pos;
        IACSharpSensor.IACSharpSensor.SensorReached(74);
        return RNTRNTRNT_39;
      }
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_40 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(75);
    return RNTRNTRNT_40;
  }
}
