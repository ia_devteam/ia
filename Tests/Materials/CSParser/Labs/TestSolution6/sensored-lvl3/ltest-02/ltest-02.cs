using System;
delegate string funcs(string s);
delegate int funci(int i);
class X
{
  static void Foo(funci fi)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(196);
    int res = fi(10);
    Console.WriteLine(res);
    IACSharpSensor.IACSharpSensor.SensorReached(197);
  }
  static void Foo(funcs fs)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(198);
    string res = fs("hello");
    Console.WriteLine(res);
    IACSharpSensor.IACSharpSensor.SensorReached(199);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(200);
    Foo(x => x + "dingus");
    IACSharpSensor.IACSharpSensor.SensorReached(201);
  }
}
