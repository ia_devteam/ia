using System;
using System.Collections.Generic;
public class Test
{
  List<object> annotations = new List<object>();
  public IEnumerable<T> Annotations<T>() where T : class
  {
    IACSharpSensor.IACSharpSensor.SensorReached(7);
    foreach (T o in Annotations(typeof(T))) {
      T RNTRNTRNT_3 = o;
      IACSharpSensor.IACSharpSensor.SensorReached(8);
      yield return RNTRNTRNT_3;
      IACSharpSensor.IACSharpSensor.SensorReached(9);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(10);
  }
  public IEnumerable<object> Annotations(Type type)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(11);
    if (annotations == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(12);
      yield break;
    }
    foreach (object o in annotations) {
      if (o.GetType() == type) {
        System.Object RNTRNTRNT_4 = o;
        IACSharpSensor.IACSharpSensor.SensorReached(13);
        yield return RNTRNTRNT_4;
        IACSharpSensor.IACSharpSensor.SensorReached(14);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(15);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    var test = new Test();
    test.Annotations<Test>();
    IACSharpSensor.IACSharpSensor.SensorReached(17);
  }
}
