using System;
using System.Collections.Generic;
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(22);
    Foo<int> nav = new Foo<int>();
    IEnumerable<int> t = TestRoutine<int>(new int[] { 1 }, nav);
    new List<int>(t);
    IACSharpSensor.IACSharpSensor.SensorReached(23);
  }
  public static IEnumerable<T> TestRoutine<T>(IEnumerable<T> a, Foo<T> f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(24);
    f.CreateItem<int>();
    foreach (T n in a) {
      T RNTRNTRNT_5 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(25);
      yield return RNTRNTRNT_5;
      IACSharpSensor.IACSharpSensor.SensorReached(26);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(27);
  }
}
public class Foo<T>
{
  public void CreateItem<G>()
  {
  }
}
