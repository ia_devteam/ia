using System;
using System.Collections.Generic;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(167);
    string[] strings = new string[] {
      "Foo",
      "Bar",
      "Baz"
    };
    foreach (var v in strings) {
      if (v.GetType() != typeof(string)) {
        System.Int32 RNTRNTRNT_98 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(168);
        return RNTRNTRNT_98;
      }
    }
    Dictionary<int, string> dict = new Dictionary<int, string>();
    dict.Add(0, "boo");
    dict.Add(1, "far");
    dict.Add(2, "faz");
    foreach (var v in dict) {
      if (v.GetType() != typeof(KeyValuePair<int, string>)) {
        System.Int32 RNTRNTRNT_99 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(169);
        return RNTRNTRNT_99;
      }
    }
    System.Int32 RNTRNTRNT_100 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    return RNTRNTRNT_100;
  }
}
