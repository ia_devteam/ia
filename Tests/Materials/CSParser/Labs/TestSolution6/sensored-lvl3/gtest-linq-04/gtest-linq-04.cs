using System;
using System.Collections.Generic;
using System.Linq;
class TestGroupBy
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(48);
    int[] int_array = new int[] {
      0,
      1,
      2,
      3,
      4
    };
    IEnumerable<IGrouping<int, int>> e;
    e = from i in int_array
      group 1 by i % 2;
    List<IGrouping<int, int>> output = e.ToList();
    if (output.Count != 2) {
      System.Int32 RNTRNTRNT_16 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(49);
      return RNTRNTRNT_16;
    }
    foreach (IGrouping<int, int> ig in e) {
      Console.WriteLine(ig.Key);
      foreach (int value in ig) {
        Console.WriteLine("\t" + value);
        if (value != 1) {
          System.Int32 RNTRNTRNT_17 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(50);
          return RNTRNTRNT_17;
        }
      }
    }
    e = from i in int_array
      group i by i % 2;
    output = e.ToList();
    if (output.Count != 2) {
      System.Int32 RNTRNTRNT_18 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(51);
      return RNTRNTRNT_18;
    }
    int[] results_a = new int[] {
      0,
      2,
      4,
      1,
      3
    };
    int pos = 0;
    foreach (IGrouping<int, int> ig in e) {
      Console.WriteLine(ig.Key);
      foreach (int value in ig) {
        Console.WriteLine("\t" + value);
        if (value != results_a[pos++]) {
          System.Int32 RNTRNTRNT_19 = 3;
          IACSharpSensor.IACSharpSensor.SensorReached(52);
          return RNTRNTRNT_19;
        }
      }
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_20 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    return RNTRNTRNT_20;
  }
}
