using System.Collections.Generic;
class C
{
  class ArrayReadOnlyList<T>
  {
    T[] array;
    bool is_value_type;
    public ArrayReadOnlyList()
    {
    }
    public T this[int index] {
      get {
        T RNTRNTRNT_1 = array[index];
        IACSharpSensor.IACSharpSensor.SensorReached(1);
        return RNTRNTRNT_1;
      }
    }
    public IEnumerator<T> GetEnumerator()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      for (int i = 0; i < array.Length; i++) {
        T RNTRNTRNT_2 = array[i];
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        yield return RNTRNTRNT_2;
        IACSharpSensor.IACSharpSensor.SensorReached(4);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5);
    }
  }
  public static void Main()
  {
  }
}
