using System;
using System.Collections.Generic;
using System.Linq;
class OrderByTests
{
  class Data
  {
    public int ID { get; set; }
    public string Name { get; set; }
    public override string ToString()
    {
      System.String RNTRNTRNT_21 = ID + " " + Name;
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      return RNTRNTRNT_21;
    }
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(55);
    int[] int_array = new int[] {
      0,
      1
    };
    string[] string_array = new string[] {
      "f",
      "a",
      "z",
      "aa"
    };
    IEnumerable<int> e;
    e = from i in int_array
      orderby i
      select i;
    List<int> l = new List<int>(e);
    if (l[0] != 0) {
      System.Int32 RNTRNTRNT_22 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(56);
      return RNTRNTRNT_22;
    }
    if (l[1] != 1) {
      System.Int32 RNTRNTRNT_23 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(57);
      return RNTRNTRNT_23;
    }
    e = from i in int_array
      orderby i ascending
      select i;
    l = new List<int>(e);
    if (l[0] != 0) {
      System.Int32 RNTRNTRNT_24 = 100;
      IACSharpSensor.IACSharpSensor.SensorReached(58);
      return RNTRNTRNT_24;
    }
    if (l[1] != 1) {
      System.Int32 RNTRNTRNT_25 = 101;
      IACSharpSensor.IACSharpSensor.SensorReached(59);
      return RNTRNTRNT_25;
    }
    e = from i in int_array
      orderby i descending
      select i + 1;
    l = new List<int>(e);
    if (l[0] != 2) {
      System.Int32 RNTRNTRNT_26 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      return RNTRNTRNT_26;
    }
    if (l[1] != 1) {
      System.Int32 RNTRNTRNT_27 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(61);
      return RNTRNTRNT_27;
    }
    IEnumerable<string> s;
    s = from i in string_array
      orderby i
      select i;
    List<string> ls = new List<string>(s);
    if (ls[0] != "a") {
      System.Int32 RNTRNTRNT_28 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(62);
      return RNTRNTRNT_28;
    }
    if (ls[1] != "aa") {
      System.Int32 RNTRNTRNT_29 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(63);
      return RNTRNTRNT_29;
    }
    if (ls[2] != "f") {
      System.Int32 RNTRNTRNT_30 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(64);
      return RNTRNTRNT_30;
    }
    if (ls[3] != "z") {
      System.Int32 RNTRNTRNT_31 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(65);
      return RNTRNTRNT_31;
    }
    s = from i in string_array
      orderby i.Length
      select i;
    Data[] data = new Data[] {
      new Data {
        ID = 10,
        Name = "bcd"
      },
      new Data {
        ID = 20,
        Name = "Abcd"
      },
      new Data {
        ID = 20,
        Name = "Ab"
      },
      new Data {
        ID = 10,
        Name = "Zyx"
      }
    };
    var de = from i in data
      orderby i.ID ascending, i.Name descending
      select i;
    List<Data> ld = new List<Data>(de);
    if (ld[0].Name != "Zyx") {
      System.Int32 RNTRNTRNT_32 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(66);
      return RNTRNTRNT_32;
    }
    var de2 = from i in data
      orderby i.ID descending, i.Name ascending
      select i;
    ld = new List<Data>(de2);
    if (ld[0].Name != "Ab") {
      System.Int32 RNTRNTRNT_33 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(67);
      return RNTRNTRNT_33;
    }
    var de3 = from i in data
      where i.ID == 10
      orderby i.ID descending, i.Name ascending
      select i;
    ld = new List<Data>(de3);
    if (ld[0].Name != "bcd") {
      System.Int32 RNTRNTRNT_34 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(68);
      return RNTRNTRNT_34;
    }
    var de4 = from i in data
      where i.ID == 20
      orderby i.Name
      group i by i.Name;
    var group_order = new List<IGrouping<string, Data>>(de4);
    ld = new List<Data>(group_order[0]);
    if (ld[0].Name != "Ab") {
      System.Int32 RNTRNTRNT_35 = 13;
      IACSharpSensor.IACSharpSensor.SensorReached(69);
      return RNTRNTRNT_35;
    }
    ld = new List<Data>(group_order[1]);
    if (ld[0].Name != "Abcd") {
      System.Int32 RNTRNTRNT_36 = 14;
      IACSharpSensor.IACSharpSensor.SensorReached(70);
      return RNTRNTRNT_36;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_37 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(71);
    return RNTRNTRNT_37;
  }
}
