using System;
delegate int IntFunc(int x);
delegate void VoidFunc(int x);
class X
{
  static IntFunc func, increment;
  static VoidFunc nothing;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(188);
    int y = 0;
    int r;
    increment = (int x) => { return x + 1; };
    r = increment(4);
    Console.WriteLine("Should be 5={0}", r);
    if (r != 5) {
      System.Int32 RNTRNTRNT_107 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(189);
      return RNTRNTRNT_107;
    }
    func = (int x) => x + 1;
    r = func(10);
    Console.WriteLine("Should be 11={0}", r);
    if (r != 11) {
      System.Int32 RNTRNTRNT_108 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(190);
      return RNTRNTRNT_108;
    }
    nothing = (int x) => { y = x; };
    nothing(10);
    Console.WriteLine("Should be 10={0}", y);
    if (y != 10) {
      System.Int32 RNTRNTRNT_109 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(191);
      return RNTRNTRNT_109;
    }
    nothing = (int x) => { new X(x); };
    nothing(314);
    if (instantiated_value != 314) {
      System.Int32 RNTRNTRNT_110 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(192);
      return RNTRNTRNT_110;
    }
    Console.WriteLine("All tests pass");
    System.Int32 RNTRNTRNT_111 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(193);
    return RNTRNTRNT_111;
  }
  static int instantiated_value;
  X(int v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(194);
    instantiated_value = v;
    IACSharpSensor.IACSharpSensor.SensorReached(195);
  }
}
