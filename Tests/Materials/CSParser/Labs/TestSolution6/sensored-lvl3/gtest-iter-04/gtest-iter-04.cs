using System;
using System.Collections.Generic;
public abstract class TestClass
{
  public abstract void ToString(object obj);
  public IEnumerable<object> TestEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(18);
    ToString(null);
    IACSharpSensor.IACSharpSensor.SensorReached(19);
    yield break;
  }
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(20);
    ToString(null);
    IACSharpSensor.IACSharpSensor.SensorReached(21);
  }
}
class M
{
  public static void Main()
  {
  }
}
