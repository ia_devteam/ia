using System;
using System.Collections.Generic;
class Repro
{
  class Runner<T>
  {
    public Runner(Action<T> action, T t)
    {
    }
  }
  static void AssertFoo<T>(IList<T> list)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(269);
    new Runner<int>(delegate {
      foreach (T item in list) {
      }
    }, 42);
    IACSharpSensor.IACSharpSensor.SensorReached(270);
  }
  static void Main()
  {
  }
}
