using System;
using System.Collections;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(163);
    string[] strings = new string[] {
      "Foo",
      "Bar",
      "Baz"
    };
    foreach (var item in strings) {
      if (item.GetType() != typeof(string)) {
        System.Int32 RNTRNTRNT_95 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(164);
        return RNTRNTRNT_95;
      }
    }
    int[] ints = new int[] {
      2,
      4,
      8,
      16,
      42
    };
    foreach (var item in ints) {
      if (item.GetType() != typeof(int)) {
        System.Int32 RNTRNTRNT_96 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(165);
        return RNTRNTRNT_96;
      }
    }
    System.Int32 RNTRNTRNT_97 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(166);
    return RNTRNTRNT_97;
  }
}
