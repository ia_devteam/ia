using System;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(148);
    var i = 5;
    var b = true;
    var s = "foobar";
    if (!b) {
      System.Int32 RNTRNTRNT_82 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(149);
      return RNTRNTRNT_82;
    }
    if (i > 5) {
      System.Int32 RNTRNTRNT_83 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(150);
      return RNTRNTRNT_83;
    }
    if (s != "foobar") {
      System.Int32 RNTRNTRNT_84 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(151);
      return RNTRNTRNT_84;
    }
    System.Int32 RNTRNTRNT_85 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(152);
    return RNTRNTRNT_85;
  }
}
