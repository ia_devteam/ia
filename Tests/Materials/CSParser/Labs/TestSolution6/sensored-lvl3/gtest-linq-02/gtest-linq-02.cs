using System;
using System.Collections.Generic;
using System.Linq;
class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(38);
    int[] int_array = new int[] {
      0,
      1
    };
    IEnumerable<int> e;
    int pos;
    e = from i in int_array
      select i;
    pos = 0;
    foreach (int actual in e) {
      Console.WriteLine(actual);
      if (int_array[pos++] != actual) {
        System.Int32 RNTRNTRNT_8 = pos;
        IACSharpSensor.IACSharpSensor.SensorReached(39);
        return RNTRNTRNT_8;
      }
    }
    e = from i in int_array
      select 19;
    pos = 0;
    foreach (int actual in e) {
      Console.WriteLine(actual);
      if (actual != 19) {
        System.Int32 RNTRNTRNT_9 = actual;
        IACSharpSensor.IACSharpSensor.SensorReached(40);
        return RNTRNTRNT_9;
      }
    }
    e = from i in int_array
      select i;
    pos = 0;
    foreach (int actual in e) {
      Console.WriteLine(actual);
      if (int_array[pos++] != actual) {
        System.Int32 RNTRNTRNT_10 = pos;
        IACSharpSensor.IACSharpSensor.SensorReached(41);
        return RNTRNTRNT_10;
      }
    }
    e = from i in int_array
      select 19;
    pos = 0;
    foreach (int actual in e) {
      Console.WriteLine(actual);
      if (actual != 19) {
        System.Int32 RNTRNTRNT_11 = actual;
        IACSharpSensor.IACSharpSensor.SensorReached(42);
        return RNTRNTRNT_11;
      }
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_12 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(43);
    return RNTRNTRNT_12;
  }
}
