using System;
public class Class1
{
  public bool Method()
  {
    System.Boolean RNTRNTRNT_86 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(153);
    return RNTRNTRNT_86;
  }
  public int Property = 16;
}
public class Test
{
  private class Class2
  {
    public bool Method()
    {
      System.Boolean RNTRNTRNT_87 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(154);
      return RNTRNTRNT_87;
    }
    public int Property = 42;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(155);
    var class1 = new Class1();
    if (class1.GetType() != typeof(Class1)) {
      System.Int32 RNTRNTRNT_88 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(156);
      return RNTRNTRNT_88;
    }
    if (!class1.Method()) {
      System.Int32 RNTRNTRNT_89 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(157);
      return RNTRNTRNT_89;
    }
    if (class1.Property != 16) {
      System.Int32 RNTRNTRNT_90 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(158);
      return RNTRNTRNT_90;
    }
    var class2 = new Class2();
    if (class2.GetType() != typeof(Class2)) {
      System.Int32 RNTRNTRNT_91 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(159);
      return RNTRNTRNT_91;
    }
    if (!class2.Method()) {
      System.Int32 RNTRNTRNT_92 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(160);
      return RNTRNTRNT_92;
    }
    if (class2.Property != 42) {
      System.Int32 RNTRNTRNT_93 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(161);
      return RNTRNTRNT_93;
    }
    System.Int32 RNTRNTRNT_94 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    return RNTRNTRNT_94;
  }
}
