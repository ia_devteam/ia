using System;
using System.Collections.Generic;
using System.Linq;
class DataA
{
  public int Key;
  public string Text;
}
class DataB
{
  public int Key;
  public string Value;
}
class GroupJoin
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(106);
    DataA[] d1 = new DataA[] { new DataA {
      Key = 1,
      Text = "Foo"
    } };
    DataB[] d2 = new DataB[] { new DataB {
      Key = 2,
      Value = "Second"
    } };
    var e = from a in d1
      join b in d2 on a.Key equals b.Key into ab
      from x in ab.DefaultIfEmpty()
      select new {
        a = x == default(DataB) ? "<empty>" : x.Value,
        b = a.Text
      };
    var res = e.ToList();
    if (res.Count != 1) {
      System.Int32 RNTRNTRNT_64 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(107);
      return RNTRNTRNT_64;
    }
    if (res[0].a != "<empty>") {
      System.Int32 RNTRNTRNT_65 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(108);
      return RNTRNTRNT_65;
    }
    if (res[0].b != "Foo") {
      System.Int32 RNTRNTRNT_66 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(109);
      return RNTRNTRNT_66;
    }
    e = from a in d1
      join b in d2 on a.Key equals b.Key into ab
      from x in ab.DefaultIfEmpty()
      select new {
        a = x == default(DataB) ? "<empty>" : x.Value,
        b = a.Text
      };
    foreach (var o in e) {
      Console.WriteLine(o);
    }
    res = e.ToList();
    if (res.Count != 1) {
      System.Int32 RNTRNTRNT_67 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(110);
      return RNTRNTRNT_67;
    }
    if (res[0].a != "<empty>") {
      System.Int32 RNTRNTRNT_68 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(111);
      return RNTRNTRNT_68;
    }
    if (res[0].b != "Foo") {
      System.Int32 RNTRNTRNT_69 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(112);
      return RNTRNTRNT_69;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_70 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(113);
    return RNTRNTRNT_70;
  }
}
