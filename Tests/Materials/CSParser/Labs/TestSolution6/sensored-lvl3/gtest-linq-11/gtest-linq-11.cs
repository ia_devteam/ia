using System;
using System.Collections.Generic;
using System.Linq;
class IntoTest
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(114);
    int[] int_array = new int[] {
      0,
      1
    };
    var e = from i in int_array
      where i > 0
      select i into x
      select x + 99;
    var l = e.ToList();
    if (l.Count != 1) {
      System.Int32 RNTRNTRNT_71 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(115);
      return RNTRNTRNT_71;
    }
    if (l[0] != 100) {
      System.Int32 RNTRNTRNT_72 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(116);
      return RNTRNTRNT_72;
    }
    e = from i in int_array
      select i + 3 into x
      where x == 3
      select x + 5;
    l = e.ToList();
    if (l.Count != 1) {
      System.Int32 RNTRNTRNT_73 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(117);
      return RNTRNTRNT_73;
    }
    if (l[0] != 8) {
      System.Int32 RNTRNTRNT_74 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(118);
      return RNTRNTRNT_74;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_75 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(119);
    return RNTRNTRNT_75;
  }
}
