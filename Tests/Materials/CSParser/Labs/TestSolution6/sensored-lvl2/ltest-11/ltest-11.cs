using System;
public class Pair<T1, T2>
{
}
public delegate Pair<T1, T2> Group<T1, T2>(T1 input);
public static class C
{
  public static void Foo<TInput, TValue, TIntermediate>(Group<TInput, TValue> Pair, Func<TValue, Group<TInput, TIntermediate>> selector)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(446);
  }
}
public class E<TI>
{
  public void Rep1<TV>(Group<TI, TV> parser)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(447);
    C.Foo(parser, x => parser);
    IACSharpSensor.IACSharpSensor.SensorReached(448);
  }
}
public class M
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(449);
  }
}
