using System;
public delegate void Func<TA>(TA arg0);
class Demo
{
  static void F<T>(T[] values, T value, Func<T> f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(450);
    Console.WriteLine(values[0]);
    f(value);
    Console.WriteLine(values[0]);
    IACSharpSensor.IACSharpSensor.SensorReached(451);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(452);
    int[] a = new int[] { 10 };
    F(a, 5, i => a[0] = i);
    IACSharpSensor.IACSharpSensor.SensorReached(453);
    if (a[0] != 5) {
      System.Int32 RNTRNTRNT_138 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(454);
      return RNTRNTRNT_138;
    }
    System.Int32 RNTRNTRNT_139 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(455);
    return RNTRNTRNT_139;
  }
}
