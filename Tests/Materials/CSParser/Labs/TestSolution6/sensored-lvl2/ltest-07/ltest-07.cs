delegate void D();
delegate void E(bool b);
public class C
{
  static void Test(D d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(418);
  }
  static void Test(object o, D d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(419);
  }
  static void Test(D d1, D d2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(420);
  }
  static void Test2(object o, E e)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(421);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(422);
    D e = () => { };
    e = (D)null;
    e = default(D);
    Test(() => { });
    Test(1, () => { });
    Test(() => { }, () => { });
    Test2(null, foo => { });
    IACSharpSensor.IACSharpSensor.SensorReached(423);
  }
}
