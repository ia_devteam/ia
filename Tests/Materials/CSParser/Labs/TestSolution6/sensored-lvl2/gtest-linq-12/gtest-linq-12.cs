using System;
using System.Linq;
class NestedQuery
{
  public void XX()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(251);
    var enumerable = new string[] {
      "aba",
      "bbb",
      "bab",
      "aaa"
    }.Select(values => new {
      values = values,
      length = values.Length
    }).Select(ti0 => ti0.values.Select(type => new {
      type = type,
      x = 9
    }).Where(ti1 => (ti0.length == 3)).Select(ti1 => ti1.type));
    IACSharpSensor.IACSharpSensor.SensorReached(252);
  }
  public static int Main()
  {
    System.Int32 RNTRNTRNT_76 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(253);
    return RNTRNTRNT_76;
  }
}
