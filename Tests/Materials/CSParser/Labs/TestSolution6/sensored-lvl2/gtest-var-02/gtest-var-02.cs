using System;
public class Class1
{
  public bool Method()
  {
    System.Boolean RNTRNTRNT_86 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(297);
    return RNTRNTRNT_86;
  }
  public int Property = 16;
}
public class Test
{
  private class Class2
  {
    public bool Method()
    {
      System.Boolean RNTRNTRNT_87 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(298);
      return RNTRNTRNT_87;
    }
    public int Property = 42;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(299);
    var class1 = new Class1();
    IACSharpSensor.IACSharpSensor.SensorReached(300);
    if (class1.GetType() != typeof(Class1)) {
      System.Int32 RNTRNTRNT_88 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(301);
      return RNTRNTRNT_88;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(302);
    if (!class1.Method()) {
      System.Int32 RNTRNTRNT_89 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(303);
      return RNTRNTRNT_89;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(304);
    if (class1.Property != 16) {
      System.Int32 RNTRNTRNT_90 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(305);
      return RNTRNTRNT_90;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(306);
    var class2 = new Class2();
    IACSharpSensor.IACSharpSensor.SensorReached(307);
    if (class2.GetType() != typeof(Class2)) {
      System.Int32 RNTRNTRNT_91 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(308);
      return RNTRNTRNT_91;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(309);
    if (!class2.Method()) {
      System.Int32 RNTRNTRNT_92 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(310);
      return RNTRNTRNT_92;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(311);
    if (class2.Property != 42) {
      System.Int32 RNTRNTRNT_93 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(312);
      return RNTRNTRNT_93;
    }
    System.Int32 RNTRNTRNT_94 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(313);
    return RNTRNTRNT_94;
  }
}
