using System;
using System.Collections.Generic;
using System.Linq;
class WhereTest
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(69);
    int[] int_array = new int[] {
      0,
      1
    };
    IEnumerable<int> e;
    e = from i in int_array
      where i > 0
      select i;
    IACSharpSensor.IACSharpSensor.SensorReached(70);
    if (e.ToList()[0] != 1) {
      System.Int32 RNTRNTRNT_13 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(71);
      return RNTRNTRNT_13;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(72);
    e = from i in int_array
      where i == 0
      select i + 1;
    IACSharpSensor.IACSharpSensor.SensorReached(73);
    if (e.ToList()[0] != 1) {
      System.Int32 RNTRNTRNT_14 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(74);
      return RNTRNTRNT_14;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(75);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_15 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(76);
    return RNTRNTRNT_15;
  }
}
