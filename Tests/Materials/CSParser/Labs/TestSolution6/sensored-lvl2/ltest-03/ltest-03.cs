using System;
public delegate TResult Func<TArg0, TResult>(TArg0 arg0);
class Demo
{
  static Y F<X, Y>(int a, X value, Func<X, Y> f1)
  {
    Y RNTRNTRNT_112 = f1(value);
    IACSharpSensor.IACSharpSensor.SensorReached(384);
    return RNTRNTRNT_112;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(385);
    object o = F(1, "1:15:30", s => TimeSpan.Parse(s));
    Console.WriteLine(o);
    System.Int32 RNTRNTRNT_113 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(386);
    return RNTRNTRNT_113;
  }
}
