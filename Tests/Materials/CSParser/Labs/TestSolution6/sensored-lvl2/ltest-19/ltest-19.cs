using System;
public class Crasher
{
  public static void Crash()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(496);
    double[] array = new double[1];
    Do(() =>
    {
      int col = 1;
      array[col] += array[col];
    });
    IACSharpSensor.IACSharpSensor.SensorReached(497);
  }
  static void Do(Action action)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(498);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(499);
  }
}
