using System;
class TestA
{
  public string value;
  public TestA(string value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(180);
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(181);
  }
  public string Select<U>(Func<TestA, U> f)
  {
    System.String RNTRNTRNT_48 = value;
    IACSharpSensor.IACSharpSensor.SensorReached(182);
    return RNTRNTRNT_48;
  }
}
static class TestB
{
  public static TestA Where(this TestA a, Func<TestA, bool> predicate)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(183);
    if (predicate(a)) {
      TestA RNTRNTRNT_49 = new TestA("where used");
      IACSharpSensor.IACSharpSensor.SensorReached(184);
      return RNTRNTRNT_49;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(185);
    return null;
  }
}
public class CustomQueryExpressionPattern
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(186);
    var v = new TestA("Oh yes");
    string foo = from a in v
      select a;
    IACSharpSensor.IACSharpSensor.SensorReached(187);
    if (foo != "Oh yes") {
      System.Int32 RNTRNTRNT_50 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(188);
      return RNTRNTRNT_50;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(189);
    v = new TestA("where?");
    v = from a in v
      where a.value != "wrong"
      select a;
    IACSharpSensor.IACSharpSensor.SensorReached(190);
    if (v.value != "where used") {
      System.Int32 RNTRNTRNT_51 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(191);
      return RNTRNTRNT_51;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(192);
    Console.WriteLine(v.value.ToString());
    System.Int32 RNTRNTRNT_52 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(193);
    return RNTRNTRNT_52;
  }
}
