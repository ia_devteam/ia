using System;
class TestClass
{
  delegate void DT(T t);
  delegate void DF(F f);
  struct T
  {
  }
  struct F
  {
  }
  static void P(DT dt)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(401);
    Console.WriteLine("True");
    dt(new T());
    IACSharpSensor.IACSharpSensor.SensorReached(402);
  }
  static void P(DF df)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(403);
    System.Console.WriteLine("False");
    df(new F());
    IACSharpSensor.IACSharpSensor.SensorReached(404);
  }
  static T And(T a1, T a2)
  {
    T RNTRNTRNT_123 = new T();
    IACSharpSensor.IACSharpSensor.SensorReached(405);
    return RNTRNTRNT_123;
  }
  static F And(T a1, F a2)
  {
    F RNTRNTRNT_124 = new F();
    IACSharpSensor.IACSharpSensor.SensorReached(406);
    return RNTRNTRNT_124;
  }
  static F And(F a1, T a2)
  {
    F RNTRNTRNT_125 = new F();
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    return RNTRNTRNT_125;
  }
  static F And(F a1, F a2)
  {
    F RNTRNTRNT_126 = new F();
    IACSharpSensor.IACSharpSensor.SensorReached(408);
    return RNTRNTRNT_126;
  }
  static T Or(T a1, T a2)
  {
    T RNTRNTRNT_127 = new T();
    IACSharpSensor.IACSharpSensor.SensorReached(409);
    return RNTRNTRNT_127;
  }
  static T Or(T a1, F a2)
  {
    T RNTRNTRNT_128 = new T();
    IACSharpSensor.IACSharpSensor.SensorReached(410);
    return RNTRNTRNT_128;
  }
  static T Or(F a1, T a2)
  {
    T RNTRNTRNT_129 = new T();
    IACSharpSensor.IACSharpSensor.SensorReached(411);
    return RNTRNTRNT_129;
  }
  static F Or(F a1, F a2)
  {
    F RNTRNTRNT_130 = new F();
    IACSharpSensor.IACSharpSensor.SensorReached(412);
    return RNTRNTRNT_130;
  }
  static F Not(T a)
  {
    F RNTRNTRNT_131 = new F();
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    return RNTRNTRNT_131;
  }
  static T Not(F a)
  {
    T RNTRNTRNT_132 = new T();
    IACSharpSensor.IACSharpSensor.SensorReached(414);
    return RNTRNTRNT_132;
  }
  static void StopTrue(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(415);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(416);
    P(v1 => P(v2 => P(v3 => StopTrue(And(Not(v3), And(Not(v1), And(Or(v1, v2), Or(v2, v3))))))));
    System.Int32 RNTRNTRNT_133 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(417);
    return RNTRNTRNT_133;
  }
}
