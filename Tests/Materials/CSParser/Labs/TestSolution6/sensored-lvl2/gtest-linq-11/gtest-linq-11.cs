using System;
using System.Collections.Generic;
using System.Linq;
class IntoTest
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(239);
    int[] int_array = new int[] {
      0,
      1
    };
    var e = from i in int_array
      where i > 0
      select i into x
      select x + 99;
    var l = e.ToList();
    IACSharpSensor.IACSharpSensor.SensorReached(240);
    if (l.Count != 1) {
      System.Int32 RNTRNTRNT_71 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(241);
      return RNTRNTRNT_71;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(242);
    if (l[0] != 100) {
      System.Int32 RNTRNTRNT_72 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(243);
      return RNTRNTRNT_72;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(244);
    e = from i in int_array
      select i + 3 into x
      where x == 3
      select x + 5;
    l = e.ToList();
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    if (l.Count != 1) {
      System.Int32 RNTRNTRNT_73 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(246);
      return RNTRNTRNT_73;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(247);
    if (l[0] != 8) {
      System.Int32 RNTRNTRNT_74 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(248);
      return RNTRNTRNT_74;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(249);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_75 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(250);
    return RNTRNTRNT_75;
  }
}
