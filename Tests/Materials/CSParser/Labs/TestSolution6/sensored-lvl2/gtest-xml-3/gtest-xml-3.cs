using System.Collections.Generic;
namespace Test
{
  class DocMe
  {
    public static void UseList(List<int> list)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(357);
    }
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(358);
    }
  }
  class DocMe<T>
  {
    public void UseList(List<T> list)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(359);
    }
    public void UseList<U>(List<U> list)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(360);
    }
    public void RefMethod<U>(ref T t, ref U u)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(361);
    }
  }
}
