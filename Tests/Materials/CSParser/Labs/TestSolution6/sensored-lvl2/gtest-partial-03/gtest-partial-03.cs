namespace N
{
  using S = System;
  public partial class C
  {
    [S.Obsolete("A")]
    partial void Foo();
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(286);
    }
  }
}
namespace N
{
  public partial class C
  {
    partial void Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(287);
    }
  }
}
