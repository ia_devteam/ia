using System;
using System.Linq;
using System.Collections.Generic;
public class C
{
  static void Test<T, R>(Func<T, R> d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(424);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(425);
    Test((int x) => { return x + 1; });
    int[] source = new int[] {
      2,
      1,
      0
    };
    IEnumerable<int> e = source.Where(i => i == 0).Select(i => i + 1);
    IACSharpSensor.IACSharpSensor.SensorReached(426);
    if (e.ToList()[0] != 1) {
      System.Int32 RNTRNTRNT_134 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(427);
      return RNTRNTRNT_134;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(428);
    e = source.Where((int i) => i == 0).Select((int i) => i + 1);
    IACSharpSensor.IACSharpSensor.SensorReached(429);
    if (e.ToList()[0] != 1) {
      System.Int32 RNTRNTRNT_135 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(430);
      return RNTRNTRNT_135;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(431);
    e = source.Where(delegate(int i) { return i == 0; }).Select(delegate(int i) { return i + 1; });
    IACSharpSensor.IACSharpSensor.SensorReached(432);
    if (e.ToList()[0] != 1) {
      System.Int32 RNTRNTRNT_136 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(433);
      return RNTRNTRNT_136;
    }
    System.Int32 RNTRNTRNT_137 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(434);
    return RNTRNTRNT_137;
  }
}
