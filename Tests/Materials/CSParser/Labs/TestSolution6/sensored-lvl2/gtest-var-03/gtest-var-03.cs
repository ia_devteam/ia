using System;
using System.Collections;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(314);
    string[] strings = new string[] {
      "Foo",
      "Bar",
      "Baz"
    };
    IACSharpSensor.IACSharpSensor.SensorReached(315);
    foreach (var item in strings) {
      IACSharpSensor.IACSharpSensor.SensorReached(316);
      if (item.GetType() != typeof(string)) {
        System.Int32 RNTRNTRNT_95 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(317);
        return RNTRNTRNT_95;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(318);
    int[] ints = new int[] {
      2,
      4,
      8,
      16,
      42
    };
    IACSharpSensor.IACSharpSensor.SensorReached(319);
    foreach (var item in ints) {
      IACSharpSensor.IACSharpSensor.SensorReached(320);
      if (item.GetType() != typeof(int)) {
        System.Int32 RNTRNTRNT_96 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(321);
        return RNTRNTRNT_96;
      }
    }
    System.Int32 RNTRNTRNT_97 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(322);
    return RNTRNTRNT_97;
  }
}
