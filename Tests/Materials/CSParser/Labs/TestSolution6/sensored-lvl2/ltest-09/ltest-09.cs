using System.Collections.Generic;
delegate TD Func<TD>();
delegate TR Func<TA, TR>(TA arg);
public class C
{
  static IEnumerable<T> Test<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(435);
    return null;
  }
  static IEnumerable<T> Test<T>(Func<T> f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(436);
    return null;
  }
  static IEnumerable<T> Test2<T>(Func<T, T> f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(437);
    return null;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(438);
    IEnumerable<int> ie = Test(1);
    IEnumerable<string> se;
    se = Test(() => "a");
    se = Test(delegate() { return "a"; });
    se = Test2((string s) => "s");
    IACSharpSensor.IACSharpSensor.SensorReached(439);
  }
}
