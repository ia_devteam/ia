using System;
using System.Collections.Generic;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(323);
    string[] strings = new string[] {
      "Foo",
      "Bar",
      "Baz"
    };
    IACSharpSensor.IACSharpSensor.SensorReached(324);
    foreach (var v in strings) {
      IACSharpSensor.IACSharpSensor.SensorReached(325);
      if (v.GetType() != typeof(string)) {
        System.Int32 RNTRNTRNT_98 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(326);
        return RNTRNTRNT_98;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(327);
    Dictionary<int, string> dict = new Dictionary<int, string>();
    dict.Add(0, "boo");
    dict.Add(1, "far");
    dict.Add(2, "faz");
    IACSharpSensor.IACSharpSensor.SensorReached(328);
    foreach (var v in dict) {
      IACSharpSensor.IACSharpSensor.SensorReached(329);
      if (v.GetType() != typeof(KeyValuePair<int, string>)) {
        System.Int32 RNTRNTRNT_99 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(330);
        return RNTRNTRNT_99;
      }
    }
    System.Int32 RNTRNTRNT_100 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(331);
    return RNTRNTRNT_100;
  }
}
