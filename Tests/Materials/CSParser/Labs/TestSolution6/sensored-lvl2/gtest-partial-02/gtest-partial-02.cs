partial class C
{
  int i;
  partial void Partial_A();
  partial void Partial_A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(273);
    i += 1;
    IACSharpSensor.IACSharpSensor.SensorReached(274);
  }
  partial void Partial_B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(275);
    i += 3;
    IACSharpSensor.IACSharpSensor.SensorReached(276);
  }
  partial void Partial_B();
  static byte s;
  static partial void Partial_S()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(277);
    s += 5;
    IACSharpSensor.IACSharpSensor.SensorReached(278);
  }
  static partial void Partial_S();
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(279);
    C c = new C();
    c.Partial_A();
    c.Partial_B();
    IACSharpSensor.IACSharpSensor.SensorReached(280);
    if (c.i != 4) {
      System.Int32 RNTRNTRNT_79 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(281);
      return RNTRNTRNT_79;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(282);
    Partial_S();
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    if (s != 5) {
      System.Int32 RNTRNTRNT_80 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(284);
      return RNTRNTRNT_80;
    }
    System.Int32 RNTRNTRNT_81 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(285);
    return RNTRNTRNT_81;
  }
}
