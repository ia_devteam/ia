using System;
using System.Collections.Generic;
using System.Linq;
class SelectMany
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(152);
    int[] a1 = new int[] {
      0,
      1
    };
    string[] a2 = new string[] {
      "10",
      "11"
    };
    int id = 0;
    var e = from i1 in a1
      from i2 in a2
      select new {
        i1,
        i2
      };
    IACSharpSensor.IACSharpSensor.SensorReached(153);
    foreach (var item in e) {
      IACSharpSensor.IACSharpSensor.SensorReached(154);
      Console.WriteLine(item);
      IACSharpSensor.IACSharpSensor.SensorReached(155);
      if (item.i1 != id / 2) {
        System.Int32 RNTRNTRNT_41 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(156);
        return RNTRNTRNT_41;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(157);
      if (id % 2 == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(158);
        if (item.i2 != "10") {
          System.Int32 RNTRNTRNT_42 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(159);
          return RNTRNTRNT_42;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(160);
      ++id;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(161);
    var e2 = from i1 in a1
      where i1 > 0
      from i2 in a2
      from i3 in a1
      orderby i3
      select new {
        pp = 9,
        i1,
        i3
      };
    id = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    foreach (var item in e2) {
      IACSharpSensor.IACSharpSensor.SensorReached(163);
      Console.WriteLine(item);
      IACSharpSensor.IACSharpSensor.SensorReached(164);
      if (item.i1 != 1) {
        System.Int32 RNTRNTRNT_43 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(165);
        return RNTRNTRNT_43;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(166);
      if (id / 2 != item.i3) {
        System.Int32 RNTRNTRNT_44 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(167);
        return RNTRNTRNT_44;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(168);
      ++id;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(169);
    var e3 = from i1 in a1
      from i2 in a2
      select new {
        i1,
        i2
      };
    id = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    foreach (var item in e3) {
      IACSharpSensor.IACSharpSensor.SensorReached(171);
      Console.WriteLine(item);
      IACSharpSensor.IACSharpSensor.SensorReached(172);
      if (item.i1 != id / 2) {
        System.Int32 RNTRNTRNT_45 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(173);
        return RNTRNTRNT_45;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(174);
      if (id % 2 == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(175);
        if (item.i2 != "10") {
          System.Int32 RNTRNTRNT_46 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(176);
          return RNTRNTRNT_46;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(177);
      ++id;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(178);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_47 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(179);
    return RNTRNTRNT_47;
  }
}
