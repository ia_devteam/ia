using System;
class TestUnary
{
  static void Foo(Action<int> a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(456);
  }
  static void Bar()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(457);
    Foo(str => ++str);
    IACSharpSensor.IACSharpSensor.SensorReached(458);
  }
}
class Program
{
  static void Foo(Action<string> a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(459);
    a("action");
    IACSharpSensor.IACSharpSensor.SensorReached(460);
  }
  static T Foo<T>(Func<string, T> f)
  {
    T RNTRNTRNT_140 = f("function");
    IACSharpSensor.IACSharpSensor.SensorReached(461);
    return RNTRNTRNT_140;
  }
  static string Bar()
  {
    System.String RNTRNTRNT_141 = Foo(str => str.ToLower());
    IACSharpSensor.IACSharpSensor.SensorReached(462);
    return RNTRNTRNT_141;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(463);
    var str = Foo(s => s);
    Console.WriteLine(str);
    Foo(s => Console.WriteLine(s));
    IACSharpSensor.IACSharpSensor.SensorReached(464);
  }
}
