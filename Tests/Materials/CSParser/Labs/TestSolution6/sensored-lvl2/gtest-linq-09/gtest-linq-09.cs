using System;
using System.Collections.Generic;
using System.Linq;
class Data
{
  public int Key;
  public string Value;
}
class Join
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(194);
    Data[] d1 = new Data[] { new Data {
      Key = 1,
      Value = "First"
    } };
    Data[] d2 = new Data[] {
      new Data {
        Key = 1,
        Value = "Second"
      },
      new Data {
        Key = 1,
        Value = "Third"
      }
    };
    var e = from a in d1
      join b in d2 on a.Key equals b.Key
      select new { Result = a.Value + b.Value };
    var res = e.ToList();
    IACSharpSensor.IACSharpSensor.SensorReached(195);
    if (res.Count != 2) {
      System.Int32 RNTRNTRNT_53 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(196);
      return RNTRNTRNT_53;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(197);
    if (res[0].Result != "FirstSecond") {
      System.Int32 RNTRNTRNT_54 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(198);
      return RNTRNTRNT_54;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(199);
    if (res[1].Result != "FirstThird") {
      System.Int32 RNTRNTRNT_55 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(200);
      return RNTRNTRNT_55;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(201);
    e = from a in d1
      join b in d2 on a.Key equals b.Key
      where b.Value == "Second"
      select new { Result = a.Value + b.Value };
    res = e.ToList();
    IACSharpSensor.IACSharpSensor.SensorReached(202);
    if (res.Count != 1) {
      System.Int32 RNTRNTRNT_56 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(203);
      return RNTRNTRNT_56;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(204);
    if (res[0].Result != "FirstSecond") {
      System.Int32 RNTRNTRNT_57 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(205);
      return RNTRNTRNT_57;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    e = from a in d1
      join b in d2 on a.Key equals b.Key
      select new { Result = a.Value + b.Value };
    res = e.ToList();
    IACSharpSensor.IACSharpSensor.SensorReached(207);
    if (res.Count != 2) {
      System.Int32 RNTRNTRNT_58 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(208);
      return RNTRNTRNT_58;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(209);
    if (res[0].Result != "FirstSecond") {
      System.Int32 RNTRNTRNT_59 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(210);
      return RNTRNTRNT_59;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(211);
    if (res[1].Result != "FirstThird") {
      System.Int32 RNTRNTRNT_60 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(212);
      return RNTRNTRNT_60;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(213);
    var e2 = from a in d1
      join b in d2 on a.Key equals b.Key
      group b by a.Key;
    var res2 = e2.ToList();
    IACSharpSensor.IACSharpSensor.SensorReached(214);
    if (res2.Count != 1) {
      System.Int32 RNTRNTRNT_61 = 20;
      IACSharpSensor.IACSharpSensor.SensorReached(215);
      return RNTRNTRNT_61;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(216);
    if (res2[0].Key != 1) {
      System.Int32 RNTRNTRNT_62 = 21;
      IACSharpSensor.IACSharpSensor.SensorReached(217);
      return RNTRNTRNT_62;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(218);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_63 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(219);
    return RNTRNTRNT_63;
  }
}
