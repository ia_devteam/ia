using System;
using System.Collections.Generic;
class Repro
{
  class Runner<T>
  {
    public Runner(Action<T> action, T t)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(484);
    }
  }
  static void AssertFoo<T>(IList<T> list)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(485);
    new Runner<int>(delegate {
      foreach (T item in list) {
      }
    }, 42);
    IACSharpSensor.IACSharpSensor.SensorReached(486);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(487);
  }
}
