using System;
using System.Collections.Generic;
using System.Linq;
class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(47);
    int[] int_array = new int[] {
      0,
      1
    };
    IEnumerable<int> e;
    int pos;
    e = from i in int_array
      select i;
    pos = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(48);
    foreach (int actual in e) {
      IACSharpSensor.IACSharpSensor.SensorReached(49);
      Console.WriteLine(actual);
      IACSharpSensor.IACSharpSensor.SensorReached(50);
      if (int_array[pos++] != actual) {
        System.Int32 RNTRNTRNT_8 = pos;
        IACSharpSensor.IACSharpSensor.SensorReached(51);
        return RNTRNTRNT_8;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(52);
    e = from i in int_array
      select 19;
    pos = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    foreach (int actual in e) {
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      Console.WriteLine(actual);
      IACSharpSensor.IACSharpSensor.SensorReached(55);
      if (actual != 19) {
        System.Int32 RNTRNTRNT_9 = actual;
        IACSharpSensor.IACSharpSensor.SensorReached(56);
        return RNTRNTRNT_9;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(57);
    e = from i in int_array
      select i;
    pos = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(58);
    foreach (int actual in e) {
      IACSharpSensor.IACSharpSensor.SensorReached(59);
      Console.WriteLine(actual);
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      if (int_array[pos++] != actual) {
        System.Int32 RNTRNTRNT_10 = pos;
        IACSharpSensor.IACSharpSensor.SensorReached(61);
        return RNTRNTRNT_10;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    e = from i in int_array
      select 19;
    pos = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(63);
    foreach (int actual in e) {
      IACSharpSensor.IACSharpSensor.SensorReached(64);
      Console.WriteLine(actual);
      IACSharpSensor.IACSharpSensor.SensorReached(65);
      if (actual != 19) {
        System.Int32 RNTRNTRNT_11 = actual;
        IACSharpSensor.IACSharpSensor.SensorReached(66);
        return RNTRNTRNT_11;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(67);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_12 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    return RNTRNTRNT_12;
  }
}
