using System;
delegate string funcs(string s);
delegate int funci(int i);
class X
{
  static void Foo(funci fi)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(378);
    int res = fi(10);
    Console.WriteLine(res);
    IACSharpSensor.IACSharpSensor.SensorReached(379);
  }
  static void Foo(funcs fs)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(380);
    string res = fs("hello");
    Console.WriteLine(res);
    IACSharpSensor.IACSharpSensor.SensorReached(381);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(382);
    Foo(x => x + "dingus");
    IACSharpSensor.IACSharpSensor.SensorReached(383);
  }
}
