using System;
using System.Collections.Generic;
using System.Linq;
class TestGroupBy
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(77);
    int[] int_array = new int[] {
      0,
      1,
      2,
      3,
      4
    };
    IEnumerable<IGrouping<int, int>> e;
    e = from i in int_array
      group 1 by i % 2;
    List<IGrouping<int, int>> output = e.ToList();
    IACSharpSensor.IACSharpSensor.SensorReached(78);
    if (output.Count != 2) {
      System.Int32 RNTRNTRNT_16 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(79);
      return RNTRNTRNT_16;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(80);
    foreach (IGrouping<int, int> ig in e) {
      IACSharpSensor.IACSharpSensor.SensorReached(81);
      Console.WriteLine(ig.Key);
      IACSharpSensor.IACSharpSensor.SensorReached(82);
      foreach (int value in ig) {
        IACSharpSensor.IACSharpSensor.SensorReached(83);
        Console.WriteLine("\t" + value);
        IACSharpSensor.IACSharpSensor.SensorReached(84);
        if (value != 1) {
          System.Int32 RNTRNTRNT_17 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(85);
          return RNTRNTRNT_17;
        }
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(86);
    e = from i in int_array
      group i by i % 2;
    output = e.ToList();
    IACSharpSensor.IACSharpSensor.SensorReached(87);
    if (output.Count != 2) {
      System.Int32 RNTRNTRNT_18 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(88);
      return RNTRNTRNT_18;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(89);
    int[] results_a = new int[] {
      0,
      2,
      4,
      1,
      3
    };
    int pos = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(90);
    foreach (IGrouping<int, int> ig in e) {
      IACSharpSensor.IACSharpSensor.SensorReached(91);
      Console.WriteLine(ig.Key);
      IACSharpSensor.IACSharpSensor.SensorReached(92);
      foreach (int value in ig) {
        IACSharpSensor.IACSharpSensor.SensorReached(93);
        Console.WriteLine("\t" + value);
        IACSharpSensor.IACSharpSensor.SensorReached(94);
        if (value != results_a[pos++]) {
          System.Int32 RNTRNTRNT_19 = 3;
          IACSharpSensor.IACSharpSensor.SensorReached(95);
          return RNTRNTRNT_19;
        }
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(96);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_20 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(97);
    return RNTRNTRNT_20;
  }
}
