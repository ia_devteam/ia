using System;
using System.Collections.Generic;
using System.Linq;
class Let
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(140);
    int[] int_array = new int[] {
      0,
      1
    };
    IEnumerable<int> e;
    int pos;
    e = from i in int_array
      let u = i * 2
      select u;
    pos = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(141);
    foreach (int actual in e) {
      IACSharpSensor.IACSharpSensor.SensorReached(142);
      Console.WriteLine(actual);
      IACSharpSensor.IACSharpSensor.SensorReached(143);
      if (int_array[pos++] * 2 != actual) {
        System.Int32 RNTRNTRNT_38 = pos;
        IACSharpSensor.IACSharpSensor.SensorReached(144);
        return RNTRNTRNT_38;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(145);
    e = from i in int_array
      let u = i * 2
      let v = u * 3
      where u != 0
      select v;
    pos = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(146);
    foreach (int actual in e) {
      IACSharpSensor.IACSharpSensor.SensorReached(147);
      Console.WriteLine(actual);
      IACSharpSensor.IACSharpSensor.SensorReached(148);
      if (int_array[pos++] * 6 != actual) {
        System.Int32 RNTRNTRNT_39 = pos;
        IACSharpSensor.IACSharpSensor.SensorReached(149);
        return RNTRNTRNT_39;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(150);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_40 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(151);
    return RNTRNTRNT_40;
  }
}
