using System;
delegate int IntFunc(int x);
delegate void VoidFunc(int x);
class X
{
  static IntFunc func, increment;
  static VoidFunc nothing;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(362);
    int y = 0;
    int r;
    increment = (int x) => { return x + 1; };
    r = increment(4);
    Console.WriteLine("Should be 5={0}", r);
    IACSharpSensor.IACSharpSensor.SensorReached(363);
    if (r != 5) {
      System.Int32 RNTRNTRNT_107 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(364);
      return RNTRNTRNT_107;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(365);
    func = (int x) => x + 1;
    r = func(10);
    Console.WriteLine("Should be 11={0}", r);
    IACSharpSensor.IACSharpSensor.SensorReached(366);
    if (r != 11) {
      System.Int32 RNTRNTRNT_108 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(367);
      return RNTRNTRNT_108;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    nothing = (int x) => { y = x; };
    nothing(10);
    Console.WriteLine("Should be 10={0}", y);
    IACSharpSensor.IACSharpSensor.SensorReached(369);
    if (y != 10) {
      System.Int32 RNTRNTRNT_109 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(370);
      return RNTRNTRNT_109;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(371);
    nothing = (int x) => { new X(x); };
    nothing(314);
    IACSharpSensor.IACSharpSensor.SensorReached(372);
    if (instantiated_value != 314) {
      System.Int32 RNTRNTRNT_110 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(373);
      return RNTRNTRNT_110;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(374);
    Console.WriteLine("All tests pass");
    System.Int32 RNTRNTRNT_111 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(375);
    return RNTRNTRNT_111;
  }
  static int instantiated_value;
  X(int v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(376);
    instantiated_value = v;
    IACSharpSensor.IACSharpSensor.SensorReached(377);
  }
}
