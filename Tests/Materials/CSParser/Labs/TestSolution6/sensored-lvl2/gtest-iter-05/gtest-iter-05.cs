using System;
using System.Collections.Generic;
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(28);
    Foo<int> nav = new Foo<int>();
    IEnumerable<int> t = TestRoutine<int>(new int[] { 1 }, nav);
    new List<int>(t);
    IACSharpSensor.IACSharpSensor.SensorReached(29);
  }
  public static IEnumerable<T> TestRoutine<T>(IEnumerable<T> a, Foo<T> f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(30);
    f.CreateItem<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(31);
    foreach (T n in a) {
      T RNTRNTRNT_5 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(32);
      yield return RNTRNTRNT_5;
      IACSharpSensor.IACSharpSensor.SensorReached(33);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(34);
  }
}
public class Foo<T>
{
  public void CreateItem<G>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(35);
  }
}
