using System;
using System.Collections.Generic;
using System.Linq;
class DataA
{
  public int Key;
  public string Text;
}
class DataB
{
  public int Key;
  public string Value;
}
class GroupJoin
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    DataA[] d1 = new DataA[] { new DataA {
      Key = 1,
      Text = "Foo"
    } };
    DataB[] d2 = new DataB[] { new DataB {
      Key = 2,
      Value = "Second"
    } };
    var e = from a in d1
      join b in d2 on a.Key equals b.Key into ab
      from x in ab.DefaultIfEmpty()
      select new {
        a = x == default(DataB) ? "<empty>" : x.Value,
        b = a.Text
      };
    var res = e.ToList();
    IACSharpSensor.IACSharpSensor.SensorReached(221);
    if (res.Count != 1) {
      System.Int32 RNTRNTRNT_64 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(222);
      return RNTRNTRNT_64;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(223);
    if (res[0].a != "<empty>") {
      System.Int32 RNTRNTRNT_65 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(224);
      return RNTRNTRNT_65;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(225);
    if (res[0].b != "Foo") {
      System.Int32 RNTRNTRNT_66 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(226);
      return RNTRNTRNT_66;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(227);
    e = from a in d1
      join b in d2 on a.Key equals b.Key into ab
      from x in ab.DefaultIfEmpty()
      select new {
        a = x == default(DataB) ? "<empty>" : x.Value,
        b = a.Text
      };
    IACSharpSensor.IACSharpSensor.SensorReached(228);
    foreach (var o in e) {
      IACSharpSensor.IACSharpSensor.SensorReached(229);
      Console.WriteLine(o);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(230);
    res = e.ToList();
    IACSharpSensor.IACSharpSensor.SensorReached(231);
    if (res.Count != 1) {
      System.Int32 RNTRNTRNT_67 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(232);
      return RNTRNTRNT_67;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(233);
    if (res[0].a != "<empty>") {
      System.Int32 RNTRNTRNT_68 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(234);
      return RNTRNTRNT_68;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(235);
    if (res[0].b != "Foo") {
      System.Int32 RNTRNTRNT_69 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(236);
      return RNTRNTRNT_69;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(237);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_70 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    return RNTRNTRNT_70;
  }
}
