using System;
using System.Collections.Generic;
using System.Linq;
namespace NameCollisionTest
{
  class Data
  {
    public int Value;
  }
  static class Ex
  {
    public static IEnumerable<TR> Foo<T, TR>(this IEnumerable<T> t, Func<T, TR> f)
    {
      TR RNTRNTRNT_77 = f(t.First());
      IACSharpSensor.IACSharpSensor.SensorReached(266);
      yield return RNTRNTRNT_77;
      IACSharpSensor.IACSharpSensor.SensorReached(267);
      IACSharpSensor.IACSharpSensor.SensorReached(268);
    }
  }
  public class C
  {
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(269);
      Data[] i = new Data[0];
      var prods = from pe in i.Foo(pe => pe.Value)
        where pe > 0
        select pe;
      IACSharpSensor.IACSharpSensor.SensorReached(270);
    }
  }
}
