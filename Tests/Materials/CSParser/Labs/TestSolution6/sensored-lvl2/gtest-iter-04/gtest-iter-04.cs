using System;
using System.Collections.Generic;
public abstract class TestClass
{
  public abstract void ToString(object obj);
  public IEnumerable<object> TestEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(23);
    ToString(null);
    IACSharpSensor.IACSharpSensor.SensorReached(24);
    yield break;
  }
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(25);
    ToString(null);
    IACSharpSensor.IACSharpSensor.SensorReached(26);
  }
}
class M
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(27);
  }
}
