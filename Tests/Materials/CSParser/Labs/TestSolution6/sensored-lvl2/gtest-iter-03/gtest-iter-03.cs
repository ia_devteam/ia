using System;
using System.Collections.Generic;
public class Test
{
  List<object> annotations = new List<object>();
  public IEnumerable<T> Annotations<T>() where T : class
  {
    IACSharpSensor.IACSharpSensor.SensorReached(10);
    foreach (T o in Annotations(typeof(T))) {
      T RNTRNTRNT_3 = o;
      IACSharpSensor.IACSharpSensor.SensorReached(11);
      yield return RNTRNTRNT_3;
      IACSharpSensor.IACSharpSensor.SensorReached(12);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(13);
  }
  public IEnumerable<object> Annotations(Type type)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(14);
    if (annotations == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(15);
      yield break;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    foreach (object o in annotations) {
      IACSharpSensor.IACSharpSensor.SensorReached(17);
      if (o.GetType() == type) {
        System.Object RNTRNTRNT_4 = o;
        IACSharpSensor.IACSharpSensor.SensorReached(18);
        yield return RNTRNTRNT_4;
        IACSharpSensor.IACSharpSensor.SensorReached(19);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(20);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(21);
    var test = new Test();
    test.Annotations<Test>();
    IACSharpSensor.IACSharpSensor.SensorReached(22);
  }
}
