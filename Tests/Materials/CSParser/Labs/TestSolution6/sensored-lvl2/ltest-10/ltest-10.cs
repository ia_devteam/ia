using System;
using System.Collections.Generic;
class C
{
  public static void Foo<TSource>(IEnumerable<TSource> a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(440);
  }
  public static void Foo<TCollection, TSource>(IEnumerable<TSource> a, Func<TSource, IEnumerable<TCollection>> b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(441);
  }
  public static void Foo<TCollection, TSource>(IEnumerable<TSource> a, Func<TSource, TCollection[], IEnumerable<TCollection>> b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(442);
  }
  public static void Foo<TCollection, TSource>(Func<TCollection[], IEnumerable<TSource>> b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(443);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(444);
    int[] a = new int[] { 1 };
    Foo(a);
    Foo(a, (int i) => { return a; });
    Foo(a, (int i, int[] b) => { return a; });
    Foo((int[] b) => { return a; });
    IACSharpSensor.IACSharpSensor.SensorReached(445);
  }
}
