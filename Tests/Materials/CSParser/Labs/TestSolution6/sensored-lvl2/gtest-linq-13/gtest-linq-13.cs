using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
class Program
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(254);
  }
  static void Foo(TypeDefinition type)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(255);
    var res = from meth in type.Methods
      select meth;
    IACSharpSensor.IACSharpSensor.SensorReached(256);
  }
}
interface IFoo
{
}
static class Extension
{
  public static IEnumerable<T> Cast<T>(this IFoo i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(257);
    return null;
  }
}
public class MethodDefinition
{
}
public class TypeDefinition
{
  public MethodDefinitionCollection Methods {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(258);
      return null;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(259); }
  }
}
public class MethodDefinitionCollection : CollectionBase
{
}
