using System;
class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(465);
  }
  static void Foo<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(466);
    Func<T, T> f = n => n;
    IACSharpSensor.IACSharpSensor.SensorReached(467);
  }
}
