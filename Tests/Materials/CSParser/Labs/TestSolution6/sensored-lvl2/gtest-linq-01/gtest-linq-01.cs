using System;
using System.Collections.Generic;
using System.Linq;
namespace @from
{
  interface ITest
  {
    int Id { get; }
  }
  class C
  {
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(36);
      int[] i2 = new int[] {
        0,
        1
      };
      int[] i_b = new int[] {
        0,
        1
      };
      ITest[] join_test = new ITest[0];
      IEnumerable<int> e;
      IEnumerable<IGrouping<int, int>> g;
      e = from i in i2
        select i;
      e = from i in i2
        select i;
      var e2 = from i in i2
        select i;
      e = from i in new int[0]
        select i;
      e = from x in i2
        from y in i2
        select y;
      e = from i in i2
        where i % 3 != 0
        select i;
      e = from i in i2
        orderby i
        select i;
      e = from i in i2
        orderby i, i, i, i
        select i;
      e = from i in i2
        orderby i descending
        select i;
      e = from i in i2
        orderby i ascending
        select i;
      e = from i in i2
        join j in join_test on i equals j.Id
        select i;
      e = from i in i2
        join j in join_test on i equals j.Id
        select i;
      e = from i in i2
        join j in join_test on i equals j.Id
        join j2 in join_test on i equals j2.Id
        select i;
      e = from i in i2
        join j in i_b on i equals j into j
        select i;
      e = from i in i2
        join j in i_b on i equals j into j
        select i;
      g = from i in i2
        group i by 2;
      g = from i in i2
        group i by 2 into i
        select i;
      e = from i in i2
        let l = i + 4
        select i;
      e = from i in i2
        let l = i + 4
        let l2 = l + 2
        select i;
      g = from i in i2
        group i by 2 into i9
        from i in i2
        group i by 2 into i9
        from i in i2
        group i by 2 into i9
        from i in i2
        group i by 2 into i9
        select i9;
      var e3 = from a in from b in i2
          select b
        orderby a
        select a;
      int @from = 0;
      bool @let = false;
      IACSharpSensor.IACSharpSensor.SensorReached(37);
    }
    void Foo(int @from, bool @where)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(38);
      int Foo = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(39);
      if (@from < Foo) {
        IACSharpSensor.IACSharpSensor.SensorReached(40);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(41);
    }
    int foo;
    void Do(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(42);
      int @from = this.foo;
      Console.WriteLine(args[@from]);
      IACSharpSensor.IACSharpSensor.SensorReached(43);
    }
  }
  class D
  {
    public bool check(object @from, object to)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(44);
      if (@from is int && to is int) {
        System.Boolean RNTRNTRNT_6 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(45);
        return RNTRNTRNT_6;
      }
      System.Boolean RNTRNTRNT_7 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(46);
      return RNTRNTRNT_7;
    }
  }
}
