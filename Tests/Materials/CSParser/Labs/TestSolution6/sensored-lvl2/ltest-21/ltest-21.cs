using System;
class Program
{
  static void Foo(Action<string> a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(504);
    a("action");
    IACSharpSensor.IACSharpSensor.SensorReached(505);
  }
  static T Foo<T>(Func<string, T> f)
  {
    T RNTRNTRNT_147 = f("function");
    IACSharpSensor.IACSharpSensor.SensorReached(506);
    return RNTRNTRNT_147;
  }
  static string Bar()
  {
    System.String RNTRNTRNT_148 = Foo(str => str.ToLower());
    IACSharpSensor.IACSharpSensor.SensorReached(507);
    return RNTRNTRNT_148;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(508);
    var str = Foo(s => s);
    Console.WriteLine(str);
    IACSharpSensor.IACSharpSensor.SensorReached(509);
    if (str != "function") {
      System.Int32 RNTRNTRNT_149 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(510);
      return RNTRNTRNT_149;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(511);
    Foo(s => Console.WriteLine(s));
    System.Int32 RNTRNTRNT_150 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(512);
    return RNTRNTRNT_150;
  }
}
