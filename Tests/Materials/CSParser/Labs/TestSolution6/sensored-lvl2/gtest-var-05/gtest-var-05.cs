using System;
public class MyClass : IDisposable
{
  private string s;
  public MyClass(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(332);
    this.s = s;
    IACSharpSensor.IACSharpSensor.SensorReached(333);
  }
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(334);
    s = "";
    IACSharpSensor.IACSharpSensor.SensorReached(335);
  }
}
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(336);
    using (var v = new MyClass("foo")) {
      IACSharpSensor.IACSharpSensor.SensorReached(337);
      if (v.GetType() != typeof(MyClass)) {
        System.Int32 RNTRNTRNT_101 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(338);
        return RNTRNTRNT_101;
      }
    }
    System.Int32 RNTRNTRNT_102 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(339);
    return RNTRNTRNT_102;
  }
}
