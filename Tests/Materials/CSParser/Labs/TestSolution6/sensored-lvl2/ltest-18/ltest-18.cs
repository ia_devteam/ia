using System;
static class Test
{
  public static void Foo<T1, T2, TResult>(T1 arg1, T2 arg2, Func<T1, T2, TResult> func)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(492);
    Bar(arg1, arg2, (a, b, _) => func(a, b));
    IACSharpSensor.IACSharpSensor.SensorReached(493);
  }
  public static void Bar<T1, T2, TResult>(T1 arg1, T2 arg2, Func<T1, T2, int, TResult> func)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(494);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(495);
  }
}
