using System;
using System.Collections.Generic;
using System.Linq.Expressions;
public enum Style : ulong
{
  Foo,
  Bar
}
public class Person
{
  public Style Style { get; set; }
}
public interface IObjectContainer
{
}
public static class Extensions
{
  public static IMarker<T> Cast<T>(this IObjectContainer container)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(260);
    return null;
  }
  public static IMarker<T> Where<T>(this IMarker<T> marker, Expression<Func<T, bool>> selector)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(261);
    return null;
  }
}
public interface IMarker<T> : IEnumerable<T>
{
}
public class Program
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(262);
  }
  public static void Assert(Action a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(263);
  }
  public static void Test(IObjectContainer o, Style s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(264);
    Assert(delegate {
      var res = from p in o
        where 0 == s
        select p;
    });
    IACSharpSensor.IACSharpSensor.SensorReached(265);
  }
}
