using System;
using System.Collections.Generic;
static class Enumerable
{
  public static int Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
  {
    System.Int32 RNTRNTRNT_142 = Sum<TSource, int>(source, (a, b) => a + selector(b));
    IACSharpSensor.IACSharpSensor.SensorReached(468);
    return RNTRNTRNT_142;
  }
  static TR Sum<TA, TR>(this IEnumerable<TA> source, Func<TR, TA, TR> selector)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(469);
    if (source == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(470);
      throw new ArgumentNullException("source");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(471);
    if (selector == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(472);
      throw new ArgumentNullException("selector");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(473);
    TR total = default(TR);
    int counter = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(474);
    foreach (var element in source) {
      IACSharpSensor.IACSharpSensor.SensorReached(475);
      total = selector(total, element);
      ++counter;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(476);
    if (counter == 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(477);
      throw new InvalidOperationException();
    }
    TR RNTRNTRNT_143 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(478);
    return RNTRNTRNT_143;
  }
}
class Repro
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(479);
    var sum = new[] {
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7"
    }.Sum(s => int.Parse(s));
    IACSharpSensor.IACSharpSensor.SensorReached(480);
    if (sum != 28) {
      System.Int32 RNTRNTRNT_144 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(481);
      return RNTRNTRNT_144;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(482);
    Console.WriteLine(sum);
    System.Int32 RNTRNTRNT_145 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(483);
    return RNTRNTRNT_145;
  }
}
