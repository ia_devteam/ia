using System;
public class App
{
  private delegate void TGenericDelegate<T>(string param);
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(60);
    App app = new App();
    app.Run();
    IACSharpSensor.IACSharpSensor.SensorReached(61);
  }
  public void Run()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    TGenericDelegate<string> del = ADelegate<string>;
    TestMethod<string>("a param", ADelegate<string>);
    TestMethod<string>("another param", del);
    IACSharpSensor.IACSharpSensor.SensorReached(63);
  }
  private void TestMethod<T>(string param, TGenericDelegate<T> del)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(64);
    Console.WriteLine("TestMethod <T> called with param: {0}. Calling a delegate", param);
    if (del != null) {
      del(param);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(65);
  }
  private void ADelegate<T>(string param)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(66);
    Console.WriteLine("ADelegate <T> called with param: {0}", param);
    IACSharpSensor.IACSharpSensor.SensorReached(67);
  }
}
