using System;
class Foo<T>
{
  public enum TestEnum
  {
    One,
    Two,
    Three
  }
  public TestEnum Test;
  public Foo(TestEnum test)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(40);
    this.Test = test;
    IACSharpSensor.IACSharpSensor.SensorReached(41);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    Foo<int>.TestEnum e = Foo<int>.TestEnum.One;
    Console.WriteLine(e);
    Foo<int> foo = new Foo<int>(e);
    foo.Test = e;
    IACSharpSensor.IACSharpSensor.SensorReached(43);
  }
}
