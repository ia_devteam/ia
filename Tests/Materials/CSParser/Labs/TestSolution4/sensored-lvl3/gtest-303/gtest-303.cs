using System;
namespace test
{
  public class BaseClass
  {
    public BaseClass()
    {
    }
    public string Hello {
      get {
        System.String RNTRNTRNT_1 = "Hello";
        IACSharpSensor.IACSharpSensor.SensorReached(10);
        return RNTRNTRNT_1;
      }
    }
  }
  public abstract class Printer
  {
    public abstract void Print<T>(T obj) where T : BaseClass;
  }
  public class PrinterImpl : Printer
  {
    public override void Print<T>(T obj)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(11);
      Console.WriteLine(obj.Hello);
      IACSharpSensor.IACSharpSensor.SensorReached(12);
    }
  }
  public class Starter
  {
    public static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(13);
      BaseClass bc = new BaseClass();
      Printer p = new PrinterImpl();
      p.Print<BaseClass>(bc);
      IACSharpSensor.IACSharpSensor.SensorReached(14);
    }
  }
}
