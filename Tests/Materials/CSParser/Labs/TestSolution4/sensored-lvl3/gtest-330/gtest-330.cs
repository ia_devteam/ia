using System;
using System.Collections;
using System.Collections.Generic;
public class BaseCollection<T> : IEnumerable<T>
{
  protected List<T> items = new List<T>();
  IEnumerator<T> IEnumerable<T>.GetEnumerator()
  {
    IEnumerator<T> RNTRNTRNT_15 = items.GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(91);
    return RNTRNTRNT_15;
  }
  IEnumerator IEnumerable.GetEnumerator()
  {
    IEnumerator RNTRNTRNT_16 = items.GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(92);
    return RNTRNTRNT_16;
  }
}
public class BaseIntList<T> : BaseCollection<T>
{
}
public class IntList : BaseIntList<int>
{
}
class X
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(93);
    IntList list = new IntList();
    foreach (int i in list) {
      Console.WriteLine(i);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(94);
  }
}
