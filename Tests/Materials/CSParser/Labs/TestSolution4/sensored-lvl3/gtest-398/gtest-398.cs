using System;
public interface IFace
{
  void Tst(IFace b);
}
public delegate string ToStr(string format, IFormatProvider format_provider);
public class GenericClass<T> where T : IFormattable
{
  T field;
  public GenericClass(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(405);
    this.field = t;
    IACSharpSensor.IACSharpSensor.SensorReached(406);
  }
  public void Method()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    ToStr str = new ToStr(field.ToString);
    Console.WriteLine(str("x", null));
    IACSharpSensor.IACSharpSensor.SensorReached(408);
  }
  public void Test(T t)
  {
  }
}
public class Foo
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(409);
    GenericClass<int> example = new GenericClass<int>(99);
    example.Method();
    IACSharpSensor.IACSharpSensor.SensorReached(410);
  }
}
