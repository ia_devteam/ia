using System;
using System.Reflection;
struct D
{
  public static D d1 = new D(1);
  public int d2;
  public D(int v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(202);
    d2 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(203);
  }
}
class T
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(204);
    ConstructorInfo mi = typeof(D).GetConstructors(BindingFlags.Instance | BindingFlags.Public)[0];
    MethodBody mb = mi.GetMethodBody();
    Console.WriteLine(mb.GetILAsByteArray().Length);
    if (mb.GetILAsByteArray().Length != 8) {
      System.Int32 RNTRNTRNT_53 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(205);
      return RNTRNTRNT_53;
    }
    mi = typeof(D).GetConstructors(BindingFlags.Static | BindingFlags.NonPublic)[0];
    mb = mi.GetMethodBody();
    Console.WriteLine(mb.GetILAsByteArray().Length);
    if (mb.GetILAsByteArray().Length != 12) {
      System.Int32 RNTRNTRNT_54 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(206);
      return RNTRNTRNT_54;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_55 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(207);
    return RNTRNTRNT_55;
  }
}
