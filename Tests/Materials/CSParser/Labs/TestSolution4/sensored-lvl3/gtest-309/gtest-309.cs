class Test<A, B>
{
  public void Foo<V, W>(Test<A, W> x, Test<V, B> y)
  {
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(34);
    Test<float, int> test = new Test<float, int>();
    test.Foo(test, test);
    IACSharpSensor.IACSharpSensor.SensorReached(35);
  }
}
