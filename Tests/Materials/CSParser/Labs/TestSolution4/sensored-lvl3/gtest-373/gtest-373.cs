using System;
namespace MonoBug
{
  class MainClass
  {
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(263);
      GenericType<bool> g = new GenericType<bool>(true);
      if (g) {
        Console.WriteLine("true");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(264);
    }
  }
  public class GenericType<T>
  {
    private T value;
    public GenericType(T value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(265);
      this.value = value;
      IACSharpSensor.IACSharpSensor.SensorReached(266);
    }
    public static implicit operator T(GenericType<T> o)
    {
      T RNTRNTRNT_79 = o.value;
      IACSharpSensor.IACSharpSensor.SensorReached(267);
      return RNTRNTRNT_79;
    }
  }
}
