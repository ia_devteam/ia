using System;
public class Test
{
  public delegate void DelegateA(bool b);
  public delegate int DelegateB(int i);
  static DelegateA dt;
  static DelegateB dt2;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(107);
    bool b = DelegateMethod == dt;
    if (b) {
      System.Int32 RNTRNTRNT_18 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(108);
      return RNTRNTRNT_18;
    }
    b = DelegateMethod != dt;
    if (!b) {
      System.Int32 RNTRNTRNT_19 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(109);
      return RNTRNTRNT_19;
    }
    b = dt2 == DelegateMethod;
    if (b) {
      System.Int32 RNTRNTRNT_20 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(110);
      return RNTRNTRNT_20;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_21 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(111);
    return RNTRNTRNT_21;
  }
  static void DelegateMethod(bool b)
  {
  }
  static int DelegateMethod(int b)
  {
    System.Int32 RNTRNTRNT_22 = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(112);
    return RNTRNTRNT_22;
  }
}
