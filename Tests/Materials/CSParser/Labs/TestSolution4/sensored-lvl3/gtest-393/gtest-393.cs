class B<T>
{
}
[A(typeof(B<>))]
public class A : System.Attribute
{
  static int ret = 1;
  public A(System.Type type)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(375);
    if (type == typeof(B<>)) {
      ret = 0;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(376);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(377);
    typeof(A).GetCustomAttributes(true);
    System.Int32 RNTRNTRNT_138 = ret;
    IACSharpSensor.IACSharpSensor.SensorReached(378);
    return RNTRNTRNT_138;
  }
}
