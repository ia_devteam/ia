using System;
public class DieSubrangeType
{
  public int? UpperBound { get; private set; }
  public DieSubrangeType()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(371);
    UpperBound = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(372);
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(373);
    DieSubrangeType subrange = new DieSubrangeType();
    Console.WriteLine(subrange.UpperBound != null);
    Console.WriteLine((int)subrange.UpperBound);
    System.Int32 RNTRNTRNT_137 = (int)subrange.UpperBound - 1;
    IACSharpSensor.IACSharpSensor.SensorReached(374);
    return RNTRNTRNT_137;
  }
}
