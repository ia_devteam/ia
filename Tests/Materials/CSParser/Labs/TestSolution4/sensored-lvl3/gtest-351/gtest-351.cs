using System;
class TestThing
{
  public int SetEnum(string a, Enum b)
  {
    System.Int32 RNTRNTRNT_50 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(198);
    return RNTRNTRNT_50;
  }
  public int SetEnum(int a, Enum b)
  {
    System.Int32 RNTRNTRNT_51 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(199);
    return RNTRNTRNT_51;
  }
}
class Test
{
  public static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(200);
    DayOfWeek? e = DayOfWeek.Monday;
    TestThing t = new TestThing();
    System.Int32 RNTRNTRNT_52 = t.SetEnum("hi", e);
    IACSharpSensor.IACSharpSensor.SensorReached(201);
    return RNTRNTRNT_52;
  }
}
