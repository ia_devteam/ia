using System;
public class TestAttribute : Attribute
{
  object type;
  public object Type {
    get {
      System.Object RNTRNTRNT_36 = type;
      IACSharpSensor.IACSharpSensor.SensorReached(131);
      return RNTRNTRNT_36;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(132);
      type = value;
      IACSharpSensor.IACSharpSensor.SensorReached(133);
    }
  }
  public TestAttribute()
  {
  }
  public TestAttribute(Type type)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(134);
    this.type = type;
    IACSharpSensor.IACSharpSensor.SensorReached(135);
  }
}
namespace N
{
  class C<T>
  {
    [Test(Type = typeof(C<>))]
    public void Bar()
    {
    }
    [Test(typeof(C<>))]
    public void Bar2()
    {
    }
    [Test(typeof(C<int>))]
    public void Bar3()
    {
    }
    [Test(typeof(C<CC>))]
    public void Bar4()
    {
    }
  }
  class CC
  {
    public static void Main()
    {
    }
  }
}
