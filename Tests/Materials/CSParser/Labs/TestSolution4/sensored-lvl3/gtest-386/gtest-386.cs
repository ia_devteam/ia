using System;
using System.Linq.Expressions;
public struct MyType
{
  int value;
  public MyType(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(341);
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(342);
  }
  public static MyType operator -(MyType a)
  {
    MyType RNTRNTRNT_119 = new MyType(-a.value);
    IACSharpSensor.IACSharpSensor.SensorReached(343);
    return RNTRNTRNT_119;
  }
}
class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(344);
    MyType? x = null;
    MyType? y = -x;
    checked {
      float? f = float.MinValue;
      f = -f + 1;
      int? i = int.MinValue;
      try {
        i = -i;
        System.Int32 RNTRNTRNT_120 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(345);
        return RNTRNTRNT_120;
      } catch (OverflowException) {
      }
    }
    System.Int32 RNTRNTRNT_121 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(346);
    return RNTRNTRNT_121;
  }
}
