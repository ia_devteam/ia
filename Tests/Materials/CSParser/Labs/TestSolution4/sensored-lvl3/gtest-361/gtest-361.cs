public class Thing
{
  public delegate void Handler();
  static void Foo()
  {
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(229);
    Method(delegate { }, "Hello", "How", "Are", "You");
    Method(delegate { });
    Method(null, null);
    Method(null);
    Method(Foo, "Hi");
    System.Int32 RNTRNTRNT_65 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(230);
    return RNTRNTRNT_65;
  }
  public static void Method(Handler handler, params string[] args)
  {
  }
}
