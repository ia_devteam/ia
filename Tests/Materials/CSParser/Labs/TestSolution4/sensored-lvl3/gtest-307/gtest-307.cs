partial class Foo<T>
{
}
partial class Foo<T>
{
  public delegate int F();
}
class Bar
{
  static int g()
  {
    System.Int32 RNTRNTRNT_5 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(23);
    return RNTRNTRNT_5;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(24);
    Foo<int>.F f = g;
    System.Int32 RNTRNTRNT_6 = f();
    IACSharpSensor.IACSharpSensor.SensorReached(25);
    return RNTRNTRNT_6;
  }
}
