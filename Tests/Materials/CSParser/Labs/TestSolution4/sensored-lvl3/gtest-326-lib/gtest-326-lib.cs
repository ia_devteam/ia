using System;
using SCG = System.Collections.Generic;
namespace C5
{
  public abstract class EnumerableBase<T> : SCG.IEnumerable<T>
  {
    public abstract SCG.IEnumerator<T> GetEnumerator();
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      System.Collections.IEnumerator RNTRNTRNT_13 = GetEnumerator();
      IACSharpSensor.IACSharpSensor.SensorReached(78);
      return RNTRNTRNT_13;
    }
  }
  public class ArrayBase<T> : EnumerableBase<T>
  {
    public override SCG.IEnumerator<T> GetEnumerator()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(79);
      yield break;
    }
  }
  public class ArrayList<T> : ArrayBase<T>
  {
    public override SCG.IEnumerator<T> GetEnumerator()
    {
      SCG.IEnumerator<T> RNTRNTRNT_14 = base.GetEnumerator();
      IACSharpSensor.IACSharpSensor.SensorReached(80);
      return RNTRNTRNT_14;
    }
  }
}
