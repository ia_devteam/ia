using System;
namespace test
{
  public class BaseClass
  {
    public BaseClass()
    {
    }
    public string Hello {
      get {
        System.String RNTRNTRNT_2 = "Hello";
        IACSharpSensor.IACSharpSensor.SensorReached(15);
        return RNTRNTRNT_2;
      }
    }
  }
  public abstract class Printer
  {
    public abstract void Print<T>(object x) where T : BaseClass;
  }
  public class PrinterImpl : Printer
  {
    public override void Print<T>(object x)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(16);
      Console.WriteLine((x as T).Hello);
      IACSharpSensor.IACSharpSensor.SensorReached(17);
    }
  }
  public class Starter
  {
    public static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(18);
      BaseClass bc = new BaseClass();
      Printer p = new PrinterImpl();
      p.Print<BaseClass>(bc);
      IACSharpSensor.IACSharpSensor.SensorReached(19);
    }
  }
}
