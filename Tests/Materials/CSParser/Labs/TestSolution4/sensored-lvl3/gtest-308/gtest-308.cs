using System;
public class Test
{
  public static Comparison<U> WrapComparison<U>(Comparison<U> comparison)
  {
    Comparison<U> RNTRNTRNT_7 = delegate(U x, U y) { return comparison(x, y); };
    IACSharpSensor.IACSharpSensor.SensorReached(26);
    return RNTRNTRNT_7;
  }
  public delegate int MyComparison<V>(V x, V y);
  public static MyComparison<W> WrapMyComparison<W>(MyComparison<W> myComparison)
  {
    MyComparison<W> RNTRNTRNT_8 = delegate(W x, W y) { return myComparison(x, y); };
    IACSharpSensor.IACSharpSensor.SensorReached(27);
    return RNTRNTRNT_8;
  }
}
public class Foo
{
  static int compare(int x, int y)
  {
    System.Int32 RNTRNTRNT_9 = x - y;
    IACSharpSensor.IACSharpSensor.SensorReached(28);
    return RNTRNTRNT_9;
  }
  static int compare(string x, string y)
  {
    System.Int32 RNTRNTRNT_10 = string.Compare(x, y);
    IACSharpSensor.IACSharpSensor.SensorReached(29);
    return RNTRNTRNT_10;
  }
  static void test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(30);
    if (i != 0) {
      throw new Exception("" + i);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(31);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(32);
    Comparison<int> ci = Test.WrapComparison<int>(compare);
    Comparison<string> cs = Test.WrapComparison<string>(compare);
    Test.MyComparison<int> mci = Test.WrapMyComparison<int>(compare);
    Test.MyComparison<string> mcs = Test.WrapMyComparison<string>(compare);
    test(ci(1, 1));
    test(cs("h", "h"));
    test(mci(2, 2));
    test(mcs("g", "g"));
    IACSharpSensor.IACSharpSensor.SensorReached(33);
  }
}
