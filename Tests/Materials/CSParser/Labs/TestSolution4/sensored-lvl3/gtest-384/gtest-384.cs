namespace N
{
  public class TestG
  {
    public static void Foo<T>()
    {
    }
  }
}
class NonGeneric
{
}
class Generic<T>
{
}
class m
{
  public global::NonGeneric compiles_fine(global::NonGeneric i, out global::NonGeneric o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(331);
    o = new global::NonGeneric();
    NonGeneric RNTRNTRNT_114 = new global::NonGeneric();
    IACSharpSensor.IACSharpSensor.SensorReached(332);
    return RNTRNTRNT_114;
  }
  public global::Generic<int> does_not_compile(global::Generic<int> i, out global::Generic<int> o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(333);
    o = new global::Generic<int>();
    Generic<System.Int32> RNTRNTRNT_115 = new global::Generic<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(334);
    return RNTRNTRNT_115;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(335);
    global::N.TestG.Foo<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(336);
  }
}
