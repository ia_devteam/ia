using System;
namespace BugReport
{
  class Program
  {
    static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(235);
      A a = new A();
      a.Counter++;
      if (a.Counter != null) {
        System.Int32 RNTRNTRNT_67 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(236);
        return RNTRNTRNT_67;
      }
      ++a.Counter;
      if (a.Counter != null) {
        System.Int32 RNTRNTRNT_68 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(237);
        return RNTRNTRNT_68;
      }
      a.Counter = 0;
      a.Counter++;
      if (a.Counter != 1) {
        System.Int32 RNTRNTRNT_69 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(238);
        return RNTRNTRNT_69;
      }
      ++a.Counter;
      if (a.Counter != 2) {
        System.Int32 RNTRNTRNT_70 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(239);
        return RNTRNTRNT_70;
      }
      Console.WriteLine("OK");
      System.Int32 RNTRNTRNT_71 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(240);
      return RNTRNTRNT_71;
    }
  }
  class A
  {
    private int? _counter;
    public int? Counter {
      get {
        System.Nullable<System.Int32> RNTRNTRNT_72 = _counter;
        IACSharpSensor.IACSharpSensor.SensorReached(241);
        return RNTRNTRNT_72;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(242);
        _counter = value;
        IACSharpSensor.IACSharpSensor.SensorReached(243);
      }
    }
  }
}
