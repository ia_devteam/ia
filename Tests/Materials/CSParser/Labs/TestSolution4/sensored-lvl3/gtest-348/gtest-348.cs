using System;
public class Bar<U> where U : EventArgs
{
  internal void OnWorldDestroyed()
  {
  }
}
public class Baz<U> where U : Bar<EventArgs>
{
  public void DestroyWorld(U bar)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(190);
    bar.OnWorldDestroyed();
    IACSharpSensor.IACSharpSensor.SensorReached(191);
  }
}
public class Bling
{
  public static void Main()
  {
  }
}
