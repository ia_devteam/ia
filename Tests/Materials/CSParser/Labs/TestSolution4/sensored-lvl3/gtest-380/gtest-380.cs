using System.Reflection;
using System;
class Test
{
  public volatile int field;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(293);
    FieldInfo fi = typeof(Test).GetField("field");
    if (fi.GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_96 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(294);
      return RNTRNTRNT_96;
    }
    Type[] t = fi.GetRequiredCustomModifiers();
    if (t.Length != 1) {
      System.Int32 RNTRNTRNT_97 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(295);
      return RNTRNTRNT_97;
    }
    if (t[0] != typeof(System.Runtime.CompilerServices.IsVolatile)) {
      System.Int32 RNTRNTRNT_98 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(296);
      return RNTRNTRNT_98;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_99 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(297);
    return RNTRNTRNT_99;
  }
}
