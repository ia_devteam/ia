interface ITest
{
  void Test();
}
class Tester<T> where T : ITest, new()
{
  public void Do()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(81);
    new T().Test();
    IACSharpSensor.IACSharpSensor.SensorReached(82);
  }
}
class Reference : ITest
{
  public void Test()
  {
  }
}
struct Value : ITest
{
  public void Test()
  {
  }
}
class C
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(83);
    new Tester<Reference>().Do();
    new Tester<Value>().Do();
    IACSharpSensor.IACSharpSensor.SensorReached(84);
  }
}
