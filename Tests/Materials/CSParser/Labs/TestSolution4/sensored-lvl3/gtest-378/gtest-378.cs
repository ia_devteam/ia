class Test
{
  static object Foo(int? i)
  {
    System.Object RNTRNTRNT_86 = (object)i;
    IACSharpSensor.IACSharpSensor.SensorReached(281);
    return RNTRNTRNT_86;
  }
  static object FooG<T>(T? i) where T : struct
  {
    System.Object RNTRNTRNT_87 = (object)i;
    IACSharpSensor.IACSharpSensor.SensorReached(282);
    return RNTRNTRNT_87;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    object o = Foo(null);
    if (o != null) {
      System.Int32 RNTRNTRNT_88 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(284);
      return RNTRNTRNT_88;
    }
    o = FooG<bool>(null);
    if (o != null) {
      System.Int32 RNTRNTRNT_89 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(285);
      return RNTRNTRNT_89;
    }
    System.Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_90 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    return RNTRNTRNT_90;
  }
}
