using System;
public class Foo<T>
{
  public void Map<S>(S value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(95);
    Foo<S> result = new Foo<S>();
    result.Test(value);
    IACSharpSensor.IACSharpSensor.SensorReached(96);
  }
  protected virtual void Test(T value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(97);
    Console.WriteLine(value);
    IACSharpSensor.IACSharpSensor.SensorReached(98);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(99);
    Foo<double> a = new Foo<double>();
    a.Map<string>("Hello World");
    IACSharpSensor.IACSharpSensor.SensorReached(100);
  }
}
