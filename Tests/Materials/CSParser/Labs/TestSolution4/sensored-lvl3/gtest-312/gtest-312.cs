using System.Collections.Generic;
class CantCastGenericListToArray
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(38);
    IList<string> list = new string[] {
      "foo",
      "bar"
    };
    string[] array = (string[])list;
    if (list.Count != array.Length) {
      throw new System.ApplicationException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(39);
  }
}
