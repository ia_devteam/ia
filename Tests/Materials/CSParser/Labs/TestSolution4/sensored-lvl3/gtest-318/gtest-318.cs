public class Foo<K>
{
}
public class Bar<Q> : Goo<Q>
{
  public class Baz
  {
  }
}
public class Goo<Q> : Foo<Bar<Q>.Baz>
{
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(54);
    Bar<int> bar = new Bar<int>();
    System.Console.WriteLine(bar);
    IACSharpSensor.IACSharpSensor.SensorReached(55);
  }
}
