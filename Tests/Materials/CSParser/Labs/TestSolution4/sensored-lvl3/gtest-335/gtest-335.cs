using System;
public class TestClass
{
  public static bool Test_1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(113);
    DayOfWeek? testEnum = DayOfWeek.Monday;
    switch (testEnum) {
      case DayOfWeek.Monday:
        System.Boolean RNTRNTRNT_23 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(114);
        return RNTRNTRNT_23;
    }
    System.Boolean RNTRNTRNT_24 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(115);
    return RNTRNTRNT_24;
  }
  public static bool Test_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(116);
    DayOfWeek? testEnum = null;
    switch (testEnum) {
      case DayOfWeek.Monday:
        System.Boolean RNTRNTRNT_25 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(117);
        return RNTRNTRNT_25;
      case null:
        System.Boolean RNTRNTRNT_26 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(118);
        return RNTRNTRNT_26;
      default:
        System.Boolean RNTRNTRNT_27 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(119);
        return RNTRNTRNT_27;
    }
  }
  public static bool Test_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(120);
    DayOfWeek? testEnum = null;
    switch (testEnum) {
      case DayOfWeek.Monday:
        System.Boolean RNTRNTRNT_28 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(121);
        return RNTRNTRNT_28;
      default:
        System.Boolean RNTRNTRNT_29 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(122);
        return RNTRNTRNT_29;
    }
  }
  public static bool Test_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(123);
    DayOfWeek? testEnum = DayOfWeek.Monday;
    switch (testEnum) {
    }
    System.Boolean RNTRNTRNT_30 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(124);
    return RNTRNTRNT_30;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(125);
    if (!Test_1()) {
      System.Int32 RNTRNTRNT_31 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(126);
      return RNTRNTRNT_31;
    }
    if (!Test_2()) {
      System.Int32 RNTRNTRNT_32 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(127);
      return RNTRNTRNT_32;
    }
    if (!Test_3()) {
      System.Int32 RNTRNTRNT_33 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(128);
      return RNTRNTRNT_33;
    }
    if (!Test_4()) {
      System.Int32 RNTRNTRNT_34 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(129);
      return RNTRNTRNT_34;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_35 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(130);
    return RNTRNTRNT_35;
  }
}
