using System;
using System.Reflection;
[AttributeUsage(AttributeTargets.All)]
public class DocAttribute : Attribute
{
  public DocAttribute()
  {
  }
  public DocAttribute(string s)
  {
  }
}
public delegate void Func<TArg, TRet>();
class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(337);
    Type[] targs = typeof(Func<, >).GetGenericArguments();
    if (targs[0].GetCustomAttributes(false).Length != 1) {
      System.Int32 RNTRNTRNT_116 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(338);
      return RNTRNTRNT_116;
    }
    if (targs[1].GetCustomAttributes(false).Length != 1) {
      System.Int32 RNTRNTRNT_117 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(339);
      return RNTRNTRNT_117;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_118 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(340);
    return RNTRNTRNT_118;
  }
}
