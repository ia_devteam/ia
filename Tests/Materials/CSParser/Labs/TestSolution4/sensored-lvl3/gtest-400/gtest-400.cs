using System;
class Gen<T> where T : class
{
  public static bool Foo(T t)
  {
    System.Boolean RNTRNTRNT_155 = t is Program;
    IACSharpSensor.IACSharpSensor.SensorReached(415);
    return RNTRNTRNT_155;
  }
}
class Program
{
  static bool Foo<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(416);
    object o = 1;
    System.Boolean RNTRNTRNT_156 = o is T;
    IACSharpSensor.IACSharpSensor.SensorReached(417);
    return RNTRNTRNT_156;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(418);
    if (Foo<bool>()) {
      System.Int32 RNTRNTRNT_157 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(419);
      return RNTRNTRNT_157;
    }
    if (!Foo<int>()) {
      System.Int32 RNTRNTRNT_158 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(420);
      return RNTRNTRNT_158;
    }
    if (Gen<object>.Foo(null)) {
      System.Int32 RNTRNTRNT_159 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(421);
      return RNTRNTRNT_159;
    }
    if (!Gen<Program>.Foo(new Program())) {
      System.Int32 RNTRNTRNT_160 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(422);
      return RNTRNTRNT_160;
    }
    Console.WriteLine("ok");
    System.Int32 RNTRNTRNT_161 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(423);
    return RNTRNTRNT_161;
  }
}
