using System.Collections.Generic;
using System.Collections;
public class Temp<T> : IEnumerable<Temp<T>.Foo>
{
  public class Foo
  {
  }
  public IEnumerator<Temp<T>.Foo> GetEnumerator()
  {
    Temp<T> RNTRNTRNT_11 = new Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(56);
    yield return RNTRNTRNT_11;
    IACSharpSensor.IACSharpSensor.SensorReached(57);
    IACSharpSensor.IACSharpSensor.SensorReached(58);
  }
  IEnumerator IEnumerable.GetEnumerator()
  {
    IEnumerator RNTRNTRNT_12 = GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(59);
    return RNTRNTRNT_12;
  }
}
class X
{
  static void Main()
  {
  }
}
