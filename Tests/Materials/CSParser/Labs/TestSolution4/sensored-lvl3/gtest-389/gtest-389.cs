using System;
enum MyEnum : byte
{
  A = 1,
  B = 2,
  Z = 255
}
class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(355);
    MyEnum? e = MyEnum.A;
    byte? b = 255;
    MyEnum? res = e + b;
    if (res != 0) {
      System.Int32 RNTRNTRNT_125 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(356);
      return RNTRNTRNT_125;
    }
    e = null;
    b = 255;
    res = e + b;
    if (res != null) {
      System.Int32 RNTRNTRNT_126 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(357);
      return RNTRNTRNT_126;
    }
    MyEnum e2 = MyEnum.A;
    byte b2 = 1;
    MyEnum res2 = e2 + b2;
    if (res2 != MyEnum.B) {
      System.Int32 RNTRNTRNT_127 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(358);
      return RNTRNTRNT_127;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_128 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(359);
    return RNTRNTRNT_128;
  }
}
