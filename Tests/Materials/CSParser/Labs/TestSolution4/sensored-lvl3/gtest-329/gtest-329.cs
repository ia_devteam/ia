using System;
public class NullableInt
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(89);
    object x = null;
    int? y = x as int?;
    Console.WriteLine("y: '{0}'", y);
    Console.WriteLine("y.HasValue: '{0}'", y.HasValue);
    IACSharpSensor.IACSharpSensor.SensorReached(90);
  }
}
