using System;
class C<T>
{
  public Type Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(101);
    T[,] a = new T[0, 0];
    Type RNTRNTRNT_17 = a.GetType();
    IACSharpSensor.IACSharpSensor.SensorReached(102);
    return RNTRNTRNT_17;
  }
}
class M
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(103);
    C<string> c1 = new C<string>();
    C<bool> c2 = new C<bool>();
    if (c1.Test() != typeof(string[,])) {
      throw new InvalidCastException();
    }
    if (c2.Test() != typeof(bool[,])) {
      throw new InvalidCastException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(104);
  }
}
