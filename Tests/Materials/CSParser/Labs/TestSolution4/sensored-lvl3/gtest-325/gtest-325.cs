public class SomeClass<T> where T : new()
{
  public void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(72);
    new T();
    IACSharpSensor.IACSharpSensor.SensorReached(73);
  }
}
class Foo
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(74);
    SomeClass<object> x = new SomeClass<object>();
    x.Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(75);
  }
}
