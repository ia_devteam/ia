using System;
public class X
{
  public static bool Compute(int x)
  {
    System.Boolean RNTRNTRNT_80 = x == null;
    IACSharpSensor.IACSharpSensor.SensorReached(270);
    return RNTRNTRNT_80;
  }
  public static bool Compute2(int x)
  {
    System.Boolean RNTRNTRNT_81 = x != null;
    IACSharpSensor.IACSharpSensor.SensorReached(271);
    return RNTRNTRNT_81;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(272);
    if (Compute(1) != false) {
      System.Int32 RNTRNTRNT_82 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(273);
      return RNTRNTRNT_82;
    }
    if (Compute2(1) != true) {
      System.Int32 RNTRNTRNT_83 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(274);
      return RNTRNTRNT_83;
    }
    System.Int32 RNTRNTRNT_84 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(275);
    return RNTRNTRNT_84;
  }
}
