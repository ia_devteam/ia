using System;
using System.Collections;
using System.Collections.Generic;
interface ITest : IEnumerable<int>
{
}
class Test : ITest
{
  IEnumerator IEnumerable.GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(6);
    throw new Exception();
  }
  IEnumerator<int> IEnumerable<int>.GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(7);
    yield break;
  }
}
class M
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(8);
    ITest foo = new Test();
    foreach (int i in foo) {
      Console.WriteLine(i);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(9);
  }
}
