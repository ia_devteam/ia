class C
{
  static internal int Foo<T>(T name, params object[] args)
  {
    System.Int32 RNTRNTRNT_61 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(225);
    return RNTRNTRNT_61;
  }
  static internal int Foo(string name, params object[] args)
  {
    System.Int32 RNTRNTRNT_62 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(226);
    return RNTRNTRNT_62;
  }
  static internal int InvokeMethod(string name, params object[] args)
  {
    System.Int32 RNTRNTRNT_63 = Foo(name, args);
    IACSharpSensor.IACSharpSensor.SensorReached(227);
    return RNTRNTRNT_63;
  }
  public static int Main()
  {
    System.Int32 RNTRNTRNT_64 = InvokeMethod("abc");
    IACSharpSensor.IACSharpSensor.SensorReached(228);
    return RNTRNTRNT_64;
  }
}
