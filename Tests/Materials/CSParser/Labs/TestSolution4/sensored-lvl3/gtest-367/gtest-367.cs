using System;
class Foo
{
}
class Repro
{
  static void Main()
  {
  }
  static void Bar<TFoo>(TFoo foo) where TFoo : Repro
  {
    IACSharpSensor.IACSharpSensor.SensorReached(253);
    Baz(foo, Gazonk);
    IACSharpSensor.IACSharpSensor.SensorReached(254);
  }
  static void Baz<T>(T t, Action<T> a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(255);
    a(t);
    IACSharpSensor.IACSharpSensor.SensorReached(256);
  }
  static void Gazonk(Repro f)
  {
  }
}
