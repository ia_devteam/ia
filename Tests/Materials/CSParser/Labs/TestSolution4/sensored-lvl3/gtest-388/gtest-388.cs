using System;
class Data
{
  public int Value;
}
class Foo
{
  static void f(Data d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(351);
    if (d.Value != 5) {
      throw new Exception();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(352);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(353);
    Data d;
    f(d = new Data { Value = 5 });
    IACSharpSensor.IACSharpSensor.SensorReached(354);
  }
}
