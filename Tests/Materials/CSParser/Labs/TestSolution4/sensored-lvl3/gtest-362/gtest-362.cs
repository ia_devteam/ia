class C
{
  static void Assert<T>(T a)
  {
  }
  static void Assert<T>(T a, T b)
  {
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(231);
    Assert(new object[,] {
      {
        1,
        2
      },
      {
        "x",
        "z"
      }
    });
    Assert(new object(), "a");
    System.Int32 RNTRNTRNT_66 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(232);
    return RNTRNTRNT_66;
  }
}
