using System;
struct Foo
{
  public static bool operator ==(Foo d1, Foo d2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(217);
    throw new ApplicationException();
  }
  public static bool operator !=(Foo d1, Foo d2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(218);
    throw new ApplicationException();
  }
}
public class Test
{
  static Foo ctx;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(219);
    if (ctx == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(220);
      return;
    }
    if (ctx != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(221);
      return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(222);
  }
}
