class C
{
  static internal int Foo<T>(T name, params object[] args)
  {
    System.Int32 RNTRNTRNT_61 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(325);
    return RNTRNTRNT_61;
  }
  static internal int Foo(string name, params object[] args)
  {
    System.Int32 RNTRNTRNT_62 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(326);
    return RNTRNTRNT_62;
  }
  static internal int InvokeMethod(string name, params object[] args)
  {
    System.Int32 RNTRNTRNT_63 = Foo(name, args);
    IACSharpSensor.IACSharpSensor.SensorReached(327);
    return RNTRNTRNT_63;
  }
  public static int Main()
  {
    System.Int32 RNTRNTRNT_64 = InvokeMethod("abc");
    IACSharpSensor.IACSharpSensor.SensorReached(328);
    return RNTRNTRNT_64;
  }
}
