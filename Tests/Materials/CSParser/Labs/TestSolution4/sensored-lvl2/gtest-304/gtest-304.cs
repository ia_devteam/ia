using System;
namespace test
{
  public class BaseClass
  {
    public BaseClass()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(20);
    }
    public string Hello {
      get {
        System.String RNTRNTRNT_2 = "Hello";
        IACSharpSensor.IACSharpSensor.SensorReached(21);
        return RNTRNTRNT_2;
      }
    }
  }
  public abstract class Printer
  {
    public abstract void Print<T>(object x) where T : BaseClass;
  }
  public class PrinterImpl : Printer
  {
    public override void Print<T>(object x)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(22);
      Console.WriteLine((x as T).Hello);
      IACSharpSensor.IACSharpSensor.SensorReached(23);
    }
  }
  public class Starter
  {
    public static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(24);
      BaseClass bc = new BaseClass();
      Printer p = new PrinterImpl();
      p.Print<BaseClass>(bc);
      IACSharpSensor.IACSharpSensor.SensorReached(25);
    }
  }
}
