using System;
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(66);
  }
}
class Foo
{
  public int X;
}
abstract class Base
{
  public abstract void Method<R>() where R : Foo, new();
}
class Derived : Base
{
  public override void Method<S>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(67);
    Method2<S>();
    IACSharpSensor.IACSharpSensor.SensorReached(68);
  }
  public void Method2<T>() where T : Foo, new()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(69);
    T t = new T();
    Console.WriteLine(t.X);
    IACSharpSensor.IACSharpSensor.SensorReached(70);
  }
}
