using System;
enum MyEnum : byte
{
  A = 1,
  B = 2,
  Z = 255
}
class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(524);
    MyEnum? e = MyEnum.A;
    byte? b = 255;
    MyEnum? res = e + b;
    IACSharpSensor.IACSharpSensor.SensorReached(525);
    if (res != 0) {
      System.Int32 RNTRNTRNT_125 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(526);
      return RNTRNTRNT_125;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(527);
    e = null;
    b = 255;
    res = e + b;
    IACSharpSensor.IACSharpSensor.SensorReached(528);
    if (res != null) {
      System.Int32 RNTRNTRNT_126 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(529);
      return RNTRNTRNT_126;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(530);
    MyEnum e2 = MyEnum.A;
    byte b2 = 1;
    MyEnum res2 = e2 + b2;
    IACSharpSensor.IACSharpSensor.SensorReached(531);
    if (res2 != MyEnum.B) {
      System.Int32 RNTRNTRNT_127 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(532);
      return RNTRNTRNT_127;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(533);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_128 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(534);
    return RNTRNTRNT_128;
  }
}
