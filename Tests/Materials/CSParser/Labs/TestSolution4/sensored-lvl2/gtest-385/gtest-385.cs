using System;
using System.Reflection;
[AttributeUsage(AttributeTargets.All)]
public class DocAttribute : Attribute
{
  public DocAttribute()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(493);
  }
  public DocAttribute(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(494);
  }
}
public delegate void Func<TArg, TRet>();
class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(495);
    Type[] targs = typeof(Func<, >).GetGenericArguments();
    IACSharpSensor.IACSharpSensor.SensorReached(496);
    if (targs[0].GetCustomAttributes(false).Length != 1) {
      System.Int32 RNTRNTRNT_116 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(497);
      return RNTRNTRNT_116;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(498);
    if (targs[1].GetCustomAttributes(false).Length != 1) {
      System.Int32 RNTRNTRNT_117 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(499);
      return RNTRNTRNT_117;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(500);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_118 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(501);
    return RNTRNTRNT_118;
  }
}
