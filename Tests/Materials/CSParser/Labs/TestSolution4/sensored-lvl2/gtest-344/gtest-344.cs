using System;
public abstract class ConfigurationElement
{
  protected ConfigurationElement()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(256);
  }
}
public class CustomConfigurationElement : ConfigurationElement
{
}
public class CustomConfigurationElementCollection : BaseCollection<CustomConfigurationElement>
{
}
public class BaseCollection<T> where T : ConfigurationElement, new()
{
}
