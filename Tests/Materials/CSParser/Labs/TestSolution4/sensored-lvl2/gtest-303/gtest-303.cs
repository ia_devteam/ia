using System;
namespace test
{
  public class BaseClass
  {
    public BaseClass()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(14);
    }
    public string Hello {
      get {
        System.String RNTRNTRNT_1 = "Hello";
        IACSharpSensor.IACSharpSensor.SensorReached(15);
        return RNTRNTRNT_1;
      }
    }
  }
  public abstract class Printer
  {
    public abstract void Print<T>(T obj) where T : BaseClass;
  }
  public class PrinterImpl : Printer
  {
    public override void Print<T>(T obj)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(16);
      Console.WriteLine(obj.Hello);
      IACSharpSensor.IACSharpSensor.SensorReached(17);
    }
  }
  public class Starter
  {
    public static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(18);
      BaseClass bc = new BaseClass();
      Printer p = new PrinterImpl();
      p.Print<BaseClass>(bc);
      IACSharpSensor.IACSharpSensor.SensorReached(19);
    }
  }
}
