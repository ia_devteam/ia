using System;
class Data
{
  public int Value;
}
class Foo
{
  static void f(Data d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(519);
    if (d.Value != 5) {
      IACSharpSensor.IACSharpSensor.SensorReached(520);
      throw new Exception();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(521);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(522);
    Data d;
    f(d = new Data { Value = 5 });
    IACSharpSensor.IACSharpSensor.SensorReached(523);
  }
}
