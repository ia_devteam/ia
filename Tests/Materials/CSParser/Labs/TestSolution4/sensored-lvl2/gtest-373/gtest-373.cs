using System;
namespace MonoBug
{
  class MainClass
  {
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(392);
      GenericType<bool> g = new GenericType<bool>(true);
      IACSharpSensor.IACSharpSensor.SensorReached(393);
      if (g) {
        IACSharpSensor.IACSharpSensor.SensorReached(394);
        Console.WriteLine("true");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(395);
    }
  }
  public class GenericType<T>
  {
    private T value;
    public GenericType(T value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(396);
      this.value = value;
      IACSharpSensor.IACSharpSensor.SensorReached(397);
    }
    public static implicit operator T(GenericType<T> o)
    {
      T RNTRNTRNT_79 = o.value;
      IACSharpSensor.IACSharpSensor.SensorReached(398);
      return RNTRNTRNT_79;
    }
  }
}
