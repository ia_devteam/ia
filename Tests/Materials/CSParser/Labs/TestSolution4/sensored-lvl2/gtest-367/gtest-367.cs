using System;
class Foo
{
}
class Repro
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(370);
  }
  static void Bar<TFoo>(TFoo foo) where TFoo : Repro
  {
    IACSharpSensor.IACSharpSensor.SensorReached(371);
    Baz(foo, Gazonk);
    IACSharpSensor.IACSharpSensor.SensorReached(372);
  }
  static void Baz<T>(T t, Action<T> a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(373);
    a(t);
    IACSharpSensor.IACSharpSensor.SensorReached(374);
  }
  static void Gazonk(Repro f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(375);
  }
}
