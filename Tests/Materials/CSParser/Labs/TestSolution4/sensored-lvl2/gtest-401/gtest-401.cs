using System;
class ParserTest
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(624);
    int[] ivals = {
      2,
      5
    };
    IACSharpSensor.IACSharpSensor.SensorReached(625);
    foreach (int x in ivals) {
      IACSharpSensor.IACSharpSensor.SensorReached(626);
      foreach (int y in ivals) {
        IACSharpSensor.IACSharpSensor.SensorReached(627);
        Console.WriteLine("{0} {1} {2} {3} {4} {5}", x, y, x + y, x - y, x < y, x >= y);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(628);
  }
}
