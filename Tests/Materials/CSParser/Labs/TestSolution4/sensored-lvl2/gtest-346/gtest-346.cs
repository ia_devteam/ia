public class test
{
  public void CreateSimpleCallSite(int x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(261);
  }
  public void CreateSimpleCallSite<A>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(262);
  }
  public void CreateSimpleCallSite<A>(int x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(263);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(264);
  }
}
