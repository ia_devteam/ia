using System;
using System.Linq.Expressions;
public struct MyType
{
  int value;
  public MyType(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(502);
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(503);
  }
  public static MyType operator -(MyType a)
  {
    MyType RNTRNTRNT_119 = new MyType(-a.value);
    IACSharpSensor.IACSharpSensor.SensorReached(504);
    return RNTRNTRNT_119;
  }
}
class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(505);
    MyType? x = null;
    MyType? y = -x;
    IACSharpSensor.IACSharpSensor.SensorReached(506);
    checked {
      IACSharpSensor.IACSharpSensor.SensorReached(507);
      float? f = float.MinValue;
      f = -f + 1;
      int? i = int.MinValue;
      IACSharpSensor.IACSharpSensor.SensorReached(508);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(509);
        i = -i;
        System.Int32 RNTRNTRNT_120 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(510);
        return RNTRNTRNT_120;
      } catch (OverflowException) {
      }
    }
    System.Int32 RNTRNTRNT_121 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(511);
    return RNTRNTRNT_121;
  }
}
