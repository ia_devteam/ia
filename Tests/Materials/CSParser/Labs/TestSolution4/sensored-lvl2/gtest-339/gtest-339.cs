using System;
using System.Collections;
using System.Collections.Generic;
class Program
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(222);
    SerializeDictionary(new SerializerLazyDictionary());
    IACSharpSensor.IACSharpSensor.SensorReached(223);
  }
  static void SerializeDictionary(IDictionary values)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(224);
  }
  static void SerializeDictionary(IDictionary<string, object> values)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(225);
  }
}
sealed class SerializerLazyDictionary : LazyDictionary
{
  protected override IEnumerator<KeyValuePair<string, object>> GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(226);
    return null;
  }
}
internal abstract class LazyDictionary : IDictionary<string, object>
{
  void IDictionary<string, object>.Add(string key, object value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(227);
    throw new NotSupportedException();
  }
  bool IDictionary<string, object>.ContainsKey(string key)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(228);
    throw new NotSupportedException();
  }
  ICollection<string> IDictionary<string, object>.Keys {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(229);
      throw new NotSupportedException();
    }
  }
  bool IDictionary<string, object>.Remove(string key)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(230);
    throw new NotSupportedException();
  }
  bool IDictionary<string, object>.TryGetValue(string key, out object value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(231);
    throw new NotSupportedException();
  }
  ICollection<object> IDictionary<string, object>.Values {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(232);
      throw new NotSupportedException();
    }
  }
  object IDictionary<string, object>.this[string key] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(233);
      throw new NotSupportedException();
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(234);
      throw new NotSupportedException();
    }
  }
  void ICollection<KeyValuePair<string, object>>.Add(KeyValuePair<string, object> item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(235);
    throw new NotSupportedException();
  }
  void ICollection<KeyValuePair<string, object>>.Clear()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(236);
    throw new NotSupportedException();
  }
  bool ICollection<KeyValuePair<string, object>>.Contains(KeyValuePair<string, object> item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(237);
    throw new NotSupportedException();
  }
  void ICollection<KeyValuePair<string, object>>.CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    throw new NotSupportedException();
  }
  int ICollection<KeyValuePair<string, object>>.Count {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(239);
      throw new NotSupportedException();
    }
  }
  bool ICollection<KeyValuePair<string, object>>.IsReadOnly {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(240);
      throw new NotSupportedException();
    }
  }
  bool ICollection<KeyValuePair<string, object>>.Remove(KeyValuePair<string, object> item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(241);
    throw new NotSupportedException();
  }
  IEnumerator<KeyValuePair<string, object>> IEnumerable<KeyValuePair<string, object>>.GetEnumerator()
  {
    IEnumerator<KeyValuePair<System.String,System.Object>> RNTRNTRNT_37 = GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(242);
    return RNTRNTRNT_37;
  }
  protected abstract IEnumerator<KeyValuePair<string, object>> GetEnumerator();
  System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
  {
    System.Collections.IEnumerator RNTRNTRNT_38 = ((IEnumerable<KeyValuePair<string, object>>)this).GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(243);
    return RNTRNTRNT_38;
  }
}
