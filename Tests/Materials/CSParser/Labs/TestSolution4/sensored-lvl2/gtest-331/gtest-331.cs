using System;
public class Foo<T>
{
  public void Map<S>(S value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(132);
    Foo<S> result = new Foo<S>();
    result.Test(value);
    IACSharpSensor.IACSharpSensor.SensorReached(133);
  }
  protected virtual void Test(T value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(134);
    Console.WriteLine(value);
    IACSharpSensor.IACSharpSensor.SensorReached(135);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(136);
    Foo<double> a = new Foo<double>();
    a.Map<string>("Hello World");
    IACSharpSensor.IACSharpSensor.SensorReached(137);
  }
}
