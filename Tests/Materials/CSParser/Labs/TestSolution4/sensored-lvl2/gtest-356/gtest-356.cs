class A<T>
{
  void Foo(B<T>.E arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(310);
  }
}
class B<U> : B
{
}
class B
{
  public class E
  {
  }
}
class C
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(311);
  }
}
