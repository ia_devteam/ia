using System;
using System.Diagnostics;
public class ShiftReduceParser<TokenValueType, TokenLocationType>
{
  [Conditional("DUMP")]
  public static void Dump(string format)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(257);
    throw new ApplicationException();
  }
}
public class Parser : ShiftReduceParser<int, int>
{
  [Conditional("DUMP")]
  static void NoCall<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(258);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(259);
    Dump("Should not be called");
    NoCall(1);
    System.Int32 RNTRNTRNT_44 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(260);
    return RNTRNTRNT_44;
  }
}
