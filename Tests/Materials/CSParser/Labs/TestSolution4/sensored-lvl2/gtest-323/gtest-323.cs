public class MyBase<K, V>
{
  public class Callback
  {
  }
  public void Hello(Callback cb)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(94);
  }
}
public class X : MyBase<string, int>
{
  public X(Callback cb)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(95);
  }
  public void Test(Callback cb)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(96);
    Hello(cb);
    IACSharpSensor.IACSharpSensor.SensorReached(97);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(98);
  }
}
