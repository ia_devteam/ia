class BaseGeneric<T>
{
  public class InnerDerived
  {
    public InnerDerived(T t)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(320);
    }
  }
  public class GenericInnerDerived<U>
  {
    public GenericInnerDerived(T t, U u)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(321);
    }
  }
}
class BaseConcrete : BaseGeneric<string>
{
}
class Concrete_A : BaseGeneric<int>
{
}
class Concrete_B : BaseConcrete
{
  InnerDerived foo1;
}
class BaseGeneric_2<T, U>
{
  public class InnerDerived
  {
    public InnerDerived(T t, U u)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(322);
    }
  }
}
class BaseGeneric_1<T> : BaseGeneric_2<T, string>
{
}
class Concrete_2 : BaseGeneric_1<bool>
{
}
class Program
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(323);
    new Concrete_B.InnerDerived("abc");
    new Concrete_A.InnerDerived(11);
    new Concrete_A.GenericInnerDerived<int>(1, 2);
    new Concrete_2.InnerDerived(false, "bb");
    IACSharpSensor.IACSharpSensor.SensorReached(324);
  }
}
