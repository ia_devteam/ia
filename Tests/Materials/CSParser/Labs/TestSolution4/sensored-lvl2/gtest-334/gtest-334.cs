using System;
public class Test
{
  public delegate void DelegateA(bool b);
  public delegate int DelegateB(int i);
  static DelegateA dt;
  static DelegateB dt2;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(149);
    bool b = DelegateMethod == dt;
    IACSharpSensor.IACSharpSensor.SensorReached(150);
    if (b) {
      System.Int32 RNTRNTRNT_18 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(151);
      return RNTRNTRNT_18;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(152);
    b = DelegateMethod != dt;
    IACSharpSensor.IACSharpSensor.SensorReached(153);
    if (!b) {
      System.Int32 RNTRNTRNT_19 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(154);
      return RNTRNTRNT_19;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(155);
    b = dt2 == DelegateMethod;
    IACSharpSensor.IACSharpSensor.SensorReached(156);
    if (b) {
      System.Int32 RNTRNTRNT_20 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(157);
      return RNTRNTRNT_20;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(158);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_21 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(159);
    return RNTRNTRNT_21;
  }
  static void DelegateMethod(bool b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(160);
  }
  static int DelegateMethod(int b)
  {
    System.Int32 RNTRNTRNT_22 = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(161);
    return RNTRNTRNT_22;
  }
}
