using System;
public interface IFace
{
  void Tst(IFace b);
}
public delegate string ToStr(string format, IFormatProvider format_provider);
public class GenericClass<T> where T : IFormattable
{
  T field;
  public GenericClass(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(598);
    this.field = t;
    IACSharpSensor.IACSharpSensor.SensorReached(599);
  }
  public void Method()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(600);
    ToStr str = new ToStr(field.ToString);
    Console.WriteLine(str("x", null));
    IACSharpSensor.IACSharpSensor.SensorReached(601);
  }
  public void Test(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(602);
  }
}
public class Foo
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(603);
    GenericClass<int> example = new GenericClass<int>(99);
    example.Method();
    IACSharpSensor.IACSharpSensor.SensorReached(604);
  }
}
