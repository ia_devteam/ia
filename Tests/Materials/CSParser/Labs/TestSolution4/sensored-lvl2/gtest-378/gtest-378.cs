class Test
{
  static object Foo(int? i)
  {
    System.Object RNTRNTRNT_86 = (object)i;
    IACSharpSensor.IACSharpSensor.SensorReached(414);
    return RNTRNTRNT_86;
  }
  static object FooG<T>(T? i) where T : struct
  {
    System.Object RNTRNTRNT_87 = (object)i;
    IACSharpSensor.IACSharpSensor.SensorReached(415);
    return RNTRNTRNT_87;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(416);
    object o = Foo(null);
    IACSharpSensor.IACSharpSensor.SensorReached(417);
    if (o != null) {
      System.Int32 RNTRNTRNT_88 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(418);
      return RNTRNTRNT_88;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(419);
    o = FooG<bool>(null);
    IACSharpSensor.IACSharpSensor.SensorReached(420);
    if (o != null) {
      System.Int32 RNTRNTRNT_89 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(421);
      return RNTRNTRNT_89;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(422);
    System.Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_90 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(423);
    return RNTRNTRNT_90;
  }
}
