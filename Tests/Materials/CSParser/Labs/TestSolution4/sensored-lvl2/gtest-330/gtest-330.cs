using System;
using System.Collections;
using System.Collections.Generic;
public class BaseCollection<T> : IEnumerable<T>
{
  protected List<T> items = new List<T>();
  IEnumerator<T> IEnumerable<T>.GetEnumerator()
  {
    IEnumerator<T> RNTRNTRNT_15 = items.GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(126);
    return RNTRNTRNT_15;
  }
  IEnumerator IEnumerable.GetEnumerator()
  {
    IEnumerator RNTRNTRNT_16 = items.GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(127);
    return RNTRNTRNT_16;
  }
}
public class BaseIntList<T> : BaseCollection<T>
{
}
public class IntList : BaseIntList<int>
{
}
class X
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(128);
    IntList list = new IntList();
    IACSharpSensor.IACSharpSensor.SensorReached(129);
    foreach (int i in list) {
      IACSharpSensor.IACSharpSensor.SensorReached(130);
      Console.WriteLine(i);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(131);
  }
}
