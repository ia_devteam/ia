using System;
using System.Reflection;
class T
{
  const byte c = 0;
  const string s = null;
  long[,,] a1 = new long[,,] {
    {
      {
        10,
        0
      },
      {
        0,
        0
      }
    },
    {
      {
        0,
        0
      },
      {
        0,
        c
      }
    }
  };
  byte[] a2 = new byte[] {
    2 - 2,
    0,
    c
  };
  decimal[] a3 = new decimal[] {
    2m - 2m,
    0m,
    c
  };
  string[,] a4 = new string[,] {
    {
      s,
      null
    },
    {
      s,
      s
    }
  };
  T[] a5 = new T[] {
    null,
    default(T)
  };
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(26);
    ConstructorInfo mi = typeof(T).GetConstructors()[0];
    MethodBody mb = mi.GetMethodBody();
    IACSharpSensor.IACSharpSensor.SensorReached(27);
    if (mb.GetILAsByteArray().Length > 90) {
      IACSharpSensor.IACSharpSensor.SensorReached(28);
      Console.WriteLine("Optimization failed");
      System.Int32 RNTRNTRNT_3 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(29);
      return RNTRNTRNT_3;
    }
    System.Int32 RNTRNTRNT_4 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(30);
    return RNTRNTRNT_4;
  }
}
