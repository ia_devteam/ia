class A
{
  public virtual bool Foo(string s)
  {
    System.Boolean RNTRNTRNT_56 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(304);
    return RNTRNTRNT_56;
  }
  public virtual string Foo<T>(string s)
  {
    System.String RNTRNTRNT_57 = "v";
    IACSharpSensor.IACSharpSensor.SensorReached(305);
    return RNTRNTRNT_57;
  }
}
class B : A
{
  public bool Goo(string s)
  {
    System.Boolean RNTRNTRNT_58 = Foo(s);
    IACSharpSensor.IACSharpSensor.SensorReached(306);
    return RNTRNTRNT_58;
  }
  public override bool Foo(string s)
  {
    System.Boolean RNTRNTRNT_59 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(307);
    return RNTRNTRNT_59;
  }
  public override string Foo<T>(string s)
  {
    System.String RNTRNTRNT_60 = "a";
    IACSharpSensor.IACSharpSensor.SensorReached(308);
    return RNTRNTRNT_60;
  }
}
class C
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(309);
  }
}
