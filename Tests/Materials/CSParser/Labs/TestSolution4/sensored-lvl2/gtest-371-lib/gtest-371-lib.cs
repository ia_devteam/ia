public class Test<A, B>
{
  public void Foo<U>(U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(385);
  }
  public void Foo<V>(V[] v, V w)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(386);
  }
  public void Hello<V, W>(V v, W w, Test<V, W> x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(387);
  }
  public void ArrayMethod<V>(params V[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(388);
  }
}
