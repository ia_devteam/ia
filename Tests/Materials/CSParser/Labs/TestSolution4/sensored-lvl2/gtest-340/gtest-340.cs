using System;
using System.IO;
using System.Reflection;
class Tests
{
  public static T[] FindAll<T>(T[] array, Predicate<T> match)
  {
    T[] RNTRNTRNT_39 = new T[0];
    IACSharpSensor.IACSharpSensor.SensorReached(244);
    return RNTRNTRNT_39;
  }
  private bool ProtectedOnly(MemberInfo input)
  {
    System.Boolean RNTRNTRNT_40 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    return RNTRNTRNT_40;
  }
  public MemberInfo[] foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(246);
    string name = "FOO";
    MemberInfo[] res = FindAll(typeof(int).GetMember(name, BindingFlags.Static | BindingFlags.Instance | BindingFlags.NonPublic), ProtectedOnly);
    MemberInfo[] RNTRNTRNT_41 = res;
    IACSharpSensor.IACSharpSensor.SensorReached(247);
    return RNTRNTRNT_41;
  }
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(248);
    new Tests().foo();
    IACSharpSensor.IACSharpSensor.SensorReached(249);
  }
}
