public class Test
{
  public delegate bool MemberFilter();
  public static void FindMembers(MemberFilter filter)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(564);
  }
  public static void GetMethodGroup(MemberFilter filter)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(565);
    FindMembers(filter ?? delegate() { return true; });
    IACSharpSensor.IACSharpSensor.SensorReached(566);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(567);
  }
}
