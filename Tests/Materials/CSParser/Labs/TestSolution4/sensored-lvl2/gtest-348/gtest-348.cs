using System;
public class Bar<U> where U : EventArgs
{
  internal void OnWorldDestroyed()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(269);
  }
}
public class Baz<U> where U : Bar<EventArgs>
{
  public void DestroyWorld(U bar)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(270);
    bar.OnWorldDestroyed();
    IACSharpSensor.IACSharpSensor.SensorReached(271);
  }
}
public class Bling
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(272);
  }
}
