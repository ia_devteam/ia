using System;
using System.Collections;
using System.Collections.Generic;
interface ITest : IEnumerable<int>
{
}
class Test : ITest
{
  IEnumerator IEnumerable.GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(8);
    throw new Exception();
  }
  IEnumerator<int> IEnumerable<int>.GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(9);
    yield break;
  }
}
class M
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(10);
    ITest foo = new Test();
    IACSharpSensor.IACSharpSensor.SensorReached(11);
    foreach (int i in foo) {
      IACSharpSensor.IACSharpSensor.SensorReached(12);
      Console.WriteLine(i);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(13);
  }
}
