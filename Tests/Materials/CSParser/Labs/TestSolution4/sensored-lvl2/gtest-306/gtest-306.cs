using System;
public partial class FuParentClass<Trow>
{
  public FuParentClass()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(31);
  }
}
public partial class FuParentClass<Trow>
{
  public class FuChildClass
  {
    public FuChildClass()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(32);
    }
  }
}
class C
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(33);
  }
}
