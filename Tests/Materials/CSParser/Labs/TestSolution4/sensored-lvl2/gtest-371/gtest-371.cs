class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(383);
    Test<float, int> test = new Test<float, int>();
    test.Foo("Hello World");
    test.Foo(new long[] {
      3,
      4,
      5
    }, 9L);
    test.Hello(3.14f, 9, test);
    test.ArrayMethod(3.14f, (float)9 / 3);
    IACSharpSensor.IACSharpSensor.SensorReached(384);
  }
}
