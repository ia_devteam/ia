public class TestClass<T> where T : class
{
  public bool Check(T x, T y)
  {
    System.Boolean RNTRNTRNT_77 = x == y;
    IACSharpSensor.IACSharpSensor.SensorReached(389);
    return RNTRNTRNT_77;
  }
}
public class C
{
}
public class TestClass2<T> where T : C
{
  public bool Check(T x, T y)
  {
    System.Boolean RNTRNTRNT_78 = x == y;
    IACSharpSensor.IACSharpSensor.SensorReached(390);
    return RNTRNTRNT_78;
  }
}
public class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(391);
  }
}
