public static class D
{
  static bool? debugging = null;
  static int? extra = 0;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(412);
    debugging |= true;
    extra |= 55;
    IACSharpSensor.IACSharpSensor.SensorReached(413);
  }
}
