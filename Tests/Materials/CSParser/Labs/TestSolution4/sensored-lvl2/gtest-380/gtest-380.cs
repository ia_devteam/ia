using System.Reflection;
using System;
class Test
{
  public volatile int field;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(430);
    FieldInfo fi = typeof(Test).GetField("field");
    IACSharpSensor.IACSharpSensor.SensorReached(431);
    if (fi.GetCustomAttributes(true).Length != 0) {
      System.Int32 RNTRNTRNT_96 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(432);
      return RNTRNTRNT_96;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(433);
    Type[] t = fi.GetRequiredCustomModifiers();
    IACSharpSensor.IACSharpSensor.SensorReached(434);
    if (t.Length != 1) {
      System.Int32 RNTRNTRNT_97 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(435);
      return RNTRNTRNT_97;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(436);
    if (t[0] != typeof(System.Runtime.CompilerServices.IsVolatile)) {
      System.Int32 RNTRNTRNT_98 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(437);
      return RNTRNTRNT_98;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(438);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_99 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(439);
    return RNTRNTRNT_99;
  }
}
