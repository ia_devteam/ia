using System;
struct Foo
{
  public static bool operator ==(Foo d1, Foo d2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(313);
    throw new ApplicationException();
  }
  public static bool operator !=(Foo d1, Foo d2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(314);
    throw new ApplicationException();
  }
}
public class Test
{
  static Foo ctx;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(315);
    if (ctx == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(316);
      return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(317);
    if (ctx != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(318);
      return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(319);
  }
}
