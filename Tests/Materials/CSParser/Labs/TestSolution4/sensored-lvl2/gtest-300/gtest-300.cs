using System;
using System.Collections.Generic;
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
    IDictionary<string, object> c = new Dictionary<string, object>();
    IACSharpSensor.IACSharpSensor.SensorReached(2);
    foreach (string s in c.Keys) {
      IACSharpSensor.IACSharpSensor.SensorReached(3);
      Console.WriteLine(s);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(4);
  }
}
