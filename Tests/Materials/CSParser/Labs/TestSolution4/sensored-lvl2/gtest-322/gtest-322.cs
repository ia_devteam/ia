public class MyBase<K, V>
{
  public delegate void Callback(K key, V value);
  public MyBase(Callback insertionCallback)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(91);
  }
}
public class X : MyBase<string, int>
{
  public X(Callback cb) : base(cb)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(92);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(93);
  }
}
