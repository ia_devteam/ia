public delegate void A();
class B
{
  public static event A D;
  long[] d = new long[1];
  void C()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(99);
    int a = 0;
    int b = 0;
    A block = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(100);
      long c = 0;
      B.D += delegate {
        d[b] = c;
        F(c);
      };
    };
    IACSharpSensor.IACSharpSensor.SensorReached(101);
  }
  public void F(long i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(102);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(103);
  }
}
