using System;
public class Test
{
  [STAThread()]
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(5);
    IShape shape;
    object[] cargs = new object[1] { "Circle" };
    shape = Factory<IShape>.CreateInstance(cargs);
    IACSharpSensor.IACSharpSensor.SensorReached(6);
  }
  interface IShape
  {
  }
}
