using System.Collections.Generic;
using System.Collections;
public class Temp<T> : IEnumerable<Temp<T>.Foo>
{
  public class Foo
  {
  }
  public IEnumerator<Temp<T>.Foo> GetEnumerator()
  {
    Temp<T> RNTRNTRNT_11 = new Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(75);
    yield return RNTRNTRNT_11;
    IACSharpSensor.IACSharpSensor.SensorReached(76);
    IACSharpSensor.IACSharpSensor.SensorReached(77);
  }
  IEnumerator IEnumerable.GetEnumerator()
  {
    IEnumerator RNTRNTRNT_12 = GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(78);
    return RNTRNTRNT_12;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(79);
  }
}
