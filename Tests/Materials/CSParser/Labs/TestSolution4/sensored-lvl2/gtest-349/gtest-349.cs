using System.Collections;
using System;
using System.Reflection;
class X
{
  public delegate R Function<T1, T2, R>(T1 arg1, T2 arg2);
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(273);
    Delegate[] e = new Delegate[] {
      new Function<IList, IList, int>(f2),
      new Function<IList, object, int>(f2)
    };
    IACSharpSensor.IACSharpSensor.SensorReached(274);
    if ((int)e[0].DynamicInvoke(null, null) != 1) {
      System.Int32 RNTRNTRNT_45 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(275);
      return RNTRNTRNT_45;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(276);
    if ((int)e[1].DynamicInvoke(null, null) != 2) {
      System.Int32 RNTRNTRNT_46 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(277);
      return RNTRNTRNT_46;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(278);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_47 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(279);
    return RNTRNTRNT_47;
  }
  static int f2(IList self, IList other)
  {
    System.Int32 RNTRNTRNT_48 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(280);
    return RNTRNTRNT_48;
  }
  static int f2(IList self, object other)
  {
    System.Int32 RNTRNTRNT_49 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(281);
    return RNTRNTRNT_49;
  }
}
