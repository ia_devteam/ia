using System;
class C<T>
{
  public Type Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(138);
    T[,] a = new T[0, 0];
    Type RNTRNTRNT_17 = a.GetType();
    IACSharpSensor.IACSharpSensor.SensorReached(139);
    return RNTRNTRNT_17;
  }
}
class M
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(140);
    C<string> c1 = new C<string>();
    C<bool> c2 = new C<bool>();
    IACSharpSensor.IACSharpSensor.SensorReached(141);
    if (c1.Test() != typeof(string[,])) {
      IACSharpSensor.IACSharpSensor.SensorReached(142);
      throw new InvalidCastException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(143);
    if (c2.Test() != typeof(bool[,])) {
      IACSharpSensor.IACSharpSensor.SensorReached(144);
      throw new InvalidCastException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(145);
  }
}
