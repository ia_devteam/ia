public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(146);
  }
  public static void Foo(object arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(147);
    string result = arg as string ?? "";
    IACSharpSensor.IACSharpSensor.SensorReached(148);
  }
}
