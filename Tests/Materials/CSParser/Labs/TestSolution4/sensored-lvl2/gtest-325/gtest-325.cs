public class SomeClass<T> where T : new()
{
  public void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(104);
    new T();
    IACSharpSensor.IACSharpSensor.SensorReached(105);
  }
}
class Foo
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(106);
    SomeClass<object> x = new SomeClass<object>();
    x.Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(107);
  }
}
