using System;
class Gen<T> where T : class
{
  public static bool Foo(T t)
  {
    System.Boolean RNTRNTRNT_155 = t is Program;
    IACSharpSensor.IACSharpSensor.SensorReached(611);
    return RNTRNTRNT_155;
  }
}
class Program
{
  static bool Foo<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(612);
    object o = 1;
    System.Boolean RNTRNTRNT_156 = o is T;
    IACSharpSensor.IACSharpSensor.SensorReached(613);
    return RNTRNTRNT_156;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(614);
    if (Foo<bool>()) {
      System.Int32 RNTRNTRNT_157 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(615);
      return RNTRNTRNT_157;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(616);
    if (!Foo<int>()) {
      System.Int32 RNTRNTRNT_158 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(617);
      return RNTRNTRNT_158;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(618);
    if (Gen<object>.Foo(null)) {
      System.Int32 RNTRNTRNT_159 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(619);
      return RNTRNTRNT_159;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(620);
    if (!Gen<Program>.Foo(new Program())) {
      System.Int32 RNTRNTRNT_160 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(621);
      return RNTRNTRNT_160;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(622);
    Console.WriteLine("ok");
    System.Int32 RNTRNTRNT_161 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(623);
    return RNTRNTRNT_161;
  }
}
