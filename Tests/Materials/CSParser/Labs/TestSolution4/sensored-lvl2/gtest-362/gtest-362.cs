class C
{
  static void Assert<T>(T a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(333);
  }
  static void Assert<T>(T a, T b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(334);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(335);
    Assert(new object[,] {
      {
        1,
        2
      },
      {
        "x",
        "z"
      }
    });
    Assert(new object(), "a");
    System.Int32 RNTRNTRNT_66 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(336);
    return RNTRNTRNT_66;
  }
}
