public class Foo<K>
{
}
public class Bar<Q> : Foo<Bar<Q>.Baz>
{
  public class Baz
  {
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(71);
    Bar<int> bar = new Bar<int>();
    System.Console.WriteLine(bar);
    IACSharpSensor.IACSharpSensor.SensorReached(72);
  }
}
