using System;
using System.Collections.Generic;
public class App
{
  class MyClass
  {
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(120);
    MyClass mc = new MyClass();
    List<string> l = new List<string>();
    TestMethod("Some format {0}", l, mc);
    IACSharpSensor.IACSharpSensor.SensorReached(121);
  }
  static void TestMethod(string format, List<string> l, params MyClass[] parms)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(122);
    Console.WriteLine(String.Format(format, parms));
    IACSharpSensor.IACSharpSensor.SensorReached(123);
  }
}
