using System;
namespace BugReport
{
  class Program
  {
    static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(340);
      A a = new A();
      a.Counter++;
      IACSharpSensor.IACSharpSensor.SensorReached(341);
      if (a.Counter != null) {
        System.Int32 RNTRNTRNT_67 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(342);
        return RNTRNTRNT_67;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(343);
      ++a.Counter;
      IACSharpSensor.IACSharpSensor.SensorReached(344);
      if (a.Counter != null) {
        System.Int32 RNTRNTRNT_68 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(345);
        return RNTRNTRNT_68;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(346);
      a.Counter = 0;
      a.Counter++;
      IACSharpSensor.IACSharpSensor.SensorReached(347);
      if (a.Counter != 1) {
        System.Int32 RNTRNTRNT_69 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(348);
        return RNTRNTRNT_69;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(349);
      ++a.Counter;
      IACSharpSensor.IACSharpSensor.SensorReached(350);
      if (a.Counter != 2) {
        System.Int32 RNTRNTRNT_70 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(351);
        return RNTRNTRNT_70;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(352);
      Console.WriteLine("OK");
      System.Int32 RNTRNTRNT_71 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(353);
      return RNTRNTRNT_71;
    }
  }
  class A
  {
    private int? _counter;
    public int? Counter {
      get {
        System.Nullable<System.Int32> RNTRNTRNT_72 = _counter;
        IACSharpSensor.IACSharpSensor.SensorReached(354);
        return RNTRNTRNT_72;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(355);
        _counter = value;
        IACSharpSensor.IACSharpSensor.SensorReached(356);
      }
    }
  }
}
