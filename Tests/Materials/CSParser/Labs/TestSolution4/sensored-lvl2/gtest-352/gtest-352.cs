using System;
using System.Reflection;
struct D
{
  public static D d1 = new D(1);
  public int d2;
  public D(int v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(287);
    d2 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(288);
  }
}
class T
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(289);
    ConstructorInfo mi = typeof(D).GetConstructors(BindingFlags.Instance | BindingFlags.Public)[0];
    MethodBody mb = mi.GetMethodBody();
    Console.WriteLine(mb.GetILAsByteArray().Length);
    IACSharpSensor.IACSharpSensor.SensorReached(290);
    if (mb.GetILAsByteArray().Length != 8) {
      System.Int32 RNTRNTRNT_53 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(291);
      return RNTRNTRNT_53;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(292);
    mi = typeof(D).GetConstructors(BindingFlags.Static | BindingFlags.NonPublic)[0];
    mb = mi.GetMethodBody();
    Console.WriteLine(mb.GetILAsByteArray().Length);
    IACSharpSensor.IACSharpSensor.SensorReached(293);
    if (mb.GetILAsByteArray().Length != 12) {
      System.Int32 RNTRNTRNT_54 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(294);
      return RNTRNTRNT_54;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(295);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_55 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(296);
    return RNTRNTRNT_55;
  }
}
