using System;
public class TestAttribute : Attribute
{
  object type;
  public object Type {
    get {
      System.Object RNTRNTRNT_36 = type;
      IACSharpSensor.IACSharpSensor.SensorReached(188);
      return RNTRNTRNT_36;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(189);
      type = value;
      IACSharpSensor.IACSharpSensor.SensorReached(190);
    }
  }
  public TestAttribute()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(191);
  }
  public TestAttribute(Type type)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(192);
    this.type = type;
    IACSharpSensor.IACSharpSensor.SensorReached(193);
  }
}
namespace N
{
  class C<T>
  {
    [Test(Type = typeof(C<>))]
    public void Bar()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(194);
    }
    [Test(typeof(C<>))]
    public void Bar2()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(195);
    }
    [Test(typeof(C<int>))]
    public void Bar3()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(196);
    }
    [Test(typeof(C<CC>))]
    public void Bar4()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(197);
    }
  }
  class CC
  {
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(198);
    }
  }
}
