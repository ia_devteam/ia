using System;
public class App
{
  private delegate void TGenericDelegate<T>(string param);
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(81);
    App app = new App();
    app.Run();
    IACSharpSensor.IACSharpSensor.SensorReached(82);
  }
  public void Run()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(83);
    TGenericDelegate<string> del = ADelegate<string>;
    TestMethod<string>("a param", ADelegate<string>);
    TestMethod<string>("another param", del);
    IACSharpSensor.IACSharpSensor.SensorReached(84);
  }
  private void TestMethod<T>(string param, TGenericDelegate<T> del)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(85);
    Console.WriteLine("TestMethod <T> called with param: {0}. Calling a delegate", param);
    IACSharpSensor.IACSharpSensor.SensorReached(86);
    if (del != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(87);
      del(param);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(88);
  }
  private void ADelegate<T>(string param)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(89);
    Console.WriteLine("ADelegate <T> called with param: {0}", param);
    IACSharpSensor.IACSharpSensor.SensorReached(90);
  }
}
