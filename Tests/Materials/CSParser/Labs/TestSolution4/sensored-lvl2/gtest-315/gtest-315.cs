public partial class A<T>
{
  public class B
  {
  }
}
public partial class A<T>
{
  public B Test;
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(64);
    A<int> a = new A<int>();
    a.Test = new A<int>.B();
    IACSharpSensor.IACSharpSensor.SensorReached(65);
  }
}
