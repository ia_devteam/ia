interface ITest
{
  void Test();
}
class Tester<T> where T : ITest, new()
{
  public void Do()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(114);
    new T().Test();
    IACSharpSensor.IACSharpSensor.SensorReached(115);
  }
}
class Reference : ITest
{
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(116);
  }
}
struct Value : ITest
{
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(117);
  }
}
class C
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(118);
    new Tester<Reference>().Do();
    new Tester<Value>().Do();
    IACSharpSensor.IACSharpSensor.SensorReached(119);
  }
}
