namespace N
{
  public class TestG
  {
    public static void Foo<T>()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(486);
    }
  }
}
class NonGeneric
{
}
class Generic<T>
{
}
class m
{
  public global::NonGeneric compiles_fine(global::NonGeneric i, out global::NonGeneric o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(487);
    o = new global::NonGeneric();
    NonGeneric RNTRNTRNT_114 = new global::NonGeneric();
    IACSharpSensor.IACSharpSensor.SensorReached(488);
    return RNTRNTRNT_114;
  }
  public global::Generic<int> does_not_compile(global::Generic<int> i, out global::Generic<int> o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(489);
    o = new global::Generic<int>();
    Generic<System.Int32> RNTRNTRNT_115 = new global::Generic<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(490);
    return RNTRNTRNT_115;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(491);
    global::N.TestG.Foo<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(492);
  }
}
