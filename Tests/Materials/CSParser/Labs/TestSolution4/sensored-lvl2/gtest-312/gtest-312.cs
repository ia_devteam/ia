using System.Collections.Generic;
class CantCastGenericListToArray
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    IList<string> list = new string[] {
      "foo",
      "bar"
    };
    string[] array = (string[])list;
    IACSharpSensor.IACSharpSensor.SensorReached(54);
    if (list.Count != array.Length) {
      IACSharpSensor.IACSharpSensor.SensorReached(55);
      throw new System.ApplicationException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(56);
  }
}
