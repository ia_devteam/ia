using System;
public class Test
{
  public static void Invoke<A, TR>(Func<A, Func<TR>> callee, A arg1, TR result)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(570);
  }
  static Func<int> Method(string arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(571);
    return null;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(572);
    Invoke(Method, "one", 1);
    IACSharpSensor.IACSharpSensor.SensorReached(573);
  }
}
