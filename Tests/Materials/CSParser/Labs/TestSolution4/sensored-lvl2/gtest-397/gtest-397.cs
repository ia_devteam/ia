using System;
struct Foo
{
  public int Value;
  public Foo(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(574);
    this.Value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(575);
  }
  public static Foo operator -(Foo? f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(576);
    if (f.HasValue) {
      Foo RNTRNTRNT_140 = new Foo(-f.Value.Value);
      IACSharpSensor.IACSharpSensor.SensorReached(577);
      return RNTRNTRNT_140;
    }
    Foo RNTRNTRNT_141 = new Foo(42);
    IACSharpSensor.IACSharpSensor.SensorReached(578);
    return RNTRNTRNT_141;
  }
}
struct Bar
{
  public int Value;
  public Bar(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(579);
    this.Value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(580);
  }
  public static Bar? operator -(Bar? b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(581);
    if (b.HasValue) {
      System.Nullable<Bar> RNTRNTRNT_142 = new Bar(-b.Value.Value);
      IACSharpSensor.IACSharpSensor.SensorReached(582);
      return RNTRNTRNT_142;
    }
    System.Nullable<Bar> RNTRNTRNT_143 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(583);
    return RNTRNTRNT_143;
  }
}
class Test
{
  static Foo NegateFoo(Foo f)
  {
    Foo RNTRNTRNT_144 = -f;
    IACSharpSensor.IACSharpSensor.SensorReached(584);
    return RNTRNTRNT_144;
  }
  static Foo NegateFooNullable(Foo? f)
  {
    Foo RNTRNTRNT_145 = -f;
    IACSharpSensor.IACSharpSensor.SensorReached(585);
    return RNTRNTRNT_145;
  }
  static Bar? NegateBarNullable(Bar? b)
  {
    System.Nullable<Bar> RNTRNTRNT_146 = -b;
    IACSharpSensor.IACSharpSensor.SensorReached(586);
    return RNTRNTRNT_146;
  }
  static Bar? NegateBar(Bar b)
  {
    System.Nullable<Bar> RNTRNTRNT_147 = -b;
    IACSharpSensor.IACSharpSensor.SensorReached(587);
    return RNTRNTRNT_147;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(588);
    if (NegateFooNullable(null).Value != 42) {
      System.Int32 RNTRNTRNT_148 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(589);
      return RNTRNTRNT_148;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(590);
    if (NegateFoo(new Foo(2)).Value != -2) {
      System.Int32 RNTRNTRNT_149 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(591);
      return RNTRNTRNT_149;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(592);
    if (NegateBarNullable(null) != null) {
      System.Int32 RNTRNTRNT_150 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(593);
      return RNTRNTRNT_150;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(594);
    if (NegateBar(new Bar(2)).Value.Value != -2) {
      System.Int32 RNTRNTRNT_151 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(595);
      return RNTRNTRNT_151;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(596);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_152 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(597);
    return RNTRNTRNT_152;
  }
}
