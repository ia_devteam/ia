using System;
public class TestClass
{
  public static bool Test_1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    DayOfWeek? testEnum = DayOfWeek.Monday;
    IACSharpSensor.IACSharpSensor.SensorReached(163);
    switch (testEnum) {
      case DayOfWeek.Monday:
        System.Boolean RNTRNTRNT_23 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(164);
        return RNTRNTRNT_23;
    }
    System.Boolean RNTRNTRNT_24 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(165);
    return RNTRNTRNT_24;
  }
  public static bool Test_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(166);
    DayOfWeek? testEnum = null;
    IACSharpSensor.IACSharpSensor.SensorReached(167);
    switch (testEnum) {
      case DayOfWeek.Monday:
        System.Boolean RNTRNTRNT_25 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(168);
        return RNTRNTRNT_25;
      case null:
        System.Boolean RNTRNTRNT_26 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(169);
        return RNTRNTRNT_26;
      default:
        System.Boolean RNTRNTRNT_27 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(170);
        return RNTRNTRNT_27;
    }
  }
  public static bool Test_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(171);
    DayOfWeek? testEnum = null;
    IACSharpSensor.IACSharpSensor.SensorReached(172);
    switch (testEnum) {
      case DayOfWeek.Monday:
        System.Boolean RNTRNTRNT_28 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(173);
        return RNTRNTRNT_28;
      default:
        System.Boolean RNTRNTRNT_29 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(174);
        return RNTRNTRNT_29;
    }
  }
  public static bool Test_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(175);
    DayOfWeek? testEnum = DayOfWeek.Monday;
    IACSharpSensor.IACSharpSensor.SensorReached(176);
    switch (testEnum) {
    }
    System.Boolean RNTRNTRNT_30 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    return RNTRNTRNT_30;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(178);
    if (!Test_1()) {
      System.Int32 RNTRNTRNT_31 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(179);
      return RNTRNTRNT_31;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(180);
    if (!Test_2()) {
      System.Int32 RNTRNTRNT_32 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(181);
      return RNTRNTRNT_32;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(182);
    if (!Test_3()) {
      System.Int32 RNTRNTRNT_33 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(183);
      return RNTRNTRNT_33;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(184);
    if (!Test_4()) {
      System.Int32 RNTRNTRNT_34 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(185);
      return RNTRNTRNT_34;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(186);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_35 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(187);
    return RNTRNTRNT_35;
  }
}
