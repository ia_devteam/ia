public class Test
{
  public static void InsertAt<T>(T[] array, int index, params T[] items)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(337);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(338);
    int[] x = new int[] {
      1,
      2
    };
    int[] y = new int[] {
      3,
      4
    };
    InsertAt(x, 0, y);
    IACSharpSensor.IACSharpSensor.SensorReached(339);
  }
}
