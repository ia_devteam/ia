public class aClass
{
  unsafe public struct foo_t
  {
    public fixed char b[16];
  }
  unsafe public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(399);
    foo_t bar;
    char* oo = bar.b;
    IACSharpSensor.IACSharpSensor.SensorReached(400);
  }
}
