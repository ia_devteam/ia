using System;
using System.Collections.Generic;
class T
{
  public X[] x;
}
class X
{
  public Z[] Prop { get; set; }
}
class Z
{
}
class Test
{
  T t = new T { x = new X[] { new X { Prop = new Z[] {
    new Z(),
    new Z()
  } } } };
  public Test(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1402);
  }
  public Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1403);
  }
}
public class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1404);
    new Test("2");
    System.Int32 RNTRNTRNT_298 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1405);
    return RNTRNTRNT_298;
  }
}
