using System;
using System.Collections.Generic;
public class Wrap<U>
{
  public List<U> t;
}
public class Test
{
  public int Run<T>(Wrap<T> t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(213);
    Action f = () => { t.t = new List<T>(); };
    f();
    System.Int32 RNTRNTRNT_57 = t.t != null ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(214);
    return RNTRNTRNT_57;
  }
  public static int Main()
  {
    System.Int32 RNTRNTRNT_58 = new Test().Run(new Wrap<byte>());
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    return RNTRNTRNT_58;
  }
}
