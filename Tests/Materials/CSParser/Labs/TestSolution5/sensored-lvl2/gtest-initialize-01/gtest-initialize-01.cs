using System;
using System.Collections;
public class MyClass
{
  public string Foo = "Bar";
  private int answer;
  public int Answer {
    get {
      System.Int32 RNTRNTRNT_272 = answer;
      IACSharpSensor.IACSharpSensor.SensorReached(1338);
      return RNTRNTRNT_272;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1339);
      answer = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1340);
    }
  }
}
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1341);
    MyClass mc = new MyClass {
      Foo = "Baz",
      Answer = 42
    };
    IACSharpSensor.IACSharpSensor.SensorReached(1342);
    if (mc.Foo != "Baz") {
      System.Int32 RNTRNTRNT_273 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1343);
      return RNTRNTRNT_273;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1344);
    if (mc.Answer != 42) {
      System.Int32 RNTRNTRNT_274 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1345);
      return RNTRNTRNT_274;
    }
    System.Int32 RNTRNTRNT_275 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1346);
    return RNTRNTRNT_275;
  }
}
