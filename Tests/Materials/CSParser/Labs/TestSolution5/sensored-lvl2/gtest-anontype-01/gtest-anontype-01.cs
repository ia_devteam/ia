using System;
using System.Collections;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(282);
    var v = new {
      Foo = "Bar",
      Baz = 42
    };
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    if (v.Foo != "Bar") {
      System.Int32 RNTRNTRNT_73 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(284);
      return RNTRNTRNT_73;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(285);
    if (v.Baz != 42) {
      System.Int32 RNTRNTRNT_74 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(286);
      return RNTRNTRNT_74;
    }
    System.Int32 RNTRNTRNT_75 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(287);
    return RNTRNTRNT_75;
  }
}
