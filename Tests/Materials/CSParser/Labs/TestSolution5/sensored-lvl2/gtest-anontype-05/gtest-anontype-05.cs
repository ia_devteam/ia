using System;
using System.Collections;
public class Test
{
  static string Null()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(316);
    return null;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(317);
    var v1 = new {
      Name = "Scott",
      Age = 21
    };
    var v2 = new {
      Age = 20,
      Name = "Sam"
    };
    var v3 = new {
      Name = Null(),
      Age = 33
    };
    IACSharpSensor.IACSharpSensor.SensorReached(318);
    if (v1.GetType() == v2.GetType()) {
      System.Int32 RNTRNTRNT_91 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(319);
      return RNTRNTRNT_91;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(320);
    if (v1.Equals(v2)) {
      System.Int32 RNTRNTRNT_92 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(321);
      return RNTRNTRNT_92;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(322);
    if (v1.GetType() != v3.GetType()) {
      System.Int32 RNTRNTRNT_93 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(323);
      return RNTRNTRNT_93;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(324);
    if (!v1.Equals(v1)) {
      System.Int32 RNTRNTRNT_94 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(325);
      return RNTRNTRNT_94;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(326);
    if (v1.GetHashCode() != v1.GetHashCode()) {
      System.Int32 RNTRNTRNT_95 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(327);
      return RNTRNTRNT_95;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(328);
    Console.WriteLine(v1);
    Console.WriteLine(v3);
    IACSharpSensor.IACSharpSensor.SensorReached(329);
    if (v1.ToString() != "Name = Scott, Age = 21") {
      System.Int32 RNTRNTRNT_96 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(330);
      return RNTRNTRNT_96;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(331);
    if (v3.ToString() != "Name = <null>, Age = 33") {
      System.Int32 RNTRNTRNT_97 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(332);
      return RNTRNTRNT_97;
    }
    System.Int32 RNTRNTRNT_98 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(333);
    return RNTRNTRNT_98;
  }
}
