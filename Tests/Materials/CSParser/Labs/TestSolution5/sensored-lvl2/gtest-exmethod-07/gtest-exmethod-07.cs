using System.Collections.Generic;
interface IA
{
}
interface IB : IA
{
}
static class E
{
  static internal void ToReadOnly<T>(this IEnumerable<T> source)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1247);
  }
  static internal void To(this IA i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1248);
  }
}
class C
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1249);
  }
  public static void Test(IEnumerable<bool> bindings)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1250);
    bindings.ToReadOnly();
    IB ib = null;
    ib.To();
    IACSharpSensor.IACSharpSensor.SensorReached(1251);
  }
}
