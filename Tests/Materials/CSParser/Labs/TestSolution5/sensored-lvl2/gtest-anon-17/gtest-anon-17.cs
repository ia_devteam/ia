public class C
{
  public delegate T Func<T>(T t);
  public static void Test<T, U>(Func<T> f, U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(44);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(45);
    Test<int, string>(delegate(int i) { return i; }, "");
    Test(delegate(int i) { return i; }, 1);
    IACSharpSensor.IACSharpSensor.SensorReached(46);
  }
}
