using System;
using System.Linq.Expressions;
delegate void EmptyDelegate();
unsafe delegate int* UnsafeDelegate();
class C
{
  static int i;
  static void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1147);
    i += 9;
    IACSharpSensor.IACSharpSensor.SensorReached(1148);
  }
  unsafe static int* Foo()
  {
    System.Int32* RNTRNTRNT_191 = (int*)1;
    IACSharpSensor.IACSharpSensor.SensorReached(1149);
    return RNTRNTRNT_191;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1150);
    Expression<Func<EmptyDelegate>> e = () => new EmptyDelegate(Test);
    IACSharpSensor.IACSharpSensor.SensorReached(1151);
    if (e.Body.ToString() != "Convert(CreateDelegate(EmptyDelegate, null, Void Test()))") {
      System.Int32 RNTRNTRNT_192 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1152);
      return RNTRNTRNT_192;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1153);
    var v = e.Compile();
    v.Invoke()();
    IACSharpSensor.IACSharpSensor.SensorReached(1154);
    if (i != 9) {
      System.Int32 RNTRNTRNT_193 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1155);
      return RNTRNTRNT_193;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1156);
    Expression<Func<EmptyDelegate>> e2 = () => Test;
    IACSharpSensor.IACSharpSensor.SensorReached(1157);
    if (e2.Body.ToString() != "Convert(CreateDelegate(EmptyDelegate, null, Void Test()))") {
      System.Int32 RNTRNTRNT_194 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1158);
      return RNTRNTRNT_194;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1159);
    var v2 = e2.Compile();
    v2.Invoke()();
    IACSharpSensor.IACSharpSensor.SensorReached(1160);
    if (i != 18) {
      System.Int32 RNTRNTRNT_195 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1161);
      return RNTRNTRNT_195;
    }
    unsafe {
      IACSharpSensor.IACSharpSensor.SensorReached(1162);
      Expression<Func<UnsafeDelegate>> e3 = () => new UnsafeDelegate(Foo);
      IACSharpSensor.IACSharpSensor.SensorReached(1163);
      if (e3.Body.ToString() != "Convert(CreateDelegate(UnsafeDelegate, null, Int32* Foo()))") {
        System.Int32 RNTRNTRNT_196 = 5;
        IACSharpSensor.IACSharpSensor.SensorReached(1164);
        return RNTRNTRNT_196;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1165);
      var v3 = e3.Compile();
      IACSharpSensor.IACSharpSensor.SensorReached(1166);
      if (v3.Invoke()() != (int*)1) {
        System.Int32 RNTRNTRNT_197 = 6;
        IACSharpSensor.IACSharpSensor.SensorReached(1167);
        return RNTRNTRNT_197;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1168);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_198 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1169);
    return RNTRNTRNT_198;
  }
}
