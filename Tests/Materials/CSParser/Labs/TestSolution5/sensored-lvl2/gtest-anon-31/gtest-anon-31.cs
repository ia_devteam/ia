using System;
public static class A
{
  public static void Fail<X>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(163);
    EventHandler t = delegate { t = delegate {X foo;}; };
    IACSharpSensor.IACSharpSensor.SensorReached(164);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(165);
  }
}
