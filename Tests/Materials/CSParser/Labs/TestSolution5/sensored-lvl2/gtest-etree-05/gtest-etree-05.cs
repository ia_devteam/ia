using System;
using System.Linq.Expressions;
using System.Collections;
using System.Collections.Generic;
class C
{
  static void AssertNodeType(LambdaExpression e, ExpressionType et)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1136);
    if (e.Body.NodeType != et) {
      IACSharpSensor.IACSharpSensor.SensorReached(1137);
      throw new ApplicationException(e.Body.NodeType + " != " + et);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1138);
  }
  static void Assert<T>(T expected, T value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1139);
    if (!EqualityComparer<T>.Default.Equals(expected, value)) {
      IACSharpSensor.IACSharpSensor.SensorReached(1140);
      throw new ApplicationException(expected + " != " + value);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1141);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1142);
    Expression<Func<ArrayList>> e1 = () => new ArrayList {
      null,
      "Hello",
      "World",
      5
    };
    AssertNodeType(e1, ExpressionType.ListInit);
    var re1 = e1.Compile().Invoke();
    Assert(null, re1[0]);
    Assert("Hello", re1[1]);
    Assert("World", re1[2]);
    Assert(5, re1[3]);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_188 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1143);
    return RNTRNTRNT_188;
  }
}
