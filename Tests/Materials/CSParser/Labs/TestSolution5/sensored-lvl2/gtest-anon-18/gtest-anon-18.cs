public class C
{
  public delegate TR Func<TR, TA>(TA t);
  public static TR Test<TR, TA>(Func<TR, TA> f)
  {
    TR RNTRNTRNT_5 = default(TR);
    IACSharpSensor.IACSharpSensor.SensorReached(47);
    return RNTRNTRNT_5;
  }
  public static TR Test<TR, TA, TB>(Func<TR, TA> f, Func<TR, TB> f2)
  {
    TR RNTRNTRNT_6 = default(TR);
    IACSharpSensor.IACSharpSensor.SensorReached(48);
    return RNTRNTRNT_6;
  }
  public static void Test2<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(49);
    T r = Test(delegate(T i) { return i; });
    IACSharpSensor.IACSharpSensor.SensorReached(50);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(51);
    int r = Test(delegate(int i) { return i < 1 ? 'a' : i; });
    string s = Test(delegate(int i) { return "a"; }, delegate(int i) { return "b"; });
    IACSharpSensor.IACSharpSensor.SensorReached(52);
  }
}
