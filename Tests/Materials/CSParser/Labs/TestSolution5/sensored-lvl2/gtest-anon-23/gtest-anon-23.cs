using System;
using System.Collections.Generic;
class MemberAccessData
{
  public volatile uint VolatileValue;
  public string[] StringValues;
  public List<string> ListValues;
  int? mt;
  public int? MyTypeProperty {
    get {
      System.Nullable<System.Int32> RNTRNTRNT_16 = mt;
      IACSharpSensor.IACSharpSensor.SensorReached(78);
      return RNTRNTRNT_16;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(79);
      mt = value;
      IACSharpSensor.IACSharpSensor.SensorReached(80);
    }
  }
}
public class C
{
  delegate void D();
  static void Test(D d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(81);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(82);
    Exception diffException;
    Test(() =>
    {
      diffException = null;
      try {
      } catch (Exception ex) {
        diffException = ex;
      } finally {
      }
      try {
      } catch {
      }
    });
    int[] i_a = new int[] {
      1,
      2,
      3
    };
    Test(() =>
    {
      foreach (int t in i_a) {
      }
    });
    Test(() => { Console.WriteLine(typeof(void)); });
    Test(() => { Console.WriteLine(typeof(Func<, >)); });
    Test(() =>
    {
      object o = new List<object> {
        "Hello",
        "",
        null,
        "World",
        5
      };
    });
    Test(() =>
    {
      var v = new MemberAccessData {
        VolatileValue = 2,
        StringValues = new string[] { "sv" },
        MyTypeProperty = null
      };
    });
    IACSharpSensor.IACSharpSensor.SensorReached(83);
  }
}
