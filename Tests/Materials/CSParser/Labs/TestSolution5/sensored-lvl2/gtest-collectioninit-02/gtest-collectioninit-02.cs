using System.Collections.Generic;
public class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(430);
    var o = new Dictionary<string, int> { {
      "Foo",
      3
    } };
    IACSharpSensor.IACSharpSensor.SensorReached(431);
    if (o["Foo"] != 3) {
      System.Int32 RNTRNTRNT_132 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(432);
      return RNTRNTRNT_132;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(433);
    o = new Dictionary<string, int> {
      {
        "A",
        1
      },
      {
        "B",
        2
      }
    };
    System.Int32 RNTRNTRNT_133 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(434);
    return RNTRNTRNT_133;
  }
}
