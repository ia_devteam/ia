using System;
using System.Collections;
public class Test
{
  static object TestA(string s)
  {
    System.Object RNTRNTRNT_76 = new { s };
    IACSharpSensor.IACSharpSensor.SensorReached(288);
    return RNTRNTRNT_76;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(289);
    string Foo = "Bar";
    int Baz = 42;
    var v = new {
      Foo,
      Baz
    };
    IACSharpSensor.IACSharpSensor.SensorReached(290);
    if (v.Foo != "Bar") {
      System.Int32 RNTRNTRNT_77 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(291);
      return RNTRNTRNT_77;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(292);
    if (v.Baz != 42) {
      System.Int32 RNTRNTRNT_78 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(293);
      return RNTRNTRNT_78;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(294);
    if (!TestA("foo").Equals(new { s = "foo" })) {
      System.Int32 RNTRNTRNT_79 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(295);
      return RNTRNTRNT_79;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(296);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_80 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(297);
    return RNTRNTRNT_80;
  }
}
