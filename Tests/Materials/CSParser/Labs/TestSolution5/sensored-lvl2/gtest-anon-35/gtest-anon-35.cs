using System;
using System.Reflection;
class C<T>
{
  static Func<T> XX()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(194);
    System.Func<T> t = () => default(T);
    Func<T> RNTRNTRNT_49 = t;
    IACSharpSensor.IACSharpSensor.SensorReached(195);
    return RNTRNTRNT_49;
  }
}
class C2<T>
{
  static Func<C<T>> XX()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(196);
    System.Func<C<T>> t = () => default(C<T>);
    Func<C<T>> RNTRNTRNT_50 = t;
    IACSharpSensor.IACSharpSensor.SensorReached(197);
    return RNTRNTRNT_50;
  }
}
class N1
{
  static Func<T> XX<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(198);
    System.Func<T> t = () => default(T);
    Func<T> RNTRNTRNT_51 = t;
    IACSharpSensor.IACSharpSensor.SensorReached(199);
    return RNTRNTRNT_51;
  }
}
public class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(200);
    var t = typeof(C<>);
    IACSharpSensor.IACSharpSensor.SensorReached(201);
    if (t.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Length != 1) {
      System.Int32 RNTRNTRNT_52 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(202);
      return RNTRNTRNT_52;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    t = typeof(C2<>);
    IACSharpSensor.IACSharpSensor.SensorReached(204);
    if (t.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Length != 1) {
      System.Int32 RNTRNTRNT_53 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(205);
      return RNTRNTRNT_53;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    t = typeof(N1);
    IACSharpSensor.IACSharpSensor.SensorReached(207);
    if (t.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Length != 0) {
      System.Int32 RNTRNTRNT_54 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(208);
      return RNTRNTRNT_54;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(209);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_55 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(210);
    return RNTRNTRNT_55;
  }
}
