delegate void TestFunc<T>(T val);
class A
{
  public A(TestFunc<int> func)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(41);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(42);
  }
}
class TestClass
{
  readonly A a = new A(delegate(int a) { });
  static void Func<T>(TestFunc<T> func)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(43);
  }
}
