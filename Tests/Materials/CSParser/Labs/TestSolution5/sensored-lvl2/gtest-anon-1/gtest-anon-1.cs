using System;
delegate void Foo();
class X
{
  public void Hello<U>(U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
  }
  public void Test<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2);
    T u = t;
    Hello(u);
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(3);
      Hello(u);
    };
    foo();
    Hello(u);
    IACSharpSensor.IACSharpSensor.SensorReached(4);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(5);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(6);
  }
}
