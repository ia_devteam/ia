using System;
using System.Collections;
public class MyClass
{
  public string Foo = "Bar";
  public int Baz {
    get {
      System.Int32 RNTRNTRNT_81 = 42;
      IACSharpSensor.IACSharpSensor.SensorReached(298);
      return RNTRNTRNT_81;
    }
  }
}
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(299);
    MyClass mc = new MyClass();
    var v = new {
      mc.Foo,
      mc.Baz
    };
    IACSharpSensor.IACSharpSensor.SensorReached(300);
    if (v.Foo != "Bar") {
      System.Int32 RNTRNTRNT_82 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(301);
      return RNTRNTRNT_82;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(302);
    if (v.Baz != 42) {
      System.Int32 RNTRNTRNT_83 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(303);
      return RNTRNTRNT_83;
    }
    System.Int32 RNTRNTRNT_84 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(304);
    return RNTRNTRNT_84;
  }
}
