using System;
using System.Collections.Generic;
unsafe class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(216);
    foreach (int item in GetItems()) {
      IACSharpSensor.IACSharpSensor.SensorReached(217);
      Console.WriteLine(item);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(218);
  }
  unsafe public static int GetItem()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(219);
    byte[] value = new byte[] {
      0xde,
      0xad,
      0xbe,
      0xef
    };
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    fixed (byte* valueptr = value) {
      System.Int32 RNTRNTRNT_59 = *(int*)valueptr;
      IACSharpSensor.IACSharpSensor.SensorReached(221);
      return RNTRNTRNT_59;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(222);
  }
  public static IEnumerable<int> GetItems()
  {
    System.Int32 RNTRNTRNT_60 = GetItem();
    IACSharpSensor.IACSharpSensor.SensorReached(223);
    yield return RNTRNTRNT_60;
    IACSharpSensor.IACSharpSensor.SensorReached(224);
    IACSharpSensor.IACSharpSensor.SensorReached(225);
  }
}
