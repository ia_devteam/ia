using System;
using System.Collections.Generic;
class HS<T>
{
  public HS(IEqualityComparer<T> comparer)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(132);
  }
}
class Test
{
  static void Foo<T>(IEqualityComparer<T> c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(133);
    Func<HS<T>> a = () => { return new HS<T>(c); };
    IACSharpSensor.IACSharpSensor.SensorReached(134);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(135);
    Foo<object>(null);
    System.Int32 RNTRNTRNT_36 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(136);
    return RNTRNTRNT_36;
  }
}
