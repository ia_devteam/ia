using System;
using System.Linq.Expressions;
struct Foo
{
  public static bool operator >(Foo d1, Foo d2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1118);
    throw new ApplicationException();
  }
  public static bool operator <(Foo d1, Foo d2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1119);
    throw new ApplicationException();
  }
  public static bool operator ==(Foo d1, Foo d2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1120);
    throw new ApplicationException();
  }
  public static bool operator !=(Foo d1, Foo d2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1121);
    throw new ApplicationException();
  }
}
class C
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1122);
    Foo f;
    Expression<Func<bool>> e = () => f > null;
    IACSharpSensor.IACSharpSensor.SensorReached(1123);
    if (e.Compile().Invoke()) {
      System.Int32 RNTRNTRNT_183 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1124);
      return RNTRNTRNT_183;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1125);
    e = () => f < null;
    IACSharpSensor.IACSharpSensor.SensorReached(1126);
    if (e.Compile().Invoke()) {
      System.Int32 RNTRNTRNT_184 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1127);
      return RNTRNTRNT_184;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1128);
    e = () => f == null;
    IACSharpSensor.IACSharpSensor.SensorReached(1129);
    if (e.Compile().Invoke()) {
      System.Int32 RNTRNTRNT_185 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1130);
      return RNTRNTRNT_185;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1131);
    e = () => f != null;
    IACSharpSensor.IACSharpSensor.SensorReached(1132);
    if (!e.Compile().Invoke()) {
      System.Int32 RNTRNTRNT_186 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1133);
      return RNTRNTRNT_186;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1134);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_187 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1135);
    return RNTRNTRNT_187;
  }
}
