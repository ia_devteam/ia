using System;
using System.Collections;
using System.Collections.Generic;
public delegate void Foo();
public class Test
{
  public static implicit operator Foo(Test test)
  {
    Foo RNTRNTRNT_69 = delegate { Console.WriteLine("Hello World!"); };
    IACSharpSensor.IACSharpSensor.SensorReached(267);
    return RNTRNTRNT_69;
  }
  public static IEnumerable<Test> operator +(Test test, Test foo)
  {
    Test RNTRNTRNT_70 = test;
    IACSharpSensor.IACSharpSensor.SensorReached(268);
    yield return RNTRNTRNT_70;
    IACSharpSensor.IACSharpSensor.SensorReached(269);
    Test RNTRNTRNT_71 = foo;
    IACSharpSensor.IACSharpSensor.SensorReached(270);
    yield return RNTRNTRNT_71;
    IACSharpSensor.IACSharpSensor.SensorReached(271);
    IACSharpSensor.IACSharpSensor.SensorReached(272);
  }
  public IEnumerable<int> Foo {
    get {
      System.Int32 RNTRNTRNT_72 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(273);
      yield return RNTRNTRNT_72;
      IACSharpSensor.IACSharpSensor.SensorReached(274);
      IACSharpSensor.IACSharpSensor.SensorReached(275);
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(276);
      Console.WriteLine("Foo!");
      IACSharpSensor.IACSharpSensor.SensorReached(277);
    }
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(278);
    Test test = new Test();
    Foo foo = test;
    foo();
    IACSharpSensor.IACSharpSensor.SensorReached(279);
    foreach (Test t in test + test) {
      IACSharpSensor.IACSharpSensor.SensorReached(280);
      Console.WriteLine(t);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(281);
  }
}
