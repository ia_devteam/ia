using System;
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("gtest-friend-13")]
public class FriendClass
{
  protected internal virtual void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1330);
  }
  internal virtual void Test_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1331);
  }
}
