using System;
public class Test
{
  private class A
  {
    public string B { get; set; }
  }
  public string Foo { get; set; }
  public int Answer { get; private set; }
  public Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(352);
    Answer = 42;
    IACSharpSensor.IACSharpSensor.SensorReached(353);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(354);
    Test t = new Test();
    t.Foo = "Bar";
    IACSharpSensor.IACSharpSensor.SensorReached(355);
    if (t.Foo != "Bar") {
      System.Int32 RNTRNTRNT_106 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(356);
      return RNTRNTRNT_106;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(357);
    if (t.Answer != 42) {
      System.Int32 RNTRNTRNT_107 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(358);
      return RNTRNTRNT_107;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(359);
    A a = new A();
    a.B = "C";
    IACSharpSensor.IACSharpSensor.SensorReached(360);
    if (a.B != "C") {
      System.Int32 RNTRNTRNT_108 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(361);
      return RNTRNTRNT_108;
    }
    System.Int32 RNTRNTRNT_109 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(362);
    return RNTRNTRNT_109;
  }
}
