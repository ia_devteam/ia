using System;
using System.Collections.Generic;
public class Program
{
  public static void Assert(Action<int> action)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(166);
    action(42);
    IACSharpSensor.IACSharpSensor.SensorReached(167);
  }
  public static void Foo<T>(IList<T> list)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(168);
    Assert(i =>
    {
      T[] backup = new T[list.Count];
    });
    IACSharpSensor.IACSharpSensor.SensorReached(169);
  }
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    Foo(args);
    IACSharpSensor.IACSharpSensor.SensorReached(171);
  }
}
