using System;
static class SimpleTest
{
  public static string Prefix(this string s, string prefix)
  {
    System.String RNTRNTRNT_213 = prefix + s;
    IACSharpSensor.IACSharpSensor.SensorReached(1201);
    return RNTRNTRNT_213;
  }
}
public class M
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1202);
    SimpleTest.Prefix("foo", "1");
    string s = "foo".Prefix("1");
    Type ex_attr = typeof(System.Runtime.CompilerServices.ExtensionAttribute);
    IACSharpSensor.IACSharpSensor.SensorReached(1203);
    if (!typeof(SimpleTest).IsDefined(ex_attr, false)) {
      System.Int32 RNTRNTRNT_214 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1204);
      return RNTRNTRNT_214;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1205);
    if (!typeof(SimpleTest).Assembly.IsDefined(ex_attr, false)) {
      System.Int32 RNTRNTRNT_215 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1206);
      return RNTRNTRNT_215;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1207);
    if (!typeof(SimpleTest).GetMethod("Prefix").IsDefined(ex_attr, false)) {
      System.Int32 RNTRNTRNT_216 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1208);
      return RNTRNTRNT_216;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1209);
    if (s != "1foo") {
      System.Int32 RNTRNTRNT_217 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(1210);
      return RNTRNTRNT_217;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1211);
    Console.WriteLine(s);
    System.Int32 RNTRNTRNT_218 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1212);
    return RNTRNTRNT_218;
  }
}
