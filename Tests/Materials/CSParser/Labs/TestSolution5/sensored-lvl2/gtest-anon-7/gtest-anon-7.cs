using System;
using System.Collections.Generic;
class MyDisposable : IDisposable
{
  static int next_id;
  int id = ++next_id;
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(247);
  }
  public int ID {
    get {
      System.Int32 RNTRNTRNT_64 = id;
      IACSharpSensor.IACSharpSensor.SensorReached(248);
      return RNTRNTRNT_64;
    }
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_65 = String.Format("{0} ({1})", GetType(), id);
    IACSharpSensor.IACSharpSensor.SensorReached(249);
    return RNTRNTRNT_65;
  }
}
class X
{
  public static IEnumerable<int> Test(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(250);
    MyDisposable d;
    IACSharpSensor.IACSharpSensor.SensorReached(251);
    using (d = new MyDisposable()) {
      System.Int32 RNTRNTRNT_66 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(252);
      yield return RNTRNTRNT_66;
      IACSharpSensor.IACSharpSensor.SensorReached(253);
      System.Int32 RNTRNTRNT_67 = d.ID;
      IACSharpSensor.IACSharpSensor.SensorReached(254);
      yield return RNTRNTRNT_67;
      IACSharpSensor.IACSharpSensor.SensorReached(255);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(256);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(257);
    foreach (int a in Test(5)) {
      IACSharpSensor.IACSharpSensor.SensorReached(258);
      Console.WriteLine(a);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(259);
  }
}
