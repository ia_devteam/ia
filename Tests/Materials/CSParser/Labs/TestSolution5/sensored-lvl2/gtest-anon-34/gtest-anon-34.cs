using System;
public class MyClass
{
  public string Foo = "Bar";
  private int answer;
  public int Answer {
    get {
      System.Int32 RNTRNTRNT_45 = answer;
      IACSharpSensor.IACSharpSensor.SensorReached(184);
      return RNTRNTRNT_45;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(185);
      answer = value;
      IACSharpSensor.IACSharpSensor.SensorReached(186);
    }
  }
}
public class Test
{
  delegate void D();
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(187);
    MyClass mc = null;
    D d = delegate() {
      IACSharpSensor.IACSharpSensor.SensorReached(188);
      mc = new MyClass {
        Foo = "Baz",
        Answer = 42
      };
    };
    d();
    IACSharpSensor.IACSharpSensor.SensorReached(189);
    if (mc.Foo != "Baz") {
      System.Int32 RNTRNTRNT_46 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(190);
      return RNTRNTRNT_46;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(191);
    if (mc.Answer != 42) {
      System.Int32 RNTRNTRNT_47 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(192);
      return RNTRNTRNT_47;
    }
    System.Int32 RNTRNTRNT_48 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(193);
    return RNTRNTRNT_48;
  }
}
