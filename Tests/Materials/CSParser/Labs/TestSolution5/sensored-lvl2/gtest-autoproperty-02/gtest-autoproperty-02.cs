using System;
public class Test
{
  private class A
  {
    public static string B { get; set; }
    public static string C { get; private set; }
    public static void DoThings()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(363);
      C = "C";
      IACSharpSensor.IACSharpSensor.SensorReached(364);
    }
  }
  public static string Foo { get; set; }
  public static int Answer { get; private set; }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(365);
    Foo = "Bar";
    IACSharpSensor.IACSharpSensor.SensorReached(366);
    if (Foo != "Bar") {
      System.Int32 RNTRNTRNT_110 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(367);
      return RNTRNTRNT_110;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    Answer = 42;
    IACSharpSensor.IACSharpSensor.SensorReached(369);
    if (Answer != 42) {
      System.Int32 RNTRNTRNT_111 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(370);
      return RNTRNTRNT_111;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(371);
    A.B = "B";
    IACSharpSensor.IACSharpSensor.SensorReached(372);
    if (A.B != "B") {
      System.Int32 RNTRNTRNT_112 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(373);
      return RNTRNTRNT_112;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(374);
    A.DoThings();
    IACSharpSensor.IACSharpSensor.SensorReached(375);
    if (A.C != "C") {
      System.Int32 RNTRNTRNT_113 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(376);
      return RNTRNTRNT_113;
    }
    System.Int32 RNTRNTRNT_114 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(377);
    return RNTRNTRNT_114;
  }
}
