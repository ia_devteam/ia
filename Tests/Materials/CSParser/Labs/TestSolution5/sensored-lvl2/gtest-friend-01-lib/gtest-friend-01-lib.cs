using System;
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("gtest-friend-05")]
[assembly: InternalsVisibleTo("gtest-friend-06")]
[assembly: InternalsVisibleTo("gtest-friend-07")]
[assembly: InternalsVisibleTo("gtest-friend-08")]
public class FriendClass
{
  static internal int StaticFriendField;
  static internal int StaticFriendProperty {
    get {
      System.Int32 RNTRNTRNT_262 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1322);
      return RNTRNTRNT_262;
    }
  }
  static internal int StaticFriendMethod()
  {
    System.Int32 RNTRNTRNT_263 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(1323);
    return RNTRNTRNT_263;
  }
  internal int InstanceFriendField;
  internal int InstanceFriendProperty {
    get {
      System.Int32 RNTRNTRNT_264 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1324);
      return RNTRNTRNT_264;
    }
  }
  internal int InstanceFriendMethod()
  {
    System.Int32 RNTRNTRNT_265 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(1325);
    return RNTRNTRNT_265;
  }
  internal class NestedInternalClass
  {
  }
  protected internal class NestedProtectedInternalClass
  {
  }
}
class InternalFriendClass
{
}
