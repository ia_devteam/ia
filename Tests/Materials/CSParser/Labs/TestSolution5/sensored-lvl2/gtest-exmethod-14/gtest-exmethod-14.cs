using System;
public interface IA
{
  void Foo(IA self);
}
public static class C
{
  public static void Foo(this IA self)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1274);
    self.Foo<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(1275);
  }
  public static void Bar<U>(this IA self)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1276);
    self.Foo<U>();
    IACSharpSensor.IACSharpSensor.SensorReached(1277);
  }
  public static void Foo<T>(this IA self)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1278);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1279);
  }
}
