using System;
using System.Collections.Generic;
using System.Text;
public static class IEnumerableRocks
{
  public static string Implode<TSource, TResult>(this IEnumerable<TSource> self, string separator, Func<TSource, TResult> selector)
  {
    System.String RNTRNTRNT_42 = Implode(self, separator, (b, e) => { b.Append(selector(e).ToString()); });
    IACSharpSensor.IACSharpSensor.SensorReached(172);
    return RNTRNTRNT_42;
  }
  public static string Implode<TSource>(this IEnumerable<TSource> self, string separator, Action<StringBuilder, TSource> appender)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(173);
    var coll = self as ICollection<TSource>;
    IACSharpSensor.IACSharpSensor.SensorReached(174);
    if (coll != null && coll.Count == 0) {
      System.String RNTRNTRNT_43 = string.Empty;
      IACSharpSensor.IACSharpSensor.SensorReached(175);
      return RNTRNTRNT_43;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(176);
    bool needSep = false;
    var s = new StringBuilder();
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    foreach (var element in self) {
      IACSharpSensor.IACSharpSensor.SensorReached(178);
      if (needSep && separator != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(179);
        s.Append(separator);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(180);
      appender(s, element);
      needSep = true;
    }
    System.String RNTRNTRNT_44 = s.ToString();
    IACSharpSensor.IACSharpSensor.SensorReached(181);
    return RNTRNTRNT_44;
  }
}
class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(182);
    Console.WriteLine(new[] {
      "foo",
      "bar"
    }.Implode(", ", e => "'" + e + "'"));
    IACSharpSensor.IACSharpSensor.SensorReached(183);
  }
}
