using System;
public delegate void Foo();
public class Test<R>
{
  public void World<S, T>(S s, T t) where S : X where T : S
  {
    IACSharpSensor.IACSharpSensor.SensorReached(17);
  }
  public void Hello<U, V>(U u, V v) where U : X where V : U
  {
    IACSharpSensor.IACSharpSensor.SensorReached(18);
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(19);
      World(u, v);
    };
    IACSharpSensor.IACSharpSensor.SensorReached(20);
  }
}
public class X
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(21);
    X x = new X();
    Test<int> test = new Test<int>();
    test.Hello(x, x);
    IACSharpSensor.IACSharpSensor.SensorReached(22);
  }
}
