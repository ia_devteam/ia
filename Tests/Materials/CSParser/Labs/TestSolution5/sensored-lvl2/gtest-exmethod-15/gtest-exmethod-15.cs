using System;
using System.Reflection;
public interface IA
{
  void Foo(IA self);
}
public static class C
{
  public static TAttribute GetCustomAttribute<TAttribute>(this ICustomAttributeProvider self)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1280);
    var attributes = self.GetCustomAttributes<TAttribute>();
    TAttribute RNTRNTRNT_243 = attributes[0];
    IACSharpSensor.IACSharpSensor.SensorReached(1281);
    return RNTRNTRNT_243;
  }
  public static TAttribute[] GetCustomAttributes<TAttribute>(this ICustomAttributeProvider self)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1282);
    return null;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1283);
  }
}
