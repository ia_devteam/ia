using System;
using System.Collections.Specialized;
class Program
{
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1264);
    var chat = new ChatClient();
    var lines = new StringCollection {
      "a",
      "b",
      "c"
    };
    chat.Say("test", lines);
    IACSharpSensor.IACSharpSensor.SensorReached(1265);
  }
}
class ChatClient
{
  public void Say(string to, string message)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1266);
    Console.WriteLine("{0}: {1}", to, message);
    IACSharpSensor.IACSharpSensor.SensorReached(1267);
  }
}
static class ChatExtensions
{
  public static void Say(this ChatClient chat, string to, StringCollection lines)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1268);
    foreach (string line in lines) {
      IACSharpSensor.IACSharpSensor.SensorReached(1269);
      chat.Say(to, line);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1270);
  }
}
