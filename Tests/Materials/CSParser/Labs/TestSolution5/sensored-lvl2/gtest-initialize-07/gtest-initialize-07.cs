public class A
{
  public string Name { get; set; }
  public bool Matches(string s)
  {
    System.Boolean RNTRNTRNT_295 = Name == s;
    IACSharpSensor.IACSharpSensor.SensorReached(1398);
    return RNTRNTRNT_295;
  }
}
class M
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1399);
    if (!new A { Name = "Foo" }.Matches("Foo")) {
      System.Int32 RNTRNTRNT_296 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1400);
      return RNTRNTRNT_296;
    }
    System.Int32 RNTRNTRNT_297 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1401);
    return RNTRNTRNT_297;
  }
}
