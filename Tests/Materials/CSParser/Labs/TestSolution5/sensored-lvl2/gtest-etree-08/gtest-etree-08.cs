using System;
using System.Linq.Expressions;
class Foo
{
  int ThisMethod()
  {
    System.Int32 RNTRNTRNT_199 = 33;
    IACSharpSensor.IACSharpSensor.SensorReached(1170);
    return RNTRNTRNT_199;
  }
  public int Goo(bool hoo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1171);
    bool local_hoo = hoo;
    Expression<Func<bool>> a = () => hoo;
    IACSharpSensor.IACSharpSensor.SensorReached(1172);
    if (a.Compile()()) {
      System.Int32 RNTRNTRNT_200 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1173);
      return RNTRNTRNT_200;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1174);
    if (true) {
      IACSharpSensor.IACSharpSensor.SensorReached(1175);
      Expression<Func<bool>> b = () => local_hoo;
      IACSharpSensor.IACSharpSensor.SensorReached(1176);
      if (b.Compile()()) {
        System.Int32 RNTRNTRNT_201 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(1177);
        return RNTRNTRNT_201;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1178);
    Expression<Func<int>> c = () => ThisMethod();
    IACSharpSensor.IACSharpSensor.SensorReached(1179);
    if (c.Compile()() != 33) {
      System.Int32 RNTRNTRNT_202 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1180);
      return RNTRNTRNT_202;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1181);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_203 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1182);
    return RNTRNTRNT_203;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1183);
    var f = new Foo();
    System.Int32 RNTRNTRNT_204 = f.Goo(false);
    IACSharpSensor.IACSharpSensor.SensorReached(1184);
    return RNTRNTRNT_204;
  }
}
