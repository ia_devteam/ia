using System;
public delegate void Simple();
public delegate Simple Foo();
class X
{
  public void Hello<U>(U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(156);
  }
  public void Test<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(160);
    {
      IACSharpSensor.IACSharpSensor.SensorReached(157);
      T u = t;
      Hello(u);
      Foo foo = delegate {
        IACSharpSensor.IACSharpSensor.SensorReached(158);
        T v = u;
        Hello(u);
        void  RNTRNTRNT_41 = delegate {
          Hello(u);
          Hello(v);
        };
        IACSharpSensor.IACSharpSensor.SensorReached(159);
        return RNTRNTRNT_41;
      };
    }
    IACSharpSensor.IACSharpSensor.SensorReached(161);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(162);
  }
}
