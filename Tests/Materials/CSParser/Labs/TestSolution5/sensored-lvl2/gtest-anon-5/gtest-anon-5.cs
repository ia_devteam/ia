using System;
using System.Collections.Generic;
public delegate void Hello();
struct Foo
{
  public int ID;
  public Foo(int id)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(226);
    this.ID = id;
    IACSharpSensor.IACSharpSensor.SensorReached(227);
  }
  public IEnumerable<Foo> Test(Foo foo)
  {
    Foo RNTRNTRNT_61 = this;
    IACSharpSensor.IACSharpSensor.SensorReached(228);
    yield return RNTRNTRNT_61;
    IACSharpSensor.IACSharpSensor.SensorReached(229);
    Foo RNTRNTRNT_62 = foo;
    IACSharpSensor.IACSharpSensor.SensorReached(230);
    yield return RNTRNTRNT_62;
    IACSharpSensor.IACSharpSensor.SensorReached(231);
    IACSharpSensor.IACSharpSensor.SensorReached(232);
  }
  public void Hello(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(233);
    if (ID != value) {
      IACSharpSensor.IACSharpSensor.SensorReached(234);
      throw new InvalidOperationException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(235);
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_63 = String.Format("Foo ({0})", ID);
    IACSharpSensor.IACSharpSensor.SensorReached(236);
    return RNTRNTRNT_63;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(237);
    Foo foo = new Foo(3);
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    foreach (Foo bar in foo.Test(new Foo(8))) {
      IACSharpSensor.IACSharpSensor.SensorReached(239);
      Console.WriteLine(bar);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(240);
  }
}
