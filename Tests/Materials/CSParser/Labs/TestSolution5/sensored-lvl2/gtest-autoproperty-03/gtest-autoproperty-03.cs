using System;
using System.Reflection;
using System.Runtime.CompilerServices;
public class Test
{
  public string Foo { get; set; }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(378);
    FieldInfo[] fields = typeof(Test).GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
    IACSharpSensor.IACSharpSensor.SensorReached(379);
    if (!(fields.Length > 0)) {
      System.Int32 RNTRNTRNT_115 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(380);
      return RNTRNTRNT_115;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(381);
    object[] field_atts = fields[0].GetCustomAttributes(false);
    IACSharpSensor.IACSharpSensor.SensorReached(382);
    if (!(field_atts.Length > 0)) {
      System.Int32 RNTRNTRNT_116 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(383);
      return RNTRNTRNT_116;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(384);
    if (field_atts[0].GetType() != typeof(CompilerGeneratedAttribute)) {
      System.Int32 RNTRNTRNT_117 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(385);
      return RNTRNTRNT_117;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(386);
    if (fields[0].Name != "<Foo>k__BackingField") {
      System.Int32 RNTRNTRNT_118 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(387);
      return RNTRNTRNT_118;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(388);
    PropertyInfo property = typeof(Test).GetProperty("Foo");
    MethodInfo @get = property.GetGetMethod(false);
    object[] get_atts = @get.GetCustomAttributes(false);
    IACSharpSensor.IACSharpSensor.SensorReached(389);
    if (!(get_atts.Length > 0)) {
      System.Int32 RNTRNTRNT_119 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(390);
      return RNTRNTRNT_119;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(391);
    if (get_atts[0].GetType() != typeof(CompilerGeneratedAttribute)) {
      System.Int32 RNTRNTRNT_120 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(392);
      return RNTRNTRNT_120;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(393);
    MethodInfo @set = property.GetSetMethod(false);
    object[] set_atts = @set.GetCustomAttributes(false);
    IACSharpSensor.IACSharpSensor.SensorReached(394);
    if (!(set_atts.Length > 0)) {
      System.Int32 RNTRNTRNT_121 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(395);
      return RNTRNTRNT_121;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(396);
    if (set_atts[0].GetType() != typeof(CompilerGeneratedAttribute)) {
      System.Int32 RNTRNTRNT_122 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(397);
      return RNTRNTRNT_122;
    }
    System.Int32 RNTRNTRNT_123 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(398);
    return RNTRNTRNT_123;
  }
}
