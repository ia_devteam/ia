using System;
using Testy;
public static class MainClass
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1290);
    Object o = new Object();
    Console.WriteLine(o.MyFormat("hello:{0}:{1}:", "there", "yak"));
    IACSharpSensor.IACSharpSensor.SensorReached(1291);
  }
}
