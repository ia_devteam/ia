using System;
public delegate void Foo();
public class World<T>
{
  public void Hello<U>(U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(23);
  }
  public void Test(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(24);
    Hello(t);
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(25);
      Hello(t);
    };
    IACSharpSensor.IACSharpSensor.SensorReached(26);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(27);
    World<X> world = new World<X>();
    world.Test(new X());
    IACSharpSensor.IACSharpSensor.SensorReached(28);
  }
}
