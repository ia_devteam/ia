using System;
using System.Collections;
using System.Collections.Generic;
public class Test
{
  class Wrap
  {
    ArrayList numbers = new ArrayList();
    public int Id;
    public Wrap Next;
    public Wrap()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(402);
      Next = new Wrap(100);
      IACSharpSensor.IACSharpSensor.SensorReached(403);
    }
    public Wrap(int i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(404);
      Next = this;
      IACSharpSensor.IACSharpSensor.SensorReached(405);
    }
    public ArrayList Numbers {
      get {
        ArrayList RNTRNTRNT_124 = numbers;
        IACSharpSensor.IACSharpSensor.SensorReached(406);
        return RNTRNTRNT_124;
      }
    }
  }
  static void TestList(List<int> list, int expectedCount)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    if (list.Count != expectedCount) {
      IACSharpSensor.IACSharpSensor.SensorReached(408);
      throw new ApplicationException(expectedCount.ToString());
    }
    IACSharpSensor.IACSharpSensor.SensorReached(409);
    foreach (int i in list) {
      IACSharpSensor.IACSharpSensor.SensorReached(410);
      Console.WriteLine(i);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(411);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(412);
    ArrayList collection = new ArrayList {
      "Foo",
      null,
      1
    };
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    if (collection.Count != 3) {
      System.Int32 RNTRNTRNT_125 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(414);
      return RNTRNTRNT_125;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(415);
    if ((string)collection[0] != "Foo") {
      System.Int32 RNTRNTRNT_126 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(416);
      return RNTRNTRNT_126;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(417);
    if ((int)collection[2] != 1) {
      System.Int32 RNTRNTRNT_127 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(418);
      return RNTRNTRNT_127;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(419);
    List<string> generic_collection = new List<string> {
      "Hello",
      "World"
    };
    IACSharpSensor.IACSharpSensor.SensorReached(420);
    foreach (string s in generic_collection) {
      IACSharpSensor.IACSharpSensor.SensorReached(421);
      if (s.Length != 5) {
        System.Int32 RNTRNTRNT_128 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(422);
        return RNTRNTRNT_128;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(423);
    List<Wrap> a = null;
    a = new List<Wrap> {
      new Wrap(0) {
        Id = 0,
        Numbers = {
          5,
          10
        },
        Next = { Id = 55 }
      },
      new Wrap {
        Id = 1,
        Numbers = { collection }
      },
      new Wrap {
        Id = 2,
        Numbers = {
          
        }
      },
      null
    };
    IACSharpSensor.IACSharpSensor.SensorReached(424);
    if (a.Count != 4) {
      System.Int32 RNTRNTRNT_129 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(425);
      return RNTRNTRNT_129;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(426);
    if ((int)a[0].Numbers[1] != 10) {
      System.Int32 RNTRNTRNT_130 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(427);
      return RNTRNTRNT_130;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(428);
    new List<int> {
      1,
      2,
      3,
      4
    };
    TestList(new List<int> {
      1,
      2,
      3,
      4
    }, 4);
    new List<int> {
      
    };
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_131 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(429);
    return RNTRNTRNT_131;
  }
}
