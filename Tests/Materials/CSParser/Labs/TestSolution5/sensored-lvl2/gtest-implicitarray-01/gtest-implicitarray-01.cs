public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1332);
    string[] array = new[] {
      "Foo",
      "Bar",
      "Baz"
    };
    IACSharpSensor.IACSharpSensor.SensorReached(1333);
    foreach (string s in array) {
      IACSharpSensor.IACSharpSensor.SensorReached(1334);
      if (s.Length != 3) {
        System.Int32 RNTRNTRNT_270 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1335);
        return RNTRNTRNT_270;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1336);
    string[] s1 = new[] {
      null,
      "a",
      default(string)
    };
    double[] s2 = new[] {
      0,
      1.0,
      2
    };
    var a1 = new[] {
      null,
      "a",
      default(string)
    };
    var a2 = new[] {
      0,
      1.0,
      2
    };
    var a3 = new[] {
      new Test(),
      null
    };
    var a4 = new[] {
      {
        1,
        2,
        3
      },
      {
        4,
        5,
        6
      }
    };
    var a5 = new[] { default(object) };
    var a6 = new[] {
      new[] {
        1,
        2,
        3
      },
      new[] {
        4,
        5,
        6
      }
    };
    const byte b = 100;
    var a7 = new[] {
      b,
      10,
      b,
      999,
      b
    };
    System.Int32 RNTRNTRNT_271 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1337);
    return RNTRNTRNT_271;
  }
}
