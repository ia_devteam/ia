using System;
using System.Collections.Generic;
class Disposable<T> : IDisposable
{
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(84);
  }
}
class Test
{
  static Func<T[]> For<T>(List<T> list)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(85);
    T[] t = new T[2];
    Func<T[]> RNTRNTRNT_17 = () =>
    {
      for (int i = 0; i < t.Length; ++i) {
        t[i] = list[i];
      }
      return t;
    };
    IACSharpSensor.IACSharpSensor.SensorReached(86);
    return RNTRNTRNT_17;
  }
  static Func<T> Throw<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(87);
    T l = t;
    Func<T> RNTRNTRNT_18 = () =>
    {
      throw new ApplicationException(l.ToString());
    };
    IACSharpSensor.IACSharpSensor.SensorReached(88);
    return RNTRNTRNT_18;
  }
  static Func<T> Do<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(89);
    T l = t;
    Func<T> RNTRNTRNT_19 = () =>
    {
      T t2;
      do {
        t2 = l;
      } while (default(T) != null);
      return t2;
    };
    IACSharpSensor.IACSharpSensor.SensorReached(90);
    return RNTRNTRNT_19;
  }
  static Func<T> Lock<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(91);
    T l = t;
    Func<T> RNTRNTRNT_20 = () =>
    {
      lock (l.GetType()) {
        l = default(T);
        return l;
      }
    };
    IACSharpSensor.IACSharpSensor.SensorReached(92);
    return RNTRNTRNT_20;
  }
  static Func<T> Catch<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(93);
    T l = t;
    Func<T> RNTRNTRNT_21 = () =>
    {
      try {
        throw new ApplicationException(l.ToString());
      } catch {
        return l;
      }
    };
    IACSharpSensor.IACSharpSensor.SensorReached(94);
    return RNTRNTRNT_21;
  }
  static Func<T> Finally<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(95);
    T l = t;
    Func<T> RNTRNTRNT_22 = () =>
    {
      try {
        l = Lock(l)();
      } finally {
        l = default(T);
      }
      return l;
    };
    IACSharpSensor.IACSharpSensor.SensorReached(96);
    return RNTRNTRNT_22;
  }
  static Func<T> Using<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(97);
    T l = t;
    IACSharpSensor.IACSharpSensor.SensorReached(98);
    using (var d = new Disposable<T>()) {
      Func<T> RNTRNTRNT_23 = () => { return l; };
      IACSharpSensor.IACSharpSensor.SensorReached(99);
      return RNTRNTRNT_23;
    }
  }
  static Func<T> Switch<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(100);
    T l = t;
    int? i = 0;
    Func<T> RNTRNTRNT_24 = () =>
    {
      switch (i) {
        default:
          return l;
      }
    };
    IACSharpSensor.IACSharpSensor.SensorReached(101);
    return RNTRNTRNT_24;
  }
  static Func<List<T>> ForForeach<T>(T[] t)
  {
    Func<List<T>> RNTRNTRNT_25 = () =>
    {
      foreach (T e in t)
        return new List<T> { e };
      throw new ApplicationException();
    };
    IACSharpSensor.IACSharpSensor.SensorReached(102);
    return RNTRNTRNT_25;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(103);
    if (For(new List<int> {
      5,
      10
    })()[1] != 10) {
      System.Int32 RNTRNTRNT_26 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(104);
      return RNTRNTRNT_26;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(105);
    var t = Throw(5);
    IACSharpSensor.IACSharpSensor.SensorReached(106);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(107);
      t();
      System.Int32 RNTRNTRNT_27 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(108);
      return RNTRNTRNT_27;
    } catch (ApplicationException) {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(109);
    var t3 = Do("rr");
    IACSharpSensor.IACSharpSensor.SensorReached(110);
    if (t3() != "rr") {
      System.Int32 RNTRNTRNT_28 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(111);
      return RNTRNTRNT_28;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(112);
    var t4 = Lock('f');
    IACSharpSensor.IACSharpSensor.SensorReached(113);
    if (t4() != '\0') {
      System.Int32 RNTRNTRNT_29 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(114);
      return RNTRNTRNT_29;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(115);
    var t5 = Catch(3);
    IACSharpSensor.IACSharpSensor.SensorReached(116);
    if (t5() != 3) {
      System.Int32 RNTRNTRNT_30 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(117);
      return RNTRNTRNT_30;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(118);
    var t6 = Finally(5);
    IACSharpSensor.IACSharpSensor.SensorReached(119);
    if (t6() != 0) {
      System.Int32 RNTRNTRNT_31 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(120);
      return RNTRNTRNT_31;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(121);
    var t7 = Using(1.1);
    IACSharpSensor.IACSharpSensor.SensorReached(122);
    if (t7() != 1.1) {
      System.Int32 RNTRNTRNT_32 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(123);
      return RNTRNTRNT_32;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(124);
    var t8 = Switch(55);
    IACSharpSensor.IACSharpSensor.SensorReached(125);
    if (t8() != 55) {
      System.Int32 RNTRNTRNT_33 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(126);
      return RNTRNTRNT_33;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(127);
    var t9 = ForForeach(new[] {
      4,
      1
    });
    IACSharpSensor.IACSharpSensor.SensorReached(128);
    if (t9()[0] != 4) {
      System.Int32 RNTRNTRNT_34 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(129);
      return RNTRNTRNT_34;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(130);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_35 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(131);
    return RNTRNTRNT_35;
  }
}
