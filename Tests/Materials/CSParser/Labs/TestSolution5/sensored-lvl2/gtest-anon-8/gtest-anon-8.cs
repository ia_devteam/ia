using System;
using System.Collections.Generic;
delegate int Foo();
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(260);
    Test("Hello World", 8);
    IACSharpSensor.IACSharpSensor.SensorReached(261);
  }
  public static void Test<R>(R r, int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(262);
    for (int b = a; b > 0; b--) {
      IACSharpSensor.IACSharpSensor.SensorReached(263);
      R s = r;
      Foo foo = delegate {
        IACSharpSensor.IACSharpSensor.SensorReached(264);
        Console.WriteLine(b);
        Console.WriteLine(s);
        void  RNTRNTRNT_68 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(265);
        return RNTRNTRNT_68;
      };
      a -= foo();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(266);
  }
}
