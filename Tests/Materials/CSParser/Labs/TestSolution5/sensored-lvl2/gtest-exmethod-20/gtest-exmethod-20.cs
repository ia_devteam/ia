using System;
using System.Collections.Generic;
interface I
{
}
namespace Outer.Inner
{
  class Test
  {
    static void M(I list)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1310);
      list.AddRange(new Test[0]);
      IACSharpSensor.IACSharpSensor.SensorReached(1311);
    }
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1312);
    }
  }
}
namespace Outer
{
  static class ExtensionMethods
  {
    public static void AddRange<T>(this I list, IEnumerable<T> items)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1313);
    }
  }
}
