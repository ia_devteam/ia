using System;
using System.Collections.Generic;
public abstract class BaseDataObjectFactory
{
  protected static T GetBusinessQueryObjectFromReader<T>() where T : BusinessQueryObject, new()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(140);
    T t = new T();
    T RNTRNTRNT_39 = t;
    IACSharpSensor.IACSharpSensor.SensorReached(141);
    return RNTRNTRNT_39;
  }
  public abstract T[] GetQueryObjects<T>(string query) where T : BusinessQueryObject, new();
}
public class BusinessQueryObject
{
}
public class MySqlDataObjectFactory : BaseDataObjectFactory
{
  public override T[] GetQueryObjects<T>(string query)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(142);
    List<T> list = new List<T>();
    list.Add(GetBusinessQueryObjectFromReader<T>());
    ExecuteReader(5, delegate() { list.Add(GetBusinessQueryObjectFromReader<T>()); });
    T[] RNTRNTRNT_40 = list.ToArray();
    IACSharpSensor.IACSharpSensor.SensorReached(143);
    return RNTRNTRNT_40;
  }
  static void ExecuteReader(int a, PerformActionWithReader action)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(144);
  }
  delegate void PerformActionWithReader();
}
public class C
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(145);
  }
}
