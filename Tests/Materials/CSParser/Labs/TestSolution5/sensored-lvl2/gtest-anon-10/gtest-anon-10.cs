using System;
using System.Collections.Generic;
class X
{
  public IEnumerable<T> Test<T>(T a, T b)
  {
    T RNTRNTRNT_1 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(7);
    yield return RNTRNTRNT_1;
    IACSharpSensor.IACSharpSensor.SensorReached(8);
    b = a;
    T RNTRNTRNT_2 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(9);
    yield return RNTRNTRNT_2;
    IACSharpSensor.IACSharpSensor.SensorReached(10);
    IACSharpSensor.IACSharpSensor.SensorReached(11);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(12);
    X x = new X();
    long sum = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(13);
    foreach (long i in x.Test(3, 5)) {
      IACSharpSensor.IACSharpSensor.SensorReached(14);
      Console.WriteLine(i);
      sum += i;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(15);
    Console.WriteLine(sum);
    System.Int32 RNTRNTRNT_3 = sum == 8 ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    return RNTRNTRNT_3;
  }
}
