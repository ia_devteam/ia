abstract class A
{
  public abstract void Foo<T>() where T : struct;
}
class B : A
{
  public delegate void Del();
  public override void Foo<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(146);
    Del d = delegate() {
      IACSharpSensor.IACSharpSensor.SensorReached(147);
      Foo<T>();
    };
    IACSharpSensor.IACSharpSensor.SensorReached(148);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(149);
  }
}
