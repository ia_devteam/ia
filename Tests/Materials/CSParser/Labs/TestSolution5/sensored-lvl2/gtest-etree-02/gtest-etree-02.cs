using System;
using System.Linq.Expressions;
class M
{
  public static void Foo<T>(Expression<Func<T, T>> x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1107);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1108);
    Foo<int>(i => i);
    Foo((int i) => i);
    Expression<Func<int, int>> func = i => i;
    Foo(func);
    IACSharpSensor.IACSharpSensor.SensorReached(1109);
  }
}
