struct Point
{
  public int X, Y;
}
class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1390);
    Point p;
    Foo(out p);
    IACSharpSensor.IACSharpSensor.SensorReached(1391);
    if (p.X != 3) {
      System.Int32 RNTRNTRNT_292 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1392);
      return RNTRNTRNT_292;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1393);
    if (p.Y != 5) {
      System.Int32 RNTRNTRNT_293 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1394);
      return RNTRNTRNT_293;
    }
    System.Int32 RNTRNTRNT_294 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1395);
    return RNTRNTRNT_294;
  }
  static void Foo(out Point p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1396);
    p = new Point {
      X = 3,
      Y = 5
    };
    IACSharpSensor.IACSharpSensor.SensorReached(1397);
  }
}
