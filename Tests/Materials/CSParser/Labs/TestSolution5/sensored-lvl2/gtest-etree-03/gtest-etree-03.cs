using System;
using System.Linq.Expressions;
struct S<T> where T : struct
{
  public static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1110);
    Expression<Func<T?, bool>> e = (T? o) => o == null;
    IACSharpSensor.IACSharpSensor.SensorReached(1111);
    if (!e.Compile().Invoke(null)) {
      System.Int32 RNTRNTRNT_179 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1112);
      return RNTRNTRNT_179;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1113);
    if (e.Compile().Invoke(default(T))) {
      System.Int32 RNTRNTRNT_180 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1114);
      return RNTRNTRNT_180;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1115);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_181 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1116);
    return RNTRNTRNT_181;
  }
}
class C
{
  static int Main()
  {
    System.Int32 RNTRNTRNT_182 = S<int>.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(1117);
    return RNTRNTRNT_182;
  }
}
