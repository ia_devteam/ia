using System;
using System.Collections.Generic;
using System.Linq.Expressions;
class Foo<T>
{
  public bool ContainsAll<U>(IEnumerable<U> items) where U : T
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1193);
    foreach (U item in items) {
      IACSharpSensor.IACSharpSensor.SensorReached(1194);
      Expression<Func<bool>> e = () => !Contains(item);
      IACSharpSensor.IACSharpSensor.SensorReached(1195);
      if (!e.Compile()()) {
        System.Boolean RNTRNTRNT_209 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1196);
        return RNTRNTRNT_209;
      }
    }
    System.Boolean RNTRNTRNT_210 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(1197);
    return RNTRNTRNT_210;
  }
  public bool Contains(T t)
  {
    System.Boolean RNTRNTRNT_211 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(1198);
    return RNTRNTRNT_211;
  }
}
class Program
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1199);
    var x = new Foo<int>();
    System.Int32 RNTRNTRNT_212 = x.ContainsAll(new[] {
      4,
      6,
      78
    }) ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1200);
    return RNTRNTRNT_212;
  }
}
