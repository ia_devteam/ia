static class Test
{
  public static void Foo<T>(this string p1)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1254);
  }
}
class C
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1255);
    "a".Foo<bool>();
    IACSharpSensor.IACSharpSensor.SensorReached(1256);
  }
}
