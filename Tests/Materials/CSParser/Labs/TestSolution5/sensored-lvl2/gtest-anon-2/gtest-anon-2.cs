using System;
public delegate void Simple();
public delegate Simple Foo();
class X
{
  public void Hello<U>(U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(57);
  }
  public void Test<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(58);
    T u = t;
    Hello(u);
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(59);
      T v = u;
      Hello(u);
      void  RNTRNTRNT_7 = delegate {
        Hello(u);
        Hello(v);
      };
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      return RNTRNTRNT_7;
    };
    Simple simple = foo();
    simple();
    Hello(u);
    IACSharpSensor.IACSharpSensor.SensorReached(61);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(63);
  }
}
