using System;
delegate void Foo<S>(S s);
class X
{
  public void Hello<U>(U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(150);
  }
  public void Test<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(151);
    Hello(t);
    Foo<T> foo = delegate(T u) {
      IACSharpSensor.IACSharpSensor.SensorReached(152);
      Hello(u);
    };
    foo(t);
    IACSharpSensor.IACSharpSensor.SensorReached(153);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(154);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(155);
  }
}
