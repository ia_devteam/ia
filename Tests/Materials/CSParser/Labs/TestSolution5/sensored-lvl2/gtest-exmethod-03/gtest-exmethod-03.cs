using System;
namespace A
{
  public static class A
  {
    public static int Foo(this int i)
    {
      System.Int32 RNTRNTRNT_220 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1218);
      return RNTRNTRNT_220;
    }
    public static int Foo(this int i, string s)
    {
      System.Int32 RNTRNTRNT_221 = 30;
      IACSharpSensor.IACSharpSensor.SensorReached(1219);
      return RNTRNTRNT_221;
    }
  }
}
namespace B
{
  public static class X
  {
    public static int Foo(this int i)
    {
      System.Int32 RNTRNTRNT_222 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1220);
      return RNTRNTRNT_222;
    }
    public static int Foo(this int i, bool b)
    {
      System.Int32 RNTRNTRNT_223 = 20;
      IACSharpSensor.IACSharpSensor.SensorReached(1221);
      return RNTRNTRNT_223;
    }
  }
}
namespace C
{
  using A;
  using B;
  using D;
  public static class F
  {
    public static bool Foo(this byte i)
    {
      System.Boolean RNTRNTRNT_224 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(1222);
      return RNTRNTRNT_224;
    }
  }
  namespace D
  {
    public static class F
    {
      public static int Foo(this int i)
      {
        System.Int32 RNTRNTRNT_225 = 66;
        IACSharpSensor.IACSharpSensor.SensorReached(1223);
        return RNTRNTRNT_225;
      }
      public static void TestX()
      {
        IACSharpSensor.IACSharpSensor.SensorReached(1224);
        int i = 2.Foo(false);
        IACSharpSensor.IACSharpSensor.SensorReached(1225);
      }
    }
  }
  public static class M
  {
    public static int Foo(this int i)
    {
      System.Int32 RNTRNTRNT_226 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1226);
      return RNTRNTRNT_226;
    }
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1227);
      if (3.Foo("a") != 30) {
        System.Int32 RNTRNTRNT_227 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1228);
        return RNTRNTRNT_227;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1229);
      if (((byte)0).Foo()) {
        System.Int32 RNTRNTRNT_228 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(1230);
        return RNTRNTRNT_228;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1231);
      if (4.Foo(false) != 20) {
        System.Int32 RNTRNTRNT_229 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(1232);
        return RNTRNTRNT_229;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1233);
      Console.WriteLine("OK");
      System.Int32 RNTRNTRNT_230 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1234);
      return RNTRNTRNT_230;
    }
  }
}
