using System;
using System.Collections;
class Data
{
  public int Value;
}
public class Test
{
  static Data Prop {
    set { IACSharpSensor.IACSharpSensor.SensorReached(1368); }
  }
  public object Foo()
  {
    System.Object RNTRNTRNT_283 = new Data { Value = 3 };
    IACSharpSensor.IACSharpSensor.SensorReached(1369);
    return RNTRNTRNT_283;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1370);
    Prop = new Data { Value = 3 };
    Data data = new Data { Value = 6 };
    Data a, b;
    a = b = new Data { Value = 3 };
    IACSharpSensor.IACSharpSensor.SensorReached(1371);
  }
}
