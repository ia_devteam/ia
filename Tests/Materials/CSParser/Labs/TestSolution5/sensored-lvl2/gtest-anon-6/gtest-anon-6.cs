using System;
delegate void Foo<R, S>(R r, S s);
class X
{
  public void Hello<U, V>(U u, V v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(241);
  }
  public void Test<A, B, C>(A a, B b, C c)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(242);
    Hello(a, b);
    C d = c;
    Foo<A, int> foo = delegate(A i, int j) {
      IACSharpSensor.IACSharpSensor.SensorReached(243);
      Hello(i, c);
      Hello(i, j);
    };
    IACSharpSensor.IACSharpSensor.SensorReached(244);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    X x = new X();
    x.Test(3, Math.PI, 1 << 8);
    IACSharpSensor.IACSharpSensor.SensorReached(246);
  }
}
