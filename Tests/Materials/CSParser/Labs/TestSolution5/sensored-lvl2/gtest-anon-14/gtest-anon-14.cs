using System;
class T
{
  void SomeMethod(Converter<Int32, Int32> converter)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(31);
  }
  void SomeCaller()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(32);
    SomeMethod(delegate(Int32 a) { return a; });
    IACSharpSensor.IACSharpSensor.SensorReached(33);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(34);
  }
}
