using System;
using External;
interface I
{
}
namespace Outer.Inner
{
  class Test
  {
    static void M(I list)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1314);
      list.AddRange();
      IACSharpSensor.IACSharpSensor.SensorReached(1315);
    }
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1316);
    }
  }
}
namespace Outer
{
}
namespace External
{
  static class ExtensionMethods
  {
    public static void AddRange(this I list)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1317);
    }
  }
}
