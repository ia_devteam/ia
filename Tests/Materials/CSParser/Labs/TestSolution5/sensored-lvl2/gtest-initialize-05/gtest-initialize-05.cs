struct Point
{
  public int X, Y;
}
class C
{
  static Point p;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1376);
    new Point {
      X = 0,
      Y = 0
    };
    var markerPosition = new Point {
      X = 2 * 3,
      Y = 9
    };
    IACSharpSensor.IACSharpSensor.SensorReached(1377);
    if (markerPosition.X != 6) {
      System.Int32 RNTRNTRNT_286 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1378);
      return RNTRNTRNT_286;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1379);
    if (markerPosition.Y != 9) {
      System.Int32 RNTRNTRNT_287 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1380);
      return RNTRNTRNT_287;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1381);
    Point[] pa = new Point[] {
      new Point { X = 9 },
      new Point { X = 8 }
    };
    IACSharpSensor.IACSharpSensor.SensorReached(1382);
    if (pa[0].X != 9) {
      System.Int32 RNTRNTRNT_288 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1383);
      return RNTRNTRNT_288;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1384);
    if (pa[1].X != 8) {
      System.Int32 RNTRNTRNT_289 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1385);
      return RNTRNTRNT_289;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1386);
    p = new Point { Y = -1 };
    IACSharpSensor.IACSharpSensor.SensorReached(1387);
    if (p.Y != -1) {
      System.Int32 RNTRNTRNT_290 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1388);
      return RNTRNTRNT_290;
    }
    System.Int32 RNTRNTRNT_291 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1389);
    return RNTRNTRNT_291;
  }
}
