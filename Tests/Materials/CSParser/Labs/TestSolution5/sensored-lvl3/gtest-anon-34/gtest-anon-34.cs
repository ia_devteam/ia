using System;
public class MyClass
{
  public string Foo = "Bar";
  private int answer;
  public int Answer {
    get {
      System.Int32 RNTRNTRNT_45 = answer;
      IACSharpSensor.IACSharpSensor.SensorReached(122);
      return RNTRNTRNT_45;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(123);
      answer = value;
      IACSharpSensor.IACSharpSensor.SensorReached(124);
    }
  }
}
public class Test
{
  delegate void D();
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(125);
    MyClass mc = null;
    D d = delegate() { mc = new MyClass {Foo = "Baz",Answer = 42}; };
    d();
    if (mc.Foo != "Baz") {
      System.Int32 RNTRNTRNT_46 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(126);
      return RNTRNTRNT_46;
    }
    if (mc.Answer != 42) {
      System.Int32 RNTRNTRNT_47 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(127);
      return RNTRNTRNT_47;
    }
    System.Int32 RNTRNTRNT_48 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(128);
    return RNTRNTRNT_48;
  }
}
