namespace A
{
  public static class Test_A
  {
    public static string Test_1(this string s)
    {
      System.String RNTRNTRNT_233 = ":";
      IACSharpSensor.IACSharpSensor.SensorReached(999);
      return RNTRNTRNT_233;
    }
  }
}
namespace A
{
  public static partial class Test_B
  {
    public static string Test_2(this string s)
    {
      System.String RNTRNTRNT_234 = ":";
      IACSharpSensor.IACSharpSensor.SensorReached(1000);
      return RNTRNTRNT_234;
    }
  }
}
namespace B
{
  using A;
  public class M
  {
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1001);
      "".Test_1();
      "".Test_2();
      IACSharpSensor.IACSharpSensor.SensorReached(1002);
    }
  }
}
