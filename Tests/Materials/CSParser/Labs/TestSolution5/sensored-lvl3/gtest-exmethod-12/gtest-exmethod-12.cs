using System;
using System.Collections.Specialized;
class Program
{
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1019);
    var chat = new ChatClient();
    var lines = new StringCollection {
      "a",
      "b",
      "c"
    };
    chat.Say("test", lines);
    IACSharpSensor.IACSharpSensor.SensorReached(1020);
  }
}
class ChatClient
{
  public void Say(string to, string message)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1021);
    Console.WriteLine("{0}: {1}", to, message);
    IACSharpSensor.IACSharpSensor.SensorReached(1022);
  }
}
static class ChatExtensions
{
  public static void Say(this ChatClient chat, string to, StringCollection lines)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1023);
    foreach (string line in lines) {
      chat.Say(to, line);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1024);
  }
}
