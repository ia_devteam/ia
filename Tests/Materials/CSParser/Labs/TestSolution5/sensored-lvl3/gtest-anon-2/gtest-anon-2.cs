using System;
public delegate void Simple();
public delegate Simple Foo();
class X
{
  public void Hello<U>(U u)
  {
  }
  public void Test<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(39);
    T u = t;
    Hello(u);
    Foo foo = delegate {
      T v = u;
      Hello(u);
      void  RNTRNTRNT_7 = delegate {
        Hello(u);
        Hello(v);
      };
      IACSharpSensor.IACSharpSensor.SensorReached(40);
      return RNTRNTRNT_7;
    };
    Simple simple = foo();
    simple();
    Hello(u);
    IACSharpSensor.IACSharpSensor.SensorReached(41);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(43);
  }
}
