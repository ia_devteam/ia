using System;
using System.Collections.Generic;
namespace MonoBugs
{
  public class BrokenGenericCast
  {
    public static Converter<TSource, TDest> GetUpcaster<TSource, TDest>() where TSource : TDest
    {
      Converter<TSource,TDest> RNTRNTRNT_37 = delegate(TSource obj) { return obj; };
      IACSharpSensor.IACSharpSensor.SensorReached(93);
      return RNTRNTRNT_37;
    }
    public static Converter<TSource, TDest> GetDowncaster<TSource, TDest>() where TDest : TSource
    {
      Converter<TSource,TDest> RNTRNTRNT_38 = delegate(TSource obj) { return (TDest)obj; };
      IACSharpSensor.IACSharpSensor.SensorReached(94);
      return RNTRNTRNT_38;
    }
    public static void Main()
    {
    }
  }
}
