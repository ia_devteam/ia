using System;
using System.Collections.Generic;
public delegate void Hello();
struct Foo
{
  public int ID;
  public Foo(int id)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(153);
    this.ID = id;
    IACSharpSensor.IACSharpSensor.SensorReached(154);
  }
  public IEnumerable<Foo> Test(Foo foo)
  {
    Foo RNTRNTRNT_61 = this;
    IACSharpSensor.IACSharpSensor.SensorReached(155);
    yield return RNTRNTRNT_61;
    IACSharpSensor.IACSharpSensor.SensorReached(156);
    Foo RNTRNTRNT_62 = foo;
    IACSharpSensor.IACSharpSensor.SensorReached(157);
    yield return RNTRNTRNT_62;
    IACSharpSensor.IACSharpSensor.SensorReached(158);
    IACSharpSensor.IACSharpSensor.SensorReached(159);
  }
  public void Hello(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(160);
    if (ID != value) {
      throw new InvalidOperationException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(161);
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_63 = String.Format("Foo ({0})", ID);
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    return RNTRNTRNT_63;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(163);
    Foo foo = new Foo(3);
    foreach (Foo bar in foo.Test(new Foo(8))) {
      Console.WriteLine(bar);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(164);
  }
}
