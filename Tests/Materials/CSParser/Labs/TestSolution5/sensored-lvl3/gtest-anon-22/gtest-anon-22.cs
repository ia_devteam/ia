using System.Linq.Expressions;
delegate int D1();
delegate long D2();
class C
{
  static int Foo(D1 d)
  {
    System.Int32 RNTRNTRNT_10 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(49);
    return RNTRNTRNT_10;
  }
  static int Foo(D2 d)
  {
    System.Int32 RNTRNTRNT_11 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(50);
    return RNTRNTRNT_11;
  }
  static int FooE(Expression<D1> d)
  {
    System.Int32 RNTRNTRNT_12 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(51);
    return RNTRNTRNT_12;
  }
  static int FooE(Expression<D2> d)
  {
    System.Int32 RNTRNTRNT_13 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(52);
    return RNTRNTRNT_13;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    if (Foo(delegate() { return 1; }) != 1) {
      System.Int32 RNTRNTRNT_14 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      return RNTRNTRNT_14;
    }
    FooE(() => 1);
    System.Int32 RNTRNTRNT_15 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(55);
    return RNTRNTRNT_15;
  }
}
