using System;
using System.Reflection;
using System.Runtime.CompilerServices;
public class Test
{
  public string Foo { get; set; }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(259);
    FieldInfo[] fields = typeof(Test).GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
    if (!(fields.Length > 0)) {
      System.Int32 RNTRNTRNT_115 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(260);
      return RNTRNTRNT_115;
    }
    object[] field_atts = fields[0].GetCustomAttributes(false);
    if (!(field_atts.Length > 0)) {
      System.Int32 RNTRNTRNT_116 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(261);
      return RNTRNTRNT_116;
    }
    if (field_atts[0].GetType() != typeof(CompilerGeneratedAttribute)) {
      System.Int32 RNTRNTRNT_117 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(262);
      return RNTRNTRNT_117;
    }
    if (fields[0].Name != "<Foo>k__BackingField") {
      System.Int32 RNTRNTRNT_118 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(263);
      return RNTRNTRNT_118;
    }
    PropertyInfo property = typeof(Test).GetProperty("Foo");
    MethodInfo @get = property.GetGetMethod(false);
    object[] get_atts = @get.GetCustomAttributes(false);
    if (!(get_atts.Length > 0)) {
      System.Int32 RNTRNTRNT_119 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(264);
      return RNTRNTRNT_119;
    }
    if (get_atts[0].GetType() != typeof(CompilerGeneratedAttribute)) {
      System.Int32 RNTRNTRNT_120 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(265);
      return RNTRNTRNT_120;
    }
    MethodInfo @set = property.GetSetMethod(false);
    object[] set_atts = @set.GetCustomAttributes(false);
    if (!(set_atts.Length > 0)) {
      System.Int32 RNTRNTRNT_121 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(266);
      return RNTRNTRNT_121;
    }
    if (set_atts[0].GetType() != typeof(CompilerGeneratedAttribute)) {
      System.Int32 RNTRNTRNT_122 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(267);
      return RNTRNTRNT_122;
    }
    System.Int32 RNTRNTRNT_123 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(268);
    return RNTRNTRNT_123;
  }
}
