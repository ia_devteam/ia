using System;
using System.Collections.Generic;
unsafe class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(145);
    foreach (int item in GetItems()) {
      Console.WriteLine(item);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(146);
  }
  unsafe public static int GetItem()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(147);
    byte[] value = new byte[] {
      0xde,
      0xad,
      0xbe,
      0xef
    };
    fixed (byte* valueptr = value) {
      System.Int32 RNTRNTRNT_59 = *(int*)valueptr;
      IACSharpSensor.IACSharpSensor.SensorReached(148);
      return RNTRNTRNT_59;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(149);
  }
  public static IEnumerable<int> GetItems()
  {
    System.Int32 RNTRNTRNT_60 = GetItem();
    IACSharpSensor.IACSharpSensor.SensorReached(150);
    yield return RNTRNTRNT_60;
    IACSharpSensor.IACSharpSensor.SensorReached(151);
    IACSharpSensor.IACSharpSensor.SensorReached(152);
  }
}
