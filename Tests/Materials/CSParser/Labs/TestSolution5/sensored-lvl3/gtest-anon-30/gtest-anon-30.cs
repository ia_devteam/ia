using System;
public delegate void Simple();
public delegate Simple Foo();
class X
{
  public void Hello<U>(U u)
  {
  }
  public void Test<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(106);
    {
      T u = t;
      Hello(u);
      Foo foo = delegate {
        T v = u;
        Hello(u);
        void  RNTRNTRNT_41 = delegate {
          Hello(u);
          Hello(v);
        };
        IACSharpSensor.IACSharpSensor.SensorReached(105);
        return RNTRNTRNT_41;
      };
    }
    IACSharpSensor.IACSharpSensor.SensorReached(107);
  }
  static void Main()
  {
  }
}
