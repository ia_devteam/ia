using System;
using System.Collections;
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(197);
    var v = new {
      Foo = "Bar",
      Baz = 42
    };
    if (v.Foo != "Bar") {
      System.Int32 RNTRNTRNT_73 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(198);
      return RNTRNTRNT_73;
    }
    if (v.Baz != 42) {
      System.Int32 RNTRNTRNT_74 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(199);
      return RNTRNTRNT_74;
    }
    System.Int32 RNTRNTRNT_75 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(200);
    return RNTRNTRNT_75;
  }
}
