using System.Collections.Generic;
interface IA
{
}
interface IB : IA
{
}
static class E
{
  static internal void ToReadOnly<T>(this IEnumerable<T> source)
  {
  }
  static internal void To(this IA i)
  {
  }
}
class C
{
  public static void Main()
  {
  }
  public static void Test(IEnumerable<bool> bindings)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1007);
    bindings.ToReadOnly();
    IB ib = null;
    ib.To();
    IACSharpSensor.IACSharpSensor.SensorReached(1008);
  }
}
