using System;
using System.Collections.Generic;
interface I
{
}
namespace Outer.Inner
{
  class Test
  {
    static void M(I list)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1056);
      list.AddRange(new Test[0]);
      IACSharpSensor.IACSharpSensor.SensorReached(1057);
    }
    static void Main()
    {
    }
  }
}
namespace Outer
{
  static class ExtensionMethods
  {
    public static void AddRange<T>(this I list, IEnumerable<T> items)
    {
    }
  }
}
