using System;
using System.Collections.Generic;
class X
{
  public IEnumerable<T> Test<T>(T a, T b)
  {
    T RNTRNTRNT_1 = b;
    IACSharpSensor.IACSharpSensor.SensorReached(5);
    yield return RNTRNTRNT_1;
    IACSharpSensor.IACSharpSensor.SensorReached(6);
    b = a;
    T RNTRNTRNT_2 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(7);
    yield return RNTRNTRNT_2;
    IACSharpSensor.IACSharpSensor.SensorReached(8);
    IACSharpSensor.IACSharpSensor.SensorReached(9);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(10);
    X x = new X();
    long sum = 0;
    foreach (long i in x.Test(3, 5)) {
      Console.WriteLine(i);
      sum += i;
    }
    Console.WriteLine(sum);
    System.Int32 RNTRNTRNT_3 = sum == 8 ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(11);
    return RNTRNTRNT_3;
  }
}
