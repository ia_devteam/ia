struct Point
{
  public int X, Y;
}
class C
{
  static Point p;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1098);
    new Point {
      X = 0,
      Y = 0
    };
    var markerPosition = new Point {
      X = 2 * 3,
      Y = 9
    };
    if (markerPosition.X != 6) {
      System.Int32 RNTRNTRNT_286 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1099);
      return RNTRNTRNT_286;
    }
    if (markerPosition.Y != 9) {
      System.Int32 RNTRNTRNT_287 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1100);
      return RNTRNTRNT_287;
    }
    Point[] pa = new Point[] {
      new Point { X = 9 },
      new Point { X = 8 }
    };
    if (pa[0].X != 9) {
      System.Int32 RNTRNTRNT_288 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1101);
      return RNTRNTRNT_288;
    }
    if (pa[1].X != 8) {
      System.Int32 RNTRNTRNT_289 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1102);
      return RNTRNTRNT_289;
    }
    p = new Point { Y = -1 };
    if (p.Y != -1) {
      System.Int32 RNTRNTRNT_290 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1103);
      return RNTRNTRNT_290;
    }
    System.Int32 RNTRNTRNT_291 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1104);
    return RNTRNTRNT_291;
  }
}
