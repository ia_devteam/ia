using System;
using External;
interface I
{
}
namespace Outer.Inner
{
  class Test
  {
    static void M(I list)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1058);
      list.AddRange();
      IACSharpSensor.IACSharpSensor.SensorReached(1059);
    }
    static void Main()
    {
    }
  }
}
namespace Outer
{
}
namespace External
{
  static class ExtensionMethods
  {
    public static void AddRange(this I list)
    {
    }
  }
}
