using System;
delegate void Foo<S>(S s);
class X
{
  public void Hello<U>(U u)
  {
  }
  public void Test<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(101);
    Hello(t);
    Foo<T> foo = delegate(T u) { Hello(u); };
    foo(t);
    IACSharpSensor.IACSharpSensor.SensorReached(102);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(103);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(104);
  }
}
