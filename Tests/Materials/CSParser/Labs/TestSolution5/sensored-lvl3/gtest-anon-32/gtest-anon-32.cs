using System;
using System.Collections.Generic;
public class Program
{
  public static void Assert(Action<int> action)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(110);
    action(42);
    IACSharpSensor.IACSharpSensor.SensorReached(111);
  }
  public static void Foo<T>(IList<T> list)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(112);
    Assert(i =>
    {
      T[] backup = new T[list.Count];
    });
    IACSharpSensor.IACSharpSensor.SensorReached(113);
  }
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(114);
    Foo(args);
    IACSharpSensor.IACSharpSensor.SensorReached(115);
  }
}
