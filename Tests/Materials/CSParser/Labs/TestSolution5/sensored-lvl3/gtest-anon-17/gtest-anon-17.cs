public class C
{
  public delegate T Func<T>(T t);
  public static void Test<T, U>(Func<T> f, U u)
  {
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(27);
    Test<int, string>(delegate(int i) { return i; }, "");
    Test(delegate(int i) { return i; }, 1);
    IACSharpSensor.IACSharpSensor.SensorReached(28);
  }
}
