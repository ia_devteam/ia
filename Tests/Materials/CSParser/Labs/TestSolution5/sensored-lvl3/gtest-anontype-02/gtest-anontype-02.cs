using System;
using System.Collections;
public class Test
{
  static object TestA(string s)
  {
    System.Object RNTRNTRNT_76 = new { s };
    IACSharpSensor.IACSharpSensor.SensorReached(201);
    return RNTRNTRNT_76;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(202);
    string Foo = "Bar";
    int Baz = 42;
    var v = new {
      Foo,
      Baz
    };
    if (v.Foo != "Bar") {
      System.Int32 RNTRNTRNT_77 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(203);
      return RNTRNTRNT_77;
    }
    if (v.Baz != 42) {
      System.Int32 RNTRNTRNT_78 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(204);
      return RNTRNTRNT_78;
    }
    if (!TestA("foo").Equals(new { s = "foo" })) {
      System.Int32 RNTRNTRNT_79 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(205);
      return RNTRNTRNT_79;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_80 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    return RNTRNTRNT_80;
  }
}
