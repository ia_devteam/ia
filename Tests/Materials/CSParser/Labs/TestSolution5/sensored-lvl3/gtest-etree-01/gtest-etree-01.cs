using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
public struct InverseLogicalOperator
{
  bool value;
  public InverseLogicalOperator(bool value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(289);
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(290);
  }
  public static bool operator true(InverseLogicalOperator u)
  {
    System.Boolean RNTRNTRNT_134 = u.value;
    IACSharpSensor.IACSharpSensor.SensorReached(291);
    return RNTRNTRNT_134;
  }
  public static bool operator false(InverseLogicalOperator u)
  {
    System.Boolean RNTRNTRNT_135 = u.value;
    IACSharpSensor.IACSharpSensor.SensorReached(292);
    return RNTRNTRNT_135;
  }
}
public struct MyType
{
  int value;
  public MyType(int value) : this()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(293);
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(294);
  }
  public short ShortProp { get; set; }
  public override int GetHashCode()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(295);
    throw new NotImplementedException();
  }
  public static implicit operator int(MyType o)
  {
    System.Int32 RNTRNTRNT_136 = o.value;
    IACSharpSensor.IACSharpSensor.SensorReached(296);
    return RNTRNTRNT_136;
  }
  public static bool operator true(MyType a)
  {
    System.Boolean RNTRNTRNT_137 = a.value == a;
    IACSharpSensor.IACSharpSensor.SensorReached(297);
    return RNTRNTRNT_137;
  }
  public static bool operator false(MyType a)
  {
    System.Boolean RNTRNTRNT_138 = a.value != a;
    IACSharpSensor.IACSharpSensor.SensorReached(298);
    return RNTRNTRNT_138;
  }
  public static MyType operator +(MyType a, MyType b)
  {
    MyType RNTRNTRNT_139 = new MyType(a.value + b.value);
    IACSharpSensor.IACSharpSensor.SensorReached(299);
    return RNTRNTRNT_139;
  }
  public static MyType operator -(MyType a, MyType b)
  {
    MyType RNTRNTRNT_140 = new MyType(a.value - b.value);
    IACSharpSensor.IACSharpSensor.SensorReached(300);
    return RNTRNTRNT_140;
  }
  public static MyType operator /(MyType a, MyType b)
  {
    MyType RNTRNTRNT_141 = new MyType(a.value / b.value);
    IACSharpSensor.IACSharpSensor.SensorReached(301);
    return RNTRNTRNT_141;
  }
  public static MyType operator *(MyType a, MyType b)
  {
    MyType RNTRNTRNT_142 = new MyType(a.value * b.value);
    IACSharpSensor.IACSharpSensor.SensorReached(302);
    return RNTRNTRNT_142;
  }
  public static MyType operator %(MyType a, MyType b)
  {
    MyType RNTRNTRNT_143 = new MyType(a.value % b.value);
    IACSharpSensor.IACSharpSensor.SensorReached(303);
    return RNTRNTRNT_143;
  }
  public static MyType operator &(MyType a, MyType b)
  {
    MyType RNTRNTRNT_144 = new MyType(a.value & b.value);
    IACSharpSensor.IACSharpSensor.SensorReached(304);
    return RNTRNTRNT_144;
  }
  public static MyType operator |(MyType a, MyType b)
  {
    MyType RNTRNTRNT_145 = new MyType(a.value | b.value);
    IACSharpSensor.IACSharpSensor.SensorReached(305);
    return RNTRNTRNT_145;
  }
  public static MyType operator ^(MyType a, MyType b)
  {
    MyType RNTRNTRNT_146 = new MyType(a.value ^ b.value);
    IACSharpSensor.IACSharpSensor.SensorReached(306);
    return RNTRNTRNT_146;
  }
  public static bool operator ==(MyType a, MyType b)
  {
    System.Boolean RNTRNTRNT_147 = a.value == b.value;
    IACSharpSensor.IACSharpSensor.SensorReached(307);
    return RNTRNTRNT_147;
  }
  public static bool operator !=(MyType a, MyType b)
  {
    System.Boolean RNTRNTRNT_148 = a.value != b.value;
    IACSharpSensor.IACSharpSensor.SensorReached(308);
    return RNTRNTRNT_148;
  }
  public static bool operator >(MyType a, MyType b)
  {
    System.Boolean RNTRNTRNT_149 = a.value > b.value;
    IACSharpSensor.IACSharpSensor.SensorReached(309);
    return RNTRNTRNT_149;
  }
  public static bool operator <(MyType a, MyType b)
  {
    System.Boolean RNTRNTRNT_150 = a.value < b.value;
    IACSharpSensor.IACSharpSensor.SensorReached(310);
    return RNTRNTRNT_150;
  }
  public static bool operator >=(MyType a, MyType b)
  {
    System.Boolean RNTRNTRNT_151 = a.value >= b.value;
    IACSharpSensor.IACSharpSensor.SensorReached(311);
    return RNTRNTRNT_151;
  }
  public static bool operator <=(MyType a, MyType b)
  {
    System.Boolean RNTRNTRNT_152 = a.value <= b.value;
    IACSharpSensor.IACSharpSensor.SensorReached(312);
    return RNTRNTRNT_152;
  }
  public static bool operator !(MyType a)
  {
    System.Boolean RNTRNTRNT_153 = a.value > 0;
    IACSharpSensor.IACSharpSensor.SensorReached(313);
    return RNTRNTRNT_153;
  }
  public static int operator >>(MyType a, int b)
  {
    System.Int32 RNTRNTRNT_154 = a.value >> b;
    IACSharpSensor.IACSharpSensor.SensorReached(314);
    return RNTRNTRNT_154;
  }
  public static int operator <<(MyType a, int b)
  {
    System.Int32 RNTRNTRNT_155 = a.value << b;
    IACSharpSensor.IACSharpSensor.SensorReached(315);
    return RNTRNTRNT_155;
  }
  public static MyType operator -(MyType a)
  {
    MyType RNTRNTRNT_156 = new MyType(-a.value);
    IACSharpSensor.IACSharpSensor.SensorReached(316);
    return RNTRNTRNT_156;
  }
  public static MyType operator +(MyType a)
  {
    MyType RNTRNTRNT_157 = new MyType(+a.value);
    IACSharpSensor.IACSharpSensor.SensorReached(317);
    return RNTRNTRNT_157;
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_158 = value.ToString();
    IACSharpSensor.IACSharpSensor.SensorReached(318);
    return RNTRNTRNT_158;
  }
}
class MyTypeExplicit
{
  int value;
  public MyTypeExplicit(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(319);
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(320);
  }
  public static explicit operator int(MyTypeExplicit m)
  {
    System.Int32 RNTRNTRNT_159 = m.value;
    IACSharpSensor.IACSharpSensor.SensorReached(321);
    return RNTRNTRNT_159;
  }
}
struct MyTypeImplicitOnly
{
  short b;
  public MyTypeImplicitOnly(short b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(322);
    this.b = b;
    IACSharpSensor.IACSharpSensor.SensorReached(323);
  }
  public static implicit operator short(MyTypeImplicitOnly m)
  {
    System.Int16 RNTRNTRNT_160 = m.b;
    IACSharpSensor.IACSharpSensor.SensorReached(324);
    return RNTRNTRNT_160;
  }
}
class MemberAccessData
{
  public bool BoolValue;
  public static decimal DecimalValue = decimal.MinValue;
  public volatile uint VolatileValue;
  public string[] StringValues;
  public List<string> ListValues;
  event Func<bool> EventField;
  public Expression<Func<Func<bool>>> GetEvent()
  {
    Expression<Func<Func<System.Boolean>>> RNTRNTRNT_161 = () => EventField;
    IACSharpSensor.IACSharpSensor.SensorReached(325);
    return RNTRNTRNT_161;
  }
  MyType mt;
  public MyType MyTypeProperty {
    get {
      MyType RNTRNTRNT_162 = mt;
      IACSharpSensor.IACSharpSensor.SensorReached(326);
      return RNTRNTRNT_162;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(327);
      mt = value;
      IACSharpSensor.IACSharpSensor.SensorReached(328);
    }
  }
  public static string StaticProperty {
    get {
      System.String RNTRNTRNT_163 = "alo";
      IACSharpSensor.IACSharpSensor.SensorReached(329);
      return RNTRNTRNT_163;
    }
  }
}
enum MyEnum : byte
{
  Value_1 = 1,
  Value_2 = 2
}
enum MyEnumUlong : ulong
{
  Value_1 = 1
}
class NewTest<T>
{
  T[] t;
  public NewTest(T i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(330);
    t = new T[] { i };
    IACSharpSensor.IACSharpSensor.SensorReached(331);
  }
  public NewTest(params T[] t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(332);
    this.t = t;
    IACSharpSensor.IACSharpSensor.SensorReached(333);
  }
  public override int GetHashCode()
  {
    System.Int32 RNTRNTRNT_164 = base.GetHashCode();
    IACSharpSensor.IACSharpSensor.SensorReached(334);
    return RNTRNTRNT_164;
  }
  public override bool Equals(object obj)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(335);
    NewTest<T> obj_t = obj as NewTest<T>;
    if (obj_t == null) {
      System.Boolean RNTRNTRNT_165 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(336);
      return RNTRNTRNT_165;
    }
    for (int i = 0; i < t.Length; ++i) {
      if (!t[i].Equals(obj_t.t[i])) {
        System.Boolean RNTRNTRNT_166 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(337);
        return RNTRNTRNT_166;
      }
    }
    System.Boolean RNTRNTRNT_167 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(338);
    return RNTRNTRNT_167;
  }
}
class Indexer
{
  public int this[int i] {
    get {
      System.Int32 RNTRNTRNT_168 = i;
      IACSharpSensor.IACSharpSensor.SensorReached(339);
      return RNTRNTRNT_168;
    }
    set { }
  }
  public string this[params string[] i] {
    get {
      System.String RNTRNTRNT_169 = string.Concat(i);
      IACSharpSensor.IACSharpSensor.SensorReached(340);
      return RNTRNTRNT_169;
    }
  }
}
class Tester
{
  delegate void EmptyDelegate();
  delegate int IntDelegate();
  static void AssertNodeType(LambdaExpression e, ExpressionType et)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(341);
    if (e.Body.NodeType != et) {
      throw new ApplicationException(e.Body.NodeType + " != " + et);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(342);
  }
  static void Assert<T>(T expected, T value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(343);
    Assert(expected, value, null);
    IACSharpSensor.IACSharpSensor.SensorReached(344);
  }
  static void Assert<T>(T expected, T value, string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(345);
    if (!EqualityComparer<T>.Default.Equals(expected, value)) {
      if (!string.IsNullOrEmpty(name)) {
        name += ": ";
      }
      throw new ApplicationException(name + expected + " != " + value);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(346);
  }
  static void Assert<T>(T[] expected, T[] value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(347);
    if (expected == null) {
      if (value != null) {
        throw new ApplicationException("Both arrays expected to be null");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(348);
      return;
    }
    if (expected.Length != value.Length) {
      throw new ApplicationException("Array length does not match " + expected.Length + " != " + value.Length);
    }
    for (int i = 0; i < expected.Length; ++i) {
      if (!EqualityComparer<T>.Default.Equals(expected[i], value[i])) {
        throw new ApplicationException("Index " + i + ": " + expected[i] + " != " + value[i]);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(349);
  }
  void AddTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(350);
    Expression<Func<int, int, int>> e = (int a, int b) => a + b;
    AssertNodeType(e, ExpressionType.Add);
    Assert(50, e.Compile().Invoke(20, 30));
    IACSharpSensor.IACSharpSensor.SensorReached(351);
  }
  void AddTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(352);
    Expression<Func<int?, int?, int?>> e2 = (a, b) => a + b;
    AssertNodeType(e2, ExpressionType.Add);
    Assert(null, e2.Compile().Invoke(null, 3));
    IACSharpSensor.IACSharpSensor.SensorReached(353);
  }
  void AddTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(354);
    Expression<Func<MyType, MyType, MyType>> e3 = (MyType a, MyType b) => a + b;
    AssertNodeType(e3, ExpressionType.Add);
    Assert(10, e3.Compile().Invoke(new MyType(-20), new MyType(30)));
    IACSharpSensor.IACSharpSensor.SensorReached(355);
  }
  void AddTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(356);
    Expression<Func<MyType?, MyType?, MyType?>> e4 = (MyType? a, MyType? b) => a + b;
    AssertNodeType(e4, ExpressionType.Add);
    Assert(new MyType(10), e4.Compile().Invoke(new MyType(-20), new MyType(30)));
    Assert(null, e4.Compile().Invoke(null, new MyType(30)));
    IACSharpSensor.IACSharpSensor.SensorReached(357);
  }
  void AddTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(358);
    Expression<Func<int, MyType, int>> e5 = (int a, MyType b) => a + b;
    AssertNodeType(e5, ExpressionType.Add);
    Assert(31, e5.Compile().Invoke(1, new MyType(30)));
    IACSharpSensor.IACSharpSensor.SensorReached(359);
  }
  void AddTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(360);
    Expression<Func<int, MyType?, int?>> e6 = (int a, MyType? b) => a + b;
    AssertNodeType(e6, ExpressionType.Add);
    Assert(-1, e6.Compile().Invoke(-31, new MyType(30)));
    IACSharpSensor.IACSharpSensor.SensorReached(361);
  }
  void AddTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(362);
    Expression<Func<MyEnum, byte, MyEnum>> e7 = (a, b) => a + b;
    AssertNodeType(e7, ExpressionType.Convert);
    Assert(MyEnum.Value_2, e7.Compile().Invoke(MyEnum.Value_1, 1));
    IACSharpSensor.IACSharpSensor.SensorReached(363);
  }
  void AddTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(364);
    Expression<Func<MyEnum?, byte?, MyEnum?>> e8 = (a, b) => a + b;
    AssertNodeType(e8, ExpressionType.Convert);
    Assert<MyEnum?>(0, e8.Compile().Invoke(MyEnum.Value_1, 255));
    Assert(null, e8.Compile().Invoke(MyEnum.Value_1, null));
    Assert(null, e8.Compile().Invoke(null, null));
    IACSharpSensor.IACSharpSensor.SensorReached(365);
  }
  void AddTest_9()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(366);
    Expression<Func<byte, MyEnum, MyEnum>> e9 = (a, b) => a + b;
    AssertNodeType(e9, ExpressionType.Convert);
    Assert(MyEnum.Value_2, e9.Compile().Invoke(1, MyEnum.Value_1));
    IACSharpSensor.IACSharpSensor.SensorReached(367);
  }
  void AddCheckedTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    checked {
      Expression<Func<int, int, int>> e = (int a, int b) => a + b;
      AssertNodeType(e, ExpressionType.AddChecked);
      Assert(50, e.Compile().Invoke(20, 30));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(369);
  }
  void AddCheckedTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(370);
    checked {
      Expression<Func<int?, int?, int?>> e2 = (a, b) => a + b;
      AssertNodeType(e2, ExpressionType.AddChecked);
      Assert(null, e2.Compile().Invoke(null, 3));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(371);
  }
  void AddCheckedTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(372);
    checked {
      Expression<Func<MyType, MyType, MyType>> e3 = (MyType a, MyType b) => a + b;
      AssertNodeType(e3, ExpressionType.Add);
      Assert(10, e3.Compile().Invoke(new MyType(-20), new MyType(30)));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(373);
  }
  void AddStringTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(374);
    Expression<Func<string, string>> e6 = a => 1 + a;
    AssertNodeType(e6, ExpressionType.Add);
    Assert("1to", e6.Compile().Invoke("to"));
    IACSharpSensor.IACSharpSensor.SensorReached(375);
  }
  void AddStringTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(376);
    Expression<Func<object, string, string>> e7 = (object a, string b) => a + b;
    AssertNodeType(e7, ExpressionType.Add);
    Assert("testme", e7.Compile().Invoke("test", "me"));
    Assert("test", e7.Compile().Invoke("test", null));
    Assert("", e7.Compile().Invoke(null, null));
    IACSharpSensor.IACSharpSensor.SensorReached(377);
  }
  void AddStringTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(378);
    Expression<Func<string, int, string>> e8 = (a, b) => a + " " + "-" + "> " + b;
    AssertNodeType(e8, ExpressionType.Add);
    Assert("test -> 2", e8.Compile().Invoke("test", 2));
    IACSharpSensor.IACSharpSensor.SensorReached(379);
  }
  void AddStringTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(380);
    Expression<Func<string, ushort?, string>> e9 = (a, b) => a + b;
    AssertNodeType(e9, ExpressionType.Add);
    Assert("test2", e9.Compile().Invoke("test", 2));
    Assert("test", e9.Compile().Invoke("test", null));
    IACSharpSensor.IACSharpSensor.SensorReached(381);
  }
  void AndTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(382);
    Expression<Func<bool, bool, bool>> e = (bool a, bool b) => a & b;
    AssertNodeType(e, ExpressionType.And);
    Func<bool, bool, bool> c = e.Compile();
    Assert(true, c(true, true));
    Assert(false, c(true, false));
    Assert(false, c(false, true));
    Assert(false, c(false, false));
    IACSharpSensor.IACSharpSensor.SensorReached(383);
  }
  void AndTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(384);
    Expression<Func<MyType, MyType, MyType>> e2 = (MyType a, MyType b) => a & b;
    AssertNodeType(e2, ExpressionType.And);
    var c2 = e2.Compile();
    Assert(new MyType(0), c2(new MyType(0), new MyType(1)));
    Assert(new MyType(1), c2(new MyType(0xff), new MyType(0x1)));
    IACSharpSensor.IACSharpSensor.SensorReached(385);
  }
  void AndTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(386);
    Expression<Func<MyEnum, MyEnum, MyEnum>> e3 = (a, b) => a & b;
    AssertNodeType(e3, ExpressionType.Convert);
    Assert<MyEnum>(0, e3.Compile().Invoke(MyEnum.Value_1, MyEnum.Value_2));
    Assert(MyEnum.Value_2, e3.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(387);
  }
  void AndNullableTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(388);
    Expression<Func<bool?, bool?, bool?>> e = (bool? a, bool? b) => a & b;
    AssertNodeType(e, ExpressionType.And);
    Func<bool?, bool?, bool?> c = e.Compile();
    Assert(true, c(true, true));
    Assert(false, c(true, false));
    Assert(false, c(false, true));
    Assert(false, c(false, false));
    Assert(null, c(true, null));
    Assert(false, c(false, null));
    Assert(false, c(null, false));
    Assert(null, c(true, null));
    Assert(null, c(null, null));
    IACSharpSensor.IACSharpSensor.SensorReached(389);
  }
  void AndNullableTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(390);
    Expression<Func<MyType?, MyType?, MyType?>> e2 = (MyType? a, MyType? b) => a & b;
    AssertNodeType(e2, ExpressionType.And);
    var c2 = e2.Compile();
    Assert(new MyType(0), c2(new MyType(0), new MyType(1)));
    Assert(new MyType(1), c2(new MyType(0xff), new MyType(0x1)));
    Assert(null, c2(new MyType(0xff), null));
    IACSharpSensor.IACSharpSensor.SensorReached(391);
  }
  void AndNullableTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(392);
    Expression<Func<MyEnum?, MyEnum?, MyEnum?>> e3 = (a, b) => a & b;
    AssertNodeType(e3, ExpressionType.Convert);
    Assert(null, e3.Compile().Invoke(null, MyEnum.Value_2));
    Assert(MyEnum.Value_2, e3.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(393);
  }
  void AndAlsoTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(394);
    Expression<Func<bool, bool, bool>> e = (bool a, bool b) => a && b;
    AssertNodeType(e, ExpressionType.AndAlso);
    Assert(false, e.Compile().Invoke(true, false));
    IACSharpSensor.IACSharpSensor.SensorReached(395);
  }
  void AndAlsoTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(396);
    Expression<Func<MyType, MyType, MyType>> e2 = (MyType a, MyType b) => a && b;
    AssertNodeType(e2, ExpressionType.AndAlso);
    Assert(new MyType(64), e2.Compile().Invoke(new MyType(64), new MyType(64)));
    Assert(new MyType(0), e2.Compile().Invoke(new MyType(32), new MyType(64)));
    IACSharpSensor.IACSharpSensor.SensorReached(397);
  }
  void AndAlsoTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(398);
    Expression<Func<bool, bool>> e3 = (bool a) => a && true;
    AssertNodeType(e3, ExpressionType.AndAlso);
    Assert(false, e3.Compile().Invoke(false));
    Assert(true, e3.Compile().Invoke(true));
    IACSharpSensor.IACSharpSensor.SensorReached(399);
  }
  void ArrayIndexTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(400);
    Expression<Func<string[], long, string>> e = (string[] a, long i) => a[i];
    AssertNodeType(e, ExpressionType.ArrayIndex);
    Assert("b", e.Compile().Invoke(new string[] {
      "a",
      "b",
      "c"
    }, 1));
    IACSharpSensor.IACSharpSensor.SensorReached(401);
  }
  void ArrayIndexTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(402);
    Expression<Func<string[], string>> e2 = (string[] a) => a[0];
    AssertNodeType(e2, ExpressionType.ArrayIndex);
    Assert("a", e2.Compile().Invoke(new string[] {
      "a",
      "b"
    }));
    IACSharpSensor.IACSharpSensor.SensorReached(403);
  }
  void ArrayIndexTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(404);
    Expression<Func<object[,], int, int, object>> e3 = (object[,] a, int i, int j) => a[i, j];
    AssertNodeType(e3, ExpressionType.Call);
    Assert("z", e3.Compile().Invoke(new object[,] {
      {
        1,
        2
      },
      {
        "x",
        "z"
      }
    }, 1, 1));
    IACSharpSensor.IACSharpSensor.SensorReached(405);
  }
  void ArrayIndexTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(406);
    Expression<Func<decimal[][], byte, decimal>> e4 = (decimal[][] a, byte b) => a[b][1];
    AssertNodeType(e4, ExpressionType.ArrayIndex);
    decimal[][] array = {
      new decimal[] {
        1,
        9
      },
      new decimal[] {
        10,
        90
      }
    };
    Assert(90, e4.Compile().Invoke(array, 1));
    IACSharpSensor.IACSharpSensor.SensorReached(407);
  }
  void ArrayIndexTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(408);
    Expression<Func<int>> e5 = () => (new int[1])[0];
    AssertNodeType(e5, ExpressionType.ArrayIndex);
    Assert(0, e5.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(409);
  }
  void ArrayLengthTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(410);
    Expression<Func<double[], int>> e = (double[] a) => a.Length;
    AssertNodeType(e, ExpressionType.ArrayLength);
    Assert(0, e.Compile().Invoke(new double[0]));
    Assert(9, e.Compile().Invoke(new double[9]));
    IACSharpSensor.IACSharpSensor.SensorReached(411);
  }
  void ArrayLengthTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(412);
    Expression<Func<string[,], int>> e2 = (string[,] a) => a.Length;
    AssertNodeType(e2, ExpressionType.MemberAccess);
    Assert(0, e2.Compile().Invoke(new string[0, 0]));
    IACSharpSensor.IACSharpSensor.SensorReached(413);
  }
  void CallTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(414);
    Expression<Func<int, int>> e = (int a) => Math.Max(a, 5);
    AssertNodeType(e, ExpressionType.Call);
    Assert(5, e.Compile().Invoke(2));
    Assert(9, e.Compile().Invoke(9));
    IACSharpSensor.IACSharpSensor.SensorReached(415);
  }
  void CallTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(416);
    Expression<Func<string, string>> e2 = (string a) => InstanceMethod(a);
    AssertNodeType(e2, ExpressionType.Call);
    Assert("abc", e2.Compile().Invoke("abc"));
    IACSharpSensor.IACSharpSensor.SensorReached(417);
  }
  void CallTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(418);
    Expression<Func<int, string, int, object>> e3 = (int index, string a, int b) => InstanceParamsMethod(index, a, b);
    AssertNodeType(e3, ExpressionType.Call);
    Assert<object>(4, e3.Compile().Invoke(1, "a", 4));
    IACSharpSensor.IACSharpSensor.SensorReached(419);
  }
  void CallTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(420);
    Expression<Func<object>> e4 = () => InstanceParamsMethod(0);
    AssertNodeType(e4, ExpressionType.Call);
    Assert<object>("<empty>", e4.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(421);
  }
  void CallTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(422);
    Expression<Func<int, int>> e5 = (int a) => GenericMethod(a);
    AssertNodeType(e5, ExpressionType.Call);
    Assert(5, e5.Compile().Invoke(5));
    IACSharpSensor.IACSharpSensor.SensorReached(423);
  }
  void CallTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(424);
    Expression<Action> e6 = () => Console.WriteLine("call test");
    AssertNodeType(e6, ExpressionType.Call);
    IACSharpSensor.IACSharpSensor.SensorReached(425);
  }
  void CallTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(426);
    Expression<Func<Indexer, int, int>> e7 = (a, b) => a[b];
    AssertNodeType(e7, ExpressionType.Call);
    Assert(3, e7.Compile().Invoke(new Indexer(), 3));
    IACSharpSensor.IACSharpSensor.SensorReached(427);
  }
  void CallTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(428);
    Expression<Func<Indexer, string, string, string, string>> e8 = (a, b, c, d) => a[b, c, d];
    AssertNodeType(e8, ExpressionType.Call);
    Assert("zyb", e8.Compile().Invoke(new Indexer(), "z", "y", "b"));
    IACSharpSensor.IACSharpSensor.SensorReached(429);
  }
  void CallTest_9()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(430);
    Expression<Action<int>> e9 = a => RefMethod(ref a);
    AssertNodeType(e9, ExpressionType.Call);
    e9.Compile().Invoke(1);
    IACSharpSensor.IACSharpSensor.SensorReached(431);
  }
  void CoalesceTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(432);
    Expression<Func<uint?, uint>> e = (uint? a) => a ?? 99;
    AssertNodeType(e, ExpressionType.Coalesce);
    var r = e.Compile();
    Assert((uint)5, r.Invoke(5));
    Assert((uint)99, r.Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(433);
  }
  void CoalesceTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(434);
    Expression<Func<MyType?, int>> e2 = (MyType? a) => a ?? -3;
    AssertNodeType(e2, ExpressionType.Coalesce);
    var r2 = e2.Compile();
    Assert(2, r2.Invoke(new MyType(2)));
    Assert(-3, r2.Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(435);
  }
  void ConditionTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(436);
    Expression<Func<bool, byte, int, int>> e = (bool a, byte b, int c) => (a ? b : c);
    AssertNodeType(e, ExpressionType.Conditional);
    var r = e.Compile();
    Assert(3, r.Invoke(true, 3, 999999));
    Assert(999999, r.Invoke(false, 3, 999999));
    IACSharpSensor.IACSharpSensor.SensorReached(437);
  }
  void ConditionTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(438);
    Expression<Func<int, decimal, decimal?>> e2 = (int a, decimal d) => (a > 0 ? d : a < 0 ? -d : (decimal?)null);
    AssertNodeType(e2, ExpressionType.Conditional);
    var r2 = e2.Compile();
    Assert(null, r2.Invoke(0, 10));
    Assert(50, r2.Invoke(1, 50));
    Assert(30, r2.Invoke(-7, -30));
    IACSharpSensor.IACSharpSensor.SensorReached(439);
  }
  void ConditionTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(440);
    Expression<Func<bool?, int?>> e3 = (bool? a) => ((bool)a ? 3 : -2);
    AssertNodeType(e3, ExpressionType.Convert);
    var r3 = e3.Compile();
    Assert(3, r3.Invoke(true));
    Assert(-2, r3.Invoke(false));
    IACSharpSensor.IACSharpSensor.SensorReached(441);
  }
  void ConditionTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(442);
    Expression<Func<InverseLogicalOperator, byte, byte, byte>> e4 = (InverseLogicalOperator a, byte b, byte c) => (a ? b : c);
    AssertNodeType(e4, ExpressionType.Conditional);
    var r4 = e4.Compile();
    Assert(3, r4.Invoke(new InverseLogicalOperator(true), 3, 4));
    Assert(4, r4.Invoke(new InverseLogicalOperator(false), 3, 4));
    IACSharpSensor.IACSharpSensor.SensorReached(443);
  }
  void ConstantTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(444);
    Expression<Func<int>> e1 = () => default(int);
    AssertNodeType(e1, ExpressionType.Constant);
    Assert(0, e1.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(445);
  }
  void ConstantTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(446);
    Expression<Func<int?>> e2 = () => default(int?);
    AssertNodeType(e2, ExpressionType.Constant);
    Assert(null, e2.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(447);
  }
  void ConstantTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(448);
    Expression<Func<Tester>> e3 = () => default(Tester);
    Assert(null, e3.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(449);
  }
  void ConstantTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(450);
    Expression<Func<object>> e4 = () => null;
    Assert(null, e4.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(451);
  }
  void ConstantTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(452);
    Expression<Func<int>> e5 = () => 8 / 4;
    AssertNodeType(e5, ExpressionType.Constant);
    Assert(2, e5.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(453);
  }
  void ConstantTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(454);
    Expression<Func<int>> e6 = () => 0xffffff >> 0x40;
    AssertNodeType(e6, ExpressionType.Constant);
    Assert(0xffffff, e6.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(455);
  }
  void ConstantTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(456);
    Expression<Func<object>> e7 = () => "Alleluia";
    AssertNodeType(e7, ExpressionType.Constant);
    Assert("Alleluia", e7.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(457);
  }
  void ConstantTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(458);
    Expression<Func<Type>> e8 = () => typeof(int);
    AssertNodeType(e8, ExpressionType.Constant);
    Assert(typeof(int), e8.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(459);
  }
  void ConstantTest_9()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(460);
    Expression<Func<Type>> e9 = () => typeof(void);
    AssertNodeType(e9, ExpressionType.Constant);
    Assert(typeof(void), e9.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(461);
  }
  void ConstantTest_10()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(462);
    Expression<Func<Type>> e10 = () => typeof(Func<, >);
    AssertNodeType(e10, ExpressionType.Constant);
    Assert(typeof(Func<, >), e10.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(463);
  }
  void ConstantTest_11()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(464);
    Expression<Func<MyEnum>> e11 = () => MyEnum.Value_2;
    AssertNodeType(e11, ExpressionType.Constant);
    Assert(MyEnum.Value_2, e11.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(465);
  }
  void ConstantTest_12()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(466);
    Expression<Func<MyEnum>> e12 = () => new MyEnum();
    AssertNodeType(e12, ExpressionType.Constant);
    Assert<MyEnum>(0, e12.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(467);
  }
  void ConstantTest_13()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(468);
    Expression<Func<int>> e13 = () => sizeof(byte);
    AssertNodeType(e13, ExpressionType.Constant);
    Assert(1, e13.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(469);
  }
  unsafe void ConstantTest_14()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(470);
    Expression<Func<Type>> e14 = () => typeof(bool*);
    AssertNodeType(e14, ExpressionType.Constant);
    Assert(typeof(bool*), e14.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(471);
  }
  void ConstantTest_15()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(472);
    Expression<Func<int?>> e15 = () => null;
    AssertNodeType(e15, ExpressionType.Constant);
    Assert(null, e15.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(473);
  }
  void ConvertTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(474);
    Expression<Func<int, byte>> e = (int a) => ((byte)a);
    AssertNodeType(e, ExpressionType.Convert);
    Assert(100, e.Compile().Invoke(100));
    IACSharpSensor.IACSharpSensor.SensorReached(475);
  }
  void ConvertTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(476);
    Expression<Func<long, ushort>> e2 = (long a) => ((ushort)a);
    AssertNodeType(e2, ExpressionType.Convert);
    Assert(100, e2.Compile().Invoke(100));
    IACSharpSensor.IACSharpSensor.SensorReached(477);
  }
  void ConvertTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(478);
    Expression<Func<float?, float>> e3 = (float? a) => ((float)a);
    AssertNodeType(e3, ExpressionType.Convert);
    Assert(-0.456f, e3.Compile().Invoke(-0.456f));
    IACSharpSensor.IACSharpSensor.SensorReached(479);
  }
  void ConvertTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(480);
    Expression<Func<MyType, int>> e4 = (MyType a) => (a);
    AssertNodeType(e4, ExpressionType.Convert);
    Assert(-9, e4.Compile().Invoke(new MyType(-9)));
    IACSharpSensor.IACSharpSensor.SensorReached(481);
  }
  void ConvertTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(482);
    Expression<Func<MyType, MyType, bool?>> e5 = (MyType a, MyType b) => a == b;
    AssertNodeType(e5, ExpressionType.Convert);
    IACSharpSensor.IACSharpSensor.SensorReached(483);
  }
  void ConvertTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(484);
    Expression<Func<MyType?, MyType?, bool?>> e6 = (MyType? a, MyType? b) => a == b;
    AssertNodeType(e6, ExpressionType.Convert);
    Assert(false, e6.Compile().Invoke(null, new MyType(-20)));
    Assert(true, e6.Compile().Invoke(null, null));
    Assert(true, e6.Compile().Invoke(new MyType(120), new MyType(120)));
    IACSharpSensor.IACSharpSensor.SensorReached(485);
  }
  void ConvertTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(486);
    Expression<Func<MyTypeExplicit, int?>> e7 = x => (int?)x;
    AssertNodeType(e7, ExpressionType.Convert);
    Assert(33, e7.Compile().Invoke(new MyTypeExplicit(33)));
    IACSharpSensor.IACSharpSensor.SensorReached(487);
  }
  void ConvertTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(488);
    Expression<Func<int?, object>> e8 = x => (object)x;
    AssertNodeType(e8, ExpressionType.Convert);
    Assert(null, e8.Compile().Invoke(null));
    Assert(-100, e8.Compile().Invoke(-100));
    IACSharpSensor.IACSharpSensor.SensorReached(489);
  }
  unsafe void ConvertTest_9()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(490);
    int*[] p = new int*[1];
    Expression<Func<object>> e9 = () => (object)p;
    AssertNodeType(e9, ExpressionType.Convert);
    Assert(p, e9.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(491);
  }
  void ConvertTest_10()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(492);
    Expression<Func<Func<int>, Delegate>> e10 = a => a + a;
    AssertNodeType(e10, ExpressionType.Convert);
    Assert(null, e10.Compile().Invoke(null));
    Assert(new Func<int>(TestInt) + new Func<int>(TestInt), e10.Compile().Invoke(TestInt));
    IACSharpSensor.IACSharpSensor.SensorReached(493);
  }
  void ConvertTest_11()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(494);
    Expression<Func<Func<int>, Delegate>> e11 = a => a - a;
    AssertNodeType(e11, ExpressionType.Convert);
    Assert(null, e11.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(495);
  }
  void ConvertTest_12()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(496);
    Expression<Func<Func<int>>> e12 = () => TestInt;
    AssertNodeType(e12, ExpressionType.Convert);
    Assert(29, e12.Compile().Invoke()());
    IACSharpSensor.IACSharpSensor.SensorReached(497);
  }
  void ConvertTest_13()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(498);
    Expression<Func<decimal, sbyte>> e13 = a => (sbyte)a;
    AssertNodeType(e13, ExpressionType.Convert);
    Assert(6, e13.Compile().Invoke(6));
    IACSharpSensor.IACSharpSensor.SensorReached(499);
  }
  void ConvertTest_14()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(500);
    Expression<Func<long, decimal>> e14 = a => a;
    AssertNodeType(e14, ExpressionType.Convert);
    Assert(-66, e14.Compile().Invoke(-66));
    IACSharpSensor.IACSharpSensor.SensorReached(501);
  }
  void ConvertTest_15()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(502);
    Expression<Func<ulong?, decimal?>> e15 = a => a;
    AssertNodeType(e15, ExpressionType.Convert);
    Assert(null, e15.Compile().Invoke(null));
    Assert(9, e15.Compile().Invoke(9));
    IACSharpSensor.IACSharpSensor.SensorReached(503);
  }
  void ConvertCheckedTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(504);
    Expression<Func<int, byte>> e = (int a) => checked((byte)a);
    AssertNodeType(e, ExpressionType.ConvertChecked);
    Assert(100, e.Compile().Invoke(100));
    IACSharpSensor.IACSharpSensor.SensorReached(505);
  }
  void ConvertCheckedTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(506);
    checked {
      Expression<Func<long, ushort>> e2 = (long a) => unchecked((ushort)a);
      AssertNodeType(e2, ExpressionType.Convert);
      Assert(100, e2.Compile().Invoke(100));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(507);
  }
  void ConvertCheckedTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(508);
    checked {
      Expression<Func<float?, float>> e3 = (float? a) => ((float)a);
      AssertNodeType(e3, ExpressionType.ConvertChecked);
      Assert(-0.456f, e3.Compile().Invoke(-0.456f));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(509);
  }
  void ConvertCheckedTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(510);
    checked {
      Expression<Func<MyType, int>> e4 = (MyType a) => (a);
      AssertNodeType(e4, ExpressionType.Convert);
      Assert(-9, e4.Compile().Invoke(new MyType(-9)));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(511);
  }
  void DivideTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(512);
    Expression<Func<int, int, int>> e = (int a, int b) => a / b;
    AssertNodeType(e, ExpressionType.Divide);
    Assert(2, e.Compile().Invoke(60, 30));
    IACSharpSensor.IACSharpSensor.SensorReached(513);
  }
  void DivideTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(514);
    Expression<Func<double?, double?, double?>> e2 = (a, b) => a / b;
    AssertNodeType(e2, ExpressionType.Divide);
    Assert(null, e2.Compile().Invoke(null, 3));
    Assert(1.5, e2.Compile().Invoke(3, 2));
    IACSharpSensor.IACSharpSensor.SensorReached(515);
  }
  void DivideTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(516);
    Expression<Func<MyType, MyType, MyType>> e3 = (MyType a, MyType b) => a / b;
    AssertNodeType(e3, ExpressionType.Divide);
    Assert(1, e3.Compile().Invoke(new MyType(-20), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(517);
  }
  void DivideTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(518);
    Expression<Func<MyType?, MyType?, MyType?>> e4 = (MyType? a, MyType? b) => a / b;
    AssertNodeType(e4, ExpressionType.Divide);
    Assert(null, e4.Compile().Invoke(null, new MyType(-20)));
    Assert(new MyType(-6), e4.Compile().Invoke(new MyType(120), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(519);
  }
  void DivideTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(520);
    Expression<Func<int, MyType, int>> e5 = (int a, MyType b) => a / b;
    AssertNodeType(e5, ExpressionType.Divide);
    Assert(50, e5.Compile().Invoke(100, new MyType(2)));
    IACSharpSensor.IACSharpSensor.SensorReached(521);
  }
  void DivideTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(522);
    Expression<Func<int, MyType?, int?>> e6 = (int a, MyType? b) => a / b;
    AssertNodeType(e6, ExpressionType.Divide);
    Assert(50, e6.Compile().Invoke(100, new MyType(2)));
    Assert(null, e6.Compile().Invoke(20, null));
    IACSharpSensor.IACSharpSensor.SensorReached(523);
  }
  void EqualTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(524);
    Expression<Func<int, int, bool>> e = (int a, int b) => a == b;
    AssertNodeType(e, ExpressionType.Equal);
    Assert(false, e.Compile().Invoke(60, 30));
    Assert(true, e.Compile().Invoke(-1, -1));
    IACSharpSensor.IACSharpSensor.SensorReached(525);
  }
  void EqualTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(526);
    Expression<Func<double?, double?, bool>> e2 = (a, b) => a == b;
    AssertNodeType(e2, ExpressionType.Equal);
    Assert(true, e2.Compile().Invoke(3, 3));
    Assert(false, e2.Compile().Invoke(3, 2));
    IACSharpSensor.IACSharpSensor.SensorReached(527);
  }
  void EqualTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(528);
    Expression<Func<MyType, MyType, bool>> e3 = (MyType a, MyType b) => a == b;
    AssertNodeType(e3, ExpressionType.Equal);
    Assert(true, e3.Compile().Invoke(new MyType(-20), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(529);
  }
  void EqualTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(530);
    Expression<Func<MyType?, MyType?, bool>> e4 = (MyType? a, MyType? b) => a == b;
    AssertNodeType(e4, ExpressionType.Equal);
    Assert(false, e4.Compile().Invoke(null, new MyType(-20)));
    Assert(true, e4.Compile().Invoke(null, null));
    Assert(true, e4.Compile().Invoke(new MyType(120), new MyType(120)));
    IACSharpSensor.IACSharpSensor.SensorReached(531);
  }
  void EqualTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(532);
    Expression<Func<bool?, bool?, bool>> e5 = (bool? a, bool? b) => a == b;
    AssertNodeType(e5, ExpressionType.Equal);
    Assert(false, e5.Compile().Invoke(true, null));
    Assert(true, e5.Compile().Invoke(null, null));
    Assert(true, e5.Compile().Invoke(false, false));
    IACSharpSensor.IACSharpSensor.SensorReached(533);
  }
  void EqualTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(534);
    Expression<Func<bool, bool>> e6 = (bool a) => a == null;
    AssertNodeType(e6, ExpressionType.Equal);
    Assert(false, e6.Compile().Invoke(true));
    Assert(false, e6.Compile().Invoke(false));
    IACSharpSensor.IACSharpSensor.SensorReached(535);
  }
  void EqualTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(536);
    Expression<Func<string, string, bool>> e7 = (string a, string b) => a == b;
    AssertNodeType(e7, ExpressionType.Equal);
    Assert(true, e7.Compile().Invoke(null, null));
    Assert(false, e7.Compile().Invoke("a", "A"));
    Assert(true, e7.Compile().Invoke("a", "a"));
    IACSharpSensor.IACSharpSensor.SensorReached(537);
  }
  void EqualTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(538);
    Expression<Func<object, bool>> e8 = (object a) => null == a;
    AssertNodeType(e8, ExpressionType.Equal);
    Assert(true, e8.Compile().Invoke(null));
    Assert(false, e8.Compile().Invoke("a"));
    Assert(false, e8.Compile().Invoke(this));
    IACSharpSensor.IACSharpSensor.SensorReached(539);
  }
  void EqualTest_9()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(540);
    Expression<Func<MyEnum, MyEnum, bool>> e9 = (a, b) => a == b;
    AssertNodeType(e9, ExpressionType.Equal);
    Assert(false, e9.Compile().Invoke(MyEnum.Value_1, MyEnum.Value_2));
    Assert(true, e9.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(541);
  }
  void EqualTest_10()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(542);
    Expression<Func<MyEnum?, MyEnum?, bool>> e10 = (a, b) => a == b;
    AssertNodeType(e10, ExpressionType.Equal);
    Assert(false, e10.Compile().Invoke(MyEnum.Value_1, null));
    Assert(true, e10.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(543);
  }
  void EqualTest_11()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(544);
    Expression<Func<MyEnum?, bool>> e11 = a => a == null;
    AssertNodeType(e11, ExpressionType.Equal);
    Assert(false, e11.Compile().Invoke(MyEnum.Value_1));
    Assert(true, e11.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(545);
  }
  void EqualTest_12()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(546);
    Expression<Func<MyEnumUlong, bool>> e12 = a => a == 0;
    AssertNodeType(e12, ExpressionType.Equal);
    Assert(false, e12.Compile().Invoke(MyEnumUlong.Value_1));
    Assert(true, e12.Compile().Invoke(0));
    IACSharpSensor.IACSharpSensor.SensorReached(547);
  }
  void EqualTest_13()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(548);
    Expression<Func<MyEnum, bool>> e13 = a => a == MyEnum.Value_2;
    AssertNodeType(e13, ExpressionType.Equal);
    Assert(true, e13.Compile().Invoke(MyEnum.Value_2));
    Assert(false, e13.Compile().Invoke(0));
    IACSharpSensor.IACSharpSensor.SensorReached(549);
  }
  void EqualTestDelegate()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(550);
    Expression<Func<Delegate, Delegate, bool>> e1 = (a, b) => a == b;
    AssertNodeType(e1, ExpressionType.Equal);
    Assert(true, e1.Compile().Invoke(null, null));
    IACSharpSensor.IACSharpSensor.SensorReached(551);
  }
  void EqualTestDelegate_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(552);
    EmptyDelegate ed = delegate() { };
    Expression<Func<EmptyDelegate, EmptyDelegate, bool>> e2 = (a, b) => a == b;
    AssertNodeType(e2, ExpressionType.Equal);
    Assert(false, e2.Compile().Invoke(delegate() { }, null));
    Assert(false, e2.Compile().Invoke(delegate() { }, delegate { }));
    Assert(false, e2.Compile().Invoke(ed, delegate { }));
    Assert(true, e2.Compile().Invoke(ed, ed));
    IACSharpSensor.IACSharpSensor.SensorReached(553);
  }
  void ExclusiveOrTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(554);
    Expression<Func<int, short, int>> e = (int a, short b) => a ^ b;
    AssertNodeType(e, ExpressionType.ExclusiveOr);
    Assert(34, e.Compile().Invoke(60, 30));
    IACSharpSensor.IACSharpSensor.SensorReached(555);
  }
  void ExclusiveOrTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(556);
    Expression<Func<byte?, byte?, int?>> e2 = (a, b) => a ^ b;
    AssertNodeType(e2, ExpressionType.ExclusiveOr);
    Assert(null, e2.Compile().Invoke(null, 3));
    Assert(1, e2.Compile().Invoke(3, 2));
    IACSharpSensor.IACSharpSensor.SensorReached(557);
  }
  void ExclusiveOrTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(558);
    Expression<Func<MyType, MyType, MyType>> e3 = (MyType a, MyType b) => a ^ b;
    AssertNodeType(e3, ExpressionType.ExclusiveOr);
    Assert(0, e3.Compile().Invoke(new MyType(-20), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(559);
  }
  void ExclusiveOrTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(560);
    Expression<Func<MyType?, MyType?, MyType?>> e4 = (MyType? a, MyType? b) => a ^ b;
    AssertNodeType(e4, ExpressionType.ExclusiveOr);
    Assert(null, e4.Compile().Invoke(null, new MyType(-20)));
    Assert(new MyType(-108), e4.Compile().Invoke(new MyType(120), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(561);
  }
  void ExclusiveOrTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(562);
    Expression<Func<MyType?, byte, int?>> e5 = (MyType? a, byte b) => a ^ b;
    AssertNodeType(e5, ExpressionType.ExclusiveOr);
    Assert(null, e5.Compile().Invoke(null, 64));
    Assert(96, e5.Compile().Invoke(new MyType(64), 32));
    IACSharpSensor.IACSharpSensor.SensorReached(563);
  }
  void ExclusiveOrTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(564);
    Expression<Func<MyEnum, MyEnum, MyEnum>> e6 = (a, b) => a ^ b;
    AssertNodeType(e6, ExpressionType.Convert);
    Assert((MyEnum)3, e6.Compile().Invoke(MyEnum.Value_1, MyEnum.Value_2));
    Assert<MyEnum>(0, e6.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(565);
  }
  void ExclusiveOrTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(566);
    Expression<Func<MyEnum?, MyEnum?, MyEnum?>> e7 = (a, b) => a ^ b;
    AssertNodeType(e7, ExpressionType.Convert);
    Assert(null, e7.Compile().Invoke(MyEnum.Value_1, null));
    Assert<MyEnum?>(0, e7.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(567);
  }
  void ExclusiveOrTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(568);
    Expression<Func<MyEnum?, MyEnum?>> e8 = a => a ^ null;
    AssertNodeType(e8, ExpressionType.Convert);
    Assert(null, e8.Compile().Invoke(MyEnum.Value_1));
    Assert(null, e8.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(569);
  }
  void GreaterThanTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(570);
    Expression<Func<int, int, bool>> e = (int a, int b) => a > b;
    AssertNodeType(e, ExpressionType.GreaterThan);
    Assert(true, e.Compile().Invoke(60, 30));
    IACSharpSensor.IACSharpSensor.SensorReached(571);
  }
  void GreaterThanTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(572);
    Expression<Func<uint?, byte?, bool>> e2 = (a, b) => a > b;
    AssertNodeType(e2, ExpressionType.GreaterThan);
    Assert(false, e2.Compile().Invoke(null, 3));
    Assert(false, e2.Compile().Invoke(2, 2));
    IACSharpSensor.IACSharpSensor.SensorReached(573);
  }
  void GreaterThanTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(574);
    Expression<Func<MyType, MyType, bool>> e3 = (MyType a, MyType b) => a > b;
    AssertNodeType(e3, ExpressionType.GreaterThan);
    Assert(false, e3.Compile().Invoke(new MyType(-20), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(575);
  }
  void GreaterThanTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(576);
    Expression<Func<MyType?, MyType?, bool>> e4 = (MyType? a, MyType? b) => a > b;
    AssertNodeType(e4, ExpressionType.GreaterThan);
    Assert(false, e4.Compile().Invoke(null, new MyType(-20)));
    Assert(false, e4.Compile().Invoke(null, null));
    Assert(true, e4.Compile().Invoke(new MyType(120), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(577);
  }
  void GreaterThanTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(578);
    Expression<Func<MyType?, sbyte, bool>> e5 = (MyType? a, sbyte b) => a > b;
    AssertNodeType(e5, ExpressionType.GreaterThan);
    Assert(false, e5.Compile().Invoke(null, 33));
    Assert(false, e5.Compile().Invoke(null, 0));
    Assert(true, e5.Compile().Invoke(new MyType(120), 3));
    IACSharpSensor.IACSharpSensor.SensorReached(579);
  }
  void GreaterThanTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(580);
    Expression<Func<ushort, bool>> e6 = (ushort a) => a > null;
    AssertNodeType(e6, ExpressionType.GreaterThan);
    Assert(false, e6.Compile().Invoke(60));
    IACSharpSensor.IACSharpSensor.SensorReached(581);
  }
  void GreaterThanTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(582);
    Expression<Func<MyEnum, MyEnum, bool>> e7 = (a, b) => a > b;
    AssertNodeType(e7, ExpressionType.GreaterThan);
    Assert(true, e7.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_1));
    Assert(false, e7.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(583);
  }
  void GreaterThanTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(584);
    Expression<Func<MyEnum?, MyEnum?, bool>> e8 = (a, b) => a > b;
    AssertNodeType(e8, ExpressionType.GreaterThan);
    Assert(false, e8.Compile().Invoke(MyEnum.Value_1, null));
    Assert(false, e8.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(585);
  }
  void GreaterThanOrEqualTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(586);
    Expression<Func<int, int, bool>> e = (int a, int b) => a >= b;
    AssertNodeType(e, ExpressionType.GreaterThanOrEqual);
    Assert(true, e.Compile().Invoke(60, 30));
    IACSharpSensor.IACSharpSensor.SensorReached(587);
  }
  void GreaterThanOrEqualTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(588);
    Expression<Func<byte?, byte?, bool>> e2 = (a, b) => a >= b;
    AssertNodeType(e2, ExpressionType.GreaterThanOrEqual);
    Assert(false, e2.Compile().Invoke(null, 3));
    Assert(true, e2.Compile().Invoke(2, 2));
    IACSharpSensor.IACSharpSensor.SensorReached(589);
  }
  void GreaterThanOrEqualTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(590);
    Expression<Func<MyType, MyType, bool>> e3 = (MyType a, MyType b) => a >= b;
    AssertNodeType(e3, ExpressionType.GreaterThanOrEqual);
    Assert(true, e3.Compile().Invoke(new MyType(-20), new MyType(-20)), "D1");
    IACSharpSensor.IACSharpSensor.SensorReached(591);
  }
  void GreaterThanOrEqualTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(592);
    Expression<Func<MyType?, MyType?, bool>> e4 = (MyType? a, MyType? b) => a >= b;
    AssertNodeType(e4, ExpressionType.GreaterThanOrEqual);
    Assert(false, e4.Compile().Invoke(null, new MyType(-20)));
    Assert(false, e4.Compile().Invoke(null, null));
    Assert(true, e4.Compile().Invoke(new MyType(120), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(593);
  }
  void GreaterThanOrEqualTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(594);
    Expression<Func<MyType?, sbyte, bool>> e5 = (MyType? a, sbyte b) => a >= b;
    AssertNodeType(e5, ExpressionType.GreaterThanOrEqual);
    Assert(false, e5.Compile().Invoke(null, 33));
    Assert(false, e5.Compile().Invoke(null, 0));
    Assert(true, e5.Compile().Invoke(new MyType(120), 3));
    IACSharpSensor.IACSharpSensor.SensorReached(595);
  }
  void GreaterThanOrEqualTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(596);
    Expression<Func<ushort, bool>> e6 = (ushort a) => a >= null;
    AssertNodeType(e6, ExpressionType.GreaterThanOrEqual);
    Assert(false, e6.Compile().Invoke(60));
    IACSharpSensor.IACSharpSensor.SensorReached(597);
  }
  void GreaterThanOrEqualTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(598);
    Expression<Func<MyEnum, MyEnum, bool>> e7 = (a, b) => a >= b;
    AssertNodeType(e7, ExpressionType.GreaterThanOrEqual);
    Assert(true, e7.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_1));
    Assert(true, e7.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(599);
  }
  void GreaterThanOrEqualTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(600);
    Expression<Func<MyEnum?, MyEnum?, bool>> e8 = (a, b) => a >= b;
    AssertNodeType(e8, ExpressionType.GreaterThanOrEqual);
    Assert(false, e8.Compile().Invoke(MyEnum.Value_1, null));
    Assert(true, e8.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(601);
  }
  void InvokeTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(602);
    var del = new IntDelegate(TestInt);
    Expression<Func<IntDelegate, int>> e = a => a();
    AssertNodeType(e, ExpressionType.Invoke);
    Assert(29, e.Compile().Invoke(del));
    IACSharpSensor.IACSharpSensor.SensorReached(603);
  }
  void InvokeTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(604);
    Expression<Func<Func<int, string>, int, string>> e2 = (a, b) => a(b);
    AssertNodeType(e2, ExpressionType.Invoke);
    Assert("4", e2.Compile().Invoke(a => (a + 1).ToString(), 3));
    IACSharpSensor.IACSharpSensor.SensorReached(605);
  }
  void LeftShiftTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(606);
    Expression<Func<ulong, short, ulong>> e = (ulong a, short b) => a << b;
    AssertNodeType(e, ExpressionType.LeftShift);
    Assert((ulong)0x7f000, e.Compile().Invoke(0xfe, 11));
    Assert((ulong)0x1fffffffeL, e.Compile().Invoke(0xffffffffu, 0xa01));
    IACSharpSensor.IACSharpSensor.SensorReached(607);
  }
  void LeftShiftTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(608);
    Expression<Func<MyType, MyType, int>> e2 = (MyType a, MyType b) => a << b;
    AssertNodeType(e2, ExpressionType.LeftShift);
    var c2 = e2.Compile();
    Assert(1024, c2(new MyType(256), new MyType(2)));
    IACSharpSensor.IACSharpSensor.SensorReached(609);
  }
  void LeftShiftTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(610);
    Expression<Func<long?, sbyte, long?>> e3 = (long? a, sbyte b) => a << b;
    AssertNodeType(e3, ExpressionType.LeftShift);
    Assert(null, e3.Compile().Invoke(null, 11));
    Assert(2048, e3.Compile().Invoke(1024, 1));
    IACSharpSensor.IACSharpSensor.SensorReached(611);
  }
  void LeftShiftTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(612);
    Expression<Func<MyType?, MyType?, int?>> e4 = (MyType? a, MyType? b) => a << b;
    AssertNodeType(e4, ExpressionType.LeftShift);
    var c4 = e4.Compile();
    Assert(null, c4(new MyType(8), null));
    Assert(null, c4(null, new MyType(8)));
    Assert(1024, c4(new MyType(256), new MyType(2)));
    IACSharpSensor.IACSharpSensor.SensorReached(613);
  }
  void LeftShiftTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(614);
    Expression<Func<ushort, int?>> e5 = (ushort a) => a << null;
    AssertNodeType(e5, ExpressionType.LeftShift);
    Assert(null, e5.Compile().Invoke(30));
    IACSharpSensor.IACSharpSensor.SensorReached(615);
  }
  void LessThanTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(616);
    Expression<Func<int, int, bool>> e = (int a, int b) => a < b;
    AssertNodeType(e, ExpressionType.LessThan);
    Assert(false, e.Compile().Invoke(60, 30));
    IACSharpSensor.IACSharpSensor.SensorReached(617);
  }
  void LessThanTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(618);
    Expression<Func<uint?, byte?, bool>> e2 = (a, b) => a < b;
    AssertNodeType(e2, ExpressionType.LessThan);
    Assert(false, e2.Compile().Invoke(null, 3));
    Assert(false, e2.Compile().Invoke(2, 2));
    IACSharpSensor.IACSharpSensor.SensorReached(619);
  }
  void LessThanTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(620);
    Expression<Func<MyType, MyType, bool>> e3 = (MyType a, MyType b) => a < b;
    AssertNodeType(e3, ExpressionType.LessThan);
    Assert(false, e3.Compile().Invoke(new MyType(-20), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(621);
  }
  void LessThanTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(622);
    Expression<Func<MyType?, MyType?, bool>> e4 = (MyType? a, MyType? b) => a < b;
    AssertNodeType(e4, ExpressionType.LessThan);
    Assert(false, e4.Compile().Invoke(null, new MyType(-20)));
    Assert(false, e4.Compile().Invoke(null, null));
    Assert(false, e4.Compile().Invoke(new MyType(120), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(623);
  }
  void LessThanTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(624);
    Expression<Func<MyType?, sbyte, bool>> e5 = (MyType? a, sbyte b) => a < b;
    AssertNodeType(e5, ExpressionType.LessThan);
    Assert(false, e5.Compile().Invoke(null, 33));
    Assert(false, e5.Compile().Invoke(null, 0));
    Assert(false, e5.Compile().Invoke(new MyType(120), 3));
    IACSharpSensor.IACSharpSensor.SensorReached(625);
  }
  void LessThanTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(626);
    Expression<Func<ushort, bool>> e6 = (ushort a) => a < null;
    AssertNodeType(e6, ExpressionType.LessThan);
    Assert(false, e6.Compile().Invoke(60));
    IACSharpSensor.IACSharpSensor.SensorReached(627);
  }
  void LessThanTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(628);
    Expression<Func<MyEnum, MyEnum, bool>> e7 = (a, b) => a < b;
    AssertNodeType(e7, ExpressionType.LessThan);
    Assert(false, e7.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_1));
    Assert(false, e7.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(629);
  }
  void LessThanTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(630);
    Expression<Func<MyEnum?, MyEnum?, bool>> e8 = (a, b) => a < b;
    AssertNodeType(e8, ExpressionType.LessThan);
    Assert(false, e8.Compile().Invoke(MyEnum.Value_1, null));
    Assert(false, e8.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(631);
  }
  void LessThanOrEqualTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(632);
    Expression<Func<int, int, bool>> e = (int a, int b) => a <= b;
    AssertNodeType(e, ExpressionType.LessThanOrEqual);
    Assert(false, e.Compile().Invoke(60, 30));
    IACSharpSensor.IACSharpSensor.SensorReached(633);
  }
  void LessThanOrEqualTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(634);
    Expression<Func<byte?, byte?, bool>> e2 = (a, b) => a <= b;
    AssertNodeType(e2, ExpressionType.LessThanOrEqual);
    Assert(false, e2.Compile().Invoke(null, 3));
    Assert(true, e2.Compile().Invoke(2, 2));
    IACSharpSensor.IACSharpSensor.SensorReached(635);
  }
  void LessThanOrEqualTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(636);
    Expression<Func<MyType, MyType, bool>> e3 = (MyType a, MyType b) => a <= b;
    AssertNodeType(e3, ExpressionType.LessThanOrEqual);
    Assert(true, e3.Compile().Invoke(new MyType(-20), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(637);
  }
  void LessThanOrEqualTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(638);
    Expression<Func<MyType?, MyType?, bool>> e4 = (MyType? a, MyType? b) => a <= b;
    AssertNodeType(e4, ExpressionType.LessThanOrEqual);
    Assert(false, e4.Compile().Invoke(null, new MyType(-20)));
    Assert(false, e4.Compile().Invoke(null, null));
    Assert(false, e4.Compile().Invoke(new MyType(120), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(639);
  }
  void LessThanOrEqualTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(640);
    Expression<Func<MyType?, sbyte, bool>> e5 = (MyType? a, sbyte b) => a <= b;
    AssertNodeType(e5, ExpressionType.LessThanOrEqual);
    Assert(false, e5.Compile().Invoke(null, 33));
    Assert(false, e5.Compile().Invoke(null, 0));
    Assert(false, e5.Compile().Invoke(new MyType(120), 3));
    IACSharpSensor.IACSharpSensor.SensorReached(641);
  }
  void LessThanOrEqualTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(642);
    Expression<Func<ushort, bool>> e6 = (ushort a) => a <= null;
    AssertNodeType(e6, ExpressionType.LessThanOrEqual);
    Assert(false, e6.Compile().Invoke(60));
    IACSharpSensor.IACSharpSensor.SensorReached(643);
  }
  void LessThanOrEqualTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(644);
    Expression<Func<MyEnum, MyEnum, bool>> e7 = (a, b) => a <= b;
    AssertNodeType(e7, ExpressionType.LessThanOrEqual);
    Assert(false, e7.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_1));
    Assert(true, e7.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(645);
  }
  void LessThanOrEqualTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(646);
    Expression<Func<MyEnum?, MyEnum?, bool>> e8 = (a, b) => a <= b;
    AssertNodeType(e8, ExpressionType.LessThanOrEqual);
    Assert(false, e8.Compile().Invoke(MyEnum.Value_1, null));
    Assert(true, e8.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(647);
  }
  void ListInitTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(648);
    Expression<Func<List<object>>> e1 = () => new List<object> {
      "Hello",
      "",
      null,
      "World",
      5
    };
    AssertNodeType(e1, ExpressionType.ListInit);
    var re1 = e1.Compile().Invoke();
    Assert(null, re1[2]);
    Assert("World", re1[3]);
    Assert(5, re1[4]);
    IACSharpSensor.IACSharpSensor.SensorReached(649);
  }
  void ListInitTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(650);
    Expression<Func<int, Dictionary<string, int>>> e2 = (int value) => new Dictionary<string, int>(3) {
      {
        "A",
        value
      },
      {
        "B",
        2
      }
    };
    AssertNodeType(e2, ExpressionType.ListInit);
    var re2 = e2.Compile().Invoke(3456);
    Assert(3456, re2["A"]);
    IACSharpSensor.IACSharpSensor.SensorReached(651);
  }
  void MemberAccessTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(652);
    MemberAccessData d = new MemberAccessData();
    d.BoolValue = true;
    Expression<Func<bool>> e = () => d.BoolValue;
    AssertNodeType(e, ExpressionType.MemberAccess);
    Assert(true, e.Compile().Invoke());
    d.BoolValue = false;
    Assert(false, e.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(653);
  }
  void MemberAccessTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(654);
    Expression<Func<decimal>> e2 = () => MemberAccessData.DecimalValue;
    AssertNodeType(e2, ExpressionType.MemberAccess);
    Assert(decimal.MinValue, e2.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(655);
  }
  void MemberAccessTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(656);
    MemberAccessData d = new MemberAccessData();
    d.VolatileValue = 492;
    Expression<Func<uint>> e3 = () => d.VolatileValue;
    AssertNodeType(e3, ExpressionType.MemberAccess);
    Assert<uint>(492, e3.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(657);
  }
  void MemberAccessTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(658);
    MemberAccessData d = new MemberAccessData();
    Expression<Func<string[]>> e4 = () => d.StringValues;
    AssertNodeType(e4, ExpressionType.MemberAccess);
    Assert(null, e4.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(659);
  }
  void MemberAccessTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(660);
    MemberAccessData d = new MemberAccessData();
    var e5 = d.GetEvent();
    AssertNodeType(e5, ExpressionType.MemberAccess);
    Assert(null, e5.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(661);
  }
  void MemberAccessTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(662);
    MemberAccessData d = new MemberAccessData();
    Expression<Func<MyType>> e6 = () => d.MyTypeProperty;
    AssertNodeType(e6, ExpressionType.MemberAccess);
    Assert(new MyType(), e6.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(663);
  }
  void MemberAccessTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(664);
    MemberAccessData d = new MemberAccessData();
    Expression<Func<MyType, short>> e7 = a => a.ShortProp;
    AssertNodeType(e7, ExpressionType.MemberAccess);
    MyType mt = new MyType();
    mt.ShortProp = 124;
    Assert(124, e7.Compile().Invoke(mt));
    IACSharpSensor.IACSharpSensor.SensorReached(665);
  }
  void MemberAccessTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(666);
    Expression<Func<string>> e8 = () => MemberAccessData.StaticProperty;
    AssertNodeType(e8, ExpressionType.MemberAccess);
    Assert("alo", e8.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(667);
  }
  void MemberAccessTest_9()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(668);
    string s = "localvar";
    Expression<Func<string>> e9 = () => s;
    AssertNodeType(e9, ExpressionType.Constant);
    Assert("localvar", e9.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(669);
  }
  void MemberInitTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(670);
    Expression<Func<MemberAccessData>> e = () => new MemberAccessData {
      VolatileValue = 2,
      StringValues = new string[] { "sv" },
      MyTypeProperty = new MyType(692)
    };
    AssertNodeType(e, ExpressionType.MemberInit);
    var r1 = e.Compile().Invoke();
    Assert<uint>(2, r1.VolatileValue);
    Assert(new string[] { "sv" }, r1.StringValues);
    Assert(new MyType(692), r1.MyTypeProperty);
    IACSharpSensor.IACSharpSensor.SensorReached(671);
  }
  void MemberInitTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(672);
    Expression<Func<MemberAccessData>> e2 = () => new MemberAccessData { ListValues = new List<string> {
      "a",
      null
    } };
    AssertNodeType(e2, ExpressionType.MemberInit);
    var r2 = e2.Compile().Invoke();
    Assert("a", r2.ListValues[0]);
    IACSharpSensor.IACSharpSensor.SensorReached(673);
  }
  void MemberInitTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(674);
    Expression<Func<short, MyType>> e3 = a => new MyType { ShortProp = a };
    AssertNodeType(e3, ExpressionType.MemberInit);
    var r3 = e3.Compile().Invoke(33);
    Assert(33, r3.ShortProp);
    IACSharpSensor.IACSharpSensor.SensorReached(675);
  }
  void ModuloTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(676);
    Expression<Func<int, int, int>> e = (int a, int b) => a % b;
    AssertNodeType(e, ExpressionType.Modulo);
    Assert(29, e.Compile().Invoke(60, 31));
    IACSharpSensor.IACSharpSensor.SensorReached(677);
  }
  void ModuloTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(678);
    Expression<Func<double?, double?, double?>> e2 = (a, b) => a % b;
    AssertNodeType(e2, ExpressionType.Modulo);
    Assert(null, e2.Compile().Invoke(null, 3));
    Assert(1.1, e2.Compile().Invoke(3.1, 2));
    IACSharpSensor.IACSharpSensor.SensorReached(679);
  }
  void ModuloTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(680);
    Expression<Func<MyType, MyType, MyType>> e3 = (MyType a, MyType b) => a % b;
    AssertNodeType(e3, ExpressionType.Modulo);
    Assert(0, e3.Compile().Invoke(new MyType(-20), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(681);
  }
  void ModuloTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(682);
    Expression<Func<MyType?, MyType?, MyType?>> e4 = (MyType? a, MyType? b) => a % b;
    AssertNodeType(e4, ExpressionType.Modulo);
    Assert(null, e4.Compile().Invoke(null, new MyType(-20)));
    Assert(new MyType(12), e4.Compile().Invoke(new MyType(12), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(683);
  }
  void ModuloTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(684);
    Expression<Func<int, MyType, int>> e5 = (int a, MyType b) => a % b;
    AssertNodeType(e5, ExpressionType.Modulo);
    Assert(1, e5.Compile().Invoke(99, new MyType(2)));
    IACSharpSensor.IACSharpSensor.SensorReached(685);
  }
  void ModuloTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(686);
    Expression<Func<int, MyType?, int?>> e6 = (int a, MyType? b) => a % b;
    AssertNodeType(e6, ExpressionType.Modulo);
    Assert(100, e6.Compile().Invoke(100, new MyType(200)));
    Assert(null, e6.Compile().Invoke(20, null));
    IACSharpSensor.IACSharpSensor.SensorReached(687);
  }
  void ModuloTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(688);
    Expression<Func<ushort, int?>> e7 = (ushort a) => a % null;
    AssertNodeType(e7, ExpressionType.Modulo);
    Assert(null, e7.Compile().Invoke(60));
    IACSharpSensor.IACSharpSensor.SensorReached(689);
  }
  void MultiplyTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(690);
    Expression<Func<int, int, int>> e = (int a, int b) => a * b;
    AssertNodeType(e, ExpressionType.Multiply);
    Assert(1860, e.Compile().Invoke(60, 31));
    Assert(2147483617, e.Compile().Invoke(int.MaxValue, 31));
    IACSharpSensor.IACSharpSensor.SensorReached(691);
  }
  void MultiplyTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(692);
    Expression<Func<double?, double?, double?>> e2 = (a, b) => a * b;
    AssertNodeType(e2, ExpressionType.Multiply);
    Assert(null, e2.Compile().Invoke(null, 3));
    Assert(6.2, e2.Compile().Invoke(3.1, 2));
    IACSharpSensor.IACSharpSensor.SensorReached(693);
  }
  void MultiplyTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(694);
    Expression<Func<MyType, MyType, MyType>> e3 = (MyType a, MyType b) => a * b;
    AssertNodeType(e3, ExpressionType.Multiply);
    Assert(400, e3.Compile().Invoke(new MyType(-20), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(695);
  }
  void MultiplyTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(696);
    Expression<Func<MyType?, MyType?, MyType?>> e4 = (MyType? a, MyType? b) => a * b;
    AssertNodeType(e4, ExpressionType.Multiply);
    Assert(null, e4.Compile().Invoke(null, new MyType(-20)));
    Assert(new MyType(-240), e4.Compile().Invoke(new MyType(12), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(697);
  }
  void MultiplyTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(698);
    Expression<Func<int, MyType, int>> e5 = (int a, MyType b) => a * b;
    AssertNodeType(e5, ExpressionType.Multiply);
    Assert(198, e5.Compile().Invoke(99, new MyType(2)));
    IACSharpSensor.IACSharpSensor.SensorReached(699);
  }
  void MultiplyTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(700);
    Expression<Func<int, MyType?, int?>> e6 = (int a, MyType? b) => a * b;
    AssertNodeType(e6, ExpressionType.Multiply);
    Assert(0, e6.Compile().Invoke(int.MinValue, new MyType(200)));
    Assert(null, e6.Compile().Invoke(20, null));
    IACSharpSensor.IACSharpSensor.SensorReached(701);
  }
  void MultiplyTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(702);
    Expression<Func<ushort, int?>> e7 = (ushort a) => a * null;
    AssertNodeType(e7, ExpressionType.Multiply);
    Assert(null, e7.Compile().Invoke(60));
    IACSharpSensor.IACSharpSensor.SensorReached(703);
  }
  void MultiplyCheckedTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(704);
    checked {
      Expression<Func<int, int, int>> e = (int a, int b) => a * b;
      AssertNodeType(e, ExpressionType.MultiplyChecked);
      try {
        e.Compile().Invoke(int.MaxValue, 309);
        throw new ApplicationException("MultiplyCheckedTest #1");
      } catch (OverflowException) {
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(705);
  }
  void MultiplyCheckedTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(706);
    checked {
      Expression<Func<byte?, byte?, int?>> e2 = (a, b) => a * b;
      AssertNodeType(e2, ExpressionType.MultiplyChecked);
      Assert(null, e2.Compile().Invoke(null, 3));
      Assert(14025, e2.Compile().Invoke(byte.MaxValue, 55));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(707);
  }
  void MultiplyCheckedTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(708);
    checked {
      Expression<Func<MyType, MyType, MyType>> e3 = (MyType a, MyType b) => a * b;
      AssertNodeType(e3, ExpressionType.Multiply);
      Assert(-600, e3.Compile().Invoke(new MyType(-20), new MyType(30)));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(709);
  }
  void MultiplyCheckedTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(710);
    checked {
      Expression<Func<double, double, double>> e4 = (a, b) => a * b;
      AssertNodeType(e4, ExpressionType.Multiply);
      Assert(double.PositiveInfinity, e4.Compile().Invoke(double.MaxValue, int.MaxValue));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(711);
  }
  void MultiplyCheckedTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(712);
    checked {
      Expression<Func<float?, float?, float?>> e5 = (a, b) => b * a;
      AssertNodeType(e5, ExpressionType.MultiplyChecked);
      Assert(float.PositiveInfinity, e5.Compile().Invoke(float.Epsilon, float.PositiveInfinity));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(713);
  }
  void NegateTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(714);
    Expression<Func<int, int>> e = a => -a;
    AssertNodeType(e, ExpressionType.Negate);
    Assert(30, e.Compile().Invoke(-30));
    IACSharpSensor.IACSharpSensor.SensorReached(715);
  }
  void NegateTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(716);
    Expression<Func<sbyte, int>> e2 = a => -(-a);
    AssertNodeType(e2, ExpressionType.Negate);
    Assert(-10, e2.Compile().Invoke(-10));
    IACSharpSensor.IACSharpSensor.SensorReached(717);
  }
  void NegateTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(718);
    Expression<Func<long?, long?>> e3 = a => -a;
    AssertNodeType(e3, ExpressionType.Negate);
    Assert(long.MinValue + 1, e3.Compile().Invoke(long.MaxValue));
    Assert(null, e3.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(719);
  }
  void NegateTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(720);
    Expression<Func<MyType, MyType>> e4 = a => -a;
    AssertNodeType(e4, ExpressionType.Negate);
    Assert(new MyType(14), e4.Compile().Invoke(new MyType(-14)));
    IACSharpSensor.IACSharpSensor.SensorReached(721);
  }
  void NegateTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(722);
    Expression<Func<MyType?, MyType?>> e5 = a => -a;
    AssertNodeType(e5, ExpressionType.Negate);
    Assert(new MyType(-33), e5.Compile().Invoke(new MyType(33)));
    Assert(null, e5.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(723);
  }
  void NegateTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(724);
    Expression<Func<MyTypeImplicitOnly, int>> e6 = (MyTypeImplicitOnly a) => -a;
    AssertNodeType(e6, ExpressionType.Negate);
    Assert(-4, e6.Compile().Invoke(new MyTypeImplicitOnly(4)));
    IACSharpSensor.IACSharpSensor.SensorReached(725);
  }
  void NegateTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(726);
    Expression<Func<MyTypeImplicitOnly?, int?>> e7 = (MyTypeImplicitOnly? a) => -a;
    AssertNodeType(e7, ExpressionType.Negate);
    Assert(-46, e7.Compile().Invoke(new MyTypeImplicitOnly(46)));
    IACSharpSensor.IACSharpSensor.SensorReached(727);
  }
  void NegateTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(728);
    Expression<Func<sbyte?, int?>> e8 = a => -a;
    AssertNodeType(e8, ExpressionType.Negate);
    Assert(11, e8.Compile().Invoke(-11));
    IACSharpSensor.IACSharpSensor.SensorReached(729);
  }
  void NegateTest_9()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(730);
    Expression<Func<uint, long>> e9 = a => -a;
    AssertNodeType(e9, ExpressionType.Negate);
    Assert(-2, e9.Compile().Invoke(2));
    IACSharpSensor.IACSharpSensor.SensorReached(731);
  }
  void NegateTestChecked()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(732);
    checked {
      Expression<Func<int, int>> e = (int a) => -a;
      AssertNodeType(e, ExpressionType.NegateChecked);
      try {
        e.Compile().Invoke(int.MinValue);
        throw new ApplicationException("NegateTestChecked #1");
      } catch (OverflowException) {
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(733);
  }
  void NegateTestChecked_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(734);
    checked {
      Expression<Func<byte?, int?>> e2 = a => -a;
      AssertNodeType(e2, ExpressionType.NegateChecked);
      Assert(null, e2.Compile().Invoke(null));
      Assert(-255, e2.Compile().Invoke(byte.MaxValue));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(735);
  }
  void NegateTestChecked_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(736);
    checked {
      Expression<Func<MyType, MyType>> e3 = (MyType a) => -a;
      AssertNodeType(e3, ExpressionType.Negate);
      Assert(20, e3.Compile().Invoke(new MyType(-20)));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(737);
  }
  void NegateTestChecked_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(738);
    checked {
      Expression<Func<double, double>> e4 = a => -a;
      AssertNodeType(e4, ExpressionType.Negate);
      Assert(double.NegativeInfinity, e4.Compile().Invoke(double.PositiveInfinity));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(739);
  }
  void NewArrayInitTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(740);
    Expression<Func<int[]>> e = () => new int[0];
    AssertNodeType(e, ExpressionType.NewArrayInit);
    Assert(new int[0], e.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(741);
  }
  void NewArrayInitTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(742);
    Expression<Func<int[]>> e1 = () => new int[] {
      
    };
    AssertNodeType(e1, ExpressionType.NewArrayInit);
    Assert(new int[0], e1.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(743);
  }
  void NewArrayInitTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(744);
    Expression < Func < ushort;
    AssertNodeType(e2, ExpressionType.NewArrayInit);
    Assert(new ulong?[1] { ushort.MaxValue }, e2.Compile().Invoke(ushort.MaxValue));
    IACSharpSensor.IACSharpSensor.SensorReached(745);
  }
  void NewArrayInitTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(746);
    Expression<Func<char[][]>> e3 = () => new char[][] { new char[] { 'a' } };
    AssertNodeType(e3, ExpressionType.NewArrayInit);
    Assert(new char[] { 'a' }, e3.Compile().Invoke()[0]);
    IACSharpSensor.IACSharpSensor.SensorReached(747);
  }
  void NewArrayBoundsTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(748);
    Expression<Func<int[,]>> e = () => new int[2, 3];
    AssertNodeType(e, ExpressionType.NewArrayBounds);
    Assert(new int[2, 3].Length, e.Compile().Invoke().Length);
    IACSharpSensor.IACSharpSensor.SensorReached(749);
  }
  void NewArrayBoundsTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(750);
    Expression<Func<int[,]>> e2 = () => new int[0, 0];
    AssertNodeType(e2, ExpressionType.NewArrayBounds);
    Assert(new int[0, 0].Length, e2.Compile().Invoke().Length);
    IACSharpSensor.IACSharpSensor.SensorReached(751);
  }
  void NewTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(752);
    Expression<Func<MyType>> e = () => new MyType(2);
    AssertNodeType(e, ExpressionType.New);
    Assert(new MyType(2), e.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(753);
  }
  void NewTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(754);
    Expression<Func<MyType>> e2 = () => new MyType();
    AssertNodeType(e2, ExpressionType.New);
    Assert(new MyType(), e2.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(755);
  }
  void NewTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(756);
    Expression<Func<NewTest<bool>>> e3 = () => new NewTest<bool>(true);
    AssertNodeType(e3, ExpressionType.New);
    Assert(new NewTest<bool>(true), e3.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(757);
  }
  void NewTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(758);
    Expression<Func<decimal, NewTest<decimal>>> e4 = (decimal d) => new NewTest<decimal>(1, 5, d);
    AssertNodeType(e4, ExpressionType.New);
    Assert(new NewTest<decimal>(1, 5, -9), e4.Compile().Invoke(-9));
    IACSharpSensor.IACSharpSensor.SensorReached(759);
  }
  void NewTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(760);
    Expression<Func<object>> e5 = () => new {
      A = 9,
      Value = "a"
    };
    AssertNodeType(e5, ExpressionType.New);
    Assert(new {
      A = 9,
      Value = "a"
    }, e5.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(761);
  }
  void NotTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(762);
    Expression<Func<bool, bool>> e = (bool a) => !a;
    AssertNodeType(e, ExpressionType.Not);
    Assert(false, e.Compile().Invoke(true));
    IACSharpSensor.IACSharpSensor.SensorReached(763);
  }
  void NotTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(764);
    Expression<Func<MyType, bool>> e2 = (MyType a) => !a;
    AssertNodeType(e2, ExpressionType.Not);
    Assert(true, e2.Compile().Invoke(new MyType(1)));
    Assert(false, e2.Compile().Invoke(new MyType(-1)));
    IACSharpSensor.IACSharpSensor.SensorReached(765);
  }
  void NotTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(766);
    Expression<Func<int, int>> e3 = (int a) => ~a;
    AssertNodeType(e3, ExpressionType.Not);
    Assert(-8, e3.Compile().Invoke(7));
    IACSharpSensor.IACSharpSensor.SensorReached(767);
  }
  void NotTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(768);
    Expression<Func<MyType, int>> e4 = (MyType a) => ~a;
    AssertNodeType(e4, ExpressionType.Not);
    Assert(0, e4.Compile().Invoke(new MyType(-1)));
    IACSharpSensor.IACSharpSensor.SensorReached(769);
  }
  void NotTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(770);
    Expression<Func<ulong, ulong>> e5 = (ulong a) => ~a;
    AssertNodeType(e5, ExpressionType.Not);
    Assert<ulong>(18446744073709551608uL, e5.Compile().Invoke(7));
    IACSharpSensor.IACSharpSensor.SensorReached(771);
  }
  void NotTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(772);
    Expression<Func<MyEnum, MyEnum>> e6 = (MyEnum a) => ~a;
    AssertNodeType(e6, ExpressionType.Convert);
    Assert((MyEnum)254, e6.Compile().Invoke(MyEnum.Value_1));
    IACSharpSensor.IACSharpSensor.SensorReached(773);
  }
  void NotNullableTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(774);
    Expression<Func<bool?, bool?>> e = (bool? a) => !a;
    AssertNodeType(e, ExpressionType.Not);
    Assert(false, e.Compile().Invoke(true));
    Assert(null, e.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(775);
  }
  void NotNullableTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(776);
    Expression<Func<MyType?, bool?>> e2 = (MyType? a) => !a;
    AssertNodeType(e2, ExpressionType.Not);
    Assert(true, e2.Compile().Invoke(new MyType(1)));
    Assert(null, e2.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(777);
  }
  void NotNullableTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(778);
    Expression<Func<sbyte?, int?>> e3 = (sbyte? a) => ~a;
    AssertNodeType(e3, ExpressionType.Not);
    Assert(-5, e3.Compile().Invoke(4));
    Assert(null, e3.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(779);
  }
  void NotNullableTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(780);
    Expression<Func<MyType?, int?>> e4 = (MyType? a) => ~a;
    AssertNodeType(e4, ExpressionType.Not);
    Assert(0, e4.Compile().Invoke(new MyType(-1)));
    Assert(null, e4.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(781);
  }
  void NotNullableTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(782);
    Expression<Func<MyEnum?, MyEnum?>> e5 = (MyEnum? a) => ~a;
    AssertNodeType(e5, ExpressionType.Convert);
    Assert((MyEnum)254, e5.Compile().Invoke(MyEnum.Value_1));
    Assert(null, e5.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(783);
  }
  void NotEqualTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(784);
    Expression<Func<int, int, bool>> e = (int a, int b) => a != b;
    AssertNodeType(e, ExpressionType.NotEqual);
    Assert(true, e.Compile().Invoke(60, 30));
    Assert(false, e.Compile().Invoke(-1, -1));
    IACSharpSensor.IACSharpSensor.SensorReached(785);
  }
  void NotEqualTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(786);
    Expression<Func<sbyte?, sbyte?, bool>> e2 = (a, b) => a != b;
    AssertNodeType(e2, ExpressionType.NotEqual);
    Assert(false, e2.Compile().Invoke(3, 3));
    Assert(true, e2.Compile().Invoke(3, 2));
    IACSharpSensor.IACSharpSensor.SensorReached(787);
  }
  void NotEqualTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(788);
    Expression<Func<MyType, MyType, bool>> e3 = (MyType a, MyType b) => a != b;
    AssertNodeType(e3, ExpressionType.NotEqual);
    Assert(false, e3.Compile().Invoke(new MyType(-20), new MyType(-20)));
    IACSharpSensor.IACSharpSensor.SensorReached(789);
  }
  void NotEqualTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(790);
    Expression<Func<MyType?, MyType?, bool>> e4 = (MyType? a, MyType? b) => a != b;
    AssertNodeType(e4, ExpressionType.NotEqual);
    Assert(true, e4.Compile().Invoke(null, new MyType(-20)));
    Assert(false, e4.Compile().Invoke(null, null));
    Assert(false, e4.Compile().Invoke(new MyType(120), new MyType(120)));
    IACSharpSensor.IACSharpSensor.SensorReached(791);
  }
  void NotEqualTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(792);
    Expression<Func<bool?, bool?, bool>> e5 = (bool? a, bool? b) => a != b;
    AssertNodeType(e5, ExpressionType.NotEqual);
    Assert(true, e5.Compile().Invoke(true, null));
    Assert(false, e5.Compile().Invoke(null, null));
    Assert(false, e5.Compile().Invoke(false, false));
    IACSharpSensor.IACSharpSensor.SensorReached(793);
  }
  void NotEqualTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(794);
    Expression<Func<bool, bool>> e6 = (bool a) => a != null;
    AssertNodeType(e6, ExpressionType.NotEqual);
    Assert(true, e6.Compile().Invoke(true));
    Assert(true, e6.Compile().Invoke(false));
    IACSharpSensor.IACSharpSensor.SensorReached(795);
  }
  void NotEqualTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(796);
    Expression<Func<string, string, bool>> e7 = (string a, string b) => a != b;
    AssertNodeType(e7, ExpressionType.NotEqual);
    Assert(false, e7.Compile().Invoke(null, null));
    Assert(true, e7.Compile().Invoke("a", "A"));
    Assert(false, e7.Compile().Invoke("a", "a"));
    IACSharpSensor.IACSharpSensor.SensorReached(797);
  }
  void NotEqualTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(798);
    Expression<Func<object, bool>> e8 = (object a) => null != a;
    AssertNodeType(e8, ExpressionType.NotEqual);
    Assert(false, e8.Compile().Invoke(null));
    Assert(true, e8.Compile().Invoke("a"));
    Assert(true, e8.Compile().Invoke(this));
    IACSharpSensor.IACSharpSensor.SensorReached(799);
  }
  void NotEqualTest_9()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(800);
    Expression<Func<MyEnum, MyEnum, bool>> e9 = (a, b) => a != b;
    AssertNodeType(e9, ExpressionType.NotEqual);
    Assert(true, e9.Compile().Invoke(MyEnum.Value_1, MyEnum.Value_2));
    Assert(false, e9.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(801);
  }
  void NotEqualTest_10()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(802);
    Expression<Func<MyEnum?, MyEnum?, bool>> e10 = (a, b) => a != b;
    AssertNodeType(e10, ExpressionType.NotEqual);
    Assert(true, e10.Compile().Invoke(MyEnum.Value_1, null));
    Assert(false, e10.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(803);
  }
  void NotEqualTest_11()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(804);
    Expression<Func<MyEnum?, bool>> e11 = a => a != null;
    AssertNodeType(e11, ExpressionType.NotEqual);
    Assert(true, e11.Compile().Invoke(MyEnum.Value_1));
    Assert(false, e11.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(805);
  }
  void OrTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(806);
    Expression<Func<bool, bool, bool>> e = (bool a, bool b) => a | b;
    AssertNodeType(e, ExpressionType.Or);
    Func<bool, bool, bool> c = e.Compile();
    Assert(true, c(true, true));
    Assert(true, c(true, false));
    Assert(true, c(false, true));
    Assert(false, c(false, false));
    IACSharpSensor.IACSharpSensor.SensorReached(807);
  }
  void OrTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(808);
    Expression<Func<MyType, MyType, MyType>> e2 = (MyType a, MyType b) => a | b;
    AssertNodeType(e2, ExpressionType.Or);
    var c2 = e2.Compile();
    Assert(new MyType(3), c2(new MyType(1), new MyType(2)));
    IACSharpSensor.IACSharpSensor.SensorReached(809);
  }
  void OrTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(810);
    Expression<Func<MyEnum, MyEnum, MyEnum>> e3 = (a, b) => a | b;
    AssertNodeType(e3, ExpressionType.Convert);
    Assert((MyEnum)3, e3.Compile().Invoke(MyEnum.Value_1, MyEnum.Value_2));
    Assert(MyEnum.Value_2, e3.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(811);
  }
  void OrNullableTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(812);
    Expression<Func<bool?, bool?, bool?>> e = (bool? a, bool? b) => a | b;
    AssertNodeType(e, ExpressionType.Or);
    Func<bool?, bool?, bool?> c = e.Compile();
    Assert(true, c(true, true));
    Assert(true, c(true, false));
    Assert(true, c(false, true));
    Assert(false, c(false, false));
    Assert(true, c(true, null));
    Assert(null, c(false, null));
    Assert(null, c(null, false));
    Assert(true, c(true, null));
    Assert(null, c(null, null));
    IACSharpSensor.IACSharpSensor.SensorReached(813);
  }
  void OrNullableTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(814);
    Expression<Func<MyType?, MyType?, MyType?>> e2 = (MyType? a, MyType? b) => a | b;
    AssertNodeType(e2, ExpressionType.Or);
    var c2 = e2.Compile();
    Assert(new MyType(3), c2(new MyType(1), new MyType(2)));
    Assert(null, c2(new MyType(1), null));
    IACSharpSensor.IACSharpSensor.SensorReached(815);
  }
  void OrNullableTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(816);
    Expression<Func<MyType?, uint, long?>> e3 = (MyType? a, uint b) => a | b;
    AssertNodeType(e3, ExpressionType.Or);
    var c3 = e3.Compile();
    Assert(9, c3(new MyType(1), 8));
    IACSharpSensor.IACSharpSensor.SensorReached(817);
  }
  void OrNullableTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(818);
    Expression<Func<MyEnum?, MyEnum?, MyEnum?>> e4 = (a, b) => a | b;
    AssertNodeType(e4, ExpressionType.Convert);
    Assert(null, e4.Compile().Invoke(null, MyEnum.Value_2));
    Assert((MyEnum)3, e4.Compile().Invoke(MyEnum.Value_1, MyEnum.Value_2));
    IACSharpSensor.IACSharpSensor.SensorReached(819);
  }
  void OrElseTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(820);
    Expression<Func<bool, bool, bool>> e = (bool a, bool b) => a || b;
    AssertNodeType(e, ExpressionType.OrElse);
    Assert(true, e.Compile().Invoke(true, false));
    IACSharpSensor.IACSharpSensor.SensorReached(821);
  }
  void OrElseTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(822);
    Expression<Func<MyType, MyType, MyType>> e2 = (MyType a, MyType b) => a || b;
    AssertNodeType(e2, ExpressionType.OrElse);
    Assert(new MyType(64), e2.Compile().Invoke(new MyType(64), new MyType(64)));
    Assert(new MyType(32), e2.Compile().Invoke(new MyType(32), new MyType(64)));
    IACSharpSensor.IACSharpSensor.SensorReached(823);
  }
  void ParameterTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(824);
    Expression<Func<string, string>> e = (string a) => a;
    AssertNodeType(e, ExpressionType.Parameter);
    Assert("t", e.Compile().Invoke("t"));
    IACSharpSensor.IACSharpSensor.SensorReached(825);
  }
  void ParameterTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(826);
    Expression<Func<object[], object[]>> e2 = (object[] a) => a;
    AssertNodeType(e2, ExpressionType.Parameter);
    Assert(new object[0], e2.Compile().Invoke(new object[0]));
    IACSharpSensor.IACSharpSensor.SensorReached(827);
  }
  void ParameterTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(828);
    Expression<Func<IntPtr, IntPtr>> e3 = a => a;
    AssertNodeType(e3, ExpressionType.Parameter);
    Assert(IntPtr.Zero, e3.Compile().Invoke(IntPtr.Zero));
    IACSharpSensor.IACSharpSensor.SensorReached(829);
  }
  unsafe void ParameterTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(830);
    Expression<Func<int*[], int*[]>> e4 = a => a;
    AssertNodeType(e4, ExpressionType.Parameter);
    Assert<int*[]>(null, e4.Compile().Invoke(null));
    int* e4_el = stackalloc int[5];
    int*[] ptr = new int*[] { e4_el };
    Assert<int*[]>(ptr, e4.Compile().Invoke(ptr));
    IACSharpSensor.IACSharpSensor.SensorReached(831);
  }
  void QuoteTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(832);
    Expression<Func<Expression<Func<int>>>> e = () => () => 2;
    AssertNodeType(e, ExpressionType.Quote);
    Assert(2, e.Compile().Invoke().Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(833);
  }
  void RightShiftTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(834);
    Expression<Func<ulong, short, ulong>> e = (ulong a, short b) => a >> b;
    AssertNodeType(e, ExpressionType.RightShift);
    Assert((ulong)0x1fd940L, e.Compile().Invoke(0xfeca0000u, 11));
    Assert((ulong)0x7fffffff, e.Compile().Invoke(0xffffffffu, 0xa01));
    IACSharpSensor.IACSharpSensor.SensorReached(835);
  }
  void RightShiftTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(836);
    Expression<Func<MyType, MyType, int>> e2 = (MyType a, MyType b) => a >> b;
    AssertNodeType(e2, ExpressionType.RightShift);
    var c2 = e2.Compile();
    Assert(64, c2(new MyType(256), new MyType(2)));
    IACSharpSensor.IACSharpSensor.SensorReached(837);
  }
  void RightShiftTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(838);
    Expression<Func<long?, sbyte, long?>> e3 = (long? a, sbyte b) => a >> b;
    AssertNodeType(e3, ExpressionType.RightShift);
    Assert(null, e3.Compile().Invoke(null, 11));
    Assert(512, e3.Compile().Invoke(1024, 1));
    IACSharpSensor.IACSharpSensor.SensorReached(839);
  }
  void RightShiftTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(840);
    Expression<Func<MyType?, MyType?, int?>> e4 = (MyType? a, MyType? b) => a >> b;
    AssertNodeType(e4, ExpressionType.RightShift);
    var c4 = e4.Compile();
    Assert(null, c4(new MyType(8), null));
    Assert(null, c4(null, new MyType(8)));
    Assert(64, c4(new MyType(256), new MyType(2)));
    IACSharpSensor.IACSharpSensor.SensorReached(841);
  }
  void SubtractTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(842);
    Expression<Func<int, int, int>> e = (int a, int b) => a - b;
    AssertNodeType(e, ExpressionType.Subtract);
    Assert(-10, e.Compile().Invoke(20, 30));
    IACSharpSensor.IACSharpSensor.SensorReached(843);
  }
  void SubtractTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(844);
    Expression<Func<int?, int?, int?>> e2 = (a, b) => a - b;
    AssertNodeType(e2, ExpressionType.Subtract);
    Assert(null, e2.Compile().Invoke(null, 3));
    IACSharpSensor.IACSharpSensor.SensorReached(845);
  }
  void SubtractTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(846);
    Expression<Func<MyType, MyType, MyType>> e3 = (MyType a, MyType b) => a - b;
    AssertNodeType(e3, ExpressionType.Subtract);
    Assert(-50, e3.Compile().Invoke(new MyType(-20), new MyType(30)));
    IACSharpSensor.IACSharpSensor.SensorReached(847);
  }
  void SubtractTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(848);
    Expression<Func<MyType?, MyType?, MyType?>> e4 = (MyType? a, MyType? b) => a - b;
    AssertNodeType(e4, ExpressionType.Subtract);
    Assert(new MyType(-50), e4.Compile().Invoke(new MyType(-20), new MyType(30)));
    Assert(null, e4.Compile().Invoke(null, new MyType(30)));
    IACSharpSensor.IACSharpSensor.SensorReached(849);
  }
  void SubtractTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(850);
    Expression<Func<int, MyType, int>> e5 = (int a, MyType b) => a - b;
    AssertNodeType(e5, ExpressionType.Subtract);
    Assert(-29, e5.Compile().Invoke(1, new MyType(30)));
    IACSharpSensor.IACSharpSensor.SensorReached(851);
  }
  void SubtractTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(852);
    Expression<Func<int, MyType?, int?>> e6 = (int a, MyType? b) => a - b;
    AssertNodeType(e6, ExpressionType.Subtract);
    Assert(-61, e6.Compile().Invoke(-31, new MyType(30)));
    IACSharpSensor.IACSharpSensor.SensorReached(853);
  }
  void SubtractTest_7()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(854);
    Expression<Func<ushort, int?>> e7 = (ushort a) => null - a;
    AssertNodeType(e7, ExpressionType.Subtract);
    Assert(null, e7.Compile().Invoke(690));
    IACSharpSensor.IACSharpSensor.SensorReached(855);
  }
  void SubtractTest_8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(856);
    Expression<Func<MyEnum, byte, MyEnum>> e8 = (a, b) => a - b;
    AssertNodeType(e8, ExpressionType.Convert);
    Assert((MyEnum)255, e8.Compile().Invoke(MyEnum.Value_1, 2));
    IACSharpSensor.IACSharpSensor.SensorReached(857);
  }
  void SubtractTest_9()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(858);
    Expression<Func<MyEnum, MyEnum, byte>> e9 = (a, b) => a - b;
    AssertNodeType(e9, ExpressionType.Convert);
    Assert(1, e9.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_1));
    IACSharpSensor.IACSharpSensor.SensorReached(859);
  }
  void SubtractTest_10()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(860);
    Expression<Func<MyEnum?, byte?, MyEnum?>> e10 = (a, b) => a - b;
    AssertNodeType(e10, ExpressionType.Convert);
    Assert((MyEnum)255, e10.Compile().Invoke(MyEnum.Value_1, 2));
    IACSharpSensor.IACSharpSensor.SensorReached(861);
  }
  void SubtractTest_11()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(862);
    Expression<Func<MyEnum?, MyEnum?, byte?>> e11 = (a, b) => a - b;
    AssertNodeType(e11, ExpressionType.Convert);
    Assert<byte?>(1, e11.Compile().Invoke(MyEnum.Value_2, MyEnum.Value_1));
    IACSharpSensor.IACSharpSensor.SensorReached(863);
  }
  void SubtractCheckedTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(864);
    checked {
      Expression<Func<long, long, long>> e = (long a, long b) => a - b;
      AssertNodeType(e, ExpressionType.SubtractChecked);
      try {
        e.Compile().Invoke(long.MinValue, 309);
        throw new ApplicationException("SubtractCheckedTest #1");
      } catch (OverflowException) {
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(865);
  }
  void SubtractCheckedTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(866);
    checked {
      Expression<Func<byte?, byte?, int?>> e2 = (a, b) => a - b;
      AssertNodeType(e2, ExpressionType.SubtractChecked);
      Assert(null, e2.Compile().Invoke(null, 3));
      Assert(-55, e2.Compile().Invoke(byte.MinValue, 55));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(867);
  }
  void SubtractCheckedTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(868);
    checked {
      Expression<Func<MyType, MyType, MyType>> e3 = (MyType a, MyType b) => a - b;
      AssertNodeType(e3, ExpressionType.Subtract);
      Assert(-50, e3.Compile().Invoke(new MyType(-20), new MyType(30)));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(869);
  }
  void SubtractCheckedTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(870);
    checked {
      Expression<Func<double, double, double>> e4 = (a, b) => a - b;
      AssertNodeType(e4, ExpressionType.Subtract);
      Assert(double.PositiveInfinity, e4.Compile().Invoke(double.MinValue, double.NegativeInfinity));
    }
    IACSharpSensor.IACSharpSensor.SensorReached(871);
  }
  void TypeAsTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(872);
    Expression<Func<object, Tester>> e = (object a) => a as Tester;
    AssertNodeType(e, ExpressionType.TypeAs);
    Assert(this, e.Compile().Invoke(this));
    IACSharpSensor.IACSharpSensor.SensorReached(873);
  }
  void TypeAsTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(874);
    Expression<Func<object, int?>> e2 = (object a) => a as int?;
    AssertNodeType(e2, ExpressionType.TypeAs);
    Assert(null, e2.Compile().Invoke(null));
    Assert(null, e2.Compile().Invoke(this));
    Assert(44, e2.Compile().Invoke(44));
    IACSharpSensor.IACSharpSensor.SensorReached(875);
  }
  void TypeAsTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(876);
    Expression<Func<object, object>> e3 = (object a) => null as object;
    AssertNodeType(e3, ExpressionType.TypeAs);
    Assert(null, e3.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(877);
  }
  void TypeIsTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(878);
    Expression<Func<object, bool>> e = (object a) => a is Tester;
    AssertNodeType(e, ExpressionType.TypeIs);
    Assert(true, e.Compile().Invoke(this));
    Assert(false, e.Compile().Invoke(1));
    IACSharpSensor.IACSharpSensor.SensorReached(879);
  }
  void TypeIsTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(880);
    Expression<Func<object, bool>> e2 = (object a) => a is int?;
    AssertNodeType(e2, ExpressionType.TypeIs);
    Assert(false, e2.Compile().Invoke(null));
    Assert(true, e2.Compile().Invoke(1));
    IACSharpSensor.IACSharpSensor.SensorReached(881);
  }
  void TypeIsTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(882);
    Expression<Func<object, bool>> e3 = (object a) => null is object;
    AssertNodeType(e3, ExpressionType.TypeIs);
    Assert(false, e3.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(883);
  }
  void TypeIsTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(884);
    Expression<Func<bool>> e5 = () => 1 is int;
    AssertNodeType(e5, ExpressionType.TypeIs);
    Assert(true, e5.Compile().Invoke());
    IACSharpSensor.IACSharpSensor.SensorReached(885);
  }
  void TypeIsTest_6()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(886);
    Expression<Func<int?, bool>> e6 = a => a is int;
    AssertNodeType(e6, ExpressionType.TypeIs);
    Assert(true, e6.Compile().Invoke(1));
    Assert(false, e6.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(887);
  }
  void UnaryPlusTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(888);
    Expression<Func<int, int>> e = a => +a;
    AssertNodeType(e, ExpressionType.Parameter);
    Assert(-30, e.Compile().Invoke(-30));
    IACSharpSensor.IACSharpSensor.SensorReached(889);
  }
  void UnaryPlusTest_2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(890);
    Expression<Func<long?, long?>> e2 = a => +a;
    AssertNodeType(e2, ExpressionType.Parameter);
    IACSharpSensor.IACSharpSensor.SensorReached(891);
  }
  void UnaryPlusTest_3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(892);
    Expression<Func<MyType, MyType>> e4 = a => +a;
    AssertNodeType(e4, ExpressionType.UnaryPlus);
    Assert(new MyType(-14), e4.Compile().Invoke(new MyType(-14)));
    IACSharpSensor.IACSharpSensor.SensorReached(893);
  }
  void UnaryPlusTest_4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(894);
    Expression<Func<MyType?, MyType?>> e5 = a => +a;
    AssertNodeType(e5, ExpressionType.UnaryPlus);
    Assert(new MyType(33), e5.Compile().Invoke(new MyType(33)));
    Assert(null, e5.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(895);
  }
  void UnaryPlusTest_5()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(896);
    Expression<Func<sbyte?, long?>> e6 = a => +a;
    AssertNodeType(e6, ExpressionType.Convert);
    Assert(3, e6.Compile().Invoke(3));
    Assert(null, e6.Compile().Invoke(null));
    IACSharpSensor.IACSharpSensor.SensorReached(897);
  }
  string InstanceMethod(string arg)
  {
    System.String RNTRNTRNT_170 = arg;
    IACSharpSensor.IACSharpSensor.SensorReached(898);
    return RNTRNTRNT_170;
  }
  object InstanceParamsMethod(int index, params object[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(899);
    if (args == null) {
      System.Object RNTRNTRNT_171 = "<null>";
      IACSharpSensor.IACSharpSensor.SensorReached(900);
      return RNTRNTRNT_171;
    }
    if (args.Length == 0) {
      System.Object RNTRNTRNT_172 = "<empty>";
      IACSharpSensor.IACSharpSensor.SensorReached(901);
      return RNTRNTRNT_172;
    }
    System.Object RNTRNTRNT_173 = args[index];
    IACSharpSensor.IACSharpSensor.SensorReached(902);
    return RNTRNTRNT_173;
  }
  static int TestInt()
  {
    System.Int32 RNTRNTRNT_174 = 29;
    IACSharpSensor.IACSharpSensor.SensorReached(903);
    return RNTRNTRNT_174;
  }
  T GenericMethod<T>(T t)
  {
    T RNTRNTRNT_175 = t;
    IACSharpSensor.IACSharpSensor.SensorReached(904);
    return RNTRNTRNT_175;
  }
  static void RefMethod(ref int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(905);
    i = 867;
    IACSharpSensor.IACSharpSensor.SensorReached(906);
  }
  static bool RunTest(MethodInfo test)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(907);
    Console.Write("Running test {0, -25}", test.Name);
    try {
      test.Invoke(new Tester(), null);
      Console.WriteLine("OK");
      System.Boolean RNTRNTRNT_176 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(908);
      return RNTRNTRNT_176;
    } catch (Exception e) {
      Console.WriteLine("FAILED");
      Console.WriteLine(e.ToString());
      System.Boolean RNTRNTRNT_177 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(909);
      return RNTRNTRNT_177;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(910);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(911);
    var tests = from test in typeof(Tester).GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.DeclaredOnly)
      where test.GetParameters().Length == 0
      orderby test.Name
      select RunTest(test);
    int failures = tests.Count(a => !a);
    Console.WriteLine(failures + " tests failed");
    System.Int32 RNTRNTRNT_178 = failures;
    IACSharpSensor.IACSharpSensor.SensorReached(912);
    return RNTRNTRNT_178;
  }
}
