using System;
delegate void Handler<T>(object sender);
interface IBar<T>
{
  event Handler<T> Handler;
}
class Foo<T>
{
  IBar<T> proxy, real;
  event Handler<T> handler;
  Handler<T> proxyHandler;
  public event Handler<T> Handler {
    add {
      if (handler == null) {
        if (proxyHandler == null)
          proxyHandler = (object s) => handler(proxy);
      }
      handler += value;
    }
    remove { handler -= value; }
  }
}
class Program
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(140);
    var x = new Foo<int>();
    x.Handler += null;
    System.Int32 RNTRNTRNT_56 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(141);
    return RNTRNTRNT_56;
  }
}
