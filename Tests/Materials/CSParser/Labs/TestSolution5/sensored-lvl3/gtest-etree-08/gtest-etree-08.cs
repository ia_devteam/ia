using System;
using System.Linq.Expressions;
class Foo
{
  int ThisMethod()
  {
    System.Int32 RNTRNTRNT_199 = 33;
    IACSharpSensor.IACSharpSensor.SensorReached(950);
    return RNTRNTRNT_199;
  }
  public int Goo(bool hoo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(951);
    bool local_hoo = hoo;
    Expression<Func<bool>> a = () => hoo;
    if (a.Compile()()) {
      System.Int32 RNTRNTRNT_200 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(952);
      return RNTRNTRNT_200;
    }
    if (true) {
      Expression<Func<bool>> b = () => local_hoo;
      if (b.Compile()()) {
        System.Int32 RNTRNTRNT_201 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(953);
        return RNTRNTRNT_201;
      }
    }
    Expression<Func<int>> c = () => ThisMethod();
    if (c.Compile()() != 33) {
      System.Int32 RNTRNTRNT_202 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(954);
      return RNTRNTRNT_202;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_203 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(955);
    return RNTRNTRNT_203;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(956);
    var f = new Foo();
    System.Int32 RNTRNTRNT_204 = f.Goo(false);
    IACSharpSensor.IACSharpSensor.SensorReached(957);
    return RNTRNTRNT_204;
  }
}
