using System;
using System.Collections.Generic;
internal delegate void EmptyDelegate();
class BaseObject
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(44);
    int? i;
    Query<BaseObject>(out i);
    System.Int32 RNTRNTRNT_8 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(45);
    return RNTRNTRNT_8;
  }
  static void Closure(EmptyDelegate x)
  {
  }
  static List<T> Query<T>(out int? count) where T : BaseObject
  {
    IACSharpSensor.IACSharpSensor.SensorReached(46);
    count = 0;
    List<T> results = new List<T>();
    Closure(delegate { results.Add(MakeSomething<T>()); });
    List<T> RNTRNTRNT_9 = results;
    IACSharpSensor.IACSharpSensor.SensorReached(47);
    return RNTRNTRNT_9;
  }
  static T MakeSomething<T>() where T : BaseObject
  {
    IACSharpSensor.IACSharpSensor.SensorReached(48);
    return null;
  }
}
