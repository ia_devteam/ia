using System;
using System.Collections;
using System.Collections.Generic;
public delegate void Foo();
public class Test
{
  public static implicit operator Foo(Test test)
  {
    Foo RNTRNTRNT_69 = delegate { Console.WriteLine("Hello World!"); };
    IACSharpSensor.IACSharpSensor.SensorReached(184);
    return RNTRNTRNT_69;
  }
  public static IEnumerable<Test> operator +(Test test, Test foo)
  {
    Test RNTRNTRNT_70 = test;
    IACSharpSensor.IACSharpSensor.SensorReached(185);
    yield return RNTRNTRNT_70;
    IACSharpSensor.IACSharpSensor.SensorReached(186);
    Test RNTRNTRNT_71 = foo;
    IACSharpSensor.IACSharpSensor.SensorReached(187);
    yield return RNTRNTRNT_71;
    IACSharpSensor.IACSharpSensor.SensorReached(188);
    IACSharpSensor.IACSharpSensor.SensorReached(189);
  }
  public IEnumerable<int> Foo {
    get {
      System.Int32 RNTRNTRNT_72 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(190);
      yield return RNTRNTRNT_72;
      IACSharpSensor.IACSharpSensor.SensorReached(191);
      IACSharpSensor.IACSharpSensor.SensorReached(192);
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(193);
      Console.WriteLine("Foo!");
      IACSharpSensor.IACSharpSensor.SensorReached(194);
    }
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(195);
    Test test = new Test();
    Foo foo = test;
    foo();
    foreach (Test t in test + test) {
      Console.WriteLine(t);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(196);
  }
}
