using System;
delegate void Foo();
class X
{
  public void Hello<U>(U u)
  {
  }
  public void Test<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
    T u = t;
    Hello(u);
    Foo foo = delegate { Hello(u); };
    foo();
    Hello(u);
    IACSharpSensor.IACSharpSensor.SensorReached(2);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(3);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(4);
  }
}
