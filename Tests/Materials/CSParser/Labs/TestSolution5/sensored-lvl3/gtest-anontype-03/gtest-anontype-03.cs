using System;
using System.Collections;
public class MyClass
{
  public string Foo = "Bar";
  public int Baz {
    get {
      System.Int32 RNTRNTRNT_81 = 42;
      IACSharpSensor.IACSharpSensor.SensorReached(207);
      return RNTRNTRNT_81;
    }
  }
}
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(208);
    MyClass mc = new MyClass();
    var v = new {
      mc.Foo,
      mc.Baz
    };
    if (v.Foo != "Bar") {
      System.Int32 RNTRNTRNT_82 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(209);
      return RNTRNTRNT_82;
    }
    if (v.Baz != 42) {
      System.Int32 RNTRNTRNT_83 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(210);
      return RNTRNTRNT_83;
    }
    System.Int32 RNTRNTRNT_84 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(211);
    return RNTRNTRNT_84;
  }
}
