using System;
public interface IA
{
  void Foo(IA self);
}
public static class C
{
  public static void Foo(this IA self)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1028);
    self.Foo<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(1029);
  }
  public static void Bar<U>(this IA self)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1030);
    self.Foo<U>();
    IACSharpSensor.IACSharpSensor.SensorReached(1031);
  }
  public static void Foo<T>(this IA self)
  {
  }
  public static void Main()
  {
  }
}
