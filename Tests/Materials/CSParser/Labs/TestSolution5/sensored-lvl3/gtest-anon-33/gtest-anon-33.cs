using System;
using System.Collections.Generic;
using System.Text;
public static class IEnumerableRocks
{
  public static string Implode<TSource, TResult>(this IEnumerable<TSource> self, string separator, Func<TSource, TResult> selector)
  {
    System.String RNTRNTRNT_42 = Implode(self, separator, (b, e) => { b.Append(selector(e).ToString()); });
    IACSharpSensor.IACSharpSensor.SensorReached(116);
    return RNTRNTRNT_42;
  }
  public static string Implode<TSource>(this IEnumerable<TSource> self, string separator, Action<StringBuilder, TSource> appender)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(117);
    var coll = self as ICollection<TSource>;
    if (coll != null && coll.Count == 0) {
      System.String RNTRNTRNT_43 = string.Empty;
      IACSharpSensor.IACSharpSensor.SensorReached(118);
      return RNTRNTRNT_43;
    }
    bool needSep = false;
    var s = new StringBuilder();
    foreach (var element in self) {
      if (needSep && separator != null) {
        s.Append(separator);
      }
      appender(s, element);
      needSep = true;
    }
    System.String RNTRNTRNT_44 = s.ToString();
    IACSharpSensor.IACSharpSensor.SensorReached(119);
    return RNTRNTRNT_44;
  }
}
class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(120);
    Console.WriteLine(new[] {
      "foo",
      "bar"
    }.Implode(", ", e => "'" + e + "'"));
    IACSharpSensor.IACSharpSensor.SensorReached(121);
  }
}
