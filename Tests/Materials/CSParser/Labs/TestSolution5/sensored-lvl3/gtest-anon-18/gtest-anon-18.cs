public class C
{
  public delegate TR Func<TR, TA>(TA t);
  public static TR Test<TR, TA>(Func<TR, TA> f)
  {
    TR RNTRNTRNT_5 = default(TR);
    IACSharpSensor.IACSharpSensor.SensorReached(29);
    return RNTRNTRNT_5;
  }
  public static TR Test<TR, TA, TB>(Func<TR, TA> f, Func<TR, TB> f2)
  {
    TR RNTRNTRNT_6 = default(TR);
    IACSharpSensor.IACSharpSensor.SensorReached(30);
    return RNTRNTRNT_6;
  }
  public static void Test2<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(31);
    T r = Test(delegate(T i) { return i; });
    IACSharpSensor.IACSharpSensor.SensorReached(32);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(33);
    int r = Test(delegate(int i) { return i < 1 ? 'a' : i; });
    string s = Test(delegate(int i) { return "a"; }, delegate(int i) { return "b"; });
    IACSharpSensor.IACSharpSensor.SensorReached(34);
  }
}
