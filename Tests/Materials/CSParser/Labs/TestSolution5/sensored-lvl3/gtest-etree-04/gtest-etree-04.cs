using System;
using System.Linq.Expressions;
struct Foo
{
  public static bool operator >(Foo d1, Foo d2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(920);
    throw new ApplicationException();
  }
  public static bool operator <(Foo d1, Foo d2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(921);
    throw new ApplicationException();
  }
  public static bool operator ==(Foo d1, Foo d2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(922);
    throw new ApplicationException();
  }
  public static bool operator !=(Foo d1, Foo d2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(923);
    throw new ApplicationException();
  }
}
class C
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(924);
    Foo f;
    Expression<Func<bool>> e = () => f > null;
    if (e.Compile().Invoke()) {
      System.Int32 RNTRNTRNT_183 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(925);
      return RNTRNTRNT_183;
    }
    e = () => f < null;
    if (e.Compile().Invoke()) {
      System.Int32 RNTRNTRNT_184 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(926);
      return RNTRNTRNT_184;
    }
    e = () => f == null;
    if (e.Compile().Invoke()) {
      System.Int32 RNTRNTRNT_185 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(927);
      return RNTRNTRNT_185;
    }
    e = () => f != null;
    if (!e.Compile().Invoke()) {
      System.Int32 RNTRNTRNT_186 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(928);
      return RNTRNTRNT_186;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_187 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(929);
    return RNTRNTRNT_187;
  }
}
