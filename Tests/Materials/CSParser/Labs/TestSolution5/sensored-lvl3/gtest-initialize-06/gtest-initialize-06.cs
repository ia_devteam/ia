struct Point
{
  public int X, Y;
}
class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1105);
    Point p;
    Foo(out p);
    if (p.X != 3) {
      System.Int32 RNTRNTRNT_292 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1106);
      return RNTRNTRNT_292;
    }
    if (p.Y != 5) {
      System.Int32 RNTRNTRNT_293 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1107);
      return RNTRNTRNT_293;
    }
    System.Int32 RNTRNTRNT_294 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1108);
    return RNTRNTRNT_294;
  }
  static void Foo(out Point p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1109);
    p = new Point {
      X = 3,
      Y = 5
    };
    IACSharpSensor.IACSharpSensor.SensorReached(1110);
  }
}
