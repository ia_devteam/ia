using System;
using System.Collections;
public class MyClass
{
  public string Foo = "Bar";
  public int Baz {
    get {
      System.Int32 RNTRNTRNT_85 = 16;
      IACSharpSensor.IACSharpSensor.SensorReached(212);
      return RNTRNTRNT_85;
    }
  }
}
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(213);
    string Hello = "World";
    MyClass mc = new MyClass();
    var v = new {
      mc.Foo,
      mc.Baz,
      Hello,
      Answer = 42
    };
    if (v.Foo != "Bar") {
      System.Int32 RNTRNTRNT_86 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(214);
      return RNTRNTRNT_86;
    }
    if (v.Baz != 16) {
      System.Int32 RNTRNTRNT_87 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(215);
      return RNTRNTRNT_87;
    }
    if (v.Hello != "World") {
      System.Int32 RNTRNTRNT_88 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(216);
      return RNTRNTRNT_88;
    }
    if (v.Answer != 42) {
      System.Int32 RNTRNTRNT_89 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(217);
      return RNTRNTRNT_89;
    }
    System.Int32 RNTRNTRNT_90 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(218);
    return RNTRNTRNT_90;
  }
}
