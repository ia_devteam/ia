class A
{
  public int X {
    get {
      System.Int32 RNTRNTRNT_103 = 100;
      IACSharpSensor.IACSharpSensor.SensorReached(240);
      return RNTRNTRNT_103;
    }
  }
}
class B : A
{
  public static int Main()
  {
    System.Int32 RNTRNTRNT_104 = new B().Test();
    IACSharpSensor.IACSharpSensor.SensorReached(241);
    return RNTRNTRNT_104;
  }
  int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(242);
    var x = new { base.X };
    System.Int32 RNTRNTRNT_105 = x.X == 100 ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(243);
    return RNTRNTRNT_105;
  }
}
