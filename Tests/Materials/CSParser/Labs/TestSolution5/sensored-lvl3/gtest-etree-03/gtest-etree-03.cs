using System;
using System.Linq.Expressions;
struct S<T> where T : struct
{
  public static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(915);
    Expression<Func<T?, bool>> e = (T? o) => o == null;
    if (!e.Compile().Invoke(null)) {
      System.Int32 RNTRNTRNT_179 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(916);
      return RNTRNTRNT_179;
    }
    if (e.Compile().Invoke(default(T))) {
      System.Int32 RNTRNTRNT_180 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(917);
      return RNTRNTRNT_180;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_181 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(918);
    return RNTRNTRNT_181;
  }
}
class C
{
  static int Main()
  {
    System.Int32 RNTRNTRNT_182 = S<int>.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(919);
    return RNTRNTRNT_182;
  }
}
