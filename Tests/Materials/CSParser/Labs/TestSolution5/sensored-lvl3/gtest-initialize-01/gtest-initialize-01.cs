using System;
using System.Collections;
public class MyClass
{
  public string Foo = "Bar";
  private int answer;
  public int Answer {
    get {
      System.Int32 RNTRNTRNT_272 = answer;
      IACSharpSensor.IACSharpSensor.SensorReached(1075);
      return RNTRNTRNT_272;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1076);
      answer = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1077);
    }
  }
}
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1078);
    MyClass mc = new MyClass {
      Foo = "Baz",
      Answer = 42
    };
    if (mc.Foo != "Baz") {
      System.Int32 RNTRNTRNT_273 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1079);
      return RNTRNTRNT_273;
    }
    if (mc.Answer != 42) {
      System.Int32 RNTRNTRNT_274 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1080);
      return RNTRNTRNT_274;
    }
    System.Int32 RNTRNTRNT_275 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1081);
    return RNTRNTRNT_275;
  }
}
