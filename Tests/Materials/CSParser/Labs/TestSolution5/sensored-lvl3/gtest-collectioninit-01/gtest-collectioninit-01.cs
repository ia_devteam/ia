using System;
using System.Collections;
using System.Collections.Generic;
public class Test
{
  class Wrap
  {
    ArrayList numbers = new ArrayList();
    public int Id;
    public Wrap Next;
    public Wrap()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(271);
      Next = new Wrap(100);
      IACSharpSensor.IACSharpSensor.SensorReached(272);
    }
    public Wrap(int i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(273);
      Next = this;
      IACSharpSensor.IACSharpSensor.SensorReached(274);
    }
    public ArrayList Numbers {
      get {
        ArrayList RNTRNTRNT_124 = numbers;
        IACSharpSensor.IACSharpSensor.SensorReached(275);
        return RNTRNTRNT_124;
      }
    }
  }
  static void TestList(List<int> list, int expectedCount)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(276);
    if (list.Count != expectedCount) {
      throw new ApplicationException(expectedCount.ToString());
    }
    foreach (int i in list) {
      Console.WriteLine(i);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(277);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(278);
    ArrayList collection = new ArrayList {
      "Foo",
      null,
      1
    };
    if (collection.Count != 3) {
      System.Int32 RNTRNTRNT_125 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(279);
      return RNTRNTRNT_125;
    }
    if ((string)collection[0] != "Foo") {
      System.Int32 RNTRNTRNT_126 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(280);
      return RNTRNTRNT_126;
    }
    if ((int)collection[2] != 1) {
      System.Int32 RNTRNTRNT_127 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(281);
      return RNTRNTRNT_127;
    }
    List<string> generic_collection = new List<string> {
      "Hello",
      "World"
    };
    foreach (string s in generic_collection) {
      if (s.Length != 5) {
        System.Int32 RNTRNTRNT_128 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(282);
        return RNTRNTRNT_128;
      }
    }
    List<Wrap> a = null;
    a = new List<Wrap> {
      new Wrap(0) {
        Id = 0,
        Numbers = {
          5,
          10
        },
        Next = { Id = 55 }
      },
      new Wrap {
        Id = 1,
        Numbers = { collection }
      },
      new Wrap {
        Id = 2,
        Numbers = {
          
        }
      },
      null
    };
    if (a.Count != 4) {
      System.Int32 RNTRNTRNT_129 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(283);
      return RNTRNTRNT_129;
    }
    if ((int)a[0].Numbers[1] != 10) {
      System.Int32 RNTRNTRNT_130 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(284);
      return RNTRNTRNT_130;
    }
    new List<int> {
      1,
      2,
      3,
      4
    };
    TestList(new List<int> {
      1,
      2,
      3,
      4
    }, 4);
    new List<int> {
      
    };
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_131 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(285);
    return RNTRNTRNT_131;
  }
}
