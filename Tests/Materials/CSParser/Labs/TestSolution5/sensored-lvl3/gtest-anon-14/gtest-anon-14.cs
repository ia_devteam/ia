using System;
class T
{
  void SomeMethod(Converter<Int32, Int32> converter)
  {
  }
  void SomeCaller()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(21);
    SomeMethod(delegate(Int32 a) { return a; });
    IACSharpSensor.IACSharpSensor.SensorReached(22);
  }
  static void Main()
  {
  }
}
