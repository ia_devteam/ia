using System;
using System.Collections.Generic;
public static class Foo
{
  public static IEnumerable<T> Reverse<T>(this IEnumerable<T> self)
  {
    IEnumerable<T> RNTRNTRNT_242 = self;
    IACSharpSensor.IACSharpSensor.SensorReached(1025);
    return RNTRNTRNT_242;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1026);
    int[] data = {
      0,
      1,
      2
    };
    var rev = data.Reverse();
    IACSharpSensor.IACSharpSensor.SensorReached(1027);
  }
}
