using System;
using System.Linq.Expressions;
delegate void EmptyDelegate();
unsafe delegate int* UnsafeDelegate();
class C
{
  static int i;
  static void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(939);
    i += 9;
    IACSharpSensor.IACSharpSensor.SensorReached(940);
  }
  unsafe static int* Foo()
  {
    System.Int32* RNTRNTRNT_191 = (int*)1;
    IACSharpSensor.IACSharpSensor.SensorReached(941);
    return RNTRNTRNT_191;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(942);
    Expression<Func<EmptyDelegate>> e = () => new EmptyDelegate(Test);
    if (e.Body.ToString() != "Convert(CreateDelegate(EmptyDelegate, null, Void Test()))") {
      System.Int32 RNTRNTRNT_192 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(943);
      return RNTRNTRNT_192;
    }
    var v = e.Compile();
    v.Invoke()();
    if (i != 9) {
      System.Int32 RNTRNTRNT_193 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(944);
      return RNTRNTRNT_193;
    }
    Expression<Func<EmptyDelegate>> e2 = () => Test;
    if (e2.Body.ToString() != "Convert(CreateDelegate(EmptyDelegate, null, Void Test()))") {
      System.Int32 RNTRNTRNT_194 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(945);
      return RNTRNTRNT_194;
    }
    var v2 = e2.Compile();
    v2.Invoke()();
    if (i != 18) {
      System.Int32 RNTRNTRNT_195 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(946);
      return RNTRNTRNT_195;
    }
    unsafe {
      Expression<Func<UnsafeDelegate>> e3 = () => new UnsafeDelegate(Foo);
      if (e3.Body.ToString() != "Convert(CreateDelegate(UnsafeDelegate, null, Int32* Foo()))") {
        System.Int32 RNTRNTRNT_196 = 5;
        IACSharpSensor.IACSharpSensor.SensorReached(947);
        return RNTRNTRNT_196;
      }
      var v3 = e3.Compile();
      if (v3.Invoke()() != (int*)1) {
        System.Int32 RNTRNTRNT_197 = 6;
        IACSharpSensor.IACSharpSensor.SensorReached(948);
        return RNTRNTRNT_197;
      }
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_198 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(949);
    return RNTRNTRNT_198;
  }
}
