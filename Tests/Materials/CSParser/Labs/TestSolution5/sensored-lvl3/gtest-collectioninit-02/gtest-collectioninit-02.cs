using System.Collections.Generic;
public class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    var o = new Dictionary<string, int> { {
      "Foo",
      3
    } };
    if (o["Foo"] != 3) {
      System.Int32 RNTRNTRNT_132 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(287);
      return RNTRNTRNT_132;
    }
    o = new Dictionary<string, int> {
      {
        "A",
        1
      },
      {
        "B",
        2
      }
    };
    System.Int32 RNTRNTRNT_133 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(288);
    return RNTRNTRNT_133;
  }
}
