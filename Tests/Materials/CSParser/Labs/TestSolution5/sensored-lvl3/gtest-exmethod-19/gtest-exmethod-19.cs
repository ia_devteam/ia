using System;
using System.Collections.Generic;
public static class Rocks
{
  public static string Test_1(this string t)
  {
    System.String RNTRNTRNT_251 = t + ":";
    IACSharpSensor.IACSharpSensor.SensorReached(1048);
    return RNTRNTRNT_251;
  }
  public static int Test_2<T>(this IEnumerable<T> e)
  {
    System.Int32 RNTRNTRNT_252 = 33;
    IACSharpSensor.IACSharpSensor.SensorReached(1049);
    return RNTRNTRNT_252;
  }
}
public class Test
{
  delegate string D();
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1050);
    string s = "jaj";
    D d = s.Test_1;
    Func<int> d2 = "33".Test_2;
    if ((string)d.Target != "jaj") {
      System.Int32 RNTRNTRNT_253 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(1051);
      return RNTRNTRNT_253;
    }
    if ((string)d2.Target != "33") {
      System.Int32 RNTRNTRNT_254 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(1052);
      return RNTRNTRNT_254;
    }
    string res = d();
    Console.WriteLine(res);
    if (res != "jaj:") {
      System.Int32 RNTRNTRNT_255 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1053);
      return RNTRNTRNT_255;
    }
    if (d2() != 33) {
      System.Int32 RNTRNTRNT_256 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1054);
      return RNTRNTRNT_256;
    }
    System.Int32 RNTRNTRNT_257 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1055);
    return RNTRNTRNT_257;
  }
}
