using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
namespace Mono
{
  class C
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(958);
      int[] i2 = new int[] {
        10,
        14
      };
      Expression<Func<IEnumerable<int>>> e = () => from i in i2
        select i;
      int sum = e.Compile()().Sum();
      if (sum != 24) {
        System.Int32 RNTRNTRNT_205 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(959);
        return RNTRNTRNT_205;
      }
      Expression<Func<IEnumerable<long>>> e2 = () => from i in GetValues()
        select i;
      var s2 = e2.Compile()().Sum();
      if (s2 != 14) {
        System.Int32 RNTRNTRNT_206 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(960);
        return RNTRNTRNT_206;
      }
      System.Int32 RNTRNTRNT_207 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(961);
      return RNTRNTRNT_207;
    }
    static long[] GetValues()
    {
      System.Int64[] RNTRNTRNT_208 = new long[] {
        9,
        2,
        3
      };
      IACSharpSensor.IACSharpSensor.SensorReached(962);
      return RNTRNTRNT_208;
    }
  }
}
