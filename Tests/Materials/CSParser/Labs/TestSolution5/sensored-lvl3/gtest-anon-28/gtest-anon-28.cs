abstract class A
{
  public abstract void Foo<T>() where T : struct;
}
class B : A
{
  public delegate void Del();
  public override void Foo<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(99);
    Del d = delegate() { Foo<T>(); };
    IACSharpSensor.IACSharpSensor.SensorReached(100);
  }
  public static void Main()
  {
  }
}
