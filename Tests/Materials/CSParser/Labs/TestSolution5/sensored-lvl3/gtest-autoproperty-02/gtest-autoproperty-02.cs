using System;
public class Test
{
  private class A
  {
    public static string B { get; set; }
    public static string C { get; private set; }
    public static void DoThings()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(251);
      C = "C";
      IACSharpSensor.IACSharpSensor.SensorReached(252);
    }
  }
  public static string Foo { get; set; }
  public static int Answer { get; private set; }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(253);
    Foo = "Bar";
    if (Foo != "Bar") {
      System.Int32 RNTRNTRNT_110 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(254);
      return RNTRNTRNT_110;
    }
    Answer = 42;
    if (Answer != 42) {
      System.Int32 RNTRNTRNT_111 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(255);
      return RNTRNTRNT_111;
    }
    A.B = "B";
    if (A.B != "B") {
      System.Int32 RNTRNTRNT_112 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(256);
      return RNTRNTRNT_112;
    }
    A.DoThings();
    if (A.C != "C") {
      System.Int32 RNTRNTRNT_113 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(257);
      return RNTRNTRNT_113;
    }
    System.Int32 RNTRNTRNT_114 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(258);
    return RNTRNTRNT_114;
  }
}
