using System;
namespace A
{
  public static class A
  {
    public static int Foo(this int i)
    {
      System.Int32 RNTRNTRNT_220 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(981);
      return RNTRNTRNT_220;
    }
    public static int Foo(this int i, string s)
    {
      System.Int32 RNTRNTRNT_221 = 30;
      IACSharpSensor.IACSharpSensor.SensorReached(982);
      return RNTRNTRNT_221;
    }
  }
}
namespace B
{
  public static class X
  {
    public static int Foo(this int i)
    {
      System.Int32 RNTRNTRNT_222 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(983);
      return RNTRNTRNT_222;
    }
    public static int Foo(this int i, bool b)
    {
      System.Int32 RNTRNTRNT_223 = 20;
      IACSharpSensor.IACSharpSensor.SensorReached(984);
      return RNTRNTRNT_223;
    }
  }
}
namespace C
{
  using A;
  using B;
  using D;
  public static class F
  {
    public static bool Foo(this byte i)
    {
      System.Boolean RNTRNTRNT_224 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(985);
      return RNTRNTRNT_224;
    }
  }
  namespace D
  {
    public static class F
    {
      public static int Foo(this int i)
      {
        System.Int32 RNTRNTRNT_225 = 66;
        IACSharpSensor.IACSharpSensor.SensorReached(986);
        return RNTRNTRNT_225;
      }
      public static void TestX()
      {
        IACSharpSensor.IACSharpSensor.SensorReached(987);
        int i = 2.Foo(false);
        IACSharpSensor.IACSharpSensor.SensorReached(988);
      }
    }
  }
  public static class M
  {
    public static int Foo(this int i)
    {
      System.Int32 RNTRNTRNT_226 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(989);
      return RNTRNTRNT_226;
    }
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(990);
      if (3.Foo("a") != 30) {
        System.Int32 RNTRNTRNT_227 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(991);
        return RNTRNTRNT_227;
      }
      if (((byte)0).Foo()) {
        System.Int32 RNTRNTRNT_228 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(992);
        return RNTRNTRNT_228;
      }
      if (4.Foo(false) != 20) {
        System.Int32 RNTRNTRNT_229 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(993);
        return RNTRNTRNT_229;
      }
      Console.WriteLine("OK");
      System.Int32 RNTRNTRNT_230 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(994);
      return RNTRNTRNT_230;
    }
  }
}
