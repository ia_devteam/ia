using System;
public delegate void Foo();
public class World<T>
{
  public void Hello<U>(U u)
  {
  }
  public void Test(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    Hello(t);
    Foo foo = delegate { Hello(t); };
    IACSharpSensor.IACSharpSensor.SensorReached(17);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(18);
    World<X> world = new World<X>();
    world.Test(new X());
    IACSharpSensor.IACSharpSensor.SensorReached(19);
  }
}
