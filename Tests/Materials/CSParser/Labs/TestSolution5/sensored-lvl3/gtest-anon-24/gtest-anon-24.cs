using System;
using System.Collections.Generic;
class Disposable<T> : IDisposable
{
  public void Dispose()
  {
  }
}
class Test
{
  static Func<T[]> For<T>(List<T> list)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(61);
    T[] t = new T[2];
    Func<T[]> RNTRNTRNT_17 = () =>
    {
      for (int i = 0; i < t.Length; ++i) {
        t[i] = list[i];
      }
      return t;
    };
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    return RNTRNTRNT_17;
  }
  static Func<T> Throw<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(63);
    T l = t;
    Func<T> RNTRNTRNT_18 = () =>
    {
      throw new ApplicationException(l.ToString());
    };
    IACSharpSensor.IACSharpSensor.SensorReached(64);
    return RNTRNTRNT_18;
  }
  static Func<T> Do<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(65);
    T l = t;
    Func<T> RNTRNTRNT_19 = () =>
    {
      T t2;
      do {
        t2 = l;
      } while (default(T) != null);
      return t2;
    };
    IACSharpSensor.IACSharpSensor.SensorReached(66);
    return RNTRNTRNT_19;
  }
  static Func<T> Lock<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(67);
    T l = t;
    Func<T> RNTRNTRNT_20 = () =>
    {
      lock (l.GetType()) {
        l = default(T);
        return l;
      }
    };
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    return RNTRNTRNT_20;
  }
  static Func<T> Catch<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(69);
    T l = t;
    Func<T> RNTRNTRNT_21 = () =>
    {
      try {
        throw new ApplicationException(l.ToString());
      } catch {
        return l;
      }
    };
    IACSharpSensor.IACSharpSensor.SensorReached(70);
    return RNTRNTRNT_21;
  }
  static Func<T> Finally<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(71);
    T l = t;
    Func<T> RNTRNTRNT_22 = () =>
    {
      try {
        l = Lock(l)();
      } finally {
        l = default(T);
      }
      return l;
    };
    IACSharpSensor.IACSharpSensor.SensorReached(72);
    return RNTRNTRNT_22;
  }
  static Func<T> Using<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(73);
    T l = t;
    using (var d = new Disposable<T>()) {
      Func<T> RNTRNTRNT_23 = () => { return l; };
      IACSharpSensor.IACSharpSensor.SensorReached(74);
      return RNTRNTRNT_23;
    }
  }
  static Func<T> Switch<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(75);
    T l = t;
    int? i = 0;
    Func<T> RNTRNTRNT_24 = () =>
    {
      switch (i) {
        default:
          return l;
      }
    };
    IACSharpSensor.IACSharpSensor.SensorReached(76);
    return RNTRNTRNT_24;
  }
  static Func<List<T>> ForForeach<T>(T[] t)
  {
    Func<List<T>> RNTRNTRNT_25 = () =>
    {
      foreach (T e in t)
        return new List<T> { e };
      throw new ApplicationException();
    };
    IACSharpSensor.IACSharpSensor.SensorReached(77);
    return RNTRNTRNT_25;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(78);
    if (For(new List<int> {
      5,
      10
    })()[1] != 10) {
      System.Int32 RNTRNTRNT_26 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(79);
      return RNTRNTRNT_26;
    }
    var t = Throw(5);
    try {
      t();
      System.Int32 RNTRNTRNT_27 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(80);
      return RNTRNTRNT_27;
    } catch (ApplicationException) {
    }
    var t3 = Do("rr");
    if (t3() != "rr") {
      System.Int32 RNTRNTRNT_28 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(81);
      return RNTRNTRNT_28;
    }
    var t4 = Lock('f');
    if (t4() != '\0') {
      System.Int32 RNTRNTRNT_29 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(82);
      return RNTRNTRNT_29;
    }
    var t5 = Catch(3);
    if (t5() != 3) {
      System.Int32 RNTRNTRNT_30 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(83);
      return RNTRNTRNT_30;
    }
    var t6 = Finally(5);
    if (t6() != 0) {
      System.Int32 RNTRNTRNT_31 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(84);
      return RNTRNTRNT_31;
    }
    var t7 = Using(1.1);
    if (t7() != 1.1) {
      System.Int32 RNTRNTRNT_32 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(85);
      return RNTRNTRNT_32;
    }
    var t8 = Switch(55);
    if (t8() != 55) {
      System.Int32 RNTRNTRNT_33 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(86);
      return RNTRNTRNT_33;
    }
    var t9 = ForForeach(new[] {
      4,
      1
    });
    if (t9()[0] != 4) {
      System.Int32 RNTRNTRNT_34 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(87);
      return RNTRNTRNT_34;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_35 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(88);
    return RNTRNTRNT_35;
  }
}
