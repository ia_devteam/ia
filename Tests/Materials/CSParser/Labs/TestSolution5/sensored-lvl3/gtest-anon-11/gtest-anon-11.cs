using System;
public delegate void Foo();
public class Test<R>
{
  public void World<S, T>(S s, T t) where S : X where T : S
  {
  }
  public void Hello<U, V>(U u, V v) where U : X where V : U
  {
    IACSharpSensor.IACSharpSensor.SensorReached(12);
    Foo foo = delegate { World(u, v); };
    IACSharpSensor.IACSharpSensor.SensorReached(13);
  }
}
public class X
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(14);
    X x = new X();
    Test<int> test = new Test<int>();
    test.Hello(x, x);
    IACSharpSensor.IACSharpSensor.SensorReached(15);
  }
}
