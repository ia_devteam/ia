using System;
using System.Collections.Generic;
class MyDisposable : IDisposable
{
  static int next_id;
  int id = ++next_id;
  public void Dispose()
  {
  }
  public int ID {
    get {
      System.Int32 RNTRNTRNT_64 = id;
      IACSharpSensor.IACSharpSensor.SensorReached(169);
      return RNTRNTRNT_64;
    }
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_65 = String.Format("{0} ({1})", GetType(), id);
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    return RNTRNTRNT_65;
  }
}
class X
{
  public static IEnumerable<int> Test(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(171);
    MyDisposable d;
    using (d = new MyDisposable()) {
      System.Int32 RNTRNTRNT_66 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(172);
      yield return RNTRNTRNT_66;
      IACSharpSensor.IACSharpSensor.SensorReached(173);
      System.Int32 RNTRNTRNT_67 = d.ID;
      IACSharpSensor.IACSharpSensor.SensorReached(174);
      yield return RNTRNTRNT_67;
      IACSharpSensor.IACSharpSensor.SensorReached(175);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(176);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    foreach (int a in Test(5)) {
      Console.WriteLine(a);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(178);
  }
}
