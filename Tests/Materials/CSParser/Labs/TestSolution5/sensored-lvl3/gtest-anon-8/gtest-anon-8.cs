using System;
using System.Collections.Generic;
delegate int Foo();
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(179);
    Test("Hello World", 8);
    IACSharpSensor.IACSharpSensor.SensorReached(180);
  }
  public static void Test<R>(R r, int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(181);
    for (int b = a; b > 0; b--) {
      R s = r;
      Foo foo = delegate {
        Console.WriteLine(b);
        Console.WriteLine(s);
        void  RNTRNTRNT_68 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(182);
        return RNTRNTRNT_68;
      };
      a -= foo();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(183);
  }
}
