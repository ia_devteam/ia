using System;
class Foo
{
  public bool IsBar {
    get {
      System.Boolean RNTRNTRNT_249 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(1044);
      return RNTRNTRNT_249;
    }
  }
}
static class FooExt
{
  public static bool IsBar(this Foo f)
  {
    System.Boolean RNTRNTRNT_250 = f.IsBar;
    IACSharpSensor.IACSharpSensor.SensorReached(1045);
    return RNTRNTRNT_250;
  }
}
class Repro
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1046);
    var f = new Foo();
    Console.WriteLine(f.IsBar());
    IACSharpSensor.IACSharpSensor.SensorReached(1047);
  }
}
