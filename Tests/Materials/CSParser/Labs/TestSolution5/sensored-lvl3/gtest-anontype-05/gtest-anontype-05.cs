using System;
using System.Collections;
public class Test
{
  static string Null()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(219);
    return null;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    var v1 = new {
      Name = "Scott",
      Age = 21
    };
    var v2 = new {
      Age = 20,
      Name = "Sam"
    };
    var v3 = new {
      Name = Null(),
      Age = 33
    };
    if (v1.GetType() == v2.GetType()) {
      System.Int32 RNTRNTRNT_91 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(221);
      return RNTRNTRNT_91;
    }
    if (v1.Equals(v2)) {
      System.Int32 RNTRNTRNT_92 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(222);
      return RNTRNTRNT_92;
    }
    if (v1.GetType() != v3.GetType()) {
      System.Int32 RNTRNTRNT_93 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(223);
      return RNTRNTRNT_93;
    }
    if (!v1.Equals(v1)) {
      System.Int32 RNTRNTRNT_94 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(224);
      return RNTRNTRNT_94;
    }
    if (v1.GetHashCode() != v1.GetHashCode()) {
      System.Int32 RNTRNTRNT_95 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(225);
      return RNTRNTRNT_95;
    }
    Console.WriteLine(v1);
    Console.WriteLine(v3);
    if (v1.ToString() != "Name = Scott, Age = 21") {
      System.Int32 RNTRNTRNT_96 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(226);
      return RNTRNTRNT_96;
    }
    if (v3.ToString() != "Name = <null>, Age = 33") {
      System.Int32 RNTRNTRNT_97 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(227);
      return RNTRNTRNT_97;
    }
    System.Int32 RNTRNTRNT_98 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(228);
    return RNTRNTRNT_98;
  }
}
