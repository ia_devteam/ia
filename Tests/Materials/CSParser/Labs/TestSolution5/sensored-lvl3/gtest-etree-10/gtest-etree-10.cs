using System;
using System.Collections.Generic;
using System.Linq.Expressions;
class Foo<T>
{
  public bool ContainsAll<U>(IEnumerable<U> items) where U : T
  {
    IACSharpSensor.IACSharpSensor.SensorReached(963);
    foreach (U item in items) {
      Expression<Func<bool>> e = () => !Contains(item);
      if (!e.Compile()()) {
        System.Boolean RNTRNTRNT_209 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(964);
        return RNTRNTRNT_209;
      }
    }
    System.Boolean RNTRNTRNT_210 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(965);
    return RNTRNTRNT_210;
  }
  public bool Contains(T t)
  {
    System.Boolean RNTRNTRNT_211 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(966);
    return RNTRNTRNT_211;
  }
}
class Program
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(967);
    var x = new Foo<int>();
    System.Int32 RNTRNTRNT_212 = x.ContainsAll(new[] {
      4,
      6,
      78
    }) ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(968);
    return RNTRNTRNT_212;
  }
}
