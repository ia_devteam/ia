using System;
public class Test
{
  private class A
  {
    public string B { get; set; }
  }
  public string Foo { get; set; }
  public int Answer { get; private set; }
  public Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(244);
    Answer = 42;
    IACSharpSensor.IACSharpSensor.SensorReached(245);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(246);
    Test t = new Test();
    t.Foo = "Bar";
    if (t.Foo != "Bar") {
      System.Int32 RNTRNTRNT_106 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(247);
      return RNTRNTRNT_106;
    }
    if (t.Answer != 42) {
      System.Int32 RNTRNTRNT_107 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(248);
      return RNTRNTRNT_107;
    }
    A a = new A();
    a.B = "C";
    if (a.B != "C") {
      System.Int32 RNTRNTRNT_108 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(249);
      return RNTRNTRNT_108;
    }
    System.Int32 RNTRNTRNT_109 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(250);
    return RNTRNTRNT_109;
  }
}
