using System;
using System.Reflection;
class C<T>
{
  static Func<T> XX()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(129);
    System.Func<T> t = () => default(T);
    Func<T> RNTRNTRNT_49 = t;
    IACSharpSensor.IACSharpSensor.SensorReached(130);
    return RNTRNTRNT_49;
  }
}
class C2<T>
{
  static Func<C<T>> XX()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(131);
    System.Func<C<T>> t = () => default(C<T>);
    Func<C<T>> RNTRNTRNT_50 = t;
    IACSharpSensor.IACSharpSensor.SensorReached(132);
    return RNTRNTRNT_50;
  }
}
class N1
{
  static Func<T> XX<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(133);
    System.Func<T> t = () => default(T);
    Func<T> RNTRNTRNT_51 = t;
    IACSharpSensor.IACSharpSensor.SensorReached(134);
    return RNTRNTRNT_51;
  }
}
public class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(135);
    var t = typeof(C<>);
    if (t.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Length != 1) {
      System.Int32 RNTRNTRNT_52 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(136);
      return RNTRNTRNT_52;
    }
    t = typeof(C2<>);
    if (t.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Length != 1) {
      System.Int32 RNTRNTRNT_53 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(137);
      return RNTRNTRNT_53;
    }
    t = typeof(N1);
    if (t.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Length != 0) {
      System.Int32 RNTRNTRNT_54 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(138);
      return RNTRNTRNT_54;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_55 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(139);
    return RNTRNTRNT_55;
  }
}
