using System;
using System.IO;
namespace Ionic.BZip2
{
  internal class BitWriter
  {
    uint accumulator;
    int nAccumulatedBits;
    Stream output;
    int totalBytesWrittenOut;
    public BitWriter(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      this.output = s;
      IACSharpSensor.IACSharpSensor.SensorReached(2);
    }
    public byte RemainingBits {
      get {
        System.Byte RNTRNTRNT_1 = (byte)(this.accumulator >> (32 - this.nAccumulatedBits) & 0xff);
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        return RNTRNTRNT_1;
      }
    }
    public int NumRemainingBits {
      get {
        System.Int32 RNTRNTRNT_2 = this.nAccumulatedBits;
        IACSharpSensor.IACSharpSensor.SensorReached(4);
        return RNTRNTRNT_2;
      }
    }
    public int TotalBytesWrittenOut {
      get {
        System.Int32 RNTRNTRNT_3 = this.totalBytesWrittenOut;
        IACSharpSensor.IACSharpSensor.SensorReached(5);
        return RNTRNTRNT_3;
      }
    }
    public void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6);
      this.accumulator = 0;
      this.nAccumulatedBits = 0;
      this.totalBytesWrittenOut = 0;
      this.output.Seek(0, SeekOrigin.Begin);
      this.output.SetLength(0);
      IACSharpSensor.IACSharpSensor.SensorReached(7);
    }
    public void WriteBits(int nbits, uint value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(8);
      int nAccumulated = this.nAccumulatedBits;
      uint u = this.accumulator;
      IACSharpSensor.IACSharpSensor.SensorReached(9);
      while (nAccumulated >= 8) {
        IACSharpSensor.IACSharpSensor.SensorReached(10);
        this.output.WriteByte((byte)(u >> 24 & 0xff));
        this.totalBytesWrittenOut++;
        u <<= 8;
        nAccumulated -= 8;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(11);
      this.accumulator = u | (value << (32 - nAccumulated - nbits));
      this.nAccumulatedBits = nAccumulated + nbits;
      IACSharpSensor.IACSharpSensor.SensorReached(12);
    }
    public void WriteByte(byte b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(13);
      WriteBits(8, b);
      IACSharpSensor.IACSharpSensor.SensorReached(14);
    }
    public void WriteInt(uint u)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(15);
      WriteBits(8, (u >> 24) & 0xff);
      WriteBits(8, (u >> 16) & 0xff);
      WriteBits(8, (u >> 8) & 0xff);
      WriteBits(8, u & 0xff);
      IACSharpSensor.IACSharpSensor.SensorReached(16);
    }
    public void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(17);
      WriteBits(0, 0);
      IACSharpSensor.IACSharpSensor.SensorReached(18);
    }
    public void FinishAndPad()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(19);
      Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(20);
      if (this.NumRemainingBits > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(21);
        byte b = (byte)((this.accumulator >> 24) & 0xff);
        this.output.WriteByte(b);
        this.totalBytesWrittenOut++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(22);
    }
  }
}
