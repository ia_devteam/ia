using System;
using System.IO;
namespace Ionic.BZip2
{
  public class BZip2InputStream : System.IO.Stream
  {
    bool _disposed;
    bool _leaveOpen;
    Int64 totalBytesRead;
    private int last;
    private int origPtr;
    private int blockSize100k;
    private bool blockRandomised;
    private int bsBuff;
    private int bsLive;
    private readonly Ionic.Crc.CRC32 crc = new Ionic.Crc.CRC32(true);
    private int nInUse;
    private Stream input;
    private int currentChar = -1;
    enum CState
    {
      EOF = 0,
      START_BLOCK = 1,
      RAND_PART_A = 2,
      RAND_PART_B = 3,
      RAND_PART_C = 4,
      NO_RAND_PART_A = 5,
      NO_RAND_PART_B = 6,
      NO_RAND_PART_C = 7
    }
    private CState currentState = CState.START_BLOCK;
    private uint storedBlockCRC, storedCombinedCRC;
    private uint computedBlockCRC, computedCombinedCRC;
    private int su_count;
    private int su_ch2;
    private int su_chPrev;
    private int su_i2;
    private int su_j2;
    private int su_rNToGo;
    private int su_rTPos;
    private int su_tPos;
    private char su_z;
    private BZip2InputStream.DecompressionState data;
    public BZip2InputStream(Stream input) : this(input, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(475);
    }
    public BZip2InputStream(Stream input, bool leaveOpen) : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(476);
      this.input = input;
      this._leaveOpen = leaveOpen;
      init();
      IACSharpSensor.IACSharpSensor.SensorReached(477);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(478);
      if (offset < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(479);
        throw new IndexOutOfRangeException(String.Format("offset ({0}) must be > 0", offset));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(480);
      if (count < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(481);
        throw new IndexOutOfRangeException(String.Format("count ({0}) must be > 0", count));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(482);
      if (offset + count > buffer.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(483);
        throw new IndexOutOfRangeException(String.Format("offset({0}) count({1}) bLength({2})", offset, count, buffer.Length));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(484);
      if (this.input == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(485);
        throw new IOException("the stream is not open");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(486);
      int hi = offset + count;
      int destOffset = offset;
      IACSharpSensor.IACSharpSensor.SensorReached(487);
      for (int b; (destOffset < hi) && ((b = ReadByte()) >= 0);) {
        IACSharpSensor.IACSharpSensor.SensorReached(488);
        buffer[destOffset++] = (byte)b;
      }
      System.Int32 RNTRNTRNT_18 = (destOffset == offset) ? -1 : (destOffset - offset);
      IACSharpSensor.IACSharpSensor.SensorReached(489);
      return RNTRNTRNT_18;
    }
    private void MakeMaps()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(490);
      bool[] inUse = this.data.inUse;
      byte[] seqToUnseq = this.data.seqToUnseq;
      int n = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(491);
      for (int i = 0; i < 256; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(492);
        if (inUse[i]) {
          IACSharpSensor.IACSharpSensor.SensorReached(493);
          seqToUnseq[n++] = (byte)i;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(494);
      this.nInUse = n;
      IACSharpSensor.IACSharpSensor.SensorReached(495);
    }
    public override int ReadByte()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(496);
      int retChar = this.currentChar;
      totalBytesRead++;
      IACSharpSensor.IACSharpSensor.SensorReached(497);
      switch (this.currentState) {
        case CState.EOF:
          System.Int32 RNTRNTRNT_19 = -1;
          IACSharpSensor.IACSharpSensor.SensorReached(498);
          return RNTRNTRNT_19;
        case CState.START_BLOCK:
          IACSharpSensor.IACSharpSensor.SensorReached(499);
          throw new IOException("bad state");
        case CState.RAND_PART_A:
          IACSharpSensor.IACSharpSensor.SensorReached(500);
          throw new IOException("bad state");
        case CState.RAND_PART_B:
          IACSharpSensor.IACSharpSensor.SensorReached(501);
          SetupRandPartB();
          IACSharpSensor.IACSharpSensor.SensorReached(502);
          break;
        case CState.RAND_PART_C:
          IACSharpSensor.IACSharpSensor.SensorReached(503);
          SetupRandPartC();
          IACSharpSensor.IACSharpSensor.SensorReached(504);
          break;
        case CState.NO_RAND_PART_A:
          IACSharpSensor.IACSharpSensor.SensorReached(505);
          throw new IOException("bad state");
        case CState.NO_RAND_PART_B:
          IACSharpSensor.IACSharpSensor.SensorReached(506);
          SetupNoRandPartB();
          IACSharpSensor.IACSharpSensor.SensorReached(507);
          break;
        case CState.NO_RAND_PART_C:
          IACSharpSensor.IACSharpSensor.SensorReached(508);
          SetupNoRandPartC();
          IACSharpSensor.IACSharpSensor.SensorReached(509);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(510);
          throw new IOException("bad state");
      }
      System.Int32 RNTRNTRNT_20 = retChar;
      IACSharpSensor.IACSharpSensor.SensorReached(511);
      return RNTRNTRNT_20;
    }
    public override bool CanRead {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(512);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(513);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_21 = this.input.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(514);
        return RNTRNTRNT_21;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_22 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(515);
        return RNTRNTRNT_22;
      }
    }
    public override bool CanWrite {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(516);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(517);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_23 = input.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(518);
        return RNTRNTRNT_23;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(519);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(520);
        throw new ObjectDisposedException("BZip2Stream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(521);
      input.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(522);
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(523);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_24 = this.totalBytesRead;
        IACSharpSensor.IACSharpSensor.SensorReached(524);
        return RNTRNTRNT_24;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(525);
        throw new NotImplementedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(526);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(527);
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(528);
      throw new NotImplementedException();
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(529);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(530);
        if (!_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(531);
          if (disposing && (this.input != null)) {
            IACSharpSensor.IACSharpSensor.SensorReached(532);
            this.input.Close();
          }
          IACSharpSensor.IACSharpSensor.SensorReached(533);
          _disposed = true;
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(534);
        base.Dispose(disposing);
        IACSharpSensor.IACSharpSensor.SensorReached(535);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(536);
    }
    void init()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(537);
      if (null == this.input) {
        IACSharpSensor.IACSharpSensor.SensorReached(538);
        throw new IOException("No input Stream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(539);
      if (!this.input.CanRead) {
        IACSharpSensor.IACSharpSensor.SensorReached(540);
        throw new IOException("Unreadable input Stream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(541);
      CheckMagicChar('B', 0);
      CheckMagicChar('Z', 1);
      CheckMagicChar('h', 2);
      int blockSize = this.input.ReadByte();
      IACSharpSensor.IACSharpSensor.SensorReached(542);
      if ((blockSize < '1') || (blockSize > '9')) {
        IACSharpSensor.IACSharpSensor.SensorReached(543);
        throw new IOException("Stream is not BZip2 formatted: illegal " + "blocksize " + (char)blockSize);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(544);
      this.blockSize100k = blockSize - '0';
      InitBlock();
      SetupBlock();
      IACSharpSensor.IACSharpSensor.SensorReached(545);
    }
    void CheckMagicChar(char expected, int position)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(546);
      int magic = this.input.ReadByte();
      IACSharpSensor.IACSharpSensor.SensorReached(547);
      if (magic != (int)expected) {
        IACSharpSensor.IACSharpSensor.SensorReached(548);
        var msg = String.Format("Not a valid BZip2 stream. byte {0}, expected '{1}', got '{2}'", position, (int)expected, magic);
        IACSharpSensor.IACSharpSensor.SensorReached(549);
        throw new IOException(msg);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(550);
    }
    void InitBlock()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(551);
      char magic0 = bsGetUByte();
      char magic1 = bsGetUByte();
      char magic2 = bsGetUByte();
      char magic3 = bsGetUByte();
      char magic4 = bsGetUByte();
      char magic5 = bsGetUByte();
      IACSharpSensor.IACSharpSensor.SensorReached(552);
      if (magic0 == 0x17 && magic1 == 0x72 && magic2 == 0x45 && magic3 == 0x38 && magic4 == 0x50 && magic5 == 0x90) {
        IACSharpSensor.IACSharpSensor.SensorReached(553);
        complete();
      } else if (magic0 != 0x31 || magic1 != 0x41 || magic2 != 0x59 || magic3 != 0x26 || magic4 != 0x53 || magic5 != 0x59) {
        IACSharpSensor.IACSharpSensor.SensorReached(558);
        this.currentState = CState.EOF;
        var msg = String.Format("bad block header at offset 0x{0:X}", this.input.Position);
        IACSharpSensor.IACSharpSensor.SensorReached(559);
        throw new IOException(msg);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(554);
        this.storedBlockCRC = bsGetInt();
        this.blockRandomised = (GetBits(1) == 1);
        IACSharpSensor.IACSharpSensor.SensorReached(555);
        if (this.data == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(556);
          this.data = new DecompressionState(this.blockSize100k);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(557);
        getAndMoveToFrontDecode();
        this.crc.Reset();
        this.currentState = CState.START_BLOCK;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(560);
    }
    private void EndBlock()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(561);
      this.computedBlockCRC = (uint)this.crc.Crc32Result;
      IACSharpSensor.IACSharpSensor.SensorReached(562);
      if (this.storedBlockCRC != this.computedBlockCRC) {
        IACSharpSensor.IACSharpSensor.SensorReached(563);
        var msg = String.Format("BZip2 CRC error (expected {0:X8}, computed {1:X8})", this.storedBlockCRC, this.computedBlockCRC);
        IACSharpSensor.IACSharpSensor.SensorReached(564);
        throw new IOException(msg);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(565);
      this.computedCombinedCRC = (this.computedCombinedCRC << 1) | (this.computedCombinedCRC >> 31);
      this.computedCombinedCRC ^= this.computedBlockCRC;
      IACSharpSensor.IACSharpSensor.SensorReached(566);
    }
    private void complete()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(567);
      this.storedCombinedCRC = bsGetInt();
      this.currentState = CState.EOF;
      this.data = null;
      IACSharpSensor.IACSharpSensor.SensorReached(568);
      if (this.storedCombinedCRC != this.computedCombinedCRC) {
        IACSharpSensor.IACSharpSensor.SensorReached(569);
        var msg = String.Format("BZip2 CRC error (expected {0:X8}, computed {1:X8})", this.storedCombinedCRC, this.computedCombinedCRC);
        IACSharpSensor.IACSharpSensor.SensorReached(570);
        throw new IOException(msg);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(571);
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(572);
      Stream inShadow = this.input;
      IACSharpSensor.IACSharpSensor.SensorReached(573);
      if (inShadow != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(574);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(575);
          if (!this._leaveOpen) {
            IACSharpSensor.IACSharpSensor.SensorReached(576);
            inShadow.Close();
          }
        } finally {
          IACSharpSensor.IACSharpSensor.SensorReached(577);
          this.data = null;
          this.input = null;
          IACSharpSensor.IACSharpSensor.SensorReached(578);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(579);
    }
    private int GetBits(int n)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(580);
      int bsLiveShadow = this.bsLive;
      int bsBuffShadow = this.bsBuff;
      IACSharpSensor.IACSharpSensor.SensorReached(581);
      if (bsLiveShadow < n) {
        IACSharpSensor.IACSharpSensor.SensorReached(582);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(583);
          int thech = this.input.ReadByte();
          IACSharpSensor.IACSharpSensor.SensorReached(584);
          if (thech < 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(585);
            throw new IOException("unexpected end of stream");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(586);
          bsBuffShadow = (bsBuffShadow << 8) | thech;
          bsLiveShadow += 8;
        } while (bsLiveShadow < n);
        IACSharpSensor.IACSharpSensor.SensorReached(587);
        this.bsBuff = bsBuffShadow;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(588);
      this.bsLive = bsLiveShadow - n;
      System.Int32 RNTRNTRNT_25 = (bsBuffShadow >> (bsLiveShadow - n)) & ((1 << n) - 1);
      IACSharpSensor.IACSharpSensor.SensorReached(589);
      return RNTRNTRNT_25;
    }
    private bool bsGetBit()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(590);
      int bit = GetBits(1);
      System.Boolean RNTRNTRNT_26 = bit != 0;
      IACSharpSensor.IACSharpSensor.SensorReached(591);
      return RNTRNTRNT_26;
    }
    private char bsGetUByte()
    {
      System.Char RNTRNTRNT_27 = (char)GetBits(8);
      IACSharpSensor.IACSharpSensor.SensorReached(592);
      return RNTRNTRNT_27;
    }
    private uint bsGetInt()
    {
      System.UInt32 RNTRNTRNT_28 = (uint)((((((GetBits(8) << 8) | GetBits(8)) << 8) | GetBits(8)) << 8) | GetBits(8));
      IACSharpSensor.IACSharpSensor.SensorReached(593);
      return RNTRNTRNT_28;
    }
    private static void hbCreateDecodeTables(int[] limit, int[] bbase, int[] perm, char[] length, int minLen, int maxLen, int alphaSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(594);
      for (int i = minLen, pp = 0; i <= maxLen; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(595);
        for (int j = 0; j < alphaSize; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(596);
          if (length[j] == i) {
            IACSharpSensor.IACSharpSensor.SensorReached(597);
            perm[pp++] = j;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(598);
      for (int i = BZip2.MaxCodeLength; --i > 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(599);
        bbase[i] = 0;
        limit[i] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(600);
      for (int i = 0; i < alphaSize; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(601);
        bbase[length[i] + 1]++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(602);
      for (int i = 1, b = bbase[0]; i < BZip2.MaxCodeLength; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(603);
        b += bbase[i];
        bbase[i] = b;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(604);
      for (int i = minLen, vec = 0, b = bbase[i]; i <= maxLen; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(605);
        int nb = bbase[i + 1];
        vec += nb - b;
        b = nb;
        limit[i] = vec - 1;
        vec <<= 1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(606);
      for (int i = minLen + 1; i <= maxLen; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(607);
        bbase[i] = ((limit[i - 1] + 1) << 1) - bbase[i];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(608);
    }
    private void recvDecodingTables()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(609);
      var s = this.data;
      bool[] inUse = s.inUse;
      byte[] pos = s.recvDecodingTables_pos;
      int inUse16 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(610);
      for (int i = 0; i < 16; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(611);
        if (bsGetBit()) {
          IACSharpSensor.IACSharpSensor.SensorReached(612);
          inUse16 |= 1 << i;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(613);
      for (int i = 256; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(614);
        inUse[i] = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(615);
      for (int i = 0; i < 16; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(616);
        if ((inUse16 & (1 << i)) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(617);
          int i16 = i << 4;
          IACSharpSensor.IACSharpSensor.SensorReached(618);
          for (int j = 0; j < 16; j++) {
            IACSharpSensor.IACSharpSensor.SensorReached(619);
            if (bsGetBit()) {
              IACSharpSensor.IACSharpSensor.SensorReached(620);
              inUse[i16 + j] = true;
            }
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(621);
      MakeMaps();
      int alphaSize = this.nInUse + 2;
      int nGroups = GetBits(3);
      int nSelectors = GetBits(15);
      IACSharpSensor.IACSharpSensor.SensorReached(622);
      for (int i = 0; i < nSelectors; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(623);
        int j = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(624);
        while (bsGetBit()) {
          IACSharpSensor.IACSharpSensor.SensorReached(625);
          j++;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(626);
        s.selectorMtf[i] = (byte)j;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(627);
      for (int v = nGroups; --v >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(628);
        pos[v] = (byte)v;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(629);
      for (int i = 0; i < nSelectors; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(630);
        int v = s.selectorMtf[i];
        byte tmp = pos[v];
        IACSharpSensor.IACSharpSensor.SensorReached(631);
        while (v > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(632);
          pos[v] = pos[v - 1];
          v--;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(633);
        pos[0] = tmp;
        s.selector[i] = tmp;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(634);
      char[][] len = s.temp_charArray2d;
      IACSharpSensor.IACSharpSensor.SensorReached(635);
      for (int t = 0; t < nGroups; t++) {
        IACSharpSensor.IACSharpSensor.SensorReached(636);
        int curr = GetBits(5);
        char[] len_t = len[t];
        IACSharpSensor.IACSharpSensor.SensorReached(637);
        for (int i = 0; i < alphaSize; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(638);
          while (bsGetBit()) {
            IACSharpSensor.IACSharpSensor.SensorReached(639);
            curr += bsGetBit() ? -1 : 1;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(640);
          len_t[i] = (char)curr;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(641);
      createHuffmanDecodingTables(alphaSize, nGroups);
      IACSharpSensor.IACSharpSensor.SensorReached(642);
    }
    private void createHuffmanDecodingTables(int alphaSize, int nGroups)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(643);
      var s = this.data;
      char[][] len = s.temp_charArray2d;
      IACSharpSensor.IACSharpSensor.SensorReached(644);
      for (int t = 0; t < nGroups; t++) {
        IACSharpSensor.IACSharpSensor.SensorReached(645);
        int minLen = 32;
        int maxLen = 0;
        char[] len_t = len[t];
        IACSharpSensor.IACSharpSensor.SensorReached(646);
        for (int i = alphaSize; --i >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(647);
          char lent = len_t[i];
          IACSharpSensor.IACSharpSensor.SensorReached(648);
          if (lent > maxLen) {
            IACSharpSensor.IACSharpSensor.SensorReached(649);
            maxLen = lent;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(650);
          if (lent < minLen) {
            IACSharpSensor.IACSharpSensor.SensorReached(651);
            minLen = lent;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(652);
        hbCreateDecodeTables(s.gLimit[t], s.gBase[t], s.gPerm[t], len[t], minLen, maxLen, alphaSize);
        s.gMinlen[t] = minLen;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(653);
    }
    private void getAndMoveToFrontDecode()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(654);
      var s = this.data;
      this.origPtr = GetBits(24);
      IACSharpSensor.IACSharpSensor.SensorReached(655);
      if (this.origPtr < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(656);
        throw new IOException("BZ_DATA_ERROR");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(657);
      if (this.origPtr > 10 + BZip2.BlockSizeMultiple * this.blockSize100k) {
        IACSharpSensor.IACSharpSensor.SensorReached(658);
        throw new IOException("BZ_DATA_ERROR");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(659);
      recvDecodingTables();
      byte[] yy = s.getAndMoveToFrontDecode_yy;
      int limitLast = this.blockSize100k * BZip2.BlockSizeMultiple;
      IACSharpSensor.IACSharpSensor.SensorReached(660);
      for (int i = 256; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(661);
        yy[i] = (byte)i;
        s.unzftab[i] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(662);
      int groupNo = 0;
      int groupPos = BZip2.G_SIZE - 1;
      int eob = this.nInUse + 1;
      int nextSym = getAndMoveToFrontDecode0(0);
      int bsBuffShadow = this.bsBuff;
      int bsLiveShadow = this.bsLive;
      int lastShadow = -1;
      int zt = s.selector[groupNo] & 0xff;
      int[] base_zt = s.gBase[zt];
      int[] limit_zt = s.gLimit[zt];
      int[] perm_zt = s.gPerm[zt];
      int minLens_zt = s.gMinlen[zt];
      IACSharpSensor.IACSharpSensor.SensorReached(663);
      while (nextSym != eob) {
        IACSharpSensor.IACSharpSensor.SensorReached(664);
        if ((nextSym == BZip2.RUNA) || (nextSym == BZip2.RUNB)) {
          IACSharpSensor.IACSharpSensor.SensorReached(665);
          int es = -1;
          IACSharpSensor.IACSharpSensor.SensorReached(666);
          for (int n = 1; true; n <<= 1) {
            IACSharpSensor.IACSharpSensor.SensorReached(667);
            if (nextSym == BZip2.RUNA) {
              IACSharpSensor.IACSharpSensor.SensorReached(668);
              es += n;
            } else if (nextSym == BZip2.RUNB) {
              IACSharpSensor.IACSharpSensor.SensorReached(670);
              es += n << 1;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(669);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(671);
            if (groupPos == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(672);
              groupPos = BZip2.G_SIZE - 1;
              zt = s.selector[++groupNo] & 0xff;
              base_zt = s.gBase[zt];
              limit_zt = s.gLimit[zt];
              perm_zt = s.gPerm[zt];
              minLens_zt = s.gMinlen[zt];
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(673);
              groupPos--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(674);
            int zn = minLens_zt;
            IACSharpSensor.IACSharpSensor.SensorReached(675);
            while (bsLiveShadow < zn) {
              IACSharpSensor.IACSharpSensor.SensorReached(676);
              int thech = this.input.ReadByte();
              IACSharpSensor.IACSharpSensor.SensorReached(677);
              if (thech >= 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(678);
                bsBuffShadow = (bsBuffShadow << 8) | thech;
                bsLiveShadow += 8;
                IACSharpSensor.IACSharpSensor.SensorReached(679);
                continue;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(680);
                throw new IOException("unexpected end of stream");
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(681);
            int zvec = (bsBuffShadow >> (bsLiveShadow - zn)) & ((1 << zn) - 1);
            bsLiveShadow -= zn;
            IACSharpSensor.IACSharpSensor.SensorReached(682);
            while (zvec > limit_zt[zn]) {
              IACSharpSensor.IACSharpSensor.SensorReached(683);
              zn++;
              IACSharpSensor.IACSharpSensor.SensorReached(684);
              while (bsLiveShadow < 1) {
                IACSharpSensor.IACSharpSensor.SensorReached(685);
                int thech = this.input.ReadByte();
                IACSharpSensor.IACSharpSensor.SensorReached(686);
                if (thech >= 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(687);
                  bsBuffShadow = (bsBuffShadow << 8) | thech;
                  bsLiveShadow += 8;
                  IACSharpSensor.IACSharpSensor.SensorReached(688);
                  continue;
                } else {
                  IACSharpSensor.IACSharpSensor.SensorReached(689);
                  throw new IOException("unexpected end of stream");
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(690);
              bsLiveShadow--;
              zvec = (zvec << 1) | ((bsBuffShadow >> bsLiveShadow) & 1);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(691);
            nextSym = perm_zt[zvec - base_zt[zn]];
          }
          IACSharpSensor.IACSharpSensor.SensorReached(692);
          byte ch = s.seqToUnseq[yy[0]];
          s.unzftab[ch & 0xff] += es + 1;
          IACSharpSensor.IACSharpSensor.SensorReached(693);
          while (es-- >= 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(694);
            s.ll8[++lastShadow] = ch;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(695);
          if (lastShadow >= limitLast) {
            IACSharpSensor.IACSharpSensor.SensorReached(696);
            throw new IOException("block overrun");
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(697);
          if (++lastShadow >= limitLast) {
            IACSharpSensor.IACSharpSensor.SensorReached(698);
            throw new IOException("block overrun");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(699);
          byte tmp = yy[nextSym - 1];
          s.unzftab[s.seqToUnseq[tmp] & 0xff]++;
          s.ll8[lastShadow] = s.seqToUnseq[tmp];
          IACSharpSensor.IACSharpSensor.SensorReached(700);
          if (nextSym <= 16) {
            IACSharpSensor.IACSharpSensor.SensorReached(701);
            for (int j = nextSym - 1; j > 0;) {
              IACSharpSensor.IACSharpSensor.SensorReached(702);
              yy[j] = yy[--j];
            }
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(703);
            System.Buffer.BlockCopy(yy, 0, yy, 1, nextSym - 1);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(704);
          yy[0] = tmp;
          IACSharpSensor.IACSharpSensor.SensorReached(705);
          if (groupPos == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(706);
            groupPos = BZip2.G_SIZE - 1;
            zt = s.selector[++groupNo] & 0xff;
            base_zt = s.gBase[zt];
            limit_zt = s.gLimit[zt];
            perm_zt = s.gPerm[zt];
            minLens_zt = s.gMinlen[zt];
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(707);
            groupPos--;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(708);
          int zn = minLens_zt;
          IACSharpSensor.IACSharpSensor.SensorReached(709);
          while (bsLiveShadow < zn) {
            IACSharpSensor.IACSharpSensor.SensorReached(710);
            int thech = this.input.ReadByte();
            IACSharpSensor.IACSharpSensor.SensorReached(711);
            if (thech >= 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(712);
              bsBuffShadow = (bsBuffShadow << 8) | thech;
              bsLiveShadow += 8;
              IACSharpSensor.IACSharpSensor.SensorReached(713);
              continue;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(714);
              throw new IOException("unexpected end of stream");
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(715);
          int zvec = (bsBuffShadow >> (bsLiveShadow - zn)) & ((1 << zn) - 1);
          bsLiveShadow -= zn;
          IACSharpSensor.IACSharpSensor.SensorReached(716);
          while (zvec > limit_zt[zn]) {
            IACSharpSensor.IACSharpSensor.SensorReached(717);
            zn++;
            IACSharpSensor.IACSharpSensor.SensorReached(718);
            while (bsLiveShadow < 1) {
              IACSharpSensor.IACSharpSensor.SensorReached(719);
              int thech = this.input.ReadByte();
              IACSharpSensor.IACSharpSensor.SensorReached(720);
              if (thech >= 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(721);
                bsBuffShadow = (bsBuffShadow << 8) | thech;
                bsLiveShadow += 8;
                IACSharpSensor.IACSharpSensor.SensorReached(722);
                continue;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(723);
                throw new IOException("unexpected end of stream");
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(724);
            bsLiveShadow--;
            zvec = (zvec << 1) | ((bsBuffShadow >> bsLiveShadow) & 1);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(725);
          nextSym = perm_zt[zvec - base_zt[zn]];
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(726);
      this.last = lastShadow;
      this.bsLive = bsLiveShadow;
      this.bsBuff = bsBuffShadow;
      IACSharpSensor.IACSharpSensor.SensorReached(727);
    }
    private int getAndMoveToFrontDecode0(int groupNo)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(728);
      var s = this.data;
      int zt = s.selector[groupNo] & 0xff;
      int[] limit_zt = s.gLimit[zt];
      int zn = s.gMinlen[zt];
      int zvec = GetBits(zn);
      int bsLiveShadow = this.bsLive;
      int bsBuffShadow = this.bsBuff;
      IACSharpSensor.IACSharpSensor.SensorReached(729);
      while (zvec > limit_zt[zn]) {
        IACSharpSensor.IACSharpSensor.SensorReached(730);
        zn++;
        IACSharpSensor.IACSharpSensor.SensorReached(731);
        while (bsLiveShadow < 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(732);
          int thech = this.input.ReadByte();
          IACSharpSensor.IACSharpSensor.SensorReached(733);
          if (thech >= 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(734);
            bsBuffShadow = (bsBuffShadow << 8) | thech;
            bsLiveShadow += 8;
            IACSharpSensor.IACSharpSensor.SensorReached(735);
            continue;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(736);
            throw new IOException("unexpected end of stream");
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(737);
        bsLiveShadow--;
        zvec = (zvec << 1) | ((bsBuffShadow >> bsLiveShadow) & 1);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(738);
      this.bsLive = bsLiveShadow;
      this.bsBuff = bsBuffShadow;
      System.Int32 RNTRNTRNT_29 = s.gPerm[zt][zvec - s.gBase[zt][zn]];
      IACSharpSensor.IACSharpSensor.SensorReached(739);
      return RNTRNTRNT_29;
    }
    private void SetupBlock()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(740);
      if (this.data == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(741);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(742);
      int i;
      var s = this.data;
      int[] tt = s.initTT(this.last + 1);
      IACSharpSensor.IACSharpSensor.SensorReached(743);
      for (i = 0; i <= 255; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(744);
        if (s.unzftab[i] < 0 || s.unzftab[i] > this.last) {
          IACSharpSensor.IACSharpSensor.SensorReached(745);
          throw new Exception("BZ_DATA_ERROR");
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(746);
      s.cftab[0] = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(747);
      for (i = 1; i <= 256; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(748);
        s.cftab[i] = s.unzftab[i - 1];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(749);
      for (i = 1; i <= 256; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(750);
        s.cftab[i] += s.cftab[i - 1];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(751);
      for (i = 0; i <= 256; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(752);
        if (s.cftab[i] < 0 || s.cftab[i] > this.last + 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(753);
          var msg = String.Format("BZ_DATA_ERROR: cftab[{0}]={1} last={2}", i, s.cftab[i], this.last);
          IACSharpSensor.IACSharpSensor.SensorReached(754);
          throw new Exception(msg);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(755);
      for (i = 1; i <= 256; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(756);
        if (s.cftab[i - 1] > s.cftab[i]) {
          IACSharpSensor.IACSharpSensor.SensorReached(757);
          throw new Exception("BZ_DATA_ERROR");
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(758);
      int lastShadow;
      IACSharpSensor.IACSharpSensor.SensorReached(759);
      for (i = 0,lastShadow = this.last; i <= lastShadow; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(760);
        tt[s.cftab[s.ll8[i] & 0xff]++] = i;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(761);
      if ((this.origPtr < 0) || (this.origPtr >= tt.Length)) {
        IACSharpSensor.IACSharpSensor.SensorReached(762);
        throw new IOException("stream corrupted");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(763);
      this.su_tPos = tt[this.origPtr];
      this.su_count = 0;
      this.su_i2 = 0;
      this.su_ch2 = 256;
      IACSharpSensor.IACSharpSensor.SensorReached(764);
      if (this.blockRandomised) {
        IACSharpSensor.IACSharpSensor.SensorReached(765);
        this.su_rNToGo = 0;
        this.su_rTPos = 0;
        SetupRandPartA();
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(766);
        SetupNoRandPartA();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(767);
    }
    private void SetupRandPartA()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(768);
      if (this.su_i2 <= this.last) {
        IACSharpSensor.IACSharpSensor.SensorReached(769);
        this.su_chPrev = this.su_ch2;
        int su_ch2Shadow = this.data.ll8[this.su_tPos] & 0xff;
        this.su_tPos = this.data.tt[this.su_tPos];
        IACSharpSensor.IACSharpSensor.SensorReached(770);
        if (this.su_rNToGo == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(771);
          this.su_rNToGo = Rand.Rnums(this.su_rTPos) - 1;
          IACSharpSensor.IACSharpSensor.SensorReached(772);
          if (++this.su_rTPos == 512) {
            IACSharpSensor.IACSharpSensor.SensorReached(773);
            this.su_rTPos = 0;
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(774);
          this.su_rNToGo--;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(775);
        this.su_ch2 = su_ch2Shadow ^= (this.su_rNToGo == 1) ? 1 : 0;
        this.su_i2++;
        this.currentChar = su_ch2Shadow;
        this.currentState = CState.RAND_PART_B;
        this.crc.UpdateCRC((byte)su_ch2Shadow);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(776);
        EndBlock();
        InitBlock();
        SetupBlock();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(777);
    }
    private void SetupNoRandPartA()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(778);
      if (this.su_i2 <= this.last) {
        IACSharpSensor.IACSharpSensor.SensorReached(779);
        this.su_chPrev = this.su_ch2;
        int su_ch2Shadow = this.data.ll8[this.su_tPos] & 0xff;
        this.su_ch2 = su_ch2Shadow;
        this.su_tPos = this.data.tt[this.su_tPos];
        this.su_i2++;
        this.currentChar = su_ch2Shadow;
        this.currentState = CState.NO_RAND_PART_B;
        this.crc.UpdateCRC((byte)su_ch2Shadow);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(780);
        this.currentState = CState.NO_RAND_PART_A;
        EndBlock();
        InitBlock();
        SetupBlock();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(781);
    }
    private void SetupRandPartB()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(782);
      if (this.su_ch2 != this.su_chPrev) {
        IACSharpSensor.IACSharpSensor.SensorReached(783);
        this.currentState = CState.RAND_PART_A;
        this.su_count = 1;
        SetupRandPartA();
      } else if (++this.su_count >= 4) {
        IACSharpSensor.IACSharpSensor.SensorReached(785);
        this.su_z = (char)(this.data.ll8[this.su_tPos] & 0xff);
        this.su_tPos = this.data.tt[this.su_tPos];
        IACSharpSensor.IACSharpSensor.SensorReached(786);
        if (this.su_rNToGo == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(787);
          this.su_rNToGo = Rand.Rnums(this.su_rTPos) - 1;
          IACSharpSensor.IACSharpSensor.SensorReached(788);
          if (++this.su_rTPos == 512) {
            IACSharpSensor.IACSharpSensor.SensorReached(789);
            this.su_rTPos = 0;
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(790);
          this.su_rNToGo--;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(791);
        this.su_j2 = 0;
        this.currentState = CState.RAND_PART_C;
        IACSharpSensor.IACSharpSensor.SensorReached(792);
        if (this.su_rNToGo == 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(793);
          this.su_z ^= (char)1;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(794);
        SetupRandPartC();
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(784);
        this.currentState = CState.RAND_PART_A;
        SetupRandPartA();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(795);
    }
    private void SetupRandPartC()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(796);
      if (this.su_j2 < this.su_z) {
        IACSharpSensor.IACSharpSensor.SensorReached(797);
        this.currentChar = this.su_ch2;
        this.crc.UpdateCRC((byte)this.su_ch2);
        this.su_j2++;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(798);
        this.currentState = CState.RAND_PART_A;
        this.su_i2++;
        this.su_count = 0;
        SetupRandPartA();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(799);
    }
    private void SetupNoRandPartB()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(800);
      if (this.su_ch2 != this.su_chPrev) {
        IACSharpSensor.IACSharpSensor.SensorReached(801);
        this.su_count = 1;
        SetupNoRandPartA();
      } else if (++this.su_count >= 4) {
        IACSharpSensor.IACSharpSensor.SensorReached(803);
        this.su_z = (char)(this.data.ll8[this.su_tPos] & 0xff);
        this.su_tPos = this.data.tt[this.su_tPos];
        this.su_j2 = 0;
        SetupNoRandPartC();
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(802);
        SetupNoRandPartA();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(804);
    }
    private void SetupNoRandPartC()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(805);
      if (this.su_j2 < this.su_z) {
        IACSharpSensor.IACSharpSensor.SensorReached(806);
        int su_ch2Shadow = this.su_ch2;
        this.currentChar = su_ch2Shadow;
        this.crc.UpdateCRC((byte)su_ch2Shadow);
        this.su_j2++;
        this.currentState = CState.NO_RAND_PART_C;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(807);
        this.su_i2++;
        this.su_count = 0;
        SetupNoRandPartA();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(808);
    }
    private sealed class DecompressionState
    {
      public readonly bool[] inUse = new bool[256];
      public readonly byte[] seqToUnseq = new byte[256];
      public readonly byte[] selector = new byte[BZip2.MaxSelectors];
      public readonly byte[] selectorMtf = new byte[BZip2.MaxSelectors];
      public readonly int[] unzftab;
      public readonly int[][] gLimit;
      public readonly int[][] gBase;
      public readonly int[][] gPerm;
      public readonly int[] gMinlen;
      public readonly int[] cftab;
      public readonly byte[] getAndMoveToFrontDecode_yy;
      public readonly char[][] temp_charArray2d;
      public readonly byte[] recvDecodingTables_pos;
      public int[] tt;
      public byte[] ll8;
      public DecompressionState(int blockSize100k)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(809);
        this.unzftab = new int[256];
        this.gLimit = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.gBase = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.gPerm = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.gMinlen = new int[BZip2.NGroups];
        this.cftab = new int[257];
        this.getAndMoveToFrontDecode_yy = new byte[256];
        this.temp_charArray2d = BZip2.InitRectangularArray<char>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.recvDecodingTables_pos = new byte[BZip2.NGroups];
        this.ll8 = new byte[blockSize100k * BZip2.BlockSizeMultiple];
        IACSharpSensor.IACSharpSensor.SensorReached(810);
      }
      public int[] initTT(int length)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(811);
        int[] ttShadow = this.tt;
        IACSharpSensor.IACSharpSensor.SensorReached(812);
        if ((ttShadow == null) || (ttShadow.Length < length)) {
          IACSharpSensor.IACSharpSensor.SensorReached(813);
          this.tt = ttShadow = new int[length];
        }
        System.Int32[] RNTRNTRNT_30 = ttShadow;
        IACSharpSensor.IACSharpSensor.SensorReached(814);
        return RNTRNTRNT_30;
      }
    }
  }
  static internal class BZip2
  {
    static internal T[][] InitRectangularArray<T>(int d1, int d2)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(815);
      var x = new T[d1][];
      IACSharpSensor.IACSharpSensor.SensorReached(816);
      for (int i = 0; i < d1; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(817);
        x[i] = new T[d2];
      }
      T[][] RNTRNTRNT_31 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(818);
      return RNTRNTRNT_31;
    }
    public static readonly int BlockSizeMultiple = 100000;
    public static readonly int MinBlockSize = 1;
    public static readonly int MaxBlockSize = 9;
    public static readonly int MaxAlphaSize = 258;
    public static readonly int MaxCodeLength = 23;
    public static readonly char RUNA = (char)0;
    public static readonly char RUNB = (char)1;
    public static readonly int NGroups = 6;
    public static readonly int G_SIZE = 50;
    public static readonly int N_ITERS = 4;
    public static readonly int MaxSelectors = (2 + (900000 / G_SIZE));
    public static readonly int NUM_OVERSHOOT_BYTES = 20;
    static internal readonly int QSORT_STACK_SIZE = 1000;
  }
}
