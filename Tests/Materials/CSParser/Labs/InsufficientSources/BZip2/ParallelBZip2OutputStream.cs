using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
namespace Ionic.BZip2
{
  internal class WorkItem
  {
    public int index;
    public BZip2Compressor Compressor { get; private set; }
    public MemoryStream ms;
    public int ordinal;
    public BitWriter bw;
    public WorkItem(int ix, int blockSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(881);
      this.ms = new MemoryStream();
      this.bw = new BitWriter(ms);
      this.Compressor = new BZip2Compressor(bw, blockSize);
      this.index = ix;
      IACSharpSensor.IACSharpSensor.SensorReached(882);
    }
  }
  public class ParallelBZip2OutputStream : System.IO.Stream
  {
    private static readonly int BufferPairsPerCore = 4;
    private int _maxWorkers;
    private bool firstWriteDone;
    private int lastFilled;
    private int lastWritten;
    private int latestCompressed;
    private int currentlyFilling;
    private volatile Exception pendingException;
    private bool handlingException;
    private bool emitting;
    private System.Collections.Generic.Queue<int> toWrite;
    private System.Collections.Generic.Queue<int> toFill;
    private System.Collections.Generic.List<WorkItem> pool;
    private object latestLock = new object();
    private object eLock = new object();
    private object outputLock = new object();
    private AutoResetEvent newlyCompressedBlob;
    long totalBytesWrittenIn;
    long totalBytesWrittenOut;
    bool leaveOpen;
    uint combinedCRC;
    Stream output;
    BitWriter bw;
    int blockSize100k;
    private TraceBits desiredTrace = TraceBits.Crc | TraceBits.Write;
    public ParallelBZip2OutputStream(Stream output) : this(output, BZip2.MaxBlockSize, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(883);
    }
    public ParallelBZip2OutputStream(Stream output, int blockSize) : this(output, blockSize, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(884);
    }
    public ParallelBZip2OutputStream(Stream output, bool leaveOpen) : this(output, BZip2.MaxBlockSize, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(885);
    }
    public ParallelBZip2OutputStream(Stream output, int blockSize, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(886);
      if (blockSize < BZip2.MinBlockSize || blockSize > BZip2.MaxBlockSize) {
        IACSharpSensor.IACSharpSensor.SensorReached(887);
        var msg = String.Format("blockSize={0} is out of range; must be between {1} and {2}", blockSize, BZip2.MinBlockSize, BZip2.MaxBlockSize);
        IACSharpSensor.IACSharpSensor.SensorReached(888);
        throw new ArgumentException(msg, "blockSize");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(889);
      this.output = output;
      IACSharpSensor.IACSharpSensor.SensorReached(890);
      if (!this.output.CanWrite) {
        IACSharpSensor.IACSharpSensor.SensorReached(891);
        throw new ArgumentException("The stream is not writable.", "output");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(892);
      this.bw = new BitWriter(this.output);
      this.blockSize100k = blockSize;
      this.leaveOpen = leaveOpen;
      this.combinedCRC = 0;
      this.MaxWorkers = 16;
      EmitHeader();
      IACSharpSensor.IACSharpSensor.SensorReached(893);
    }
    private void InitializePoolOfWorkItems()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(894);
      this.toWrite = new Queue<int>();
      this.toFill = new Queue<int>();
      this.pool = new System.Collections.Generic.List<WorkItem>();
      int nWorkers = BufferPairsPerCore * Environment.ProcessorCount;
      nWorkers = Math.Min(nWorkers, this.MaxWorkers);
      IACSharpSensor.IACSharpSensor.SensorReached(895);
      for (int i = 0; i < nWorkers; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(896);
        this.pool.Add(new WorkItem(i, this.blockSize100k));
        this.toFill.Enqueue(i);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(897);
      this.newlyCompressedBlob = new AutoResetEvent(false);
      this.currentlyFilling = -1;
      this.lastFilled = -1;
      this.lastWritten = -1;
      this.latestCompressed = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(898);
    }
    public int MaxWorkers {
      get {
        System.Int32 RNTRNTRNT_37 = _maxWorkers;
        IACSharpSensor.IACSharpSensor.SensorReached(899);
        return RNTRNTRNT_37;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(900);
        if (value < 4) {
          IACSharpSensor.IACSharpSensor.SensorReached(901);
          throw new ArgumentException("MaxWorkers", "Value must be 4 or greater.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(902);
        _maxWorkers = value;
        IACSharpSensor.IACSharpSensor.SensorReached(903);
      }
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(904);
      if (this.pendingException != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(905);
        this.handlingException = true;
        var pe = this.pendingException;
        this.pendingException = null;
        IACSharpSensor.IACSharpSensor.SensorReached(906);
        throw pe;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(907);
      if (this.handlingException) {
        IACSharpSensor.IACSharpSensor.SensorReached(908);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(909);
      if (output == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(910);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(911);
      Stream o = this.output;
      IACSharpSensor.IACSharpSensor.SensorReached(912);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(913);
        FlushOutput(true);
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(914);
        this.output = null;
        this.bw = null;
        IACSharpSensor.IACSharpSensor.SensorReached(915);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(916);
      if (!leaveOpen) {
        IACSharpSensor.IACSharpSensor.SensorReached(917);
        o.Close();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(918);
    }
    private void FlushOutput(bool lastInput)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(919);
      if (this.emitting) {
        IACSharpSensor.IACSharpSensor.SensorReached(920);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(921);
      if (this.currentlyFilling >= 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(922);
        WorkItem workitem = this.pool[this.currentlyFilling];
        CompressOne(workitem);
        this.currentlyFilling = -1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(923);
      if (lastInput) {
        IACSharpSensor.IACSharpSensor.SensorReached(924);
        EmitPendingBuffers(true, false);
        EmitTrailer();
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(925);
        EmitPendingBuffers(false, false);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(926);
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(927);
      if (this.output != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(928);
        FlushOutput(false);
        this.bw.Flush();
        this.output.Flush();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(929);
    }
    private void EmitHeader()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(930);
      var magic = new byte[] {
        (byte)'B',
        (byte)'Z',
        (byte)'h',
        (byte)('0' + this.blockSize100k)
      };
      this.output.Write(magic, 0, magic.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(931);
    }
    private void EmitTrailer()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(932);
      TraceOutput(TraceBits.Write, "total written out: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      this.bw.WriteByte(0x17);
      this.bw.WriteByte(0x72);
      this.bw.WriteByte(0x45);
      this.bw.WriteByte(0x38);
      this.bw.WriteByte(0x50);
      this.bw.WriteByte(0x90);
      this.bw.WriteInt(this.combinedCRC);
      this.bw.FinishAndPad();
      TraceOutput(TraceBits.Write, "final total : {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      IACSharpSensor.IACSharpSensor.SensorReached(933);
    }
    public int BlockSize {
      get {
        System.Int32 RNTRNTRNT_38 = this.blockSize100k;
        IACSharpSensor.IACSharpSensor.SensorReached(934);
        return RNTRNTRNT_38;
      }
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(935);
      bool mustWait = false;
      IACSharpSensor.IACSharpSensor.SensorReached(936);
      if (this.output == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(937);
        throw new IOException("the stream is not open");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(938);
      if (this.pendingException != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(939);
        this.handlingException = true;
        var pe = this.pendingException;
        this.pendingException = null;
        IACSharpSensor.IACSharpSensor.SensorReached(940);
        throw pe;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(941);
      if (offset < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(942);
        throw new IndexOutOfRangeException(String.Format("offset ({0}) must be > 0", offset));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(943);
      if (count < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(944);
        throw new IndexOutOfRangeException(String.Format("count ({0}) must be > 0", count));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(945);
      if (offset + count > buffer.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(946);
        throw new IndexOutOfRangeException(String.Format("offset({0}) count({1}) bLength({2})", offset, count, buffer.Length));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(947);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(948);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(949);
      if (!this.firstWriteDone) {
        IACSharpSensor.IACSharpSensor.SensorReached(950);
        InitializePoolOfWorkItems();
        this.firstWriteDone = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(951);
      int bytesWritten = 0;
      int bytesRemaining = count;
      IACSharpSensor.IACSharpSensor.SensorReached(952);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(953);
        EmitPendingBuffers(false, mustWait);
        mustWait = false;
        int ix = -1;
        IACSharpSensor.IACSharpSensor.SensorReached(954);
        if (this.currentlyFilling >= 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(955);
          ix = this.currentlyFilling;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(956);
          if (this.toFill.Count == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(957);
            mustWait = true;
            IACSharpSensor.IACSharpSensor.SensorReached(958);
            continue;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(959);
          ix = this.toFill.Dequeue();
          ++this.lastFilled;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(960);
        WorkItem workitem = this.pool[ix];
        workitem.ordinal = this.lastFilled;
        int n = workitem.Compressor.Fill(buffer, offset, bytesRemaining);
        IACSharpSensor.IACSharpSensor.SensorReached(961);
        if (n != bytesRemaining) {
          IACSharpSensor.IACSharpSensor.SensorReached(962);
          if (!ThreadPool.QueueUserWorkItem(CompressOne, workitem)) {
            IACSharpSensor.IACSharpSensor.SensorReached(963);
            throw new Exception("Cannot enqueue workitem");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(964);
          this.currentlyFilling = -1;
          offset += n;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(965);
          this.currentlyFilling = ix;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(966);
        bytesRemaining -= n;
        bytesWritten += n;
      } while (bytesRemaining > 0);
      IACSharpSensor.IACSharpSensor.SensorReached(967);
      totalBytesWrittenIn += bytesWritten;
      IACSharpSensor.IACSharpSensor.SensorReached(968);
      return;
    }
    private void EmitPendingBuffers(bool doAll, bool mustWait)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(969);
      if (emitting) {
        IACSharpSensor.IACSharpSensor.SensorReached(970);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(971);
      emitting = true;
      IACSharpSensor.IACSharpSensor.SensorReached(972);
      if (doAll || mustWait) {
        IACSharpSensor.IACSharpSensor.SensorReached(973);
        this.newlyCompressedBlob.WaitOne();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(974);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(975);
        int firstSkip = -1;
        int millisecondsToWait = doAll ? 200 : (mustWait ? -1 : 0);
        int nextToWrite = -1;
        IACSharpSensor.IACSharpSensor.SensorReached(976);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(977);
          if (Monitor.TryEnter(this.toWrite, millisecondsToWait)) {
            IACSharpSensor.IACSharpSensor.SensorReached(978);
            nextToWrite = -1;
            IACSharpSensor.IACSharpSensor.SensorReached(979);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(980);
              if (this.toWrite.Count > 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(981);
                nextToWrite = this.toWrite.Dequeue();
              }
            } finally {
              IACSharpSensor.IACSharpSensor.SensorReached(982);
              Monitor.Exit(this.toWrite);
              IACSharpSensor.IACSharpSensor.SensorReached(983);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(984);
            if (nextToWrite >= 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(985);
              WorkItem workitem = this.pool[nextToWrite];
              IACSharpSensor.IACSharpSensor.SensorReached(986);
              if (workitem.ordinal != this.lastWritten + 1) {
                lock (this.toWrite) {
                  IACSharpSensor.IACSharpSensor.SensorReached(987);
                  this.toWrite.Enqueue(nextToWrite);
                }
                IACSharpSensor.IACSharpSensor.SensorReached(988);
                if (firstSkip == nextToWrite) {
                  IACSharpSensor.IACSharpSensor.SensorReached(989);
                  this.newlyCompressedBlob.WaitOne();
                  firstSkip = -1;
                } else if (firstSkip == -1) {
                  IACSharpSensor.IACSharpSensor.SensorReached(990);
                  firstSkip = nextToWrite;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(991);
                continue;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(992);
              firstSkip = -1;
              TraceOutput(TraceBits.Write, "Writing block {0}", workitem.ordinal);
              var bw2 = workitem.bw;
              bw2.Flush();
              var ms = workitem.ms;
              ms.Seek(0, SeekOrigin.Begin);
              int n;
              int y = -1;
              long totOut = 0;
              var buffer = new byte[1024];
              IACSharpSensor.IACSharpSensor.SensorReached(993);
              while ((n = ms.Read(buffer, 0, buffer.Length)) > 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(994);
                y = n;
                IACSharpSensor.IACSharpSensor.SensorReached(995);
                for (int k = 0; k < n; k++) {
                  IACSharpSensor.IACSharpSensor.SensorReached(996);
                  this.bw.WriteByte(buffer[k]);
                }
                IACSharpSensor.IACSharpSensor.SensorReached(997);
                totOut += n;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(998);
              TraceOutput(TraceBits.Write, " remaining bits: {0} 0x{1:X}", bw2.NumRemainingBits, bw2.RemainingBits);
              IACSharpSensor.IACSharpSensor.SensorReached(999);
              if (bw2.NumRemainingBits > 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(1000);
                this.bw.WriteBits(bw2.NumRemainingBits, bw2.RemainingBits);
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1001);
              TraceOutput(TraceBits.Crc, " combined CRC (before): {0:X8}", this.combinedCRC);
              this.combinedCRC = (this.combinedCRC << 1) | (this.combinedCRC >> 31);
              this.combinedCRC ^= (uint)workitem.Compressor.Crc32;
              TraceOutput(TraceBits.Crc, " block    CRC         : {0:X8}", workitem.Compressor.Crc32);
              TraceOutput(TraceBits.Crc, " combined CRC (after) : {0:X8}", this.combinedCRC);
              TraceOutput(TraceBits.Write, "total written out: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
              TraceOutput(TraceBits.Write | TraceBits.Crc, "");
              this.totalBytesWrittenOut += totOut;
              bw2.Reset();
              this.lastWritten = workitem.ordinal;
              workitem.ordinal = -1;
              this.toFill.Enqueue(workitem.index);
              IACSharpSensor.IACSharpSensor.SensorReached(1002);
              if (millisecondsToWait == -1) {
                IACSharpSensor.IACSharpSensor.SensorReached(1003);
                millisecondsToWait = 0;
              }
            }
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1004);
            nextToWrite = -1;
          }
        } while (nextToWrite >= 0);
      } while (doAll && (this.lastWritten != this.latestCompressed));
      IACSharpSensor.IACSharpSensor.SensorReached(1005);
      if (doAll) {
        IACSharpSensor.IACSharpSensor.SensorReached(1006);
        TraceOutput(TraceBits.Crc, " combined CRC (final) : {0:X8}", this.combinedCRC);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1007);
      emitting = false;
      IACSharpSensor.IACSharpSensor.SensorReached(1008);
    }
    private void CompressOne(Object wi)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1009);
      WorkItem workitem = (WorkItem)wi;
      IACSharpSensor.IACSharpSensor.SensorReached(1010);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(1011);
        workitem.Compressor.CompressAndWrite();
        lock (this.latestLock) {
          IACSharpSensor.IACSharpSensor.SensorReached(1012);
          if (workitem.ordinal > this.latestCompressed) {
            IACSharpSensor.IACSharpSensor.SensorReached(1013);
            this.latestCompressed = workitem.ordinal;
          }
        }
        lock (this.toWrite) {
          IACSharpSensor.IACSharpSensor.SensorReached(1014);
          this.toWrite.Enqueue(workitem.index);
        }
        this.newlyCompressedBlob.Set();
      } catch (System.Exception exc1) {
        lock (this.eLock) {
          IACSharpSensor.IACSharpSensor.SensorReached(1015);
          if (this.pendingException != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(1016);
            this.pendingException = exc1;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1017);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1018);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_39 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1019);
        return RNTRNTRNT_39;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_40 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1020);
        return RNTRNTRNT_40;
      }
    }
    public override bool CanWrite {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1021);
        if (this.output == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1022);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_41 = this.output.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(1023);
        return RNTRNTRNT_41;
      }
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1024);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_42 = this.totalBytesWrittenIn;
        IACSharpSensor.IACSharpSensor.SensorReached(1025);
        return RNTRNTRNT_42;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1026);
        throw new NotImplementedException();
      }
    }
    public Int64 BytesWrittenOut {
      get {
        Int64 RNTRNTRNT_43 = totalBytesWrittenOut;
        IACSharpSensor.IACSharpSensor.SensorReached(1027);
        return RNTRNTRNT_43;
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1028);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1029);
      throw new NotImplementedException();
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1030);
      throw new NotImplementedException();
    }
    [Flags()]
    enum TraceBits : uint
    {
      None = 0,
      Crc = 1,
      Write = 2,
      All = 0xffffffffu
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceOutput(TraceBits bits, string format, params object[] varParams)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1031);
      if ((bits & this.desiredTrace) != 0) {
        lock (outputLock) {
          IACSharpSensor.IACSharpSensor.SensorReached(1032);
          int tid = Thread.CurrentThread.GetHashCode();
          Console.ForegroundColor = (ConsoleColor)(tid % 8 + 10);
          Console.Write("{0:000} PBOS ", tid);
          Console.WriteLine(format, varParams);
          Console.ResetColor();
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1033);
    }
  }
}
