using System;
using System.IO;
namespace Ionic.BZip2
{
  internal class BZip2Compressor
  {
    private int blockSize100k;
    private int currentByte = -1;
    private int runLength = 0;
    private int last;
    private int outBlockFillThreshold;
    private CompressionState cstate;
    private readonly Ionic.Crc.CRC32 crc = new Ionic.Crc.CRC32(true);
    BitWriter bw;
    int runs;
    private int workDone;
    private int workLimit;
    private bool firstAttempt;
    private bool blockRandomised;
    private int origPtr;
    private int nInUse;
    private int nMTF;
    private static readonly int SETMASK = (1 << 21);
    private static readonly int CLEARMASK = (~SETMASK);
    private static readonly byte GREATER_ICOST = 15;
    private static readonly byte LESSER_ICOST = 0;
    private static readonly int SMALL_THRESH = 20;
    private static readonly int DEPTH_THRESH = 10;
    private static readonly int WORK_FACTOR = 30;
    private static readonly int[] increments = {
      1,
      4,
      13,
      40,
      121,
      364,
      1093,
      3280,
      9841,
      29524,
      88573,
      265720,
      797161,
      2391484
    };
    public BZip2Compressor(BitWriter writer) : this(writer, BZip2.MaxBlockSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(23);
    }
    public BZip2Compressor(BitWriter writer, int blockSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(24);
      this.blockSize100k = blockSize;
      this.bw = writer;
      this.outBlockFillThreshold = (blockSize * BZip2.BlockSizeMultiple) - 20;
      this.cstate = new CompressionState(blockSize);
      Reset();
      IACSharpSensor.IACSharpSensor.SensorReached(25);
    }
    void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(26);
      this.crc.Reset();
      this.currentByte = -1;
      this.runLength = 0;
      this.last = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(27);
      for (int i = 256; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(28);
        this.cstate.inUse[i] = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(29);
    }
    public int BlockSize {
      get {
        System.Int32 RNTRNTRNT_4 = this.blockSize100k;
        IACSharpSensor.IACSharpSensor.SensorReached(30);
        return RNTRNTRNT_4;
      }
    }
    public uint Crc32 { get; private set; }
    public int AvailableBytesOut { get; private set; }
    public int UncompressedBytes {
      get {
        System.Int32 RNTRNTRNT_5 = this.last + 1;
        IACSharpSensor.IACSharpSensor.SensorReached(31);
        return RNTRNTRNT_5;
      }
    }
    public int Fill(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(32);
      if (this.last >= this.outBlockFillThreshold) {
        System.Int32 RNTRNTRNT_6 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(33);
        return RNTRNTRNT_6;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(34);
      int bytesWritten = 0;
      int limit = offset + count;
      int rc;
      IACSharpSensor.IACSharpSensor.SensorReached(35);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(36);
        rc = write0(buffer[offset++]);
        IACSharpSensor.IACSharpSensor.SensorReached(37);
        if (rc > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(38);
          bytesWritten++;
        }
      } while (offset < limit && rc == 1);
      System.Int32 RNTRNTRNT_7 = bytesWritten;
      IACSharpSensor.IACSharpSensor.SensorReached(39);
      return RNTRNTRNT_7;
    }
    private int write0(byte b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(40);
      bool rc;
      IACSharpSensor.IACSharpSensor.SensorReached(41);
      if (this.currentByte == -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(42);
        this.currentByte = b;
        this.runLength++;
        System.Int32 RNTRNTRNT_8 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(43);
        return RNTRNTRNT_8;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(44);
      if (this.currentByte == b) {
        IACSharpSensor.IACSharpSensor.SensorReached(45);
        if (++this.runLength > 254) {
          IACSharpSensor.IACSharpSensor.SensorReached(46);
          rc = AddRunToOutputBlock(false);
          this.currentByte = -1;
          this.runLength = 0;
          System.Int32 RNTRNTRNT_9 = (rc) ? 2 : 1;
          IACSharpSensor.IACSharpSensor.SensorReached(47);
          return RNTRNTRNT_9;
        }
        System.Int32 RNTRNTRNT_10 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(48);
        return RNTRNTRNT_10;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(49);
      rc = AddRunToOutputBlock(false);
      IACSharpSensor.IACSharpSensor.SensorReached(50);
      if (rc) {
        IACSharpSensor.IACSharpSensor.SensorReached(51);
        this.currentByte = -1;
        this.runLength = 0;
        System.Int32 RNTRNTRNT_11 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(52);
        return RNTRNTRNT_11;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(53);
      this.runLength = 1;
      this.currentByte = b;
      System.Int32 RNTRNTRNT_12 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      return RNTRNTRNT_12;
    }
    private bool AddRunToOutputBlock(bool final)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(55);
      runs++;
      int previousLast = this.last;
      IACSharpSensor.IACSharpSensor.SensorReached(56);
      if (previousLast >= this.outBlockFillThreshold && !final) {
        IACSharpSensor.IACSharpSensor.SensorReached(57);
        var msg = String.Format("block overrun(final={2}): {0} >= threshold ({1})", previousLast, this.outBlockFillThreshold, final);
        IACSharpSensor.IACSharpSensor.SensorReached(58);
        throw new Exception(msg);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(59);
      byte b = (byte)this.currentByte;
      byte[] block = this.cstate.block;
      this.cstate.inUse[b] = true;
      int rl = this.runLength;
      this.crc.UpdateCRC(b, rl);
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      switch (rl) {
        case 1:
          IACSharpSensor.IACSharpSensor.SensorReached(61);
          block[previousLast + 2] = b;
          this.last = previousLast + 1;
          IACSharpSensor.IACSharpSensor.SensorReached(62);
          break;
        case 2:
          IACSharpSensor.IACSharpSensor.SensorReached(63);
          block[previousLast + 2] = b;
          block[previousLast + 3] = b;
          this.last = previousLast + 2;
          IACSharpSensor.IACSharpSensor.SensorReached(64);
          break;
        case 3:
          IACSharpSensor.IACSharpSensor.SensorReached(65);
          block[previousLast + 2] = b;
          block[previousLast + 3] = b;
          block[previousLast + 4] = b;
          this.last = previousLast + 3;
          IACSharpSensor.IACSharpSensor.SensorReached(66);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(67);
          rl -= 4;
          this.cstate.inUse[rl] = true;
          block[previousLast + 2] = b;
          block[previousLast + 3] = b;
          block[previousLast + 4] = b;
          block[previousLast + 5] = b;
          block[previousLast + 6] = (byte)rl;
          this.last = previousLast + 5;
          IACSharpSensor.IACSharpSensor.SensorReached(68);
          break;
      }
      System.Boolean RNTRNTRNT_13 = (this.last >= this.outBlockFillThreshold);
      IACSharpSensor.IACSharpSensor.SensorReached(69);
      return RNTRNTRNT_13;
    }
    public void CompressAndWrite()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(70);
      if (this.runLength > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(71);
        AddRunToOutputBlock(true);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(72);
      this.currentByte = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(73);
      if (this.last == -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(74);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(75);
      blockSort();
      this.bw.WriteByte(0x31);
      this.bw.WriteByte(0x41);
      this.bw.WriteByte(0x59);
      this.bw.WriteByte(0x26);
      this.bw.WriteByte(0x53);
      this.bw.WriteByte(0x59);
      this.Crc32 = (uint)this.crc.Crc32Result;
      this.bw.WriteInt(this.Crc32);
      this.bw.WriteBits(1, (this.blockRandomised) ? 1u : 0u);
      moveToFrontCodeAndSend();
      Reset();
      IACSharpSensor.IACSharpSensor.SensorReached(76);
    }
    private void randomiseBlock()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(77);
      bool[] inUse = this.cstate.inUse;
      byte[] block = this.cstate.block;
      int lastShadow = this.last;
      IACSharpSensor.IACSharpSensor.SensorReached(78);
      for (int i = 256; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(79);
        inUse[i] = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(80);
      int rNToGo = 0;
      int rTPos = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(81);
      for (int i = 0, j = 1; i <= lastShadow; i = j,j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(82);
        if (rNToGo == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(83);
          rNToGo = (char)Rand.Rnums(rTPos);
          IACSharpSensor.IACSharpSensor.SensorReached(84);
          if (++rTPos == 512) {
            IACSharpSensor.IACSharpSensor.SensorReached(85);
            rTPos = 0;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(86);
        rNToGo--;
        block[j] ^= (byte)((rNToGo == 1) ? 1 : 0);
        inUse[block[j] & 0xff] = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(87);
      this.blockRandomised = true;
      IACSharpSensor.IACSharpSensor.SensorReached(88);
    }
    private void mainSort()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(89);
      CompressionState dataShadow = this.cstate;
      int[] runningOrder = dataShadow.mainSort_runningOrder;
      int[] copy = dataShadow.mainSort_copy;
      bool[] bigDone = dataShadow.mainSort_bigDone;
      int[] ftab = dataShadow.ftab;
      byte[] block = dataShadow.block;
      int[] fmap = dataShadow.fmap;
      char[] quadrant = dataShadow.quadrant;
      int lastShadow = this.last;
      int workLimitShadow = this.workLimit;
      bool firstAttemptShadow = this.firstAttempt;
      IACSharpSensor.IACSharpSensor.SensorReached(90);
      for (int i = 65537; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(91);
        ftab[i] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(92);
      for (int i = 0; i < BZip2.NUM_OVERSHOOT_BYTES; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(93);
        block[lastShadow + i + 2] = block[(i % (lastShadow + 1)) + 1];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(94);
      for (int i = lastShadow + BZip2.NUM_OVERSHOOT_BYTES + 1; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(95);
        quadrant[i] = '\0';
      }
      IACSharpSensor.IACSharpSensor.SensorReached(96);
      block[0] = block[lastShadow + 1];
      int c1 = block[0] & 0xff;
      IACSharpSensor.IACSharpSensor.SensorReached(97);
      for (int i = 0; i <= lastShadow; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(98);
        int c2 = block[i + 1] & 0xff;
        ftab[(c1 << 8) + c2]++;
        c1 = c2;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(99);
      for (int i = 1; i <= 65536; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(100);
        ftab[i] += ftab[i - 1];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(101);
      c1 = block[1] & 0xff;
      IACSharpSensor.IACSharpSensor.SensorReached(102);
      for (int i = 0; i < lastShadow; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(103);
        int c2 = block[i + 2] & 0xff;
        fmap[--ftab[(c1 << 8) + c2]] = i;
        c1 = c2;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(104);
      fmap[--ftab[((block[lastShadow + 1] & 0xff) << 8) + (block[1] & 0xff)]] = lastShadow;
      IACSharpSensor.IACSharpSensor.SensorReached(105);
      for (int i = 256; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(106);
        bigDone[i] = false;
        runningOrder[i] = i;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(107);
      for (int h = 364; h != 1;) {
        IACSharpSensor.IACSharpSensor.SensorReached(108);
        h /= 3;
        IACSharpSensor.IACSharpSensor.SensorReached(109);
        for (int i = h; i <= 255; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(110);
          int vv = runningOrder[i];
          int a = ftab[(vv + 1) << 8] - ftab[vv << 8];
          int b = h - 1;
          int j = i;
          IACSharpSensor.IACSharpSensor.SensorReached(111);
          for (int ro = runningOrder[j - h]; (ftab[(ro + 1) << 8] - ftab[ro << 8]) > a; ro = runningOrder[j - h]) {
            IACSharpSensor.IACSharpSensor.SensorReached(112);
            runningOrder[j] = ro;
            j -= h;
            IACSharpSensor.IACSharpSensor.SensorReached(113);
            if (j <= b) {
              IACSharpSensor.IACSharpSensor.SensorReached(114);
              break;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(115);
          runningOrder[j] = vv;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(116);
      for (int i = 0; i <= 255; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(117);
        int ss = runningOrder[i];
        IACSharpSensor.IACSharpSensor.SensorReached(118);
        for (int j = 0; j <= 255; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(119);
          int sb = (ss << 8) + j;
          int ftab_sb = ftab[sb];
          IACSharpSensor.IACSharpSensor.SensorReached(120);
          if ((ftab_sb & SETMASK) != SETMASK) {
            IACSharpSensor.IACSharpSensor.SensorReached(121);
            int lo = ftab_sb & CLEARMASK;
            int hi = (ftab[sb + 1] & CLEARMASK) - 1;
            IACSharpSensor.IACSharpSensor.SensorReached(122);
            if (hi > lo) {
              IACSharpSensor.IACSharpSensor.SensorReached(123);
              mainQSort3(dataShadow, lo, hi, 2);
              IACSharpSensor.IACSharpSensor.SensorReached(124);
              if (firstAttemptShadow && (this.workDone > workLimitShadow)) {
                IACSharpSensor.IACSharpSensor.SensorReached(125);
                return;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(126);
            ftab[sb] = ftab_sb | SETMASK;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(127);
        for (int j = 0; j <= 255; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(128);
          copy[j] = ftab[(j << 8) + ss] & CLEARMASK;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(129);
        for (int j = ftab[ss << 8] & CLEARMASK, hj = (ftab[(ss + 1) << 8] & CLEARMASK); j < hj; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(130);
          int fmap_j = fmap[j];
          c1 = block[fmap_j] & 0xff;
          IACSharpSensor.IACSharpSensor.SensorReached(131);
          if (!bigDone[c1]) {
            IACSharpSensor.IACSharpSensor.SensorReached(132);
            fmap[copy[c1]] = (fmap_j == 0) ? lastShadow : (fmap_j - 1);
            copy[c1]++;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(133);
        for (int j = 256; --j >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(134);
          ftab[(j << 8) + ss] |= SETMASK;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(135);
        bigDone[ss] = true;
        IACSharpSensor.IACSharpSensor.SensorReached(136);
        if (i < 255) {
          IACSharpSensor.IACSharpSensor.SensorReached(137);
          int bbStart = ftab[ss << 8] & CLEARMASK;
          int bbSize = (ftab[(ss + 1) << 8] & CLEARMASK) - bbStart;
          int shifts = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(138);
          while ((bbSize >> shifts) > 65534) {
            IACSharpSensor.IACSharpSensor.SensorReached(139);
            shifts++;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(140);
          for (int j = 0; j < bbSize; j++) {
            IACSharpSensor.IACSharpSensor.SensorReached(141);
            int a2update = fmap[bbStart + j];
            char qVal = (char)(j >> shifts);
            quadrant[a2update] = qVal;
            IACSharpSensor.IACSharpSensor.SensorReached(142);
            if (a2update < BZip2.NUM_OVERSHOOT_BYTES) {
              IACSharpSensor.IACSharpSensor.SensorReached(143);
              quadrant[a2update + lastShadow + 1] = qVal;
            }
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(144);
    }
    private void blockSort()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(145);
      this.workLimit = WORK_FACTOR * this.last;
      this.workDone = 0;
      this.blockRandomised = false;
      this.firstAttempt = true;
      mainSort();
      IACSharpSensor.IACSharpSensor.SensorReached(146);
      if (this.firstAttempt && (this.workDone > this.workLimit)) {
        IACSharpSensor.IACSharpSensor.SensorReached(147);
        randomiseBlock();
        this.workLimit = this.workDone = 0;
        this.firstAttempt = false;
        mainSort();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(148);
      int[] fmap = this.cstate.fmap;
      this.origPtr = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(149);
      for (int i = 0, lastShadow = this.last; i <= lastShadow; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(150);
        if (fmap[i] == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(151);
          this.origPtr = i;
          IACSharpSensor.IACSharpSensor.SensorReached(152);
          break;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(153);
    }
    private bool mainSimpleSort(CompressionState dataShadow, int lo, int hi, int d)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(154);
      int bigN = hi - lo + 1;
      IACSharpSensor.IACSharpSensor.SensorReached(155);
      if (bigN < 2) {
        System.Boolean RNTRNTRNT_14 = this.firstAttempt && (this.workDone > this.workLimit);
        IACSharpSensor.IACSharpSensor.SensorReached(156);
        return RNTRNTRNT_14;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(157);
      int hp = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(158);
      while (increments[hp] < bigN) {
        IACSharpSensor.IACSharpSensor.SensorReached(159);
        hp++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(160);
      int[] fmap = dataShadow.fmap;
      char[] quadrant = dataShadow.quadrant;
      byte[] block = dataShadow.block;
      int lastShadow = this.last;
      int lastPlus1 = lastShadow + 1;
      bool firstAttemptShadow = this.firstAttempt;
      int workLimitShadow = this.workLimit;
      int workDoneShadow = this.workDone;
      IACSharpSensor.IACSharpSensor.SensorReached(161);
      while (--hp >= 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(162);
        int h = increments[hp];
        int mj = lo + h - 1;
        IACSharpSensor.IACSharpSensor.SensorReached(163);
        for (int i = lo + h; i <= hi;) {
          IACSharpSensor.IACSharpSensor.SensorReached(164);
          for (int k = 3; (i <= hi) && (--k >= 0); i++) {
            IACSharpSensor.IACSharpSensor.SensorReached(165);
            int v = fmap[i];
            int vd = v + d;
            int j = i;
            bool onceRunned = false;
            int a = 0;
            HAMMER:
            IACSharpSensor.IACSharpSensor.SensorReached(166);
            while (true) {
              IACSharpSensor.IACSharpSensor.SensorReached(167);
              if (onceRunned) {
                IACSharpSensor.IACSharpSensor.SensorReached(168);
                fmap[j] = a;
                IACSharpSensor.IACSharpSensor.SensorReached(169);
                if ((j -= h) <= mj) {
                  IACSharpSensor.IACSharpSensor.SensorReached(170);
                  goto END_HAMMER;
                }
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(171);
                onceRunned = true;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(172);
              a = fmap[j - h];
              int i1 = a + d;
              int i2 = vd;
              IACSharpSensor.IACSharpSensor.SensorReached(173);
              if (block[i1 + 1] == block[i2 + 1]) {
                IACSharpSensor.IACSharpSensor.SensorReached(174);
                if (block[i1 + 2] == block[i2 + 2]) {
                  IACSharpSensor.IACSharpSensor.SensorReached(175);
                  if (block[i1 + 3] == block[i2 + 3]) {
                    IACSharpSensor.IACSharpSensor.SensorReached(176);
                    if (block[i1 + 4] == block[i2 + 4]) {
                      IACSharpSensor.IACSharpSensor.SensorReached(177);
                      if (block[i1 + 5] == block[i2 + 5]) {
                        IACSharpSensor.IACSharpSensor.SensorReached(178);
                        if (block[(i1 += 6)] == block[(i2 += 6)]) {
                          IACSharpSensor.IACSharpSensor.SensorReached(179);
                          int x = lastShadow;
                          X:
                          IACSharpSensor.IACSharpSensor.SensorReached(180);
                          while (x > 0) {
                            IACSharpSensor.IACSharpSensor.SensorReached(181);
                            x -= 4;
                            IACSharpSensor.IACSharpSensor.SensorReached(182);
                            if (block[i1 + 1] == block[i2 + 1]) {
                              IACSharpSensor.IACSharpSensor.SensorReached(183);
                              if (quadrant[i1] == quadrant[i2]) {
                                IACSharpSensor.IACSharpSensor.SensorReached(184);
                                if (block[i1 + 2] == block[i2 + 2]) {
                                  IACSharpSensor.IACSharpSensor.SensorReached(185);
                                  if (quadrant[i1 + 1] == quadrant[i2 + 1]) {
                                    IACSharpSensor.IACSharpSensor.SensorReached(186);
                                    if (block[i1 + 3] == block[i2 + 3]) {
                                      IACSharpSensor.IACSharpSensor.SensorReached(187);
                                      if (quadrant[i1 + 2] == quadrant[i2 + 2]) {
                                        IACSharpSensor.IACSharpSensor.SensorReached(188);
                                        if (block[i1 + 4] == block[i2 + 4]) {
                                          IACSharpSensor.IACSharpSensor.SensorReached(189);
                                          if (quadrant[i1 + 3] == quadrant[i2 + 3]) {
                                            IACSharpSensor.IACSharpSensor.SensorReached(190);
                                            if ((i1 += 4) >= lastPlus1) {
                                              IACSharpSensor.IACSharpSensor.SensorReached(191);
                                              i1 -= lastPlus1;
                                            }
                                            IACSharpSensor.IACSharpSensor.SensorReached(192);
                                            if ((i2 += 4) >= lastPlus1) {
                                              IACSharpSensor.IACSharpSensor.SensorReached(193);
                                              i2 -= lastPlus1;
                                            }
                                            IACSharpSensor.IACSharpSensor.SensorReached(194);
                                            workDoneShadow++;
                                            IACSharpSensor.IACSharpSensor.SensorReached(195);
                                            goto X;
                                          } else if ((quadrant[i1 + 3] > quadrant[i2 + 3])) {
                                            IACSharpSensor.IACSharpSensor.SensorReached(197);
                                            goto HAMMER;
                                          } else {
                                            IACSharpSensor.IACSharpSensor.SensorReached(196);
                                            goto END_HAMMER;
                                          }
                                        } else if ((block[i1 + 4] & 0xff) > (block[i2 + 4] & 0xff)) {
                                          IACSharpSensor.IACSharpSensor.SensorReached(199);
                                          goto HAMMER;
                                        } else {
                                          IACSharpSensor.IACSharpSensor.SensorReached(198);
                                          goto END_HAMMER;
                                        }
                                      } else if ((quadrant[i1 + 2] > quadrant[i2 + 2])) {
                                        IACSharpSensor.IACSharpSensor.SensorReached(201);
                                        goto HAMMER;
                                      } else {
                                        IACSharpSensor.IACSharpSensor.SensorReached(200);
                                        goto END_HAMMER;
                                      }
                                    } else if ((block[i1 + 3] & 0xff) > (block[i2 + 3] & 0xff)) {
                                      IACSharpSensor.IACSharpSensor.SensorReached(203);
                                      goto HAMMER;
                                    } else {
                                      IACSharpSensor.IACSharpSensor.SensorReached(202);
                                      goto END_HAMMER;
                                    }
                                  } else if ((quadrant[i1 + 1] > quadrant[i2 + 1])) {
                                    IACSharpSensor.IACSharpSensor.SensorReached(205);
                                    goto HAMMER;
                                  } else {
                                    IACSharpSensor.IACSharpSensor.SensorReached(204);
                                    goto END_HAMMER;
                                  }
                                } else if ((block[i1 + 2] & 0xff) > (block[i2 + 2] & 0xff)) {
                                  IACSharpSensor.IACSharpSensor.SensorReached(207);
                                  goto HAMMER;
                                } else {
                                  IACSharpSensor.IACSharpSensor.SensorReached(206);
                                  goto END_HAMMER;
                                }
                              } else if ((quadrant[i1] > quadrant[i2])) {
                                IACSharpSensor.IACSharpSensor.SensorReached(209);
                                goto HAMMER;
                              } else {
                                IACSharpSensor.IACSharpSensor.SensorReached(208);
                                goto END_HAMMER;
                              }
                            } else if ((block[i1 + 1] & 0xff) > (block[i2 + 1] & 0xff)) {
                              IACSharpSensor.IACSharpSensor.SensorReached(211);
                              goto HAMMER;
                            } else {
                              IACSharpSensor.IACSharpSensor.SensorReached(210);
                              goto END_HAMMER;
                            }
                          }
                          IACSharpSensor.IACSharpSensor.SensorReached(212);
                          goto END_HAMMER;
                        } else {
                          IACSharpSensor.IACSharpSensor.SensorReached(213);
                          if ((block[i1] & 0xff) > (block[i2] & 0xff)) {
                            IACSharpSensor.IACSharpSensor.SensorReached(214);
                            goto HAMMER;
                          } else {
                            IACSharpSensor.IACSharpSensor.SensorReached(215);
                            goto END_HAMMER;
                          }
                        }
                      } else if ((block[i1 + 5] & 0xff) > (block[i2 + 5] & 0xff)) {
                        IACSharpSensor.IACSharpSensor.SensorReached(217);
                        goto HAMMER;
                      } else {
                        IACSharpSensor.IACSharpSensor.SensorReached(216);
                        goto END_HAMMER;
                      }
                    } else if ((block[i1 + 4] & 0xff) > (block[i2 + 4] & 0xff)) {
                      IACSharpSensor.IACSharpSensor.SensorReached(219);
                      goto HAMMER;
                    } else {
                      IACSharpSensor.IACSharpSensor.SensorReached(218);
                      goto END_HAMMER;
                    }
                  } else if ((block[i1 + 3] & 0xff) > (block[i2 + 3] & 0xff)) {
                    IACSharpSensor.IACSharpSensor.SensorReached(221);
                    goto HAMMER;
                  } else {
                    IACSharpSensor.IACSharpSensor.SensorReached(220);
                    goto END_HAMMER;
                  }
                } else if ((block[i1 + 2] & 0xff) > (block[i2 + 2] & 0xff)) {
                  IACSharpSensor.IACSharpSensor.SensorReached(223);
                  goto HAMMER;
                } else {
                  IACSharpSensor.IACSharpSensor.SensorReached(222);
                  goto END_HAMMER;
                }
              } else if ((block[i1 + 1] & 0xff) > (block[i2 + 1] & 0xff)) {
                IACSharpSensor.IACSharpSensor.SensorReached(225);
                goto HAMMER;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(224);
                goto END_HAMMER;
              }
            }
            END_HAMMER:
            IACSharpSensor.IACSharpSensor.SensorReached(226);
            fmap[j] = v;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(227);
          if (firstAttemptShadow && (i <= hi) && (workDoneShadow > workLimitShadow)) {
            IACSharpSensor.IACSharpSensor.SensorReached(228);
            goto END_HP;
          }
        }
      }
      END_HP:
      IACSharpSensor.IACSharpSensor.SensorReached(229);
      this.workDone = workDoneShadow;
      System.Boolean RNTRNTRNT_15 = firstAttemptShadow && (workDoneShadow > workLimitShadow);
      IACSharpSensor.IACSharpSensor.SensorReached(230);
      return RNTRNTRNT_15;
    }
    private static void vswap(int[] fmap, int p1, int p2, int n)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(231);
      n += p1;
      IACSharpSensor.IACSharpSensor.SensorReached(232);
      while (p1 < n) {
        IACSharpSensor.IACSharpSensor.SensorReached(233);
        int t = fmap[p1];
        fmap[p1++] = fmap[p2];
        fmap[p2++] = t;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(234);
    }
    private static byte med3(byte a, byte b, byte c)
    {
      System.Byte RNTRNTRNT_16 = (a < b) ? (b < c ? b : a < c ? c : a) : (b > c ? b : a > c ? c : a);
      IACSharpSensor.IACSharpSensor.SensorReached(235);
      return RNTRNTRNT_16;
    }
    private void mainQSort3(CompressionState dataShadow, int loSt, int hiSt, int dSt)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(236);
      int[] stack_ll = dataShadow.stack_ll;
      int[] stack_hh = dataShadow.stack_hh;
      int[] stack_dd = dataShadow.stack_dd;
      int[] fmap = dataShadow.fmap;
      byte[] block = dataShadow.block;
      stack_ll[0] = loSt;
      stack_hh[0] = hiSt;
      stack_dd[0] = dSt;
      IACSharpSensor.IACSharpSensor.SensorReached(237);
      for (int sp = 1; --sp >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(238);
        int lo = stack_ll[sp];
        int hi = stack_hh[sp];
        int d = stack_dd[sp];
        IACSharpSensor.IACSharpSensor.SensorReached(239);
        if ((hi - lo < SMALL_THRESH) || (d > DEPTH_THRESH)) {
          IACSharpSensor.IACSharpSensor.SensorReached(240);
          if (mainSimpleSort(dataShadow, lo, hi, d)) {
            IACSharpSensor.IACSharpSensor.SensorReached(241);
            return;
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(242);
          int d1 = d + 1;
          int med = med3(block[fmap[lo] + d1], block[fmap[hi] + d1], block[fmap[(lo + hi) >> 1] + d1]) & 0xff;
          int unLo = lo;
          int unHi = hi;
          int ltLo = lo;
          int gtHi = hi;
          IACSharpSensor.IACSharpSensor.SensorReached(243);
          while (true) {
            IACSharpSensor.IACSharpSensor.SensorReached(244);
            while (unLo <= unHi) {
              IACSharpSensor.IACSharpSensor.SensorReached(245);
              int n = (block[fmap[unLo] + d1] & 0xff) - med;
              IACSharpSensor.IACSharpSensor.SensorReached(246);
              if (n == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(247);
                int temp = fmap[unLo];
                fmap[unLo++] = fmap[ltLo];
                fmap[ltLo++] = temp;
              } else if (n < 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(249);
                unLo++;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(248);
                break;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(250);
            while (unLo <= unHi) {
              IACSharpSensor.IACSharpSensor.SensorReached(251);
              int n = (block[fmap[unHi] + d1] & 0xff) - med;
              IACSharpSensor.IACSharpSensor.SensorReached(252);
              if (n == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(253);
                int temp = fmap[unHi];
                fmap[unHi--] = fmap[gtHi];
                fmap[gtHi--] = temp;
              } else if (n > 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(255);
                unHi--;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(254);
                break;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(256);
            if (unLo <= unHi) {
              IACSharpSensor.IACSharpSensor.SensorReached(257);
              int temp = fmap[unLo];
              fmap[unLo++] = fmap[unHi];
              fmap[unHi--] = temp;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(258);
              break;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(259);
          if (gtHi < ltLo) {
            IACSharpSensor.IACSharpSensor.SensorReached(260);
            stack_ll[sp] = lo;
            stack_hh[sp] = hi;
            stack_dd[sp] = d1;
            sp++;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(261);
            int n = ((ltLo - lo) < (unLo - ltLo)) ? (ltLo - lo) : (unLo - ltLo);
            vswap(fmap, lo, unLo - n, n);
            int m = ((hi - gtHi) < (gtHi - unHi)) ? (hi - gtHi) : (gtHi - unHi);
            vswap(fmap, unLo, hi - m + 1, m);
            n = lo + unLo - ltLo - 1;
            m = hi - (gtHi - unHi) + 1;
            stack_ll[sp] = lo;
            stack_hh[sp] = n;
            stack_dd[sp] = d;
            sp++;
            stack_ll[sp] = n + 1;
            stack_hh[sp] = m - 1;
            stack_dd[sp] = d1;
            sp++;
            stack_ll[sp] = m;
            stack_hh[sp] = hi;
            stack_dd[sp] = d;
            sp++;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(262);
    }
    private void generateMTFValues()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(263);
      int lastShadow = this.last;
      CompressionState dataShadow = this.cstate;
      bool[] inUse = dataShadow.inUse;
      byte[] block = dataShadow.block;
      int[] fmap = dataShadow.fmap;
      char[] sfmap = dataShadow.sfmap;
      int[] mtfFreq = dataShadow.mtfFreq;
      byte[] unseqToSeq = dataShadow.unseqToSeq;
      byte[] yy = dataShadow.generateMTFValues_yy;
      int nInUseShadow = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(264);
      for (int i = 0; i < 256; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(265);
        if (inUse[i]) {
          IACSharpSensor.IACSharpSensor.SensorReached(266);
          unseqToSeq[i] = (byte)nInUseShadow;
          nInUseShadow++;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(267);
      this.nInUse = nInUseShadow;
      int eob = nInUseShadow + 1;
      IACSharpSensor.IACSharpSensor.SensorReached(268);
      for (int i = eob; i >= 0; i--) {
        IACSharpSensor.IACSharpSensor.SensorReached(269);
        mtfFreq[i] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(270);
      for (int i = nInUseShadow; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(271);
        yy[i] = (byte)i;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(272);
      int wr = 0;
      int zPend = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(273);
      for (int i = 0; i <= lastShadow; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(274);
        byte ll_i = unseqToSeq[block[fmap[i]] & 0xff];
        byte tmp = yy[0];
        int j = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(275);
        while (ll_i != tmp) {
          IACSharpSensor.IACSharpSensor.SensorReached(276);
          j++;
          byte tmp2 = tmp;
          tmp = yy[j];
          yy[j] = tmp2;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(277);
        yy[0] = tmp;
        IACSharpSensor.IACSharpSensor.SensorReached(278);
        if (j == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(279);
          zPend++;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(280);
          if (zPend > 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(281);
            zPend--;
            IACSharpSensor.IACSharpSensor.SensorReached(282);
            while (true) {
              IACSharpSensor.IACSharpSensor.SensorReached(283);
              if ((zPend & 1) == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(284);
                sfmap[wr] = BZip2.RUNA;
                wr++;
                mtfFreq[BZip2.RUNA]++;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(285);
                sfmap[wr] = BZip2.RUNB;
                wr++;
                mtfFreq[BZip2.RUNB]++;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(286);
              if (zPend >= 2) {
                IACSharpSensor.IACSharpSensor.SensorReached(287);
                zPend = (zPend - 2) >> 1;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(288);
                break;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(289);
            zPend = 0;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(290);
          sfmap[wr] = (char)(j + 1);
          wr++;
          mtfFreq[j + 1]++;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(291);
      if (zPend > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(292);
        zPend--;
        IACSharpSensor.IACSharpSensor.SensorReached(293);
        while (true) {
          IACSharpSensor.IACSharpSensor.SensorReached(294);
          if ((zPend & 1) == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(295);
            sfmap[wr] = BZip2.RUNA;
            wr++;
            mtfFreq[BZip2.RUNA]++;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(296);
            sfmap[wr] = BZip2.RUNB;
            wr++;
            mtfFreq[BZip2.RUNB]++;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(297);
          if (zPend >= 2) {
            IACSharpSensor.IACSharpSensor.SensorReached(298);
            zPend = (zPend - 2) >> 1;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(299);
            break;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(300);
      sfmap[wr] = (char)eob;
      mtfFreq[eob]++;
      this.nMTF = wr + 1;
      IACSharpSensor.IACSharpSensor.SensorReached(301);
    }
    private static void hbAssignCodes(int[] code, byte[] length, int minLen, int maxLen, int alphaSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(302);
      int vec = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(303);
      for (int n = minLen; n <= maxLen; n++) {
        IACSharpSensor.IACSharpSensor.SensorReached(304);
        for (int i = 0; i < alphaSize; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(305);
          if ((length[i] & 0xff) == n) {
            IACSharpSensor.IACSharpSensor.SensorReached(306);
            code[i] = vec;
            vec++;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(307);
        vec <<= 1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(308);
    }
    private void sendMTFValues()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(309);
      byte[][] len = this.cstate.sendMTFValues_len;
      int alphaSize = this.nInUse + 2;
      IACSharpSensor.IACSharpSensor.SensorReached(310);
      for (int t = BZip2.NGroups; --t >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(311);
        byte[] len_t = len[t];
        IACSharpSensor.IACSharpSensor.SensorReached(312);
        for (int v = alphaSize; --v >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(313);
          len_t[v] = GREATER_ICOST;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(314);
      int nGroups = (this.nMTF < 200) ? 2 : (this.nMTF < 600) ? 3 : (this.nMTF < 1200) ? 4 : (this.nMTF < 2400) ? 5 : 6;
      sendMTFValues0(nGroups, alphaSize);
      int nSelectors = sendMTFValues1(nGroups, alphaSize);
      sendMTFValues2(nGroups, nSelectors);
      sendMTFValues3(nGroups, alphaSize);
      sendMTFValues4();
      sendMTFValues5(nGroups, nSelectors);
      sendMTFValues6(nGroups, alphaSize);
      sendMTFValues7(nSelectors);
      IACSharpSensor.IACSharpSensor.SensorReached(315);
    }
    private void sendMTFValues0(int nGroups, int alphaSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(316);
      byte[][] len = this.cstate.sendMTFValues_len;
      int[] mtfFreq = this.cstate.mtfFreq;
      int remF = this.nMTF;
      int gs = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(317);
      for (int nPart = nGroups; nPart > 0; nPart--) {
        IACSharpSensor.IACSharpSensor.SensorReached(318);
        int tFreq = remF / nPart;
        int ge = gs - 1;
        int aFreq = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(319);
        for (int a = alphaSize - 1; (aFreq < tFreq) && (ge < a);) {
          IACSharpSensor.IACSharpSensor.SensorReached(320);
          aFreq += mtfFreq[++ge];
        }
        IACSharpSensor.IACSharpSensor.SensorReached(321);
        if ((ge > gs) && (nPart != nGroups) && (nPart != 1) && (((nGroups - nPart) & 1) != 0)) {
          IACSharpSensor.IACSharpSensor.SensorReached(322);
          aFreq -= mtfFreq[ge--];
        }
        IACSharpSensor.IACSharpSensor.SensorReached(323);
        byte[] len_np = len[nPart - 1];
        IACSharpSensor.IACSharpSensor.SensorReached(324);
        for (int v = alphaSize; --v >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(325);
          if ((v >= gs) && (v <= ge)) {
            IACSharpSensor.IACSharpSensor.SensorReached(326);
            len_np[v] = LESSER_ICOST;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(327);
            len_np[v] = GREATER_ICOST;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(328);
        gs = ge + 1;
        remF -= aFreq;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(329);
    }
    private static void hbMakeCodeLengths(byte[] len, int[] freq, CompressionState state1, int alphaSize, int maxLen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(330);
      int[] heap = state1.heap;
      int[] weight = state1.weight;
      int[] parent = state1.parent;
      IACSharpSensor.IACSharpSensor.SensorReached(331);
      for (int i = alphaSize; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(332);
        weight[i + 1] = (freq[i] == 0 ? 1 : freq[i]) << 8;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(333);
      for (bool tooLong = true; tooLong;) {
        IACSharpSensor.IACSharpSensor.SensorReached(334);
        tooLong = false;
        int nNodes = alphaSize;
        int nHeap = 0;
        heap[0] = 0;
        weight[0] = 0;
        parent[0] = -2;
        IACSharpSensor.IACSharpSensor.SensorReached(335);
        for (int i = 1; i <= alphaSize; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(336);
          parent[i] = -1;
          nHeap++;
          heap[nHeap] = i;
          int zz = nHeap;
          int tmp = heap[zz];
          IACSharpSensor.IACSharpSensor.SensorReached(337);
          while (weight[tmp] < weight[heap[zz >> 1]]) {
            IACSharpSensor.IACSharpSensor.SensorReached(338);
            heap[zz] = heap[zz >> 1];
            zz >>= 1;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(339);
          heap[zz] = tmp;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(340);
        while (nHeap > 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(341);
          int n1 = heap[1];
          heap[1] = heap[nHeap];
          nHeap--;
          int yy = 0;
          int zz = 1;
          int tmp = heap[1];
          IACSharpSensor.IACSharpSensor.SensorReached(342);
          while (true) {
            IACSharpSensor.IACSharpSensor.SensorReached(343);
            yy = zz << 1;
            IACSharpSensor.IACSharpSensor.SensorReached(344);
            if (yy > nHeap) {
              IACSharpSensor.IACSharpSensor.SensorReached(345);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(346);
            if ((yy < nHeap) && (weight[heap[yy + 1]] < weight[heap[yy]])) {
              IACSharpSensor.IACSharpSensor.SensorReached(347);
              yy++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(348);
            if (weight[tmp] < weight[heap[yy]]) {
              IACSharpSensor.IACSharpSensor.SensorReached(349);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(350);
            heap[zz] = heap[yy];
            zz = yy;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(351);
          heap[zz] = tmp;
          int n2 = heap[1];
          heap[1] = heap[nHeap];
          nHeap--;
          yy = 0;
          zz = 1;
          tmp = heap[1];
          IACSharpSensor.IACSharpSensor.SensorReached(352);
          while (true) {
            IACSharpSensor.IACSharpSensor.SensorReached(353);
            yy = zz << 1;
            IACSharpSensor.IACSharpSensor.SensorReached(354);
            if (yy > nHeap) {
              IACSharpSensor.IACSharpSensor.SensorReached(355);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(356);
            if ((yy < nHeap) && (weight[heap[yy + 1]] < weight[heap[yy]])) {
              IACSharpSensor.IACSharpSensor.SensorReached(357);
              yy++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(358);
            if (weight[tmp] < weight[heap[yy]]) {
              IACSharpSensor.IACSharpSensor.SensorReached(359);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(360);
            heap[zz] = heap[yy];
            zz = yy;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(361);
          heap[zz] = tmp;
          nNodes++;
          parent[n1] = parent[n2] = nNodes;
          int weight_n1 = weight[n1];
          int weight_n2 = weight[n2];
          weight[nNodes] = (int)(((uint)weight_n1 & 0xffffff00u) + ((uint)weight_n2 & 0xffffff00u)) | (1 + (((weight_n1 & 0xff) > (weight_n2 & 0xff)) ? (weight_n1 & 0xff) : (weight_n2 & 0xff)));
          parent[nNodes] = -1;
          nHeap++;
          heap[nHeap] = nNodes;
          tmp = 0;
          zz = nHeap;
          tmp = heap[zz];
          int weight_tmp = weight[tmp];
          IACSharpSensor.IACSharpSensor.SensorReached(362);
          while (weight_tmp < weight[heap[zz >> 1]]) {
            IACSharpSensor.IACSharpSensor.SensorReached(363);
            heap[zz] = heap[zz >> 1];
            zz >>= 1;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(364);
          heap[zz] = tmp;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(365);
        for (int i = 1; i <= alphaSize; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(366);
          int j = 0;
          int k = i;
          IACSharpSensor.IACSharpSensor.SensorReached(367);
          for (int parent_k; (parent_k = parent[k]) >= 0;) {
            IACSharpSensor.IACSharpSensor.SensorReached(368);
            k = parent_k;
            j++;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(369);
          len[i - 1] = (byte)j;
          IACSharpSensor.IACSharpSensor.SensorReached(370);
          if (j > maxLen) {
            IACSharpSensor.IACSharpSensor.SensorReached(371);
            tooLong = true;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(372);
        if (tooLong) {
          IACSharpSensor.IACSharpSensor.SensorReached(373);
          for (int i = 1; i < alphaSize; i++) {
            IACSharpSensor.IACSharpSensor.SensorReached(374);
            int j = weight[i] >> 8;
            j = 1 + (j >> 1);
            weight[i] = j << 8;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(375);
    }
    private int sendMTFValues1(int nGroups, int alphaSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(376);
      CompressionState dataShadow = this.cstate;
      int[][] rfreq = dataShadow.sendMTFValues_rfreq;
      int[] fave = dataShadow.sendMTFValues_fave;
      short[] cost = dataShadow.sendMTFValues_cost;
      char[] sfmap = dataShadow.sfmap;
      byte[] selector = dataShadow.selector;
      byte[][] len = dataShadow.sendMTFValues_len;
      byte[] len_0 = len[0];
      byte[] len_1 = len[1];
      byte[] len_2 = len[2];
      byte[] len_3 = len[3];
      byte[] len_4 = len[4];
      byte[] len_5 = len[5];
      int nMTFShadow = this.nMTF;
      int nSelectors = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(377);
      for (int iter = 0; iter < BZip2.N_ITERS; iter++) {
        IACSharpSensor.IACSharpSensor.SensorReached(378);
        for (int t = nGroups; --t >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(379);
          fave[t] = 0;
          int[] rfreqt = rfreq[t];
          IACSharpSensor.IACSharpSensor.SensorReached(380);
          for (int i = alphaSize; --i >= 0;) {
            IACSharpSensor.IACSharpSensor.SensorReached(381);
            rfreqt[i] = 0;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(382);
        nSelectors = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(383);
        for (int gs = 0; gs < this.nMTF;) {
          IACSharpSensor.IACSharpSensor.SensorReached(384);
          int ge = Math.Min(gs + BZip2.G_SIZE - 1, nMTFShadow - 1);
          IACSharpSensor.IACSharpSensor.SensorReached(385);
          if (nGroups == BZip2.NGroups) {
            IACSharpSensor.IACSharpSensor.SensorReached(386);
            int[] c = new int[6];
            IACSharpSensor.IACSharpSensor.SensorReached(387);
            for (int i = gs; i <= ge; i++) {
              IACSharpSensor.IACSharpSensor.SensorReached(388);
              int icv = sfmap[i];
              c[0] += len_0[icv] & 0xff;
              c[1] += len_1[icv] & 0xff;
              c[2] += len_2[icv] & 0xff;
              c[3] += len_3[icv] & 0xff;
              c[4] += len_4[icv] & 0xff;
              c[5] += len_5[icv] & 0xff;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(389);
            cost[0] = (short)c[0];
            cost[1] = (short)c[1];
            cost[2] = (short)c[2];
            cost[3] = (short)c[3];
            cost[4] = (short)c[4];
            cost[5] = (short)c[5];
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(390);
            for (int t = nGroups; --t >= 0;) {
              IACSharpSensor.IACSharpSensor.SensorReached(391);
              cost[t] = 0;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(392);
            for (int i = gs; i <= ge; i++) {
              IACSharpSensor.IACSharpSensor.SensorReached(393);
              int icv = sfmap[i];
              IACSharpSensor.IACSharpSensor.SensorReached(394);
              for (int t = nGroups; --t >= 0;) {
                IACSharpSensor.IACSharpSensor.SensorReached(395);
                cost[t] += (short)(len[t][icv] & 0xff);
              }
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(396);
          int bt = -1;
          IACSharpSensor.IACSharpSensor.SensorReached(397);
          for (int t = nGroups, bc = 999999999; --t >= 0;) {
            IACSharpSensor.IACSharpSensor.SensorReached(398);
            int cost_t = cost[t];
            IACSharpSensor.IACSharpSensor.SensorReached(399);
            if (cost_t < bc) {
              IACSharpSensor.IACSharpSensor.SensorReached(400);
              bc = cost_t;
              bt = t;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(401);
          fave[bt]++;
          selector[nSelectors] = (byte)bt;
          nSelectors++;
          int[] rfreq_bt = rfreq[bt];
          IACSharpSensor.IACSharpSensor.SensorReached(402);
          for (int i = gs; i <= ge; i++) {
            IACSharpSensor.IACSharpSensor.SensorReached(403);
            rfreq_bt[sfmap[i]]++;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(404);
          gs = ge + 1;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(405);
        for (int t = 0; t < nGroups; t++) {
          IACSharpSensor.IACSharpSensor.SensorReached(406);
          hbMakeCodeLengths(len[t], rfreq[t], this.cstate, alphaSize, 20);
        }
      }
      System.Int32 RNTRNTRNT_17 = nSelectors;
      IACSharpSensor.IACSharpSensor.SensorReached(407);
      return RNTRNTRNT_17;
    }
    private void sendMTFValues2(int nGroups, int nSelectors)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(408);
      CompressionState dataShadow = this.cstate;
      byte[] pos = dataShadow.sendMTFValues2_pos;
      IACSharpSensor.IACSharpSensor.SensorReached(409);
      for (int i = nGroups; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(410);
        pos[i] = (byte)i;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(411);
      for (int i = 0; i < nSelectors; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(412);
        byte ll_i = dataShadow.selector[i];
        byte tmp = pos[0];
        int j = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(413);
        while (ll_i != tmp) {
          IACSharpSensor.IACSharpSensor.SensorReached(414);
          j++;
          byte tmp2 = tmp;
          tmp = pos[j];
          pos[j] = tmp2;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(415);
        pos[0] = tmp;
        dataShadow.selectorMtf[i] = (byte)j;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(416);
    }
    private void sendMTFValues3(int nGroups, int alphaSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(417);
      int[][] code = this.cstate.sendMTFValues_code;
      byte[][] len = this.cstate.sendMTFValues_len;
      IACSharpSensor.IACSharpSensor.SensorReached(418);
      for (int t = 0; t < nGroups; t++) {
        IACSharpSensor.IACSharpSensor.SensorReached(419);
        int minLen = 32;
        int maxLen = 0;
        byte[] len_t = len[t];
        IACSharpSensor.IACSharpSensor.SensorReached(420);
        for (int i = alphaSize; --i >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(421);
          int l = len_t[i] & 0xff;
          IACSharpSensor.IACSharpSensor.SensorReached(422);
          if (l > maxLen) {
            IACSharpSensor.IACSharpSensor.SensorReached(423);
            maxLen = l;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(424);
          if (l < minLen) {
            IACSharpSensor.IACSharpSensor.SensorReached(425);
            minLen = l;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(426);
        hbAssignCodes(code[t], len[t], minLen, maxLen, alphaSize);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(427);
    }
    private void sendMTFValues4()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(428);
      bool[] inUse = this.cstate.inUse;
      bool[] inUse16 = this.cstate.sentMTFValues4_inUse16;
      IACSharpSensor.IACSharpSensor.SensorReached(429);
      for (int i = 16; --i >= 0;) {
        IACSharpSensor.IACSharpSensor.SensorReached(430);
        inUse16[i] = false;
        int i16 = i * 16;
        IACSharpSensor.IACSharpSensor.SensorReached(431);
        for (int j = 16; --j >= 0;) {
          IACSharpSensor.IACSharpSensor.SensorReached(432);
          if (inUse[i16 + j]) {
            IACSharpSensor.IACSharpSensor.SensorReached(433);
            inUse16[i] = true;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(434);
      uint u = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(435);
      for (int i = 0; i < 16; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(436);
        if (inUse16[i]) {
          IACSharpSensor.IACSharpSensor.SensorReached(437);
          u |= 1u << (16 - i - 1);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(438);
      this.bw.WriteBits(16, u);
      IACSharpSensor.IACSharpSensor.SensorReached(439);
      for (int i = 0; i < 16; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(440);
        if (inUse16[i]) {
          IACSharpSensor.IACSharpSensor.SensorReached(441);
          int i16 = i * 16;
          u = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(442);
          for (int j = 0; j < 16; j++) {
            IACSharpSensor.IACSharpSensor.SensorReached(443);
            if (inUse[i16 + j]) {
              IACSharpSensor.IACSharpSensor.SensorReached(444);
              u |= 1u << (16 - j - 1);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(445);
          this.bw.WriteBits(16, u);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(446);
    }
    private void sendMTFValues5(int nGroups, int nSelectors)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(447);
      this.bw.WriteBits(3, (uint)nGroups);
      this.bw.WriteBits(15, (uint)nSelectors);
      byte[] selectorMtf = this.cstate.selectorMtf;
      IACSharpSensor.IACSharpSensor.SensorReached(448);
      for (int i = 0; i < nSelectors; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(449);
        for (int j = 0, hj = selectorMtf[i] & 0xff; j < hj; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(450);
          this.bw.WriteBits(1, 1);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(451);
        this.bw.WriteBits(1, 0);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(452);
    }
    private void sendMTFValues6(int nGroups, int alphaSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(453);
      byte[][] len = this.cstate.sendMTFValues_len;
      IACSharpSensor.IACSharpSensor.SensorReached(454);
      for (int t = 0; t < nGroups; t++) {
        IACSharpSensor.IACSharpSensor.SensorReached(455);
        byte[] len_t = len[t];
        uint curr = (uint)(len_t[0] & 0xff);
        this.bw.WriteBits(5, curr);
        IACSharpSensor.IACSharpSensor.SensorReached(456);
        for (int i = 0; i < alphaSize; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(457);
          int lti = len_t[i] & 0xff;
          IACSharpSensor.IACSharpSensor.SensorReached(458);
          while (curr < lti) {
            IACSharpSensor.IACSharpSensor.SensorReached(459);
            this.bw.WriteBits(2, 2u);
            curr++;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(460);
          while (curr > lti) {
            IACSharpSensor.IACSharpSensor.SensorReached(461);
            this.bw.WriteBits(2, 3u);
            curr--;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(462);
          this.bw.WriteBits(1, 0u);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(463);
    }
    private void sendMTFValues7(int nSelectors)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(464);
      byte[][] len = this.cstate.sendMTFValues_len;
      int[][] code = this.cstate.sendMTFValues_code;
      byte[] selector = this.cstate.selector;
      char[] sfmap = this.cstate.sfmap;
      int nMTFShadow = this.nMTF;
      int selCtr = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(465);
      for (int gs = 0; gs < nMTFShadow;) {
        IACSharpSensor.IACSharpSensor.SensorReached(466);
        int ge = Math.Min(gs + BZip2.G_SIZE - 1, nMTFShadow - 1);
        int ix = selector[selCtr] & 0xff;
        int[] code_selCtr = code[ix];
        byte[] len_selCtr = len[ix];
        IACSharpSensor.IACSharpSensor.SensorReached(467);
        while (gs <= ge) {
          IACSharpSensor.IACSharpSensor.SensorReached(468);
          int sfmap_i = sfmap[gs];
          int n = len_selCtr[sfmap_i] & 0xff;
          this.bw.WriteBits(n, (uint)code_selCtr[sfmap_i]);
          gs++;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(469);
        gs = ge + 1;
        selCtr++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(470);
    }
    private void moveToFrontCodeAndSend()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(471);
      this.bw.WriteBits(24, (uint)this.origPtr);
      generateMTFValues();
      sendMTFValues();
      IACSharpSensor.IACSharpSensor.SensorReached(472);
    }
    private class CompressionState
    {
      public readonly bool[] inUse = new bool[256];
      public readonly byte[] unseqToSeq = new byte[256];
      public readonly int[] mtfFreq = new int[BZip2.MaxAlphaSize];
      public readonly byte[] selector = new byte[BZip2.MaxSelectors];
      public readonly byte[] selectorMtf = new byte[BZip2.MaxSelectors];
      public readonly byte[] generateMTFValues_yy = new byte[256];
      public byte[][] sendMTFValues_len;
      public int[][] sendMTFValues_rfreq;
      public readonly int[] sendMTFValues_fave = new int[BZip2.NGroups];
      public readonly short[] sendMTFValues_cost = new short[BZip2.NGroups];
      public int[][] sendMTFValues_code;
      public readonly byte[] sendMTFValues2_pos = new byte[BZip2.NGroups];
      public readonly bool[] sentMTFValues4_inUse16 = new bool[16];
      public readonly int[] stack_ll = new int[BZip2.QSORT_STACK_SIZE];
      public readonly int[] stack_hh = new int[BZip2.QSORT_STACK_SIZE];
      public readonly int[] stack_dd = new int[BZip2.QSORT_STACK_SIZE];
      public readonly int[] mainSort_runningOrder = new int[256];
      public readonly int[] mainSort_copy = new int[256];
      public readonly bool[] mainSort_bigDone = new bool[256];
      public int[] heap = new int[BZip2.MaxAlphaSize + 2];
      public int[] weight = new int[BZip2.MaxAlphaSize * 2];
      public int[] parent = new int[BZip2.MaxAlphaSize * 2];
      public readonly int[] ftab = new int[65537];
      public byte[] block;
      public int[] fmap;
      public char[] sfmap;
      public char[] quadrant;
      public CompressionState(int blockSize100k)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(473);
        int n = blockSize100k * BZip2.BlockSizeMultiple;
        this.block = new byte[(n + 1 + BZip2.NUM_OVERSHOOT_BYTES)];
        this.fmap = new int[n];
        this.sfmap = new char[2 * n];
        this.quadrant = this.sfmap;
        this.sendMTFValues_len = BZip2.InitRectangularArray<byte>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.sendMTFValues_rfreq = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        this.sendMTFValues_code = BZip2.InitRectangularArray<int>(BZip2.NGroups, BZip2.MaxAlphaSize);
        IACSharpSensor.IACSharpSensor.SensorReached(474);
      }
    }
  }
}
