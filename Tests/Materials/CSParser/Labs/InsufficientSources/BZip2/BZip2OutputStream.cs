using System;
using System.IO;
namespace Ionic.BZip2
{
  public class BZip2OutputStream : System.IO.Stream
  {
    int totalBytesWrittenIn;
    bool leaveOpen;
    BZip2Compressor compressor;
    uint combinedCRC;
    Stream output;
    BitWriter bw;
    int blockSize100k;
    private TraceBits desiredTrace = TraceBits.Crc | TraceBits.Write;
    public BZip2OutputStream(Stream output) : this(output, BZip2.MaxBlockSize, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(819);
    }
    public BZip2OutputStream(Stream output, int blockSize) : this(output, blockSize, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(820);
    }
    public BZip2OutputStream(Stream output, bool leaveOpen) : this(output, BZip2.MaxBlockSize, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(821);
    }
    public BZip2OutputStream(Stream output, int blockSize, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(822);
      if (blockSize < BZip2.MinBlockSize || blockSize > BZip2.MaxBlockSize) {
        IACSharpSensor.IACSharpSensor.SensorReached(823);
        var msg = String.Format("blockSize={0} is out of range; must be between {1} and {2}", blockSize, BZip2.MinBlockSize, BZip2.MaxBlockSize);
        IACSharpSensor.IACSharpSensor.SensorReached(824);
        throw new ArgumentException(msg, "blockSize");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(825);
      this.output = output;
      IACSharpSensor.IACSharpSensor.SensorReached(826);
      if (!this.output.CanWrite) {
        IACSharpSensor.IACSharpSensor.SensorReached(827);
        throw new ArgumentException("The stream is not writable.", "output");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(828);
      this.bw = new BitWriter(this.output);
      this.blockSize100k = blockSize;
      this.compressor = new BZip2Compressor(this.bw, blockSize);
      this.leaveOpen = leaveOpen;
      this.combinedCRC = 0;
      EmitHeader();
      IACSharpSensor.IACSharpSensor.SensorReached(829);
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(830);
      if (output != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(831);
        Stream o = this.output;
        Finish();
        IACSharpSensor.IACSharpSensor.SensorReached(832);
        if (!leaveOpen) {
          IACSharpSensor.IACSharpSensor.SensorReached(833);
          o.Close();
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(834);
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(835);
      if (this.output != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(836);
        this.bw.Flush();
        this.output.Flush();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(837);
    }
    private void EmitHeader()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(838);
      var magic = new byte[] {
        (byte)'B',
        (byte)'Z',
        (byte)'h',
        (byte)('0' + this.blockSize100k)
      };
      this.output.Write(magic, 0, magic.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(839);
    }
    private void EmitTrailer()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(840);
      TraceOutput(TraceBits.Write, "total written out: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      this.bw.WriteByte(0x17);
      this.bw.WriteByte(0x72);
      this.bw.WriteByte(0x45);
      this.bw.WriteByte(0x38);
      this.bw.WriteByte(0x50);
      this.bw.WriteByte(0x90);
      this.bw.WriteInt(this.combinedCRC);
      this.bw.FinishAndPad();
      TraceOutput(TraceBits.Write, "final total: {0} (0x{0:X})", this.bw.TotalBytesWrittenOut);
      IACSharpSensor.IACSharpSensor.SensorReached(841);
    }
    void Finish()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(842);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(843);
        var totalBefore = this.bw.TotalBytesWrittenOut;
        this.compressor.CompressAndWrite();
        TraceOutput(TraceBits.Write, "out block length (bytes): {0} (0x{0:X})", this.bw.TotalBytesWrittenOut - totalBefore);
        TraceOutput(TraceBits.Crc, " combined CRC (before): {0:X8}", this.combinedCRC);
        this.combinedCRC = (this.combinedCRC << 1) | (this.combinedCRC >> 31);
        this.combinedCRC ^= (uint)compressor.Crc32;
        TraceOutput(TraceBits.Crc, " block    CRC         : {0:X8}", this.compressor.Crc32);
        TraceOutput(TraceBits.Crc, " combined CRC (final) : {0:X8}", this.combinedCRC);
        EmitTrailer();
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(844);
        this.output = null;
        this.compressor = null;
        this.bw = null;
        IACSharpSensor.IACSharpSensor.SensorReached(845);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(846);
    }
    public int BlockSize {
      get {
        System.Int32 RNTRNTRNT_32 = this.blockSize100k;
        IACSharpSensor.IACSharpSensor.SensorReached(847);
        return RNTRNTRNT_32;
      }
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(848);
      if (offset < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(849);
        throw new IndexOutOfRangeException(String.Format("offset ({0}) must be > 0", offset));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(850);
      if (count < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(851);
        throw new IndexOutOfRangeException(String.Format("count ({0}) must be > 0", count));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(852);
      if (offset + count > buffer.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(853);
        throw new IndexOutOfRangeException(String.Format("offset({0}) count({1}) bLength({2})", offset, count, buffer.Length));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(854);
      if (this.output == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(855);
        throw new IOException("the stream is not open");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(856);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(857);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(858);
      int bytesWritten = 0;
      int bytesRemaining = count;
      IACSharpSensor.IACSharpSensor.SensorReached(859);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(860);
        int n = compressor.Fill(buffer, offset, bytesRemaining);
        IACSharpSensor.IACSharpSensor.SensorReached(861);
        if (n != bytesRemaining) {
          IACSharpSensor.IACSharpSensor.SensorReached(862);
          var totalBefore = this.bw.TotalBytesWrittenOut;
          this.compressor.CompressAndWrite();
          TraceOutput(TraceBits.Write, "out block length (bytes): {0} (0x{0:X})", this.bw.TotalBytesWrittenOut - totalBefore);
          TraceOutput(TraceBits.Write, " remaining: {0} 0x{1:X}", this.bw.NumRemainingBits, this.bw.RemainingBits);
          TraceOutput(TraceBits.Crc, " combined CRC (before): {0:X8}", this.combinedCRC);
          this.combinedCRC = (this.combinedCRC << 1) | (this.combinedCRC >> 31);
          this.combinedCRC ^= (uint)compressor.Crc32;
          TraceOutput(TraceBits.Crc, " block    CRC         : {0:X8}", compressor.Crc32);
          TraceOutput(TraceBits.Crc, " combined CRC (after) : {0:X8}", this.combinedCRC);
          offset += n;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(863);
        bytesRemaining -= n;
        bytesWritten += n;
      } while (bytesRemaining > 0);
      IACSharpSensor.IACSharpSensor.SensorReached(864);
      totalBytesWrittenIn += bytesWritten;
      IACSharpSensor.IACSharpSensor.SensorReached(865);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_33 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(866);
        return RNTRNTRNT_33;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_34 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(867);
        return RNTRNTRNT_34;
      }
    }
    public override bool CanWrite {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(868);
        if (this.output == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(869);
          throw new ObjectDisposedException("BZip2Stream");
        }
        System.Boolean RNTRNTRNT_35 = this.output.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(870);
        return RNTRNTRNT_35;
      }
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(871);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_36 = this.totalBytesWrittenIn;
        IACSharpSensor.IACSharpSensor.SensorReached(872);
        return RNTRNTRNT_36;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(873);
        throw new NotImplementedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(874);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(875);
      throw new NotImplementedException();
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(876);
      throw new NotImplementedException();
    }
    [Flags()]
    enum TraceBits : uint
    {
      None = 0,
      Crc = 1,
      Write = 2,
      All = 0xffffffffu
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceOutput(TraceBits bits, string format, params object[] varParams)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(877);
      if ((bits & this.desiredTrace) != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(879);
        {
          IACSharpSensor.IACSharpSensor.SensorReached(878);
          int tid = System.Threading.Thread.CurrentThread.GetHashCode();
          Console.ForegroundColor = (ConsoleColor)(tid % 8 + 10);
          Console.Write("{0:000} PBOS ", tid);
          Console.WriteLine(format, varParams);
          Console.ResetColor();
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(880);
    }
  }
}
