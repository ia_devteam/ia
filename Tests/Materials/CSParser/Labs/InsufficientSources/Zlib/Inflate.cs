using System;
namespace Ionic.Zlib
{
  sealed class InflateBlocks
  {
    private const int MANY = 1440;
    static internal readonly int[] border = new int[] {
      16,
      17,
      18,
      0,
      8,
      7,
      9,
      6,
      10,
      5,
      11,
      4,
      12,
      3,
      13,
      2,
      14,
      1,
      15
    };
    private enum InflateBlockMode
    {
      TYPE = 0,
      LENS = 1,
      STORED = 2,
      TABLE = 3,
      BTREE = 4,
      DTREE = 5,
      CODES = 6,
      DRY = 7,
      DONE = 8,
      BAD = 9
    }
    private InflateBlockMode mode;
    internal int left;
    internal int table;
    internal int index;
    internal int[] blens;
    internal int[] bb = new int[1];
    internal int[] tb = new int[1];
    internal InflateCodes codes = new InflateCodes();
    internal int last;
    internal ZlibCodec _codec;
    internal int bitk;
    internal int bitb;
    internal int[] hufts;
    internal byte[] window;
    internal int end;
    internal int readAt;
    internal int writeAt;
    internal System.Object checkfn;
    internal uint check;
    internal InfTree inftree = new InfTree();
    internal InflateBlocks(ZlibCodec codec, System.Object checkfn, int w)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5717);
      _codec = codec;
      hufts = new int[MANY * 3];
      window = new byte[w];
      end = w;
      this.checkfn = checkfn;
      mode = InflateBlockMode.TYPE;
      Reset();
      IACSharpSensor.IACSharpSensor.SensorReached(5718);
    }
    internal uint Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5719);
      uint oldCheck = check;
      mode = InflateBlockMode.TYPE;
      bitk = 0;
      bitb = 0;
      readAt = writeAt = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5720);
      if (checkfn != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(5721);
        _codec._Adler32 = check = Adler.Adler32(0, null, 0, 0);
      }
      System.UInt32 RNTRNTRNT_630 = oldCheck;
      IACSharpSensor.IACSharpSensor.SensorReached(5722);
      return RNTRNTRNT_630;
    }
    internal int Process(int r)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5723);
      int t;
      int b;
      int k;
      int p;
      int n;
      int q;
      int m;
      p = _codec.NextIn;
      n = _codec.AvailableBytesIn;
      b = bitb;
      k = bitk;
      q = writeAt;
      m = (int)(q < readAt ? readAt - q - 1 : end - q);
      IACSharpSensor.IACSharpSensor.SensorReached(5724);
      while (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(5725);
        switch (mode) {
          case InflateBlockMode.TYPE:
            IACSharpSensor.IACSharpSensor.SensorReached(5726);
            while (k < (3)) {
              IACSharpSensor.IACSharpSensor.SensorReached(5727);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5728);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5729);
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_631 = Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5730);
                return RNTRNTRNT_631;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5731);
              n--;
              b |= (_codec.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5732);
            t = (int)(b & 7);
            last = t & 1;
            IACSharpSensor.IACSharpSensor.SensorReached(5733);
            switch ((uint)t >> 1) {
              case 0:
                IACSharpSensor.IACSharpSensor.SensorReached(5734);
                b >>= 3;
                k -= (3);
                t = k & 7;
                b >>= t;
                k -= t;
                mode = InflateBlockMode.LENS;
                IACSharpSensor.IACSharpSensor.SensorReached(5735);
                break;
              case 1:
                IACSharpSensor.IACSharpSensor.SensorReached(5736);
                int[] bl = new int[1];
                int[] bd = new int[1];
                int[][] tl = new int[1][];
                int[][] td = new int[1][];
                InfTree.inflate_trees_fixed(bl, bd, tl, td, _codec);
                codes.Init(bl[0], bd[0], tl[0], 0, td[0], 0);
                b >>= 3;
                k -= 3;
                mode = InflateBlockMode.CODES;
                IACSharpSensor.IACSharpSensor.SensorReached(5737);
                break;
              case 2:
                IACSharpSensor.IACSharpSensor.SensorReached(5738);
                b >>= 3;
                k -= 3;
                mode = InflateBlockMode.TABLE;
                IACSharpSensor.IACSharpSensor.SensorReached(5739);
                break;
              case 3:
                IACSharpSensor.IACSharpSensor.SensorReached(5740);
                b >>= 3;
                k -= 3;
                mode = InflateBlockMode.BAD;
                _codec.Message = "invalid block type";
                r = ZlibConstants.Z_DATA_ERROR;
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_632 = Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5741);
                return RNTRNTRNT_632;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5742);
            break;
          case InflateBlockMode.LENS:
            IACSharpSensor.IACSharpSensor.SensorReached(5743);
            while (k < (32)) {
              IACSharpSensor.IACSharpSensor.SensorReached(5744);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5745);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5746);
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_633 = Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5747);
                return RNTRNTRNT_633;
              }
              ;
              n--;
              b |= (_codec.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5748);
            if ((((~b) >> 16) & 0xffff) != (b & 0xffff)) {
              IACSharpSensor.IACSharpSensor.SensorReached(5749);
              mode = InflateBlockMode.BAD;
              _codec.Message = "invalid stored block lengths";
              r = ZlibConstants.Z_DATA_ERROR;
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_634 = Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(5750);
              return RNTRNTRNT_634;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5751);
            left = (b & 0xffff);
            b = k = 0;
            mode = left != 0 ? InflateBlockMode.STORED : (last != 0 ? InflateBlockMode.DRY : InflateBlockMode.TYPE);
            IACSharpSensor.IACSharpSensor.SensorReached(5752);
            break;
          case InflateBlockMode.STORED:
            IACSharpSensor.IACSharpSensor.SensorReached(5753);
            if (n == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5754);
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_635 = Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(5755);
              return RNTRNTRNT_635;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5756);
            if (m == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5757);
              if (q == end && readAt != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5758);
                q = 0;
                m = (int)(q < readAt ? readAt - q - 1 : end - q);
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5759);
              if (m == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5760);
                writeAt = q;
                r = Flush(r);
                q = writeAt;
                m = (int)(q < readAt ? readAt - q - 1 : end - q);
                IACSharpSensor.IACSharpSensor.SensorReached(5761);
                if (q == end && readAt != 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5762);
                  q = 0;
                  m = (int)(q < readAt ? readAt - q - 1 : end - q);
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5763);
                if (m == 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5764);
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_636 = Flush(r);
                  IACSharpSensor.IACSharpSensor.SensorReached(5765);
                  return RNTRNTRNT_636;
                }
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5766);
            r = ZlibConstants.Z_OK;
            t = left;
            IACSharpSensor.IACSharpSensor.SensorReached(5767);
            if (t > n) {
              IACSharpSensor.IACSharpSensor.SensorReached(5768);
              t = n;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5769);
            if (t > m) {
              IACSharpSensor.IACSharpSensor.SensorReached(5770);
              t = m;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5771);
            Array.Copy(_codec.InputBuffer, p, window, q, t);
            p += t;
            n -= t;
            q += t;
            m -= t;
            IACSharpSensor.IACSharpSensor.SensorReached(5772);
            if ((left -= t) != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5773);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5774);
            mode = last != 0 ? InflateBlockMode.DRY : InflateBlockMode.TYPE;
            IACSharpSensor.IACSharpSensor.SensorReached(5775);
            break;
          case InflateBlockMode.TABLE:
            IACSharpSensor.IACSharpSensor.SensorReached(5776);
            while (k < (14)) {
              IACSharpSensor.IACSharpSensor.SensorReached(5777);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5778);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5779);
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_637 = Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5780);
                return RNTRNTRNT_637;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5781);
              n--;
              b |= (_codec.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5782);
            table = t = (b & 0x3fff);
            IACSharpSensor.IACSharpSensor.SensorReached(5783);
            if ((t & 0x1f) > 29 || ((t >> 5) & 0x1f) > 29) {
              IACSharpSensor.IACSharpSensor.SensorReached(5784);
              mode = InflateBlockMode.BAD;
              _codec.Message = "too many length or distance symbols";
              r = ZlibConstants.Z_DATA_ERROR;
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_638 = Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(5785);
              return RNTRNTRNT_638;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5786);
            t = 258 + (t & 0x1f) + ((t >> 5) & 0x1f);
            IACSharpSensor.IACSharpSensor.SensorReached(5787);
            if (blens == null || blens.Length < t) {
              IACSharpSensor.IACSharpSensor.SensorReached(5788);
              blens = new int[t];
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(5789);
              Array.Clear(blens, 0, t);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5790);
            b >>= 14;
            k -= 14;
            index = 0;
            mode = InflateBlockMode.BTREE;
            IACSharpSensor.IACSharpSensor.SensorReached(5791);
            goto case InflateBlockMode.BTREE;
          case InflateBlockMode.BTREE:
            IACSharpSensor.IACSharpSensor.SensorReached(5792);
            while (index < 4 + (table >> 10)) {
              IACSharpSensor.IACSharpSensor.SensorReached(5793);
              while (k < (3)) {
                IACSharpSensor.IACSharpSensor.SensorReached(5794);
                if (n != 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5795);
                  r = ZlibConstants.Z_OK;
                } else {
                  IACSharpSensor.IACSharpSensor.SensorReached(5796);
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_639 = Flush(r);
                  IACSharpSensor.IACSharpSensor.SensorReached(5797);
                  return RNTRNTRNT_639;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5798);
                n--;
                b |= (_codec.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5799);
              blens[border[index++]] = b & 7;
              b >>= 3;
              k -= 3;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5800);
            while (index < 19) {
              IACSharpSensor.IACSharpSensor.SensorReached(5801);
              blens[border[index++]] = 0;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5802);
            bb[0] = 7;
            t = inftree.inflate_trees_bits(blens, bb, tb, hufts, _codec);
            IACSharpSensor.IACSharpSensor.SensorReached(5803);
            if (t != ZlibConstants.Z_OK) {
              IACSharpSensor.IACSharpSensor.SensorReached(5804);
              r = t;
              IACSharpSensor.IACSharpSensor.SensorReached(5805);
              if (r == ZlibConstants.Z_DATA_ERROR) {
                IACSharpSensor.IACSharpSensor.SensorReached(5806);
                blens = null;
                mode = InflateBlockMode.BAD;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5807);
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_640 = Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(5808);
              return RNTRNTRNT_640;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5809);
            index = 0;
            mode = InflateBlockMode.DTREE;
            IACSharpSensor.IACSharpSensor.SensorReached(5810);
            goto case InflateBlockMode.DTREE;
          case InflateBlockMode.DTREE:
            IACSharpSensor.IACSharpSensor.SensorReached(5811);
            while (true) {
              IACSharpSensor.IACSharpSensor.SensorReached(5812);
              t = table;
              IACSharpSensor.IACSharpSensor.SensorReached(5813);
              if (!(index < 258 + (t & 0x1f) + ((t >> 5) & 0x1f))) {
                IACSharpSensor.IACSharpSensor.SensorReached(5814);
                break;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5815);
              int i, j, c;
              t = bb[0];
              IACSharpSensor.IACSharpSensor.SensorReached(5816);
              while (k < t) {
                IACSharpSensor.IACSharpSensor.SensorReached(5817);
                if (n != 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5818);
                  r = ZlibConstants.Z_OK;
                } else {
                  IACSharpSensor.IACSharpSensor.SensorReached(5819);
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_641 = Flush(r);
                  IACSharpSensor.IACSharpSensor.SensorReached(5820);
                  return RNTRNTRNT_641;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5821);
                n--;
                b |= (_codec.InputBuffer[p++] & 0xff) << k;
                k += 8;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5822);
              t = hufts[(tb[0] + (b & InternalInflateConstants.InflateMask[t])) * 3 + 1];
              c = hufts[(tb[0] + (b & InternalInflateConstants.InflateMask[t])) * 3 + 2];
              IACSharpSensor.IACSharpSensor.SensorReached(5823);
              if (c < 16) {
                IACSharpSensor.IACSharpSensor.SensorReached(5824);
                b >>= t;
                k -= t;
                blens[index++] = c;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5825);
                i = c == 18 ? 7 : c - 14;
                j = c == 18 ? 11 : 3;
                IACSharpSensor.IACSharpSensor.SensorReached(5826);
                while (k < (t + i)) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5827);
                  if (n != 0) {
                    IACSharpSensor.IACSharpSensor.SensorReached(5828);
                    r = ZlibConstants.Z_OK;
                  } else {
                    IACSharpSensor.IACSharpSensor.SensorReached(5829);
                    bitb = b;
                    bitk = k;
                    _codec.AvailableBytesIn = n;
                    _codec.TotalBytesIn += p - _codec.NextIn;
                    _codec.NextIn = p;
                    writeAt = q;
                    System.Int32 RNTRNTRNT_642 = Flush(r);
                    IACSharpSensor.IACSharpSensor.SensorReached(5830);
                    return RNTRNTRNT_642;
                  }
                  IACSharpSensor.IACSharpSensor.SensorReached(5831);
                  n--;
                  b |= (_codec.InputBuffer[p++] & 0xff) << k;
                  k += 8;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5832);
                b >>= t;
                k -= t;
                j += (b & InternalInflateConstants.InflateMask[i]);
                b >>= i;
                k -= i;
                i = index;
                t = table;
                IACSharpSensor.IACSharpSensor.SensorReached(5833);
                if (i + j > 258 + (t & 0x1f) + ((t >> 5) & 0x1f) || (c == 16 && i < 1)) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5834);
                  blens = null;
                  mode = InflateBlockMode.BAD;
                  _codec.Message = "invalid bit length repeat";
                  r = ZlibConstants.Z_DATA_ERROR;
                  bitb = b;
                  bitk = k;
                  _codec.AvailableBytesIn = n;
                  _codec.TotalBytesIn += p - _codec.NextIn;
                  _codec.NextIn = p;
                  writeAt = q;
                  System.Int32 RNTRNTRNT_643 = Flush(r);
                  IACSharpSensor.IACSharpSensor.SensorReached(5835);
                  return RNTRNTRNT_643;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5836);
                c = (c == 16) ? blens[i - 1] : 0;
                IACSharpSensor.IACSharpSensor.SensorReached(5837);
                do {
                  IACSharpSensor.IACSharpSensor.SensorReached(5838);
                  blens[i++] = c;
                } while (--j != 0);
                IACSharpSensor.IACSharpSensor.SensorReached(5839);
                index = i;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5840);
            tb[0] = -1;
            IACSharpSensor.IACSharpSensor.SensorReached(5848);
            
            {
              IACSharpSensor.IACSharpSensor.SensorReached(5841);
              int[] bl = new int[] { 9 };
              int[] bd = new int[] { 6 };
              int[] tl = new int[1];
              int[] td = new int[1];
              t = table;
              t = inftree.inflate_trees_dynamic(257 + (t & 0x1f), 1 + ((t >> 5) & 0x1f), blens, bl, bd, tl, td, hufts, _codec);
              IACSharpSensor.IACSharpSensor.SensorReached(5842);
              if (t != ZlibConstants.Z_OK) {
                IACSharpSensor.IACSharpSensor.SensorReached(5843);
                if (t == ZlibConstants.Z_DATA_ERROR) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5844);
                  blens = null;
                  mode = InflateBlockMode.BAD;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5845);
                r = t;
                bitb = b;
                bitk = k;
                _codec.AvailableBytesIn = n;
                _codec.TotalBytesIn += p - _codec.NextIn;
                _codec.NextIn = p;
                writeAt = q;
                System.Int32 RNTRNTRNT_644 = Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5846);
                return RNTRNTRNT_644;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5847);
              codes.Init(bl[0], bd[0], hufts, tl[0], hufts, td[0]);
            }

            mode = InflateBlockMode.CODES;
            IACSharpSensor.IACSharpSensor.SensorReached(5849);
            goto case InflateBlockMode.CODES;
          case InflateBlockMode.CODES:
            IACSharpSensor.IACSharpSensor.SensorReached(5850);
            bitb = b;
            bitk = k;
            _codec.AvailableBytesIn = n;
            _codec.TotalBytesIn += p - _codec.NextIn;
            _codec.NextIn = p;
            writeAt = q;
            r = codes.Process(this, r);
            IACSharpSensor.IACSharpSensor.SensorReached(5851);
            if (r != ZlibConstants.Z_STREAM_END) {
              System.Int32 RNTRNTRNT_645 = Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(5852);
              return RNTRNTRNT_645;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5853);
            r = ZlibConstants.Z_OK;
            p = _codec.NextIn;
            n = _codec.AvailableBytesIn;
            b = bitb;
            k = bitk;
            q = writeAt;
            m = (int)(q < readAt ? readAt - q - 1 : end - q);
            IACSharpSensor.IACSharpSensor.SensorReached(5854);
            if (last == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5855);
              mode = InflateBlockMode.TYPE;
              IACSharpSensor.IACSharpSensor.SensorReached(5856);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5857);
            mode = InflateBlockMode.DRY;
            IACSharpSensor.IACSharpSensor.SensorReached(5858);
            goto case InflateBlockMode.DRY;
          case InflateBlockMode.DRY:
            IACSharpSensor.IACSharpSensor.SensorReached(5859);
            writeAt = q;
            r = Flush(r);
            q = writeAt;
            m = (int)(q < readAt ? readAt - q - 1 : end - q);
            IACSharpSensor.IACSharpSensor.SensorReached(5860);
            if (readAt != writeAt) {
              IACSharpSensor.IACSharpSensor.SensorReached(5861);
              bitb = b;
              bitk = k;
              _codec.AvailableBytesIn = n;
              _codec.TotalBytesIn += p - _codec.NextIn;
              _codec.NextIn = p;
              writeAt = q;
              System.Int32 RNTRNTRNT_646 = Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(5862);
              return RNTRNTRNT_646;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5863);
            mode = InflateBlockMode.DONE;
            IACSharpSensor.IACSharpSensor.SensorReached(5864);
            goto case InflateBlockMode.DONE;
          case InflateBlockMode.DONE:
            IACSharpSensor.IACSharpSensor.SensorReached(5865);
            r = ZlibConstants.Z_STREAM_END;
            bitb = b;
            bitk = k;
            _codec.AvailableBytesIn = n;
            _codec.TotalBytesIn += p - _codec.NextIn;
            _codec.NextIn = p;
            writeAt = q;
            System.Int32 RNTRNTRNT_647 = Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5866);
            return RNTRNTRNT_647;
          case InflateBlockMode.BAD:
            IACSharpSensor.IACSharpSensor.SensorReached(5867);
            r = ZlibConstants.Z_DATA_ERROR;
            bitb = b;
            bitk = k;
            _codec.AvailableBytesIn = n;
            _codec.TotalBytesIn += p - _codec.NextIn;
            _codec.NextIn = p;
            writeAt = q;
            System.Int32 RNTRNTRNT_648 = Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5868);
            return RNTRNTRNT_648;
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(5869);
            r = ZlibConstants.Z_STREAM_ERROR;
            bitb = b;
            bitk = k;
            _codec.AvailableBytesIn = n;
            _codec.TotalBytesIn += p - _codec.NextIn;
            _codec.NextIn = p;
            writeAt = q;
            System.Int32 RNTRNTRNT_649 = Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5870);
            return RNTRNTRNT_649;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5871);
    }
    internal void Free()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5872);
      Reset();
      window = null;
      hufts = null;
      IACSharpSensor.IACSharpSensor.SensorReached(5873);
    }
    internal void SetDictionary(byte[] d, int start, int n)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5874);
      Array.Copy(d, start, window, 0, n);
      readAt = writeAt = n;
      IACSharpSensor.IACSharpSensor.SensorReached(5875);
    }
    internal int SyncPoint()
    {
      System.Int32 RNTRNTRNT_650 = mode == InflateBlockMode.LENS ? 1 : 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5876);
      return RNTRNTRNT_650;
    }
    internal int Flush(int r)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5877);
      int nBytes;
      IACSharpSensor.IACSharpSensor.SensorReached(5878);
      for (int pass = 0; pass < 2; pass++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5879);
        if (pass == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5880);
          nBytes = (int)((readAt <= writeAt ? writeAt : end) - readAt);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5881);
          nBytes = writeAt - readAt;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5882);
        if (nBytes == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5883);
          if (r == ZlibConstants.Z_BUF_ERROR) {
            IACSharpSensor.IACSharpSensor.SensorReached(5884);
            r = ZlibConstants.Z_OK;
          }
          System.Int32 RNTRNTRNT_651 = r;
          IACSharpSensor.IACSharpSensor.SensorReached(5885);
          return RNTRNTRNT_651;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5886);
        if (nBytes > _codec.AvailableBytesOut) {
          IACSharpSensor.IACSharpSensor.SensorReached(5887);
          nBytes = _codec.AvailableBytesOut;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5888);
        if (nBytes != 0 && r == ZlibConstants.Z_BUF_ERROR) {
          IACSharpSensor.IACSharpSensor.SensorReached(5889);
          r = ZlibConstants.Z_OK;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5890);
        _codec.AvailableBytesOut -= nBytes;
        _codec.TotalBytesOut += nBytes;
        IACSharpSensor.IACSharpSensor.SensorReached(5891);
        if (checkfn != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(5892);
          _codec._Adler32 = check = Adler.Adler32(check, window, readAt, nBytes);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5893);
        Array.Copy(window, readAt, _codec.OutputBuffer, _codec.NextOut, nBytes);
        _codec.NextOut += nBytes;
        readAt += nBytes;
        IACSharpSensor.IACSharpSensor.SensorReached(5894);
        if (readAt == end && pass == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5895);
          readAt = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(5896);
          if (writeAt == end) {
            IACSharpSensor.IACSharpSensor.SensorReached(5897);
            writeAt = 0;
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5898);
          pass++;
        }
      }
      System.Int32 RNTRNTRNT_652 = r;
      IACSharpSensor.IACSharpSensor.SensorReached(5899);
      return RNTRNTRNT_652;
    }
  }
  static internal class InternalInflateConstants
  {
    static internal readonly int[] InflateMask = new int[] {
      0x0,
      0x1,
      0x3,
      0x7,
      0xf,
      0x1f,
      0x3f,
      0x7f,
      0xff,
      0x1ff,
      0x3ff,
      0x7ff,
      0xfff,
      0x1fff,
      0x3fff,
      0x7fff,
      0xffff
    };
  }
  sealed class InflateCodes
  {
    private const int START = 0;
    private const int LEN = 1;
    private const int LENEXT = 2;
    private const int DIST = 3;
    private const int DISTEXT = 4;
    private const int COPY = 5;
    private const int LIT = 6;
    private const int WASH = 7;
    private const int END = 8;
    private const int BADCODE = 9;
    internal int mode;
    internal int len;
    internal int[] tree;
    internal int tree_index = 0;
    internal int need;
    internal int lit;
    internal int bitsToGet;
    internal int dist;
    internal byte lbits;
    internal byte dbits;
    internal int[] ltree;
    internal int ltree_index;
    internal int[] dtree;
    internal int dtree_index;
    internal InflateCodes()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5900);
    }
    internal void Init(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5901);
      mode = START;
      lbits = (byte)bl;
      dbits = (byte)bd;
      ltree = tl;
      ltree_index = tl_index;
      dtree = td;
      dtree_index = td_index;
      tree = null;
      IACSharpSensor.IACSharpSensor.SensorReached(5902);
    }
    internal int Process(InflateBlocks blocks, int r)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5903);
      int j;
      int tindex;
      int e;
      int b = 0;
      int k = 0;
      int p = 0;
      int n;
      int q;
      int m;
      int f;
      ZlibCodec z = blocks._codec;
      p = z.NextIn;
      n = z.AvailableBytesIn;
      b = blocks.bitb;
      k = blocks.bitk;
      q = blocks.writeAt;
      m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
      IACSharpSensor.IACSharpSensor.SensorReached(5904);
      while (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(5905);
        switch (mode) {
          case START:
            IACSharpSensor.IACSharpSensor.SensorReached(5906);
            if (m >= 258 && n >= 10) {
              IACSharpSensor.IACSharpSensor.SensorReached(5907);
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              r = InflateFast(lbits, dbits, ltree, ltree_index, dtree, dtree_index, blocks, z);
              p = z.NextIn;
              n = z.AvailableBytesIn;
              b = blocks.bitb;
              k = blocks.bitk;
              q = blocks.writeAt;
              m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
              IACSharpSensor.IACSharpSensor.SensorReached(5908);
              if (r != ZlibConstants.Z_OK) {
                IACSharpSensor.IACSharpSensor.SensorReached(5909);
                mode = (r == ZlibConstants.Z_STREAM_END) ? WASH : BADCODE;
                IACSharpSensor.IACSharpSensor.SensorReached(5910);
                break;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5911);
            need = lbits;
            tree = ltree;
            tree_index = ltree_index;
            mode = LEN;
            IACSharpSensor.IACSharpSensor.SensorReached(5912);
            goto case LEN;
          case LEN:
            IACSharpSensor.IACSharpSensor.SensorReached(5913);
            j = need;
            IACSharpSensor.IACSharpSensor.SensorReached(5914);
            while (k < j) {
              IACSharpSensor.IACSharpSensor.SensorReached(5915);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5916);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5917);
                blocks.bitb = b;
                blocks.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                blocks.writeAt = q;
                System.Int32 RNTRNTRNT_653 = blocks.Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5918);
                return RNTRNTRNT_653;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5919);
              n--;
              b |= (z.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5920);
            tindex = (tree_index + (b & InternalInflateConstants.InflateMask[j])) * 3;
            b >>= (tree[tindex + 1]);
            k -= (tree[tindex + 1]);
            e = tree[tindex];
            IACSharpSensor.IACSharpSensor.SensorReached(5921);
            if (e == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5922);
              lit = tree[tindex + 2];
              mode = LIT;
              IACSharpSensor.IACSharpSensor.SensorReached(5923);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5924);
            if ((e & 16) != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5925);
              bitsToGet = e & 15;
              len = tree[tindex + 2];
              mode = LENEXT;
              IACSharpSensor.IACSharpSensor.SensorReached(5926);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5927);
            if ((e & 64) == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5928);
              need = e;
              tree_index = tindex / 3 + tree[tindex + 2];
              IACSharpSensor.IACSharpSensor.SensorReached(5929);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5930);
            if ((e & 32) != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5931);
              mode = WASH;
              IACSharpSensor.IACSharpSensor.SensorReached(5932);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5933);
            mode = BADCODE;
            z.Message = "invalid literal/length code";
            r = ZlibConstants.Z_DATA_ERROR;
            blocks.bitb = b;
            blocks.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            blocks.writeAt = q;
            System.Int32 RNTRNTRNT_654 = blocks.Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5934);
            return RNTRNTRNT_654;
          case LENEXT:
            IACSharpSensor.IACSharpSensor.SensorReached(5935);
            j = bitsToGet;
            IACSharpSensor.IACSharpSensor.SensorReached(5936);
            while (k < j) {
              IACSharpSensor.IACSharpSensor.SensorReached(5937);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5938);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5939);
                blocks.bitb = b;
                blocks.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                blocks.writeAt = q;
                System.Int32 RNTRNTRNT_655 = blocks.Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5940);
                return RNTRNTRNT_655;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5941);
              n--;
              b |= (z.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5942);
            len += (b & InternalInflateConstants.InflateMask[j]);
            b >>= j;
            k -= j;
            need = dbits;
            tree = dtree;
            tree_index = dtree_index;
            mode = DIST;
            IACSharpSensor.IACSharpSensor.SensorReached(5943);
            goto case DIST;
          case DIST:
            IACSharpSensor.IACSharpSensor.SensorReached(5944);
            j = need;
            IACSharpSensor.IACSharpSensor.SensorReached(5945);
            while (k < j) {
              IACSharpSensor.IACSharpSensor.SensorReached(5946);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5947);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5948);
                blocks.bitb = b;
                blocks.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                blocks.writeAt = q;
                System.Int32 RNTRNTRNT_656 = blocks.Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5949);
                return RNTRNTRNT_656;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5950);
              n--;
              b |= (z.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5951);
            tindex = (tree_index + (b & InternalInflateConstants.InflateMask[j])) * 3;
            b >>= tree[tindex + 1];
            k -= tree[tindex + 1];
            e = (tree[tindex]);
            IACSharpSensor.IACSharpSensor.SensorReached(5952);
            if ((e & 0x10) != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5953);
              bitsToGet = e & 15;
              dist = tree[tindex + 2];
              mode = DISTEXT;
              IACSharpSensor.IACSharpSensor.SensorReached(5954);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5955);
            if ((e & 64) == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5956);
              need = e;
              tree_index = tindex / 3 + tree[tindex + 2];
              IACSharpSensor.IACSharpSensor.SensorReached(5957);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5958);
            mode = BADCODE;
            z.Message = "invalid distance code";
            r = ZlibConstants.Z_DATA_ERROR;
            blocks.bitb = b;
            blocks.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            blocks.writeAt = q;
            System.Int32 RNTRNTRNT_657 = blocks.Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(5959);
            return RNTRNTRNT_657;
          case DISTEXT:
            IACSharpSensor.IACSharpSensor.SensorReached(5960);
            j = bitsToGet;
            IACSharpSensor.IACSharpSensor.SensorReached(5961);
            while (k < j) {
              IACSharpSensor.IACSharpSensor.SensorReached(5962);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5963);
                r = ZlibConstants.Z_OK;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(5964);
                blocks.bitb = b;
                blocks.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                blocks.writeAt = q;
                System.Int32 RNTRNTRNT_658 = blocks.Flush(r);
                IACSharpSensor.IACSharpSensor.SensorReached(5965);
                return RNTRNTRNT_658;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5966);
              n--;
              b |= (z.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5967);
            dist += (b & InternalInflateConstants.InflateMask[j]);
            b >>= j;
            k -= j;
            mode = COPY;
            IACSharpSensor.IACSharpSensor.SensorReached(5968);
            goto case COPY;
          case COPY:
            IACSharpSensor.IACSharpSensor.SensorReached(5969);
            f = q - dist;
            IACSharpSensor.IACSharpSensor.SensorReached(5970);
            while (f < 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5971);
              f += blocks.end;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5972);
            while (len != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5973);
              if (m == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5974);
                if (q == blocks.end && blocks.readAt != 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5975);
                  q = 0;
                  m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5976);
                if (m == 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5977);
                  blocks.writeAt = q;
                  r = blocks.Flush(r);
                  q = blocks.writeAt;
                  m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                  IACSharpSensor.IACSharpSensor.SensorReached(5978);
                  if (q == blocks.end && blocks.readAt != 0) {
                    IACSharpSensor.IACSharpSensor.SensorReached(5979);
                    q = 0;
                    m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                  }
                  IACSharpSensor.IACSharpSensor.SensorReached(5980);
                  if (m == 0) {
                    IACSharpSensor.IACSharpSensor.SensorReached(5981);
                    blocks.bitb = b;
                    blocks.bitk = k;
                    z.AvailableBytesIn = n;
                    z.TotalBytesIn += p - z.NextIn;
                    z.NextIn = p;
                    blocks.writeAt = q;
                    System.Int32 RNTRNTRNT_659 = blocks.Flush(r);
                    IACSharpSensor.IACSharpSensor.SensorReached(5982);
                    return RNTRNTRNT_659;
                  }
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5983);
              blocks.window[q++] = blocks.window[f++];
              m--;
              IACSharpSensor.IACSharpSensor.SensorReached(5984);
              if (f == blocks.end) {
                IACSharpSensor.IACSharpSensor.SensorReached(5985);
                f = 0;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5986);
              len--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5987);
            mode = START;
            IACSharpSensor.IACSharpSensor.SensorReached(5988);
            break;
          case LIT:
            IACSharpSensor.IACSharpSensor.SensorReached(5989);
            if (m == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5990);
              if (q == blocks.end && blocks.readAt != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5991);
                q = 0;
                m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(5992);
              if (m == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(5993);
                blocks.writeAt = q;
                r = blocks.Flush(r);
                q = blocks.writeAt;
                m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                IACSharpSensor.IACSharpSensor.SensorReached(5994);
                if (q == blocks.end && blocks.readAt != 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5995);
                  q = 0;
                  m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(5996);
                if (m == 0) {
                  IACSharpSensor.IACSharpSensor.SensorReached(5997);
                  blocks.bitb = b;
                  blocks.bitk = k;
                  z.AvailableBytesIn = n;
                  z.TotalBytesIn += p - z.NextIn;
                  z.NextIn = p;
                  blocks.writeAt = q;
                  System.Int32 RNTRNTRNT_660 = blocks.Flush(r);
                  IACSharpSensor.IACSharpSensor.SensorReached(5998);
                  return RNTRNTRNT_660;
                }
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5999);
            r = ZlibConstants.Z_OK;
            blocks.window[q++] = (byte)lit;
            m--;
            mode = START;
            IACSharpSensor.IACSharpSensor.SensorReached(6000);
            break;
          case WASH:
            IACSharpSensor.IACSharpSensor.SensorReached(6001);
            if (k > 7) {
              IACSharpSensor.IACSharpSensor.SensorReached(6002);
              k -= 8;
              n++;
              p--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6003);
            blocks.writeAt = q;
            r = blocks.Flush(r);
            q = blocks.writeAt;
            m = q < blocks.readAt ? blocks.readAt - q - 1 : blocks.end - q;
            IACSharpSensor.IACSharpSensor.SensorReached(6004);
            if (blocks.readAt != blocks.writeAt) {
              IACSharpSensor.IACSharpSensor.SensorReached(6005);
              blocks.bitb = b;
              blocks.bitk = k;
              z.AvailableBytesIn = n;
              z.TotalBytesIn += p - z.NextIn;
              z.NextIn = p;
              blocks.writeAt = q;
              System.Int32 RNTRNTRNT_661 = blocks.Flush(r);
              IACSharpSensor.IACSharpSensor.SensorReached(6006);
              return RNTRNTRNT_661;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6007);
            mode = END;
            IACSharpSensor.IACSharpSensor.SensorReached(6008);
            goto case END;
          case END:
            IACSharpSensor.IACSharpSensor.SensorReached(6009);
            r = ZlibConstants.Z_STREAM_END;
            blocks.bitb = b;
            blocks.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            blocks.writeAt = q;
            System.Int32 RNTRNTRNT_662 = blocks.Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(6010);
            return RNTRNTRNT_662;
          case BADCODE:
            IACSharpSensor.IACSharpSensor.SensorReached(6011);
            r = ZlibConstants.Z_DATA_ERROR;
            blocks.bitb = b;
            blocks.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            blocks.writeAt = q;
            System.Int32 RNTRNTRNT_663 = blocks.Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(6012);
            return RNTRNTRNT_663;
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(6013);
            r = ZlibConstants.Z_STREAM_ERROR;
            blocks.bitb = b;
            blocks.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            blocks.writeAt = q;
            System.Int32 RNTRNTRNT_664 = blocks.Flush(r);
            IACSharpSensor.IACSharpSensor.SensorReached(6014);
            return RNTRNTRNT_664;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6015);
    }
    internal int InflateFast(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index, InflateBlocks s, ZlibCodec z)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6016);
      int t;
      int[] tp;
      int tp_index;
      int e;
      int b;
      int k;
      int p;
      int n;
      int q;
      int m;
      int ml;
      int md;
      int c;
      int d;
      int r;
      int tp_index_t_3;
      p = z.NextIn;
      n = z.AvailableBytesIn;
      b = s.bitb;
      k = s.bitk;
      q = s.writeAt;
      m = q < s.readAt ? s.readAt - q - 1 : s.end - q;
      ml = InternalInflateConstants.InflateMask[bl];
      md = InternalInflateConstants.InflateMask[bd];
      IACSharpSensor.IACSharpSensor.SensorReached(6017);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(6018);
        while (k < (20)) {
          IACSharpSensor.IACSharpSensor.SensorReached(6019);
          n--;
          b |= (z.InputBuffer[p++] & 0xff) << k;
          k += 8;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6020);
        t = b & ml;
        tp = tl;
        tp_index = tl_index;
        tp_index_t_3 = (tp_index + t) * 3;
        IACSharpSensor.IACSharpSensor.SensorReached(6021);
        if ((e = tp[tp_index_t_3]) == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(6022);
          b >>= (tp[tp_index_t_3 + 1]);
          k -= (tp[tp_index_t_3 + 1]);
          s.window[q++] = (byte)tp[tp_index_t_3 + 2];
          m--;
          IACSharpSensor.IACSharpSensor.SensorReached(6023);
          continue;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6024);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(6025);
          b >>= (tp[tp_index_t_3 + 1]);
          k -= (tp[tp_index_t_3 + 1]);
          IACSharpSensor.IACSharpSensor.SensorReached(6026);
          if ((e & 16) != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6027);
            e &= 15;
            c = tp[tp_index_t_3 + 2] + ((int)b & InternalInflateConstants.InflateMask[e]);
            b >>= e;
            k -= e;
            IACSharpSensor.IACSharpSensor.SensorReached(6028);
            while (k < 15) {
              IACSharpSensor.IACSharpSensor.SensorReached(6029);
              n--;
              b |= (z.InputBuffer[p++] & 0xff) << k;
              k += 8;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6030);
            t = b & md;
            tp = td;
            tp_index = td_index;
            tp_index_t_3 = (tp_index + t) * 3;
            e = tp[tp_index_t_3];
            IACSharpSensor.IACSharpSensor.SensorReached(6031);
            do {
              IACSharpSensor.IACSharpSensor.SensorReached(6032);
              b >>= (tp[tp_index_t_3 + 1]);
              k -= (tp[tp_index_t_3 + 1]);
              IACSharpSensor.IACSharpSensor.SensorReached(6033);
              if ((e & 16) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(6034);
                e &= 15;
                IACSharpSensor.IACSharpSensor.SensorReached(6035);
                while (k < e) {
                  IACSharpSensor.IACSharpSensor.SensorReached(6036);
                  n--;
                  b |= (z.InputBuffer[p++] & 0xff) << k;
                  k += 8;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(6037);
                d = tp[tp_index_t_3 + 2] + (b & InternalInflateConstants.InflateMask[e]);
                b >>= e;
                k -= e;
                m -= c;
                IACSharpSensor.IACSharpSensor.SensorReached(6038);
                if (q >= d) {
                  IACSharpSensor.IACSharpSensor.SensorReached(6039);
                  r = q - d;
                  IACSharpSensor.IACSharpSensor.SensorReached(6040);
                  if (q - r > 0 && 2 > (q - r)) {
                    IACSharpSensor.IACSharpSensor.SensorReached(6041);
                    s.window[q++] = s.window[r++];
                    s.window[q++] = s.window[r++];
                    c -= 2;
                  } else {
                    IACSharpSensor.IACSharpSensor.SensorReached(6042);
                    Array.Copy(s.window, r, s.window, q, 2);
                    q += 2;
                    r += 2;
                    c -= 2;
                  }
                } else {
                  IACSharpSensor.IACSharpSensor.SensorReached(6043);
                  r = q - d;
                  IACSharpSensor.IACSharpSensor.SensorReached(6044);
                  do {
                    IACSharpSensor.IACSharpSensor.SensorReached(6045);
                    r += s.end;
                  } while (r < 0);
                  IACSharpSensor.IACSharpSensor.SensorReached(6046);
                  e = s.end - r;
                  IACSharpSensor.IACSharpSensor.SensorReached(6047);
                  if (c > e) {
                    IACSharpSensor.IACSharpSensor.SensorReached(6048);
                    c -= e;
                    IACSharpSensor.IACSharpSensor.SensorReached(6049);
                    if (q - r > 0 && e > (q - r)) {
                      IACSharpSensor.IACSharpSensor.SensorReached(6050);
                      do {
                        IACSharpSensor.IACSharpSensor.SensorReached(6051);
                        s.window[q++] = s.window[r++];
                      } while (--e != 0);
                    } else {
                      IACSharpSensor.IACSharpSensor.SensorReached(6052);
                      Array.Copy(s.window, r, s.window, q, e);
                      q += e;
                      r += e;
                      e = 0;
                    }
                    IACSharpSensor.IACSharpSensor.SensorReached(6053);
                    r = 0;
                  }
                }
                IACSharpSensor.IACSharpSensor.SensorReached(6054);
                if (q - r > 0 && c > (q - r)) {
                  IACSharpSensor.IACSharpSensor.SensorReached(6055);
                  do {
                    IACSharpSensor.IACSharpSensor.SensorReached(6056);
                    s.window[q++] = s.window[r++];
                  } while (--c != 0);
                } else {
                  IACSharpSensor.IACSharpSensor.SensorReached(6057);
                  Array.Copy(s.window, r, s.window, q, c);
                  q += c;
                  r += c;
                  c = 0;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(6058);
                break;
              } else if ((e & 64) == 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(6061);
                t += tp[tp_index_t_3 + 2];
                t += (b & InternalInflateConstants.InflateMask[e]);
                tp_index_t_3 = (tp_index + t) * 3;
                e = tp[tp_index_t_3];
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(6059);
                z.Message = "invalid distance code";
                c = z.AvailableBytesIn - n;
                c = (k >> 3) < c ? k >> 3 : c;
                n += c;
                p -= c;
                k -= (c << 3);
                s.bitb = b;
                s.bitk = k;
                z.AvailableBytesIn = n;
                z.TotalBytesIn += p - z.NextIn;
                z.NextIn = p;
                s.writeAt = q;
                System.Int32 RNTRNTRNT_665 = ZlibConstants.Z_DATA_ERROR;
                IACSharpSensor.IACSharpSensor.SensorReached(6060);
                return RNTRNTRNT_665;
              }
            } while (true);
            IACSharpSensor.IACSharpSensor.SensorReached(6062);
            break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(6063);
          if ((e & 64) == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6064);
            t += tp[tp_index_t_3 + 2];
            t += (b & InternalInflateConstants.InflateMask[e]);
            tp_index_t_3 = (tp_index + t) * 3;
            IACSharpSensor.IACSharpSensor.SensorReached(6065);
            if ((e = tp[tp_index_t_3]) == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(6066);
              b >>= (tp[tp_index_t_3 + 1]);
              k -= (tp[tp_index_t_3 + 1]);
              s.window[q++] = (byte)tp[tp_index_t_3 + 2];
              m--;
              IACSharpSensor.IACSharpSensor.SensorReached(6067);
              break;
            }
          } else if ((e & 32) != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6070);
            c = z.AvailableBytesIn - n;
            c = (k >> 3) < c ? k >> 3 : c;
            n += c;
            p -= c;
            k -= (c << 3);
            s.bitb = b;
            s.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            s.writeAt = q;
            System.Int32 RNTRNTRNT_667 = ZlibConstants.Z_STREAM_END;
            IACSharpSensor.IACSharpSensor.SensorReached(6071);
            return RNTRNTRNT_667;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(6068);
            z.Message = "invalid literal/length code";
            c = z.AvailableBytesIn - n;
            c = (k >> 3) < c ? k >> 3 : c;
            n += c;
            p -= c;
            k -= (c << 3);
            s.bitb = b;
            s.bitk = k;
            z.AvailableBytesIn = n;
            z.TotalBytesIn += p - z.NextIn;
            z.NextIn = p;
            s.writeAt = q;
            System.Int32 RNTRNTRNT_666 = ZlibConstants.Z_DATA_ERROR;
            IACSharpSensor.IACSharpSensor.SensorReached(6069);
            return RNTRNTRNT_666;
          }
        } while (true);
      } while (m >= 258 && n >= 10);
      IACSharpSensor.IACSharpSensor.SensorReached(6072);
      c = z.AvailableBytesIn - n;
      c = (k >> 3) < c ? k >> 3 : c;
      n += c;
      p -= c;
      k -= (c << 3);
      s.bitb = b;
      s.bitk = k;
      z.AvailableBytesIn = n;
      z.TotalBytesIn += p - z.NextIn;
      z.NextIn = p;
      s.writeAt = q;
      System.Int32 RNTRNTRNT_668 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(6073);
      return RNTRNTRNT_668;
    }
  }
  internal sealed class InflateManager
  {
    private const int PRESET_DICT = 0x20;
    private const int Z_DEFLATED = 8;
    private enum InflateManagerMode
    {
      METHOD = 0,
      FLAG = 1,
      DICT4 = 2,
      DICT3 = 3,
      DICT2 = 4,
      DICT1 = 5,
      DICT0 = 6,
      BLOCKS = 7,
      CHECK4 = 8,
      CHECK3 = 9,
      CHECK2 = 10,
      CHECK1 = 11,
      DONE = 12,
      BAD = 13
    }
    private InflateManagerMode mode;
    internal ZlibCodec _codec;
    internal int method;
    internal uint computedCheck;
    internal uint expectedCheck;
    internal int marker;
    private bool _handleRfc1950HeaderBytes = true;
    internal bool HandleRfc1950HeaderBytes {
      get {
        System.Boolean RNTRNTRNT_669 = _handleRfc1950HeaderBytes;
        IACSharpSensor.IACSharpSensor.SensorReached(6074);
        return RNTRNTRNT_669;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6075);
        _handleRfc1950HeaderBytes = value;
        IACSharpSensor.IACSharpSensor.SensorReached(6076);
      }
    }
    internal int wbits;
    internal InflateBlocks blocks;
    public InflateManager()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6077);
    }
    public InflateManager(bool expectRfc1950HeaderBytes)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6078);
      _handleRfc1950HeaderBytes = expectRfc1950HeaderBytes;
      IACSharpSensor.IACSharpSensor.SensorReached(6079);
    }
    internal int Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6080);
      _codec.TotalBytesIn = _codec.TotalBytesOut = 0;
      _codec.Message = null;
      mode = HandleRfc1950HeaderBytes ? InflateManagerMode.METHOD : InflateManagerMode.BLOCKS;
      blocks.Reset();
      System.Int32 RNTRNTRNT_670 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(6081);
      return RNTRNTRNT_670;
    }
    internal int End()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6082);
      if (blocks != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6083);
        blocks.Free();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6084);
      blocks = null;
      System.Int32 RNTRNTRNT_671 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(6085);
      return RNTRNTRNT_671;
    }
    internal int Initialize(ZlibCodec codec, int w)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6086);
      _codec = codec;
      _codec.Message = null;
      blocks = null;
      IACSharpSensor.IACSharpSensor.SensorReached(6087);
      if (w < 8 || w > 15) {
        IACSharpSensor.IACSharpSensor.SensorReached(6088);
        End();
        IACSharpSensor.IACSharpSensor.SensorReached(6089);
        throw new ZlibException("Bad window size.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6090);
      wbits = w;
      blocks = new InflateBlocks(codec, HandleRfc1950HeaderBytes ? this : null, 1 << w);
      Reset();
      System.Int32 RNTRNTRNT_672 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(6091);
      return RNTRNTRNT_672;
    }
    internal int Inflate(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6092);
      int b;
      IACSharpSensor.IACSharpSensor.SensorReached(6093);
      if (_codec.InputBuffer == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6094);
        throw new ZlibException("InputBuffer is null. ");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6095);
      int f = ZlibConstants.Z_OK;
      int r = ZlibConstants.Z_BUF_ERROR;
      IACSharpSensor.IACSharpSensor.SensorReached(6096);
      while (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(6097);
        switch (mode) {
          case InflateManagerMode.METHOD:
            IACSharpSensor.IACSharpSensor.SensorReached(6098);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_673 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(6099);
              return RNTRNTRNT_673;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6100);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            IACSharpSensor.IACSharpSensor.SensorReached(6101);
            if (((method = _codec.InputBuffer[_codec.NextIn++]) & 0xf) != Z_DEFLATED) {
              IACSharpSensor.IACSharpSensor.SensorReached(6102);
              mode = InflateManagerMode.BAD;
              _codec.Message = String.Format("unknown compression method (0x{0:X2})", method);
              marker = 5;
              IACSharpSensor.IACSharpSensor.SensorReached(6103);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6104);
            if ((method >> 4) + 8 > wbits) {
              IACSharpSensor.IACSharpSensor.SensorReached(6105);
              mode = InflateManagerMode.BAD;
              _codec.Message = String.Format("invalid window size ({0})", (method >> 4) + 8);
              marker = 5;
              IACSharpSensor.IACSharpSensor.SensorReached(6106);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6107);
            mode = InflateManagerMode.FLAG;
            IACSharpSensor.IACSharpSensor.SensorReached(6108);
            break;
          case InflateManagerMode.FLAG:
            IACSharpSensor.IACSharpSensor.SensorReached(6109);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_674 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(6110);
              return RNTRNTRNT_674;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6111);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            b = (_codec.InputBuffer[_codec.NextIn++]) & 0xff;
            IACSharpSensor.IACSharpSensor.SensorReached(6112);
            if ((((method << 8) + b) % 31) != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(6113);
              mode = InflateManagerMode.BAD;
              _codec.Message = "incorrect header check";
              marker = 5;
              IACSharpSensor.IACSharpSensor.SensorReached(6114);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6115);
            mode = ((b & PRESET_DICT) == 0) ? InflateManagerMode.BLOCKS : InflateManagerMode.DICT4;
            IACSharpSensor.IACSharpSensor.SensorReached(6116);
            break;
          case InflateManagerMode.DICT4:
            IACSharpSensor.IACSharpSensor.SensorReached(6117);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_675 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(6118);
              return RNTRNTRNT_675;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6119);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck = (uint)((_codec.InputBuffer[_codec.NextIn++] << 24) & 0xff000000u);
            mode = InflateManagerMode.DICT3;
            IACSharpSensor.IACSharpSensor.SensorReached(6120);
            break;
          case InflateManagerMode.DICT3:
            IACSharpSensor.IACSharpSensor.SensorReached(6121);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_676 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(6122);
              return RNTRNTRNT_676;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6123);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 16) & 0xff0000);
            mode = InflateManagerMode.DICT2;
            IACSharpSensor.IACSharpSensor.SensorReached(6124);
            break;
          case InflateManagerMode.DICT2:
            IACSharpSensor.IACSharpSensor.SensorReached(6125);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_677 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(6126);
              return RNTRNTRNT_677;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6127);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 8) & 0xff00);
            mode = InflateManagerMode.DICT1;
            IACSharpSensor.IACSharpSensor.SensorReached(6128);
            break;
          case InflateManagerMode.DICT1:
            IACSharpSensor.IACSharpSensor.SensorReached(6129);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_678 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(6130);
              return RNTRNTRNT_678;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6131);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck += (uint)(_codec.InputBuffer[_codec.NextIn++] & 0xff);
            _codec._Adler32 = expectedCheck;
            mode = InflateManagerMode.DICT0;
            System.Int32 RNTRNTRNT_679 = ZlibConstants.Z_NEED_DICT;
            IACSharpSensor.IACSharpSensor.SensorReached(6132);
            return RNTRNTRNT_679;
          case InflateManagerMode.DICT0:
            IACSharpSensor.IACSharpSensor.SensorReached(6133);
            mode = InflateManagerMode.BAD;
            _codec.Message = "need dictionary";
            marker = 0;
            System.Int32 RNTRNTRNT_680 = ZlibConstants.Z_STREAM_ERROR;
            IACSharpSensor.IACSharpSensor.SensorReached(6134);
            return RNTRNTRNT_680;
          case InflateManagerMode.BLOCKS:
            IACSharpSensor.IACSharpSensor.SensorReached(6135);
            r = blocks.Process(r);
            IACSharpSensor.IACSharpSensor.SensorReached(6136);
            if (r == ZlibConstants.Z_DATA_ERROR) {
              IACSharpSensor.IACSharpSensor.SensorReached(6137);
              mode = InflateManagerMode.BAD;
              marker = 0;
              IACSharpSensor.IACSharpSensor.SensorReached(6138);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6139);
            if (r == ZlibConstants.Z_OK) {
              IACSharpSensor.IACSharpSensor.SensorReached(6140);
              r = f;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6141);
            if (r != ZlibConstants.Z_STREAM_END) {
              System.Int32 RNTRNTRNT_681 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(6142);
              return RNTRNTRNT_681;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6143);
            r = f;
            computedCheck = blocks.Reset();
            IACSharpSensor.IACSharpSensor.SensorReached(6144);
            if (!HandleRfc1950HeaderBytes) {
              IACSharpSensor.IACSharpSensor.SensorReached(6145);
              mode = InflateManagerMode.DONE;
              System.Int32 RNTRNTRNT_682 = ZlibConstants.Z_STREAM_END;
              IACSharpSensor.IACSharpSensor.SensorReached(6146);
              return RNTRNTRNT_682;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6147);
            mode = InflateManagerMode.CHECK4;
            IACSharpSensor.IACSharpSensor.SensorReached(6148);
            break;
          case InflateManagerMode.CHECK4:
            IACSharpSensor.IACSharpSensor.SensorReached(6149);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_683 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(6150);
              return RNTRNTRNT_683;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6151);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck = (uint)((_codec.InputBuffer[_codec.NextIn++] << 24) & 0xff000000u);
            mode = InflateManagerMode.CHECK3;
            IACSharpSensor.IACSharpSensor.SensorReached(6152);
            break;
          case InflateManagerMode.CHECK3:
            IACSharpSensor.IACSharpSensor.SensorReached(6153);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_684 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(6154);
              return RNTRNTRNT_684;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6155);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 16) & 0xff0000);
            mode = InflateManagerMode.CHECK2;
            IACSharpSensor.IACSharpSensor.SensorReached(6156);
            break;
          case InflateManagerMode.CHECK2:
            IACSharpSensor.IACSharpSensor.SensorReached(6157);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_685 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(6158);
              return RNTRNTRNT_685;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6159);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck += (uint)((_codec.InputBuffer[_codec.NextIn++] << 8) & 0xff00);
            mode = InflateManagerMode.CHECK1;
            IACSharpSensor.IACSharpSensor.SensorReached(6160);
            break;
          case InflateManagerMode.CHECK1:
            IACSharpSensor.IACSharpSensor.SensorReached(6161);
            if (_codec.AvailableBytesIn == 0) {
              System.Int32 RNTRNTRNT_686 = r;
              IACSharpSensor.IACSharpSensor.SensorReached(6162);
              return RNTRNTRNT_686;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6163);
            r = f;
            _codec.AvailableBytesIn--;
            _codec.TotalBytesIn++;
            expectedCheck += (uint)(_codec.InputBuffer[_codec.NextIn++] & 0xff);
            IACSharpSensor.IACSharpSensor.SensorReached(6164);
            if (computedCheck != expectedCheck) {
              IACSharpSensor.IACSharpSensor.SensorReached(6165);
              mode = InflateManagerMode.BAD;
              _codec.Message = "incorrect data check";
              marker = 5;
              IACSharpSensor.IACSharpSensor.SensorReached(6166);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6167);
            mode = InflateManagerMode.DONE;
            System.Int32 RNTRNTRNT_687 = ZlibConstants.Z_STREAM_END;
            IACSharpSensor.IACSharpSensor.SensorReached(6168);
            return RNTRNTRNT_687;
          case InflateManagerMode.DONE:
            System.Int32 RNTRNTRNT_688 = ZlibConstants.Z_STREAM_END;
            IACSharpSensor.IACSharpSensor.SensorReached(6169);
            return RNTRNTRNT_688;
          case InflateManagerMode.BAD:
            IACSharpSensor.IACSharpSensor.SensorReached(6170);
            throw new ZlibException(String.Format("Bad state ({0})", _codec.Message));
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(6171);
            throw new ZlibException("Stream error.");
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6172);
    }
    internal int SetDictionary(byte[] dictionary)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6173);
      int index = 0;
      int length = dictionary.Length;
      IACSharpSensor.IACSharpSensor.SensorReached(6174);
      if (mode != InflateManagerMode.DICT0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6175);
        throw new ZlibException("Stream error.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6176);
      if (Adler.Adler32(1, dictionary, 0, dictionary.Length) != _codec._Adler32) {
        System.Int32 RNTRNTRNT_689 = ZlibConstants.Z_DATA_ERROR;
        IACSharpSensor.IACSharpSensor.SensorReached(6177);
        return RNTRNTRNT_689;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6178);
      _codec._Adler32 = Adler.Adler32(0, null, 0, 0);
      IACSharpSensor.IACSharpSensor.SensorReached(6179);
      if (length >= (1 << wbits)) {
        IACSharpSensor.IACSharpSensor.SensorReached(6180);
        length = (1 << wbits) - 1;
        index = dictionary.Length - length;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6181);
      blocks.SetDictionary(dictionary, index, length);
      mode = InflateManagerMode.BLOCKS;
      System.Int32 RNTRNTRNT_690 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(6182);
      return RNTRNTRNT_690;
    }
    private static readonly byte[] mark = new byte[] {
      0,
      0,
      0xff,
      0xff
    };
    internal int Sync()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6183);
      int n;
      int p;
      int m;
      long r, w;
      IACSharpSensor.IACSharpSensor.SensorReached(6184);
      if (mode != InflateManagerMode.BAD) {
        IACSharpSensor.IACSharpSensor.SensorReached(6185);
        mode = InflateManagerMode.BAD;
        marker = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6186);
      if ((n = _codec.AvailableBytesIn) == 0) {
        System.Int32 RNTRNTRNT_691 = ZlibConstants.Z_BUF_ERROR;
        IACSharpSensor.IACSharpSensor.SensorReached(6187);
        return RNTRNTRNT_691;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6188);
      p = _codec.NextIn;
      m = marker;
      IACSharpSensor.IACSharpSensor.SensorReached(6189);
      while (n != 0 && m < 4) {
        IACSharpSensor.IACSharpSensor.SensorReached(6190);
        if (_codec.InputBuffer[p] == mark[m]) {
          IACSharpSensor.IACSharpSensor.SensorReached(6191);
          m++;
        } else if (_codec.InputBuffer[p] != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(6193);
          m = 0;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(6192);
          m = 4 - m;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6194);
        p++;
        n--;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6195);
      _codec.TotalBytesIn += p - _codec.NextIn;
      _codec.NextIn = p;
      _codec.AvailableBytesIn = n;
      marker = m;
      IACSharpSensor.IACSharpSensor.SensorReached(6196);
      if (m != 4) {
        System.Int32 RNTRNTRNT_692 = ZlibConstants.Z_DATA_ERROR;
        IACSharpSensor.IACSharpSensor.SensorReached(6197);
        return RNTRNTRNT_692;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6198);
      r = _codec.TotalBytesIn;
      w = _codec.TotalBytesOut;
      Reset();
      _codec.TotalBytesIn = r;
      _codec.TotalBytesOut = w;
      mode = InflateManagerMode.BLOCKS;
      System.Int32 RNTRNTRNT_693 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(6199);
      return RNTRNTRNT_693;
    }
    internal int SyncPoint(ZlibCodec z)
    {
      System.Int32 RNTRNTRNT_694 = blocks.SyncPoint();
      IACSharpSensor.IACSharpSensor.SensorReached(6200);
      return RNTRNTRNT_694;
    }
  }
}
