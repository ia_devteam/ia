using System;
namespace Ionic.Zlib
{
  public class DeflateStream : System.IO.Stream
  {
    internal ZlibBaseStream _baseStream;
    internal System.IO.Stream _innerStream;
    bool _disposed;
    public DeflateStream(System.IO.Stream stream, CompressionMode mode) : this(stream, mode, CompressionLevel.Default, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5535);
    }
    public DeflateStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level) : this(stream, mode, level, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5536);
    }
    public DeflateStream(System.IO.Stream stream, CompressionMode mode, bool leaveOpen) : this(stream, mode, CompressionLevel.Default, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5537);
    }
    public DeflateStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5538);
      _innerStream = stream;
      _baseStream = new ZlibBaseStream(stream, mode, level, ZlibStreamFlavor.DEFLATE, leaveOpen);
      IACSharpSensor.IACSharpSensor.SensorReached(5539);
    }
    public virtual FlushType FlushMode {
      get {
        FlushType RNTRNTRNT_595 = (this._baseStream._flushMode);
        IACSharpSensor.IACSharpSensor.SensorReached(5540);
        return RNTRNTRNT_595;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5541);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5542);
          throw new ObjectDisposedException("DeflateStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5543);
        this._baseStream._flushMode = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5544);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_596 = this._baseStream._bufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(5545);
        return RNTRNTRNT_596;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5546);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5547);
          throw new ObjectDisposedException("DeflateStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5548);
        if (this._baseStream._workingBuffer != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(5549);
          throw new ZlibException("The working buffer is already set.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5550);
        if (value < ZlibConstants.WorkingBufferSizeMin) {
          IACSharpSensor.IACSharpSensor.SensorReached(5551);
          throw new ZlibException(String.Format("Don't be silly. {0} bytes?? Use a bigger buffer, at least {1}.", value, ZlibConstants.WorkingBufferSizeMin));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5552);
        this._baseStream._bufferSize = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5553);
      }
    }
    public CompressionStrategy Strategy {
      get {
        CompressionStrategy RNTRNTRNT_597 = this._baseStream.Strategy;
        IACSharpSensor.IACSharpSensor.SensorReached(5554);
        return RNTRNTRNT_597;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5555);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5556);
          throw new ObjectDisposedException("DeflateStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5557);
        this._baseStream.Strategy = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5558);
      }
    }
    public virtual long TotalIn {
      get {
        System.Int64 RNTRNTRNT_598 = this._baseStream._z.TotalBytesIn;
        IACSharpSensor.IACSharpSensor.SensorReached(5559);
        return RNTRNTRNT_598;
      }
    }
    public virtual long TotalOut {
      get {
        System.Int64 RNTRNTRNT_599 = this._baseStream._z.TotalBytesOut;
        IACSharpSensor.IACSharpSensor.SensorReached(5560);
        return RNTRNTRNT_599;
      }
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5561);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(5562);
        if (!_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5563);
          if (disposing && (this._baseStream != null)) {
            IACSharpSensor.IACSharpSensor.SensorReached(5564);
            this._baseStream.Close();
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5565);
          _disposed = true;
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(5566);
        base.Dispose(disposing);
        IACSharpSensor.IACSharpSensor.SensorReached(5567);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5568);
    }
    public override bool CanRead {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5569);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5570);
          throw new ObjectDisposedException("DeflateStream");
        }
        System.Boolean RNTRNTRNT_600 = _baseStream._stream.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(5571);
        return RNTRNTRNT_600;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_601 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(5572);
        return RNTRNTRNT_601;
      }
    }
    public override bool CanWrite {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5573);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5574);
          throw new ObjectDisposedException("DeflateStream");
        }
        System.Boolean RNTRNTRNT_602 = _baseStream._stream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(5575);
        return RNTRNTRNT_602;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5576);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5577);
        throw new ObjectDisposedException("DeflateStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5578);
      _baseStream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(5579);
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5580);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5581);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Writer) {
          System.Int64 RNTRNTRNT_603 = this._baseStream._z.TotalBytesOut;
          IACSharpSensor.IACSharpSensor.SensorReached(5582);
          return RNTRNTRNT_603;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5583);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Reader) {
          System.Int64 RNTRNTRNT_604 = this._baseStream._z.TotalBytesIn;
          IACSharpSensor.IACSharpSensor.SensorReached(5584);
          return RNTRNTRNT_604;
        }
        System.Int64 RNTRNTRNT_605 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(5585);
        return RNTRNTRNT_605;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5586);
        throw new NotImplementedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5587);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5588);
        throw new ObjectDisposedException("DeflateStream");
      }
      System.Int32 RNTRNTRNT_606 = _baseStream.Read(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(5589);
      return RNTRNTRNT_606;
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5590);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5591);
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5592);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5593);
        throw new ObjectDisposedException("DeflateStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5594);
      _baseStream.Write(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(5595);
    }
    public static byte[] CompressString(String s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5596);
      using (var ms = new System.IO.MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(5597);
        System.IO.Stream compressor = new DeflateStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressString(s, compressor);
        System.Byte[] RNTRNTRNT_607 = ms.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(5598);
        return RNTRNTRNT_607;
      }
    }
    public static byte[] CompressBuffer(byte[] b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5599);
      using (var ms = new System.IO.MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(5600);
        System.IO.Stream compressor = new DeflateStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressBuffer(b, compressor);
        System.Byte[] RNTRNTRNT_608 = ms.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(5601);
        return RNTRNTRNT_608;
      }
    }
    public static String UncompressString(byte[] compressed)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5602);
      using (var input = new System.IO.MemoryStream(compressed)) {
        IACSharpSensor.IACSharpSensor.SensorReached(5603);
        System.IO.Stream decompressor = new DeflateStream(input, CompressionMode.Decompress);
        String RNTRNTRNT_609 = ZlibBaseStream.UncompressString(compressed, decompressor);
        IACSharpSensor.IACSharpSensor.SensorReached(5604);
        return RNTRNTRNT_609;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5605);
      using (var input = new System.IO.MemoryStream(compressed)) {
        IACSharpSensor.IACSharpSensor.SensorReached(5606);
        System.IO.Stream decompressor = new DeflateStream(input, CompressionMode.Decompress);
        System.Byte[] RNTRNTRNT_610 = ZlibBaseStream.UncompressBuffer(compressed, decompressor);
        IACSharpSensor.IACSharpSensor.SensorReached(5607);
        return RNTRNTRNT_610;
      }
    }
  }
}
