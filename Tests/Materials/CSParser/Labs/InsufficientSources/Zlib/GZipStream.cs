using System;
using System.IO;
namespace Ionic.Zlib
{
  public class GZipStream : System.IO.Stream
  {
    public String Comment {
      get {
        String RNTRNTRNT_611 = _Comment;
        IACSharpSensor.IACSharpSensor.SensorReached(5608);
        return RNTRNTRNT_611;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5609);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5610);
          throw new ObjectDisposedException("GZipStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5611);
        _Comment = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5612);
      }
    }
    public String FileName {
      get {
        String RNTRNTRNT_612 = _FileName;
        IACSharpSensor.IACSharpSensor.SensorReached(5613);
        return RNTRNTRNT_612;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5614);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5615);
          throw new ObjectDisposedException("GZipStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5616);
        _FileName = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5617);
        if (_FileName == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(5618);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5619);
        if (_FileName.IndexOf("/") != -1) {
          IACSharpSensor.IACSharpSensor.SensorReached(5620);
          _FileName = _FileName.Replace("/", "\\");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5621);
        if (_FileName.EndsWith("\\")) {
          IACSharpSensor.IACSharpSensor.SensorReached(5622);
          throw new Exception("Illegal filename");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5623);
        if (_FileName.IndexOf("\\") != -1) {
          IACSharpSensor.IACSharpSensor.SensorReached(5624);
          _FileName = Path.GetFileName(_FileName);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5625);
      }
    }
    public DateTime? LastModified;
    public int Crc32 {
      get {
        System.Int32 RNTRNTRNT_613 = _Crc32;
        IACSharpSensor.IACSharpSensor.SensorReached(5626);
        return RNTRNTRNT_613;
      }
    }
    private int _headerByteCount;
    internal ZlibBaseStream _baseStream;
    bool _disposed;
    bool _firstReadDone;
    string _FileName;
    string _Comment;
    int _Crc32;
    public GZipStream(Stream stream, CompressionMode mode) : this(stream, mode, CompressionLevel.Default, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5627);
    }
    public GZipStream(Stream stream, CompressionMode mode, CompressionLevel level) : this(stream, mode, level, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5628);
    }
    public GZipStream(Stream stream, CompressionMode mode, bool leaveOpen) : this(stream, mode, CompressionLevel.Default, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5629);
    }
    public GZipStream(Stream stream, CompressionMode mode, CompressionLevel level, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5630);
      _baseStream = new ZlibBaseStream(stream, mode, level, ZlibStreamFlavor.GZIP, leaveOpen);
      IACSharpSensor.IACSharpSensor.SensorReached(5631);
    }
    public virtual FlushType FlushMode {
      get {
        FlushType RNTRNTRNT_614 = (this._baseStream._flushMode);
        IACSharpSensor.IACSharpSensor.SensorReached(5632);
        return RNTRNTRNT_614;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5633);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5634);
          throw new ObjectDisposedException("GZipStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5635);
        this._baseStream._flushMode = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5636);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_615 = this._baseStream._bufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(5637);
        return RNTRNTRNT_615;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5638);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5639);
          throw new ObjectDisposedException("GZipStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5640);
        if (this._baseStream._workingBuffer != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(5641);
          throw new ZlibException("The working buffer is already set.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5642);
        if (value < ZlibConstants.WorkingBufferSizeMin) {
          IACSharpSensor.IACSharpSensor.SensorReached(5643);
          throw new ZlibException(String.Format("Don't be silly. {0} bytes?? Use a bigger buffer, at least {1}.", value, ZlibConstants.WorkingBufferSizeMin));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5644);
        this._baseStream._bufferSize = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5645);
      }
    }
    public virtual long TotalIn {
      get {
        System.Int64 RNTRNTRNT_616 = this._baseStream._z.TotalBytesIn;
        IACSharpSensor.IACSharpSensor.SensorReached(5646);
        return RNTRNTRNT_616;
      }
    }
    public virtual long TotalOut {
      get {
        System.Int64 RNTRNTRNT_617 = this._baseStream._z.TotalBytesOut;
        IACSharpSensor.IACSharpSensor.SensorReached(5647);
        return RNTRNTRNT_617;
      }
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5648);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(5649);
        if (!_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5650);
          if (disposing && (this._baseStream != null)) {
            IACSharpSensor.IACSharpSensor.SensorReached(5651);
            this._baseStream.Close();
            this._Crc32 = _baseStream.Crc32;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5652);
          _disposed = true;
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(5653);
        base.Dispose(disposing);
        IACSharpSensor.IACSharpSensor.SensorReached(5654);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5655);
    }
    public override bool CanRead {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5656);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5657);
          throw new ObjectDisposedException("GZipStream");
        }
        System.Boolean RNTRNTRNT_618 = _baseStream._stream.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(5658);
        return RNTRNTRNT_618;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_619 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(5659);
        return RNTRNTRNT_619;
      }
    }
    public override bool CanWrite {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5660);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(5661);
          throw new ObjectDisposedException("GZipStream");
        }
        System.Boolean RNTRNTRNT_620 = _baseStream._stream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(5662);
        return RNTRNTRNT_620;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5663);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5664);
        throw new ObjectDisposedException("GZipStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5665);
      _baseStream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(5666);
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5667);
        throw new NotImplementedException();
      }
    }
    public override long Position {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5668);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Writer) {
          System.Int64 RNTRNTRNT_621 = this._baseStream._z.TotalBytesOut + _headerByteCount;
          IACSharpSensor.IACSharpSensor.SensorReached(5669);
          return RNTRNTRNT_621;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5670);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Reader) {
          System.Int64 RNTRNTRNT_622 = this._baseStream._z.TotalBytesIn + this._baseStream._gzipHeaderByteCount;
          IACSharpSensor.IACSharpSensor.SensorReached(5671);
          return RNTRNTRNT_622;
        }
        System.Int64 RNTRNTRNT_623 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(5672);
        return RNTRNTRNT_623;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5673);
        throw new NotImplementedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5674);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5675);
        throw new ObjectDisposedException("GZipStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5676);
      int n = _baseStream.Read(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(5677);
      if (!_firstReadDone) {
        IACSharpSensor.IACSharpSensor.SensorReached(5678);
        _firstReadDone = true;
        FileName = _baseStream._GzipFileName;
        Comment = _baseStream._GzipComment;
      }
      System.Int32 RNTRNTRNT_624 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(5679);
      return RNTRNTRNT_624;
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5680);
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5681);
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5682);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(5683);
        throw new ObjectDisposedException("GZipStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5684);
      if (_baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Undefined) {
        IACSharpSensor.IACSharpSensor.SensorReached(5685);
        if (_baseStream._wantCompress) {
          IACSharpSensor.IACSharpSensor.SensorReached(5686);
          _headerByteCount = EmitHeader();
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5687);
          throw new InvalidOperationException();
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5688);
      _baseStream.Write(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(5689);
    }
    static internal readonly System.DateTime _unixEpoch = new System.DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    static internal readonly System.Text.Encoding iso8859dash1 = System.Text.Encoding.GetEncoding("iso-8859-1");
    private int EmitHeader()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5690);
      byte[] commentBytes = (Comment == null) ? null : iso8859dash1.GetBytes(Comment);
      byte[] filenameBytes = (FileName == null) ? null : iso8859dash1.GetBytes(FileName);
      int cbLength = (Comment == null) ? 0 : commentBytes.Length + 1;
      int fnLength = (FileName == null) ? 0 : filenameBytes.Length + 1;
      int bufferLength = 10 + cbLength + fnLength;
      byte[] header = new byte[bufferLength];
      int i = 0;
      header[i++] = 0x1f;
      header[i++] = 0x8b;
      header[i++] = 8;
      byte flag = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5691);
      if (Comment != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(5692);
        flag ^= 0x10;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5693);
      if (FileName != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(5694);
        flag ^= 0x8;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5695);
      header[i++] = flag;
      IACSharpSensor.IACSharpSensor.SensorReached(5696);
      if (!LastModified.HasValue) {
        IACSharpSensor.IACSharpSensor.SensorReached(5697);
        LastModified = DateTime.Now;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5698);
      System.TimeSpan delta = LastModified.Value - _unixEpoch;
      Int32 timet = (Int32)delta.TotalSeconds;
      Array.Copy(BitConverter.GetBytes(timet), 0, header, i, 4);
      i += 4;
      header[i++] = 0;
      header[i++] = 0xff;
      IACSharpSensor.IACSharpSensor.SensorReached(5699);
      if (fnLength != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5700);
        Array.Copy(filenameBytes, 0, header, i, fnLength - 1);
        i += fnLength - 1;
        header[i++] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5701);
      if (cbLength != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5702);
        Array.Copy(commentBytes, 0, header, i, cbLength - 1);
        i += cbLength - 1;
        header[i++] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5703);
      _baseStream._stream.Write(header, 0, header.Length);
      System.Int32 RNTRNTRNT_625 = header.Length;
      IACSharpSensor.IACSharpSensor.SensorReached(5704);
      return RNTRNTRNT_625;
    }
    public static byte[] CompressString(String s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5705);
      using (var ms = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(5706);
        System.IO.Stream compressor = new GZipStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressString(s, compressor);
        System.Byte[] RNTRNTRNT_626 = ms.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(5707);
        return RNTRNTRNT_626;
      }
    }
    public static byte[] CompressBuffer(byte[] b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5708);
      using (var ms = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(5709);
        System.IO.Stream compressor = new GZipStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressBuffer(b, compressor);
        System.Byte[] RNTRNTRNT_627 = ms.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(5710);
        return RNTRNTRNT_627;
      }
    }
    public static String UncompressString(byte[] compressed)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5711);
      using (var input = new MemoryStream(compressed)) {
        IACSharpSensor.IACSharpSensor.SensorReached(5712);
        Stream decompressor = new GZipStream(input, CompressionMode.Decompress);
        String RNTRNTRNT_628 = ZlibBaseStream.UncompressString(compressed, decompressor);
        IACSharpSensor.IACSharpSensor.SensorReached(5713);
        return RNTRNTRNT_628;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5714);
      using (var input = new System.IO.MemoryStream(compressed)) {
        IACSharpSensor.IACSharpSensor.SensorReached(5715);
        System.IO.Stream decompressor = new GZipStream(input, CompressionMode.Decompress);
        System.Byte[] RNTRNTRNT_629 = ZlibBaseStream.UncompressBuffer(compressed, decompressor);
        IACSharpSensor.IACSharpSensor.SensorReached(5716);
        return RNTRNTRNT_629;
      }
    }
  }
}
