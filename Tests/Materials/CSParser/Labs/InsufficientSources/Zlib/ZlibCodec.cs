using System;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Zlib
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000D")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public sealed class ZlibCodec
  {
    public byte[] InputBuffer;
    public int NextIn;
    public int AvailableBytesIn;
    public long TotalBytesIn;
    public byte[] OutputBuffer;
    public int NextOut;
    public int AvailableBytesOut;
    public long TotalBytesOut;
    public System.String Message;
    internal DeflateManager dstate;
    internal InflateManager istate;
    internal uint _Adler32;
    public CompressionLevel CompressLevel = CompressionLevel.Default;
    public int WindowBits = ZlibConstants.WindowBitsDefault;
    public CompressionStrategy Strategy = CompressionStrategy.Default;
    public int Adler32 {
      get {
        System.Int32 RNTRNTRNT_743 = (int)_Adler32;
        IACSharpSensor.IACSharpSensor.SensorReached(6747);
        return RNTRNTRNT_743;
      }
    }
    public ZlibCodec()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6748);
    }
    public ZlibCodec(CompressionMode mode)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6749);
      if (mode == CompressionMode.Compress) {
        IACSharpSensor.IACSharpSensor.SensorReached(6750);
        int rc = InitializeDeflate();
        IACSharpSensor.IACSharpSensor.SensorReached(6751);
        if (rc != ZlibConstants.Z_OK) {
          IACSharpSensor.IACSharpSensor.SensorReached(6752);
          throw new ZlibException("Cannot initialize for deflate.");
        }
      } else if (mode == CompressionMode.Decompress) {
        IACSharpSensor.IACSharpSensor.SensorReached(6754);
        int rc = InitializeInflate();
        IACSharpSensor.IACSharpSensor.SensorReached(6755);
        if (rc != ZlibConstants.Z_OK) {
          IACSharpSensor.IACSharpSensor.SensorReached(6756);
          throw new ZlibException("Cannot initialize for inflate.");
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(6753);
        throw new ZlibException("Invalid ZlibStreamFlavor.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6757);
    }
    public int InitializeInflate()
    {
      System.Int32 RNTRNTRNT_744 = InitializeInflate(this.WindowBits);
      IACSharpSensor.IACSharpSensor.SensorReached(6758);
      return RNTRNTRNT_744;
    }
    public int InitializeInflate(bool expectRfc1950Header)
    {
      System.Int32 RNTRNTRNT_745 = InitializeInflate(this.WindowBits, expectRfc1950Header);
      IACSharpSensor.IACSharpSensor.SensorReached(6759);
      return RNTRNTRNT_745;
    }
    public int InitializeInflate(int windowBits)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6760);
      this.WindowBits = windowBits;
      System.Int32 RNTRNTRNT_746 = InitializeInflate(windowBits, true);
      IACSharpSensor.IACSharpSensor.SensorReached(6761);
      return RNTRNTRNT_746;
    }
    public int InitializeInflate(int windowBits, bool expectRfc1950Header)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6762);
      this.WindowBits = windowBits;
      IACSharpSensor.IACSharpSensor.SensorReached(6763);
      if (dstate != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6764);
        throw new ZlibException("You may not call InitializeInflate() after calling InitializeDeflate().");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6765);
      istate = new InflateManager(expectRfc1950Header);
      System.Int32 RNTRNTRNT_747 = istate.Initialize(this, windowBits);
      IACSharpSensor.IACSharpSensor.SensorReached(6766);
      return RNTRNTRNT_747;
    }
    public int Inflate(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6767);
      if (istate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6768);
        throw new ZlibException("No Inflate State!");
      }
      System.Int32 RNTRNTRNT_748 = istate.Inflate(flush);
      IACSharpSensor.IACSharpSensor.SensorReached(6769);
      return RNTRNTRNT_748;
    }
    public int EndInflate()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6770);
      if (istate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6771);
        throw new ZlibException("No Inflate State!");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6772);
      int ret = istate.End();
      istate = null;
      System.Int32 RNTRNTRNT_749 = ret;
      IACSharpSensor.IACSharpSensor.SensorReached(6773);
      return RNTRNTRNT_749;
    }
    public int SyncInflate()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6774);
      if (istate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6775);
        throw new ZlibException("No Inflate State!");
      }
      System.Int32 RNTRNTRNT_750 = istate.Sync();
      IACSharpSensor.IACSharpSensor.SensorReached(6776);
      return RNTRNTRNT_750;
    }
    public int InitializeDeflate()
    {
      System.Int32 RNTRNTRNT_751 = _InternalInitializeDeflate(true);
      IACSharpSensor.IACSharpSensor.SensorReached(6777);
      return RNTRNTRNT_751;
    }
    public int InitializeDeflate(CompressionLevel level)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6778);
      this.CompressLevel = level;
      System.Int32 RNTRNTRNT_752 = _InternalInitializeDeflate(true);
      IACSharpSensor.IACSharpSensor.SensorReached(6779);
      return RNTRNTRNT_752;
    }
    public int InitializeDeflate(CompressionLevel level, bool wantRfc1950Header)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6780);
      this.CompressLevel = level;
      System.Int32 RNTRNTRNT_753 = _InternalInitializeDeflate(wantRfc1950Header);
      IACSharpSensor.IACSharpSensor.SensorReached(6781);
      return RNTRNTRNT_753;
    }
    public int InitializeDeflate(CompressionLevel level, int bits)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6782);
      this.CompressLevel = level;
      this.WindowBits = bits;
      System.Int32 RNTRNTRNT_754 = _InternalInitializeDeflate(true);
      IACSharpSensor.IACSharpSensor.SensorReached(6783);
      return RNTRNTRNT_754;
    }
    public int InitializeDeflate(CompressionLevel level, int bits, bool wantRfc1950Header)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6784);
      this.CompressLevel = level;
      this.WindowBits = bits;
      System.Int32 RNTRNTRNT_755 = _InternalInitializeDeflate(wantRfc1950Header);
      IACSharpSensor.IACSharpSensor.SensorReached(6785);
      return RNTRNTRNT_755;
    }
    private int _InternalInitializeDeflate(bool wantRfc1950Header)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6786);
      if (istate != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6787);
        throw new ZlibException("You may not call InitializeDeflate() after calling InitializeInflate().");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6788);
      dstate = new DeflateManager();
      dstate.WantRfc1950HeaderBytes = wantRfc1950Header;
      System.Int32 RNTRNTRNT_756 = dstate.Initialize(this, this.CompressLevel, this.WindowBits, this.Strategy);
      IACSharpSensor.IACSharpSensor.SensorReached(6789);
      return RNTRNTRNT_756;
    }
    public int Deflate(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6790);
      if (dstate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6791);
        throw new ZlibException("No Deflate State!");
      }
      System.Int32 RNTRNTRNT_757 = dstate.Deflate(flush);
      IACSharpSensor.IACSharpSensor.SensorReached(6792);
      return RNTRNTRNT_757;
    }
    public int EndDeflate()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6793);
      if (dstate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6794);
        throw new ZlibException("No Deflate State!");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6795);
      dstate = null;
      System.Int32 RNTRNTRNT_758 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(6796);
      return RNTRNTRNT_758;
    }
    public void ResetDeflate()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6797);
      if (dstate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6798);
        throw new ZlibException("No Deflate State!");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6799);
      dstate.Reset();
      IACSharpSensor.IACSharpSensor.SensorReached(6800);
    }
    public int SetDeflateParams(CompressionLevel level, CompressionStrategy strategy)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6801);
      if (dstate == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6802);
        throw new ZlibException("No Deflate State!");
      }
      System.Int32 RNTRNTRNT_759 = dstate.SetParams(level, strategy);
      IACSharpSensor.IACSharpSensor.SensorReached(6803);
      return RNTRNTRNT_759;
    }
    public int SetDictionary(byte[] dictionary)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6804);
      if (istate != null) {
        System.Int32 RNTRNTRNT_760 = istate.SetDictionary(dictionary);
        IACSharpSensor.IACSharpSensor.SensorReached(6805);
        return RNTRNTRNT_760;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6806);
      if (dstate != null) {
        System.Int32 RNTRNTRNT_761 = dstate.SetDictionary(dictionary);
        IACSharpSensor.IACSharpSensor.SensorReached(6807);
        return RNTRNTRNT_761;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6808);
      throw new ZlibException("No Inflate or Deflate state!");
    }
    internal void flush_pending()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6809);
      int len = dstate.pendingCount;
      IACSharpSensor.IACSharpSensor.SensorReached(6810);
      if (len > AvailableBytesOut) {
        IACSharpSensor.IACSharpSensor.SensorReached(6811);
        len = AvailableBytesOut;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6812);
      if (len == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6813);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6814);
      if (dstate.pending.Length <= dstate.nextPending || OutputBuffer.Length <= NextOut || dstate.pending.Length < (dstate.nextPending + len) || OutputBuffer.Length < (NextOut + len)) {
        IACSharpSensor.IACSharpSensor.SensorReached(6815);
        throw new ZlibException(String.Format("Invalid State. (pending.Length={0}, pendingCount={1})", dstate.pending.Length, dstate.pendingCount));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6816);
      Array.Copy(dstate.pending, dstate.nextPending, OutputBuffer, NextOut, len);
      NextOut += len;
      dstate.nextPending += len;
      TotalBytesOut += len;
      AvailableBytesOut -= len;
      dstate.pendingCount -= len;
      IACSharpSensor.IACSharpSensor.SensorReached(6817);
      if (dstate.pendingCount == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6818);
        dstate.nextPending = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6819);
    }
    internal int read_buf(byte[] buf, int start, int size)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6820);
      int len = AvailableBytesIn;
      IACSharpSensor.IACSharpSensor.SensorReached(6821);
      if (len > size) {
        IACSharpSensor.IACSharpSensor.SensorReached(6822);
        len = size;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6823);
      if (len == 0) {
        System.Int32 RNTRNTRNT_762 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6824);
        return RNTRNTRNT_762;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6825);
      AvailableBytesIn -= len;
      IACSharpSensor.IACSharpSensor.SensorReached(6826);
      if (dstate.WantRfc1950HeaderBytes) {
        IACSharpSensor.IACSharpSensor.SensorReached(6827);
        _Adler32 = Adler.Adler32(_Adler32, InputBuffer, NextIn, len);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6828);
      Array.Copy(InputBuffer, NextIn, buf, start, len);
      NextIn += len;
      TotalBytesIn += len;
      System.Int32 RNTRNTRNT_763 = len;
      IACSharpSensor.IACSharpSensor.SensorReached(6829);
      return RNTRNTRNT_763;
    }
  }
}
