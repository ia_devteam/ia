using System;
using System.IO;
namespace Ionic.Zlib
{
  internal enum ZlibStreamFlavor
  {
    ZLIB = 1950,
    DEFLATE = 1951,
    GZIP = 1952
  }
  internal class ZlibBaseStream : System.IO.Stream
  {
    protected internal ZlibCodec _z = null;
    protected internal StreamMode _streamMode = StreamMode.Undefined;
    protected internal FlushType _flushMode;
    protected internal ZlibStreamFlavor _flavor;
    protected internal CompressionMode _compressionMode;
    protected internal CompressionLevel _level;
    protected internal bool _leaveOpen;
    protected internal byte[] _workingBuffer;
    protected internal int _bufferSize = ZlibConstants.WorkingBufferSizeDefault;
    protected internal byte[] _buf1 = new byte[1];
    protected internal System.IO.Stream _stream;
    protected internal CompressionStrategy Strategy = CompressionStrategy.Default;
    Ionic.Crc.CRC32 crc;
    protected internal string _GzipFileName;
    protected internal string _GzipComment;
    protected internal DateTime _GzipMtime;
    protected internal int _gzipHeaderByteCount;
    internal int Crc32 {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6550);
        if (crc == null) {
          System.Int32 RNTRNTRNT_724 = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(6551);
          return RNTRNTRNT_724;
        }
        System.Int32 RNTRNTRNT_725 = crc.Crc32Result;
        IACSharpSensor.IACSharpSensor.SensorReached(6552);
        return RNTRNTRNT_725;
      }
    }
    public ZlibBaseStream(System.IO.Stream stream, CompressionMode compressionMode, CompressionLevel level, ZlibStreamFlavor flavor, bool leaveOpen) : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6553);
      this._flushMode = FlushType.None;
      this._stream = stream;
      this._leaveOpen = leaveOpen;
      this._compressionMode = compressionMode;
      this._flavor = flavor;
      this._level = level;
      IACSharpSensor.IACSharpSensor.SensorReached(6554);
      if (flavor == ZlibStreamFlavor.GZIP) {
        IACSharpSensor.IACSharpSensor.SensorReached(6555);
        this.crc = new Ionic.Crc.CRC32();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6556);
    }
    protected internal bool _wantCompress {
      get {
        System.Boolean RNTRNTRNT_726 = (this._compressionMode == CompressionMode.Compress);
        IACSharpSensor.IACSharpSensor.SensorReached(6557);
        return RNTRNTRNT_726;
      }
    }
    private ZlibCodec z {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6558);
        if (_z == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(6559);
          bool wantRfc1950Header = (this._flavor == ZlibStreamFlavor.ZLIB);
          _z = new ZlibCodec();
          IACSharpSensor.IACSharpSensor.SensorReached(6560);
          if (this._compressionMode == CompressionMode.Decompress) {
            IACSharpSensor.IACSharpSensor.SensorReached(6561);
            _z.InitializeInflate(wantRfc1950Header);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(6562);
            _z.Strategy = Strategy;
            _z.InitializeDeflate(this._level, wantRfc1950Header);
          }
        }
        ZlibCodec RNTRNTRNT_727 = _z;
        IACSharpSensor.IACSharpSensor.SensorReached(6563);
        return RNTRNTRNT_727;
      }
    }
    private byte[] workingBuffer {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6564);
        if (_workingBuffer == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(6565);
          _workingBuffer = new byte[_bufferSize];
        }
        System.Byte[] RNTRNTRNT_728 = _workingBuffer;
        IACSharpSensor.IACSharpSensor.SensorReached(6566);
        return RNTRNTRNT_728;
      }
    }
    public override void Write(System.Byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6567);
      if (crc != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6568);
        crc.SlurpBlock(buffer, offset, count);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6569);
      if (_streamMode == StreamMode.Undefined) {
        IACSharpSensor.IACSharpSensor.SensorReached(6570);
        _streamMode = StreamMode.Writer;
      } else if (_streamMode != StreamMode.Writer) {
        IACSharpSensor.IACSharpSensor.SensorReached(6571);
        throw new ZlibException("Cannot Write after Reading.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6572);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6573);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6574);
      z.InputBuffer = buffer;
      _z.NextIn = offset;
      _z.AvailableBytesIn = count;
      bool done = false;
      IACSharpSensor.IACSharpSensor.SensorReached(6575);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(6576);
        _z.OutputBuffer = workingBuffer;
        _z.NextOut = 0;
        _z.AvailableBytesOut = _workingBuffer.Length;
        int rc = (_wantCompress) ? _z.Deflate(_flushMode) : _z.Inflate(_flushMode);
        IACSharpSensor.IACSharpSensor.SensorReached(6577);
        if (rc != ZlibConstants.Z_OK && rc != ZlibConstants.Z_STREAM_END) {
          IACSharpSensor.IACSharpSensor.SensorReached(6578);
          throw new ZlibException((_wantCompress ? "de" : "in") + "flating: " + _z.Message);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6579);
        _stream.Write(_workingBuffer, 0, _workingBuffer.Length - _z.AvailableBytesOut);
        done = _z.AvailableBytesIn == 0 && _z.AvailableBytesOut != 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6580);
        if (_flavor == ZlibStreamFlavor.GZIP && !_wantCompress) {
          IACSharpSensor.IACSharpSensor.SensorReached(6581);
          done = (_z.AvailableBytesIn == 8 && _z.AvailableBytesOut != 0);
        }
      } while (!done);
      IACSharpSensor.IACSharpSensor.SensorReached(6582);
    }
    private void finish()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6583);
      if (_z == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6584);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6585);
      if (_streamMode == StreamMode.Writer) {
        IACSharpSensor.IACSharpSensor.SensorReached(6586);
        bool done = false;
        IACSharpSensor.IACSharpSensor.SensorReached(6587);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(6588);
          _z.OutputBuffer = workingBuffer;
          _z.NextOut = 0;
          _z.AvailableBytesOut = _workingBuffer.Length;
          int rc = (_wantCompress) ? _z.Deflate(FlushType.Finish) : _z.Inflate(FlushType.Finish);
          IACSharpSensor.IACSharpSensor.SensorReached(6589);
          if (rc != ZlibConstants.Z_STREAM_END && rc != ZlibConstants.Z_OK) {
            IACSharpSensor.IACSharpSensor.SensorReached(6590);
            string verb = (_wantCompress ? "de" : "in") + "flating";
            IACSharpSensor.IACSharpSensor.SensorReached(6591);
            if (_z.Message == null) {
              IACSharpSensor.IACSharpSensor.SensorReached(6592);
              throw new ZlibException(String.Format("{0}: (rc = {1})", verb, rc));
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(6593);
              throw new ZlibException(verb + ": " + _z.Message);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(6594);
          if (_workingBuffer.Length - _z.AvailableBytesOut > 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6595);
            _stream.Write(_workingBuffer, 0, _workingBuffer.Length - _z.AvailableBytesOut);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(6596);
          done = _z.AvailableBytesIn == 0 && _z.AvailableBytesOut != 0;
          IACSharpSensor.IACSharpSensor.SensorReached(6597);
          if (_flavor == ZlibStreamFlavor.GZIP && !_wantCompress) {
            IACSharpSensor.IACSharpSensor.SensorReached(6598);
            done = (_z.AvailableBytesIn == 8 && _z.AvailableBytesOut != 0);
          }
        } while (!done);
        IACSharpSensor.IACSharpSensor.SensorReached(6599);
        Flush();
        IACSharpSensor.IACSharpSensor.SensorReached(6600);
        if (_flavor == ZlibStreamFlavor.GZIP) {
          IACSharpSensor.IACSharpSensor.SensorReached(6601);
          if (_wantCompress) {
            IACSharpSensor.IACSharpSensor.SensorReached(6602);
            int c1 = crc.Crc32Result;
            _stream.Write(BitConverter.GetBytes(c1), 0, 4);
            int c2 = (Int32)(crc.TotalBytesRead & 0xffffffffu);
            _stream.Write(BitConverter.GetBytes(c2), 0, 4);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(6603);
            throw new ZlibException("Writing with decompression is not supported.");
          }
        }
      } else if (_streamMode == StreamMode.Reader) {
        IACSharpSensor.IACSharpSensor.SensorReached(6604);
        if (_flavor == ZlibStreamFlavor.GZIP) {
          IACSharpSensor.IACSharpSensor.SensorReached(6605);
          if (!_wantCompress) {
            IACSharpSensor.IACSharpSensor.SensorReached(6606);
            if (_z.TotalBytesOut == 0L) {
              IACSharpSensor.IACSharpSensor.SensorReached(6607);
              return;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6608);
            byte[] trailer = new byte[8];
            IACSharpSensor.IACSharpSensor.SensorReached(6609);
            if (_z.AvailableBytesIn < 8) {
              IACSharpSensor.IACSharpSensor.SensorReached(6610);
              Array.Copy(_z.InputBuffer, _z.NextIn, trailer, 0, _z.AvailableBytesIn);
              int bytesNeeded = 8 - _z.AvailableBytesIn;
              int bytesRead = _stream.Read(trailer, _z.AvailableBytesIn, bytesNeeded);
              IACSharpSensor.IACSharpSensor.SensorReached(6611);
              if (bytesNeeded != bytesRead) {
                IACSharpSensor.IACSharpSensor.SensorReached(6612);
                throw new ZlibException(String.Format("Missing or incomplete GZIP trailer. Expected 8 bytes, got {0}.", _z.AvailableBytesIn + bytesRead));
              }
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(6613);
              Array.Copy(_z.InputBuffer, _z.NextIn, trailer, 0, trailer.Length);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6614);
            Int32 crc32_expected = BitConverter.ToInt32(trailer, 0);
            Int32 crc32_actual = crc.Crc32Result;
            Int32 isize_expected = BitConverter.ToInt32(trailer, 4);
            Int32 isize_actual = (Int32)(_z.TotalBytesOut & 0xffffffffu);
            IACSharpSensor.IACSharpSensor.SensorReached(6615);
            if (crc32_actual != crc32_expected) {
              IACSharpSensor.IACSharpSensor.SensorReached(6616);
              throw new ZlibException(String.Format("Bad CRC32 in GZIP trailer. (actual({0:X8})!=expected({1:X8}))", crc32_actual, crc32_expected));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6617);
            if (isize_actual != isize_expected) {
              IACSharpSensor.IACSharpSensor.SensorReached(6618);
              throw new ZlibException(String.Format("Bad size in GZIP trailer. (actual({0})!=expected({1}))", isize_actual, isize_expected));
            }
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(6619);
            throw new ZlibException("Reading with compression is not supported.");
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6620);
    }
    private void end()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6621);
      if (z == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6622);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6623);
      if (_wantCompress) {
        IACSharpSensor.IACSharpSensor.SensorReached(6624);
        _z.EndDeflate();
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(6625);
        _z.EndInflate();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6626);
      _z = null;
      IACSharpSensor.IACSharpSensor.SensorReached(6627);
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6628);
      if (_stream == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6629);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6630);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(6631);
        finish();
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(6632);
        end();
        IACSharpSensor.IACSharpSensor.SensorReached(6633);
        if (!_leaveOpen) {
          IACSharpSensor.IACSharpSensor.SensorReached(6634);
          _stream.Close();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6635);
        _stream = null;
        IACSharpSensor.IACSharpSensor.SensorReached(6636);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6637);
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6638);
      _stream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(6639);
    }
    public override System.Int64 Seek(System.Int64 offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6640);
      throw new NotImplementedException();
    }
    public override void SetLength(System.Int64 value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6641);
      _stream.SetLength(value);
      IACSharpSensor.IACSharpSensor.SensorReached(6642);
    }
    private bool nomoreinput = false;
    private string ReadZeroTerminatedString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6643);
      var list = new System.Collections.Generic.List<byte>();
      bool done = false;
      IACSharpSensor.IACSharpSensor.SensorReached(6644);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(6645);
        int n = _stream.Read(_buf1, 0, 1);
        IACSharpSensor.IACSharpSensor.SensorReached(6646);
        if (n != 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(6647);
          throw new ZlibException("Unexpected EOF reading GZIP header.");
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(6648);
          if (_buf1[0] == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6649);
            done = true;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(6650);
            list.Add(_buf1[0]);
          }
        }
      } while (!done);
      IACSharpSensor.IACSharpSensor.SensorReached(6651);
      byte[] a = list.ToArray();
      System.String RNTRNTRNT_729 = GZipStream.iso8859dash1.GetString(a, 0, a.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(6652);
      return RNTRNTRNT_729;
    }
    private int _ReadAndValidateGzipHeader()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6653);
      int totalBytesRead = 0;
      byte[] header = new byte[10];
      int n = _stream.Read(header, 0, header.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(6654);
      if (n == 0) {
        System.Int32 RNTRNTRNT_730 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6655);
        return RNTRNTRNT_730;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6656);
      if (n != 10) {
        IACSharpSensor.IACSharpSensor.SensorReached(6657);
        throw new ZlibException("Not a valid GZIP stream.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6658);
      if (header[0] != 0x1f || header[1] != 0x8b || header[2] != 8) {
        IACSharpSensor.IACSharpSensor.SensorReached(6659);
        throw new ZlibException("Bad GZIP header.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6660);
      Int32 timet = BitConverter.ToInt32(header, 4);
      _GzipMtime = GZipStream._unixEpoch.AddSeconds(timet);
      totalBytesRead += n;
      IACSharpSensor.IACSharpSensor.SensorReached(6661);
      if ((header[3] & 0x4) == 0x4) {
        IACSharpSensor.IACSharpSensor.SensorReached(6662);
        n = _stream.Read(header, 0, 2);
        totalBytesRead += n;
        Int16 extraLength = (Int16)(header[0] + header[1] * 256);
        byte[] extra = new byte[extraLength];
        n = _stream.Read(extra, 0, extra.Length);
        IACSharpSensor.IACSharpSensor.SensorReached(6663);
        if (n != extraLength) {
          IACSharpSensor.IACSharpSensor.SensorReached(6664);
          throw new ZlibException("Unexpected end-of-file reading GZIP header.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6665);
        totalBytesRead += n;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6666);
      if ((header[3] & 0x8) == 0x8) {
        IACSharpSensor.IACSharpSensor.SensorReached(6667);
        _GzipFileName = ReadZeroTerminatedString();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6668);
      if ((header[3] & 0x10) == 0x10) {
        IACSharpSensor.IACSharpSensor.SensorReached(6669);
        _GzipComment = ReadZeroTerminatedString();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6670);
      if ((header[3] & 0x2) == 0x2) {
        IACSharpSensor.IACSharpSensor.SensorReached(6671);
        Read(_buf1, 0, 1);
      }
      System.Int32 RNTRNTRNT_731 = totalBytesRead;
      IACSharpSensor.IACSharpSensor.SensorReached(6672);
      return RNTRNTRNT_731;
    }
    public override System.Int32 Read(System.Byte[] buffer, System.Int32 offset, System.Int32 count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6673);
      if (_streamMode == StreamMode.Undefined) {
        IACSharpSensor.IACSharpSensor.SensorReached(6674);
        if (!this._stream.CanRead) {
          IACSharpSensor.IACSharpSensor.SensorReached(6675);
          throw new ZlibException("The stream is not readable.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6676);
        _streamMode = StreamMode.Reader;
        z.AvailableBytesIn = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6677);
        if (_flavor == ZlibStreamFlavor.GZIP) {
          IACSharpSensor.IACSharpSensor.SensorReached(6678);
          _gzipHeaderByteCount = _ReadAndValidateGzipHeader();
          IACSharpSensor.IACSharpSensor.SensorReached(6679);
          if (_gzipHeaderByteCount == 0) {
            System.Int32 RNTRNTRNT_732 = 0;
            IACSharpSensor.IACSharpSensor.SensorReached(6680);
            return RNTRNTRNT_732;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6681);
      if (_streamMode != StreamMode.Reader) {
        IACSharpSensor.IACSharpSensor.SensorReached(6682);
        throw new ZlibException("Cannot Read after Writing.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6683);
      if (count == 0) {
        System.Int32 RNTRNTRNT_733 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6684);
        return RNTRNTRNT_733;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6685);
      if (nomoreinput && _wantCompress) {
        System.Int32 RNTRNTRNT_734 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6686);
        return RNTRNTRNT_734;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6687);
      if (buffer == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6688);
        throw new ArgumentNullException("buffer");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6689);
      if (count < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6690);
        throw new ArgumentOutOfRangeException("count");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6691);
      if (offset < buffer.GetLowerBound(0)) {
        IACSharpSensor.IACSharpSensor.SensorReached(6692);
        throw new ArgumentOutOfRangeException("offset");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6693);
      if ((offset + count) > buffer.GetLength(0)) {
        IACSharpSensor.IACSharpSensor.SensorReached(6694);
        throw new ArgumentOutOfRangeException("count");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6695);
      int rc = 0;
      _z.OutputBuffer = buffer;
      _z.NextOut = offset;
      _z.AvailableBytesOut = count;
      _z.InputBuffer = workingBuffer;
      IACSharpSensor.IACSharpSensor.SensorReached(6696);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(6697);
        if ((_z.AvailableBytesIn == 0) && (!nomoreinput)) {
          IACSharpSensor.IACSharpSensor.SensorReached(6698);
          _z.NextIn = 0;
          _z.AvailableBytesIn = _stream.Read(_workingBuffer, 0, _workingBuffer.Length);
          IACSharpSensor.IACSharpSensor.SensorReached(6699);
          if (_z.AvailableBytesIn == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6700);
            nomoreinput = true;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6701);
        rc = (_wantCompress) ? _z.Deflate(_flushMode) : _z.Inflate(_flushMode);
        IACSharpSensor.IACSharpSensor.SensorReached(6702);
        if (nomoreinput && (rc == ZlibConstants.Z_BUF_ERROR)) {
          System.Int32 RNTRNTRNT_735 = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(6703);
          return RNTRNTRNT_735;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6704);
        if (rc != ZlibConstants.Z_OK && rc != ZlibConstants.Z_STREAM_END) {
          IACSharpSensor.IACSharpSensor.SensorReached(6705);
          throw new ZlibException(String.Format("{0}flating:  rc={1}  msg={2}", (_wantCompress ? "de" : "in"), rc, _z.Message));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6706);
        if ((nomoreinput || rc == ZlibConstants.Z_STREAM_END) && (_z.AvailableBytesOut == count)) {
          IACSharpSensor.IACSharpSensor.SensorReached(6707);
          break;
        }
      } while (_z.AvailableBytesOut > 0 && !nomoreinput && rc == ZlibConstants.Z_OK);
      IACSharpSensor.IACSharpSensor.SensorReached(6708);
      if (_z.AvailableBytesOut > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6709);
        if (rc == ZlibConstants.Z_OK && _z.AvailableBytesIn == 0) {
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6710);
        if (nomoreinput) {
          IACSharpSensor.IACSharpSensor.SensorReached(6711);
          if (_wantCompress) {
            IACSharpSensor.IACSharpSensor.SensorReached(6712);
            rc = _z.Deflate(FlushType.Finish);
            IACSharpSensor.IACSharpSensor.SensorReached(6713);
            if (rc != ZlibConstants.Z_OK && rc != ZlibConstants.Z_STREAM_END) {
              IACSharpSensor.IACSharpSensor.SensorReached(6714);
              throw new ZlibException(String.Format("Deflating:  rc={0}  msg={1}", rc, _z.Message));
            }
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6715);
      rc = (count - _z.AvailableBytesOut);
      IACSharpSensor.IACSharpSensor.SensorReached(6716);
      if (crc != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6717);
        crc.SlurpBlock(buffer, offset, rc);
      }
      System.Int32 RNTRNTRNT_736 = rc;
      IACSharpSensor.IACSharpSensor.SensorReached(6718);
      return RNTRNTRNT_736;
    }
    public override System.Boolean CanRead {
      get {
        System.Boolean RNTRNTRNT_737 = this._stream.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(6719);
        return RNTRNTRNT_737;
      }
    }
    public override System.Boolean CanSeek {
      get {
        System.Boolean RNTRNTRNT_738 = this._stream.CanSeek;
        IACSharpSensor.IACSharpSensor.SensorReached(6720);
        return RNTRNTRNT_738;
      }
    }
    public override System.Boolean CanWrite {
      get {
        System.Boolean RNTRNTRNT_739 = this._stream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(6721);
        return RNTRNTRNT_739;
      }
    }
    public override System.Int64 Length {
      get {
        System.Int64 RNTRNTRNT_740 = _stream.Length;
        IACSharpSensor.IACSharpSensor.SensorReached(6722);
        return RNTRNTRNT_740;
      }
    }
    public override long Position {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6723);
        throw new NotImplementedException();
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6724);
        throw new NotImplementedException();
      }
    }
    internal enum StreamMode
    {
      Writer,
      Reader,
      Undefined
    }
    public static void CompressString(String s, Stream compressor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6725);
      byte[] uncompressed = System.Text.Encoding.UTF8.GetBytes(s);
      IACSharpSensor.IACSharpSensor.SensorReached(6726);
      using (compressor) {
        IACSharpSensor.IACSharpSensor.SensorReached(6727);
        compressor.Write(uncompressed, 0, uncompressed.Length);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6728);
    }
    public static void CompressBuffer(byte[] b, Stream compressor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6729);
      using (compressor) {
        IACSharpSensor.IACSharpSensor.SensorReached(6730);
        compressor.Write(b, 0, b.Length);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6731);
    }
    public static String UncompressString(byte[] compressed, Stream decompressor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6732);
      byte[] working = new byte[1024];
      var encoding = System.Text.Encoding.UTF8;
      IACSharpSensor.IACSharpSensor.SensorReached(6733);
      using (var output = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(6734);
        using (decompressor) {
          IACSharpSensor.IACSharpSensor.SensorReached(6735);
          int n;
          IACSharpSensor.IACSharpSensor.SensorReached(6736);
          while ((n = decompressor.Read(working, 0, working.Length)) != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6737);
            output.Write(working, 0, n);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6738);
        output.Seek(0, SeekOrigin.Begin);
        var sr = new StreamReader(output, encoding);
        String RNTRNTRNT_741 = sr.ReadToEnd();
        IACSharpSensor.IACSharpSensor.SensorReached(6739);
        return RNTRNTRNT_741;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed, Stream decompressor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6740);
      byte[] working = new byte[1024];
      IACSharpSensor.IACSharpSensor.SensorReached(6741);
      using (var output = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(6742);
        using (decompressor) {
          IACSharpSensor.IACSharpSensor.SensorReached(6743);
          int n;
          IACSharpSensor.IACSharpSensor.SensorReached(6744);
          while ((n = decompressor.Read(working, 0, working.Length)) != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6745);
            output.Write(working, 0, n);
          }
        }
        System.Byte[] RNTRNTRNT_742 = output.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(6746);
        return RNTRNTRNT_742;
      }
    }
  }
}
