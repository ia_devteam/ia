using System;
using System.IO;
namespace Ionic.Zlib
{
  public class ZlibStream : System.IO.Stream
  {
    internal ZlibBaseStream _baseStream;
    bool _disposed;
    public ZlibStream(System.IO.Stream stream, CompressionMode mode) : this(stream, mode, CompressionLevel.Default, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6830);
    }
    public ZlibStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level) : this(stream, mode, level, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6831);
    }
    public ZlibStream(System.IO.Stream stream, CompressionMode mode, bool leaveOpen) : this(stream, mode, CompressionLevel.Default, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6832);
    }
    public ZlibStream(System.IO.Stream stream, CompressionMode mode, CompressionLevel level, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6833);
      _baseStream = new ZlibBaseStream(stream, mode, level, ZlibStreamFlavor.ZLIB, leaveOpen);
      IACSharpSensor.IACSharpSensor.SensorReached(6834);
    }
    public virtual FlushType FlushMode {
      get {
        FlushType RNTRNTRNT_764 = (this._baseStream._flushMode);
        IACSharpSensor.IACSharpSensor.SensorReached(6835);
        return RNTRNTRNT_764;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6836);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(6837);
          throw new ObjectDisposedException("ZlibStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6838);
        this._baseStream._flushMode = value;
        IACSharpSensor.IACSharpSensor.SensorReached(6839);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_765 = this._baseStream._bufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(6840);
        return RNTRNTRNT_765;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6841);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(6842);
          throw new ObjectDisposedException("ZlibStream");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6843);
        if (this._baseStream._workingBuffer != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(6844);
          throw new ZlibException("The working buffer is already set.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6845);
        if (value < ZlibConstants.WorkingBufferSizeMin) {
          IACSharpSensor.IACSharpSensor.SensorReached(6846);
          throw new ZlibException(String.Format("Don't be silly. {0} bytes?? Use a bigger buffer, at least {1}.", value, ZlibConstants.WorkingBufferSizeMin));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6847);
        this._baseStream._bufferSize = value;
        IACSharpSensor.IACSharpSensor.SensorReached(6848);
      }
    }
    public virtual long TotalIn {
      get {
        System.Int64 RNTRNTRNT_766 = this._baseStream._z.TotalBytesIn;
        IACSharpSensor.IACSharpSensor.SensorReached(6849);
        return RNTRNTRNT_766;
      }
    }
    public virtual long TotalOut {
      get {
        System.Int64 RNTRNTRNT_767 = this._baseStream._z.TotalBytesOut;
        IACSharpSensor.IACSharpSensor.SensorReached(6850);
        return RNTRNTRNT_767;
      }
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6851);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(6852);
        if (!_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(6853);
          if (disposing && (this._baseStream != null)) {
            IACSharpSensor.IACSharpSensor.SensorReached(6854);
            this._baseStream.Close();
          }
          IACSharpSensor.IACSharpSensor.SensorReached(6855);
          _disposed = true;
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(6856);
        base.Dispose(disposing);
        IACSharpSensor.IACSharpSensor.SensorReached(6857);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6858);
    }
    public override bool CanRead {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6859);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(6860);
          throw new ObjectDisposedException("ZlibStream");
        }
        System.Boolean RNTRNTRNT_768 = _baseStream._stream.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(6861);
        return RNTRNTRNT_768;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_769 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(6862);
        return RNTRNTRNT_769;
      }
    }
    public override bool CanWrite {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6863);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(6864);
          throw new ObjectDisposedException("ZlibStream");
        }
        System.Boolean RNTRNTRNT_770 = _baseStream._stream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(6865);
        return RNTRNTRNT_770;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6866);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(6867);
        throw new ObjectDisposedException("ZlibStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6868);
      _baseStream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(6869);
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6870);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6871);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Writer) {
          System.Int64 RNTRNTRNT_771 = this._baseStream._z.TotalBytesOut;
          IACSharpSensor.IACSharpSensor.SensorReached(6872);
          return RNTRNTRNT_771;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6873);
        if (this._baseStream._streamMode == Ionic.Zlib.ZlibBaseStream.StreamMode.Reader) {
          System.Int64 RNTRNTRNT_772 = this._baseStream._z.TotalBytesIn;
          IACSharpSensor.IACSharpSensor.SensorReached(6874);
          return RNTRNTRNT_772;
        }
        System.Int64 RNTRNTRNT_773 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(6875);
        return RNTRNTRNT_773;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6876);
        throw new NotSupportedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6877);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(6878);
        throw new ObjectDisposedException("ZlibStream");
      }
      System.Int32 RNTRNTRNT_774 = _baseStream.Read(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(6879);
      return RNTRNTRNT_774;
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6880);
      throw new NotSupportedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6881);
      throw new NotSupportedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6882);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(6883);
        throw new ObjectDisposedException("ZlibStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6884);
      _baseStream.Write(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(6885);
    }
    public static byte[] CompressString(String s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6886);
      using (var ms = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(6887);
        Stream compressor = new ZlibStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressString(s, compressor);
        System.Byte[] RNTRNTRNT_775 = ms.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(6888);
        return RNTRNTRNT_775;
      }
    }
    public static byte[] CompressBuffer(byte[] b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6889);
      using (var ms = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(6890);
        Stream compressor = new ZlibStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression);
        ZlibBaseStream.CompressBuffer(b, compressor);
        System.Byte[] RNTRNTRNT_776 = ms.ToArray();
        IACSharpSensor.IACSharpSensor.SensorReached(6891);
        return RNTRNTRNT_776;
      }
    }
    public static String UncompressString(byte[] compressed)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6892);
      using (var input = new MemoryStream(compressed)) {
        IACSharpSensor.IACSharpSensor.SensorReached(6893);
        Stream decompressor = new ZlibStream(input, CompressionMode.Decompress);
        String RNTRNTRNT_777 = ZlibBaseStream.UncompressString(compressed, decompressor);
        IACSharpSensor.IACSharpSensor.SensorReached(6894);
        return RNTRNTRNT_777;
      }
    }
    public static byte[] UncompressBuffer(byte[] compressed)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6895);
      using (var input = new MemoryStream(compressed)) {
        IACSharpSensor.IACSharpSensor.SensorReached(6896);
        Stream decompressor = new ZlibStream(input, CompressionMode.Decompress);
        System.Byte[] RNTRNTRNT_778 = ZlibBaseStream.UncompressBuffer(compressed, decompressor);
        IACSharpSensor.IACSharpSensor.SensorReached(6897);
        return RNTRNTRNT_778;
      }
    }
  }
}
