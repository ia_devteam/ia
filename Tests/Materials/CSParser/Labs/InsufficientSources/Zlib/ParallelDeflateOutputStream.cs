using System;
using System.Collections.Generic;
using System.Threading;
using Ionic.Zlib;
using System.IO;
namespace Ionic.Zlib
{
  internal class WorkItem
  {
    public byte[] buffer;
    public byte[] compressed;
    public int crc;
    public int index;
    public int ordinal;
    public int inputBytesAvailable;
    public int compressedBytesAvailable;
    public ZlibCodec compressor;
    public WorkItem(int size, Ionic.Zlib.CompressionLevel compressLevel, CompressionStrategy strategy, int ix)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6295);
      this.buffer = new byte[size];
      int n = size + ((size / 32768) + 1) * 5 * 2;
      this.compressed = new byte[n];
      this.compressor = new ZlibCodec();
      this.compressor.InitializeDeflate(compressLevel, false);
      this.compressor.OutputBuffer = this.compressed;
      this.compressor.InputBuffer = this.buffer;
      this.index = ix;
      IACSharpSensor.IACSharpSensor.SensorReached(6296);
    }
  }
  public class ParallelDeflateOutputStream : System.IO.Stream
  {
    private static readonly int IO_BUFFER_SIZE_DEFAULT = 64 * 1024;
    private static readonly int BufferPairsPerCore = 4;
    private System.Collections.Generic.List<WorkItem> _pool;
    private bool _leaveOpen;
    private bool emitting;
    private System.IO.Stream _outStream;
    private int _maxBufferPairs;
    private int _bufferSize = IO_BUFFER_SIZE_DEFAULT;
    private AutoResetEvent _newlyCompressedBlob;
    private object _outputLock = new object();
    private bool _isClosed;
    private bool _firstWriteDone;
    private int _currentlyFilling;
    private int _lastFilled;
    private int _lastWritten;
    private int _latestCompressed;
    private int _Crc32;
    private Ionic.Crc.CRC32 _runningCrc;
    private object _latestLock = new object();
    private System.Collections.Generic.Queue<int> _toWrite;
    private System.Collections.Generic.Queue<int> _toFill;
    private Int64 _totalBytesProcessed;
    private Ionic.Zlib.CompressionLevel _compressLevel;
    private volatile Exception _pendingException;
    private bool _handlingException;
    private object _eLock = new Object();
    private TraceBits _DesiredTrace = TraceBits.Session | TraceBits.Compress | TraceBits.WriteTake | TraceBits.WriteEnter | TraceBits.EmitEnter | TraceBits.EmitDone | TraceBits.EmitLock | TraceBits.EmitSkip | TraceBits.EmitBegin;
    public ParallelDeflateOutputStream(System.IO.Stream stream) : this(stream, CompressionLevel.Default, CompressionStrategy.Default, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6297);
    }
    public ParallelDeflateOutputStream(System.IO.Stream stream, CompressionLevel level) : this(stream, level, CompressionStrategy.Default, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6298);
    }
    public ParallelDeflateOutputStream(System.IO.Stream stream, bool leaveOpen) : this(stream, CompressionLevel.Default, CompressionStrategy.Default, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6299);
    }
    public ParallelDeflateOutputStream(System.IO.Stream stream, CompressionLevel level, bool leaveOpen) : this(stream, CompressionLevel.Default, CompressionStrategy.Default, leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6300);
    }
    public ParallelDeflateOutputStream(System.IO.Stream stream, CompressionLevel level, CompressionStrategy strategy, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6301);
      TraceOutput(TraceBits.Lifecycle | TraceBits.Session, "-------------------------------------------------------");
      TraceOutput(TraceBits.Lifecycle | TraceBits.Session, "Create {0:X8}", this.GetHashCode());
      _outStream = stream;
      _compressLevel = level;
      Strategy = strategy;
      _leaveOpen = leaveOpen;
      this.MaxBufferPairs = 16;
      IACSharpSensor.IACSharpSensor.SensorReached(6302);
    }
    public CompressionStrategy Strategy { get; private set; }
    public int MaxBufferPairs {
      get {
        System.Int32 RNTRNTRNT_705 = _maxBufferPairs;
        IACSharpSensor.IACSharpSensor.SensorReached(6303);
        return RNTRNTRNT_705;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6304);
        if (value < 4) {
          IACSharpSensor.IACSharpSensor.SensorReached(6305);
          throw new ArgumentException("MaxBufferPairs", "Value must be 4 or greater.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6306);
        _maxBufferPairs = value;
        IACSharpSensor.IACSharpSensor.SensorReached(6307);
      }
    }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_706 = _bufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(6308);
        return RNTRNTRNT_706;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6309);
        if (value < 1024) {
          IACSharpSensor.IACSharpSensor.SensorReached(6310);
          throw new ArgumentOutOfRangeException("BufferSize", "BufferSize must be greater than 1024 bytes");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6311);
        _bufferSize = value;
        IACSharpSensor.IACSharpSensor.SensorReached(6312);
      }
    }
    public int Crc32 {
      get {
        System.Int32 RNTRNTRNT_707 = _Crc32;
        IACSharpSensor.IACSharpSensor.SensorReached(6313);
        return RNTRNTRNT_707;
      }
    }
    public Int64 BytesProcessed {
      get {
        Int64 RNTRNTRNT_708 = _totalBytesProcessed;
        IACSharpSensor.IACSharpSensor.SensorReached(6314);
        return RNTRNTRNT_708;
      }
    }
    private void _InitializePoolOfWorkItems()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6315);
      _toWrite = new Queue<int>();
      _toFill = new Queue<int>();
      _pool = new System.Collections.Generic.List<WorkItem>();
      int nTasks = BufferPairsPerCore * Environment.ProcessorCount;
      nTasks = Math.Min(nTasks, _maxBufferPairs);
      IACSharpSensor.IACSharpSensor.SensorReached(6316);
      for (int i = 0; i < nTasks; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(6317);
        _pool.Add(new WorkItem(_bufferSize, _compressLevel, Strategy, i));
        _toFill.Enqueue(i);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6318);
      _newlyCompressedBlob = new AutoResetEvent(false);
      _runningCrc = new Ionic.Crc.CRC32();
      _currentlyFilling = -1;
      _lastFilled = -1;
      _lastWritten = -1;
      _latestCompressed = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(6319);
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6320);
      bool mustWait = false;
      IACSharpSensor.IACSharpSensor.SensorReached(6321);
      if (_isClosed) {
        IACSharpSensor.IACSharpSensor.SensorReached(6322);
        throw new InvalidOperationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6323);
      if (_pendingException != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6324);
        _handlingException = true;
        var pe = _pendingException;
        _pendingException = null;
        IACSharpSensor.IACSharpSensor.SensorReached(6325);
        throw pe;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6326);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6327);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6328);
      if (!_firstWriteDone) {
        IACSharpSensor.IACSharpSensor.SensorReached(6329);
        _InitializePoolOfWorkItems();
        _firstWriteDone = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6330);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(6331);
        EmitPendingBuffers(false, mustWait);
        mustWait = false;
        int ix = -1;
        IACSharpSensor.IACSharpSensor.SensorReached(6332);
        if (_currentlyFilling >= 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(6333);
          ix = _currentlyFilling;
          TraceOutput(TraceBits.WriteTake, "Write    notake   wi({0}) lf({1})", ix, _lastFilled);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(6334);
          TraceOutput(TraceBits.WriteTake, "Write    take?");
          IACSharpSensor.IACSharpSensor.SensorReached(6335);
          if (_toFill.Count == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(6336);
            mustWait = true;
            IACSharpSensor.IACSharpSensor.SensorReached(6337);
            continue;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(6338);
          ix = _toFill.Dequeue();
          TraceOutput(TraceBits.WriteTake, "Write    take     wi({0}) lf({1})", ix, _lastFilled);
          ++_lastFilled;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6339);
        WorkItem workitem = _pool[ix];
        int limit = ((workitem.buffer.Length - workitem.inputBytesAvailable) > count) ? count : (workitem.buffer.Length - workitem.inputBytesAvailable);
        workitem.ordinal = _lastFilled;
        TraceOutput(TraceBits.Write, "Write    lock     wi({0}) ord({1}) iba({2})", workitem.index, workitem.ordinal, workitem.inputBytesAvailable);
        Buffer.BlockCopy(buffer, offset, workitem.buffer, workitem.inputBytesAvailable, limit);
        count -= limit;
        offset += limit;
        workitem.inputBytesAvailable += limit;
        IACSharpSensor.IACSharpSensor.SensorReached(6340);
        if (workitem.inputBytesAvailable == workitem.buffer.Length) {
          IACSharpSensor.IACSharpSensor.SensorReached(6341);
          TraceOutput(TraceBits.Write, "Write    QUWI     wi({0}) ord({1}) iba({2}) nf({3})", workitem.index, workitem.ordinal, workitem.inputBytesAvailable);
          IACSharpSensor.IACSharpSensor.SensorReached(6342);
          if (!ThreadPool.QueueUserWorkItem(_DeflateOne, workitem)) {
            IACSharpSensor.IACSharpSensor.SensorReached(6343);
            throw new Exception("Cannot enqueue workitem");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(6344);
          _currentlyFilling = -1;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(6345);
          _currentlyFilling = ix;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6346);
        if (count > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(6347);
          TraceOutput(TraceBits.WriteEnter, "Write    more");
        }
      } while (count > 0);
      IACSharpSensor.IACSharpSensor.SensorReached(6348);
      TraceOutput(TraceBits.WriteEnter, "Write    exit");
      IACSharpSensor.IACSharpSensor.SensorReached(6349);
      return;
    }
    private void _FlushFinish()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6350);
      byte[] buffer = new byte[128];
      var compressor = new ZlibCodec();
      int rc = compressor.InitializeDeflate(_compressLevel, false);
      compressor.InputBuffer = null;
      compressor.NextIn = 0;
      compressor.AvailableBytesIn = 0;
      compressor.OutputBuffer = buffer;
      compressor.NextOut = 0;
      compressor.AvailableBytesOut = buffer.Length;
      rc = compressor.Deflate(FlushType.Finish);
      IACSharpSensor.IACSharpSensor.SensorReached(6351);
      if (rc != ZlibConstants.Z_STREAM_END && rc != ZlibConstants.Z_OK) {
        IACSharpSensor.IACSharpSensor.SensorReached(6352);
        throw new Exception("deflating: " + compressor.Message);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6353);
      if (buffer.Length - compressor.AvailableBytesOut > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6354);
        TraceOutput(TraceBits.EmitBegin, "Emit     begin    flush bytes({0})", buffer.Length - compressor.AvailableBytesOut);
        _outStream.Write(buffer, 0, buffer.Length - compressor.AvailableBytesOut);
        TraceOutput(TraceBits.EmitDone, "Emit     done     flush");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6355);
      compressor.EndDeflate();
      _Crc32 = _runningCrc.Crc32Result;
      IACSharpSensor.IACSharpSensor.SensorReached(6356);
    }
    private void _Flush(bool lastInput)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6357);
      if (_isClosed) {
        IACSharpSensor.IACSharpSensor.SensorReached(6358);
        throw new InvalidOperationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6359);
      if (emitting) {
        IACSharpSensor.IACSharpSensor.SensorReached(6360);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6361);
      if (_currentlyFilling >= 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(6362);
        WorkItem workitem = _pool[_currentlyFilling];
        _DeflateOne(workitem);
        _currentlyFilling = -1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6363);
      if (lastInput) {
        IACSharpSensor.IACSharpSensor.SensorReached(6364);
        EmitPendingBuffers(true, false);
        _FlushFinish();
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(6365);
        EmitPendingBuffers(false, false);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6366);
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6367);
      if (_pendingException != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6368);
        _handlingException = true;
        var pe = _pendingException;
        _pendingException = null;
        IACSharpSensor.IACSharpSensor.SensorReached(6369);
        throw pe;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6370);
      if (_handlingException) {
        IACSharpSensor.IACSharpSensor.SensorReached(6371);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6372);
      _Flush(false);
      IACSharpSensor.IACSharpSensor.SensorReached(6373);
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6374);
      TraceOutput(TraceBits.Session, "Close {0:X8}", this.GetHashCode());
      IACSharpSensor.IACSharpSensor.SensorReached(6375);
      if (_pendingException != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(6376);
        _handlingException = true;
        var pe = _pendingException;
        _pendingException = null;
        IACSharpSensor.IACSharpSensor.SensorReached(6377);
        throw pe;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6378);
      if (_handlingException) {
        IACSharpSensor.IACSharpSensor.SensorReached(6379);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6380);
      if (_isClosed) {
        IACSharpSensor.IACSharpSensor.SensorReached(6381);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6382);
      _Flush(true);
      IACSharpSensor.IACSharpSensor.SensorReached(6383);
      if (!_leaveOpen) {
        IACSharpSensor.IACSharpSensor.SensorReached(6384);
        _outStream.Close();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6385);
      _isClosed = true;
      IACSharpSensor.IACSharpSensor.SensorReached(6386);
    }
    public new void Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6387);
      TraceOutput(TraceBits.Lifecycle, "Dispose  {0:X8}", this.GetHashCode());
      Close();
      _pool = null;
      Dispose(true);
      IACSharpSensor.IACSharpSensor.SensorReached(6388);
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6389);
      base.Dispose(disposing);
      IACSharpSensor.IACSharpSensor.SensorReached(6390);
    }
    public void Reset(Stream stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6391);
      TraceOutput(TraceBits.Session, "-------------------------------------------------------");
      TraceOutput(TraceBits.Session, "Reset {0:X8} firstDone({1})", this.GetHashCode(), _firstWriteDone);
      IACSharpSensor.IACSharpSensor.SensorReached(6392);
      if (!_firstWriteDone) {
        IACSharpSensor.IACSharpSensor.SensorReached(6393);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6394);
      _toWrite.Clear();
      _toFill.Clear();
      IACSharpSensor.IACSharpSensor.SensorReached(6395);
      foreach (var workitem in _pool) {
        IACSharpSensor.IACSharpSensor.SensorReached(6396);
        _toFill.Enqueue(workitem.index);
        workitem.ordinal = -1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6397);
      _firstWriteDone = false;
      _totalBytesProcessed = 0L;
      _runningCrc = new Ionic.Crc.CRC32();
      _isClosed = false;
      _currentlyFilling = -1;
      _lastFilled = -1;
      _lastWritten = -1;
      _latestCompressed = -1;
      _outStream = stream;
      IACSharpSensor.IACSharpSensor.SensorReached(6398);
    }
    private void EmitPendingBuffers(bool doAll, bool mustWait)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6399);
      if (emitting) {
        IACSharpSensor.IACSharpSensor.SensorReached(6400);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6401);
      emitting = true;
      IACSharpSensor.IACSharpSensor.SensorReached(6402);
      if (doAll || mustWait) {
        IACSharpSensor.IACSharpSensor.SensorReached(6403);
        _newlyCompressedBlob.WaitOne();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6404);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(6405);
        int firstSkip = -1;
        int millisecondsToWait = doAll ? 200 : (mustWait ? -1 : 0);
        int nextToWrite = -1;
        IACSharpSensor.IACSharpSensor.SensorReached(6406);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(6407);
          if (Monitor.TryEnter(_toWrite, millisecondsToWait)) {
            IACSharpSensor.IACSharpSensor.SensorReached(6408);
            nextToWrite = -1;
            IACSharpSensor.IACSharpSensor.SensorReached(6409);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(6410);
              if (_toWrite.Count > 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(6411);
                nextToWrite = _toWrite.Dequeue();
              }
            } finally {
              IACSharpSensor.IACSharpSensor.SensorReached(6412);
              Monitor.Exit(_toWrite);
              IACSharpSensor.IACSharpSensor.SensorReached(6413);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(6414);
            if (nextToWrite >= 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(6415);
              WorkItem workitem = _pool[nextToWrite];
              IACSharpSensor.IACSharpSensor.SensorReached(6416);
              if (workitem.ordinal != _lastWritten + 1) {
                IACSharpSensor.IACSharpSensor.SensorReached(6417);
                TraceOutput(TraceBits.EmitSkip, "Emit     skip     wi({0}) ord({1}) lw({2}) fs({3})", workitem.index, workitem.ordinal, _lastWritten, firstSkip);
                lock (_toWrite) {
                  IACSharpSensor.IACSharpSensor.SensorReached(6418);
                  _toWrite.Enqueue(nextToWrite);
                }
                IACSharpSensor.IACSharpSensor.SensorReached(6419);
                if (firstSkip == nextToWrite) {
                  IACSharpSensor.IACSharpSensor.SensorReached(6420);
                  _newlyCompressedBlob.WaitOne();
                  firstSkip = -1;
                } else if (firstSkip == -1) {
                  IACSharpSensor.IACSharpSensor.SensorReached(6421);
                  firstSkip = nextToWrite;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(6422);
                continue;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(6423);
              firstSkip = -1;
              TraceOutput(TraceBits.EmitBegin, "Emit     begin    wi({0}) ord({1})              cba({2})", workitem.index, workitem.ordinal, workitem.compressedBytesAvailable);
              _outStream.Write(workitem.compressed, 0, workitem.compressedBytesAvailable);
              _runningCrc.Combine(workitem.crc, workitem.inputBytesAvailable);
              _totalBytesProcessed += workitem.inputBytesAvailable;
              workitem.inputBytesAvailable = 0;
              TraceOutput(TraceBits.EmitDone, "Emit     done     wi({0}) ord({1})              cba({2}) mtw({3})", workitem.index, workitem.ordinal, workitem.compressedBytesAvailable, millisecondsToWait);
              _lastWritten = workitem.ordinal;
              _toFill.Enqueue(workitem.index);
              IACSharpSensor.IACSharpSensor.SensorReached(6424);
              if (millisecondsToWait == -1) {
                IACSharpSensor.IACSharpSensor.SensorReached(6425);
                millisecondsToWait = 0;
              }
            }
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(6426);
            nextToWrite = -1;
          }
        } while (nextToWrite >= 0);
      } while (doAll && (_lastWritten != _latestCompressed));
      IACSharpSensor.IACSharpSensor.SensorReached(6427);
      emitting = false;
      IACSharpSensor.IACSharpSensor.SensorReached(6428);
    }
    private void _DeflateOne(Object wi)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6429);
      WorkItem workitem = (WorkItem)wi;
      IACSharpSensor.IACSharpSensor.SensorReached(6430);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(6431);
        int myItem = workitem.index;
        Ionic.Crc.CRC32 crc = new Ionic.Crc.CRC32();
        crc.SlurpBlock(workitem.buffer, 0, workitem.inputBytesAvailable);
        DeflateOneSegment(workitem);
        workitem.crc = crc.Crc32Result;
        TraceOutput(TraceBits.Compress, "Compress          wi({0}) ord({1}) len({2})", workitem.index, workitem.ordinal, workitem.compressedBytesAvailable);
        lock (_latestLock) {
          IACSharpSensor.IACSharpSensor.SensorReached(6432);
          if (workitem.ordinal > _latestCompressed) {
            IACSharpSensor.IACSharpSensor.SensorReached(6433);
            _latestCompressed = workitem.ordinal;
          }
        }
        lock (_toWrite) {
          IACSharpSensor.IACSharpSensor.SensorReached(6434);
          _toWrite.Enqueue(workitem.index);
        }
        _newlyCompressedBlob.Set();
      } catch (System.Exception exc1) {
        lock (_eLock) {
          IACSharpSensor.IACSharpSensor.SensorReached(6435);
          if (_pendingException != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(6436);
            _pendingException = exc1;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(6437);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6438);
    }
    private bool DeflateOneSegment(WorkItem workitem)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6439);
      ZlibCodec compressor = workitem.compressor;
      int rc = 0;
      compressor.ResetDeflate();
      compressor.NextIn = 0;
      compressor.AvailableBytesIn = workitem.inputBytesAvailable;
      compressor.NextOut = 0;
      compressor.AvailableBytesOut = workitem.compressed.Length;
      IACSharpSensor.IACSharpSensor.SensorReached(6440);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(6441);
        compressor.Deflate(FlushType.None);
      } while (compressor.AvailableBytesIn > 0 || compressor.AvailableBytesOut == 0);
      IACSharpSensor.IACSharpSensor.SensorReached(6442);
      rc = compressor.Deflate(FlushType.Sync);
      workitem.compressedBytesAvailable = (int)compressor.TotalBytesOut;
      System.Boolean RNTRNTRNT_709 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(6443);
      return RNTRNTRNT_709;
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceOutput(TraceBits bits, string format, params object[] varParams)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6444);
      if ((bits & _DesiredTrace) != 0) {
        lock (_outputLock) {
          IACSharpSensor.IACSharpSensor.SensorReached(6445);
          int tid = Thread.CurrentThread.GetHashCode();
          Console.ForegroundColor = (ConsoleColor)(tid % 8 + 8);
          Console.Write("{0:000} PDOS ", tid);
          Console.WriteLine(format, varParams);
          Console.ResetColor();
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(6446);
    }
    [Flags()]
    enum TraceBits : uint
    {
      None = 0,
      NotUsed1 = 1,
      EmitLock = 2,
      EmitEnter = 4,
      EmitBegin = 8,
      EmitDone = 16,
      EmitSkip = 32,
      EmitAll = 58,
      Flush = 64,
      Lifecycle = 128,
      Session = 256,
      Synch = 512,
      Instance = 1024,
      Compress = 2048,
      Write = 4096,
      WriteEnter = 8192,
      WriteTake = 16384,
      All = 0xffffffffu
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_710 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(6447);
        return RNTRNTRNT_710;
      }
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_711 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(6448);
        return RNTRNTRNT_711;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_712 = _outStream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(6449);
        return RNTRNTRNT_712;
      }
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(6450);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_713 = _outStream.Position;
        IACSharpSensor.IACSharpSensor.SensorReached(6451);
        return RNTRNTRNT_713;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(6452);
        throw new NotSupportedException();
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6453);
      throw new NotSupportedException();
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6454);
      throw new NotSupportedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6455);
      throw new NotSupportedException();
    }
  }
}
