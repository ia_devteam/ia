using System;
namespace Ionic.Zlib
{
  internal enum BlockState
  {
    NeedMore = 0,
    BlockDone,
    FinishStarted,
    FinishDone
  }
  internal enum DeflateFlavor
  {
    Store,
    Fast,
    Slow
  }
  internal sealed class DeflateManager
  {
    private static readonly int MEM_LEVEL_MAX = 9;
    private static readonly int MEM_LEVEL_DEFAULT = 8;
    internal delegate BlockState CompressFunc(FlushType flush);
    internal class Config
    {
      internal int GoodLength;
      internal int MaxLazy;
      internal int NiceLength;
      internal int MaxChainLength;
      internal DeflateFlavor Flavor;
      private Config(int goodLength, int maxLazy, int niceLength, int maxChainLength, DeflateFlavor flavor)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(5135);
        this.GoodLength = goodLength;
        this.MaxLazy = maxLazy;
        this.NiceLength = niceLength;
        this.MaxChainLength = maxChainLength;
        this.Flavor = flavor;
        IACSharpSensor.IACSharpSensor.SensorReached(5136);
      }
      public static Config Lookup(CompressionLevel level)
      {
        Config RNTRNTRNT_555 = Table[(int)level];
        IACSharpSensor.IACSharpSensor.SensorReached(5137);
        return RNTRNTRNT_555;
      }
      static Config()
      {
        IACSharpSensor.IACSharpSensor.SensorReached(5138);
        Table = new Config[] {
          new Config(0, 0, 0, 0, DeflateFlavor.Store),
          new Config(4, 4, 8, 4, DeflateFlavor.Fast),
          new Config(4, 5, 16, 8, DeflateFlavor.Fast),
          new Config(4, 6, 32, 32, DeflateFlavor.Fast),
          new Config(4, 4, 16, 16, DeflateFlavor.Slow),
          new Config(8, 16, 32, 32, DeflateFlavor.Slow),
          new Config(8, 16, 128, 128, DeflateFlavor.Slow),
          new Config(8, 32, 128, 256, DeflateFlavor.Slow),
          new Config(32, 128, 258, 1024, DeflateFlavor.Slow),
          new Config(32, 258, 258, 4096, DeflateFlavor.Slow)
        };
        IACSharpSensor.IACSharpSensor.SensorReached(5139);
      }
      private static readonly Config[] Table;
    }
    private CompressFunc DeflateFunction;
    private static readonly System.String[] _ErrorMessage = new System.String[] {
      "need dictionary",
      "stream end",
      "",
      "file error",
      "stream error",
      "data error",
      "insufficient memory",
      "buffer error",
      "incompatible version",
      ""
    };
    private static readonly int PRESET_DICT = 0x20;
    private static readonly int INIT_STATE = 42;
    private static readonly int BUSY_STATE = 113;
    private static readonly int FINISH_STATE = 666;
    private static readonly int Z_DEFLATED = 8;
    private static readonly int STORED_BLOCK = 0;
    private static readonly int STATIC_TREES = 1;
    private static readonly int DYN_TREES = 2;
    private static readonly int Z_BINARY = 0;
    private static readonly int Z_ASCII = 1;
    private static readonly int Z_UNKNOWN = 2;
    private static readonly int Buf_size = 8 * 2;
    private static readonly int MIN_MATCH = 3;
    private static readonly int MAX_MATCH = 258;
    private static readonly int MIN_LOOKAHEAD = (MAX_MATCH + MIN_MATCH + 1);
    private static readonly int HEAP_SIZE = (2 * InternalConstants.L_CODES + 1);
    private static readonly int END_BLOCK = 256;
    internal ZlibCodec _codec;
    internal int status;
    internal byte[] pending;
    internal int nextPending;
    internal int pendingCount;
    internal sbyte data_type;
    internal int last_flush;
    internal int w_size;
    internal int w_bits;
    internal int w_mask;
    internal byte[] window;
    internal int window_size;
    internal short[] prev;
    internal short[] head;
    internal int ins_h;
    internal int hash_size;
    internal int hash_bits;
    internal int hash_mask;
    internal int hash_shift;
    internal int block_start;
    Config config;
    internal int match_length;
    internal int prev_match;
    internal int match_available;
    internal int strstart;
    internal int match_start;
    internal int lookahead;
    internal int prev_length;
    internal CompressionLevel compressionLevel;
    internal CompressionStrategy compressionStrategy;
    internal short[] dyn_ltree;
    internal short[] dyn_dtree;
    internal short[] bl_tree;
    internal Tree treeLiterals = new Tree();
    internal Tree treeDistances = new Tree();
    internal Tree treeBitLengths = new Tree();
    internal short[] bl_count = new short[InternalConstants.MAX_BITS + 1];
    internal int[] heap = new int[2 * InternalConstants.L_CODES + 1];
    internal int heap_len;
    internal int heap_max;
    internal sbyte[] depth = new sbyte[2 * InternalConstants.L_CODES + 1];
    internal int _lengthOffset;
    internal int lit_bufsize;
    internal int last_lit;
    internal int _distanceOffset;
    internal int opt_len;
    internal int static_len;
    internal int matches;
    internal int last_eob_len;
    internal short bi_buf;
    internal int bi_valid;
    internal DeflateManager()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5140);
      dyn_ltree = new short[HEAP_SIZE * 2];
      dyn_dtree = new short[(2 * InternalConstants.D_CODES + 1) * 2];
      bl_tree = new short[(2 * InternalConstants.BL_CODES + 1) * 2];
      IACSharpSensor.IACSharpSensor.SensorReached(5141);
    }
    private void _InitializeLazyMatch()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5142);
      window_size = 2 * w_size;
      Array.Clear(head, 0, hash_size);
      config = Config.Lookup(compressionLevel);
      SetDeflater();
      strstart = 0;
      block_start = 0;
      lookahead = 0;
      match_length = prev_length = MIN_MATCH - 1;
      match_available = 0;
      ins_h = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5143);
    }
    private void _InitializeTreeData()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5144);
      treeLiterals.dyn_tree = dyn_ltree;
      treeLiterals.staticTree = StaticTree.Literals;
      treeDistances.dyn_tree = dyn_dtree;
      treeDistances.staticTree = StaticTree.Distances;
      treeBitLengths.dyn_tree = bl_tree;
      treeBitLengths.staticTree = StaticTree.BitLengths;
      bi_buf = 0;
      bi_valid = 0;
      last_eob_len = 8;
      _InitializeBlocks();
      IACSharpSensor.IACSharpSensor.SensorReached(5145);
    }
    internal void _InitializeBlocks()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5146);
      for (int i = 0; i < InternalConstants.L_CODES; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5147);
        dyn_ltree[i * 2] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5148);
      for (int i = 0; i < InternalConstants.D_CODES; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5149);
        dyn_dtree[i * 2] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5150);
      for (int i = 0; i < InternalConstants.BL_CODES; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5151);
        bl_tree[i * 2] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5152);
      dyn_ltree[END_BLOCK * 2] = 1;
      opt_len = static_len = 0;
      last_lit = matches = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5153);
    }
    internal void pqdownheap(short[] tree, int k)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5154);
      int v = heap[k];
      int j = k << 1;
      IACSharpSensor.IACSharpSensor.SensorReached(5155);
      while (j <= heap_len) {
        IACSharpSensor.IACSharpSensor.SensorReached(5156);
        if (j < heap_len && _IsSmaller(tree, heap[j + 1], heap[j], depth)) {
          IACSharpSensor.IACSharpSensor.SensorReached(5157);
          j++;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5158);
        if (_IsSmaller(tree, v, heap[j], depth)) {
          IACSharpSensor.IACSharpSensor.SensorReached(5159);
          break;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5160);
        heap[k] = heap[j];
        k = j;
        j <<= 1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5161);
      heap[k] = v;
      IACSharpSensor.IACSharpSensor.SensorReached(5162);
    }
    static internal bool _IsSmaller(short[] tree, int n, int m, sbyte[] depth)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5163);
      short tn2 = tree[n * 2];
      short tm2 = tree[m * 2];
      System.Boolean RNTRNTRNT_556 = (tn2 < tm2 || (tn2 == tm2 && depth[n] <= depth[m]));
      IACSharpSensor.IACSharpSensor.SensorReached(5164);
      return RNTRNTRNT_556;
    }
    internal void scan_tree(short[] tree, int max_code)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5165);
      int n;
      int prevlen = -1;
      int curlen;
      int nextlen = (int)tree[0 * 2 + 1];
      int count = 0;
      int max_count = 7;
      int min_count = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(5166);
      if (nextlen == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5167);
        max_count = 138;
        min_count = 3;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5168);
      tree[(max_code + 1) * 2 + 1] = (short)0x7fff;
      IACSharpSensor.IACSharpSensor.SensorReached(5169);
      for (n = 0; n <= max_code; n++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5170);
        curlen = nextlen;
        nextlen = (int)tree[(n + 1) * 2 + 1];
        IACSharpSensor.IACSharpSensor.SensorReached(5171);
        if (++count < max_count && curlen == nextlen) {
          IACSharpSensor.IACSharpSensor.SensorReached(5172);
          continue;
        } else if (count < min_count) {
          IACSharpSensor.IACSharpSensor.SensorReached(5178);
          bl_tree[curlen * 2] = (short)(bl_tree[curlen * 2] + count);
        } else if (curlen != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5175);
          if (curlen != prevlen) {
            IACSharpSensor.IACSharpSensor.SensorReached(5176);
            bl_tree[curlen * 2]++;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5177);
          bl_tree[InternalConstants.REP_3_6 * 2]++;
        } else if (count <= 10) {
          IACSharpSensor.IACSharpSensor.SensorReached(5174);
          bl_tree[InternalConstants.REPZ_3_10 * 2]++;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5173);
          bl_tree[InternalConstants.REPZ_11_138 * 2]++;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5179);
        count = 0;
        prevlen = curlen;
        IACSharpSensor.IACSharpSensor.SensorReached(5180);
        if (nextlen == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5181);
          max_count = 138;
          min_count = 3;
        } else if (curlen == nextlen) {
          IACSharpSensor.IACSharpSensor.SensorReached(5183);
          max_count = 6;
          min_count = 3;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5182);
          max_count = 7;
          min_count = 4;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5184);
    }
    internal int build_bl_tree()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5185);
      int max_blindex;
      scan_tree(dyn_ltree, treeLiterals.max_code);
      scan_tree(dyn_dtree, treeDistances.max_code);
      treeBitLengths.build_tree(this);
      IACSharpSensor.IACSharpSensor.SensorReached(5186);
      for (max_blindex = InternalConstants.BL_CODES - 1; max_blindex >= 3; max_blindex--) {
        IACSharpSensor.IACSharpSensor.SensorReached(5187);
        if (bl_tree[Tree.bl_order[max_blindex] * 2 + 1] != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5188);
          break;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5189);
      opt_len += 3 * (max_blindex + 1) + 5 + 5 + 4;
      System.Int32 RNTRNTRNT_557 = max_blindex;
      IACSharpSensor.IACSharpSensor.SensorReached(5190);
      return RNTRNTRNT_557;
    }
    internal void send_all_trees(int lcodes, int dcodes, int blcodes)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5191);
      int rank;
      send_bits(lcodes - 257, 5);
      send_bits(dcodes - 1, 5);
      send_bits(blcodes - 4, 4);
      IACSharpSensor.IACSharpSensor.SensorReached(5192);
      for (rank = 0; rank < blcodes; rank++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5193);
        send_bits(bl_tree[Tree.bl_order[rank] * 2 + 1], 3);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5194);
      send_tree(dyn_ltree, lcodes - 1);
      send_tree(dyn_dtree, dcodes - 1);
      IACSharpSensor.IACSharpSensor.SensorReached(5195);
    }
    internal void send_tree(short[] tree, int max_code)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5196);
      int n;
      int prevlen = -1;
      int curlen;
      int nextlen = tree[0 * 2 + 1];
      int count = 0;
      int max_count = 7;
      int min_count = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(5197);
      if (nextlen == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5198);
        max_count = 138;
        min_count = 3;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5199);
      for (n = 0; n <= max_code; n++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5200);
        curlen = nextlen;
        nextlen = tree[(n + 1) * 2 + 1];
        IACSharpSensor.IACSharpSensor.SensorReached(5201);
        if (++count < max_count && curlen == nextlen) {
          IACSharpSensor.IACSharpSensor.SensorReached(5202);
          continue;
        } else if (count < min_count) {
          IACSharpSensor.IACSharpSensor.SensorReached(5208);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(5209);
            send_code(curlen, bl_tree);
          } while (--count != 0);
        } else if (curlen != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5205);
          if (curlen != prevlen) {
            IACSharpSensor.IACSharpSensor.SensorReached(5206);
            send_code(curlen, bl_tree);
            count--;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5207);
          send_code(InternalConstants.REP_3_6, bl_tree);
          send_bits(count - 3, 2);
        } else if (count <= 10) {
          IACSharpSensor.IACSharpSensor.SensorReached(5204);
          send_code(InternalConstants.REPZ_3_10, bl_tree);
          send_bits(count - 3, 3);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5203);
          send_code(InternalConstants.REPZ_11_138, bl_tree);
          send_bits(count - 11, 7);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5210);
        count = 0;
        prevlen = curlen;
        IACSharpSensor.IACSharpSensor.SensorReached(5211);
        if (nextlen == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5212);
          max_count = 138;
          min_count = 3;
        } else if (curlen == nextlen) {
          IACSharpSensor.IACSharpSensor.SensorReached(5214);
          max_count = 6;
          min_count = 3;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5213);
          max_count = 7;
          min_count = 4;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5215);
    }
    private void put_bytes(byte[] p, int start, int len)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5216);
      Array.Copy(p, start, pending, pendingCount, len);
      pendingCount += len;
      IACSharpSensor.IACSharpSensor.SensorReached(5217);
    }
    internal void send_code(int c, short[] tree)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5218);
      int c2 = c * 2;
      send_bits((tree[c2] & 0xffff), (tree[c2 + 1] & 0xffff));
      IACSharpSensor.IACSharpSensor.SensorReached(5219);
    }
    internal void send_bits(int value, int length)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5220);
      int len = length;
      unchecked {
        IACSharpSensor.IACSharpSensor.SensorReached(5221);
        if (bi_valid > (int)Buf_size - len) {
          IACSharpSensor.IACSharpSensor.SensorReached(5222);
          bi_buf |= (short)((value << bi_valid) & 0xffff);
          pending[pendingCount++] = (byte)bi_buf;
          pending[pendingCount++] = (byte)(bi_buf >> 8);
          bi_buf = (short)((uint)value >> (Buf_size - bi_valid));
          bi_valid += len - Buf_size;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5223);
          bi_buf |= (short)((value << bi_valid) & 0xffff);
          bi_valid += len;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5224);
    }
    internal void _tr_align()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5225);
      send_bits(STATIC_TREES << 1, 3);
      send_code(END_BLOCK, StaticTree.lengthAndLiteralsTreeCodes);
      bi_flush();
      IACSharpSensor.IACSharpSensor.SensorReached(5226);
      if (1 + last_eob_len + 10 - bi_valid < 9) {
        IACSharpSensor.IACSharpSensor.SensorReached(5227);
        send_bits(STATIC_TREES << 1, 3);
        send_code(END_BLOCK, StaticTree.lengthAndLiteralsTreeCodes);
        bi_flush();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5228);
      last_eob_len = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(5229);
    }
    internal bool _tr_tally(int dist, int lc)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5230);
      pending[_distanceOffset + last_lit * 2] = unchecked((byte)((uint)dist >> 8));
      pending[_distanceOffset + last_lit * 2 + 1] = unchecked((byte)dist);
      pending[_lengthOffset + last_lit] = unchecked((byte)lc);
      last_lit++;
      IACSharpSensor.IACSharpSensor.SensorReached(5231);
      if (dist == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5232);
        dyn_ltree[lc * 2]++;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(5233);
        matches++;
        dist--;
        dyn_ltree[(Tree.LengthCode[lc] + InternalConstants.LITERALS + 1) * 2]++;
        dyn_dtree[Tree.DistanceCode(dist) * 2]++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5234);
      if ((last_lit & 0x1fff) == 0 && (int)compressionLevel > 2) {
        IACSharpSensor.IACSharpSensor.SensorReached(5235);
        int out_length = last_lit << 3;
        int in_length = strstart - block_start;
        int dcode;
        IACSharpSensor.IACSharpSensor.SensorReached(5236);
        for (dcode = 0; dcode < InternalConstants.D_CODES; dcode++) {
          IACSharpSensor.IACSharpSensor.SensorReached(5237);
          out_length = (int)(out_length + (int)dyn_dtree[dcode * 2] * (5L + Tree.ExtraDistanceBits[dcode]));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5238);
        out_length >>= 3;
        IACSharpSensor.IACSharpSensor.SensorReached(5239);
        if ((matches < (last_lit / 2)) && out_length < in_length / 2) {
          System.Boolean RNTRNTRNT_558 = true;
          IACSharpSensor.IACSharpSensor.SensorReached(5240);
          return RNTRNTRNT_558;
        }
      }
      System.Boolean RNTRNTRNT_559 = (last_lit == lit_bufsize - 1) || (last_lit == lit_bufsize);
      IACSharpSensor.IACSharpSensor.SensorReached(5241);
      return RNTRNTRNT_559;
    }
    internal void send_compressed_block(short[] ltree, short[] dtree)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5242);
      int distance;
      int lc;
      int lx = 0;
      int code;
      int extra;
      IACSharpSensor.IACSharpSensor.SensorReached(5243);
      if (last_lit != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5244);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(5245);
          int ix = _distanceOffset + lx * 2;
          distance = ((pending[ix] << 8) & 0xff00) | (pending[ix + 1] & 0xff);
          lc = (pending[_lengthOffset + lx]) & 0xff;
          lx++;
          IACSharpSensor.IACSharpSensor.SensorReached(5246);
          if (distance == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(5247);
            send_code(lc, ltree);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(5248);
            code = Tree.LengthCode[lc];
            send_code(code + InternalConstants.LITERALS + 1, ltree);
            extra = Tree.ExtraLengthBits[code];
            IACSharpSensor.IACSharpSensor.SensorReached(5249);
            if (extra != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5250);
              lc -= Tree.LengthBase[code];
              send_bits(lc, extra);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(5251);
            distance--;
            code = Tree.DistanceCode(distance);
            send_code(code, dtree);
            extra = Tree.ExtraDistanceBits[code];
            IACSharpSensor.IACSharpSensor.SensorReached(5252);
            if (extra != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(5253);
              distance -= Tree.DistanceBase[code];
              send_bits(distance, extra);
            }
          }
        } while (lx < last_lit);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5254);
      send_code(END_BLOCK, ltree);
      last_eob_len = ltree[END_BLOCK * 2 + 1];
      IACSharpSensor.IACSharpSensor.SensorReached(5255);
    }
    internal void set_data_type()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5256);
      int n = 0;
      int ascii_freq = 0;
      int bin_freq = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5257);
      while (n < 7) {
        IACSharpSensor.IACSharpSensor.SensorReached(5258);
        bin_freq += dyn_ltree[n * 2];
        n++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5259);
      while (n < 128) {
        IACSharpSensor.IACSharpSensor.SensorReached(5260);
        ascii_freq += dyn_ltree[n * 2];
        n++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5261);
      while (n < InternalConstants.LITERALS) {
        IACSharpSensor.IACSharpSensor.SensorReached(5262);
        bin_freq += dyn_ltree[n * 2];
        n++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5263);
      data_type = (sbyte)(bin_freq > (ascii_freq >> 2) ? Z_BINARY : Z_ASCII);
      IACSharpSensor.IACSharpSensor.SensorReached(5264);
    }
    internal void bi_flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5265);
      if (bi_valid == 16) {
        IACSharpSensor.IACSharpSensor.SensorReached(5266);
        pending[pendingCount++] = (byte)bi_buf;
        pending[pendingCount++] = (byte)(bi_buf >> 8);
        bi_buf = 0;
        bi_valid = 0;
      } else if (bi_valid >= 8) {
        IACSharpSensor.IACSharpSensor.SensorReached(5267);
        pending[pendingCount++] = (byte)bi_buf;
        bi_buf >>= 8;
        bi_valid -= 8;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5268);
    }
    internal void bi_windup()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5269);
      if (bi_valid > 8) {
        IACSharpSensor.IACSharpSensor.SensorReached(5270);
        pending[pendingCount++] = (byte)bi_buf;
        pending[pendingCount++] = (byte)(bi_buf >> 8);
      } else if (bi_valid > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5271);
        pending[pendingCount++] = (byte)bi_buf;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5272);
      bi_buf = 0;
      bi_valid = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5273);
    }
    internal void copy_block(int buf, int len, bool header)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5274);
      bi_windup();
      last_eob_len = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(5275);
      if (header) {
        unchecked {
          IACSharpSensor.IACSharpSensor.SensorReached(5276);
          pending[pendingCount++] = (byte)len;
          pending[pendingCount++] = (byte)(len >> 8);
          pending[pendingCount++] = (byte)~len;
          pending[pendingCount++] = (byte)(~len >> 8);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5277);
      put_bytes(window, buf, len);
      IACSharpSensor.IACSharpSensor.SensorReached(5278);
    }
    internal void flush_block_only(bool eof)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5279);
      _tr_flush_block(block_start >= 0 ? block_start : -1, strstart - block_start, eof);
      block_start = strstart;
      _codec.flush_pending();
      IACSharpSensor.IACSharpSensor.SensorReached(5280);
    }
    internal BlockState DeflateNone(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5281);
      int max_block_size = 0xffff;
      int max_start;
      IACSharpSensor.IACSharpSensor.SensorReached(5282);
      if (max_block_size > pending.Length - 5) {
        IACSharpSensor.IACSharpSensor.SensorReached(5283);
        max_block_size = pending.Length - 5;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5284);
      while (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(5285);
        if (lookahead <= 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(5286);
          _fillWindow();
          IACSharpSensor.IACSharpSensor.SensorReached(5287);
          if (lookahead == 0 && flush == FlushType.None) {
            BlockState RNTRNTRNT_560 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(5288);
            return RNTRNTRNT_560;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5289);
          if (lookahead == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(5290);
            break;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5291);
        strstart += lookahead;
        lookahead = 0;
        max_start = block_start + max_block_size;
        IACSharpSensor.IACSharpSensor.SensorReached(5292);
        if (strstart == 0 || strstart >= max_start) {
          IACSharpSensor.IACSharpSensor.SensorReached(5293);
          lookahead = (int)(strstart - max_start);
          strstart = (int)max_start;
          flush_block_only(false);
          IACSharpSensor.IACSharpSensor.SensorReached(5294);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_561 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(5295);
            return RNTRNTRNT_561;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5296);
        if (strstart - block_start >= w_size - MIN_LOOKAHEAD) {
          IACSharpSensor.IACSharpSensor.SensorReached(5297);
          flush_block_only(false);
          IACSharpSensor.IACSharpSensor.SensorReached(5298);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_562 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(5299);
            return RNTRNTRNT_562;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5300);
      flush_block_only(flush == FlushType.Finish);
      IACSharpSensor.IACSharpSensor.SensorReached(5301);
      if (_codec.AvailableBytesOut == 0) {
        BlockState RNTRNTRNT_563 = (flush == FlushType.Finish) ? BlockState.FinishStarted : BlockState.NeedMore;
        IACSharpSensor.IACSharpSensor.SensorReached(5302);
        return RNTRNTRNT_563;
      }
      BlockState RNTRNTRNT_564 = flush == FlushType.Finish ? BlockState.FinishDone : BlockState.BlockDone;
      IACSharpSensor.IACSharpSensor.SensorReached(5303);
      return RNTRNTRNT_564;
    }
    internal void _tr_stored_block(int buf, int stored_len, bool eof)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5304);
      send_bits((STORED_BLOCK << 1) + (eof ? 1 : 0), 3);
      copy_block(buf, stored_len, true);
      IACSharpSensor.IACSharpSensor.SensorReached(5305);
    }
    internal void _tr_flush_block(int buf, int stored_len, bool eof)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5306);
      int opt_lenb, static_lenb;
      int max_blindex = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5307);
      if (compressionLevel > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5308);
        if (data_type == Z_UNKNOWN) {
          IACSharpSensor.IACSharpSensor.SensorReached(5309);
          set_data_type();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5310);
        treeLiterals.build_tree(this);
        treeDistances.build_tree(this);
        max_blindex = build_bl_tree();
        opt_lenb = (opt_len + 3 + 7) >> 3;
        static_lenb = (static_len + 3 + 7) >> 3;
        IACSharpSensor.IACSharpSensor.SensorReached(5311);
        if (static_lenb <= opt_lenb) {
          IACSharpSensor.IACSharpSensor.SensorReached(5312);
          opt_lenb = static_lenb;
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(5313);
        opt_lenb = static_lenb = stored_len + 5;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5314);
      if (stored_len + 4 <= opt_lenb && buf != -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(5315);
        _tr_stored_block(buf, stored_len, eof);
      } else if (static_lenb == opt_lenb) {
        IACSharpSensor.IACSharpSensor.SensorReached(5317);
        send_bits((STATIC_TREES << 1) + (eof ? 1 : 0), 3);
        send_compressed_block(StaticTree.lengthAndLiteralsTreeCodes, StaticTree.distTreeCodes);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(5316);
        send_bits((DYN_TREES << 1) + (eof ? 1 : 0), 3);
        send_all_trees(treeLiterals.max_code + 1, treeDistances.max_code + 1, max_blindex + 1);
        send_compressed_block(dyn_ltree, dyn_dtree);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5318);
      _InitializeBlocks();
      IACSharpSensor.IACSharpSensor.SensorReached(5319);
      if (eof) {
        IACSharpSensor.IACSharpSensor.SensorReached(5320);
        bi_windup();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5321);
    }
    private void _fillWindow()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5322);
      int n, m;
      int p;
      int more;
      IACSharpSensor.IACSharpSensor.SensorReached(5323);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(5324);
        more = (window_size - lookahead - strstart);
        IACSharpSensor.IACSharpSensor.SensorReached(5325);
        if (more == 0 && strstart == 0 && lookahead == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5326);
          more = w_size;
        } else if (more == -1) {
          IACSharpSensor.IACSharpSensor.SensorReached(5334);
          more--;
        } else if (strstart >= w_size + w_size - MIN_LOOKAHEAD) {
          IACSharpSensor.IACSharpSensor.SensorReached(5327);
          Array.Copy(window, w_size, window, 0, w_size);
          match_start -= w_size;
          strstart -= w_size;
          block_start -= w_size;
          n = hash_size;
          p = n;
          IACSharpSensor.IACSharpSensor.SensorReached(5328);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(5329);
            m = (head[--p] & 0xffff);
            head[p] = (short)((m >= w_size) ? (m - w_size) : 0);
          } while (--n != 0);
          IACSharpSensor.IACSharpSensor.SensorReached(5330);
          n = w_size;
          p = n;
          IACSharpSensor.IACSharpSensor.SensorReached(5331);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(5332);
            m = (prev[--p] & 0xffff);
            prev[p] = (short)((m >= w_size) ? (m - w_size) : 0);
          } while (--n != 0);
          IACSharpSensor.IACSharpSensor.SensorReached(5333);
          more += w_size;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5335);
        if (_codec.AvailableBytesIn == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5336);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5337);
        n = _codec.read_buf(window, strstart + lookahead, more);
        lookahead += n;
        IACSharpSensor.IACSharpSensor.SensorReached(5338);
        if (lookahead >= MIN_MATCH) {
          IACSharpSensor.IACSharpSensor.SensorReached(5339);
          ins_h = window[strstart] & 0xff;
          ins_h = (((ins_h) << hash_shift) ^ (window[strstart + 1] & 0xff)) & hash_mask;
        }
      } while (lookahead < MIN_LOOKAHEAD && _codec.AvailableBytesIn != 0);
      IACSharpSensor.IACSharpSensor.SensorReached(5340);
    }
    internal BlockState DeflateFast(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5341);
      int hash_head = 0;
      bool bflush;
      IACSharpSensor.IACSharpSensor.SensorReached(5342);
      while (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(5343);
        if (lookahead < MIN_LOOKAHEAD) {
          IACSharpSensor.IACSharpSensor.SensorReached(5344);
          _fillWindow();
          IACSharpSensor.IACSharpSensor.SensorReached(5345);
          if (lookahead < MIN_LOOKAHEAD && flush == FlushType.None) {
            BlockState RNTRNTRNT_565 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(5346);
            return RNTRNTRNT_565;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5347);
          if (lookahead == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(5348);
            break;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5349);
        if (lookahead >= MIN_MATCH) {
          IACSharpSensor.IACSharpSensor.SensorReached(5350);
          ins_h = (((ins_h) << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
          hash_head = (head[ins_h] & 0xffff);
          prev[strstart & w_mask] = head[ins_h];
          head[ins_h] = unchecked((short)strstart);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5351);
        if (hash_head != 0L && ((strstart - hash_head) & 0xffff) <= w_size - MIN_LOOKAHEAD) {
          IACSharpSensor.IACSharpSensor.SensorReached(5352);
          if (compressionStrategy != CompressionStrategy.HuffmanOnly) {
            IACSharpSensor.IACSharpSensor.SensorReached(5353);
            match_length = longest_match(hash_head);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5354);
        if (match_length >= MIN_MATCH) {
          IACSharpSensor.IACSharpSensor.SensorReached(5355);
          bflush = _tr_tally(strstart - match_start, match_length - MIN_MATCH);
          lookahead -= match_length;
          IACSharpSensor.IACSharpSensor.SensorReached(5356);
          if (match_length <= config.MaxLazy && lookahead >= MIN_MATCH) {
            IACSharpSensor.IACSharpSensor.SensorReached(5357);
            match_length--;
            IACSharpSensor.IACSharpSensor.SensorReached(5358);
            do {
              IACSharpSensor.IACSharpSensor.SensorReached(5359);
              strstart++;
              ins_h = ((ins_h << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
              hash_head = (head[ins_h] & 0xffff);
              prev[strstart & w_mask] = head[ins_h];
              head[ins_h] = unchecked((short)strstart);
            } while (--match_length != 0);
            IACSharpSensor.IACSharpSensor.SensorReached(5360);
            strstart++;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(5361);
            strstart += match_length;
            match_length = 0;
            ins_h = window[strstart] & 0xff;
            ins_h = (((ins_h) << hash_shift) ^ (window[strstart + 1] & 0xff)) & hash_mask;
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5362);
          bflush = _tr_tally(0, window[strstart] & 0xff);
          lookahead--;
          strstart++;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5363);
        if (bflush) {
          IACSharpSensor.IACSharpSensor.SensorReached(5364);
          flush_block_only(false);
          IACSharpSensor.IACSharpSensor.SensorReached(5365);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_566 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(5366);
            return RNTRNTRNT_566;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5367);
      flush_block_only(flush == FlushType.Finish);
      IACSharpSensor.IACSharpSensor.SensorReached(5368);
      if (_codec.AvailableBytesOut == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5369);
        if (flush == FlushType.Finish) {
          BlockState RNTRNTRNT_567 = BlockState.FinishStarted;
          IACSharpSensor.IACSharpSensor.SensorReached(5370);
          return RNTRNTRNT_567;
        } else {
          BlockState RNTRNTRNT_568 = BlockState.NeedMore;
          IACSharpSensor.IACSharpSensor.SensorReached(5371);
          return RNTRNTRNT_568;
        }
      }
      BlockState RNTRNTRNT_569 = flush == FlushType.Finish ? BlockState.FinishDone : BlockState.BlockDone;
      IACSharpSensor.IACSharpSensor.SensorReached(5372);
      return RNTRNTRNT_569;
    }
    internal BlockState DeflateSlow(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5373);
      int hash_head = 0;
      bool bflush;
      IACSharpSensor.IACSharpSensor.SensorReached(5374);
      while (true) {
        IACSharpSensor.IACSharpSensor.SensorReached(5375);
        if (lookahead < MIN_LOOKAHEAD) {
          IACSharpSensor.IACSharpSensor.SensorReached(5376);
          _fillWindow();
          IACSharpSensor.IACSharpSensor.SensorReached(5377);
          if (lookahead < MIN_LOOKAHEAD && flush == FlushType.None) {
            BlockState RNTRNTRNT_570 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(5378);
            return RNTRNTRNT_570;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5379);
          if (lookahead == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(5380);
            break;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5381);
        if (lookahead >= MIN_MATCH) {
          IACSharpSensor.IACSharpSensor.SensorReached(5382);
          ins_h = (((ins_h) << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
          hash_head = (head[ins_h] & 0xffff);
          prev[strstart & w_mask] = head[ins_h];
          head[ins_h] = unchecked((short)strstart);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5383);
        prev_length = match_length;
        prev_match = match_start;
        match_length = MIN_MATCH - 1;
        IACSharpSensor.IACSharpSensor.SensorReached(5384);
        if (hash_head != 0 && prev_length < config.MaxLazy && ((strstart - hash_head) & 0xffff) <= w_size - MIN_LOOKAHEAD) {
          IACSharpSensor.IACSharpSensor.SensorReached(5385);
          if (compressionStrategy != CompressionStrategy.HuffmanOnly) {
            IACSharpSensor.IACSharpSensor.SensorReached(5386);
            match_length = longest_match(hash_head);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5387);
          if (match_length <= 5 && (compressionStrategy == CompressionStrategy.Filtered || (match_length == MIN_MATCH && strstart - match_start > 4096))) {
            IACSharpSensor.IACSharpSensor.SensorReached(5388);
            match_length = MIN_MATCH - 1;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5389);
        if (prev_length >= MIN_MATCH && match_length <= prev_length) {
          IACSharpSensor.IACSharpSensor.SensorReached(5390);
          int max_insert = strstart + lookahead - MIN_MATCH;
          bflush = _tr_tally(strstart - 1 - prev_match, prev_length - MIN_MATCH);
          lookahead -= (prev_length - 1);
          prev_length -= 2;
          IACSharpSensor.IACSharpSensor.SensorReached(5391);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(5392);
            if (++strstart <= max_insert) {
              IACSharpSensor.IACSharpSensor.SensorReached(5393);
              ins_h = (((ins_h) << hash_shift) ^ (window[(strstart) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
              hash_head = (head[ins_h] & 0xffff);
              prev[strstart & w_mask] = head[ins_h];
              head[ins_h] = unchecked((short)strstart);
            }
          } while (--prev_length != 0);
          IACSharpSensor.IACSharpSensor.SensorReached(5394);
          match_available = 0;
          match_length = MIN_MATCH - 1;
          strstart++;
          IACSharpSensor.IACSharpSensor.SensorReached(5395);
          if (bflush) {
            IACSharpSensor.IACSharpSensor.SensorReached(5396);
            flush_block_only(false);
            IACSharpSensor.IACSharpSensor.SensorReached(5397);
            if (_codec.AvailableBytesOut == 0) {
              BlockState RNTRNTRNT_571 = BlockState.NeedMore;
              IACSharpSensor.IACSharpSensor.SensorReached(5398);
              return RNTRNTRNT_571;
            }
          }
        } else if (match_available != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5400);
          bflush = _tr_tally(0, window[strstart - 1] & 0xff);
          IACSharpSensor.IACSharpSensor.SensorReached(5401);
          if (bflush) {
            IACSharpSensor.IACSharpSensor.SensorReached(5402);
            flush_block_only(false);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5403);
          strstart++;
          lookahead--;
          IACSharpSensor.IACSharpSensor.SensorReached(5404);
          if (_codec.AvailableBytesOut == 0) {
            BlockState RNTRNTRNT_572 = BlockState.NeedMore;
            IACSharpSensor.IACSharpSensor.SensorReached(5405);
            return RNTRNTRNT_572;
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(5399);
          match_available = 1;
          strstart++;
          lookahead--;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5406);
      if (match_available != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5407);
        bflush = _tr_tally(0, window[strstart - 1] & 0xff);
        match_available = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5408);
      flush_block_only(flush == FlushType.Finish);
      IACSharpSensor.IACSharpSensor.SensorReached(5409);
      if (_codec.AvailableBytesOut == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5410);
        if (flush == FlushType.Finish) {
          BlockState RNTRNTRNT_573 = BlockState.FinishStarted;
          IACSharpSensor.IACSharpSensor.SensorReached(5411);
          return RNTRNTRNT_573;
        } else {
          BlockState RNTRNTRNT_574 = BlockState.NeedMore;
          IACSharpSensor.IACSharpSensor.SensorReached(5412);
          return RNTRNTRNT_574;
        }
      }
      BlockState RNTRNTRNT_575 = flush == FlushType.Finish ? BlockState.FinishDone : BlockState.BlockDone;
      IACSharpSensor.IACSharpSensor.SensorReached(5413);
      return RNTRNTRNT_575;
    }
    internal int longest_match(int cur_match)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5414);
      int chain_length = config.MaxChainLength;
      int scan = strstart;
      int match;
      int len;
      int best_len = prev_length;
      int limit = strstart > (w_size - MIN_LOOKAHEAD) ? strstart - (w_size - MIN_LOOKAHEAD) : 0;
      int niceLength = config.NiceLength;
      int wmask = w_mask;
      int strend = strstart + MAX_MATCH;
      byte scan_end1 = window[scan + best_len - 1];
      byte scan_end = window[scan + best_len];
      IACSharpSensor.IACSharpSensor.SensorReached(5415);
      if (prev_length >= config.GoodLength) {
        IACSharpSensor.IACSharpSensor.SensorReached(5416);
        chain_length >>= 2;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5417);
      if (niceLength > lookahead) {
        IACSharpSensor.IACSharpSensor.SensorReached(5418);
        niceLength = lookahead;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5419);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(5420);
        match = cur_match;
        IACSharpSensor.IACSharpSensor.SensorReached(5421);
        if (window[match + best_len] != scan_end || window[match + best_len - 1] != scan_end1 || window[match] != window[scan] || window[++match] != window[scan + 1]) {
          IACSharpSensor.IACSharpSensor.SensorReached(5422);
          continue;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5423);
        scan += 2;
        match++;
        IACSharpSensor.IACSharpSensor.SensorReached(5424);
        do {
        } while (window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && window[++scan] == window[++match] && scan < strend);
        IACSharpSensor.IACSharpSensor.SensorReached(5425);
        len = MAX_MATCH - (int)(strend - scan);
        scan = strend - MAX_MATCH;
        IACSharpSensor.IACSharpSensor.SensorReached(5426);
        if (len > best_len) {
          IACSharpSensor.IACSharpSensor.SensorReached(5427);
          match_start = cur_match;
          best_len = len;
          IACSharpSensor.IACSharpSensor.SensorReached(5428);
          if (len >= niceLength) {
            IACSharpSensor.IACSharpSensor.SensorReached(5429);
            break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5430);
          scan_end1 = window[scan + best_len - 1];
          scan_end = window[scan + best_len];
        }
      } while ((cur_match = (prev[cur_match & wmask] & 0xffff)) > limit && --chain_length != 0);
      IACSharpSensor.IACSharpSensor.SensorReached(5431);
      if (best_len <= lookahead) {
        System.Int32 RNTRNTRNT_576 = best_len;
        IACSharpSensor.IACSharpSensor.SensorReached(5432);
        return RNTRNTRNT_576;
      }
      System.Int32 RNTRNTRNT_577 = lookahead;
      IACSharpSensor.IACSharpSensor.SensorReached(5433);
      return RNTRNTRNT_577;
    }
    private bool Rfc1950BytesEmitted = false;
    private bool _WantRfc1950HeaderBytes = true;
    internal bool WantRfc1950HeaderBytes {
      get {
        System.Boolean RNTRNTRNT_578 = _WantRfc1950HeaderBytes;
        IACSharpSensor.IACSharpSensor.SensorReached(5434);
        return RNTRNTRNT_578;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5435);
        _WantRfc1950HeaderBytes = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5436);
      }
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level)
    {
      System.Int32 RNTRNTRNT_579 = Initialize(codec, level, ZlibConstants.WindowBitsMax);
      IACSharpSensor.IACSharpSensor.SensorReached(5437);
      return RNTRNTRNT_579;
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level, int bits)
    {
      System.Int32 RNTRNTRNT_580 = Initialize(codec, level, bits, MEM_LEVEL_DEFAULT, CompressionStrategy.Default);
      IACSharpSensor.IACSharpSensor.SensorReached(5438);
      return RNTRNTRNT_580;
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level, int bits, CompressionStrategy compressionStrategy)
    {
      System.Int32 RNTRNTRNT_581 = Initialize(codec, level, bits, MEM_LEVEL_DEFAULT, compressionStrategy);
      IACSharpSensor.IACSharpSensor.SensorReached(5439);
      return RNTRNTRNT_581;
    }
    internal int Initialize(ZlibCodec codec, CompressionLevel level, int windowBits, int memLevel, CompressionStrategy strategy)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5440);
      _codec = codec;
      _codec.Message = null;
      IACSharpSensor.IACSharpSensor.SensorReached(5441);
      if (windowBits < 9 || windowBits > 15) {
        IACSharpSensor.IACSharpSensor.SensorReached(5442);
        throw new ZlibException("windowBits must be in the range 9..15.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5443);
      if (memLevel < 1 || memLevel > MEM_LEVEL_MAX) {
        IACSharpSensor.IACSharpSensor.SensorReached(5444);
        throw new ZlibException(String.Format("memLevel must be in the range 1.. {0}", MEM_LEVEL_MAX));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5445);
      _codec.dstate = this;
      w_bits = windowBits;
      w_size = 1 << w_bits;
      w_mask = w_size - 1;
      hash_bits = memLevel + 7;
      hash_size = 1 << hash_bits;
      hash_mask = hash_size - 1;
      hash_shift = ((hash_bits + MIN_MATCH - 1) / MIN_MATCH);
      window = new byte[w_size * 2];
      prev = new short[w_size];
      head = new short[hash_size];
      lit_bufsize = 1 << (memLevel + 6);
      pending = new byte[lit_bufsize * 4];
      _distanceOffset = lit_bufsize;
      _lengthOffset = (1 + 2) * lit_bufsize;
      this.compressionLevel = level;
      this.compressionStrategy = strategy;
      Reset();
      System.Int32 RNTRNTRNT_582 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(5446);
      return RNTRNTRNT_582;
    }
    internal void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5447);
      _codec.TotalBytesIn = _codec.TotalBytesOut = 0;
      _codec.Message = null;
      pendingCount = 0;
      nextPending = 0;
      Rfc1950BytesEmitted = false;
      status = (WantRfc1950HeaderBytes) ? INIT_STATE : BUSY_STATE;
      _codec._Adler32 = Adler.Adler32(0, null, 0, 0);
      last_flush = (int)FlushType.None;
      _InitializeTreeData();
      _InitializeLazyMatch();
      IACSharpSensor.IACSharpSensor.SensorReached(5448);
    }
    internal int End()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5449);
      if (status != INIT_STATE && status != BUSY_STATE && status != FINISH_STATE) {
        System.Int32 RNTRNTRNT_583 = ZlibConstants.Z_STREAM_ERROR;
        IACSharpSensor.IACSharpSensor.SensorReached(5450);
        return RNTRNTRNT_583;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5451);
      pending = null;
      head = null;
      prev = null;
      window = null;
      System.Int32 RNTRNTRNT_584 = status == BUSY_STATE ? ZlibConstants.Z_DATA_ERROR : ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(5452);
      return RNTRNTRNT_584;
    }
    private void SetDeflater()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5453);
      switch (config.Flavor) {
        case DeflateFlavor.Store:
          IACSharpSensor.IACSharpSensor.SensorReached(5454);
          DeflateFunction = DeflateNone;
          IACSharpSensor.IACSharpSensor.SensorReached(5455);
          break;
        case DeflateFlavor.Fast:
          IACSharpSensor.IACSharpSensor.SensorReached(5456);
          DeflateFunction = DeflateFast;
          IACSharpSensor.IACSharpSensor.SensorReached(5457);
          break;
        case DeflateFlavor.Slow:
          IACSharpSensor.IACSharpSensor.SensorReached(5458);
          DeflateFunction = DeflateSlow;
          IACSharpSensor.IACSharpSensor.SensorReached(5459);
          break;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5460);
    }
    internal int SetParams(CompressionLevel level, CompressionStrategy strategy)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5461);
      int result = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(5462);
      if (compressionLevel != level) {
        IACSharpSensor.IACSharpSensor.SensorReached(5463);
        Config newConfig = Config.Lookup(level);
        IACSharpSensor.IACSharpSensor.SensorReached(5464);
        if (newConfig.Flavor != config.Flavor && _codec.TotalBytesIn != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5465);
          result = _codec.Deflate(FlushType.Partial);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5466);
        compressionLevel = level;
        config = newConfig;
        SetDeflater();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5467);
      compressionStrategy = strategy;
      System.Int32 RNTRNTRNT_585 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(5468);
      return RNTRNTRNT_585;
    }
    internal int SetDictionary(byte[] dictionary)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5469);
      int length = dictionary.Length;
      int index = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(5470);
      if (dictionary == null || status != INIT_STATE) {
        IACSharpSensor.IACSharpSensor.SensorReached(5471);
        throw new ZlibException("Stream error.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5472);
      _codec._Adler32 = Adler.Adler32(_codec._Adler32, dictionary, 0, dictionary.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(5473);
      if (length < MIN_MATCH) {
        System.Int32 RNTRNTRNT_586 = ZlibConstants.Z_OK;
        IACSharpSensor.IACSharpSensor.SensorReached(5474);
        return RNTRNTRNT_586;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5475);
      if (length > w_size - MIN_LOOKAHEAD) {
        IACSharpSensor.IACSharpSensor.SensorReached(5476);
        length = w_size - MIN_LOOKAHEAD;
        index = dictionary.Length - length;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5477);
      Array.Copy(dictionary, index, window, 0, length);
      strstart = length;
      block_start = length;
      ins_h = window[0] & 0xff;
      ins_h = (((ins_h) << hash_shift) ^ (window[1] & 0xff)) & hash_mask;
      IACSharpSensor.IACSharpSensor.SensorReached(5478);
      for (int n = 0; n <= length - MIN_MATCH; n++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5479);
        ins_h = (((ins_h) << hash_shift) ^ (window[(n) + (MIN_MATCH - 1)] & 0xff)) & hash_mask;
        prev[n & w_mask] = head[ins_h];
        head[ins_h] = (short)n;
      }
      System.Int32 RNTRNTRNT_587 = ZlibConstants.Z_OK;
      IACSharpSensor.IACSharpSensor.SensorReached(5480);
      return RNTRNTRNT_587;
    }
    internal int Deflate(FlushType flush)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5481);
      int old_flush;
      IACSharpSensor.IACSharpSensor.SensorReached(5482);
      if (_codec.OutputBuffer == null || (_codec.InputBuffer == null && _codec.AvailableBytesIn != 0) || (status == FINISH_STATE && flush != FlushType.Finish)) {
        IACSharpSensor.IACSharpSensor.SensorReached(5483);
        _codec.Message = _ErrorMessage[ZlibConstants.Z_NEED_DICT - (ZlibConstants.Z_STREAM_ERROR)];
        IACSharpSensor.IACSharpSensor.SensorReached(5484);
        throw new ZlibException(String.Format("Something is fishy. [{0}]", _codec.Message));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5485);
      if (_codec.AvailableBytesOut == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5486);
        _codec.Message = _ErrorMessage[ZlibConstants.Z_NEED_DICT - (ZlibConstants.Z_BUF_ERROR)];
        IACSharpSensor.IACSharpSensor.SensorReached(5487);
        throw new ZlibException("OutputBuffer is full (AvailableBytesOut == 0)");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5488);
      old_flush = last_flush;
      last_flush = (int)flush;
      IACSharpSensor.IACSharpSensor.SensorReached(5489);
      if (status == INIT_STATE) {
        IACSharpSensor.IACSharpSensor.SensorReached(5490);
        int header = (Z_DEFLATED + ((w_bits - 8) << 4)) << 8;
        int level_flags = (((int)compressionLevel - 1) & 0xff) >> 1;
        IACSharpSensor.IACSharpSensor.SensorReached(5491);
        if (level_flags > 3) {
          IACSharpSensor.IACSharpSensor.SensorReached(5492);
          level_flags = 3;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5493);
        header |= (level_flags << 6);
        IACSharpSensor.IACSharpSensor.SensorReached(5494);
        if (strstart != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5495);
          header |= PRESET_DICT;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5496);
        header += 31 - (header % 31);
        status = BUSY_STATE;
        unchecked {
          IACSharpSensor.IACSharpSensor.SensorReached(5497);
          pending[pendingCount++] = (byte)(header >> 8);
          pending[pendingCount++] = (byte)header;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5498);
        if (strstart != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5499);
          pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff000000u) >> 24);
          pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff0000) >> 16);
          pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff00) >> 8);
          pending[pendingCount++] = (byte)(_codec._Adler32 & 0xff);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5500);
        _codec._Adler32 = Adler.Adler32(0, null, 0, 0);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5501);
      if (pendingCount != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5502);
        _codec.flush_pending();
        IACSharpSensor.IACSharpSensor.SensorReached(5503);
        if (_codec.AvailableBytesOut == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(5504);
          last_flush = -1;
          System.Int32 RNTRNTRNT_588 = ZlibConstants.Z_OK;
          IACSharpSensor.IACSharpSensor.SensorReached(5505);
          return RNTRNTRNT_588;
        }
      } else if (_codec.AvailableBytesIn == 0 && (int)flush <= old_flush && flush != FlushType.Finish) {
        System.Int32 RNTRNTRNT_589 = ZlibConstants.Z_OK;
        IACSharpSensor.IACSharpSensor.SensorReached(5506);
        return RNTRNTRNT_589;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5507);
      if (status == FINISH_STATE && _codec.AvailableBytesIn != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(5508);
        _codec.Message = _ErrorMessage[ZlibConstants.Z_NEED_DICT - (ZlibConstants.Z_BUF_ERROR)];
        IACSharpSensor.IACSharpSensor.SensorReached(5509);
        throw new ZlibException("status == FINISH_STATE && _codec.AvailableBytesIn != 0");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5510);
      if (_codec.AvailableBytesIn != 0 || lookahead != 0 || (flush != FlushType.None && status != FINISH_STATE)) {
        IACSharpSensor.IACSharpSensor.SensorReached(5511);
        BlockState bstate = DeflateFunction(flush);
        IACSharpSensor.IACSharpSensor.SensorReached(5512);
        if (bstate == BlockState.FinishStarted || bstate == BlockState.FinishDone) {
          IACSharpSensor.IACSharpSensor.SensorReached(5513);
          status = FINISH_STATE;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5514);
        if (bstate == BlockState.NeedMore || bstate == BlockState.FinishStarted) {
          IACSharpSensor.IACSharpSensor.SensorReached(5515);
          if (_codec.AvailableBytesOut == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(5516);
            last_flush = -1;
          }
          System.Int32 RNTRNTRNT_590 = ZlibConstants.Z_OK;
          IACSharpSensor.IACSharpSensor.SensorReached(5517);
          return RNTRNTRNT_590;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5518);
        if (bstate == BlockState.BlockDone) {
          IACSharpSensor.IACSharpSensor.SensorReached(5519);
          if (flush == FlushType.Partial) {
            IACSharpSensor.IACSharpSensor.SensorReached(5520);
            _tr_align();
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(5521);
            _tr_stored_block(0, 0, false);
            IACSharpSensor.IACSharpSensor.SensorReached(5522);
            if (flush == FlushType.Full) {
              IACSharpSensor.IACSharpSensor.SensorReached(5523);
              for (int i = 0; i < hash_size; i++) {
                IACSharpSensor.IACSharpSensor.SensorReached(5524);
                head[i] = 0;
              }
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5525);
          _codec.flush_pending();
          IACSharpSensor.IACSharpSensor.SensorReached(5526);
          if (_codec.AvailableBytesOut == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(5527);
            last_flush = -1;
            System.Int32 RNTRNTRNT_591 = ZlibConstants.Z_OK;
            IACSharpSensor.IACSharpSensor.SensorReached(5528);
            return RNTRNTRNT_591;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5529);
      if (flush != FlushType.Finish) {
        System.Int32 RNTRNTRNT_592 = ZlibConstants.Z_OK;
        IACSharpSensor.IACSharpSensor.SensorReached(5530);
        return RNTRNTRNT_592;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5531);
      if (!WantRfc1950HeaderBytes || Rfc1950BytesEmitted) {
        System.Int32 RNTRNTRNT_593 = ZlibConstants.Z_STREAM_END;
        IACSharpSensor.IACSharpSensor.SensorReached(5532);
        return RNTRNTRNT_593;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5533);
      pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff000000u) >> 24);
      pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff0000) >> 16);
      pending[pendingCount++] = (byte)((_codec._Adler32 & 0xff00) >> 8);
      pending[pendingCount++] = (byte)(_codec._Adler32 & 0xff);
      _codec.flush_pending();
      Rfc1950BytesEmitted = true;
      System.Int32 RNTRNTRNT_594 = pendingCount != 0 ? ZlibConstants.Z_OK : ZlibConstants.Z_STREAM_END;
      IACSharpSensor.IACSharpSensor.SensorReached(5534);
      return RNTRNTRNT_594;
    }
  }
}
