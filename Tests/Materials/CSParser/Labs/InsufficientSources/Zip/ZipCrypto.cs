using System;
namespace Ionic.Zip
{
  internal class ZipCrypto
  {
    private ZipCrypto()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1792);
    }
    public static ZipCrypto ForWrite(string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1793);
      ZipCrypto z = new ZipCrypto();
      IACSharpSensor.IACSharpSensor.SensorReached(1794);
      if (password == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1795);
        throw new BadPasswordException("This entry requires a password.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1796);
      z.InitCipher(password);
      ZipCrypto RNTRNTRNT_173 = z;
      IACSharpSensor.IACSharpSensor.SensorReached(1797);
      return RNTRNTRNT_173;
    }
    public static ZipCrypto ForRead(string password, ZipEntry e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1798);
      System.IO.Stream s = e._archiveStream;
      e._WeakEncryptionHeader = new byte[12];
      byte[] eh = e._WeakEncryptionHeader;
      ZipCrypto z = new ZipCrypto();
      IACSharpSensor.IACSharpSensor.SensorReached(1799);
      if (password == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1800);
        throw new BadPasswordException("This entry requires a password.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1801);
      z.InitCipher(password);
      ZipEntry.ReadWeakEncryptionHeader(s, eh);
      byte[] DecryptedHeader = z.DecryptMessage(eh, eh.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(1802);
      if (DecryptedHeader[11] != (byte)((e._Crc32 >> 24) & 0xff)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1803);
        if ((e._BitField & 0x8) != 0x8) {
          IACSharpSensor.IACSharpSensor.SensorReached(1804);
          throw new BadPasswordException("The password did not match.");
        } else if (DecryptedHeader[11] != (byte)((e._TimeBlob >> 8) & 0xff)) {
          IACSharpSensor.IACSharpSensor.SensorReached(1805);
          throw new BadPasswordException("The password did not match.");
        }
      } else {
      }
      ZipCrypto RNTRNTRNT_174 = z;
      IACSharpSensor.IACSharpSensor.SensorReached(1806);
      return RNTRNTRNT_174;
    }
    private byte MagicByte {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1807);
        UInt16 t = (UInt16)((UInt16)(_Keys[2] & 0xffff) | 2);
        System.Byte RNTRNTRNT_175 = (byte)((t * (t ^ 1)) >> 8);
        IACSharpSensor.IACSharpSensor.SensorReached(1808);
        return RNTRNTRNT_175;
      }
    }
    public byte[] DecryptMessage(byte[] cipherText, int length)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1809);
      if (cipherText == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1810);
        throw new ArgumentNullException("cipherText");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1811);
      if (length > cipherText.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(1812);
        throw new ArgumentOutOfRangeException("length", "Bad length during Decryption: the length parameter must be smaller than or equal to the size of the destination array.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1813);
      byte[] plainText = new byte[length];
      IACSharpSensor.IACSharpSensor.SensorReached(1814);
      for (int i = 0; i < length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1815);
        byte C = (byte)(cipherText[i] ^ MagicByte);
        UpdateKeys(C);
        plainText[i] = C;
      }
      System.Byte[] RNTRNTRNT_176 = plainText;
      IACSharpSensor.IACSharpSensor.SensorReached(1816);
      return RNTRNTRNT_176;
    }
    public byte[] EncryptMessage(byte[] plainText, int length)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1817);
      if (plainText == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1818);
        throw new ArgumentNullException("plaintext");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1819);
      if (length > plainText.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(1820);
        throw new ArgumentOutOfRangeException("length", "Bad length during Encryption: The length parameter must be smaller than or equal to the size of the destination array.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1821);
      byte[] cipherText = new byte[length];
      IACSharpSensor.IACSharpSensor.SensorReached(1822);
      for (int i = 0; i < length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1823);
        byte C = plainText[i];
        cipherText[i] = (byte)(plainText[i] ^ MagicByte);
        UpdateKeys(C);
      }
      System.Byte[] RNTRNTRNT_177 = cipherText;
      IACSharpSensor.IACSharpSensor.SensorReached(1824);
      return RNTRNTRNT_177;
    }
    public void InitCipher(string passphrase)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1825);
      byte[] p = SharedUtilities.StringToByteArray(passphrase);
      IACSharpSensor.IACSharpSensor.SensorReached(1826);
      for (int i = 0; i < passphrase.Length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1827);
        UpdateKeys(p[i]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1828);
    }
    private void UpdateKeys(byte byteValue)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1829);
      _Keys[0] = (UInt32)crc32.ComputeCrc32((int)_Keys[0], byteValue);
      _Keys[1] = _Keys[1] + (byte)_Keys[0];
      _Keys[1] = _Keys[1] * 0x8088405 + 1;
      _Keys[2] = (UInt32)crc32.ComputeCrc32((int)_Keys[2], (byte)(_Keys[1] >> 24));
      IACSharpSensor.IACSharpSensor.SensorReached(1830);
    }
    private UInt32[] _Keys = {
      0x12345678,
      0x23456789,
      0x34567890
    };
    private Ionic.Crc.CRC32 crc32 = new Ionic.Crc.CRC32();
  }
  internal enum CryptoMode
  {
    Encrypt,
    Decrypt
  }
  internal class ZipCipherStream : System.IO.Stream
  {
    private ZipCrypto _cipher;
    private System.IO.Stream _s;
    private CryptoMode _mode;
    public ZipCipherStream(System.IO.Stream s, ZipCrypto cipher, CryptoMode mode) : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1831);
      _cipher = cipher;
      _s = s;
      _mode = mode;
      IACSharpSensor.IACSharpSensor.SensorReached(1832);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1833);
      if (_mode == CryptoMode.Encrypt) {
        IACSharpSensor.IACSharpSensor.SensorReached(1834);
        throw new NotSupportedException("This stream does not encrypt via Read()");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1835);
      if (buffer == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1836);
        throw new ArgumentNullException("buffer");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1837);
      byte[] db = new byte[count];
      int n = _s.Read(db, 0, count);
      byte[] decrypted = _cipher.DecryptMessage(db, n);
      IACSharpSensor.IACSharpSensor.SensorReached(1838);
      for (int i = 0; i < n; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1839);
        buffer[offset + i] = decrypted[i];
      }
      System.Int32 RNTRNTRNT_178 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(1840);
      return RNTRNTRNT_178;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1841);
      if (_mode == CryptoMode.Decrypt) {
        IACSharpSensor.IACSharpSensor.SensorReached(1842);
        throw new NotSupportedException("This stream does not Decrypt via Write()");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1843);
      if (buffer == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1844);
        throw new ArgumentNullException("buffer");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1845);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1846);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1847);
      byte[] plaintext = null;
      IACSharpSensor.IACSharpSensor.SensorReached(1848);
      if (offset != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1849);
        plaintext = new byte[count];
        IACSharpSensor.IACSharpSensor.SensorReached(1850);
        for (int i = 0; i < count; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(1851);
          plaintext[i] = buffer[offset + i];
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(1852);
        plaintext = buffer;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1853);
      byte[] encrypted = _cipher.EncryptMessage(plaintext, count);
      _s.Write(encrypted, 0, encrypted.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(1854);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_179 = (_mode == CryptoMode.Decrypt);
        IACSharpSensor.IACSharpSensor.SensorReached(1855);
        return RNTRNTRNT_179;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_180 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1856);
        return RNTRNTRNT_180;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_181 = (_mode == CryptoMode.Encrypt);
        IACSharpSensor.IACSharpSensor.SensorReached(1857);
        return RNTRNTRNT_181;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1858);
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1859);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1860);
        throw new NotSupportedException();
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1861);
        throw new NotSupportedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1862);
      throw new NotSupportedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1863);
      throw new NotSupportedException();
    }
  }
}
