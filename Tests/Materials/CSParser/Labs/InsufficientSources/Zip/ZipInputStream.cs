using System;
using System.Threading;
using System.Collections.Generic;
using System.IO;
using Ionic.Zip;
namespace Ionic.Zip
{
  public class ZipInputStream : Stream
  {
    public ZipInputStream(Stream stream) : this(stream, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4100);
    }
    public ZipInputStream(String fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4101);
      Stream stream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
      _Init(stream, false, fileName);
      IACSharpSensor.IACSharpSensor.SensorReached(4102);
    }
    public ZipInputStream(Stream stream, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4103);
      _Init(stream, leaveOpen, null);
      IACSharpSensor.IACSharpSensor.SensorReached(4104);
    }
    private void _Init(Stream stream, bool leaveOpen, string name)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4105);
      _inputStream = stream;
      IACSharpSensor.IACSharpSensor.SensorReached(4106);
      if (!_inputStream.CanRead) {
        IACSharpSensor.IACSharpSensor.SensorReached(4107);
        throw new ZipException("The stream must be readable.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4108);
      _container = new ZipContainer(this);
      _provisionalAlternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
      _leaveUnderlyingStreamOpen = leaveOpen;
      _findRequired = true;
      _name = name ?? "(stream)";
      IACSharpSensor.IACSharpSensor.SensorReached(4109);
    }
    public override String ToString()
    {
      String RNTRNTRNT_419 = String.Format("ZipInputStream::{0}(leaveOpen({1})))", _name, _leaveUnderlyingStreamOpen);
      IACSharpSensor.IACSharpSensor.SensorReached(4110);
      return RNTRNTRNT_419;
    }
    public System.Text.Encoding ProvisionalAlternateEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_420 = _provisionalAlternateEncoding;
        IACSharpSensor.IACSharpSensor.SensorReached(4111);
        return RNTRNTRNT_420;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4112);
        _provisionalAlternateEncoding = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4113);
      }
    }
    public int CodecBufferSize { get; set; }
    public String Password {
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4114);
        if (_closed) {
          IACSharpSensor.IACSharpSensor.SensorReached(4115);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(4116);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4117);
        _Password = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4118);
      }
    }
    private void SetupStream()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4119);
      _crcStream = _currentEntry.InternalOpenReader(_Password);
      _LeftToRead = _crcStream.Length;
      _needSetup = false;
      IACSharpSensor.IACSharpSensor.SensorReached(4120);
    }
    internal Stream ReadStream {
      get {
        Stream RNTRNTRNT_421 = _inputStream;
        IACSharpSensor.IACSharpSensor.SensorReached(4121);
        return RNTRNTRNT_421;
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4122);
      if (_closed) {
        IACSharpSensor.IACSharpSensor.SensorReached(4123);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4124);
        throw new System.InvalidOperationException("The stream has been closed.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4125);
      if (_needSetup) {
        IACSharpSensor.IACSharpSensor.SensorReached(4126);
        SetupStream();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4127);
      if (_LeftToRead == 0) {
        System.Int32 RNTRNTRNT_422 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(4128);
        return RNTRNTRNT_422;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4129);
      int len = (_LeftToRead > count) ? count : (int)_LeftToRead;
      int n = _crcStream.Read(buffer, offset, len);
      _LeftToRead -= n;
      IACSharpSensor.IACSharpSensor.SensorReached(4130);
      if (_LeftToRead == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4131);
        int CrcResult = _crcStream.Crc;
        _currentEntry.VerifyCrcAfterExtract(CrcResult);
        _inputStream.Seek(_endOfEntry, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_inputStream);
      }
      System.Int32 RNTRNTRNT_423 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(4132);
      return RNTRNTRNT_423;
    }
    public ZipEntry GetNextEntry()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4133);
      if (_findRequired) {
        IACSharpSensor.IACSharpSensor.SensorReached(4134);
        long d = SharedUtilities.FindSignature(_inputStream, ZipConstants.ZipEntrySignature);
        IACSharpSensor.IACSharpSensor.SensorReached(4135);
        if (d == -1) {
          IACSharpSensor.IACSharpSensor.SensorReached(4136);
          return null;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4137);
        _inputStream.Seek(-4, SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_inputStream);
      } else if (_firstEntry) {
        IACSharpSensor.IACSharpSensor.SensorReached(4138);
        _inputStream.Seek(_endOfEntry, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_inputStream);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4139);
      _currentEntry = ZipEntry.ReadEntry(_container, !_firstEntry);
      _endOfEntry = _inputStream.Position;
      _firstEntry = true;
      _needSetup = true;
      _findRequired = false;
      ZipEntry RNTRNTRNT_424 = _currentEntry;
      IACSharpSensor.IACSharpSensor.SensorReached(4140);
      return RNTRNTRNT_424;
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4141);
      if (_closed) {
        IACSharpSensor.IACSharpSensor.SensorReached(4142);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4143);
      if (disposing) {
        IACSharpSensor.IACSharpSensor.SensorReached(4144);
        if (_exceptionPending) {
          IACSharpSensor.IACSharpSensor.SensorReached(4145);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4146);
        if (!_leaveUnderlyingStreamOpen) {
          IACSharpSensor.IACSharpSensor.SensorReached(4147);
          _inputStream.Dispose();
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4148);
      _closed = true;
      IACSharpSensor.IACSharpSensor.SensorReached(4149);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_425 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4150);
        return RNTRNTRNT_425;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_426 = _inputStream.CanSeek;
        IACSharpSensor.IACSharpSensor.SensorReached(4151);
        return RNTRNTRNT_426;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_427 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(4152);
        return RNTRNTRNT_427;
      }
    }
    public override long Length {
      get {
        System.Int64 RNTRNTRNT_428 = _inputStream.Length;
        IACSharpSensor.IACSharpSensor.SensorReached(4153);
        return RNTRNTRNT_428;
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_429 = _inputStream.Position;
        IACSharpSensor.IACSharpSensor.SensorReached(4154);
        return RNTRNTRNT_429;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4155);
        Seek(value, SeekOrigin.Begin);
        IACSharpSensor.IACSharpSensor.SensorReached(4156);
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4157);
      throw new NotSupportedException("Flush");
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4158);
      throw new NotSupportedException("Write");
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4159);
      _findRequired = true;
      var x = _inputStream.Seek(offset, origin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_inputStream);
      System.Int64 RNTRNTRNT_430 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(4160);
      return RNTRNTRNT_430;
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4161);
      throw new NotSupportedException();
    }
    private Stream _inputStream;
    private System.Text.Encoding _provisionalAlternateEncoding;
    private ZipEntry _currentEntry;
    private bool _firstEntry;
    private bool _needSetup;
    private ZipContainer _container;
    private Ionic.Crc.CrcCalculatorStream _crcStream;
    private Int64 _LeftToRead;
    internal String _Password;
    private Int64 _endOfEntry;
    private string _name;
    private bool _leaveUnderlyingStreamOpen;
    private bool _closed;
    private bool _findRequired;
    private bool _exceptionPending;
  }
}
