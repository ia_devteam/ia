using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    public ZipEntry AddItem(string fileOrDirectoryName)
    {
      ZipEntry RNTRNTRNT_290 = AddItem(fileOrDirectoryName, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2962);
      return RNTRNTRNT_290;
    }
    public ZipEntry AddItem(String fileOrDirectoryName, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2963);
      if (File.Exists(fileOrDirectoryName)) {
        ZipEntry RNTRNTRNT_291 = AddFile(fileOrDirectoryName, directoryPathInArchive);
        IACSharpSensor.IACSharpSensor.SensorReached(2964);
        return RNTRNTRNT_291;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2965);
      if (Directory.Exists(fileOrDirectoryName)) {
        ZipEntry RNTRNTRNT_292 = AddDirectory(fileOrDirectoryName, directoryPathInArchive);
        IACSharpSensor.IACSharpSensor.SensorReached(2966);
        return RNTRNTRNT_292;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2967);
      throw new FileNotFoundException(String.Format("That file or directory ({0}) does not exist!", fileOrDirectoryName));
    }
    public ZipEntry AddFile(string fileName)
    {
      ZipEntry RNTRNTRNT_293 = AddFile(fileName, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2968);
      return RNTRNTRNT_293;
    }
    public ZipEntry AddFile(string fileName, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2969);
      string nameInArchive = ZipEntry.NameInArchive(fileName, directoryPathInArchive);
      ZipEntry ze = ZipEntry.CreateFromFile(fileName, nameInArchive);
      IACSharpSensor.IACSharpSensor.SensorReached(2970);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(2971);
        StatusMessageTextWriter.WriteLine("adding {0}...", fileName);
      }
      ZipEntry RNTRNTRNT_294 = _InternalAddEntry(ze);
      IACSharpSensor.IACSharpSensor.SensorReached(2972);
      return RNTRNTRNT_294;
    }
    public void RemoveEntries(System.Collections.Generic.ICollection<ZipEntry> entriesToRemove)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2973);
      if (entriesToRemove == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2974);
        throw new ArgumentNullException("entriesToRemove");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2975);
      foreach (ZipEntry e in entriesToRemove) {
        IACSharpSensor.IACSharpSensor.SensorReached(2976);
        this.RemoveEntry(e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2977);
    }
    public void RemoveEntries(System.Collections.Generic.ICollection<String> entriesToRemove)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2978);
      if (entriesToRemove == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2979);
        throw new ArgumentNullException("entriesToRemove");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2980);
      foreach (String e in entriesToRemove) {
        IACSharpSensor.IACSharpSensor.SensorReached(2981);
        this.RemoveEntry(e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2982);
    }
    public void AddFiles(System.Collections.Generic.IEnumerable<String> fileNames)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2983);
      this.AddFiles(fileNames, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2984);
    }
    public void UpdateFiles(System.Collections.Generic.IEnumerable<String> fileNames)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2985);
      this.UpdateFiles(fileNames, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2986);
    }
    public void AddFiles(System.Collections.Generic.IEnumerable<String> fileNames, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2987);
      AddFiles(fileNames, false, directoryPathInArchive);
      IACSharpSensor.IACSharpSensor.SensorReached(2988);
    }
    public void AddFiles(System.Collections.Generic.IEnumerable<String> fileNames, bool preserveDirHierarchy, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2989);
      if (fileNames == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2990);
        throw new ArgumentNullException("fileNames");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2991);
      _addOperationCanceled = false;
      OnAddStarted();
      IACSharpSensor.IACSharpSensor.SensorReached(2992);
      if (preserveDirHierarchy) {
        IACSharpSensor.IACSharpSensor.SensorReached(2993);
        foreach (var f in fileNames) {
          IACSharpSensor.IACSharpSensor.SensorReached(2994);
          if (_addOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(2995);
            break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2996);
          if (directoryPathInArchive != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2997);
            string s = Path.GetFullPath(Path.Combine(directoryPathInArchive, Path.GetDirectoryName(f)));
            this.AddFile(f, s);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2998);
            this.AddFile(f, null);
          }
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2999);
        foreach (var f in fileNames) {
          IACSharpSensor.IACSharpSensor.SensorReached(3000);
          if (_addOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(3001);
            break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3002);
          this.AddFile(f, directoryPathInArchive);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3003);
      if (!_addOperationCanceled) {
        IACSharpSensor.IACSharpSensor.SensorReached(3004);
        OnAddCompleted();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3005);
    }
    public void UpdateFiles(System.Collections.Generic.IEnumerable<String> fileNames, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3006);
      if (fileNames == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3007);
        throw new ArgumentNullException("fileNames");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3008);
      OnAddStarted();
      IACSharpSensor.IACSharpSensor.SensorReached(3009);
      foreach (var f in fileNames) {
        IACSharpSensor.IACSharpSensor.SensorReached(3010);
        this.UpdateFile(f, directoryPathInArchive);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3011);
      OnAddCompleted();
      IACSharpSensor.IACSharpSensor.SensorReached(3012);
    }
    public ZipEntry UpdateFile(string fileName)
    {
      ZipEntry RNTRNTRNT_295 = UpdateFile(fileName, null);
      IACSharpSensor.IACSharpSensor.SensorReached(3013);
      return RNTRNTRNT_295;
    }
    public ZipEntry UpdateFile(string fileName, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3014);
      var key = ZipEntry.NameInArchive(fileName, directoryPathInArchive);
      IACSharpSensor.IACSharpSensor.SensorReached(3015);
      if (this[key] != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3016);
        this.RemoveEntry(key);
      }
      ZipEntry RNTRNTRNT_296 = this.AddFile(fileName, directoryPathInArchive);
      IACSharpSensor.IACSharpSensor.SensorReached(3017);
      return RNTRNTRNT_296;
    }
    public ZipEntry UpdateDirectory(string directoryName)
    {
      ZipEntry RNTRNTRNT_297 = UpdateDirectory(directoryName, null);
      IACSharpSensor.IACSharpSensor.SensorReached(3018);
      return RNTRNTRNT_297;
    }
    public ZipEntry UpdateDirectory(string directoryName, String directoryPathInArchive)
    {
      ZipEntry RNTRNTRNT_298 = this.AddOrUpdateDirectoryImpl(directoryName, directoryPathInArchive, AddOrUpdateAction.AddOrUpdate);
      IACSharpSensor.IACSharpSensor.SensorReached(3019);
      return RNTRNTRNT_298;
    }
    public void UpdateItem(string itemName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3020);
      UpdateItem(itemName, null);
      IACSharpSensor.IACSharpSensor.SensorReached(3021);
    }
    public void UpdateItem(string itemName, string directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3022);
      if (File.Exists(itemName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3023);
        UpdateFile(itemName, directoryPathInArchive);
      } else if (Directory.Exists(itemName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3025);
        UpdateDirectory(itemName, directoryPathInArchive);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(3024);
        throw new FileNotFoundException(String.Format("That file or directory ({0}) does not exist!", itemName));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3026);
    }
    public ZipEntry AddEntry(string entryName, string content)
    {
      ZipEntry RNTRNTRNT_299 = AddEntry(entryName, content, System.Text.Encoding.Default);
      IACSharpSensor.IACSharpSensor.SensorReached(3027);
      return RNTRNTRNT_299;
    }
    public ZipEntry AddEntry(string entryName, string content, System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3028);
      var ms = new MemoryStream();
      var sw = new StreamWriter(ms, encoding);
      sw.Write(content);
      sw.Flush();
      ms.Seek(0, SeekOrigin.Begin);
      ZipEntry RNTRNTRNT_300 = AddEntry(entryName, ms);
      IACSharpSensor.IACSharpSensor.SensorReached(3029);
      return RNTRNTRNT_300;
    }
    public ZipEntry AddEntry(string entryName, Stream stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3030);
      ZipEntry ze = ZipEntry.CreateForStream(entryName, stream);
      ze.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      IACSharpSensor.IACSharpSensor.SensorReached(3031);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(3032);
        StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
      }
      ZipEntry RNTRNTRNT_301 = _InternalAddEntry(ze);
      IACSharpSensor.IACSharpSensor.SensorReached(3033);
      return RNTRNTRNT_301;
    }
    public ZipEntry AddEntry(string entryName, WriteDelegate writer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3034);
      ZipEntry ze = ZipEntry.CreateForWriter(entryName, writer);
      IACSharpSensor.IACSharpSensor.SensorReached(3035);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(3036);
        StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
      }
      ZipEntry RNTRNTRNT_302 = _InternalAddEntry(ze);
      IACSharpSensor.IACSharpSensor.SensorReached(3037);
      return RNTRNTRNT_302;
    }
    public ZipEntry AddEntry(string entryName, OpenDelegate opener, CloseDelegate closer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3038);
      ZipEntry ze = ZipEntry.CreateForJitStreamProvider(entryName, opener, closer);
      ze.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      IACSharpSensor.IACSharpSensor.SensorReached(3039);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(3040);
        StatusMessageTextWriter.WriteLine("adding {0}...", entryName);
      }
      ZipEntry RNTRNTRNT_303 = _InternalAddEntry(ze);
      IACSharpSensor.IACSharpSensor.SensorReached(3041);
      return RNTRNTRNT_303;
    }
    private ZipEntry _InternalAddEntry(ZipEntry ze)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3042);
      ze._container = new ZipContainer(this);
      ze.CompressionMethod = this.CompressionMethod;
      ze.CompressionLevel = this.CompressionLevel;
      ze.ExtractExistingFile = this.ExtractExistingFile;
      ze.ZipErrorAction = this.ZipErrorAction;
      ze.SetCompression = this.SetCompression;
      ze.AlternateEncoding = this.AlternateEncoding;
      ze.AlternateEncodingUsage = this.AlternateEncodingUsage;
      ze.Password = this._Password;
      ze.Encryption = this.Encryption;
      ze.EmitTimesInWindowsFormatWhenSaving = this._emitNtfsTimes;
      ze.EmitTimesInUnixFormatWhenSaving = this._emitUnixTimes;
      InternalAddEntry(ze.FileName, ze);
      AfterAddEntry(ze);
      ZipEntry RNTRNTRNT_304 = ze;
      IACSharpSensor.IACSharpSensor.SensorReached(3043);
      return RNTRNTRNT_304;
    }
    public ZipEntry UpdateEntry(string entryName, string content)
    {
      ZipEntry RNTRNTRNT_305 = UpdateEntry(entryName, content, System.Text.Encoding.Default);
      IACSharpSensor.IACSharpSensor.SensorReached(3044);
      return RNTRNTRNT_305;
    }
    public ZipEntry UpdateEntry(string entryName, string content, System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3045);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_306 = AddEntry(entryName, content, encoding);
      IACSharpSensor.IACSharpSensor.SensorReached(3046);
      return RNTRNTRNT_306;
    }
    public ZipEntry UpdateEntry(string entryName, WriteDelegate writer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3047);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_307 = AddEntry(entryName, writer);
      IACSharpSensor.IACSharpSensor.SensorReached(3048);
      return RNTRNTRNT_307;
    }
    public ZipEntry UpdateEntry(string entryName, OpenDelegate opener, CloseDelegate closer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3049);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_308 = AddEntry(entryName, opener, closer);
      IACSharpSensor.IACSharpSensor.SensorReached(3050);
      return RNTRNTRNT_308;
    }
    public ZipEntry UpdateEntry(string entryName, Stream stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3051);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_309 = AddEntry(entryName, stream);
      IACSharpSensor.IACSharpSensor.SensorReached(3052);
      return RNTRNTRNT_309;
    }
    private void RemoveEntryForUpdate(string entryName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3053);
      if (String.IsNullOrEmpty(entryName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3054);
        throw new ArgumentNullException("entryName");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3055);
      string directoryPathInArchive = null;
      IACSharpSensor.IACSharpSensor.SensorReached(3056);
      if (entryName.IndexOf('\\') != -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(3057);
        directoryPathInArchive = Path.GetDirectoryName(entryName);
        entryName = Path.GetFileName(entryName);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3058);
      var key = ZipEntry.NameInArchive(entryName, directoryPathInArchive);
      IACSharpSensor.IACSharpSensor.SensorReached(3059);
      if (this[key] != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3060);
        this.RemoveEntry(key);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3061);
    }
    public ZipEntry AddEntry(string entryName, byte[] byteContent)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3062);
      if (byteContent == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3063);
        throw new ArgumentException("bad argument", "byteContent");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3064);
      var ms = new MemoryStream(byteContent);
      ZipEntry RNTRNTRNT_310 = AddEntry(entryName, ms);
      IACSharpSensor.IACSharpSensor.SensorReached(3065);
      return RNTRNTRNT_310;
    }
    public ZipEntry UpdateEntry(string entryName, byte[] byteContent)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3066);
      RemoveEntryForUpdate(entryName);
      ZipEntry RNTRNTRNT_311 = AddEntry(entryName, byteContent);
      IACSharpSensor.IACSharpSensor.SensorReached(3067);
      return RNTRNTRNT_311;
    }
    public ZipEntry AddDirectory(string directoryName)
    {
      ZipEntry RNTRNTRNT_312 = AddDirectory(directoryName, null);
      IACSharpSensor.IACSharpSensor.SensorReached(3068);
      return RNTRNTRNT_312;
    }
    public ZipEntry AddDirectory(string directoryName, string directoryPathInArchive)
    {
      ZipEntry RNTRNTRNT_313 = AddOrUpdateDirectoryImpl(directoryName, directoryPathInArchive, AddOrUpdateAction.AddOnly);
      IACSharpSensor.IACSharpSensor.SensorReached(3069);
      return RNTRNTRNT_313;
    }
    public ZipEntry AddDirectoryByName(string directoryNameInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3070);
      ZipEntry dir = ZipEntry.CreateFromNothing(directoryNameInArchive);
      dir._container = new ZipContainer(this);
      dir.MarkAsDirectory();
      dir.AlternateEncoding = this.AlternateEncoding;
      dir.AlternateEncodingUsage = this.AlternateEncodingUsage;
      dir.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      dir.EmitTimesInWindowsFormatWhenSaving = _emitNtfsTimes;
      dir.EmitTimesInUnixFormatWhenSaving = _emitUnixTimes;
      dir._Source = ZipEntrySource.Stream;
      InternalAddEntry(dir.FileName, dir);
      AfterAddEntry(dir);
      ZipEntry RNTRNTRNT_314 = dir;
      IACSharpSensor.IACSharpSensor.SensorReached(3071);
      return RNTRNTRNT_314;
    }
    private ZipEntry AddOrUpdateDirectoryImpl(string directoryName, string rootDirectoryPathInArchive, AddOrUpdateAction action)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3072);
      if (rootDirectoryPathInArchive == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3073);
        rootDirectoryPathInArchive = "";
      }
      ZipEntry RNTRNTRNT_315 = AddOrUpdateDirectoryImpl(directoryName, rootDirectoryPathInArchive, action, true, 0);
      IACSharpSensor.IACSharpSensor.SensorReached(3074);
      return RNTRNTRNT_315;
    }
    internal void InternalAddEntry(String name, ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3075);
      _entries.Add(name, entry);
      _zipEntriesAsList = null;
      _contentsChanged = true;
      IACSharpSensor.IACSharpSensor.SensorReached(3076);
    }
    private ZipEntry AddOrUpdateDirectoryImpl(string directoryName, string rootDirectoryPathInArchive, AddOrUpdateAction action, bool recurse, int level)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3077);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(3078);
        StatusMessageTextWriter.WriteLine("{0} {1}...", (action == AddOrUpdateAction.AddOnly) ? "adding" : "Adding or updating", directoryName);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3079);
      if (level == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3080);
        _addOperationCanceled = false;
        OnAddStarted();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3081);
      if (_addOperationCanceled) {
        IACSharpSensor.IACSharpSensor.SensorReached(3082);
        return null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3083);
      string dirForEntries = rootDirectoryPathInArchive;
      ZipEntry baseDir = null;
      IACSharpSensor.IACSharpSensor.SensorReached(3084);
      if (level > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3085);
        int f = directoryName.Length;
        IACSharpSensor.IACSharpSensor.SensorReached(3086);
        for (int i = level; i > 0; i--) {
          IACSharpSensor.IACSharpSensor.SensorReached(3087);
          f = directoryName.LastIndexOfAny("/\\".ToCharArray(), f - 1, f - 1);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3088);
        dirForEntries = directoryName.Substring(f + 1);
        dirForEntries = Path.Combine(rootDirectoryPathInArchive, dirForEntries);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3089);
      if (level > 0 || rootDirectoryPathInArchive != "") {
        IACSharpSensor.IACSharpSensor.SensorReached(3090);
        baseDir = ZipEntry.CreateFromFile(directoryName, dirForEntries);
        baseDir._container = new ZipContainer(this);
        baseDir.AlternateEncoding = this.AlternateEncoding;
        baseDir.AlternateEncodingUsage = this.AlternateEncodingUsage;
        baseDir.MarkAsDirectory();
        baseDir.EmitTimesInWindowsFormatWhenSaving = _emitNtfsTimes;
        baseDir.EmitTimesInUnixFormatWhenSaving = _emitUnixTimes;
        IACSharpSensor.IACSharpSensor.SensorReached(3091);
        if (!_entries.ContainsKey(baseDir.FileName)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3092);
          InternalAddEntry(baseDir.FileName, baseDir);
          AfterAddEntry(baseDir);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3093);
        dirForEntries = baseDir.FileName;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3094);
      if (!_addOperationCanceled) {
        IACSharpSensor.IACSharpSensor.SensorReached(3095);
        String[] filenames = Directory.GetFiles(directoryName);
        IACSharpSensor.IACSharpSensor.SensorReached(3096);
        if (recurse) {
          IACSharpSensor.IACSharpSensor.SensorReached(3097);
          foreach (String filename in filenames) {
            IACSharpSensor.IACSharpSensor.SensorReached(3098);
            if (_addOperationCanceled) {
              IACSharpSensor.IACSharpSensor.SensorReached(3099);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3100);
            if (action == AddOrUpdateAction.AddOnly) {
              IACSharpSensor.IACSharpSensor.SensorReached(3101);
              AddFile(filename, dirForEntries);
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(3102);
              UpdateFile(filename, dirForEntries);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3103);
          if (!_addOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(3104);
            String[] dirnames = Directory.GetDirectories(directoryName);
            IACSharpSensor.IACSharpSensor.SensorReached(3105);
            foreach (String dir in dirnames) {
              IACSharpSensor.IACSharpSensor.SensorReached(3106);
              FileAttributes fileAttrs = System.IO.File.GetAttributes(dir);
              IACSharpSensor.IACSharpSensor.SensorReached(3107);
              if (this.AddDirectoryWillTraverseReparsePoints || ((fileAttrs & FileAttributes.ReparsePoint) == 0)) {
                IACSharpSensor.IACSharpSensor.SensorReached(3108);
                AddOrUpdateDirectoryImpl(dir, rootDirectoryPathInArchive, action, recurse, level + 1);
              }
            }
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3109);
      if (level == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3110);
        OnAddCompleted();
      }
      ZipEntry RNTRNTRNT_316 = baseDir;
      IACSharpSensor.IACSharpSensor.SensorReached(3111);
      return RNTRNTRNT_316;
    }
  }
}
