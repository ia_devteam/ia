using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    public void ExtractAll(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3498);
      _InternalExtractAll(path, true);
      IACSharpSensor.IACSharpSensor.SensorReached(3499);
    }
    public void ExtractAll(string path, ExtractExistingFileAction extractExistingFile)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3500);
      ExtractExistingFile = extractExistingFile;
      _InternalExtractAll(path, true);
      IACSharpSensor.IACSharpSensor.SensorReached(3501);
    }
    private void _InternalExtractAll(string path, bool overrideExtractExistingProperty)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3502);
      bool header = Verbose;
      _inExtractAll = true;
      IACSharpSensor.IACSharpSensor.SensorReached(3503);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3504);
        OnExtractAllStarted(path);
        int n = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(3505);
        foreach (ZipEntry e in _entries.Values) {
          IACSharpSensor.IACSharpSensor.SensorReached(3506);
          if (header) {
            IACSharpSensor.IACSharpSensor.SensorReached(3507);
            StatusMessageTextWriter.WriteLine("\n{1,-22} {2,-8} {3,4}   {4,-8}  {0}", "Name", "Modified", "Size", "Ratio", "Packed");
            StatusMessageTextWriter.WriteLine(new System.String('-', 72));
            header = false;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3508);
          if (Verbose) {
            IACSharpSensor.IACSharpSensor.SensorReached(3509);
            StatusMessageTextWriter.WriteLine("{1,-22} {2,-8} {3,4:F0}%   {4,-8} {0}", e.FileName, e.LastModified.ToString("yyyy-MM-dd HH:mm:ss"), e.UncompressedSize, e.CompressionRatio, e.CompressedSize);
            IACSharpSensor.IACSharpSensor.SensorReached(3510);
            if (!String.IsNullOrEmpty(e.Comment)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3511);
              StatusMessageTextWriter.WriteLine("  Comment: {0}", e.Comment);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3512);
          e.Password = _Password;
          OnExtractEntry(n, true, e, path);
          IACSharpSensor.IACSharpSensor.SensorReached(3513);
          if (overrideExtractExistingProperty) {
            IACSharpSensor.IACSharpSensor.SensorReached(3514);
            e.ExtractExistingFile = this.ExtractExistingFile;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3515);
          e.Extract(path);
          n++;
          OnExtractEntry(n, false, e, path);
          IACSharpSensor.IACSharpSensor.SensorReached(3516);
          if (_extractOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(3517);
            break;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3518);
        if (!_extractOperationCanceled) {
          IACSharpSensor.IACSharpSensor.SensorReached(3519);
          foreach (ZipEntry e in _entries.Values) {
            IACSharpSensor.IACSharpSensor.SensorReached(3520);
            if ((e.IsDirectory) || (e.FileName.EndsWith("/"))) {
              IACSharpSensor.IACSharpSensor.SensorReached(3521);
              string outputFile = (e.FileName.StartsWith("/")) ? Path.Combine(path, e.FileName.Substring(1)) : Path.Combine(path, e.FileName);
              e._SetTimes(outputFile, false);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3522);
          OnExtractAllCompleted(path);
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(3523);
        _inExtractAll = false;
        IACSharpSensor.IACSharpSensor.SensorReached(3524);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3525);
    }
  }
}
