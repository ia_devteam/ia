using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
namespace Ionic.Zip
{
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000B")]
  public class BadPasswordException : ZipException
  {
    public BadPasswordException()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1258);
    }
    public BadPasswordException(String message) : base(message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1259);
    }
    public BadPasswordException(String message, Exception innerException) : base(message, innerException)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1260);
    }
    protected BadPasswordException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1261);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000A")]
  public class BadReadException : ZipException
  {
    public BadReadException()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1262);
    }
    public BadReadException(String message) : base(message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1263);
    }
    public BadReadException(String message, Exception innerException) : base(message, innerException)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1264);
    }
    protected BadReadException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1265);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00009")]
  public class BadCrcException : ZipException
  {
    public BadCrcException()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1266);
    }
    public BadCrcException(String message) : base(message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1267);
    }
    protected BadCrcException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1268);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00008")]
  public class SfxGenerationException : ZipException
  {
    public SfxGenerationException()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1269);
    }
    public SfxGenerationException(String message) : base(message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1270);
    }
    protected SfxGenerationException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1271);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00007")]
  public class BadStateException : ZipException
  {
    public BadStateException()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1272);
    }
    public BadStateException(String message) : base(message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1273);
    }
    public BadStateException(String message, Exception innerException) : base(message, innerException)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1274);
    }
    protected BadStateException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1275);
    }
  }
  [Serializable()]
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00006")]
  public class ZipException : Exception
  {
    public ZipException()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1276);
    }
    public ZipException(String message) : base(message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1277);
    }
    public ZipException(String message, Exception innerException) : base(message, innerException)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1278);
    }
    protected ZipException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1279);
    }
  }
}
