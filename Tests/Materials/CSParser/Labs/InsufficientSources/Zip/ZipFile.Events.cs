using System;
using System.IO;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    private string ArchiveNameForEvent {
      get {
        System.String RNTRNTRNT_370 = (_name != null) ? _name : "(stream)";
        IACSharpSensor.IACSharpSensor.SensorReached(3397);
        return RNTRNTRNT_370;
      }
    }
    public event EventHandler<SaveProgressEventArgs> SaveProgress;
    internal bool OnSaveBlock(ZipEntry entry, Int64 bytesXferred, Int64 totalBytesToXfer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3398);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3399);
      if (sp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3400);
        var e = SaveProgressEventArgs.ByteUpdate(ArchiveNameForEvent, entry, bytesXferred, totalBytesToXfer);
        sp(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(3401);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(3402);
          _saveOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_371 = _saveOperationCanceled;
      IACSharpSensor.IACSharpSensor.SensorReached(3403);
      return RNTRNTRNT_371;
    }
    private void OnSaveEntry(int current, ZipEntry entry, bool before)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3404);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3405);
      if (sp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3406);
        var e = new SaveProgressEventArgs(ArchiveNameForEvent, before, _entries.Count, current, entry);
        sp(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(3407);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(3408);
          _saveOperationCanceled = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3409);
    }
    private void OnSaveEvent(ZipProgressEventType eventFlavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3410);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3411);
      if (sp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3412);
        var e = new SaveProgressEventArgs(ArchiveNameForEvent, eventFlavor);
        sp(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(3413);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(3414);
          _saveOperationCanceled = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3415);
    }
    private void OnSaveStarted()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3416);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3417);
      if (sp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3418);
        var e = SaveProgressEventArgs.Started(ArchiveNameForEvent);
        sp(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(3419);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(3420);
          _saveOperationCanceled = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3421);
    }
    private void OnSaveCompleted()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3422);
      EventHandler<SaveProgressEventArgs> sp = SaveProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3423);
      if (sp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3424);
        var e = SaveProgressEventArgs.Completed(ArchiveNameForEvent);
        sp(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3425);
    }
    public event EventHandler<ReadProgressEventArgs> ReadProgress;
    private void OnReadStarted()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3426);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3427);
      if (rp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3428);
        var e = ReadProgressEventArgs.Started(ArchiveNameForEvent);
        rp(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3429);
    }
    private void OnReadCompleted()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3430);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3431);
      if (rp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3432);
        var e = ReadProgressEventArgs.Completed(ArchiveNameForEvent);
        rp(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3433);
    }
    internal void OnReadBytes(ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3434);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3435);
      if (rp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3436);
        var e = ReadProgressEventArgs.ByteUpdate(ArchiveNameForEvent, entry, ReadStream.Position, LengthOfReadStream);
        rp(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3437);
    }
    internal void OnReadEntry(bool before, ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3438);
      EventHandler<ReadProgressEventArgs> rp = ReadProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3439);
      if (rp != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3440);
        ReadProgressEventArgs e = (before) ? ReadProgressEventArgs.Before(ArchiveNameForEvent, _entries.Count) : ReadProgressEventArgs.After(ArchiveNameForEvent, entry, _entries.Count);
        rp(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3441);
    }
    private Int64 _lengthOfReadStream = -99;
    private Int64 LengthOfReadStream {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(3442);
        if (_lengthOfReadStream == -99) {
          IACSharpSensor.IACSharpSensor.SensorReached(3443);
          _lengthOfReadStream = (_ReadStreamIsOurs) ? SharedUtilities.GetFileLength(_name) : -1L;
        }
        Int64 RNTRNTRNT_372 = _lengthOfReadStream;
        IACSharpSensor.IACSharpSensor.SensorReached(3444);
        return RNTRNTRNT_372;
      }
    }
    public event EventHandler<ExtractProgressEventArgs> ExtractProgress;
    private void OnExtractEntry(int current, bool before, ZipEntry currentEntry, string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3445);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3446);
      if (ep != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3447);
        var e = new ExtractProgressEventArgs(ArchiveNameForEvent, before, _entries.Count, current, currentEntry, path);
        ep(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(3448);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(3449);
          _extractOperationCanceled = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3450);
    }
    internal bool OnExtractBlock(ZipEntry entry, Int64 bytesWritten, Int64 totalBytesToWrite)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3451);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3452);
      if (ep != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3453);
        var e = ExtractProgressEventArgs.ByteUpdate(ArchiveNameForEvent, entry, bytesWritten, totalBytesToWrite);
        ep(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(3454);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(3455);
          _extractOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_373 = _extractOperationCanceled;
      IACSharpSensor.IACSharpSensor.SensorReached(3456);
      return RNTRNTRNT_373;
    }
    internal bool OnSingleEntryExtract(ZipEntry entry, string path, bool before)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3457);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3458);
      if (ep != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3459);
        var e = (before) ? ExtractProgressEventArgs.BeforeExtractEntry(ArchiveNameForEvent, entry, path) : ExtractProgressEventArgs.AfterExtractEntry(ArchiveNameForEvent, entry, path);
        ep(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(3460);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(3461);
          _extractOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_374 = _extractOperationCanceled;
      IACSharpSensor.IACSharpSensor.SensorReached(3462);
      return RNTRNTRNT_374;
    }
    internal bool OnExtractExisting(ZipEntry entry, string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3463);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3464);
      if (ep != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3465);
        var e = ExtractProgressEventArgs.ExtractExisting(ArchiveNameForEvent, entry, path);
        ep(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(3466);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(3467);
          _extractOperationCanceled = true;
        }
      }
      System.Boolean RNTRNTRNT_375 = _extractOperationCanceled;
      IACSharpSensor.IACSharpSensor.SensorReached(3468);
      return RNTRNTRNT_375;
    }
    private void OnExtractAllCompleted(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3469);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3470);
      if (ep != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3471);
        var e = ExtractProgressEventArgs.ExtractAllCompleted(ArchiveNameForEvent, path);
        ep(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3472);
    }
    private void OnExtractAllStarted(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3473);
      EventHandler<ExtractProgressEventArgs> ep = ExtractProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3474);
      if (ep != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3475);
        var e = ExtractProgressEventArgs.ExtractAllStarted(ArchiveNameForEvent, path);
        ep(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3476);
    }
    public event EventHandler<AddProgressEventArgs> AddProgress;
    private void OnAddStarted()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3477);
      EventHandler<AddProgressEventArgs> ap = AddProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3478);
      if (ap != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3479);
        var e = AddProgressEventArgs.Started(ArchiveNameForEvent);
        ap(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(3480);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(3481);
          _addOperationCanceled = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3482);
    }
    private void OnAddCompleted()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3483);
      EventHandler<AddProgressEventArgs> ap = AddProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3484);
      if (ap != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3485);
        var e = AddProgressEventArgs.Completed(ArchiveNameForEvent);
        ap(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3486);
    }
    internal void AfterAddEntry(ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3487);
      EventHandler<AddProgressEventArgs> ap = AddProgress;
      IACSharpSensor.IACSharpSensor.SensorReached(3488);
      if (ap != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3489);
        var e = AddProgressEventArgs.AfterEntry(ArchiveNameForEvent, entry, _entries.Count);
        ap(this, e);
        IACSharpSensor.IACSharpSensor.SensorReached(3490);
        if (e.Cancel) {
          IACSharpSensor.IACSharpSensor.SensorReached(3491);
          _addOperationCanceled = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3492);
    }
    public event EventHandler<ZipErrorEventArgs> ZipError;
    internal bool OnZipErrorSaving(ZipEntry entry, Exception exc)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3493);
      if (ZipError != null) {
        lock (LOCK) {
          IACSharpSensor.IACSharpSensor.SensorReached(3494);
          var e = ZipErrorEventArgs.Saving(this.Name, entry, exc);
          ZipError(this, e);
          IACSharpSensor.IACSharpSensor.SensorReached(3495);
          if (e.Cancel) {
            IACSharpSensor.IACSharpSensor.SensorReached(3496);
            _saveOperationCanceled = true;
          }
        }
      }
      System.Boolean RNTRNTRNT_376 = _saveOperationCanceled;
      IACSharpSensor.IACSharpSensor.SensorReached(3497);
      return RNTRNTRNT_376;
    }
  }
}
