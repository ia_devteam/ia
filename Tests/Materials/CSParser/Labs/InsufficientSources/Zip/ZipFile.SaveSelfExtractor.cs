using System;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public enum SelfExtractorFlavor
  {
    ConsoleApplication = 0,
    WinFormsApplication
  }
  public class SelfExtractorSaveOptions
  {
    public SelfExtractorFlavor Flavor { get; set; }
    public String PostExtractCommandLine { get; set; }
    public String DefaultExtractDirectory { get; set; }
    public string IconFile { get; set; }
    public bool Quiet { get; set; }
    public Ionic.Zip.ExtractExistingFileAction ExtractExistingFile { get; set; }
    public bool RemoveUnpackedFilesAfterExecute { get; set; }
    public Version FileVersion { get; set; }
    public String ProductVersion { get; set; }
    public String Copyright { get; set; }
    public String Description { get; set; }
    public String ProductName { get; set; }
    public String SfxExeWindowTitle { get; set; }
    public string AdditionalCompilerSwitches { get; set; }
  }
  partial class ZipFile
  {
    class ExtractorSettings
    {
      public SelfExtractorFlavor Flavor;
      public List<string> ReferencedAssemblies;
      public List<string> CopyThroughResources;
      public List<string> ResourcesToCompile;
    }
    private static ExtractorSettings[] SettingsList = {
      new ExtractorSettings {
        Flavor = SelfExtractorFlavor.WinFormsApplication,
        ReferencedAssemblies = new List<string> {
          "System.dll",
          "System.Windows.Forms.dll",
          "System.Drawing.dll"
        },
        CopyThroughResources = new List<string> {
          "Ionic.Zip.WinFormsSelfExtractorStub.resources",
          "Ionic.Zip.Forms.PasswordDialog.resources",
          "Ionic.Zip.Forms.ZipContentsDialog.resources"
        },
        ResourcesToCompile = new List<string> {
          "WinFormsSelfExtractorStub.cs",
          "WinFormsSelfExtractorStub.Designer.cs",
          "PasswordDialog.cs",
          "PasswordDialog.Designer.cs",
          "ZipContentsDialog.cs",
          "ZipContentsDialog.Designer.cs",
          "FolderBrowserDialogEx.cs"
        }
      },
      new ExtractorSettings {
        Flavor = SelfExtractorFlavor.ConsoleApplication,
        ReferencedAssemblies = new List<string> { "System.dll" },
        CopyThroughResources = null,
        ResourcesToCompile = new List<string> { "CommandLineSelfExtractorStub.cs" }
      }
    };
    public void SaveSelfExtractor(string exeToGenerate, SelfExtractorFlavor flavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3848);
      SelfExtractorSaveOptions options = new SelfExtractorSaveOptions();
      options.Flavor = flavor;
      SaveSelfExtractor(exeToGenerate, options);
      IACSharpSensor.IACSharpSensor.SensorReached(3849);
    }
    public void SaveSelfExtractor(string exeToGenerate, SelfExtractorSaveOptions options)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3850);
      if (_name == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3851);
        _writestream = null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3852);
      _SavingSfx = true;
      _name = exeToGenerate;
      IACSharpSensor.IACSharpSensor.SensorReached(3853);
      if (Directory.Exists(_name)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3854);
        throw new ZipException("Bad Directory", new System.ArgumentException("That name specifies an existing directory. Please specify a filename.", "exeToGenerate"));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3855);
      _contentsChanged = true;
      _fileAlreadyExists = File.Exists(_name);
      _SaveSfxStub(exeToGenerate, options);
      Save();
      _SavingSfx = false;
      IACSharpSensor.IACSharpSensor.SensorReached(3856);
    }
    private static void ExtractResourceToFile(Assembly a, string resourceName, string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3857);
      int n = 0;
      byte[] bytes = new byte[1024];
      IACSharpSensor.IACSharpSensor.SensorReached(3858);
      using (Stream instream = a.GetManifestResourceStream(resourceName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3859);
        if (instream == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3860);
          throw new ZipException(String.Format("missing resource '{0}'", resourceName));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3861);
        using (FileStream outstream = File.OpenWrite(filename)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3862);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(3863);
            n = instream.Read(bytes, 0, bytes.Length);
            outstream.Write(bytes, 0, n);
          } while (n > 0);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3864);
    }
    private void _SaveSfxStub(string exeToGenerate, SelfExtractorSaveOptions options)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3865);
      string nameOfIconFile = null;
      string stubExe = null;
      string unpackedResourceDir = null;
      string tmpDir = null;
      IACSharpSensor.IACSharpSensor.SensorReached(3866);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3867);
        if (File.Exists(exeToGenerate)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3868);
          if (Verbose) {
            IACSharpSensor.IACSharpSensor.SensorReached(3869);
            StatusMessageTextWriter.WriteLine("The existing file ({0}) will be overwritten.", exeToGenerate);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3870);
        if (!exeToGenerate.EndsWith(".exe")) {
          IACSharpSensor.IACSharpSensor.SensorReached(3871);
          if (Verbose) {
            IACSharpSensor.IACSharpSensor.SensorReached(3872);
            StatusMessageTextWriter.WriteLine("Warning: The generated self-extracting file will not have an .exe extension.");
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3873);
        tmpDir = TempFileFolder ?? Path.GetDirectoryName(exeToGenerate);
        stubExe = GenerateTempPathname(tmpDir, "exe");
        Assembly a1 = typeof(ZipFile).Assembly;
        IACSharpSensor.IACSharpSensor.SensorReached(3874);
        using (var csharp = new Microsoft.CSharp.CSharpCodeProvider()) {
          IACSharpSensor.IACSharpSensor.SensorReached(3875);
          ExtractorSettings settings = null;
          IACSharpSensor.IACSharpSensor.SensorReached(3876);
          foreach (var x in SettingsList) {
            IACSharpSensor.IACSharpSensor.SensorReached(3877);
            if (x.Flavor == options.Flavor) {
              IACSharpSensor.IACSharpSensor.SensorReached(3878);
              settings = x;
              IACSharpSensor.IACSharpSensor.SensorReached(3879);
              break;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3880);
          if (settings == null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3881);
            throw new BadStateException(String.Format("While saving a Self-Extracting Zip, Cannot find that flavor ({0})?", options.Flavor));
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3882);
          var cp = new System.CodeDom.Compiler.CompilerParameters();
          cp.ReferencedAssemblies.Add(a1.Location);
          IACSharpSensor.IACSharpSensor.SensorReached(3883);
          if (settings.ReferencedAssemblies != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3884);
            foreach (string ra in settings.ReferencedAssemblies) {
              IACSharpSensor.IACSharpSensor.SensorReached(3885);
              cp.ReferencedAssemblies.Add(ra);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3886);
          cp.GenerateInMemory = false;
          cp.GenerateExecutable = true;
          cp.IncludeDebugInformation = false;
          cp.CompilerOptions = "";
          Assembly a2 = Assembly.GetExecutingAssembly();
          var sb = new System.Text.StringBuilder();
          string sourceFile = GenerateTempPathname(tmpDir, "cs");
          IACSharpSensor.IACSharpSensor.SensorReached(3887);
          using (ZipFile zip = ZipFile.Read(a2.GetManifestResourceStream("Ionic.Zip.Resources.ZippedResources.zip"))) {
            IACSharpSensor.IACSharpSensor.SensorReached(3888);
            unpackedResourceDir = GenerateTempPathname(tmpDir, "tmp");
            IACSharpSensor.IACSharpSensor.SensorReached(3889);
            if (String.IsNullOrEmpty(options.IconFile)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3890);
              System.IO.Directory.CreateDirectory(unpackedResourceDir);
              ZipEntry e = zip["zippedFile.ico"];
              IACSharpSensor.IACSharpSensor.SensorReached(3891);
              if ((e.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
                IACSharpSensor.IACSharpSensor.SensorReached(3892);
                e.Attributes ^= FileAttributes.ReadOnly;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(3893);
              e.Extract(unpackedResourceDir);
              nameOfIconFile = Path.Combine(unpackedResourceDir, "zippedFile.ico");
              cp.CompilerOptions += String.Format("/win32icon:\"{0}\"", nameOfIconFile);
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(3894);
              cp.CompilerOptions += String.Format("/win32icon:\"{0}\"", options.IconFile);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3895);
            cp.OutputAssembly = stubExe;
            IACSharpSensor.IACSharpSensor.SensorReached(3896);
            if (options.Flavor == SelfExtractorFlavor.WinFormsApplication) {
              IACSharpSensor.IACSharpSensor.SensorReached(3897);
              cp.CompilerOptions += " /target:winexe";
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3898);
            if (!String.IsNullOrEmpty(options.AdditionalCompilerSwitches)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3899);
              cp.CompilerOptions += " " + options.AdditionalCompilerSwitches;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3900);
            if (String.IsNullOrEmpty(cp.CompilerOptions)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3901);
              cp.CompilerOptions = null;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3902);
            if ((settings.CopyThroughResources != null) && (settings.CopyThroughResources.Count != 0)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3903);
              if (!Directory.Exists(unpackedResourceDir)) {
                IACSharpSensor.IACSharpSensor.SensorReached(3904);
                System.IO.Directory.CreateDirectory(unpackedResourceDir);
              }
              IACSharpSensor.IACSharpSensor.SensorReached(3905);
              foreach (string re in settings.CopyThroughResources) {
                IACSharpSensor.IACSharpSensor.SensorReached(3906);
                string filename = Path.Combine(unpackedResourceDir, re);
                ExtractResourceToFile(a2, re, filename);
                cp.EmbeddedResources.Add(filename);
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3907);
            cp.EmbeddedResources.Add(a1.Location);
            sb.Append("// " + Path.GetFileName(sourceFile) + "\n").Append("// --------------------------------------------\n//\n").Append("// This SFX source file was generated by DotNetZip ").Append(ZipFile.LibraryVersion.ToString()).Append("\n//         at ").Append(System.DateTime.Now.ToString("yyyy MMMM dd  HH:mm:ss")).Append("\n//\n// --------------------------------------------\n\n\n");
            IACSharpSensor.IACSharpSensor.SensorReached(3908);
            if (!String.IsNullOrEmpty(options.Description)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3909);
              sb.Append("[assembly: System.Reflection.AssemblyTitle(\"" + options.Description.Replace("\"", "") + "\")]\n");
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(3910);
              sb.Append("[assembly: System.Reflection.AssemblyTitle(\"DotNetZip SFX Archive\")]\n");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3911);
            if (!String.IsNullOrEmpty(options.ProductVersion)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3912);
              sb.Append("[assembly: System.Reflection.AssemblyInformationalVersion(\"" + options.ProductVersion.Replace("\"", "") + "\")]\n");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3913);
            string copyright = (String.IsNullOrEmpty(options.Copyright)) ? "Extractor: Copyright � Dino Chiesa 2008-2011" : options.Copyright.Replace("\"", "");
            IACSharpSensor.IACSharpSensor.SensorReached(3914);
            if (!String.IsNullOrEmpty(options.ProductName)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3915);
              sb.Append("[assembly: System.Reflection.AssemblyProduct(\"").Append(options.ProductName.Replace("\"", "")).Append("\")]\n");
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(3916);
              sb.Append("[assembly: System.Reflection.AssemblyProduct(\"DotNetZip\")]\n");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3917);
            sb.Append("[assembly: System.Reflection.AssemblyCopyright(\"" + copyright + "\")]\n").Append(String.Format("[assembly: System.Reflection.AssemblyVersion(\"{0}\")]\n", ZipFile.LibraryVersion.ToString()));
            IACSharpSensor.IACSharpSensor.SensorReached(3918);
            if (options.FileVersion != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(3919);
              sb.Append(String.Format("[assembly: System.Reflection.AssemblyFileVersion(\"{0}\")]\n", options.FileVersion.ToString()));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3920);
            sb.Append("\n\n\n");
            string extractLoc = options.DefaultExtractDirectory;
            IACSharpSensor.IACSharpSensor.SensorReached(3921);
            if (extractLoc != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(3922);
              extractLoc = extractLoc.Replace("\"", "").Replace("\\", "\\\\");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3923);
            string postExCmdLine = options.PostExtractCommandLine;
            IACSharpSensor.IACSharpSensor.SensorReached(3924);
            if (postExCmdLine != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(3925);
              postExCmdLine = postExCmdLine.Replace("\\", "\\\\");
              postExCmdLine = postExCmdLine.Replace("\"", "\\\"");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3926);
            foreach (string rc in settings.ResourcesToCompile) {
              IACSharpSensor.IACSharpSensor.SensorReached(3927);
              using (Stream s = zip[rc].OpenReader()) {
                IACSharpSensor.IACSharpSensor.SensorReached(3928);
                if (s == null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(3929);
                  throw new ZipException(String.Format("missing resource '{0}'", rc));
                }
                IACSharpSensor.IACSharpSensor.SensorReached(3930);
                using (StreamReader sr = new StreamReader(s)) {
                  IACSharpSensor.IACSharpSensor.SensorReached(3931);
                  while (sr.Peek() >= 0) {
                    IACSharpSensor.IACSharpSensor.SensorReached(3932);
                    string line = sr.ReadLine();
                    IACSharpSensor.IACSharpSensor.SensorReached(3933);
                    if (extractLoc != null) {
                      IACSharpSensor.IACSharpSensor.SensorReached(3934);
                      line = line.Replace("@@EXTRACTLOCATION", extractLoc);
                    }
                    IACSharpSensor.IACSharpSensor.SensorReached(3935);
                    line = line.Replace("@@REMOVE_AFTER_EXECUTE", options.RemoveUnpackedFilesAfterExecute.ToString());
                    line = line.Replace("@@QUIET", options.Quiet.ToString());
                    IACSharpSensor.IACSharpSensor.SensorReached(3936);
                    if (!String.IsNullOrEmpty(options.SfxExeWindowTitle)) {
                      IACSharpSensor.IACSharpSensor.SensorReached(3937);
                      line = line.Replace("@@SFX_EXE_WINDOW_TITLE", options.SfxExeWindowTitle);
                    }
                    IACSharpSensor.IACSharpSensor.SensorReached(3938);
                    line = line.Replace("@@EXTRACT_EXISTING_FILE", ((int)options.ExtractExistingFile).ToString());
                    IACSharpSensor.IACSharpSensor.SensorReached(3939);
                    if (postExCmdLine != null) {
                      IACSharpSensor.IACSharpSensor.SensorReached(3940);
                      line = line.Replace("@@POST_UNPACK_CMD_LINE", postExCmdLine);
                    }
                    IACSharpSensor.IACSharpSensor.SensorReached(3941);
                    sb.Append(line).Append("\n");
                  }
                }
                IACSharpSensor.IACSharpSensor.SensorReached(3942);
                sb.Append("\n\n");
              }
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3943);
          string LiteralSource = sb.ToString();
          var cr = csharp.CompileAssemblyFromSource(cp, LiteralSource);
          IACSharpSensor.IACSharpSensor.SensorReached(3944);
          if (cr == null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3945);
            throw new SfxGenerationException("Cannot compile the extraction logic!");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3946);
          if (Verbose) {
            IACSharpSensor.IACSharpSensor.SensorReached(3947);
            foreach (string output in cr.Output) {
              IACSharpSensor.IACSharpSensor.SensorReached(3948);
              StatusMessageTextWriter.WriteLine(output);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3949);
          if (cr.Errors.Count != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(3950);
            using (TextWriter tw = new StreamWriter(sourceFile)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3951);
              tw.Write(LiteralSource);
              tw.Write("\n\n\n// ------------------------------------------------------------------\n");
              tw.Write("// Errors during compilation: \n//\n");
              string p = Path.GetFileName(sourceFile);
              IACSharpSensor.IACSharpSensor.SensorReached(3952);
              foreach (System.CodeDom.Compiler.CompilerError error in cr.Errors) {
                IACSharpSensor.IACSharpSensor.SensorReached(3953);
                tw.Write(String.Format("//   {0}({1},{2}): {3} {4}: {5}\n//\n", p, error.Line, error.Column, error.IsWarning ? "Warning" : "error", error.ErrorNumber, error.ErrorText));
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3954);
            throw new SfxGenerationException(String.Format("Errors compiling the extraction logic!  {0}", sourceFile));
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3955);
          OnSaveEvent(ZipProgressEventType.Saving_AfterCompileSelfExtractor);
          IACSharpSensor.IACSharpSensor.SensorReached(3956);
          using (System.IO.Stream input = System.IO.File.OpenRead(stubExe)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3957);
            byte[] buffer = new byte[4000];
            int n = 1;
            IACSharpSensor.IACSharpSensor.SensorReached(3958);
            while (n != 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(3959);
              n = input.Read(buffer, 0, buffer.Length);
              IACSharpSensor.IACSharpSensor.SensorReached(3960);
              if (n != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(3961);
                WriteStream.Write(buffer, 0, n);
              }
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3962);
        OnSaveEvent(ZipProgressEventType.Saving_AfterSaveTempArchive);
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(3963);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(3964);
          if (Directory.Exists(unpackedResourceDir)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3965);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(3966);
              Directory.Delete(unpackedResourceDir, true);
            } catch (System.IO.IOException exc1) {
              IACSharpSensor.IACSharpSensor.SensorReached(3967);
              StatusMessageTextWriter.WriteLine("Warning: Exception: {0}", exc1);
              IACSharpSensor.IACSharpSensor.SensorReached(3968);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3969);
          if (File.Exists(stubExe)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3970);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(3971);
              File.Delete(stubExe);
            } catch (System.IO.IOException exc1) {
              IACSharpSensor.IACSharpSensor.SensorReached(3972);
              StatusMessageTextWriter.WriteLine("Warning: Exception: {0}", exc1);
              IACSharpSensor.IACSharpSensor.SensorReached(3973);
            }
          }
        } catch (System.IO.IOException) {
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3974);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3975);
      return;
    }
    static internal string GenerateTempPathname(string dir, string extension)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3976);
      string candidate = null;
      String AppName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
      IACSharpSensor.IACSharpSensor.SensorReached(3977);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(3978);
        string uuid = System.Guid.NewGuid().ToString();
        string Name = String.Format("{0}-{1}-{2}.{3}", AppName, System.DateTime.Now.ToString("yyyyMMMdd-HHmmss"), uuid, extension);
        candidate = System.IO.Path.Combine(dir, Name);
      } while (System.IO.File.Exists(candidate) || System.IO.Directory.Exists(candidate));
      System.String RNTRNTRNT_398 = candidate;
      IACSharpSensor.IACSharpSensor.SensorReached(3979);
      return RNTRNTRNT_398;
    }
  }
}
