using System;
using System.Threading;
using System.Collections.Generic;
using System.IO;
using Ionic.Zip;
namespace Ionic.Zip
{
  public class ZipOutputStream : Stream
  {
    public ZipOutputStream(Stream stream) : this(stream, false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4162);
    }
    public ZipOutputStream(String fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4163);
      Stream stream = File.Open(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
      _Init(stream, false, fileName);
      IACSharpSensor.IACSharpSensor.SensorReached(4164);
    }
    public ZipOutputStream(Stream stream, bool leaveOpen)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4165);
      _Init(stream, leaveOpen, null);
      IACSharpSensor.IACSharpSensor.SensorReached(4166);
    }
    private void _Init(Stream stream, bool leaveOpen, string name)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4167);
      _outputStream = stream.CanRead ? stream : new CountingStream(stream);
      CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
      CompressionMethod = Ionic.Zip.CompressionMethod.Deflate;
      _encryption = EncryptionAlgorithm.None;
      _entriesWritten = new Dictionary<String, ZipEntry>(StringComparer.Ordinal);
      _zip64 = Zip64Option.Never;
      _leaveUnderlyingStreamOpen = leaveOpen;
      Strategy = Ionic.Zlib.CompressionStrategy.Default;
      _name = name ?? "(stream)";
      ParallelDeflateThreshold = -1L;
      IACSharpSensor.IACSharpSensor.SensorReached(4168);
    }
    public override String ToString()
    {
      String RNTRNTRNT_431 = String.Format("ZipOutputStream::{0}(leaveOpen({1})))", _name, _leaveUnderlyingStreamOpen);
      IACSharpSensor.IACSharpSensor.SensorReached(4169);
      return RNTRNTRNT_431;
    }
    public String Password {
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4170);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(4171);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(4172);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4173);
        _password = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4174);
        if (_password == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4175);
          _encryption = EncryptionAlgorithm.None;
        } else if (_encryption == EncryptionAlgorithm.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(4176);
          _encryption = EncryptionAlgorithm.PkzipWeak;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4177);
      }
    }
    public EncryptionAlgorithm Encryption {
      get {
        EncryptionAlgorithm RNTRNTRNT_432 = _encryption;
        IACSharpSensor.IACSharpSensor.SensorReached(4178);
        return RNTRNTRNT_432;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4179);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(4180);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(4181);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4182);
        if (value == EncryptionAlgorithm.Unsupported) {
          IACSharpSensor.IACSharpSensor.SensorReached(4183);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(4184);
          throw new InvalidOperationException("You may not set Encryption to that value.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4185);
        _encryption = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4186);
      }
    }
    public int CodecBufferSize { get; set; }
    public Ionic.Zlib.CompressionStrategy Strategy { get; set; }
    public ZipEntryTimestamp Timestamp {
      get {
        ZipEntryTimestamp RNTRNTRNT_433 = _timestamp;
        IACSharpSensor.IACSharpSensor.SensorReached(4187);
        return RNTRNTRNT_433;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4188);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(4189);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(4190);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4191);
        _timestamp = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4192);
      }
    }
    public Ionic.Zlib.CompressionLevel CompressionLevel { get; set; }
    public Ionic.Zip.CompressionMethod CompressionMethod { get; set; }
    public string Comment {
      get {
        System.String RNTRNTRNT_434 = _comment;
        IACSharpSensor.IACSharpSensor.SensorReached(4193);
        return RNTRNTRNT_434;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4194);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(4195);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(4196);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4197);
        _comment = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4198);
      }
    }
    public Zip64Option EnableZip64 {
      get {
        Zip64Option RNTRNTRNT_435 = _zip64;
        IACSharpSensor.IACSharpSensor.SensorReached(4199);
        return RNTRNTRNT_435;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4200);
        if (_disposed) {
          IACSharpSensor.IACSharpSensor.SensorReached(4201);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(4202);
          throw new System.InvalidOperationException("The stream has been closed.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4203);
        _zip64 = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4204);
      }
    }
    public bool OutputUsedZip64 {
      get {
        System.Boolean RNTRNTRNT_436 = _anyEntriesUsedZip64 || _directoryNeededZip64;
        IACSharpSensor.IACSharpSensor.SensorReached(4205);
        return RNTRNTRNT_436;
      }
    }
    public bool IgnoreCase {
      get {
        System.Boolean RNTRNTRNT_437 = !_DontIgnoreCase;
        IACSharpSensor.IACSharpSensor.SensorReached(4206);
        return RNTRNTRNT_437;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4207);
        _DontIgnoreCase = !value;
        IACSharpSensor.IACSharpSensor.SensorReached(4208);
      }
    }
    [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete. It will be removed in a future version of the library. Use AlternateEncoding and AlternateEncodingUsage instead.")]
    public bool UseUnicodeAsNecessary {
      get {
        System.Boolean RNTRNTRNT_438 = (_alternateEncoding == System.Text.Encoding.UTF8) && (AlternateEncodingUsage == ZipOption.AsNecessary);
        IACSharpSensor.IACSharpSensor.SensorReached(4209);
        return RNTRNTRNT_438;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4210);
        if (value) {
          IACSharpSensor.IACSharpSensor.SensorReached(4211);
          _alternateEncoding = System.Text.Encoding.UTF8;
          _alternateEncodingUsage = ZipOption.AsNecessary;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4212);
          _alternateEncoding = Ionic.Zip.ZipOutputStream.DefaultEncoding;
          _alternateEncodingUsage = ZipOption.Never;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4213);
      }
    }
    [Obsolete("use AlternateEncoding and AlternateEncodingUsage instead.")]
    public System.Text.Encoding ProvisionalAlternateEncoding {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4214);
        if (_alternateEncodingUsage == ZipOption.AsNecessary) {
          System.Text.Encoding RNTRNTRNT_439 = _alternateEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(4215);
          return RNTRNTRNT_439;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4216);
        return null;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4217);
        _alternateEncoding = value;
        _alternateEncodingUsage = ZipOption.AsNecessary;
        IACSharpSensor.IACSharpSensor.SensorReached(4218);
      }
    }
    public System.Text.Encoding AlternateEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_440 = _alternateEncoding;
        IACSharpSensor.IACSharpSensor.SensorReached(4219);
        return RNTRNTRNT_440;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4220);
        _alternateEncoding = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4221);
      }
    }
    public ZipOption AlternateEncodingUsage {
      get {
        ZipOption RNTRNTRNT_441 = _alternateEncodingUsage;
        IACSharpSensor.IACSharpSensor.SensorReached(4222);
        return RNTRNTRNT_441;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4223);
        _alternateEncodingUsage = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4224);
      }
    }
    public static System.Text.Encoding DefaultEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_442 = System.Text.Encoding.GetEncoding("IBM437");
        IACSharpSensor.IACSharpSensor.SensorReached(4225);
        return RNTRNTRNT_442;
      }
    }
    public long ParallelDeflateThreshold {
      get {
        System.Int64 RNTRNTRNT_443 = _ParallelDeflateThreshold;
        IACSharpSensor.IACSharpSensor.SensorReached(4226);
        return RNTRNTRNT_443;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4227);
        if ((value != 0) && (value != -1) && (value < 64 * 1024)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4228);
          throw new ArgumentOutOfRangeException("value must be greater than 64k, or 0, or -1");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4229);
        _ParallelDeflateThreshold = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4230);
      }
    }
    public int ParallelDeflateMaxBufferPairs {
      get {
        System.Int32 RNTRNTRNT_444 = _maxBufferPairs;
        IACSharpSensor.IACSharpSensor.SensorReached(4231);
        return RNTRNTRNT_444;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4232);
        if (value < 4) {
          IACSharpSensor.IACSharpSensor.SensorReached(4233);
          throw new ArgumentOutOfRangeException("ParallelDeflateMaxBufferPairs", "Value must be 4 or greater.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4234);
        _maxBufferPairs = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4235);
      }
    }
    private void InsureUniqueEntry(ZipEntry ze1)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4236);
      if (_entriesWritten.ContainsKey(ze1.FileName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(4237);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4238);
        throw new ArgumentException(String.Format("The entry '{0}' already exists in the zip archive.", ze1.FileName));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4239);
    }
    internal Stream OutputStream {
      get {
        Stream RNTRNTRNT_445 = _outputStream;
        IACSharpSensor.IACSharpSensor.SensorReached(4240);
        return RNTRNTRNT_445;
      }
    }
    internal String Name {
      get {
        String RNTRNTRNT_446 = _name;
        IACSharpSensor.IACSharpSensor.SensorReached(4241);
        return RNTRNTRNT_446;
      }
    }
    public bool ContainsEntry(string name)
    {
      System.Boolean RNTRNTRNT_447 = _entriesWritten.ContainsKey(SharedUtilities.NormalizePathForUseInZipFile(name));
      IACSharpSensor.IACSharpSensor.SensorReached(4242);
      return RNTRNTRNT_447;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4243);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(4244);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4245);
        throw new System.InvalidOperationException("The stream has been closed.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4246);
      if (buffer == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4247);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4248);
        throw new System.ArgumentNullException("buffer");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4249);
      if (_currentEntry == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4250);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4251);
        throw new System.InvalidOperationException("You must call PutNextEntry() before calling Write().");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4252);
      if (_currentEntry.IsDirectory) {
        IACSharpSensor.IACSharpSensor.SensorReached(4253);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4254);
        throw new System.InvalidOperationException("You cannot Write() data for an entry that is a directory.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4255);
      if (_needToWriteEntryHeader) {
        IACSharpSensor.IACSharpSensor.SensorReached(4256);
        _InitiateCurrentEntry(false);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4257);
      if (count != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4258);
        _entryOutputStream.Write(buffer, offset, count);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4259);
    }
    public ZipEntry PutNextEntry(String entryName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4260);
      if (String.IsNullOrEmpty(entryName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(4261);
        throw new ArgumentNullException("entryName");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4262);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(4263);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4264);
        throw new System.InvalidOperationException("The stream has been closed.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4265);
      _FinishCurrentEntry();
      _currentEntry = ZipEntry.CreateForZipOutputStream(entryName);
      _currentEntry._container = new ZipContainer(this);
      _currentEntry._BitField |= 0x8;
      _currentEntry.SetEntryTimes(DateTime.Now, DateTime.Now, DateTime.Now);
      _currentEntry.CompressionLevel = this.CompressionLevel;
      _currentEntry.CompressionMethod = this.CompressionMethod;
      _currentEntry.Password = _password;
      _currentEntry.Encryption = this.Encryption;
      _currentEntry.AlternateEncoding = this.AlternateEncoding;
      _currentEntry.AlternateEncodingUsage = this.AlternateEncodingUsage;
      IACSharpSensor.IACSharpSensor.SensorReached(4266);
      if (entryName.EndsWith("/")) {
        IACSharpSensor.IACSharpSensor.SensorReached(4267);
        _currentEntry.MarkAsDirectory();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4268);
      _currentEntry.EmitTimesInWindowsFormatWhenSaving = ((_timestamp & ZipEntryTimestamp.Windows) != 0);
      _currentEntry.EmitTimesInUnixFormatWhenSaving = ((_timestamp & ZipEntryTimestamp.Unix) != 0);
      InsureUniqueEntry(_currentEntry);
      _needToWriteEntryHeader = true;
      ZipEntry RNTRNTRNT_448 = _currentEntry;
      IACSharpSensor.IACSharpSensor.SensorReached(4269);
      return RNTRNTRNT_448;
    }
    private void _InitiateCurrentEntry(bool finishing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4270);
      _entriesWritten.Add(_currentEntry.FileName, _currentEntry);
      _entryCount++;
      IACSharpSensor.IACSharpSensor.SensorReached(4271);
      if (_entryCount > 65534 && _zip64 == Zip64Option.Never) {
        IACSharpSensor.IACSharpSensor.SensorReached(4272);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4273);
        throw new System.InvalidOperationException("Too many entries. Consider setting ZipOutputStream.EnableZip64.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4274);
      _currentEntry.WriteHeader(_outputStream, finishing ? 99 : 0);
      _currentEntry.StoreRelativeOffset();
      IACSharpSensor.IACSharpSensor.SensorReached(4275);
      if (!_currentEntry.IsDirectory) {
        IACSharpSensor.IACSharpSensor.SensorReached(4276);
        _currentEntry.WriteSecurityMetadata(_outputStream);
        _currentEntry.PrepOutputStream(_outputStream, finishing ? 0 : -1, out _outputCounter, out _encryptor, out _deflater, out _entryOutputStream);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4277);
      _needToWriteEntryHeader = false;
      IACSharpSensor.IACSharpSensor.SensorReached(4278);
    }
    private void _FinishCurrentEntry()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4279);
      if (_currentEntry != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4280);
        if (_needToWriteEntryHeader) {
          IACSharpSensor.IACSharpSensor.SensorReached(4281);
          _InitiateCurrentEntry(true);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4282);
        _currentEntry.FinishOutputStream(_outputStream, _outputCounter, _encryptor, _deflater, _entryOutputStream);
        _currentEntry.PostProcessOutput(_outputStream);
        IACSharpSensor.IACSharpSensor.SensorReached(4283);
        if (_currentEntry.OutputUsedZip64 != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4284);
          _anyEntriesUsedZip64 |= _currentEntry.OutputUsedZip64.Value;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4285);
        _outputCounter = null;
        _encryptor = _deflater = null;
        _entryOutputStream = null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4286);
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4287);
      if (_disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(4288);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4289);
      if (disposing) {
        IACSharpSensor.IACSharpSensor.SensorReached(4290);
        if (!_exceptionPending) {
          IACSharpSensor.IACSharpSensor.SensorReached(4291);
          _FinishCurrentEntry();
          _directoryNeededZip64 = ZipOutput.WriteCentralDirectoryStructure(_outputStream, _entriesWritten.Values, 1, _zip64, Comment, new ZipContainer(this));
          Stream wrappedStream = null;
          CountingStream cs = _outputStream as CountingStream;
          IACSharpSensor.IACSharpSensor.SensorReached(4292);
          if (cs != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(4293);
            wrappedStream = cs.WrappedStream;
            cs.Dispose();
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(4294);
            wrappedStream = _outputStream;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4295);
          if (!_leaveUnderlyingStreamOpen) {
            IACSharpSensor.IACSharpSensor.SensorReached(4296);
            wrappedStream.Dispose();
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4297);
          _outputStream = null;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4298);
      _disposed = true;
      IACSharpSensor.IACSharpSensor.SensorReached(4299);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_449 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(4300);
        return RNTRNTRNT_449;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_450 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(4301);
        return RNTRNTRNT_450;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_451 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4302);
        return RNTRNTRNT_451;
      }
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4303);
        throw new NotSupportedException();
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_452 = _outputStream.Position;
        IACSharpSensor.IACSharpSensor.SensorReached(4304);
        return RNTRNTRNT_452;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4305);
        throw new NotSupportedException();
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4306);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4307);
      throw new NotSupportedException("Read");
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4308);
      throw new NotSupportedException("Seek");
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4309);
      throw new NotSupportedException();
    }
    private EncryptionAlgorithm _encryption;
    private ZipEntryTimestamp _timestamp;
    internal String _password;
    private String _comment;
    private Stream _outputStream;
    private ZipEntry _currentEntry;
    internal Zip64Option _zip64;
    private Dictionary<String, ZipEntry> _entriesWritten;
    private int _entryCount;
    private ZipOption _alternateEncodingUsage = ZipOption.Never;
    private System.Text.Encoding _alternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
    private bool _leaveUnderlyingStreamOpen;
    private bool _disposed;
    private bool _exceptionPending;
    private bool _anyEntriesUsedZip64, _directoryNeededZip64;
    private CountingStream _outputCounter;
    private Stream _encryptor;
    private Stream _deflater;
    private Ionic.Crc.CrcCalculatorStream _entryOutputStream;
    private bool _needToWriteEntryHeader;
    private string _name;
    private bool _DontIgnoreCase;
    internal Ionic.Zlib.ParallelDeflateOutputStream ParallelDeflater;
    private long _ParallelDeflateThreshold;
    private int _maxBufferPairs = 16;
  }
  internal class ZipContainer
  {
    private ZipFile _zf;
    private ZipOutputStream _zos;
    private ZipInputStream _zis;
    public ZipContainer(Object o)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4310);
      _zf = (o as ZipFile);
      _zos = (o as ZipOutputStream);
      _zis = (o as ZipInputStream);
      IACSharpSensor.IACSharpSensor.SensorReached(4311);
    }
    public ZipFile ZipFile {
      get {
        ZipFile RNTRNTRNT_453 = _zf;
        IACSharpSensor.IACSharpSensor.SensorReached(4312);
        return RNTRNTRNT_453;
      }
    }
    public ZipOutputStream ZipOutputStream {
      get {
        ZipOutputStream RNTRNTRNT_454 = _zos;
        IACSharpSensor.IACSharpSensor.SensorReached(4313);
        return RNTRNTRNT_454;
      }
    }
    public string Name {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4314);
        if (_zf != null) {
          System.String RNTRNTRNT_455 = _zf.Name;
          IACSharpSensor.IACSharpSensor.SensorReached(4315);
          return RNTRNTRNT_455;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4316);
        if (_zis != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4317);
          throw new NotSupportedException();
        }
        System.String RNTRNTRNT_456 = _zos.Name;
        IACSharpSensor.IACSharpSensor.SensorReached(4318);
        return RNTRNTRNT_456;
      }
    }
    public string Password {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4319);
        if (_zf != null) {
          System.String RNTRNTRNT_457 = _zf._Password;
          IACSharpSensor.IACSharpSensor.SensorReached(4320);
          return RNTRNTRNT_457;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4321);
        if (_zis != null) {
          System.String RNTRNTRNT_458 = _zis._Password;
          IACSharpSensor.IACSharpSensor.SensorReached(4322);
          return RNTRNTRNT_458;
        }
        System.String RNTRNTRNT_459 = _zos._password;
        IACSharpSensor.IACSharpSensor.SensorReached(4323);
        return RNTRNTRNT_459;
      }
    }
    public Zip64Option Zip64 {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4324);
        if (_zf != null) {
          Zip64Option RNTRNTRNT_460 = _zf._zip64;
          IACSharpSensor.IACSharpSensor.SensorReached(4325);
          return RNTRNTRNT_460;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4326);
        if (_zis != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4327);
          throw new NotSupportedException();
        }
        Zip64Option RNTRNTRNT_461 = _zos._zip64;
        IACSharpSensor.IACSharpSensor.SensorReached(4328);
        return RNTRNTRNT_461;
      }
    }
    public int BufferSize {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4329);
        if (_zf != null) {
          System.Int32 RNTRNTRNT_462 = _zf.BufferSize;
          IACSharpSensor.IACSharpSensor.SensorReached(4330);
          return RNTRNTRNT_462;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4331);
        if (_zis != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4332);
          throw new NotSupportedException();
        }
        System.Int32 RNTRNTRNT_463 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(4333);
        return RNTRNTRNT_463;
      }
    }
    public Ionic.Zlib.ParallelDeflateOutputStream ParallelDeflater {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4334);
        if (_zf != null) {
          Ionic.Zlib.ParallelDeflateOutputStream RNTRNTRNT_464 = _zf.ParallelDeflater;
          IACSharpSensor.IACSharpSensor.SensorReached(4335);
          return RNTRNTRNT_464;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4336);
        if (_zis != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4337);
          return null;
        }
        Ionic.Zlib.ParallelDeflateOutputStream RNTRNTRNT_465 = _zos.ParallelDeflater;
        IACSharpSensor.IACSharpSensor.SensorReached(4338);
        return RNTRNTRNT_465;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4339);
        if (_zf != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4340);
          _zf.ParallelDeflater = value;
        } else if (_zos != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4341);
          _zos.ParallelDeflater = value;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4342);
      }
    }
    public long ParallelDeflateThreshold {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4343);
        if (_zf != null) {
          System.Int64 RNTRNTRNT_466 = _zf.ParallelDeflateThreshold;
          IACSharpSensor.IACSharpSensor.SensorReached(4344);
          return RNTRNTRNT_466;
        }
        System.Int64 RNTRNTRNT_467 = _zos.ParallelDeflateThreshold;
        IACSharpSensor.IACSharpSensor.SensorReached(4345);
        return RNTRNTRNT_467;
      }
    }
    public int ParallelDeflateMaxBufferPairs {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4346);
        if (_zf != null) {
          System.Int32 RNTRNTRNT_468 = _zf.ParallelDeflateMaxBufferPairs;
          IACSharpSensor.IACSharpSensor.SensorReached(4347);
          return RNTRNTRNT_468;
        }
        System.Int32 RNTRNTRNT_469 = _zos.ParallelDeflateMaxBufferPairs;
        IACSharpSensor.IACSharpSensor.SensorReached(4348);
        return RNTRNTRNT_469;
      }
    }
    public int CodecBufferSize {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4349);
        if (_zf != null) {
          System.Int32 RNTRNTRNT_470 = _zf.CodecBufferSize;
          IACSharpSensor.IACSharpSensor.SensorReached(4350);
          return RNTRNTRNT_470;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4351);
        if (_zis != null) {
          System.Int32 RNTRNTRNT_471 = _zis.CodecBufferSize;
          IACSharpSensor.IACSharpSensor.SensorReached(4352);
          return RNTRNTRNT_471;
        }
        System.Int32 RNTRNTRNT_472 = _zos.CodecBufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(4353);
        return RNTRNTRNT_472;
      }
    }
    public Ionic.Zlib.CompressionStrategy Strategy {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4354);
        if (_zf != null) {
          Ionic.Zlib.CompressionStrategy RNTRNTRNT_473 = _zf.Strategy;
          IACSharpSensor.IACSharpSensor.SensorReached(4355);
          return RNTRNTRNT_473;
        }
        Ionic.Zlib.CompressionStrategy RNTRNTRNT_474 = _zos.Strategy;
        IACSharpSensor.IACSharpSensor.SensorReached(4356);
        return RNTRNTRNT_474;
      }
    }
    public Zip64Option UseZip64WhenSaving {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4357);
        if (_zf != null) {
          Zip64Option RNTRNTRNT_475 = _zf.UseZip64WhenSaving;
          IACSharpSensor.IACSharpSensor.SensorReached(4358);
          return RNTRNTRNT_475;
        }
        Zip64Option RNTRNTRNT_476 = _zos.EnableZip64;
        IACSharpSensor.IACSharpSensor.SensorReached(4359);
        return RNTRNTRNT_476;
      }
    }
    public System.Text.Encoding AlternateEncoding {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4360);
        if (_zf != null) {
          System.Text.Encoding RNTRNTRNT_477 = _zf.AlternateEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(4361);
          return RNTRNTRNT_477;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4362);
        if (_zos != null) {
          System.Text.Encoding RNTRNTRNT_478 = _zos.AlternateEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(4363);
          return RNTRNTRNT_478;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4364);
        return null;
      }
    }
    public System.Text.Encoding DefaultEncoding {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4365);
        if (_zf != null) {
          System.Text.Encoding RNTRNTRNT_479 = ZipFile.DefaultEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(4366);
          return RNTRNTRNT_479;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4367);
        if (_zos != null) {
          System.Text.Encoding RNTRNTRNT_480 = ZipOutputStream.DefaultEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(4368);
          return RNTRNTRNT_480;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4369);
        return null;
      }
    }
    public ZipOption AlternateEncodingUsage {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4370);
        if (_zf != null) {
          ZipOption RNTRNTRNT_481 = _zf.AlternateEncodingUsage;
          IACSharpSensor.IACSharpSensor.SensorReached(4371);
          return RNTRNTRNT_481;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4372);
        if (_zos != null) {
          ZipOption RNTRNTRNT_482 = _zos.AlternateEncodingUsage;
          IACSharpSensor.IACSharpSensor.SensorReached(4373);
          return RNTRNTRNT_482;
        }
        ZipOption RNTRNTRNT_483 = ZipOption.Never;
        IACSharpSensor.IACSharpSensor.SensorReached(4374);
        return RNTRNTRNT_483;
      }
    }
    public Stream ReadStream {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4375);
        if (_zf != null) {
          Stream RNTRNTRNT_484 = _zf.ReadStream;
          IACSharpSensor.IACSharpSensor.SensorReached(4376);
          return RNTRNTRNT_484;
        }
        Stream RNTRNTRNT_485 = _zis.ReadStream;
        IACSharpSensor.IACSharpSensor.SensorReached(4377);
        return RNTRNTRNT_485;
      }
    }
  }
}
