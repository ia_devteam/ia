using System;
using System.IO;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Collections.Generic;
namespace Ionic
{
  internal enum LogicalConjunction
  {
    NONE,
    AND,
    OR,
    XOR
  }
  internal enum WhichTime
  {
    atime,
    mtime,
    ctime
  }
  internal enum ComparisonOperator
  {
    [Description(">")]
    GreaterThan,
    [Description(">=")]
    GreaterThanOrEqualTo,
    [Description("<")]
    LesserThan,
    [Description("<=")]
    LesserThanOrEqualTo,
    [Description("=")]
    EqualTo,
    [Description("!=")]
    NotEqualTo
  }
  internal abstract partial class SelectionCriterion
  {
    internal virtual bool Verbose { get; set; }
    internal abstract bool Evaluate(string filename);
    [System.Diagnostics.Conditional("SelectorTrace")]
    protected static void CriterionTrace(string format, params object[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1280);
    }
  }
  internal partial class SizeCriterion : SelectionCriterion
  {
    internal ComparisonOperator Operator;
    internal Int64 Size;
    public override String ToString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1281);
      StringBuilder sb = new StringBuilder();
      sb.Append("size ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(Size.ToString());
      String RNTRNTRNT_100 = sb.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(1282);
      return RNTRNTRNT_100;
    }
    internal override bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1283);
      System.IO.FileInfo fi = new System.IO.FileInfo(filename);
      CriterionTrace("SizeCriterion::Evaluate('{0}' [{1}])", filename, this.ToString());
      System.Boolean RNTRNTRNT_101 = _Evaluate(fi.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(1284);
      return RNTRNTRNT_101;
    }
    private bool _Evaluate(Int64 Length)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1285);
      bool result = false;
      IACSharpSensor.IACSharpSensor.SensorReached(1286);
      switch (Operator) {
        case ComparisonOperator.GreaterThanOrEqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(1287);
          result = Length >= Size;
          IACSharpSensor.IACSharpSensor.SensorReached(1288);
          break;
        case ComparisonOperator.GreaterThan:
          IACSharpSensor.IACSharpSensor.SensorReached(1289);
          result = Length > Size;
          IACSharpSensor.IACSharpSensor.SensorReached(1290);
          break;
        case ComparisonOperator.LesserThanOrEqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(1291);
          result = Length <= Size;
          IACSharpSensor.IACSharpSensor.SensorReached(1292);
          break;
        case ComparisonOperator.LesserThan:
          IACSharpSensor.IACSharpSensor.SensorReached(1293);
          result = Length < Size;
          IACSharpSensor.IACSharpSensor.SensorReached(1294);
          break;
        case ComparisonOperator.EqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(1295);
          result = Length == Size;
          IACSharpSensor.IACSharpSensor.SensorReached(1296);
          break;
        case ComparisonOperator.NotEqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(1297);
          result = Length != Size;
          IACSharpSensor.IACSharpSensor.SensorReached(1298);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(1299);
          throw new ArgumentException("Operator");
      }
      System.Boolean RNTRNTRNT_102 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(1300);
      return RNTRNTRNT_102;
    }
  }
  internal partial class TimeCriterion : SelectionCriterion
  {
    internal ComparisonOperator Operator;
    internal WhichTime Which;
    internal DateTime Time;
    public override String ToString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1301);
      StringBuilder sb = new StringBuilder();
      sb.Append(Which.ToString()).Append(" ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(Time.ToString("yyyy-MM-dd-HH:mm:ss"));
      String RNTRNTRNT_103 = sb.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(1302);
      return RNTRNTRNT_103;
    }
    internal override bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1303);
      DateTime x;
      IACSharpSensor.IACSharpSensor.SensorReached(1304);
      switch (Which) {
        case WhichTime.atime:
          IACSharpSensor.IACSharpSensor.SensorReached(1305);
          x = System.IO.File.GetLastAccessTime(filename).ToUniversalTime();
          IACSharpSensor.IACSharpSensor.SensorReached(1306);
          break;
        case WhichTime.mtime:
          IACSharpSensor.IACSharpSensor.SensorReached(1307);
          x = System.IO.File.GetLastWriteTime(filename).ToUniversalTime();
          IACSharpSensor.IACSharpSensor.SensorReached(1308);
          break;
        case WhichTime.ctime:
          IACSharpSensor.IACSharpSensor.SensorReached(1309);
          x = System.IO.File.GetCreationTime(filename).ToUniversalTime();
          IACSharpSensor.IACSharpSensor.SensorReached(1310);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(1311);
          throw new ArgumentException("Operator");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1312);
      CriterionTrace("TimeCriterion({0},{1})= {2}", filename, Which.ToString(), x);
      System.Boolean RNTRNTRNT_104 = _Evaluate(x);
      IACSharpSensor.IACSharpSensor.SensorReached(1313);
      return RNTRNTRNT_104;
    }
    private bool _Evaluate(DateTime x)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1314);
      bool result = false;
      IACSharpSensor.IACSharpSensor.SensorReached(1315);
      switch (Operator) {
        case ComparisonOperator.GreaterThanOrEqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(1316);
          result = (x >= Time);
          IACSharpSensor.IACSharpSensor.SensorReached(1317);
          break;
        case ComparisonOperator.GreaterThan:
          IACSharpSensor.IACSharpSensor.SensorReached(1318);
          result = (x > Time);
          IACSharpSensor.IACSharpSensor.SensorReached(1319);
          break;
        case ComparisonOperator.LesserThanOrEqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(1320);
          result = (x <= Time);
          IACSharpSensor.IACSharpSensor.SensorReached(1321);
          break;
        case ComparisonOperator.LesserThan:
          IACSharpSensor.IACSharpSensor.SensorReached(1322);
          result = (x < Time);
          IACSharpSensor.IACSharpSensor.SensorReached(1323);
          break;
        case ComparisonOperator.EqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(1324);
          result = (x == Time);
          IACSharpSensor.IACSharpSensor.SensorReached(1325);
          break;
        case ComparisonOperator.NotEqualTo:
          IACSharpSensor.IACSharpSensor.SensorReached(1326);
          result = (x != Time);
          IACSharpSensor.IACSharpSensor.SensorReached(1327);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(1328);
          throw new ArgumentException("Operator");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1329);
      CriterionTrace("TimeCriterion: {0}", result);
      System.Boolean RNTRNTRNT_105 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(1330);
      return RNTRNTRNT_105;
    }
  }
  internal partial class NameCriterion : SelectionCriterion
  {
    private Regex _re;
    private String _regexString;
    internal ComparisonOperator Operator;
    private string _MatchingFileSpec;
    internal virtual string MatchingFileSpec {
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1331);
        if (Directory.Exists(value)) {
          IACSharpSensor.IACSharpSensor.SensorReached(1332);
          _MatchingFileSpec = ".\\" + value + "\\*.*";
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1333);
          _MatchingFileSpec = value;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1334);
        _regexString = "^" + Regex.Escape(_MatchingFileSpec).Replace("\\\\\\*\\.\\*", "\\\\([^\\.]+|.*\\.[^\\\\\\.]*)").Replace("\\.\\*", "\\.[^\\\\\\.]*").Replace("\\*", ".*").Replace("\\?", "[^\\\\\\.]") + "$";
        CriterionTrace("NameCriterion regexString({0})", _regexString);
        _re = new Regex(_regexString, RegexOptions.IgnoreCase);
        IACSharpSensor.IACSharpSensor.SensorReached(1335);
      }
    }
    public override String ToString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1336);
      StringBuilder sb = new StringBuilder();
      sb.Append("name ").Append(EnumUtil.GetDescription(Operator)).Append(" '").Append(_MatchingFileSpec).Append("'");
      String RNTRNTRNT_106 = sb.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(1337);
      return RNTRNTRNT_106;
    }
    internal override bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1338);
      CriterionTrace("NameCriterion::Evaluate('{0}' pattern[{1}])", filename, _MatchingFileSpec);
      System.Boolean RNTRNTRNT_107 = _Evaluate(filename);
      IACSharpSensor.IACSharpSensor.SensorReached(1339);
      return RNTRNTRNT_107;
    }
    private bool _Evaluate(string fullpath)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1340);
      CriterionTrace("NameCriterion::Evaluate({0})", fullpath);
      String f = (_MatchingFileSpec.IndexOf('\\') == -1) ? System.IO.Path.GetFileName(fullpath) : fullpath;
      bool result = _re.IsMatch(f);
      IACSharpSensor.IACSharpSensor.SensorReached(1341);
      if (Operator != ComparisonOperator.EqualTo) {
        IACSharpSensor.IACSharpSensor.SensorReached(1342);
        result = !result;
      }
      System.Boolean RNTRNTRNT_108 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(1343);
      return RNTRNTRNT_108;
    }
  }
  internal partial class TypeCriterion : SelectionCriterion
  {
    private char ObjectType;
    internal ComparisonOperator Operator;
    internal string AttributeString {
      get {
        System.String RNTRNTRNT_109 = ObjectType.ToString();
        IACSharpSensor.IACSharpSensor.SensorReached(1344);
        return RNTRNTRNT_109;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1345);
        if (value.Length != 1 || (value[0] != 'D' && value[0] != 'F')) {
          IACSharpSensor.IACSharpSensor.SensorReached(1346);
          throw new ArgumentException("Specify a single character: either D or F");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1347);
        ObjectType = value[0];
        IACSharpSensor.IACSharpSensor.SensorReached(1348);
      }
    }
    public override String ToString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1349);
      StringBuilder sb = new StringBuilder();
      sb.Append("type ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(AttributeString);
      String RNTRNTRNT_110 = sb.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(1350);
      return RNTRNTRNT_110;
    }
    internal override bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1351);
      CriterionTrace("TypeCriterion::Evaluate({0})", filename);
      bool result = (ObjectType == 'D') ? Directory.Exists(filename) : File.Exists(filename);
      IACSharpSensor.IACSharpSensor.SensorReached(1352);
      if (Operator != ComparisonOperator.EqualTo) {
        IACSharpSensor.IACSharpSensor.SensorReached(1353);
        result = !result;
      }
      System.Boolean RNTRNTRNT_111 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(1354);
      return RNTRNTRNT_111;
    }
  }
  internal partial class AttributesCriterion : SelectionCriterion
  {
    private FileAttributes _Attributes;
    internal ComparisonOperator Operator;
    internal string AttributeString {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1355);
        string result = "";
        IACSharpSensor.IACSharpSensor.SensorReached(1356);
        if ((_Attributes & FileAttributes.Hidden) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(1357);
          result += "H";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1358);
        if ((_Attributes & FileAttributes.System) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(1359);
          result += "S";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1360);
        if ((_Attributes & FileAttributes.ReadOnly) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(1361);
          result += "R";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1362);
        if ((_Attributes & FileAttributes.Archive) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(1363);
          result += "A";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1364);
        if ((_Attributes & FileAttributes.ReparsePoint) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(1365);
          result += "L";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1366);
        if ((_Attributes & FileAttributes.NotContentIndexed) != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(1367);
          result += "I";
        }
        System.String RNTRNTRNT_112 = result;
        IACSharpSensor.IACSharpSensor.SensorReached(1368);
        return RNTRNTRNT_112;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1369);
        _Attributes = FileAttributes.Normal;
        IACSharpSensor.IACSharpSensor.SensorReached(1370);
        foreach (char c in value.ToUpper()) {
          IACSharpSensor.IACSharpSensor.SensorReached(1371);
          switch (c) {
            case 'H':
              IACSharpSensor.IACSharpSensor.SensorReached(1372);
              if ((_Attributes & FileAttributes.Hidden) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(1373);
                throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1374);
              _Attributes |= FileAttributes.Hidden;
              IACSharpSensor.IACSharpSensor.SensorReached(1375);
              break;
            case 'R':
              IACSharpSensor.IACSharpSensor.SensorReached(1376);
              if ((_Attributes & FileAttributes.ReadOnly) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(1377);
                throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1378);
              _Attributes |= FileAttributes.ReadOnly;
              IACSharpSensor.IACSharpSensor.SensorReached(1379);
              break;
            case 'S':
              IACSharpSensor.IACSharpSensor.SensorReached(1380);
              if ((_Attributes & FileAttributes.System) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(1381);
                throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1382);
              _Attributes |= FileAttributes.System;
              IACSharpSensor.IACSharpSensor.SensorReached(1383);
              break;
            case 'A':
              IACSharpSensor.IACSharpSensor.SensorReached(1384);
              if ((_Attributes & FileAttributes.Archive) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(1385);
                throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1386);
              _Attributes |= FileAttributes.Archive;
              IACSharpSensor.IACSharpSensor.SensorReached(1387);
              break;
            case 'I':
              IACSharpSensor.IACSharpSensor.SensorReached(1388);
              if ((_Attributes & FileAttributes.NotContentIndexed) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(1389);
                throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1390);
              _Attributes |= FileAttributes.NotContentIndexed;
              IACSharpSensor.IACSharpSensor.SensorReached(1391);
              break;
            case 'L':
              IACSharpSensor.IACSharpSensor.SensorReached(1392);
              if ((_Attributes & FileAttributes.ReparsePoint) != 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(1393);
                throw new ArgumentException(String.Format("Repeated flag. ({0})", c), "value");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1394);
              _Attributes |= FileAttributes.ReparsePoint;
              IACSharpSensor.IACSharpSensor.SensorReached(1395);
              break;
            default:
              IACSharpSensor.IACSharpSensor.SensorReached(1396);
              throw new ArgumentException(value);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1397);
      }
    }
    public override String ToString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1398);
      StringBuilder sb = new StringBuilder();
      sb.Append("attributes ").Append(EnumUtil.GetDescription(Operator)).Append(" ").Append(AttributeString);
      String RNTRNTRNT_113 = sb.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(1399);
      return RNTRNTRNT_113;
    }
    private bool _EvaluateOne(FileAttributes fileAttrs, FileAttributes criterionAttrs)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1400);
      bool result = false;
      IACSharpSensor.IACSharpSensor.SensorReached(1401);
      if ((_Attributes & criterionAttrs) == criterionAttrs) {
        IACSharpSensor.IACSharpSensor.SensorReached(1402);
        result = ((fileAttrs & criterionAttrs) == criterionAttrs);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(1403);
        result = true;
      }
      System.Boolean RNTRNTRNT_114 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(1404);
      return RNTRNTRNT_114;
    }
    internal override bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1405);
      if (Directory.Exists(filename)) {
        System.Boolean RNTRNTRNT_115 = (Operator != ComparisonOperator.EqualTo);
        IACSharpSensor.IACSharpSensor.SensorReached(1406);
        return RNTRNTRNT_115;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1407);
      FileAttributes fileAttrs = System.IO.File.GetAttributes(filename);
      System.Boolean RNTRNTRNT_116 = _Evaluate(fileAttrs);
      IACSharpSensor.IACSharpSensor.SensorReached(1408);
      return RNTRNTRNT_116;
    }
    private bool _Evaluate(FileAttributes fileAttrs)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1409);
      bool result = _EvaluateOne(fileAttrs, FileAttributes.Hidden);
      IACSharpSensor.IACSharpSensor.SensorReached(1410);
      if (result) {
        IACSharpSensor.IACSharpSensor.SensorReached(1411);
        result = _EvaluateOne(fileAttrs, FileAttributes.System);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1412);
      if (result) {
        IACSharpSensor.IACSharpSensor.SensorReached(1413);
        result = _EvaluateOne(fileAttrs, FileAttributes.ReadOnly);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1414);
      if (result) {
        IACSharpSensor.IACSharpSensor.SensorReached(1415);
        result = _EvaluateOne(fileAttrs, FileAttributes.Archive);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1416);
      if (result) {
        IACSharpSensor.IACSharpSensor.SensorReached(1417);
        result = _EvaluateOne(fileAttrs, FileAttributes.NotContentIndexed);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1418);
      if (result) {
        IACSharpSensor.IACSharpSensor.SensorReached(1419);
        result = _EvaluateOne(fileAttrs, FileAttributes.ReparsePoint);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1420);
      if (Operator != ComparisonOperator.EqualTo) {
        IACSharpSensor.IACSharpSensor.SensorReached(1421);
        result = !result;
      }
      System.Boolean RNTRNTRNT_117 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(1422);
      return RNTRNTRNT_117;
    }
  }
  internal partial class CompoundCriterion : SelectionCriterion
  {
    internal LogicalConjunction Conjunction;
    internal SelectionCriterion Left;
    private SelectionCriterion _Right;
    internal SelectionCriterion Right {
      get {
        SelectionCriterion RNTRNTRNT_118 = _Right;
        IACSharpSensor.IACSharpSensor.SensorReached(1423);
        return RNTRNTRNT_118;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1424);
        _Right = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1425);
        if (value == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1426);
          Conjunction = LogicalConjunction.NONE;
        } else if (Conjunction == LogicalConjunction.NONE) {
          IACSharpSensor.IACSharpSensor.SensorReached(1427);
          Conjunction = LogicalConjunction.AND;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1428);
      }
    }
    internal override bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1429);
      bool result = Left.Evaluate(filename);
      IACSharpSensor.IACSharpSensor.SensorReached(1430);
      switch (Conjunction) {
        case LogicalConjunction.AND:
          IACSharpSensor.IACSharpSensor.SensorReached(1431);
          if (result) {
            IACSharpSensor.IACSharpSensor.SensorReached(1432);
            result = Right.Evaluate(filename);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1433);
          break;
        case LogicalConjunction.OR:
          IACSharpSensor.IACSharpSensor.SensorReached(1434);
          if (!result) {
            IACSharpSensor.IACSharpSensor.SensorReached(1435);
            result = Right.Evaluate(filename);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1436);
          break;
        case LogicalConjunction.XOR:
          IACSharpSensor.IACSharpSensor.SensorReached(1437);
          result ^= Right.Evaluate(filename);
          IACSharpSensor.IACSharpSensor.SensorReached(1438);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(1439);
          throw new ArgumentException("Conjunction");
      }
      System.Boolean RNTRNTRNT_119 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(1440);
      return RNTRNTRNT_119;
    }
    public override String ToString()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1441);
      StringBuilder sb = new StringBuilder();
      sb.Append("(").Append((Left != null) ? Left.ToString() : "null").Append(" ").Append(Conjunction.ToString()).Append(" ").Append((Right != null) ? Right.ToString() : "null").Append(")");
      String RNTRNTRNT_120 = sb.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(1442);
      return RNTRNTRNT_120;
    }
  }
  public partial class FileSelector
  {
    internal SelectionCriterion _Criterion;
    public FileSelector(String selectionCriteria) : this(selectionCriteria, true)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1443);
    }
    public FileSelector(String selectionCriteria, bool traverseDirectoryReparsePoints)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1444);
      if (!String.IsNullOrEmpty(selectionCriteria)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1445);
        _Criterion = _ParseCriterion(selectionCriteria);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1446);
      TraverseReparsePoints = traverseDirectoryReparsePoints;
      IACSharpSensor.IACSharpSensor.SensorReached(1447);
    }
    public String SelectionCriteria {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1448);
        if (_Criterion == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1449);
          return null;
        }
        String RNTRNTRNT_121 = _Criterion.ToString();
        IACSharpSensor.IACSharpSensor.SensorReached(1450);
        return RNTRNTRNT_121;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1451);
        if (value == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1452);
          _Criterion = null;
        } else if (value.Trim() == "") {
          IACSharpSensor.IACSharpSensor.SensorReached(1454);
          _Criterion = null;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1453);
          _Criterion = _ParseCriterion(value);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1455);
      }
    }
    public bool TraverseReparsePoints { get; set; }
    private enum ParseState
    {
      Start,
      OpenParen,
      CriterionDone,
      ConjunctionPending,
      Whitespace
    }
    private static class RegexAssertions
    {
      public static readonly String PrecededByOddNumberOfSingleQuotes = "(?<=(?:[^']*'[^']*')*'[^']*)";
      public static readonly String FollowedByOddNumberOfSingleQuotesAndLineEnd = "(?=[^']*'(?:[^']*'[^']*')*[^']*$)";
      public static readonly String PrecededByEvenNumberOfSingleQuotes = "(?<=(?:[^']*'[^']*')*[^']*)";
      public static readonly String FollowedByEvenNumberOfSingleQuotesAndLineEnd = "(?=(?:[^']*'[^']*')*[^']*$)";
    }
    private static string NormalizeCriteriaExpression(string source)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1456);
      string[][] prPairs = {
        new string[] {
          "([^']*)\\(\\(([^']+)",
          "$1( ($2"
        },
        new string[] {
          "(.)\\)\\)",
          "$1) )"
        },
        new string[] {
          "\\((\\S)",
          "( $1"
        },
        new string[] {
          "(\\S)\\)",
          "$1 )"
        },
        new string[] {
          "^\\)",
          " )"
        },
        new string[] {
          "(\\S)\\(",
          "$1 ("
        },
        new string[] {
          "\\)(\\S)",
          ") $1"
        },
        new string[] {
          "(=)('[^']*')",
          "$1 $2"
        },
        new string[] {
          "([^ !><])(>|<|!=|=)",
          "$1 $2"
        },
        new string[] {
          "(>|<|!=|=)([^ =])",
          "$1 $2"
        },
        new string[] {
          "/",
          "\\"
        }
      };
      string interim = source;
      IACSharpSensor.IACSharpSensor.SensorReached(1457);
      for (int i = 0; i < prPairs.Length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1458);
        string pattern = RegexAssertions.PrecededByEvenNumberOfSingleQuotes + prPairs[i][0] + RegexAssertions.FollowedByEvenNumberOfSingleQuotesAndLineEnd;
        interim = Regex.Replace(interim, pattern, prPairs[i][1]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1459);
      var regexPattern = "/" + RegexAssertions.FollowedByOddNumberOfSingleQuotesAndLineEnd;
      interim = Regex.Replace(interim, regexPattern, "\\");
      regexPattern = " " + RegexAssertions.FollowedByOddNumberOfSingleQuotesAndLineEnd;
      System.String RNTRNTRNT_122 = Regex.Replace(interim, regexPattern, "\u0006");
      IACSharpSensor.IACSharpSensor.SensorReached(1460);
      return RNTRNTRNT_122;
    }
    private static SelectionCriterion _ParseCriterion(String s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1461);
      if (s == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1462);
        return null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1463);
      s = NormalizeCriteriaExpression(s);
      IACSharpSensor.IACSharpSensor.SensorReached(1464);
      if (s.IndexOf(" ") == -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(1465);
        s = "name = " + s;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1466);
      string[] tokens = s.Trim().Split(' ', '\t');
      IACSharpSensor.IACSharpSensor.SensorReached(1467);
      if (tokens.Length < 3) {
        IACSharpSensor.IACSharpSensor.SensorReached(1468);
        throw new ArgumentException(s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1469);
      SelectionCriterion current = null;
      LogicalConjunction pendingConjunction = LogicalConjunction.NONE;
      ParseState state;
      var stateStack = new System.Collections.Generic.Stack<ParseState>();
      var critStack = new System.Collections.Generic.Stack<SelectionCriterion>();
      stateStack.Push(ParseState.Start);
      IACSharpSensor.IACSharpSensor.SensorReached(1470);
      for (int i = 0; i < tokens.Length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1471);
        string tok1 = tokens[i].ToLower();
        IACSharpSensor.IACSharpSensor.SensorReached(1472);
        switch (tok1) {
          case "and":
          case "xor":
          case "or":
            IACSharpSensor.IACSharpSensor.SensorReached(1473);
            state = stateStack.Peek();
            IACSharpSensor.IACSharpSensor.SensorReached(1474);
            if (state != ParseState.CriterionDone) {
              IACSharpSensor.IACSharpSensor.SensorReached(1475);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1476);
            if (tokens.Length <= i + 3) {
              IACSharpSensor.IACSharpSensor.SensorReached(1477);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1478);
            pendingConjunction = (LogicalConjunction)Enum.Parse(typeof(LogicalConjunction), tokens[i].ToUpper(), true);
            current = new CompoundCriterion {
              Left = current,
              Right = null,
              Conjunction = pendingConjunction
            };
            stateStack.Push(state);
            stateStack.Push(ParseState.ConjunctionPending);
            critStack.Push(current);
            IACSharpSensor.IACSharpSensor.SensorReached(1479);
            break;
          case "(":
            IACSharpSensor.IACSharpSensor.SensorReached(1480);
            state = stateStack.Peek();
            IACSharpSensor.IACSharpSensor.SensorReached(1481);
            if (state != ParseState.Start && state != ParseState.ConjunctionPending && state != ParseState.OpenParen) {
              IACSharpSensor.IACSharpSensor.SensorReached(1482);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1483);
            if (tokens.Length <= i + 4) {
              IACSharpSensor.IACSharpSensor.SensorReached(1484);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1485);
            stateStack.Push(ParseState.OpenParen);
            IACSharpSensor.IACSharpSensor.SensorReached(1486);
            break;
          case ")":
            IACSharpSensor.IACSharpSensor.SensorReached(1487);
            state = stateStack.Pop();
            IACSharpSensor.IACSharpSensor.SensorReached(1488);
            if (stateStack.Peek() != ParseState.OpenParen) {
              IACSharpSensor.IACSharpSensor.SensorReached(1489);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1490);
            stateStack.Pop();
            stateStack.Push(ParseState.CriterionDone);
            IACSharpSensor.IACSharpSensor.SensorReached(1491);
            break;
          case "atime":
          case "ctime":
          case "mtime":
            IACSharpSensor.IACSharpSensor.SensorReached(1492);
            if (tokens.Length <= i + 2) {
              IACSharpSensor.IACSharpSensor.SensorReached(1493);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1494);
            DateTime t;
            IACSharpSensor.IACSharpSensor.SensorReached(1495);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(1496);
              t = DateTime.ParseExact(tokens[i + 2], "yyyy-MM-dd-HH:mm:ss", null);
            } catch (FormatException) {
              IACSharpSensor.IACSharpSensor.SensorReached(1497);
              try {
                IACSharpSensor.IACSharpSensor.SensorReached(1498);
                t = DateTime.ParseExact(tokens[i + 2], "yyyy/MM/dd-HH:mm:ss", null);
              } catch (FormatException) {
                IACSharpSensor.IACSharpSensor.SensorReached(1499);
                try {
                  IACSharpSensor.IACSharpSensor.SensorReached(1500);
                  t = DateTime.ParseExact(tokens[i + 2], "yyyy/MM/dd", null);
                } catch (FormatException) {
                  IACSharpSensor.IACSharpSensor.SensorReached(1501);
                  try {
                    IACSharpSensor.IACSharpSensor.SensorReached(1502);
                    t = DateTime.ParseExact(tokens[i + 2], "MM/dd/yyyy", null);
                  } catch (FormatException) {
                    IACSharpSensor.IACSharpSensor.SensorReached(1503);
                    t = DateTime.ParseExact(tokens[i + 2], "yyyy-MM-dd", null);
                    IACSharpSensor.IACSharpSensor.SensorReached(1504);
                  }
                  IACSharpSensor.IACSharpSensor.SensorReached(1505);
                }
                IACSharpSensor.IACSharpSensor.SensorReached(1506);
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1507);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1508);
            t = DateTime.SpecifyKind(t, DateTimeKind.Local).ToUniversalTime();
            current = new TimeCriterion {
              Which = (WhichTime)Enum.Parse(typeof(WhichTime), tokens[i], true),
              Operator = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1]),
              Time = t
            };
            i += 2;
            stateStack.Push(ParseState.CriterionDone);
            IACSharpSensor.IACSharpSensor.SensorReached(1509);
            break;
          case "length":
          case "size":
            IACSharpSensor.IACSharpSensor.SensorReached(1510);
            if (tokens.Length <= i + 2) {
              IACSharpSensor.IACSharpSensor.SensorReached(1511);
              throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1512);
            Int64 sz = 0;
            string v = tokens[i + 2];
            IACSharpSensor.IACSharpSensor.SensorReached(1513);
            if (v.ToUpper().EndsWith("K")) {
              IACSharpSensor.IACSharpSensor.SensorReached(1514);
              sz = Int64.Parse(v.Substring(0, v.Length - 1)) * 1024;
            } else if (v.ToUpper().EndsWith("KB")) {
              IACSharpSensor.IACSharpSensor.SensorReached(1520);
              sz = Int64.Parse(v.Substring(0, v.Length - 2)) * 1024;
            } else if (v.ToUpper().EndsWith("M")) {
              IACSharpSensor.IACSharpSensor.SensorReached(1519);
              sz = Int64.Parse(v.Substring(0, v.Length - 1)) * 1024 * 1024;
            } else if (v.ToUpper().EndsWith("MB")) {
              IACSharpSensor.IACSharpSensor.SensorReached(1518);
              sz = Int64.Parse(v.Substring(0, v.Length - 2)) * 1024 * 1024;
            } else if (v.ToUpper().EndsWith("G")) {
              IACSharpSensor.IACSharpSensor.SensorReached(1517);
              sz = Int64.Parse(v.Substring(0, v.Length - 1)) * 1024 * 1024 * 1024;
            } else if (v.ToUpper().EndsWith("GB")) {
              IACSharpSensor.IACSharpSensor.SensorReached(1516);
              sz = Int64.Parse(v.Substring(0, v.Length - 2)) * 1024 * 1024 * 1024;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(1515);
              sz = Int64.Parse(tokens[i + 2]);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1521);
            current = new SizeCriterion {
              Size = sz,
              Operator = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1])
            };
            i += 2;
            stateStack.Push(ParseState.CriterionDone);
            IACSharpSensor.IACSharpSensor.SensorReached(1522);
            break;
          case "filename":
          case "name":
            
            {
              IACSharpSensor.IACSharpSensor.SensorReached(1523);
              if (tokens.Length <= i + 2) {
                IACSharpSensor.IACSharpSensor.SensorReached(1524);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1525);
              ComparisonOperator c = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1]);
              IACSharpSensor.IACSharpSensor.SensorReached(1526);
              if (c != ComparisonOperator.NotEqualTo && c != ComparisonOperator.EqualTo) {
                IACSharpSensor.IACSharpSensor.SensorReached(1527);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1528);
              string m = tokens[i + 2];
              IACSharpSensor.IACSharpSensor.SensorReached(1529);
              if (m.StartsWith("'") && m.EndsWith("'")) {
                IACSharpSensor.IACSharpSensor.SensorReached(1530);
                m = m.Substring(1, m.Length - 2).Replace("\u0006", " ");
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1531);
              current = new NameCriterion {
                MatchingFileSpec = m,
                Operator = c
              };
              i += 2;
              stateStack.Push(ParseState.CriterionDone);
              IACSharpSensor.IACSharpSensor.SensorReached(1532);
              break;
            }

          case "attrs":
          case "attributes":
          case "type":
            
            {
              IACSharpSensor.IACSharpSensor.SensorReached(1533);
              if (tokens.Length <= i + 2) {
                IACSharpSensor.IACSharpSensor.SensorReached(1534);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1535);
              ComparisonOperator c = (ComparisonOperator)EnumUtil.Parse(typeof(ComparisonOperator), tokens[i + 1]);
              IACSharpSensor.IACSharpSensor.SensorReached(1536);
              if (c != ComparisonOperator.NotEqualTo && c != ComparisonOperator.EqualTo) {
                IACSharpSensor.IACSharpSensor.SensorReached(1537);
                throw new ArgumentException(String.Join(" ", tokens, i, tokens.Length - i));
              }
              IACSharpSensor.IACSharpSensor.SensorReached(1538);
              current = (tok1 == "type") ? (SelectionCriterion)new TypeCriterion {
                AttributeString = tokens[i + 2],
                Operator = c
              } : (SelectionCriterion)new AttributesCriterion {
                AttributeString = tokens[i + 2],
                Operator = c
              };
              i += 2;
              stateStack.Push(ParseState.CriterionDone);
              IACSharpSensor.IACSharpSensor.SensorReached(1539);
              break;
            }

          case "":
            IACSharpSensor.IACSharpSensor.SensorReached(1540);
            stateStack.Push(ParseState.Whitespace);
            IACSharpSensor.IACSharpSensor.SensorReached(1541);
            break;
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(1542);
            throw new ArgumentException("'" + tokens[i] + "'");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1543);
        state = stateStack.Peek();
        IACSharpSensor.IACSharpSensor.SensorReached(1544);
        if (state == ParseState.CriterionDone) {
          IACSharpSensor.IACSharpSensor.SensorReached(1545);
          stateStack.Pop();
          IACSharpSensor.IACSharpSensor.SensorReached(1546);
          if (stateStack.Peek() == ParseState.ConjunctionPending) {
            IACSharpSensor.IACSharpSensor.SensorReached(1547);
            while (stateStack.Peek() == ParseState.ConjunctionPending) {
              IACSharpSensor.IACSharpSensor.SensorReached(1548);
              var cc = critStack.Pop() as CompoundCriterion;
              cc.Right = current;
              current = cc;
              stateStack.Pop();
              state = stateStack.Pop();
              IACSharpSensor.IACSharpSensor.SensorReached(1549);
              if (state != ParseState.CriterionDone) {
                IACSharpSensor.IACSharpSensor.SensorReached(1550);
                throw new ArgumentException("??");
              }
            }
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1551);
            stateStack.Push(ParseState.CriterionDone);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1552);
        if (state == ParseState.Whitespace) {
          IACSharpSensor.IACSharpSensor.SensorReached(1553);
          stateStack.Pop();
        }
      }
      SelectionCriterion RNTRNTRNT_123 = current;
      IACSharpSensor.IACSharpSensor.SensorReached(1554);
      return RNTRNTRNT_123;
    }
    public override String ToString()
    {
      String RNTRNTRNT_124 = "FileSelector(" + _Criterion.ToString() + ")";
      IACSharpSensor.IACSharpSensor.SensorReached(1555);
      return RNTRNTRNT_124;
    }
    private bool Evaluate(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1556);
      SelectorTrace("Evaluate({0})", filename);
      bool result = _Criterion.Evaluate(filename);
      System.Boolean RNTRNTRNT_125 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(1557);
      return RNTRNTRNT_125;
    }
    [System.Diagnostics.Conditional("SelectorTrace")]
    private void SelectorTrace(string format, params object[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1558);
      if (_Criterion != null && _Criterion.Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(1559);
        System.Console.WriteLine(format, args);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1560);
    }
    public System.Collections.Generic.ICollection<String> SelectFiles(String directory)
    {
      System.Collections.Generic.ICollection<String> RNTRNTRNT_126 = SelectFiles(directory, false);
      IACSharpSensor.IACSharpSensor.SensorReached(1561);
      return RNTRNTRNT_126;
    }
    public System.Collections.ObjectModel.ReadOnlyCollection<String> SelectFiles(String directory, bool recurseDirectories)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1562);
      if (_Criterion == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1563);
        throw new ArgumentException("SelectionCriteria has not been set");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1564);
      var list = new List<String>();
      IACSharpSensor.IACSharpSensor.SensorReached(1565);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(1566);
        if (Directory.Exists(directory)) {
          IACSharpSensor.IACSharpSensor.SensorReached(1567);
          String[] filenames = Directory.GetFiles(directory);
          IACSharpSensor.IACSharpSensor.SensorReached(1568);
          foreach (String filename in filenames) {
            IACSharpSensor.IACSharpSensor.SensorReached(1569);
            if (Evaluate(filename)) {
              IACSharpSensor.IACSharpSensor.SensorReached(1570);
              list.Add(filename);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1571);
          if (recurseDirectories) {
            IACSharpSensor.IACSharpSensor.SensorReached(1572);
            String[] dirnames = Directory.GetDirectories(directory);
            IACSharpSensor.IACSharpSensor.SensorReached(1573);
            foreach (String dir in dirnames) {
              IACSharpSensor.IACSharpSensor.SensorReached(1574);
              if (this.TraverseReparsePoints || ((File.GetAttributes(dir) & FileAttributes.ReparsePoint) == 0)) {
                IACSharpSensor.IACSharpSensor.SensorReached(1575);
                if (Evaluate(dir)) {
                  IACSharpSensor.IACSharpSensor.SensorReached(1576);
                  list.Add(dir);
                }
                IACSharpSensor.IACSharpSensor.SensorReached(1577);
                list.AddRange(this.SelectFiles(dir, recurseDirectories));
              }
            }
          }
        }
      } catch (System.UnauthorizedAccessException) {
      } catch (System.IO.IOException) {
      }
      System.Collections.ObjectModel.ReadOnlyCollection<String> RNTRNTRNT_127 = list.AsReadOnly();
      IACSharpSensor.IACSharpSensor.SensorReached(1578);
      return RNTRNTRNT_127;
    }
  }
  internal sealed class EnumUtil
  {
    private EnumUtil()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1579);
    }
    static internal string GetDescription(System.Enum value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1580);
      FieldInfo fi = value.GetType().GetField(value.ToString());
      var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
      IACSharpSensor.IACSharpSensor.SensorReached(1581);
      if (attributes.Length > 0) {
        System.String RNTRNTRNT_128 = attributes[0].Description;
        IACSharpSensor.IACSharpSensor.SensorReached(1582);
        return RNTRNTRNT_128;
      } else {
        System.String RNTRNTRNT_129 = value.ToString();
        IACSharpSensor.IACSharpSensor.SensorReached(1583);
        return RNTRNTRNT_129;
      }
    }
    static internal object Parse(Type enumType, string stringRepresentation)
    {
      System.Object RNTRNTRNT_130 = Parse(enumType, stringRepresentation, false);
      IACSharpSensor.IACSharpSensor.SensorReached(1584);
      return RNTRNTRNT_130;
    }
    static internal object Parse(Type enumType, string stringRepresentation, bool ignoreCase)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1585);
      if (ignoreCase) {
        IACSharpSensor.IACSharpSensor.SensorReached(1586);
        stringRepresentation = stringRepresentation.ToLower();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1587);
      foreach (System.Enum enumVal in System.Enum.GetValues(enumType)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1588);
        string description = GetDescription(enumVal);
        IACSharpSensor.IACSharpSensor.SensorReached(1589);
        if (ignoreCase) {
          IACSharpSensor.IACSharpSensor.SensorReached(1590);
          description = description.ToLower();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1591);
        if (description == stringRepresentation) {
          System.Object RNTRNTRNT_131 = enumVal;
          IACSharpSensor.IACSharpSensor.SensorReached(1592);
          return RNTRNTRNT_131;
        }
      }
      System.Object RNTRNTRNT_132 = System.Enum.Parse(enumType, stringRepresentation, ignoreCase);
      IACSharpSensor.IACSharpSensor.SensorReached(1593);
      return RNTRNTRNT_132;
    }
  }
}
