using System;
using System.IO;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Zip
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00004")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public partial class ZipEntry
  {
    public ZipEntry()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1932);
      _CompressionMethod = (Int16)CompressionMethod.Deflate;
      _CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
      _Encryption = EncryptionAlgorithm.None;
      _Source = ZipEntrySource.None;
      AlternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
      AlternateEncodingUsage = ZipOption.Never;
      IACSharpSensor.IACSharpSensor.SensorReached(1933);
    }
    public DateTime LastModified {
      get {
        DateTime RNTRNTRNT_187 = _LastModified.ToLocalTime();
        IACSharpSensor.IACSharpSensor.SensorReached(1934);
        return RNTRNTRNT_187;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1935);
        _LastModified = (value.Kind == DateTimeKind.Unspecified) ? DateTime.SpecifyKind(value, DateTimeKind.Local) : value.ToLocalTime();
        _Mtime = Ionic.Zip.SharedUtilities.AdjustTime_Reverse(_LastModified).ToUniversalTime();
        _metadataChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1936);
      }
    }
    private int BufferSize {
      get {
        System.Int32 RNTRNTRNT_188 = this._container.BufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(1937);
        return RNTRNTRNT_188;
      }
    }
    public DateTime ModifiedTime {
      get {
        DateTime RNTRNTRNT_189 = _Mtime;
        IACSharpSensor.IACSharpSensor.SensorReached(1938);
        return RNTRNTRNT_189;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1939);
        SetEntryTimes(_Ctime, _Atime, value);
        IACSharpSensor.IACSharpSensor.SensorReached(1940);
      }
    }
    public DateTime AccessedTime {
      get {
        DateTime RNTRNTRNT_190 = _Atime;
        IACSharpSensor.IACSharpSensor.SensorReached(1941);
        return RNTRNTRNT_190;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1942);
        SetEntryTimes(_Ctime, value, _Mtime);
        IACSharpSensor.IACSharpSensor.SensorReached(1943);
      }
    }
    public DateTime CreationTime {
      get {
        DateTime RNTRNTRNT_191 = _Ctime;
        IACSharpSensor.IACSharpSensor.SensorReached(1944);
        return RNTRNTRNT_191;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1945);
        SetEntryTimes(value, _Atime, _Mtime);
        IACSharpSensor.IACSharpSensor.SensorReached(1946);
      }
    }
    public void SetEntryTimes(DateTime created, DateTime accessed, DateTime modified)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1947);
      _ntfsTimesAreSet = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1948);
      if (created == _zeroHour && created.Kind == _zeroHour.Kind) {
        IACSharpSensor.IACSharpSensor.SensorReached(1949);
        created = _win32Epoch;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1950);
      if (accessed == _zeroHour && accessed.Kind == _zeroHour.Kind) {
        IACSharpSensor.IACSharpSensor.SensorReached(1951);
        accessed = _win32Epoch;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1952);
      if (modified == _zeroHour && modified.Kind == _zeroHour.Kind) {
        IACSharpSensor.IACSharpSensor.SensorReached(1953);
        modified = _win32Epoch;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1954);
      _Ctime = created.ToUniversalTime();
      _Atime = accessed.ToUniversalTime();
      _Mtime = modified.ToUniversalTime();
      _LastModified = _Mtime;
      IACSharpSensor.IACSharpSensor.SensorReached(1955);
      if (!_emitUnixTimes && !_emitNtfsTimes) {
        IACSharpSensor.IACSharpSensor.SensorReached(1956);
        _emitNtfsTimes = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1957);
      _metadataChanged = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1958);
    }
    public bool EmitTimesInWindowsFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_192 = _emitNtfsTimes;
        IACSharpSensor.IACSharpSensor.SensorReached(1959);
        return RNTRNTRNT_192;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1960);
        _emitNtfsTimes = value;
        _metadataChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1961);
      }
    }
    public bool EmitTimesInUnixFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_193 = _emitUnixTimes;
        IACSharpSensor.IACSharpSensor.SensorReached(1962);
        return RNTRNTRNT_193;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1963);
        _emitUnixTimes = value;
        _metadataChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1964);
      }
    }
    public ZipEntryTimestamp Timestamp {
      get {
        ZipEntryTimestamp RNTRNTRNT_194 = _timestamp;
        IACSharpSensor.IACSharpSensor.SensorReached(1965);
        return RNTRNTRNT_194;
      }
    }
    public System.IO.FileAttributes Attributes {
      get {
        System.IO.FileAttributes RNTRNTRNT_195 = (System.IO.FileAttributes)_ExternalFileAttrs;
        IACSharpSensor.IACSharpSensor.SensorReached(1966);
        return RNTRNTRNT_195;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1967);
        _ExternalFileAttrs = (int)value;
        _VersionMadeBy = (0 << 8) + 45;
        _metadataChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1968);
      }
    }
    internal string LocalFileName {
      get {
        System.String RNTRNTRNT_196 = _LocalFileName;
        IACSharpSensor.IACSharpSensor.SensorReached(1969);
        return RNTRNTRNT_196;
      }
    }
    public string FileName {
      get {
        System.String RNTRNTRNT_197 = _FileNameInArchive;
        IACSharpSensor.IACSharpSensor.SensorReached(1970);
        return RNTRNTRNT_197;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1971);
        if (_container.ZipFile == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1972);
          throw new ZipException("Cannot rename; this is not supported in ZipOutputStream/ZipInputStream.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1973);
        if (String.IsNullOrEmpty(value)) {
          IACSharpSensor.IACSharpSensor.SensorReached(1974);
          throw new ZipException("The FileName must be non empty and non-null.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1975);
        var filename = ZipEntry.NameInArchive(value, null);
        IACSharpSensor.IACSharpSensor.SensorReached(1976);
        if (_FileNameInArchive == filename) {
          IACSharpSensor.IACSharpSensor.SensorReached(1977);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1978);
        this._container.ZipFile.RemoveEntry(this);
        this._container.ZipFile.InternalAddEntry(filename, this);
        _FileNameInArchive = filename;
        _container.ZipFile.NotifyEntryChanged();
        _metadataChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1979);
      }
    }
    public Stream InputStream {
      get {
        Stream RNTRNTRNT_198 = _sourceStream;
        IACSharpSensor.IACSharpSensor.SensorReached(1980);
        return RNTRNTRNT_198;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1981);
        if (this._Source != ZipEntrySource.Stream) {
          IACSharpSensor.IACSharpSensor.SensorReached(1982);
          throw new ZipException("You must not set the input stream for this entry.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1983);
        _sourceWasJitProvided = true;
        _sourceStream = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1984);
      }
    }
    public bool InputStreamWasJitProvided {
      get {
        System.Boolean RNTRNTRNT_199 = _sourceWasJitProvided;
        IACSharpSensor.IACSharpSensor.SensorReached(1985);
        return RNTRNTRNT_199;
      }
    }
    public ZipEntrySource Source {
      get {
        ZipEntrySource RNTRNTRNT_200 = _Source;
        IACSharpSensor.IACSharpSensor.SensorReached(1986);
        return RNTRNTRNT_200;
      }
    }
    public Int16 VersionNeeded {
      get {
        Int16 RNTRNTRNT_201 = _VersionNeeded;
        IACSharpSensor.IACSharpSensor.SensorReached(1987);
        return RNTRNTRNT_201;
      }
    }
    public string Comment {
      get {
        System.String RNTRNTRNT_202 = _Comment;
        IACSharpSensor.IACSharpSensor.SensorReached(1988);
        return RNTRNTRNT_202;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1989);
        _Comment = value;
        _metadataChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(1990);
      }
    }
    public Nullable<bool> RequiresZip64 {
      get {
        Nullable<System.Boolean> RNTRNTRNT_203 = _entryRequiresZip64;
        IACSharpSensor.IACSharpSensor.SensorReached(1991);
        return RNTRNTRNT_203;
      }
    }
    public Nullable<bool> OutputUsedZip64 {
      get {
        Nullable<System.Boolean> RNTRNTRNT_204 = _OutputUsesZip64;
        IACSharpSensor.IACSharpSensor.SensorReached(1992);
        return RNTRNTRNT_204;
      }
    }
    public Int16 BitField {
      get {
        Int16 RNTRNTRNT_205 = _BitField;
        IACSharpSensor.IACSharpSensor.SensorReached(1993);
        return RNTRNTRNT_205;
      }
    }
    public CompressionMethod CompressionMethod {
      get {
        CompressionMethod RNTRNTRNT_206 = (CompressionMethod)_CompressionMethod;
        IACSharpSensor.IACSharpSensor.SensorReached(1994);
        return RNTRNTRNT_206;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1995);
        if (value == (CompressionMethod)_CompressionMethod) {
          IACSharpSensor.IACSharpSensor.SensorReached(1996);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1997);
        if (value != CompressionMethod.None && value != CompressionMethod.Deflate) {
          IACSharpSensor.IACSharpSensor.SensorReached(1998);
          throw new InvalidOperationException("Unsupported compression method.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1999);
        _CompressionMethod = (Int16)value;
        IACSharpSensor.IACSharpSensor.SensorReached(2000);
        if (_CompressionMethod == (Int16)Ionic.Zip.CompressionMethod.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(2001);
          _CompressionLevel = Ionic.Zlib.CompressionLevel.None;
        } else if (CompressionLevel == Ionic.Zlib.CompressionLevel.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(2002);
          _CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2003);
        if (_container.ZipFile != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2004);
          _container.ZipFile.NotifyEntryChanged();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2005);
        _restreamRequiredOnSave = true;
        IACSharpSensor.IACSharpSensor.SensorReached(2006);
      }
    }
    public Ionic.Zlib.CompressionLevel CompressionLevel {
      get {
        Ionic.Zlib.CompressionLevel RNTRNTRNT_207 = _CompressionLevel;
        IACSharpSensor.IACSharpSensor.SensorReached(2007);
        return RNTRNTRNT_207;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2008);
        if (_CompressionMethod != (short)CompressionMethod.Deflate && _CompressionMethod != (short)CompressionMethod.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(2009);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2010);
        if (value == Ionic.Zlib.CompressionLevel.Default && _CompressionMethod == (short)CompressionMethod.Deflate) {
          IACSharpSensor.IACSharpSensor.SensorReached(2011);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2012);
        _CompressionLevel = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2013);
        if (value == Ionic.Zlib.CompressionLevel.None && _CompressionMethod == (short)CompressionMethod.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(2014);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2015);
        if (_CompressionLevel == Ionic.Zlib.CompressionLevel.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(2016);
          _CompressionMethod = (short)Ionic.Zip.CompressionMethod.None;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2017);
          _CompressionMethod = (short)Ionic.Zip.CompressionMethod.Deflate;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2018);
        if (_container.ZipFile != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2019);
          _container.ZipFile.NotifyEntryChanged();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2020);
        _restreamRequiredOnSave = true;
        IACSharpSensor.IACSharpSensor.SensorReached(2021);
      }
    }
    public Int64 CompressedSize {
      get {
        Int64 RNTRNTRNT_208 = _CompressedSize;
        IACSharpSensor.IACSharpSensor.SensorReached(2022);
        return RNTRNTRNT_208;
      }
    }
    public Int64 UncompressedSize {
      get {
        Int64 RNTRNTRNT_209 = _UncompressedSize;
        IACSharpSensor.IACSharpSensor.SensorReached(2023);
        return RNTRNTRNT_209;
      }
    }
    public Double CompressionRatio {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2024);
        if (UncompressedSize == 0) {
          Double RNTRNTRNT_210 = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(2025);
          return RNTRNTRNT_210;
        }
        Double RNTRNTRNT_211 = 100 * (1.0 - (1.0 * CompressedSize) / (1.0 * UncompressedSize));
        IACSharpSensor.IACSharpSensor.SensorReached(2026);
        return RNTRNTRNT_211;
      }
    }
    public Int32 Crc {
      get {
        Int32 RNTRNTRNT_212 = _Crc32;
        IACSharpSensor.IACSharpSensor.SensorReached(2027);
        return RNTRNTRNT_212;
      }
    }
    public bool IsDirectory {
      get {
        System.Boolean RNTRNTRNT_213 = _IsDirectory;
        IACSharpSensor.IACSharpSensor.SensorReached(2028);
        return RNTRNTRNT_213;
      }
    }
    public bool UsesEncryption {
      get {
        System.Boolean RNTRNTRNT_214 = (_Encryption_FromZipFile != EncryptionAlgorithm.None);
        IACSharpSensor.IACSharpSensor.SensorReached(2029);
        return RNTRNTRNT_214;
      }
    }
    public EncryptionAlgorithm Encryption {
      get {
        EncryptionAlgorithm RNTRNTRNT_215 = _Encryption;
        IACSharpSensor.IACSharpSensor.SensorReached(2030);
        return RNTRNTRNT_215;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2031);
        if (value == _Encryption) {
          IACSharpSensor.IACSharpSensor.SensorReached(2032);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2033);
        if (value == EncryptionAlgorithm.Unsupported) {
          IACSharpSensor.IACSharpSensor.SensorReached(2034);
          throw new InvalidOperationException("You may not set Encryption to that value.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2035);
        _Encryption = value;
        _restreamRequiredOnSave = true;
        IACSharpSensor.IACSharpSensor.SensorReached(2036);
        if (_container.ZipFile != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2037);
          _container.ZipFile.NotifyEntryChanged();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2038);
      }
    }
    public string Password {
      private get {
        System.String RNTRNTRNT_216 = _Password;
        IACSharpSensor.IACSharpSensor.SensorReached(2039);
        return RNTRNTRNT_216;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2040);
        _Password = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2041);
        if (_Password == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2042);
          _Encryption = EncryptionAlgorithm.None;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2043);
          if (this._Source == ZipEntrySource.ZipFile && !_sourceIsEncrypted) {
            IACSharpSensor.IACSharpSensor.SensorReached(2044);
            _restreamRequiredOnSave = true;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2045);
          if (Encryption == EncryptionAlgorithm.None) {
            IACSharpSensor.IACSharpSensor.SensorReached(2046);
            _Encryption = EncryptionAlgorithm.PkzipWeak;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2047);
      }
    }
    internal bool IsChanged {
      get {
        System.Boolean RNTRNTRNT_217 = _restreamRequiredOnSave | _metadataChanged;
        IACSharpSensor.IACSharpSensor.SensorReached(2048);
        return RNTRNTRNT_217;
      }
    }
    public ExtractExistingFileAction ExtractExistingFile { get; set; }
    public ZipErrorAction ZipErrorAction { get; set; }
    public bool IncludedInMostRecentSave {
      get {
        System.Boolean RNTRNTRNT_218 = !_skippedDuringSave;
        IACSharpSensor.IACSharpSensor.SensorReached(2049);
        return RNTRNTRNT_218;
      }
    }
    public SetCompressionCallback SetCompression { get; set; }
    [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete.  It will be removed in a future version of the library. Your applications should  use AlternateEncoding and AlternateEncodingUsage instead.")]
    public bool UseUnicodeAsNecessary {
      get {
        System.Boolean RNTRNTRNT_219 = (AlternateEncoding == System.Text.Encoding.GetEncoding("UTF-8")) && (AlternateEncodingUsage == ZipOption.AsNecessary);
        IACSharpSensor.IACSharpSensor.SensorReached(2050);
        return RNTRNTRNT_219;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2051);
        if (value) {
          IACSharpSensor.IACSharpSensor.SensorReached(2052);
          AlternateEncoding = System.Text.Encoding.GetEncoding("UTF-8");
          AlternateEncodingUsage = ZipOption.AsNecessary;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2053);
          AlternateEncoding = Ionic.Zip.ZipFile.DefaultEncoding;
          AlternateEncodingUsage = ZipOption.Never;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2054);
      }
    }
    [Obsolete("This property is obsolete since v1.9.1.6. Use AlternateEncoding and AlternateEncodingUsage instead.", true)]
    public System.Text.Encoding ProvisionalAlternateEncoding { get; set; }
    public System.Text.Encoding AlternateEncoding { get; set; }
    public ZipOption AlternateEncodingUsage { get; set; }
    static internal string NameInArchive(String filename, string directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2055);
      string result = null;
      IACSharpSensor.IACSharpSensor.SensorReached(2056);
      if (directoryPathInArchive == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2057);
        result = filename;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2058);
        if (String.IsNullOrEmpty(directoryPathInArchive)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2059);
          result = Path.GetFileName(filename);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2060);
          result = Path.Combine(directoryPathInArchive, Path.GetFileName(filename));
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2061);
      result = SharedUtilities.NormalizePathForUseInZipFile(result);
      System.String RNTRNTRNT_220 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(2062);
      return RNTRNTRNT_220;
    }
    static internal ZipEntry CreateFromNothing(String nameInArchive)
    {
      ZipEntry RNTRNTRNT_221 = Create(nameInArchive, ZipEntrySource.None, null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2063);
      return RNTRNTRNT_221;
    }
    static internal ZipEntry CreateFromFile(String filename, string nameInArchive)
    {
      ZipEntry RNTRNTRNT_222 = Create(nameInArchive, ZipEntrySource.FileSystem, filename, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2064);
      return RNTRNTRNT_222;
    }
    static internal ZipEntry CreateForStream(String entryName, Stream s)
    {
      ZipEntry RNTRNTRNT_223 = Create(entryName, ZipEntrySource.Stream, s, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2065);
      return RNTRNTRNT_223;
    }
    static internal ZipEntry CreateForWriter(String entryName, WriteDelegate d)
    {
      ZipEntry RNTRNTRNT_224 = Create(entryName, ZipEntrySource.WriteDelegate, d, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2066);
      return RNTRNTRNT_224;
    }
    static internal ZipEntry CreateForJitStreamProvider(string nameInArchive, OpenDelegate opener, CloseDelegate closer)
    {
      ZipEntry RNTRNTRNT_225 = Create(nameInArchive, ZipEntrySource.JitStream, opener, closer);
      IACSharpSensor.IACSharpSensor.SensorReached(2067);
      return RNTRNTRNT_225;
    }
    static internal ZipEntry CreateForZipOutputStream(string nameInArchive)
    {
      ZipEntry RNTRNTRNT_226 = Create(nameInArchive, ZipEntrySource.ZipOutputStream, null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2068);
      return RNTRNTRNT_226;
    }
    private static ZipEntry Create(string nameInArchive, ZipEntrySource source, Object arg1, Object arg2)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2069);
      if (String.IsNullOrEmpty(nameInArchive)) {
        IACSharpSensor.IACSharpSensor.SensorReached(2070);
        throw new Ionic.Zip.ZipException("The entry name must be non-null and non-empty.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2071);
      ZipEntry entry = new ZipEntry();
      entry._VersionMadeBy = (0 << 8) + 45;
      entry._Source = source;
      entry._Mtime = entry._Atime = entry._Ctime = DateTime.UtcNow;
      IACSharpSensor.IACSharpSensor.SensorReached(2072);
      if (source == ZipEntrySource.Stream) {
        IACSharpSensor.IACSharpSensor.SensorReached(2073);
        entry._sourceStream = (arg1 as Stream);
      } else if (source == ZipEntrySource.WriteDelegate) {
        IACSharpSensor.IACSharpSensor.SensorReached(2087);
        entry._WriteDelegate = (arg1 as WriteDelegate);
      } else if (source == ZipEntrySource.JitStream) {
        IACSharpSensor.IACSharpSensor.SensorReached(2086);
        entry._OpenDelegate = (arg1 as OpenDelegate);
        entry._CloseDelegate = (arg2 as CloseDelegate);
      } else if (source == ZipEntrySource.ZipOutputStream) {
      } else if (source == ZipEntrySource.None) {
        IACSharpSensor.IACSharpSensor.SensorReached(2085);
        entry._Source = ZipEntrySource.FileSystem;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2074);
        String filename = (arg1 as String);
        IACSharpSensor.IACSharpSensor.SensorReached(2075);
        if (String.IsNullOrEmpty(filename)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2076);
          throw new Ionic.Zip.ZipException("The filename must be non-null and non-empty.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2077);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(2078);
          entry._Mtime = File.GetLastWriteTime(filename).ToUniversalTime();
          entry._Ctime = File.GetCreationTime(filename).ToUniversalTime();
          entry._Atime = File.GetLastAccessTime(filename).ToUniversalTime();
          IACSharpSensor.IACSharpSensor.SensorReached(2079);
          if (File.Exists(filename) || Directory.Exists(filename)) {
            IACSharpSensor.IACSharpSensor.SensorReached(2080);
            entry._ExternalFileAttrs = (int)File.GetAttributes(filename);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2081);
          entry._ntfsTimesAreSet = true;
          entry._LocalFileName = Path.GetFullPath(filename);
        } catch (System.IO.PathTooLongException ptle) {
          IACSharpSensor.IACSharpSensor.SensorReached(2082);
          var msg = String.Format("The path is too long, filename={0}", filename);
          IACSharpSensor.IACSharpSensor.SensorReached(2083);
          throw new ZipException(msg, ptle);
          IACSharpSensor.IACSharpSensor.SensorReached(2084);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2088);
      entry._LastModified = entry._Mtime;
      entry._FileNameInArchive = SharedUtilities.NormalizePathForUseInZipFile(nameInArchive);
      ZipEntry RNTRNTRNT_227 = entry;
      IACSharpSensor.IACSharpSensor.SensorReached(2089);
      return RNTRNTRNT_227;
    }
    internal void MarkAsDirectory()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2090);
      _IsDirectory = true;
      IACSharpSensor.IACSharpSensor.SensorReached(2091);
      if (!_FileNameInArchive.EndsWith("/")) {
        IACSharpSensor.IACSharpSensor.SensorReached(2092);
        _FileNameInArchive += "/";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2093);
    }
    public bool IsText {
      get {
        System.Boolean RNTRNTRNT_228 = _IsText;
        IACSharpSensor.IACSharpSensor.SensorReached(2094);
        return RNTRNTRNT_228;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(2095);
        _IsText = value;
        IACSharpSensor.IACSharpSensor.SensorReached(2096);
      }
    }
    public override String ToString()
    {
      String RNTRNTRNT_229 = String.Format("ZipEntry::{0}", FileName);
      IACSharpSensor.IACSharpSensor.SensorReached(2097);
      return RNTRNTRNT_229;
    }
    internal Stream ArchiveStream {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2098);
        if (_archiveStream == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2099);
          if (_container.ZipFile != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2100);
            var zf = _container.ZipFile;
            zf.Reset(false);
            _archiveStream = zf.StreamForDiskNumber(_diskNumber);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2101);
            _archiveStream = _container.ZipOutputStream.OutputStream;
          }
        }
        Stream RNTRNTRNT_230 = _archiveStream;
        IACSharpSensor.IACSharpSensor.SensorReached(2102);
        return RNTRNTRNT_230;
      }
    }
    private void SetFdpLoh()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2103);
      long origPosition = this.ArchiveStream.Position;
      IACSharpSensor.IACSharpSensor.SensorReached(2104);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2105);
        this.ArchiveStream.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      } catch (System.IO.IOException exc1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2106);
        string description = String.Format("Exception seeking  entry({0}) offset(0x{1:X8}) len(0x{2:X8})", this.FileName, this._RelativeOffsetOfLocalHeader, this.ArchiveStream.Length);
        IACSharpSensor.IACSharpSensor.SensorReached(2107);
        throw new BadStateException(description, exc1);
        IACSharpSensor.IACSharpSensor.SensorReached(2108);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2109);
      byte[] block = new byte[30];
      this.ArchiveStream.Read(block, 0, block.Length);
      Int16 filenameLength = (short)(block[26] + block[27] * 256);
      Int16 extraFieldLength = (short)(block[28] + block[29] * 256);
      this.ArchiveStream.Seek(filenameLength + extraFieldLength, SeekOrigin.Current);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      this._LengthOfHeader = 30 + extraFieldLength + filenameLength + GetLengthOfCryptoHeaderBytes(_Encryption_FromZipFile);
      this.__FileDataPosition = _RelativeOffsetOfLocalHeader + _LengthOfHeader;
      this.ArchiveStream.Seek(origPosition, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      IACSharpSensor.IACSharpSensor.SensorReached(2110);
    }
    static internal int GetLengthOfCryptoHeaderBytes(EncryptionAlgorithm a)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2111);
      if (a == EncryptionAlgorithm.None) {
        System.Int32 RNTRNTRNT_231 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(2112);
        return RNTRNTRNT_231;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2113);
      if (a == EncryptionAlgorithm.PkzipWeak) {
        System.Int32 RNTRNTRNT_232 = 12;
        IACSharpSensor.IACSharpSensor.SensorReached(2114);
        return RNTRNTRNT_232;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2115);
      throw new ZipException("internal error");
    }
    internal long FileDataPosition {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2116);
        if (__FileDataPosition == -1) {
          IACSharpSensor.IACSharpSensor.SensorReached(2117);
          SetFdpLoh();
        }
        System.Int64 RNTRNTRNT_233 = __FileDataPosition;
        IACSharpSensor.IACSharpSensor.SensorReached(2118);
        return RNTRNTRNT_233;
      }
    }
    private int LengthOfHeader {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2119);
        if (_LengthOfHeader == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2120);
          SetFdpLoh();
        }
        System.Int32 RNTRNTRNT_234 = _LengthOfHeader;
        IACSharpSensor.IACSharpSensor.SensorReached(2121);
        return RNTRNTRNT_234;
      }
    }
    private ZipCrypto _zipCrypto_forExtract;
    private ZipCrypto _zipCrypto_forWrite;
    internal DateTime _LastModified;
    private DateTime _Mtime, _Atime, _Ctime;
    private bool _ntfsTimesAreSet;
    private bool _emitNtfsTimes = true;
    private bool _emitUnixTimes;
    private bool _TrimVolumeFromFullyQualifiedPaths = true;
    internal string _LocalFileName;
    private string _FileNameInArchive;
    internal Int16 _VersionNeeded;
    internal Int16 _BitField;
    internal Int16 _CompressionMethod;
    private Int16 _CompressionMethod_FromZipFile;
    private Ionic.Zlib.CompressionLevel _CompressionLevel;
    internal string _Comment;
    private bool _IsDirectory;
    private byte[] _CommentBytes;
    internal Int64 _CompressedSize;
    internal Int64 _CompressedFileDataSize;
    internal Int64 _UncompressedSize;
    internal Int32 _TimeBlob;
    private bool _crcCalculated;
    internal Int32 _Crc32;
    internal byte[] _Extra;
    private bool _metadataChanged;
    private bool _restreamRequiredOnSave;
    private bool _sourceIsEncrypted;
    private bool _skippedDuringSave;
    private UInt32 _diskNumber;
    private static System.Text.Encoding ibm437 = System.Text.Encoding.GetEncoding("IBM437");
    private System.Text.Encoding _actualEncoding;
    internal ZipContainer _container;
    private long __FileDataPosition = -1;
    private byte[] _EntryHeader;
    internal Int64 _RelativeOffsetOfLocalHeader;
    private Int64 _future_ROLH;
    private Int64 _TotalEntrySize;
    private int _LengthOfHeader;
    private int _LengthOfTrailer;
    internal bool _InputUsesZip64;
    private UInt32 _UnsupportedAlgorithmId;
    internal string _Password;
    internal ZipEntrySource _Source;
    internal EncryptionAlgorithm _Encryption;
    internal EncryptionAlgorithm _Encryption_FromZipFile;
    internal byte[] _WeakEncryptionHeader;
    internal Stream _archiveStream;
    private Stream _sourceStream;
    private Nullable<Int64> _sourceStreamOriginalPosition;
    private bool _sourceWasJitProvided;
    private bool _ioOperationCanceled;
    private bool _presumeZip64;
    private Nullable<bool> _entryRequiresZip64;
    private Nullable<bool> _OutputUsesZip64;
    private bool _IsText;
    private ZipEntryTimestamp _timestamp;
    private static System.DateTime _unixEpoch = new System.DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    private static System.DateTime _win32Epoch = System.DateTime.FromFileTimeUtc(0L);
    private static System.DateTime _zeroHour = new System.DateTime(1, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    private WriteDelegate _WriteDelegate;
    private OpenDelegate _OpenDelegate;
    private CloseDelegate _CloseDelegate;
  }
  [Flags()]
  public enum ZipEntryTimestamp
  {
    None = 0,
    DOS = 1,
    Windows = 2,
    Unix = 4,
    InfoZip1 = 8
  }
  public enum CompressionMethod
  {
    None = 0,
    Deflate = 8
  }
}
