using Interop = System.Runtime.InteropServices;
namespace Ionic.Zip
{
  [System.Runtime.InteropServices.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000F")]
  [System.Runtime.InteropServices.ComVisible(true)]
  [System.Runtime.InteropServices.ClassInterface(System.Runtime.InteropServices.ClassInterfaceType.AutoDispatch)]
  public class ComHelper
  {
    public bool IsZipFile(string filename)
    {
      System.Boolean RNTRNTRNT_65 = ZipFile.IsZipFile(filename);
      IACSharpSensor.IACSharpSensor.SensorReached(1173);
      return RNTRNTRNT_65;
    }
    public bool IsZipFileWithExtract(string filename)
    {
      System.Boolean RNTRNTRNT_66 = ZipFile.IsZipFile(filename, true);
      IACSharpSensor.IACSharpSensor.SensorReached(1174);
      return RNTRNTRNT_66;
    }
    public bool CheckZip(string filename)
    {
      System.Boolean RNTRNTRNT_67 = ZipFile.CheckZip(filename);
      IACSharpSensor.IACSharpSensor.SensorReached(1175);
      return RNTRNTRNT_67;
    }
    public bool CheckZipPassword(string filename, string password)
    {
      System.Boolean RNTRNTRNT_68 = ZipFile.CheckZipPassword(filename, password);
      IACSharpSensor.IACSharpSensor.SensorReached(1176);
      return RNTRNTRNT_68;
    }
    public void FixZipDirectory(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1177);
      ZipFile.FixZipDirectory(filename);
      IACSharpSensor.IACSharpSensor.SensorReached(1178);
    }
    public string GetZipLibraryVersion()
    {
      System.String RNTRNTRNT_69 = ZipFile.LibraryVersion.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(1179);
      return RNTRNTRNT_69;
    }
  }
}
