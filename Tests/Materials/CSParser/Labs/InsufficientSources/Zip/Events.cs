using System;
using System.Collections.Generic;
using System.Text;
namespace Ionic.Zip
{
  public delegate void WriteDelegate(string entryName, System.IO.Stream stream);
  public delegate System.IO.Stream OpenDelegate(string entryName);
  public delegate void CloseDelegate(string entryName, System.IO.Stream stream);
  public delegate Ionic.Zlib.CompressionLevel SetCompressionCallback(string localFileName, string fileNameInArchive);
  public enum ZipProgressEventType
  {
    Adding_Started,
    Adding_AfterAddEntry,
    Adding_Completed,
    Reading_Started,
    Reading_BeforeReadEntry,
    Reading_AfterReadEntry,
    Reading_Completed,
    Reading_ArchiveBytesRead,
    Saving_Started,
    Saving_BeforeWriteEntry,
    Saving_AfterWriteEntry,
    Saving_Completed,
    Saving_AfterSaveTempArchive,
    Saving_BeforeRenameTempArchive,
    Saving_AfterRenameTempArchive,
    Saving_AfterCompileSelfExtractor,
    Saving_EntryBytesRead,
    Extracting_BeforeExtractEntry,
    Extracting_AfterExtractEntry,
    Extracting_ExtractEntryWouldOverwrite,
    Extracting_EntryBytesWritten,
    Extracting_BeforeExtractAll,
    Extracting_AfterExtractAll,
    Error_Saving
  }
  public class ZipProgressEventArgs : EventArgs
  {
    private int _entriesTotal;
    private bool _cancel;
    private ZipEntry _latestEntry;
    private ZipProgressEventType _flavor;
    private String _archiveName;
    private Int64 _bytesTransferred;
    private Int64 _totalBytesToTransfer;
    internal ZipProgressEventArgs()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1180);
    }
    internal ZipProgressEventArgs(string archiveName, ZipProgressEventType flavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1181);
      this._archiveName = archiveName;
      this._flavor = flavor;
      IACSharpSensor.IACSharpSensor.SensorReached(1182);
    }
    public int EntriesTotal {
      get {
        System.Int32 RNTRNTRNT_70 = _entriesTotal;
        IACSharpSensor.IACSharpSensor.SensorReached(1183);
        return RNTRNTRNT_70;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1184);
        _entriesTotal = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1185);
      }
    }
    public ZipEntry CurrentEntry {
      get {
        ZipEntry RNTRNTRNT_71 = _latestEntry;
        IACSharpSensor.IACSharpSensor.SensorReached(1186);
        return RNTRNTRNT_71;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1187);
        _latestEntry = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1188);
      }
    }
    public bool Cancel {
      get {
        System.Boolean RNTRNTRNT_72 = _cancel;
        IACSharpSensor.IACSharpSensor.SensorReached(1189);
        return RNTRNTRNT_72;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1190);
        _cancel = _cancel || value;
        IACSharpSensor.IACSharpSensor.SensorReached(1191);
      }
    }
    public ZipProgressEventType EventType {
      get {
        ZipProgressEventType RNTRNTRNT_73 = _flavor;
        IACSharpSensor.IACSharpSensor.SensorReached(1192);
        return RNTRNTRNT_73;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1193);
        _flavor = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1194);
      }
    }
    public String ArchiveName {
      get {
        String RNTRNTRNT_74 = _archiveName;
        IACSharpSensor.IACSharpSensor.SensorReached(1195);
        return RNTRNTRNT_74;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1196);
        _archiveName = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1197);
      }
    }
    public Int64 BytesTransferred {
      get {
        Int64 RNTRNTRNT_75 = _bytesTransferred;
        IACSharpSensor.IACSharpSensor.SensorReached(1198);
        return RNTRNTRNT_75;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1199);
        _bytesTransferred = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1200);
      }
    }
    public Int64 TotalBytesToTransfer {
      get {
        Int64 RNTRNTRNT_76 = _totalBytesToTransfer;
        IACSharpSensor.IACSharpSensor.SensorReached(1201);
        return RNTRNTRNT_76;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1202);
        _totalBytesToTransfer = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1203);
      }
    }
  }
  public class ReadProgressEventArgs : ZipProgressEventArgs
  {
    internal ReadProgressEventArgs()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1204);
    }
    private ReadProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1205);
    }
    static internal ReadProgressEventArgs Before(string archiveName, int entriesTotal)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1206);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_BeforeReadEntry);
      x.EntriesTotal = entriesTotal;
      ReadProgressEventArgs RNTRNTRNT_77 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1207);
      return RNTRNTRNT_77;
    }
    static internal ReadProgressEventArgs After(string archiveName, ZipEntry entry, int entriesTotal)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1208);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_AfterReadEntry);
      x.EntriesTotal = entriesTotal;
      x.CurrentEntry = entry;
      ReadProgressEventArgs RNTRNTRNT_78 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1209);
      return RNTRNTRNT_78;
    }
    static internal ReadProgressEventArgs Started(string archiveName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1210);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_Started);
      ReadProgressEventArgs RNTRNTRNT_79 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1211);
      return RNTRNTRNT_79;
    }
    static internal ReadProgressEventArgs ByteUpdate(string archiveName, ZipEntry entry, Int64 bytesXferred, Int64 totalBytes)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1212);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_ArchiveBytesRead);
      x.CurrentEntry = entry;
      x.BytesTransferred = bytesXferred;
      x.TotalBytesToTransfer = totalBytes;
      ReadProgressEventArgs RNTRNTRNT_80 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1213);
      return RNTRNTRNT_80;
    }
    static internal ReadProgressEventArgs Completed(string archiveName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1214);
      var x = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_Completed);
      ReadProgressEventArgs RNTRNTRNT_81 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1215);
      return RNTRNTRNT_81;
    }
  }
  public class AddProgressEventArgs : ZipProgressEventArgs
  {
    internal AddProgressEventArgs()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1216);
    }
    private AddProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1217);
    }
    static internal AddProgressEventArgs AfterEntry(string archiveName, ZipEntry entry, int entriesTotal)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1218);
      var x = new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_AfterAddEntry);
      x.EntriesTotal = entriesTotal;
      x.CurrentEntry = entry;
      AddProgressEventArgs RNTRNTRNT_82 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1219);
      return RNTRNTRNT_82;
    }
    static internal AddProgressEventArgs Started(string archiveName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1220);
      var x = new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_Started);
      AddProgressEventArgs RNTRNTRNT_83 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1221);
      return RNTRNTRNT_83;
    }
    static internal AddProgressEventArgs Completed(string archiveName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1222);
      var x = new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_Completed);
      AddProgressEventArgs RNTRNTRNT_84 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1223);
      return RNTRNTRNT_84;
    }
  }
  public class SaveProgressEventArgs : ZipProgressEventArgs
  {
    private int _entriesSaved;
    internal SaveProgressEventArgs(string archiveName, bool before, int entriesTotal, int entriesSaved, ZipEntry entry) : base(archiveName, (before) ? ZipProgressEventType.Saving_BeforeWriteEntry : ZipProgressEventType.Saving_AfterWriteEntry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1224);
      this.EntriesTotal = entriesTotal;
      this.CurrentEntry = entry;
      this._entriesSaved = entriesSaved;
      IACSharpSensor.IACSharpSensor.SensorReached(1225);
    }
    internal SaveProgressEventArgs()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1226);
    }
    internal SaveProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1227);
    }
    static internal SaveProgressEventArgs ByteUpdate(string archiveName, ZipEntry entry, Int64 bytesXferred, Int64 totalBytes)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1228);
      var x = new SaveProgressEventArgs(archiveName, ZipProgressEventType.Saving_EntryBytesRead);
      x.ArchiveName = archiveName;
      x.CurrentEntry = entry;
      x.BytesTransferred = bytesXferred;
      x.TotalBytesToTransfer = totalBytes;
      SaveProgressEventArgs RNTRNTRNT_85 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1229);
      return RNTRNTRNT_85;
    }
    static internal SaveProgressEventArgs Started(string archiveName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1230);
      var x = new SaveProgressEventArgs(archiveName, ZipProgressEventType.Saving_Started);
      SaveProgressEventArgs RNTRNTRNT_86 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1231);
      return RNTRNTRNT_86;
    }
    static internal SaveProgressEventArgs Completed(string archiveName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1232);
      var x = new SaveProgressEventArgs(archiveName, ZipProgressEventType.Saving_Completed);
      SaveProgressEventArgs RNTRNTRNT_87 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1233);
      return RNTRNTRNT_87;
    }
    public int EntriesSaved {
      get {
        System.Int32 RNTRNTRNT_88 = _entriesSaved;
        IACSharpSensor.IACSharpSensor.SensorReached(1234);
        return RNTRNTRNT_88;
      }
    }
  }
  public class ExtractProgressEventArgs : ZipProgressEventArgs
  {
    private int _entriesExtracted;
    private string _target;
    internal ExtractProgressEventArgs(string archiveName, bool before, int entriesTotal, int entriesExtracted, ZipEntry entry, string extractLocation) : base(archiveName, (before) ? ZipProgressEventType.Extracting_BeforeExtractEntry : ZipProgressEventType.Extracting_AfterExtractEntry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1235);
      this.EntriesTotal = entriesTotal;
      this.CurrentEntry = entry;
      this._entriesExtracted = entriesExtracted;
      this._target = extractLocation;
      IACSharpSensor.IACSharpSensor.SensorReached(1236);
    }
    internal ExtractProgressEventArgs(string archiveName, ZipProgressEventType flavor) : base(archiveName, flavor)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1237);
    }
    internal ExtractProgressEventArgs()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1238);
    }
    static internal ExtractProgressEventArgs BeforeExtractEntry(string archiveName, ZipEntry entry, string extractLocation)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1239);
      var x = new ExtractProgressEventArgs {
        ArchiveName = archiveName,
        EventType = ZipProgressEventType.Extracting_BeforeExtractEntry,
        CurrentEntry = entry,
        _target = extractLocation
      };
      ExtractProgressEventArgs RNTRNTRNT_89 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1240);
      return RNTRNTRNT_89;
    }
    static internal ExtractProgressEventArgs ExtractExisting(string archiveName, ZipEntry entry, string extractLocation)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1241);
      var x = new ExtractProgressEventArgs {
        ArchiveName = archiveName,
        EventType = ZipProgressEventType.Extracting_ExtractEntryWouldOverwrite,
        CurrentEntry = entry,
        _target = extractLocation
      };
      ExtractProgressEventArgs RNTRNTRNT_90 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1242);
      return RNTRNTRNT_90;
    }
    static internal ExtractProgressEventArgs AfterExtractEntry(string archiveName, ZipEntry entry, string extractLocation)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1243);
      var x = new ExtractProgressEventArgs {
        ArchiveName = archiveName,
        EventType = ZipProgressEventType.Extracting_AfterExtractEntry,
        CurrentEntry = entry,
        _target = extractLocation
      };
      ExtractProgressEventArgs RNTRNTRNT_91 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1244);
      return RNTRNTRNT_91;
    }
    static internal ExtractProgressEventArgs ExtractAllStarted(string archiveName, string extractLocation)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1245);
      var x = new ExtractProgressEventArgs(archiveName, ZipProgressEventType.Extracting_BeforeExtractAll);
      x._target = extractLocation;
      ExtractProgressEventArgs RNTRNTRNT_92 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1246);
      return RNTRNTRNT_92;
    }
    static internal ExtractProgressEventArgs ExtractAllCompleted(string archiveName, string extractLocation)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1247);
      var x = new ExtractProgressEventArgs(archiveName, ZipProgressEventType.Extracting_AfterExtractAll);
      x._target = extractLocation;
      ExtractProgressEventArgs RNTRNTRNT_93 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1248);
      return RNTRNTRNT_93;
    }
    static internal ExtractProgressEventArgs ByteUpdate(string archiveName, ZipEntry entry, Int64 bytesWritten, Int64 totalBytes)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1249);
      var x = new ExtractProgressEventArgs(archiveName, ZipProgressEventType.Extracting_EntryBytesWritten);
      x.ArchiveName = archiveName;
      x.CurrentEntry = entry;
      x.BytesTransferred = bytesWritten;
      x.TotalBytesToTransfer = totalBytes;
      ExtractProgressEventArgs RNTRNTRNT_94 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1250);
      return RNTRNTRNT_94;
    }
    public int EntriesExtracted {
      get {
        System.Int32 RNTRNTRNT_95 = _entriesExtracted;
        IACSharpSensor.IACSharpSensor.SensorReached(1251);
        return RNTRNTRNT_95;
      }
    }
    public String ExtractLocation {
      get {
        String RNTRNTRNT_96 = _target;
        IACSharpSensor.IACSharpSensor.SensorReached(1252);
        return RNTRNTRNT_96;
      }
    }
  }
  public class ZipErrorEventArgs : ZipProgressEventArgs
  {
    private Exception _exc;
    private ZipErrorEventArgs()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1253);
    }
    static internal ZipErrorEventArgs Saving(string archiveName, ZipEntry entry, Exception exception)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1254);
      var x = new ZipErrorEventArgs {
        EventType = ZipProgressEventType.Error_Saving,
        ArchiveName = archiveName,
        CurrentEntry = entry,
        _exc = exception
      };
      ZipErrorEventArgs RNTRNTRNT_97 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1255);
      return RNTRNTRNT_97;
    }
    public Exception Exception {
      get {
        Exception RNTRNTRNT_98 = _exc;
        IACSharpSensor.IACSharpSensor.SensorReached(1256);
        return RNTRNTRNT_98;
      }
    }
    public String FileName {
      get {
        String RNTRNTRNT_99 = CurrentEntry.LocalFileName;
        IACSharpSensor.IACSharpSensor.SensorReached(1257);
        return RNTRNTRNT_99;
      }
    }
  }
}
