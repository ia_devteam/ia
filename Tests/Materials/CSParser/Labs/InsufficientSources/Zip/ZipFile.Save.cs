using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    private void DeleteFileWithRetry(string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3683);
      bool done = false;
      int nRetries = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(3684);
      for (int i = 0; i < nRetries && !done; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3685);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(3686);
          File.Delete(filename);
          done = true;
        } catch (System.UnauthorizedAccessException) {
          IACSharpSensor.IACSharpSensor.SensorReached(3687);
          Console.WriteLine("************************************************** Retry delete.");
          System.Threading.Thread.Sleep(200 + i * 200);
          IACSharpSensor.IACSharpSensor.SensorReached(3688);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3689);
    }
    public void Save()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3690);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3691);
        bool thisSaveUsedZip64 = false;
        _saveOperationCanceled = false;
        _numberOfSegmentsForMostRecentSave = 0;
        OnSaveStarted();
        IACSharpSensor.IACSharpSensor.SensorReached(3692);
        if (WriteStream == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3693);
          throw new BadStateException("You haven't specified where to save the zip.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3694);
        if (_name != null && _name.EndsWith(".exe") && !_SavingSfx) {
          IACSharpSensor.IACSharpSensor.SensorReached(3695);
          throw new BadStateException("You specified an EXE for a plain zip file.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3696);
        if (!_contentsChanged) {
          IACSharpSensor.IACSharpSensor.SensorReached(3697);
          OnSaveCompleted();
          IACSharpSensor.IACSharpSensor.SensorReached(3698);
          if (Verbose) {
            IACSharpSensor.IACSharpSensor.SensorReached(3699);
            StatusMessageTextWriter.WriteLine("No save is necessary....");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3700);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3701);
        Reset(true);
        IACSharpSensor.IACSharpSensor.SensorReached(3702);
        if (Verbose) {
          IACSharpSensor.IACSharpSensor.SensorReached(3703);
          StatusMessageTextWriter.WriteLine("saving....");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3704);
        if (_entries.Count >= 0xffff && _zip64 == Zip64Option.Never) {
          IACSharpSensor.IACSharpSensor.SensorReached(3705);
          throw new ZipException("The number of entries is 65535 or greater. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3706);
        int n = 0;
        ICollection<ZipEntry> c = (SortEntriesBeforeSaving) ? EntriesSorted : Entries;
        IACSharpSensor.IACSharpSensor.SensorReached(3707);
        foreach (ZipEntry e in c) {
          IACSharpSensor.IACSharpSensor.SensorReached(3708);
          OnSaveEntry(n, e, true);
          e.Write(WriteStream);
          IACSharpSensor.IACSharpSensor.SensorReached(3709);
          if (_saveOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(3710);
            break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3711);
          n++;
          OnSaveEntry(n, e, false);
          IACSharpSensor.IACSharpSensor.SensorReached(3712);
          if (_saveOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(3713);
            break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3714);
          if (e.IncludedInMostRecentSave) {
            IACSharpSensor.IACSharpSensor.SensorReached(3715);
            thisSaveUsedZip64 |= e.OutputUsedZip64.Value;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3716);
        if (_saveOperationCanceled) {
          IACSharpSensor.IACSharpSensor.SensorReached(3717);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3718);
        var zss = WriteStream as ZipSegmentedStream;
        _numberOfSegmentsForMostRecentSave = (zss != null) ? zss.CurrentSegment : 1;
        bool directoryNeededZip64 = ZipOutput.WriteCentralDirectoryStructure(WriteStream, c, _numberOfSegmentsForMostRecentSave, _zip64, Comment, new ZipContainer(this));
        OnSaveEvent(ZipProgressEventType.Saving_AfterSaveTempArchive);
        _hasBeenSaved = true;
        _contentsChanged = false;
        thisSaveUsedZip64 |= directoryNeededZip64;
        _OutputUsesZip64 = new Nullable<bool>(thisSaveUsedZip64);
        IACSharpSensor.IACSharpSensor.SensorReached(3719);
        if (_name != null && (_temporaryFileName != null || zss != null)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3720);
          WriteStream.Dispose();
          IACSharpSensor.IACSharpSensor.SensorReached(3721);
          if (_saveOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(3722);
            return;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3723);
          if (_fileAlreadyExists && this._readstream != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3724);
            this._readstream.Close();
            this._readstream = null;
            IACSharpSensor.IACSharpSensor.SensorReached(3725);
            foreach (var e in c) {
              IACSharpSensor.IACSharpSensor.SensorReached(3726);
              var zss1 = e._archiveStream as ZipSegmentedStream;
              IACSharpSensor.IACSharpSensor.SensorReached(3727);
              if (zss1 != null) {
                IACSharpSensor.IACSharpSensor.SensorReached(3728);
                zss1.Dispose();
              }
              IACSharpSensor.IACSharpSensor.SensorReached(3729);
              e._archiveStream = null;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3730);
          string tmpName = null;
          IACSharpSensor.IACSharpSensor.SensorReached(3731);
          if (File.Exists(_name)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3732);
            tmpName = _name + "." + Path.GetRandomFileName();
            IACSharpSensor.IACSharpSensor.SensorReached(3733);
            if (File.Exists(tmpName)) {
              IACSharpSensor.IACSharpSensor.SensorReached(3734);
              DeleteFileWithRetry(tmpName);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3735);
            File.Move(_name, tmpName);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3736);
          OnSaveEvent(ZipProgressEventType.Saving_BeforeRenameTempArchive);
          File.Move((zss != null) ? zss.CurrentTempName : _temporaryFileName, _name);
          OnSaveEvent(ZipProgressEventType.Saving_AfterRenameTempArchive);
          IACSharpSensor.IACSharpSensor.SensorReached(3737);
          if (tmpName != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3738);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(3739);
              if (File.Exists(tmpName)) {
                IACSharpSensor.IACSharpSensor.SensorReached(3740);
                File.Delete(tmpName);
              }
            } catch {
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3741);
          _fileAlreadyExists = true;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3742);
        NotifyEntriesSaveComplete(c);
        OnSaveCompleted();
        _JustSaved = true;
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(3743);
        CleanupAfterSaveOperation();
        IACSharpSensor.IACSharpSensor.SensorReached(3744);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3745);
      return;
    }
    private static void NotifyEntriesSaveComplete(ICollection<ZipEntry> c)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3746);
      foreach (ZipEntry e in c) {
        IACSharpSensor.IACSharpSensor.SensorReached(3747);
        e.NotifySaveComplete();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3748);
    }
    private void RemoveTempFile()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3749);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3750);
        if (File.Exists(_temporaryFileName)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3751);
          File.Delete(_temporaryFileName);
        }
      } catch (IOException ex1) {
        IACSharpSensor.IACSharpSensor.SensorReached(3752);
        if (Verbose) {
          IACSharpSensor.IACSharpSensor.SensorReached(3753);
          StatusMessageTextWriter.WriteLine("ZipFile::Save: could not delete temp file: {0}.", ex1.Message);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3754);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3755);
    }
    private void CleanupAfterSaveOperation()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3756);
      if (_name != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3757);
        if (_writestream != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3758);
          try {
            IACSharpSensor.IACSharpSensor.SensorReached(3759);
            _writestream.Dispose();
          } catch (System.IO.IOException) {
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3760);
        _writestream = null;
        IACSharpSensor.IACSharpSensor.SensorReached(3761);
        if (_temporaryFileName != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3762);
          RemoveTempFile();
          _temporaryFileName = null;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3763);
    }
    public void Save(String fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3764);
      if (_name == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3765);
        _writestream = null;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(3766);
        _readName = _name;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3767);
      _name = fileName;
      IACSharpSensor.IACSharpSensor.SensorReached(3768);
      if (Directory.Exists(_name)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3769);
        throw new ZipException("Bad Directory", new System.ArgumentException("That name specifies an existing directory. Please specify a filename.", "fileName"));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3770);
      _contentsChanged = true;
      _fileAlreadyExists = File.Exists(_name);
      Save();
      IACSharpSensor.IACSharpSensor.SensorReached(3771);
    }
    public void Save(Stream outputStream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3772);
      if (outputStream == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3773);
        throw new ArgumentNullException("outputStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3774);
      if (!outputStream.CanWrite) {
        IACSharpSensor.IACSharpSensor.SensorReached(3775);
        throw new ArgumentException("Must be a writable stream.", "outputStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3776);
      _name = null;
      _writestream = new CountingStream(outputStream);
      _contentsChanged = true;
      _fileAlreadyExists = false;
      Save();
      IACSharpSensor.IACSharpSensor.SensorReached(3777);
    }
  }
  static internal class ZipOutput
  {
    public static bool WriteCentralDirectoryStructure(Stream s, ICollection<ZipEntry> entries, uint numSegments, Zip64Option zip64, String comment, ZipContainer container)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3778);
      var zss = s as ZipSegmentedStream;
      IACSharpSensor.IACSharpSensor.SensorReached(3779);
      if (zss != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3780);
        zss.ContiguousWrite = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3781);
      Int64 aLength = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3782);
      using (var ms = new MemoryStream()) {
        IACSharpSensor.IACSharpSensor.SensorReached(3783);
        foreach (ZipEntry e in entries) {
          IACSharpSensor.IACSharpSensor.SensorReached(3784);
          if (e.IncludedInMostRecentSave) {
            IACSharpSensor.IACSharpSensor.SensorReached(3785);
            e.WriteCentralDirectoryEntry(ms);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3786);
        var a = ms.ToArray();
        s.Write(a, 0, a.Length);
        aLength = a.Length;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3787);
      var output = s as CountingStream;
      long Finish = (output != null) ? output.ComputedPosition : s.Position;
      long Start = Finish - aLength;
      UInt32 startSegment = (zss != null) ? zss.CurrentSegment : 0;
      Int64 SizeOfCentralDirectory = Finish - Start;
      int countOfEntries = CountEntries(entries);
      bool needZip64CentralDirectory = zip64 == Zip64Option.Always || countOfEntries >= 0xffff || SizeOfCentralDirectory > 0xffffffffu || Start > 0xffffffffu;
      byte[] a2 = null;
      IACSharpSensor.IACSharpSensor.SensorReached(3788);
      if (needZip64CentralDirectory) {
        IACSharpSensor.IACSharpSensor.SensorReached(3789);
        if (zip64 == Zip64Option.Never) {
          IACSharpSensor.IACSharpSensor.SensorReached(3790);
          System.Diagnostics.StackFrame sf = new System.Diagnostics.StackFrame(1);
          IACSharpSensor.IACSharpSensor.SensorReached(3791);
          if (sf.GetMethod().DeclaringType == typeof(ZipFile)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3792);
            throw new ZipException("The archive requires a ZIP64 Central Directory. Consider setting the ZipFile.UseZip64WhenSaving property.");
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(3793);
            throw new ZipException("The archive requires a ZIP64 Central Directory. Consider setting the ZipOutputStream.EnableZip64 property.");
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3794);
        var a = GenZip64EndOfCentralDirectory(Start, Finish, countOfEntries, numSegments);
        a2 = GenCentralDirectoryFooter(Start, Finish, zip64, countOfEntries, comment, container);
        IACSharpSensor.IACSharpSensor.SensorReached(3795);
        if (startSegment != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3796);
          UInt32 thisSegment = zss.ComputeSegment(a.Length + a2.Length);
          int i = 16;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
          i += 4;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
          i = 60;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
          i += 4;
          i += 8;
          Array.Copy(BitConverter.GetBytes(thisSegment), 0, a, i, 4);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3797);
        s.Write(a, 0, a.Length);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(3798);
        a2 = GenCentralDirectoryFooter(Start, Finish, zip64, countOfEntries, comment, container);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3799);
      if (startSegment != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3800);
        UInt16 thisSegment = (UInt16)zss.ComputeSegment(a2.Length);
        int i = 4;
        Array.Copy(BitConverter.GetBytes(thisSegment), 0, a2, i, 2);
        i += 2;
        Array.Copy(BitConverter.GetBytes(thisSegment), 0, a2, i, 2);
        i += 2;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3801);
      s.Write(a2, 0, a2.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(3802);
      if (zss != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3803);
        zss.ContiguousWrite = false;
      }
      System.Boolean RNTRNTRNT_389 = needZip64CentralDirectory;
      IACSharpSensor.IACSharpSensor.SensorReached(3804);
      return RNTRNTRNT_389;
    }
    private static System.Text.Encoding GetEncoding(ZipContainer container, string t)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3805);
      switch (container.AlternateEncodingUsage) {
        case ZipOption.Always:
          System.Text.Encoding RNTRNTRNT_390 = container.AlternateEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(3806);
          return RNTRNTRNT_390;
        case ZipOption.Never:
          System.Text.Encoding RNTRNTRNT_391 = container.DefaultEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(3807);
          return RNTRNTRNT_391;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3808);
      var e = container.DefaultEncoding;
      IACSharpSensor.IACSharpSensor.SensorReached(3809);
      if (t == null) {
        System.Text.Encoding RNTRNTRNT_392 = e;
        IACSharpSensor.IACSharpSensor.SensorReached(3810);
        return RNTRNTRNT_392;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3811);
      var bytes = e.GetBytes(t);
      var t2 = e.GetString(bytes, 0, bytes.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(3812);
      if (t2.Equals(t)) {
        System.Text.Encoding RNTRNTRNT_393 = e;
        IACSharpSensor.IACSharpSensor.SensorReached(3813);
        return RNTRNTRNT_393;
      }
      System.Text.Encoding RNTRNTRNT_394 = container.AlternateEncoding;
      IACSharpSensor.IACSharpSensor.SensorReached(3814);
      return RNTRNTRNT_394;
    }
    private static byte[] GenCentralDirectoryFooter(long StartOfCentralDirectory, long EndOfCentralDirectory, Zip64Option zip64, int entryCount, string comment, ZipContainer container)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3815);
      System.Text.Encoding encoding = GetEncoding(container, comment);
      int j = 0;
      int bufferLength = 22;
      byte[] block = null;
      Int16 commentLength = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3816);
      if ((comment != null) && (comment.Length != 0)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3817);
        block = encoding.GetBytes(comment);
        commentLength = (Int16)block.Length;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3818);
      bufferLength += commentLength;
      byte[] bytes = new byte[bufferLength];
      int i = 0;
      byte[] sig = BitConverter.GetBytes(ZipConstants.EndOfCentralDirectorySignature);
      Array.Copy(sig, 0, bytes, i, 4);
      i += 4;
      bytes[i++] = 0;
      bytes[i++] = 0;
      bytes[i++] = 0;
      bytes[i++] = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3819);
      if (entryCount >= 0xffff || zip64 == Zip64Option.Always) {
        IACSharpSensor.IACSharpSensor.SensorReached(3820);
        for (j = 0; j < 4; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(3821);
          bytes[i++] = 0xff;
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(3822);
        bytes[i++] = (byte)(entryCount & 0xff);
        bytes[i++] = (byte)((entryCount & 0xff00) >> 8);
        bytes[i++] = (byte)(entryCount & 0xff);
        bytes[i++] = (byte)((entryCount & 0xff00) >> 8);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3823);
      Int64 SizeOfCentralDirectory = EndOfCentralDirectory - StartOfCentralDirectory;
      IACSharpSensor.IACSharpSensor.SensorReached(3824);
      if (SizeOfCentralDirectory >= 0xffffffffu || StartOfCentralDirectory >= 0xffffffffu) {
        IACSharpSensor.IACSharpSensor.SensorReached(3825);
        for (j = 0; j < 8; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(3826);
          bytes[i++] = 0xff;
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(3827);
        bytes[i++] = (byte)(SizeOfCentralDirectory & 0xff);
        bytes[i++] = (byte)((SizeOfCentralDirectory & 0xff00) >> 8);
        bytes[i++] = (byte)((SizeOfCentralDirectory & 0xff0000) >> 16);
        bytes[i++] = (byte)((SizeOfCentralDirectory & 0xff000000u) >> 24);
        bytes[i++] = (byte)(StartOfCentralDirectory & 0xff);
        bytes[i++] = (byte)((StartOfCentralDirectory & 0xff00) >> 8);
        bytes[i++] = (byte)((StartOfCentralDirectory & 0xff0000) >> 16);
        bytes[i++] = (byte)((StartOfCentralDirectory & 0xff000000u) >> 24);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3828);
      if ((comment == null) || (comment.Length == 0)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3829);
        bytes[i++] = (byte)0;
        bytes[i++] = (byte)0;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(3830);
        if (commentLength + i + 2 > bytes.Length) {
          IACSharpSensor.IACSharpSensor.SensorReached(3831);
          commentLength = (Int16)(bytes.Length - i - 2);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3832);
        bytes[i++] = (byte)(commentLength & 0xff);
        bytes[i++] = (byte)((commentLength & 0xff00) >> 8);
        IACSharpSensor.IACSharpSensor.SensorReached(3833);
        if (commentLength != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3834);
          for (j = 0; (j < commentLength) && (i + j < bytes.Length); j++) {
            IACSharpSensor.IACSharpSensor.SensorReached(3835);
            bytes[i + j] = block[j];
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3836);
          i += j;
        }
      }
      System.Byte[] RNTRNTRNT_395 = bytes;
      IACSharpSensor.IACSharpSensor.SensorReached(3837);
      return RNTRNTRNT_395;
    }
    private static byte[] GenZip64EndOfCentralDirectory(long StartOfCentralDirectory, long EndOfCentralDirectory, int entryCount, uint numSegments)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3838);
      const int bufferLength = 12 + 44 + 20;
      byte[] bytes = new byte[bufferLength];
      int i = 0;
      byte[] sig = BitConverter.GetBytes(ZipConstants.Zip64EndOfCentralDirectoryRecordSignature);
      Array.Copy(sig, 0, bytes, i, 4);
      i += 4;
      long DataSize = 44;
      Array.Copy(BitConverter.GetBytes(DataSize), 0, bytes, i, 8);
      i += 8;
      bytes[i++] = 45;
      bytes[i++] = 0x0;
      bytes[i++] = 45;
      bytes[i++] = 0x0;
      IACSharpSensor.IACSharpSensor.SensorReached(3839);
      for (int j = 0; j < 8; j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(3840);
        bytes[i++] = 0x0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3841);
      long numberOfEntries = entryCount;
      Array.Copy(BitConverter.GetBytes(numberOfEntries), 0, bytes, i, 8);
      i += 8;
      Array.Copy(BitConverter.GetBytes(numberOfEntries), 0, bytes, i, 8);
      i += 8;
      Int64 SizeofCentraldirectory = EndOfCentralDirectory - StartOfCentralDirectory;
      Array.Copy(BitConverter.GetBytes(SizeofCentraldirectory), 0, bytes, i, 8);
      i += 8;
      Array.Copy(BitConverter.GetBytes(StartOfCentralDirectory), 0, bytes, i, 8);
      i += 8;
      sig = BitConverter.GetBytes(ZipConstants.Zip64EndOfCentralDirectoryLocatorSignature);
      Array.Copy(sig, 0, bytes, i, 4);
      i += 4;
      uint x2 = (numSegments == 0) ? 0 : (uint)(numSegments - 1);
      Array.Copy(BitConverter.GetBytes(x2), 0, bytes, i, 4);
      i += 4;
      Array.Copy(BitConverter.GetBytes(EndOfCentralDirectory), 0, bytes, i, 8);
      i += 8;
      Array.Copy(BitConverter.GetBytes(numSegments), 0, bytes, i, 4);
      i += 4;
      System.Byte[] RNTRNTRNT_396 = bytes;
      IACSharpSensor.IACSharpSensor.SensorReached(3842);
      return RNTRNTRNT_396;
    }
    private static int CountEntries(ICollection<ZipEntry> _entries)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3843);
      int count = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3844);
      foreach (var entry in _entries) {
        IACSharpSensor.IACSharpSensor.SensorReached(3845);
        if (entry.IncludedInMostRecentSave) {
          IACSharpSensor.IACSharpSensor.SensorReached(3846);
          count++;
        }
      }
      System.Int32 RNTRNTRNT_397 = count;
      IACSharpSensor.IACSharpSensor.SensorReached(3847);
      return RNTRNTRNT_397;
    }
  }
}
