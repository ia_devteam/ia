using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public partial class ZipFile
  {
    public static bool CheckZip(string zipFileName)
    {
      System.Boolean RNTRNTRNT_317 = CheckZip(zipFileName, false, null);
      IACSharpSensor.IACSharpSensor.SensorReached(3112);
      return RNTRNTRNT_317;
    }
    public static bool CheckZip(string zipFileName, bool fixIfNecessary, TextWriter writer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3113);
      ZipFile zip1 = null, zip2 = null;
      bool isOk = true;
      IACSharpSensor.IACSharpSensor.SensorReached(3114);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3115);
        zip1 = new ZipFile();
        zip1.FullScan = true;
        zip1.Initialize(zipFileName);
        zip2 = ZipFile.Read(zipFileName);
        IACSharpSensor.IACSharpSensor.SensorReached(3116);
        foreach (var e1 in zip1) {
          IACSharpSensor.IACSharpSensor.SensorReached(3117);
          foreach (var e2 in zip2) {
            IACSharpSensor.IACSharpSensor.SensorReached(3118);
            if (e1.FileName == e2.FileName) {
              IACSharpSensor.IACSharpSensor.SensorReached(3119);
              if (e1._RelativeOffsetOfLocalHeader != e2._RelativeOffsetOfLocalHeader) {
                IACSharpSensor.IACSharpSensor.SensorReached(3120);
                isOk = false;
                IACSharpSensor.IACSharpSensor.SensorReached(3121);
                if (writer != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(3122);
                  writer.WriteLine("{0}: mismatch in RelativeOffsetOfLocalHeader  (0x{1:X16} != 0x{2:X16})", e1.FileName, e1._RelativeOffsetOfLocalHeader, e2._RelativeOffsetOfLocalHeader);
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(3123);
              if (e1._CompressedSize != e2._CompressedSize) {
                IACSharpSensor.IACSharpSensor.SensorReached(3124);
                isOk = false;
                IACSharpSensor.IACSharpSensor.SensorReached(3125);
                if (writer != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(3126);
                  writer.WriteLine("{0}: mismatch in CompressedSize  (0x{1:X16} != 0x{2:X16})", e1.FileName, e1._CompressedSize, e2._CompressedSize);
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(3127);
              if (e1._UncompressedSize != e2._UncompressedSize) {
                IACSharpSensor.IACSharpSensor.SensorReached(3128);
                isOk = false;
                IACSharpSensor.IACSharpSensor.SensorReached(3129);
                if (writer != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(3130);
                  writer.WriteLine("{0}: mismatch in UncompressedSize  (0x{1:X16} != 0x{2:X16})", e1.FileName, e1._UncompressedSize, e2._UncompressedSize);
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(3131);
              if (e1.CompressionMethod != e2.CompressionMethod) {
                IACSharpSensor.IACSharpSensor.SensorReached(3132);
                isOk = false;
                IACSharpSensor.IACSharpSensor.SensorReached(3133);
                if (writer != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(3134);
                  writer.WriteLine("{0}: mismatch in CompressionMethod  (0x{1:X4} != 0x{2:X4})", e1.FileName, e1.CompressionMethod, e2.CompressionMethod);
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(3135);
              if (e1.Crc != e2.Crc) {
                IACSharpSensor.IACSharpSensor.SensorReached(3136);
                isOk = false;
                IACSharpSensor.IACSharpSensor.SensorReached(3137);
                if (writer != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(3138);
                  writer.WriteLine("{0}: mismatch in Crc32  (0x{1:X4} != 0x{2:X4})", e1.FileName, e1.Crc, e2.Crc);
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(3139);
              break;
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3140);
        zip2.Dispose();
        zip2 = null;
        IACSharpSensor.IACSharpSensor.SensorReached(3141);
        if (!isOk && fixIfNecessary) {
          IACSharpSensor.IACSharpSensor.SensorReached(3142);
          string newFileName = Path.GetFileNameWithoutExtension(zipFileName);
          newFileName = System.String.Format("{0}_fixed.zip", newFileName);
          zip1.Save(newFileName);
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(3143);
        if (zip1 != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3144);
          zip1.Dispose();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3145);
        if (zip2 != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3146);
          zip2.Dispose();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3147);
      }
      System.Boolean RNTRNTRNT_318 = isOk;
      IACSharpSensor.IACSharpSensor.SensorReached(3148);
      return RNTRNTRNT_318;
    }
    public static void FixZipDirectory(string zipFileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3149);
      using (var zip = new ZipFile()) {
        IACSharpSensor.IACSharpSensor.SensorReached(3150);
        zip.FullScan = true;
        zip.Initialize(zipFileName);
        zip.Save(zipFileName);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3151);
    }
    public static bool CheckZipPassword(string zipFileName, string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3152);
      bool success = false;
      IACSharpSensor.IACSharpSensor.SensorReached(3153);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3154);
        using (ZipFile zip1 = ZipFile.Read(zipFileName)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3155);
          foreach (var e in zip1) {
            IACSharpSensor.IACSharpSensor.SensorReached(3156);
            if (!e.IsDirectory && e.UsesEncryption) {
              IACSharpSensor.IACSharpSensor.SensorReached(3157);
              e.ExtractWithPassword(System.IO.Stream.Null, password);
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3158);
        success = true;
      } catch (Ionic.Zip.BadPasswordException) {
      }
      System.Boolean RNTRNTRNT_319 = success;
      IACSharpSensor.IACSharpSensor.SensorReached(3159);
      return RNTRNTRNT_319;
    }
    public string Info {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(3160);
        var builder = new System.Text.StringBuilder();
        builder.Append(string.Format("          ZipFile: {0}\n", this.Name));
        IACSharpSensor.IACSharpSensor.SensorReached(3161);
        if (!string.IsNullOrEmpty(this._Comment)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3162);
          builder.Append(string.Format("          Comment: {0}\n", this._Comment));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3163);
        if (this._versionMadeBy != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3164);
          builder.Append(string.Format("  version made by: 0x{0:X4}\n", this._versionMadeBy));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3165);
        if (this._versionNeededToExtract != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3166);
          builder.Append(string.Format("needed to extract: 0x{0:X4}\n", this._versionNeededToExtract));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3167);
        builder.Append(string.Format("       uses ZIP64: {0}\n", this.InputUsesZip64));
        builder.Append(string.Format("     disk with CD: {0}\n", this._diskNumberWithCd));
        IACSharpSensor.IACSharpSensor.SensorReached(3168);
        if (this._OffsetOfCentralDirectory == 0xffffffffu) {
          IACSharpSensor.IACSharpSensor.SensorReached(3169);
          builder.Append(string.Format("      CD64 offset: 0x{0:X16}\n", this._OffsetOfCentralDirectory64));
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(3170);
          builder.Append(string.Format("        CD offset: 0x{0:X8}\n", this._OffsetOfCentralDirectory));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3171);
        builder.Append("\n");
        IACSharpSensor.IACSharpSensor.SensorReached(3172);
        foreach (ZipEntry entry in this._entries.Values) {
          IACSharpSensor.IACSharpSensor.SensorReached(3173);
          builder.Append(entry.Info);
        }
        System.String RNTRNTRNT_320 = builder.ToString();
        IACSharpSensor.IACSharpSensor.SensorReached(3174);
        return RNTRNTRNT_320;
      }
    }
  }
}
