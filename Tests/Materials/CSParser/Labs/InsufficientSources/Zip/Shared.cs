using System;
using System.IO;
using System.Security.Permissions;
namespace Ionic.Zip
{
  static internal class SharedUtilities
  {
    public static Int64 GetFileLength(string fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1613);
      if (!File.Exists(fileName)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1614);
        throw new System.IO.FileNotFoundException(fileName);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1615);
      long fileLength = 0L;
      FileShare fs = FileShare.ReadWrite;
      fs |= FileShare.Delete;
      IACSharpSensor.IACSharpSensor.SensorReached(1616);
      using (var s = File.Open(fileName, FileMode.Open, FileAccess.Read, fs)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1617);
        fileLength = s.Length;
      }
      Int64 RNTRNTRNT_140 = fileLength;
      IACSharpSensor.IACSharpSensor.SensorReached(1618);
      return RNTRNTRNT_140;
    }
    [System.Diagnostics.Conditional("NETCF")]
    public static void Workaround_Ladybug318918(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1619);
      s.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(1620);
    }
    private static System.Text.RegularExpressions.Regex doubleDotRegex1 = new System.Text.RegularExpressions.Regex("^(.*/)?([^/\\\\.]+/\\\\.\\\\./)(.+)$");
    private static string SimplifyFwdSlashPath(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1621);
      if (path.StartsWith("./")) {
        IACSharpSensor.IACSharpSensor.SensorReached(1622);
        path = path.Substring(2);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1623);
      path = path.Replace("/./", "/");
      path = doubleDotRegex1.Replace(path, "$1$3");
      System.String RNTRNTRNT_141 = path;
      IACSharpSensor.IACSharpSensor.SensorReached(1624);
      return RNTRNTRNT_141;
    }
    public static string NormalizePathForUseInZipFile(string pathName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1625);
      if (String.IsNullOrEmpty(pathName)) {
        System.String RNTRNTRNT_142 = pathName;
        IACSharpSensor.IACSharpSensor.SensorReached(1626);
        return RNTRNTRNT_142;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1627);
      if ((pathName.Length >= 2) && ((pathName[1] == ':') && (pathName[2] == '\\'))) {
        IACSharpSensor.IACSharpSensor.SensorReached(1628);
        pathName = pathName.Substring(3);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1629);
      pathName = pathName.Replace('\\', '/');
      IACSharpSensor.IACSharpSensor.SensorReached(1630);
      while (pathName.StartsWith("/")) {
        IACSharpSensor.IACSharpSensor.SensorReached(1631);
        pathName = pathName.Substring(1);
      }
      System.String RNTRNTRNT_143 = SimplifyFwdSlashPath(pathName);
      IACSharpSensor.IACSharpSensor.SensorReached(1632);
      return RNTRNTRNT_143;
    }
    static System.Text.Encoding ibm437 = System.Text.Encoding.GetEncoding("IBM437");
    static System.Text.Encoding utf8 = System.Text.Encoding.GetEncoding("UTF-8");
    static internal byte[] StringToByteArray(string value, System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1633);
      byte[] a = encoding.GetBytes(value);
      System.Byte[] RNTRNTRNT_144 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(1634);
      return RNTRNTRNT_144;
    }
    static internal byte[] StringToByteArray(string value)
    {
      System.Byte[] RNTRNTRNT_145 = StringToByteArray(value, ibm437);
      IACSharpSensor.IACSharpSensor.SensorReached(1635);
      return RNTRNTRNT_145;
    }
    static internal string Utf8StringFromBuffer(byte[] buf)
    {
      System.String RNTRNTRNT_146 = StringFromBuffer(buf, utf8);
      IACSharpSensor.IACSharpSensor.SensorReached(1636);
      return RNTRNTRNT_146;
    }
    static internal string StringFromBuffer(byte[] buf, System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1637);
      string s = encoding.GetString(buf, 0, buf.Length);
      System.String RNTRNTRNT_147 = s;
      IACSharpSensor.IACSharpSensor.SensorReached(1638);
      return RNTRNTRNT_147;
    }
    static internal int ReadSignature(System.IO.Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1639);
      int x = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1640);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(1641);
        x = _ReadFourBytes(s, "n/a");
      } catch (BadReadException) {
      }
      System.Int32 RNTRNTRNT_148 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1642);
      return RNTRNTRNT_148;
    }
    static internal int ReadEntrySignature(System.IO.Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1643);
      int x = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1644);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(1645);
        x = _ReadFourBytes(s, "n/a");
        IACSharpSensor.IACSharpSensor.SensorReached(1646);
        if (x == ZipConstants.ZipEntryDataDescriptorSignature) {
          IACSharpSensor.IACSharpSensor.SensorReached(1647);
          s.Seek(12, SeekOrigin.Current);
          Workaround_Ladybug318918(s);
          x = _ReadFourBytes(s, "n/a");
          IACSharpSensor.IACSharpSensor.SensorReached(1648);
          if (x != ZipConstants.ZipEntrySignature) {
            IACSharpSensor.IACSharpSensor.SensorReached(1649);
            s.Seek(8, SeekOrigin.Current);
            Workaround_Ladybug318918(s);
            x = _ReadFourBytes(s, "n/a");
            IACSharpSensor.IACSharpSensor.SensorReached(1650);
            if (x != ZipConstants.ZipEntrySignature) {
              IACSharpSensor.IACSharpSensor.SensorReached(1651);
              s.Seek(-24, SeekOrigin.Current);
              Workaround_Ladybug318918(s);
              x = _ReadFourBytes(s, "n/a");
            }
          }
        }
      } catch (BadReadException) {
      }
      System.Int32 RNTRNTRNT_149 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(1652);
      return RNTRNTRNT_149;
    }
    static internal int ReadInt(System.IO.Stream s)
    {
      System.Int32 RNTRNTRNT_150 = _ReadFourBytes(s, "Could not read block - no data!  (position 0x{0:X8})");
      IACSharpSensor.IACSharpSensor.SensorReached(1653);
      return RNTRNTRNT_150;
    }
    private static int _ReadFourBytes(System.IO.Stream s, string message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1654);
      int n = 0;
      byte[] block = new byte[4];
      n = s.Read(block, 0, block.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(1655);
      if (n != block.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(1656);
        throw new BadReadException(String.Format(message, s.Position));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1657);
      int data = unchecked((((block[3] * 256 + block[2]) * 256) + block[1]) * 256 + block[0]);
      System.Int32 RNTRNTRNT_151 = data;
      IACSharpSensor.IACSharpSensor.SensorReached(1658);
      return RNTRNTRNT_151;
    }
    static internal long FindSignature(System.IO.Stream stream, int SignatureToFind)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1659);
      long startingPosition = stream.Position;
      int BATCH_SIZE = 65536;
      byte[] targetBytes = new byte[4];
      targetBytes[0] = (byte)(SignatureToFind >> 24);
      targetBytes[1] = (byte)((SignatureToFind & 0xff0000) >> 16);
      targetBytes[2] = (byte)((SignatureToFind & 0xff00) >> 8);
      targetBytes[3] = (byte)(SignatureToFind & 0xff);
      byte[] batch = new byte[BATCH_SIZE];
      int n = 0;
      bool success = false;
      IACSharpSensor.IACSharpSensor.SensorReached(1660);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(1661);
        n = stream.Read(batch, 0, batch.Length);
        IACSharpSensor.IACSharpSensor.SensorReached(1662);
        if (n != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(1663);
          for (int i = 0; i < n; i++) {
            IACSharpSensor.IACSharpSensor.SensorReached(1664);
            if (batch[i] == targetBytes[3]) {
              IACSharpSensor.IACSharpSensor.SensorReached(1665);
              long curPosition = stream.Position;
              stream.Seek(i - n, System.IO.SeekOrigin.Current);
              Workaround_Ladybug318918(stream);
              int sig = ReadSignature(stream);
              success = (sig == SignatureToFind);
              IACSharpSensor.IACSharpSensor.SensorReached(1666);
              if (!success) {
                IACSharpSensor.IACSharpSensor.SensorReached(1667);
                stream.Seek(curPosition, System.IO.SeekOrigin.Begin);
                Workaround_Ladybug318918(stream);
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(1668);
                break;
              }
            }
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1669);
          break;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1670);
        if (success) {
          IACSharpSensor.IACSharpSensor.SensorReached(1671);
          break;
        }
      } while (true);
      IACSharpSensor.IACSharpSensor.SensorReached(1672);
      if (!success) {
        IACSharpSensor.IACSharpSensor.SensorReached(1673);
        stream.Seek(startingPosition, System.IO.SeekOrigin.Begin);
        Workaround_Ladybug318918(stream);
        System.Int64 RNTRNTRNT_152 = -1;
        IACSharpSensor.IACSharpSensor.SensorReached(1674);
        return RNTRNTRNT_152;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1675);
      long bytesRead = (stream.Position - startingPosition) - 4;
      System.Int64 RNTRNTRNT_153 = bytesRead;
      IACSharpSensor.IACSharpSensor.SensorReached(1676);
      return RNTRNTRNT_153;
    }
    static internal DateTime AdjustTime_Reverse(DateTime time)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1677);
      if (time.Kind == DateTimeKind.Utc) {
        DateTime RNTRNTRNT_154 = time;
        IACSharpSensor.IACSharpSensor.SensorReached(1678);
        return RNTRNTRNT_154;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1679);
      DateTime adjusted = time;
      IACSharpSensor.IACSharpSensor.SensorReached(1680);
      if (DateTime.Now.IsDaylightSavingTime() && !time.IsDaylightSavingTime()) {
        IACSharpSensor.IACSharpSensor.SensorReached(1681);
        adjusted = time - new System.TimeSpan(1, 0, 0);
      } else if (!DateTime.Now.IsDaylightSavingTime() && time.IsDaylightSavingTime()) {
        IACSharpSensor.IACSharpSensor.SensorReached(1682);
        adjusted = time + new System.TimeSpan(1, 0, 0);
      }
      DateTime RNTRNTRNT_155 = adjusted;
      IACSharpSensor.IACSharpSensor.SensorReached(1683);
      return RNTRNTRNT_155;
    }
    static internal DateTime PackedToDateTime(Int32 packedDateTime)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1684);
      if (packedDateTime == 0xffff || packedDateTime == 0) {
        DateTime RNTRNTRNT_156 = new System.DateTime(1995, 1, 1, 0, 0, 0, 0);
        IACSharpSensor.IACSharpSensor.SensorReached(1685);
        return RNTRNTRNT_156;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1686);
      Int16 packedTime = unchecked((Int16)(packedDateTime & 0xffff));
      Int16 packedDate = unchecked((Int16)((packedDateTime & 0xffff0000u) >> 16));
      int year = 1980 + ((packedDate & 0xfe00) >> 9);
      int month = (packedDate & 0x1e0) >> 5;
      int day = packedDate & 0x1f;
      int hour = (packedTime & 0xf800) >> 11;
      int minute = (packedTime & 0x7e0) >> 5;
      int second = (packedTime & 0x1f) * 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1687);
      if (second >= 60) {
        IACSharpSensor.IACSharpSensor.SensorReached(1688);
        minute++;
        second = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1689);
      if (minute >= 60) {
        IACSharpSensor.IACSharpSensor.SensorReached(1690);
        hour++;
        minute = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1691);
      if (hour >= 24) {
        IACSharpSensor.IACSharpSensor.SensorReached(1692);
        day++;
        hour = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1693);
      DateTime d = System.DateTime.Now;
      bool success = false;
      IACSharpSensor.IACSharpSensor.SensorReached(1694);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(1695);
        d = new System.DateTime(year, month, day, hour, minute, second, 0);
        success = true;
      } catch (System.ArgumentOutOfRangeException) {
        IACSharpSensor.IACSharpSensor.SensorReached(1696);
        if (year == 1980 && (month == 0 || day == 0)) {
          IACSharpSensor.IACSharpSensor.SensorReached(1697);
          try {
            IACSharpSensor.IACSharpSensor.SensorReached(1698);
            d = new System.DateTime(1980, 1, 1, hour, minute, second, 0);
            success = true;
          } catch (System.ArgumentOutOfRangeException) {
            IACSharpSensor.IACSharpSensor.SensorReached(1699);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(1700);
              d = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
              success = true;
            } catch (System.ArgumentOutOfRangeException) {
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1701);
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1702);
          try {
            IACSharpSensor.IACSharpSensor.SensorReached(1703);
            while (year < 1980) {
              IACSharpSensor.IACSharpSensor.SensorReached(1704);
              year++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1705);
            while (year > 2030) {
              IACSharpSensor.IACSharpSensor.SensorReached(1706);
              year--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1707);
            while (month < 1) {
              IACSharpSensor.IACSharpSensor.SensorReached(1708);
              month++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1709);
            while (month > 12) {
              IACSharpSensor.IACSharpSensor.SensorReached(1710);
              month--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1711);
            while (day < 1) {
              IACSharpSensor.IACSharpSensor.SensorReached(1712);
              day++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1713);
            while (day > 28) {
              IACSharpSensor.IACSharpSensor.SensorReached(1714);
              day--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1715);
            while (minute < 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(1716);
              minute++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1717);
            while (minute > 59) {
              IACSharpSensor.IACSharpSensor.SensorReached(1718);
              minute--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1719);
            while (second < 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(1720);
              second++;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1721);
            while (second > 59) {
              IACSharpSensor.IACSharpSensor.SensorReached(1722);
              second--;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1723);
            d = new System.DateTime(year, month, day, hour, minute, second, 0);
            success = true;
          } catch (System.ArgumentOutOfRangeException) {
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1724);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1725);
      if (!success) {
        IACSharpSensor.IACSharpSensor.SensorReached(1726);
        string msg = String.Format("y({0}) m({1}) d({2}) h({3}) m({4}) s({5})", year, month, day, hour, minute, second);
        IACSharpSensor.IACSharpSensor.SensorReached(1727);
        throw new ZipException(String.Format("Bad date/time format in the zip file. ({0})", msg));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1728);
      d = DateTime.SpecifyKind(d, DateTimeKind.Local);
      DateTime RNTRNTRNT_157 = d;
      IACSharpSensor.IACSharpSensor.SensorReached(1729);
      return RNTRNTRNT_157;
    }
    static internal Int32 DateTimeToPacked(DateTime time)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1730);
      time = time.ToLocalTime();
      UInt16 packedDate = (UInt16)((time.Day & 0x1f) | ((time.Month << 5) & 0x1e0) | (((time.Year - 1980) << 9) & 0xfe00));
      UInt16 packedTime = (UInt16)((time.Second / 2 & 0x1f) | ((time.Minute << 5) & 0x7e0) | ((time.Hour << 11) & 0xf800));
      Int32 result = (Int32)(((UInt32)(packedDate << 16)) | packedTime);
      Int32 RNTRNTRNT_158 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(1731);
      return RNTRNTRNT_158;
    }
    public static void CreateAndOpenUniqueTempFile(string dir, out Stream fs, out string filename)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1732);
      for (int i = 0; i < 3; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1733);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(1734);
          filename = Path.Combine(dir, InternalGetTempFileName());
          fs = new FileStream(filename, FileMode.CreateNew);
          IACSharpSensor.IACSharpSensor.SensorReached(1735);
          return;
        } catch (IOException) {
          IACSharpSensor.IACSharpSensor.SensorReached(1736);
          if (i == 2) {
            IACSharpSensor.IACSharpSensor.SensorReached(1737);
            throw;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1738);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1739);
      throw new IOException();
    }
    public static string InternalGetTempFileName()
    {
      System.String RNTRNTRNT_159 = "DotNetZip-" + Path.GetRandomFileName().Substring(0, 8) + ".tmp";
      IACSharpSensor.IACSharpSensor.SensorReached(1740);
      return RNTRNTRNT_159;
    }
    static internal int ReadWithRetry(System.IO.Stream s, byte[] buffer, int offset, int count, string FileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1741);
      int n = 0;
      bool done = false;
      int retries = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1742);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(1743);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(1744);
          n = s.Read(buffer, offset, count);
          done = true;
        } catch (System.IO.IOException ioexc1) {
          IACSharpSensor.IACSharpSensor.SensorReached(1745);
          var p = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);
          IACSharpSensor.IACSharpSensor.SensorReached(1746);
          if (p.IsUnrestricted()) {
            IACSharpSensor.IACSharpSensor.SensorReached(1747);
            uint hresult = _HRForException(ioexc1);
            IACSharpSensor.IACSharpSensor.SensorReached(1748);
            if (hresult != 0x80070021u) {
              IACSharpSensor.IACSharpSensor.SensorReached(1749);
              throw new System.IO.IOException(String.Format("Cannot read file {0}", FileName), ioexc1);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1750);
            retries++;
            IACSharpSensor.IACSharpSensor.SensorReached(1751);
            if (retries > 10) {
              IACSharpSensor.IACSharpSensor.SensorReached(1752);
              throw new System.IO.IOException(String.Format("Cannot read file {0}, at offset 0x{1:X8} after 10 retries", FileName, offset), ioexc1);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(1753);
            System.Threading.Thread.Sleep(250 + retries * 550);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1754);
            throw;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1755);
        }
      } while (!done);
      System.Int32 RNTRNTRNT_160 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(1756);
      return RNTRNTRNT_160;
    }
    private static uint _HRForException(System.Exception ex1)
    {
      System.UInt32 RNTRNTRNT_161 = unchecked((uint)System.Runtime.InteropServices.Marshal.GetHRForException(ex1));
      IACSharpSensor.IACSharpSensor.SensorReached(1757);
      return RNTRNTRNT_161;
    }
  }
  public class CountingStream : System.IO.Stream
  {
    private System.IO.Stream _s;
    private Int64 _bytesWritten;
    private Int64 _bytesRead;
    private Int64 _initialOffset;
    public CountingStream(System.IO.Stream stream) : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1758);
      _s = stream;
      IACSharpSensor.IACSharpSensor.SensorReached(1759);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(1760);
        _initialOffset = _s.Position;
      } catch {
        IACSharpSensor.IACSharpSensor.SensorReached(1761);
        _initialOffset = 0L;
        IACSharpSensor.IACSharpSensor.SensorReached(1762);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1763);
    }
    public Stream WrappedStream {
      get {
        Stream RNTRNTRNT_162 = _s;
        IACSharpSensor.IACSharpSensor.SensorReached(1764);
        return RNTRNTRNT_162;
      }
    }
    public Int64 BytesWritten {
      get {
        Int64 RNTRNTRNT_163 = _bytesWritten;
        IACSharpSensor.IACSharpSensor.SensorReached(1765);
        return RNTRNTRNT_163;
      }
    }
    public Int64 BytesRead {
      get {
        Int64 RNTRNTRNT_164 = _bytesRead;
        IACSharpSensor.IACSharpSensor.SensorReached(1766);
        return RNTRNTRNT_164;
      }
    }
    public void Adjust(Int64 delta)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1767);
      _bytesWritten -= delta;
      IACSharpSensor.IACSharpSensor.SensorReached(1768);
      if (_bytesWritten < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1769);
        throw new InvalidOperationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1770);
      if (_s as CountingStream != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1771);
        ((CountingStream)_s).Adjust(delta);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1772);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1773);
      int n = _s.Read(buffer, offset, count);
      _bytesRead += n;
      System.Int32 RNTRNTRNT_165 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(1774);
      return RNTRNTRNT_165;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1775);
      if (count == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1776);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1777);
      _s.Write(buffer, offset, count);
      _bytesWritten += count;
      IACSharpSensor.IACSharpSensor.SensorReached(1778);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_166 = _s.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(1779);
        return RNTRNTRNT_166;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_167 = _s.CanSeek;
        IACSharpSensor.IACSharpSensor.SensorReached(1780);
        return RNTRNTRNT_167;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_168 = _s.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(1781);
        return RNTRNTRNT_168;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1782);
      _s.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(1783);
    }
    public override long Length {
      get {
        System.Int64 RNTRNTRNT_169 = _s.Length;
        IACSharpSensor.IACSharpSensor.SensorReached(1784);
        return RNTRNTRNT_169;
      }
    }
    public long ComputedPosition {
      get {
        System.Int64 RNTRNTRNT_170 = _initialOffset + _bytesWritten;
        IACSharpSensor.IACSharpSensor.SensorReached(1785);
        return RNTRNTRNT_170;
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_171 = _s.Position;
        IACSharpSensor.IACSharpSensor.SensorReached(1786);
        return RNTRNTRNT_171;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1787);
        _s.Seek(value, System.IO.SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_s);
        IACSharpSensor.IACSharpSensor.SensorReached(1788);
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      System.Int64 RNTRNTRNT_172 = _s.Seek(offset, origin);
      IACSharpSensor.IACSharpSensor.SensorReached(1789);
      return RNTRNTRNT_172;
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1790);
      _s.SetLength(value);
      IACSharpSensor.IACSharpSensor.SensorReached(1791);
    }
  }
}
