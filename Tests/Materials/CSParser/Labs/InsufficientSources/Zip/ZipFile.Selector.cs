using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  partial class ZipFile
  {
    public void AddSelectedFiles(String selectionCriteria)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3980);
      this.AddSelectedFiles(selectionCriteria, ".", null, false);
      IACSharpSensor.IACSharpSensor.SensorReached(3981);
    }
    public void AddSelectedFiles(String selectionCriteria, bool recurseDirectories)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3982);
      this.AddSelectedFiles(selectionCriteria, ".", null, recurseDirectories);
      IACSharpSensor.IACSharpSensor.SensorReached(3983);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3984);
      this.AddSelectedFiles(selectionCriteria, directoryOnDisk, null, false);
      IACSharpSensor.IACSharpSensor.SensorReached(3985);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk, bool recurseDirectories)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3986);
      this.AddSelectedFiles(selectionCriteria, directoryOnDisk, null, recurseDirectories);
      IACSharpSensor.IACSharpSensor.SensorReached(3987);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3988);
      this.AddSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, false);
      IACSharpSensor.IACSharpSensor.SensorReached(3989);
    }
    public void AddSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive, bool recurseDirectories)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3990);
      _AddOrUpdateSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, recurseDirectories, false);
      IACSharpSensor.IACSharpSensor.SensorReached(3991);
    }
    public void UpdateSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive, bool recurseDirectories)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3992);
      _AddOrUpdateSelectedFiles(selectionCriteria, directoryOnDisk, directoryPathInArchive, recurseDirectories, true);
      IACSharpSensor.IACSharpSensor.SensorReached(3993);
    }
    private string EnsureendInSlash(string s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3994);
      if (s.EndsWith("\\")) {
        System.String RNTRNTRNT_399 = s;
        IACSharpSensor.IACSharpSensor.SensorReached(3995);
        return RNTRNTRNT_399;
      }
      System.String RNTRNTRNT_400 = s + "\\";
      IACSharpSensor.IACSharpSensor.SensorReached(3996);
      return RNTRNTRNT_400;
    }
    private void _AddOrUpdateSelectedFiles(String selectionCriteria, String directoryOnDisk, String directoryPathInArchive, bool recurseDirectories, bool wantUpdate)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3997);
      if (directoryOnDisk == null && (Directory.Exists(selectionCriteria))) {
        IACSharpSensor.IACSharpSensor.SensorReached(3998);
        directoryOnDisk = selectionCriteria;
        selectionCriteria = "*.*";
      } else if (String.IsNullOrEmpty(directoryOnDisk)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3999);
        directoryOnDisk = ".";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4000);
      while (directoryOnDisk.EndsWith("\\")) {
        IACSharpSensor.IACSharpSensor.SensorReached(4001);
        directoryOnDisk = directoryOnDisk.Substring(0, directoryOnDisk.Length - 1);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4002);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(4003);
        StatusMessageTextWriter.WriteLine("adding selection '{0}' from dir '{1}'...", selectionCriteria, directoryOnDisk);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4004);
      Ionic.FileSelector ff = new Ionic.FileSelector(selectionCriteria, AddDirectoryWillTraverseReparsePoints);
      var itemsToAdd = ff.SelectFiles(directoryOnDisk, recurseDirectories);
      IACSharpSensor.IACSharpSensor.SensorReached(4005);
      if (Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(4006);
        StatusMessageTextWriter.WriteLine("found {0} files...", itemsToAdd.Count);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4007);
      OnAddStarted();
      AddOrUpdateAction action = (wantUpdate) ? AddOrUpdateAction.AddOrUpdate : AddOrUpdateAction.AddOnly;
      IACSharpSensor.IACSharpSensor.SensorReached(4008);
      foreach (var item in itemsToAdd) {
        IACSharpSensor.IACSharpSensor.SensorReached(4009);
        string dirInArchive = (directoryPathInArchive == null) ? null : ReplaceLeadingDirectory(Path.GetDirectoryName(item), directoryOnDisk, directoryPathInArchive);
        IACSharpSensor.IACSharpSensor.SensorReached(4010);
        if (File.Exists(item)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4011);
          if (wantUpdate) {
            IACSharpSensor.IACSharpSensor.SensorReached(4012);
            this.UpdateFile(item, dirInArchive);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(4013);
            this.AddFile(item, dirInArchive);
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4014);
          AddOrUpdateDirectoryImpl(item, dirInArchive, action, false, 0);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4015);
      OnAddCompleted();
      IACSharpSensor.IACSharpSensor.SensorReached(4016);
    }
    private static string ReplaceLeadingDirectory(string original, string pattern, string replacement)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4017);
      string upperString = original.ToUpper();
      string upperPattern = pattern.ToUpper();
      int p1 = upperString.IndexOf(upperPattern);
      IACSharpSensor.IACSharpSensor.SensorReached(4018);
      if (p1 != 0) {
        System.String RNTRNTRNT_401 = original;
        IACSharpSensor.IACSharpSensor.SensorReached(4019);
        return RNTRNTRNT_401;
      }
      System.String RNTRNTRNT_402 = replacement + original.Substring(upperPattern.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(4020);
      return RNTRNTRNT_402;
    }
    public ICollection<ZipEntry> SelectEntries(String selectionCriteria)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4021);
      Ionic.FileSelector ff = new Ionic.FileSelector(selectionCriteria, AddDirectoryWillTraverseReparsePoints);
      ICollection<ZipEntry> RNTRNTRNT_403 = ff.SelectEntries(this);
      IACSharpSensor.IACSharpSensor.SensorReached(4022);
      return RNTRNTRNT_403;
    }
    public ICollection<ZipEntry> SelectEntries(String selectionCriteria, string directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4023);
      Ionic.FileSelector ff = new Ionic.FileSelector(selectionCriteria, AddDirectoryWillTraverseReparsePoints);
      ICollection<ZipEntry> RNTRNTRNT_404 = ff.SelectEntries(this, directoryPathInArchive);
      IACSharpSensor.IACSharpSensor.SensorReached(4024);
      return RNTRNTRNT_404;
    }
    public int RemoveSelectedEntries(String selectionCriteria)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4025);
      var selection = this.SelectEntries(selectionCriteria);
      this.RemoveEntries(selection);
      System.Int32 RNTRNTRNT_405 = selection.Count;
      IACSharpSensor.IACSharpSensor.SensorReached(4026);
      return RNTRNTRNT_405;
    }
    public int RemoveSelectedEntries(String selectionCriteria, string directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4027);
      var selection = this.SelectEntries(selectionCriteria, directoryPathInArchive);
      this.RemoveEntries(selection);
      System.Int32 RNTRNTRNT_406 = selection.Count;
      IACSharpSensor.IACSharpSensor.SensorReached(4028);
      return RNTRNTRNT_406;
    }
    public void ExtractSelectedEntries(String selectionCriteria)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4029);
      foreach (ZipEntry e in SelectEntries(selectionCriteria)) {
        IACSharpSensor.IACSharpSensor.SensorReached(4030);
        e.Password = _Password;
        e.Extract();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4031);
    }
    public void ExtractSelectedEntries(String selectionCriteria, ExtractExistingFileAction extractExistingFile)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4032);
      foreach (ZipEntry e in SelectEntries(selectionCriteria)) {
        IACSharpSensor.IACSharpSensor.SensorReached(4033);
        e.Password = _Password;
        e.Extract(extractExistingFile);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4034);
    }
    public void ExtractSelectedEntries(String selectionCriteria, String directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4035);
      foreach (ZipEntry e in SelectEntries(selectionCriteria, directoryPathInArchive)) {
        IACSharpSensor.IACSharpSensor.SensorReached(4036);
        e.Password = _Password;
        e.Extract();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4037);
    }
    public void ExtractSelectedEntries(String selectionCriteria, string directoryInArchive, string extractDirectory)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4038);
      foreach (ZipEntry e in SelectEntries(selectionCriteria, directoryInArchive)) {
        IACSharpSensor.IACSharpSensor.SensorReached(4039);
        e.Password = _Password;
        e.Extract(extractDirectory);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4040);
    }
    public void ExtractSelectedEntries(String selectionCriteria, string directoryPathInArchive, string extractDirectory, ExtractExistingFileAction extractExistingFile)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4041);
      foreach (ZipEntry e in SelectEntries(selectionCriteria, directoryPathInArchive)) {
        IACSharpSensor.IACSharpSensor.SensorReached(4042);
        e.Password = _Password;
        e.Extract(extractDirectory, extractExistingFile);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4043);
    }
  }
}
namespace Ionic
{
  internal abstract partial class SelectionCriterion
  {
    internal abstract bool Evaluate(Ionic.Zip.ZipEntry entry);
  }
  internal partial class NameCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4044);
      string transformedFileName = entry.FileName.Replace("/", "\\");
      System.Boolean RNTRNTRNT_407 = _Evaluate(transformedFileName);
      IACSharpSensor.IACSharpSensor.SensorReached(4045);
      return RNTRNTRNT_407;
    }
  }
  internal partial class SizeCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      System.Boolean RNTRNTRNT_408 = _Evaluate(entry.UncompressedSize);
      IACSharpSensor.IACSharpSensor.SensorReached(4046);
      return RNTRNTRNT_408;
    }
  }
  internal partial class TimeCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4047);
      DateTime x;
      IACSharpSensor.IACSharpSensor.SensorReached(4048);
      switch (Which) {
        case WhichTime.atime:
          IACSharpSensor.IACSharpSensor.SensorReached(4049);
          x = entry.AccessedTime;
          IACSharpSensor.IACSharpSensor.SensorReached(4050);
          break;
        case WhichTime.mtime:
          IACSharpSensor.IACSharpSensor.SensorReached(4051);
          x = entry.ModifiedTime;
          IACSharpSensor.IACSharpSensor.SensorReached(4052);
          break;
        case WhichTime.ctime:
          IACSharpSensor.IACSharpSensor.SensorReached(4053);
          x = entry.CreationTime;
          IACSharpSensor.IACSharpSensor.SensorReached(4054);
          break;
        default:
          IACSharpSensor.IACSharpSensor.SensorReached(4055);
          throw new ArgumentException("??time");
      }
      System.Boolean RNTRNTRNT_409 = _Evaluate(x);
      IACSharpSensor.IACSharpSensor.SensorReached(4056);
      return RNTRNTRNT_409;
    }
  }
  internal partial class TypeCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4057);
      bool result = (ObjectType == 'D') ? entry.IsDirectory : !entry.IsDirectory;
      IACSharpSensor.IACSharpSensor.SensorReached(4058);
      if (Operator != ComparisonOperator.EqualTo) {
        IACSharpSensor.IACSharpSensor.SensorReached(4059);
        result = !result;
      }
      System.Boolean RNTRNTRNT_410 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(4060);
      return RNTRNTRNT_410;
    }
  }
  internal partial class AttributesCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4061);
      FileAttributes fileAttrs = entry.Attributes;
      System.Boolean RNTRNTRNT_411 = _Evaluate(fileAttrs);
      IACSharpSensor.IACSharpSensor.SensorReached(4062);
      return RNTRNTRNT_411;
    }
  }
  internal partial class CompoundCriterion : SelectionCriterion
  {
    internal override bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4063);
      bool result = Left.Evaluate(entry);
      IACSharpSensor.IACSharpSensor.SensorReached(4064);
      switch (Conjunction) {
        case LogicalConjunction.AND:
          IACSharpSensor.IACSharpSensor.SensorReached(4065);
          if (result) {
            IACSharpSensor.IACSharpSensor.SensorReached(4066);
            result = Right.Evaluate(entry);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4067);
          break;
        case LogicalConjunction.OR:
          IACSharpSensor.IACSharpSensor.SensorReached(4068);
          if (!result) {
            IACSharpSensor.IACSharpSensor.SensorReached(4069);
            result = Right.Evaluate(entry);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4070);
          break;
        case LogicalConjunction.XOR:
          IACSharpSensor.IACSharpSensor.SensorReached(4071);
          result ^= Right.Evaluate(entry);
          IACSharpSensor.IACSharpSensor.SensorReached(4072);
          break;
      }
      System.Boolean RNTRNTRNT_412 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(4073);
      return RNTRNTRNT_412;
    }
  }
  public partial class FileSelector
  {
    private bool Evaluate(Ionic.Zip.ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4074);
      bool result = _Criterion.Evaluate(entry);
      System.Boolean RNTRNTRNT_413 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(4075);
      return RNTRNTRNT_413;
    }
    public ICollection<Ionic.Zip.ZipEntry> SelectEntries(Ionic.Zip.ZipFile zip)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4076);
      if (zip == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4077);
        throw new ArgumentNullException("zip");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4078);
      var list = new List<Ionic.Zip.ZipEntry>();
      IACSharpSensor.IACSharpSensor.SensorReached(4079);
      foreach (Ionic.Zip.ZipEntry e in zip) {
        IACSharpSensor.IACSharpSensor.SensorReached(4080);
        if (this.Evaluate(e)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4081);
          list.Add(e);
        }
      }
      ICollection<Ionic.Zip.ZipEntry> RNTRNTRNT_414 = list;
      IACSharpSensor.IACSharpSensor.SensorReached(4082);
      return RNTRNTRNT_414;
    }
    public ICollection<Ionic.Zip.ZipEntry> SelectEntries(Ionic.Zip.ZipFile zip, string directoryPathInArchive)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4083);
      if (zip == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4084);
        throw new ArgumentNullException("zip");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4085);
      var list = new List<Ionic.Zip.ZipEntry>();
      string slashSwapped = (directoryPathInArchive == null) ? null : directoryPathInArchive.Replace("/", "\\");
      IACSharpSensor.IACSharpSensor.SensorReached(4086);
      if (slashSwapped != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4087);
        while (slashSwapped.EndsWith("\\")) {
          IACSharpSensor.IACSharpSensor.SensorReached(4088);
          slashSwapped = slashSwapped.Substring(0, slashSwapped.Length - 1);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4089);
      foreach (Ionic.Zip.ZipEntry e in zip) {
        IACSharpSensor.IACSharpSensor.SensorReached(4090);
        if (directoryPathInArchive == null || (Path.GetDirectoryName(e.FileName) == directoryPathInArchive) || (Path.GetDirectoryName(e.FileName) == slashSwapped)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4091);
          if (this.Evaluate(e)) {
            IACSharpSensor.IACSharpSensor.SensorReached(4092);
            list.Add(e);
          }
        }
      }
      ICollection<Ionic.Zip.ZipEntry> RNTRNTRNT_415 = list;
      IACSharpSensor.IACSharpSensor.SensorReached(4093);
      return RNTRNTRNT_415;
    }
  }
}
