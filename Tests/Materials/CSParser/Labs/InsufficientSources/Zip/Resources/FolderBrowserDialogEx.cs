namespace Ionic.Utils
{
  using System;
  using System.Windows.Forms;
  using System.Runtime.InteropServices;
  using System.ComponentModel;
  using System.Security.Permissions;
  using System.Security;
  using System.Threading;
  public class FolderBrowserDialogEx : System.Windows.Forms.CommonDialog
  {
    private static readonly int MAX_PATH = 260;
    private PInvoke.BrowseFolderCallbackProc _callback;
    private string _descriptionText;
    private Environment.SpecialFolder _rootFolder;
    private string _selectedPath;
    private bool _selectedPathNeedsCheck;
    private bool _showNewFolderButton;
    private bool _showEditBox;
    private bool _showBothFilesAndFolders;
    private bool _newStyle = true;
    private bool _showFullPathInEditBox = true;
    private bool _dontIncludeNetworkFoldersBelowDomainLevel;
    private int _uiFlags;
    private IntPtr _hwndEdit;
    private IntPtr _rootFolderLocation;
    public new event EventHandler HelpRequest {
      add { base.HelpRequest += value; }
      remove { base.HelpRequest -= value; }
    }
    public FolderBrowserDialogEx()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4680);
      this.Reset();
      IACSharpSensor.IACSharpSensor.SensorReached(4681);
    }
    public static FolderBrowserDialogEx PrinterBrowser()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4682);
      FolderBrowserDialogEx x = new FolderBrowserDialogEx();
      x.BecomePrinterBrowser();
      FolderBrowserDialogEx RNTRNTRNT_520 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(4683);
      return RNTRNTRNT_520;
    }
    public static FolderBrowserDialogEx ComputerBrowser()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4684);
      FolderBrowserDialogEx x = new FolderBrowserDialogEx();
      x.BecomeComputerBrowser();
      FolderBrowserDialogEx RNTRNTRNT_521 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(4685);
      return RNTRNTRNT_521;
    }
    private void BecomePrinterBrowser()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4686);
      _uiFlags += BrowseFlags.BIF_BROWSEFORPRINTER;
      Description = "Select a printer:";
      PInvoke.Shell32.SHGetSpecialFolderLocation(IntPtr.Zero, CSIDL.PRINTERS, ref this._rootFolderLocation);
      ShowNewFolderButton = false;
      ShowEditBox = false;
      IACSharpSensor.IACSharpSensor.SensorReached(4687);
    }
    private void BecomeComputerBrowser()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4688);
      _uiFlags += BrowseFlags.BIF_BROWSEFORCOMPUTER;
      Description = "Select a computer:";
      PInvoke.Shell32.SHGetSpecialFolderLocation(IntPtr.Zero, CSIDL.NETWORK, ref this._rootFolderLocation);
      ShowNewFolderButton = false;
      ShowEditBox = false;
      IACSharpSensor.IACSharpSensor.SensorReached(4689);
    }
    private class CSIDL
    {
      public const int PRINTERS = 4;
      public const int NETWORK = 0x12;
    }
    private class BrowseFlags
    {
      public const int BIF_DEFAULT = 0x0;
      public const int BIF_BROWSEFORCOMPUTER = 0x1000;
      public const int BIF_BROWSEFORPRINTER = 0x2000;
      public const int BIF_BROWSEINCLUDEFILES = 0x4000;
      public const int BIF_BROWSEINCLUDEURLS = 0x80;
      public const int BIF_DONTGOBELOWDOMAIN = 0x2;
      public const int BIF_EDITBOX = 0x10;
      public const int BIF_NEWDIALOGSTYLE = 0x40;
      public const int BIF_NONEWFOLDERBUTTON = 0x200;
      public const int BIF_RETURNFSANCESTORS = 0x8;
      public const int BIF_RETURNONLYFSDIRS = 0x1;
      public const int BIF_SHAREABLE = 0x8000;
      public const int BIF_STATUSTEXT = 0x4;
      public const int BIF_UAHINT = 0x100;
      public const int BIF_VALIDATE = 0x20;
      public const int BIF_NOTRANSLATETARGETS = 0x400;
    }
    private static class BrowseForFolderMessages
    {
      public const int BFFM_INITIALIZED = 1;
      public const int BFFM_SELCHANGED = 2;
      public const int BFFM_VALIDATEFAILEDA = 3;
      public const int BFFM_VALIDATEFAILEDW = 4;
      public const int BFFM_IUNKNOWN = 5;
      public const int BFFM_SETSTATUSTEXT = 0x464;
      public const int BFFM_ENABLEOK = 0x465;
      public const int BFFM_SETSELECTIONA = 0x466;
      public const int BFFM_SETSELECTIONW = 0x467;
    }
    private int FolderBrowserCallback(IntPtr hwnd, int msg, IntPtr lParam, IntPtr lpData)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4690);
      switch (msg) {
        case BrowseForFolderMessages.BFFM_INITIALIZED:
          IACSharpSensor.IACSharpSensor.SensorReached(4691);
          if (this._selectedPath.Length != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(4692);
            PInvoke.User32.SendMessage(new HandleRef(null, hwnd), BrowseForFolderMessages.BFFM_SETSELECTIONW, 1, this._selectedPath);
            IACSharpSensor.IACSharpSensor.SensorReached(4693);
            if (this._showEditBox && this._showFullPathInEditBox) {
              IACSharpSensor.IACSharpSensor.SensorReached(4694);
              _hwndEdit = PInvoke.User32.FindWindowEx(new HandleRef(null, hwnd), IntPtr.Zero, "Edit", null);
              PInvoke.User32.SetWindowText(_hwndEdit, this._selectedPath);
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4695);
          break;
        case BrowseForFolderMessages.BFFM_SELCHANGED:
          IACSharpSensor.IACSharpSensor.SensorReached(4696);
          IntPtr pidl = lParam;
          IACSharpSensor.IACSharpSensor.SensorReached(4697);
          if (pidl != IntPtr.Zero) {
            IACSharpSensor.IACSharpSensor.SensorReached(4698);
            if (((_uiFlags & BrowseFlags.BIF_BROWSEFORPRINTER) == BrowseFlags.BIF_BROWSEFORPRINTER) || ((_uiFlags & BrowseFlags.BIF_BROWSEFORCOMPUTER) == BrowseFlags.BIF_BROWSEFORCOMPUTER)) {
              IACSharpSensor.IACSharpSensor.SensorReached(4699);
              PInvoke.User32.SendMessage(new HandleRef(null, hwnd), BrowseForFolderMessages.BFFM_ENABLEOK, 0, 1);
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(4700);
              IntPtr pszPath = Marshal.AllocHGlobal(MAX_PATH * Marshal.SystemDefaultCharSize);
              bool haveValidPath = PInvoke.Shell32.SHGetPathFromIDList(pidl, pszPath);
              String displayedPath = Marshal.PtrToStringAuto(pszPath);
              Marshal.FreeHGlobal(pszPath);
              PInvoke.User32.SendMessage(new HandleRef(null, hwnd), BrowseForFolderMessages.BFFM_ENABLEOK, 0, haveValidPath ? 1 : 0);
              IACSharpSensor.IACSharpSensor.SensorReached(4701);
              if (haveValidPath && !String.IsNullOrEmpty(displayedPath)) {
                IACSharpSensor.IACSharpSensor.SensorReached(4702);
                if (_showEditBox && _showFullPathInEditBox) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4703);
                  if (_hwndEdit != IntPtr.Zero) {
                    IACSharpSensor.IACSharpSensor.SensorReached(4704);
                    PInvoke.User32.SetWindowText(_hwndEdit, displayedPath);
                  }
                }
                IACSharpSensor.IACSharpSensor.SensorReached(4705);
                if ((_uiFlags & BrowseFlags.BIF_STATUSTEXT) == BrowseFlags.BIF_STATUSTEXT) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4706);
                  PInvoke.User32.SendMessage(new HandleRef(null, hwnd), BrowseForFolderMessages.BFFM_SETSTATUSTEXT, 0, displayedPath);
                }
              }
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4707);
          break;
      }
      System.Int32 RNTRNTRNT_522 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4708);
      return RNTRNTRNT_522;
    }
    private static PInvoke.IMalloc GetSHMalloc()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4709);
      PInvoke.IMalloc[] ppMalloc = new PInvoke.IMalloc[1];
      PInvoke.Shell32.SHGetMalloc(ppMalloc);
      PInvoke.IMalloc RNTRNTRNT_523 = ppMalloc[0];
      IACSharpSensor.IACSharpSensor.SensorReached(4710);
      return RNTRNTRNT_523;
    }
    public override void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4711);
      this._rootFolder = (Environment.SpecialFolder)0;
      this._descriptionText = string.Empty;
      this._selectedPath = string.Empty;
      this._selectedPathNeedsCheck = false;
      this._showNewFolderButton = true;
      this._showEditBox = true;
      this._newStyle = true;
      this._dontIncludeNetworkFoldersBelowDomainLevel = false;
      this._hwndEdit = IntPtr.Zero;
      this._rootFolderLocation = IntPtr.Zero;
      IACSharpSensor.IACSharpSensor.SensorReached(4712);
    }
    protected override bool RunDialog(IntPtr hWndOwner)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4713);
      bool result = false;
      IACSharpSensor.IACSharpSensor.SensorReached(4714);
      if (_rootFolderLocation == IntPtr.Zero) {
        IACSharpSensor.IACSharpSensor.SensorReached(4715);
        PInvoke.Shell32.SHGetSpecialFolderLocation(hWndOwner, (int)this._rootFolder, ref _rootFolderLocation);
        IACSharpSensor.IACSharpSensor.SensorReached(4716);
        if (_rootFolderLocation == IntPtr.Zero) {
          IACSharpSensor.IACSharpSensor.SensorReached(4717);
          PInvoke.Shell32.SHGetSpecialFolderLocation(hWndOwner, 0, ref _rootFolderLocation);
          IACSharpSensor.IACSharpSensor.SensorReached(4718);
          if (_rootFolderLocation == IntPtr.Zero) {
            IACSharpSensor.IACSharpSensor.SensorReached(4719);
            throw new InvalidOperationException("FolderBrowserDialogNoRootFolder");
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4720);
      _hwndEdit = IntPtr.Zero;
      IACSharpSensor.IACSharpSensor.SensorReached(4721);
      if (_dontIncludeNetworkFoldersBelowDomainLevel) {
        IACSharpSensor.IACSharpSensor.SensorReached(4722);
        _uiFlags += BrowseFlags.BIF_DONTGOBELOWDOMAIN;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4723);
      if (this._newStyle) {
        IACSharpSensor.IACSharpSensor.SensorReached(4724);
        _uiFlags += BrowseFlags.BIF_NEWDIALOGSTYLE;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4725);
      if (!this._showNewFolderButton) {
        IACSharpSensor.IACSharpSensor.SensorReached(4726);
        _uiFlags += BrowseFlags.BIF_NONEWFOLDERBUTTON;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4727);
      if (this._showEditBox) {
        IACSharpSensor.IACSharpSensor.SensorReached(4728);
        _uiFlags += BrowseFlags.BIF_EDITBOX;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4729);
      if (this._showBothFilesAndFolders) {
        IACSharpSensor.IACSharpSensor.SensorReached(4730);
        _uiFlags += BrowseFlags.BIF_BROWSEINCLUDEFILES;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4731);
      if (Control.CheckForIllegalCrossThreadCalls && (Application.OleRequired() != ApartmentState.STA)) {
        IACSharpSensor.IACSharpSensor.SensorReached(4732);
        throw new ThreadStateException("DebuggingException: ThreadMustBeSTA");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4733);
      IntPtr pidl = IntPtr.Zero;
      IntPtr hglobal = IntPtr.Zero;
      IntPtr pszPath = IntPtr.Zero;
      IACSharpSensor.IACSharpSensor.SensorReached(4734);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(4735);
        PInvoke.BROWSEINFO browseInfo = new PInvoke.BROWSEINFO();
        hglobal = Marshal.AllocHGlobal(MAX_PATH * Marshal.SystemDefaultCharSize);
        pszPath = Marshal.AllocHGlobal(MAX_PATH * Marshal.SystemDefaultCharSize);
        this._callback = new PInvoke.BrowseFolderCallbackProc(this.FolderBrowserCallback);
        browseInfo.pidlRoot = _rootFolderLocation;
        browseInfo.Owner = hWndOwner;
        browseInfo.pszDisplayName = hglobal;
        browseInfo.Title = this._descriptionText;
        browseInfo.Flags = _uiFlags;
        browseInfo.callback = this._callback;
        browseInfo.lParam = IntPtr.Zero;
        browseInfo.iImage = 0;
        pidl = PInvoke.Shell32.SHBrowseForFolder(browseInfo);
        IACSharpSensor.IACSharpSensor.SensorReached(4736);
        if (((_uiFlags & BrowseFlags.BIF_BROWSEFORPRINTER) == BrowseFlags.BIF_BROWSEFORPRINTER) || ((_uiFlags & BrowseFlags.BIF_BROWSEFORCOMPUTER) == BrowseFlags.BIF_BROWSEFORCOMPUTER)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4737);
          this._selectedPath = Marshal.PtrToStringAuto(browseInfo.pszDisplayName);
          result = true;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4738);
          if (pidl != IntPtr.Zero) {
            IACSharpSensor.IACSharpSensor.SensorReached(4739);
            PInvoke.Shell32.SHGetPathFromIDList(pidl, pszPath);
            this._selectedPathNeedsCheck = true;
            this._selectedPath = Marshal.PtrToStringAuto(pszPath);
            result = true;
          }
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(4740);
        PInvoke.IMalloc sHMalloc = GetSHMalloc();
        sHMalloc.Free(_rootFolderLocation);
        _rootFolderLocation = IntPtr.Zero;
        IACSharpSensor.IACSharpSensor.SensorReached(4741);
        if (pidl != IntPtr.Zero) {
          IACSharpSensor.IACSharpSensor.SensorReached(4742);
          sHMalloc.Free(pidl);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4743);
        if (pszPath != IntPtr.Zero) {
          IACSharpSensor.IACSharpSensor.SensorReached(4744);
          Marshal.FreeHGlobal(pszPath);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4745);
        if (hglobal != IntPtr.Zero) {
          IACSharpSensor.IACSharpSensor.SensorReached(4746);
          Marshal.FreeHGlobal(hglobal);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4747);
        this._callback = null;
        IACSharpSensor.IACSharpSensor.SensorReached(4748);
      }
      System.Boolean RNTRNTRNT_524 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(4749);
      return RNTRNTRNT_524;
    }
    public string Description {
      get {
        System.String RNTRNTRNT_525 = this._descriptionText;
        IACSharpSensor.IACSharpSensor.SensorReached(4750);
        return RNTRNTRNT_525;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4751);
        this._descriptionText = (value == null) ? string.Empty : value;
        IACSharpSensor.IACSharpSensor.SensorReached(4752);
      }
    }
    public Environment.SpecialFolder RootFolder {
      get {
        Environment.SpecialFolder RNTRNTRNT_526 = this._rootFolder;
        IACSharpSensor.IACSharpSensor.SensorReached(4753);
        return RNTRNTRNT_526;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4754);
        if (!Enum.IsDefined(typeof(Environment.SpecialFolder), value)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4755);
          throw new InvalidEnumArgumentException("value", (int)value, typeof(Environment.SpecialFolder));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4756);
        this._rootFolder = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4757);
      }
    }
    public string SelectedPath {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4758);
        if (((this._selectedPath != null) && (this._selectedPath.Length != 0)) && this._selectedPathNeedsCheck) {
          IACSharpSensor.IACSharpSensor.SensorReached(4759);
          new FileIOPermission(FileIOPermissionAccess.PathDiscovery, this._selectedPath).Demand();
          this._selectedPathNeedsCheck = false;
        }
        System.String RNTRNTRNT_527 = this._selectedPath;
        IACSharpSensor.IACSharpSensor.SensorReached(4760);
        return RNTRNTRNT_527;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4761);
        this._selectedPath = (value == null) ? string.Empty : value;
        this._selectedPathNeedsCheck = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4762);
      }
    }
    public bool ShowNewFolderButton {
      get {
        System.Boolean RNTRNTRNT_528 = this._showNewFolderButton;
        IACSharpSensor.IACSharpSensor.SensorReached(4763);
        return RNTRNTRNT_528;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4764);
        this._showNewFolderButton = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4765);
      }
    }
    public bool ShowEditBox {
      get {
        System.Boolean RNTRNTRNT_529 = this._showEditBox;
        IACSharpSensor.IACSharpSensor.SensorReached(4766);
        return RNTRNTRNT_529;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4767);
        this._showEditBox = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4768);
      }
    }
    public bool NewStyle {
      get {
        System.Boolean RNTRNTRNT_530 = this._newStyle;
        IACSharpSensor.IACSharpSensor.SensorReached(4769);
        return RNTRNTRNT_530;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4770);
        this._newStyle = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4771);
      }
    }
    public bool DontIncludeNetworkFoldersBelowDomainLevel {
      get {
        System.Boolean RNTRNTRNT_531 = _dontIncludeNetworkFoldersBelowDomainLevel;
        IACSharpSensor.IACSharpSensor.SensorReached(4772);
        return RNTRNTRNT_531;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4773);
        _dontIncludeNetworkFoldersBelowDomainLevel = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4774);
      }
    }
    public bool ShowFullPathInEditBox {
      get {
        System.Boolean RNTRNTRNT_532 = _showFullPathInEditBox;
        IACSharpSensor.IACSharpSensor.SensorReached(4775);
        return RNTRNTRNT_532;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4776);
        _showFullPathInEditBox = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4777);
      }
    }
    public bool ShowBothFilesAndFolders {
      get {
        System.Boolean RNTRNTRNT_533 = _showBothFilesAndFolders;
        IACSharpSensor.IACSharpSensor.SensorReached(4778);
        return RNTRNTRNT_533;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4779);
        _showBothFilesAndFolders = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4780);
      }
    }
  }
  static internal class PInvoke
  {
    static PInvoke()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4781);
    }
    public delegate int BrowseFolderCallbackProc(IntPtr hwnd, int msg, IntPtr lParam, IntPtr lpData);
    static internal class User32
    {
      [DllImport("user32.dll", CharSet = CharSet.Auto)]
      public static extern IntPtr SendMessage(HandleRef hWnd, int msg, int wParam, string lParam);
      [DllImport("user32.dll", CharSet = CharSet.Auto)]
      public static extern IntPtr SendMessage(HandleRef hWnd, int msg, int wParam, int lParam);
      [DllImport("user32.dll", SetLastError = true)]
      public static extern IntPtr FindWindowEx(HandleRef hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
      [DllImport("user32.dll", SetLastError = true)]
      public static extern Boolean SetWindowText(IntPtr hWnd, String text);
    }
    [ComImport(), Guid("00000002-0000-0000-c000-000000000046"), SuppressUnmanagedCodeSecurity(), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IMalloc
    {
      [PreserveSig()]
      IntPtr Alloc(int cb);
      [PreserveSig()]
      IntPtr Realloc(IntPtr pv, int cb);
      [PreserveSig()]
      void Free(IntPtr pv);
      [PreserveSig()]
      int GetSize(IntPtr pv);
      [PreserveSig()]
      int DidAlloc(IntPtr pv);
      [PreserveSig()]
      void HeapMinimize();
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public class BROWSEINFO
    {
      public IntPtr Owner;
      public IntPtr pidlRoot;
      public IntPtr pszDisplayName;
      public string Title;
      public int Flags;
      public BrowseFolderCallbackProc callback;
      public IntPtr lParam;
      public int iImage;
    }
    [SuppressUnmanagedCodeSecurity()]
    static internal class Shell32
    {
      [DllImport("shell32.dll", CharSet = CharSet.Auto)]
      public static extern IntPtr SHBrowseForFolder(      [In()]
PInvoke.BROWSEINFO lpbi);
      [DllImport("shell32.dll")]
      public static extern int SHGetMalloc(      [Out(), MarshalAs(UnmanagedType.LPArray)]
PInvoke.IMalloc[] ppMalloc);
      [DllImport("shell32.dll", CharSet = CharSet.Auto)]
      public static extern bool SHGetPathFromIDList(IntPtr pidl, IntPtr pszPath);
      [DllImport("shell32.dll")]
      public static extern int SHGetSpecialFolderLocation(IntPtr hwnd, int csidl, ref IntPtr ppidl);
    }
  }
}
