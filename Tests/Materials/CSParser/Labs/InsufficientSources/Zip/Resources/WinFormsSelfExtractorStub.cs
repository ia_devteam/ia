namespace Ionic.Zip
{
  using System;
  using System.Reflection;
  using System.IO;
  using System.Collections.Generic;
  using System.Windows.Forms;
  using System.Diagnostics;
  using System.Threading;
  using Ionic.Zip;
  using Ionic.Zip.Forms;
  public partial class WinFormsSelfExtractorStub : Form
  {
    private const string DllResourceName = "Ionic.Zip.dll";
    private int entryCount;
    private int Overwrite;
    private bool Interactive;
    private ManualResetEvent postUpackExeDone;
    delegate void ExtractEntryProgress(ExtractProgressEventArgs e);
    void _SetDefaultExtractLocation()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4800);
      this.txtExtractDirectory.Text = "@@EXTRACTLOCATION";
      this.txtExtractDirectory.Text = ReplaceEnvVars(this.txtExtractDirectory.Text);
      IACSharpSensor.IACSharpSensor.SensorReached(4801);
      if (this.txtExtractDirectory.Text.StartsWith("@@") && this.txtExtractDirectory.Text.EndsWith("EXTRACTLOCATION")) {
        IACSharpSensor.IACSharpSensor.SensorReached(4802);
        this.txtExtractDirectory.Text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), ZipName);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4803);
    }
    private string ReplaceEnvVars(string v)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4804);
      string s = v;
      System.Collections.IDictionary envVars = Environment.GetEnvironmentVariables();
      IACSharpSensor.IACSharpSensor.SensorReached(4805);
      foreach (System.Collections.DictionaryEntry de in envVars) {
        IACSharpSensor.IACSharpSensor.SensorReached(4806);
        string t = "%" + de.Key + "%";
        s = s.Replace(t, de.Value as String);
      }
      System.String RNTRNTRNT_536 = s;
      IACSharpSensor.IACSharpSensor.SensorReached(4807);
      return RNTRNTRNT_536;
    }
    private bool SetInteractiveFlag()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4808);
      bool result = false;
      Boolean.TryParse("@@QUIET", out result);
      Interactive = !result;
      System.Boolean RNTRNTRNT_537 = Interactive;
      IACSharpSensor.IACSharpSensor.SensorReached(4809);
      return RNTRNTRNT_537;
    }
    private int SetOverwriteBehavior()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4810);
      Int32 result = 0;
      Int32.TryParse("@@EXTRACT_EXISTING_FILE", out result);
      Overwrite = result;
      System.Int32 RNTRNTRNT_538 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(4811);
      return RNTRNTRNT_538;
    }
    private bool PostUnpackCmdLineIsSet()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4812);
      string s = txtPostUnpackCmdLine.Text;
      bool result = !(s.StartsWith("@@") && s.EndsWith("POST_UNPACK_CMD_LINE"));
      System.Boolean RNTRNTRNT_539 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(4813);
      return RNTRNTRNT_539;
    }
    void _SetPostUnpackCmdLine()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4814);
      this.txtPostUnpackCmdLine.Text = "@@POST_UNPACK_CMD_LINE";
      this.txtPostUnpackCmdLine.Text = ReplaceEnvVars(this.txtPostUnpackCmdLine.Text);
      IACSharpSensor.IACSharpSensor.SensorReached(4815);
      if (this.txtPostUnpackCmdLine.Text.StartsWith("@@") && this.txtPostUnpackCmdLine.Text.EndsWith("POST_UNPACK_CMD_LINE")) {
        IACSharpSensor.IACSharpSensor.SensorReached(4816);
        txtPostUnpackCmdLine.Enabled = txtPostUnpackCmdLine.Visible = false;
        chk_ExeAfterUnpack.Enabled = chk_ExeAfterUnpack.Visible = false;
        this.chk_Remove.Enabled = this.chk_Remove.Visible = false;
        int delta = this.progressBar1.Location.Y - this.chk_ExeAfterUnpack.Location.Y;
        this.MinimumSize = new System.Drawing.Size(this.MinimumSize.Width, this.MinimumSize.Height - (delta - 4));
        MoveDown(this.comboExistingFileAction, delta);
        MoveDown(this.label1, delta);
        MoveDown(this.chk_OpenExplorer, delta);
        MoveDown(this.btnDirBrowse, delta);
        MoveDown(this.txtExtractDirectory, delta);
        MoveDown(this.lblExtractDir, delta);
        this.Size = new System.Drawing.Size(this.Width, this.Height - (delta - 4));
        this.txtComment.Size = new System.Drawing.Size(this.txtComment.Width, this.txtComment.Height + delta);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4817);
        bool result = false;
        Boolean.TryParse("@@REMOVE_AFTER_EXECUTE", out result);
        this.chk_Remove.Checked = result;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4818);
    }
    private void MoveDown(System.Windows.Forms.Control c, int delta)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4819);
      c.Location = new System.Drawing.Point(c.Location.X, c.Location.Y + delta);
      IACSharpSensor.IACSharpSensor.SensorReached(4820);
    }
    private void FixTitle()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4821);
      string foo = "@@SFX_EXE_WINDOW_TITLE";
      IACSharpSensor.IACSharpSensor.SensorReached(4822);
      if (foo.StartsWith("@@") && foo.EndsWith("SFX_EXE_WINDOW_TITLE")) {
        IACSharpSensor.IACSharpSensor.SensorReached(4823);
        this.Text = String.Format("DotNetZip v{0} Self-extractor (www.codeplex.com/DotNetZip)", Ionic.Zip.ZipFile.LibraryVersion.ToString());
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4824);
        this.Text = foo;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4825);
    }
    private void HideComment()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4826);
      int smallerHeight = this.MinimumSize.Height - (this.txtComment.Height + this.lblComment.Height + 5);
      lblComment.Visible = false;
      txtComment.Visible = false;
      this.MinimumSize = new System.Drawing.Size(this.MinimumSize.Width, smallerHeight);
      this.MaximumSize = new System.Drawing.Size(this.MaximumSize.Width, this.MinimumSize.Height);
      this.Size = new System.Drawing.Size(this.Width, this.MinimumSize.Height);
      IACSharpSensor.IACSharpSensor.SensorReached(4827);
    }
    private void InitExtractExistingFileList()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4828);
      List<String> _ExtractActionNames = new List<string>(Enum.GetNames(typeof(Ionic.Zip.ExtractExistingFileAction)));
      IACSharpSensor.IACSharpSensor.SensorReached(4829);
      foreach (String name in _ExtractActionNames) {
        IACSharpSensor.IACSharpSensor.SensorReached(4830);
        if (!name.StartsWith("Invoke")) {
          IACSharpSensor.IACSharpSensor.SensorReached(4831);
          if (name.StartsWith("Throw")) {
            IACSharpSensor.IACSharpSensor.SensorReached(4832);
            comboExistingFileAction.Items.Add("Stop");
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(4833);
            comboExistingFileAction.Items.Add(name);
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4834);
      comboExistingFileAction.SelectedIndex = Overwrite;
      IACSharpSensor.IACSharpSensor.SensorReached(4835);
    }
    public WinFormsSelfExtractorStub()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4836);
      InitializeComponent();
      FixTitle();
      _setCancel = true;
      entryCount = 0;
      _SetDefaultExtractLocation();
      _SetPostUnpackCmdLine();
      SetInteractiveFlag();
      SetOverwriteBehavior();
      InitExtractExistingFileList();
      IACSharpSensor.IACSharpSensor.SensorReached(4837);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(4838);
        if (!String.IsNullOrEmpty(zip.Comment)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4839);
          txtComment.Text = zip.Comment;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4840);
          HideComment();
        }
      } catch (Exception e1) {
        IACSharpSensor.IACSharpSensor.SensorReached(4841);
        this.lblStatus.Text = "exception while resetting size: " + e1.ToString();
        HideComment();
        IACSharpSensor.IACSharpSensor.SensorReached(4842);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4843);
    }
    static WinFormsSelfExtractorStub()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4844);
      AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(Resolver);
      IACSharpSensor.IACSharpSensor.SensorReached(4845);
    }
    static System.Reflection.Assembly Resolver(object sender, ResolveEventArgs args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4846);
      Assembly a1 = Assembly.GetExecutingAssembly();
      IACSharpSensor.IACSharpSensor.SensorReached(4847);
      if (a1 == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4848);
        throw new Exception("GetExecutingAssembly returns null.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4849);
      string[] tokens = args.Name.Split(',');
      String[] names = a1.GetManifestResourceNames();
      IACSharpSensor.IACSharpSensor.SensorReached(4850);
      if (names == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4851);
        throw new Exception("GetManifestResourceNames returns null.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4852);
      Stream s = null;
      IACSharpSensor.IACSharpSensor.SensorReached(4853);
      foreach (string n in names) {
        IACSharpSensor.IACSharpSensor.SensorReached(4854);
        string root = n.Substring(0, n.Length - 4);
        string ext = n.Substring(n.Length - 3);
        IACSharpSensor.IACSharpSensor.SensorReached(4855);
        if (root.Equals(tokens[0]) && ext.ToLower().Equals("dll")) {
          IACSharpSensor.IACSharpSensor.SensorReached(4856);
          s = a1.GetManifestResourceStream(n);
          IACSharpSensor.IACSharpSensor.SensorReached(4857);
          if (s != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(4858);
            break;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4859);
      if (s == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4860);
        throw new Exception(String.Format("GetManifestResourceStream returns null. Available resources: [{0}]", String.Join("|", names)));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4861);
      byte[] block = new byte[s.Length];
      IACSharpSensor.IACSharpSensor.SensorReached(4862);
      if (block == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4863);
        throw new Exception(String.Format("Cannot allocated buffer of length({0}).", s.Length));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4864);
      s.Read(block, 0, block.Length);
      Assembly a2 = Assembly.Load(block);
      IACSharpSensor.IACSharpSensor.SensorReached(4865);
      if (a2 == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4866);
        throw new Exception("Assembly.Load(block) returns null");
      }
      System.Reflection.Assembly RNTRNTRNT_540 = a2;
      IACSharpSensor.IACSharpSensor.SensorReached(4867);
      return RNTRNTRNT_540;
    }
    private void Form_Shown(object sender, EventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4868);
      if (!Interactive) {
        IACSharpSensor.IACSharpSensor.SensorReached(4869);
        RemoveInteractiveComponents();
        KickoffExtract();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4870);
    }
    private void RemoveInteractiveComponents()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4871);
      if (this.btnContents.Visible) {
        IACSharpSensor.IACSharpSensor.SensorReached(4872);
        txtPostUnpackCmdLine.Enabled = txtPostUnpackCmdLine.Visible = false;
        chk_ExeAfterUnpack.Enabled = chk_ExeAfterUnpack.Visible = false;
        chk_Remove.Enabled = chk_Remove.Visible = false;
        comboExistingFileAction.Enabled = comboExistingFileAction.Visible = false;
        label1.Enabled = label1.Visible = false;
        chk_OpenExplorer.Checked = false;
        chk_OpenExplorer.Enabled = chk_OpenExplorer.Visible = false;
        btnDirBrowse.Enabled = btnDirBrowse.Visible = false;
        btnContents.Enabled = btnContents.Visible = false;
        btnExtract.Enabled = btnExtract.Visible = false;
        int delta = this.progressBar1.Location.Y - this.chk_OpenExplorer.Location.Y;
        this.MinimumSize = new System.Drawing.Size(this.MinimumSize.Width, this.MinimumSize.Height - (delta - 4));
        MoveDown(this.txtExtractDirectory, delta);
        MoveDown(this.lblExtractDir, delta);
        this.Size = new System.Drawing.Size(this.Width, this.Height - (delta - 4));
        IACSharpSensor.IACSharpSensor.SensorReached(4873);
        if (txtComment.Visible) {
          IACSharpSensor.IACSharpSensor.SensorReached(4874);
          this.txtComment.Size = new System.Drawing.Size(this.txtComment.Width, this.txtComment.Height + delta);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4875);
    }
    private void chk_ExeAfterUnpack_CheckedChanged(object sender, EventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4876);
      this.chk_Remove.Enabled = (this.chk_ExeAfterUnpack.Checked);
      IACSharpSensor.IACSharpSensor.SensorReached(4877);
    }
    private void btnDirBrowse_Click(object sender, EventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4878);
      Ionic.Utils.FolderBrowserDialogEx dlg1 = new Ionic.Utils.FolderBrowserDialogEx();
      dlg1.Description = "Select a folder for the extracted files:";
      dlg1.ShowNewFolderButton = true;
      dlg1.ShowEditBox = true;
      IACSharpSensor.IACSharpSensor.SensorReached(4879);
      if (Directory.Exists(txtExtractDirectory.Text)) {
        IACSharpSensor.IACSharpSensor.SensorReached(4880);
        dlg1.SelectedPath = txtExtractDirectory.Text;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4881);
        string d = txtExtractDirectory.Text;
        IACSharpSensor.IACSharpSensor.SensorReached(4882);
        while (d.Length > 2 && !Directory.Exists(d)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4883);
          d = Path.GetDirectoryName(d);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4884);
        if (d.Length < 2) {
          IACSharpSensor.IACSharpSensor.SensorReached(4885);
          dlg1.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4886);
          dlg1.SelectedPath = d;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4887);
      dlg1.ShowFullPathInEditBox = true;
      DialogResult result = dlg1.ShowDialog();
      IACSharpSensor.IACSharpSensor.SensorReached(4888);
      if (result == DialogResult.OK) {
        IACSharpSensor.IACSharpSensor.SensorReached(4889);
        txtExtractDirectory.Text = dlg1.SelectedPath;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4890);
    }
    private void btnExtract_Click(object sender, EventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4891);
      KickoffExtract();
      IACSharpSensor.IACSharpSensor.SensorReached(4892);
    }
    private void KickoffExtract()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4893);
      this.btnContents.Enabled = false;
      this.btnExtract.Enabled = false;
      this.chk_OpenExplorer.Enabled = false;
      this.comboExistingFileAction.Enabled = false;
      this.label1.Enabled = false;
      this.chk_ExeAfterUnpack.Enabled = false;
      this.chk_Remove.Enabled = false;
      this.txtExtractDirectory.Enabled = false;
      this.txtPostUnpackCmdLine.Enabled = false;
      this.btnDirBrowse.Enabled = false;
      this.btnExtract.Text = "Extracting...";
      ThreadPool.QueueUserWorkItem(new WaitCallback(DoExtract), null);
      this.Cursor = Cursors.WaitCursor;
      IACSharpSensor.IACSharpSensor.SensorReached(4894);
    }
    private void DoExtract(Object obj)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4895);
      List<string> itemsExtracted = new List<String>();
      string targetDirectory = txtExtractDirectory.Text;
      global::Ionic.Zip.ExtractExistingFileAction WantOverwrite = (Ionic.Zip.ExtractExistingFileAction)Overwrite;
      bool extractCancelled = false;
      System.Collections.Generic.List<String> didNotOverwrite = new System.Collections.Generic.List<String>();
      _setCancel = false;
      string currentPassword = "";
      SetProgressBars();
      IACSharpSensor.IACSharpSensor.SensorReached(4896);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(4897);
        zip.ExtractProgress += ExtractProgress;
        IACSharpSensor.IACSharpSensor.SensorReached(4898);
        foreach (global::Ionic.Zip.ZipEntry entry in zip) {
          IACSharpSensor.IACSharpSensor.SensorReached(4899);
          if (_setCancel) {
            IACSharpSensor.IACSharpSensor.SensorReached(4900);
            extractCancelled = true;
            IACSharpSensor.IACSharpSensor.SensorReached(4901);
            break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4902);
          if (entry.Encryption == global::Ionic.Zip.EncryptionAlgorithm.None) {
            IACSharpSensor.IACSharpSensor.SensorReached(4903);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(4904);
              entry.Extract(targetDirectory, WantOverwrite);
              entryCount++;
              itemsExtracted.Add(entry.FileName);
            } catch (Exception ex1) {
              IACSharpSensor.IACSharpSensor.SensorReached(4905);
              if (WantOverwrite != global::Ionic.Zip.ExtractExistingFileAction.OverwriteSilently && ex1.Message.Contains("already exists.")) {
                IACSharpSensor.IACSharpSensor.SensorReached(4906);
                didNotOverwrite.Add("    " + entry.FileName);
              } else if (WantOverwrite == global::Ionic.Zip.ExtractExistingFileAction.OverwriteSilently || ex1.Message.Contains("already exists.")) {
                IACSharpSensor.IACSharpSensor.SensorReached(4907);
                DialogResult result = MessageBox.Show(String.Format("Failed to extract entry {0} -- {1}", entry.FileName, ex1.Message), String.Format("Error Extracting {0}", entry.FileName), MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                IACSharpSensor.IACSharpSensor.SensorReached(4908);
                if (result == DialogResult.Cancel) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4909);
                  _setCancel = true;
                  IACSharpSensor.IACSharpSensor.SensorReached(4910);
                  break;
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(4911);
            }
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(4912);
            bool done = false;
            IACSharpSensor.IACSharpSensor.SensorReached(4913);
            while (!done) {
              IACSharpSensor.IACSharpSensor.SensorReached(4914);
              if (currentPassword == "") {
                IACSharpSensor.IACSharpSensor.SensorReached(4915);
                string t = PromptForPassword(entry.FileName);
                IACSharpSensor.IACSharpSensor.SensorReached(4916);
                if (t == "") {
                  IACSharpSensor.IACSharpSensor.SensorReached(4917);
                  done = true;
                  IACSharpSensor.IACSharpSensor.SensorReached(4918);
                  continue;
                }
                IACSharpSensor.IACSharpSensor.SensorReached(4919);
                currentPassword = t;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(4920);
              if (currentPassword == null) {
                IACSharpSensor.IACSharpSensor.SensorReached(4921);
                _setCancel = true;
                currentPassword = "";
                IACSharpSensor.IACSharpSensor.SensorReached(4922);
                break;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(4923);
              try {
                IACSharpSensor.IACSharpSensor.SensorReached(4924);
                entry.ExtractWithPassword(targetDirectory, WantOverwrite, currentPassword);
                entryCount++;
                itemsExtracted.Add(entry.FileName);
                done = true;
              } catch (Exception ex2) {
                IACSharpSensor.IACSharpSensor.SensorReached(4925);
                if (ex2 as Ionic.Zip.BadPasswordException != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4926);
                  currentPassword = "";
                  IACSharpSensor.IACSharpSensor.SensorReached(4927);
                  continue;
                } else if (WantOverwrite != global::Ionic.Zip.ExtractExistingFileAction.OverwriteSilently && ex2.Message.Contains("already exists.")) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4932);
                  didNotOverwrite.Add("    " + entry.FileName);
                  done = true;
                } else if (WantOverwrite == global::Ionic.Zip.ExtractExistingFileAction.OverwriteSilently && !ex2.Message.Contains("already exists.")) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4928);
                  DialogResult result = MessageBox.Show(String.Format("Failed to extract the password-encrypted entry {0} -- {1}", entry.FileName, ex2.Message.ToString()), String.Format("Error Extracting {0}", entry.FileName), MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                  done = true;
                  IACSharpSensor.IACSharpSensor.SensorReached(4929);
                  if (result == DialogResult.Cancel) {
                    IACSharpSensor.IACSharpSensor.SensorReached(4930);
                    _setCancel = true;
                    IACSharpSensor.IACSharpSensor.SensorReached(4931);
                    break;
                  }
                }
                IACSharpSensor.IACSharpSensor.SensorReached(4933);
              }
            }
          }
        }
      } catch (Exception) {
        IACSharpSensor.IACSharpSensor.SensorReached(4934);
        MessageBox.Show("The self-extracting zip file is corrupted.", "Error Extracting", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        Application.Exit();
        IACSharpSensor.IACSharpSensor.SensorReached(4935);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4936);
      if (didNotOverwrite.Count > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4937);
        UnzipStatusReport f = new UnzipStatusReport();
        IACSharpSensor.IACSharpSensor.SensorReached(4938);
        if (didNotOverwrite.Count == 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(4939);
          f.Header = "This file was not extracted because the target file already exists:";
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4940);
          f.Header = String.Format("These {0} files were not extracted because the target files already exist:", didNotOverwrite.Count);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4941);
        f.Message = String.Join("\r\n", didNotOverwrite.ToArray());
        f.ShowDialog();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4942);
      SetUiDone();
      IACSharpSensor.IACSharpSensor.SensorReached(4943);
      if (extractCancelled) {
        IACSharpSensor.IACSharpSensor.SensorReached(4944);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4945);
      if (chk_OpenExplorer.Checked) {
        IACSharpSensor.IACSharpSensor.SensorReached(4946);
        string w = System.Environment.GetEnvironmentVariable("WINDIR");
        IACSharpSensor.IACSharpSensor.SensorReached(4947);
        if (w == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4948);
          w = "c:\\windows";
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4949);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(4950);
          Process.Start(Path.Combine(w, "explorer.exe"), targetDirectory);
        } catch {
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4951);
      postUpackExeDone = new ManualResetEvent(false);
      IACSharpSensor.IACSharpSensor.SensorReached(4952);
      if (this.chk_ExeAfterUnpack.Checked && PostUnpackCmdLineIsSet()) {
        IACSharpSensor.IACSharpSensor.SensorReached(4953);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(4954);
          string[] cmd = SplitCommandLine(txtPostUnpackCmdLine.Text);
          IACSharpSensor.IACSharpSensor.SensorReached(4955);
          if (cmd != null && cmd.Length > 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(4956);
            object[] args = {
              cmd,
              this.chk_Remove.Checked,
              itemsExtracted,
              targetDirectory
            };
            ThreadPool.QueueUserWorkItem(new WaitCallback(StartPostUnpackProc), args);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(4957);
            postUpackExeDone.Set();
          }
        } catch {
          IACSharpSensor.IACSharpSensor.SensorReached(4958);
          postUpackExeDone.Set();
          IACSharpSensor.IACSharpSensor.SensorReached(4959);
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4960);
        postUpackExeDone.Set();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4961);
      if (!Interactive) {
        IACSharpSensor.IACSharpSensor.SensorReached(4962);
        postUpackExeDone.WaitOne();
        Application.Exit();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4963);
    }
    private delegate void StatusProvider(string h, string m);
    private void ProvideStatus(string header, string message)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4964);
      if (this.InvokeRequired) {
        IACSharpSensor.IACSharpSensor.SensorReached(4965);
        this.Invoke(new StatusProvider(this.ProvideStatus), new object[] {
          header,
          message
        });
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4966);
        UnzipStatusReport f = new UnzipStatusReport();
        f.Header = header;
        f.Message = message;
        f.ShowDialog();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4967);
    }
    private void StartPostUnpackProc(object arg)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4968);
      Object[] args = (object[])arg;
      String[] cmd = (String[])args[0];
      bool removeAfter = (bool)args[1];
      List<String> itemsToRemove = (List<String>)args[2];
      String basePath = (String)args[3];
      ProcessStartInfo startInfo = new ProcessStartInfo(cmd[0]);
      startInfo.WorkingDirectory = basePath;
      startInfo.CreateNoWindow = true;
      IACSharpSensor.IACSharpSensor.SensorReached(4969);
      if (cmd.Length > 1) {
        IACSharpSensor.IACSharpSensor.SensorReached(4970);
        startInfo.Arguments = cmd[1];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4971);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(4972);
        using (Process p = Process.Start(startInfo)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4973);
          if (p != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(4974);
            p.WaitForExit();
            IACSharpSensor.IACSharpSensor.SensorReached(4975);
            if (p.ExitCode == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(4976);
              if (removeAfter) {
                IACSharpSensor.IACSharpSensor.SensorReached(4977);
                List<string> failedToRemove = new List<String>();
                IACSharpSensor.IACSharpSensor.SensorReached(4978);
                foreach (string s in itemsToRemove) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4979);
                  string fullPath = Path.Combine(basePath, s);
                  IACSharpSensor.IACSharpSensor.SensorReached(4980);
                  try {
                    IACSharpSensor.IACSharpSensor.SensorReached(4981);
                    if (File.Exists(fullPath)) {
                      IACSharpSensor.IACSharpSensor.SensorReached(4982);
                      File.Delete(fullPath);
                    } else if (Directory.Exists(fullPath)) {
                      IACSharpSensor.IACSharpSensor.SensorReached(4983);
                      Directory.Delete(fullPath, true);
                    }
                  } catch {
                    IACSharpSensor.IACSharpSensor.SensorReached(4984);
                    failedToRemove.Add(s);
                    IACSharpSensor.IACSharpSensor.SensorReached(4985);
                  }
                  IACSharpSensor.IACSharpSensor.SensorReached(4986);
                  if (failedToRemove.Count > 0) {
                    IACSharpSensor.IACSharpSensor.SensorReached(4987);
                    string header = (failedToRemove.Count == 1) ? "This file was not removed:" : String.Format("These {0} files were not removed:", failedToRemove.Count);
                    string message = String.Join("\r\n", failedToRemove.ToArray());
                    ProvideStatus(header, message);
                  }
                }
              }
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(4988);
              ProvideStatus("Error Running Post-Unpack command", String.Format("Post-extract command failed, exit code {0}", p.ExitCode));
            }
          }
        }
      } catch (Exception exc1) {
        IACSharpSensor.IACSharpSensor.SensorReached(4989);
        ProvideStatus("Error Running Post-Unpack command", String.Format("The post-extract command failed: {0}", exc1.Message));
        IACSharpSensor.IACSharpSensor.SensorReached(4990);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4991);
      postUpackExeDone.Set();
      IACSharpSensor.IACSharpSensor.SensorReached(4992);
    }
    private void SetUiDone()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4993);
      if (this.btnExtract.InvokeRequired) {
        IACSharpSensor.IACSharpSensor.SensorReached(4994);
        this.btnExtract.Invoke(new MethodInvoker(this.SetUiDone));
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4995);
        this.lblStatus.Text = String.Format("Finished extracting {0} entries.", entryCount);
        btnExtract.Text = "Extracted.";
        btnExtract.Enabled = false;
        btnCancel.Text = "Quit";
        _setCancel = true;
        this.Cursor = Cursors.Default;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4996);
    }
    private void ExtractProgress(object sender, ExtractProgressEventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4997);
      if (e.EventType == ZipProgressEventType.Extracting_EntryBytesWritten) {
        IACSharpSensor.IACSharpSensor.SensorReached(4998);
        StepEntryProgress(e);
      } else if (e.EventType == ZipProgressEventType.Extracting_AfterExtractEntry) {
        IACSharpSensor.IACSharpSensor.SensorReached(4999);
        StepArchiveProgress(e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5000);
      if (_setCancel) {
        IACSharpSensor.IACSharpSensor.SensorReached(5001);
        e.Cancel = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5002);
    }
    private void StepArchiveProgress(ExtractProgressEventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5003);
      if (this.progressBar1.InvokeRequired) {
        IACSharpSensor.IACSharpSensor.SensorReached(5004);
        this.progressBar2.Invoke(new ExtractEntryProgress(this.StepArchiveProgress), new object[] { e });
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(5005);
        this.progressBar1.PerformStep();
        this.progressBar2.Value = this.progressBar2.Maximum = 1;
        this.lblStatus.Text = "";
        this.Update();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5006);
    }
    private void StepEntryProgress(ExtractProgressEventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5007);
      if (this.progressBar2.InvokeRequired) {
        IACSharpSensor.IACSharpSensor.SensorReached(5008);
        this.progressBar2.Invoke(new ExtractEntryProgress(this.StepEntryProgress), new object[] { e });
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(5009);
        if (this.progressBar2.Maximum == 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(5010);
          Int64 max = e.TotalBytesToTransfer;
          _progress2MaxFactor = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(5011);
          while (max > System.Int32.MaxValue) {
            IACSharpSensor.IACSharpSensor.SensorReached(5012);
            max /= 2;
            _progress2MaxFactor++;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5013);
          this.progressBar2.Maximum = (int)max;
          this.lblStatus.Text = String.Format("Extracting {0}/{1}: {2} ...", this.progressBar1.Value, zip.Entries.Count, e.CurrentEntry.FileName);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5014);
        int xferred = (int)(e.BytesTransferred >> _progress2MaxFactor);
        this.progressBar2.Value = (xferred >= this.progressBar2.Maximum) ? this.progressBar2.Maximum : xferred;
        this.Update();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5015);
    }
    private void SetProgressBars()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5016);
      if (this.progressBar1.InvokeRequired) {
        IACSharpSensor.IACSharpSensor.SensorReached(5017);
        this.progressBar1.Invoke(new MethodInvoker(this.SetProgressBars));
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(5018);
        this.progressBar1.Value = 0;
        this.progressBar1.Maximum = zip.Entries.Count;
        this.progressBar1.Minimum = 0;
        this.progressBar1.Step = 1;
        this.progressBar2.Value = 0;
        this.progressBar2.Minimum = 0;
        this.progressBar2.Maximum = 1;
        this.progressBar2.Step = 1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5019);
    }
    private String ZipName {
      get {
        String RNTRNTRNT_541 = Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetExecutingAssembly().Location);
        IACSharpSensor.IACSharpSensor.SensorReached(5020);
        return RNTRNTRNT_541;
      }
    }
    private Stream ZipStream {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5021);
        if (_s != null) {
          Stream RNTRNTRNT_542 = _s;
          IACSharpSensor.IACSharpSensor.SensorReached(5022);
          return RNTRNTRNT_542;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5023);
        Assembly a = Assembly.GetExecutingAssembly();
        _s = File.OpenRead(a.Location);
        Stream RNTRNTRNT_543 = _s;
        IACSharpSensor.IACSharpSensor.SensorReached(5024);
        return RNTRNTRNT_543;
      }
    }
    private ZipFile zip {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(5025);
        if (_zip == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(5026);
          _zip = global::Ionic.Zip.ZipFile.Read(ZipStream);
        }
        ZipFile RNTRNTRNT_544 = _zip;
        IACSharpSensor.IACSharpSensor.SensorReached(5027);
        return RNTRNTRNT_544;
      }
    }
    private string PromptForPassword(string entryName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5028);
      PasswordDialog dlg1 = new PasswordDialog();
      dlg1.EntryName = entryName;
      bool done = false;
      IACSharpSensor.IACSharpSensor.SensorReached(5029);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(5030);
        dlg1.ShowDialog();
        done = (dlg1.Result != PasswordDialog.PasswordDialogResult.OK || dlg1.Password != "");
      } while (!done);
      IACSharpSensor.IACSharpSensor.SensorReached(5031);
      if (dlg1.Result == PasswordDialog.PasswordDialogResult.OK) {
        System.String RNTRNTRNT_545 = dlg1.Password;
        IACSharpSensor.IACSharpSensor.SensorReached(5032);
        return RNTRNTRNT_545;
      } else if (dlg1.Result == PasswordDialog.PasswordDialogResult.Skip) {
        System.String RNTRNTRNT_546 = "";
        IACSharpSensor.IACSharpSensor.SensorReached(5033);
        return RNTRNTRNT_546;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5034);
      return null;
    }
    private void btnCancel_Click(object sender, EventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5035);
      if (_setCancel == false) {
        IACSharpSensor.IACSharpSensor.SensorReached(5036);
        _setCancel = true;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(5037);
        Application.Exit();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5038);
    }
    private void btnContents_Click(object sender, EventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5039);
      ZipContentsDialog dlg1 = new ZipContentsDialog();
      dlg1.ZipFile = zip;
      dlg1.ShowDialog();
      IACSharpSensor.IACSharpSensor.SensorReached(5040);
      return;
    }
    private string[] SplitCommandLine(string cmdline)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5041);
      if (cmdline[0] != '"') {
        System.String[] RNTRNTRNT_547 = cmdline.Split(new char[] { ' ' }, 2);
        IACSharpSensor.IACSharpSensor.SensorReached(5042);
        return RNTRNTRNT_547;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5043);
      int ix = cmdline.IndexOf('"', 1);
      IACSharpSensor.IACSharpSensor.SensorReached(5044);
      if (ix == -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(5045);
        return null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5046);
      if (ix + 1 == cmdline.Length) {
        System.String[] RNTRNTRNT_548 = new string[] { cmdline.Substring(1, ix - 1) };
        IACSharpSensor.IACSharpSensor.SensorReached(5047);
        return RNTRNTRNT_548;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5048);
      if (cmdline[ix + 1] != ' ') {
        IACSharpSensor.IACSharpSensor.SensorReached(5049);
        return null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5050);
      string[] args = new string[2];
      args[0] = cmdline.Substring(1, ix - 1);
      IACSharpSensor.IACSharpSensor.SensorReached(5051);
      while (cmdline[ix + 1] == ' ') {
        IACSharpSensor.IACSharpSensor.SensorReached(5052);
        ix++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5053);
      args[1] = cmdline.Substring(ix + 1);
      System.String[] RNTRNTRNT_549 = args;
      IACSharpSensor.IACSharpSensor.SensorReached(5054);
      return RNTRNTRNT_549;
    }
    private int _progress2MaxFactor;
    private bool _setCancel;
    Stream _s;
    global::Ionic.Zip.ZipFile _zip;
  }
  public class UnzipStatusReport : Form
  {
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox tbMessage;
    private System.Windows.Forms.Button btnOK;
    public UnzipStatusReport()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5055);
      InitializeComponent();
      IACSharpSensor.IACSharpSensor.SensorReached(5056);
    }
    private void UnzipStatusReport_Load(object sender, EventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5057);
      this.Text = "DotNetZip: Unzip status report...";
      IACSharpSensor.IACSharpSensor.SensorReached(5058);
    }
    private void btnOK_Click(object sender, EventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5059);
      DialogResult = DialogResult.OK;
      this.Close();
      IACSharpSensor.IACSharpSensor.SensorReached(5060);
    }
    public string Message {
      get {
        System.String RNTRNTRNT_550 = this.tbMessage.Text;
        IACSharpSensor.IACSharpSensor.SensorReached(5061);
        return RNTRNTRNT_550;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5062);
        this.tbMessage.Text = value;
        this.tbMessage.Select(0, 0);
        IACSharpSensor.IACSharpSensor.SensorReached(5063);
      }
    }
    public string Header {
      get {
        System.String RNTRNTRNT_551 = this.label1.Text;
        IACSharpSensor.IACSharpSensor.SensorReached(5064);
        return RNTRNTRNT_551;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5065);
        this.label1.Text = value;
        IACSharpSensor.IACSharpSensor.SensorReached(5066);
      }
    }
    private System.ComponentModel.IContainer components = null;
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5067);
      if (disposing && (components != null)) {
        IACSharpSensor.IACSharpSensor.SensorReached(5068);
        components.Dispose();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5069);
      base.Dispose(disposing);
      IACSharpSensor.IACSharpSensor.SensorReached(5070);
    }
    private void InitializeComponent()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5071);
      this.components = new System.ComponentModel.Container();
      this.label1 = new System.Windows.Forms.Label();
      this.tbMessage = new System.Windows.Forms.TextBox();
      this.btnOK = new System.Windows.Forms.Button();
      this.SuspendLayout();
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 12);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(50, 13);
      this.label1.TabIndex = 2;
      this.label1.Text = "Status";
      this.tbMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right) | System.Windows.Forms.AnchorStyles.Bottom)));
      this.tbMessage.Location = new System.Drawing.Point(20, 31);
      this.tbMessage.Name = "tbMessage";
      this.tbMessage.Multiline = true;
      this.tbMessage.ScrollBars = ScrollBars.Vertical;
      this.tbMessage.ReadOnly = true;
      this.tbMessage.Size = new System.Drawing.Size(340, 110);
      this.tbMessage.TabIndex = 10;
      this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnOK.Location = new System.Drawing.Point(290, 156);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(82, 24);
      this.btnOK.TabIndex = 20;
      this.btnOK.Text = "OK";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(380, 190);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.tbMessage);
      this.Controls.Add(this.btnOK);
      this.Name = "UnzipStatusReport";
      this.Text = "Not Unzipped";
      this.Load += new System.EventHandler(this.UnzipStatusReport_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
      IACSharpSensor.IACSharpSensor.SensorReached(5072);
    }
  }
  class WinFormsSelfExtractorStubProgram
  {
    [STAThread()]
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5073);
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new WinFormsSelfExtractorStub());
      IACSharpSensor.IACSharpSensor.SensorReached(5074);
    }
    [System.Runtime.InteropServices.DllImport("kernel32.dll")]
    private static extern bool AllocConsole();
    [System.Runtime.InteropServices.DllImport("kernel32.dll")]
    private static extern bool AttachConsole(int pid);
  }
}
