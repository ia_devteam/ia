namespace Ionic.Zip.Forms
{
  partial class ZipContentsDialog
  {
    private System.ComponentModel.IContainer components = null;
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5129);
      if (disposing && (components != null)) {
        IACSharpSensor.IACSharpSensor.SensorReached(5130);
        components.Dispose();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5131);
      base.Dispose(disposing);
      IACSharpSensor.IACSharpSensor.SensorReached(5132);
    }
    private void InitializeComponent()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5133);
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WinFormsSelfExtractorStub));
      this.listView1 = new System.Windows.Forms.ListView();
      this.button1 = new System.Windows.Forms.Button();
      this.SuspendLayout();
      this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
      this.listView1.Location = new System.Drawing.Point(12, 12);
      this.listView1.Name = "listView1";
      this.listView1.Size = new System.Drawing.Size(700, 287);
      this.listView1.TabIndex = 0;
      this.listView1.UseCompatibleStateImageBehavior = false;
      this.listView1.View = System.Windows.Forms.View.Details;
      this.listView1.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listView1_ColumnClick);
      this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.button1.Location = new System.Drawing.Point(637, 305);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(75, 23);
      this.button1.TabIndex = 1;
      this.button1.Text = "Close";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(724, 340);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.listView1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "ZipContentsDialog";
      this.Text = "Contents of the zip archive";
      this.ResumeLayout(false);
      IACSharpSensor.IACSharpSensor.SensorReached(5134);
    }
    private System.Windows.Forms.ListView listView1;
    private System.Windows.Forms.Button button1;
  }
}
