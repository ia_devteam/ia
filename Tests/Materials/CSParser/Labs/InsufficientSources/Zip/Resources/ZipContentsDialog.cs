namespace Ionic.Zip.Forms
{
  using System;
  using System.Collections.Generic;
  using System.Windows.Forms;
  using Ionic.Zip;
  public partial class ZipContentsDialog : Form
  {
    public ZipContentsDialog()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5081);
      InitializeComponent();
      FixTitle();
      IACSharpSensor.IACSharpSensor.SensorReached(5082);
    }
    private void FixTitle()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5083);
      this.Text = String.Format("Contents of the zip archive (DotNetZip v{0})", Ionic.Zip.ZipFile.LibraryVersion.ToString());
      IACSharpSensor.IACSharpSensor.SensorReached(5084);
    }
    public ZipFile ZipFile {
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(5085);
        listView1.Clear();
        listView1.BeginUpdate();
        string[] columnHeaders = new string[] {
          "name",
          "lastmod",
          "original",
          "ratio",
          "compressed",
          "enc?",
          "CRC"
        };
        IACSharpSensor.IACSharpSensor.SensorReached(5086);
        foreach (string label in columnHeaders) {
          IACSharpSensor.IACSharpSensor.SensorReached(5087);
          SortableColumnHeader ch = new SortableColumnHeader(label);
          IACSharpSensor.IACSharpSensor.SensorReached(5088);
          if (label != "name" && label != "lastmod") {
            IACSharpSensor.IACSharpSensor.SensorReached(5089);
            ch.TextAlign = HorizontalAlignment.Right;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5090);
          listView1.Columns.Add(ch);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5091);
        foreach (ZipEntry e in value) {
          IACSharpSensor.IACSharpSensor.SensorReached(5092);
          ListViewItem item = new ListViewItem(e.FileName);
          string[] subitems = new string[] {
            e.LastModified.ToString("yyyy-MM-dd HH:mm:ss"),
            e.UncompressedSize.ToString(),
            String.Format("{0,5:F0}%", e.CompressionRatio),
            e.CompressedSize.ToString(),
            (e.UsesEncryption) ? "Y" : "N",
            String.Format("{0:X8}", e.Crc)
          };
          IACSharpSensor.IACSharpSensor.SensorReached(5093);
          foreach (String s in subitems) {
            IACSharpSensor.IACSharpSensor.SensorReached(5094);
            ListViewItem.ListViewSubItem subitem = new ListViewItem.ListViewSubItem();
            subitem.Text = s;
            item.SubItems.Add(subitem);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(5095);
          this.listView1.Items.Add(item);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5096);
        listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        int aggWidth = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(5097);
        for (int i = 0; i < this.listView1.Columns.Count; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(5098);
          aggWidth += this.listView1.Columns[i].Width + 1;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(5099);
        aggWidth += this.listView1.Location.X * 4 + 4;
        this.Size = new System.Drawing.Size(aggWidth, this.Height);
        this.listView1.EndUpdate();
        IACSharpSensor.IACSharpSensor.SensorReached(5100);
      }
    }
    private void button1_Click(object sender, EventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5101);
      this.Close();
      IACSharpSensor.IACSharpSensor.SensorReached(5102);
    }
    private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5103);
      SortableColumnHeader clickedCol = (SortableColumnHeader)this.listView1.Columns[e.Column];
      clickedCol.SortAscending = !clickedCol.SortAscending;
      int numItems = this.listView1.Items.Count;
      this.listView1.BeginUpdate();
      List<ItemWrapper> list = new List<ItemWrapper>();
      IACSharpSensor.IACSharpSensor.SensorReached(5104);
      for (int i = 0; i < numItems; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5105);
        list.Add(new ItemWrapper(this.listView1.Items[i], e.Column));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5106);
      if (e.Column == 2 || e.Column == 4) {
        IACSharpSensor.IACSharpSensor.SensorReached(5107);
        list.Sort(new ItemWrapper.NumericComparer(clickedCol.SortAscending));
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(5108);
        list.Sort(new ItemWrapper.StringComparer(clickedCol.SortAscending));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5109);
      this.listView1.Items.Clear();
      IACSharpSensor.IACSharpSensor.SensorReached(5110);
      for (int i = 0; i < numItems; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(5111);
        this.listView1.Items.Add(list[i].Item);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(5112);
      this.listView1.EndUpdate();
      IACSharpSensor.IACSharpSensor.SensorReached(5113);
    }
  }
  public class SortableColumnHeader : ColumnHeader
  {
    public bool SortAscending;
    public SortableColumnHeader(string text)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5114);
      this.Text = text;
      this.SortAscending = true;
      IACSharpSensor.IACSharpSensor.SensorReached(5115);
    }
  }
  public class ItemWrapper
  {
    internal ListViewItem Item;
    internal int Column;
    public ItemWrapper(ListViewItem item, int column)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(5116);
      Item = item;
      Column = column;
      IACSharpSensor.IACSharpSensor.SensorReached(5117);
    }
    public string Text {
      get {
        System.String RNTRNTRNT_552 = Item.SubItems[Column].Text;
        IACSharpSensor.IACSharpSensor.SensorReached(5118);
        return RNTRNTRNT_552;
      }
    }
    public class StringComparer : IComparer<ItemWrapper>
    {
      bool @ascending;
      public StringComparer(bool asc)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(5119);
        this.@ascending = asc;
        IACSharpSensor.IACSharpSensor.SensorReached(5120);
      }
      public int Compare(ItemWrapper xItem, ItemWrapper yItem)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(5121);
        string xText = xItem.Item.SubItems[xItem.Column].Text;
        string yText = yItem.Item.SubItems[yItem.Column].Text;
        System.Int32 RNTRNTRNT_553 = xText.CompareTo(yText) * (this.@ascending ? 1 : -1);
        IACSharpSensor.IACSharpSensor.SensorReached(5122);
        return RNTRNTRNT_553;
      }
    }
    public class NumericComparer : IComparer<ItemWrapper>
    {
      bool @ascending;
      public NumericComparer(bool asc)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(5123);
        this.@ascending = asc;
        IACSharpSensor.IACSharpSensor.SensorReached(5124);
      }
      public int Compare(ItemWrapper xItem, ItemWrapper yItem)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(5125);
        int x = 0, y = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(5126);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(5127);
          x = Int32.Parse(xItem.Item.SubItems[xItem.Column].Text);
          y = Int32.Parse(yItem.Item.SubItems[yItem.Column].Text);
        } catch {
        }
        System.Int32 RNTRNTRNT_554 = (x - y) * (this.@ascending ? 1 : -1);
        IACSharpSensor.IACSharpSensor.SensorReached(5128);
        return RNTRNTRNT_554;
      }
    }
  }
}
