namespace Ionic.Zip.Forms
{
  using System;
  using System.Collections.Generic;
  using System.Windows.Forms;
  public partial class PasswordDialog : Form
  {
    public enum PasswordDialogResult
    {
      OK,
      Skip,
      Cancel
    }
    public PasswordDialog()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4782);
      InitializeComponent();
      this.textBox1.Focus();
      IACSharpSensor.IACSharpSensor.SensorReached(4783);
    }
    public PasswordDialogResult Result {
      get {
        PasswordDialogResult RNTRNTRNT_534 = _result;
        IACSharpSensor.IACSharpSensor.SensorReached(4784);
        return RNTRNTRNT_534;
      }
    }
    public string EntryName {
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4785);
        prompt.Text = "Enter the password for " + value;
        IACSharpSensor.IACSharpSensor.SensorReached(4786);
      }
    }
    public string Password {
      get {
        System.String RNTRNTRNT_535 = textBox1.Text;
        IACSharpSensor.IACSharpSensor.SensorReached(4787);
        return RNTRNTRNT_535;
      }
    }
    private void btnOk_Click(object sender, EventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4788);
      _result = PasswordDialogResult.OK;
      this.Close();
      IACSharpSensor.IACSharpSensor.SensorReached(4789);
    }
    private void btnCancel_Click(object sender, EventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4790);
      _result = PasswordDialogResult.Cancel;
      this.Close();
      IACSharpSensor.IACSharpSensor.SensorReached(4791);
    }
    private void button2_Click(object sender, EventArgs e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4792);
      _result = PasswordDialogResult.Skip;
      this.Close();
      IACSharpSensor.IACSharpSensor.SensorReached(4793);
    }
    private PasswordDialogResult _result;
  }
}
