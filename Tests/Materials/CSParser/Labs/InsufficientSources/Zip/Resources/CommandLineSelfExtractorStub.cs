namespace Ionic.Zip
{
  using System;
  using System.Reflection;
  using System.Resources;
  using System.IO;
  using System.Collections.Generic;
  using System.Diagnostics;
  using Ionic.Zip;
  public class CommandLineSelfExtractor
  {
    const string DllResourceName = "Ionic.Zip.dll";
    string TargetDirectory = "@@EXTRACTLOCATION";
    string PostUnpackCmdLine = "@@POST_UNPACK_CMD_LINE";
    bool ReplacedEnvVarsForTargetDirectory;
    bool ReplacedEnvVarsForCmdLine;
    bool ListOnly;
    bool Verbose;
    bool ReallyVerbose;
    bool RemoveFilesAfterExe;
    bool SkipPostUnpackCommand;
    string Password = null;
    int Overwrite;
    private bool PostUnpackCmdLineIsSet()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4495);
      bool result = !(PostUnpackCmdLine.StartsWith("@@") && PostUnpackCmdLine.EndsWith("POST_UNPACK_CMD_LINE"));
      IACSharpSensor.IACSharpSensor.SensorReached(4496);
      if (result && ReplacedEnvVarsForCmdLine == false) {
        IACSharpSensor.IACSharpSensor.SensorReached(4497);
        PostUnpackCmdLine = ReplaceEnvVars(PostUnpackCmdLine);
        ReplacedEnvVarsForCmdLine = true;
      }
      System.Boolean RNTRNTRNT_506 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(4498);
      return RNTRNTRNT_506;
    }
    private bool TargetDirectoryIsSet()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4499);
      bool result = !(TargetDirectory.StartsWith("@@") && TargetDirectory.EndsWith("EXTRACTLOCATION"));
      IACSharpSensor.IACSharpSensor.SensorReached(4500);
      if (result && ReplacedEnvVarsForTargetDirectory == false) {
        IACSharpSensor.IACSharpSensor.SensorReached(4501);
        TargetDirectory = ReplaceEnvVars(TargetDirectory);
        ReplacedEnvVarsForTargetDirectory = true;
      }
      System.Boolean RNTRNTRNT_507 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(4502);
      return RNTRNTRNT_507;
    }
    private string ReplaceEnvVars(string s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4503);
      System.Collections.IDictionary envVars = Environment.GetEnvironmentVariables();
      IACSharpSensor.IACSharpSensor.SensorReached(4504);
      foreach (System.Collections.DictionaryEntry de in envVars) {
        IACSharpSensor.IACSharpSensor.SensorReached(4505);
        string t = "%" + de.Key + "%";
        s = s.Replace(t, de.Value as String);
      }
      System.String RNTRNTRNT_508 = s;
      IACSharpSensor.IACSharpSensor.SensorReached(4506);
      return RNTRNTRNT_508;
    }
    private bool SetRemoveFilesFlag()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4507);
      bool result = false;
      Boolean.TryParse("@@REMOVE_AFTER_EXECUTE", out result);
      RemoveFilesAfterExe = result;
      System.Boolean RNTRNTRNT_509 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(4508);
      return RNTRNTRNT_509;
    }
    private bool SetVerboseFlag()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4509);
      bool result = false;
      Boolean.TryParse("@@QUIET", out result);
      Verbose = !result;
      System.Boolean RNTRNTRNT_510 = Verbose;
      IACSharpSensor.IACSharpSensor.SensorReached(4510);
      return RNTRNTRNT_510;
    }
    private int SetOverwriteBehavior()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4511);
      Int32 result = 0;
      Int32.TryParse("@@EXTRACT_EXISTING_FILE", out result);
      Overwrite = (int)result;
      System.Int32 RNTRNTRNT_511 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(4512);
      return RNTRNTRNT_511;
    }
    private CommandLineSelfExtractor()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4513);
      SetRemoveFilesFlag();
      SetVerboseFlag();
      SetOverwriteBehavior();
      PostUnpackCmdLineIsSet();
      TargetDirectoryIsSet();
      IACSharpSensor.IACSharpSensor.SensorReached(4514);
    }
    public CommandLineSelfExtractor(string[] args) : this()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4515);
      string specifiedDirectory = null;
      IACSharpSensor.IACSharpSensor.SensorReached(4516);
      for (int i = 0; i < args.Length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4517);
        switch (args[i]) {
          case "-d":
            IACSharpSensor.IACSharpSensor.SensorReached(4518);
            i++;
            IACSharpSensor.IACSharpSensor.SensorReached(4519);
            if (args.Length <= i) {
              IACSharpSensor.IACSharpSensor.SensorReached(4520);
              Console.WriteLine("please supply a directory.\n");
              GiveUsageAndExit();
            }
            IACSharpSensor.IACSharpSensor.SensorReached(4521);
            if (specifiedDirectory != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(4522);
              Console.WriteLine("You already provided a directory.\n");
              GiveUsageAndExit();
            }
            IACSharpSensor.IACSharpSensor.SensorReached(4523);
            specifiedDirectory = args[i];
            IACSharpSensor.IACSharpSensor.SensorReached(4524);
            break;
          case "-p":
            IACSharpSensor.IACSharpSensor.SensorReached(4525);
            i++;
            IACSharpSensor.IACSharpSensor.SensorReached(4526);
            if (args.Length <= i) {
              IACSharpSensor.IACSharpSensor.SensorReached(4527);
              Console.WriteLine("please supply a password.\n");
              GiveUsageAndExit();
            }
            IACSharpSensor.IACSharpSensor.SensorReached(4528);
            if (Password != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(4529);
              Console.WriteLine("You already provided a password.\n");
              GiveUsageAndExit();
            }
            IACSharpSensor.IACSharpSensor.SensorReached(4530);
            Password = args[i];
            IACSharpSensor.IACSharpSensor.SensorReached(4531);
            break;
          case "-o":
            IACSharpSensor.IACSharpSensor.SensorReached(4532);
            Overwrite = 1;
            IACSharpSensor.IACSharpSensor.SensorReached(4533);
            break;
          case "-n":
            IACSharpSensor.IACSharpSensor.SensorReached(4534);
            Overwrite = 2;
            IACSharpSensor.IACSharpSensor.SensorReached(4535);
            break;
          case "-l":
            IACSharpSensor.IACSharpSensor.SensorReached(4536);
            ListOnly = true;
            IACSharpSensor.IACSharpSensor.SensorReached(4537);
            break;
          case "-r+":
            IACSharpSensor.IACSharpSensor.SensorReached(4538);
            RemoveFilesAfterExe = true;
            IACSharpSensor.IACSharpSensor.SensorReached(4539);
            break;
          case "-r-":
            IACSharpSensor.IACSharpSensor.SensorReached(4540);
            RemoveFilesAfterExe = false;
            IACSharpSensor.IACSharpSensor.SensorReached(4541);
            break;
          case "-x":
            IACSharpSensor.IACSharpSensor.SensorReached(4542);
            SkipPostUnpackCommand = true;
            IACSharpSensor.IACSharpSensor.SensorReached(4543);
            break;
          case "-?":
            IACSharpSensor.IACSharpSensor.SensorReached(4544);
            GiveUsageAndExit();
            IACSharpSensor.IACSharpSensor.SensorReached(4545);
            break;
          case "-v-":
            IACSharpSensor.IACSharpSensor.SensorReached(4546);
            Verbose = false;
            IACSharpSensor.IACSharpSensor.SensorReached(4547);
            break;
          case "-v+":
            IACSharpSensor.IACSharpSensor.SensorReached(4548);
            if (Verbose) {
              IACSharpSensor.IACSharpSensor.SensorReached(4549);
              ReallyVerbose = true;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(4550);
              Verbose = true;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(4551);
            break;
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(4552);
            Console.WriteLine("unrecognized argument: '{0}'\n", args[i]);
            GiveUsageAndExit();
            IACSharpSensor.IACSharpSensor.SensorReached(4553);
            break;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4554);
      if (!ListOnly) {
        IACSharpSensor.IACSharpSensor.SensorReached(4555);
        if (specifiedDirectory != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4556);
          TargetDirectory = specifiedDirectory;
        } else if (!TargetDirectoryIsSet()) {
          IACSharpSensor.IACSharpSensor.SensorReached(4557);
          TargetDirectory = ".";
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4558);
      if (ListOnly && ((Overwrite != 0) || (specifiedDirectory != null))) {
        IACSharpSensor.IACSharpSensor.SensorReached(4559);
        Console.WriteLine("Inconsistent options.\n");
        GiveUsageAndExit();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4560);
    }
    private string[] SplitCommandLine(string cmdline)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4561);
      if (cmdline[0] != '"') {
        System.String[] RNTRNTRNT_512 = cmdline.Split(new char[] { ' ' }, 2);
        IACSharpSensor.IACSharpSensor.SensorReached(4562);
        return RNTRNTRNT_512;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4563);
      int ix = cmdline.IndexOf('"', 1);
      IACSharpSensor.IACSharpSensor.SensorReached(4564);
      if (ix == -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(4565);
        return null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4566);
      if (ix + 1 == cmdline.Length) {
        System.String[] RNTRNTRNT_513 = new string[] { cmdline.Substring(1, ix - 1) };
        IACSharpSensor.IACSharpSensor.SensorReached(4567);
        return RNTRNTRNT_513;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4568);
      if (cmdline[ix + 1] != ' ') {
        IACSharpSensor.IACSharpSensor.SensorReached(4569);
        return null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4570);
      string[] args = new string[2];
      args[0] = cmdline.Substring(1, ix - 1);
      IACSharpSensor.IACSharpSensor.SensorReached(4571);
      while (cmdline[ix + 1] == ' ') {
        IACSharpSensor.IACSharpSensor.SensorReached(4572);
        ix++;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4573);
      args[1] = cmdline.Substring(ix + 1);
      System.String[] RNTRNTRNT_514 = args;
      IACSharpSensor.IACSharpSensor.SensorReached(4574);
      return RNTRNTRNT_514;
    }
    static CommandLineSelfExtractor()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4575);
      AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(Resolver);
      IACSharpSensor.IACSharpSensor.SensorReached(4576);
    }
    static System.Reflection.Assembly Resolver(object sender, ResolveEventArgs args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4577);
      Assembly a1 = Assembly.GetExecutingAssembly();
      IACSharpSensor.IACSharpSensor.SensorReached(4578);
      if (a1 == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4579);
        throw new Exception("GetExecutingAssembly returns null.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4580);
      string[] tokens = args.Name.Split(',');
      String[] names = a1.GetManifestResourceNames();
      IACSharpSensor.IACSharpSensor.SensorReached(4581);
      if (names == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4582);
        throw new Exception("GetManifestResourceNames returns null.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4583);
      Stream s = null;
      IACSharpSensor.IACSharpSensor.SensorReached(4584);
      foreach (string n in names) {
        IACSharpSensor.IACSharpSensor.SensorReached(4585);
        string root = n.Substring(0, n.Length - 4);
        string ext = n.Substring(n.Length - 3);
        IACSharpSensor.IACSharpSensor.SensorReached(4586);
        if (root.Equals(tokens[0]) && ext.ToLower().Equals("dll")) {
          IACSharpSensor.IACSharpSensor.SensorReached(4587);
          s = a1.GetManifestResourceStream(n);
          IACSharpSensor.IACSharpSensor.SensorReached(4588);
          if (s != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(4589);
            break;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4590);
      if (s == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4591);
        throw new Exception(String.Format("GetManifestResourceStream returns null. Available resources: [{0}]", String.Join("|", names)));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4592);
      byte[] block = new byte[s.Length];
      IACSharpSensor.IACSharpSensor.SensorReached(4593);
      if (block == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4594);
        throw new Exception(String.Format("Cannot allocated buffer of length({0}).", s.Length));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4595);
      s.Read(block, 0, block.Length);
      Assembly a2 = Assembly.Load(block);
      IACSharpSensor.IACSharpSensor.SensorReached(4596);
      if (a2 == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4597);
        throw new Exception("Assembly.Load(block) returns null");
      }
      System.Reflection.Assembly RNTRNTRNT_515 = a2;
      IACSharpSensor.IACSharpSensor.SensorReached(4598);
      return RNTRNTRNT_515;
    }
    public int Run()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4599);
      List<String> itemsExtracted = new List<String>();
      global::Ionic.Zip.ExtractExistingFileAction WantOverwrite = (Ionic.Zip.ExtractExistingFileAction)Overwrite;
      Assembly a = Assembly.GetExecutingAssembly();
      int rc = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4600);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(4601);
        using (global::Ionic.Zip.ZipFile zip = global::Ionic.Zip.ZipFile.Read(a.Location)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4602);
          if (Verbose) {
            IACSharpSensor.IACSharpSensor.SensorReached(4603);
            Console.WriteLine("Command-Line Self Extractor generated by DotNetZip.");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4604);
          if (!ListOnly) {
            IACSharpSensor.IACSharpSensor.SensorReached(4605);
            if (Verbose) {
              IACSharpSensor.IACSharpSensor.SensorReached(4606);
              Console.Write("Extracting to {0}", TargetDirectory);
              System.Console.WriteLine(" (Existing file action: {0})", WantOverwrite.ToString());
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4607);
          bool header = true;
          IACSharpSensor.IACSharpSensor.SensorReached(4608);
          foreach (global::Ionic.Zip.ZipEntry entry in zip) {
            IACSharpSensor.IACSharpSensor.SensorReached(4609);
            if (ListOnly || ReallyVerbose) {
              IACSharpSensor.IACSharpSensor.SensorReached(4610);
              if (header) {
                IACSharpSensor.IACSharpSensor.SensorReached(4611);
                System.Console.WriteLine("Extracting Zip file: {0}", zip.Name);
                IACSharpSensor.IACSharpSensor.SensorReached(4612);
                if ((zip.Comment != null) && (zip.Comment != "")) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4613);
                  System.Console.WriteLine("Comment: {0}", zip.Comment);
                }
                IACSharpSensor.IACSharpSensor.SensorReached(4614);
                System.Console.WriteLine("\n{1,-22} {2,9}  {3,5}   {4,9}  {5,3} {6,8} {0}", "Filename", "Modified", "Size", "Ratio", "Packed", "pw?", "CRC");
                System.Console.WriteLine(new System.String('-', 80));
                header = false;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(4615);
              System.Console.WriteLine("{1,-22} {2,9} {3,5:F0}%   {4,9}  {5,3} {6:X8} {0}", entry.FileName, entry.LastModified.ToString("yyyy-MM-dd HH:mm:ss"), entry.UncompressedSize, entry.CompressionRatio, entry.CompressedSize, (entry.UsesEncryption) ? "Y" : "N", entry.Crc);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(4616);
            if (!ListOnly) {
              IACSharpSensor.IACSharpSensor.SensorReached(4617);
              if (Verbose && !ReallyVerbose) {
                IACSharpSensor.IACSharpSensor.SensorReached(4618);
                System.Console.WriteLine("  {0}", entry.FileName);
              }
              IACSharpSensor.IACSharpSensor.SensorReached(4619);
              if (entry.Encryption == global::Ionic.Zip.EncryptionAlgorithm.None) {
                IACSharpSensor.IACSharpSensor.SensorReached(4620);
                try {
                  IACSharpSensor.IACSharpSensor.SensorReached(4621);
                  entry.Extract(TargetDirectory, WantOverwrite);
                  itemsExtracted.Add(entry.FileName);
                } catch (Exception ex1) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4622);
                  Console.WriteLine("  Error -- {0}", ex1.Message);
                  rc++;
                  IACSharpSensor.IACSharpSensor.SensorReached(4623);
                }
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(4624);
                if (Password == null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4625);
                  Console.WriteLine("Cannot extract entry {0} without a password.", entry.FileName);
                  rc++;
                } else {
                  IACSharpSensor.IACSharpSensor.SensorReached(4626);
                  try {
                    IACSharpSensor.IACSharpSensor.SensorReached(4627);
                    entry.ExtractWithPassword(TargetDirectory, WantOverwrite, Password);
                    itemsExtracted.Add(entry.FileName);
                  } catch (Exception ex2) {
                    IACSharpSensor.IACSharpSensor.SensorReached(4628);
                    Console.WriteLine("  Error -- {0}", ex2.Message);
                    rc++;
                    IACSharpSensor.IACSharpSensor.SensorReached(4629);
                  }
                }
              }
            }
          }
        }
      } catch (Exception) {
        IACSharpSensor.IACSharpSensor.SensorReached(4630);
        Console.WriteLine("The self-extracting zip file is corrupted.");
        System.Int32 RNTRNTRNT_516 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(4631);
        return RNTRNTRNT_516;
        IACSharpSensor.IACSharpSensor.SensorReached(4632);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4633);
      if (rc != 0) {
        System.Int32 RNTRNTRNT_517 = rc;
        IACSharpSensor.IACSharpSensor.SensorReached(4634);
        return RNTRNTRNT_517;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4635);
      if (PostUnpackCmdLineIsSet() && !SkipPostUnpackCommand) {
        IACSharpSensor.IACSharpSensor.SensorReached(4636);
        if (ListOnly) {
          IACSharpSensor.IACSharpSensor.SensorReached(4637);
          Console.WriteLine("\nExecute on unpack: {0}", PostUnpackCmdLine);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(4638);
          try {
            IACSharpSensor.IACSharpSensor.SensorReached(4639);
            string[] args = SplitCommandLine(PostUnpackCmdLine);
            IACSharpSensor.IACSharpSensor.SensorReached(4640);
            if (args != null && args.Length > 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(4641);
              if (Verbose) {
                IACSharpSensor.IACSharpSensor.SensorReached(4642);
                System.Console.WriteLine("Running command:  {0}", PostUnpackCmdLine);
              }
              IACSharpSensor.IACSharpSensor.SensorReached(4643);
              ProcessStartInfo startInfo = new ProcessStartInfo(args[0]);
              startInfo.WorkingDirectory = TargetDirectory;
              startInfo.CreateNoWindow = true;
              IACSharpSensor.IACSharpSensor.SensorReached(4644);
              if (args.Length > 1) {
                IACSharpSensor.IACSharpSensor.SensorReached(4645);
                startInfo.Arguments = args[1];
              }
              IACSharpSensor.IACSharpSensor.SensorReached(4646);
              using (Process p = Process.Start(startInfo)) {
                IACSharpSensor.IACSharpSensor.SensorReached(4647);
                if (p != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(4648);
                  p.WaitForExit();
                  rc = p.ExitCode;
                  IACSharpSensor.IACSharpSensor.SensorReached(4649);
                  if (p.ExitCode == 0) {
                    IACSharpSensor.IACSharpSensor.SensorReached(4650);
                    if (RemoveFilesAfterExe) {
                      IACSharpSensor.IACSharpSensor.SensorReached(4651);
                      foreach (string s in itemsExtracted) {
                        IACSharpSensor.IACSharpSensor.SensorReached(4652);
                        string fullPath = Path.Combine(TargetDirectory, s);
                        IACSharpSensor.IACSharpSensor.SensorReached(4653);
                        try {
                          IACSharpSensor.IACSharpSensor.SensorReached(4654);
                          if (File.Exists(fullPath)) {
                            IACSharpSensor.IACSharpSensor.SensorReached(4655);
                            File.Delete(fullPath);
                          } else if (Directory.Exists(fullPath)) {
                            IACSharpSensor.IACSharpSensor.SensorReached(4656);
                            Directory.Delete(fullPath, true);
                          }
                        } catch {
                        }
                      }
                    }
                  }
                }
              }
            }
          } catch (Exception exc1) {
            IACSharpSensor.IACSharpSensor.SensorReached(4657);
            System.Console.WriteLine("{0}", exc1);
            rc = 5;
            IACSharpSensor.IACSharpSensor.SensorReached(4658);
          }
        }
      }
      System.Int32 RNTRNTRNT_518 = rc;
      IACSharpSensor.IACSharpSensor.SensorReached(4659);
      return RNTRNTRNT_518;
    }
    private void GiveUsageAndExit()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4660);
      Assembly a = Assembly.GetExecutingAssembly();
      string s = Path.GetFileName(a.Location);
      Console.WriteLine("DotNetZip Command-Line Self Extractor, see http://DotNetZip.codeplex.com/");
      Console.WriteLine("Copyright (c) 2008-2011 Dino Chiesa.");
      Console.WriteLine("usage:\n  {0} [-p <password>] [-d <directory>]", s);
      string more = "    Extracts entries from the archive. If any files to be extracted already\n" + "    exist, the program will stop.\n\n    Additional Options:\n" + "{0}" + "{1}" + "{2}" + "{3}";
      string overwriteString = String.Format("      -o    overwrite any existing files upon extraction{0}.\n" + "      -n    do not overwrite any existing files upon extraction{1}.\n", (Overwrite == 1) ? " (default)" : "", (Overwrite == 2) ? " (default)" : "");
      string removeString = PostUnpackCmdLineIsSet() ? String.Format("      -r+   remove files after the optional post-unpack exe completes{0}.\n" + "      -r-   don't remove files after the optional post-unpack exe completes{1}.\n", RemoveFilesAfterExe ? " (default)" : "", RemoveFilesAfterExe ? "" : " (default)") : "";
      string verbString = String.Format("      -v-   turn OFF verbose messages{0}.\n" + "      -v+   turn ON verbose messages{1}.\n", Verbose ? "" : " (default)", Verbose ? " (default)" : "");
      string cmdString = PostUnpackCmdLineIsSet() ? String.Format("      -x    don't run the post-unpack exe.\n            [cmd is: {0}]\n", PostUnpackCmdLine) : "";
      Console.WriteLine(more, overwriteString, removeString, cmdString, verbString);
      IACSharpSensor.IACSharpSensor.SensorReached(4661);
      if (TargetDirectoryIsSet()) {
        IACSharpSensor.IACSharpSensor.SensorReached(4662);
        Console.WriteLine("    default extract dir: [{0}]\n", TargetDirectory);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4663);
      Console.WriteLine("  {0} -l", s);
      Console.WriteLine("    Lists entries in the archive.");
      FreeConsole();
      Environment.Exit(1);
      IACSharpSensor.IACSharpSensor.SensorReached(4664);
    }
    [STAThread()]
    public static int Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4665);
      int left = 0;
      int top = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4666);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(4667);
        left = Console.CursorLeft;
        top = Console.CursorTop;
      } catch {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4668);
      bool wantPause = (left == 0 && top == 0);
      int rc = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(4669);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(4670);
        CommandLineSelfExtractor me = new CommandLineSelfExtractor(args);
        IACSharpSensor.IACSharpSensor.SensorReached(4671);
        if (!me.Verbose) {
          IACSharpSensor.IACSharpSensor.SensorReached(4672);
          IntPtr myHandle = Process.GetCurrentProcess().MainWindowHandle;
          ShowWindow(myHandle, SW_HIDE);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4673);
        rc = me.Run();
        IACSharpSensor.IACSharpSensor.SensorReached(4674);
        if (rc != 0 && wantPause && me.Verbose) {
          IACSharpSensor.IACSharpSensor.SensorReached(4675);
          Console.Write("<ENTER> to continue...");
          Console.ReadLine();
        }
      } catch (System.Exception exc1) {
        IACSharpSensor.IACSharpSensor.SensorReached(4676);
        Console.WriteLine("Exception while extracting: {0}", exc1.ToString());
        rc = 255;
        IACSharpSensor.IACSharpSensor.SensorReached(4677);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4678);
      FreeConsole();
      System.Int32 RNTRNTRNT_519 = rc;
      IACSharpSensor.IACSharpSensor.SensorReached(4679);
      return RNTRNTRNT_519;
    }
    private static readonly int SW_HIDE = 0;
    [System.Runtime.InteropServices.DllImport("user32.dll")]
    private static extern Boolean ShowWindow(IntPtr hWnd, Int32 nCmdShow);
    [System.Runtime.InteropServices.DllImport("kernel32.dll")]
    private static extern bool AttachConsole(int pid);
    [System.Runtime.InteropServices.DllImport("kernel32.dll")]
    private static extern bool AllocConsole();
    [System.Runtime.InteropServices.DllImport("kernel32.dll")]
    private static extern bool FreeConsole();
  }
}
