using System;
using System.IO;
using System.Collections.Generic;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Zip
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d00005")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public partial class ZipFile : System.Collections.IEnumerable, System.Collections.Generic.IEnumerable<ZipEntry>, IDisposable
  {
    public bool FullScan { get; set; }
    public bool SortEntriesBeforeSaving { get; set; }
    public bool AddDirectoryWillTraverseReparsePoints { get; set; }
    public int BufferSize {
      get {
        System.Int32 RNTRNTRNT_321 = _BufferSize;
        IACSharpSensor.IACSharpSensor.SensorReached(3175);
        return RNTRNTRNT_321;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3176);
        _BufferSize = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3177);
      }
    }
    public int CodecBufferSize { get; set; }
    public bool FlattenFoldersOnExtract { get; set; }
    public Ionic.Zlib.CompressionStrategy Strategy {
      get {
        Ionic.Zlib.CompressionStrategy RNTRNTRNT_322 = _Strategy;
        IACSharpSensor.IACSharpSensor.SensorReached(3178);
        return RNTRNTRNT_322;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3179);
        _Strategy = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3180);
      }
    }
    public string Name {
      get {
        System.String RNTRNTRNT_323 = _name;
        IACSharpSensor.IACSharpSensor.SensorReached(3181);
        return RNTRNTRNT_323;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3182);
        _name = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3183);
      }
    }
    public Ionic.Zlib.CompressionLevel CompressionLevel { get; set; }
    public Ionic.Zip.CompressionMethod CompressionMethod {
      get {
        Ionic.Zip.CompressionMethod RNTRNTRNT_324 = _compressionMethod;
        IACSharpSensor.IACSharpSensor.SensorReached(3184);
        return RNTRNTRNT_324;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3185);
        _compressionMethod = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3186);
      }
    }
    public string Comment {
      get {
        System.String RNTRNTRNT_325 = _Comment;
        IACSharpSensor.IACSharpSensor.SensorReached(3187);
        return RNTRNTRNT_325;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3188);
        _Comment = value;
        _contentsChanged = true;
        IACSharpSensor.IACSharpSensor.SensorReached(3189);
      }
    }
    public bool EmitTimesInWindowsFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_326 = _emitNtfsTimes;
        IACSharpSensor.IACSharpSensor.SensorReached(3190);
        return RNTRNTRNT_326;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3191);
        _emitNtfsTimes = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3192);
      }
    }
    public bool EmitTimesInUnixFormatWhenSaving {
      get {
        System.Boolean RNTRNTRNT_327 = _emitUnixTimes;
        IACSharpSensor.IACSharpSensor.SensorReached(3193);
        return RNTRNTRNT_327;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3194);
        _emitUnixTimes = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3195);
      }
    }
    internal bool Verbose {
      get {
        System.Boolean RNTRNTRNT_328 = (_StatusMessageTextWriter != null);
        IACSharpSensor.IACSharpSensor.SensorReached(3196);
        return RNTRNTRNT_328;
      }
    }
    public bool ContainsEntry(string name)
    {
      System.Boolean RNTRNTRNT_329 = _entries.ContainsKey(SharedUtilities.NormalizePathForUseInZipFile(name));
      IACSharpSensor.IACSharpSensor.SensorReached(3197);
      return RNTRNTRNT_329;
    }
    public bool CaseSensitiveRetrieval {
      get {
        System.Boolean RNTRNTRNT_330 = _CaseSensitiveRetrieval;
        IACSharpSensor.IACSharpSensor.SensorReached(3198);
        return RNTRNTRNT_330;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3199);
        if (value != _CaseSensitiveRetrieval) {
          IACSharpSensor.IACSharpSensor.SensorReached(3200);
          _CaseSensitiveRetrieval = value;
          _initEntriesDictionary();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3201);
      }
    }
    [Obsolete("Beginning with v1.9.1.6 of DotNetZip, this property is obsolete.  It will be removed in a future version of the library. Your applications should  use AlternateEncoding and AlternateEncodingUsage instead.")]
    public bool UseUnicodeAsNecessary {
      get {
        System.Boolean RNTRNTRNT_331 = (_alternateEncoding == System.Text.Encoding.GetEncoding("UTF-8")) && (_alternateEncodingUsage == ZipOption.AsNecessary);
        IACSharpSensor.IACSharpSensor.SensorReached(3202);
        return RNTRNTRNT_331;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3203);
        if (value) {
          IACSharpSensor.IACSharpSensor.SensorReached(3204);
          _alternateEncoding = System.Text.Encoding.GetEncoding("UTF-8");
          _alternateEncodingUsage = ZipOption.AsNecessary;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(3205);
          _alternateEncoding = Ionic.Zip.ZipFile.DefaultEncoding;
          _alternateEncodingUsage = ZipOption.Never;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3206);
      }
    }
    public Zip64Option UseZip64WhenSaving {
      get {
        Zip64Option RNTRNTRNT_332 = _zip64;
        IACSharpSensor.IACSharpSensor.SensorReached(3207);
        return RNTRNTRNT_332;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3208);
        _zip64 = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3209);
      }
    }
    public Nullable<bool> RequiresZip64 {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(3210);
        if (_entries.Count > 65534) {
          Nullable<System.Boolean> RNTRNTRNT_333 = new Nullable<bool>(true);
          IACSharpSensor.IACSharpSensor.SensorReached(3211);
          return RNTRNTRNT_333;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3212);
        if (!_hasBeenSaved || _contentsChanged) {
          IACSharpSensor.IACSharpSensor.SensorReached(3213);
          return null;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3214);
        foreach (ZipEntry e in _entries.Values) {
          IACSharpSensor.IACSharpSensor.SensorReached(3215);
          if (e.RequiresZip64.Value) {
            Nullable<System.Boolean> RNTRNTRNT_334 = new Nullable<bool>(true);
            IACSharpSensor.IACSharpSensor.SensorReached(3216);
            return RNTRNTRNT_334;
          }
        }
        Nullable<System.Boolean> RNTRNTRNT_335 = new Nullable<bool>(false);
        IACSharpSensor.IACSharpSensor.SensorReached(3217);
        return RNTRNTRNT_335;
      }
    }
    public Nullable<bool> OutputUsedZip64 {
      get {
        Nullable<System.Boolean> RNTRNTRNT_336 = _OutputUsesZip64;
        IACSharpSensor.IACSharpSensor.SensorReached(3218);
        return RNTRNTRNT_336;
      }
    }
    public Nullable<bool> InputUsesZip64 {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(3219);
        if (_entries.Count > 65534) {
          Nullable<System.Boolean> RNTRNTRNT_337 = true;
          IACSharpSensor.IACSharpSensor.SensorReached(3220);
          return RNTRNTRNT_337;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3221);
        foreach (ZipEntry e in this) {
          IACSharpSensor.IACSharpSensor.SensorReached(3222);
          if (e.Source != ZipEntrySource.ZipFile) {
            IACSharpSensor.IACSharpSensor.SensorReached(3223);
            return null;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3224);
          if (e._InputUsesZip64) {
            Nullable<System.Boolean> RNTRNTRNT_338 = true;
            IACSharpSensor.IACSharpSensor.SensorReached(3225);
            return RNTRNTRNT_338;
          }
        }
        Nullable<System.Boolean> RNTRNTRNT_339 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(3226);
        return RNTRNTRNT_339;
      }
    }
    [Obsolete("use AlternateEncoding instead.")]
    public System.Text.Encoding ProvisionalAlternateEncoding {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(3227);
        if (_alternateEncodingUsage == ZipOption.AsNecessary) {
          System.Text.Encoding RNTRNTRNT_340 = _alternateEncoding;
          IACSharpSensor.IACSharpSensor.SensorReached(3228);
          return RNTRNTRNT_340;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3229);
        return null;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3230);
        _alternateEncoding = value;
        _alternateEncodingUsage = ZipOption.AsNecessary;
        IACSharpSensor.IACSharpSensor.SensorReached(3231);
      }
    }
    public System.Text.Encoding AlternateEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_341 = _alternateEncoding;
        IACSharpSensor.IACSharpSensor.SensorReached(3232);
        return RNTRNTRNT_341;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3233);
        _alternateEncoding = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3234);
      }
    }
    public ZipOption AlternateEncodingUsage {
      get {
        ZipOption RNTRNTRNT_342 = _alternateEncodingUsage;
        IACSharpSensor.IACSharpSensor.SensorReached(3235);
        return RNTRNTRNT_342;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3236);
        _alternateEncodingUsage = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3237);
      }
    }
    public static System.Text.Encoding DefaultEncoding {
      get {
        System.Text.Encoding RNTRNTRNT_343 = _defaultEncoding;
        IACSharpSensor.IACSharpSensor.SensorReached(3238);
        return RNTRNTRNT_343;
      }
    }
    public TextWriter StatusMessageTextWriter {
      get {
        TextWriter RNTRNTRNT_344 = _StatusMessageTextWriter;
        IACSharpSensor.IACSharpSensor.SensorReached(3239);
        return RNTRNTRNT_344;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3240);
        _StatusMessageTextWriter = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3241);
      }
    }
    public String TempFileFolder {
      get {
        String RNTRNTRNT_345 = _TempFileFolder;
        IACSharpSensor.IACSharpSensor.SensorReached(3242);
        return RNTRNTRNT_345;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3243);
        _TempFileFolder = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3244);
        if (value == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3245);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3246);
        if (!Directory.Exists(value)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3247);
          throw new FileNotFoundException(String.Format("That directory ({0}) does not exist.", value));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3248);
      }
    }
    public String Password {
      private get {
        String RNTRNTRNT_346 = _Password;
        IACSharpSensor.IACSharpSensor.SensorReached(3249);
        return RNTRNTRNT_346;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3250);
        _Password = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3251);
        if (_Password == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3252);
          Encryption = EncryptionAlgorithm.None;
        } else if (Encryption == EncryptionAlgorithm.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(3253);
          Encryption = EncryptionAlgorithm.PkzipWeak;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3254);
      }
    }
    public ExtractExistingFileAction ExtractExistingFile { get; set; }
    public ZipErrorAction ZipErrorAction {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(3255);
        if (ZipError != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3256);
          _zipErrorAction = ZipErrorAction.InvokeErrorEvent;
        }
        ZipErrorAction RNTRNTRNT_347 = _zipErrorAction;
        IACSharpSensor.IACSharpSensor.SensorReached(3257);
        return RNTRNTRNT_347;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3258);
        _zipErrorAction = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3259);
        if (_zipErrorAction != ZipErrorAction.InvokeErrorEvent && ZipError != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3260);
          ZipError = null;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3261);
      }
    }
    public EncryptionAlgorithm Encryption {
      get {
        EncryptionAlgorithm RNTRNTRNT_348 = _Encryption;
        IACSharpSensor.IACSharpSensor.SensorReached(3262);
        return RNTRNTRNT_348;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3263);
        if (value == EncryptionAlgorithm.Unsupported) {
          IACSharpSensor.IACSharpSensor.SensorReached(3264);
          throw new InvalidOperationException("You may not set Encryption to that value.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3265);
        _Encryption = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3266);
      }
    }
    public SetCompressionCallback SetCompression { get; set; }
    public Int32 MaxOutputSegmentSize {
      get {
        Int32 RNTRNTRNT_349 = _maxOutputSegmentSize;
        IACSharpSensor.IACSharpSensor.SensorReached(3267);
        return RNTRNTRNT_349;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3268);
        if (value < 65536 && value != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3269);
          throw new ZipException("The minimum acceptable segment size is 65536.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3270);
        _maxOutputSegmentSize = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3271);
      }
    }
    public Int32 NumberOfSegmentsForMostRecentSave {
      get {
        Int32 RNTRNTRNT_350 = unchecked((Int32)_numberOfSegmentsForMostRecentSave + 1);
        IACSharpSensor.IACSharpSensor.SensorReached(3272);
        return RNTRNTRNT_350;
      }
    }
    public long ParallelDeflateThreshold {
      get {
        System.Int64 RNTRNTRNT_351 = _ParallelDeflateThreshold;
        IACSharpSensor.IACSharpSensor.SensorReached(3273);
        return RNTRNTRNT_351;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3274);
        if ((value != 0) && (value != -1) && (value < 64 * 1024)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3275);
          throw new ArgumentOutOfRangeException("ParallelDeflateThreshold should be -1, 0, or > 65536");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3276);
        _ParallelDeflateThreshold = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3277);
      }
    }
    public int ParallelDeflateMaxBufferPairs {
      get {
        System.Int32 RNTRNTRNT_352 = _maxBufferPairs;
        IACSharpSensor.IACSharpSensor.SensorReached(3278);
        return RNTRNTRNT_352;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3279);
        if (value < 4) {
          IACSharpSensor.IACSharpSensor.SensorReached(3280);
          throw new ArgumentOutOfRangeException("ParallelDeflateMaxBufferPairs", "Value must be 4 or greater.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3281);
        _maxBufferPairs = value;
        IACSharpSensor.IACSharpSensor.SensorReached(3282);
      }
    }
    public override String ToString()
    {
      String RNTRNTRNT_353 = String.Format("ZipFile::{0}", Name);
      IACSharpSensor.IACSharpSensor.SensorReached(3283);
      return RNTRNTRNT_353;
    }
    public static System.Version LibraryVersion {
      get {
        System.Version RNTRNTRNT_354 = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        IACSharpSensor.IACSharpSensor.SensorReached(3284);
        return RNTRNTRNT_354;
      }
    }
    internal void NotifyEntryChanged()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3285);
      _contentsChanged = true;
      IACSharpSensor.IACSharpSensor.SensorReached(3286);
    }
    internal Stream StreamForDiskNumber(uint diskNumber)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3287);
      if (diskNumber + 1 == this._diskNumberWithCd || (diskNumber == 0 && this._diskNumberWithCd == 0)) {
        Stream RNTRNTRNT_355 = this.ReadStream;
        IACSharpSensor.IACSharpSensor.SensorReached(3288);
        return RNTRNTRNT_355;
      }
      Stream RNTRNTRNT_356 = ZipSegmentedStream.ForReading(this._readName ?? this._name, diskNumber, _diskNumberWithCd);
      IACSharpSensor.IACSharpSensor.SensorReached(3289);
      return RNTRNTRNT_356;
    }
    internal void Reset(bool whileSaving)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3290);
      if (_JustSaved) {
        IACSharpSensor.IACSharpSensor.SensorReached(3291);
        using (ZipFile x = new ZipFile()) {
          IACSharpSensor.IACSharpSensor.SensorReached(3292);
          x._readName = x._name = whileSaving ? (this._readName ?? this._name) : this._name;
          x.AlternateEncoding = this.AlternateEncoding;
          x.AlternateEncodingUsage = this.AlternateEncodingUsage;
          ReadIntoInstance(x);
          IACSharpSensor.IACSharpSensor.SensorReached(3293);
          foreach (ZipEntry e1 in x) {
            IACSharpSensor.IACSharpSensor.SensorReached(3294);
            foreach (ZipEntry e2 in this) {
              IACSharpSensor.IACSharpSensor.SensorReached(3295);
              if (e1.FileName == e2.FileName) {
                IACSharpSensor.IACSharpSensor.SensorReached(3296);
                e2.CopyMetaData(e1);
                IACSharpSensor.IACSharpSensor.SensorReached(3297);
                break;
              }
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3298);
        _JustSaved = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3299);
    }
    public ZipFile(string fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3300);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3301);
        _InitInstance(fileName, null);
      } catch (Exception e1) {
        IACSharpSensor.IACSharpSensor.SensorReached(3302);
        throw new ZipException(String.Format("Could not read {0} as a zip file", fileName), e1);
        IACSharpSensor.IACSharpSensor.SensorReached(3303);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3304);
    }
    public ZipFile(string fileName, System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3305);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3306);
        AlternateEncoding = encoding;
        AlternateEncodingUsage = ZipOption.Always;
        _InitInstance(fileName, null);
      } catch (Exception e1) {
        IACSharpSensor.IACSharpSensor.SensorReached(3307);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
        IACSharpSensor.IACSharpSensor.SensorReached(3308);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3309);
    }
    public ZipFile()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3310);
      _InitInstance(null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(3311);
    }
    public ZipFile(System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3312);
      AlternateEncoding = encoding;
      AlternateEncodingUsage = ZipOption.Always;
      _InitInstance(null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(3313);
    }
    public ZipFile(string fileName, TextWriter statusMessageWriter)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3314);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3315);
        _InitInstance(fileName, statusMessageWriter);
      } catch (Exception e1) {
        IACSharpSensor.IACSharpSensor.SensorReached(3316);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
        IACSharpSensor.IACSharpSensor.SensorReached(3317);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3318);
    }
    public ZipFile(string fileName, TextWriter statusMessageWriter, System.Text.Encoding encoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3319);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3320);
        AlternateEncoding = encoding;
        AlternateEncodingUsage = ZipOption.Always;
        _InitInstance(fileName, statusMessageWriter);
      } catch (Exception e1) {
        IACSharpSensor.IACSharpSensor.SensorReached(3321);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
        IACSharpSensor.IACSharpSensor.SensorReached(3322);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3323);
    }
    public void Initialize(string fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3324);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3325);
        _InitInstance(fileName, null);
      } catch (Exception e1) {
        IACSharpSensor.IACSharpSensor.SensorReached(3326);
        throw new ZipException(String.Format("{0} is not a valid zip file", fileName), e1);
        IACSharpSensor.IACSharpSensor.SensorReached(3327);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3328);
    }
    private void _initEntriesDictionary()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3329);
      StringComparer sc = (CaseSensitiveRetrieval) ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase;
      _entries = (_entries == null) ? new Dictionary<String, ZipEntry>(sc) : new Dictionary<String, ZipEntry>(_entries, sc);
      IACSharpSensor.IACSharpSensor.SensorReached(3330);
    }
    private void _InitInstance(string zipFileName, TextWriter statusMessageWriter)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3331);
      _name = zipFileName;
      _StatusMessageTextWriter = statusMessageWriter;
      _contentsChanged = true;
      AddDirectoryWillTraverseReparsePoints = true;
      CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
      ParallelDeflateThreshold = 512 * 1024;
      _initEntriesDictionary();
      IACSharpSensor.IACSharpSensor.SensorReached(3332);
      if (File.Exists(_name)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3333);
        if (FullScan) {
          IACSharpSensor.IACSharpSensor.SensorReached(3334);
          ReadIntoInstance_Orig(this);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(3335);
          ReadIntoInstance(this);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3336);
        this._fileAlreadyExists = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3337);
      return;
    }
    private List<ZipEntry> ZipEntriesAsList {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(3338);
        if (_zipEntriesAsList == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3339);
          _zipEntriesAsList = new List<ZipEntry>(_entries.Values);
        }
        List<ZipEntry> RNTRNTRNT_357 = _zipEntriesAsList;
        IACSharpSensor.IACSharpSensor.SensorReached(3340);
        return RNTRNTRNT_357;
      }
    }
    public ZipEntry this[int ix] {
      get {
        ZipEntry RNTRNTRNT_358 = ZipEntriesAsList[ix];
        IACSharpSensor.IACSharpSensor.SensorReached(3341);
        return RNTRNTRNT_358;
      }
    }
    public ZipEntry this[String fileName] {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(3342);
        var key = SharedUtilities.NormalizePathForUseInZipFile(fileName);
        IACSharpSensor.IACSharpSensor.SensorReached(3343);
        if (_entries.ContainsKey(key)) {
          ZipEntry RNTRNTRNT_359 = _entries[key];
          IACSharpSensor.IACSharpSensor.SensorReached(3344);
          return RNTRNTRNT_359;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3345);
        key = key.Replace("/", "\\");
        IACSharpSensor.IACSharpSensor.SensorReached(3346);
        if (_entries.ContainsKey(key)) {
          ZipEntry RNTRNTRNT_360 = _entries[key];
          IACSharpSensor.IACSharpSensor.SensorReached(3347);
          return RNTRNTRNT_360;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3348);
        return null;
      }
    }
    public System.Collections.Generic.ICollection<String> EntryFileNames {
      get {
        System.Collections.Generic.ICollection<String> RNTRNTRNT_361 = _entries.Keys;
        IACSharpSensor.IACSharpSensor.SensorReached(3349);
        return RNTRNTRNT_361;
      }
    }
    public System.Collections.Generic.ICollection<ZipEntry> Entries {
      get {
        System.Collections.Generic.ICollection<ZipEntry> RNTRNTRNT_362 = _entries.Values;
        IACSharpSensor.IACSharpSensor.SensorReached(3350);
        return RNTRNTRNT_362;
      }
    }
    public System.Collections.Generic.ICollection<ZipEntry> EntriesSorted {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(3351);
        var coll = new System.Collections.Generic.List<ZipEntry>();
        IACSharpSensor.IACSharpSensor.SensorReached(3352);
        foreach (var e in this.Entries) {
          IACSharpSensor.IACSharpSensor.SensorReached(3353);
          coll.Add(e);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3354);
        StringComparison sc = (CaseSensitiveRetrieval) ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase;
        coll.Sort((x, y) => { return String.Compare(x.FileName, y.FileName, sc); });
        System.Collections.Generic.ICollection<ZipEntry> RNTRNTRNT_363 = coll.AsReadOnly();
        IACSharpSensor.IACSharpSensor.SensorReached(3355);
        return RNTRNTRNT_363;
      }
    }
    public int Count {
      get {
        System.Int32 RNTRNTRNT_364 = _entries.Count;
        IACSharpSensor.IACSharpSensor.SensorReached(3356);
        return RNTRNTRNT_364;
      }
    }
    public void RemoveEntry(ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3357);
      if (entry == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3358);
        throw new ArgumentNullException("entry");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3359);
      _entries.Remove(SharedUtilities.NormalizePathForUseInZipFile(entry.FileName));
      _zipEntriesAsList = null;
      _contentsChanged = true;
      IACSharpSensor.IACSharpSensor.SensorReached(3360);
    }
    public void RemoveEntry(String fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3361);
      string modifiedName = ZipEntry.NameInArchive(fileName, null);
      ZipEntry e = this[modifiedName];
      IACSharpSensor.IACSharpSensor.SensorReached(3362);
      if (e == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3363);
        throw new ArgumentException("The entry you specified was not found in the zip archive.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3364);
      RemoveEntry(e);
      IACSharpSensor.IACSharpSensor.SensorReached(3365);
    }
    public void Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3366);
      Dispose(true);
      GC.SuppressFinalize(this);
      IACSharpSensor.IACSharpSensor.SensorReached(3367);
    }
    protected virtual void Dispose(bool disposeManagedResources)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3368);
      if (!this._disposed) {
        IACSharpSensor.IACSharpSensor.SensorReached(3369);
        if (disposeManagedResources) {
          IACSharpSensor.IACSharpSensor.SensorReached(3370);
          if (_ReadStreamIsOurs) {
            IACSharpSensor.IACSharpSensor.SensorReached(3371);
            if (_readstream != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(3372);
              _readstream.Dispose();
              _readstream = null;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3373);
          if ((_temporaryFileName != null) && (_name != null)) {
            IACSharpSensor.IACSharpSensor.SensorReached(3374);
            if (_writestream != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(3375);
              _writestream.Dispose();
              _writestream = null;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3376);
          if (this.ParallelDeflater != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3377);
            this.ParallelDeflater.Dispose();
            this.ParallelDeflater = null;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3378);
        this._disposed = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3379);
    }
    internal Stream ReadStream {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(3380);
        if (_readstream == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3381);
          if (_readName != null || _name != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3382);
            _readstream = File.Open(_readName ?? _name, FileMode.Open, FileAccess.Read, FileShare.Read | FileShare.Write);
            _ReadStreamIsOurs = true;
          }
        }
        Stream RNTRNTRNT_365 = _readstream;
        IACSharpSensor.IACSharpSensor.SensorReached(3383);
        return RNTRNTRNT_365;
      }
    }
    private Stream WriteStream {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(3384);
        if (_writestream != null) {
          Stream RNTRNTRNT_366 = _writestream;
          IACSharpSensor.IACSharpSensor.SensorReached(3385);
          return RNTRNTRNT_366;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3386);
        if (_name == null) {
          Stream RNTRNTRNT_367 = _writestream;
          IACSharpSensor.IACSharpSensor.SensorReached(3387);
          return RNTRNTRNT_367;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3388);
        if (_maxOutputSegmentSize != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3389);
          _writestream = ZipSegmentedStream.ForWriting(this._name, _maxOutputSegmentSize);
          Stream RNTRNTRNT_368 = _writestream;
          IACSharpSensor.IACSharpSensor.SensorReached(3390);
          return RNTRNTRNT_368;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3391);
        SharedUtilities.CreateAndOpenUniqueTempFile(TempFileFolder ?? Path.GetDirectoryName(_name), out _writestream, out _temporaryFileName);
        Stream RNTRNTRNT_369 = _writestream;
        IACSharpSensor.IACSharpSensor.SensorReached(3392);
        return RNTRNTRNT_369;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(3393);
        if (value != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3394);
          throw new ZipException("Cannot set the stream to a non-null value.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3395);
        _writestream = null;
        IACSharpSensor.IACSharpSensor.SensorReached(3396);
      }
    }
    private TextWriter _StatusMessageTextWriter;
    private bool _CaseSensitiveRetrieval;
    private Stream _readstream;
    private Stream _writestream;
    private UInt16 _versionMadeBy;
    private UInt16 _versionNeededToExtract;
    private UInt32 _diskNumberWithCd;
    private Int32 _maxOutputSegmentSize;
    private UInt32 _numberOfSegmentsForMostRecentSave;
    private ZipErrorAction _zipErrorAction;
    private bool _disposed;
    private System.Collections.Generic.Dictionary<String, ZipEntry> _entries;
    private List<ZipEntry> _zipEntriesAsList;
    private string _name;
    private string _readName;
    private string _Comment;
    internal string _Password;
    private bool _emitNtfsTimes = true;
    private bool _emitUnixTimes;
    private Ionic.Zlib.CompressionStrategy _Strategy = Ionic.Zlib.CompressionStrategy.Default;
    private Ionic.Zip.CompressionMethod _compressionMethod = Ionic.Zip.CompressionMethod.Deflate;
    private bool _fileAlreadyExists;
    private string _temporaryFileName;
    private bool _contentsChanged;
    private bool _hasBeenSaved;
    private String _TempFileFolder;
    private bool _ReadStreamIsOurs = true;
    private object LOCK = new object();
    private bool _saveOperationCanceled;
    private bool _extractOperationCanceled;
    private bool _addOperationCanceled;
    private EncryptionAlgorithm _Encryption;
    private bool _JustSaved;
    private long _locEndOfCDS = -1;
    private uint _OffsetOfCentralDirectory;
    private Int64 _OffsetOfCentralDirectory64;
    private Nullable<bool> _OutputUsesZip64;
    internal bool _inExtractAll;
    private System.Text.Encoding _alternateEncoding = System.Text.Encoding.GetEncoding("IBM437");
    private ZipOption _alternateEncodingUsage = ZipOption.Never;
    private static System.Text.Encoding _defaultEncoding = System.Text.Encoding.GetEncoding("IBM437");
    private int _BufferSize = BufferSizeDefault;
    internal Ionic.Zlib.ParallelDeflateOutputStream ParallelDeflater;
    private long _ParallelDeflateThreshold;
    private int _maxBufferPairs = 16;
    internal Zip64Option _zip64 = Zip64Option.Default;
    private bool _SavingSfx;
    public static readonly int BufferSizeDefault = 32768;
  }
  public enum Zip64Option
  {
    Default = 0,
    Never = 0,
    AsNecessary = 1,
    Always
  }
  public enum ZipOption
  {
    Default = 0,
    Never = 0,
    AsNecessary = 1,
    Always
  }
  enum AddOrUpdateAction
  {
    AddOnly = 0,
    AddOrUpdate
  }
}
