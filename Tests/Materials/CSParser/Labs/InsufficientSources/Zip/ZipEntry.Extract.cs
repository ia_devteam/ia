using System;
using System.IO;
namespace Ionic.Zip
{
  public partial class ZipEntry
  {
    public void Extract()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2122);
      InternalExtract(".", null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2123);
    }
    public void Extract(ExtractExistingFileAction extractExistingFile)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2124);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(".", null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2125);
    }
    public void Extract(Stream stream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2126);
      InternalExtract(null, stream, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2127);
    }
    public void Extract(string baseDirectory)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2128);
      InternalExtract(baseDirectory, null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2129);
    }
    public void Extract(string baseDirectory, ExtractExistingFileAction extractExistingFile)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2130);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(baseDirectory, null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2131);
    }
    public void ExtractWithPassword(string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2132);
      InternalExtract(".", null, password);
      IACSharpSensor.IACSharpSensor.SensorReached(2133);
    }
    public void ExtractWithPassword(string baseDirectory, string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2134);
      InternalExtract(baseDirectory, null, password);
      IACSharpSensor.IACSharpSensor.SensorReached(2135);
    }
    public void ExtractWithPassword(ExtractExistingFileAction extractExistingFile, string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2136);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(".", null, password);
      IACSharpSensor.IACSharpSensor.SensorReached(2137);
    }
    public void ExtractWithPassword(string baseDirectory, ExtractExistingFileAction extractExistingFile, string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2138);
      ExtractExistingFile = extractExistingFile;
      InternalExtract(baseDirectory, null, password);
      IACSharpSensor.IACSharpSensor.SensorReached(2139);
    }
    public void ExtractWithPassword(Stream stream, string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2140);
      InternalExtract(null, stream, password);
      IACSharpSensor.IACSharpSensor.SensorReached(2141);
    }
    public Ionic.Crc.CrcCalculatorStream OpenReader()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2142);
      if (_container.ZipFile == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2143);
        throw new InvalidOperationException("Use OpenReader() only with ZipFile.");
      }
      Ionic.Crc.CrcCalculatorStream RNTRNTRNT_235 = InternalOpenReader(this._Password ?? this._container.Password);
      IACSharpSensor.IACSharpSensor.SensorReached(2144);
      return RNTRNTRNT_235;
    }
    public Ionic.Crc.CrcCalculatorStream OpenReader(string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2145);
      if (_container.ZipFile == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2146);
        throw new InvalidOperationException("Use OpenReader() only with ZipFile.");
      }
      Ionic.Crc.CrcCalculatorStream RNTRNTRNT_236 = InternalOpenReader(password);
      IACSharpSensor.IACSharpSensor.SensorReached(2147);
      return RNTRNTRNT_236;
    }
    internal Ionic.Crc.CrcCalculatorStream InternalOpenReader(string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2148);
      ValidateCompression();
      ValidateEncryption();
      SetupCryptoForExtract(password);
      IACSharpSensor.IACSharpSensor.SensorReached(2149);
      if (this._Source != ZipEntrySource.ZipFile) {
        IACSharpSensor.IACSharpSensor.SensorReached(2150);
        throw new BadStateException("You must call ZipFile.Save before calling OpenReader");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2151);
      Int64 LeftToRead = (_CompressionMethod_FromZipFile == (short)CompressionMethod.None) ? this._CompressedFileDataSize : this.UncompressedSize;
      Stream input = this.ArchiveStream;
      this.ArchiveStream.Seek(this.FileDataPosition, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      _inputDecryptorStream = GetExtractDecryptor(input);
      Stream input3 = GetExtractDecompressor(_inputDecryptorStream);
      Ionic.Crc.CrcCalculatorStream RNTRNTRNT_237 = new Ionic.Crc.CrcCalculatorStream(input3, LeftToRead);
      IACSharpSensor.IACSharpSensor.SensorReached(2152);
      return RNTRNTRNT_237;
    }
    private void OnExtractProgress(Int64 bytesWritten, Int64 totalBytesToWrite)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2153);
      if (_container.ZipFile != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2154);
        _ioOperationCanceled = _container.ZipFile.OnExtractBlock(this, bytesWritten, totalBytesToWrite);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2155);
    }
    private void OnBeforeExtract(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2156);
      if (_container.ZipFile != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2157);
        if (!_container.ZipFile._inExtractAll) {
          IACSharpSensor.IACSharpSensor.SensorReached(2158);
          _ioOperationCanceled = _container.ZipFile.OnSingleEntryExtract(this, path, true);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2159);
    }
    private void OnAfterExtract(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2160);
      if (_container.ZipFile != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2161);
        if (!_container.ZipFile._inExtractAll) {
          IACSharpSensor.IACSharpSensor.SensorReached(2162);
          _container.ZipFile.OnSingleEntryExtract(this, path, false);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2163);
    }
    private void OnExtractExisting(string path)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2164);
      if (_container.ZipFile != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2165);
        _ioOperationCanceled = _container.ZipFile.OnExtractExisting(this, path);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2166);
    }
    private static void ReallyDelete(string fileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2167);
      if ((File.GetAttributes(fileName) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
        IACSharpSensor.IACSharpSensor.SensorReached(2168);
        File.SetAttributes(fileName, FileAttributes.Normal);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2169);
      File.Delete(fileName);
      IACSharpSensor.IACSharpSensor.SensorReached(2170);
    }
    private void WriteStatus(string format, params Object[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2171);
      if (_container.ZipFile != null && _container.ZipFile.Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(2172);
        _container.ZipFile.StatusMessageTextWriter.WriteLine(format, args);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2173);
    }
    private void InternalExtract(string baseDir, Stream outstream, string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2174);
      if (_container == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2175);
        throw new BadStateException("This entry is an orphan");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2176);
      if (_container.ZipFile == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2177);
        throw new InvalidOperationException("Use Extract() only with ZipFile.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2178);
      _container.ZipFile.Reset(false);
      IACSharpSensor.IACSharpSensor.SensorReached(2179);
      if (this._Source != ZipEntrySource.ZipFile) {
        IACSharpSensor.IACSharpSensor.SensorReached(2180);
        throw new BadStateException("You must call ZipFile.Save before calling any Extract method");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2181);
      OnBeforeExtract(baseDir);
      _ioOperationCanceled = false;
      string targetFileName = null;
      Stream output = null;
      bool fileExistsBeforeExtraction = false;
      bool checkLaterForResetDirTimes = false;
      IACSharpSensor.IACSharpSensor.SensorReached(2182);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2183);
        ValidateCompression();
        ValidateEncryption();
        IACSharpSensor.IACSharpSensor.SensorReached(2184);
        if (ValidateOutput(baseDir, outstream, out targetFileName)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2185);
          WriteStatus("extract dir {0}...", targetFileName);
          OnAfterExtract(baseDir);
          IACSharpSensor.IACSharpSensor.SensorReached(2186);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2187);
        if (targetFileName != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2188);
          if (File.Exists(targetFileName)) {
            IACSharpSensor.IACSharpSensor.SensorReached(2189);
            fileExistsBeforeExtraction = true;
            int rc = CheckExtractExistingFile(baseDir, targetFileName);
            IACSharpSensor.IACSharpSensor.SensorReached(2190);
            if (rc == 2) {
              IACSharpSensor.IACSharpSensor.SensorReached(2191);
              goto ExitTry;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2192);
            if (rc == 1) {
              IACSharpSensor.IACSharpSensor.SensorReached(2193);
              return;
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2194);
        string p = password ?? this._Password ?? this._container.Password;
        IACSharpSensor.IACSharpSensor.SensorReached(2195);
        if (_Encryption_FromZipFile != EncryptionAlgorithm.None) {
          IACSharpSensor.IACSharpSensor.SensorReached(2196);
          if (p == null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2197);
            throw new BadPasswordException();
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2198);
          SetupCryptoForExtract(p);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2199);
        if (targetFileName != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2200);
          WriteStatus("extract file {0}...", targetFileName);
          targetFileName += ".tmp";
          var dirName = Path.GetDirectoryName(targetFileName);
          IACSharpSensor.IACSharpSensor.SensorReached(2201);
          if (!Directory.Exists(dirName)) {
            IACSharpSensor.IACSharpSensor.SensorReached(2202);
            Directory.CreateDirectory(dirName);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2203);
            if (_container.ZipFile != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(2204);
              checkLaterForResetDirTimes = _container.ZipFile._inExtractAll;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2205);
          output = new FileStream(targetFileName, FileMode.CreateNew);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2206);
          WriteStatus("extract entry {0} to stream...", FileName);
          output = outstream;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2207);
        if (_ioOperationCanceled) {
          IACSharpSensor.IACSharpSensor.SensorReached(2208);
          goto ExitTry;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2209);
        Int32 ActualCrc32 = ExtractOne(output);
        IACSharpSensor.IACSharpSensor.SensorReached(2210);
        if (_ioOperationCanceled) {
          IACSharpSensor.IACSharpSensor.SensorReached(2211);
          goto ExitTry;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2212);
        VerifyCrcAfterExtract(ActualCrc32);
        IACSharpSensor.IACSharpSensor.SensorReached(2213);
        if (targetFileName != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2214);
          output.Close();
          output = null;
          string tmpName = targetFileName;
          string zombie = null;
          targetFileName = tmpName.Substring(0, tmpName.Length - 4);
          IACSharpSensor.IACSharpSensor.SensorReached(2215);
          if (fileExistsBeforeExtraction) {
            IACSharpSensor.IACSharpSensor.SensorReached(2216);
            zombie = targetFileName + ".PendingOverwrite";
            File.Move(targetFileName, zombie);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2217);
          File.Move(tmpName, targetFileName);
          _SetTimes(targetFileName, true);
          IACSharpSensor.IACSharpSensor.SensorReached(2218);
          if (zombie != null && File.Exists(zombie)) {
            IACSharpSensor.IACSharpSensor.SensorReached(2219);
            ReallyDelete(zombie);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2220);
          if (checkLaterForResetDirTimes) {
            IACSharpSensor.IACSharpSensor.SensorReached(2221);
            if (this.FileName.IndexOf('/') != -1) {
              IACSharpSensor.IACSharpSensor.SensorReached(2222);
              string dirname = Path.GetDirectoryName(this.FileName);
              IACSharpSensor.IACSharpSensor.SensorReached(2223);
              if (this._container.ZipFile[dirname] == null) {
                IACSharpSensor.IACSharpSensor.SensorReached(2224);
                _SetTimes(Path.GetDirectoryName(targetFileName), false);
              }
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2225);
          if ((_VersionMadeBy & 0xff00) == 0xa00 || (_VersionMadeBy & 0xff00) == 0x0) {
            IACSharpSensor.IACSharpSensor.SensorReached(2226);
            File.SetAttributes(targetFileName, (FileAttributes)_ExternalFileAttrs);
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2227);
        OnAfterExtract(baseDir);
        ExitTry:
        ;
      } catch (Exception) {
        IACSharpSensor.IACSharpSensor.SensorReached(2228);
        _ioOperationCanceled = true;
        IACSharpSensor.IACSharpSensor.SensorReached(2229);
        throw;
        IACSharpSensor.IACSharpSensor.SensorReached(2230);
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(2231);
        if (_ioOperationCanceled) {
          IACSharpSensor.IACSharpSensor.SensorReached(2232);
          if (targetFileName != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2233);
            try {
              IACSharpSensor.IACSharpSensor.SensorReached(2234);
              if (output != null) {
                IACSharpSensor.IACSharpSensor.SensorReached(2235);
                output.Close();
              }
              IACSharpSensor.IACSharpSensor.SensorReached(2236);
              if (File.Exists(targetFileName) && !fileExistsBeforeExtraction) {
                IACSharpSensor.IACSharpSensor.SensorReached(2237);
                File.Delete(targetFileName);
              }
            } finally {
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2238);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2239);
    }
    internal void VerifyCrcAfterExtract(Int32 actualCrc32)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2240);
      if (actualCrc32 != _Crc32) {
        IACSharpSensor.IACSharpSensor.SensorReached(2241);
        throw new BadCrcException("CRC error: the file being extracted appears to be corrupted. " + String.Format("Expected 0x{0:X8}, Actual 0x{1:X8}", _Crc32, actualCrc32));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2242);
    }
    private int CheckExtractExistingFile(string baseDir, string targetFileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2243);
      int loop = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(2244);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(2245);
        switch (ExtractExistingFile) {
          case ExtractExistingFileAction.OverwriteSilently:
            IACSharpSensor.IACSharpSensor.SensorReached(2246);
            WriteStatus("the file {0} exists; will overwrite it...", targetFileName);
            System.Int32 RNTRNTRNT_238 = 0;
            IACSharpSensor.IACSharpSensor.SensorReached(2247);
            return RNTRNTRNT_238;
          case ExtractExistingFileAction.DoNotOverwrite:
            IACSharpSensor.IACSharpSensor.SensorReached(2248);
            WriteStatus("the file {0} exists; not extracting entry...", FileName);
            OnAfterExtract(baseDir);
            System.Int32 RNTRNTRNT_239 = 1;
            IACSharpSensor.IACSharpSensor.SensorReached(2249);
            return RNTRNTRNT_239;
          case ExtractExistingFileAction.InvokeExtractProgressEvent:
            IACSharpSensor.IACSharpSensor.SensorReached(2250);
            if (loop > 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(2251);
              throw new ZipException(String.Format("The file {0} already exists.", targetFileName));
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2252);
            OnExtractExisting(baseDir);
            IACSharpSensor.IACSharpSensor.SensorReached(2253);
            if (_ioOperationCanceled) {
              System.Int32 RNTRNTRNT_240 = 2;
              IACSharpSensor.IACSharpSensor.SensorReached(2254);
              return RNTRNTRNT_240;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2255);
            break;
          case ExtractExistingFileAction.Throw:
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(2256);
            throw new ZipException(String.Format("The file {0} already exists.", targetFileName));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2257);
        loop++;
      } while (true);
      IACSharpSensor.IACSharpSensor.SensorReached(2258);
    }
    private void _CheckRead(int nbytes)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2259);
      if (nbytes == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2260);
        throw new BadReadException(String.Format("bad read of entry {0} from compressed archive.", this.FileName));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2261);
    }
    private Stream _inputDecryptorStream;
    private Int32 ExtractOne(Stream output)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2262);
      Int32 CrcResult = 0;
      Stream input = this.ArchiveStream;
      IACSharpSensor.IACSharpSensor.SensorReached(2263);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2264);
        input.Seek(this.FileDataPosition, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(input);
        byte[] bytes = new byte[BufferSize];
        Int64 LeftToRead = (_CompressionMethod_FromZipFile != (short)CompressionMethod.None) ? this.UncompressedSize : this._CompressedFileDataSize;
        _inputDecryptorStream = GetExtractDecryptor(input);
        Stream input3 = GetExtractDecompressor(_inputDecryptorStream);
        Int64 bytesWritten = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(2265);
        using (var s1 = new Ionic.Crc.CrcCalculatorStream(input3)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2266);
          while (LeftToRead > 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(2267);
            int len = (LeftToRead > bytes.Length) ? bytes.Length : (int)LeftToRead;
            int n = s1.Read(bytes, 0, len);
            _CheckRead(n);
            output.Write(bytes, 0, n);
            LeftToRead -= n;
            bytesWritten += n;
            OnExtractProgress(bytesWritten, UncompressedSize);
            IACSharpSensor.IACSharpSensor.SensorReached(2268);
            if (_ioOperationCanceled) {
              IACSharpSensor.IACSharpSensor.SensorReached(2269);
              break;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2270);
          CrcResult = s1.Crc;
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(2271);
        var zss = input as ZipSegmentedStream;
        IACSharpSensor.IACSharpSensor.SensorReached(2272);
        if (zss != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2273);
          zss.Dispose();
          _archiveStream = null;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2274);
      }
      Int32 RNTRNTRNT_241 = CrcResult;
      IACSharpSensor.IACSharpSensor.SensorReached(2275);
      return RNTRNTRNT_241;
    }
    internal Stream GetExtractDecompressor(Stream input2)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2276);
      switch (_CompressionMethod_FromZipFile) {
        case (short)CompressionMethod.None:
          Stream RNTRNTRNT_242 = input2;
          IACSharpSensor.IACSharpSensor.SensorReached(2277);
          return RNTRNTRNT_242;
        case (short)CompressionMethod.Deflate:
          Stream RNTRNTRNT_243 = new Ionic.Zlib.DeflateStream(input2, Ionic.Zlib.CompressionMode.Decompress, true);
          IACSharpSensor.IACSharpSensor.SensorReached(2278);
          return RNTRNTRNT_243;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2279);
      return null;
    }
    internal Stream GetExtractDecryptor(Stream input)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2280);
      Stream input2 = null;
      IACSharpSensor.IACSharpSensor.SensorReached(2281);
      if (_Encryption_FromZipFile == EncryptionAlgorithm.PkzipWeak) {
        IACSharpSensor.IACSharpSensor.SensorReached(2282);
        input2 = new ZipCipherStream(input, _zipCrypto_forExtract, CryptoMode.Decrypt);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2283);
        input2 = input;
      }
      Stream RNTRNTRNT_244 = input2;
      IACSharpSensor.IACSharpSensor.SensorReached(2284);
      return RNTRNTRNT_244;
    }
    internal void _SetTimes(string fileOrDirectory, bool isFile)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2285);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2286);
        if (_ntfsTimesAreSet) {
          IACSharpSensor.IACSharpSensor.SensorReached(2287);
          if (isFile) {
            IACSharpSensor.IACSharpSensor.SensorReached(2288);
            if (File.Exists(fileOrDirectory)) {
              IACSharpSensor.IACSharpSensor.SensorReached(2289);
              File.SetCreationTimeUtc(fileOrDirectory, _Ctime);
              File.SetLastAccessTimeUtc(fileOrDirectory, _Atime);
              File.SetLastWriteTimeUtc(fileOrDirectory, _Mtime);
            }
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2290);
            if (Directory.Exists(fileOrDirectory)) {
              IACSharpSensor.IACSharpSensor.SensorReached(2291);
              Directory.SetCreationTimeUtc(fileOrDirectory, _Ctime);
              Directory.SetLastAccessTimeUtc(fileOrDirectory, _Atime);
              Directory.SetLastWriteTimeUtc(fileOrDirectory, _Mtime);
            }
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2292);
          DateTime AdjustedLastModified = Ionic.Zip.SharedUtilities.AdjustTime_Reverse(LastModified);
          IACSharpSensor.IACSharpSensor.SensorReached(2293);
          if (isFile) {
            IACSharpSensor.IACSharpSensor.SensorReached(2294);
            File.SetLastWriteTime(fileOrDirectory, AdjustedLastModified);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2295);
            Directory.SetLastWriteTime(fileOrDirectory, AdjustedLastModified);
          }
        }
      } catch (System.IO.IOException ioexc1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2296);
        WriteStatus("failed to set time on {0}: {1}", fileOrDirectory, ioexc1.Message);
        IACSharpSensor.IACSharpSensor.SensorReached(2297);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2298);
    }
    private string UnsupportedAlgorithm {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2299);
        string alg = String.Empty;
        IACSharpSensor.IACSharpSensor.SensorReached(2300);
        switch (_UnsupportedAlgorithmId) {
          case 0:
            IACSharpSensor.IACSharpSensor.SensorReached(2301);
            alg = "--";
            IACSharpSensor.IACSharpSensor.SensorReached(2302);
            break;
          case 0x6601:
            IACSharpSensor.IACSharpSensor.SensorReached(2303);
            alg = "DES";
            IACSharpSensor.IACSharpSensor.SensorReached(2304);
            break;
          case 0x6602:
            IACSharpSensor.IACSharpSensor.SensorReached(2305);
            alg = "RC2";
            IACSharpSensor.IACSharpSensor.SensorReached(2306);
            break;
          case 0x6603:
            IACSharpSensor.IACSharpSensor.SensorReached(2307);
            alg = "3DES-168";
            IACSharpSensor.IACSharpSensor.SensorReached(2308);
            break;
          case 0x6609:
            IACSharpSensor.IACSharpSensor.SensorReached(2309);
            alg = "3DES-112";
            IACSharpSensor.IACSharpSensor.SensorReached(2310);
            break;
          case 0x660e:
            IACSharpSensor.IACSharpSensor.SensorReached(2311);
            alg = "PKWare AES128";
            IACSharpSensor.IACSharpSensor.SensorReached(2312);
            break;
          case 0x660f:
            IACSharpSensor.IACSharpSensor.SensorReached(2313);
            alg = "PKWare AES192";
            IACSharpSensor.IACSharpSensor.SensorReached(2314);
            break;
          case 0x6610:
            IACSharpSensor.IACSharpSensor.SensorReached(2315);
            alg = "PKWare AES256";
            IACSharpSensor.IACSharpSensor.SensorReached(2316);
            break;
          case 0x6702:
            IACSharpSensor.IACSharpSensor.SensorReached(2317);
            alg = "RC2";
            IACSharpSensor.IACSharpSensor.SensorReached(2318);
            break;
          case 0x6720:
            IACSharpSensor.IACSharpSensor.SensorReached(2319);
            alg = "Blowfish";
            IACSharpSensor.IACSharpSensor.SensorReached(2320);
            break;
          case 0x6721:
            IACSharpSensor.IACSharpSensor.SensorReached(2321);
            alg = "Twofish";
            IACSharpSensor.IACSharpSensor.SensorReached(2322);
            break;
          case 0x6801:
            IACSharpSensor.IACSharpSensor.SensorReached(2323);
            alg = "RC4";
            IACSharpSensor.IACSharpSensor.SensorReached(2324);
            break;
          case 0xffff:
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(2325);
            alg = String.Format("Unknown (0x{0:X4})", _UnsupportedAlgorithmId);
            IACSharpSensor.IACSharpSensor.SensorReached(2326);
            break;
        }
        System.String RNTRNTRNT_245 = alg;
        IACSharpSensor.IACSharpSensor.SensorReached(2327);
        return RNTRNTRNT_245;
      }
    }
    private string UnsupportedCompressionMethod {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(2328);
        string meth = String.Empty;
        IACSharpSensor.IACSharpSensor.SensorReached(2329);
        switch ((int)_CompressionMethod) {
          case 0:
            IACSharpSensor.IACSharpSensor.SensorReached(2330);
            meth = "Store";
            IACSharpSensor.IACSharpSensor.SensorReached(2331);
            break;
          case 1:
            IACSharpSensor.IACSharpSensor.SensorReached(2332);
            meth = "Shrink";
            IACSharpSensor.IACSharpSensor.SensorReached(2333);
            break;
          case 8:
            IACSharpSensor.IACSharpSensor.SensorReached(2334);
            meth = "DEFLATE";
            IACSharpSensor.IACSharpSensor.SensorReached(2335);
            break;
          case 9:
            IACSharpSensor.IACSharpSensor.SensorReached(2336);
            meth = "Deflate64";
            IACSharpSensor.IACSharpSensor.SensorReached(2337);
            break;
          case 12:
            IACSharpSensor.IACSharpSensor.SensorReached(2338);
            meth = "BZIP2";
            IACSharpSensor.IACSharpSensor.SensorReached(2339);
            break;
          case 14:
            IACSharpSensor.IACSharpSensor.SensorReached(2340);
            meth = "LZMA";
            IACSharpSensor.IACSharpSensor.SensorReached(2341);
            break;
          case 19:
            IACSharpSensor.IACSharpSensor.SensorReached(2342);
            meth = "LZ77";
            IACSharpSensor.IACSharpSensor.SensorReached(2343);
            break;
          case 98:
            IACSharpSensor.IACSharpSensor.SensorReached(2344);
            meth = "PPMd";
            IACSharpSensor.IACSharpSensor.SensorReached(2345);
            break;
          default:
            IACSharpSensor.IACSharpSensor.SensorReached(2346);
            meth = String.Format("Unknown (0x{0:X4})", _CompressionMethod);
            IACSharpSensor.IACSharpSensor.SensorReached(2347);
            break;
        }
        System.String RNTRNTRNT_246 = meth;
        IACSharpSensor.IACSharpSensor.SensorReached(2348);
        return RNTRNTRNT_246;
      }
    }
    internal void ValidateEncryption()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2349);
      if (Encryption != EncryptionAlgorithm.PkzipWeak && Encryption != EncryptionAlgorithm.None) {
        IACSharpSensor.IACSharpSensor.SensorReached(2350);
        if (_UnsupportedAlgorithmId != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2351);
          throw new ZipException(String.Format("Cannot extract: Entry {0} is encrypted with an algorithm not supported by DotNetZip: {1}", FileName, UnsupportedAlgorithm));
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2352);
          throw new ZipException(String.Format("Cannot extract: Entry {0} uses an unsupported encryption algorithm ({1:X2})", FileName, (int)Encryption));
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2353);
    }
    private void ValidateCompression()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2354);
      if ((_CompressionMethod_FromZipFile != (short)CompressionMethod.None) && (_CompressionMethod_FromZipFile != (short)CompressionMethod.Deflate)) {
        IACSharpSensor.IACSharpSensor.SensorReached(2355);
        throw new ZipException(String.Format("Entry {0} uses an unsupported compression method (0x{1:X2}, {2})", FileName, _CompressionMethod_FromZipFile, UnsupportedCompressionMethod));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2356);
    }
    private void SetupCryptoForExtract(string password)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2357);
      if (_Encryption_FromZipFile == EncryptionAlgorithm.None) {
        IACSharpSensor.IACSharpSensor.SensorReached(2358);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2359);
      if (_Encryption_FromZipFile == EncryptionAlgorithm.PkzipWeak) {
        IACSharpSensor.IACSharpSensor.SensorReached(2360);
        if (password == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2361);
          throw new ZipException("Missing password.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2362);
        this.ArchiveStream.Seek(this.FileDataPosition - 12, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
        _zipCrypto_forExtract = ZipCrypto.ForRead(password, this);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2363);
    }
    private bool ValidateOutput(string basedir, Stream outstream, out string outFileName)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2364);
      if (basedir != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2365);
        string f = this.FileName.Replace("\\", "/");
        IACSharpSensor.IACSharpSensor.SensorReached(2366);
        if (f.IndexOf(':') == 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(2367);
          f = f.Substring(2);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2368);
        if (f.StartsWith("/")) {
          IACSharpSensor.IACSharpSensor.SensorReached(2369);
          f = f.Substring(1);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2370);
        if (_container.ZipFile.FlattenFoldersOnExtract) {
          IACSharpSensor.IACSharpSensor.SensorReached(2371);
          outFileName = Path.Combine(basedir, (f.IndexOf('/') != -1) ? Path.GetFileName(f) : f);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2372);
          outFileName = Path.Combine(basedir, f);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2373);
        outFileName = outFileName.Replace("/", "\\");
        IACSharpSensor.IACSharpSensor.SensorReached(2374);
        if ((IsDirectory) || (FileName.EndsWith("/"))) {
          IACSharpSensor.IACSharpSensor.SensorReached(2375);
          if (!Directory.Exists(outFileName)) {
            IACSharpSensor.IACSharpSensor.SensorReached(2376);
            Directory.CreateDirectory(outFileName);
            _SetTimes(outFileName, false);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2377);
            if (ExtractExistingFile == ExtractExistingFileAction.OverwriteSilently) {
              IACSharpSensor.IACSharpSensor.SensorReached(2378);
              _SetTimes(outFileName, false);
            }
          }
          System.Boolean RNTRNTRNT_247 = true;
          IACSharpSensor.IACSharpSensor.SensorReached(2379);
          return RNTRNTRNT_247;
        }
        System.Boolean RNTRNTRNT_248 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2380);
        return RNTRNTRNT_248;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2381);
      if (outstream != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2382);
        outFileName = null;
        IACSharpSensor.IACSharpSensor.SensorReached(2383);
        if ((IsDirectory) || (FileName.EndsWith("/"))) {
          System.Boolean RNTRNTRNT_249 = true;
          IACSharpSensor.IACSharpSensor.SensorReached(2384);
          return RNTRNTRNT_249;
        }
        System.Boolean RNTRNTRNT_250 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2385);
        return RNTRNTRNT_250;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2386);
      throw new ArgumentNullException("outstream");
    }
  }
}
