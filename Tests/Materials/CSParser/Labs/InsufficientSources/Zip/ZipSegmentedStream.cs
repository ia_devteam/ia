using System;
using System.Collections.Generic;
using System.IO;
namespace Ionic.Zip
{
  internal class ZipSegmentedStream : System.IO.Stream
  {
    enum RwMode
    {
      None = 0,
      ReadOnly = 1,
      Write = 2
    }
    private RwMode rwMode;
    private bool _exceptionPending;
    private string _baseName;
    private string _baseDir;
    private string _currentName;
    private string _currentTempName;
    private uint _currentDiskNumber;
    private uint _maxDiskNumber;
    private int _maxSegmentSize;
    private System.IO.Stream _innerStream;
    private ZipSegmentedStream() : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4378);
      _exceptionPending = false;
      IACSharpSensor.IACSharpSensor.SensorReached(4379);
    }
    public static ZipSegmentedStream ForReading(string name, uint initialDiskNumber, uint maxDiskNumber)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4380);
      ZipSegmentedStream zss = new ZipSegmentedStream {
        rwMode = RwMode.ReadOnly,
        CurrentSegment = initialDiskNumber,
        _maxDiskNumber = maxDiskNumber,
        _baseName = name
      };
      zss._SetReadStream();
      ZipSegmentedStream RNTRNTRNT_486 = zss;
      IACSharpSensor.IACSharpSensor.SensorReached(4381);
      return RNTRNTRNT_486;
    }
    public static ZipSegmentedStream ForWriting(string name, int maxSegmentSize)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4382);
      ZipSegmentedStream zss = new ZipSegmentedStream {
        rwMode = RwMode.Write,
        CurrentSegment = 0,
        _baseName = name,
        _maxSegmentSize = maxSegmentSize,
        _baseDir = Path.GetDirectoryName(name)
      };
      IACSharpSensor.IACSharpSensor.SensorReached(4383);
      if (zss._baseDir == "") {
        IACSharpSensor.IACSharpSensor.SensorReached(4384);
        zss._baseDir = ".";
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4385);
      zss._SetWriteStream(0);
      ZipSegmentedStream RNTRNTRNT_487 = zss;
      IACSharpSensor.IACSharpSensor.SensorReached(4386);
      return RNTRNTRNT_487;
    }
    public static Stream ForUpdate(string name, uint diskNumber)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4387);
      if (diskNumber >= 99) {
        IACSharpSensor.IACSharpSensor.SensorReached(4388);
        throw new ArgumentOutOfRangeException("diskNumber");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4389);
      string fname = String.Format("{0}.z{1:D2}", Path.Combine(Path.GetDirectoryName(name), Path.GetFileNameWithoutExtension(name)), diskNumber + 1);
      Stream RNTRNTRNT_488 = File.Open(fname, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
      IACSharpSensor.IACSharpSensor.SensorReached(4390);
      return RNTRNTRNT_488;
    }
    public bool ContiguousWrite { get; set; }
    public UInt32 CurrentSegment {
      get {
        UInt32 RNTRNTRNT_489 = _currentDiskNumber;
        IACSharpSensor.IACSharpSensor.SensorReached(4391);
        return RNTRNTRNT_489;
      }
      private set {
        IACSharpSensor.IACSharpSensor.SensorReached(4392);
        _currentDiskNumber = value;
        _currentName = null;
        IACSharpSensor.IACSharpSensor.SensorReached(4393);
      }
    }
    public String CurrentName {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(4394);
        if (_currentName == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4395);
          _currentName = _NameForSegment(CurrentSegment);
        }
        String RNTRNTRNT_490 = _currentName;
        IACSharpSensor.IACSharpSensor.SensorReached(4396);
        return RNTRNTRNT_490;
      }
    }
    public String CurrentTempName {
      get {
        String RNTRNTRNT_491 = _currentTempName;
        IACSharpSensor.IACSharpSensor.SensorReached(4397);
        return RNTRNTRNT_491;
      }
    }
    private string _NameForSegment(uint diskNumber)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4398);
      if (diskNumber >= 99) {
        IACSharpSensor.IACSharpSensor.SensorReached(4399);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4400);
        throw new OverflowException("The number of zip segments would exceed 99.");
      }
      System.String RNTRNTRNT_492 = String.Format("{0}.z{1:D2}", Path.Combine(Path.GetDirectoryName(_baseName), Path.GetFileNameWithoutExtension(_baseName)), diskNumber + 1);
      IACSharpSensor.IACSharpSensor.SensorReached(4401);
      return RNTRNTRNT_492;
    }
    public UInt32 ComputeSegment(int length)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4402);
      if (_innerStream.Position + length > _maxSegmentSize) {
        UInt32 RNTRNTRNT_493 = CurrentSegment + 1;
        IACSharpSensor.IACSharpSensor.SensorReached(4403);
        return RNTRNTRNT_493;
      }
      UInt32 RNTRNTRNT_494 = CurrentSegment;
      IACSharpSensor.IACSharpSensor.SensorReached(4404);
      return RNTRNTRNT_494;
    }
    public override String ToString()
    {
      String RNTRNTRNT_495 = String.Format("{0}[{1}][{2}], pos=0x{3:X})", "ZipSegmentedStream", CurrentName, rwMode.ToString(), this.Position);
      IACSharpSensor.IACSharpSensor.SensorReached(4405);
      return RNTRNTRNT_495;
    }
    private void _SetReadStream()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4406);
      if (_innerStream != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4407);
        _innerStream.Dispose();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4408);
      if (CurrentSegment + 1 == _maxDiskNumber) {
        IACSharpSensor.IACSharpSensor.SensorReached(4409);
        _currentName = _baseName;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4410);
      _innerStream = File.OpenRead(CurrentName);
      IACSharpSensor.IACSharpSensor.SensorReached(4411);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4412);
      if (rwMode != RwMode.ReadOnly) {
        IACSharpSensor.IACSharpSensor.SensorReached(4413);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4414);
        throw new InvalidOperationException("Stream Error: Cannot Read.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4415);
      int r = _innerStream.Read(buffer, offset, count);
      int r1 = r;
      IACSharpSensor.IACSharpSensor.SensorReached(4416);
      while (r1 != count) {
        IACSharpSensor.IACSharpSensor.SensorReached(4417);
        if (_innerStream.Position != _innerStream.Length) {
          IACSharpSensor.IACSharpSensor.SensorReached(4418);
          _exceptionPending = true;
          IACSharpSensor.IACSharpSensor.SensorReached(4419);
          throw new ZipException(String.Format("Read error in file {0}", CurrentName));
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4420);
        if (CurrentSegment + 1 == _maxDiskNumber) {
          System.Int32 RNTRNTRNT_496 = r;
          IACSharpSensor.IACSharpSensor.SensorReached(4421);
          return RNTRNTRNT_496;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4422);
        CurrentSegment++;
        _SetReadStream();
        offset += r1;
        count -= r1;
        r1 = _innerStream.Read(buffer, offset, count);
        r += r1;
      }
      System.Int32 RNTRNTRNT_497 = r;
      IACSharpSensor.IACSharpSensor.SensorReached(4423);
      return RNTRNTRNT_497;
    }
    private void _SetWriteStream(uint increment)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4424);
      if (_innerStream != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4425);
        _innerStream.Dispose();
        IACSharpSensor.IACSharpSensor.SensorReached(4426);
        if (File.Exists(CurrentName)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4427);
          File.Delete(CurrentName);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(4428);
        File.Move(_currentTempName, CurrentName);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4429);
      if (increment > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4430);
        CurrentSegment += increment;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4431);
      SharedUtilities.CreateAndOpenUniqueTempFile(_baseDir, out _innerStream, out _currentTempName);
      IACSharpSensor.IACSharpSensor.SensorReached(4432);
      if (CurrentSegment == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(4433);
        _innerStream.Write(BitConverter.GetBytes(ZipConstants.SplitArchiveSignature), 0, 4);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4434);
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4435);
      if (rwMode != RwMode.Write) {
        IACSharpSensor.IACSharpSensor.SensorReached(4436);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4437);
        throw new InvalidOperationException("Stream Error: Cannot Write.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4438);
      if (ContiguousWrite) {
        IACSharpSensor.IACSharpSensor.SensorReached(4439);
        if (_innerStream.Position + count > _maxSegmentSize) {
          IACSharpSensor.IACSharpSensor.SensorReached(4440);
          _SetWriteStream(1);
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(4441);
        while (_innerStream.Position + count > _maxSegmentSize) {
          IACSharpSensor.IACSharpSensor.SensorReached(4442);
          int c = unchecked(_maxSegmentSize - (int)_innerStream.Position);
          _innerStream.Write(buffer, offset, c);
          _SetWriteStream(1);
          count -= c;
          offset += c;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4443);
      _innerStream.Write(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(4444);
    }
    public long TruncateBackward(uint diskNumber, long offset)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4445);
      if (diskNumber >= 99) {
        IACSharpSensor.IACSharpSensor.SensorReached(4446);
        throw new ArgumentOutOfRangeException("diskNumber");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4447);
      if (rwMode != RwMode.Write) {
        IACSharpSensor.IACSharpSensor.SensorReached(4448);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4449);
        throw new ZipException("bad state.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4450);
      if (diskNumber == CurrentSegment) {
        IACSharpSensor.IACSharpSensor.SensorReached(4451);
        var x = _innerStream.Seek(offset, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_innerStream);
        System.Int64 RNTRNTRNT_498 = x;
        IACSharpSensor.IACSharpSensor.SensorReached(4452);
        return RNTRNTRNT_498;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4453);
      if (_innerStream != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(4454);
        _innerStream.Dispose();
        IACSharpSensor.IACSharpSensor.SensorReached(4455);
        if (File.Exists(_currentTempName)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4456);
          File.Delete(_currentTempName);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4457);
      for (uint j = CurrentSegment - 1; j > diskNumber; j--) {
        IACSharpSensor.IACSharpSensor.SensorReached(4458);
        string s = _NameForSegment(j);
        IACSharpSensor.IACSharpSensor.SensorReached(4459);
        if (File.Exists(s)) {
          IACSharpSensor.IACSharpSensor.SensorReached(4460);
          File.Delete(s);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4461);
      CurrentSegment = diskNumber;
      IACSharpSensor.IACSharpSensor.SensorReached(4462);
      for (int i = 0; i < 3; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(4463);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(4464);
          _currentTempName = SharedUtilities.InternalGetTempFileName();
          File.Move(CurrentName, _currentTempName);
          IACSharpSensor.IACSharpSensor.SensorReached(4465);
          break;
        } catch (IOException) {
          IACSharpSensor.IACSharpSensor.SensorReached(4466);
          if (i == 2) {
            IACSharpSensor.IACSharpSensor.SensorReached(4467);
            throw;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(4468);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4469);
      _innerStream = new FileStream(_currentTempName, FileMode.Open);
      var r = _innerStream.Seek(offset, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_innerStream);
      System.Int64 RNTRNTRNT_499 = r;
      IACSharpSensor.IACSharpSensor.SensorReached(4470);
      return RNTRNTRNT_499;
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_500 = (rwMode == RwMode.ReadOnly && (_innerStream != null) && _innerStream.CanRead);
        IACSharpSensor.IACSharpSensor.SensorReached(4471);
        return RNTRNTRNT_500;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_501 = (_innerStream != null) && _innerStream.CanSeek;
        IACSharpSensor.IACSharpSensor.SensorReached(4472);
        return RNTRNTRNT_501;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_502 = (rwMode == RwMode.Write) && (_innerStream != null) && _innerStream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(4473);
        return RNTRNTRNT_502;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4474);
      _innerStream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(4475);
    }
    public override long Length {
      get {
        System.Int64 RNTRNTRNT_503 = _innerStream.Length;
        IACSharpSensor.IACSharpSensor.SensorReached(4476);
        return RNTRNTRNT_503;
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_504 = _innerStream.Position;
        IACSharpSensor.IACSharpSensor.SensorReached(4477);
        return RNTRNTRNT_504;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(4478);
        _innerStream.Position = value;
        IACSharpSensor.IACSharpSensor.SensorReached(4479);
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4480);
      var x = _innerStream.Seek(offset, origin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(_innerStream);
      System.Int64 RNTRNTRNT_505 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(4481);
      return RNTRNTRNT_505;
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4482);
      if (rwMode != RwMode.Write) {
        IACSharpSensor.IACSharpSensor.SensorReached(4483);
        _exceptionPending = true;
        IACSharpSensor.IACSharpSensor.SensorReached(4484);
        throw new InvalidOperationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4485);
      _innerStream.SetLength(value);
      IACSharpSensor.IACSharpSensor.SensorReached(4486);
    }
    protected override void Dispose(bool disposing)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(4487);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(4488);
        if (_innerStream != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(4489);
          _innerStream.Dispose();
          IACSharpSensor.IACSharpSensor.SensorReached(4490);
          if (rwMode == RwMode.Write) {
            IACSharpSensor.IACSharpSensor.SensorReached(4491);
            if (_exceptionPending) {
            } else {
            }
          }
        }
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(4492);
        base.Dispose(disposing);
        IACSharpSensor.IACSharpSensor.SensorReached(4493);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(4494);
    }
  }
}
