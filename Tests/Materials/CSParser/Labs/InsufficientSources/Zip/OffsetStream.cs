using System;
using System.IO;
namespace Ionic.Zip
{
  internal class OffsetStream : System.IO.Stream, System.IDisposable
  {
    private Int64 _originalPosition;
    private Stream _innerStream;
    public OffsetStream(Stream s) : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1594);
      _originalPosition = s.Position;
      _innerStream = s;
      IACSharpSensor.IACSharpSensor.SensorReached(1595);
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      System.Int32 RNTRNTRNT_133 = _innerStream.Read(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(1596);
      return RNTRNTRNT_133;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1597);
      throw new NotImplementedException();
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_134 = _innerStream.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(1598);
        return RNTRNTRNT_134;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_135 = _innerStream.CanSeek;
        IACSharpSensor.IACSharpSensor.SensorReached(1599);
        return RNTRNTRNT_135;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_136 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1600);
        return RNTRNTRNT_136;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1601);
      _innerStream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(1602);
    }
    public override long Length {
      get {
        System.Int64 RNTRNTRNT_137 = _innerStream.Length;
        IACSharpSensor.IACSharpSensor.SensorReached(1603);
        return RNTRNTRNT_137;
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_138 = _innerStream.Position - _originalPosition;
        IACSharpSensor.IACSharpSensor.SensorReached(1604);
        return RNTRNTRNT_138;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1605);
        _innerStream.Position = _originalPosition + value;
        IACSharpSensor.IACSharpSensor.SensorReached(1606);
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      System.Int64 RNTRNTRNT_139 = _innerStream.Seek(_originalPosition + offset, origin) - _originalPosition;
      IACSharpSensor.IACSharpSensor.SensorReached(1607);
      return RNTRNTRNT_139;
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1608);
      throw new NotImplementedException();
    }
    void IDisposable.Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1609);
      Close();
      IACSharpSensor.IACSharpSensor.SensorReached(1610);
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1611);
      base.Close();
      IACSharpSensor.IACSharpSensor.SensorReached(1612);
    }
  }
}
