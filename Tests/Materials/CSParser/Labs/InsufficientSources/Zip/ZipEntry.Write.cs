using System;
using System.IO;
using RE = System.Text.RegularExpressions;
namespace Ionic.Zip
{
  public partial class ZipEntry
  {
    internal void WriteCentralDirectoryEntry(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2531);
      byte[] bytes = new byte[4096];
      int i = 0;
      bytes[i++] = (byte)(ZipConstants.ZipDirEntrySignature & 0xff);
      bytes[i++] = (byte)((ZipConstants.ZipDirEntrySignature & 0xff00) >> 8);
      bytes[i++] = (byte)((ZipConstants.ZipDirEntrySignature & 0xff0000) >> 16);
      bytes[i++] = (byte)((ZipConstants.ZipDirEntrySignature & 0xff000000u) >> 24);
      bytes[i++] = (byte)(_VersionMadeBy & 0xff);
      bytes[i++] = (byte)((_VersionMadeBy & 0xff00) >> 8);
      Int16 vNeeded = (Int16)(VersionNeeded != 0 ? VersionNeeded : 20);
      IACSharpSensor.IACSharpSensor.SensorReached(2532);
      if (_OutputUsesZip64 == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2533);
        _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2534);
      Int16 versionNeededToExtract = (Int16)(_OutputUsesZip64.Value ? 45 : vNeeded);
      bytes[i++] = (byte)(versionNeededToExtract & 0xff);
      bytes[i++] = (byte)((versionNeededToExtract & 0xff00) >> 8);
      bytes[i++] = (byte)(_BitField & 0xff);
      bytes[i++] = (byte)((_BitField & 0xff00) >> 8);
      bytes[i++] = (byte)(_CompressionMethod & 0xff);
      bytes[i++] = (byte)((_CompressionMethod & 0xff00) >> 8);
      bytes[i++] = (byte)(_TimeBlob & 0xff);
      bytes[i++] = (byte)((_TimeBlob & 0xff00) >> 8);
      bytes[i++] = (byte)((_TimeBlob & 0xff0000) >> 16);
      bytes[i++] = (byte)((_TimeBlob & 0xff000000u) >> 24);
      bytes[i++] = (byte)(_Crc32 & 0xff);
      bytes[i++] = (byte)((_Crc32 & 0xff00) >> 8);
      bytes[i++] = (byte)((_Crc32 & 0xff0000) >> 16);
      bytes[i++] = (byte)((_Crc32 & 0xff000000u) >> 24);
      int j = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(2535);
      if (_OutputUsesZip64.Value) {
        IACSharpSensor.IACSharpSensor.SensorReached(2536);
        for (j = 0; j < 8; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(2537);
          bytes[i++] = 0xff;
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2538);
        bytes[i++] = (byte)(_CompressedSize & 0xff);
        bytes[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
        bytes[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
        bytes[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
        bytes[i++] = (byte)(_UncompressedSize & 0xff);
        bytes[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
        bytes[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
        bytes[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2539);
      byte[] fileNameBytes = GetEncodedFileNameBytes();
      Int16 filenameLength = (Int16)fileNameBytes.Length;
      bytes[i++] = (byte)(filenameLength & 0xff);
      bytes[i++] = (byte)((filenameLength & 0xff00) >> 8);
      _presumeZip64 = _OutputUsesZip64.Value;
      _Extra = ConstructExtraField(true);
      Int16 extraFieldLength = (Int16)((_Extra == null) ? 0 : _Extra.Length);
      bytes[i++] = (byte)(extraFieldLength & 0xff);
      bytes[i++] = (byte)((extraFieldLength & 0xff00) >> 8);
      int commentLength = (_CommentBytes == null) ? 0 : _CommentBytes.Length;
      IACSharpSensor.IACSharpSensor.SensorReached(2540);
      if (commentLength + i > bytes.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(2541);
        commentLength = bytes.Length - i;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2542);
      bytes[i++] = (byte)(commentLength & 0xff);
      bytes[i++] = (byte)((commentLength & 0xff00) >> 8);
      bool segmented = (this._container.ZipFile != null) && (this._container.ZipFile.MaxOutputSegmentSize != 0);
      IACSharpSensor.IACSharpSensor.SensorReached(2543);
      if (segmented) {
        IACSharpSensor.IACSharpSensor.SensorReached(2544);
        bytes[i++] = (byte)(_diskNumber & 0xff);
        bytes[i++] = (byte)((_diskNumber & 0xff00) >> 8);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2545);
        bytes[i++] = 0;
        bytes[i++] = 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2546);
      bytes[i++] = (byte)((_IsText) ? 1 : 0);
      bytes[i++] = 0;
      bytes[i++] = (byte)(_ExternalFileAttrs & 0xff);
      bytes[i++] = (byte)((_ExternalFileAttrs & 0xff00) >> 8);
      bytes[i++] = (byte)((_ExternalFileAttrs & 0xff0000) >> 16);
      bytes[i++] = (byte)((_ExternalFileAttrs & 0xff000000u) >> 24);
      IACSharpSensor.IACSharpSensor.SensorReached(2547);
      if (_RelativeOffsetOfLocalHeader > 0xffffffffL) {
        IACSharpSensor.IACSharpSensor.SensorReached(2548);
        bytes[i++] = 0xff;
        bytes[i++] = 0xff;
        bytes[i++] = 0xff;
        bytes[i++] = 0xff;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2549);
        bytes[i++] = (byte)(_RelativeOffsetOfLocalHeader & 0xff);
        bytes[i++] = (byte)((_RelativeOffsetOfLocalHeader & 0xff00) >> 8);
        bytes[i++] = (byte)((_RelativeOffsetOfLocalHeader & 0xff0000) >> 16);
        bytes[i++] = (byte)((_RelativeOffsetOfLocalHeader & 0xff000000u) >> 24);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2550);
      Buffer.BlockCopy(fileNameBytes, 0, bytes, i, filenameLength);
      i += filenameLength;
      IACSharpSensor.IACSharpSensor.SensorReached(2551);
      if (_Extra != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2552);
        byte[] h = _Extra;
        int offx = 0;
        Buffer.BlockCopy(h, offx, bytes, i, extraFieldLength);
        i += extraFieldLength;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2553);
      if (commentLength != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2554);
        Buffer.BlockCopy(_CommentBytes, 0, bytes, i, commentLength);
        i += commentLength;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2555);
      s.Write(bytes, 0, i);
      IACSharpSensor.IACSharpSensor.SensorReached(2556);
    }
    private byte[] ConstructExtraField(bool forCentralDirectory)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2557);
      var listOfBlocks = new System.Collections.Generic.List<byte[]>();
      byte[] block;
      IACSharpSensor.IACSharpSensor.SensorReached(2558);
      if (_container.Zip64 == Zip64Option.Always || (_container.Zip64 == Zip64Option.AsNecessary && (!forCentralDirectory || _entryRequiresZip64.Value))) {
        IACSharpSensor.IACSharpSensor.SensorReached(2559);
        int sz = 4 + (forCentralDirectory ? 28 : 16);
        block = new byte[sz];
        int i = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(2560);
        if (_presumeZip64 || forCentralDirectory) {
          IACSharpSensor.IACSharpSensor.SensorReached(2561);
          block[i++] = 0x1;
          block[i++] = 0x0;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2562);
          block[i++] = 0x99;
          block[i++] = 0x99;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2563);
        block[i++] = (byte)(sz - 4);
        block[i++] = 0x0;
        Array.Copy(BitConverter.GetBytes(_UncompressedSize), 0, block, i, 8);
        i += 8;
        Array.Copy(BitConverter.GetBytes(_CompressedSize), 0, block, i, 8);
        i += 8;
        IACSharpSensor.IACSharpSensor.SensorReached(2564);
        if (forCentralDirectory) {
          IACSharpSensor.IACSharpSensor.SensorReached(2565);
          Array.Copy(BitConverter.GetBytes(_RelativeOffsetOfLocalHeader), 0, block, i, 8);
          i += 8;
          Array.Copy(BitConverter.GetBytes(0), 0, block, i, 4);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2566);
        listOfBlocks.Add(block);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2567);
      if (_ntfsTimesAreSet && _emitNtfsTimes) {
        IACSharpSensor.IACSharpSensor.SensorReached(2568);
        block = new byte[32 + 4];
        int i = 0;
        block[i++] = 0xa;
        block[i++] = 0x0;
        block[i++] = 32;
        block[i++] = 0;
        i += 4;
        block[i++] = 0x1;
        block[i++] = 0x0;
        block[i++] = 24;
        block[i++] = 0;
        Int64 z = _Mtime.ToFileTime();
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 8);
        i += 8;
        z = _Atime.ToFileTime();
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 8);
        i += 8;
        z = _Ctime.ToFileTime();
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 8);
        i += 8;
        listOfBlocks.Add(block);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2569);
      if (_ntfsTimesAreSet && _emitUnixTimes) {
        IACSharpSensor.IACSharpSensor.SensorReached(2570);
        int len = 5 + 4;
        IACSharpSensor.IACSharpSensor.SensorReached(2571);
        if (!forCentralDirectory) {
          IACSharpSensor.IACSharpSensor.SensorReached(2572);
          len += 8;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2573);
        block = new byte[len];
        int i = 0;
        block[i++] = 0x55;
        block[i++] = 0x54;
        block[i++] = unchecked((byte)(len - 4));
        block[i++] = 0;
        block[i++] = 0x7;
        Int32 z = unchecked((int)((_Mtime - _unixEpoch).TotalSeconds));
        Array.Copy(BitConverter.GetBytes(z), 0, block, i, 4);
        i += 4;
        IACSharpSensor.IACSharpSensor.SensorReached(2574);
        if (!forCentralDirectory) {
          IACSharpSensor.IACSharpSensor.SensorReached(2575);
          z = unchecked((int)((_Atime - _unixEpoch).TotalSeconds));
          Array.Copy(BitConverter.GetBytes(z), 0, block, i, 4);
          i += 4;
          z = unchecked((int)((_Ctime - _unixEpoch).TotalSeconds));
          Array.Copy(BitConverter.GetBytes(z), 0, block, i, 4);
          i += 4;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2576);
        listOfBlocks.Add(block);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2577);
      byte[] aggregateBlock = null;
      IACSharpSensor.IACSharpSensor.SensorReached(2578);
      if (listOfBlocks.Count > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2579);
        int totalLength = 0;
        int i, current = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(2580);
        for (i = 0; i < listOfBlocks.Count; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(2581);
          totalLength += listOfBlocks[i].Length;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2582);
        aggregateBlock = new byte[totalLength];
        IACSharpSensor.IACSharpSensor.SensorReached(2583);
        for (i = 0; i < listOfBlocks.Count; i++) {
          IACSharpSensor.IACSharpSensor.SensorReached(2584);
          System.Array.Copy(listOfBlocks[i], 0, aggregateBlock, current, listOfBlocks[i].Length);
          current += listOfBlocks[i].Length;
        }
      }
      System.Byte[] RNTRNTRNT_268 = aggregateBlock;
      IACSharpSensor.IACSharpSensor.SensorReached(2585);
      return RNTRNTRNT_268;
    }
    private string NormalizeFileName()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2586);
      string SlashFixed = FileName.Replace("\\", "/");
      string s1 = null;
      IACSharpSensor.IACSharpSensor.SensorReached(2587);
      if ((_TrimVolumeFromFullyQualifiedPaths) && (FileName.Length >= 3) && (FileName[1] == ':') && (SlashFixed[2] == '/')) {
        IACSharpSensor.IACSharpSensor.SensorReached(2588);
        s1 = SlashFixed.Substring(3);
      } else if ((FileName.Length >= 4) && ((SlashFixed[0] == '/') && (SlashFixed[1] == '/'))) {
        IACSharpSensor.IACSharpSensor.SensorReached(2591);
        int n = SlashFixed.IndexOf('/', 2);
        IACSharpSensor.IACSharpSensor.SensorReached(2592);
        if (n == -1) {
          IACSharpSensor.IACSharpSensor.SensorReached(2593);
          throw new ArgumentException("The path for that entry appears to be badly formatted");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2594);
        s1 = SlashFixed.Substring(n + 1);
      } else if ((FileName.Length >= 3) && ((SlashFixed[0] == '.') && (SlashFixed[1] == '/'))) {
        IACSharpSensor.IACSharpSensor.SensorReached(2590);
        s1 = SlashFixed.Substring(2);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2589);
        s1 = SlashFixed;
      }
      System.String RNTRNTRNT_269 = s1;
      IACSharpSensor.IACSharpSensor.SensorReached(2595);
      return RNTRNTRNT_269;
    }
    private byte[] GetEncodedFileNameBytes()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2596);
      var s1 = NormalizeFileName();
      IACSharpSensor.IACSharpSensor.SensorReached(2597);
      switch (AlternateEncodingUsage) {
        case ZipOption.Always:
          IACSharpSensor.IACSharpSensor.SensorReached(2598);
          if (!(_Comment == null || _Comment.Length == 0)) {
            IACSharpSensor.IACSharpSensor.SensorReached(2599);
            _CommentBytes = AlternateEncoding.GetBytes(_Comment);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2600);
          _actualEncoding = AlternateEncoding;
          System.Byte[] RNTRNTRNT_270 = AlternateEncoding.GetBytes(s1);
          IACSharpSensor.IACSharpSensor.SensorReached(2601);
          return RNTRNTRNT_270;
        case ZipOption.Never:
          IACSharpSensor.IACSharpSensor.SensorReached(2602);
          if (!(_Comment == null || _Comment.Length == 0)) {
            IACSharpSensor.IACSharpSensor.SensorReached(2603);
            _CommentBytes = ibm437.GetBytes(_Comment);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2604);
          _actualEncoding = ibm437;
          System.Byte[] RNTRNTRNT_271 = ibm437.GetBytes(s1);
          IACSharpSensor.IACSharpSensor.SensorReached(2605);
          return RNTRNTRNT_271;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2606);
      byte[] result = ibm437.GetBytes(s1);
      string s2 = ibm437.GetString(result, 0, result.Length);
      _CommentBytes = null;
      IACSharpSensor.IACSharpSensor.SensorReached(2607);
      if (s2 != s1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2608);
        result = AlternateEncoding.GetBytes(s1);
        IACSharpSensor.IACSharpSensor.SensorReached(2609);
        if (_Comment != null && _Comment.Length != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2610);
          _CommentBytes = AlternateEncoding.GetBytes(_Comment);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2611);
        _actualEncoding = AlternateEncoding;
        System.Byte[] RNTRNTRNT_272 = result;
        IACSharpSensor.IACSharpSensor.SensorReached(2612);
        return RNTRNTRNT_272;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2613);
      _actualEncoding = ibm437;
      IACSharpSensor.IACSharpSensor.SensorReached(2614);
      if (_Comment == null || _Comment.Length == 0) {
        System.Byte[] RNTRNTRNT_273 = result;
        IACSharpSensor.IACSharpSensor.SensorReached(2615);
        return RNTRNTRNT_273;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2616);
      byte[] cbytes = ibm437.GetBytes(_Comment);
      string c2 = ibm437.GetString(cbytes, 0, cbytes.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(2617);
      if (c2 != Comment) {
        IACSharpSensor.IACSharpSensor.SensorReached(2618);
        result = AlternateEncoding.GetBytes(s1);
        _CommentBytes = AlternateEncoding.GetBytes(_Comment);
        _actualEncoding = AlternateEncoding;
        System.Byte[] RNTRNTRNT_274 = result;
        IACSharpSensor.IACSharpSensor.SensorReached(2619);
        return RNTRNTRNT_274;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2620);
      _CommentBytes = cbytes;
      System.Byte[] RNTRNTRNT_275 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(2621);
      return RNTRNTRNT_275;
    }
    private bool WantReadAgain()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2622);
      if (_UncompressedSize < 0x10) {
        System.Boolean RNTRNTRNT_276 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2623);
        return RNTRNTRNT_276;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2624);
      if (_CompressionMethod == 0x0) {
        System.Boolean RNTRNTRNT_277 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2625);
        return RNTRNTRNT_277;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2626);
      if (CompressionLevel == Ionic.Zlib.CompressionLevel.None) {
        System.Boolean RNTRNTRNT_278 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2627);
        return RNTRNTRNT_278;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2628);
      if (_CompressedSize < _UncompressedSize) {
        System.Boolean RNTRNTRNT_279 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2629);
        return RNTRNTRNT_279;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2630);
      if (this._Source == ZipEntrySource.Stream && !this._sourceStream.CanSeek) {
        System.Boolean RNTRNTRNT_280 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2631);
        return RNTRNTRNT_280;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2632);
      if (_zipCrypto_forWrite != null && (CompressedSize - 12) <= UncompressedSize) {
        System.Boolean RNTRNTRNT_281 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2633);
        return RNTRNTRNT_281;
      }
      System.Boolean RNTRNTRNT_282 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(2634);
      return RNTRNTRNT_282;
    }
    private void MaybeUnsetCompressionMethodForWriting(int cycle)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2635);
      if (cycle > 1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2636);
        _CompressionMethod = 0x0;
        IACSharpSensor.IACSharpSensor.SensorReached(2637);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2638);
      if (IsDirectory) {
        IACSharpSensor.IACSharpSensor.SensorReached(2639);
        _CompressionMethod = 0x0;
        IACSharpSensor.IACSharpSensor.SensorReached(2640);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2641);
      if (this._Source == ZipEntrySource.ZipFile) {
        IACSharpSensor.IACSharpSensor.SensorReached(2642);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2643);
      if (this._Source == ZipEntrySource.Stream) {
        IACSharpSensor.IACSharpSensor.SensorReached(2644);
        if (_sourceStream != null && _sourceStream.CanSeek) {
          IACSharpSensor.IACSharpSensor.SensorReached(2645);
          long fileLength = _sourceStream.Length;
          IACSharpSensor.IACSharpSensor.SensorReached(2646);
          if (fileLength == 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(2647);
            _CompressionMethod = 0x0;
            IACSharpSensor.IACSharpSensor.SensorReached(2648);
            return;
          }
        }
      } else if ((this._Source == ZipEntrySource.FileSystem) && (SharedUtilities.GetFileLength(LocalFileName) == 0L)) {
        IACSharpSensor.IACSharpSensor.SensorReached(2649);
        _CompressionMethod = 0x0;
        IACSharpSensor.IACSharpSensor.SensorReached(2650);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2651);
      if (SetCompression != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2652);
        CompressionLevel = SetCompression(LocalFileName, _FileNameInArchive);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2653);
      if (CompressionLevel == (short)Ionic.Zlib.CompressionLevel.None && CompressionMethod == Ionic.Zip.CompressionMethod.Deflate) {
        IACSharpSensor.IACSharpSensor.SensorReached(2654);
        _CompressionMethod = 0x0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2655);
      return;
    }
    internal void WriteHeader(Stream s, int cycle)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2656);
      var counter = s as CountingStream;
      _future_ROLH = (counter != null) ? counter.ComputedPosition : s.Position;
      int j = 0, i = 0;
      byte[] block = new byte[30];
      block[i++] = (byte)(ZipConstants.ZipEntrySignature & 0xff);
      block[i++] = (byte)((ZipConstants.ZipEntrySignature & 0xff00) >> 8);
      block[i++] = (byte)((ZipConstants.ZipEntrySignature & 0xff0000) >> 16);
      block[i++] = (byte)((ZipConstants.ZipEntrySignature & 0xff000000u) >> 24);
      _presumeZip64 = (_container.Zip64 == Zip64Option.Always || (_container.Zip64 == Zip64Option.AsNecessary && !s.CanSeek));
      Int16 VersionNeededToExtract = (Int16)(_presumeZip64 ? 45 : 20);
      block[i++] = (byte)(VersionNeededToExtract & 0xff);
      block[i++] = (byte)((VersionNeededToExtract & 0xff00) >> 8);
      byte[] fileNameBytes = GetEncodedFileNameBytes();
      Int16 filenameLength = (Int16)fileNameBytes.Length;
      IACSharpSensor.IACSharpSensor.SensorReached(2657);
      if (_Encryption == EncryptionAlgorithm.None) {
        IACSharpSensor.IACSharpSensor.SensorReached(2658);
        _BitField &= ~1;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2659);
        _BitField |= 1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2660);
      if (_actualEncoding.CodePage == System.Text.Encoding.UTF8.CodePage) {
        IACSharpSensor.IACSharpSensor.SensorReached(2661);
        _BitField |= 0x800;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2662);
      if (IsDirectory || cycle == 99) {
        IACSharpSensor.IACSharpSensor.SensorReached(2663);
        _BitField &= ~0x8;
        _BitField &= ~0x1;
        Encryption = EncryptionAlgorithm.None;
        Password = null;
      } else if (!s.CanSeek) {
        IACSharpSensor.IACSharpSensor.SensorReached(2664);
        _BitField |= 0x8;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2665);
      block[i++] = (byte)(_BitField & 0xff);
      block[i++] = (byte)((_BitField & 0xff00) >> 8);
      IACSharpSensor.IACSharpSensor.SensorReached(2666);
      if (this.__FileDataPosition == -1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2667);
        _CompressedSize = 0;
        _crcCalculated = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2668);
      MaybeUnsetCompressionMethodForWriting(cycle);
      block[i++] = (byte)(_CompressionMethod & 0xff);
      block[i++] = (byte)((_CompressionMethod & 0xff00) >> 8);
      IACSharpSensor.IACSharpSensor.SensorReached(2669);
      if (cycle == 99) {
        IACSharpSensor.IACSharpSensor.SensorReached(2670);
        SetZip64Flags();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2671);
      _TimeBlob = Ionic.Zip.SharedUtilities.DateTimeToPacked(LastModified);
      block[i++] = (byte)(_TimeBlob & 0xff);
      block[i++] = (byte)((_TimeBlob & 0xff00) >> 8);
      block[i++] = (byte)((_TimeBlob & 0xff0000) >> 16);
      block[i++] = (byte)((_TimeBlob & 0xff000000u) >> 24);
      block[i++] = (byte)(_Crc32 & 0xff);
      block[i++] = (byte)((_Crc32 & 0xff00) >> 8);
      block[i++] = (byte)((_Crc32 & 0xff0000) >> 16);
      block[i++] = (byte)((_Crc32 & 0xff000000u) >> 24);
      IACSharpSensor.IACSharpSensor.SensorReached(2672);
      if (_presumeZip64) {
        IACSharpSensor.IACSharpSensor.SensorReached(2673);
        for (j = 0; j < 8; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(2674);
          block[i++] = 0xff;
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2675);
        block[i++] = (byte)(_CompressedSize & 0xff);
        block[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
        block[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
        block[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
        block[i++] = (byte)(_UncompressedSize & 0xff);
        block[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
        block[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
        block[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2676);
      block[i++] = (byte)(filenameLength & 0xff);
      block[i++] = (byte)((filenameLength & 0xff00) >> 8);
      _Extra = ConstructExtraField(false);
      Int16 extraFieldLength = (Int16)((_Extra == null) ? 0 : _Extra.Length);
      block[i++] = (byte)(extraFieldLength & 0xff);
      block[i++] = (byte)((extraFieldLength & 0xff00) >> 8);
      byte[] bytes = new byte[i + filenameLength + extraFieldLength];
      Buffer.BlockCopy(block, 0, bytes, 0, i);
      Buffer.BlockCopy(fileNameBytes, 0, bytes, i, fileNameBytes.Length);
      i += fileNameBytes.Length;
      IACSharpSensor.IACSharpSensor.SensorReached(2677);
      if (_Extra != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2678);
        Buffer.BlockCopy(_Extra, 0, bytes, i, _Extra.Length);
        i += _Extra.Length;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2679);
      _LengthOfHeader = i;
      var zss = s as ZipSegmentedStream;
      IACSharpSensor.IACSharpSensor.SensorReached(2680);
      if (zss != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2681);
        zss.ContiguousWrite = true;
        UInt32 requiredSegment = zss.ComputeSegment(i);
        IACSharpSensor.IACSharpSensor.SensorReached(2682);
        if (requiredSegment != zss.CurrentSegment) {
          IACSharpSensor.IACSharpSensor.SensorReached(2683);
          _future_ROLH = 0;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2684);
          _future_ROLH = zss.Position;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2685);
        _diskNumber = requiredSegment;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2686);
      if (_container.Zip64 == Zip64Option.Never && (uint)_RelativeOffsetOfLocalHeader >= 0xffffffffu) {
        IACSharpSensor.IACSharpSensor.SensorReached(2687);
        throw new ZipException("Offset within the zip archive exceeds 0xFFFFFFFF. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2688);
      s.Write(bytes, 0, i);
      IACSharpSensor.IACSharpSensor.SensorReached(2689);
      if (zss != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2690);
        zss.ContiguousWrite = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2691);
      _EntryHeader = bytes;
      IACSharpSensor.IACSharpSensor.SensorReached(2692);
    }
    private Int32 FigureCrc32()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2693);
      if (_crcCalculated == false) {
        IACSharpSensor.IACSharpSensor.SensorReached(2694);
        Stream input = null;
        IACSharpSensor.IACSharpSensor.SensorReached(2695);
        if (this._Source == ZipEntrySource.WriteDelegate) {
          IACSharpSensor.IACSharpSensor.SensorReached(2696);
          var output = new Ionic.Crc.CrcCalculatorStream(Stream.Null);
          this._WriteDelegate(this.FileName, output);
          _Crc32 = output.Crc;
        } else if (this._Source == ZipEntrySource.ZipFile) {
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2697);
          if (this._Source == ZipEntrySource.Stream) {
            IACSharpSensor.IACSharpSensor.SensorReached(2698);
            PrepSourceStream();
            input = this._sourceStream;
          } else if (this._Source == ZipEntrySource.JitStream) {
            IACSharpSensor.IACSharpSensor.SensorReached(2700);
            if (this._sourceStream == null) {
              IACSharpSensor.IACSharpSensor.SensorReached(2701);
              _sourceStream = this._OpenDelegate(this.FileName);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2702);
            PrepSourceStream();
            input = this._sourceStream;
          } else if (this._Source == ZipEntrySource.ZipOutputStream) {
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2699);
            input = File.Open(LocalFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2703);
          var crc32 = new Ionic.Crc.CRC32();
          _Crc32 = crc32.GetCrc32(input);
          IACSharpSensor.IACSharpSensor.SensorReached(2704);
          if (_sourceStream == null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2705);
            input.Dispose();
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2706);
        _crcCalculated = true;
      }
      Int32 RNTRNTRNT_283 = _Crc32;
      IACSharpSensor.IACSharpSensor.SensorReached(2707);
      return RNTRNTRNT_283;
    }
    private void PrepSourceStream()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2708);
      if (_sourceStream == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2709);
        throw new ZipException(String.Format("The input stream is null for entry '{0}'.", FileName));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2710);
      if (this._sourceStreamOriginalPosition != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2711);
        this._sourceStream.Position = this._sourceStreamOriginalPosition.Value;
      } else if (this._sourceStream.CanSeek) {
        IACSharpSensor.IACSharpSensor.SensorReached(2714);
        this._sourceStreamOriginalPosition = new Nullable<Int64>(this._sourceStream.Position);
      } else if (this.Encryption == EncryptionAlgorithm.PkzipWeak) {
        IACSharpSensor.IACSharpSensor.SensorReached(2712);
        if (this._Source != ZipEntrySource.ZipFile && ((this._BitField & 0x8) != 0x8)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2713);
          throw new ZipException("It is not possible to use PKZIP encryption on a non-seekable input stream");
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2715);
    }
    internal void CopyMetaData(ZipEntry source)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2716);
      this.__FileDataPosition = source.__FileDataPosition;
      this.CompressionMethod = source.CompressionMethod;
      this._CompressionMethod_FromZipFile = source._CompressionMethod_FromZipFile;
      this._CompressedFileDataSize = source._CompressedFileDataSize;
      this._UncompressedSize = source._UncompressedSize;
      this._BitField = source._BitField;
      this._Source = source._Source;
      this._LastModified = source._LastModified;
      this._Mtime = source._Mtime;
      this._Atime = source._Atime;
      this._Ctime = source._Ctime;
      this._ntfsTimesAreSet = source._ntfsTimesAreSet;
      this._emitUnixTimes = source._emitUnixTimes;
      this._emitNtfsTimes = source._emitNtfsTimes;
      IACSharpSensor.IACSharpSensor.SensorReached(2717);
    }
    private void OnWriteBlock(Int64 bytesXferred, Int64 totalBytesToXfer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2718);
      if (_container.ZipFile != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2719);
        _ioOperationCanceled = _container.ZipFile.OnSaveBlock(this, bytesXferred, totalBytesToXfer);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2720);
    }
    private void _WriteEntryData(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2721);
      Stream input = null;
      long fdp = -1L;
      IACSharpSensor.IACSharpSensor.SensorReached(2722);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2723);
        fdp = s.Position;
      } catch (Exception) {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2724);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(2725);
        long fileLength = SetInputAndFigureFileLength(ref input);
        CountingStream entryCounter = new CountingStream(s);
        Stream encryptor;
        Stream compressor;
        IACSharpSensor.IACSharpSensor.SensorReached(2726);
        if (fileLength != 0L) {
          IACSharpSensor.IACSharpSensor.SensorReached(2727);
          encryptor = MaybeApplyEncryption(entryCounter);
          compressor = MaybeApplyCompression(encryptor, fileLength);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2728);
          encryptor = compressor = entryCounter;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2729);
        var output = new Ionic.Crc.CrcCalculatorStream(compressor, true);
        IACSharpSensor.IACSharpSensor.SensorReached(2730);
        if (this._Source == ZipEntrySource.WriteDelegate) {
          IACSharpSensor.IACSharpSensor.SensorReached(2731);
          this._WriteDelegate(this.FileName, output);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2732);
          byte[] buffer = new byte[BufferSize];
          int n;
          IACSharpSensor.IACSharpSensor.SensorReached(2733);
          while ((n = SharedUtilities.ReadWithRetry(input, buffer, 0, buffer.Length, FileName)) != 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(2734);
            output.Write(buffer, 0, n);
            OnWriteBlock(output.TotalBytesSlurped, fileLength);
            IACSharpSensor.IACSharpSensor.SensorReached(2735);
            if (_ioOperationCanceled) {
              IACSharpSensor.IACSharpSensor.SensorReached(2736);
              break;
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2737);
        FinishOutputStream(s, entryCounter, encryptor, compressor, output);
      } finally {
        IACSharpSensor.IACSharpSensor.SensorReached(2738);
        if (this._Source == ZipEntrySource.JitStream) {
          IACSharpSensor.IACSharpSensor.SensorReached(2739);
          if (this._CloseDelegate != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2740);
            this._CloseDelegate(this.FileName, input);
          }
        } else if ((input as FileStream) != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2741);
          input.Dispose();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2742);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2743);
      if (_ioOperationCanceled) {
        IACSharpSensor.IACSharpSensor.SensorReached(2744);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2745);
      this.__FileDataPosition = fdp;
      PostProcessOutput(s);
      IACSharpSensor.IACSharpSensor.SensorReached(2746);
    }
    private long SetInputAndFigureFileLength(ref Stream input)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2747);
      long fileLength = -1L;
      IACSharpSensor.IACSharpSensor.SensorReached(2748);
      if (this._Source == ZipEntrySource.Stream) {
        IACSharpSensor.IACSharpSensor.SensorReached(2749);
        PrepSourceStream();
        input = this._sourceStream;
        IACSharpSensor.IACSharpSensor.SensorReached(2750);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(2751);
          fileLength = this._sourceStream.Length;
        } catch (NotSupportedException) {
        }
      } else if (this._Source == ZipEntrySource.ZipFile) {
        IACSharpSensor.IACSharpSensor.SensorReached(2758);
        string pwd = (_Encryption_FromZipFile == EncryptionAlgorithm.None) ? null : (this._Password ?? this._container.Password);
        this._sourceStream = InternalOpenReader(pwd);
        PrepSourceStream();
        input = this._sourceStream;
        fileLength = this._sourceStream.Length;
      } else if (this._Source == ZipEntrySource.JitStream) {
        IACSharpSensor.IACSharpSensor.SensorReached(2753);
        if (this._sourceStream == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2754);
          _sourceStream = this._OpenDelegate(this.FileName);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2755);
        PrepSourceStream();
        input = this._sourceStream;
        IACSharpSensor.IACSharpSensor.SensorReached(2756);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(2757);
          fileLength = this._sourceStream.Length;
        } catch (NotSupportedException) {
        }
      } else if (this._Source == ZipEntrySource.FileSystem) {
        IACSharpSensor.IACSharpSensor.SensorReached(2752);
        FileShare fs = FileShare.ReadWrite;
        fs |= FileShare.Delete;
        input = File.Open(LocalFileName, FileMode.Open, FileAccess.Read, fs);
        fileLength = input.Length;
      }
      System.Int64 RNTRNTRNT_284 = fileLength;
      IACSharpSensor.IACSharpSensor.SensorReached(2759);
      return RNTRNTRNT_284;
    }
    internal void FinishOutputStream(Stream s, CountingStream entryCounter, Stream encryptor, Stream compressor, Ionic.Crc.CrcCalculatorStream output)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2760);
      if (output == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2761);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2762);
      output.Close();
      IACSharpSensor.IACSharpSensor.SensorReached(2763);
      if ((compressor as Ionic.Zlib.DeflateStream) != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2764);
        compressor.Close();
      } else if ((compressor as Ionic.Zlib.ParallelDeflateOutputStream) != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2765);
        compressor.Close();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2766);
      encryptor.Flush();
      encryptor.Close();
      _LengthOfTrailer = 0;
      _UncompressedSize = output.TotalBytesSlurped;
      _CompressedFileDataSize = entryCounter.BytesWritten;
      _CompressedSize = _CompressedFileDataSize;
      _Crc32 = output.Crc;
      StoreRelativeOffset();
      IACSharpSensor.IACSharpSensor.SensorReached(2767);
    }
    internal void PostProcessOutput(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2768);
      var s1 = s as CountingStream;
      IACSharpSensor.IACSharpSensor.SensorReached(2769);
      if (_UncompressedSize == 0 && _CompressedSize == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2770);
        if (this._Source == ZipEntrySource.ZipOutputStream) {
          IACSharpSensor.IACSharpSensor.SensorReached(2771);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2772);
        if (_Password != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(2773);
          int headerBytesToRetract = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(2774);
          if (Encryption == EncryptionAlgorithm.PkzipWeak) {
            IACSharpSensor.IACSharpSensor.SensorReached(2775);
            headerBytesToRetract = 12;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2776);
          if (this._Source == ZipEntrySource.ZipOutputStream && !s.CanSeek) {
            IACSharpSensor.IACSharpSensor.SensorReached(2777);
            throw new ZipException("Zero bytes written, encryption in use, and non-seekable output.");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2778);
          if (Encryption != EncryptionAlgorithm.None) {
            IACSharpSensor.IACSharpSensor.SensorReached(2779);
            s.Seek(-1 * headerBytesToRetract, SeekOrigin.Current);
            s.SetLength(s.Position);
            Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
            IACSharpSensor.IACSharpSensor.SensorReached(2780);
            if (s1 != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(2781);
              s1.Adjust(headerBytesToRetract);
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2782);
            _LengthOfHeader -= headerBytesToRetract;
            __FileDataPosition -= headerBytesToRetract;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2783);
          _Password = null;
          _BitField &= ~(0x1);
          int j = 6;
          _EntryHeader[j++] = (byte)(_BitField & 0xff);
          _EntryHeader[j++] = (byte)((_BitField & 0xff00) >> 8);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2784);
        CompressionMethod = 0;
        Encryption = EncryptionAlgorithm.None;
      } else if (_zipCrypto_forWrite != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2785);
        if (Encryption == EncryptionAlgorithm.PkzipWeak) {
          IACSharpSensor.IACSharpSensor.SensorReached(2786);
          _CompressedSize += 12;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2787);
      int i = 8;
      _EntryHeader[i++] = (byte)(_CompressionMethod & 0xff);
      _EntryHeader[i++] = (byte)((_CompressionMethod & 0xff00) >> 8);
      i = 14;
      _EntryHeader[i++] = (byte)(_Crc32 & 0xff);
      _EntryHeader[i++] = (byte)((_Crc32 & 0xff00) >> 8);
      _EntryHeader[i++] = (byte)((_Crc32 & 0xff0000) >> 16);
      _EntryHeader[i++] = (byte)((_Crc32 & 0xff000000u) >> 24);
      SetZip64Flags();
      Int16 filenameLength = (short)(_EntryHeader[26] + _EntryHeader[27] * 256);
      Int16 extraFieldLength = (short)(_EntryHeader[28] + _EntryHeader[29] * 256);
      IACSharpSensor.IACSharpSensor.SensorReached(2788);
      if (_OutputUsesZip64.Value) {
        IACSharpSensor.IACSharpSensor.SensorReached(2789);
        _EntryHeader[4] = (byte)(45 & 0xff);
        _EntryHeader[5] = 0x0;
        IACSharpSensor.IACSharpSensor.SensorReached(2790);
        for (int j = 0; j < 8; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(2791);
          _EntryHeader[i++] = 0xff;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2792);
        i = 30 + filenameLength;
        _EntryHeader[i++] = 0x1;
        _EntryHeader[i++] = 0x0;
        i += 2;
        Array.Copy(BitConverter.GetBytes(_UncompressedSize), 0, _EntryHeader, i, 8);
        i += 8;
        Array.Copy(BitConverter.GetBytes(_CompressedSize), 0, _EntryHeader, i, 8);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2793);
        _EntryHeader[4] = (byte)(20 & 0xff);
        _EntryHeader[5] = 0x0;
        i = 18;
        _EntryHeader[i++] = (byte)(_CompressedSize & 0xff);
        _EntryHeader[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
        _EntryHeader[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
        _EntryHeader[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
        _EntryHeader[i++] = (byte)(_UncompressedSize & 0xff);
        _EntryHeader[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
        _EntryHeader[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
        _EntryHeader[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
        IACSharpSensor.IACSharpSensor.SensorReached(2794);
        if (extraFieldLength != 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2795);
          i = 30 + filenameLength;
          Int16 DataSize = (short)(_EntryHeader[i + 2] + _EntryHeader[i + 3] * 256);
          IACSharpSensor.IACSharpSensor.SensorReached(2796);
          if (DataSize == 16) {
            IACSharpSensor.IACSharpSensor.SensorReached(2797);
            _EntryHeader[i++] = 0x99;
            _EntryHeader[i++] = 0x99;
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2798);
      if ((_BitField & 0x8) != 0x8 || (this._Source == ZipEntrySource.ZipOutputStream && s.CanSeek)) {
        IACSharpSensor.IACSharpSensor.SensorReached(2799);
        var zss = s as ZipSegmentedStream;
        IACSharpSensor.IACSharpSensor.SensorReached(2800);
        if (zss != null && _diskNumber != zss.CurrentSegment) {
          IACSharpSensor.IACSharpSensor.SensorReached(2801);
          using (Stream hseg = ZipSegmentedStream.ForUpdate(this._container.ZipFile.Name, _diskNumber)) {
            IACSharpSensor.IACSharpSensor.SensorReached(2802);
            hseg.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
            hseg.Write(_EntryHeader, 0, _EntryHeader.Length);
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2803);
          s.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
          s.Write(_EntryHeader, 0, _EntryHeader.Length);
          IACSharpSensor.IACSharpSensor.SensorReached(2804);
          if (s1 != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2805);
            s1.Adjust(_EntryHeader.Length);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2806);
          s.Seek(_CompressedSize, SeekOrigin.Current);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2807);
      if (((_BitField & 0x8) == 0x8) && !IsDirectory) {
        IACSharpSensor.IACSharpSensor.SensorReached(2808);
        byte[] Descriptor = new byte[16 + (_OutputUsesZip64.Value ? 8 : 0)];
        i = 0;
        Array.Copy(BitConverter.GetBytes(ZipConstants.ZipEntryDataDescriptorSignature), 0, Descriptor, i, 4);
        i += 4;
        Array.Copy(BitConverter.GetBytes(_Crc32), 0, Descriptor, i, 4);
        i += 4;
        IACSharpSensor.IACSharpSensor.SensorReached(2809);
        if (_OutputUsesZip64.Value) {
          IACSharpSensor.IACSharpSensor.SensorReached(2810);
          Array.Copy(BitConverter.GetBytes(_CompressedSize), 0, Descriptor, i, 8);
          i += 8;
          Array.Copy(BitConverter.GetBytes(_UncompressedSize), 0, Descriptor, i, 8);
          i += 8;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2811);
          Descriptor[i++] = (byte)(_CompressedSize & 0xff);
          Descriptor[i++] = (byte)((_CompressedSize & 0xff00) >> 8);
          Descriptor[i++] = (byte)((_CompressedSize & 0xff0000) >> 16);
          Descriptor[i++] = (byte)((_CompressedSize & 0xff000000u) >> 24);
          Descriptor[i++] = (byte)(_UncompressedSize & 0xff);
          Descriptor[i++] = (byte)((_UncompressedSize & 0xff00) >> 8);
          Descriptor[i++] = (byte)((_UncompressedSize & 0xff0000) >> 16);
          Descriptor[i++] = (byte)((_UncompressedSize & 0xff000000u) >> 24);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2812);
        s.Write(Descriptor, 0, Descriptor.Length);
        _LengthOfTrailer += Descriptor.Length;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2813);
    }
    private void SetZip64Flags()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2814);
      _entryRequiresZip64 = new Nullable<bool>(_CompressedSize >= 0xffffffffu || _UncompressedSize >= 0xffffffffu || _RelativeOffsetOfLocalHeader >= 0xffffffffu);
      IACSharpSensor.IACSharpSensor.SensorReached(2815);
      if (_container.Zip64 == Zip64Option.Never && _entryRequiresZip64.Value) {
        IACSharpSensor.IACSharpSensor.SensorReached(2816);
        throw new ZipException("Compressed or Uncompressed size, or offset exceeds the maximum value. Consider setting the UseZip64WhenSaving property on the ZipFile instance.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2817);
      _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always || _entryRequiresZip64.Value);
      IACSharpSensor.IACSharpSensor.SensorReached(2818);
    }
    internal void PrepOutputStream(Stream s, long streamLength, out CountingStream outputCounter, out Stream encryptor, out Stream compressor, out Ionic.Crc.CrcCalculatorStream output)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2819);
      TraceWriteLine("PrepOutputStream: e({0}) comp({1}) crypto({2}) zf({3})", FileName, CompressionLevel, Encryption, _container.Name);
      outputCounter = new CountingStream(s);
      IACSharpSensor.IACSharpSensor.SensorReached(2820);
      if (streamLength != 0L) {
        IACSharpSensor.IACSharpSensor.SensorReached(2821);
        encryptor = MaybeApplyEncryption(outputCounter);
        compressor = MaybeApplyCompression(encryptor, streamLength);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2822);
        encryptor = compressor = outputCounter;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2823);
      output = new Ionic.Crc.CrcCalculatorStream(compressor, true);
      IACSharpSensor.IACSharpSensor.SensorReached(2824);
    }
    private Stream MaybeApplyCompression(Stream s, long streamLength)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2825);
      if (_CompressionMethod == 0x8 && CompressionLevel != Ionic.Zlib.CompressionLevel.None) {
        IACSharpSensor.IACSharpSensor.SensorReached(2826);
        if (_container.ParallelDeflateThreshold == 0L || (streamLength > _container.ParallelDeflateThreshold && _container.ParallelDeflateThreshold > 0L)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2827);
          if (_container.ParallelDeflater == null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2828);
            _container.ParallelDeflater = new Ionic.Zlib.ParallelDeflateOutputStream(s, CompressionLevel, _container.Strategy, true);
            IACSharpSensor.IACSharpSensor.SensorReached(2829);
            if (_container.CodecBufferSize > 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(2830);
              _container.ParallelDeflater.BufferSize = _container.CodecBufferSize;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2831);
            if (_container.ParallelDeflateMaxBufferPairs > 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(2832);
              _container.ParallelDeflater.MaxBufferPairs = _container.ParallelDeflateMaxBufferPairs;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2833);
          Ionic.Zlib.ParallelDeflateOutputStream o1 = _container.ParallelDeflater;
          o1.Reset(s);
          Stream RNTRNTRNT_285 = o1;
          IACSharpSensor.IACSharpSensor.SensorReached(2834);
          return RNTRNTRNT_285;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2835);
        var o = new Ionic.Zlib.DeflateStream(s, Ionic.Zlib.CompressionMode.Compress, CompressionLevel, true);
        IACSharpSensor.IACSharpSensor.SensorReached(2836);
        if (_container.CodecBufferSize > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2837);
          o.BufferSize = _container.CodecBufferSize;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2838);
        o.Strategy = _container.Strategy;
        Stream RNTRNTRNT_286 = o;
        IACSharpSensor.IACSharpSensor.SensorReached(2839);
        return RNTRNTRNT_286;
      }
      Stream RNTRNTRNT_287 = s;
      IACSharpSensor.IACSharpSensor.SensorReached(2840);
      return RNTRNTRNT_287;
    }
    private Stream MaybeApplyEncryption(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2841);
      if (Encryption == EncryptionAlgorithm.PkzipWeak) {
        IACSharpSensor.IACSharpSensor.SensorReached(2842);
        TraceWriteLine("MaybeApplyEncryption: e({0}) PKZIP", FileName);
        Stream RNTRNTRNT_288 = new ZipCipherStream(s, _zipCrypto_forWrite, CryptoMode.Encrypt);
        IACSharpSensor.IACSharpSensor.SensorReached(2843);
        return RNTRNTRNT_288;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2844);
      TraceWriteLine("MaybeApplyEncryption: e({0}) None", FileName);
      Stream RNTRNTRNT_289 = s;
      IACSharpSensor.IACSharpSensor.SensorReached(2845);
      return RNTRNTRNT_289;
    }
    private void OnZipErrorWhileSaving(Exception e)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2846);
      if (_container.ZipFile != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2847);
        _ioOperationCanceled = _container.ZipFile.OnZipErrorSaving(this, e);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2848);
    }
    internal void Write(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2849);
      var cs1 = s as CountingStream;
      var zss1 = s as ZipSegmentedStream;
      bool done = false;
      IACSharpSensor.IACSharpSensor.SensorReached(2850);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(2851);
        try {
          IACSharpSensor.IACSharpSensor.SensorReached(2852);
          if (_Source == ZipEntrySource.ZipFile && !_restreamRequiredOnSave) {
            IACSharpSensor.IACSharpSensor.SensorReached(2853);
            CopyThroughOneEntry(s);
            IACSharpSensor.IACSharpSensor.SensorReached(2854);
            return;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2855);
          if (IsDirectory) {
            IACSharpSensor.IACSharpSensor.SensorReached(2856);
            WriteHeader(s, 1);
            StoreRelativeOffset();
            _entryRequiresZip64 = new Nullable<bool>(_RelativeOffsetOfLocalHeader >= 0xffffffffu);
            _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always || _entryRequiresZip64.Value);
            IACSharpSensor.IACSharpSensor.SensorReached(2857);
            if (zss1 != null) {
              IACSharpSensor.IACSharpSensor.SensorReached(2858);
              _diskNumber = zss1.CurrentSegment;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2859);
            return;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2860);
          bool readAgain = true;
          int nCycles = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(2861);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(2862);
            nCycles++;
            WriteHeader(s, nCycles);
            WriteSecurityMetadata(s);
            _WriteEntryData(s);
            _TotalEntrySize = _LengthOfHeader + _CompressedFileDataSize + _LengthOfTrailer;
            IACSharpSensor.IACSharpSensor.SensorReached(2863);
            if (nCycles > 1) {
              IACSharpSensor.IACSharpSensor.SensorReached(2864);
              readAgain = false;
            } else if (!s.CanSeek) {
              IACSharpSensor.IACSharpSensor.SensorReached(2866);
              readAgain = false;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(2865);
              readAgain = WantReadAgain();
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2867);
            if (readAgain) {
              IACSharpSensor.IACSharpSensor.SensorReached(2868);
              if (zss1 != null) {
                IACSharpSensor.IACSharpSensor.SensorReached(2869);
                zss1.TruncateBackward(_diskNumber, _RelativeOffsetOfLocalHeader);
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(2870);
                s.Seek(_RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
              }
              IACSharpSensor.IACSharpSensor.SensorReached(2871);
              s.SetLength(s.Position);
              IACSharpSensor.IACSharpSensor.SensorReached(2872);
              if (cs1 != null) {
                IACSharpSensor.IACSharpSensor.SensorReached(2873);
                cs1.Adjust(_TotalEntrySize);
              }
            }
          } while (readAgain);
          IACSharpSensor.IACSharpSensor.SensorReached(2874);
          _skippedDuringSave = false;
          done = true;
        } catch (System.Exception exc1) {
          IACSharpSensor.IACSharpSensor.SensorReached(2875);
          ZipErrorAction orig = this.ZipErrorAction;
          int loop = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(2876);
          do {
            IACSharpSensor.IACSharpSensor.SensorReached(2877);
            if (ZipErrorAction == ZipErrorAction.Throw) {
              IACSharpSensor.IACSharpSensor.SensorReached(2878);
              throw;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2879);
            if (ZipErrorAction == ZipErrorAction.Skip || ZipErrorAction == ZipErrorAction.Retry) {
              IACSharpSensor.IACSharpSensor.SensorReached(2880);
              long p1 = (cs1 != null) ? cs1.ComputedPosition : s.Position;
              long delta = p1 - _future_ROLH;
              IACSharpSensor.IACSharpSensor.SensorReached(2881);
              if (delta > 0) {
                IACSharpSensor.IACSharpSensor.SensorReached(2882);
                s.Seek(delta, SeekOrigin.Current);
                long p2 = s.Position;
                s.SetLength(s.Position);
                IACSharpSensor.IACSharpSensor.SensorReached(2883);
                if (cs1 != null) {
                  IACSharpSensor.IACSharpSensor.SensorReached(2884);
                  cs1.Adjust(p1 - p2);
                }
              }
              IACSharpSensor.IACSharpSensor.SensorReached(2885);
              if (ZipErrorAction == ZipErrorAction.Skip) {
                IACSharpSensor.IACSharpSensor.SensorReached(2886);
                WriteStatus("Skipping file {0} (exception: {1})", LocalFileName, exc1.ToString());
                _skippedDuringSave = true;
                done = true;
              } else {
                IACSharpSensor.IACSharpSensor.SensorReached(2887);
                this.ZipErrorAction = orig;
              }
              IACSharpSensor.IACSharpSensor.SensorReached(2888);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2889);
            if (loop > 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(2890);
              throw;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2891);
            if (ZipErrorAction == ZipErrorAction.InvokeErrorEvent) {
              IACSharpSensor.IACSharpSensor.SensorReached(2892);
              OnZipErrorWhileSaving(exc1);
              IACSharpSensor.IACSharpSensor.SensorReached(2893);
              if (_ioOperationCanceled) {
                IACSharpSensor.IACSharpSensor.SensorReached(2894);
                done = true;
                IACSharpSensor.IACSharpSensor.SensorReached(2895);
                break;
              }
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2896);
            loop++;
          } while (true);
          IACSharpSensor.IACSharpSensor.SensorReached(2897);
        }
      } while (!done);
      IACSharpSensor.IACSharpSensor.SensorReached(2898);
    }
    internal void StoreRelativeOffset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2899);
      _RelativeOffsetOfLocalHeader = _future_ROLH;
      IACSharpSensor.IACSharpSensor.SensorReached(2900);
    }
    internal void NotifySaveComplete()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2901);
      _Encryption_FromZipFile = _Encryption;
      _CompressionMethod_FromZipFile = _CompressionMethod;
      _restreamRequiredOnSave = false;
      _metadataChanged = false;
      _Source = ZipEntrySource.ZipFile;
      IACSharpSensor.IACSharpSensor.SensorReached(2902);
    }
    internal void WriteSecurityMetadata(Stream outstream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2903);
      if (Encryption == EncryptionAlgorithm.None) {
        IACSharpSensor.IACSharpSensor.SensorReached(2904);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2905);
      string pwd = this._Password;
      IACSharpSensor.IACSharpSensor.SensorReached(2906);
      if (this._Source == ZipEntrySource.ZipFile && pwd == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2907);
        pwd = this._container.Password;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2908);
      if (pwd == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2909);
        _zipCrypto_forWrite = null;
        IACSharpSensor.IACSharpSensor.SensorReached(2910);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2911);
      TraceWriteLine("WriteSecurityMetadata: e({0}) crypto({1}) pw({2})", FileName, Encryption.ToString(), pwd);
      IACSharpSensor.IACSharpSensor.SensorReached(2912);
      if (Encryption == EncryptionAlgorithm.PkzipWeak) {
        IACSharpSensor.IACSharpSensor.SensorReached(2913);
        _zipCrypto_forWrite = ZipCrypto.ForWrite(pwd);
        var rnd = new System.Random();
        byte[] encryptionHeader = new byte[12];
        rnd.NextBytes(encryptionHeader);
        IACSharpSensor.IACSharpSensor.SensorReached(2914);
        if ((this._BitField & 0x8) == 0x8) {
          IACSharpSensor.IACSharpSensor.SensorReached(2915);
          _TimeBlob = Ionic.Zip.SharedUtilities.DateTimeToPacked(LastModified);
          encryptionHeader[11] = (byte)((this._TimeBlob >> 8) & 0xff);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2916);
          FigureCrc32();
          encryptionHeader[11] = (byte)((this._Crc32 >> 24) & 0xff);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2917);
        byte[] cipherText = _zipCrypto_forWrite.EncryptMessage(encryptionHeader, encryptionHeader.Length);
        outstream.Write(cipherText, 0, cipherText.Length);
        _LengthOfHeader += cipherText.Length;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2918);
    }
    private void CopyThroughOneEntry(Stream outStream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2919);
      if (this.LengthOfHeader == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2920);
        throw new BadStateException("Bad header length.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2921);
      bool needRecompute = _metadataChanged || (this.ArchiveStream is ZipSegmentedStream) || (outStream is ZipSegmentedStream) || (_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Never) || (!_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Always);
      IACSharpSensor.IACSharpSensor.SensorReached(2922);
      if (needRecompute) {
        IACSharpSensor.IACSharpSensor.SensorReached(2923);
        CopyThroughWithRecompute(outStream);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2924);
        CopyThroughWithNoChange(outStream);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2925);
      _entryRequiresZip64 = new Nullable<bool>(_CompressedSize >= 0xffffffffu || _UncompressedSize >= 0xffffffffu || _RelativeOffsetOfLocalHeader >= 0xffffffffu);
      _OutputUsesZip64 = new Nullable<bool>(_container.Zip64 == Zip64Option.Always || _entryRequiresZip64.Value);
      IACSharpSensor.IACSharpSensor.SensorReached(2926);
    }
    private void CopyThroughWithRecompute(Stream outstream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2927);
      int n;
      byte[] bytes = new byte[BufferSize];
      var input = new CountingStream(this.ArchiveStream);
      long origRelativeOffsetOfHeader = _RelativeOffsetOfLocalHeader;
      int origLengthOfHeader = LengthOfHeader;
      WriteHeader(outstream, 0);
      StoreRelativeOffset();
      IACSharpSensor.IACSharpSensor.SensorReached(2928);
      if (!this.FileName.EndsWith("/")) {
        IACSharpSensor.IACSharpSensor.SensorReached(2929);
        long pos = origRelativeOffsetOfHeader + origLengthOfHeader;
        int len = GetLengthOfCryptoHeaderBytes(_Encryption_FromZipFile);
        pos -= len;
        _LengthOfHeader += len;
        input.Seek(pos, SeekOrigin.Begin);
        long remaining = this._CompressedSize;
        IACSharpSensor.IACSharpSensor.SensorReached(2930);
        while (remaining > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(2931);
          len = (remaining > bytes.Length) ? bytes.Length : (int)remaining;
          n = input.Read(bytes, 0, len);
          outstream.Write(bytes, 0, n);
          remaining -= n;
          OnWriteBlock(input.BytesRead, this._CompressedSize);
          IACSharpSensor.IACSharpSensor.SensorReached(2932);
          if (_ioOperationCanceled) {
            IACSharpSensor.IACSharpSensor.SensorReached(2933);
            break;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2934);
        if ((this._BitField & 0x8) == 0x8) {
          IACSharpSensor.IACSharpSensor.SensorReached(2935);
          int size = 16;
          IACSharpSensor.IACSharpSensor.SensorReached(2936);
          if (_InputUsesZip64) {
            IACSharpSensor.IACSharpSensor.SensorReached(2937);
            size += 8;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2938);
          byte[] Descriptor = new byte[size];
          input.Read(Descriptor, 0, size);
          IACSharpSensor.IACSharpSensor.SensorReached(2939);
          if (_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Never) {
            IACSharpSensor.IACSharpSensor.SensorReached(2940);
            outstream.Write(Descriptor, 0, 8);
            IACSharpSensor.IACSharpSensor.SensorReached(2941);
            if (_CompressedSize > 0xffffffffu) {
              IACSharpSensor.IACSharpSensor.SensorReached(2942);
              throw new InvalidOperationException("ZIP64 is required");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2943);
            outstream.Write(Descriptor, 8, 4);
            IACSharpSensor.IACSharpSensor.SensorReached(2944);
            if (_UncompressedSize > 0xffffffffu) {
              IACSharpSensor.IACSharpSensor.SensorReached(2945);
              throw new InvalidOperationException("ZIP64 is required");
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2946);
            outstream.Write(Descriptor, 16, 4);
            _LengthOfTrailer -= 8;
          } else if (!_InputUsesZip64 && _container.UseZip64WhenSaving == Zip64Option.Always) {
            IACSharpSensor.IACSharpSensor.SensorReached(2948);
            byte[] pad = new byte[4];
            outstream.Write(Descriptor, 0, 8);
            outstream.Write(Descriptor, 8, 4);
            outstream.Write(pad, 0, 4);
            outstream.Write(Descriptor, 12, 4);
            outstream.Write(pad, 0, 4);
            _LengthOfTrailer += 8;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2947);
            outstream.Write(Descriptor, 0, size);
          }
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2949);
      _TotalEntrySize = _LengthOfHeader + _CompressedFileDataSize + _LengthOfTrailer;
      IACSharpSensor.IACSharpSensor.SensorReached(2950);
    }
    private void CopyThroughWithNoChange(Stream outstream)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2951);
      int n;
      byte[] bytes = new byte[BufferSize];
      var input = new CountingStream(this.ArchiveStream);
      input.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
      IACSharpSensor.IACSharpSensor.SensorReached(2952);
      if (this._TotalEntrySize == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2953);
        this._TotalEntrySize = this._LengthOfHeader + this._CompressedFileDataSize + _LengthOfTrailer;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2954);
      var counter = outstream as CountingStream;
      _RelativeOffsetOfLocalHeader = (counter != null) ? counter.ComputedPosition : outstream.Position;
      long remaining = this._TotalEntrySize;
      IACSharpSensor.IACSharpSensor.SensorReached(2955);
      while (remaining > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2956);
        int len = (remaining > bytes.Length) ? bytes.Length : (int)remaining;
        n = input.Read(bytes, 0, len);
        outstream.Write(bytes, 0, n);
        remaining -= n;
        OnWriteBlock(input.BytesRead, this._TotalEntrySize);
        IACSharpSensor.IACSharpSensor.SensorReached(2957);
        if (_ioOperationCanceled) {
          IACSharpSensor.IACSharpSensor.SensorReached(2958);
          break;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2959);
    }
    [System.Diagnostics.ConditionalAttribute("Trace")]
    private void TraceWriteLine(string format, params object[] varParams)
    {
      lock (_outputLock) {
        IACSharpSensor.IACSharpSensor.SensorReached(2960);
        int tid = System.Threading.Thread.CurrentThread.GetHashCode();
        Console.ForegroundColor = (ConsoleColor)(tid % 8 + 8);
        Console.Write("{0:000} ZipEntry.Write ", tid);
        Console.WriteLine(format, varParams);
        Console.ResetColor();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2961);
    }
    private object _outputLock = new Object();
  }
}
