using System;
using System.IO;
namespace Ionic.Zip
{
  public partial class ZipEntry
  {
    private int _readExtraDepth;
    private void ReadExtraField()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2387);
      _readExtraDepth++;
      long posn = this.ArchiveStream.Position;
      this.ArchiveStream.Seek(this._RelativeOffsetOfLocalHeader, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      byte[] block = new byte[30];
      this.ArchiveStream.Read(block, 0, block.Length);
      int i = 26;
      Int16 filenameLength = (short)(block[i++] + block[i++] * 256);
      Int16 extraFieldLength = (short)(block[i++] + block[i++] * 256);
      this.ArchiveStream.Seek(filenameLength, SeekOrigin.Current);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      ProcessExtraField(this.ArchiveStream, extraFieldLength);
      this.ArchiveStream.Seek(posn, SeekOrigin.Begin);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(this.ArchiveStream);
      _readExtraDepth--;
      IACSharpSensor.IACSharpSensor.SensorReached(2388);
    }
    private static bool ReadHeader(ZipEntry ze, System.Text.Encoding defaultEncoding)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2389);
      int bytesRead = 0;
      ze._RelativeOffsetOfLocalHeader = ze.ArchiveStream.Position;
      int signature = Ionic.Zip.SharedUtilities.ReadEntrySignature(ze.ArchiveStream);
      bytesRead += 4;
      IACSharpSensor.IACSharpSensor.SensorReached(2390);
      if (ZipEntry.IsNotValidSig(signature)) {
        IACSharpSensor.IACSharpSensor.SensorReached(2391);
        ze.ArchiveStream.Seek(-4, SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(ze.ArchiveStream);
        IACSharpSensor.IACSharpSensor.SensorReached(2392);
        if (ZipEntry.IsNotValidZipDirEntrySig(signature) && (signature != ZipConstants.EndOfCentralDirectorySignature)) {
          IACSharpSensor.IACSharpSensor.SensorReached(2393);
          throw new BadReadException(String.Format("  Bad signature (0x{0:X8}) at position  0x{1:X8}", signature, ze.ArchiveStream.Position));
        }
        System.Boolean RNTRNTRNT_251 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2394);
        return RNTRNTRNT_251;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2395);
      byte[] block = new byte[26];
      int n = ze.ArchiveStream.Read(block, 0, block.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(2396);
      if (n != block.Length) {
        System.Boolean RNTRNTRNT_252 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(2397);
        return RNTRNTRNT_252;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2398);
      bytesRead += n;
      int i = 0;
      ze._VersionNeeded = (Int16)(block[i++] + block[i++] * 256);
      ze._BitField = (Int16)(block[i++] + block[i++] * 256);
      ze._CompressionMethod_FromZipFile = ze._CompressionMethod = (Int16)(block[i++] + block[i++] * 256);
      ze._TimeBlob = block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256;
      ze._LastModified = Ionic.Zip.SharedUtilities.PackedToDateTime(ze._TimeBlob);
      ze._timestamp |= ZipEntryTimestamp.DOS;
      IACSharpSensor.IACSharpSensor.SensorReached(2399);
      if ((ze._BitField & 0x1) == 0x1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2400);
        ze._Encryption_FromZipFile = ze._Encryption = EncryptionAlgorithm.PkzipWeak;
        ze._sourceIsEncrypted = true;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2404);
      {
        IACSharpSensor.IACSharpSensor.SensorReached(2401);
        ze._Crc32 = (Int32)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
        ze._CompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
        ze._UncompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
        IACSharpSensor.IACSharpSensor.SensorReached(2402);
        if ((uint)ze._CompressedSize == 0xffffffffu || (uint)ze._UncompressedSize == 0xffffffffu) {
          IACSharpSensor.IACSharpSensor.SensorReached(2403);
          ze._InputUsesZip64 = true;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2405);
      Int16 filenameLength = (short)(block[i++] + block[i++] * 256);
      Int16 extraFieldLength = (short)(block[i++] + block[i++] * 256);
      block = new byte[filenameLength];
      n = ze.ArchiveStream.Read(block, 0, block.Length);
      bytesRead += n;
      IACSharpSensor.IACSharpSensor.SensorReached(2406);
      if ((ze._BitField & 0x800) == 0x800) {
        IACSharpSensor.IACSharpSensor.SensorReached(2407);
        ze.AlternateEncoding = System.Text.Encoding.UTF8;
        ze.AlternateEncodingUsage = ZipOption.Always;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2408);
      ze._FileNameInArchive = ze.AlternateEncoding.GetString(block, 0, block.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(2409);
      if (ze._FileNameInArchive.EndsWith("/")) {
        IACSharpSensor.IACSharpSensor.SensorReached(2410);
        ze.MarkAsDirectory();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2411);
      bytesRead += ze.ProcessExtraField(ze.ArchiveStream, extraFieldLength);
      ze._LengthOfTrailer = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(2412);
      if (!ze._FileNameInArchive.EndsWith("/") && (ze._BitField & 0x8) == 0x8) {
        IACSharpSensor.IACSharpSensor.SensorReached(2413);
        long posn = ze.ArchiveStream.Position;
        bool wantMore = true;
        long SizeOfDataRead = 0;
        int tries = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(2414);
        while (wantMore) {
          IACSharpSensor.IACSharpSensor.SensorReached(2415);
          tries++;
          IACSharpSensor.IACSharpSensor.SensorReached(2416);
          if (ze._container.ZipFile != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(2417);
            ze._container.ZipFile.OnReadBytes(ze);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2418);
          long d = Ionic.Zip.SharedUtilities.FindSignature(ze.ArchiveStream, ZipConstants.ZipEntryDataDescriptorSignature);
          IACSharpSensor.IACSharpSensor.SensorReached(2419);
          if (d == -1) {
            System.Boolean RNTRNTRNT_253 = false;
            IACSharpSensor.IACSharpSensor.SensorReached(2420);
            return RNTRNTRNT_253;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2421);
          SizeOfDataRead += d;
          IACSharpSensor.IACSharpSensor.SensorReached(2422);
          if (ze._InputUsesZip64) {
            IACSharpSensor.IACSharpSensor.SensorReached(2423);
            block = new byte[20];
            n = ze.ArchiveStream.Read(block, 0, block.Length);
            IACSharpSensor.IACSharpSensor.SensorReached(2424);
            if (n != 20) {
              System.Boolean RNTRNTRNT_254 = false;
              IACSharpSensor.IACSharpSensor.SensorReached(2425);
              return RNTRNTRNT_254;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2426);
            i = 0;
            ze._Crc32 = (Int32)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
            ze._CompressedSize = BitConverter.ToInt64(block, i);
            i += 8;
            ze._UncompressedSize = BitConverter.ToInt64(block, i);
            i += 8;
            ze._LengthOfTrailer += 24;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2427);
            block = new byte[12];
            n = ze.ArchiveStream.Read(block, 0, block.Length);
            IACSharpSensor.IACSharpSensor.SensorReached(2428);
            if (n != 12) {
              System.Boolean RNTRNTRNT_255 = false;
              IACSharpSensor.IACSharpSensor.SensorReached(2429);
              return RNTRNTRNT_255;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(2430);
            i = 0;
            ze._Crc32 = (Int32)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
            ze._CompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
            ze._UncompressedSize = (uint)(block[i++] + block[i++] * 256 + block[i++] * 256 * 256 + block[i++] * 256 * 256 * 256);
            ze._LengthOfTrailer += 16;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2431);
          wantMore = (SizeOfDataRead != ze._CompressedSize);
          IACSharpSensor.IACSharpSensor.SensorReached(2432);
          if (wantMore) {
            IACSharpSensor.IACSharpSensor.SensorReached(2433);
            ze.ArchiveStream.Seek(-12, SeekOrigin.Current);
            Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(ze.ArchiveStream);
            SizeOfDataRead += 4;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2434);
        ze.ArchiveStream.Seek(posn, SeekOrigin.Begin);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(ze.ArchiveStream);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2435);
      ze._CompressedFileDataSize = ze._CompressedSize;
      IACSharpSensor.IACSharpSensor.SensorReached(2436);
      if ((ze._BitField & 0x1) == 0x1) {
        IACSharpSensor.IACSharpSensor.SensorReached(2438);
        {
          IACSharpSensor.IACSharpSensor.SensorReached(2437);
          ze._WeakEncryptionHeader = new byte[12];
          bytesRead += ZipEntry.ReadWeakEncryptionHeader(ze._archiveStream, ze._WeakEncryptionHeader);
          ze._CompressedFileDataSize -= 12;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2439);
      ze._LengthOfHeader = bytesRead;
      ze._TotalEntrySize = ze._LengthOfHeader + ze._CompressedFileDataSize + ze._LengthOfTrailer;
      System.Boolean RNTRNTRNT_256 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(2440);
      return RNTRNTRNT_256;
    }
    static internal int ReadWeakEncryptionHeader(Stream s, byte[] buffer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2441);
      int additionalBytesRead = s.Read(buffer, 0, 12);
      IACSharpSensor.IACSharpSensor.SensorReached(2442);
      if (additionalBytesRead != 12) {
        IACSharpSensor.IACSharpSensor.SensorReached(2443);
        throw new ZipException(String.Format("Unexpected end of data at position 0x{0:X8}", s.Position));
      }
      System.Int32 RNTRNTRNT_257 = additionalBytesRead;
      IACSharpSensor.IACSharpSensor.SensorReached(2444);
      return RNTRNTRNT_257;
    }
    private static bool IsNotValidSig(int signature)
    {
      System.Boolean RNTRNTRNT_258 = (signature != ZipConstants.ZipEntrySignature);
      IACSharpSensor.IACSharpSensor.SensorReached(2445);
      return RNTRNTRNT_258;
    }
    static internal ZipEntry ReadEntry(ZipContainer zc, bool first)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2446);
      ZipFile zf = zc.ZipFile;
      Stream s = zc.ReadStream;
      System.Text.Encoding defaultEncoding = zc.AlternateEncoding;
      ZipEntry entry = new ZipEntry();
      entry._Source = ZipEntrySource.ZipFile;
      entry._container = zc;
      entry._archiveStream = s;
      IACSharpSensor.IACSharpSensor.SensorReached(2447);
      if (zf != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2448);
        zf.OnReadEntry(true, null);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2449);
      if (first) {
        IACSharpSensor.IACSharpSensor.SensorReached(2450);
        HandlePK00Prefix(s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2451);
      if (!ReadHeader(entry, defaultEncoding)) {
        IACSharpSensor.IACSharpSensor.SensorReached(2452);
        return null;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2453);
      entry.__FileDataPosition = entry.ArchiveStream.Position;
      s.Seek(entry._CompressedFileDataSize + entry._LengthOfTrailer, SeekOrigin.Current);
      Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
      HandleUnexpectedDataDescriptor(entry);
      IACSharpSensor.IACSharpSensor.SensorReached(2454);
      if (zf != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(2455);
        zf.OnReadBytes(entry);
        zf.OnReadEntry(false, entry);
      }
      ZipEntry RNTRNTRNT_259 = entry;
      IACSharpSensor.IACSharpSensor.SensorReached(2456);
      return RNTRNTRNT_259;
    }
    static internal void HandlePK00Prefix(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2457);
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      IACSharpSensor.IACSharpSensor.SensorReached(2458);
      if (datum != ZipConstants.PackedToRemovableMedia) {
        IACSharpSensor.IACSharpSensor.SensorReached(2459);
        s.Seek(-4, SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2460);
    }
    private static void HandleUnexpectedDataDescriptor(ZipEntry entry)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2461);
      Stream s = entry.ArchiveStream;
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      IACSharpSensor.IACSharpSensor.SensorReached(2462);
      if (datum == entry._Crc32) {
        IACSharpSensor.IACSharpSensor.SensorReached(2463);
        int sz = Ionic.Zip.SharedUtilities.ReadInt(s);
        IACSharpSensor.IACSharpSensor.SensorReached(2464);
        if (sz == entry._CompressedSize) {
          IACSharpSensor.IACSharpSensor.SensorReached(2465);
          sz = Ionic.Zip.SharedUtilities.ReadInt(s);
          IACSharpSensor.IACSharpSensor.SensorReached(2466);
          if (sz == entry._UncompressedSize) {
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(2467);
            s.Seek(-12, SeekOrigin.Current);
            Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
          }
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(2468);
          s.Seek(-8, SeekOrigin.Current);
          Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
        }
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2469);
        s.Seek(-4, SeekOrigin.Current);
        Ionic.Zip.SharedUtilities.Workaround_Ladybug318918(s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2470);
    }
    static internal int FindExtraFieldSegment(byte[] extra, int offx, UInt16 targetHeaderId)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2471);
      int j = offx;
      IACSharpSensor.IACSharpSensor.SensorReached(2472);
      while (j + 3 < extra.Length) {
        IACSharpSensor.IACSharpSensor.SensorReached(2473);
        UInt16 headerId = (UInt16)(extra[j++] + extra[j++] * 256);
        IACSharpSensor.IACSharpSensor.SensorReached(2474);
        if (headerId == targetHeaderId) {
          System.Int32 RNTRNTRNT_260 = j - 2;
          IACSharpSensor.IACSharpSensor.SensorReached(2475);
          return RNTRNTRNT_260;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2476);
        Int16 dataSize = (short)(extra[j++] + extra[j++] * 256);
        j += dataSize;
      }
      System.Int32 RNTRNTRNT_261 = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(2477);
      return RNTRNTRNT_261;
    }
    internal int ProcessExtraField(Stream s, Int16 extraFieldLength)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2478);
      int additionalBytesRead = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(2479);
      if (extraFieldLength > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2480);
        byte[] buffer = this._Extra = new byte[extraFieldLength];
        additionalBytesRead = s.Read(buffer, 0, buffer.Length);
        long posn = s.Position - additionalBytesRead;
        int j = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(2481);
        while (j + 3 < buffer.Length) {
          IACSharpSensor.IACSharpSensor.SensorReached(2482);
          int start = j;
          UInt16 headerId = (UInt16)(buffer[j++] + buffer[j++] * 256);
          Int16 dataSize = (short)(buffer[j++] + buffer[j++] * 256);
          IACSharpSensor.IACSharpSensor.SensorReached(2483);
          switch (headerId) {
            case 0xa:
              IACSharpSensor.IACSharpSensor.SensorReached(2484);
              j = ProcessExtraFieldWindowsTimes(buffer, j, dataSize, posn);
              IACSharpSensor.IACSharpSensor.SensorReached(2485);
              break;
            case 0x5455:
              IACSharpSensor.IACSharpSensor.SensorReached(2486);
              j = ProcessExtraFieldUnixTimes(buffer, j, dataSize, posn);
              IACSharpSensor.IACSharpSensor.SensorReached(2487);
              break;
            case 0x5855:
              IACSharpSensor.IACSharpSensor.SensorReached(2488);
              j = ProcessExtraFieldInfoZipTimes(buffer, j, dataSize, posn);
              IACSharpSensor.IACSharpSensor.SensorReached(2489);
              break;
            case 0x7855:
              IACSharpSensor.IACSharpSensor.SensorReached(2490);
              break;
            case 0x7875:
              IACSharpSensor.IACSharpSensor.SensorReached(2491);
              break;
            case 0x1:
              IACSharpSensor.IACSharpSensor.SensorReached(2492);
              j = ProcessExtraFieldZip64(buffer, j, dataSize, posn);
              IACSharpSensor.IACSharpSensor.SensorReached(2493);
              break;
            case 0x17:
              IACSharpSensor.IACSharpSensor.SensorReached(2494);
              j = ProcessExtraFieldPkwareStrongEncryption(buffer, j);
              IACSharpSensor.IACSharpSensor.SensorReached(2495);
              break;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(2496);
          j = start + dataSize + 4;
        }
      }
      System.Int32 RNTRNTRNT_262 = additionalBytesRead;
      IACSharpSensor.IACSharpSensor.SensorReached(2497);
      return RNTRNTRNT_262;
    }
    private int ProcessExtraFieldPkwareStrongEncryption(byte[] Buffer, int j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2498);
      j += 2;
      _UnsupportedAlgorithmId = (UInt16)(Buffer[j++] + Buffer[j++] * 256);
      _Encryption_FromZipFile = _Encryption = EncryptionAlgorithm.Unsupported;
      System.Int32 RNTRNTRNT_263 = j;
      IACSharpSensor.IACSharpSensor.SensorReached(2499);
      return RNTRNTRNT_263;
    }
    private delegate T Func<T>();
    private int ProcessExtraFieldZip64(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2500);
      this._InputUsesZip64 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(2501);
      if (dataSize > 28) {
        IACSharpSensor.IACSharpSensor.SensorReached(2502);
        throw new BadReadException(String.Format("  Inconsistent size (0x{0:X4}) for ZIP64 extra field at position 0x{1:X16}", dataSize, posn));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2503);
      int remainingData = dataSize;
      var slurp = new Func<Int64>(() =>
      {
        if (remainingData < 8)
          throw new BadReadException(String.Format("  Missing data for ZIP64 extra field, position 0x{0:X16}", posn));
        var x = BitConverter.ToInt64(buffer, j);
        j += 8;
        remainingData -= 8;
        return x;
      });
      IACSharpSensor.IACSharpSensor.SensorReached(2504);
      if (this._UncompressedSize == 0xffffffffu) {
        IACSharpSensor.IACSharpSensor.SensorReached(2505);
        this._UncompressedSize = slurp();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2506);
      if (this._CompressedSize == 0xffffffffu) {
        IACSharpSensor.IACSharpSensor.SensorReached(2507);
        this._CompressedSize = slurp();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2508);
      if (this._RelativeOffsetOfLocalHeader == 0xffffffffu) {
        IACSharpSensor.IACSharpSensor.SensorReached(2509);
        this._RelativeOffsetOfLocalHeader = slurp();
      }
      System.Int32 RNTRNTRNT_264 = j;
      IACSharpSensor.IACSharpSensor.SensorReached(2510);
      return RNTRNTRNT_264;
    }
    private int ProcessExtraFieldInfoZipTimes(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2511);
      if (dataSize != 12 && dataSize != 8) {
        IACSharpSensor.IACSharpSensor.SensorReached(2512);
        throw new BadReadException(String.Format("  Unexpected size (0x{0:X4}) for InfoZip v1 extra field at position 0x{1:X16}", dataSize, posn));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2513);
      Int32 timet = BitConverter.ToInt32(buffer, j);
      this._Mtime = _unixEpoch.AddSeconds(timet);
      j += 4;
      timet = BitConverter.ToInt32(buffer, j);
      this._Atime = _unixEpoch.AddSeconds(timet);
      j += 4;
      this._Ctime = DateTime.UtcNow;
      _ntfsTimesAreSet = true;
      _timestamp |= ZipEntryTimestamp.InfoZip1;
      System.Int32 RNTRNTRNT_265 = j;
      IACSharpSensor.IACSharpSensor.SensorReached(2514);
      return RNTRNTRNT_265;
    }
    private int ProcessExtraFieldUnixTimes(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2515);
      if (dataSize != 13 && dataSize != 9 && dataSize != 5) {
        IACSharpSensor.IACSharpSensor.SensorReached(2516);
        throw new BadReadException(String.Format("  Unexpected size (0x{0:X4}) for Extended Timestamp extra field at position 0x{1:X16}", dataSize, posn));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2517);
      int remainingData = dataSize;
      var slurp = new Func<DateTime>(() =>
      {
        Int32 timet = BitConverter.ToInt32(buffer, j);
        j += 4;
        remainingData -= 4;
        return _unixEpoch.AddSeconds(timet);
      });
      IACSharpSensor.IACSharpSensor.SensorReached(2518);
      if (dataSize == 13 || _readExtraDepth > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(2519);
        byte flag = buffer[j++];
        remainingData--;
        IACSharpSensor.IACSharpSensor.SensorReached(2520);
        if ((flag & 0x1) != 0 && remainingData >= 4) {
          IACSharpSensor.IACSharpSensor.SensorReached(2521);
          this._Mtime = slurp();
        }
        IACSharpSensor.IACSharpSensor.SensorReached(2522);
        this._Atime = ((flag & 0x2) != 0 && remainingData >= 4) ? slurp() : DateTime.UtcNow;
        this._Ctime = ((flag & 0x4) != 0 && remainingData >= 4) ? slurp() : DateTime.UtcNow;
        _timestamp |= ZipEntryTimestamp.Unix;
        _ntfsTimesAreSet = true;
        _emitUnixTimes = true;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(2523);
        ReadExtraField();
      }
      System.Int32 RNTRNTRNT_266 = j;
      IACSharpSensor.IACSharpSensor.SensorReached(2524);
      return RNTRNTRNT_266;
    }
    private int ProcessExtraFieldWindowsTimes(byte[] buffer, int j, Int16 dataSize, long posn)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2525);
      if (dataSize != 32) {
        IACSharpSensor.IACSharpSensor.SensorReached(2526);
        throw new BadReadException(String.Format("  Unexpected size (0x{0:X4}) for NTFS times extra field at position 0x{1:X16}", dataSize, posn));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2527);
      j += 4;
      Int16 timetag = (Int16)(buffer[j] + buffer[j + 1] * 256);
      Int16 addlsize = (Int16)(buffer[j + 2] + buffer[j + 3] * 256);
      j += 4;
      IACSharpSensor.IACSharpSensor.SensorReached(2528);
      if (timetag == 0x1 && addlsize == 24) {
        IACSharpSensor.IACSharpSensor.SensorReached(2529);
        Int64 z = BitConverter.ToInt64(buffer, j);
        this._Mtime = DateTime.FromFileTimeUtc(z);
        j += 8;
        z = BitConverter.ToInt64(buffer, j);
        this._Atime = DateTime.FromFileTimeUtc(z);
        j += 8;
        z = BitConverter.ToInt64(buffer, j);
        this._Ctime = DateTime.FromFileTimeUtc(z);
        j += 8;
        _ntfsTimesAreSet = true;
        _timestamp |= ZipEntryTimestamp.Windows;
        _emitNtfsTimes = true;
      }
      System.Int32 RNTRNTRNT_267 = j;
      IACSharpSensor.IACSharpSensor.SensorReached(2530);
      return RNTRNTRNT_267;
    }
  }
}
