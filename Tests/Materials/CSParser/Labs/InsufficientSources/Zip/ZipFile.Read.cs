using System;
using System.IO;
using System.Collections.Generic;
namespace Ionic.Zip
{
  public class ReadOptions
  {
    public EventHandler<ReadProgressEventArgs> ReadProgress { get; set; }
    public TextWriter StatusMessageWriter { get; set; }
    public System.Text.Encoding Encoding { get; set; }
  }
  public partial class ZipFile
  {
    public static ZipFile Read(string fileName)
    {
      ZipFile RNTRNTRNT_377 = ZipFile.Read(fileName, null, null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(3526);
      return RNTRNTRNT_377;
    }
    public static ZipFile Read(string fileName, ReadOptions options)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3527);
      if (options == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3528);
        throw new ArgumentNullException("options");
      }
      ZipFile RNTRNTRNT_378 = Read(fileName, options.StatusMessageWriter, options.Encoding, options.ReadProgress);
      IACSharpSensor.IACSharpSensor.SensorReached(3529);
      return RNTRNTRNT_378;
    }
    private static ZipFile Read(string fileName, TextWriter statusMessageWriter, System.Text.Encoding encoding, EventHandler<ReadProgressEventArgs> readProgress)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3530);
      ZipFile zf = new ZipFile();
      zf.AlternateEncoding = encoding ?? DefaultEncoding;
      zf.AlternateEncodingUsage = ZipOption.Always;
      zf._StatusMessageTextWriter = statusMessageWriter;
      zf._name = fileName;
      IACSharpSensor.IACSharpSensor.SensorReached(3531);
      if (readProgress != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3532);
        zf.ReadProgress = readProgress;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3533);
      if (zf.Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(3534);
        zf._StatusMessageTextWriter.WriteLine("reading from {0}...", fileName);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3535);
      ReadIntoInstance(zf);
      zf._fileAlreadyExists = true;
      ZipFile RNTRNTRNT_379 = zf;
      IACSharpSensor.IACSharpSensor.SensorReached(3536);
      return RNTRNTRNT_379;
    }
    public static ZipFile Read(Stream zipStream)
    {
      ZipFile RNTRNTRNT_380 = Read(zipStream, null, null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(3537);
      return RNTRNTRNT_380;
    }
    public static ZipFile Read(Stream zipStream, ReadOptions options)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3538);
      if (options == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3539);
        throw new ArgumentNullException("options");
      }
      ZipFile RNTRNTRNT_381 = Read(zipStream, options.StatusMessageWriter, options.Encoding, options.ReadProgress);
      IACSharpSensor.IACSharpSensor.SensorReached(3540);
      return RNTRNTRNT_381;
    }
    private static ZipFile Read(Stream zipStream, TextWriter statusMessageWriter, System.Text.Encoding encoding, EventHandler<ReadProgressEventArgs> readProgress)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3541);
      if (zipStream == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3542);
        throw new ArgumentNullException("zipStream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3543);
      ZipFile zf = new ZipFile();
      zf._StatusMessageTextWriter = statusMessageWriter;
      zf._alternateEncoding = encoding ?? ZipFile.DefaultEncoding;
      zf._alternateEncodingUsage = ZipOption.Always;
      IACSharpSensor.IACSharpSensor.SensorReached(3544);
      if (readProgress != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3545);
        zf.ReadProgress += readProgress;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3546);
      zf._readstream = (zipStream.Position == 0L) ? zipStream : new OffsetStream(zipStream);
      zf._ReadStreamIsOurs = false;
      IACSharpSensor.IACSharpSensor.SensorReached(3547);
      if (zf.Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(3548);
        zf._StatusMessageTextWriter.WriteLine("reading from stream...");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3549);
      ReadIntoInstance(zf);
      ZipFile RNTRNTRNT_382 = zf;
      IACSharpSensor.IACSharpSensor.SensorReached(3550);
      return RNTRNTRNT_382;
    }
    private static void ReadIntoInstance(ZipFile zf)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3551);
      Stream s = zf.ReadStream;
      IACSharpSensor.IACSharpSensor.SensorReached(3552);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3553);
        zf._readName = zf._name;
        IACSharpSensor.IACSharpSensor.SensorReached(3554);
        if (!s.CanSeek) {
          IACSharpSensor.IACSharpSensor.SensorReached(3555);
          ReadIntoInstance_Orig(zf);
          IACSharpSensor.IACSharpSensor.SensorReached(3556);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3557);
        zf.OnReadStarted();
        uint datum = ReadFirstFourBytes(s);
        IACSharpSensor.IACSharpSensor.SensorReached(3558);
        if (datum == ZipConstants.EndOfCentralDirectorySignature) {
          IACSharpSensor.IACSharpSensor.SensorReached(3559);
          return;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3560);
        int nTries = 0;
        bool success = false;
        long posn = s.Length - 64;
        long maxSeekback = Math.Max(s.Length - 0x4000, 10);
        IACSharpSensor.IACSharpSensor.SensorReached(3561);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(3562);
          if (posn < 0) {
            IACSharpSensor.IACSharpSensor.SensorReached(3563);
            posn = 0;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3564);
          s.Seek(posn, SeekOrigin.Begin);
          long bytesRead = SharedUtilities.FindSignature(s, (int)ZipConstants.EndOfCentralDirectorySignature);
          IACSharpSensor.IACSharpSensor.SensorReached(3565);
          if (bytesRead != -1) {
            IACSharpSensor.IACSharpSensor.SensorReached(3566);
            success = true;
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(3567);
            if (posn == 0) {
              IACSharpSensor.IACSharpSensor.SensorReached(3568);
              break;
            }
            IACSharpSensor.IACSharpSensor.SensorReached(3569);
            nTries++;
            posn -= (32 * (nTries + 1) * nTries);
          }
        } while (!success && posn > maxSeekback);
        IACSharpSensor.IACSharpSensor.SensorReached(3570);
        if (success) {
          IACSharpSensor.IACSharpSensor.SensorReached(3571);
          zf._locEndOfCDS = s.Position - 4;
          byte[] block = new byte[16];
          s.Read(block, 0, block.Length);
          zf._diskNumberWithCd = BitConverter.ToUInt16(block, 2);
          IACSharpSensor.IACSharpSensor.SensorReached(3572);
          if (zf._diskNumberWithCd == 0xffff) {
            IACSharpSensor.IACSharpSensor.SensorReached(3573);
            throw new ZipException("Spanned archives with more than 65534 segments are not supported at this time.");
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3574);
          zf._diskNumberWithCd++;
          int i = 12;
          uint offset32 = (uint)BitConverter.ToUInt32(block, i);
          IACSharpSensor.IACSharpSensor.SensorReached(3575);
          if (offset32 == 0xffffffffu) {
            IACSharpSensor.IACSharpSensor.SensorReached(3576);
            Zip64SeekToCentralDirectory(zf);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(3577);
            zf._OffsetOfCentralDirectory = offset32;
            s.Seek(offset32, SeekOrigin.Begin);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3578);
          ReadCentralDirectory(zf);
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(3579);
          s.Seek(0L, SeekOrigin.Begin);
          ReadIntoInstance_Orig(zf);
        }
      } catch (Exception ex1) {
        IACSharpSensor.IACSharpSensor.SensorReached(3580);
        if (zf._ReadStreamIsOurs && zf._readstream != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3581);
          try {
            IACSharpSensor.IACSharpSensor.SensorReached(3582);
            zf._readstream.Dispose();
            zf._readstream = null;
          } finally {
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3583);
        throw new ZipException("Cannot read that as a ZipFile", ex1);
        IACSharpSensor.IACSharpSensor.SensorReached(3584);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3585);
      zf._contentsChanged = false;
      IACSharpSensor.IACSharpSensor.SensorReached(3586);
    }
    private static void Zip64SeekToCentralDirectory(ZipFile zf)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3587);
      Stream s = zf.ReadStream;
      byte[] block = new byte[16];
      s.Seek(-40, SeekOrigin.Current);
      s.Read(block, 0, 16);
      Int64 offset64 = BitConverter.ToInt64(block, 8);
      zf._OffsetOfCentralDirectory = 0xffffffffu;
      zf._OffsetOfCentralDirectory64 = offset64;
      s.Seek(offset64, SeekOrigin.Begin);
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      IACSharpSensor.IACSharpSensor.SensorReached(3588);
      if (datum != ZipConstants.Zip64EndOfCentralDirectoryRecordSignature) {
        IACSharpSensor.IACSharpSensor.SensorReached(3589);
        throw new BadReadException(String.Format("  Bad signature (0x{0:X8}) looking for ZIP64 EoCD Record at position 0x{1:X8}", datum, s.Position));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3590);
      s.Read(block, 0, 8);
      Int64 Size = BitConverter.ToInt64(block, 0);
      block = new byte[Size];
      s.Read(block, 0, block.Length);
      offset64 = BitConverter.ToInt64(block, 36);
      s.Seek(offset64, SeekOrigin.Begin);
      IACSharpSensor.IACSharpSensor.SensorReached(3591);
    }
    private static uint ReadFirstFourBytes(Stream s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3592);
      uint datum = (uint)Ionic.Zip.SharedUtilities.ReadInt(s);
      System.UInt32 RNTRNTRNT_383 = datum;
      IACSharpSensor.IACSharpSensor.SensorReached(3593);
      return RNTRNTRNT_383;
    }
    private static void ReadCentralDirectory(ZipFile zf)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3594);
      bool inputUsesZip64 = false;
      ZipEntry de;
      var previouslySeen = new Dictionary<String, object>();
      IACSharpSensor.IACSharpSensor.SensorReached(3595);
      while ((de = ZipEntry.ReadDirEntry(zf, previouslySeen)) != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3596);
        de.ResetDirEntry();
        zf.OnReadEntry(true, null);
        IACSharpSensor.IACSharpSensor.SensorReached(3597);
        if (zf.Verbose) {
          IACSharpSensor.IACSharpSensor.SensorReached(3598);
          zf.StatusMessageTextWriter.WriteLine("entry {0}", de.FileName);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3599);
        zf._entries.Add(de.FileName, de);
        IACSharpSensor.IACSharpSensor.SensorReached(3600);
        if (de._InputUsesZip64) {
          IACSharpSensor.IACSharpSensor.SensorReached(3601);
          inputUsesZip64 = true;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3602);
        previouslySeen.Add(de.FileName, null);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3603);
      if (inputUsesZip64) {
        IACSharpSensor.IACSharpSensor.SensorReached(3604);
        zf.UseZip64WhenSaving = Zip64Option.Always;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3605);
      if (zf._locEndOfCDS > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3606);
        zf.ReadStream.Seek(zf._locEndOfCDS, SeekOrigin.Begin);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3607);
      ReadCentralDirectoryFooter(zf);
      IACSharpSensor.IACSharpSensor.SensorReached(3608);
      if (zf.Verbose && !String.IsNullOrEmpty(zf.Comment)) {
        IACSharpSensor.IACSharpSensor.SensorReached(3609);
        zf.StatusMessageTextWriter.WriteLine("Zip file Comment: {0}", zf.Comment);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3610);
      if (zf.Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(3611);
        zf.StatusMessageTextWriter.WriteLine("read in {0} entries.", zf._entries.Count);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3612);
      zf.OnReadCompleted();
      IACSharpSensor.IACSharpSensor.SensorReached(3613);
    }
    private static void ReadIntoInstance_Orig(ZipFile zf)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3614);
      zf.OnReadStarted();
      zf._entries = new System.Collections.Generic.Dictionary<String, ZipEntry>();
      ZipEntry e;
      IACSharpSensor.IACSharpSensor.SensorReached(3615);
      if (zf.Verbose) {
        IACSharpSensor.IACSharpSensor.SensorReached(3616);
        if (zf.Name == null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3617);
          zf.StatusMessageTextWriter.WriteLine("Reading zip from stream...");
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(3618);
          zf.StatusMessageTextWriter.WriteLine("Reading zip {0}...", zf.Name);
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3619);
      bool firstEntry = true;
      ZipContainer zc = new ZipContainer(zf);
      IACSharpSensor.IACSharpSensor.SensorReached(3620);
      while ((e = ZipEntry.ReadEntry(zc, firstEntry)) != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3621);
        if (zf.Verbose) {
          IACSharpSensor.IACSharpSensor.SensorReached(3622);
          zf.StatusMessageTextWriter.WriteLine("  {0}", e.FileName);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3623);
        zf._entries.Add(e.FileName, e);
        firstEntry = false;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3624);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3625);
        ZipEntry de;
        var previouslySeen = new Dictionary<String, Object>();
        IACSharpSensor.IACSharpSensor.SensorReached(3626);
        while ((de = ZipEntry.ReadDirEntry(zf, previouslySeen)) != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(3627);
          ZipEntry e1 = zf._entries[de.FileName];
          IACSharpSensor.IACSharpSensor.SensorReached(3628);
          if (e1 != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(3629);
            e1._Comment = de.Comment;
            IACSharpSensor.IACSharpSensor.SensorReached(3630);
            if (de.IsDirectory) {
              IACSharpSensor.IACSharpSensor.SensorReached(3631);
              e1.MarkAsDirectory();
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(3632);
          previouslySeen.Add(de.FileName, null);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3633);
        if (zf._locEndOfCDS > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(3634);
          zf.ReadStream.Seek(zf._locEndOfCDS, SeekOrigin.Begin);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3635);
        ReadCentralDirectoryFooter(zf);
        IACSharpSensor.IACSharpSensor.SensorReached(3636);
        if (zf.Verbose && !String.IsNullOrEmpty(zf.Comment)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3637);
          zf.StatusMessageTextWriter.WriteLine("Zip file Comment: {0}", zf.Comment);
        }
      } catch (ZipException) {
      } catch (IOException) {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3638);
      zf.OnReadCompleted();
      IACSharpSensor.IACSharpSensor.SensorReached(3639);
    }
    private static void ReadCentralDirectoryFooter(ZipFile zf)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3640);
      Stream s = zf.ReadStream;
      int signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
      byte[] block = null;
      int j = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(3641);
      if (signature == ZipConstants.Zip64EndOfCentralDirectoryRecordSignature) {
        IACSharpSensor.IACSharpSensor.SensorReached(3642);
        block = new byte[8 + 44];
        s.Read(block, 0, block.Length);
        Int64 DataSize = BitConverter.ToInt64(block, 0);
        IACSharpSensor.IACSharpSensor.SensorReached(3643);
        if (DataSize < 44) {
          IACSharpSensor.IACSharpSensor.SensorReached(3644);
          throw new ZipException("Bad size in the ZIP64 Central Directory.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3645);
        zf._versionMadeBy = BitConverter.ToUInt16(block, j);
        j += 2;
        zf._versionNeededToExtract = BitConverter.ToUInt16(block, j);
        j += 2;
        zf._diskNumberWithCd = BitConverter.ToUInt32(block, j);
        j += 2;
        block = new byte[DataSize - 44];
        s.Read(block, 0, block.Length);
        signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
        IACSharpSensor.IACSharpSensor.SensorReached(3646);
        if (signature != ZipConstants.Zip64EndOfCentralDirectoryLocatorSignature) {
          IACSharpSensor.IACSharpSensor.SensorReached(3647);
          throw new ZipException("Inconsistent metadata in the ZIP64 Central Directory.");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3648);
        block = new byte[16];
        s.Read(block, 0, block.Length);
        signature = Ionic.Zip.SharedUtilities.ReadSignature(s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3649);
      if (signature != ZipConstants.EndOfCentralDirectorySignature) {
        IACSharpSensor.IACSharpSensor.SensorReached(3650);
        s.Seek(-4, SeekOrigin.Current);
        IACSharpSensor.IACSharpSensor.SensorReached(3651);
        throw new BadReadException(String.Format("Bad signature ({0:X8}) at position 0x{1:X8}", signature, s.Position));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3652);
      block = new byte[16];
      zf.ReadStream.Read(block, 0, block.Length);
      IACSharpSensor.IACSharpSensor.SensorReached(3653);
      if (zf._diskNumberWithCd == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3654);
        zf._diskNumberWithCd = BitConverter.ToUInt16(block, 2);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3655);
      ReadZipFileComment(zf);
      IACSharpSensor.IACSharpSensor.SensorReached(3656);
    }
    private static void ReadZipFileComment(ZipFile zf)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3657);
      byte[] block = new byte[2];
      zf.ReadStream.Read(block, 0, block.Length);
      Int16 commentLength = (short)(block[0] + block[1] * 256);
      IACSharpSensor.IACSharpSensor.SensorReached(3658);
      if (commentLength > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(3659);
        block = new byte[commentLength];
        zf.ReadStream.Read(block, 0, block.Length);
        string s1 = zf.AlternateEncoding.GetString(block, 0, block.Length);
        zf.Comment = s1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3660);
    }
    public static bool IsZipFile(string fileName)
    {
      System.Boolean RNTRNTRNT_384 = IsZipFile(fileName, false);
      IACSharpSensor.IACSharpSensor.SensorReached(3661);
      return RNTRNTRNT_384;
    }
    public static bool IsZipFile(string fileName, bool testExtract)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3662);
      bool result = false;
      IACSharpSensor.IACSharpSensor.SensorReached(3663);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3664);
        if (!File.Exists(fileName)) {
          System.Boolean RNTRNTRNT_385 = false;
          IACSharpSensor.IACSharpSensor.SensorReached(3665);
          return RNTRNTRNT_385;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3666);
        using (var s = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3667);
          result = IsZipFile(s, testExtract);
        }
      } catch (IOException) {
      } catch (ZipException) {
      }
      System.Boolean RNTRNTRNT_386 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(3668);
      return RNTRNTRNT_386;
    }
    public static bool IsZipFile(Stream stream, bool testExtract)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(3669);
      if (stream == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(3670);
        throw new ArgumentNullException("stream");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(3671);
      bool result = false;
      IACSharpSensor.IACSharpSensor.SensorReached(3672);
      try {
        IACSharpSensor.IACSharpSensor.SensorReached(3673);
        if (!stream.CanRead) {
          System.Boolean RNTRNTRNT_387 = false;
          IACSharpSensor.IACSharpSensor.SensorReached(3674);
          return RNTRNTRNT_387;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3675);
        var bitBucket = Stream.Null;
        IACSharpSensor.IACSharpSensor.SensorReached(3676);
        using (ZipFile zip1 = ZipFile.Read(stream, null, null, null)) {
          IACSharpSensor.IACSharpSensor.SensorReached(3677);
          if (testExtract) {
            IACSharpSensor.IACSharpSensor.SensorReached(3678);
            foreach (var e in zip1) {
              IACSharpSensor.IACSharpSensor.SensorReached(3679);
              if (!e.IsDirectory) {
                IACSharpSensor.IACSharpSensor.SensorReached(3680);
                e.Extract(bitBucket);
              }
            }
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(3681);
        result = true;
      } catch (IOException) {
      } catch (ZipException) {
      }
      System.Boolean RNTRNTRNT_388 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(3682);
      return RNTRNTRNT_388;
    }
  }
}
