using System;
using Interop = System.Runtime.InteropServices;
namespace Ionic.Crc
{
  [Interop.GuidAttribute("ebc25cf6-9120-4283-b972-0e5520d0000C")]
  [Interop.ComVisible(true)]
  [Interop.ClassInterface(Interop.ClassInterfaceType.AutoDispatch)]
  public class CRC32
  {
    public Int64 TotalBytesRead {
      get {
        Int64 RNTRNTRNT_45 = _TotalBytesRead;
        IACSharpSensor.IACSharpSensor.SensorReached(1035);
        return RNTRNTRNT_45;
      }
    }
    public Int32 Crc32Result {
      get {
        Int32 RNTRNTRNT_46 = unchecked((Int32)(~_register));
        IACSharpSensor.IACSharpSensor.SensorReached(1036);
        return RNTRNTRNT_46;
      }
    }
    public Int32 GetCrc32(System.IO.Stream input)
    {
      Int32 RNTRNTRNT_47 = GetCrc32AndCopy(input, null);
      IACSharpSensor.IACSharpSensor.SensorReached(1037);
      return RNTRNTRNT_47;
    }
    public Int32 GetCrc32AndCopy(System.IO.Stream input, System.IO.Stream output)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1038);
      if (input == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1039);
        throw new Exception("The input stream must not be null.");
      }
      unchecked {
        IACSharpSensor.IACSharpSensor.SensorReached(1040);
        byte[] buffer = new byte[BUFFER_SIZE];
        int readSize = BUFFER_SIZE;
        _TotalBytesRead = 0;
        int count = input.Read(buffer, 0, readSize);
        IACSharpSensor.IACSharpSensor.SensorReached(1041);
        if (output != null) {
          IACSharpSensor.IACSharpSensor.SensorReached(1042);
          output.Write(buffer, 0, count);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1043);
        _TotalBytesRead += count;
        IACSharpSensor.IACSharpSensor.SensorReached(1044);
        while (count > 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(1045);
          SlurpBlock(buffer, 0, count);
          count = input.Read(buffer, 0, readSize);
          IACSharpSensor.IACSharpSensor.SensorReached(1046);
          if (output != null) {
            IACSharpSensor.IACSharpSensor.SensorReached(1047);
            output.Write(buffer, 0, count);
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1048);
          _TotalBytesRead += count;
        }
        Int32 RNTRNTRNT_48 = (Int32)(~_register);
        IACSharpSensor.IACSharpSensor.SensorReached(1049);
        return RNTRNTRNT_48;
      }
    }
    public Int32 ComputeCrc32(Int32 W, byte B)
    {
      Int32 RNTRNTRNT_49 = _InternalComputeCrc32((UInt32)W, B);
      IACSharpSensor.IACSharpSensor.SensorReached(1050);
      return RNTRNTRNT_49;
    }
    internal Int32 _InternalComputeCrc32(UInt32 W, byte B)
    {
      Int32 RNTRNTRNT_50 = (Int32)(crc32Table[(W ^ B) & 0xff] ^ (W >> 8));
      IACSharpSensor.IACSharpSensor.SensorReached(1051);
      return RNTRNTRNT_50;
    }
    public void SlurpBlock(byte[] block, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1052);
      if (block == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(1053);
        throw new Exception("The data buffer must not be null.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1054);
      for (int i = 0; i < count; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1055);
        int x = offset + i;
        byte b = block[x];
        IACSharpSensor.IACSharpSensor.SensorReached(1056);
        if (this.reverseBits) {
          IACSharpSensor.IACSharpSensor.SensorReached(1057);
          UInt32 temp = (_register >> 24) ^ b;
          _register = (_register << 8) ^ crc32Table[temp];
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1058);
          UInt32 temp = (_register & 0xff) ^ b;
          _register = (_register >> 8) ^ crc32Table[temp];
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1059);
      _TotalBytesRead += count;
      IACSharpSensor.IACSharpSensor.SensorReached(1060);
    }
    public void UpdateCRC(byte b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1061);
      if (this.reverseBits) {
        IACSharpSensor.IACSharpSensor.SensorReached(1062);
        UInt32 temp = (_register >> 24) ^ b;
        _register = (_register << 8) ^ crc32Table[temp];
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(1063);
        UInt32 temp = (_register & 0xff) ^ b;
        _register = (_register >> 8) ^ crc32Table[temp];
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1064);
    }
    public void UpdateCRC(byte b, int n)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1065);
      while (n-- > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1066);
        if (this.reverseBits) {
          IACSharpSensor.IACSharpSensor.SensorReached(1067);
          uint temp = (_register >> 24) ^ b;
          _register = (_register << 8) ^ crc32Table[(temp >= 0) ? temp : (temp + 256)];
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(1068);
          UInt32 temp = (_register & 0xff) ^ b;
          _register = (_register >> 8) ^ crc32Table[(temp >= 0) ? temp : (temp + 256)];
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1069);
    }
    private static uint ReverseBits(uint data)
    {
      unchecked {
        IACSharpSensor.IACSharpSensor.SensorReached(1070);
        uint ret = data;
        ret = (ret & 0x55555555) << 1 | (ret >> 1) & 0x55555555;
        ret = (ret & 0x33333333) << 2 | (ret >> 2) & 0x33333333;
        ret = (ret & 0xf0f0f0f) << 4 | (ret >> 4) & 0xf0f0f0f;
        ret = (ret << 24) | ((ret & 0xff00) << 8) | ((ret >> 8) & 0xff00) | (ret >> 24);
        System.UInt32 RNTRNTRNT_51 = ret;
        IACSharpSensor.IACSharpSensor.SensorReached(1071);
        return RNTRNTRNT_51;
      }
    }
    private static byte ReverseBits(byte data)
    {
      unchecked {
        IACSharpSensor.IACSharpSensor.SensorReached(1072);
        uint u = (uint)data * 0x20202;
        uint m = 0x1044010;
        uint s = u & m;
        uint t = (u << 2) & (m << 1);
        System.Byte RNTRNTRNT_52 = (byte)((0x1001001 * (s + t)) >> 24);
        IACSharpSensor.IACSharpSensor.SensorReached(1073);
        return RNTRNTRNT_52;
      }
    }
    private void GenerateLookupTable()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1074);
      crc32Table = new UInt32[256];
      unchecked {
        IACSharpSensor.IACSharpSensor.SensorReached(1075);
        UInt32 dwCrc;
        byte i = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(1076);
        do {
          IACSharpSensor.IACSharpSensor.SensorReached(1077);
          dwCrc = i;
          IACSharpSensor.IACSharpSensor.SensorReached(1078);
          for (byte j = 8; j > 0; j--) {
            IACSharpSensor.IACSharpSensor.SensorReached(1079);
            if ((dwCrc & 1) == 1) {
              IACSharpSensor.IACSharpSensor.SensorReached(1080);
              dwCrc = (dwCrc >> 1) ^ dwPolynomial;
            } else {
              IACSharpSensor.IACSharpSensor.SensorReached(1081);
              dwCrc >>= 1;
            }
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1082);
          if (reverseBits) {
            IACSharpSensor.IACSharpSensor.SensorReached(1083);
            crc32Table[ReverseBits(i)] = ReverseBits(dwCrc);
          } else {
            IACSharpSensor.IACSharpSensor.SensorReached(1084);
            crc32Table[i] = dwCrc;
          }
          IACSharpSensor.IACSharpSensor.SensorReached(1085);
          i++;
        } while (i != 0);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1086);
    }
    private uint gf2_matrix_times(uint[] matrix, uint vec)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1087);
      uint sum = 0;
      int i = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1088);
      while (vec != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1089);
        if ((vec & 0x1) == 0x1) {
          IACSharpSensor.IACSharpSensor.SensorReached(1090);
          sum ^= matrix[i];
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1091);
        vec >>= 1;
        i++;
      }
      System.UInt32 RNTRNTRNT_53 = sum;
      IACSharpSensor.IACSharpSensor.SensorReached(1092);
      return RNTRNTRNT_53;
    }
    private void gf2_matrix_square(uint[] square, uint[] mat)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1093);
      for (int i = 0; i < 32; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1094);
        square[i] = gf2_matrix_times(mat, mat[i]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1095);
    }
    public void Combine(int crc, int length)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1096);
      uint[] even = new uint[32];
      uint[] odd = new uint[32];
      IACSharpSensor.IACSharpSensor.SensorReached(1097);
      if (length == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1098);
        return;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1099);
      uint crc1 = ~_register;
      uint crc2 = (uint)crc;
      odd[0] = this.dwPolynomial;
      uint row = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1100);
      for (int i = 1; i < 32; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1101);
        odd[i] = row;
        row <<= 1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1102);
      gf2_matrix_square(even, odd);
      gf2_matrix_square(odd, even);
      uint len2 = (uint)length;
      IACSharpSensor.IACSharpSensor.SensorReached(1103);
      do {
        IACSharpSensor.IACSharpSensor.SensorReached(1104);
        gf2_matrix_square(even, odd);
        IACSharpSensor.IACSharpSensor.SensorReached(1105);
        if ((len2 & 1) == 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(1106);
          crc1 = gf2_matrix_times(even, crc1);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1107);
        len2 >>= 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1108);
        if (len2 == 0) {
          IACSharpSensor.IACSharpSensor.SensorReached(1109);
          break;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1110);
        gf2_matrix_square(odd, even);
        IACSharpSensor.IACSharpSensor.SensorReached(1111);
        if ((len2 & 1) == 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(1112);
          crc1 = gf2_matrix_times(odd, crc1);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1113);
        len2 >>= 1;
      } while (len2 != 0);
      IACSharpSensor.IACSharpSensor.SensorReached(1114);
      crc1 ^= crc2;
      _register = ~crc1;
      IACSharpSensor.IACSharpSensor.SensorReached(1115);
      return;
    }
    public CRC32() : this(false)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1116);
    }
    public CRC32(bool reverseBits) : this(unchecked((int)0xedb88320u), reverseBits)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1117);
    }
    public CRC32(int polynomial, bool reverseBits)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1118);
      this.reverseBits = reverseBits;
      this.dwPolynomial = (uint)polynomial;
      this.GenerateLookupTable();
      IACSharpSensor.IACSharpSensor.SensorReached(1119);
    }
    public void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1120);
      _register = 0xffffffffu;
      IACSharpSensor.IACSharpSensor.SensorReached(1121);
    }
    private UInt32 dwPolynomial;
    private Int64 _TotalBytesRead;
    private bool reverseBits;
    private UInt32[] crc32Table;
    private const int BUFFER_SIZE = 8192;
    private UInt32 _register = 0xffffffffu;
  }
  public class CrcCalculatorStream : System.IO.Stream, System.IDisposable
  {
    private static readonly Int64 UnsetLengthLimit = -99;
    internal System.IO.Stream _innerStream;
    private CRC32 _Crc32;
    private Int64 _lengthLimit = -99;
    private bool _leaveOpen;
    public CrcCalculatorStream(System.IO.Stream stream) : this(true, CrcCalculatorStream.UnsetLengthLimit, stream, null)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1122);
    }
    public CrcCalculatorStream(System.IO.Stream stream, bool leaveOpen) : this(leaveOpen, CrcCalculatorStream.UnsetLengthLimit, stream, null)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1123);
    }
    public CrcCalculatorStream(System.IO.Stream stream, Int64 length) : this(true, length, stream, null)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1124);
      if (length < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1125);
        throw new ArgumentException("length");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1126);
    }
    public CrcCalculatorStream(System.IO.Stream stream, Int64 length, bool leaveOpen) : this(leaveOpen, length, stream, null)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1127);
      if (length < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1128);
        throw new ArgumentException("length");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1129);
    }
    public CrcCalculatorStream(System.IO.Stream stream, Int64 length, bool leaveOpen, CRC32 crc32) : this(leaveOpen, length, stream, crc32)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1130);
      if (length < 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1131);
        throw new ArgumentException("length");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1132);
    }
    private CrcCalculatorStream(bool leaveOpen, Int64 length, System.IO.Stream stream, CRC32 crc32) : base()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1133);
      _innerStream = stream;
      _Crc32 = crc32 ?? new CRC32();
      _lengthLimit = length;
      _leaveOpen = leaveOpen;
      IACSharpSensor.IACSharpSensor.SensorReached(1134);
    }
    public Int64 TotalBytesSlurped {
      get {
        Int64 RNTRNTRNT_54 = _Crc32.TotalBytesRead;
        IACSharpSensor.IACSharpSensor.SensorReached(1135);
        return RNTRNTRNT_54;
      }
    }
    public Int32 Crc {
      get {
        Int32 RNTRNTRNT_55 = _Crc32.Crc32Result;
        IACSharpSensor.IACSharpSensor.SensorReached(1136);
        return RNTRNTRNT_55;
      }
    }
    public bool LeaveOpen {
      get {
        System.Boolean RNTRNTRNT_56 = _leaveOpen;
        IACSharpSensor.IACSharpSensor.SensorReached(1137);
        return RNTRNTRNT_56;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1138);
        _leaveOpen = value;
        IACSharpSensor.IACSharpSensor.SensorReached(1139);
      }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1140);
      int bytesToRead = count;
      IACSharpSensor.IACSharpSensor.SensorReached(1141);
      if (_lengthLimit != CrcCalculatorStream.UnsetLengthLimit) {
        IACSharpSensor.IACSharpSensor.SensorReached(1142);
        if (_Crc32.TotalBytesRead >= _lengthLimit) {
          System.Int32 RNTRNTRNT_57 = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(1143);
          return RNTRNTRNT_57;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(1144);
        Int64 bytesRemaining = _lengthLimit - _Crc32.TotalBytesRead;
        IACSharpSensor.IACSharpSensor.SensorReached(1145);
        if (bytesRemaining < count) {
          IACSharpSensor.IACSharpSensor.SensorReached(1146);
          bytesToRead = (int)bytesRemaining;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1147);
      int n = _innerStream.Read(buffer, offset, bytesToRead);
      IACSharpSensor.IACSharpSensor.SensorReached(1148);
      if (n > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1149);
        _Crc32.SlurpBlock(buffer, offset, n);
      }
      System.Int32 RNTRNTRNT_58 = n;
      IACSharpSensor.IACSharpSensor.SensorReached(1150);
      return RNTRNTRNT_58;
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1151);
      if (count > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(1152);
        _Crc32.SlurpBlock(buffer, offset, count);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1153);
      _innerStream.Write(buffer, offset, count);
      IACSharpSensor.IACSharpSensor.SensorReached(1154);
    }
    public override bool CanRead {
      get {
        System.Boolean RNTRNTRNT_59 = _innerStream.CanRead;
        IACSharpSensor.IACSharpSensor.SensorReached(1155);
        return RNTRNTRNT_59;
      }
    }
    public override bool CanSeek {
      get {
        System.Boolean RNTRNTRNT_60 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1156);
        return RNTRNTRNT_60;
      }
    }
    public override bool CanWrite {
      get {
        System.Boolean RNTRNTRNT_61 = _innerStream.CanWrite;
        IACSharpSensor.IACSharpSensor.SensorReached(1157);
        return RNTRNTRNT_61;
      }
    }
    public override void Flush()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1158);
      _innerStream.Flush();
      IACSharpSensor.IACSharpSensor.SensorReached(1159);
    }
    public override long Length {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(1160);
        if (_lengthLimit == CrcCalculatorStream.UnsetLengthLimit) {
          System.Int64 RNTRNTRNT_62 = _innerStream.Length;
          IACSharpSensor.IACSharpSensor.SensorReached(1161);
          return RNTRNTRNT_62;
        } else {
          System.Int64 RNTRNTRNT_63 = _lengthLimit;
          IACSharpSensor.IACSharpSensor.SensorReached(1162);
          return RNTRNTRNT_63;
        }
      }
    }
    public override long Position {
      get {
        System.Int64 RNTRNTRNT_64 = _Crc32.TotalBytesRead;
        IACSharpSensor.IACSharpSensor.SensorReached(1163);
        return RNTRNTRNT_64;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(1164);
        throw new NotSupportedException();
      }
    }
    public override long Seek(long offset, System.IO.SeekOrigin origin)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1165);
      throw new NotSupportedException();
    }
    public override void SetLength(long value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1166);
      throw new NotSupportedException();
    }
    void IDisposable.Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1167);
      Close();
      IACSharpSensor.IACSharpSensor.SensorReached(1168);
    }
    public override void Close()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1169);
      base.Close();
      IACSharpSensor.IACSharpSensor.SensorReached(1170);
      if (!_leaveOpen) {
        IACSharpSensor.IACSharpSensor.SensorReached(1171);
        _innerStream.Close();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1172);
    }
  }
}
