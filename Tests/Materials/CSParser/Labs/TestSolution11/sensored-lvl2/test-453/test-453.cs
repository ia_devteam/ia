using System;
class C
{
  internal enum Flags
  {
    Removed = 0,
    Public = 1
  }
  static Flags _enumFlags;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(312);
    if ((Flags.Removed | 0).ToString() != "Removed") {
      IACSharpSensor.IACSharpSensor.SensorReached(313);
      throw new ApplicationException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(314);
  }
}
