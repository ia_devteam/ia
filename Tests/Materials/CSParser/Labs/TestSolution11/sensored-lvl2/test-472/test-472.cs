using System;
using System.Reflection;
class Test
{
  public int Prop {
    get {
      System.Int32 RNTRNTRNT_101 = prop;
      IACSharpSensor.IACSharpSensor.SensorReached(410);
      return RNTRNTRNT_101;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(411);
      prop = value;
      IACSharpSensor.IACSharpSensor.SensorReached(412);
    }
  }
  int prop = 0;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    MethodInfo mi = typeof(Test).GetMethod("set_Prop");
    IACSharpSensor.IACSharpSensor.SensorReached(414);
    if (mi.GetParameters().Length != 1) {
      System.Int32 RNTRNTRNT_102 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(415);
      return RNTRNTRNT_102;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(416);
    if ((mi.GetParameters()[0].Name) != "value") {
      System.Int32 RNTRNTRNT_103 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(417);
      return RNTRNTRNT_103;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(418);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_104 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(419);
    return RNTRNTRNT_104;
  }
}
