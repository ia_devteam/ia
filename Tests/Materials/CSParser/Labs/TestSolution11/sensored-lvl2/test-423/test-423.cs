unsafe class Test
{
  static void lowLevelCall(int* pv)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(211);
  }
  static void Func(out int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(212);
    fixed (int* pi = &i) {
      IACSharpSensor.IACSharpSensor.SensorReached(213);
      lowLevelCall(pi);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(214);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    int i = 0;
    Func(out i);
    IACSharpSensor.IACSharpSensor.SensorReached(216);
  }
}
