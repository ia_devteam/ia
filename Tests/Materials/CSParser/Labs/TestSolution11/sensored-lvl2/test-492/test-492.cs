using System;
using System.Reflection;
namespace Test
{
  [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
  public class My1Attribute : Attribute
  {
    public My1Attribute(object o)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(491);
      if (o != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(492);
        throw new ApplicationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(493);
    }
  }
  public class My2Attribute : Attribute
  {
    public My2Attribute(string[] s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(494);
      if (s.Length != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(495);
        throw new ApplicationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(496);
    }
  }
  public class My3Attribute : Attribute
  {
    public My3Attribute(byte b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(497);
      if (b != 0xff) {
        IACSharpSensor.IACSharpSensor.SensorReached(498);
        throw new ApplicationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(499);
    }
  }
  [My3(unchecked((byte)-1))]
  [My1((object)null)]
  [My1(null)]
  [My2(new string[0])]
  public class Test
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(500);
      System.Reflection.MemberInfo info = typeof(Test);
      object[] attributes = info.GetCustomAttributes(false);
      IACSharpSensor.IACSharpSensor.SensorReached(501);
      if (attributes.Length != 4) {
        System.Int32 RNTRNTRNT_111 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(502);
        return RNTRNTRNT_111;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(503);
      for (int i = 0; i < attributes.Length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(504);
        Console.WriteLine(attributes[i]);
      }
      System.Int32 RNTRNTRNT_112 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(505);
      return RNTRNTRNT_112;
    }
  }
}
