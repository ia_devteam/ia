using System;
namespace testapp
{
  unsafe public class LibTestAPI
  {
    struct LibTestStruct
    {
      void* pData;
      void* pTest1;
    }
    LibTestStruct* the_struct;
    public void Create()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(121);
      IntPtr MyPtr = new IntPtr(0);
      the_struct = (LibTestStruct*)0;
      IACSharpSensor.IACSharpSensor.SensorReached(122);
    }
  }
  class TestApp
  {
    static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(123);
      LibTestAPI myapi = new LibTestAPI();
      myapi.Create();
      IACSharpSensor.IACSharpSensor.SensorReached(124);
    }
  }
}
