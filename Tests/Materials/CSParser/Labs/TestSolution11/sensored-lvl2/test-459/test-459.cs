using System;
class CC
{
  public class IfElseStateMachine
  {
    public enum State
    {
      START,
      IF_SEEN,
      ELSEIF_SEEN,
      ELSE_SEEN,
      ENDIF_SEEN,
      MAX
    }
    public enum Token
    {
      START,
      IF,
      ELSEIF,
      ELSE,
      ENDIF,
      EOF,
      MAX
    }
    State state;
    public IfElseStateMachine()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(340);
    }
    public void HandleToken(Token tok)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(341);
      if (tok == Token.IF) {
        IACSharpSensor.IACSharpSensor.SensorReached(342);
        state = (State)tok;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(343);
    }
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(344);
    new IfElseStateMachine().HandleToken(IfElseStateMachine.Token.IF);
    System.Int32 RNTRNTRNT_81 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(345);
    return RNTRNTRNT_81;
  }
}
