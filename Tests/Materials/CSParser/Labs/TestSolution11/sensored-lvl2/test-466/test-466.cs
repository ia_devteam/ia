unsafe public struct B
{
  private fixed int a[5];
}
unsafe public class C
{
  private B x;
  public void Goo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(377);
    fixed (B* y = &x) {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(378);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(379);
  }
}
