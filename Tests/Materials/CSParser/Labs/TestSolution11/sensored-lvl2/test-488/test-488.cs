using System.Collections;
class P
{
  public int x;
}
struct Q
{
  public P p;
  public Q(P p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(477);
    this.p = p;
    IACSharpSensor.IACSharpSensor.SensorReached(478);
  }
}
class Test
{
  static IEnumerable foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(479);
    return null;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(480);
    IEnumerable f = foo();
    IACSharpSensor.IACSharpSensor.SensorReached(481);
    if (f != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(482);
      foreach (P p in f) {
        IACSharpSensor.IACSharpSensor.SensorReached(483);
        p.x = 0;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(484);
    if (f != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(485);
      foreach (Q q in f) {
        IACSharpSensor.IACSharpSensor.SensorReached(486);
        q.p.x = 0;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(487);
  }
}
