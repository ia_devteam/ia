struct Foo
{
  public int x;
  public override int GetHashCode()
  {
    System.Int32 RNTRNTRNT_77 = base.GetHashCode();
    IACSharpSensor.IACSharpSensor.SensorReached(320);
    return RNTRNTRNT_77;
  }
}
class Test
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(321);
    Foo foo = new Foo();
    System.Console.WriteLine(foo.GetHashCode());
    IACSharpSensor.IACSharpSensor.SensorReached(322);
  }
}
