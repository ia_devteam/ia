using System;
namespace ConsoleApplication1
{
  class Program
  {
    unsafe static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(276);
      int[] i = new int[] { 10 };
      IACSharpSensor.IACSharpSensor.SensorReached(277);
      fixed (int* p = i) {
        IACSharpSensor.IACSharpSensor.SensorReached(278);
        int*[] q = new int*[] { p };
        *q[0] = 5;
        Console.WriteLine(*q[0]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(279);
    }
  }
}
