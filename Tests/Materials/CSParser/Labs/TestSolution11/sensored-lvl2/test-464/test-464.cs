using System.Reflection;
using System;
class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(363);
    string[] s = typeof(C).Assembly.GetManifestResourceNames();
    IACSharpSensor.IACSharpSensor.SensorReached(364);
    if (s[0] != "TEST") {
      System.Int32 RNTRNTRNT_88 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(365);
      return RNTRNTRNT_88;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(366);
    if (typeof(C).Assembly.GetManifestResourceStream("TEST") == null) {
      System.Int32 RNTRNTRNT_89 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(367);
      return RNTRNTRNT_89;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_90 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(369);
    return RNTRNTRNT_90;
  }
}
