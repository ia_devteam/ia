unsafe class X
{
  static int v;
  static int v_calls;
  static int* get_v()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(110);
    v_calls++;
    IACSharpSensor.IACSharpSensor.SensorReached(111);
    fixed (int* ptr = &v) {
      System.Int32* RNTRNTRNT_38 = ptr;
      IACSharpSensor.IACSharpSensor.SensorReached(112);
      return RNTRNTRNT_38;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(113);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(114);
    if ((*get_v())++ != 0) {
      System.Int32 RNTRNTRNT_39 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(115);
      return RNTRNTRNT_39;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(116);
    if (v != 1) {
      System.Int32 RNTRNTRNT_40 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(117);
      return RNTRNTRNT_40;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(118);
    if (v_calls != 1) {
      System.Int32 RNTRNTRNT_41 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(119);
      return RNTRNTRNT_41;
    }
    System.Int32 RNTRNTRNT_42 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(120);
    return RNTRNTRNT_42;
  }
}
