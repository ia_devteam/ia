using System;
public delegate void TestDelegate(out int a);
public static class TestClass
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(450);
    TestDelegate out_delegate = delegate(out int a) {
      IACSharpSensor.IACSharpSensor.SensorReached(451);
      a = 0;
    };
    int x = 5;
    out_delegate(out x);
    System.Int32 RNTRNTRNT_109 = x;
    IACSharpSensor.IACSharpSensor.SensorReached(452);
    return RNTRNTRNT_109;
  }
}
