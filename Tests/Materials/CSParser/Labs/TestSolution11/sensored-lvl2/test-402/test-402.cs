using System;
unsafe class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(101);
    int y = 20;
    byte* x = (byte*)0;
    x += (long)y;
    System.Int32 RNTRNTRNT_35 = (int)x - 20 * sizeof(byte);
    IACSharpSensor.IACSharpSensor.SensorReached(102);
    return RNTRNTRNT_35;
  }
}
