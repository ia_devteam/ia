using globalA = A;
class A
{
}
class X
{
  class A
  {
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(239);
    global::A a = new globalA();
    System.Console.WriteLine(a.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(240);
  }
}
