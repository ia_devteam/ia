using System;
public class A : Attribute
{
  [A()]
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(230);
  }
}
public class AAttribute : Attribute
{
}
