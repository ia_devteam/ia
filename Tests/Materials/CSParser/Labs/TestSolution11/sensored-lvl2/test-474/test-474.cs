class Z
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(420);
    TestPreinc(1);
    TestPostinc(1);
    IACSharpSensor.IACSharpSensor.SensorReached(421);
  }
  delegate void X();
  static void TestPreinc(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(422);
    Assert(i, 1);
    X x = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(423);
      int z = ++i;
      Assert(z, 2);
      Assert(i, 2);
    };
    x();
    Assert(i, 2);
    IACSharpSensor.IACSharpSensor.SensorReached(424);
  }
  static void TestPostinc(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(425);
    Assert(i, 1);
    X x = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(426);
      int z = i++;
      Assert(z, 1);
      Assert(i, 2);
    };
    x();
    Assert(i, 2);
    IACSharpSensor.IACSharpSensor.SensorReached(427);
  }
  static void Assert(int a, int b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(428);
    if (a == b) {
      IACSharpSensor.IACSharpSensor.SensorReached(429);
      return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(430);
    throw new System.Exception("Incorrect was: " + a + " should have been " + b + ".");
  }
}
