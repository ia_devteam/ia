namespace QtSamples
{
  using Qt;
  public class QtClass : QtSupport
  {
    public QtClass()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(156);
      mousePressEvent += new MousePressEvent(pressEvent);
      IACSharpSensor.IACSharpSensor.SensorReached(157);
    }
    public void pressEvent()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(158);
    }
  }
  public class Testing
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(159);
      QtClass q = new QtClass();
      System.Int32 RNTRNTRNT_58 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(160);
      return RNTRNTRNT_58;
    }
  }
}
