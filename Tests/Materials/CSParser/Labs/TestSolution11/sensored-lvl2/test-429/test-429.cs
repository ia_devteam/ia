using global = Foo;
namespace Foo
{
  class A
  {
  }
}
class A
{
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(237);
    A a = new global::A();
    System.Console.WriteLine(a.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(238);
  }
}
