using System;
class Test
{
  static ulong value = 0;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(273);
    if (value < 9223372036854775809uL) {
      IACSharpSensor.IACSharpSensor.SensorReached(274);
      Console.WriteLine();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(275);
  }
}
