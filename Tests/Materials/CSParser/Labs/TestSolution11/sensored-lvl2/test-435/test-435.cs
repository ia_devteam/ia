using foo = System.Collections;
class X : foo.IEnumerable
{
  System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(249);
    return null;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(250);
    System.Collections.IEnumerable x = new X();
    IACSharpSensor.IACSharpSensor.SensorReached(251);
  }
}
