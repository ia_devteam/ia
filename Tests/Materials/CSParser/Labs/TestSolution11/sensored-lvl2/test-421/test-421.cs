using System;
public interface I
{
  void SetObject(string foo);
}
public class A
{
  public virtual void SetObject(string foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(201);
    Console.WriteLine("A.SetObject {0}", foo);
    IACSharpSensor.IACSharpSensor.SensorReached(202);
  }
}
public class B : A, I
{
}
public class C : B
{
  public static bool ok = false;
  public override void SetObject(string foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    Console.WriteLine("C.SetObject {0}", foo);
    ok = true;
    IACSharpSensor.IACSharpSensor.SensorReached(204);
  }
}
public class X
{
  public static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(205);
    I i = new C();
    i.SetObject("hi");
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    if (!C.ok) {
      System.Int32 RNTRNTRNT_66 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(207);
      return RNTRNTRNT_66;
    }
    System.Int32 RNTRNTRNT_67 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(208);
    return RNTRNTRNT_67;
  }
}
