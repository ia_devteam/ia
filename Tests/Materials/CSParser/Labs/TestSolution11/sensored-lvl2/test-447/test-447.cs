using System;
[assembly: CLSCompliant(true)]
namespace System
{
  [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
  internal sealed class MonoTODOAttribute : Attribute
  {
    string comment;
    public MonoTODOAttribute()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(300);
    }
  }
}
namespace System.Web
{
  public partial class HttpBrowserCapabilities
  {
    [MonoTODO()]
    public Version A {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(301);
        throw new Exception();
      }
    }
  }
}
class Test
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(302);
  }
}
