using System;
public struct LayerMask
{
  private ushort mask;
  public static implicit operator int(LayerMask mask)
  {
    System.Int32 RNTRNTRNT_70 = (int)mask.mask;
    IACSharpSensor.IACSharpSensor.SensorReached(262);
    return RNTRNTRNT_70;
  }
  public static implicit operator LayerMask(int intVal)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(263);
    LayerMask mask;
    mask.mask = unchecked((ushort)intVal);
    LayerMask RNTRNTRNT_71 = mask;
    IACSharpSensor.IACSharpSensor.SensorReached(264);
    return RNTRNTRNT_71;
  }
}
class Test
{
  private static LayerMask test;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(265);
    LayerMask a = ~test;
    IACSharpSensor.IACSharpSensor.SensorReached(266);
    if (a != 0xffff) {
      IACSharpSensor.IACSharpSensor.SensorReached(267);
      throw new Exception("");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(268);
  }
}
