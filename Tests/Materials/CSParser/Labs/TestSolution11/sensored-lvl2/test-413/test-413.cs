using Inner = Foo.Bar.Baz.Inner;
public class Driver
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    Inner.Frob();
    IACSharpSensor.IACSharpSensor.SensorReached(163);
  }
}
