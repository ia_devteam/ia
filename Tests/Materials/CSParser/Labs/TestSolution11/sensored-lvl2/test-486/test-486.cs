using System;
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(461);
    string[] aPath = {
      "a",
      "b"
    };
    char c = '.';
    IACSharpSensor.IACSharpSensor.SensorReached(462);
    if (c.ToString() != ".") {
      IACSharpSensor.IACSharpSensor.SensorReached(463);
      throw new Exception("c.ToString () is not \".\"");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(464);
    string erg = "";
    erg += String.Join(c.ToString(), aPath);
    IACSharpSensor.IACSharpSensor.SensorReached(465);
    if (erg != "a.b") {
      IACSharpSensor.IACSharpSensor.SensorReached(466);
      throw new Exception("erg is " + erg);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(467);
  }
}
