struct Obsolete
{
  int a;
}
struct A
{
  int a, b;
}
class MainClass
{
  unsafe public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(143);
    System.Console.WriteLine(sizeof(Obsolete));
    IACSharpSensor.IACSharpSensor.SensorReached(144);
  }
}
