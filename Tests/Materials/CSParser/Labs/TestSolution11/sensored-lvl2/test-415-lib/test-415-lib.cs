using System;
public abstract class MyTestAbstract
{
  protected abstract string GetName();
  public MyTestAbstract()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(172);
  }
  public void PrintName()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(173);
    Console.WriteLine("Name=" + GetName());
    IACSharpSensor.IACSharpSensor.SensorReached(174);
  }
}
