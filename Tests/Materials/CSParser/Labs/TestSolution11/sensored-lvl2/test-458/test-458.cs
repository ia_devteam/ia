using System;
internal class MyAttr : Attribute
{
  internal MyAttr()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(333);
  }
  internal MyAttr(Type type)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(334);
  }
  internal MyAttr(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(335);
  }
  internal MyAttr(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(336);
  }
}
[MyAttr()]
internal class ClassA
{
}
[MyAttr(typeof(string))]
internal class ClassB
{
}
[MyAttr("abc")]
internal class ClassC
{
}
[MyAttr(3)]
internal class ClassD
{
}
internal class Top
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(337);
    if (typeof(ClassA).GetCustomAttributes(false).Length != 1) {
      System.Int32 RNTRNTRNT_79 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(338);
      return RNTRNTRNT_79;
    }
    System.Int32 RNTRNTRNT_80 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(339);
    return RNTRNTRNT_80;
  }
}
