interface IInteger
{
  void Add(int i);
}
interface IDouble
{
  void Add(double d);
}
interface INumber : IInteger, IDouble
{
}
class Number : INumber
{
  void IDouble.Add(double d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(327);
    System.Console.WriteLine("IDouble.Add (double d)");
    IACSharpSensor.IACSharpSensor.SensorReached(328);
  }
  void IInteger.Add(int d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(329);
    System.Console.WriteLine("IInteger.Add (int d)");
    IACSharpSensor.IACSharpSensor.SensorReached(330);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(331);
    INumber n = new Number();
    n.Add(1);
    n.Add(1.0);
    ((IInteger)n).Add(1);
    ((IDouble)n).Add(1);
    System.Int32 RNTRNTRNT_78 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(332);
    return RNTRNTRNT_78;
  }
}
