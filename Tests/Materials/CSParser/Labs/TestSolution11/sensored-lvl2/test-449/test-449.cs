using System;
using System.Threading;
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(305);
  }
  void Z()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(306);
    ThreadPool.QueueUserWorkItem(delegate {
      Z();
      ThreadPool.QueueUserWorkItem(delegate { Z(); });
    });
    IACSharpSensor.IACSharpSensor.SensorReached(307);
  }
}
