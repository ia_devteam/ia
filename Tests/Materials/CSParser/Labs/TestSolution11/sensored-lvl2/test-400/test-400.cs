using System;
unsafe struct Y
{
  public int a;
  public int s;
}
unsafe class X
{
  static int TestDereference()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
    Y y;
    Y* z;
    Y a;
    z = &y;
    y.a = 1;
    y.s = 2;
    a.a = z->a;
    a.s = z->s;
    IACSharpSensor.IACSharpSensor.SensorReached(2);
    if (a.a != y.a) {
      System.Int32 RNTRNTRNT_1 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(3);
      return RNTRNTRNT_1;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(4);
    if (a.s != y.s) {
      System.Int32 RNTRNTRNT_2 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(5);
      return RNTRNTRNT_2;
    }
    System.Int32 RNTRNTRNT_3 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(6);
    return RNTRNTRNT_3;
  }
  static int TestPtrAdd()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(7);
    int[] a = new int[10];
    int i;
    IACSharpSensor.IACSharpSensor.SensorReached(8);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(9);
      a[i] = i;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(10);
    i = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(11);
    fixed (int* b = &a[0]) {
      IACSharpSensor.IACSharpSensor.SensorReached(12);
      int* p = b;
      IACSharpSensor.IACSharpSensor.SensorReached(13);
      for (i = 0; i < 10; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(14);
        if (*p != a[i]) {
          System.Int32 RNTRNTRNT_4 = 10 + i;
          IACSharpSensor.IACSharpSensor.SensorReached(15);
          return RNTRNTRNT_4;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(16);
        p++;
      }
    }
    System.Int32 RNTRNTRNT_5 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(17);
    return RNTRNTRNT_5;
  }
  static int i = 1;
  static char c = 'a';
  static long l = 123;
  static double d = 1.2;
  static float f = 1.3f;
  static short s = 4;
  static int TestPtrAssign()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(18);
    fixed (int* ii = &i) {
      IACSharpSensor.IACSharpSensor.SensorReached(19);
      *ii = 10;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(20);
    fixed (char* cc = &c) {
      IACSharpSensor.IACSharpSensor.SensorReached(21);
      *cc = 'b';
    }
    IACSharpSensor.IACSharpSensor.SensorReached(22);
    fixed (long* ll = &l) {
      IACSharpSensor.IACSharpSensor.SensorReached(23);
      *ll = 100;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(24);
    fixed (double* dd = &d) {
      IACSharpSensor.IACSharpSensor.SensorReached(25);
      *dd = 3.0;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(26);
    fixed (float* ff = &f) {
      IACSharpSensor.IACSharpSensor.SensorReached(27);
      *ff = 1.2f;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(28);
    fixed (short* ss = &s) {
      IACSharpSensor.IACSharpSensor.SensorReached(29);
      *ss = 102;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(30);
    if (i != 10) {
      System.Int32 RNTRNTRNT_6 = 100;
      IACSharpSensor.IACSharpSensor.SensorReached(31);
      return RNTRNTRNT_6;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(32);
    if (c != 'b') {
      System.Int32 RNTRNTRNT_7 = 101;
      IACSharpSensor.IACSharpSensor.SensorReached(33);
      return RNTRNTRNT_7;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(34);
    if (l != 100) {
      System.Int32 RNTRNTRNT_8 = 102;
      IACSharpSensor.IACSharpSensor.SensorReached(35);
      return RNTRNTRNT_8;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(36);
    if (d != 3.0) {
      System.Int32 RNTRNTRNT_9 = 103;
      IACSharpSensor.IACSharpSensor.SensorReached(37);
      return RNTRNTRNT_9;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(38);
    if (f != 1.2f) {
      System.Int32 RNTRNTRNT_10 = 104;
      IACSharpSensor.IACSharpSensor.SensorReached(39);
      return RNTRNTRNT_10;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(40);
    if (s != 102) {
      System.Int32 RNTRNTRNT_11 = 105;
      IACSharpSensor.IACSharpSensor.SensorReached(41);
      return RNTRNTRNT_11;
    }
    System.Int32 RNTRNTRNT_12 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    return RNTRNTRNT_12;
  }
  static int TestPtrArithmetic()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(43);
    char[] array = new char[10];
    char* pb;
    array[5] = 'j';
    IACSharpSensor.IACSharpSensor.SensorReached(44);
    fixed (char* pa = array) {
      IACSharpSensor.IACSharpSensor.SensorReached(45);
      pb = pa + 1;
      IACSharpSensor.IACSharpSensor.SensorReached(46);
      if (pa[5] != 'j') {
        System.Int32 RNTRNTRNT_13 = 199;
        IACSharpSensor.IACSharpSensor.SensorReached(47);
        return RNTRNTRNT_13;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(48);
      Console.WriteLine("V: " + (pb - pa));
      IACSharpSensor.IACSharpSensor.SensorReached(49);
      if ((pb - pa) != 1) {
        System.Int32 RNTRNTRNT_14 = 200;
        IACSharpSensor.IACSharpSensor.SensorReached(50);
        return RNTRNTRNT_14;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(51);
      pb++;
      IACSharpSensor.IACSharpSensor.SensorReached(52);
      if (pb == pa) {
        System.Int32 RNTRNTRNT_15 = 201;
        IACSharpSensor.IACSharpSensor.SensorReached(53);
        return RNTRNTRNT_15;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      if (pb < pa) {
        System.Int32 RNTRNTRNT_16 = 202;
        IACSharpSensor.IACSharpSensor.SensorReached(55);
        return RNTRNTRNT_16;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(56);
      if (pa > pb) {
        System.Int32 RNTRNTRNT_17 = 203;
        IACSharpSensor.IACSharpSensor.SensorReached(57);
        return RNTRNTRNT_17;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(58);
      if (pa >= pb) {
        System.Int32 RNTRNTRNT_18 = 204;
        IACSharpSensor.IACSharpSensor.SensorReached(59);
        return RNTRNTRNT_18;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      if (pb <= pa) {
        System.Int32 RNTRNTRNT_19 = 205;
        IACSharpSensor.IACSharpSensor.SensorReached(61);
        return RNTRNTRNT_19;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(62);
      pb = pb - 2;
      IACSharpSensor.IACSharpSensor.SensorReached(63);
      if (pb != pa) {
        IACSharpSensor.IACSharpSensor.SensorReached(64);
        Console.WriteLine("VV: " + (pb - pa));
        System.Int32 RNTRNTRNT_20 = 206;
        IACSharpSensor.IACSharpSensor.SensorReached(65);
        return RNTRNTRNT_20;
      }
    }
    System.Int32 RNTRNTRNT_21 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(66);
    return RNTRNTRNT_21;
  }
  static int TestMultiple()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(67);
    char[] array = new char[10];
    int count = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    fixed (char* pa = array, pb = array) {
      IACSharpSensor.IACSharpSensor.SensorReached(69);
      count++;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(70);
    if (count != 1) {
      System.Int32 RNTRNTRNT_22 = 300;
      IACSharpSensor.IACSharpSensor.SensorReached(71);
      return RNTRNTRNT_22;
    }
    System.Int32 RNTRNTRNT_23 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(72);
    return RNTRNTRNT_23;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(73);
    int v;
    IACSharpSensor.IACSharpSensor.SensorReached(74);
    if ((v = TestDereference()) != 0) {
      System.Int32 RNTRNTRNT_24 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(75);
      return RNTRNTRNT_24;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(76);
    if ((v = TestPtrAdd()) != 0) {
      System.Int32 RNTRNTRNT_25 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(77);
      return RNTRNTRNT_25;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(78);
    if ((v = TestPtrAssign()) != 0) {
      System.Int32 RNTRNTRNT_26 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(79);
      return RNTRNTRNT_26;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(80);
    if ((v = TestPtrArithmetic()) != 0) {
      System.Int32 RNTRNTRNT_27 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(81);
      return RNTRNTRNT_27;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(82);
    if ((v = TestMultiple()) != 0) {
      System.Int32 RNTRNTRNT_28 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(83);
      return RNTRNTRNT_28;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(84);
    Console.WriteLine("Ok");
    System.Int32 RNTRNTRNT_29 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(85);
    return RNTRNTRNT_29;
  }
}
