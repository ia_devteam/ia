using System;
using System.Collections;
using System.Text;
class Space
{
  public int Value = -1;
  public delegate void DoCopy();
  public DoCopy CopyIt;
  public void Leak(bool useArray, int max)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(445);
    DoCopy one;
    IACSharpSensor.IACSharpSensor.SensorReached(447);
    {
      IACSharpSensor.IACSharpSensor.SensorReached(446);
      int answer = 0;
      int[] work;
      CopyIt = delegate { Value = answer; };
      one = delegate {
        work = new int[max];
        foreach (int x in work)
          answer += x;
      };
    }
    one();
    IACSharpSensor.IACSharpSensor.SensorReached(448);
  }
}
class Program
{
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(449);
  }
}
