using System;
unsafe class Foo
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(103);
    int a;
    int* b;
    int** c;
    a = 42;
    b = &a;
    c = &b;
    Console.WriteLine("*c == b : {0}", *c == b);
    Console.WriteLine("**c == a : {0}", **c == a);
    IACSharpSensor.IACSharpSensor.SensorReached(104);
    if (*c == b && **c == a) {
      IACSharpSensor.IACSharpSensor.SensorReached(105);
      Console.WriteLine("Test passed");
      System.Int32 RNTRNTRNT_36 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(106);
      return RNTRNTRNT_36;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(107);
      Console.WriteLine("Test failed");
      System.Int32 RNTRNTRNT_37 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(108);
      return RNTRNTRNT_37;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(109);
  }
}
