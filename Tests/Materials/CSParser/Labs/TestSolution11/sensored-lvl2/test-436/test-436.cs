using foo = System.Collections;
class X : foo.IEnumerable
{
  foo.IEnumerator foo.IEnumerable.GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(252);
    return null;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(253);
    System.Collections.IEnumerable x = new X();
    IACSharpSensor.IACSharpSensor.SensorReached(254);
  }
}
