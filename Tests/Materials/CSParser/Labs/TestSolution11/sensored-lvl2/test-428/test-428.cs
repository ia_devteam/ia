using globalA = A;
class A
{
}
class X
{
  class A
  {
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(235);
    globalA a = new global::A();
    System.Console.WriteLine(a.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(236);
  }
}
