struct Foo
{
  public float f;
  public void foo()
  {
    unsafe {
      IACSharpSensor.IACSharpSensor.SensorReached(323);
      fixed (float* pf2 = &f) {
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(324);
  }
}
class Test
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(325);
    Foo x = new Foo();
    x.foo();
    IACSharpSensor.IACSharpSensor.SensorReached(326);
  }
}
