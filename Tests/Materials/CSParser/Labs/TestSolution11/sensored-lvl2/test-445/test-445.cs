using System;
public class ConvFromInt
{
  public int val;
  public ConvFromInt()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(281);
    val = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(282);
  }
  public ConvFromInt(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    val = value + 1;
    IACSharpSensor.IACSharpSensor.SensorReached(284);
  }
  public static implicit operator ConvFromInt(int value)
  {
    ConvFromInt RNTRNTRNT_74 = new ConvFromInt(value);
    IACSharpSensor.IACSharpSensor.SensorReached(285);
    return RNTRNTRNT_74;
  }
}
public class Foo
{
  public static ConvFromInt i = 0;
  public static object BoolObj = (bool)false;
  public static object ByteObj = (byte)0;
  public static ValueType BoolVal = (bool)false;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    if (i == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(287);
      throw new Exception("i");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(288);
    if (i.val == 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(289);
      throw new Exception("i.val");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(290);
    if (BoolObj == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(291);
      throw new Exception("BoolObj");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(292);
    if (ByteObj == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(293);
      throw new Exception("ByteObj");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(294);
    if (BoolVal == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(295);
      throw new Exception("BoolVal");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(296);
  }
}
