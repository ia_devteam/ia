using System;
public class M3 : M1
{
  public M3() : base("FOO")
  {
    IACSharpSensor.IACSharpSensor.SensorReached(183);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(184);
    if (new M3().Foo != "FOO") {
      System.Int32 RNTRNTRNT_61 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(185);
      return RNTRNTRNT_61;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(186);
    if (typeof(M3).Assembly.GetTypes().Length != 3) {
      System.Int32 RNTRNTRNT_62 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(187);
      return RNTRNTRNT_62;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(188);
    if (typeof(M3).Assembly.GetType("M2") == null) {
      System.Int32 RNTRNTRNT_63 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(189);
      return RNTRNTRNT_63;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(190);
    if (typeof(M3).Assembly.GetType("M2") != typeof(M2)) {
      System.Int32 RNTRNTRNT_64 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(191);
      return RNTRNTRNT_64;
    }
    System.Int32 RNTRNTRNT_65 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(192);
    return RNTRNTRNT_65;
  }
}
