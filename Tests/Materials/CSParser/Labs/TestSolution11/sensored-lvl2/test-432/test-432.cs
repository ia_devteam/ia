using foo = Foo;
namespace Foo
{
  class A
  {
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(243);
    Foo.A a = new foo.A();
    System.Console.WriteLine(a.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(244);
  }
}
