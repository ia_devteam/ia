using foo = Foo;
namespace Foo
{
  class A
  {
  }
}
class X
{
  static foo.A a = new Foo.A();
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    System.Console.WriteLine(a.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(246);
  }
}
