using System;
public class ExceptionWithAnonMethod
{
  public delegate void EmptyCallback();
  static string res;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(380);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(381);
      throw new Exception("e is afraid to enter anonymous land");
    } catch (Exception e) {
      IACSharpSensor.IACSharpSensor.SensorReached(382);
      AnonHandler(delegate {
        Console.WriteLine(e.Message);
        res = e.Message;
      });
      IACSharpSensor.IACSharpSensor.SensorReached(383);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(384);
    if (res == "e is afraid to enter anonymous land") {
      IACSharpSensor.IACSharpSensor.SensorReached(385);
      Console.WriteLine("Test passed");
      System.Int32 RNTRNTRNT_94 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(386);
      return RNTRNTRNT_94;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(387);
    Console.WriteLine("Test failed");
    System.Int32 RNTRNTRNT_95 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(388);
    return RNTRNTRNT_95;
  }
  public static void AnonHandler(EmptyCallback handler)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(389);
    if (handler != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(390);
      handler();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(391);
  }
}
