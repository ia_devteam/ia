using System;
using blah;
namespace blah2
{
  public class MyClass
  {
    public event MyFunnyDelegate DoSomething;
    public void DoSomethingFunny()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(178);
      if (DoSomething != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(179);
        DoSomething(this, "hello there", "my friend");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(180);
    }
    public static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(181);
      MyClass mc = new MyClass();
      mc.DoSomethingFunny();
      IACSharpSensor.IACSharpSensor.SensorReached(182);
    }
  }
}
