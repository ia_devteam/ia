using foo = Foo;
namespace Foo
{
  class A
  {
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(241);
    foo.A a = new Foo.A();
    System.Console.WriteLine(a.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(242);
  }
}
