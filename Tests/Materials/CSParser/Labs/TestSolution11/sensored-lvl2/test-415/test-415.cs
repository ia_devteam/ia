using System;
public class MyTestExtended : MyTestAbstract
{
  public MyTestExtended() : base()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(168);
  }
  protected override string GetName()
  {
    System.String RNTRNTRNT_59 = "foo";
    IACSharpSensor.IACSharpSensor.SensorReached(169);
    return RNTRNTRNT_59;
  }
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    Console.WriteLine("Calling PrintName");
    MyTestExtended test = new MyTestExtended();
    test.PrintName();
    Console.WriteLine("Out of PrintName");
    IACSharpSensor.IACSharpSensor.SensorReached(171);
  }
}
