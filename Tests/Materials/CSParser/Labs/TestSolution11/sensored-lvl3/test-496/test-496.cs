using System;
using System.Collections;
public class Test
{
  public IEnumerator GetEnumerator()
  {
    Object RNTRNTRNT_115 = "TEST";
    IACSharpSensor.IACSharpSensor.SensorReached(306);
    yield return RNTRNTRNT_115;
    IACSharpSensor.IACSharpSensor.SensorReached(307);
    try {
      int.Parse(arg);
    } catch {
      IACSharpSensor.IACSharpSensor.SensorReached(308);
      yield break;
    }
    Object RNTRNTRNT_116 = "TEST2";
    IACSharpSensor.IACSharpSensor.SensorReached(309);
    yield return RNTRNTRNT_116;
    IACSharpSensor.IACSharpSensor.SensorReached(310);
    IACSharpSensor.IACSharpSensor.SensorReached(311);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(312);
    new Test().Run();
    IACSharpSensor.IACSharpSensor.SensorReached(313);
  }
  string arg;
  void Run()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(314);
    int i = 0;
    foreach (string s in this) {
      i++;
    }
    if (i != 1) {
      throw new Exception();
    }
    arg = "1";
    i = 0;
    foreach (string s in this) {
      i++;
    }
    if (i != 2) {
      throw new Exception();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(315);
  }
}
