using System;
public class MonoDivideProblem
{
  static uint dividend = 0x80000000u;
  static uint divisor = 1;
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(184);
    Console.WriteLine("Dividend/Divisor = {0}", dividend / divisor);
    IACSharpSensor.IACSharpSensor.SensorReached(185);
  }
}
