using System;
class CC
{
  public class IfElseStateMachine
  {
    public enum State
    {
      START,
      IF_SEEN,
      ELSEIF_SEEN,
      ELSE_SEEN,
      ENDIF_SEEN,
      MAX
    }
    public enum Token
    {
      START,
      IF,
      ELSEIF,
      ELSE,
      ENDIF,
      EOF,
      MAX
    }
    State state;
    public IfElseStateMachine()
    {
    }
    public void HandleToken(Token tok)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(212);
      if (tok == Token.IF) {
        state = (State)tok;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(213);
    }
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(214);
    new IfElseStateMachine().HandleToken(IfElseStateMachine.Token.IF);
    System.Int32 RNTRNTRNT_81 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    return RNTRNTRNT_81;
  }
}
