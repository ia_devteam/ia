interface IInteger
{
  void Add(int i);
}
interface IDouble
{
  void Add(double d);
}
interface INumber : IInteger, IDouble
{
}
class Number : INumber
{
  void IDouble.Add(double d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    System.Console.WriteLine("IDouble.Add (double d)");
    IACSharpSensor.IACSharpSensor.SensorReached(204);
  }
  void IInteger.Add(int d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(205);
    System.Console.WriteLine("IInteger.Add (int d)");
    IACSharpSensor.IACSharpSensor.SensorReached(206);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(207);
    INumber n = new Number();
    n.Add(1);
    n.Add(1.0);
    ((IInteger)n).Add(1);
    ((IDouble)n).Add(1);
    System.Int32 RNTRNTRNT_78 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(208);
    return RNTRNTRNT_78;
  }
}
