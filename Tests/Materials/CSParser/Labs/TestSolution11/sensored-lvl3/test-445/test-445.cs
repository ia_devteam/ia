using System;
public class ConvFromInt
{
  public int val;
  public ConvFromInt()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(174);
    val = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(175);
  }
  public ConvFromInt(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(176);
    val = value + 1;
    IACSharpSensor.IACSharpSensor.SensorReached(177);
  }
  public static implicit operator ConvFromInt(int value)
  {
    ConvFromInt RNTRNTRNT_74 = new ConvFromInt(value);
    IACSharpSensor.IACSharpSensor.SensorReached(178);
    return RNTRNTRNT_74;
  }
}
public class Foo
{
  public static ConvFromInt i = 0;
  public static object BoolObj = (bool)false;
  public static object ByteObj = (byte)0;
  public static ValueType BoolVal = (bool)false;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(179);
    if (i == null) {
      throw new Exception("i");
    }
    if (i.val == 0) {
      throw new Exception("i.val");
    }
    if (BoolObj == null) {
      throw new Exception("BoolObj");
    }
    if (ByteObj == null) {
      throw new Exception("ByteObj");
    }
    if (BoolVal == null) {
      throw new Exception("BoolVal");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(180);
  }
}
