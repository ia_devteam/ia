using Inner = Foo.Bar.Baz.Inner;
public class Driver
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(85);
    Inner.Frob();
    IACSharpSensor.IACSharpSensor.SensorReached(86);
  }
}
