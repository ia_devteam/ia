using foo = System.Collections;
class X : foo.IEnumerable
{
  foo.IEnumerator foo.IEnumerable.GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(154);
    return null;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(155);
    System.Collections.IEnumerable x = new X();
    IACSharpSensor.IACSharpSensor.SensorReached(156);
  }
}
