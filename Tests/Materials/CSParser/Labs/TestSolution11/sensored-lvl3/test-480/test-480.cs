using System;
using System.Collections;
using System.Text;
class Space
{
  public int Value = -1;
  public delegate void DoCopy();
  public DoCopy CopyIt;
  public void Leak(bool useArray, int max)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(274);
    DoCopy one;
    {
      int answer = 0;
      int[] work;
      CopyIt = delegate { Value = answer; };
      one = delegate {
        work = new int[max];
        foreach (int x in work)
          answer += x;
      };
    }
    one();
    IACSharpSensor.IACSharpSensor.SensorReached(275);
  }
}
class Program
{
  static void Main(string[] args)
  {
  }
}
