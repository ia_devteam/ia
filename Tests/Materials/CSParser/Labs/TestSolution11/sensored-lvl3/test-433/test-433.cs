using foo = Foo;
namespace Foo
{
  class A
  {
  }
}
class X
{
  static foo.A a = new Foo.A();
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(147);
    System.Console.WriteLine(a.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(148);
  }
}
