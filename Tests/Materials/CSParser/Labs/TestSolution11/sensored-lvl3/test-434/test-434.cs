using foo = Foo;
namespace Foo
{
  class A
  {
  }
}
class X
{
  static Foo.A a = new foo.A();
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(149);
    System.Console.WriteLine(a.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(150);
  }
}
