class A
{
  protected int f {
    get {
      System.Int32 RNTRNTRNT_113 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(302);
      return RNTRNTRNT_113;
    }
  }
}
class B : A
{
  int bar()
  {
    System.Int32 RNTRNTRNT_114 = new C().f;
    IACSharpSensor.IACSharpSensor.SensorReached(303);
    return RNTRNTRNT_114;
  }
}
class C : B
{
  static void Main()
  {
  }
}
