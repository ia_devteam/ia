using System;
struct X
{
  int i;
  static bool pass = false;
  X(object dummy)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(284);
    X x = new X();
    x.i = 1;
    int n = 0;
    if ((this = x).i == 1) {
      n++;
    }
    if (this.i == 1) {
      n++;
    }
    pass = (n == 2);
    IACSharpSensor.IACSharpSensor.SensorReached(285);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    new X(null);
    System.Int32 RNTRNTRNT_110 = pass ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(287);
    return RNTRNTRNT_110;
  }
}
