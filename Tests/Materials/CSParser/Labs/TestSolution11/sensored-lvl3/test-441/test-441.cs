using System;
class Test
{
  static ulong value = 0;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    if (value < 9223372036854775809uL) {
      Console.WriteLine();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(171);
  }
}
