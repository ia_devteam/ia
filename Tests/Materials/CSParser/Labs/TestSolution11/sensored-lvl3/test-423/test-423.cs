unsafe class Test
{
  static void lowLevelCall(int* pv)
  {
  }
  static void Func(out int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(121);
    fixed (int* pi = &i) {
      lowLevelCall(pi);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(122);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(123);
    int i = 0;
    Func(out i);
    IACSharpSensor.IACSharpSensor.SensorReached(124);
  }
}
