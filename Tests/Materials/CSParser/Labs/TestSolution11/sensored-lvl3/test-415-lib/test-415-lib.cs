using System;
public abstract class MyTestAbstract
{
  protected abstract string GetName();
  public MyTestAbstract()
  {
  }
  public void PrintName()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(92);
    Console.WriteLine("Name=" + GetName());
    IACSharpSensor.IACSharpSensor.SensorReached(93);
  }
}
