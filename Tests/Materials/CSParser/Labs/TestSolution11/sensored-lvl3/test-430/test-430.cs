using globalA = A;
class A
{
}
class X
{
  class A
  {
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(141);
    global::A a = new globalA();
    System.Console.WriteLine(a.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(142);
  }
}
