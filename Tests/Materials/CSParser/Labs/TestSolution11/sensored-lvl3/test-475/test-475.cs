using System;
public delegate void MyDelegate(int a);
public class X
{
  static event MyDelegate e = X.Test;
  static int cc = 4;
  static void Test(int foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(267);
    X.cc = foo;
    Console.WriteLine("OK");
    IACSharpSensor.IACSharpSensor.SensorReached(268);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(269);
    e(10);
    if (cc != 10) {
      System.Int32 RNTRNTRNT_105 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(270);
      return RNTRNTRNT_105;
    }
    System.Int32 RNTRNTRNT_106 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(271);
    return RNTRNTRNT_106;
  }
}
