using System;
public interface I
{
  void SetObject(string foo);
}
public class A
{
  public virtual void SetObject(string foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(113);
    Console.WriteLine("A.SetObject {0}", foo);
    IACSharpSensor.IACSharpSensor.SensorReached(114);
  }
}
public class B : A, I
{
}
public class C : B
{
  public static bool ok = false;
  public override void SetObject(string foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(115);
    Console.WriteLine("C.SetObject {0}", foo);
    ok = true;
    IACSharpSensor.IACSharpSensor.SensorReached(116);
  }
}
public class X
{
  public static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(117);
    I i = new C();
    i.SetObject("hi");
    if (!C.ok) {
      System.Int32 RNTRNTRNT_66 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(118);
      return RNTRNTRNT_66;
    }
    System.Int32 RNTRNTRNT_67 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(119);
    return RNTRNTRNT_67;
  }
}
