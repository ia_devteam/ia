struct Obsolete
{
  int a;
}
struct A
{
  int a, b;
}
class MainClass
{
  unsafe public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(71);
    System.Console.WriteLine(sizeof(Obsolete));
    IACSharpSensor.IACSharpSensor.SensorReached(72);
  }
}
