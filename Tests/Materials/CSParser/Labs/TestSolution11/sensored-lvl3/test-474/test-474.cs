class Z
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(259);
    TestPreinc(1);
    TestPostinc(1);
    IACSharpSensor.IACSharpSensor.SensorReached(260);
  }
  delegate void X();
  static void TestPreinc(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(261);
    Assert(i, 1);
    X x = delegate {
      int z = ++i;
      Assert(z, 2);
      Assert(i, 2);
    };
    x();
    Assert(i, 2);
    IACSharpSensor.IACSharpSensor.SensorReached(262);
  }
  static void TestPostinc(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(263);
    Assert(i, 1);
    X x = delegate {
      int z = i++;
      Assert(z, 1);
      Assert(i, 2);
    };
    x();
    Assert(i, 2);
    IACSharpSensor.IACSharpSensor.SensorReached(264);
  }
  static void Assert(int a, int b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(265);
    if (a == b) {
      IACSharpSensor.IACSharpSensor.SensorReached(266);
      return;
    }
    throw new System.Exception("Incorrect was: " + a + " should have been " + b + ".");
  }
}
