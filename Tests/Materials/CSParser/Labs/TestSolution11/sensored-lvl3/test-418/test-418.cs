using System;
public class M3 : M1
{
  public M3() : base("FOO")
  {
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(101);
    if (new M3().Foo != "FOO") {
      System.Int32 RNTRNTRNT_61 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(102);
      return RNTRNTRNT_61;
    }
    if (typeof(M3).Assembly.GetTypes().Length != 3) {
      System.Int32 RNTRNTRNT_62 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(103);
      return RNTRNTRNT_62;
    }
    if (typeof(M3).Assembly.GetType("M2") == null) {
      System.Int32 RNTRNTRNT_63 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(104);
      return RNTRNTRNT_63;
    }
    if (typeof(M3).Assembly.GetType("M2") != typeof(M2)) {
      System.Int32 RNTRNTRNT_64 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(105);
      return RNTRNTRNT_64;
    }
    System.Int32 RNTRNTRNT_65 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(106);
    return RNTRNTRNT_65;
  }
}
