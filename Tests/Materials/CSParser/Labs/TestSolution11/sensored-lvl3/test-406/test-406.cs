using System;
namespace TestCase
{
  unsafe public class Test
  {
    static int Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      uint[] uArr = {
        0,
        200
      };
      uint[] uArr2 = {
        0,
        200
      };
      fixed (uint* u = uArr, u2 = uArr2) {
        if (DoOp(u) != 100) {
          System.Int32 RNTRNTRNT_43 = 1;
          IACSharpSensor.IACSharpSensor.SensorReached(61);
          return RNTRNTRNT_43;
        }
        if (uArr[0] != 100) {
          System.Int32 RNTRNTRNT_44 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(62);
          return RNTRNTRNT_44;
        }
        if (uArr[1] != 200) {
          System.Int32 RNTRNTRNT_45 = 3;
          IACSharpSensor.IACSharpSensor.SensorReached(63);
          return RNTRNTRNT_45;
        }
        if (DoOp2(u2) != 100) {
          System.Int32 RNTRNTRNT_46 = 4;
          IACSharpSensor.IACSharpSensor.SensorReached(64);
          return RNTRNTRNT_46;
        }
        if (uArr2[0] != 100) {
          System.Int32 RNTRNTRNT_47 = 5;
          IACSharpSensor.IACSharpSensor.SensorReached(65);
          return RNTRNTRNT_47;
        }
        if (uArr2[1] != 200) {
          System.Int32 RNTRNTRNT_48 = 6;
          IACSharpSensor.IACSharpSensor.SensorReached(66);
          return RNTRNTRNT_48;
        }
      }
      System.Int32 RNTRNTRNT_49 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(67);
      return RNTRNTRNT_49;
    }
    private static uint DoOp(uint* u)
    {
      System.UInt32 RNTRNTRNT_50 = *(u) += 100;
      IACSharpSensor.IACSharpSensor.SensorReached(68);
      return RNTRNTRNT_50;
    }
    private static uint DoOp2(uint* u)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(69);
      *(u) += 100;
      System.UInt32 RNTRNTRNT_51 = *u;
      IACSharpSensor.IACSharpSensor.SensorReached(70);
      return RNTRNTRNT_51;
    }
  }
}
