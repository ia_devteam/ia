using System.Collections;
class P
{
  public int x;
}
struct Q
{
  public P p;
  public Q(P p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(288);
    this.p = p;
    IACSharpSensor.IACSharpSensor.SensorReached(289);
  }
}
class Test
{
  static IEnumerable foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(290);
    return null;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(291);
    IEnumerable f = foo();
    if (f != null) {
      foreach (P p in f) {
        p.x = 0;
      }
    }
    if (f != null) {
      foreach (Q q in f) {
        q.p.x = 0;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(292);
  }
}
