using System;
unsafe class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    int y = 20;
    byte* x = (byte*)0;
    x += (long)y;
    System.Int32 RNTRNTRNT_35 = (int)x - 20 * sizeof(byte);
    IACSharpSensor.IACSharpSensor.SensorReached(43);
    return RNTRNTRNT_35;
  }
}
