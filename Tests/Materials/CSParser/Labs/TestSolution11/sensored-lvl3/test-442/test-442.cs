using System;
namespace ConsoleApplication1
{
  class Program
  {
    unsafe static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(172);
      int[] i = new int[] { 10 };
      fixed (int* p = i) {
        int*[] q = new int*[] { p };
        *q[0] = 5;
        Console.WriteLine(*q[0]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(173);
    }
  }
}
