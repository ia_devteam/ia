using System;
class C
{
  internal enum Flags
  {
    Removed = 0,
    Public = 1
  }
  static Flags _enumFlags;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(192);
    if ((Flags.Removed | 0).ToString() != "Removed") {
      throw new ApplicationException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(193);
  }
}
