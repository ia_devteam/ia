using System;
using System.Reflection;
namespace Test
{
  [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
  public class My1Attribute : Attribute
  {
    public My1Attribute(object o)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(293);
      if (o != null) {
        throw new ApplicationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(294);
    }
  }
  public class My2Attribute : Attribute
  {
    public My2Attribute(string[] s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(295);
      if (s.Length != 0) {
        throw new ApplicationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(296);
    }
  }
  public class My3Attribute : Attribute
  {
    public My3Attribute(byte b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(297);
      if (b != 0xff) {
        throw new ApplicationException();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(298);
    }
  }
  [My3(unchecked((byte)-1))]
  [My1((object)null)]
  [My1(null)]
  [My2(new string[0])]
  public class Test
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(299);
      System.Reflection.MemberInfo info = typeof(Test);
      object[] attributes = info.GetCustomAttributes(false);
      if (attributes.Length != 4) {
        System.Int32 RNTRNTRNT_111 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(300);
        return RNTRNTRNT_111;
      }
      for (int i = 0; i < attributes.Length; i++) {
        Console.WriteLine(attributes[i]);
      }
      System.Int32 RNTRNTRNT_112 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(301);
      return RNTRNTRNT_112;
    }
  }
}
