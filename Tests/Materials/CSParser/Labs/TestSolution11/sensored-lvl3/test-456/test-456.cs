struct Foo
{
  public float f;
  public void foo()
  {
    unsafe {
      fixed (float* pf2 = &f) {
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(200);
  }
}
class Test
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(201);
    Foo x = new Foo();
    x.foo();
    IACSharpSensor.IACSharpSensor.SensorReached(202);
  }
}
