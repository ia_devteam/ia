unsafe class X
{
  static int v;
  static int v_calls;
  static int* get_v()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(48);
    v_calls++;
    fixed (int* ptr = &v) {
      System.Int32* RNTRNTRNT_38 = ptr;
      IACSharpSensor.IACSharpSensor.SensorReached(49);
      return RNTRNTRNT_38;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(50);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(51);
    if ((*get_v())++ != 0) {
      System.Int32 RNTRNTRNT_39 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(52);
      return RNTRNTRNT_39;
    }
    if (v != 1) {
      System.Int32 RNTRNTRNT_40 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(53);
      return RNTRNTRNT_40;
    }
    if (v_calls != 1) {
      System.Int32 RNTRNTRNT_41 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      return RNTRNTRNT_41;
    }
    System.Int32 RNTRNTRNT_42 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(55);
    return RNTRNTRNT_42;
  }
}
