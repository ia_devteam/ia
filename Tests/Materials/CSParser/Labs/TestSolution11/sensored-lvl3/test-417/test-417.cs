using System;
using blah;
namespace blah2
{
  public class MyClass
  {
    public event MyFunnyDelegate DoSomething;
    public void DoSomethingFunny()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(97);
      if (DoSomething != null) {
        DoSomething(this, "hello there", "my friend");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(98);
    }
    public static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(99);
      MyClass mc = new MyClass();
      mc.DoSomethingFunny();
      IACSharpSensor.IACSharpSensor.SensorReached(100);
    }
  }
}
