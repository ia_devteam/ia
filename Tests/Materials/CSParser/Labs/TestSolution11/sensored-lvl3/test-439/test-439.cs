using System;
public struct LayerMask
{
  private ushort mask;
  public static implicit operator int(LayerMask mask)
  {
    System.Int32 RNTRNTRNT_70 = (int)mask.mask;
    IACSharpSensor.IACSharpSensor.SensorReached(161);
    return RNTRNTRNT_70;
  }
  public static implicit operator LayerMask(int intVal)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    LayerMask mask;
    mask.mask = unchecked((ushort)intVal);
    LayerMask RNTRNTRNT_71 = mask;
    IACSharpSensor.IACSharpSensor.SensorReached(163);
    return RNTRNTRNT_71;
  }
}
class Test
{
  private static LayerMask test;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(164);
    LayerMask a = ~test;
    if (a != 0xffff) {
      throw new Exception("");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(165);
  }
}
