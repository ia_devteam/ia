unsafe public struct B
{
  private fixed int a[5];
}
unsafe public class C
{
  private B x;
  public void Goo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(234);
    fixed (B* y = &x) {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(235);
  }
  public static void Main()
  {
  }
}
