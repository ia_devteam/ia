using System.Reflection;
using System;
class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(230);
    string[] s = typeof(C).Assembly.GetManifestResourceNames();
    if (s[0] != "test-465.cs") {
      System.Int32 RNTRNTRNT_91 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(231);
      return RNTRNTRNT_91;
    }
    if (typeof(C).Assembly.GetManifestResourceStream("test-465.cs") == null) {
      System.Int32 RNTRNTRNT_92 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(232);
      return RNTRNTRNT_92;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_93 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(233);
    return RNTRNTRNT_93;
  }
}
