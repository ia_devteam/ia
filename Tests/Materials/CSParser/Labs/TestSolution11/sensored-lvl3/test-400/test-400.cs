using System;
unsafe struct Y
{
  public int a;
  public int s;
}
unsafe class X
{
  static int TestDereference()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
    Y y;
    Y* z;
    Y a;
    z = &y;
    y.a = 1;
    y.s = 2;
    a.a = z->a;
    a.s = z->s;
    if (a.a != y.a) {
      System.Int32 RNTRNTRNT_1 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      return RNTRNTRNT_1;
    }
    if (a.s != y.s) {
      System.Int32 RNTRNTRNT_2 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(3);
      return RNTRNTRNT_2;
    }
    System.Int32 RNTRNTRNT_3 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(4);
    return RNTRNTRNT_3;
  }
  static int TestPtrAdd()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(5);
    int[] a = new int[10];
    int i;
    for (i = 0; i < 10; i++) {
      a[i] = i;
    }
    i = 0;
    fixed (int* b = &a[0]) {
      int* p = b;
      for (i = 0; i < 10; i++) {
        if (*p != a[i]) {
          System.Int32 RNTRNTRNT_4 = 10 + i;
          IACSharpSensor.IACSharpSensor.SensorReached(6);
          return RNTRNTRNT_4;
        }
        p++;
      }
    }
    System.Int32 RNTRNTRNT_5 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(7);
    return RNTRNTRNT_5;
  }
  static int i = 1;
  static char c = 'a';
  static long l = 123;
  static double d = 1.2;
  static float f = 1.3f;
  static short s = 4;
  static int TestPtrAssign()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(8);
    fixed (int* ii = &i) {
      *ii = 10;
    }
    fixed (char* cc = &c) {
      *cc = 'b';
    }
    fixed (long* ll = &l) {
      *ll = 100;
    }
    fixed (double* dd = &d) {
      *dd = 3.0;
    }
    fixed (float* ff = &f) {
      *ff = 1.2f;
    }
    fixed (short* ss = &s) {
      *ss = 102;
    }
    if (i != 10) {
      System.Int32 RNTRNTRNT_6 = 100;
      IACSharpSensor.IACSharpSensor.SensorReached(9);
      return RNTRNTRNT_6;
    }
    if (c != 'b') {
      System.Int32 RNTRNTRNT_7 = 101;
      IACSharpSensor.IACSharpSensor.SensorReached(10);
      return RNTRNTRNT_7;
    }
    if (l != 100) {
      System.Int32 RNTRNTRNT_8 = 102;
      IACSharpSensor.IACSharpSensor.SensorReached(11);
      return RNTRNTRNT_8;
    }
    if (d != 3.0) {
      System.Int32 RNTRNTRNT_9 = 103;
      IACSharpSensor.IACSharpSensor.SensorReached(12);
      return RNTRNTRNT_9;
    }
    if (f != 1.2f) {
      System.Int32 RNTRNTRNT_10 = 104;
      IACSharpSensor.IACSharpSensor.SensorReached(13);
      return RNTRNTRNT_10;
    }
    if (s != 102) {
      System.Int32 RNTRNTRNT_11 = 105;
      IACSharpSensor.IACSharpSensor.SensorReached(14);
      return RNTRNTRNT_11;
    }
    System.Int32 RNTRNTRNT_12 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(15);
    return RNTRNTRNT_12;
  }
  static int TestPtrArithmetic()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    char[] array = new char[10];
    char* pb;
    array[5] = 'j';
    fixed (char* pa = array) {
      pb = pa + 1;
      if (pa[5] != 'j') {
        System.Int32 RNTRNTRNT_13 = 199;
        IACSharpSensor.IACSharpSensor.SensorReached(17);
        return RNTRNTRNT_13;
      }
      Console.WriteLine("V: " + (pb - pa));
      if ((pb - pa) != 1) {
        System.Int32 RNTRNTRNT_14 = 200;
        IACSharpSensor.IACSharpSensor.SensorReached(18);
        return RNTRNTRNT_14;
      }
      pb++;
      if (pb == pa) {
        System.Int32 RNTRNTRNT_15 = 201;
        IACSharpSensor.IACSharpSensor.SensorReached(19);
        return RNTRNTRNT_15;
      }
      if (pb < pa) {
        System.Int32 RNTRNTRNT_16 = 202;
        IACSharpSensor.IACSharpSensor.SensorReached(20);
        return RNTRNTRNT_16;
      }
      if (pa > pb) {
        System.Int32 RNTRNTRNT_17 = 203;
        IACSharpSensor.IACSharpSensor.SensorReached(21);
        return RNTRNTRNT_17;
      }
      if (pa >= pb) {
        System.Int32 RNTRNTRNT_18 = 204;
        IACSharpSensor.IACSharpSensor.SensorReached(22);
        return RNTRNTRNT_18;
      }
      if (pb <= pa) {
        System.Int32 RNTRNTRNT_19 = 205;
        IACSharpSensor.IACSharpSensor.SensorReached(23);
        return RNTRNTRNT_19;
      }
      pb = pb - 2;
      if (pb != pa) {
        Console.WriteLine("VV: " + (pb - pa));
        System.Int32 RNTRNTRNT_20 = 206;
        IACSharpSensor.IACSharpSensor.SensorReached(24);
        return RNTRNTRNT_20;
      }
    }
    System.Int32 RNTRNTRNT_21 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(25);
    return RNTRNTRNT_21;
  }
  static int TestMultiple()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(26);
    char[] array = new char[10];
    int count = 0;
    fixed (char* pa = array, pb = array) {
      count++;
    }
    if (count != 1) {
      System.Int32 RNTRNTRNT_22 = 300;
      IACSharpSensor.IACSharpSensor.SensorReached(27);
      return RNTRNTRNT_22;
    }
    System.Int32 RNTRNTRNT_23 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(28);
    return RNTRNTRNT_23;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(29);
    int v;
    if ((v = TestDereference()) != 0) {
      System.Int32 RNTRNTRNT_24 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(30);
      return RNTRNTRNT_24;
    }
    if ((v = TestPtrAdd()) != 0) {
      System.Int32 RNTRNTRNT_25 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(31);
      return RNTRNTRNT_25;
    }
    if ((v = TestPtrAssign()) != 0) {
      System.Int32 RNTRNTRNT_26 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(32);
      return RNTRNTRNT_26;
    }
    if ((v = TestPtrArithmetic()) != 0) {
      System.Int32 RNTRNTRNT_27 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(33);
      return RNTRNTRNT_27;
    }
    if ((v = TestMultiple()) != 0) {
      System.Int32 RNTRNTRNT_28 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(34);
      return RNTRNTRNT_28;
    }
    Console.WriteLine("Ok");
    System.Int32 RNTRNTRNT_29 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(35);
    return RNTRNTRNT_29;
  }
}
