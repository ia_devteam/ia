using System;
unsafe class Foo
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(44);
    int a;
    int* b;
    int** c;
    a = 42;
    b = &a;
    c = &b;
    Console.WriteLine("*c == b : {0}", *c == b);
    Console.WriteLine("**c == a : {0}", **c == a);
    if (*c == b && **c == a) {
      Console.WriteLine("Test passed");
      System.Int32 RNTRNTRNT_36 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(45);
      return RNTRNTRNT_36;
    } else {
      Console.WriteLine("Test failed");
      System.Int32 RNTRNTRNT_37 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(46);
      return RNTRNTRNT_37;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(47);
  }
}
