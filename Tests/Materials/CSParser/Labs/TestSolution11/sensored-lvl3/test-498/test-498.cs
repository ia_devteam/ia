using System.Reflection;
using System.Runtime.CompilerServices;
struct C
{
  [MethodImplAttribute(MethodImplOptions.InternalCall)]
  public extern C(float value);
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(326);
    MethodImplAttributes iflags = typeof(C).GetConstructors()[0].GetMethodImplementationFlags();
    if ((iflags & MethodImplAttributes.InternalCall) == 0) {
      System.Int32 RNTRNTRNT_120 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(327);
      return RNTRNTRNT_120;
    }
    System.Int32 RNTRNTRNT_121 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(328);
    return RNTRNTRNT_121;
  }
}
