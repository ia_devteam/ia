using System;
delegate void Del(int n);
class Lambda
{
  static int v;
  static void f(int va)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(243);
    v = va;
    IACSharpSensor.IACSharpSensor.SensorReached(244);
  }
  static Del[] Make2(int x)
  {
    Del[] RNTRNTRNT_96 = new Del[] {
      delegate(int a) { f(x += a); },
      delegate(int b) { f(x += b); }
    };
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    return RNTRNTRNT_96;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(246);
    Del[] d = Make2(10);
    d[0](10);
    if (v != 20) {
      System.Int32 RNTRNTRNT_97 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(247);
      return RNTRNTRNT_97;
    }
    d[1](11);
    if (v != 31) {
      System.Int32 RNTRNTRNT_98 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(248);
      return RNTRNTRNT_98;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_99 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(249);
    return RNTRNTRNT_99;
  }
}
