using System;
public class ExceptionWithAnonMethod
{
  public delegate void EmptyCallback();
  static string res;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(236);
    try {
      throw new Exception("e is afraid to enter anonymous land");
    } catch (Exception e) {
      AnonHandler(delegate {
        Console.WriteLine(e.Message);
        res = e.Message;
      });
    }
    if (res == "e is afraid to enter anonymous land") {
      Console.WriteLine("Test passed");
      System.Int32 RNTRNTRNT_94 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(237);
      return RNTRNTRNT_94;
    }
    Console.WriteLine("Test failed");
    System.Int32 RNTRNTRNT_95 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    return RNTRNTRNT_95;
  }
  public static void AnonHandler(EmptyCallback handler)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(239);
    if (handler != null) {
      handler();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(240);
  }
}
