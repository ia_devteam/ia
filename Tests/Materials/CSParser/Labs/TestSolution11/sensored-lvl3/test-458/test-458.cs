using System;
internal class MyAttr : Attribute
{
  internal MyAttr()
  {
  }
  internal MyAttr(Type type)
  {
  }
  internal MyAttr(string name)
  {
  }
  internal MyAttr(int i)
  {
  }
}
[MyAttr()]
internal class ClassA
{
}
[MyAttr(typeof(string))]
internal class ClassB
{
}
[MyAttr("abc")]
internal class ClassC
{
}
[MyAttr(3)]
internal class ClassD
{
}
internal class Top
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(209);
    if (typeof(ClassA).GetCustomAttributes(false).Length != 1) {
      System.Int32 RNTRNTRNT_79 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(210);
      return RNTRNTRNT_79;
    }
    System.Int32 RNTRNTRNT_80 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(211);
    return RNTRNTRNT_80;
  }
}
