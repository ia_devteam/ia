using System;
using System.Reflection;
class Test
{
  public int Prop {
    get {
      System.Int32 RNTRNTRNT_101 = prop;
      IACSharpSensor.IACSharpSensor.SensorReached(252);
      return RNTRNTRNT_101;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(253);
      prop = value;
      IACSharpSensor.IACSharpSensor.SensorReached(254);
    }
  }
  int prop = 0;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(255);
    MethodInfo mi = typeof(Test).GetMethod("set_Prop");
    if (mi.GetParameters().Length != 1) {
      System.Int32 RNTRNTRNT_102 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(256);
      return RNTRNTRNT_102;
    }
    if ((mi.GetParameters()[0].Name) != "value") {
      System.Int32 RNTRNTRNT_103 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(257);
      return RNTRNTRNT_103;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_104 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(258);
    return RNTRNTRNT_104;
  }
}
