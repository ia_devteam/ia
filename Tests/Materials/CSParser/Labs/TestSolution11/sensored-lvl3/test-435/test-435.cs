using foo = System.Collections;
class X : foo.IEnumerable
{
  System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(151);
    return null;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(152);
    System.Collections.IEnumerable x = new X();
    IACSharpSensor.IACSharpSensor.SensorReached(153);
  }
}
