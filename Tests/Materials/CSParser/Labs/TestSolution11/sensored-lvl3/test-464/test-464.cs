using System.Reflection;
using System;
class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(226);
    string[] s = typeof(C).Assembly.GetManifestResourceNames();
    if (s[0] != "TEST") {
      System.Int32 RNTRNTRNT_88 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(227);
      return RNTRNTRNT_88;
    }
    if (typeof(C).Assembly.GetManifestResourceStream("TEST") == null) {
      System.Int32 RNTRNTRNT_89 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(228);
      return RNTRNTRNT_89;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_90 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(229);
    return RNTRNTRNT_90;
  }
}
