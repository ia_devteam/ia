using System;
using System.Collections;
class XX
{
  static void Metodo(Exception e)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(125);
    if (e is NotImplementedException) {
      Console.WriteLine("OK");
    } else {
      Console.WriteLine("Fail");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(126);
  }
  static IEnumerable X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(127);
    try {
      throw new NotImplementedException();
    } catch (Exception e) {
      Metodo(e);
    }
    IEnumerable RNTRNTRNT_69 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(128);
    yield return RNTRNTRNT_69;
    IACSharpSensor.IACSharpSensor.SensorReached(129);
    IACSharpSensor.IACSharpSensor.SensorReached(130);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(131);
    foreach (int a in X()) {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(132);
  }
}
