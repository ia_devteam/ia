using System;
enum E : uint
{
  Value = 24
}
class A
{
  public static implicit operator sbyte(A mask)
  {
    System.SByte RNTRNTRNT_54 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(205);
    return RNTRNTRNT_54;
  }
  public static implicit operator byte(A mask)
  {
    System.Byte RNTRNTRNT_55 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    return RNTRNTRNT_55;
  }
  public static implicit operator short(A mask)
  {
    System.Int16 RNTRNTRNT_56 = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(207);
    return RNTRNTRNT_56;
  }
  public static implicit operator ushort(A mask)
  {
    System.UInt16 RNTRNTRNT_57 = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(208);
    return RNTRNTRNT_57;
  }
  public static implicit operator int(A mask)
  {
    System.Int32 RNTRNTRNT_58 = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(209);
    return RNTRNTRNT_58;
  }
  public static implicit operator uint(A mask)
  {
    System.UInt32 RNTRNTRNT_59 = 6;
    IACSharpSensor.IACSharpSensor.SensorReached(210);
    return RNTRNTRNT_59;
  }
  public static implicit operator long(A mask)
  {
    System.Int64 RNTRNTRNT_60 = 7;
    IACSharpSensor.IACSharpSensor.SensorReached(211);
    return RNTRNTRNT_60;
  }
  public static implicit operator ulong(A mask)
  {
    System.UInt64 RNTRNTRNT_61 = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(212);
    return RNTRNTRNT_61;
  }
}
class A2
{
  public static implicit operator sbyte(A2 mask)
  {
    System.SByte RNTRNTRNT_62 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(213);
    return RNTRNTRNT_62;
  }
  public static implicit operator byte(A2 mask)
  {
    System.Byte RNTRNTRNT_63 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(214);
    return RNTRNTRNT_63;
  }
  public static implicit operator short(A2 mask)
  {
    System.Int16 RNTRNTRNT_64 = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    return RNTRNTRNT_64;
  }
  public static implicit operator uint(A2 mask)
  {
    System.UInt32 RNTRNTRNT_65 = 6;
    IACSharpSensor.IACSharpSensor.SensorReached(216);
    return RNTRNTRNT_65;
  }
  public static implicit operator long(A2 mask)
  {
    System.Int64 RNTRNTRNT_66 = 7;
    IACSharpSensor.IACSharpSensor.SensorReached(217);
    return RNTRNTRNT_66;
  }
  public static implicit operator ulong(A2 mask)
  {
    System.UInt64 RNTRNTRNT_67 = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(218);
    return RNTRNTRNT_67;
  }
}
class A3
{
  public static implicit operator sbyte(A3 mask)
  {
    System.SByte RNTRNTRNT_68 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(219);
    return RNTRNTRNT_68;
  }
  public static implicit operator uint(A3 mask)
  {
    System.UInt32 RNTRNTRNT_69 = 6;
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    return RNTRNTRNT_69;
  }
  public static implicit operator long(A3 mask)
  {
    System.Int64 RNTRNTRNT_70 = 7;
    IACSharpSensor.IACSharpSensor.SensorReached(221);
    return RNTRNTRNT_70;
  }
  public static implicit operator ulong(A3 mask)
  {
    System.UInt64 RNTRNTRNT_71 = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(222);
    return RNTRNTRNT_71;
  }
}
class A4
{
  public static implicit operator uint(A4 mask)
  {
    System.UInt32 RNTRNTRNT_72 = 6;
    IACSharpSensor.IACSharpSensor.SensorReached(223);
    return RNTRNTRNT_72;
  }
  public static implicit operator long(A4 mask)
  {
    System.Int64 RNTRNTRNT_73 = 7;
    IACSharpSensor.IACSharpSensor.SensorReached(224);
    return RNTRNTRNT_73;
  }
  public static implicit operator ulong(A4 mask)
  {
    System.UInt64 RNTRNTRNT_74 = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(225);
    return RNTRNTRNT_74;
  }
}
class A5
{
  public static implicit operator uint(A5 mask)
  {
    System.UInt32 RNTRNTRNT_75 = 6;
    IACSharpSensor.IACSharpSensor.SensorReached(226);
    return RNTRNTRNT_75;
  }
  public static implicit operator int(A5 mask)
  {
    System.Int32 RNTRNTRNT_76 = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(227);
    return RNTRNTRNT_76;
  }
}
class A6
{
  public static implicit operator byte(A6 mask)
  {
    System.Byte RNTRNTRNT_77 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(228);
    return RNTRNTRNT_77;
  }
}
class MyDecimal
{
  public static implicit operator decimal(MyDecimal d)
  {
    System.Decimal RNTRNTRNT_78 = 42;
    IACSharpSensor.IACSharpSensor.SensorReached(229);
    return RNTRNTRNT_78;
  }
}
public class Constraint
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(230);
    A a = null;
    A2 a2 = null;
    A3 a3 = null;
    A4 a4 = null;
    A5 a5 = null;
    A6 a6 = null;
    IACSharpSensor.IACSharpSensor.SensorReached(231);
    if (-a != -5) {
      System.Int32 RNTRNTRNT_79 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(232);
      return RNTRNTRNT_79;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(233);
    if (-a2 != -3) {
      System.Int32 RNTRNTRNT_80 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(234);
      return RNTRNTRNT_80;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(235);
    if (-a3 != -1) {
      System.Int32 RNTRNTRNT_81 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(236);
      return RNTRNTRNT_81;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(237);
    if (-a4 != -7) {
      System.Int32 RNTRNTRNT_82 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(238);
      return RNTRNTRNT_82;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(239);
    if (-a5 != -8) {
      System.Int32 RNTRNTRNT_83 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(240);
      return RNTRNTRNT_83;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(241);
    if (-a6 != -2) {
      System.Int32 RNTRNTRNT_84 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(242);
      return RNTRNTRNT_84;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(243);
    if (~a != -6) {
      System.Int32 RNTRNTRNT_85 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(244);
      return RNTRNTRNT_85;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    if (~a2 != -4) {
      System.Int32 RNTRNTRNT_86 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(246);
      return RNTRNTRNT_86;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(247);
    if (~a3 != -2) {
      System.Int32 RNTRNTRNT_87 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(248);
      return RNTRNTRNT_87;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(249);
    if (~a4 != 4294967289u) {
      System.Int32 RNTRNTRNT_88 = 13;
      IACSharpSensor.IACSharpSensor.SensorReached(250);
      return RNTRNTRNT_88;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(251);
    if (~a5 != -9) {
      System.Int32 RNTRNTRNT_89 = 14;
      IACSharpSensor.IACSharpSensor.SensorReached(252);
      return RNTRNTRNT_89;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(253);
    if (~a6 != -3) {
      System.Int32 RNTRNTRNT_90 = 15;
      IACSharpSensor.IACSharpSensor.SensorReached(254);
      return RNTRNTRNT_90;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(255);
    MyDecimal d = new MyDecimal();
    IACSharpSensor.IACSharpSensor.SensorReached(256);
    if (-d != -42) {
      System.Int32 RNTRNTRNT_91 = 20;
      IACSharpSensor.IACSharpSensor.SensorReached(257);
      return RNTRNTRNT_91;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(258);
    E e = E.Value;
    IACSharpSensor.IACSharpSensor.SensorReached(259);
    if (~e != (E)4294967271u) {
      System.Int32 RNTRNTRNT_92 = 21;
      IACSharpSensor.IACSharpSensor.SensorReached(260);
      return RNTRNTRNT_92;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(261);
    uint dp = 0;
    dp = +dp;
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_93 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(262);
    return RNTRNTRNT_93;
  }
}
