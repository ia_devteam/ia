interface I
{
  void a();
}
abstract class X : I
{
  public abstract void a();
}
class Y : X
{
  public override void a()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(146);
    System.Console.WriteLine("Hello!");
    IACSharpSensor.IACSharpSensor.SensorReached(147);
    return;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(148);
    Y y = new Y();
    ((I)y).a();
    IACSharpSensor.IACSharpSensor.SensorReached(149);
  }
}
