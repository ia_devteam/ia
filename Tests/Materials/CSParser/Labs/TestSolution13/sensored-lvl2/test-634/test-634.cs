public class Test
{
  delegate void D();
  static D d;
  public static void TestFunc()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(275);
    return;
    IACSharpSensor.IACSharpSensor.SensorReached(276);
    string testStr;
    d += delegate() { testStr = "sss"; };
    IACSharpSensor.IACSharpSensor.SensorReached(277);
  }
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(278);
    TestFunc();
    IACSharpSensor.IACSharpSensor.SensorReached(279);
  }
}
