class Foo
{
  bool got;
  string s {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(295);
      got = true;
      System.String RNTRNTRNT_96 = "";
      IACSharpSensor.IACSharpSensor.SensorReached(296);
      return RNTRNTRNT_96;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(297);
      if (!got || value != "A1B2") {
        IACSharpSensor.IACSharpSensor.SensorReached(298);
        throw new System.Exception();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(299);
    }
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(300);
    (new Foo()).s += "A" + 1 + "B" + 2;
    IACSharpSensor.IACSharpSensor.SensorReached(301);
  }
}
