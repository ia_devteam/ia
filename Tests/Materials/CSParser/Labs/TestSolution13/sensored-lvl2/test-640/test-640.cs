enum MyEnum : byte
{
  Value_1 = 1
}
public class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(302);
    MyEnum me = MyEnum.Value_1;
    MyEnum b = ~me;
    IACSharpSensor.IACSharpSensor.SensorReached(303);
    if (b != (MyEnum)254) {
      System.Int32 RNTRNTRNT_97 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(304);
      return RNTRNTRNT_97;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(305);
    byte r = b - me;
    IACSharpSensor.IACSharpSensor.SensorReached(306);
    if (r != 253) {
      System.Int32 RNTRNTRNT_98 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(307);
      return RNTRNTRNT_98;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(308);
    b = b - 2;
    IACSharpSensor.IACSharpSensor.SensorReached(309);
    if (b != (MyEnum)252) {
      System.Int32 RNTRNTRNT_99 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(310);
      return RNTRNTRNT_99;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(311);
    me -= MyEnum.Value_1;
    b = (MyEnum)255;
    b &= ~MyEnum.Value_1;
    IACSharpSensor.IACSharpSensor.SensorReached(312);
    if (b != (MyEnum)254) {
      System.Int32 RNTRNTRNT_100 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(313);
      return RNTRNTRNT_100;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(314);
    System.Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_101 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(315);
    return RNTRNTRNT_101;
  }
}
