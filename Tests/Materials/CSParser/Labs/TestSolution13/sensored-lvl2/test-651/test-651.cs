using System.Collections;
class Foo
{
  void Open(IList a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(406);
  }
  void Open(out ArrayList a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    a = null;
    Open((IList)a);
    Open(a);
    IACSharpSensor.IACSharpSensor.SensorReached(408);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(409);
  }
}
