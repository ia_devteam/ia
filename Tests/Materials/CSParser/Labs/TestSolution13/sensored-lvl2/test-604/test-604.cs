using System;
using System.Reflection;
class Program
{
  interface Iface1
  {
    void IfaceMethod1();
  }
  interface Iface2
  {
    void IfaceMethod2();
  }
  public class ImplementingExplicitInterfacesMembers : Iface1, Iface2
  {
    void Iface1.IfaceMethod1()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(35);
    }
    void Iface2.IfaceMethod2()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(36);
    }
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(37);
    object[] o = typeof(ImplementingExplicitInterfacesMembers).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);
    IACSharpSensor.IACSharpSensor.SensorReached(38);
    foreach (MethodInfo mi in o) {
      IACSharpSensor.IACSharpSensor.SensorReached(39);
      if (mi.Name.IndexOf('+') != -1) {
        System.Int32 RNTRNTRNT_20 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(40);
        return RNTRNTRNT_20;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(41);
      Console.WriteLine(mi.Name);
    }
    System.Int32 RNTRNTRNT_21 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    return RNTRNTRNT_21;
  }
}
