using System;
using System.Reflection;
public delegate long MyDelegate();
public interface X
{
  event MyDelegate Foo;
  int Prop { get; }
}
public class Y : X
{
  event MyDelegate X.Foo {
    add { }
    remove { }
  }
  int X.Prop {
    get {
      System.Int32 RNTRNTRNT_41 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(119);
      return RNTRNTRNT_41;
    }
  }
  public event MyDelegate Foo;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(120);
    MethodInfo o = typeof(Y).GetMethod("X.add_Foo", BindingFlags.NonPublic | BindingFlags.Instance);
    IACSharpSensor.IACSharpSensor.SensorReached(121);
    if (o == null) {
      System.Int32 RNTRNTRNT_42 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(122);
      return RNTRNTRNT_42;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(123);
    o = typeof(Y).GetMethod("X.get_Prop", BindingFlags.NonPublic | BindingFlags.Instance);
    IACSharpSensor.IACSharpSensor.SensorReached(124);
    if (o == null) {
      System.Int32 RNTRNTRNT_43 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(125);
      return RNTRNTRNT_43;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(126);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_44 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(127);
    return RNTRNTRNT_44;
  }
}
