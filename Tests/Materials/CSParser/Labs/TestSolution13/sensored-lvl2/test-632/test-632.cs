using System;
class Bug379822
{
  static void Assert(bool expected, bool value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(263);
    if (value != expected) {
      IACSharpSensor.IACSharpSensor.SensorReached(264);
      throw new Exception("unexpected value");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(265);
  }
  static void TestAnd(bool var)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(266);
    Assert(false, false && var);
    Assert(false, var && false);
    Assert(false, false & var);
    Assert(false, var & false);
    Assert(var, true && var);
    Assert(var, var && true);
    Assert(var, true & var);
    Assert(var, var & true);
    IACSharpSensor.IACSharpSensor.SensorReached(267);
  }
  static void TestOr(bool var)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(268);
    Assert(var, false || var);
    Assert(var, var || false);
    Assert(var, false | var);
    Assert(var, var | false);
    Assert(true, true || var);
    Assert(true, var || true);
    Assert(true, true | var);
    Assert(true, var | true);
    IACSharpSensor.IACSharpSensor.SensorReached(269);
  }
  static void Test(bool var)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(270);
    TestAnd(var);
    TestOr(var);
    IACSharpSensor.IACSharpSensor.SensorReached(271);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(272);
    Test(false);
    Test(true);
    IACSharpSensor.IACSharpSensor.SensorReached(273);
  }
}
