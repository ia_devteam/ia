using System.Collections;
class Foo
{
  public static IEnumerable foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(171);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(172);
      yield break;
    } catch {
    } finally {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(173);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(174);
    int i = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(175);
    foreach (object o in foo()) {
      IACSharpSensor.IACSharpSensor.SensorReached(176);
      ++i;
    }
    System.Int32 RNTRNTRNT_49 = i;
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    return RNTRNTRNT_49;
  }
}
