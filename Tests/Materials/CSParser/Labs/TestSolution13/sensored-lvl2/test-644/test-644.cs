using System;
class Program
{
  public delegate object D(int member);
  private D _value;
  private object M(int member)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(351);
    return null;
  }
  void Test_1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(352);
    Delegate d1 = M + _value;
    Delegate d2 = _value + M;
    IACSharpSensor.IACSharpSensor.SensorReached(353);
  }
  public bool Test_2()
  {
    System.Boolean RNTRNTRNT_120 = _value == M;
    IACSharpSensor.IACSharpSensor.SensorReached(354);
    return RNTRNTRNT_120;
  }
  public bool Test_3()
  {
    System.Boolean RNTRNTRNT_121 = _value != M;
    IACSharpSensor.IACSharpSensor.SensorReached(355);
    return RNTRNTRNT_121;
  }
  public bool Test_4(D d)
  {
    System.Boolean RNTRNTRNT_122 = d == _value;
    IACSharpSensor.IACSharpSensor.SensorReached(356);
    return RNTRNTRNT_122;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(357);
    Program p = new Program();
    IACSharpSensor.IACSharpSensor.SensorReached(358);
    if (p.Test_2()) {
      System.Int32 RNTRNTRNT_123 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(359);
      return RNTRNTRNT_123;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(360);
    p._value = p.M;
    IACSharpSensor.IACSharpSensor.SensorReached(361);
    if (!p.Test_2()) {
      System.Int32 RNTRNTRNT_124 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(362);
      return RNTRNTRNT_124;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(363);
    if (p.Test_3()) {
      System.Int32 RNTRNTRNT_125 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(364);
      return RNTRNTRNT_125;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(365);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_126 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(366);
    return RNTRNTRNT_126;
  }
}
