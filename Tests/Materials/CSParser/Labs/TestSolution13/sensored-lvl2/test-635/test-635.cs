using System;
class ShortCircuitFold
{
  static int calls;
  static bool False {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(280);
      ++calls;
      System.Boolean RNTRNTRNT_94 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(281);
      return RNTRNTRNT_94;
    }
  }
  static bool True {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(282);
      ++calls;
      System.Boolean RNTRNTRNT_95 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(283);
      return RNTRNTRNT_95;
    }
  }
  static void a(bool e, bool v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(284);
    if (e != v) {
      IACSharpSensor.IACSharpSensor.SensorReached(285);
      throw new Exception("unexpected value");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(286);
  }
  static void c(int e)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(287);
    if (e != calls) {
      IACSharpSensor.IACSharpSensor.SensorReached(288);
      throw new Exception("call count mismatch: expected " + e + " but got " + calls);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(289);
  }
  static bool f()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(290);
    throw new Exception("not short circuited out");
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(291);
    a(false, false && f());
    a(true, true || f());
    a(false, false && False);
    c(0);
    a(true, true || True);
    c(0);
    a(false, true && False);
    c(1);
    a(true, false || True);
    c(2);
    a(false, false & False);
    c(3);
    a(true, true | True);
    c(4);
    a(false, true & False);
    c(5);
    a(true, false | True);
    c(6);
    IACSharpSensor.IACSharpSensor.SensorReached(292);
  }
}
