using System;
using System.Collections;
using System.Reflection;
using Mono.Test;
class Program
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(46);
    BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly;
    Type type = typeof(Info);
    PropertyInfo[] properties = type.GetProperties(flags);
    IACSharpSensor.IACSharpSensor.SensorReached(47);
    if (properties.Length != 2) {
      IACSharpSensor.IACSharpSensor.SensorReached(48);
      Console.WriteLine("#1: " + properties.Length.ToString());
      System.Int32 RNTRNTRNT_22 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(49);
      return RNTRNTRNT_22;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(50);
    if (properties[0].Name != "System.Collections.IEnumerator.Current") {
      IACSharpSensor.IACSharpSensor.SensorReached(51);
      Console.WriteLine("#2: " + properties[0].Name);
      System.Int32 RNTRNTRNT_23 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(52);
      return RNTRNTRNT_23;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    if (properties[1].Name != "Mono.Test.ITest.Item") {
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      Console.WriteLine("#3: " + properties[1].Name);
      System.Int32 RNTRNTRNT_24 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(55);
      return RNTRNTRNT_24;
    }
    System.Int32 RNTRNTRNT_25 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(56);
    return RNTRNTRNT_25;
  }
}
namespace Mono.Test
{
  interface ITest
  {
    object this[int index] { get; set; }
  }
}
class Info : IEnumerator, ITest
{
  object IEnumerator.Current {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(57);
      return null;
    }
  }
  bool IEnumerator.MoveNext()
  {
    System.Boolean RNTRNTRNT_26 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(58);
    return RNTRNTRNT_26;
  }
  void IEnumerator.Reset()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(59);
  }
  object ITest.this[int index] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      return null;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(61); }
  }
}
