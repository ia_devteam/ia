using System;
interface A
{
  void B();
}
interface X
{
  void B();
}
class B : A, X
{
  void X.B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(165);
  }
  void A.B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(166);
  }
}
namespace N
{
  interface B
  {
  }
}
class M
{
  static void N(object N)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(167);
    object x = (N.B)N;
    IACSharpSensor.IACSharpSensor.SensorReached(168);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(169);
  }
}
