using System;
public class MyEx : Exception
{
  public MyEx()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(178);
  }
}
public class Ex
{
  public static int test(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(179);
    int res;
    int fin = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(180);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(181);
      res = 10 / a;
      IACSharpSensor.IACSharpSensor.SensorReached(182);
      throw new MyEx();
    } catch (Exception ex) {
      IACSharpSensor.IACSharpSensor.SensorReached(183);
      ex = new MyEx();
      IACSharpSensor.IACSharpSensor.SensorReached(184);
      throw;
      IACSharpSensor.IACSharpSensor.SensorReached(185);
    } finally {
      IACSharpSensor.IACSharpSensor.SensorReached(186);
      fin = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(187);
    }
    System.Int32 RNTRNTRNT_50 = res;
    IACSharpSensor.IACSharpSensor.SensorReached(188);
    return RNTRNTRNT_50;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(189);
    int catched = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(190);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(191);
      test(1);
    } catch (MyEx ex) {
      IACSharpSensor.IACSharpSensor.SensorReached(192);
      catched = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(193);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(194);
    if (catched != 1) {
      System.Int32 RNTRNTRNT_51 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(195);
      return RNTRNTRNT_51;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(196);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(197);
      test(0);
    } catch (MyEx ex) {
      IACSharpSensor.IACSharpSensor.SensorReached(198);
      catched = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(199);
    } catch {
      IACSharpSensor.IACSharpSensor.SensorReached(200);
      catched = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(201);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(202);
    if (catched != 3) {
      System.Int32 RNTRNTRNT_52 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(203);
      return RNTRNTRNT_52;
    }
    System.Int32 RNTRNTRNT_53 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(204);
    return RNTRNTRNT_53;
  }
}
