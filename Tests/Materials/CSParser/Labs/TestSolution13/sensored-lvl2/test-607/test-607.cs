using System;
using System.Reflection;
[assembly: AssemblyVersion("7")]
class Program
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    Assembly a = Assembly.GetExecutingAssembly();
    IACSharpSensor.IACSharpSensor.SensorReached(63);
    if (a.GetName().Version != new Version(7, 0, 0, 0)) {
      System.Int32 RNTRNTRNT_27 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(64);
      return RNTRNTRNT_27;
    }
    System.Int32 RNTRNTRNT_28 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(65);
    return RNTRNTRNT_28;
  }
}
