using System;
class Program
{
  static long CastUIntPtrToInt64(UIntPtr ptr)
  {
    System.Int64 RNTRNTRNT_128 = (long)ptr;
    IACSharpSensor.IACSharpSensor.SensorReached(379);
    return RNTRNTRNT_128;
  }
  static uint CastIntPtrToUInt32(IntPtr ptr)
  {
    System.UInt32 RNTRNTRNT_129 = (uint)ptr;
    IACSharpSensor.IACSharpSensor.SensorReached(380);
    return RNTRNTRNT_129;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(381);
    if (IntPtr.Size < 8) {
      IACSharpSensor.IACSharpSensor.SensorReached(382);
      if (CastUIntPtrToInt64(new UIntPtr(uint.MaxValue)) != uint.MaxValue) {
        System.Int32 RNTRNTRNT_130 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(383);
        return RNTRNTRNT_130;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(384);
      if (CastIntPtrToUInt32(new IntPtr(int.MaxValue)) != int.MaxValue) {
        System.Int32 RNTRNTRNT_131 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(385);
        return RNTRNTRNT_131;
      }
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(386);
      if (CastUIntPtrToInt64(new UIntPtr(ulong.MaxValue)) != -1) {
        System.Int32 RNTRNTRNT_132 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(387);
        return RNTRNTRNT_132;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(388);
      if (CastIntPtrToUInt32(new IntPtr(long.MaxValue)) != uint.MaxValue) {
        System.Int32 RNTRNTRNT_133 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(389);
        return RNTRNTRNT_133;
      }
    }
    System.Int32 RNTRNTRNT_134 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(390);
    return RNTRNTRNT_134;
  }
}
