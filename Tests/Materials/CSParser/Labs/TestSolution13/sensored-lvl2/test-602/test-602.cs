using System;
public class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(13);
    int s = Error("{0} - {0}", "a");
    Console.WriteLine(s);
    IACSharpSensor.IACSharpSensor.SensorReached(14);
    if (s != 2) {
      System.Int32 RNTRNTRNT_6 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(15);
      return RNTRNTRNT_6;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    s = Test_A("aaaa");
    IACSharpSensor.IACSharpSensor.SensorReached(17);
    if (s != 1) {
      System.Int32 RNTRNTRNT_7 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(18);
      return RNTRNTRNT_7;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(19);
    s = Test_C(typeof(C), null, null);
    Console.WriteLine(s);
    IACSharpSensor.IACSharpSensor.SensorReached(20);
    if (s != 2) {
      System.Int32 RNTRNTRNT_8 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(21);
      return RNTRNTRNT_8;
    }
    System.Int32 RNTRNTRNT_9 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(22);
    return RNTRNTRNT_9;
  }
  public static int Error(string format, params object[] args)
  {
    System.Int32 RNTRNTRNT_10 = Format(format, args);
    IACSharpSensor.IACSharpSensor.SensorReached(23);
    return RNTRNTRNT_10;
  }
  static int Format(string s, object o)
  {
    System.Int32 RNTRNTRNT_11 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(24);
    return RNTRNTRNT_11;
  }
  static int Format(string s, params object[] o)
  {
    System.Int32 RNTRNTRNT_12 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(25);
    return RNTRNTRNT_12;
  }
  static int Format(string s, object o, params object[] o2)
  {
    System.Int32 RNTRNTRNT_13 = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(26);
    return RNTRNTRNT_13;
  }
  static int Test_A(string s)
  {
    System.Int32 RNTRNTRNT_14 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(27);
    return RNTRNTRNT_14;
  }
  static int Test_A(string s, params object[] o)
  {
    System.Int32 RNTRNTRNT_15 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(28);
    return RNTRNTRNT_15;
  }
  static int Test_C(Type t, params int[] a)
  {
    System.Int32 RNTRNTRNT_16 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(29);
    return RNTRNTRNT_16;
  }
  static int Test_C(Type t, int[] a, int[] b)
  {
    System.Int32 RNTRNTRNT_17 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(30);
    return RNTRNTRNT_17;
  }
}
