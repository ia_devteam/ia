class X
{
  static void A(ref int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(136);
    a++;
    IACSharpSensor.IACSharpSensor.SensorReached(137);
  }
  static void B(ref int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(138);
    A(ref a);
    IACSharpSensor.IACSharpSensor.SensorReached(139);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(140);
    int a = 10;
    B(ref a);
    IACSharpSensor.IACSharpSensor.SensorReached(141);
    if (a == 11) {
      System.Int32 RNTRNTRNT_45 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(142);
      return RNTRNTRNT_45;
    } else {
      System.Int32 RNTRNTRNT_46 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(143);
      return RNTRNTRNT_46;
    }
  }
}
