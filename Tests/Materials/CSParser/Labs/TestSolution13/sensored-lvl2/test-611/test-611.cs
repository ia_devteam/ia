public class T
{
  protected class Protected
  {
  }
}
public class D : T
{
  private class Private
  {
    public void Stuff(Protected p)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(74);
    }
  }
}
public class D2 : T
{
  public class P
  {
    private class Private
    {
      public void Stuff(Protected p)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(75);
      }
    }
  }
}
public class Z
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(76);
  }
}
