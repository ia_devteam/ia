using System;
namespace ParamMismatch
{
  public class TestCase
  {
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(393);
    }
    public TestCase()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(394);
    }
    public event EventHandler Culprit {
      add { }
      remove { }
    }
    ~TestCase()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(395);
    }
  }
}
