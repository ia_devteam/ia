class TestA
{
  public virtual string Method {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(43);
      return null;
    }
  }
}
class TestB : TestA
{
  private string Method {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(44);
      return null;
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(45);
  }
}
