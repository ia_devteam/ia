using System;
namespace ConsoleApplication1
{
  public struct Strct
  {
    public uint a;
    public uint b;
  }
  unsafe class Program
  {
    static Strct* ptr = null;
    static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(31);
      Strct* values = ptr;
      values++;
      values++;
      long diff = values - ptr;
      IACSharpSensor.IACSharpSensor.SensorReached(32);
      if (diff != 2) {
        System.Int32 RNTRNTRNT_18 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(33);
        return RNTRNTRNT_18;
      }
      System.Int32 RNTRNTRNT_19 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(34);
      return RNTRNTRNT_19;
    }
  }
}
