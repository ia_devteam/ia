using System;
class C
{
  unsafe static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(414);
      uint* i = stackalloc uint[int.MaxValue];
      uint v = 0;
      i[v] = v;
      i[0] = v;
      System.Int32 RNTRNTRNT_140 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(415);
      return RNTRNTRNT_140;
    } catch (OverflowException) {
      System.Int32 RNTRNTRNT_141 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(416);
      return RNTRNTRNT_141;
      IACSharpSensor.IACSharpSensor.SensorReached(417);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(418);
  }
  unsafe static void Test2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(419);
    byte* b = null;
    b = b + (byte)1;
    b = b + (sbyte)1;
    b = b + (short)1;
    b = b + (int)1;
    b = b + (long)1;
    b = b + (ulong)1;
    IACSharpSensor.IACSharpSensor.SensorReached(420);
  }
  unsafe static void Test2(sbyte sb, short s, int i, long l, ulong ul)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(421);
    short* b = null;
    b = b + sb;
    b = b + s;
    b = b + i;
    b = b + l;
    b = b + ul;
    IACSharpSensor.IACSharpSensor.SensorReached(422);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(423);
    Test2();
    Test2(1, 2, 3, 4, 5);
    IACSharpSensor.IACSharpSensor.SensorReached(424);
    if (Test() != 0) {
      System.Int32 RNTRNTRNT_142 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(425);
      return RNTRNTRNT_142;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(426);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_143 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(427);
    return RNTRNTRNT_143;
  }
}
