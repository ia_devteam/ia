public class Test
{
  delegate void D();
  static D d;
  public static void TestFunc()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(168);
    return;
    string testStr;
    d += delegate() { testStr = "sss"; };
    IACSharpSensor.IACSharpSensor.SensorReached(169);
  }
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    TestFunc();
    IACSharpSensor.IACSharpSensor.SensorReached(171);
  }
}
