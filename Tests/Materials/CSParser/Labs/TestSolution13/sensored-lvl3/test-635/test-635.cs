using System;
class ShortCircuitFold
{
  static int calls;
  static bool False {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(172);
      ++calls;
      System.Boolean RNTRNTRNT_94 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(173);
      return RNTRNTRNT_94;
    }
  }
  static bool True {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(174);
      ++calls;
      System.Boolean RNTRNTRNT_95 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(175);
      return RNTRNTRNT_95;
    }
  }
  static void a(bool e, bool v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(176);
    if (e != v) {
      throw new Exception("unexpected value");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(177);
  }
  static void c(int e)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(178);
    if (e != calls) {
      throw new Exception("call count mismatch: expected " + e + " but got " + calls);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(179);
  }
  static bool f()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(180);
    throw new Exception("not short circuited out");
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(181);
    a(false, false && f());
    a(true, true || f());
    a(false, false && False);
    c(0);
    a(true, true || True);
    c(0);
    a(false, true && False);
    c(1);
    a(true, false || True);
    c(2);
    a(false, false & False);
    c(3);
    a(true, true | True);
    c(4);
    a(false, true & False);
    c(5);
    a(true, false | True);
    c(6);
    IACSharpSensor.IACSharpSensor.SensorReached(182);
  }
}
