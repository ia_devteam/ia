using System;
class C
{
  unsafe static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(264);
    try {
      uint* i = stackalloc uint[int.MaxValue];
      uint v = 0;
      i[v] = v;
      i[0] = v;
      System.Int32 RNTRNTRNT_140 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(265);
      return RNTRNTRNT_140;
    } catch (OverflowException) {
      System.Int32 RNTRNTRNT_141 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(266);
      return RNTRNTRNT_141;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(267);
  }
  unsafe static void Test2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(268);
    byte* b = null;
    b = b + (byte)1;
    b = b + (sbyte)1;
    b = b + (short)1;
    b = b + (int)1;
    b = b + (long)1;
    b = b + (ulong)1;
    IACSharpSensor.IACSharpSensor.SensorReached(269);
  }
  unsafe static void Test2(sbyte sb, short s, int i, long l, ulong ul)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(270);
    short* b = null;
    b = b + sb;
    b = b + s;
    b = b + i;
    b = b + l;
    b = b + ul;
    IACSharpSensor.IACSharpSensor.SensorReached(271);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(272);
    Test2();
    Test2(1, 2, 3, 4, 5);
    if (Test() != 0) {
      System.Int32 RNTRNTRNT_142 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(273);
      return RNTRNTRNT_142;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_143 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(274);
    return RNTRNTRNT_143;
  }
}
