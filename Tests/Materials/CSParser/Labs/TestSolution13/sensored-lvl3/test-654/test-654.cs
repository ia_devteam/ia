using System;
class Program
{
  unsafe static int Main()
  {
    System.Int32 RNTRNTRNT_144 = Test((sbyte*)(-1));
    IACSharpSensor.IACSharpSensor.SensorReached(275);
    return RNTRNTRNT_144;
  }
  unsafe static int Test(sbyte* x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(276);
    if ((x + 1) < x) {
      Console.WriteLine("OK");
      System.Int32 RNTRNTRNT_145 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(277);
      return RNTRNTRNT_145;
    } else {
      Console.WriteLine("BAD");
      System.Int32 RNTRNTRNT_146 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(278);
      return RNTRNTRNT_146;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(279);
  }
}
