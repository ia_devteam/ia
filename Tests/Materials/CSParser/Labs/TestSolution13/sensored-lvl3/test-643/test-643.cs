using System;
class PointerArithmeticTest
{
  unsafe static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(208);
    try {
      System.Int32 RNTRNTRNT_109 = CheckAdd((byte*)(-1), -1);
      IACSharpSensor.IACSharpSensor.SensorReached(209);
      return RNTRNTRNT_109;
    } catch (System.OverflowException) {
    }
    try {
      System.Int32 RNTRNTRNT_110 = CheckSub((short*)(-1), int.MaxValue);
      IACSharpSensor.IACSharpSensor.SensorReached(210);
      return RNTRNTRNT_110;
    } catch (System.OverflowException) {
    }
    CheckSub2((short*)(-1), int.MaxValue);
    if ((long)Conversions(long.MaxValue) != (IntPtr.Size <= 4 ? uint.MaxValue : long.MaxValue)) {
      System.Int32 RNTRNTRNT_111 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(211);
      return RNTRNTRNT_111;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_112 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(212);
    return RNTRNTRNT_112;
  }
  unsafe static int* Conversions(long b)
  {
    System.Int32* RNTRNTRNT_113 = (int*)b;
    IACSharpSensor.IACSharpSensor.SensorReached(213);
    return RNTRNTRNT_113;
  }
  unsafe static int CheckAdd(byte* ptr, int offset)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(214);
    if (checked(ptr + offset < ptr)) {
      System.Int32 RNTRNTRNT_114 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(215);
      return RNTRNTRNT_114;
    }
    System.Int32 RNTRNTRNT_115 = 101;
    IACSharpSensor.IACSharpSensor.SensorReached(216);
    return RNTRNTRNT_115;
  }
  unsafe static int CheckSub(short* ptr, int offset)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(217);
    if (checked(ptr - offset < ptr)) {
      System.Int32 RNTRNTRNT_116 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(218);
      return RNTRNTRNT_116;
    }
    System.Int32 RNTRNTRNT_117 = 102;
    IACSharpSensor.IACSharpSensor.SensorReached(219);
    return RNTRNTRNT_117;
  }
  unsafe static int CheckSub2(short* ptr, int offset)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    short* b = ptr + offset;
    if (checked(ptr - b < 0)) {
      System.Int32 RNTRNTRNT_118 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(221);
      return RNTRNTRNT_118;
    }
    System.Int32 RNTRNTRNT_119 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(222);
    return RNTRNTRNT_119;
  }
}
