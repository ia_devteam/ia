using System;
public class MyEx : Exception
{
  public MyEx()
  {
  }
}
public class Ex
{
  public static int test(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(111);
    int res;
    int fin = 0;
    try {
      res = 10 / a;
      throw new MyEx();
    } catch (Exception ex) {
      ex = new MyEx();
      throw;
    } finally {
      fin = 1;
    }
    System.Int32 RNTRNTRNT_50 = res;
    IACSharpSensor.IACSharpSensor.SensorReached(112);
    return RNTRNTRNT_50;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(113);
    int catched = 0;
    try {
      test(1);
    } catch (MyEx ex) {
      catched = 1;
    }
    if (catched != 1) {
      System.Int32 RNTRNTRNT_51 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(114);
      return RNTRNTRNT_51;
    }
    try {
      test(0);
    } catch (MyEx ex) {
      catched = 2;
    } catch {
      catched = 3;
    }
    if (catched != 3) {
      System.Int32 RNTRNTRNT_52 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(115);
      return RNTRNTRNT_52;
    }
    System.Int32 RNTRNTRNT_53 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(116);
    return RNTRNTRNT_53;
  }
}
