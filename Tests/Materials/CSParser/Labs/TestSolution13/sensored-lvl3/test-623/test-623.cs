interface I
{
  void a();
}
abstract class X : I
{
  public abstract void a();
}
class Y : X
{
  public override void a()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(92);
    System.Console.WriteLine("Hello!");
    IACSharpSensor.IACSharpSensor.SensorReached(93);
    return;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(94);
    Y y = new Y();
    ((I)y).a();
    IACSharpSensor.IACSharpSensor.SensorReached(95);
  }
}
