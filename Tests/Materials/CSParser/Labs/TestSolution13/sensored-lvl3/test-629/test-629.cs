using System.Collections;
class Foo
{
  public static IEnumerable foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(106);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(107);
      yield break;
    } catch {
    } finally {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(108);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(109);
    int i = 0;
    foreach (object o in foo()) {
      ++i;
    }
    System.Int32 RNTRNTRNT_49 = i;
    IACSharpSensor.IACSharpSensor.SensorReached(110);
    return RNTRNTRNT_49;
  }
}
