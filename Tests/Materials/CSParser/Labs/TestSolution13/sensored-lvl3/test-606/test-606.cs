using System;
using System.Collections;
using System.Reflection;
using Mono.Test;
class Program
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(28);
    BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly;
    Type type = typeof(Info);
    PropertyInfo[] properties = type.GetProperties(flags);
    if (properties.Length != 2) {
      Console.WriteLine("#1: " + properties.Length.ToString());
      System.Int32 RNTRNTRNT_22 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(29);
      return RNTRNTRNT_22;
    }
    if (properties[0].Name != "System.Collections.IEnumerator.Current") {
      Console.WriteLine("#2: " + properties[0].Name);
      System.Int32 RNTRNTRNT_23 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(30);
      return RNTRNTRNT_23;
    }
    if (properties[1].Name != "Mono.Test.ITest.Item") {
      Console.WriteLine("#3: " + properties[1].Name);
      System.Int32 RNTRNTRNT_24 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(31);
      return RNTRNTRNT_24;
    }
    System.Int32 RNTRNTRNT_25 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(32);
    return RNTRNTRNT_25;
  }
}
namespace Mono.Test
{
  interface ITest
  {
    object this[int index] { get; set; }
  }
}
class Info : IEnumerator, ITest
{
  object IEnumerator.Current {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(33);
      return null;
    }
  }
  bool IEnumerator.MoveNext()
  {
    System.Boolean RNTRNTRNT_26 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(34);
    return RNTRNTRNT_26;
  }
  void IEnumerator.Reset()
  {
  }
  object ITest.this[int index] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(35);
      return null;
    }
    set { }
  }
}
