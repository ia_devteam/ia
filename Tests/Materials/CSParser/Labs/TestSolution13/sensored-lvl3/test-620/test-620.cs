class X
{
  static void A(ref int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(83);
    a++;
    IACSharpSensor.IACSharpSensor.SensorReached(84);
  }
  static void B(ref int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(85);
    A(ref a);
    IACSharpSensor.IACSharpSensor.SensorReached(86);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(87);
    int a = 10;
    B(ref a);
    if (a == 11) {
      System.Int32 RNTRNTRNT_45 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(88);
      return RNTRNTRNT_45;
    } else {
      System.Int32 RNTRNTRNT_46 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(89);
      return RNTRNTRNT_46;
    }
  }
}
