using System;
class MainClass
{
  public struct Decimal2
  {
    public Decimal2(double d)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(250);
      value = new Decimal(d);
      IACSharpSensor.IACSharpSensor.SensorReached(251);
    }
    public Decimal2(Decimal d)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(252);
      value = d;
      IACSharpSensor.IACSharpSensor.SensorReached(253);
    }
    public static explicit operator Decimal2(Decimal d)
    {
      Decimal2 RNTRNTRNT_135 = new Decimal2(d);
      IACSharpSensor.IACSharpSensor.SensorReached(254);
      return RNTRNTRNT_135;
    }
    public static explicit operator Decimal2(double d)
    {
      Decimal2 RNTRNTRNT_136 = new Decimal2(d);
      IACSharpSensor.IACSharpSensor.SensorReached(255);
      return RNTRNTRNT_136;
    }
    public static implicit operator Decimal(Decimal2 d)
    {
      Decimal RNTRNTRNT_137 = d.value;
      IACSharpSensor.IACSharpSensor.SensorReached(256);
      return RNTRNTRNT_137;
    }
    private Decimal value;
  }
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(257);
    Console.WriteLine("double   = {0}", 1.1367 * 11.9767 - 0.6);
    Console.WriteLine("Decimal2 = {0}", ((Decimal2)1.1367 * (Decimal2)11.9767 - (Decimal2)0.6));
    Console.WriteLine("Decimal2 = {0}", new Decimal2(1.1367) * (Decimal2)11.9767 - (Decimal2)0.6);
    Console.WriteLine("Decimal2 = {0}", (Decimal2)11.9767 * new Decimal2(1.1367) - (Decimal2)0.6);
    Console.WriteLine("Decimal2 = {0}", (new Decimal2(1.1367) * (Decimal2)11.9767 - (Decimal2)0.6));
    Console.WriteLine("Decimal2 = {0}", ((Decimal2)1.1367 * (Decimal2)11.9767 - (Decimal)0.6));
    Console.WriteLine("Decimal2 = {0}", (1.14 * 11.9767 - 0.6));
    Console.WriteLine("Decimal2 = {0}", ((Decimal2)1.1367 * (Decimal)11.9767 - (Decimal2)0.6));
    Console.WriteLine("Decimal2 = {0}", ((Decimal2)1.1367 * (Decimal2)11.9767 - (Decimal2)0.6));
    IACSharpSensor.IACSharpSensor.SensorReached(258);
  }
}
