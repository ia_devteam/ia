using System;
interface A
{
  void B();
}
interface X
{
  void B();
}
class B : A, X
{
  void X.B()
  {
  }
  void A.B()
  {
  }
}
namespace N
{
  interface B
  {
  }
}
class M
{
  static void N(object N)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(104);
    object x = (N.B)N;
    IACSharpSensor.IACSharpSensor.SensorReached(105);
  }
  static void Main()
  {
  }
}
