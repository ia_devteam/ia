using System;
class C
{
  public static int value;
  static internal void And()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(56);
    if ((false & (value++ == 1)) != (false & (++value == 1))) {
      IACSharpSensor.IACSharpSensor.SensorReached(57);
      return;
    }
    if (((value++ == 1) & false) != ((++value == 1) & false)) {
      IACSharpSensor.IACSharpSensor.SensorReached(58);
      return;
    }
    if ((false && (value++ == 1)) != (false && (++value == 1))) {
      IACSharpSensor.IACSharpSensor.SensorReached(59);
      return;
    }
    if (((value++ == 1) && false) != ((++value == 1) && false)) {
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(61);
  }
  static internal void Or()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    if ((false | (value++ == 1)) != (false | (++value == 1))) {
      IACSharpSensor.IACSharpSensor.SensorReached(63);
      return;
    }
    if (((value++ == 1) | false) != ((++value == 1) | false)) {
      IACSharpSensor.IACSharpSensor.SensorReached(64);
      return;
    }
    if ((true || (value++ == 1)) != (true || (++value == 1))) {
      IACSharpSensor.IACSharpSensor.SensorReached(65);
      return;
    }
    if (((value++ == 1) || true) != ((++value == 1) || true)) {
      IACSharpSensor.IACSharpSensor.SensorReached(66);
      return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(67);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    value = 0;
    And();
    Console.WriteLine(value);
    if (value != 6) {
      System.Int32 RNTRNTRNT_38 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(69);
      return RNTRNTRNT_38;
    }
    value = 0;
    Or();
    Console.WriteLine(value);
    if (value != 6) {
      System.Int32 RNTRNTRNT_39 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(70);
      return RNTRNTRNT_39;
    }
    System.Int32 RNTRNTRNT_40 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(71);
    return RNTRNTRNT_40;
  }
}
