using System;
using System.Reflection;
public delegate long MyDelegate();
public interface X
{
  event MyDelegate Foo;
  int Prop { get; }
}
public class Y : X
{
  event MyDelegate X.Foo {
    add { }
    remove { }
  }
  int X.Prop {
    get {
      System.Int32 RNTRNTRNT_41 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(74);
      return RNTRNTRNT_41;
    }
  }
  public event MyDelegate Foo;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(75);
    MethodInfo o = typeof(Y).GetMethod("X.add_Foo", BindingFlags.NonPublic | BindingFlags.Instance);
    if (o == null) {
      System.Int32 RNTRNTRNT_42 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(76);
      return RNTRNTRNT_42;
    }
    o = typeof(Y).GetMethod("X.get_Prop", BindingFlags.NonPublic | BindingFlags.Instance);
    if (o == null) {
      System.Int32 RNTRNTRNT_43 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(77);
      return RNTRNTRNT_43;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_44 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(78);
    return RNTRNTRNT_44;
  }
}
