using System;
class Program
{
  public delegate object D(int member);
  private D _value;
  private object M(int member)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(223);
    return null;
  }
  void Test_1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(224);
    Delegate d1 = M + _value;
    Delegate d2 = _value + M;
    IACSharpSensor.IACSharpSensor.SensorReached(225);
  }
  public bool Test_2()
  {
    System.Boolean RNTRNTRNT_120 = _value == M;
    IACSharpSensor.IACSharpSensor.SensorReached(226);
    return RNTRNTRNT_120;
  }
  public bool Test_3()
  {
    System.Boolean RNTRNTRNT_121 = _value != M;
    IACSharpSensor.IACSharpSensor.SensorReached(227);
    return RNTRNTRNT_121;
  }
  public bool Test_4(D d)
  {
    System.Boolean RNTRNTRNT_122 = d == _value;
    IACSharpSensor.IACSharpSensor.SensorReached(228);
    return RNTRNTRNT_122;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(229);
    Program p = new Program();
    if (p.Test_2()) {
      System.Int32 RNTRNTRNT_123 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(230);
      return RNTRNTRNT_123;
    }
    p._value = p.M;
    if (!p.Test_2()) {
      System.Int32 RNTRNTRNT_124 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(231);
      return RNTRNTRNT_124;
    }
    if (p.Test_3()) {
      System.Int32 RNTRNTRNT_125 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(232);
      return RNTRNTRNT_125;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_126 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(233);
    return RNTRNTRNT_126;
  }
}
