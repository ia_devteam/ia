using System;
namespace Test
{
  enum Key_byte : byte
  {
    A = 1
  }
  enum Key_ulong : ulong
  {
    A = 1
  }
  class Regression
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1);
      IntPtr a = new IntPtr(1);
      UIntPtr b = new UIntPtr(1);
      Key_byte k1 = (Key_byte)a;
      Key_byte k2 = (Key_byte)b;
      if (k1 != Key_byte.A) {
        System.Int32 RNTRNTRNT_1 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(2);
        return RNTRNTRNT_1;
      }
      if (k2 != Key_byte.A) {
        System.Int32 RNTRNTRNT_2 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(3);
        return RNTRNTRNT_2;
      }
      Key_ulong k1_u = (Key_ulong)a;
      Key_ulong k2_u = (Key_ulong)b;
      if (k1_u != Key_ulong.A) {
        System.Int32 RNTRNTRNT_3 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(4);
        return RNTRNTRNT_3;
      }
      if (k2_u != Key_ulong.A) {
        System.Int32 RNTRNTRNT_4 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(5);
        return RNTRNTRNT_4;
      }
      System.Int32 RNTRNTRNT_5 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(6);
      return RNTRNTRNT_5;
    }
  }
}
