class Foo
{
  bool got;
  string s {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(185);
      got = true;
      System.String RNTRNTRNT_96 = "";
      IACSharpSensor.IACSharpSensor.SensorReached(186);
      return RNTRNTRNT_96;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(187);
      if (!got || value != "A1B2") {
        throw new System.Exception();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(188);
    }
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(189);
    (new Foo()).s += "A" + 1 + "B" + 2;
    IACSharpSensor.IACSharpSensor.SensorReached(190);
  }
}
