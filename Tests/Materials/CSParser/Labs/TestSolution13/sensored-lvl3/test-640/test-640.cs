enum MyEnum : byte
{
  Value_1 = 1
}
public class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(191);
    MyEnum me = MyEnum.Value_1;
    MyEnum b = ~me;
    if (b != (MyEnum)254) {
      System.Int32 RNTRNTRNT_97 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(192);
      return RNTRNTRNT_97;
    }
    byte r = b - me;
    if (r != 253) {
      System.Int32 RNTRNTRNT_98 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(193);
      return RNTRNTRNT_98;
    }
    b = b - 2;
    if (b != (MyEnum)252) {
      System.Int32 RNTRNTRNT_99 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(194);
      return RNTRNTRNT_99;
    }
    me -= MyEnum.Value_1;
    b = (MyEnum)255;
    b &= ~MyEnum.Value_1;
    if (b != (MyEnum)254) {
      System.Int32 RNTRNTRNT_100 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(195);
      return RNTRNTRNT_100;
    }
    System.Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_101 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(196);
    return RNTRNTRNT_101;
  }
}
