using System;
using System.Reflection;
class Program
{
  interface Iface1
  {
    void IfaceMethod1();
  }
  interface Iface2
  {
    void IfaceMethod2();
  }
  public class ImplementingExplicitInterfacesMembers : Iface1, Iface2
  {
    void Iface1.IfaceMethod1()
    {
    }
    void Iface2.IfaceMethod2()
    {
    }
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(23);
    object[] o = typeof(ImplementingExplicitInterfacesMembers).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);
    foreach (MethodInfo mi in o) {
      if (mi.Name.IndexOf('+') != -1) {
        System.Int32 RNTRNTRNT_20 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(24);
        return RNTRNTRNT_20;
      }
      Console.WriteLine(mi.Name);
    }
    System.Int32 RNTRNTRNT_21 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(25);
    return RNTRNTRNT_21;
  }
}
