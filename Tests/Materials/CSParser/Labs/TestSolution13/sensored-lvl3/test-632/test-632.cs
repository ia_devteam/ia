using System;
class Bug379822
{
  static void Assert(bool expected, bool value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(158);
    if (value != expected) {
      throw new Exception("unexpected value");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(159);
  }
  static void TestAnd(bool var)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(160);
    Assert(false, false && var);
    Assert(false, var && false);
    Assert(false, false & var);
    Assert(false, var & false);
    Assert(var, true && var);
    Assert(var, var && true);
    Assert(var, true & var);
    Assert(var, var & true);
    IACSharpSensor.IACSharpSensor.SensorReached(161);
  }
  static void TestOr(bool var)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    Assert(var, false || var);
    Assert(var, var || false);
    Assert(var, false | var);
    Assert(var, var | false);
    Assert(true, true || var);
    Assert(true, var || true);
    Assert(true, true | var);
    Assert(true, var | true);
    IACSharpSensor.IACSharpSensor.SensorReached(163);
  }
  static void Test(bool var)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(164);
    TestAnd(var);
    TestOr(var);
    IACSharpSensor.IACSharpSensor.SensorReached(165);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(166);
    Test(false);
    Test(true);
    IACSharpSensor.IACSharpSensor.SensorReached(167);
  }
}
