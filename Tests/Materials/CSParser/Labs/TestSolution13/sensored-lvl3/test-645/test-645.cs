using System;
using System.Collections;
class Program
{
  public static IEnumerable Empty {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(234);
      object[] os = new object[] {
        
      };
      foreach (object o in os) {
        IEnumerable RNTRNTRNT_127 = o;
        IACSharpSensor.IACSharpSensor.SensorReached(235);
        yield return RNTRNTRNT_127;
        IACSharpSensor.IACSharpSensor.SensorReached(236);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(237);
    }
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    IEnumerator enumerator = Empty.GetEnumerator();
    if (enumerator.Current == null) {
      Console.WriteLine("Successful");
    }
    enumerator.MoveNext();
    if (enumerator.Current == null) {
      Console.WriteLine("Successful");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(239);
  }
}
