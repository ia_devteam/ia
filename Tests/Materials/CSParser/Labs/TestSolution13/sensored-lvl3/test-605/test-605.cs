class TestA
{
  public virtual string Method {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(26);
      return null;
    }
  }
}
class TestB : TestA
{
  private string Method {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(27);
      return null;
    }
  }
  public static void Main()
  {
  }
}
