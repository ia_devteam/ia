using System;
using System.Reflection;
using System.Runtime.CompilerServices;
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(81);
    object[] attrs = typeof(X).Assembly.GetCustomAttributes(true);
    IACSharpSensor.IACSharpSensor.SensorReached(82);
    foreach (object o in attrs) {
      IACSharpSensor.IACSharpSensor.SensorReached(83);
      if (o is RuntimeCompatibilityAttribute) {
        IACSharpSensor.IACSharpSensor.SensorReached(84);
        RuntimeCompatibilityAttribute a = (RuntimeCompatibilityAttribute)o;
        IACSharpSensor.IACSharpSensor.SensorReached(85);
        if (a.WrapNonExceptionThrows) {
          System.Int32 RNTRNTRNT_16 = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(86);
          return RNTRNTRNT_16;
        }
      }
    }
    System.Int32 RNTRNTRNT_17 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(87);
    return RNTRNTRNT_17;
  }
}
