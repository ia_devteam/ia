using System;
using System.Diagnostics;
using SCG = System.Collections.Generic;
public abstract class EnumerableBase<T> : SCG.IEnumerable<T>
{
  public abstract SCG.IEnumerator<T> GetEnumerator();
  System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
  {
    System.Collections.IEnumerator RNTRNTRNT_80 = GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(281);
    return RNTRNTRNT_80;
  }
}
public abstract class CollectionValueBase<T> : EnumerableBase<T>
{
  protected virtual void raiseItemsAdded(T item, int count)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(282);
  }
  protected class RaiseForRemoveAllHandler
  {
    CircularQueue<T> wasRemoved;
  }
  public override abstract SCG.IEnumerator<T> GetEnumerator();
}
public class CircularQueue<T> : EnumerableBase<T>
{
  public override SCG.IEnumerator<T> GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    yield break;
  }
  public virtual void Enqueue(T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(284);
  }
}
public class HashSet<T> : CollectionValueBase<T>
{
  private bool searchoradd(ref T item, bool @add, bool update, bool raise)
  {
    System.Boolean RNTRNTRNT_81 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(285);
    return RNTRNTRNT_81;
  }
  public virtual void RemoveAll<U>(SCG.IEnumerable<U> items) where U : T
  {
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    RaiseForRemoveAllHandler raiseHandler = new RaiseForRemoveAllHandler();
    IACSharpSensor.IACSharpSensor.SensorReached(287);
  }
  public virtual void AddAll<U>(SCG.IEnumerable<U> items) where U : T
  {
    IACSharpSensor.IACSharpSensor.SensorReached(288);
    CircularQueue<T> wasAdded = new CircularQueue<T>();
    IACSharpSensor.IACSharpSensor.SensorReached(289);
    foreach (T item in wasAdded) {
      IACSharpSensor.IACSharpSensor.SensorReached(290);
      raiseItemsAdded(item, 1);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(291);
  }
  public override SCG.IEnumerator<T> GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(292);
    yield break;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(293);
  }
}
