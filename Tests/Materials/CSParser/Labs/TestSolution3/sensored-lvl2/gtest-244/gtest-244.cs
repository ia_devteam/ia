public class B
{
  public virtual T Get<T>()
  {
    T RNTRNTRNT_77 = default(T);
    IACSharpSensor.IACSharpSensor.SensorReached(268);
    return RNTRNTRNT_77;
  }
}
public class A : B
{
  public override T Get<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(269);
    T resp = base.Get<T>();
    System.Console.WriteLine("T: " + resp);
    T RNTRNTRNT_78 = resp;
    IACSharpSensor.IACSharpSensor.SensorReached(270);
    return RNTRNTRNT_78;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(271);
    new A().Get<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(272);
  }
}
