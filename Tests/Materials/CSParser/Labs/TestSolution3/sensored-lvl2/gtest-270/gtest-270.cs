using System;
class X
{
  static int Test(int? a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(443);
    switch (a) {
      case 0:
        System.Int32 RNTRNTRNT_120 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(444);
        return RNTRNTRNT_120;
      case 1:
        System.Int32 RNTRNTRNT_121 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(445);
        return RNTRNTRNT_121;
      default:
        System.Int32 RNTRNTRNT_122 = -1;
        IACSharpSensor.IACSharpSensor.SensorReached(446);
        return RNTRNTRNT_122;
    }
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(447);
    if (Test(null) != -1) {
      System.Int32 RNTRNTRNT_123 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(448);
      return RNTRNTRNT_123;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(449);
    if (Test(0) != 0) {
      System.Int32 RNTRNTRNT_124 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(450);
      return RNTRNTRNT_124;
    }
    System.Int32 RNTRNTRNT_125 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(451);
    return RNTRNTRNT_125;
  }
}
