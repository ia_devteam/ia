using System;
namespace TestCase
{
  interface ITest
  {
  }
  class CTest : ITest
  {
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(277);
    }
    public void Bar()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(278);
    }
  }
  class CGenericTest<T, V> where T : ITest where V : CTest, T, new()
  {
    public V Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(279);
      V TestObject = new V();
      TestObject.Bar();
      V RNTRNTRNT_79 = TestObject;
      IACSharpSensor.IACSharpSensor.SensorReached(280);
      return RNTRNTRNT_79;
    }
  }
}
