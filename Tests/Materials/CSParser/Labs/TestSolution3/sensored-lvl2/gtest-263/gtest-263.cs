using System;
using System.Collections.Generic;
class Foo<S>
{
  public ICloneable Test(S t)
  {
    ICloneable RNTRNTRNT_91 = (ICloneable)t;
    IACSharpSensor.IACSharpSensor.SensorReached(378);
    return RNTRNTRNT_91;
  }
}
public static class ConvertHelper
{
  public static IEnumerator<T> Test<S, T>(S s) where T : S
  {
    T RNTRNTRNT_92 = (T)s;
    IACSharpSensor.IACSharpSensor.SensorReached(379);
    yield return RNTRNTRNT_92;
    IACSharpSensor.IACSharpSensor.SensorReached(380);
    IACSharpSensor.IACSharpSensor.SensorReached(381);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(382);
  }
}
