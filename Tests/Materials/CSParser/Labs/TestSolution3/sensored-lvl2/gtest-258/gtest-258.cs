using System;
public class A
{
  public A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(348);
  }
}
public class B
{
}
class Foo<T> where T : new()
{
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(349);
    Foo<A> foo = new Foo<A>();
    IACSharpSensor.IACSharpSensor.SensorReached(350);
  }
}
