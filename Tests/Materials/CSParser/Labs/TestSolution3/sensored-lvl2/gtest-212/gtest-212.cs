public interface SomeInterface
{
  bool Valid { get; }
}
public struct SomeStruct : SomeInterface
{
  public bool Valid {
    get {
      System.Boolean RNTRNTRNT_14 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(66);
      return RNTRNTRNT_14;
    }
  }
}
public class Test
{
  public static void Fun<T>(T t) where T : SomeInterface
  {
    IACSharpSensor.IACSharpSensor.SensorReached(67);
    bool a = t.Valid;
    IACSharpSensor.IACSharpSensor.SensorReached(68);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(69);
    Fun(new SomeStruct());
    IACSharpSensor.IACSharpSensor.SensorReached(70);
  }
}
