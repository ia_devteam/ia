using System;
public delegate void Handler<T>(T t);
public static class X
{
  public static void Foo<T>(Handler<T> handler)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(458);
    AsyncCallback d = delegate(IAsyncResult ar) { Response<T>(handler); };
    IACSharpSensor.IACSharpSensor.SensorReached(459);
  }
  static void Response<T>(Handler<T> handler)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(460);
  }
  static void Test<T>(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(461);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(462);
    Foo<long>(Test);
    IACSharpSensor.IACSharpSensor.SensorReached(463);
  }
}
