using System;
using System.Collections;
using System.Collections.Generic;
public class B : IComparable<B>
{
  public int CompareTo(B b)
  {
    System.Int32 RNTRNTRNT_28 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(150);
    return RNTRNTRNT_28;
  }
}
public class Tester
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(151);
    B b = new B();
    IACSharpSensor.IACSharpSensor.SensorReached(152);
    if (b is IComparable<object>) {
      System.Int32 RNTRNTRNT_29 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(153);
      return RNTRNTRNT_29;
    }
    System.Int32 RNTRNTRNT_30 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(154);
    return RNTRNTRNT_30;
  }
}
