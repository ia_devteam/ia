public interface SomeInterface
{
  int Foo { get; set; }
}
public struct SomeStruct : SomeInterface
{
  int x;
  public int Foo {
    get {
      System.Int32 RNTRNTRNT_15 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(71);
      return RNTRNTRNT_15;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(72);
      x = value;
      IACSharpSensor.IACSharpSensor.SensorReached(73);
    }
  }
}
public class Test
{
  public static void Fun<T>(T t) where T : SomeInterface
  {
    IACSharpSensor.IACSharpSensor.SensorReached(74);
    if (++t.Foo != 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(75);
      throw new System.Exception("not 1");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(76);
    if (t.Foo != 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(77);
      throw new System.Exception("didn't update 't'");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(78);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(79);
    Fun(new SomeStruct());
    IACSharpSensor.IACSharpSensor.SensorReached(80);
  }
}
