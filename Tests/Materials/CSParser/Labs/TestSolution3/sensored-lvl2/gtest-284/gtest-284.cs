using System;
using System.Collections;
using System.Collections.Generic;
public interface Y
{
}
public class X : Y
{
}
public struct Foo : Y
{
}
public static class CollectionTester
{
  static int Test<T>(IList<T> list)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(510);
    if (list.Count != 1) {
      System.Int32 RNTRNTRNT_135 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(511);
      return RNTRNTRNT_135;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(512);
    ICollection<T> collection = list;
    IACSharpSensor.IACSharpSensor.SensorReached(513);
    if (collection.Count != 1) {
      System.Int32 RNTRNTRNT_136 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(514);
      return RNTRNTRNT_136;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(515);
    IEnumerable<T> enumerable = list;
    IEnumerator<T> enumerator = enumerable.GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(516);
    if (!enumerator.MoveNext()) {
      System.Int32 RNTRNTRNT_137 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(517);
      return RNTRNTRNT_137;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(518);
    if (enumerator.MoveNext()) {
      System.Int32 RNTRNTRNT_138 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(519);
      return RNTRNTRNT_138;
    }
    System.Int32 RNTRNTRNT_139 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(520);
    return RNTRNTRNT_139;
  }
  public static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(521);
    X[] xarray = new X[] { new X() };
    int result;
    result = Test<X>(xarray);
    IACSharpSensor.IACSharpSensor.SensorReached(522);
    if (result != 0) {
      System.Int32 RNTRNTRNT_140 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(523);
      return RNTRNTRNT_140;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(524);
    result = Test<object>(xarray);
    IACSharpSensor.IACSharpSensor.SensorReached(525);
    if (result != 0) {
      System.Int32 RNTRNTRNT_141 = 10 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(526);
      return RNTRNTRNT_141;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(527);
    result = Test<Y>(xarray);
    IACSharpSensor.IACSharpSensor.SensorReached(528);
    if (result != 0) {
      System.Int32 RNTRNTRNT_142 = 20 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(529);
      return RNTRNTRNT_142;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(530);
    int[] iarray = new int[] { 5 };
    result = Test<int>(iarray);
    IACSharpSensor.IACSharpSensor.SensorReached(531);
    if (result != 0) {
      System.Int32 RNTRNTRNT_143 = 30 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(532);
      return RNTRNTRNT_143;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(533);
    result = Test<uint>((IList<uint>)(object)iarray);
    IACSharpSensor.IACSharpSensor.SensorReached(534);
    if (result != 0) {
      System.Int32 RNTRNTRNT_144 = 40 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(535);
      return RNTRNTRNT_144;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(536);
    uint[] uiarray = new uint[] { 5 };
    result = Test<int>((IList<int>)(object)uiarray);
    IACSharpSensor.IACSharpSensor.SensorReached(537);
    if (result != 0) {
      System.Int32 RNTRNTRNT_145 = 50 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(538);
      return RNTRNTRNT_145;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(539);
    result = Test<uint>(uiarray);
    IACSharpSensor.IACSharpSensor.SensorReached(540);
    if (result != 0) {
      System.Int32 RNTRNTRNT_146 = 60 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(541);
      return RNTRNTRNT_146;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(542);
    long[] larray = new long[] { 5 };
    result = Test<long>(larray);
    IACSharpSensor.IACSharpSensor.SensorReached(543);
    if (result != 0) {
      System.Int32 RNTRNTRNT_147 = 70 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(544);
      return RNTRNTRNT_147;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(545);
    result = Test<ulong>((IList<ulong>)(object)larray);
    IACSharpSensor.IACSharpSensor.SensorReached(546);
    if (result != 0) {
      System.Int32 RNTRNTRNT_148 = 80 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(547);
      return RNTRNTRNT_148;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(548);
    ulong[] ularray = new ulong[] { 5 };
    result = Test<long>((IList<long>)(object)ularray);
    IACSharpSensor.IACSharpSensor.SensorReached(549);
    if (result != 0) {
      System.Int32 RNTRNTRNT_149 = 90 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(550);
      return RNTRNTRNT_149;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(551);
    result = Test<ulong>(ularray);
    IACSharpSensor.IACSharpSensor.SensorReached(552);
    if (result != 0) {
      System.Int32 RNTRNTRNT_150 = 100 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(553);
      return RNTRNTRNT_150;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(554);
    short[] sarray = new short[] { 5 };
    result = Test<short>(sarray);
    IACSharpSensor.IACSharpSensor.SensorReached(555);
    if (result != 0) {
      System.Int32 RNTRNTRNT_151 = 110 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(556);
      return RNTRNTRNT_151;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(557);
    result = Test<ushort>((IList<ushort>)(object)sarray);
    IACSharpSensor.IACSharpSensor.SensorReached(558);
    if (result != 0) {
      System.Int32 RNTRNTRNT_152 = 120 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(559);
      return RNTRNTRNT_152;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(560);
    ushort[] usarray = new ushort[] { 5 };
    result = Test<short>((IList<short>)(object)usarray);
    IACSharpSensor.IACSharpSensor.SensorReached(561);
    if (result != 0) {
      System.Int32 RNTRNTRNT_153 = 130 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(562);
      return RNTRNTRNT_153;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(563);
    result = Test<ushort>(usarray);
    IACSharpSensor.IACSharpSensor.SensorReached(564);
    if (result != 0) {
      System.Int32 RNTRNTRNT_154 = 140 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(565);
      return RNTRNTRNT_154;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(566);
    byte[] barray = new byte[] { 5 };
    result = Test<byte>(barray);
    IACSharpSensor.IACSharpSensor.SensorReached(567);
    if (result != 0) {
      System.Int32 RNTRNTRNT_155 = 150 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(568);
      return RNTRNTRNT_155;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(569);
    result = Test<sbyte>((IList<sbyte>)(object)barray);
    IACSharpSensor.IACSharpSensor.SensorReached(570);
    if (result != 0) {
      System.Int32 RNTRNTRNT_156 = 160 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(571);
      return RNTRNTRNT_156;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(572);
    sbyte[] sbarray = new sbyte[] { 5 };
    result = Test<byte>((IList<byte>)(object)sbarray);
    IACSharpSensor.IACSharpSensor.SensorReached(573);
    if (result != 0) {
      System.Int32 RNTRNTRNT_157 = 170 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(574);
      return RNTRNTRNT_157;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(575);
    result = Test<sbyte>(sbarray);
    IACSharpSensor.IACSharpSensor.SensorReached(576);
    if (result != 0) {
      System.Int32 RNTRNTRNT_158 = 180 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(577);
      return RNTRNTRNT_158;
    }
    System.Int32 RNTRNTRNT_159 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(578);
    return RNTRNTRNT_159;
  }
}
public static class InterfaceTester
{
  public const bool Debug = false;
  static readonly Type ilist_type;
  static readonly Type icollection_type;
  static readonly Type ienumerable_type;
  static readonly Type generic_ilist_type;
  static readonly Type generic_icollection_type;
  static readonly Type generic_ienumerable_type;
  static readonly Type icloneable_type;
  static InterfaceTester()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(579);
    ilist_type = typeof(IList);
    icollection_type = typeof(ICollection);
    ienumerable_type = typeof(IEnumerable);
    generic_ilist_type = typeof(IList<>);
    generic_icollection_type = typeof(ICollection<>);
    generic_ienumerable_type = typeof(IEnumerable<>);
    icloneable_type = typeof(ICloneable);
    IACSharpSensor.IACSharpSensor.SensorReached(580);
  }
  enum State
  {
    Missing,
    Found,
    Extra
  }
  static int Test(Type t, params Type[] iface_types)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(581);
    Hashtable ifaces = new Hashtable();
    ifaces.Add(ilist_type, State.Missing);
    ifaces.Add(icollection_type, State.Missing);
    ifaces.Add(ienumerable_type, State.Missing);
    ifaces.Add(icloneable_type, State.Missing);
    Type array_type = t.MakeArrayType();
    IACSharpSensor.IACSharpSensor.SensorReached(582);
    if (Debug) {
      IACSharpSensor.IACSharpSensor.SensorReached(583);
      Console.WriteLine("Checking {0}", t);
      IACSharpSensor.IACSharpSensor.SensorReached(584);
      foreach (Type iface in t.GetInterfaces()) {
        IACSharpSensor.IACSharpSensor.SensorReached(585);
        Console.WriteLine("  {0}", iface);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(586);
    foreach (Type iface in iface_types) {
      IACSharpSensor.IACSharpSensor.SensorReached(587);
      Type[] gargs = new Type[] { iface };
      ifaces.Add(generic_ilist_type.MakeGenericType(gargs), State.Missing);
      ifaces.Add(generic_icollection_type.MakeGenericType(gargs), State.Missing);
      ifaces.Add(generic_ienumerable_type.MakeGenericType(gargs), State.Missing);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(588);
    foreach (Type iface in array_type.GetInterfaces()) {
      IACSharpSensor.IACSharpSensor.SensorReached(589);
      if (ifaces.Contains(iface)) {
        IACSharpSensor.IACSharpSensor.SensorReached(590);
        ifaces[iface] = State.Found;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(591);
        ifaces.Add(iface, State.Extra);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(592);
    int errors = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(593);
    foreach (Type iface in ifaces.Keys) {
      IACSharpSensor.IACSharpSensor.SensorReached(594);
      State state = (State)ifaces[iface];
      IACSharpSensor.IACSharpSensor.SensorReached(595);
      if (state == State.Found) {
        IACSharpSensor.IACSharpSensor.SensorReached(596);
        if (Debug) {
          IACSharpSensor.IACSharpSensor.SensorReached(597);
          Console.WriteLine("Found {0}", iface);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(598);
        continue;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(599);
        if (Debug) {
          IACSharpSensor.IACSharpSensor.SensorReached(600);
          Console.WriteLine("ERROR: {0} {1}", iface, state);
        }
        IACSharpSensor.IACSharpSensor.SensorReached(601);
        errors++;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(602);
    if (Debug) {
      IACSharpSensor.IACSharpSensor.SensorReached(603);
      Console.WriteLine();
    }
    System.Int32 RNTRNTRNT_160 = errors;
    IACSharpSensor.IACSharpSensor.SensorReached(604);
    return RNTRNTRNT_160;
  }
  public static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(605);
    int result = Test(typeof(X), typeof(X));
    IACSharpSensor.IACSharpSensor.SensorReached(606);
    if (result != 0) {
      System.Int32 RNTRNTRNT_161 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(607);
      return RNTRNTRNT_161;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(608);
    result = Test(typeof(Y), typeof(Y));
    IACSharpSensor.IACSharpSensor.SensorReached(609);
    if (result != 0) {
      System.Int32 RNTRNTRNT_162 = 100 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(610);
      return RNTRNTRNT_162;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(611);
    result = Test(typeof(DateTime), typeof(DateTime));
    IACSharpSensor.IACSharpSensor.SensorReached(612);
    if (result != 0) {
      System.Int32 RNTRNTRNT_163 = 200 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(613);
      return RNTRNTRNT_163;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(614);
    result = Test(typeof(float), typeof(float));
    IACSharpSensor.IACSharpSensor.SensorReached(615);
    if (result != 0) {
      System.Int32 RNTRNTRNT_164 = 300 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(616);
      return RNTRNTRNT_164;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(617);
    result = Test(typeof(int), typeof(int));
    IACSharpSensor.IACSharpSensor.SensorReached(618);
    if (result != 0) {
      System.Int32 RNTRNTRNT_165 = 400 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(619);
      return RNTRNTRNT_165;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(620);
    result = Test(typeof(uint), typeof(uint));
    IACSharpSensor.IACSharpSensor.SensorReached(621);
    if (result != 0) {
      System.Int32 RNTRNTRNT_166 = 500 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(622);
      return RNTRNTRNT_166;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(623);
    result = Test(typeof(long), typeof(long));
    IACSharpSensor.IACSharpSensor.SensorReached(624);
    if (result != 0) {
      System.Int32 RNTRNTRNT_167 = 600 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(625);
      return RNTRNTRNT_167;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(626);
    result = Test(typeof(ulong), typeof(ulong));
    IACSharpSensor.IACSharpSensor.SensorReached(627);
    if (result != 0) {
      System.Int32 RNTRNTRNT_168 = 700 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(628);
      return RNTRNTRNT_168;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(629);
    result = Test(typeof(short), typeof(short));
    IACSharpSensor.IACSharpSensor.SensorReached(630);
    if (result != 0) {
      System.Int32 RNTRNTRNT_169 = 800 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(631);
      return RNTRNTRNT_169;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(632);
    result = Test(typeof(ushort), typeof(ushort));
    IACSharpSensor.IACSharpSensor.SensorReached(633);
    if (result != 0) {
      System.Int32 RNTRNTRNT_170 = 900 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(634);
      return RNTRNTRNT_170;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(635);
    result = Test(typeof(Foo), typeof(Foo));
    IACSharpSensor.IACSharpSensor.SensorReached(636);
    if (result != 0) {
      System.Int32 RNTRNTRNT_171 = 1000 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(637);
      return RNTRNTRNT_171;
    }
    System.Int32 RNTRNTRNT_172 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(638);
    return RNTRNTRNT_172;
  }
}
class Z
{
  static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(639);
    int result;
    result = CollectionTester.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(640);
    if (result != 0) {
      System.Int32 RNTRNTRNT_173 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(641);
      return RNTRNTRNT_173;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(642);
    result = InterfaceTester.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(643);
    if (result != 0) {
      System.Int32 RNTRNTRNT_174 = 10000 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(644);
      return RNTRNTRNT_174;
    }
    System.Int32 RNTRNTRNT_175 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(645);
    return RNTRNTRNT_175;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(646);
    int result = Test();
    IACSharpSensor.IACSharpSensor.SensorReached(647);
    if (result == 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(648);
      Console.WriteLine("OK");
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(649);
      Console.WriteLine("ERROR: {0}", result);
    }
    System.Int32 RNTRNTRNT_176 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(650);
    return RNTRNTRNT_176;
  }
}
