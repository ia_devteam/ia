using System;
class Foo<T>
{
  public int Test(Foo<T> foo)
  {
    System.Int32 RNTRNTRNT_35 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(182);
    return RNTRNTRNT_35;
  }
  public int Test(Foo<int> foo)
  {
    System.Int32 RNTRNTRNT_36 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(183);
    return RNTRNTRNT_36;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(184);
    Foo<long> foo = new Foo<long>();
    Foo<int> bar = new Foo<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(185);
    if (foo.Test(foo) != 1) {
      System.Int32 RNTRNTRNT_37 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(186);
      return RNTRNTRNT_37;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(187);
    if (foo.Test(bar) != 2) {
      System.Int32 RNTRNTRNT_38 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(188);
      return RNTRNTRNT_38;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(189);
    if (bar.Test(bar) != 2) {
      System.Int32 RNTRNTRNT_39 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(190);
      return RNTRNTRNT_39;
    }
    System.Int32 RNTRNTRNT_40 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(191);
    return RNTRNTRNT_40;
  }
}
