using System;
class DerivedGenericClass<T> : BaseClass
{
  public override void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(273);
  }
  public void Baz()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(274);
    Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(275);
  }
}
abstract class BaseClass
{
  public abstract void Foo();
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(276);
  }
}
