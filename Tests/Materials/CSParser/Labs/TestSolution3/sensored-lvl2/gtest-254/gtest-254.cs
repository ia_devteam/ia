using System;
public class HashedLinkedList<T>
{
  public int? Offset;
  public static HashedLinkedList<T> GetList()
  {
    HashedLinkedList<T> RNTRNTRNT_86 = new HashedLinkedList<T>();
    IACSharpSensor.IACSharpSensor.SensorReached(315);
    return RNTRNTRNT_86;
  }
  public static void Test(int added)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(316);
    GetList().Offset += added;
    IACSharpSensor.IACSharpSensor.SensorReached(317);
  }
  public void Test(HashedLinkedList<T> view)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(318);
    view.Offset--;
    IACSharpSensor.IACSharpSensor.SensorReached(319);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(320);
    HashedLinkedList<int>.Test(5);
    HashedLinkedList<long> list = new HashedLinkedList<long>();
    list.Test(list);
    IACSharpSensor.IACSharpSensor.SensorReached(321);
  }
}
