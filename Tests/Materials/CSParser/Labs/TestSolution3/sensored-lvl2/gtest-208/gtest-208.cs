public class SomeClass
{
}
public class Foo<T> where T : class
{
  public T Do(object o)
  {
    T RNTRNTRNT_12 = o as T;
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    return RNTRNTRNT_12;
  }
}
class Driver
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(54);
    Foo<SomeClass> f = new Foo<SomeClass>();
    f.Do("something");
    IACSharpSensor.IACSharpSensor.SensorReached(55);
  }
}
