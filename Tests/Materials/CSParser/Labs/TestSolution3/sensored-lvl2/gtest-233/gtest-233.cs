using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
class Program
{
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(168);
    MyClass<int> list = new MyClass<int>();
    list.ListChanged += new ListChangedEventHandler(list_ListChanged);
    IACSharpSensor.IACSharpSensor.SensorReached(169);
  }
  static void list_ListChanged(object sender, ListChangedEventArgs e)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(170);
  }
}
