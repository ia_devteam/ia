using System;
using System.Collections;
using System.Collections.Generic;
public class Qux<X, V> : IEnumerable<V> where V : IComparable<V>
{
  public IEnumerator<V> GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(452);
    yield break;
  }
  IEnumerator IEnumerable.GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(453);
    yield break;
  }
}
public class Foo<X, V> : Qux<X, V> where V : IComparable<V>
{
}
public class Test<T> : IComparable<Test<T>>
{
  public int CompareTo(Test<T> t)
  {
    System.Int32 RNTRNTRNT_126 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(454);
    return RNTRNTRNT_126;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(455);
    Foo<X, Test<X>> foo = new Foo<X, Test<X>>();
    IACSharpSensor.IACSharpSensor.SensorReached(456);
    foreach (Test<X> test in foo) {
      ;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(457);
  }
}
