class Test<T>
{
  int priv;
  private sealed class Inner<U>
  {
    Test<U> test;
    void Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(388);
      test.priv = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(389);
    }
  }
}
class Test
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(390);
  }
}
