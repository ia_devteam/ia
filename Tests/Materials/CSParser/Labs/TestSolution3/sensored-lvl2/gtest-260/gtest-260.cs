class A<T> where T : class
{
}
class B<T> : A<T> where T : class
{
}
class Test
{
  static internal A<Test> x = new B<Test>();
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(355);
  }
}
