using System;
public delegate void GenericEventHandler<U, V>(U u, V v);
public class GenericEventNotUsedTest<T>
{
  event GenericEventHandler<GenericEventNotUsedTest<T>, T> TestEvent;
  public void RaiseTestEvent(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(675);
    TestEvent(this, t);
    IACSharpSensor.IACSharpSensor.SensorReached(676);
  }
}
public interface IFoo
{
  event EventHandler blah;
}
public static class TestEntry
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(677);
  }
}
