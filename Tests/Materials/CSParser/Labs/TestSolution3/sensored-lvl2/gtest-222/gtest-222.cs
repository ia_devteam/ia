interface IFoo
{
}
interface IBar : IFoo
{
}
class Mona<T> where T : IFoo
{
}
class Test
{
  public Mona<K> GetMona<K>() where K : IBar
  {
    Mona<K> RNTRNTRNT_23 = new Mona<K>();
    IACSharpSensor.IACSharpSensor.SensorReached(117);
    return RNTRNTRNT_23;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(118);
  }
}
