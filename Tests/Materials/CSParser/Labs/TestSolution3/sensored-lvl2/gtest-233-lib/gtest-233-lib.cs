using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
public class MyClass<TItem>
{
  public MyClass()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(171);
  }
  public event ListChangedEventHandler ListChanged;
  public void AddListChangedEventHandler(ListChangedEventHandler handler)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(172);
    ListChanged += handler;
    IACSharpSensor.IACSharpSensor.SensorReached(173);
  }
  protected void OnListChanged(ListChangedEventArgs e)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(174);
  }
}
