public class Generic<T>
{
  private T[,] container = new T[1, 1];
  public T this[int row, int col] {
    get {
      T RNTRNTRNT_6 = container[row, col];
      IACSharpSensor.IACSharpSensor.SensorReached(19);
      return RNTRNTRNT_6;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(20);
      container[row, col] = value;
      IACSharpSensor.IACSharpSensor.SensorReached(21);
    }
  }
}
public struct Fault
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(22);
    Generic<Fault> gen = new Generic<Fault>();
    gen[0, 0] = new Fault();
    System.Console.WriteLine(gen[0, 0].ToString());
    IACSharpSensor.IACSharpSensor.SensorReached(23);
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_7 = "Hi!";
    IACSharpSensor.IACSharpSensor.SensorReached(24);
    return RNTRNTRNT_7;
  }
}
