using System;
using System.Collections.Generic;
interface I
{
  void D();
}
class X : I
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(661);
    List<object> l = new List<object>();
    List<I> i = new List<I>();
    i.Add(new X());
    l.AddRange(i.ToArray());
    IACSharpSensor.IACSharpSensor.SensorReached(662);
  }
  public void D()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(663);
  }
}
