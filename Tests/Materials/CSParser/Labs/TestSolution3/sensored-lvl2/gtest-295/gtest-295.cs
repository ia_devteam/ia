namespace Test
{
  class Cache<T> where T : class
  {
  }
  class Base
  {
  }
  class MyType<T> where T : Base
  {
    Cache<T> _cache;
  }
  class Foo
  {
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(697);
      object foo = new MyType<Base>();
      IACSharpSensor.IACSharpSensor.SensorReached(698);
    }
  }
}
