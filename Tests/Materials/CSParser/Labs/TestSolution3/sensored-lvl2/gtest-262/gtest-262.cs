using System;
using System.Reflection;
using System.Runtime.InteropServices;
public class Test
{
  public void f1(  [System.Runtime.InteropServices.DefaultParameterValue(null)]
object x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(365);
  }
  public void f2(  [System.Runtime.InteropServices.DefaultParameterValue(null)]
string x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(366);
  }
  public void f3(  [System.Runtime.InteropServices.DefaultParameterValue(null)]
Test x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(367);
  }
  public void f4(  [System.Runtime.InteropServices.DefaultParameterValue(1)]
int x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(368);
  }
  public void f5(  [System.Runtime.InteropServices.DefaultParameterValue((short)1)]
short x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(369);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(370);
    string problems = "";
    Type t = typeof(Test);
    IACSharpSensor.IACSharpSensor.SensorReached(371);
    foreach (MethodInfo m in t.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)) {
      IACSharpSensor.IACSharpSensor.SensorReached(372);
      ParameterInfo p = m.GetParameters()[0];
      Console.WriteLine(m.Name + " parameter attributes: " + p.Attributes);
      IACSharpSensor.IACSharpSensor.SensorReached(373);
      if ((p.Attributes & ParameterAttributes.HasDefault) == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(374);
        problems = problems + " " + m.Name;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(375);
      if (problems.Length != 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(376);
        throw new Exception("these functions don't have default values: " + problems);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(377);
  }
}
