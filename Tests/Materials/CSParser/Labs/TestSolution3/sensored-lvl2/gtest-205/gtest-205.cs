using System;
using System.Reflection;
public class Foo<S>
{
}
public struct Bar<T>
{
}
public class Test<U>
{
  public static void Func(U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(39);
    Console.WriteLine(u);
    IACSharpSensor.IACSharpSensor.SensorReached(40);
  }
}
class X
{
  static void Test(Type t, object arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(41);
    MethodInfo mi = t.GetMethod("Func");
    mi.Invoke(null, new object[] { arg });
    IACSharpSensor.IACSharpSensor.SensorReached(42);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(43);
    Test(typeof(Test<Foo<int>>), new Foo<int>());
    Test(typeof(Test<Bar<int>>), new Bar<int>());
    Test(typeof(Test<Bar<string>>), new Bar<string>());
    Test(typeof(Test<Foo<DateTime>>), new Foo<DateTime>());
    Test(typeof(Test<DateTime>), DateTime.Now);
    Test(typeof(Test<string>), "Hello");
    IACSharpSensor.IACSharpSensor.SensorReached(44);
  }
}
