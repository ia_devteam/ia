class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(202);
    Foo<long> foo = new Foo<long>();
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    if (foo.Test(3) != 1) {
      System.Int32 RNTRNTRNT_47 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(204);
      return RNTRNTRNT_47;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(205);
    if (foo.Test(5L) != 2) {
      System.Int32 RNTRNTRNT_48 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(206);
      return RNTRNTRNT_48;
    }
    System.Int32 RNTRNTRNT_49 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(207);
    return RNTRNTRNT_49;
  }
}
