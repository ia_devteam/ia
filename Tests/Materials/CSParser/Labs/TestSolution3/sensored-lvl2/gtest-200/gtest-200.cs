class Test
{
  public static T QueryInterface<T>(object val) where T : class
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
    if (val == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      return null;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(3);
    T tval = val as T;
    IACSharpSensor.IACSharpSensor.SensorReached(4);
    if (tval != null) {
      T RNTRNTRNT_1 = tval;
      IACSharpSensor.IACSharpSensor.SensorReached(5);
      return RNTRNTRNT_1;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(6);
    return null;
  }
}
class Driver
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(7);
  }
}
