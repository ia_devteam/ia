using System;
using System.Collections.Generic;
public class Test<T>
{
  public void Invalid(T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(682);
    Other(new T[] { item });
    IACSharpSensor.IACSharpSensor.SensorReached(683);
  }
  public void Other(IEnumerable<T> collection)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(684);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(685);
  }
}
