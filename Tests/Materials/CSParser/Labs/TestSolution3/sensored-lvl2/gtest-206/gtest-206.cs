class Continuation<R, A>
{
  public static Continuation<R, A> CallCC<B>(object f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(45);
    return null;
  }
}
class Driver
{
  static Continuation<B, A> myTry<A, B>(B f, A x)
  {
    Continuation<B,A> RNTRNTRNT_11 = Continuation<B, A>.CallCC<object>(null);
    IACSharpSensor.IACSharpSensor.SensorReached(46);
    return RNTRNTRNT_11;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(47);
    myTry<int, int>(3, 7);
    IACSharpSensor.IACSharpSensor.SensorReached(48);
  }
}
