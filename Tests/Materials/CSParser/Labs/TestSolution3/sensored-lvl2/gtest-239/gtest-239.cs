using System;
class Foo<T, U>
{
  public int Test(T t, U u)
  {
    System.Int32 RNTRNTRNT_52 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(212);
    return RNTRNTRNT_52;
  }
  public int Test(int t, U u)
  {
    System.Int32 RNTRNTRNT_53 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(213);
    return RNTRNTRNT_53;
  }
  public int Test(T t, float u)
  {
    System.Int32 RNTRNTRNT_54 = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(214);
    return RNTRNTRNT_54;
  }
  public int Test(int t, float u)
  {
    System.Int32 RNTRNTRNT_55 = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    return RNTRNTRNT_55;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(216);
    Foo<long, float> a = new Foo<long, float>();
    IACSharpSensor.IACSharpSensor.SensorReached(217);
    if (a.Test(3L, 3.14f) != 3) {
      System.Int32 RNTRNTRNT_56 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(218);
      return RNTRNTRNT_56;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(219);
    if (a.Test(3L, 8) != 3) {
      System.Int32 RNTRNTRNT_57 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(220);
      return RNTRNTRNT_57;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(221);
    if (a.Test(3, 3.14f) != 4) {
      System.Int32 RNTRNTRNT_58 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(222);
      return RNTRNTRNT_58;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(223);
    if (a.Test(3, 8) != 4) {
      System.Int32 RNTRNTRNT_59 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(224);
      return RNTRNTRNT_59;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(225);
    Foo<long, double> b = new Foo<long, double>();
    IACSharpSensor.IACSharpSensor.SensorReached(226);
    if (b.Test(3L, 3.14f) != 3) {
      System.Int32 RNTRNTRNT_60 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(227);
      return RNTRNTRNT_60;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(228);
    if (b.Test(3, 3.14f) != 4) {
      System.Int32 RNTRNTRNT_61 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(229);
      return RNTRNTRNT_61;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(230);
    if (b.Test(3L, 3.14f) != 3) {
      System.Int32 RNTRNTRNT_62 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(231);
      return RNTRNTRNT_62;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(232);
    if (b.Test(3L, 5) != 3) {
      System.Int32 RNTRNTRNT_63 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(233);
      return RNTRNTRNT_63;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(234);
    Foo<string, float> c = new Foo<string, float>();
    IACSharpSensor.IACSharpSensor.SensorReached(235);
    if (c.Test("Hello", 3.14f) != 3) {
      System.Int32 RNTRNTRNT_64 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(236);
      return RNTRNTRNT_64;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(237);
    Foo<int, string> d = new Foo<int, string>();
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    if (d.Test(3, "Hello") != 2) {
      System.Int32 RNTRNTRNT_65 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(239);
      return RNTRNTRNT_65;
    }
    System.Int32 RNTRNTRNT_66 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(240);
    return RNTRNTRNT_66;
  }
}
