using System;
public abstract class A
{
  protected bool Test(int a)
  {
    System.Boolean RNTRNTRNT_179 = a == 5;
    IACSharpSensor.IACSharpSensor.SensorReached(664);
    return RNTRNTRNT_179;
  }
}
public class B : A
{
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(665);
  }
  class C : A
  {
    B b;
    public bool Foo(int a)
    {
      System.Boolean RNTRNTRNT_180 = b.Test(a);
      IACSharpSensor.IACSharpSensor.SensorReached(666);
      return RNTRNTRNT_180;
    }
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(667);
  }
}
