using System;
using System.Collections.Generic;
public delegate R Fun<A1, R>(A1 x);
class MyTest
{
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(94);
    foreach (Object d in Map<int, int, String, Object>(delegate(int x) { return x.ToString(); }, FromTo(10, 20))) {
      IACSharpSensor.IACSharpSensor.SensorReached(95);
      Console.WriteLine(d);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(96);
  }
  public static IEnumerable<Rr> Map<Aa, Af, Rf, Rr>(Fun<Af, Rf> f, IEnumerable<Aa> xs) where Aa : Af where Rf : Rr
  {
    IACSharpSensor.IACSharpSensor.SensorReached(97);
    foreach (Aa x in xs) {
      Rr RNTRNTRNT_18 = f(x);
      IACSharpSensor.IACSharpSensor.SensorReached(98);
      yield return RNTRNTRNT_18;
      IACSharpSensor.IACSharpSensor.SensorReached(99);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(100);
  }
  public static IEnumerable<int> FromTo(int @from, int to)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(101);
    for (int i = @from; i <= to; i++) {
      System.Int32 RNTRNTRNT_19 = i;
      IACSharpSensor.IACSharpSensor.SensorReached(102);
      yield return RNTRNTRNT_19;
      IACSharpSensor.IACSharpSensor.SensorReached(103);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(104);
  }
}
