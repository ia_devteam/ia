using System;
[AttributeUsage(AttributeTargets.GenericParameter)]
class GenParAttribute : Attribute
{
}
class cons<A, B>
{
  public void abc<M>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(148);
  }
}
class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(149);
  }
}
