class M
{
  static void p(string x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(49);
    System.Console.WriteLine(x);
    IACSharpSensor.IACSharpSensor.SensorReached(50);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(51);
    string[] arr = new string[] {
      "a",
      "b",
      "c"
    };
    System.Array.ForEach(arr, p);
    IACSharpSensor.IACSharpSensor.SensorReached(52);
  }
}
