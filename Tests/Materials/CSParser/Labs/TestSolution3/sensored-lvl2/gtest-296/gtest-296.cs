using System.Collections.Generic;
using System.Collections.ObjectModel;
public class MyCollection<T> : Collection<T>
{
  public void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(699);
    T t = Items[0];
    IACSharpSensor.IACSharpSensor.SensorReached(700);
  }
}
public class C
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(701);
  }
}
