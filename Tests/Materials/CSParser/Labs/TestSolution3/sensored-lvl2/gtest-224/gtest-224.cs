class Base
{
  public virtual void Foo<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(125);
  }
}
class Derived : Base
{
  public override void Foo<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(126);
  }
}
class Driver
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(127);
    new Derived().Foo<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(128);
  }
}
