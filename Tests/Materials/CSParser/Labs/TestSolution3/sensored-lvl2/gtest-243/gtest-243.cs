using System;
using System.Reflection;
using System.Collections.Generic;
public class Foo<T>
{
  public void Test(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(248);
  }
}
public class Tests
{
  public static void foo<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(249);
  }
  public static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(250);
    MethodInfo mi = typeof(Tests).GetMethod("foo");
    IACSharpSensor.IACSharpSensor.SensorReached(251);
    if (!mi.IsGenericMethod) {
      System.Int32 RNTRNTRNT_69 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(252);
      return RNTRNTRNT_69;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(253);
    if (!mi.IsGenericMethodDefinition) {
      System.Int32 RNTRNTRNT_70 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(254);
      return RNTRNTRNT_70;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(255);
    MethodInfo mi2 = mi.MakeGenericMethod(new Type[] { typeof(int) });
    IACSharpSensor.IACSharpSensor.SensorReached(256);
    if (!mi2.IsGenericMethod) {
      System.Int32 RNTRNTRNT_71 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(257);
      return RNTRNTRNT_71;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(258);
    if (mi2.IsGenericMethodDefinition) {
      System.Int32 RNTRNTRNT_72 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(259);
      return RNTRNTRNT_72;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(260);
    MethodInfo mi3 = typeof(Foo<int>).GetMethod("Test");
    IACSharpSensor.IACSharpSensor.SensorReached(261);
    if (mi3.IsGenericMethod) {
      System.Int32 RNTRNTRNT_73 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(262);
      return RNTRNTRNT_73;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(263);
    if (mi3.IsGenericMethodDefinition) {
      System.Int32 RNTRNTRNT_74 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(264);
      return RNTRNTRNT_74;
    }
    System.Int32 RNTRNTRNT_75 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(265);
    return RNTRNTRNT_75;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(266);
    int result = Test();
    System.Int32 RNTRNTRNT_76 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(267);
    return RNTRNTRNT_76;
  }
}
