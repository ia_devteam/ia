using System;
using System.Reflection;
public class TestAttribute : Attribute
{
  public Type type;
  public TestAttribute(Type type)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(653);
    this.type = type;
    IACSharpSensor.IACSharpSensor.SensorReached(654);
  }
}
class C<T>
{
  [Test(typeof(C<string>))]
  public static void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(655);
  }
}
public class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(656);
    MethodInfo mi = typeof(C<>).GetMethod("Foo");
    object[] a = mi.GetCustomAttributes(false);
    IACSharpSensor.IACSharpSensor.SensorReached(657);
    if (((TestAttribute)a[0]).type.ToString() != "C`1[System.String]") {
      System.Int32 RNTRNTRNT_177 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(658);
      return RNTRNTRNT_177;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(659);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_178 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(660);
    return RNTRNTRNT_178;
  }
}
