delegate void TestFunc<T>(T val);
class A
{
  public A(TestFunc<int> func)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(704);
    func(0);
    IACSharpSensor.IACSharpSensor.SensorReached(705);
  }
}
class TestClass
{
  static int i = 1;
  static readonly A a = new A(delegate(int a) { i = a; });
  static int Main()
  {
    System.Int32 RNTRNTRNT_190 = i;
    IACSharpSensor.IACSharpSensor.SensorReached(706);
    return RNTRNTRNT_190;
  }
}
