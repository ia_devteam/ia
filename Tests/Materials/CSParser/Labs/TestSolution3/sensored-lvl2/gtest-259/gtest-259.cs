using System;
public class Class1<T> where T : MyType
{
  public void MethodOfClass1(T a, MyType b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(351);
    a.MethodOfMyBaseType();
    IACSharpSensor.IACSharpSensor.SensorReached(352);
  }
}
public class MyType : MyBaseType
{
  public override void MethodOfMyBaseType()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(353);
  }
}
public abstract class MyBaseType
{
  public abstract void MethodOfMyBaseType();
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(354);
  }
}
