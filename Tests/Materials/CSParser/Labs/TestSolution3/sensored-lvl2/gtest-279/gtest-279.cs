using System;
using System.Collections.Generic;
interface IFoo
{
  void Bar();
  IList<T> Bar<T>();
}
class Foo : IFoo
{
  public void Bar()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(492);
    Console.WriteLine("Bar");
    IACSharpSensor.IACSharpSensor.SensorReached(493);
  }
  public IList<T> Bar<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(494);
    Console.WriteLine("Bar<T>");
    IACSharpSensor.IACSharpSensor.SensorReached(495);
    return null;
  }
}
class BugReport
{
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(496);
    Foo f = new Foo();
    f.Bar();
    f.Bar<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(497);
  }
}
