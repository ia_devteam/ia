using System;
class Cons<T, U>
{
  public T car;
  public U cdr;
  public Cons(T x, U y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(356);
    car = x;
    cdr = y;
    IACSharpSensor.IACSharpSensor.SensorReached(357);
  }
  public override String ToString()
  {
    String RNTRNTRNT_90 = "(" + car + '.' + cdr + ')';
    IACSharpSensor.IACSharpSensor.SensorReached(358);
    return RNTRNTRNT_90;
  }
}
class List<A> : Cons<A, List<A>>
{
  public List(A value) : base(value, null)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(359);
  }
  public List(A value, List<A> next) : base(value, next)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(360);
  }
  public void zip<B>(List<B> other)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(361);
    cdr.zip(other.cdr);
    IACSharpSensor.IACSharpSensor.SensorReached(362);
  }
}
abstract class Test
{
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(363);
    List<int> list = new List<Int32>(3);
    Console.WriteLine(list);
    IACSharpSensor.IACSharpSensor.SensorReached(364);
  }
}
