using System;
using System.Collections.Generic;
public class Test
{
  public IEnumerator<string> GetEnumerator()
  {
    System.String RNTRNTRNT_88 = "TEST";
    IACSharpSensor.IACSharpSensor.SensorReached(324);
    yield return RNTRNTRNT_88;
    IACSharpSensor.IACSharpSensor.SensorReached(325);
    IACSharpSensor.IACSharpSensor.SensorReached(326);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(327);
      int.Parse(arg);
    } catch {
      IACSharpSensor.IACSharpSensor.SensorReached(328);
      yield break;
      IACSharpSensor.IACSharpSensor.SensorReached(329);
    }
    System.String RNTRNTRNT_89 = "TEST2";
    IACSharpSensor.IACSharpSensor.SensorReached(330);
    yield return RNTRNTRNT_89;
    IACSharpSensor.IACSharpSensor.SensorReached(331);
    IACSharpSensor.IACSharpSensor.SensorReached(332);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(333);
    new Test().Run();
    IACSharpSensor.IACSharpSensor.SensorReached(334);
  }
  string arg;
  void Run()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(335);
    int i = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(336);
    foreach (string s in this) {
      IACSharpSensor.IACSharpSensor.SensorReached(337);
      i++;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(338);
    if (i != 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(339);
      throw new Exception();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(340);
    arg = "1";
    i = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(341);
    foreach (string s in this) {
      IACSharpSensor.IACSharpSensor.SensorReached(342);
      i++;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(343);
    if (i != 2) {
      IACSharpSensor.IACSharpSensor.SensorReached(344);
      throw new Exception();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(345);
  }
}
