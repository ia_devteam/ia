class C<X, Y>
{
  class Q<A, B>
  {
    public void apply(C<X, Y> t)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(25);
      t.bar<A, B>();
      IACSharpSensor.IACSharpSensor.SensorReached(26);
    }
  }
  public void foo<A, B>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(27);
    Q<A, B> q = new Q<A, B>();
    q.apply(this);
    IACSharpSensor.IACSharpSensor.SensorReached(28);
  }
  public void bar<A, B>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(29);
    System.Console.WriteLine("'{0} {1} {2} {3}'", typeof(X), typeof(Y), typeof(A), typeof(B));
    IACSharpSensor.IACSharpSensor.SensorReached(30);
  }
}
class X
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(31);
    C<int, string> c = new C<int, string>();
    c.foo<float, string>();
    IACSharpSensor.IACSharpSensor.SensorReached(32);
  }
}
