public abstract class a
{
  public abstract void func<T>(ref T arg);
}
public class b : a
{
  public override void func<T>(ref T arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(242);
  }
}
class main
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(243);
  }
}
