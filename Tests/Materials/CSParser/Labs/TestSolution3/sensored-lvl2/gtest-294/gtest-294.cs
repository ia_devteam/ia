class A
{
}
class B : A
{
}
class X
{
  public static A Test(A a, B b)
  {
    A RNTRNTRNT_184 = b ?? a;
    IACSharpSensor.IACSharpSensor.SensorReached(686);
    return RNTRNTRNT_184;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(687);
    A a = new A();
    B b = new B();
    IACSharpSensor.IACSharpSensor.SensorReached(688);
    if (Test(a, b) != b) {
      System.Int32 RNTRNTRNT_185 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(689);
      return RNTRNTRNT_185;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(690);
    if (Test(null, b) != b) {
      System.Int32 RNTRNTRNT_186 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(691);
      return RNTRNTRNT_186;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(692);
    if (Test(a, null) != a) {
      System.Int32 RNTRNTRNT_187 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(693);
      return RNTRNTRNT_187;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(694);
    if (Test(null, null) != null) {
      System.Int32 RNTRNTRNT_188 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(695);
      return RNTRNTRNT_188;
    }
    System.Int32 RNTRNTRNT_189 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(696);
    return RNTRNTRNT_189;
  }
}
