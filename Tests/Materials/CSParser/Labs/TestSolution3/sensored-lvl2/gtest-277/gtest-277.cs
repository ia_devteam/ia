using System;
using System.Collections.Generic;
public interface INode<K> : IComparable<K> where K : IComparable<K>
{
  K Key { get; }
}
public interface IBTNode<C> where C : IBTNode<C>
{
  C Parent { get; set; }
  C Left { get; set; }
  C Right { get; set; }
}
public interface IBSTNode<K, C> : IBTNode<C>, INode<K> where K : IComparable<K> where C : IBSTNode<K, C>
{
}
public interface IAVLNode<K, C> : IBSTNode<K, C> where K : IComparable<K> where C : IAVLNode<K, C>
{
  int Balance { get; set; }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(487);
  }
}
