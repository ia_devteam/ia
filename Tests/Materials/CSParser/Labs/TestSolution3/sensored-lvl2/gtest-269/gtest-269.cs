using System;
[Flags()]
enum IrishBeer
{
  Stout = 0x1000,
  Ale = 0x2000,
  Lager = 0x3000,
  Guinness = 1 | Stout,
  Smithwicks = 2 | Ale
}
struct IrishPub
{
  public readonly IrishBeer Beer;
  public IrishPub(IrishBeer beer)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(398);
    this.Beer = beer;
    IACSharpSensor.IACSharpSensor.SensorReached(399);
  }
  public static implicit operator long(IrishPub? pub)
  {
    System.Int64 RNTRNTRNT_93 = pub.HasValue ? (long)pub.Value.Beer : 0;
    IACSharpSensor.IACSharpSensor.SensorReached(400);
    return RNTRNTRNT_93;
  }
  public static implicit operator IrishPub?(long value)
  {
    System.Nullable<IrishPub> RNTRNTRNT_94 = new IrishPub((IrishBeer)value);
    IACSharpSensor.IACSharpSensor.SensorReached(401);
    return RNTRNTRNT_94;
  }
}
class X
{
  static int Beer(IrishPub? pub)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(402);
    switch (pub) {
      case 0x1001:
        System.Int32 RNTRNTRNT_95 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(403);
        return RNTRNTRNT_95;
      case 0x2002:
        System.Int32 RNTRNTRNT_96 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(404);
        return RNTRNTRNT_96;
      default:
        System.Int32 RNTRNTRNT_97 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(405);
        return RNTRNTRNT_97;
    }
  }
  static long PubToLong(IrishPub pub)
  {
    System.Int64 RNTRNTRNT_98 = pub;
    IACSharpSensor.IACSharpSensor.SensorReached(406);
    return RNTRNTRNT_98;
  }
  static int Test(int? a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    switch (a) {
      case 0:
        System.Int32 RNTRNTRNT_99 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(408);
        return RNTRNTRNT_99;
      case 3:
        System.Int32 RNTRNTRNT_100 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(409);
        return RNTRNTRNT_100;
      default:
        System.Int32 RNTRNTRNT_101 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(410);
        return RNTRNTRNT_101;
    }
  }
  static int TestWithNull(int? a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(411);
    switch (a) {
      case 0:
        System.Int32 RNTRNTRNT_102 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(412);
        return RNTRNTRNT_102;
      case 3:
        System.Int32 RNTRNTRNT_103 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(413);
        return RNTRNTRNT_103;
      case null:
        System.Int32 RNTRNTRNT_104 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(414);
        return RNTRNTRNT_104;
      default:
        System.Int32 RNTRNTRNT_105 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(415);
        return RNTRNTRNT_105;
    }
  }
  static long? Foo(bool flag)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(416);
    if (flag) {
      System.Nullable<System.Int64> RNTRNTRNT_106 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(417);
      return RNTRNTRNT_106;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(418);
      return null;
    }
  }
  static int Test(bool flag)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(419);
    switch (Foo(flag)) {
      case 0:
        System.Int32 RNTRNTRNT_107 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(420);
        return RNTRNTRNT_107;
      case 4:
        System.Int32 RNTRNTRNT_108 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(421);
        return RNTRNTRNT_108;
      default:
        System.Int32 RNTRNTRNT_109 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(422);
        return RNTRNTRNT_109;
    }
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(423);
    IrishPub pub = new IrishPub(IrishBeer.Guinness);
    IACSharpSensor.IACSharpSensor.SensorReached(424);
    if (PubToLong(pub) != 0x1001) {
      System.Int32 RNTRNTRNT_110 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(425);
      return RNTRNTRNT_110;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(426);
    if (Beer(null) != 3) {
      System.Int32 RNTRNTRNT_111 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(427);
      return RNTRNTRNT_111;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(428);
    if (Beer(new IrishPub(IrishBeer.Guinness)) != 1) {
      System.Int32 RNTRNTRNT_112 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(429);
      return RNTRNTRNT_112;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(430);
    if (Test(null) != 2) {
      System.Int32 RNTRNTRNT_113 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(431);
      return RNTRNTRNT_113;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(432);
    if (Test(3) != 1) {
      System.Int32 RNTRNTRNT_114 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(433);
      return RNTRNTRNT_114;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(434);
    if (Test(true) != 1) {
      System.Int32 RNTRNTRNT_115 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(435);
      return RNTRNTRNT_115;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(436);
    if (Test(false) != 2) {
      System.Int32 RNTRNTRNT_116 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(437);
      return RNTRNTRNT_116;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(438);
    if (TestWithNull(null) != 2) {
      System.Int32 RNTRNTRNT_117 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(439);
      return RNTRNTRNT_117;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(440);
    if (TestWithNull(3) != 1) {
      System.Int32 RNTRNTRNT_118 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(441);
      return RNTRNTRNT_118;
    }
    System.Int32 RNTRNTRNT_119 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(442);
    return RNTRNTRNT_119;
  }
}
