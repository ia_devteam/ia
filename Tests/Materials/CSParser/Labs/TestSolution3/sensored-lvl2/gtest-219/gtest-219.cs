using System;
using System.Reflection;
public interface IFoo : ICloneable
{
}
public class Test
{
  public void Foo<T>() where T : IFoo
  {
    IACSharpSensor.IACSharpSensor.SensorReached(106);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(107);
    MethodInfo mi = typeof(Test).GetMethod("Foo");
    Type t = mi.GetGenericArguments()[0];
    Type[] ifaces = t.GetGenericParameterConstraints();
    IACSharpSensor.IACSharpSensor.SensorReached(108);
    if (ifaces.Length != 1) {
      System.Int32 RNTRNTRNT_20 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(109);
      return RNTRNTRNT_20;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(110);
    if (ifaces[0] != typeof(IFoo)) {
      System.Int32 RNTRNTRNT_21 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(111);
      return RNTRNTRNT_21;
    }
    System.Int32 RNTRNTRNT_22 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(112);
    return RNTRNTRNT_22;
  }
}
