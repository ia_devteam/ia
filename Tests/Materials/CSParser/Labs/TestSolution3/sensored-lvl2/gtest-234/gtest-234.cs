public interface IFoo
{
}
public interface IFoo<T> : IFoo
{
}
public class Test
{
  public IFoo GetFoo()
  {
    IFoo RNTRNTRNT_33 = GetFooGeneric<object>();
    IACSharpSensor.IACSharpSensor.SensorReached(175);
    return RNTRNTRNT_33;
  }
  public IFoo<T> GetFooGeneric<T>()
  {
    IFoo<T> RNTRNTRNT_34 = default(IFoo<T>);
    IACSharpSensor.IACSharpSensor.SensorReached(176);
    return RNTRNTRNT_34;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    Test test = new Test();
    test.GetFoo();
    IACSharpSensor.IACSharpSensor.SensorReached(178);
  }
}
