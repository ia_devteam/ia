using System;
using SCG = System.Collections.Generic;
public interface ISorted<S>
{
  void AddSorted<T>(SCG.IEnumerable<T> items) where T : S;
}
public class SortedIndexedTester<T>
{
  public void Test(ISorted<int> sorted)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(302);
    sorted.AddSorted(new int[] {
      31,
      62,
      63,
      93
    });
    IACSharpSensor.IACSharpSensor.SensorReached(303);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(304);
  }
}
