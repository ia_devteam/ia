using System;
using System.Collections;
using System.Collections.Generic;
public class Test
{
  public class C
  {
    public C()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(476);
      Type t = typeof(Dictionary<, >);
      IACSharpSensor.IACSharpSensor.SensorReached(477);
    }
  }
  public class D<T, U>
  {
    public D()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(478);
      Type t = typeof(Dictionary<, >);
      IACSharpSensor.IACSharpSensor.SensorReached(479);
    }
  }
  public class E<T>
  {
    public E()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(480);
      Type t = typeof(Dictionary<, >);
      IACSharpSensor.IACSharpSensor.SensorReached(481);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(482);
    new C();
    new D<string, string>();
    new E<string>();
    IACSharpSensor.IACSharpSensor.SensorReached(483);
  }
}
