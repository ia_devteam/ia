using System;
using SCG = System.Collections.Generic;
public abstract class EnumerableBase<T> : SCG.IEnumerable<T>
{
  public abstract SCG.IEnumerator<T> GetEnumerator();
  System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
  {
    System.Collections.IEnumerator RNTRNTRNT_83 = GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(296);
    return RNTRNTRNT_83;
  }
}
public abstract class ArrayBase<T> : EnumerableBase<T>
{
  public override SCG.IEnumerator<T> GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(297);
    yield break;
  }
}
public class HashedArrayList<T> : ArrayBase<T>
{
  public override SCG.IEnumerator<T> GetEnumerator()
  {
    SCG.IEnumerator<T> RNTRNTRNT_84 = base.GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(298);
    return RNTRNTRNT_84;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(299);
  }
}
