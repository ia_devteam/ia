using System;
using System.Reflection;
public abstract class Foo<T> where T : class
{
}
public class Test
{
  public Foo<K> Hoge<K>() where K : class
  {
    IACSharpSensor.IACSharpSensor.SensorReached(119);
    return null;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(120);
    MethodInfo mi = typeof(Test).GetMethod("Hoge");
    IACSharpSensor.IACSharpSensor.SensorReached(121);
    foreach (Type t in mi.GetGenericArguments()) {
      IACSharpSensor.IACSharpSensor.SensorReached(122);
      if ((t.GenericParameterAttributes & GenericParameterAttributes.ReferenceTypeConstraint) == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(123);
        throw new Exception();
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(124);
  }
}
