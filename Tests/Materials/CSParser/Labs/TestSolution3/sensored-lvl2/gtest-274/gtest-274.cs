using System;
public struct Foo
{
  public readonly long Value;
  public Foo(long value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(465);
    this.Value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(466);
  }
  public static implicit operator Foo(long value)
  {
    Foo RNTRNTRNT_127 = new Foo(value);
    IACSharpSensor.IACSharpSensor.SensorReached(467);
    return RNTRNTRNT_127;
  }
}
public struct Bar
{
  public readonly Foo Foo;
  public Bar(Foo foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(468);
    this.Foo = foo;
    IACSharpSensor.IACSharpSensor.SensorReached(469);
  }
  public static implicit operator Bar(Foo foo)
  {
    Bar RNTRNTRNT_128 = new Bar(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(470);
    return RNTRNTRNT_128;
  }
}
public struct Baz
{
  public readonly Foo Foo;
  public Baz(Foo foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(471);
    this.Foo = foo;
    IACSharpSensor.IACSharpSensor.SensorReached(472);
  }
  public static explicit operator Baz(Foo foo)
  {
    Baz RNTRNTRNT_129 = new Baz(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(473);
    return RNTRNTRNT_129;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(474);
    int a = 3;
    int? b = a;
    int? b0 = null;
    Foo? f1 = a;
    Foo? f2 = b;
    Foo? f3 = b0;
    Foo f4 = (Foo)b;
    Bar? b1 = f1;
    Bar? b2 = f2;
    Bar? b3 = f3;
    Bar b4 = (Bar)f2;
    Baz? z1 = (Baz?)f1;
    Baz? z2 = (Baz?)f2;
    Baz? z3 = (Baz?)f3;
    Baz z4 = (Baz)f2;
    IACSharpSensor.IACSharpSensor.SensorReached(475);
  }
}
