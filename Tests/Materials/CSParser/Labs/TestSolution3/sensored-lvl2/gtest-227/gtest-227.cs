using System;
using System.Runtime.CompilerServices;
public interface Indexed
{
  [IndexerName("Foo")]
  int this[int ix] { get; }
}
public class Foo<G> where G : Indexed
{
  public static void Bar()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(145);
    int i = default(G)[0];
    IACSharpSensor.IACSharpSensor.SensorReached(146);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(147);
  }
}
