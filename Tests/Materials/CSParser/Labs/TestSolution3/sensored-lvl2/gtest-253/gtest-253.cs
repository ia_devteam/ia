using System;
using SCG = System.Collections.Generic;
public interface IExtensible<T>
{
  void AddAll<U>(SCG.IEnumerable<U> items) where U : T;
}
public abstract class CollectionValueTester<R, S> where R : IExtensible<S>
{
  protected R collection;
}
public class ExtensibleTester<U> : CollectionValueTester<U, int> where U : IExtensible<int>
{
  public ExtensibleTester(U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(308);
    this.collection = u;
    IACSharpSensor.IACSharpSensor.SensorReached(309);
  }
  public void Direct()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(310);
    collection.AddAll<int>(new int[] {
      
    });
    IACSharpSensor.IACSharpSensor.SensorReached(311);
  }
}
public class Extensible<V> : IExtensible<V>
{
  public void AddAll<W>(SCG.IEnumerable<W> items) where W : V
  {
    IACSharpSensor.IACSharpSensor.SensorReached(312);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(313);
    Extensible<int> ext = new Extensible<int>();
    ExtensibleTester<Extensible<int>> tester = new ExtensibleTester<Extensible<int>>(ext);
    tester.Direct();
    IACSharpSensor.IACSharpSensor.SensorReached(314);
  }
}
