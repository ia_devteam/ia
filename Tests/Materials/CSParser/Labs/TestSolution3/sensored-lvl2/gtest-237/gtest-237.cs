using System;
class Foo<T>
{
  public int Test(T foo)
  {
    System.Int32 RNTRNTRNT_41 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(192);
    return RNTRNTRNT_41;
  }
  public int Test(int foo)
  {
    System.Int32 RNTRNTRNT_42 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(193);
    return RNTRNTRNT_42;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(194);
    Foo<long> foo = new Foo<long>();
    Foo<int> bar = new Foo<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(195);
    if (foo.Test(4L) != 1) {
      System.Int32 RNTRNTRNT_43 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(196);
      return RNTRNTRNT_43;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(197);
    if (foo.Test(3) != 2) {
      System.Int32 RNTRNTRNT_44 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(198);
      return RNTRNTRNT_44;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(199);
    if (bar.Test(3) != 2) {
      System.Int32 RNTRNTRNT_45 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(200);
      return RNTRNTRNT_45;
    }
    System.Int32 RNTRNTRNT_46 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(201);
    return RNTRNTRNT_46;
  }
}
