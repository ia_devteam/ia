using System;
public delegate void Handler<T>(T t);
public class T
{
  public void Foo<T>(Handler<T> handler)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(391);
    AsyncCallback d = delegate(IAsyncResult ar) { Response<T>(handler); };
    IACSharpSensor.IACSharpSensor.SensorReached(392);
  }
  void Response<T>(Handler<T> handler)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(393);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(394);
  }
}
