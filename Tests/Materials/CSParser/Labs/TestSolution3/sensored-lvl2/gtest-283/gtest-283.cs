public interface IFoo
{
  void Foo<T>(ref T? v) where T : struct;
  void Foo<T>(ref T v) where T : new();
}
public struct Point
{
  int x, y;
  public Point(int x, int y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(505);
    this.x = x;
    this.y = y;
    IACSharpSensor.IACSharpSensor.SensorReached(506);
  }
}
struct TestPoint
{
  public static void Serialize(IFoo h)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(507);
    Point point1 = new Point(0, 1);
    Point? point2 = new Point(1, 2);
    h.Foo(ref point1);
    h.Foo(ref point2);
    IACSharpSensor.IACSharpSensor.SensorReached(508);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(509);
  }
}
