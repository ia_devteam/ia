using System;
public abstract class A
{
  public abstract T Foo<T>();
}
public abstract class B : A
{
  public override T Foo<T>()
  {
    T RNTRNTRNT_87 = default(T);
    IACSharpSensor.IACSharpSensor.SensorReached(322);
    return RNTRNTRNT_87;
  }
}
public class C : B
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(323);
  }
}
