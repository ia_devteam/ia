using System;
using System.Reflection;
public struct Container<T>
{
  public T content;
  public Container(T content)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(134);
    this.content = content;
    IACSharpSensor.IACSharpSensor.SensorReached(135);
  }
}
public class A
{
  public Container<long> field;
  public A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(136);
    field = new Container<long>(0xdeadbeafu);
    IACSharpSensor.IACSharpSensor.SensorReached(137);
  }
}
public class M
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(138);
    A a = new A();
    IACSharpSensor.IACSharpSensor.SensorReached(139);
    if (a.field.content != 0xdeadbeafu) {
      System.Int32 RNTRNTRNT_25 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(140);
      return RNTRNTRNT_25;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(141);
    FieldInfo fi = a.GetType().GetField("field");
    object o = fi.GetValue(a);
    Container<long> unboxed = (Container<long>)o;
    IACSharpSensor.IACSharpSensor.SensorReached(142);
    if (unboxed.content != 0xdeadbeafu) {
      System.Int32 RNTRNTRNT_26 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(143);
      return RNTRNTRNT_26;
    }
    System.Int32 RNTRNTRNT_27 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(144);
    return RNTRNTRNT_27;
  }
}
