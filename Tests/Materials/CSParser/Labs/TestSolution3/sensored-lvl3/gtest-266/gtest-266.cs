class Test<T>
{
  int priv;
  private sealed class Inner<U>
  {
    Test<U> test;
    void Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(262);
      test.priv = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(263);
    }
  }
}
class Test
{
  static void Main()
  {
  }
}
