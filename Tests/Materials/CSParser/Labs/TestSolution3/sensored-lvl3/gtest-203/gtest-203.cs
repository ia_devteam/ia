class C<X, Y>
{
  class Q<A, B>
  {
    public void apply(C<X, Y> t)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(20);
      t.bar<A, B>();
      IACSharpSensor.IACSharpSensor.SensorReached(21);
    }
  }
  public void foo<A, B>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(22);
    Q<A, B> q = new Q<A, B>();
    q.apply(this);
    IACSharpSensor.IACSharpSensor.SensorReached(23);
  }
  public void bar<A, B>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(24);
    System.Console.WriteLine("'{0} {1} {2} {3}'", typeof(X), typeof(Y), typeof(A), typeof(B));
    IACSharpSensor.IACSharpSensor.SensorReached(25);
  }
}
class X
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(26);
    C<int, string> c = new C<int, string>();
    c.foo<float, string>();
    IACSharpSensor.IACSharpSensor.SensorReached(27);
  }
}
