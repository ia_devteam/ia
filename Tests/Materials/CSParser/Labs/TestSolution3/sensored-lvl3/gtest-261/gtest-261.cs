using System;
class Cons<T, U>
{
  public T car;
  public U cdr;
  public Cons(T x, U y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    car = x;
    cdr = y;
    IACSharpSensor.IACSharpSensor.SensorReached(246);
  }
  public override String ToString()
  {
    String RNTRNTRNT_90 = "(" + car + '.' + cdr + ')';
    IACSharpSensor.IACSharpSensor.SensorReached(247);
    return RNTRNTRNT_90;
  }
}
class List<A> : Cons<A, List<A>>
{
  public List(A value) : base(value, null)
  {
  }
  public List(A value, List<A> next) : base(value, next)
  {
  }
  public void zip<B>(List<B> other)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(248);
    cdr.zip(other.cdr);
    IACSharpSensor.IACSharpSensor.SensorReached(249);
  }
}
abstract class Test
{
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(250);
    List<int> list = new List<Int32>(3);
    Console.WriteLine(list);
    IACSharpSensor.IACSharpSensor.SensorReached(251);
  }
}
