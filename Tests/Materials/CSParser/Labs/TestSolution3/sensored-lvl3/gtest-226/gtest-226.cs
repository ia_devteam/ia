using System;
using System.Reflection;
public struct Container<T>
{
  public T content;
  public Container(T content)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(104);
    this.content = content;
    IACSharpSensor.IACSharpSensor.SensorReached(105);
  }
}
public class A
{
  public Container<long> field;
  public A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(106);
    field = new Container<long>(0xdeadbeafu);
    IACSharpSensor.IACSharpSensor.SensorReached(107);
  }
}
public class M
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(108);
    A a = new A();
    if (a.field.content != 0xdeadbeafu) {
      System.Int32 RNTRNTRNT_25 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(109);
      return RNTRNTRNT_25;
    }
    FieldInfo fi = a.GetType().GetField("field");
    object o = fi.GetValue(a);
    Container<long> unboxed = (Container<long>)o;
    if (unboxed.content != 0xdeadbeafu) {
      System.Int32 RNTRNTRNT_26 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(110);
      return RNTRNTRNT_26;
    }
    System.Int32 RNTRNTRNT_27 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(111);
    return RNTRNTRNT_27;
  }
}
