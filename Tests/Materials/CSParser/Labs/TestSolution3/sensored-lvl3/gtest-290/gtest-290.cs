using System;
public delegate void GenericEventHandler<U, V>(U u, V v);
public class GenericEventNotUsedTest<T>
{
  event GenericEventHandler<GenericEventNotUsedTest<T>, T> TestEvent;
  public void RaiseTestEvent(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(427);
    TestEvent(this, t);
    IACSharpSensor.IACSharpSensor.SensorReached(428);
  }
}
public interface IFoo
{
  event EventHandler blah;
}
public static class TestEntry
{
  public static void Main()
  {
  }
}
