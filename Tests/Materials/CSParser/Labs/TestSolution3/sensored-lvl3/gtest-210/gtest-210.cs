public class Foo<T> where T : Foo<T>
{
  public T n;
  public T next()
  {
    T RNTRNTRNT_13 = n;
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    return RNTRNTRNT_13;
  }
}
public class Goo : Foo<Goo>
{
  public int x;
}
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(54);
    Goo x = new Goo();
    x = x.next();
    IACSharpSensor.IACSharpSensor.SensorReached(55);
  }
}
