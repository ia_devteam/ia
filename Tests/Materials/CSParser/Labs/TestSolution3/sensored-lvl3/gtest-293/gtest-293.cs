using System;
using System.Collections.Generic;
public class Test<T>
{
  public void Invalid(T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(432);
    Other(new T[] { item });
    IACSharpSensor.IACSharpSensor.SensorReached(433);
  }
  public void Other(IEnumerable<T> collection)
  {
  }
}
class X
{
  static void Main()
  {
  }
}
