public class B
{
  public virtual T Get<T>()
  {
    T RNTRNTRNT_77 = default(T);
    IACSharpSensor.IACSharpSensor.SensorReached(189);
    return RNTRNTRNT_77;
  }
}
public class A : B
{
  public override T Get<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(190);
    T resp = base.Get<T>();
    System.Console.WriteLine("T: " + resp);
    T RNTRNTRNT_78 = resp;
    IACSharpSensor.IACSharpSensor.SensorReached(191);
    return RNTRNTRNT_78;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(192);
    new A().Get<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(193);
  }
}
