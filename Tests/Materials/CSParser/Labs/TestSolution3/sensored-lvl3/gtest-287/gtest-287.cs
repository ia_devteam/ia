using System;
using System.Collections.Generic;
interface I
{
  void D();
}
class X : I
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(420);
    List<object> l = new List<object>();
    List<I> i = new List<I>();
    i.Add(new X());
    l.AddRange(i.ToArray());
    IACSharpSensor.IACSharpSensor.SensorReached(421);
  }
  public void D()
  {
  }
}
