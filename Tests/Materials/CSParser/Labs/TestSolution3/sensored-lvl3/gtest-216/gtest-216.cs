public interface Ret
{
}
public interface Ret<T>
{
}
public abstract class BaseClass
{
  public virtual Ret Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(75);
    return null;
  }
  public virtual Ret<T> Foo<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(76);
    return null;
  }
  public static void Main()
  {
  }
}
public class DerivedClass : BaseClass
{
  public override Ret Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(77);
    return null;
  }
  public override Ret<T> Foo<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(78);
    return null;
  }
}
