using System;
class DerivedGenericClass<T> : BaseClass
{
  public override void Foo()
  {
  }
  public void Baz()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(194);
    Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(195);
  }
}
abstract class BaseClass
{
  public abstract void Foo();
}
class X
{
  static void Main()
  {
  }
}
