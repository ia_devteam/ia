public interface SomeInterface
{
  bool Valid { get; }
}
public struct SomeStruct : SomeInterface
{
  public bool Valid {
    get {
      System.Boolean RNTRNTRNT_14 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      return RNTRNTRNT_14;
    }
  }
}
public class Test
{
  public static void Fun<T>(T t) where T : SomeInterface
  {
    IACSharpSensor.IACSharpSensor.SensorReached(61);
    bool a = t.Valid;
    IACSharpSensor.IACSharpSensor.SensorReached(62);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(63);
    Fun(new SomeStruct());
    IACSharpSensor.IACSharpSensor.SensorReached(64);
  }
}
