using System;
class Foo<T>
{
  public int Test(Foo<T> foo)
  {
    System.Int32 RNTRNTRNT_35 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(137);
    return RNTRNTRNT_35;
  }
  public int Test(Foo<int> foo)
  {
    System.Int32 RNTRNTRNT_36 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(138);
    return RNTRNTRNT_36;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(139);
    Foo<long> foo = new Foo<long>();
    Foo<int> bar = new Foo<int>();
    if (foo.Test(foo) != 1) {
      System.Int32 RNTRNTRNT_37 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(140);
      return RNTRNTRNT_37;
    }
    if (foo.Test(bar) != 2) {
      System.Int32 RNTRNTRNT_38 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(141);
      return RNTRNTRNT_38;
    }
    if (bar.Test(bar) != 2) {
      System.Int32 RNTRNTRNT_39 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(142);
      return RNTRNTRNT_39;
    }
    System.Int32 RNTRNTRNT_40 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(143);
    return RNTRNTRNT_40;
  }
}
