class Base
{
  public virtual void Foo<T>()
  {
  }
}
class Derived : Base
{
  public override void Foo<T>()
  {
  }
}
class Driver
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(100);
    new Derived().Foo<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(101);
  }
}
