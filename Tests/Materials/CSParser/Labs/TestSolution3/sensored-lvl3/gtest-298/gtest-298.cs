delegate void TestFunc<T>(T val);
class A
{
  public A(TestFunc<int> func)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(447);
    func(0);
    IACSharpSensor.IACSharpSensor.SensorReached(448);
  }
}
class TestClass
{
  static int i = 1;
  static readonly A a = new A(delegate(int a) { i = a; });
  static int Main()
  {
    System.Int32 RNTRNTRNT_190 = i;
    IACSharpSensor.IACSharpSensor.SensorReached(449);
    return RNTRNTRNT_190;
  }
}
