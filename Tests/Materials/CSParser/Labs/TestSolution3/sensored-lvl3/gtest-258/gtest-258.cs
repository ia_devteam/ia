using System;
public class A
{
  public A()
  {
  }
}
public class B
{
}
class Foo<T> where T : new()
{
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(241);
    Foo<A> foo = new Foo<A>();
    IACSharpSensor.IACSharpSensor.SensorReached(242);
  }
}
