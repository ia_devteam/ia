using System;
public class Class1<T> where T : MyType
{
  public void MethodOfClass1(T a, MyType b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(243);
    a.MethodOfMyBaseType();
    IACSharpSensor.IACSharpSensor.SensorReached(244);
  }
}
public class MyType : MyBaseType
{
  public override void MethodOfMyBaseType()
  {
  }
}
public abstract class MyBaseType
{
  public abstract void MethodOfMyBaseType();
}
class X
{
  static void Main()
  {
  }
}
