using System;
using System.Runtime.CompilerServices;
public interface Indexed
{
  [IndexerName("Foo")]
  int this[int ix] { get; }
}
public class Foo<G> where G : Indexed
{
  public static void Bar()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(112);
    int i = default(G)[0];
    IACSharpSensor.IACSharpSensor.SensorReached(113);
  }
}
class X
{
  static void Main()
  {
  }
}
