using System;
[Flags()]
enum IrishBeer
{
  Stout = 0x1000,
  Ale = 0x2000,
  Lager = 0x3000,
  Guinness = 1 | Stout,
  Smithwicks = 2 | Ale
}
struct IrishPub
{
  public readonly IrishBeer Beer;
  public IrishPub(IrishBeer beer)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(268);
    this.Beer = beer;
    IACSharpSensor.IACSharpSensor.SensorReached(269);
  }
  public static implicit operator long(IrishPub? pub)
  {
    System.Int64 RNTRNTRNT_93 = pub.HasValue ? (long)pub.Value.Beer : 0;
    IACSharpSensor.IACSharpSensor.SensorReached(270);
    return RNTRNTRNT_93;
  }
  public static implicit operator IrishPub?(long value)
  {
    System.Nullable<IrishPub> RNTRNTRNT_94 = new IrishPub((IrishBeer)value);
    IACSharpSensor.IACSharpSensor.SensorReached(271);
    return RNTRNTRNT_94;
  }
}
class X
{
  static int Beer(IrishPub? pub)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(272);
    switch (pub) {
      case 0x1001:
        System.Int32 RNTRNTRNT_95 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(273);
        return RNTRNTRNT_95;
      case 0x2002:
        System.Int32 RNTRNTRNT_96 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(274);
        return RNTRNTRNT_96;
      default:
        System.Int32 RNTRNTRNT_97 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(275);
        return RNTRNTRNT_97;
    }
  }
  static long PubToLong(IrishPub pub)
  {
    System.Int64 RNTRNTRNT_98 = pub;
    IACSharpSensor.IACSharpSensor.SensorReached(276);
    return RNTRNTRNT_98;
  }
  static int Test(int? a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(277);
    switch (a) {
      case 0:
        System.Int32 RNTRNTRNT_99 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(278);
        return RNTRNTRNT_99;
      case 3:
        System.Int32 RNTRNTRNT_100 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(279);
        return RNTRNTRNT_100;
      default:
        System.Int32 RNTRNTRNT_101 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(280);
        return RNTRNTRNT_101;
    }
  }
  static int TestWithNull(int? a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(281);
    switch (a) {
      case 0:
        System.Int32 RNTRNTRNT_102 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(282);
        return RNTRNTRNT_102;
      case 3:
        System.Int32 RNTRNTRNT_103 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(283);
        return RNTRNTRNT_103;
      case null:
        System.Int32 RNTRNTRNT_104 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(284);
        return RNTRNTRNT_104;
      default:
        System.Int32 RNTRNTRNT_105 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(285);
        return RNTRNTRNT_105;
    }
  }
  static long? Foo(bool flag)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    if (flag) {
      System.Nullable<System.Int64> RNTRNTRNT_106 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(287);
      return RNTRNTRNT_106;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(288);
      return null;
    }
  }
  static int Test(bool flag)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(289);
    switch (Foo(flag)) {
      case 0:
        System.Int32 RNTRNTRNT_107 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(290);
        return RNTRNTRNT_107;
      case 4:
        System.Int32 RNTRNTRNT_108 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(291);
        return RNTRNTRNT_108;
      default:
        System.Int32 RNTRNTRNT_109 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(292);
        return RNTRNTRNT_109;
    }
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(293);
    IrishPub pub = new IrishPub(IrishBeer.Guinness);
    if (PubToLong(pub) != 0x1001) {
      System.Int32 RNTRNTRNT_110 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(294);
      return RNTRNTRNT_110;
    }
    if (Beer(null) != 3) {
      System.Int32 RNTRNTRNT_111 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(295);
      return RNTRNTRNT_111;
    }
    if (Beer(new IrishPub(IrishBeer.Guinness)) != 1) {
      System.Int32 RNTRNTRNT_112 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(296);
      return RNTRNTRNT_112;
    }
    if (Test(null) != 2) {
      System.Int32 RNTRNTRNT_113 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(297);
      return RNTRNTRNT_113;
    }
    if (Test(3) != 1) {
      System.Int32 RNTRNTRNT_114 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(298);
      return RNTRNTRNT_114;
    }
    if (Test(true) != 1) {
      System.Int32 RNTRNTRNT_115 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(299);
      return RNTRNTRNT_115;
    }
    if (Test(false) != 2) {
      System.Int32 RNTRNTRNT_116 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(300);
      return RNTRNTRNT_116;
    }
    if (TestWithNull(null) != 2) {
      System.Int32 RNTRNTRNT_117 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(301);
      return RNTRNTRNT_117;
    }
    if (TestWithNull(3) != 1) {
      System.Int32 RNTRNTRNT_118 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(302);
      return RNTRNTRNT_118;
    }
    System.Int32 RNTRNTRNT_119 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(303);
    return RNTRNTRNT_119;
  }
}
