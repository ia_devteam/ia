using System;
public class HashedLinkedList<T>
{
  public int? Offset;
  public static HashedLinkedList<T> GetList()
  {
    HashedLinkedList<T> RNTRNTRNT_86 = new HashedLinkedList<T>();
    IACSharpSensor.IACSharpSensor.SensorReached(221);
    return RNTRNTRNT_86;
  }
  public static void Test(int added)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(222);
    GetList().Offset += added;
    IACSharpSensor.IACSharpSensor.SensorReached(223);
  }
  public void Test(HashedLinkedList<T> view)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(224);
    view.Offset--;
    IACSharpSensor.IACSharpSensor.SensorReached(225);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(226);
    HashedLinkedList<int>.Test(5);
    HashedLinkedList<long> list = new HashedLinkedList<long>();
    list.Test(list);
    IACSharpSensor.IACSharpSensor.SensorReached(227);
  }
}
