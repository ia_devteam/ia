using System;
using System.Collections.Generic;
public class Test
{
  public IEnumerator<string> GetEnumerator()
  {
    System.String RNTRNTRNT_88 = "TEST";
    IACSharpSensor.IACSharpSensor.SensorReached(229);
    yield return RNTRNTRNT_88;
    IACSharpSensor.IACSharpSensor.SensorReached(230);
    try {
      int.Parse(arg);
    } catch {
      IACSharpSensor.IACSharpSensor.SensorReached(231);
      yield break;
    }
    System.String RNTRNTRNT_89 = "TEST2";
    IACSharpSensor.IACSharpSensor.SensorReached(232);
    yield return RNTRNTRNT_89;
    IACSharpSensor.IACSharpSensor.SensorReached(233);
    IACSharpSensor.IACSharpSensor.SensorReached(234);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(235);
    new Test().Run();
    IACSharpSensor.IACSharpSensor.SensorReached(236);
  }
  string arg;
  void Run()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(237);
    int i = 0;
    foreach (string s in this) {
      i++;
    }
    if (i != 1) {
      throw new Exception();
    }
    arg = "1";
    i = 0;
    foreach (string s in this) {
      i++;
    }
    if (i != 2) {
      throw new Exception();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(238);
  }
}
