using System;
using System.Collections;
using System.Collections.Generic;
public interface Y
{
}
public class X : Y
{
}
public struct Foo : Y
{
}
public static class CollectionTester
{
  static int Test<T>(IList<T> list)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(363);
    if (list.Count != 1) {
      System.Int32 RNTRNTRNT_135 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(364);
      return RNTRNTRNT_135;
    }
    ICollection<T> collection = list;
    if (collection.Count != 1) {
      System.Int32 RNTRNTRNT_136 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(365);
      return RNTRNTRNT_136;
    }
    IEnumerable<T> enumerable = list;
    IEnumerator<T> enumerator = enumerable.GetEnumerator();
    if (!enumerator.MoveNext()) {
      System.Int32 RNTRNTRNT_137 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(366);
      return RNTRNTRNT_137;
    }
    if (enumerator.MoveNext()) {
      System.Int32 RNTRNTRNT_138 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(367);
      return RNTRNTRNT_138;
    }
    System.Int32 RNTRNTRNT_139 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    return RNTRNTRNT_139;
  }
  public static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(369);
    X[] xarray = new X[] { new X() };
    int result;
    result = Test<X>(xarray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_140 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(370);
      return RNTRNTRNT_140;
    }
    result = Test<object>(xarray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_141 = 10 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(371);
      return RNTRNTRNT_141;
    }
    result = Test<Y>(xarray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_142 = 20 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(372);
      return RNTRNTRNT_142;
    }
    int[] iarray = new int[] { 5 };
    result = Test<int>(iarray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_143 = 30 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(373);
      return RNTRNTRNT_143;
    }
    result = Test<uint>((IList<uint>)(object)iarray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_144 = 40 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(374);
      return RNTRNTRNT_144;
    }
    uint[] uiarray = new uint[] { 5 };
    result = Test<int>((IList<int>)(object)uiarray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_145 = 50 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(375);
      return RNTRNTRNT_145;
    }
    result = Test<uint>(uiarray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_146 = 60 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(376);
      return RNTRNTRNT_146;
    }
    long[] larray = new long[] { 5 };
    result = Test<long>(larray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_147 = 70 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(377);
      return RNTRNTRNT_147;
    }
    result = Test<ulong>((IList<ulong>)(object)larray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_148 = 80 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(378);
      return RNTRNTRNT_148;
    }
    ulong[] ularray = new ulong[] { 5 };
    result = Test<long>((IList<long>)(object)ularray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_149 = 90 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(379);
      return RNTRNTRNT_149;
    }
    result = Test<ulong>(ularray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_150 = 100 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(380);
      return RNTRNTRNT_150;
    }
    short[] sarray = new short[] { 5 };
    result = Test<short>(sarray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_151 = 110 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(381);
      return RNTRNTRNT_151;
    }
    result = Test<ushort>((IList<ushort>)(object)sarray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_152 = 120 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(382);
      return RNTRNTRNT_152;
    }
    ushort[] usarray = new ushort[] { 5 };
    result = Test<short>((IList<short>)(object)usarray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_153 = 130 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(383);
      return RNTRNTRNT_153;
    }
    result = Test<ushort>(usarray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_154 = 140 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(384);
      return RNTRNTRNT_154;
    }
    byte[] barray = new byte[] { 5 };
    result = Test<byte>(barray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_155 = 150 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(385);
      return RNTRNTRNT_155;
    }
    result = Test<sbyte>((IList<sbyte>)(object)barray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_156 = 160 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(386);
      return RNTRNTRNT_156;
    }
    sbyte[] sbarray = new sbyte[] { 5 };
    result = Test<byte>((IList<byte>)(object)sbarray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_157 = 170 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(387);
      return RNTRNTRNT_157;
    }
    result = Test<sbyte>(sbarray);
    if (result != 0) {
      System.Int32 RNTRNTRNT_158 = 180 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(388);
      return RNTRNTRNT_158;
    }
    System.Int32 RNTRNTRNT_159 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(389);
    return RNTRNTRNT_159;
  }
}
public static class InterfaceTester
{
  public const bool Debug = false;
  static readonly Type ilist_type;
  static readonly Type icollection_type;
  static readonly Type ienumerable_type;
  static readonly Type generic_ilist_type;
  static readonly Type generic_icollection_type;
  static readonly Type generic_ienumerable_type;
  static readonly Type icloneable_type;
  static InterfaceTester()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(390);
    ilist_type = typeof(IList);
    icollection_type = typeof(ICollection);
    ienumerable_type = typeof(IEnumerable);
    generic_ilist_type = typeof(IList<>);
    generic_icollection_type = typeof(ICollection<>);
    generic_ienumerable_type = typeof(IEnumerable<>);
    icloneable_type = typeof(ICloneable);
    IACSharpSensor.IACSharpSensor.SensorReached(391);
  }
  enum State
  {
    Missing,
    Found,
    Extra
  }
  static int Test(Type t, params Type[] iface_types)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(392);
    Hashtable ifaces = new Hashtable();
    ifaces.Add(ilist_type, State.Missing);
    ifaces.Add(icollection_type, State.Missing);
    ifaces.Add(ienumerable_type, State.Missing);
    ifaces.Add(icloneable_type, State.Missing);
    Type array_type = t.MakeArrayType();
    if (Debug) {
      Console.WriteLine("Checking {0}", t);
      foreach (Type iface in t.GetInterfaces()) {
        Console.WriteLine("  {0}", iface);
      }
    }
    foreach (Type iface in iface_types) {
      Type[] gargs = new Type[] { iface };
      ifaces.Add(generic_ilist_type.MakeGenericType(gargs), State.Missing);
      ifaces.Add(generic_icollection_type.MakeGenericType(gargs), State.Missing);
      ifaces.Add(generic_ienumerable_type.MakeGenericType(gargs), State.Missing);
    }
    foreach (Type iface in array_type.GetInterfaces()) {
      if (ifaces.Contains(iface)) {
        ifaces[iface] = State.Found;
      } else {
        ifaces.Add(iface, State.Extra);
      }
    }
    int errors = 0;
    foreach (Type iface in ifaces.Keys) {
      State state = (State)ifaces[iface];
      if (state == State.Found) {
        if (Debug) {
          Console.WriteLine("Found {0}", iface);
        }
        continue;
      } else {
        if (Debug) {
          Console.WriteLine("ERROR: {0} {1}", iface, state);
        }
        errors++;
      }
    }
    if (Debug) {
      Console.WriteLine();
    }
    System.Int32 RNTRNTRNT_160 = errors;
    IACSharpSensor.IACSharpSensor.SensorReached(393);
    return RNTRNTRNT_160;
  }
  public static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(394);
    int result = Test(typeof(X), typeof(X));
    if (result != 0) {
      System.Int32 RNTRNTRNT_161 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(395);
      return RNTRNTRNT_161;
    }
    result = Test(typeof(Y), typeof(Y));
    if (result != 0) {
      System.Int32 RNTRNTRNT_162 = 100 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(396);
      return RNTRNTRNT_162;
    }
    result = Test(typeof(DateTime), typeof(DateTime));
    if (result != 0) {
      System.Int32 RNTRNTRNT_163 = 200 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(397);
      return RNTRNTRNT_163;
    }
    result = Test(typeof(float), typeof(float));
    if (result != 0) {
      System.Int32 RNTRNTRNT_164 = 300 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(398);
      return RNTRNTRNT_164;
    }
    result = Test(typeof(int), typeof(int));
    if (result != 0) {
      System.Int32 RNTRNTRNT_165 = 400 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(399);
      return RNTRNTRNT_165;
    }
    result = Test(typeof(uint), typeof(uint));
    if (result != 0) {
      System.Int32 RNTRNTRNT_166 = 500 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(400);
      return RNTRNTRNT_166;
    }
    result = Test(typeof(long), typeof(long));
    if (result != 0) {
      System.Int32 RNTRNTRNT_167 = 600 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(401);
      return RNTRNTRNT_167;
    }
    result = Test(typeof(ulong), typeof(ulong));
    if (result != 0) {
      System.Int32 RNTRNTRNT_168 = 700 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(402);
      return RNTRNTRNT_168;
    }
    result = Test(typeof(short), typeof(short));
    if (result != 0) {
      System.Int32 RNTRNTRNT_169 = 800 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(403);
      return RNTRNTRNT_169;
    }
    result = Test(typeof(ushort), typeof(ushort));
    if (result != 0) {
      System.Int32 RNTRNTRNT_170 = 900 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(404);
      return RNTRNTRNT_170;
    }
    result = Test(typeof(Foo), typeof(Foo));
    if (result != 0) {
      System.Int32 RNTRNTRNT_171 = 1000 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(405);
      return RNTRNTRNT_171;
    }
    System.Int32 RNTRNTRNT_172 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(406);
    return RNTRNTRNT_172;
  }
}
class Z
{
  static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    int result;
    result = CollectionTester.Test();
    if (result != 0) {
      System.Int32 RNTRNTRNT_173 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(408);
      return RNTRNTRNT_173;
    }
    result = InterfaceTester.Test();
    if (result != 0) {
      System.Int32 RNTRNTRNT_174 = 10000 + result;
      IACSharpSensor.IACSharpSensor.SensorReached(409);
      return RNTRNTRNT_174;
    }
    System.Int32 RNTRNTRNT_175 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(410);
    return RNTRNTRNT_175;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(411);
    int result = Test();
    if (result == 0) {
      Console.WriteLine("OK");
    } else {
      Console.WriteLine("ERROR: {0}", result);
    }
    System.Int32 RNTRNTRNT_176 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(412);
    return RNTRNTRNT_176;
  }
}
