using System;
using System.Reflection;
public class Foo<S>
{
}
public struct Bar<T>
{
}
public class Test<U>
{
  public static void Func(U u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(34);
    Console.WriteLine(u);
    IACSharpSensor.IACSharpSensor.SensorReached(35);
  }
}
class X
{
  static void Test(Type t, object arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(36);
    MethodInfo mi = t.GetMethod("Func");
    mi.Invoke(null, new object[] { arg });
    IACSharpSensor.IACSharpSensor.SensorReached(37);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(38);
    Test(typeof(Test<Foo<int>>), new Foo<int>());
    Test(typeof(Test<Bar<int>>), new Bar<int>());
    Test(typeof(Test<Bar<string>>), new Bar<string>());
    Test(typeof(Test<Foo<DateTime>>), new Foo<DateTime>());
    Test(typeof(Test<DateTime>), DateTime.Now);
    Test(typeof(Test<string>), "Hello");
    IACSharpSensor.IACSharpSensor.SensorReached(39);
  }
}
