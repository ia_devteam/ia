using System;
using System.Collections.Generic;
interface IFoo
{
  void Bar();
  IList<T> Bar<T>();
}
class Foo : IFoo
{
  public void Bar()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(347);
    Console.WriteLine("Bar");
    IACSharpSensor.IACSharpSensor.SensorReached(348);
  }
  public IList<T> Bar<T>()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(349);
    Console.WriteLine("Bar<T>");
    IACSharpSensor.IACSharpSensor.SensorReached(350);
    return null;
  }
}
class BugReport
{
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(351);
    Foo f = new Foo();
    f.Bar();
    f.Bar<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(352);
  }
}
