class A
{
}
class B : A
{
}
class X
{
  public static A Test(A a, B b)
  {
    A RNTRNTRNT_184 = b ?? a;
    IACSharpSensor.IACSharpSensor.SensorReached(434);
    return RNTRNTRNT_184;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(435);
    A a = new A();
    B b = new B();
    if (Test(a, b) != b) {
      System.Int32 RNTRNTRNT_185 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(436);
      return RNTRNTRNT_185;
    }
    if (Test(null, b) != b) {
      System.Int32 RNTRNTRNT_186 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(437);
      return RNTRNTRNT_186;
    }
    if (Test(a, null) != a) {
      System.Int32 RNTRNTRNT_187 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(438);
      return RNTRNTRNT_187;
    }
    if (Test(null, null) != null) {
      System.Int32 RNTRNTRNT_188 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(439);
      return RNTRNTRNT_188;
    }
    System.Int32 RNTRNTRNT_189 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(440);
    return RNTRNTRNT_189;
  }
}
