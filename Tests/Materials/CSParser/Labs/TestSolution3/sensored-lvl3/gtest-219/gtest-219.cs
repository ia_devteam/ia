using System;
using System.Reflection;
public interface IFoo : ICloneable
{
}
public class Test
{
  public void Foo<T>() where T : IFoo
  {
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(89);
    MethodInfo mi = typeof(Test).GetMethod("Foo");
    Type t = mi.GetGenericArguments()[0];
    Type[] ifaces = t.GetGenericParameterConstraints();
    if (ifaces.Length != 1) {
      System.Int32 RNTRNTRNT_20 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(90);
      return RNTRNTRNT_20;
    }
    if (ifaces[0] != typeof(IFoo)) {
      System.Int32 RNTRNTRNT_21 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(91);
      return RNTRNTRNT_21;
    }
    System.Int32 RNTRNTRNT_22 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(92);
    return RNTRNTRNT_22;
  }
}
