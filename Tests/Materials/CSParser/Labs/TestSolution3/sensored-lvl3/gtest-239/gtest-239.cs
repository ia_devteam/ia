using System;
class Foo<T, U>
{
  public int Test(T t, U u)
  {
    System.Int32 RNTRNTRNT_52 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(159);
    return RNTRNTRNT_52;
  }
  public int Test(int t, U u)
  {
    System.Int32 RNTRNTRNT_53 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(160);
    return RNTRNTRNT_53;
  }
  public int Test(T t, float u)
  {
    System.Int32 RNTRNTRNT_54 = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(161);
    return RNTRNTRNT_54;
  }
  public int Test(int t, float u)
  {
    System.Int32 RNTRNTRNT_55 = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    return RNTRNTRNT_55;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(163);
    Foo<long, float> a = new Foo<long, float>();
    if (a.Test(3L, 3.14f) != 3) {
      System.Int32 RNTRNTRNT_56 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(164);
      return RNTRNTRNT_56;
    }
    if (a.Test(3L, 8) != 3) {
      System.Int32 RNTRNTRNT_57 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(165);
      return RNTRNTRNT_57;
    }
    if (a.Test(3, 3.14f) != 4) {
      System.Int32 RNTRNTRNT_58 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(166);
      return RNTRNTRNT_58;
    }
    if (a.Test(3, 8) != 4) {
      System.Int32 RNTRNTRNT_59 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(167);
      return RNTRNTRNT_59;
    }
    Foo<long, double> b = new Foo<long, double>();
    if (b.Test(3L, 3.14f) != 3) {
      System.Int32 RNTRNTRNT_60 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(168);
      return RNTRNTRNT_60;
    }
    if (b.Test(3, 3.14f) != 4) {
      System.Int32 RNTRNTRNT_61 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(169);
      return RNTRNTRNT_61;
    }
    if (b.Test(3L, 3.14f) != 3) {
      System.Int32 RNTRNTRNT_62 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(170);
      return RNTRNTRNT_62;
    }
    if (b.Test(3L, 5) != 3) {
      System.Int32 RNTRNTRNT_63 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(171);
      return RNTRNTRNT_63;
    }
    Foo<string, float> c = new Foo<string, float>();
    if (c.Test("Hello", 3.14f) != 3) {
      System.Int32 RNTRNTRNT_64 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(172);
      return RNTRNTRNT_64;
    }
    Foo<int, string> d = new Foo<int, string>();
    if (d.Test(3, "Hello") != 2) {
      System.Int32 RNTRNTRNT_65 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(173);
      return RNTRNTRNT_65;
    }
    System.Int32 RNTRNTRNT_66 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(174);
    return RNTRNTRNT_66;
  }
}
