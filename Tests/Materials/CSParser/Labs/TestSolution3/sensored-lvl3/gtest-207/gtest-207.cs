class M
{
  static void p(string x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(44);
    System.Console.WriteLine(x);
    IACSharpSensor.IACSharpSensor.SensorReached(45);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(46);
    string[] arr = new string[] {
      "a",
      "b",
      "c"
    };
    System.Array.ForEach(arr, p);
    IACSharpSensor.IACSharpSensor.SensorReached(47);
  }
}
