using System;
using System.Reflection;
public class TestAttribute : Attribute
{
  public Type type;
  public TestAttribute(Type type)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(415);
    this.type = type;
    IACSharpSensor.IACSharpSensor.SensorReached(416);
  }
}
class C<T>
{
  [Test(typeof(C<string>))]
  public static void Foo()
  {
  }
}
public class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(417);
    MethodInfo mi = typeof(C<>).GetMethod("Foo");
    object[] a = mi.GetCustomAttributes(false);
    if (((TestAttribute)a[0]).type.ToString() != "C`1[System.String]") {
      System.Int32 RNTRNTRNT_177 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(418);
      return RNTRNTRNT_177;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_178 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(419);
    return RNTRNTRNT_178;
  }
}
