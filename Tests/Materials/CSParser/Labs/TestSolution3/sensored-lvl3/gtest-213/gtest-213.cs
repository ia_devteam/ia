public interface SomeInterface
{
  int Foo { get; set; }
}
public struct SomeStruct : SomeInterface
{
  int x;
  public int Foo {
    get {
      System.Int32 RNTRNTRNT_15 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(65);
      return RNTRNTRNT_15;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(66);
      x = value;
      IACSharpSensor.IACSharpSensor.SensorReached(67);
    }
  }
}
public class Test
{
  public static void Fun<T>(T t) where T : SomeInterface
  {
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    if (++t.Foo != 1) {
      throw new System.Exception("not 1");
    }
    if (t.Foo != 1) {
      throw new System.Exception("didn't update 't'");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(69);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(70);
    Fun(new SomeStruct());
    IACSharpSensor.IACSharpSensor.SensorReached(71);
  }
}
