using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
public class MyClass<TItem>
{
  public MyClass()
  {
  }
  public event ListChangedEventHandler ListChanged;
  public void AddListChangedEventHandler(ListChangedEventHandler handler)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(129);
    ListChanged += handler;
    IACSharpSensor.IACSharpSensor.SensorReached(130);
  }
  protected void OnListChanged(ListChangedEventArgs e)
  {
  }
}
