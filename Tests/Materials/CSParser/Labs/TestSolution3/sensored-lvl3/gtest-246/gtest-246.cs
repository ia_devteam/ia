using System;
namespace TestCase
{
  interface ITest
  {
  }
  class CTest : ITest
  {
    static void Main()
    {
    }
    public void Bar()
    {
    }
  }
  class CGenericTest<T, V> where T : ITest where V : CTest, T, new()
  {
    public V Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(196);
      V TestObject = new V();
      TestObject.Bar();
      V RNTRNTRNT_79 = TestObject;
      IACSharpSensor.IACSharpSensor.SensorReached(197);
      return RNTRNTRNT_79;
    }
  }
}
