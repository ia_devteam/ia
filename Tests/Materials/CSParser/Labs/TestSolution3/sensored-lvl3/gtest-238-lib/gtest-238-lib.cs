using System;
public class Foo<T>
{
  public int Test(int index)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(155);
    Console.WriteLine("Test 1: {0}", index);
    System.Int32 RNTRNTRNT_50 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(156);
    return RNTRNTRNT_50;
  }
  public int Test(T index)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(157);
    Console.WriteLine("Test 2: {0}", index);
    System.Int32 RNTRNTRNT_51 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(158);
    return RNTRNTRNT_51;
  }
}
