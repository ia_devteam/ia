using System;
using System.Collections.Generic;
public delegate R Fun<A1, R>(A1 x);
class MyTest
{
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(79);
    foreach (Object d in Map<int, int, String, Object>(delegate(int x) { return x.ToString(); }, FromTo(10, 20))) {
      Console.WriteLine(d);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(80);
  }
  public static IEnumerable<Rr> Map<Aa, Af, Rf, Rr>(Fun<Af, Rf> f, IEnumerable<Aa> xs) where Aa : Af where Rf : Rr
  {
    IACSharpSensor.IACSharpSensor.SensorReached(81);
    foreach (Aa x in xs) {
      Rr RNTRNTRNT_18 = f(x);
      IACSharpSensor.IACSharpSensor.SensorReached(82);
      yield return RNTRNTRNT_18;
      IACSharpSensor.IACSharpSensor.SensorReached(83);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(84);
  }
  public static IEnumerable<int> FromTo(int @from, int to)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(85);
    for (int i = @from; i <= to; i++) {
      System.Int32 RNTRNTRNT_19 = i;
      IACSharpSensor.IACSharpSensor.SensorReached(86);
      yield return RNTRNTRNT_19;
      IACSharpSensor.IACSharpSensor.SensorReached(87);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(88);
  }
}
