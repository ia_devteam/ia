using System;
public delegate void Handler<T>(T t);
public static class X
{
  public static void Foo<T>(Handler<T> handler)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(317);
    AsyncCallback d = delegate(IAsyncResult ar) { Response<T>(handler); };
    IACSharpSensor.IACSharpSensor.SensorReached(318);
  }
  static void Response<T>(Handler<T> handler)
  {
  }
  static void Test<T>(T t)
  {
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(319);
    Foo<long>(Test);
    IACSharpSensor.IACSharpSensor.SensorReached(320);
  }
}
