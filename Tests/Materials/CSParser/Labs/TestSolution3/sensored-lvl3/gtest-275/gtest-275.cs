using System;
using System.Collections;
using System.Collections.Generic;
public class Test
{
  public class C
  {
    public C()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(332);
      Type t = typeof(Dictionary<, >);
      IACSharpSensor.IACSharpSensor.SensorReached(333);
    }
  }
  public class D<T, U>
  {
    public D()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(334);
      Type t = typeof(Dictionary<, >);
      IACSharpSensor.IACSharpSensor.SensorReached(335);
    }
  }
  public class E<T>
  {
    public E()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(336);
      Type t = typeof(Dictionary<, >);
      IACSharpSensor.IACSharpSensor.SensorReached(337);
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(338);
    new C();
    new D<string, string>();
    new E<string>();
    IACSharpSensor.IACSharpSensor.SensorReached(339);
  }
}
