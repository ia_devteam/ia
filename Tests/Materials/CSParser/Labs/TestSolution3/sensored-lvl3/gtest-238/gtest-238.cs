class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(151);
    Foo<long> foo = new Foo<long>();
    if (foo.Test(3) != 1) {
      System.Int32 RNTRNTRNT_47 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(152);
      return RNTRNTRNT_47;
    }
    if (foo.Test(5L) != 2) {
      System.Int32 RNTRNTRNT_48 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(153);
      return RNTRNTRNT_48;
    }
    System.Int32 RNTRNTRNT_49 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(154);
    return RNTRNTRNT_49;
  }
}
