using System;
using System.Reflection;
using System.Runtime.CompilerServices;
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(72);
    object[] attrs = typeof(X).Assembly.GetCustomAttributes(true);
    foreach (object o in attrs) {
      if (o is RuntimeCompatibilityAttribute) {
        RuntimeCompatibilityAttribute a = (RuntimeCompatibilityAttribute)o;
        if (a.WrapNonExceptionThrows) {
          System.Int32 RNTRNTRNT_16 = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(73);
          return RNTRNTRNT_16;
        }
      }
    }
    System.Int32 RNTRNTRNT_17 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(74);
    return RNTRNTRNT_17;
  }
}
