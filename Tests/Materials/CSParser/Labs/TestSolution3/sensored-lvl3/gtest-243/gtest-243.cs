using System;
using System.Reflection;
using System.Collections.Generic;
public class Foo<T>
{
  public void Test(T t)
  {
  }
}
public class Tests
{
  public static void foo<T>()
  {
  }
  public static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(179);
    MethodInfo mi = typeof(Tests).GetMethod("foo");
    if (!mi.IsGenericMethod) {
      System.Int32 RNTRNTRNT_69 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(180);
      return RNTRNTRNT_69;
    }
    if (!mi.IsGenericMethodDefinition) {
      System.Int32 RNTRNTRNT_70 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(181);
      return RNTRNTRNT_70;
    }
    MethodInfo mi2 = mi.MakeGenericMethod(new Type[] { typeof(int) });
    if (!mi2.IsGenericMethod) {
      System.Int32 RNTRNTRNT_71 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(182);
      return RNTRNTRNT_71;
    }
    if (mi2.IsGenericMethodDefinition) {
      System.Int32 RNTRNTRNT_72 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(183);
      return RNTRNTRNT_72;
    }
    MethodInfo mi3 = typeof(Foo<int>).GetMethod("Test");
    if (mi3.IsGenericMethod) {
      System.Int32 RNTRNTRNT_73 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(184);
      return RNTRNTRNT_73;
    }
    if (mi3.IsGenericMethodDefinition) {
      System.Int32 RNTRNTRNT_74 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(185);
      return RNTRNTRNT_74;
    }
    System.Int32 RNTRNTRNT_75 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(186);
    return RNTRNTRNT_75;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(187);
    int result = Test();
    System.Int32 RNTRNTRNT_76 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(188);
    return RNTRNTRNT_76;
  }
}
