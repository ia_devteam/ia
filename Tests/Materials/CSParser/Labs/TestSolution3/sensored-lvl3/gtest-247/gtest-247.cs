using System;
using System.Diagnostics;
using SCG = System.Collections.Generic;
public abstract class EnumerableBase<T> : SCG.IEnumerable<T>
{
  public abstract SCG.IEnumerator<T> GetEnumerator();
  System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
  {
    System.Collections.IEnumerator RNTRNTRNT_80 = GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(198);
    return RNTRNTRNT_80;
  }
}
public abstract class CollectionValueBase<T> : EnumerableBase<T>
{
  protected virtual void raiseItemsAdded(T item, int count)
  {
  }
  protected class RaiseForRemoveAllHandler
  {
    CircularQueue<T> wasRemoved;
  }
  public override abstract SCG.IEnumerator<T> GetEnumerator();
}
public class CircularQueue<T> : EnumerableBase<T>
{
  public override SCG.IEnumerator<T> GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(199);
    yield break;
  }
  public virtual void Enqueue(T item)
  {
  }
}
public class HashSet<T> : CollectionValueBase<T>
{
  private bool searchoradd(ref T item, bool @add, bool update, bool raise)
  {
    System.Boolean RNTRNTRNT_81 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(200);
    return RNTRNTRNT_81;
  }
  public virtual void RemoveAll<U>(SCG.IEnumerable<U> items) where U : T
  {
    IACSharpSensor.IACSharpSensor.SensorReached(201);
    RaiseForRemoveAllHandler raiseHandler = new RaiseForRemoveAllHandler();
    IACSharpSensor.IACSharpSensor.SensorReached(202);
  }
  public virtual void AddAll<U>(SCG.IEnumerable<U> items) where U : T
  {
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    CircularQueue<T> wasAdded = new CircularQueue<T>();
    foreach (T item in wasAdded) {
      raiseItemsAdded(item, 1);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(204);
  }
  public override SCG.IEnumerator<T> GetEnumerator()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(205);
    yield break;
  }
}
class X
{
  static void Main()
  {
  }
}
