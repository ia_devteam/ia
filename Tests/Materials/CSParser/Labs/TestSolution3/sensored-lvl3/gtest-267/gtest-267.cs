using System;
public delegate void Handler<T>(T t);
public class T
{
  public void Foo<T>(Handler<T> handler)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(264);
    AsyncCallback d = delegate(IAsyncResult ar) { Response<T>(handler); };
    IACSharpSensor.IACSharpSensor.SensorReached(265);
  }
  void Response<T>(Handler<T> handler)
  {
  }
  static void Main()
  {
  }
}
