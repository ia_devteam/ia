public class SomeClass
{
}
public class Foo<T> where T : class
{
  public T Do(object o)
  {
    T RNTRNTRNT_12 = o as T;
    IACSharpSensor.IACSharpSensor.SensorReached(48);
    return RNTRNTRNT_12;
  }
}
class Driver
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(49);
    Foo<SomeClass> f = new Foo<SomeClass>();
    f.Do("something");
    IACSharpSensor.IACSharpSensor.SensorReached(50);
  }
}
