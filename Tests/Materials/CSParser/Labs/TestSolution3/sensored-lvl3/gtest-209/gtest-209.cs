using System;
using System.Collections.Generic;
using System.Text;
namespace ClassLibrary3
{
  public class Dictionary1<TKey, TValue> : Dictionary<TKey, TValue>
  {
  }
  public class Test
  {
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(51);
      Dictionary1<Guid, String> _D = new Dictionary1<Guid, string>();
      _D.Add(Guid.NewGuid(), "foo");
      IACSharpSensor.IACSharpSensor.SensorReached(52);
    }
  }
}
