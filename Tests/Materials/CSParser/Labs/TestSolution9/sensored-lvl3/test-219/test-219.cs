using System;
public class TestAttribute : Attribute
{
  Type type;
  public TestAttribute(Type type)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(116);
    this.type = type;
    IACSharpSensor.IACSharpSensor.SensorReached(117);
  }
  public Type Type {
    get {
      Type RNTRNTRNT_51 = type;
      IACSharpSensor.IACSharpSensor.SensorReached(118);
      return RNTRNTRNT_51;
    }
  }
}
[TestAttribute(typeof(void))]
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(119);
    object[] attrs = typeof(Test).GetCustomAttributes(typeof(TestAttribute), false);
    foreach (TestAttribute attr in attrs) {
      Console.WriteLine("TestAttribute({0})", attr.Type);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(120);
  }
}
