delegate void FooHandler();
class X
{
  public static void foo()
  {
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    object o = new FooHandler(foo);
    ((FooHandler)o)();
    IACSharpSensor.IACSharpSensor.SensorReached(69);
  }
}
