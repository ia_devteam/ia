namespace Foo
{
  public class Hello
  {
    public static int World = 8;
  }
}
namespace Bar
{
  public class Hello
  {
    public static int World = 9;
  }
}
namespace Test
{
  using Foo;
  public class Test1
  {
    public static int World()
    {
      System.Int32 RNTRNTRNT_293 = Hello.World;
      IACSharpSensor.IACSharpSensor.SensorReached(533);
      return RNTRNTRNT_293;
    }
  }
}
namespace Test
{
  using Bar;
  public class Test2
  {
    public static int World()
    {
      System.Int32 RNTRNTRNT_294 = Hello.World;
      IACSharpSensor.IACSharpSensor.SensorReached(534);
      return RNTRNTRNT_294;
    }
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(535);
    if (Test.Test1.World() != 8) {
      System.Int32 RNTRNTRNT_295 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(536);
      return RNTRNTRNT_295;
    }
    if (Test.Test2.World() != 9) {
      System.Int32 RNTRNTRNT_296 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(537);
      return RNTRNTRNT_296;
    }
    System.Int32 RNTRNTRNT_297 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(538);
    return RNTRNTRNT_297;
  }
}
