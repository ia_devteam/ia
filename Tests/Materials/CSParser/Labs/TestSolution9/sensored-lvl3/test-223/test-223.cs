enum Foo
{
  Bar
}
class T
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(172);
    System.Enum e = Foo.Bar;
    System.ValueType vt1 = Foo.Bar, vt2 = 1;
    if (((Foo)e) != Foo.Bar) {
      System.Int32 RNTRNTRNT_86 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(173);
      return RNTRNTRNT_86;
    }
    if (((Foo)vt1) != Foo.Bar) {
      System.Int32 RNTRNTRNT_87 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(174);
      return RNTRNTRNT_87;
    }
    if (((int)vt2) != 1) {
      System.Int32 RNTRNTRNT_88 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(175);
      return RNTRNTRNT_88;
    }
    System.ValueType vt = null;
    System.Int32 RNTRNTRNT_89 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(176);
    return RNTRNTRNT_89;
  }
}
