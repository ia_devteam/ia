using System;
interface I
{
  int[] this[params int[] ii] { get; }
}
class X : I
{
  public int this[int i] {
    get {
      System.Int32 RNTRNTRNT_21 = i;
      IACSharpSensor.IACSharpSensor.SensorReached(33);
      return RNTRNTRNT_21;
    }
  }
  public int[] this[params int[] ii] {
    get {
      System.Int32[] RNTRNTRNT_22 = new int[] {
        this[1],
        this[2],
        this[ii.Length]
      };
      IACSharpSensor.IACSharpSensor.SensorReached(34);
      return RNTRNTRNT_22;
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(35);
    X x = new X();
    Console.WriteLine(x[1]);
    int[] r = x[2, 2, 1, 2, 0];
    for (int i = 0; i < r.Length; i++) {
      Console.Write(r[i] + " ");
    }
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(36);
  }
}
