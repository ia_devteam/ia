using System;
namespace Test
{
  public interface IBook
  {
    string GetItem(int i);
    string this[int i] { get; }
  }
  public interface IMovie
  {
    string GetItem(int i);
    string this[int i] { get; }
  }
  public class BookAboutMovie : IBook, IMovie
  {
    private string title = "";
    public BookAboutMovie(string title)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(570);
      this.title = title;
      IACSharpSensor.IACSharpSensor.SensorReached(571);
    }
    public string GetItem(int i)
    {
      System.String RNTRNTRNT_317 = title;
      IACSharpSensor.IACSharpSensor.SensorReached(572);
      return RNTRNTRNT_317;
    }
    public string this[int i] {
      get {
        System.String RNTRNTRNT_318 = title;
        IACSharpSensor.IACSharpSensor.SensorReached(573);
        return RNTRNTRNT_318;
      }
    }
    public static int Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(574);
      BookAboutMovie jurassicPark = new BookAboutMovie("Jurassic Park");
      Console.WriteLine("Book Title : " + jurassicPark.GetItem(2));
      Console.WriteLine("Book Title : " + ((IBook)jurassicPark)[2]);
      System.Int32 RNTRNTRNT_319 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(575);
      return RNTRNTRNT_319;
    }
  }
}
