using System;
[Obsolete()]
class ObsoleteClass
{
}
public class Test
{
  private string _name;
  [Obsolete()]
  public Test() : this("layout", false)
  {
  }
  [Obsolete()]
  public Test(string a, bool writeToErrorStream)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(586);
    Name = a;
    IACSharpSensor.IACSharpSensor.SensorReached(587);
  }
  [Obsolete()]
  public string Name {
    get {
      System.String RNTRNTRNT_323 = _name;
      IACSharpSensor.IACSharpSensor.SensorReached(588);
      return RNTRNTRNT_323;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(589);
      _name = value;
      IACSharpSensor.IACSharpSensor.SensorReached(590);
    }
  }
}
[Obsolete()]
public class DerivedTest : Test
{
  ObsoleteClass member;
  [Obsolete()]
  public DerivedTest(string a) : base(a, false)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(591);
    Name = a;
    IACSharpSensor.IACSharpSensor.SensorReached(592);
  }
  public string Method()
  {
    System.String RNTRNTRNT_324 = base.Name;
    IACSharpSensor.IACSharpSensor.SensorReached(593);
    return RNTRNTRNT_324;
  }
  [Obsolete()]
  public void T2()
  {
  }
  public static void Main()
  {
  }
}
[Obsolete()]
class ObsoleteClass2 : ObsoleteClass
{
}
class ObsoleteClass3
{
  public static readonly double XSmall = 0.6444444444444;
  [Obsolete("E1")]
  public readonly double X_Small = XSmall;
  [Obsolete("E2")]
  public static readonly double X_Small2 = XSmall;
}
class ObsoleteClass4
{
  [Obsolete()]
  public void T()
  {
    lock (typeof(ObsoleteClass4)) {
    }
    lock (typeof(ObsoleteClass2)) {
    }
  }
}
