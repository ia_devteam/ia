class T
{
  static int ret_code = 0;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(229);
    try {
      T t = null;
      t.Foo();
    } catch {
      System.Int32 RNTRNTRNT_111 = ret_code;
      IACSharpSensor.IACSharpSensor.SensorReached(230);
      return RNTRNTRNT_111;
    }
    ret_code = 1;
    System.Int32 RNTRNTRNT_112 = ret_code;
    IACSharpSensor.IACSharpSensor.SensorReached(231);
    return RNTRNTRNT_112;
  }
  void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(232);
    if (this == null) {
      System.Console.WriteLine("This isnt anything!?!?");
      ret_code = 1;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(233);
  }
}
