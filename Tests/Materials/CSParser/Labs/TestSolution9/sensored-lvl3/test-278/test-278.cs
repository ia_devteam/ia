using System;
struct Rect
{
  int x;
  public int X {
    get {
      System.Int32 RNTRNTRNT_287 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(522);
      return RNTRNTRNT_287;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(523);
      x = value;
      IACSharpSensor.IACSharpSensor.SensorReached(524);
    }
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(525);
    Rect rect = new Rect();
    rect.X += 20;
    Console.WriteLine("Should be 20: " + rect.X);
    System.Int32 RNTRNTRNT_288 = rect.X == 20 ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(526);
    return RNTRNTRNT_288;
  }
}
