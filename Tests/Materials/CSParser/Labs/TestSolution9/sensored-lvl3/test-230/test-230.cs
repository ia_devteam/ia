using System;
using System.Reflection;
using System.Diagnostics;
[module: DebuggableAttribute(false, false)]
class TestClass
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(226);
    Module[] moduleArray;
    moduleArray = Assembly.GetExecutingAssembly().GetModules(false);
    Module myModule = moduleArray[0];
    object[] attributes;
    attributes = myModule.GetCustomAttributes(typeof(DebuggableAttribute), false);
    if (attributes[0] != null) {
      Console.WriteLine("Succeeded");
      System.Int32 RNTRNTRNT_109 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(227);
      return RNTRNTRNT_109;
    }
    System.Int32 RNTRNTRNT_110 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(228);
    return RNTRNTRNT_110;
  }
}
