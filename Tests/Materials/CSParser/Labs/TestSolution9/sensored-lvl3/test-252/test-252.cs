delegate void Foo();
class A
{
  public event Foo Bar;
  public static void m1()
  {
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(420);
    A a = new A();
    a.Bar += new Foo(m1);
    a.Bar -= new Foo(m1);
    System.Int32 RNTRNTRNT_233 = (a.Bar == null) ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(421);
    return RNTRNTRNT_233;
  }
}
