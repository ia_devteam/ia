using System;
using System.Reflection;
static class StaticClass
{
  const int Foo = 1;
  delegate object D();
  enum E
  {
  }
  public static string Name()
  {
    System.String RNTRNTRNT_313 = "OK";
    IACSharpSensor.IACSharpSensor.SensorReached(565);
    return RNTRNTRNT_313;
  }
}
public class MainClass
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(566);
    Type type = typeof(StaticClass);
    if (!type.IsAbstract || !type.IsSealed) {
      Console.WriteLine("Is not abstract sealed");
      System.Int32 RNTRNTRNT_314 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(567);
      return RNTRNTRNT_314;
    }
    if (type.GetConstructors().Length > 0) {
      Console.WriteLine("Has constructor");
      System.Int32 RNTRNTRNT_315 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(568);
      return RNTRNTRNT_315;
    }
    Console.WriteLine(StaticClass.Name());
    System.Int32 RNTRNTRNT_316 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(569);
    return RNTRNTRNT_316;
  }
}
