namespace n1
{
  class Attribute
  {
  }
}
namespace n3
{
  using n1;
  using System;
  class A
  {
    void Attribute()
    {
    }
    void X()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(455);
      Attribute();
      IACSharpSensor.IACSharpSensor.SensorReached(456);
    }
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(457);
      new A().X();
      IACSharpSensor.IACSharpSensor.SensorReached(458);
    }
  }
}
