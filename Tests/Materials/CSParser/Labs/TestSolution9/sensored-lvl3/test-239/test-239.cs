using System;
using System.Diagnostics;
class BaseClass
{
  [Conditional("AAXXAA")]
  public virtual void ConditionalMethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(372);
    Environment.Exit(1);
    IACSharpSensor.IACSharpSensor.SensorReached(373);
  }
}
class TestClass : BaseClass
{
  public override void ConditionalMethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(374);
    base.ConditionalMethod();
    IACSharpSensor.IACSharpSensor.SensorReached(375);
  }
}
class MainClass
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(376);
    TestClass ts = new TestClass();
    ts.ConditionalMethod();
    Console.WriteLine("Succeeded");
    System.Int32 RNTRNTRNT_210 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(377);
    return RNTRNTRNT_210;
  }
}
