using System;
class EntryPoint
{
  delegate void EventHandler(object sender);
  static event EventHandler FooEvent;
  static void bar_f(object sender)
  {
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(578);
    if (FooEvent != null) {
      FooEvent(null);
    }
    object bar = new EventHandler(bar_f);
    IACSharpSensor.IACSharpSensor.SensorReached(579);
  }
}
