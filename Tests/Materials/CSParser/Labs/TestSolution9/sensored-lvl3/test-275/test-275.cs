using System;
using System.Reflection;
using System.Runtime.CompilerServices;
public delegate void DelType();
struct S
{
  public event DelType MyEvent;
}
public class Test
{
  public event DelType MyEvent;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(513);
    EventInfo ei = typeof(Test).GetEvent("MyEvent");
    MethodImplAttributes methodImplAttributes = ei.GetAddMethod().GetMethodImplementationFlags();
    if ((methodImplAttributes & MethodImplAttributes.Synchronized) == 0) {
      Console.WriteLine("FAILED");
      System.Int32 RNTRNTRNT_279 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(514);
      return RNTRNTRNT_279;
    }
    methodImplAttributes = ei.GetRemoveMethod().GetMethodImplementationFlags();
    if ((methodImplAttributes & MethodImplAttributes.Synchronized) == 0) {
      Console.WriteLine("FAILED");
      System.Int32 RNTRNTRNT_280 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(515);
      return RNTRNTRNT_280;
    }
    ei = typeof(S).GetEvent("MyEvent");
    methodImplAttributes = ei.GetAddMethod().GetMethodImplementationFlags();
    if ((methodImplAttributes & MethodImplAttributes.Synchronized) != 0) {
      Console.WriteLine("FAILED");
      System.Int32 RNTRNTRNT_281 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(516);
      return RNTRNTRNT_281;
    }
    methodImplAttributes = ei.GetRemoveMethod().GetMethodImplementationFlags();
    if ((methodImplAttributes & MethodImplAttributes.Synchronized) != 0) {
      Console.WriteLine("FAILED");
      System.Int32 RNTRNTRNT_282 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(517);
      return RNTRNTRNT_282;
    }
    System.Int32 RNTRNTRNT_283 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(518);
    return RNTRNTRNT_283;
  }
}
