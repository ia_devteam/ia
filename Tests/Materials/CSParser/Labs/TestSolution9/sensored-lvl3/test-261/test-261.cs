using System;
class T
{
  T Me {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(443);
      calls++;
      T RNTRNTRNT_239 = this;
      IACSharpSensor.IACSharpSensor.SensorReached(444);
      return RNTRNTRNT_239;
    }
  }
  T GetMe()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(445);
    foo++;
    T RNTRNTRNT_240 = this;
    IACSharpSensor.IACSharpSensor.SensorReached(446);
    return RNTRNTRNT_240;
  }
  int blah = 0, calls = 0, foo = 0, bar = 0;
  static int Test(T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(447);
    t.Me.Me.blah++;
    t.GetMe().GetMe().bar++;
    if (t.blah != 1) {
      System.Int32 RNTRNTRNT_241 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(448);
      return RNTRNTRNT_241;
    }
    if (t.bar != 1) {
      System.Int32 RNTRNTRNT_242 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(449);
      return RNTRNTRNT_242;
    }
    if (t.calls != 2) {
      System.Int32 RNTRNTRNT_243 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(450);
      return RNTRNTRNT_243;
    }
    if (t.foo != 2) {
      System.Int32 RNTRNTRNT_244 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(451);
      return RNTRNTRNT_244;
    }
    System.Int32 RNTRNTRNT_245 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(452);
    return RNTRNTRNT_245;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(453);
    T t = new T();
    int result = Test(t);
    Console.WriteLine("RESULT: {0}", result);
    System.Int32 RNTRNTRNT_246 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(454);
    return RNTRNTRNT_246;
  }
}
