using System;
class X
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
    int x = 7;
    int y = 2;
    x = (y += 3) + 10;
    if (y != 5) {
      System.Int32 RNTRNTRNT_1 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      return RNTRNTRNT_1;
    }
    if (x != 15) {
      System.Int32 RNTRNTRNT_2 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(3);
      return RNTRNTRNT_2;
    }
    x += 9;
    if (x != 24) {
      System.Int32 RNTRNTRNT_3 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(4);
      return RNTRNTRNT_3;
    }
    byte c = 3;
    byte d = 5;
    x = d ^= c;
    Console.WriteLine(x);
    short s = 5;
    int i = 30000001;
    s <<= i;
    Console.WriteLine(s);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_4 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(5);
    return RNTRNTRNT_4;
  }
}
