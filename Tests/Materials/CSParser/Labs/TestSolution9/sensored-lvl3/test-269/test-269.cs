using System;
struct Result
{
  public int res;
  double duh;
  long bah;
  public Result(int val)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(471);
    res = val;
    bah = val;
    duh = val;
    IACSharpSensor.IACSharpSensor.SensorReached(472);
  }
}
class Class1
{
  static int AddABunchOfInts(__arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(473);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    System.Int32 RNTRNTRNT_253 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(474);
    return RNTRNTRNT_253;
  }
  static int AddASecondBunchOfInts(int a, __arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(475);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    System.Int32 RNTRNTRNT_254 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(476);
    return RNTRNTRNT_254;
  }
  static Result VtAddABunchOfInts(__arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(477);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    Result RNTRNTRNT_255 = new Result(result);
    IACSharpSensor.IACSharpSensor.SensorReached(478);
    return RNTRNTRNT_255;
  }
  static Result VtAddASecondBunchOfInts(int a, __arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(479);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    Result RNTRNTRNT_256 = new Result(result);
    IACSharpSensor.IACSharpSensor.SensorReached(480);
    return RNTRNTRNT_256;
  }
  int InstAddABunchOfInts(__arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(481);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    System.Int32 RNTRNTRNT_257 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(482);
    return RNTRNTRNT_257;
  }
  int InstAddASecondBunchOfInts(int a, __arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(483);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    System.Int32 RNTRNTRNT_258 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(484);
    return RNTRNTRNT_258;
  }
  Result InstVtAddABunchOfInts(__arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(485);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    Result RNTRNTRNT_259 = new Result(result);
    IACSharpSensor.IACSharpSensor.SensorReached(486);
    return RNTRNTRNT_259;
  }
  Result InstVtAddASecondBunchOfInts(int a, __arglist __arglist)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(487);
    int result = 0;
    System.ArgIterator iter = new System.ArgIterator(__arglist);
    int argCount = iter.GetRemainingCount();
    for (int i = 0; i < argCount; i++) {
      System.TypedReference typedRef = iter.GetNextArg();
      result += (int)TypedReference.ToObject(typedRef);
    }
    Result RNTRNTRNT_260 = new Result(result);
    IACSharpSensor.IACSharpSensor.SensorReached(488);
    return RNTRNTRNT_260;
  }
  static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(489);
    int result = AddABunchOfInts(__arglist(2, 3, 4));
    Console.WriteLine("Answer: {0}", result);
    if (result != 9) {
      System.Int32 RNTRNTRNT_261 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(490);
      return RNTRNTRNT_261;
    }
    result = AddASecondBunchOfInts(16, __arglist(2, 3, 4));
    Console.WriteLine("Answer: {0}", result);
    if (result != 9) {
      System.Int32 RNTRNTRNT_262 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(491);
      return RNTRNTRNT_262;
    }
    Class1 s = new Class1();
    result = s.InstAddABunchOfInts(__arglist(2, 3, 4, 5));
    Console.WriteLine("Answer: {0}", result);
    if (result != 14) {
      System.Int32 RNTRNTRNT_263 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(492);
      return RNTRNTRNT_263;
    }
    result = s.InstAddASecondBunchOfInts(16, __arglist(2, 3, 4, 5, 6));
    Console.WriteLine("Answer: {0}", result);
    if (result != 20) {
      System.Int32 RNTRNTRNT_264 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(493);
      return RNTRNTRNT_264;
    }
    result = s.InstVtAddABunchOfInts(__arglist(2, 3, 4, 5)).res;
    Console.WriteLine("Answer: {0}", result);
    if (result != 14) {
      System.Int32 RNTRNTRNT_265 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(494);
      return RNTRNTRNT_265;
    }
    result = s.InstVtAddASecondBunchOfInts(16, __arglist(2, 3, 4, 5, 6)).res;
    Console.WriteLine("Answer: {0}", result);
    if (result != 20) {
      System.Int32 RNTRNTRNT_266 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(495);
      return RNTRNTRNT_266;
    }
    result = VtAddABunchOfInts(__arglist(2, 3, 4, 5, 1)).res;
    Console.WriteLine("Answer: {0}", result);
    if (result != 15) {
      System.Int32 RNTRNTRNT_267 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(496);
      return RNTRNTRNT_267;
    }
    result = VtAddASecondBunchOfInts(16, __arglist(2, 3, 4, 5, 6, 1)).res;
    Console.WriteLine("Answer: {0}", result);
    if (result != 21) {
      System.Int32 RNTRNTRNT_268 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(497);
      return RNTRNTRNT_268;
    }
    result = s.InstAddABunchOfInts(__arglist());
    if (result != 0) {
      System.Int32 RNTRNTRNT_269 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(498);
      return RNTRNTRNT_269;
    }
    System.Int32 RNTRNTRNT_270 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(499);
    return RNTRNTRNT_270;
  }
}
