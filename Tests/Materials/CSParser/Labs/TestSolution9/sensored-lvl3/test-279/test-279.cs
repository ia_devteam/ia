using System;
class FlagsAttributeDemo
{
  [FlagsAttribute()]
  enum MultiHue : short
  {
    Black = 0,
    Red = 1,
    Green = 2,
    Blue = 4
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(527);
    string s = ((MultiHue)7).ToString();
    Console.WriteLine(s);
    if (s != "Red, Green, Blue") {
      System.Int32 RNTRNTRNT_289 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(528);
      return RNTRNTRNT_289;
    }
    System.Int32 RNTRNTRNT_290 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(529);
    return RNTRNTRNT_290;
  }
}
