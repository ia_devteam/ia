class A
{
  public virtual int Blah {
    get {
      System.Int32 RNTRNTRNT_61 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(143);
      return RNTRNTRNT_61;
    }
    set { }
  }
}
class B : A
{
  public override int Blah {
    get {
      System.Int32 RNTRNTRNT_62 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(144);
      return RNTRNTRNT_62;
    }
  }
  public static bool Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(145);
    B b = new B();
    if (b.Blah != 2) {
      System.Boolean RNTRNTRNT_63 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(146);
      return RNTRNTRNT_63;
    }
    if (b.Blah++ != 2) {
      System.Boolean RNTRNTRNT_64 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(147);
      return RNTRNTRNT_64;
    }
    b.Blah = 0;
    System.Boolean RNTRNTRNT_65 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(148);
    return RNTRNTRNT_65;
  }
}
abstract class C
{
  public abstract int Blah { get; set; }
}
class D : C
{
  public override int Blah {
    get {
      System.Int32 RNTRNTRNT_66 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(149);
      return RNTRNTRNT_66;
    }
    set { }
  }
}
class E : D
{
  public override int Blah {
    get {
      System.Int32 RNTRNTRNT_67 = base.Blah;
      IACSharpSensor.IACSharpSensor.SensorReached(150);
      return RNTRNTRNT_67;
    }
  }
  public static bool Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(151);
    E e = new E();
    if (e.Blah != 2) {
      System.Boolean RNTRNTRNT_68 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(152);
      return RNTRNTRNT_68;
    }
    if (e.Blah++ != 2) {
      System.Boolean RNTRNTRNT_69 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(153);
      return RNTRNTRNT_69;
    }
    e.Blah = 2;
    System.Boolean RNTRNTRNT_70 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(154);
    return RNTRNTRNT_70;
  }
}
interface IBlah
{
  int this[int i] { get; set; }
  int Blah { get; set; }
}
class F : IBlah
{
  int IBlah.this[int i] {
    get {
      System.Int32 RNTRNTRNT_71 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(155);
      return RNTRNTRNT_71;
    }
    set { }
  }
  int IBlah.Blah {
    get {
      System.Int32 RNTRNTRNT_72 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(156);
      return RNTRNTRNT_72;
    }
    set { }
  }
  public int this[int i] {
    get {
      System.Int32 RNTRNTRNT_73 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(157);
      return RNTRNTRNT_73;
    }
    set { }
  }
  public int Blah {
    get {
      System.Int32 RNTRNTRNT_74 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(158);
      return RNTRNTRNT_74;
    }
    set { }
  }
  public static bool Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(159);
    F f = new F();
    if (f.Blah != 2) {
      System.Boolean RNTRNTRNT_75 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(160);
      return RNTRNTRNT_75;
    }
    if (f.Blah++ != 2) {
      System.Boolean RNTRNTRNT_76 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(161);
      return RNTRNTRNT_76;
    }
    f.Blah = 2;
    if (f[1] != 2) {
      System.Boolean RNTRNTRNT_77 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(162);
      return RNTRNTRNT_77;
    }
    if (f[1]++ != 2) {
      System.Boolean RNTRNTRNT_78 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(163);
      return RNTRNTRNT_78;
    }
    f[1] = 2;
    System.Boolean RNTRNTRNT_79 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(164);
    return RNTRNTRNT_79;
  }
}
class Driver
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(165);
    if (!B.Test()) {
      System.Int32 RNTRNTRNT_80 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(166);
      return RNTRNTRNT_80;
    }
    if (!E.Test()) {
      System.Int32 RNTRNTRNT_81 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(167);
      return RNTRNTRNT_81;
    }
    if (!F.Test()) {
      System.Int32 RNTRNTRNT_82 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(168);
      return RNTRNTRNT_82;
    }
    System.Int32 RNTRNTRNT_83 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(169);
    return RNTRNTRNT_83;
  }
}
