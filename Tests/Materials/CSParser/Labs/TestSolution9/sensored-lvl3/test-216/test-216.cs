using System;
public delegate void OnWhateverDelegate(string s);
class cls
{
  public event OnWhateverDelegate OnWhatever;
  class nestedcls
  {
    internal void CallParentDel(cls c, string s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(98);
      c.OnWhatever(s);
      IACSharpSensor.IACSharpSensor.SensorReached(99);
    }
  }
  internal void CallMyDel(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(100);
    (new nestedcls()).CallParentDel(this, s);
    IACSharpSensor.IACSharpSensor.SensorReached(101);
  }
}
class MonoEmbed
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(102);
    cls c = new cls();
    c.OnWhatever += new OnWhateverDelegate(Whatever);
    c.CallMyDel("test");
    IACSharpSensor.IACSharpSensor.SensorReached(103);
  }
  static void Whatever(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(104);
    Console.WriteLine(s);
    IACSharpSensor.IACSharpSensor.SensorReached(105);
  }
}
