using System;
class A
{
  public int foo = 1;
}
class B : A
{
  public new int foo()
  {
    System.Int32 RNTRNTRNT_93 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(188);
    return RNTRNTRNT_93;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(189);
    B b = new B();
    Console.WriteLine(b.foo());
    IACSharpSensor.IACSharpSensor.SensorReached(190);
  }
}
