using System;
using System.Collections;
namespace Tests
{
  public interface IIndexer
  {
    object this[int index] { get; set; }
  }
  public class Test : IIndexer
  {
    object[] InnerList;
    object IIndexer.this[int index] {
      get {
        System.Object RNTRNTRNT_208 = InnerList[index];
        IACSharpSensor.IACSharpSensor.SensorReached(359);
        return RNTRNTRNT_208;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(360);
        InnerList[index] = value;
        IACSharpSensor.IACSharpSensor.SensorReached(361);
      }
    }
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(362);
      if (Attribute.GetCustomAttribute(typeof(Test), typeof(System.Reflection.DefaultMemberAttribute)) != null) {
        throw new Exception("Class 'Test' has a DefaultMemberAttribute");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(363);
    }
  }
}
