using System;
public struct integer
{
  private readonly int value;
  public integer(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(539);
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(540);
  }
  public static implicit operator integer(int i)
  {
    integer RNTRNTRNT_298 = new integer(i);
    IACSharpSensor.IACSharpSensor.SensorReached(541);
    return RNTRNTRNT_298;
  }
  public static implicit operator double(integer i)
  {
    System.Double RNTRNTRNT_299 = Convert.ToDouble(i.value);
    IACSharpSensor.IACSharpSensor.SensorReached(542);
    return RNTRNTRNT_299;
  }
  public static integer operator +(integer x, integer y)
  {
    integer RNTRNTRNT_300 = new integer(x.value + y.value);
    IACSharpSensor.IACSharpSensor.SensorReached(543);
    return RNTRNTRNT_300;
  }
}
class X
{
  public static object Add(integer x, object other)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(544);
    if (other is int) {
      System.Object RNTRNTRNT_301 = x + ((int)other);
      IACSharpSensor.IACSharpSensor.SensorReached(545);
      return RNTRNTRNT_301;
    }
    if (other is double) {
      System.Object RNTRNTRNT_302 = x + ((double)other);
      IACSharpSensor.IACSharpSensor.SensorReached(546);
      return RNTRNTRNT_302;
    }
    throw new InvalidOperationException();
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(547);
    integer i = new integer(3);
    double d = 4.0;
    object result = Add(i, d);
    if (!(result is double)) {
      System.Int32 RNTRNTRNT_303 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(548);
      return RNTRNTRNT_303;
    }
    if ((double)result != 7.0) {
      System.Int32 RNTRNTRNT_304 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(549);
      return RNTRNTRNT_304;
    }
    System.Int32 RNTRNTRNT_305 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(550);
    return RNTRNTRNT_305;
  }
}
