using System;
using System.Threading;
using System.Reflection;
using System.Reflection.Emit;
[AttributeUsage(AttributeTargets.All)]
public class MyAttribute : Attribute
{
  public MyAttribute(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(594);
    this.o = o;
    IACSharpSensor.IACSharpSensor.SensorReached(595);
  }
  public object my {
    get {
      System.Object RNTRNTRNT_325 = o;
      IACSharpSensor.IACSharpSensor.SensorReached(596);
      return RNTRNTRNT_325;
    }
  }
  object o;
}
public class MyConstructorBuilder
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(597);
    Type myHelloworld = MyCreateCallee(Thread.GetDomain());
    ConstructorInfo myConstructor = myHelloworld.GetConstructor(new Type[] { typeof(String) });
    object[] myAttributes1 = myConstructor.GetCustomAttributes(true);
    if (myAttributes1 == null) {
      System.Int32 RNTRNTRNT_326 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(598);
      return RNTRNTRNT_326;
    }
    if (myAttributes1.Length != 1) {
      System.Int32 RNTRNTRNT_327 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(599);
      return RNTRNTRNT_327;
    }
    MyAttribute myAttribute = myAttributes1[0] as MyAttribute;
    if (myAttribute == null) {
      System.Int32 RNTRNTRNT_328 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(600);
      return RNTRNTRNT_328;
    }
    if (myAttribute.my.GetType() != typeof(TypeCode)) {
      System.Int32 RNTRNTRNT_329 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(601);
      return RNTRNTRNT_329;
    }
    System.Int32 RNTRNTRNT_330 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(602);
    return RNTRNTRNT_330;
  }
  private static Type MyCreateCallee(AppDomain domain)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(603);
    AssemblyName myAssemblyName = new AssemblyName();
    myAssemblyName.Name = "EmittedAssembly";
    AssemblyBuilder myAssembly = domain.DefineDynamicAssembly(myAssemblyName, AssemblyBuilderAccess.Run);
    ModuleBuilder myModuleBuilder = myAssembly.DefineDynamicModule("EmittedModule");
    TypeBuilder myTypeBuilder = myModuleBuilder.DefineType("HelloWorld", TypeAttributes.Public);
    ConstructorBuilder myConstructor = myTypeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, new Type[] { typeof(String) });
    ILGenerator myILGenerator = myConstructor.GetILGenerator();
    myILGenerator.Emit(OpCodes.Ldstr, "Constructor is invoked");
    myILGenerator.Emit(OpCodes.Ldarg_1);
    MethodInfo myMethodInfo = typeof(Console).GetMethod("WriteLine", new Type[] { typeof(string) });
    myILGenerator.Emit(OpCodes.Call, myMethodInfo);
    myILGenerator.Emit(OpCodes.Ret);
    Type myType = typeof(MyAttribute);
    ConstructorInfo myConstructorInfo = myType.GetConstructor(new Type[] { typeof(object) });
    try {
      CustomAttributeBuilder methodCABuilder = new CustomAttributeBuilder(myConstructorInfo, new object[] { TypeCode.Double });
      myConstructor.SetCustomAttribute(methodCABuilder);
    } catch (ArgumentNullException ex) {
      Console.WriteLine("The following exception has occured : " + ex.Message);
    } catch (Exception ex) {
      Console.WriteLine("The following exception has occured : " + ex.Message);
    }
    Type RNTRNTRNT_331 = myTypeBuilder.CreateType();
    IACSharpSensor.IACSharpSensor.SensorReached(604);
    return RNTRNTRNT_331;
  }
}
