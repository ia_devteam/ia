using System;
class X
{
  static int Test(string format, params object[] args)
  {
    System.Int32 RNTRNTRNT_271 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(500);
    return RNTRNTRNT_271;
  }
  static int Test(string format, __arglist __arglist)
  {
    System.Int32 RNTRNTRNT_272 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(501);
    return RNTRNTRNT_272;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(502);
    if (Test("Hello", 1, 2, "World") != 1) {
      System.Int32 RNTRNTRNT_273 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(503);
      return RNTRNTRNT_273;
    }
    if (Test("Hello", __arglist("Boston")) != 2) {
      System.Int32 RNTRNTRNT_274 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(504);
      return RNTRNTRNT_274;
    }
    System.Int32 RNTRNTRNT_275 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(505);
    return RNTRNTRNT_275;
  }
}
