using System;
interface A
{
  string this[string s] { get; }
}
interface B : A
{
  void Test();
}
class X : B
{
  public string this[string s] {
    get {
      System.String RNTRNTRNT_27 = s;
      IACSharpSensor.IACSharpSensor.SensorReached(46);
      return RNTRNTRNT_27;
    }
  }
  public void Test()
  {
  }
}
public class Y
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(47);
    B b = new X();
    string s = b["test"];
    IACSharpSensor.IACSharpSensor.SensorReached(48);
  }
}
