public class Class2
{
  private AliasDefOperations __delegate;
  public string def_kind {
    get {
      System.String RNTRNTRNT_230 = __delegate.def_kind;
      IACSharpSensor.IACSharpSensor.SensorReached(409);
      return RNTRNTRNT_230;
    }
  }
  static void Main()
  {
  }
}
public interface AliasDefOperations : ContainedOperations, IDLTypeOperations
{
}
public interface ContainedOperations : IRObjectOperations
{
}
public interface IDLTypeOperations : IRObjectOperations
{
}
public interface IRObjectOperations
{
  string def_kind { get; }
}
