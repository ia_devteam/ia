using System;
using System.Collections;
using System.Collections.Specialized;
namespace MonoBUG
{
  public class Bug
  {
    public static int Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(121);
      FooList l = new FooList();
      Foo f1 = new Foo("First");
      Foo f2 = new Foo("Second");
      l.Add(f1);
      l.Add(f2);
      foreach (Foo f in l) {
      }
      if (FooList.foo_current_called != true) {
        System.Int32 RNTRNTRNT_52 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(122);
        return RNTRNTRNT_52;
      }
      if (FooList.ienumerator_current_called != false) {
        System.Int32 RNTRNTRNT_53 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(123);
        return RNTRNTRNT_53;
      }
      Console.WriteLine("Test passes");
      System.Int32 RNTRNTRNT_54 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(124);
      return RNTRNTRNT_54;
    }
  }
  public class Foo
  {
    private string m_name;
    public Foo(string name)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(125);
      m_name = name;
      IACSharpSensor.IACSharpSensor.SensorReached(126);
    }
    public string Name {
      get {
        System.String RNTRNTRNT_55 = m_name;
        IACSharpSensor.IACSharpSensor.SensorReached(127);
        return RNTRNTRNT_55;
      }
    }
  }
  [Serializable()]
  public class FooList : DictionaryBase
  {
    public static bool foo_current_called = false;
    public static bool ienumerator_current_called = false;
    public FooList()
    {
    }
    public void Add(Foo value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(128);
      Dictionary.Add(value.Name, value);
      IACSharpSensor.IACSharpSensor.SensorReached(129);
    }
    public new FooEnumerator GetEnumerator()
    {
      FooEnumerator RNTRNTRNT_56 = new FooEnumerator(this);
      IACSharpSensor.IACSharpSensor.SensorReached(130);
      return RNTRNTRNT_56;
    }
    public class FooEnumerator : object, IEnumerator
    {
      private IEnumerator baseEnumerator;
      private IEnumerable temp;
      public FooEnumerator(FooList mappings)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(131);
        this.temp = (IEnumerable)(mappings);
        this.baseEnumerator = temp.GetEnumerator();
        IACSharpSensor.IACSharpSensor.SensorReached(132);
      }
      public Foo Current {
        get {
          IACSharpSensor.IACSharpSensor.SensorReached(133);
          Console.WriteLine("Foo Current()");
          foo_current_called = true;
          Foo RNTRNTRNT_57 = (Foo)((DictionaryEntry)(baseEnumerator.Current)).Value;
          IACSharpSensor.IACSharpSensor.SensorReached(134);
          return RNTRNTRNT_57;
        }
      }
      object IEnumerator.Current {
        get {
          IACSharpSensor.IACSharpSensor.SensorReached(135);
          Console.WriteLine("object IEnumerator.Current()");
          ienumerator_current_called = true;
          System.Object RNTRNTRNT_58 = baseEnumerator.Current;
          IACSharpSensor.IACSharpSensor.SensorReached(136);
          return RNTRNTRNT_58;
        }
      }
      public bool MoveNext()
      {
        System.Boolean RNTRNTRNT_59 = baseEnumerator.MoveNext();
        IACSharpSensor.IACSharpSensor.SensorReached(137);
        return RNTRNTRNT_59;
      }
      bool IEnumerator.MoveNext()
      {
        System.Boolean RNTRNTRNT_60 = baseEnumerator.MoveNext();
        IACSharpSensor.IACSharpSensor.SensorReached(138);
        return RNTRNTRNT_60;
      }
      public void Reset()
      {
        IACSharpSensor.IACSharpSensor.SensorReached(139);
        baseEnumerator.Reset();
        IACSharpSensor.IACSharpSensor.SensorReached(140);
      }
      void IEnumerator.Reset()
      {
        IACSharpSensor.IACSharpSensor.SensorReached(141);
        baseEnumerator.Reset();
        IACSharpSensor.IACSharpSensor.SensorReached(142);
      }
    }
  }
}
