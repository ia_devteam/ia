using System;
class Foo
{
  static int t_count = 0, f_count = 0;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(392);
    Console.WriteLine(t && f);
    if (t_count != 1) {
      System.Int32 RNTRNTRNT_218 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(393);
      return RNTRNTRNT_218;
    }
    if (f_count != 1) {
      System.Int32 RNTRNTRNT_219 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(394);
      return RNTRNTRNT_219;
    }
    Console.WriteLine();
    Console.WriteLine(t && t);
    if (t_count != 3) {
      System.Int32 RNTRNTRNT_220 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(395);
      return RNTRNTRNT_220;
    }
    if (f_count != 1) {
      System.Int32 RNTRNTRNT_221 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(396);
      return RNTRNTRNT_221;
    }
    Console.WriteLine();
    System.Int32 RNTRNTRNT_222 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(397);
    return RNTRNTRNT_222;
  }
  static MyBool t {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(398);
      Console.WriteLine("t");
      t_count++;
      MyBool RNTRNTRNT_223 = new MyBool(true);
      IACSharpSensor.IACSharpSensor.SensorReached(399);
      return RNTRNTRNT_223;
    }
  }
  static MyBool f {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(400);
      Console.WriteLine("f");
      f_count++;
      MyBool RNTRNTRNT_224 = new MyBool(false);
      IACSharpSensor.IACSharpSensor.SensorReached(401);
      return RNTRNTRNT_224;
    }
  }
}
public struct MyBool
{
  bool v;
  public MyBool(bool v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(402);
    this.v = v;
    IACSharpSensor.IACSharpSensor.SensorReached(403);
  }
  public static MyBool operator &(MyBool x, MyBool y)
  {
    MyBool RNTRNTRNT_225 = new MyBool(x.v & y.v);
    IACSharpSensor.IACSharpSensor.SensorReached(404);
    return RNTRNTRNT_225;
  }
  public static MyBool operator |(MyBool x, MyBool y)
  {
    MyBool RNTRNTRNT_226 = new MyBool(x.v | y.v);
    IACSharpSensor.IACSharpSensor.SensorReached(405);
    return RNTRNTRNT_226;
  }
  public static bool operator true(MyBool x)
  {
    System.Boolean RNTRNTRNT_227 = x.v;
    IACSharpSensor.IACSharpSensor.SensorReached(406);
    return RNTRNTRNT_227;
  }
  public static bool operator false(MyBool x)
  {
    System.Boolean RNTRNTRNT_228 = !x.v;
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    return RNTRNTRNT_228;
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_229 = v.ToString();
    IACSharpSensor.IACSharpSensor.SensorReached(408);
    return RNTRNTRNT_229;
  }
}
