using System;
using System.Collections;
using System.Collections.Specialized;
public class List : IEnumerable
{
  int pos = 0;
  int[] items;
  public List(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(204);
    items = new int[i];
    IACSharpSensor.IACSharpSensor.SensorReached(205);
  }
  public void Add(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    items[pos++] = value;
    IACSharpSensor.IACSharpSensor.SensorReached(207);
  }
  public MyEnumerator GetEnumerator()
  {
    MyEnumerator RNTRNTRNT_98 = new MyEnumerator(this);
    IACSharpSensor.IACSharpSensor.SensorReached(208);
    return RNTRNTRNT_98;
  }
  IEnumerator IEnumerable.GetEnumerator()
  {
    IEnumerator RNTRNTRNT_99 = GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(209);
    return RNTRNTRNT_99;
  }
  public struct MyEnumerator : IEnumerator
  {
    List l;
    int p;
    public MyEnumerator(List l)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(210);
      this.l = l;
      p = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(211);
    }
    public object Current {
      get {
        System.Object RNTRNTRNT_100 = l.items[p];
        IACSharpSensor.IACSharpSensor.SensorReached(212);
        return RNTRNTRNT_100;
      }
    }
    public bool MoveNext()
    {
      System.Boolean RNTRNTRNT_101 = ++p < l.pos;
      IACSharpSensor.IACSharpSensor.SensorReached(213);
      return RNTRNTRNT_101;
    }
    public void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(214);
      p = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(215);
    }
  }
}
public class UberList : List
{
  public UberList(int i) : base(i)
  {
  }
  public static int Main(string[] args)
  {
    System.Int32 RNTRNTRNT_102 = One() && Two() && Three() ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(216);
    return RNTRNTRNT_102;
  }
  static bool One()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(217);
    List l = new List(1);
    l.Add(1);
    foreach (int i in l) {
      if (i == 1) {
        System.Boolean RNTRNTRNT_103 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(218);
        return RNTRNTRNT_103;
      }
    }
    System.Boolean RNTRNTRNT_104 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(219);
    return RNTRNTRNT_104;
  }
  static bool Two()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    List l = new UberList(1);
    l.Add(1);
    foreach (int i in l) {
      if (i == 1) {
        System.Boolean RNTRNTRNT_105 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(221);
        return RNTRNTRNT_105;
      }
    }
    System.Boolean RNTRNTRNT_106 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(222);
    return RNTRNTRNT_106;
  }
  static bool Three()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(223);
    UberList l = new UberList(1);
    l.Add(1);
    foreach (int i in l) {
      if (i == 1) {
        System.Boolean RNTRNTRNT_107 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(224);
        return RNTRNTRNT_107;
      }
    }
    System.Boolean RNTRNTRNT_108 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(225);
    return RNTRNTRNT_108;
  }
}
