using System;
using System.Reflection;
[AttributeUsage(AttributeTargets.Field)]
public class AccessibleAttribute : Attribute
{
}
public class MyClass
{
  [Accessible()]
  public const int MyConst = 1;
}
public class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(422);
    FieldInfo fieldInfo = typeof(MyClass).GetField("MyConst", BindingFlags.Static | BindingFlags.Public);
    AccessibleAttribute[] attributes = fieldInfo.GetCustomAttributes(typeof(AccessibleAttribute), true) as AccessibleAttribute[];
    if (attributes != null) {
      Console.WriteLine("Succeeded");
      System.Int32 RNTRNTRNT_234 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(423);
      return RNTRNTRNT_234;
    }
    System.Int32 RNTRNTRNT_235 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(424);
    return RNTRNTRNT_235;
  }
}
