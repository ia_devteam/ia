using System;
delegate void Test(int test);
class X
{
  static int result = 0;
  public static void hello(int arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(37);
    result += arg;
    IACSharpSensor.IACSharpSensor.SensorReached(38);
  }
  public static void world(int arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(39);
    result += 16 * arg;
    IACSharpSensor.IACSharpSensor.SensorReached(40);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(41);
    Test a = new Test(hello);
    Test b = new Test(world);
    (a + b)(1);
    if (result != 17) {
      System.Int32 RNTRNTRNT_23 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(42);
      return RNTRNTRNT_23;
    }
    ((result == 17) ? a : b)(2);
    if (result != 19) {
      System.Int32 RNTRNTRNT_24 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(43);
      return RNTRNTRNT_24;
    }
    ((result == 17) ? a : b)(2);
    if (result != 51) {
      System.Int32 RNTRNTRNT_25 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(44);
      return RNTRNTRNT_25;
    }
    System.Int32 RNTRNTRNT_26 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(45);
    return RNTRNTRNT_26;
  }
}
