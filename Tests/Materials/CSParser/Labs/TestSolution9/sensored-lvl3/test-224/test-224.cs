using System;
using System.Reflection;
[AttributeUsage(AttributeTargets.Field, Inherited = true)]
public class XmlMemberArrayAttribute : Attribute
{
  char[] separator = new char[] { ',' };
  string name;
  bool isRequired;
  public XmlMemberArrayAttribute(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    this.name = name;
    isRequired = false;
    IACSharpSensor.IACSharpSensor.SensorReached(178);
  }
  public char[] Separator {
    get {
      System.Char[] RNTRNTRNT_90 = separator;
      IACSharpSensor.IACSharpSensor.SensorReached(179);
      return RNTRNTRNT_90;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(180);
      separator = value;
      IACSharpSensor.IACSharpSensor.SensorReached(181);
    }
  }
  public string Name {
    get {
      System.String RNTRNTRNT_91 = name;
      IACSharpSensor.IACSharpSensor.SensorReached(182);
      return RNTRNTRNT_91;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(183);
      name = value;
      IACSharpSensor.IACSharpSensor.SensorReached(184);
    }
  }
  public bool IsRequired {
    get {
      System.Boolean RNTRNTRNT_92 = isRequired;
      IACSharpSensor.IACSharpSensor.SensorReached(185);
      return RNTRNTRNT_92;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(186);
      isRequired = value;
      IACSharpSensor.IACSharpSensor.SensorReached(187);
    }
  }
}
public class t
{
  [XmlMemberArrayAttribute("shortcut", Separator = new char[] { '|' })]
  string[] shortcut;
  public static void Main()
  {
  }
}
