public enum Modifiers
{
  Public = 0x1
}
class Foo
{
  internal Modifiers Modifiers {
    get {
      Modifiers RNTRNTRNT_8 = Modifiers.Public;
      IACSharpSensor.IACSharpSensor.SensorReached(15);
      return RNTRNTRNT_8;
    }
  }
}
class Bar
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    System.Console.WriteLine(Modifiers.Public);
    System.Int32 RNTRNTRNT_9 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(17);
    return RNTRNTRNT_9;
  }
}
