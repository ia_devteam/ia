using System;
using System.Diagnostics;
class TestClass
{
  [Conditional("UNDEFINED_CONDITION")]
  static void ConditionalMethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    Environment.Exit(1);
    IACSharpSensor.IACSharpSensor.SensorReached(369);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(370);
    ConditionalMethod();
    Console.WriteLine("Succeeded");
    System.Int32 RNTRNTRNT_209 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(371);
    return RNTRNTRNT_209;
  }
}
