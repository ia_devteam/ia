using System;
struct Blah : System.IDisposable
{
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(410);
    Console.WriteLine("foo");
    IACSharpSensor.IACSharpSensor.SensorReached(411);
  }
}
class B
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(412);
    using (Blah b = new Blah()) {
      Console.WriteLine("...");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(413);
  }
}
