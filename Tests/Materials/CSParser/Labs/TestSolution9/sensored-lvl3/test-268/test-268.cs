public enum MyEnum
{
  V = 1
}
class X
{
  public MyEnum MyEnum;
  class Nested
  {
    internal MyEnum D()
    {
      MyEnum RNTRNTRNT_251 = MyEnum.V;
      IACSharpSensor.IACSharpSensor.SensorReached(468);
      return RNTRNTRNT_251;
    }
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(469);
    Nested n = new Nested();
    System.Int32 RNTRNTRNT_252 = n.D() == MyEnum.V ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(470);
    return RNTRNTRNT_252;
  }
}
