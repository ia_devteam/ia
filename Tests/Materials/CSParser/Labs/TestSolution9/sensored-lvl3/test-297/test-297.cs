using System;
[My((long)1)]
[My(TypeCode.Empty)]
[My(typeof(System.Enum))]
class T
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(608);
    object[] a = Attribute.GetCustomAttributes(typeof(T), false);
    if (a.Length != 3) {
      System.Int32 RNTRNTRNT_334 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(609);
      return RNTRNTRNT_334;
    }
    foreach (object o in a) {
      My attr = (My)o;
      if (attr.obj.GetType() == typeof(long)) {
        long val = (long)attr.obj;
        if (val != 1) {
          System.Int32 RNTRNTRNT_335 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(610);
          return RNTRNTRNT_335;
        }
      } else if (attr.obj.GetType() == typeof(TypeCode)) {
        TypeCode val = (TypeCode)attr.obj;
        if (val != TypeCode.Empty) {
          System.Int32 RNTRNTRNT_338 = 3;
          IACSharpSensor.IACSharpSensor.SensorReached(613);
          return RNTRNTRNT_338;
        }
      } else if (attr.obj.GetType().IsSubclassOf(typeof(Type))) {
        Type val = (Type)attr.obj;
        if (val != typeof(System.Enum)) {
          System.Int32 RNTRNTRNT_337 = 4;
          IACSharpSensor.IACSharpSensor.SensorReached(612);
          return RNTRNTRNT_337;
        }
      } else {
        System.Int32 RNTRNTRNT_336 = 5;
        IACSharpSensor.IACSharpSensor.SensorReached(611);
        return RNTRNTRNT_336;
      }
    }
    object[] ats = typeof(T).GetMethod("Login").GetCustomAttributes(typeof(My), true);
    My at = (My)ats[0];
    if (at.Val != AnEnum.a) {
      System.Int32 RNTRNTRNT_339 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(614);
      return RNTRNTRNT_339;
    }
    System.Int32 RNTRNTRNT_340 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(615);
    return RNTRNTRNT_340;
  }
  [My(1, Val = AnEnum.a)]
  public void Login(string a)
  {
  }
}
[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
class My : Attribute
{
  public object obj;
  public My(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(616);
    obj = o;
    IACSharpSensor.IACSharpSensor.SensorReached(617);
  }
  public AnEnum Val;
}
public enum AnEnum
{
  a,
  b,
  c
}
