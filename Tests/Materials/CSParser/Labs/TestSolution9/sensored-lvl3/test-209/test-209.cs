using System;
struct A
{
  public readonly int i;
  public A(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(49);
    this.i = i;
    IACSharpSensor.IACSharpSensor.SensorReached(50);
  }
}
class X
{
  int i;
  public int Foo {
    get {
      System.Int32 RNTRNTRNT_28 = 2 * i;
      IACSharpSensor.IACSharpSensor.SensorReached(51);
      return RNTRNTRNT_28;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(52);
      i = value;
      IACSharpSensor.IACSharpSensor.SensorReached(53);
    }
  }
  public int this[int a] {
    get {
      System.Int32 RNTRNTRNT_29 = (int)Foo;
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      return RNTRNTRNT_29;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(55);
      Foo = a;
      IACSharpSensor.IACSharpSensor.SensorReached(56);
    }
  }
  public string this[string a] {
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(57);
      Console.WriteLine(a);
      IACSharpSensor.IACSharpSensor.SensorReached(58);
    }
  }
  public string Bar {
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(59);
      Console.WriteLine(value);
      IACSharpSensor.IACSharpSensor.SensorReached(60);
    }
  }
  public A A {
    get {
      A RNTRNTRNT_30 = new A(5);
      IACSharpSensor.IACSharpSensor.SensorReached(61);
      return RNTRNTRNT_30;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(62);
      Console.WriteLine(value);
      IACSharpSensor.IACSharpSensor.SensorReached(63);
    }
  }
  public X(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(64);
    this.i = i;
    IACSharpSensor.IACSharpSensor.SensorReached(65);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(66);
    X x = new X(9);
    int a = x.Foo = 16;
    int b = x[8] = 32;
    x["Test"] = "Hello";
    x.Bar = "World";
    x.A = new A(9);
    System.Int32 RNTRNTRNT_31 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(67);
    return RNTRNTRNT_31;
  }
}
