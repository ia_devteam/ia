public class Parent
{
  public Parent()
  {
  }
  private Collide Collide;
}
public class Child : Parent
{
  public class Nested
  {
    public readonly Collide Test;
    public Nested()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(6);
      Test = Collide.Die;
      IACSharpSensor.IACSharpSensor.SensorReached(7);
    }
  }
}
public class Collide
{
  public Collide(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(8);
    this.A = a;
    IACSharpSensor.IACSharpSensor.SensorReached(9);
  }
  public readonly int A;
  public static readonly Collide Die = new Collide(5);
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(10);
    Child.Nested nested = new Child.Nested();
    if (nested.Test.A != 5) {
      System.Int32 RNTRNTRNT_5 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(11);
      return RNTRNTRNT_5;
    }
    System.Int32 RNTRNTRNT_6 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(12);
    return RNTRNTRNT_6;
  }
}
