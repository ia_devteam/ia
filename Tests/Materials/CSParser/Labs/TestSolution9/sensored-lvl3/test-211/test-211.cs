class X
{
  public readonly int value;
  public X(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(70);
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(71);
  }
  public static implicit operator X(int y)
  {
    X RNTRNTRNT_32 = new X(y);
    IACSharpSensor.IACSharpSensor.SensorReached(72);
    return RNTRNTRNT_32;
  }
}
class Y
{
  public readonly X x;
  public Y(X x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(73);
    this.x = x;
    IACSharpSensor.IACSharpSensor.SensorReached(74);
  }
  public static implicit operator Y(X x)
  {
    Y RNTRNTRNT_33 = new Y(x);
    IACSharpSensor.IACSharpSensor.SensorReached(75);
    return RNTRNTRNT_33;
  }
}
class Z
{
  public readonly Y y;
  public Z(Y y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(76);
    this.y = y;
    IACSharpSensor.IACSharpSensor.SensorReached(77);
  }
  public static implicit operator Z(Y y)
  {
    Z RNTRNTRNT_34 = new Z(y);
    IACSharpSensor.IACSharpSensor.SensorReached(78);
    return RNTRNTRNT_34;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(79);
    int a = 5;
    Y y = (Y)(X)a;
    int b = (System.Int32)int.Parse("1");
    System.Int32 RNTRNTRNT_35 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(80);
    return RNTRNTRNT_35;
  }
}
