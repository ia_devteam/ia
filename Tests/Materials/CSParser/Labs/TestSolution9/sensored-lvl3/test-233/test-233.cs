using System;
using System.Reflection;
class Tests
{
  static int Main()
  {
    System.Int32 RNTRNTRNT_113 = TestDriver.RunTests(typeof(Tests));
    IACSharpSensor.IACSharpSensor.SensorReached(239);
    return RNTRNTRNT_113;
  }
  static int test_0_beq()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(240);
    double a = 2.0;
    if (a != 2.0) {
      System.Int32 RNTRNTRNT_114 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(241);
      return RNTRNTRNT_114;
    }
    System.Int32 RNTRNTRNT_115 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(242);
    return RNTRNTRNT_115;
  }
  static int test_0_bne_un()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(243);
    double a = 2.0;
    if (a == 1.0) {
      System.Int32 RNTRNTRNT_116 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(244);
      return RNTRNTRNT_116;
    }
    System.Int32 RNTRNTRNT_117 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    return RNTRNTRNT_117;
  }
  static int test_0_conv_r8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(246);
    double a = 2;
    if (a != 2.0) {
      System.Int32 RNTRNTRNT_118 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(247);
      return RNTRNTRNT_118;
    }
    System.Int32 RNTRNTRNT_119 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(248);
    return RNTRNTRNT_119;
  }
  static int test_0_conv_i()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(249);
    double a = 2.0;
    int i = (int)a;
    if (i != 2) {
      System.Int32 RNTRNTRNT_120 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(250);
      return RNTRNTRNT_120;
    }
    uint ui = (uint)a;
    if (ui != 2) {
      System.Int32 RNTRNTRNT_121 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(251);
      return RNTRNTRNT_121;
    }
    short s = (short)a;
    if (s != 2) {
      System.Int32 RNTRNTRNT_122 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(252);
      return RNTRNTRNT_122;
    }
    ushort us = (ushort)a;
    if (us != 2) {
      System.Int32 RNTRNTRNT_123 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(253);
      return RNTRNTRNT_123;
    }
    byte b = (byte)a;
    if (b != 2) {
      System.Int32 RNTRNTRNT_124 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(254);
      return RNTRNTRNT_124;
    }
    System.Int32 RNTRNTRNT_125 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(255);
    return RNTRNTRNT_125;
  }
  static int test_5_conv_r4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(256);
    int i = 5;
    float f = (float)i;
    System.Int32 RNTRNTRNT_126 = (int)f;
    IACSharpSensor.IACSharpSensor.SensorReached(257);
    return RNTRNTRNT_126;
  }
  static int test_5_double_conv_r4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(258);
    double d = 5.0;
    float f = (float)d;
    System.Int32 RNTRNTRNT_127 = (int)f;
    IACSharpSensor.IACSharpSensor.SensorReached(259);
    return RNTRNTRNT_127;
  }
  static int test_5_float_conv_r8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(260);
    float f = 5.0f;
    double d = (double)f;
    System.Int32 RNTRNTRNT_128 = (int)d;
    IACSharpSensor.IACSharpSensor.SensorReached(261);
    return RNTRNTRNT_128;
  }
  static int test_5_conv_r8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(262);
    int i = 5;
    double f = (double)i;
    System.Int32 RNTRNTRNT_129 = (int)f;
    IACSharpSensor.IACSharpSensor.SensorReached(263);
    return RNTRNTRNT_129;
  }
  static int test_5_add()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(264);
    double a = 2.0;
    double b = 3.0;
    System.Int32 RNTRNTRNT_130 = (int)(a + b);
    IACSharpSensor.IACSharpSensor.SensorReached(265);
    return RNTRNTRNT_130;
  }
  static int test_5_sub()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(266);
    double a = 8.0;
    double b = 3.0;
    System.Int32 RNTRNTRNT_131 = (int)(a - b);
    IACSharpSensor.IACSharpSensor.SensorReached(267);
    return RNTRNTRNT_131;
  }
  static int test_24_mul()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(268);
    double a = 8.0;
    double b = 3.0;
    System.Int32 RNTRNTRNT_132 = (int)(a * b);
    IACSharpSensor.IACSharpSensor.SensorReached(269);
    return RNTRNTRNT_132;
  }
  static int test_4_div()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(270);
    double a = 8.0;
    double b = 2.0;
    System.Int32 RNTRNTRNT_133 = (int)(a / b);
    IACSharpSensor.IACSharpSensor.SensorReached(271);
    return RNTRNTRNT_133;
  }
  static int test_2_rem()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(272);
    double a = 8.0;
    double b = 3.0;
    System.Int32 RNTRNTRNT_134 = (int)(a % b);
    IACSharpSensor.IACSharpSensor.SensorReached(273);
    return RNTRNTRNT_134;
  }
  static int test_2_neg()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(274);
    double a = -2.0;
    System.Int32 RNTRNTRNT_135 = (int)(-a);
    IACSharpSensor.IACSharpSensor.SensorReached(275);
    return RNTRNTRNT_135;
  }
  static int test_46_float_add_spill()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(276);
    double a = 1;
    double b = 2;
    double c = 3;
    double d = 4;
    double e = 5;
    double f = 6;
    double g = 7;
    double h = 8;
    double i = 9;
    System.Int32 RNTRNTRNT_136 = (int)(1.0 + (a + (b + (c + (d + (e + (f + (g + (h + i)))))))));
    IACSharpSensor.IACSharpSensor.SensorReached(277);
    return RNTRNTRNT_136;
  }
  static int test_362880_float_mul_spill()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(278);
    double a = 1;
    double b = 2;
    double c = 3;
    double d = 4;
    double e = 5;
    double f = 6;
    double g = 7;
    double h = 8;
    double i = 9;
    System.Int32 RNTRNTRNT_137 = (int)(1.0 * (a * (b * (c * (d * (e * (f * (g * (h * i)))))))));
    IACSharpSensor.IACSharpSensor.SensorReached(279);
    return RNTRNTRNT_137;
  }
  static int test_4_long_cast()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(280);
    long a = 1000;
    double d = (double)a;
    long b = (long)d;
    if (b != 1000) {
      System.Int32 RNTRNTRNT_138 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(281);
      return RNTRNTRNT_138;
    }
    System.Int32 RNTRNTRNT_139 = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(282);
    return RNTRNTRNT_139;
  }
  static int test_16_float_cmp()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    double a = 2.0;
    double b = 1.0;
    int result = 0;
    bool val;
    val = a == a;
    if (!val) {
      System.Int32 RNTRNTRNT_140 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(284);
      return RNTRNTRNT_140;
    }
    result++;
    val = (a != a);
    if (val) {
      System.Int32 RNTRNTRNT_141 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(285);
      return RNTRNTRNT_141;
    }
    result++;
    val = a < a;
    if (val) {
      System.Int32 RNTRNTRNT_142 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(286);
      return RNTRNTRNT_142;
    }
    result++;
    val = a > a;
    if (val) {
      System.Int32 RNTRNTRNT_143 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(287);
      return RNTRNTRNT_143;
    }
    result++;
    val = a <= a;
    if (!val) {
      System.Int32 RNTRNTRNT_144 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(288);
      return RNTRNTRNT_144;
    }
    result++;
    val = a >= a;
    if (!val) {
      System.Int32 RNTRNTRNT_145 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(289);
      return RNTRNTRNT_145;
    }
    result++;
    val = b == a;
    if (val) {
      System.Int32 RNTRNTRNT_146 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(290);
      return RNTRNTRNT_146;
    }
    result++;
    val = b < a;
    if (!val) {
      System.Int32 RNTRNTRNT_147 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(291);
      return RNTRNTRNT_147;
    }
    result++;
    val = b > a;
    if (val) {
      System.Int32 RNTRNTRNT_148 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(292);
      return RNTRNTRNT_148;
    }
    result++;
    val = b <= a;
    if (!val) {
      System.Int32 RNTRNTRNT_149 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(293);
      return RNTRNTRNT_149;
    }
    result++;
    val = b >= a;
    if (val) {
      System.Int32 RNTRNTRNT_150 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(294);
      return RNTRNTRNT_150;
    }
    result++;
    val = a == b;
    if (val) {
      System.Int32 RNTRNTRNT_151 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(295);
      return RNTRNTRNT_151;
    }
    result++;
    val = a < b;
    if (val) {
      System.Int32 RNTRNTRNT_152 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(296);
      return RNTRNTRNT_152;
    }
    result++;
    val = a > b;
    if (!val) {
      System.Int32 RNTRNTRNT_153 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(297);
      return RNTRNTRNT_153;
    }
    result++;
    val = a <= b;
    if (val) {
      System.Int32 RNTRNTRNT_154 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(298);
      return RNTRNTRNT_154;
    }
    result++;
    val = a >= b;
    if (!val) {
      System.Int32 RNTRNTRNT_155 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(299);
      return RNTRNTRNT_155;
    }
    result++;
    System.Int32 RNTRNTRNT_156 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(300);
    return RNTRNTRNT_156;
  }
  static int test_15_float_cmp_un()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(301);
    double a = Double.NaN;
    double b = 1.0;
    int result = 0;
    bool val;
    val = a == a;
    if (val) {
      System.Int32 RNTRNTRNT_157 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(302);
      return RNTRNTRNT_157;
    }
    result++;
    val = a < a;
    if (val) {
      System.Int32 RNTRNTRNT_158 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(303);
      return RNTRNTRNT_158;
    }
    result++;
    val = a > a;
    if (val) {
      System.Int32 RNTRNTRNT_159 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(304);
      return RNTRNTRNT_159;
    }
    result++;
    val = a <= a;
    if (val) {
      System.Int32 RNTRNTRNT_160 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(305);
      return RNTRNTRNT_160;
    }
    result++;
    val = a >= a;
    if (val) {
      System.Int32 RNTRNTRNT_161 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(306);
      return RNTRNTRNT_161;
    }
    result++;
    val = b == a;
    if (val) {
      System.Int32 RNTRNTRNT_162 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(307);
      return RNTRNTRNT_162;
    }
    result++;
    val = b < a;
    if (val) {
      System.Int32 RNTRNTRNT_163 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(308);
      return RNTRNTRNT_163;
    }
    result++;
    val = b > a;
    if (val) {
      System.Int32 RNTRNTRNT_164 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(309);
      return RNTRNTRNT_164;
    }
    result++;
    val = b <= a;
    if (val) {
      System.Int32 RNTRNTRNT_165 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(310);
      return RNTRNTRNT_165;
    }
    result++;
    val = b >= a;
    if (val) {
      System.Int32 RNTRNTRNT_166 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(311);
      return RNTRNTRNT_166;
    }
    result++;
    val = a == b;
    if (val) {
      System.Int32 RNTRNTRNT_167 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(312);
      return RNTRNTRNT_167;
    }
    result++;
    val = a < b;
    if (val) {
      System.Int32 RNTRNTRNT_168 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(313);
      return RNTRNTRNT_168;
    }
    result++;
    val = a > b;
    if (val) {
      System.Int32 RNTRNTRNT_169 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(314);
      return RNTRNTRNT_169;
    }
    result++;
    val = a <= b;
    if (val) {
      System.Int32 RNTRNTRNT_170 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(315);
      return RNTRNTRNT_170;
    }
    result++;
    val = a >= b;
    if (val) {
      System.Int32 RNTRNTRNT_171 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(316);
      return RNTRNTRNT_171;
    }
    result++;
    System.Int32 RNTRNTRNT_172 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(317);
    return RNTRNTRNT_172;
  }
  static int test_15_float_branch()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(318);
    double a = 2.0;
    double b = 1.0;
    int result = 0;
    if (!(a == a)) {
      System.Int32 RNTRNTRNT_173 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(319);
      return RNTRNTRNT_173;
    }
    result++;
    if (a < a) {
      System.Int32 RNTRNTRNT_174 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(320);
      return RNTRNTRNT_174;
    }
    result++;
    if (a > a) {
      System.Int32 RNTRNTRNT_175 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(321);
      return RNTRNTRNT_175;
    }
    result++;
    if (!(a <= a)) {
      System.Int32 RNTRNTRNT_176 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(322);
      return RNTRNTRNT_176;
    }
    result++;
    if (!(a >= a)) {
      System.Int32 RNTRNTRNT_177 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(323);
      return RNTRNTRNT_177;
    }
    result++;
    if (b == a) {
      System.Int32 RNTRNTRNT_178 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(324);
      return RNTRNTRNT_178;
    }
    result++;
    if (!(b < a)) {
      System.Int32 RNTRNTRNT_179 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(325);
      return RNTRNTRNT_179;
    }
    result++;
    if (b > a) {
      System.Int32 RNTRNTRNT_180 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(326);
      return RNTRNTRNT_180;
    }
    result++;
    if (!(b <= a)) {
      System.Int32 RNTRNTRNT_181 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(327);
      return RNTRNTRNT_181;
    }
    result++;
    if (b >= a) {
      System.Int32 RNTRNTRNT_182 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(328);
      return RNTRNTRNT_182;
    }
    result++;
    if (a == b) {
      System.Int32 RNTRNTRNT_183 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(329);
      return RNTRNTRNT_183;
    }
    result++;
    if (a < b) {
      System.Int32 RNTRNTRNT_184 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(330);
      return RNTRNTRNT_184;
    }
    result++;
    if (!(a > b)) {
      System.Int32 RNTRNTRNT_185 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(331);
      return RNTRNTRNT_185;
    }
    result++;
    if (a <= b) {
      System.Int32 RNTRNTRNT_186 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(332);
      return RNTRNTRNT_186;
    }
    result++;
    if (!(a >= b)) {
      System.Int32 RNTRNTRNT_187 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(333);
      return RNTRNTRNT_187;
    }
    result++;
    System.Int32 RNTRNTRNT_188 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(334);
    return RNTRNTRNT_188;
  }
  static int test_15_float_branch_un()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(335);
    double a = Double.NaN;
    double b = 1.0;
    int result = 0;
    if (a == a) {
      System.Int32 RNTRNTRNT_189 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(336);
      return RNTRNTRNT_189;
    }
    result++;
    if (a < a) {
      System.Int32 RNTRNTRNT_190 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(337);
      return RNTRNTRNT_190;
    }
    result++;
    if (a > a) {
      System.Int32 RNTRNTRNT_191 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(338);
      return RNTRNTRNT_191;
    }
    result++;
    if (a <= a) {
      System.Int32 RNTRNTRNT_192 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(339);
      return RNTRNTRNT_192;
    }
    result++;
    if (a >= a) {
      System.Int32 RNTRNTRNT_193 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(340);
      return RNTRNTRNT_193;
    }
    result++;
    if (b == a) {
      System.Int32 RNTRNTRNT_194 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(341);
      return RNTRNTRNT_194;
    }
    result++;
    if (b < a) {
      System.Int32 RNTRNTRNT_195 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(342);
      return RNTRNTRNT_195;
    }
    result++;
    if (b > a) {
      System.Int32 RNTRNTRNT_196 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(343);
      return RNTRNTRNT_196;
    }
    result++;
    if (b <= a) {
      System.Int32 RNTRNTRNT_197 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(344);
      return RNTRNTRNT_197;
    }
    result++;
    if (b >= a) {
      System.Int32 RNTRNTRNT_198 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(345);
      return RNTRNTRNT_198;
    }
    result++;
    if (a == b) {
      System.Int32 RNTRNTRNT_199 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(346);
      return RNTRNTRNT_199;
    }
    result++;
    if (a < b) {
      System.Int32 RNTRNTRNT_200 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(347);
      return RNTRNTRNT_200;
    }
    result++;
    if (a > b) {
      System.Int32 RNTRNTRNT_201 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(348);
      return RNTRNTRNT_201;
    }
    result++;
    if (a <= b) {
      System.Int32 RNTRNTRNT_202 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(349);
      return RNTRNTRNT_202;
    }
    result++;
    if (a >= b) {
      System.Int32 RNTRNTRNT_203 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(350);
      return RNTRNTRNT_203;
    }
    result++;
    System.Int32 RNTRNTRNT_204 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(351);
    return RNTRNTRNT_204;
  }
}
public class TestDriver
{
  public static int RunTests(Type type, string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(352);
    int failed = 0, ran = 0;
    int result, expected, elen;
    int i, j;
    string name;
    MethodInfo[] methods;
    bool do_timings = false;
    int tms = 0;
    DateTime start, end = DateTime.Now;
    if (args != null && args.Length > 0) {
      for (j = 0; j < args.Length; j++) {
        if (args[j] == "--time") {
          do_timings = true;
          string[] new_args = new string[args.Length - 1];
          for (i = 0; i < j; ++i) {
            new_args[i] = args[i];
          }
          j++;
          for (; j < args.Length; ++i,++j) {
            new_args[i] = args[j];
          }
          args = new_args;
          break;
        }
      }
    }
    methods = type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
    for (i = 0; i < methods.Length; ++i) {
      name = methods[i].Name;
      if (!name.StartsWith("test_")) {
        continue;
      }
      if (args != null && args.Length > 0) {
        bool found = false;
        for (j = 0; j < args.Length; j++) {
          if (name.EndsWith(args[j])) {
            found = true;
            break;
          }
        }
        if (!found) {
          continue;
        }
      }
      for (j = 5; j < name.Length; ++j) {
        if (!Char.IsDigit(name[j])) {
          break;
        }
      }
      expected = Int32.Parse(name.Substring(5, j - 5));
      start = DateTime.Now;
      result = (int)methods[i].Invoke(null, null);
      if (do_timings) {
        end = DateTime.Now;
        long tdiff = end.Ticks - start.Ticks;
        int mdiff = (int)tdiff / 10000;
        tms += mdiff;
        Console.WriteLine("{0} took {1} ms", name, mdiff);
      }
      ran++;
      if (result != expected) {
        failed++;
        Console.WriteLine("{0} failed: got {1}, expected {2}", name, result, expected);
      }
    }
    if (do_timings) {
      Console.WriteLine("Total ms: {0}", tms);
    }
    Console.WriteLine("Regression tests: {0} ran, {1} failed in {2}", ran, failed, type);
    System.Int32 RNTRNTRNT_205 = failed;
    IACSharpSensor.IACSharpSensor.SensorReached(353);
    return RNTRNTRNT_205;
  }
  public static int RunTests(Type type)
  {
    System.Int32 RNTRNTRNT_206 = RunTests(type, null);
    IACSharpSensor.IACSharpSensor.SensorReached(354);
    return RNTRNTRNT_206;
  }
}
