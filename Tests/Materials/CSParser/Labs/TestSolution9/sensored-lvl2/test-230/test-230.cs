using System;
using System.Reflection;
using System.Diagnostics;
[module: DebuggableAttribute(false, false)]
class TestClass
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(310);
    Module[] moduleArray;
    moduleArray = Assembly.GetExecutingAssembly().GetModules(false);
    Module myModule = moduleArray[0];
    object[] attributes;
    attributes = myModule.GetCustomAttributes(typeof(DebuggableAttribute), false);
    IACSharpSensor.IACSharpSensor.SensorReached(311);
    if (attributes[0] != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(312);
      Console.WriteLine("Succeeded");
      System.Int32 RNTRNTRNT_109 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(313);
      return RNTRNTRNT_109;
    }
    System.Int32 RNTRNTRNT_110 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(314);
    return RNTRNTRNT_110;
  }
}
