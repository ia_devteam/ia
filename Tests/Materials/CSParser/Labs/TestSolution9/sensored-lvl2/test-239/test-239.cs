using System;
using System.Diagnostics;
class BaseClass
{
  [Conditional("AAXXAA")]
  public virtual void ConditionalMethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(719);
    Environment.Exit(1);
    IACSharpSensor.IACSharpSensor.SensorReached(720);
  }
}
class TestClass : BaseClass
{
  public override void ConditionalMethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(721);
    base.ConditionalMethod();
    IACSharpSensor.IACSharpSensor.SensorReached(722);
  }
}
class MainClass
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(723);
    TestClass ts = new TestClass();
    ts.ConditionalMethod();
    Console.WriteLine("Succeeded");
    System.Int32 RNTRNTRNT_210 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(724);
    return RNTRNTRNT_210;
  }
}
