using T1 = N1.C1;
namespace N2
{
  class Test : T1
  {
    static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(116);
      Foo();
      System.Int32 RNTRNTRNT_39 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(117);
      return RNTRNTRNT_39;
    }
  }
}
namespace N1
{
  public class C1
  {
    public static void Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(118);
    }
  }
}
