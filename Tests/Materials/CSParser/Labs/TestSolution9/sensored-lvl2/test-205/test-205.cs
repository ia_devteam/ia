using System;
[AttributeUsage(AttributeTargets.All)]
public class A : Attribute
{
  public A(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(44);
  }
}
[A((object)AttributeTargets.All)]
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(45);
  }
}
