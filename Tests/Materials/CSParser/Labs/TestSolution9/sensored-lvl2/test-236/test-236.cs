using System;
using System.Collections;
namespace Tests
{
  public interface IIndexer
  {
    object this[int index] { get; set; }
  }
  public class Test : IIndexer
  {
    object[] InnerList;
    object IIndexer.this[int index] {
      get {
        System.Object RNTRNTRNT_208 = InnerList[index];
        IACSharpSensor.IACSharpSensor.SensorReached(701);
        return RNTRNTRNT_208;
      }
      set {
        IACSharpSensor.IACSharpSensor.SensorReached(702);
        InnerList[index] = value;
        IACSharpSensor.IACSharpSensor.SensorReached(703);
      }
    }
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(704);
      if (Attribute.GetCustomAttribute(typeof(Test), typeof(System.Reflection.DefaultMemberAttribute)) != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(705);
        throw new Exception("Class 'Test' has a DefaultMemberAttribute");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(706);
    }
  }
}
