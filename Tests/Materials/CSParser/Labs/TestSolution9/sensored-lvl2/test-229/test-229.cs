using System;
using System.Collections;
using System.Collections.Specialized;
public class List : IEnumerable
{
  int pos = 0;
  int[] items;
  public List(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(281);
    items = new int[i];
    IACSharpSensor.IACSharpSensor.SensorReached(282);
  }
  public void Add(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    items[pos++] = value;
    IACSharpSensor.IACSharpSensor.SensorReached(284);
  }
  public MyEnumerator GetEnumerator()
  {
    MyEnumerator RNTRNTRNT_98 = new MyEnumerator(this);
    IACSharpSensor.IACSharpSensor.SensorReached(285);
    return RNTRNTRNT_98;
  }
  IEnumerator IEnumerable.GetEnumerator()
  {
    IEnumerator RNTRNTRNT_99 = GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    return RNTRNTRNT_99;
  }
  public struct MyEnumerator : IEnumerator
  {
    List l;
    int p;
    public MyEnumerator(List l)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(287);
      this.l = l;
      p = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(288);
    }
    public object Current {
      get {
        System.Object RNTRNTRNT_100 = l.items[p];
        IACSharpSensor.IACSharpSensor.SensorReached(289);
        return RNTRNTRNT_100;
      }
    }
    public bool MoveNext()
    {
      System.Boolean RNTRNTRNT_101 = ++p < l.pos;
      IACSharpSensor.IACSharpSensor.SensorReached(290);
      return RNTRNTRNT_101;
    }
    public void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(291);
      p = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(292);
    }
  }
}
public class UberList : List
{
  public UberList(int i) : base(i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(293);
  }
  public static int Main(string[] args)
  {
    System.Int32 RNTRNTRNT_102 = One() && Two() && Three() ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(294);
    return RNTRNTRNT_102;
  }
  static bool One()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(295);
    List l = new List(1);
    l.Add(1);
    IACSharpSensor.IACSharpSensor.SensorReached(296);
    foreach (int i in l) {
      IACSharpSensor.IACSharpSensor.SensorReached(297);
      if (i == 1) {
        System.Boolean RNTRNTRNT_103 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(298);
        return RNTRNTRNT_103;
      }
    }
    System.Boolean RNTRNTRNT_104 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(299);
    return RNTRNTRNT_104;
  }
  static bool Two()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(300);
    List l = new UberList(1);
    l.Add(1);
    IACSharpSensor.IACSharpSensor.SensorReached(301);
    foreach (int i in l) {
      IACSharpSensor.IACSharpSensor.SensorReached(302);
      if (i == 1) {
        System.Boolean RNTRNTRNT_105 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(303);
        return RNTRNTRNT_105;
      }
    }
    System.Boolean RNTRNTRNT_106 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(304);
    return RNTRNTRNT_106;
  }
  static bool Three()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(305);
    UberList l = new UberList(1);
    l.Add(1);
    IACSharpSensor.IACSharpSensor.SensorReached(306);
    foreach (int i in l) {
      IACSharpSensor.IACSharpSensor.SensorReached(307);
      if (i == 1) {
        System.Boolean RNTRNTRNT_107 = true;
        IACSharpSensor.IACSharpSensor.SensorReached(308);
        return RNTRNTRNT_107;
      }
    }
    System.Boolean RNTRNTRNT_108 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(309);
    return RNTRNTRNT_108;
  }
}
