using System;
using System.Reflection;
using System.Runtime.CompilerServices;
public delegate void DelType();
struct S
{
  public event DelType MyEvent;
}
public class Test
{
  public event DelType MyEvent;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(949);
    EventInfo ei = typeof(Test).GetEvent("MyEvent");
    MethodImplAttributes methodImplAttributes = ei.GetAddMethod().GetMethodImplementationFlags();
    IACSharpSensor.IACSharpSensor.SensorReached(950);
    if ((methodImplAttributes & MethodImplAttributes.Synchronized) == 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(951);
      Console.WriteLine("FAILED");
      System.Int32 RNTRNTRNT_279 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(952);
      return RNTRNTRNT_279;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(953);
    methodImplAttributes = ei.GetRemoveMethod().GetMethodImplementationFlags();
    IACSharpSensor.IACSharpSensor.SensorReached(954);
    if ((methodImplAttributes & MethodImplAttributes.Synchronized) == 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(955);
      Console.WriteLine("FAILED");
      System.Int32 RNTRNTRNT_280 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(956);
      return RNTRNTRNT_280;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(957);
    ei = typeof(S).GetEvent("MyEvent");
    methodImplAttributes = ei.GetAddMethod().GetMethodImplementationFlags();
    IACSharpSensor.IACSharpSensor.SensorReached(958);
    if ((methodImplAttributes & MethodImplAttributes.Synchronized) != 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(959);
      Console.WriteLine("FAILED");
      System.Int32 RNTRNTRNT_281 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(960);
      return RNTRNTRNT_281;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(961);
    methodImplAttributes = ei.GetRemoveMethod().GetMethodImplementationFlags();
    IACSharpSensor.IACSharpSensor.SensorReached(962);
    if ((methodImplAttributes & MethodImplAttributes.Synchronized) != 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(963);
      Console.WriteLine("FAILED");
      System.Int32 RNTRNTRNT_282 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(964);
      return RNTRNTRNT_282;
    }
    System.Int32 RNTRNTRNT_283 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(965);
    return RNTRNTRNT_283;
  }
}
