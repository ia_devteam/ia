using System;
using System.Diagnostics;
class TestClass
{
  static int return_code = 1;
  [Conditional("C1"), Conditional("C2")]
  public static void ConditionalMethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(725);
    Console.WriteLine("Succeeded");
    return_code = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(726);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(727);
    ConditionalMethod();
    System.Int32 RNTRNTRNT_211 = return_code;
    IACSharpSensor.IACSharpSensor.SensorReached(728);
    return RNTRNTRNT_211;
  }
}
