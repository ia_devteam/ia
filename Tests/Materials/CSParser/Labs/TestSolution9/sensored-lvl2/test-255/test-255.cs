using System;
[AttributeUsage(AttributeTargets.All)]
public class TestAttribute : Attribute
{
}
[type: Obsolete()]
public class C
{
  [return: Test()]
  [Test()]
  void Method()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(803);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(804);
  }
}
