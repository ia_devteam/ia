using System;
using System.Diagnostics;
class TestClass
{
  [Conditional("UNDEFINED_CONDITION")]
  static void ConditionalMethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(715);
    Environment.Exit(1);
    IACSharpSensor.IACSharpSensor.SensorReached(716);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(717);
    ConditionalMethod();
    Console.WriteLine("Succeeded");
    System.Int32 RNTRNTRNT_209 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(718);
    return RNTRNTRNT_209;
  }
}
