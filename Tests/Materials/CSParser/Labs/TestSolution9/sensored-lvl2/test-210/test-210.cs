delegate void FooHandler();
class X
{
  public static void foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(90);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(91);
    object o = new FooHandler(foo);
    ((FooHandler)o)();
    IACSharpSensor.IACSharpSensor.SensorReached(92);
  }
}
