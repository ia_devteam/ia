using System;
using System.Threading;
using System.Reflection;
using System.Reflection.Emit;
[AttributeUsage(AttributeTargets.All)]
public class MyAttribute : Attribute
{
  public MyAttribute(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1074);
    this.o = o;
    IACSharpSensor.IACSharpSensor.SensorReached(1075);
  }
  public object my {
    get {
      System.Object RNTRNTRNT_325 = o;
      IACSharpSensor.IACSharpSensor.SensorReached(1076);
      return RNTRNTRNT_325;
    }
  }
  object o;
}
public class MyConstructorBuilder
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1077);
    Type myHelloworld = MyCreateCallee(Thread.GetDomain());
    ConstructorInfo myConstructor = myHelloworld.GetConstructor(new Type[] { typeof(String) });
    object[] myAttributes1 = myConstructor.GetCustomAttributes(true);
    IACSharpSensor.IACSharpSensor.SensorReached(1078);
    if (myAttributes1 == null) {
      System.Int32 RNTRNTRNT_326 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1079);
      return RNTRNTRNT_326;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1080);
    if (myAttributes1.Length != 1) {
      System.Int32 RNTRNTRNT_327 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1081);
      return RNTRNTRNT_327;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1082);
    MyAttribute myAttribute = myAttributes1[0] as MyAttribute;
    IACSharpSensor.IACSharpSensor.SensorReached(1083);
    if (myAttribute == null) {
      System.Int32 RNTRNTRNT_328 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1084);
      return RNTRNTRNT_328;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1085);
    if (myAttribute.my.GetType() != typeof(TypeCode)) {
      System.Int32 RNTRNTRNT_329 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1086);
      return RNTRNTRNT_329;
    }
    System.Int32 RNTRNTRNT_330 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1087);
    return RNTRNTRNT_330;
  }
  private static Type MyCreateCallee(AppDomain domain)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1088);
    AssemblyName myAssemblyName = new AssemblyName();
    myAssemblyName.Name = "EmittedAssembly";
    AssemblyBuilder myAssembly = domain.DefineDynamicAssembly(myAssemblyName, AssemblyBuilderAccess.Run);
    ModuleBuilder myModuleBuilder = myAssembly.DefineDynamicModule("EmittedModule");
    TypeBuilder myTypeBuilder = myModuleBuilder.DefineType("HelloWorld", TypeAttributes.Public);
    ConstructorBuilder myConstructor = myTypeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, new Type[] { typeof(String) });
    ILGenerator myILGenerator = myConstructor.GetILGenerator();
    myILGenerator.Emit(OpCodes.Ldstr, "Constructor is invoked");
    myILGenerator.Emit(OpCodes.Ldarg_1);
    MethodInfo myMethodInfo = typeof(Console).GetMethod("WriteLine", new Type[] { typeof(string) });
    myILGenerator.Emit(OpCodes.Call, myMethodInfo);
    myILGenerator.Emit(OpCodes.Ret);
    Type myType = typeof(MyAttribute);
    ConstructorInfo myConstructorInfo = myType.GetConstructor(new Type[] { typeof(object) });
    IACSharpSensor.IACSharpSensor.SensorReached(1089);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(1090);
      CustomAttributeBuilder methodCABuilder = new CustomAttributeBuilder(myConstructorInfo, new object[] { TypeCode.Double });
      myConstructor.SetCustomAttribute(methodCABuilder);
    } catch (ArgumentNullException ex) {
      IACSharpSensor.IACSharpSensor.SensorReached(1091);
      Console.WriteLine("The following exception has occured : " + ex.Message);
      IACSharpSensor.IACSharpSensor.SensorReached(1092);
    } catch (Exception ex) {
      IACSharpSensor.IACSharpSensor.SensorReached(1093);
      Console.WriteLine("The following exception has occured : " + ex.Message);
      IACSharpSensor.IACSharpSensor.SensorReached(1094);
    }
    Type RNTRNTRNT_331 = myTypeBuilder.CreateType();
    IACSharpSensor.IACSharpSensor.SensorReached(1095);
    return RNTRNTRNT_331;
  }
}
