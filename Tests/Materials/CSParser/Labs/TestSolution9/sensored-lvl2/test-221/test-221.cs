class A
{
  public virtual int Blah {
    get {
      System.Int32 RNTRNTRNT_61 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(187);
      return RNTRNTRNT_61;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(188); }
  }
}
class B : A
{
  public override int Blah {
    get {
      System.Int32 RNTRNTRNT_62 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(189);
      return RNTRNTRNT_62;
    }
  }
  public static bool Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(190);
    B b = new B();
    IACSharpSensor.IACSharpSensor.SensorReached(191);
    if (b.Blah != 2) {
      System.Boolean RNTRNTRNT_63 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(192);
      return RNTRNTRNT_63;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(193);
    if (b.Blah++ != 2) {
      System.Boolean RNTRNTRNT_64 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(194);
      return RNTRNTRNT_64;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(195);
    b.Blah = 0;
    System.Boolean RNTRNTRNT_65 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(196);
    return RNTRNTRNT_65;
  }
}
abstract class C
{
  public abstract int Blah { get; set; }
}
class D : C
{
  public override int Blah {
    get {
      System.Int32 RNTRNTRNT_66 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(197);
      return RNTRNTRNT_66;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(198); }
  }
}
class E : D
{
  public override int Blah {
    get {
      System.Int32 RNTRNTRNT_67 = base.Blah;
      IACSharpSensor.IACSharpSensor.SensorReached(199);
      return RNTRNTRNT_67;
    }
  }
  public static bool Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(200);
    E e = new E();
    IACSharpSensor.IACSharpSensor.SensorReached(201);
    if (e.Blah != 2) {
      System.Boolean RNTRNTRNT_68 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(202);
      return RNTRNTRNT_68;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    if (e.Blah++ != 2) {
      System.Boolean RNTRNTRNT_69 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(204);
      return RNTRNTRNT_69;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(205);
    e.Blah = 2;
    System.Boolean RNTRNTRNT_70 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    return RNTRNTRNT_70;
  }
}
interface IBlah
{
  int this[int i] { get; set; }
  int Blah { get; set; }
}
class F : IBlah
{
  int IBlah.this[int i] {
    get {
      System.Int32 RNTRNTRNT_71 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(207);
      return RNTRNTRNT_71;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(208); }
  }
  int IBlah.Blah {
    get {
      System.Int32 RNTRNTRNT_72 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(209);
      return RNTRNTRNT_72;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(210); }
  }
  public int this[int i] {
    get {
      System.Int32 RNTRNTRNT_73 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(211);
      return RNTRNTRNT_73;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(212); }
  }
  public int Blah {
    get {
      System.Int32 RNTRNTRNT_74 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(213);
      return RNTRNTRNT_74;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(214); }
  }
  public static bool Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    F f = new F();
    IACSharpSensor.IACSharpSensor.SensorReached(216);
    if (f.Blah != 2) {
      System.Boolean RNTRNTRNT_75 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(217);
      return RNTRNTRNT_75;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(218);
    if (f.Blah++ != 2) {
      System.Boolean RNTRNTRNT_76 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(219);
      return RNTRNTRNT_76;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    f.Blah = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(221);
    if (f[1] != 2) {
      System.Boolean RNTRNTRNT_77 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(222);
      return RNTRNTRNT_77;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(223);
    if (f[1]++ != 2) {
      System.Boolean RNTRNTRNT_78 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(224);
      return RNTRNTRNT_78;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(225);
    f[1] = 2;
    System.Boolean RNTRNTRNT_79 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(226);
    return RNTRNTRNT_79;
  }
}
class Driver
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(227);
    if (!B.Test()) {
      System.Int32 RNTRNTRNT_80 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(228);
      return RNTRNTRNT_80;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(229);
    if (!E.Test()) {
      System.Int32 RNTRNTRNT_81 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(230);
      return RNTRNTRNT_81;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(231);
    if (!F.Test()) {
      System.Int32 RNTRNTRNT_82 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(232);
      return RNTRNTRNT_82;
    }
    System.Int32 RNTRNTRNT_83 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(233);
    return RNTRNTRNT_83;
  }
}
