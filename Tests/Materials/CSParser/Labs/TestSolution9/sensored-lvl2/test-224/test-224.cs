using System;
using System.Reflection;
[AttributeUsage(AttributeTargets.Field, Inherited = true)]
public class XmlMemberArrayAttribute : Attribute
{
  char[] separator = new char[] { ',' };
  string name;
  bool isRequired;
  public XmlMemberArrayAttribute(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    this.name = name;
    isRequired = false;
    IACSharpSensor.IACSharpSensor.SensorReached(246);
  }
  public char[] Separator {
    get {
      System.Char[] RNTRNTRNT_90 = separator;
      IACSharpSensor.IACSharpSensor.SensorReached(247);
      return RNTRNTRNT_90;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(248);
      separator = value;
      IACSharpSensor.IACSharpSensor.SensorReached(249);
    }
  }
  public string Name {
    get {
      System.String RNTRNTRNT_91 = name;
      IACSharpSensor.IACSharpSensor.SensorReached(250);
      return RNTRNTRNT_91;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(251);
      name = value;
      IACSharpSensor.IACSharpSensor.SensorReached(252);
    }
  }
  public bool IsRequired {
    get {
      System.Boolean RNTRNTRNT_92 = isRequired;
      IACSharpSensor.IACSharpSensor.SensorReached(253);
      return RNTRNTRNT_92;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(254);
      isRequired = value;
      IACSharpSensor.IACSharpSensor.SensorReached(255);
    }
  }
}
public class t
{
  [XmlMemberArrayAttribute("shortcut", Separator = new char[] { '|' })]
  string[] shortcut;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(256);
  }
}
