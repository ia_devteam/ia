unsafe class X
{
  static int x = 0;
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1056);
    fixed (void* p = &x) {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1057);
    fixed (void* p = &x) {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1058);
  }
}
