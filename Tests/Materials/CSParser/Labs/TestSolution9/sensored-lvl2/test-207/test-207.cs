using System;
delegate void Test(int test);
class X
{
  static int result = 0;
  public static void hello(int arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    result += arg;
    IACSharpSensor.IACSharpSensor.SensorReached(54);
  }
  public static void world(int arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(55);
    result += 16 * arg;
    IACSharpSensor.IACSharpSensor.SensorReached(56);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(57);
    Test a = new Test(hello);
    Test b = new Test(world);
    (a + b)(1);
    IACSharpSensor.IACSharpSensor.SensorReached(58);
    if (result != 17) {
      System.Int32 RNTRNTRNT_23 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(59);
      return RNTRNTRNT_23;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(60);
    ((result == 17) ? a : b)(2);
    IACSharpSensor.IACSharpSensor.SensorReached(61);
    if (result != 19) {
      System.Int32 RNTRNTRNT_24 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(62);
      return RNTRNTRNT_24;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(63);
    ((result == 17) ? a : b)(2);
    IACSharpSensor.IACSharpSensor.SensorReached(64);
    if (result != 51) {
      System.Int32 RNTRNTRNT_25 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(65);
      return RNTRNTRNT_25;
    }
    System.Int32 RNTRNTRNT_26 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(66);
    return RNTRNTRNT_26;
  }
}
