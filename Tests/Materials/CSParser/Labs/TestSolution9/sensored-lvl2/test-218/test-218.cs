class T
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(151);
  }
  delegate void foo(object o);
  static foo[] f = { new foo(T.compareQueryQuery1) };
  static void compareQueryQuery1(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(152);
  }
}
