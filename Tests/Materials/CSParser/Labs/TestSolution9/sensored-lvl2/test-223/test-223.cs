enum Foo
{
  Bar
}
class T
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(236);
    System.Enum e = Foo.Bar;
    System.ValueType vt1 = Foo.Bar, vt2 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(237);
    if (((Foo)e) != Foo.Bar) {
      System.Int32 RNTRNTRNT_86 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(238);
      return RNTRNTRNT_86;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(239);
    if (((Foo)vt1) != Foo.Bar) {
      System.Int32 RNTRNTRNT_87 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(240);
      return RNTRNTRNT_87;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(241);
    if (((int)vt2) != 1) {
      System.Int32 RNTRNTRNT_88 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(242);
      return RNTRNTRNT_88;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(243);
    System.ValueType vt = null;
    System.Int32 RNTRNTRNT_89 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(244);
    return RNTRNTRNT_89;
  }
}
