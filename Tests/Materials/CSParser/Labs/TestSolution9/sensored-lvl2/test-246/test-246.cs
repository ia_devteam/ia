using System;
struct Blah : System.IDisposable
{
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(766);
    Console.WriteLine("foo");
    IACSharpSensor.IACSharpSensor.SensorReached(767);
  }
}
class B
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(768);
    using (Blah b = new Blah()) {
      IACSharpSensor.IACSharpSensor.SensorReached(769);
      Console.WriteLine("...");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(770);
  }
}
