using System;
public class FooAttribute : Attribute
{
  public char[] Separator;
}
[Foo(Separator = new char[] { 'A' })]
public class Tests
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(945);
    FooAttribute foo = (FooAttribute)(typeof(Tests).GetCustomAttributes(false)[0]);
    Console.WriteLine(foo.Separator);
    IACSharpSensor.IACSharpSensor.SensorReached(946);
  }
}
