using System;
public class Foo
{
  protected virtual int SomeProperty {
    get {
      System.Int32 RNTRNTRNT_212 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(729);
      return RNTRNTRNT_212;
    }
  }
  protected virtual int M()
  {
    System.Int32 RNTRNTRNT_213 = 10;
    IACSharpSensor.IACSharpSensor.SensorReached(730);
    return RNTRNTRNT_213;
  }
  private class FooPrivate : Foo
  {
    Foo _realFoo;
    internal FooPrivate(Foo f)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(731);
      _realFoo = f;
      IACSharpSensor.IACSharpSensor.SensorReached(732);
    }
    protected override int SomeProperty {
      get {
        System.Int32 RNTRNTRNT_214 = this._realFoo.SomeProperty + _realFoo.M();
        IACSharpSensor.IACSharpSensor.SensorReached(733);
        return RNTRNTRNT_214;
      }
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(734);
  }
}
