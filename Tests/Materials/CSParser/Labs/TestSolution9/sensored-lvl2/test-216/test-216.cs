using System;
public delegate void OnWhateverDelegate(string s);
class cls
{
  public event OnWhateverDelegate OnWhatever;
  class nestedcls
  {
    internal void CallParentDel(cls c, string s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(122);
      c.OnWhatever(s);
      IACSharpSensor.IACSharpSensor.SensorReached(123);
    }
  }
  internal void CallMyDel(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(124);
    (new nestedcls()).CallParentDel(this, s);
    IACSharpSensor.IACSharpSensor.SensorReached(125);
  }
}
class MonoEmbed
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(126);
    cls c = new cls();
    c.OnWhatever += new OnWhateverDelegate(Whatever);
    c.CallMyDel("test");
    IACSharpSensor.IACSharpSensor.SensorReached(127);
  }
  static void Whatever(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(128);
    Console.WriteLine(s);
    IACSharpSensor.IACSharpSensor.SensorReached(129);
  }
}
