using System;
struct A
{
  public readonly int i;
  public A(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(71);
    this.i = i;
    IACSharpSensor.IACSharpSensor.SensorReached(72);
  }
}
class X
{
  int i;
  public int Foo {
    get {
      System.Int32 RNTRNTRNT_28 = 2 * i;
      IACSharpSensor.IACSharpSensor.SensorReached(73);
      return RNTRNTRNT_28;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(74);
      i = value;
      IACSharpSensor.IACSharpSensor.SensorReached(75);
    }
  }
  public int this[int a] {
    get {
      System.Int32 RNTRNTRNT_29 = (int)Foo;
      IACSharpSensor.IACSharpSensor.SensorReached(76);
      return RNTRNTRNT_29;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(77);
      Foo = a;
      IACSharpSensor.IACSharpSensor.SensorReached(78);
    }
  }
  public string this[string a] {
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(79);
      Console.WriteLine(a);
      IACSharpSensor.IACSharpSensor.SensorReached(80);
    }
  }
  public string Bar {
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(81);
      Console.WriteLine(value);
      IACSharpSensor.IACSharpSensor.SensorReached(82);
    }
  }
  public A A {
    get {
      A RNTRNTRNT_30 = new A(5);
      IACSharpSensor.IACSharpSensor.SensorReached(83);
      return RNTRNTRNT_30;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(84);
      Console.WriteLine(value);
      IACSharpSensor.IACSharpSensor.SensorReached(85);
    }
  }
  public X(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(86);
    this.i = i;
    IACSharpSensor.IACSharpSensor.SensorReached(87);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(88);
    X x = new X(9);
    int a = x.Foo = 16;
    int b = x[8] = 32;
    x["Test"] = "Hello";
    x.Bar = "World";
    x.A = new A(9);
    System.Int32 RNTRNTRNT_31 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(89);
    return RNTRNTRNT_31;
  }
}
