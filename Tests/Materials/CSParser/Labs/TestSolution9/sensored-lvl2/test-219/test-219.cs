using System;
public class TestAttribute : Attribute
{
  Type type;
  public TestAttribute(Type type)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(153);
    this.type = type;
    IACSharpSensor.IACSharpSensor.SensorReached(154);
  }
  public Type Type {
    get {
      Type RNTRNTRNT_51 = type;
      IACSharpSensor.IACSharpSensor.SensorReached(155);
      return RNTRNTRNT_51;
    }
  }
}
[TestAttribute(typeof(void))]
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(156);
    object[] attrs = typeof(Test).GetCustomAttributes(typeof(TestAttribute), false);
    IACSharpSensor.IACSharpSensor.SensorReached(157);
    foreach (TestAttribute attr in attrs) {
      IACSharpSensor.IACSharpSensor.SensorReached(158);
      Console.WriteLine("TestAttribute({0})", attr.Type);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(159);
  }
}
