using System;
class X
{
  static int Test(string format, params object[] args)
  {
    System.Int32 RNTRNTRNT_271 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(925);
    return RNTRNTRNT_271;
  }
  static int Test(string format, __arglist __arglist)
  {
    System.Int32 RNTRNTRNT_272 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(926);
    return RNTRNTRNT_272;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(927);
    if (Test("Hello", 1, 2, "World") != 1) {
      System.Int32 RNTRNTRNT_273 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(928);
      return RNTRNTRNT_273;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(929);
    if (Test("Hello", __arglist("Boston")) != 2) {
      System.Int32 RNTRNTRNT_274 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(930);
      return RNTRNTRNT_274;
    }
    System.Int32 RNTRNTRNT_275 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(931);
    return RNTRNTRNT_275;
  }
}
