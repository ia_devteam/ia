namespace Foo
{
  public class Hello
  {
    public static int World = 8;
  }
}
namespace Bar
{
  public class Hello
  {
    public static int World = 9;
  }
}
namespace Test
{
  using Foo;
  public class Test1
  {
    public static int World()
    {
      System.Int32 RNTRNTRNT_293 = Hello.World;
      IACSharpSensor.IACSharpSensor.SensorReached(983);
      return RNTRNTRNT_293;
    }
  }
}
namespace Test
{
  using Bar;
  public class Test2
  {
    public static int World()
    {
      System.Int32 RNTRNTRNT_294 = Hello.World;
      IACSharpSensor.IACSharpSensor.SensorReached(984);
      return RNTRNTRNT_294;
    }
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(985);
    if (Test.Test1.World() != 8) {
      System.Int32 RNTRNTRNT_295 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(986);
      return RNTRNTRNT_295;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(987);
    if (Test.Test2.World() != 9) {
      System.Int32 RNTRNTRNT_296 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(988);
      return RNTRNTRNT_296;
    }
    System.Int32 RNTRNTRNT_297 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(989);
    return RNTRNTRNT_297;
  }
}
