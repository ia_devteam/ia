using System;
using System.Reflection;
[AttributeUsage(AttributeTargets.Field)]
public class AccessibleAttribute : Attribute
{
}
public class MyClass
{
  [Accessible()]
  public const int MyConst = 1;
}
public class Test
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(783);
    FieldInfo fieldInfo = typeof(MyClass).GetField("MyConst", BindingFlags.Static | BindingFlags.Public);
    AccessibleAttribute[] attributes = fieldInfo.GetCustomAttributes(typeof(AccessibleAttribute), true) as AccessibleAttribute[];
    IACSharpSensor.IACSharpSensor.SensorReached(784);
    if (attributes != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(785);
      Console.WriteLine("Succeeded");
      System.Int32 RNTRNTRNT_234 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(786);
      return RNTRNTRNT_234;
    }
    System.Int32 RNTRNTRNT_235 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(787);
    return RNTRNTRNT_235;
  }
}
