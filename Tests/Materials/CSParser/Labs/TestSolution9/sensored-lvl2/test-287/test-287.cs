using System;
using System.Reflection;
static class StaticClass
{
  const int Foo = 1;
  delegate object D();
  enum E
  {
  }
  public static string Name()
  {
    System.String RNTRNTRNT_313 = "OK";
    IACSharpSensor.IACSharpSensor.SensorReached(1026);
    return RNTRNTRNT_313;
  }
}
public class MainClass
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1027);
    Type type = typeof(StaticClass);
    IACSharpSensor.IACSharpSensor.SensorReached(1028);
    if (!type.IsAbstract || !type.IsSealed) {
      IACSharpSensor.IACSharpSensor.SensorReached(1029);
      Console.WriteLine("Is not abstract sealed");
      System.Int32 RNTRNTRNT_314 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1030);
      return RNTRNTRNT_314;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1031);
    if (type.GetConstructors().Length > 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(1032);
      Console.WriteLine("Has constructor");
      System.Int32 RNTRNTRNT_315 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1033);
      return RNTRNTRNT_315;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1034);
    Console.WriteLine(StaticClass.Name());
    System.Int32 RNTRNTRNT_316 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1035);
    return RNTRNTRNT_316;
  }
}
