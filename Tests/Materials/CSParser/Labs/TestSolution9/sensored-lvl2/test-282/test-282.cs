using System;
public struct integer
{
  private readonly int value;
  public integer(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(990);
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(991);
  }
  public static implicit operator integer(int i)
  {
    integer RNTRNTRNT_298 = new integer(i);
    IACSharpSensor.IACSharpSensor.SensorReached(992);
    return RNTRNTRNT_298;
  }
  public static implicit operator double(integer i)
  {
    System.Double RNTRNTRNT_299 = Convert.ToDouble(i.value);
    IACSharpSensor.IACSharpSensor.SensorReached(993);
    return RNTRNTRNT_299;
  }
  public static integer operator +(integer x, integer y)
  {
    integer RNTRNTRNT_300 = new integer(x.value + y.value);
    IACSharpSensor.IACSharpSensor.SensorReached(994);
    return RNTRNTRNT_300;
  }
}
class X
{
  public static object Add(integer x, object other)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(995);
    if (other is int) {
      System.Object RNTRNTRNT_301 = x + ((int)other);
      IACSharpSensor.IACSharpSensor.SensorReached(996);
      return RNTRNTRNT_301;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(997);
    if (other is double) {
      System.Object RNTRNTRNT_302 = x + ((double)other);
      IACSharpSensor.IACSharpSensor.SensorReached(998);
      return RNTRNTRNT_302;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(999);
    throw new InvalidOperationException();
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1000);
    integer i = new integer(3);
    double d = 4.0;
    object result = Add(i, d);
    IACSharpSensor.IACSharpSensor.SensorReached(1001);
    if (!(result is double)) {
      System.Int32 RNTRNTRNT_303 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1002);
      return RNTRNTRNT_303;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1003);
    if ((double)result != 7.0) {
      System.Int32 RNTRNTRNT_304 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1004);
      return RNTRNTRNT_304;
    }
    System.Int32 RNTRNTRNT_305 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1005);
    return RNTRNTRNT_305;
  }
}
