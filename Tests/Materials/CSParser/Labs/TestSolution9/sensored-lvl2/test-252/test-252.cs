delegate void Foo();
class A
{
  public event Foo Bar;
  public static void m1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(780);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(781);
    A a = new A();
    a.Bar += new Foo(m1);
    a.Bar -= new Foo(m1);
    System.Int32 RNTRNTRNT_233 = (a.Bar == null) ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(782);
    return RNTRNTRNT_233;
  }
}
