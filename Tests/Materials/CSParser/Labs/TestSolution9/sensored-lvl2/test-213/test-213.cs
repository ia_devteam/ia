using System;
class MyTest
{
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(110);
    S s1 = new S(11);
    I s2 = s1;
    S s3 = (S)s2;
    s3.Print();
    IACSharpSensor.IACSharpSensor.SensorReached(111);
  }
}
interface I
{
  void Print();
}
struct S : I
{
  public int i;
  public S(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(112);
    this.i = i;
    IACSharpSensor.IACSharpSensor.SensorReached(113);
  }
  public void Print()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(114);
    Console.WriteLine(i);
    IACSharpSensor.IACSharpSensor.SensorReached(115);
  }
}
