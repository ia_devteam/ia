class X
{
  public readonly int value;
  public X(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(93);
    this.value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(94);
  }
  public static implicit operator X(int y)
  {
    X RNTRNTRNT_32 = new X(y);
    IACSharpSensor.IACSharpSensor.SensorReached(95);
    return RNTRNTRNT_32;
  }
}
class Y
{
  public readonly X x;
  public Y(X x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(96);
    this.x = x;
    IACSharpSensor.IACSharpSensor.SensorReached(97);
  }
  public static implicit operator Y(X x)
  {
    Y RNTRNTRNT_33 = new Y(x);
    IACSharpSensor.IACSharpSensor.SensorReached(98);
    return RNTRNTRNT_33;
  }
}
class Z
{
  public readonly Y y;
  public Z(Y y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(99);
    this.y = y;
    IACSharpSensor.IACSharpSensor.SensorReached(100);
  }
  public static implicit operator Z(Y y)
  {
    Z RNTRNTRNT_34 = new Z(y);
    IACSharpSensor.IACSharpSensor.SensorReached(101);
    return RNTRNTRNT_34;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(102);
    int a = 5;
    Y y = (Y)(X)a;
    int b = (System.Int32)int.Parse("1");
    System.Int32 RNTRNTRNT_35 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(103);
    return RNTRNTRNT_35;
  }
}
