using System;
public class A
{
  [Obsolete()]
  public virtual string Warning {
    get {
      System.String RNTRNTRNT_321 = "";
      IACSharpSensor.IACSharpSensor.SensorReached(1059);
      return RNTRNTRNT_321;
    }
  }
}
public class B : A
{
  [Obsolete()]
  public override string Warning {
    get {
      System.String RNTRNTRNT_322 = "";
      IACSharpSensor.IACSharpSensor.SensorReached(1060);
      return RNTRNTRNT_322;
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1061);
  }
}
