using System;
class X
{
  public readonly int x;
  public X(int x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(25);
    this.x = x;
    IACSharpSensor.IACSharpSensor.SensorReached(26);
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_10 = String.Format("X ({0})", x);
    IACSharpSensor.IACSharpSensor.SensorReached(27);
    return RNTRNTRNT_10;
  }
  public static X operator &(X a, X b)
  {
    X RNTRNTRNT_11 = new X(a.x * b.x);
    IACSharpSensor.IACSharpSensor.SensorReached(28);
    return RNTRNTRNT_11;
  }
  public static X operator |(X a, X b)
  {
    X RNTRNTRNT_12 = new X(a.x + b.x);
    IACSharpSensor.IACSharpSensor.SensorReached(29);
    return RNTRNTRNT_12;
  }
  public static bool operator true(X x)
  {
    System.Boolean RNTRNTRNT_13 = (x.x % 2) != 0;
    IACSharpSensor.IACSharpSensor.SensorReached(30);
    return RNTRNTRNT_13;
  }
  public static bool operator false(X x)
  {
    System.Boolean RNTRNTRNT_14 = (x.x % 2) == 0;
    IACSharpSensor.IACSharpSensor.SensorReached(31);
    return RNTRNTRNT_14;
  }
  public static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(32);
    X x = new X(3);
    X y = new X(4);
    X t1 = x && y;
    X t2 = y && x;
    X t3 = x || y;
    X t4 = y || x;
    IACSharpSensor.IACSharpSensor.SensorReached(33);
    if (t1.x != 12) {
      System.Int32 RNTRNTRNT_15 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(34);
      return RNTRNTRNT_15;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(35);
    if (t2.x != 4) {
      System.Int32 RNTRNTRNT_16 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(36);
      return RNTRNTRNT_16;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(37);
    if (t3.x != 3) {
      System.Int32 RNTRNTRNT_17 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(38);
      return RNTRNTRNT_17;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(39);
    if (t4.x != 7) {
      System.Int32 RNTRNTRNT_18 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(40);
      return RNTRNTRNT_18;
    }
    System.Int32 RNTRNTRNT_19 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(41);
    return RNTRNTRNT_19;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    int result = Test();
    Console.WriteLine("RESULT: {0}", result);
    System.Int32 RNTRNTRNT_20 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(43);
    return RNTRNTRNT_20;
  }
}
