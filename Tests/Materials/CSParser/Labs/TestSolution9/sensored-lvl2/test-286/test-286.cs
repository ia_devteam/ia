abstract class MethodWrapper
{
  private string[] declaredExceptions;
  internal void SetDeclaredExceptions(MapXml.Throws[] throws)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1020);
    if (throws != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(1021);
      declaredExceptions = new string[throws.Length];
      IACSharpSensor.IACSharpSensor.SensorReached(1022);
      for (int i = 0; i < throws.Length; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1023);
        declaredExceptions[i] = throws[i].Class;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1024);
  }
}
namespace MapXml
{
  using System;
  using System.Xml.Serialization;
  public class Throws
  {
    [XmlAttribute("class")]
    public string Class;
    public static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1025);
    }
  }
}
