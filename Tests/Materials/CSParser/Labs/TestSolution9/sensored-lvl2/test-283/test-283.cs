class X
{
  public virtual int Foo()
  {
    System.Int32 RNTRNTRNT_306 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1006);
    return RNTRNTRNT_306;
  }
}
class Y : X
{
  delegate int D();
  D GetIt()
  {
    D RNTRNTRNT_307 = new D(base.Foo);
    IACSharpSensor.IACSharpSensor.SensorReached(1007);
    return RNTRNTRNT_307;
  }
  D GetIt2()
  {
    D RNTRNTRNT_308 = base.Foo;
    IACSharpSensor.IACSharpSensor.SensorReached(1008);
    return RNTRNTRNT_308;
  }
  public override int Foo()
  {
    System.Int32 RNTRNTRNT_309 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1009);
    return RNTRNTRNT_309;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1010);
    if (new Y().GetIt()() == 1 && new Y().GetIt2()() == 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(1011);
      System.Console.WriteLine("good");
      System.Int32 RNTRNTRNT_310 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1012);
      return RNTRNTRNT_310;
    }
    System.Int32 RNTRNTRNT_311 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1013);
    return RNTRNTRNT_311;
  }
}
