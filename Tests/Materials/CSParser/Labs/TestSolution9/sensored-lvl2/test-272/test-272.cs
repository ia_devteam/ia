enum Foo
{
  Bar
}
class BazAttribute : System.Attribute
{
  public BazAttribute()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(936);
  }
  public BazAttribute(Foo foo1)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(937);
  }
  public Foo foo2;
  public Foo foo3 {
    get {
      Foo RNTRNTRNT_278 = Foo.Bar;
      IACSharpSensor.IACSharpSensor.SensorReached(938);
      return RNTRNTRNT_278;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(939); }
  }
}
class Test
{
  [Baz(Foo.Bar)]
  void f0()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(940);
  }
  [Baz((Foo)1)]
  void f1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(941);
  }
  [Baz(foo2 = (Foo)2)]
  void f2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(942);
  }
  [Baz(foo3 = (Foo)3)]
  void f3()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(943);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(944);
  }
}
