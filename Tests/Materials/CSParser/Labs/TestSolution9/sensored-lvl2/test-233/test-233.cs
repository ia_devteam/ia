using System;
using System.Reflection;
class Tests
{
  static int Main()
  {
    System.Int32 RNTRNTRNT_113 = TestDriver.RunTests(typeof(Tests));
    IACSharpSensor.IACSharpSensor.SensorReached(343);
    return RNTRNTRNT_113;
  }
  static int test_0_beq()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(344);
    double a = 2.0;
    IACSharpSensor.IACSharpSensor.SensorReached(345);
    if (a != 2.0) {
      System.Int32 RNTRNTRNT_114 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(346);
      return RNTRNTRNT_114;
    }
    System.Int32 RNTRNTRNT_115 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(347);
    return RNTRNTRNT_115;
  }
  static int test_0_bne_un()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(348);
    double a = 2.0;
    IACSharpSensor.IACSharpSensor.SensorReached(349);
    if (a == 1.0) {
      System.Int32 RNTRNTRNT_116 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(350);
      return RNTRNTRNT_116;
    }
    System.Int32 RNTRNTRNT_117 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(351);
    return RNTRNTRNT_117;
  }
  static int test_0_conv_r8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(352);
    double a = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(353);
    if (a != 2.0) {
      System.Int32 RNTRNTRNT_118 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(354);
      return RNTRNTRNT_118;
    }
    System.Int32 RNTRNTRNT_119 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(355);
    return RNTRNTRNT_119;
  }
  static int test_0_conv_i()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(356);
    double a = 2.0;
    int i = (int)a;
    IACSharpSensor.IACSharpSensor.SensorReached(357);
    if (i != 2) {
      System.Int32 RNTRNTRNT_120 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(358);
      return RNTRNTRNT_120;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(359);
    uint ui = (uint)a;
    IACSharpSensor.IACSharpSensor.SensorReached(360);
    if (ui != 2) {
      System.Int32 RNTRNTRNT_121 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(361);
      return RNTRNTRNT_121;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(362);
    short s = (short)a;
    IACSharpSensor.IACSharpSensor.SensorReached(363);
    if (s != 2) {
      System.Int32 RNTRNTRNT_122 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(364);
      return RNTRNTRNT_122;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(365);
    ushort us = (ushort)a;
    IACSharpSensor.IACSharpSensor.SensorReached(366);
    if (us != 2) {
      System.Int32 RNTRNTRNT_123 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(367);
      return RNTRNTRNT_123;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    byte b = (byte)a;
    IACSharpSensor.IACSharpSensor.SensorReached(369);
    if (b != 2) {
      System.Int32 RNTRNTRNT_124 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(370);
      return RNTRNTRNT_124;
    }
    System.Int32 RNTRNTRNT_125 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(371);
    return RNTRNTRNT_125;
  }
  static int test_5_conv_r4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(372);
    int i = 5;
    float f = (float)i;
    System.Int32 RNTRNTRNT_126 = (int)f;
    IACSharpSensor.IACSharpSensor.SensorReached(373);
    return RNTRNTRNT_126;
  }
  static int test_5_double_conv_r4()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(374);
    double d = 5.0;
    float f = (float)d;
    System.Int32 RNTRNTRNT_127 = (int)f;
    IACSharpSensor.IACSharpSensor.SensorReached(375);
    return RNTRNTRNT_127;
  }
  static int test_5_float_conv_r8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(376);
    float f = 5.0f;
    double d = (double)f;
    System.Int32 RNTRNTRNT_128 = (int)d;
    IACSharpSensor.IACSharpSensor.SensorReached(377);
    return RNTRNTRNT_128;
  }
  static int test_5_conv_r8()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(378);
    int i = 5;
    double f = (double)i;
    System.Int32 RNTRNTRNT_129 = (int)f;
    IACSharpSensor.IACSharpSensor.SensorReached(379);
    return RNTRNTRNT_129;
  }
  static int test_5_add()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(380);
    double a = 2.0;
    double b = 3.0;
    System.Int32 RNTRNTRNT_130 = (int)(a + b);
    IACSharpSensor.IACSharpSensor.SensorReached(381);
    return RNTRNTRNT_130;
  }
  static int test_5_sub()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(382);
    double a = 8.0;
    double b = 3.0;
    System.Int32 RNTRNTRNT_131 = (int)(a - b);
    IACSharpSensor.IACSharpSensor.SensorReached(383);
    return RNTRNTRNT_131;
  }
  static int test_24_mul()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(384);
    double a = 8.0;
    double b = 3.0;
    System.Int32 RNTRNTRNT_132 = (int)(a * b);
    IACSharpSensor.IACSharpSensor.SensorReached(385);
    return RNTRNTRNT_132;
  }
  static int test_4_div()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(386);
    double a = 8.0;
    double b = 2.0;
    System.Int32 RNTRNTRNT_133 = (int)(a / b);
    IACSharpSensor.IACSharpSensor.SensorReached(387);
    return RNTRNTRNT_133;
  }
  static int test_2_rem()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(388);
    double a = 8.0;
    double b = 3.0;
    System.Int32 RNTRNTRNT_134 = (int)(a % b);
    IACSharpSensor.IACSharpSensor.SensorReached(389);
    return RNTRNTRNT_134;
  }
  static int test_2_neg()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(390);
    double a = -2.0;
    System.Int32 RNTRNTRNT_135 = (int)(-a);
    IACSharpSensor.IACSharpSensor.SensorReached(391);
    return RNTRNTRNT_135;
  }
  static int test_46_float_add_spill()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(392);
    double a = 1;
    double b = 2;
    double c = 3;
    double d = 4;
    double e = 5;
    double f = 6;
    double g = 7;
    double h = 8;
    double i = 9;
    System.Int32 RNTRNTRNT_136 = (int)(1.0 + (a + (b + (c + (d + (e + (f + (g + (h + i)))))))));
    IACSharpSensor.IACSharpSensor.SensorReached(393);
    return RNTRNTRNT_136;
  }
  static int test_362880_float_mul_spill()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(394);
    double a = 1;
    double b = 2;
    double c = 3;
    double d = 4;
    double e = 5;
    double f = 6;
    double g = 7;
    double h = 8;
    double i = 9;
    System.Int32 RNTRNTRNT_137 = (int)(1.0 * (a * (b * (c * (d * (e * (f * (g * (h * i)))))))));
    IACSharpSensor.IACSharpSensor.SensorReached(395);
    return RNTRNTRNT_137;
  }
  static int test_4_long_cast()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(396);
    long a = 1000;
    double d = (double)a;
    long b = (long)d;
    IACSharpSensor.IACSharpSensor.SensorReached(397);
    if (b != 1000) {
      System.Int32 RNTRNTRNT_138 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(398);
      return RNTRNTRNT_138;
    }
    System.Int32 RNTRNTRNT_139 = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(399);
    return RNTRNTRNT_139;
  }
  static int test_16_float_cmp()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(400);
    double a = 2.0;
    double b = 1.0;
    int result = 0;
    bool val;
    val = a == a;
    IACSharpSensor.IACSharpSensor.SensorReached(401);
    if (!val) {
      System.Int32 RNTRNTRNT_140 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(402);
      return RNTRNTRNT_140;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(403);
    result++;
    val = (a != a);
    IACSharpSensor.IACSharpSensor.SensorReached(404);
    if (val) {
      System.Int32 RNTRNTRNT_141 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(405);
      return RNTRNTRNT_141;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(406);
    result++;
    val = a < a;
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    if (val) {
      System.Int32 RNTRNTRNT_142 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(408);
      return RNTRNTRNT_142;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(409);
    result++;
    val = a > a;
    IACSharpSensor.IACSharpSensor.SensorReached(410);
    if (val) {
      System.Int32 RNTRNTRNT_143 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(411);
      return RNTRNTRNT_143;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(412);
    result++;
    val = a <= a;
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    if (!val) {
      System.Int32 RNTRNTRNT_144 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(414);
      return RNTRNTRNT_144;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(415);
    result++;
    val = a >= a;
    IACSharpSensor.IACSharpSensor.SensorReached(416);
    if (!val) {
      System.Int32 RNTRNTRNT_145 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(417);
      return RNTRNTRNT_145;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(418);
    result++;
    val = b == a;
    IACSharpSensor.IACSharpSensor.SensorReached(419);
    if (val) {
      System.Int32 RNTRNTRNT_146 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(420);
      return RNTRNTRNT_146;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(421);
    result++;
    val = b < a;
    IACSharpSensor.IACSharpSensor.SensorReached(422);
    if (!val) {
      System.Int32 RNTRNTRNT_147 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(423);
      return RNTRNTRNT_147;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(424);
    result++;
    val = b > a;
    IACSharpSensor.IACSharpSensor.SensorReached(425);
    if (val) {
      System.Int32 RNTRNTRNT_148 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(426);
      return RNTRNTRNT_148;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(427);
    result++;
    val = b <= a;
    IACSharpSensor.IACSharpSensor.SensorReached(428);
    if (!val) {
      System.Int32 RNTRNTRNT_149 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(429);
      return RNTRNTRNT_149;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(430);
    result++;
    val = b >= a;
    IACSharpSensor.IACSharpSensor.SensorReached(431);
    if (val) {
      System.Int32 RNTRNTRNT_150 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(432);
      return RNTRNTRNT_150;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(433);
    result++;
    val = a == b;
    IACSharpSensor.IACSharpSensor.SensorReached(434);
    if (val) {
      System.Int32 RNTRNTRNT_151 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(435);
      return RNTRNTRNT_151;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(436);
    result++;
    val = a < b;
    IACSharpSensor.IACSharpSensor.SensorReached(437);
    if (val) {
      System.Int32 RNTRNTRNT_152 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(438);
      return RNTRNTRNT_152;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(439);
    result++;
    val = a > b;
    IACSharpSensor.IACSharpSensor.SensorReached(440);
    if (!val) {
      System.Int32 RNTRNTRNT_153 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(441);
      return RNTRNTRNT_153;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(442);
    result++;
    val = a <= b;
    IACSharpSensor.IACSharpSensor.SensorReached(443);
    if (val) {
      System.Int32 RNTRNTRNT_154 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(444);
      return RNTRNTRNT_154;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(445);
    result++;
    val = a >= b;
    IACSharpSensor.IACSharpSensor.SensorReached(446);
    if (!val) {
      System.Int32 RNTRNTRNT_155 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(447);
      return RNTRNTRNT_155;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(448);
    result++;
    System.Int32 RNTRNTRNT_156 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(449);
    return RNTRNTRNT_156;
  }
  static int test_15_float_cmp_un()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(450);
    double a = Double.NaN;
    double b = 1.0;
    int result = 0;
    bool val;
    val = a == a;
    IACSharpSensor.IACSharpSensor.SensorReached(451);
    if (val) {
      System.Int32 RNTRNTRNT_157 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(452);
      return RNTRNTRNT_157;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(453);
    result++;
    val = a < a;
    IACSharpSensor.IACSharpSensor.SensorReached(454);
    if (val) {
      System.Int32 RNTRNTRNT_158 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(455);
      return RNTRNTRNT_158;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(456);
    result++;
    val = a > a;
    IACSharpSensor.IACSharpSensor.SensorReached(457);
    if (val) {
      System.Int32 RNTRNTRNT_159 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(458);
      return RNTRNTRNT_159;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(459);
    result++;
    val = a <= a;
    IACSharpSensor.IACSharpSensor.SensorReached(460);
    if (val) {
      System.Int32 RNTRNTRNT_160 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(461);
      return RNTRNTRNT_160;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(462);
    result++;
    val = a >= a;
    IACSharpSensor.IACSharpSensor.SensorReached(463);
    if (val) {
      System.Int32 RNTRNTRNT_161 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(464);
      return RNTRNTRNT_161;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(465);
    result++;
    val = b == a;
    IACSharpSensor.IACSharpSensor.SensorReached(466);
    if (val) {
      System.Int32 RNTRNTRNT_162 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(467);
      return RNTRNTRNT_162;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(468);
    result++;
    val = b < a;
    IACSharpSensor.IACSharpSensor.SensorReached(469);
    if (val) {
      System.Int32 RNTRNTRNT_163 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(470);
      return RNTRNTRNT_163;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(471);
    result++;
    val = b > a;
    IACSharpSensor.IACSharpSensor.SensorReached(472);
    if (val) {
      System.Int32 RNTRNTRNT_164 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(473);
      return RNTRNTRNT_164;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(474);
    result++;
    val = b <= a;
    IACSharpSensor.IACSharpSensor.SensorReached(475);
    if (val) {
      System.Int32 RNTRNTRNT_165 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(476);
      return RNTRNTRNT_165;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(477);
    result++;
    val = b >= a;
    IACSharpSensor.IACSharpSensor.SensorReached(478);
    if (val) {
      System.Int32 RNTRNTRNT_166 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(479);
      return RNTRNTRNT_166;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(480);
    result++;
    val = a == b;
    IACSharpSensor.IACSharpSensor.SensorReached(481);
    if (val) {
      System.Int32 RNTRNTRNT_167 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(482);
      return RNTRNTRNT_167;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(483);
    result++;
    val = a < b;
    IACSharpSensor.IACSharpSensor.SensorReached(484);
    if (val) {
      System.Int32 RNTRNTRNT_168 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(485);
      return RNTRNTRNT_168;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(486);
    result++;
    val = a > b;
    IACSharpSensor.IACSharpSensor.SensorReached(487);
    if (val) {
      System.Int32 RNTRNTRNT_169 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(488);
      return RNTRNTRNT_169;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(489);
    result++;
    val = a <= b;
    IACSharpSensor.IACSharpSensor.SensorReached(490);
    if (val) {
      System.Int32 RNTRNTRNT_170 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(491);
      return RNTRNTRNT_170;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(492);
    result++;
    val = a >= b;
    IACSharpSensor.IACSharpSensor.SensorReached(493);
    if (val) {
      System.Int32 RNTRNTRNT_171 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(494);
      return RNTRNTRNT_171;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(495);
    result++;
    System.Int32 RNTRNTRNT_172 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(496);
    return RNTRNTRNT_172;
  }
  static int test_15_float_branch()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(497);
    double a = 2.0;
    double b = 1.0;
    int result = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(498);
    if (!(a == a)) {
      System.Int32 RNTRNTRNT_173 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(499);
      return RNTRNTRNT_173;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(500);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(501);
    if (a < a) {
      System.Int32 RNTRNTRNT_174 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(502);
      return RNTRNTRNT_174;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(503);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(504);
    if (a > a) {
      System.Int32 RNTRNTRNT_175 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(505);
      return RNTRNTRNT_175;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(506);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(507);
    if (!(a <= a)) {
      System.Int32 RNTRNTRNT_176 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(508);
      return RNTRNTRNT_176;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(509);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(510);
    if (!(a >= a)) {
      System.Int32 RNTRNTRNT_177 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(511);
      return RNTRNTRNT_177;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(512);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(513);
    if (b == a) {
      System.Int32 RNTRNTRNT_178 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(514);
      return RNTRNTRNT_178;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(515);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(516);
    if (!(b < a)) {
      System.Int32 RNTRNTRNT_179 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(517);
      return RNTRNTRNT_179;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(518);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(519);
    if (b > a) {
      System.Int32 RNTRNTRNT_180 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(520);
      return RNTRNTRNT_180;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(521);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(522);
    if (!(b <= a)) {
      System.Int32 RNTRNTRNT_181 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(523);
      return RNTRNTRNT_181;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(524);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(525);
    if (b >= a) {
      System.Int32 RNTRNTRNT_182 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(526);
      return RNTRNTRNT_182;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(527);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(528);
    if (a == b) {
      System.Int32 RNTRNTRNT_183 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(529);
      return RNTRNTRNT_183;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(530);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(531);
    if (a < b) {
      System.Int32 RNTRNTRNT_184 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(532);
      return RNTRNTRNT_184;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(533);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(534);
    if (!(a > b)) {
      System.Int32 RNTRNTRNT_185 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(535);
      return RNTRNTRNT_185;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(536);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(537);
    if (a <= b) {
      System.Int32 RNTRNTRNT_186 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(538);
      return RNTRNTRNT_186;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(539);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(540);
    if (!(a >= b)) {
      System.Int32 RNTRNTRNT_187 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(541);
      return RNTRNTRNT_187;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(542);
    result++;
    System.Int32 RNTRNTRNT_188 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(543);
    return RNTRNTRNT_188;
  }
  static int test_15_float_branch_un()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(544);
    double a = Double.NaN;
    double b = 1.0;
    int result = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(545);
    if (a == a) {
      System.Int32 RNTRNTRNT_189 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(546);
      return RNTRNTRNT_189;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(547);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(548);
    if (a < a) {
      System.Int32 RNTRNTRNT_190 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(549);
      return RNTRNTRNT_190;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(550);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(551);
    if (a > a) {
      System.Int32 RNTRNTRNT_191 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(552);
      return RNTRNTRNT_191;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(553);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(554);
    if (a <= a) {
      System.Int32 RNTRNTRNT_192 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(555);
      return RNTRNTRNT_192;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(556);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(557);
    if (a >= a) {
      System.Int32 RNTRNTRNT_193 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(558);
      return RNTRNTRNT_193;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(559);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(560);
    if (b == a) {
      System.Int32 RNTRNTRNT_194 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(561);
      return RNTRNTRNT_194;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(562);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(563);
    if (b < a) {
      System.Int32 RNTRNTRNT_195 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(564);
      return RNTRNTRNT_195;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(565);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(566);
    if (b > a) {
      System.Int32 RNTRNTRNT_196 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(567);
      return RNTRNTRNT_196;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(568);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(569);
    if (b <= a) {
      System.Int32 RNTRNTRNT_197 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(570);
      return RNTRNTRNT_197;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(571);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(572);
    if (b >= a) {
      System.Int32 RNTRNTRNT_198 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(573);
      return RNTRNTRNT_198;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(574);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(575);
    if (a == b) {
      System.Int32 RNTRNTRNT_199 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(576);
      return RNTRNTRNT_199;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(577);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(578);
    if (a < b) {
      System.Int32 RNTRNTRNT_200 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(579);
      return RNTRNTRNT_200;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(580);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(581);
    if (a > b) {
      System.Int32 RNTRNTRNT_201 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(582);
      return RNTRNTRNT_201;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(583);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(584);
    if (a <= b) {
      System.Int32 RNTRNTRNT_202 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(585);
      return RNTRNTRNT_202;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(586);
    result++;
    IACSharpSensor.IACSharpSensor.SensorReached(587);
    if (a >= b) {
      System.Int32 RNTRNTRNT_203 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(588);
      return RNTRNTRNT_203;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(589);
    result++;
    System.Int32 RNTRNTRNT_204 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(590);
    return RNTRNTRNT_204;
  }
}
public class TestDriver
{
  public static int RunTests(Type type, string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(591);
    int failed = 0, ran = 0;
    int result, expected, elen;
    int i, j;
    string name;
    MethodInfo[] methods;
    bool do_timings = false;
    int tms = 0;
    DateTime start, end = DateTime.Now;
    IACSharpSensor.IACSharpSensor.SensorReached(592);
    if (args != null && args.Length > 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(593);
      for (j = 0; j < args.Length; j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(594);
        if (args[j] == "--time") {
          IACSharpSensor.IACSharpSensor.SensorReached(595);
          do_timings = true;
          string[] new_args = new string[args.Length - 1];
          IACSharpSensor.IACSharpSensor.SensorReached(596);
          for (i = 0; i < j; ++i) {
            IACSharpSensor.IACSharpSensor.SensorReached(597);
            new_args[i] = args[i];
          }
          IACSharpSensor.IACSharpSensor.SensorReached(598);
          j++;
          IACSharpSensor.IACSharpSensor.SensorReached(599);
          for (; j < args.Length; ++i,++j) {
            IACSharpSensor.IACSharpSensor.SensorReached(600);
            new_args[i] = args[j];
          }
          IACSharpSensor.IACSharpSensor.SensorReached(601);
          args = new_args;
          IACSharpSensor.IACSharpSensor.SensorReached(602);
          break;
        }
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(603);
    methods = type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
    IACSharpSensor.IACSharpSensor.SensorReached(604);
    for (i = 0; i < methods.Length; ++i) {
      IACSharpSensor.IACSharpSensor.SensorReached(605);
      name = methods[i].Name;
      IACSharpSensor.IACSharpSensor.SensorReached(606);
      if (!name.StartsWith("test_")) {
        IACSharpSensor.IACSharpSensor.SensorReached(607);
        continue;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(608);
      if (args != null && args.Length > 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(609);
        bool found = false;
        IACSharpSensor.IACSharpSensor.SensorReached(610);
        for (j = 0; j < args.Length; j++) {
          IACSharpSensor.IACSharpSensor.SensorReached(611);
          if (name.EndsWith(args[j])) {
            IACSharpSensor.IACSharpSensor.SensorReached(612);
            found = true;
            IACSharpSensor.IACSharpSensor.SensorReached(613);
            break;
          }
        }
        IACSharpSensor.IACSharpSensor.SensorReached(614);
        if (!found) {
          IACSharpSensor.IACSharpSensor.SensorReached(615);
          continue;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(616);
      for (j = 5; j < name.Length; ++j) {
        IACSharpSensor.IACSharpSensor.SensorReached(617);
        if (!Char.IsDigit(name[j])) {
          IACSharpSensor.IACSharpSensor.SensorReached(618);
          break;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(619);
      expected = Int32.Parse(name.Substring(5, j - 5));
      start = DateTime.Now;
      result = (int)methods[i].Invoke(null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(620);
      if (do_timings) {
        IACSharpSensor.IACSharpSensor.SensorReached(621);
        end = DateTime.Now;
        long tdiff = end.Ticks - start.Ticks;
        int mdiff = (int)tdiff / 10000;
        tms += mdiff;
        Console.WriteLine("{0} took {1} ms", name, mdiff);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(622);
      ran++;
      IACSharpSensor.IACSharpSensor.SensorReached(623);
      if (result != expected) {
        IACSharpSensor.IACSharpSensor.SensorReached(624);
        failed++;
        Console.WriteLine("{0} failed: got {1}, expected {2}", name, result, expected);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(625);
    if (do_timings) {
      IACSharpSensor.IACSharpSensor.SensorReached(626);
      Console.WriteLine("Total ms: {0}", tms);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(627);
    Console.WriteLine("Regression tests: {0} ran, {1} failed in {2}", ran, failed, type);
    System.Int32 RNTRNTRNT_205 = failed;
    IACSharpSensor.IACSharpSensor.SensorReached(628);
    return RNTRNTRNT_205;
  }
  public static int RunTests(Type type)
  {
    System.Int32 RNTRNTRNT_206 = RunTests(type, null);
    IACSharpSensor.IACSharpSensor.SensorReached(629);
    return RNTRNTRNT_206;
  }
}
