public class Parent
{
  public Parent()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(11);
  }
  private Collide Collide;
}
public class Child : Parent
{
  public class Nested
  {
    public readonly Collide Test;
    public Nested()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(12);
      Test = Collide.Die;
      IACSharpSensor.IACSharpSensor.SensorReached(13);
    }
  }
}
public class Collide
{
  public Collide(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(14);
    this.A = a;
    IACSharpSensor.IACSharpSensor.SensorReached(15);
  }
  public readonly int A;
  public static readonly Collide Die = new Collide(5);
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    Child.Nested nested = new Child.Nested();
    IACSharpSensor.IACSharpSensor.SensorReached(17);
    if (nested.Test.A != 5) {
      System.Int32 RNTRNTRNT_5 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(18);
      return RNTRNTRNT_5;
    }
    System.Int32 RNTRNTRNT_6 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(19);
    return RNTRNTRNT_6;
  }
}
