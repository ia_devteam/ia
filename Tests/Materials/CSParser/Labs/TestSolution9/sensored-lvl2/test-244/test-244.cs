using System;
class Foo
{
  static int t_count = 0, f_count = 0;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(741);
    Console.WriteLine(t && f);
    IACSharpSensor.IACSharpSensor.SensorReached(742);
    if (t_count != 1) {
      System.Int32 RNTRNTRNT_218 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(743);
      return RNTRNTRNT_218;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(744);
    if (f_count != 1) {
      System.Int32 RNTRNTRNT_219 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(745);
      return RNTRNTRNT_219;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(746);
    Console.WriteLine();
    Console.WriteLine(t && t);
    IACSharpSensor.IACSharpSensor.SensorReached(747);
    if (t_count != 3) {
      System.Int32 RNTRNTRNT_220 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(748);
      return RNTRNTRNT_220;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(749);
    if (f_count != 1) {
      System.Int32 RNTRNTRNT_221 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(750);
      return RNTRNTRNT_221;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(751);
    Console.WriteLine();
    System.Int32 RNTRNTRNT_222 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(752);
    return RNTRNTRNT_222;
  }
  static MyBool t {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(753);
      Console.WriteLine("t");
      t_count++;
      MyBool RNTRNTRNT_223 = new MyBool(true);
      IACSharpSensor.IACSharpSensor.SensorReached(754);
      return RNTRNTRNT_223;
    }
  }
  static MyBool f {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(755);
      Console.WriteLine("f");
      f_count++;
      MyBool RNTRNTRNT_224 = new MyBool(false);
      IACSharpSensor.IACSharpSensor.SensorReached(756);
      return RNTRNTRNT_224;
    }
  }
}
public struct MyBool
{
  bool v;
  public MyBool(bool v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(757);
    this.v = v;
    IACSharpSensor.IACSharpSensor.SensorReached(758);
  }
  public static MyBool operator &(MyBool x, MyBool y)
  {
    MyBool RNTRNTRNT_225 = new MyBool(x.v & y.v);
    IACSharpSensor.IACSharpSensor.SensorReached(759);
    return RNTRNTRNT_225;
  }
  public static MyBool operator |(MyBool x, MyBool y)
  {
    MyBool RNTRNTRNT_226 = new MyBool(x.v | y.v);
    IACSharpSensor.IACSharpSensor.SensorReached(760);
    return RNTRNTRNT_226;
  }
  public static bool operator true(MyBool x)
  {
    System.Boolean RNTRNTRNT_227 = x.v;
    IACSharpSensor.IACSharpSensor.SensorReached(761);
    return RNTRNTRNT_227;
  }
  public static bool operator false(MyBool x)
  {
    System.Boolean RNTRNTRNT_228 = !x.v;
    IACSharpSensor.IACSharpSensor.SensorReached(762);
    return RNTRNTRNT_228;
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_229 = v.ToString();
    IACSharpSensor.IACSharpSensor.SensorReached(763);
    return RNTRNTRNT_229;
  }
}
