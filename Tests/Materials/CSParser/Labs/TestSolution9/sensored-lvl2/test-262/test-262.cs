namespace n1
{
  class Attribute
  {
  }
}
namespace n3
{
  using n1;
  using System;
  class A
  {
    void Attribute()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(838);
    }
    void X()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(839);
      Attribute();
      IACSharpSensor.IACSharpSensor.SensorReached(840);
    }
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(841);
      new A().X();
      IACSharpSensor.IACSharpSensor.SensorReached(842);
    }
  }
}
