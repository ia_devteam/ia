using System;
[My((long)1)]
[My(TypeCode.Empty)]
[My(typeof(System.Enum))]
class T
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1100);
    object[] a = Attribute.GetCustomAttributes(typeof(T), false);
    IACSharpSensor.IACSharpSensor.SensorReached(1101);
    if (a.Length != 3) {
      System.Int32 RNTRNTRNT_334 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1102);
      return RNTRNTRNT_334;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1103);
    foreach (object o in a) {
      IACSharpSensor.IACSharpSensor.SensorReached(1104);
      My attr = (My)o;
      IACSharpSensor.IACSharpSensor.SensorReached(1105);
      if (attr.obj.GetType() == typeof(long)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1106);
        long val = (long)attr.obj;
        IACSharpSensor.IACSharpSensor.SensorReached(1107);
        if (val != 1) {
          System.Int32 RNTRNTRNT_335 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(1108);
          return RNTRNTRNT_335;
        }
      } else if (attr.obj.GetType() == typeof(TypeCode)) {
        IACSharpSensor.IACSharpSensor.SensorReached(1113);
        TypeCode val = (TypeCode)attr.obj;
        IACSharpSensor.IACSharpSensor.SensorReached(1114);
        if (val != TypeCode.Empty) {
          System.Int32 RNTRNTRNT_338 = 3;
          IACSharpSensor.IACSharpSensor.SensorReached(1115);
          return RNTRNTRNT_338;
        }
      } else if (attr.obj.GetType().IsSubclassOf(typeof(Type))) {
        IACSharpSensor.IACSharpSensor.SensorReached(1110);
        Type val = (Type)attr.obj;
        IACSharpSensor.IACSharpSensor.SensorReached(1111);
        if (val != typeof(System.Enum)) {
          System.Int32 RNTRNTRNT_337 = 4;
          IACSharpSensor.IACSharpSensor.SensorReached(1112);
          return RNTRNTRNT_337;
        }
      } else {
        System.Int32 RNTRNTRNT_336 = 5;
        IACSharpSensor.IACSharpSensor.SensorReached(1109);
        return RNTRNTRNT_336;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1116);
    object[] ats = typeof(T).GetMethod("Login").GetCustomAttributes(typeof(My), true);
    My at = (My)ats[0];
    IACSharpSensor.IACSharpSensor.SensorReached(1117);
    if (at.Val != AnEnum.a) {
      System.Int32 RNTRNTRNT_339 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(1118);
      return RNTRNTRNT_339;
    }
    System.Int32 RNTRNTRNT_340 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1119);
    return RNTRNTRNT_340;
  }
  [My(1, Val = AnEnum.a)]
  public void Login(string a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1120);
  }
}
[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
class My : Attribute
{
  public object obj;
  public My(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1121);
    obj = o;
    IACSharpSensor.IACSharpSensor.SensorReached(1122);
  }
  public AnEnum Val;
}
public enum AnEnum
{
  a,
  b,
  c
}
