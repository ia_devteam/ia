using System;
namespace MyTest
{
  public class Test
  {
    public interface Inner
    {
      void Foo();
    }
  }
  public class Test2 : MarshalByRefObject, Test.Inner
  {
    void Test.Inner.Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(817);
    }
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(818);
    }
  }
}
