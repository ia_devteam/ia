using System;
internal class ClassFormatError
{
  internal ClassFormatError(string msg, params object[] p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(852);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(853);
  }
}
internal class UnsupportedClassVersionError : ClassFormatError
{
  internal UnsupportedClassVersionError(string msg) : base(msg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(854);
  }
}
