using System;
using System.Reflection;
public class CtorInfoTest
{
  enum E
  {
    A = 0,
    B = 1
  }
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(324);
    int[] iarray = {
      0,
      1,
      2,
      3,
      4,
      5,
      6
    };
    object[] oarray = {
      0,
      E.A,
      null,
      "A",
      new int(),
      1.1,
      -2m
    };
    object[] ooarray = {
      null,
      new int[] {
        0,
        0
      },
      0,
      new object[0]
    };
    ConstructorInfo[] ciarray = {
      null,
      null,
      null,
      null,
      null,
      null,
      null
    };
    string[] scarray = {
      "a",
      "b",
      "c",
      "d",
      "e",
      "f",
      "g"
    };
    string[] snarray = {
      null,
      null,
      null,
      null,
      null,
      null,
      null
    };
    decimal[] darray = {
      0m,
      1m,
      2m,
      3m,
      4m,
      5m,
      6m,
      7m
    };
    IConvertible[] lcarray = {
      0,
      1,
      2,
      3,
      4,
      5,
      6,
      7
    };
    System.Enum[] eatarray = {
      AttributeTargets.Assembly,
      AttributeTargets.Module,
      AttributeTargets.Class,
      AttributeTargets.Struct,
      AttributeTargets.Enum,
      AttributeTargets.Constructor,
      AttributeTargets.Method,
      AttributeTargets.Property,
      AttributeTargets.Field,
      AttributeTargets.Event,
      AttributeTargets.Interface,
      AttributeTargets.Parameter,
      AttributeTargets.Delegate,
      AttributeTargets.ReturnValue,
      AttributeTargets.All
    };
    E[] atarray = {
      E.A,
      E.B
    };
    string[] smarray = {
      null,
      "a"
    };
    IACSharpSensor.IACSharpSensor.SensorReached(325);
    for (int i = 0; i < iarray.Length; ++i) {
      IACSharpSensor.IACSharpSensor.SensorReached(326);
      Assert(i, iarray[i]);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(327);
    for (int i = 0; i < ciarray.Length; ++i) {
      IACSharpSensor.IACSharpSensor.SensorReached(328);
      Assert(null, ciarray[i]);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(329);
    Assert("a", scarray[0]);
    IACSharpSensor.IACSharpSensor.SensorReached(330);
    for (int i = 0; i < snarray.Length; ++i) {
      IACSharpSensor.IACSharpSensor.SensorReached(331);
      Assert(null, snarray[i]);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(332);
    for (decimal i = 0; i < darray.Length; ++i) {
      IACSharpSensor.IACSharpSensor.SensorReached(333);
      Assert(i, darray[(int)i]);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(334);
    for (int i = 0; i < lcarray.Length; ++i) {
      IACSharpSensor.IACSharpSensor.SensorReached(335);
      Assert(i, lcarray[i]);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(336);
    Assert(E.A, atarray[0]);
    Assert(E.B, atarray[1]);
    Assert(AttributeTargets.Assembly, eatarray[0]);
    Assert(AttributeTargets.Class, eatarray[2]);
    Assert(null, smarray[0]);
    Assert("a", smarray[1]);
    IACSharpSensor.IACSharpSensor.SensorReached(337);
  }
  static void Assert(object expected, object value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(338);
    if (expected == null && value == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(339);
      return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(340);
    if (!expected.Equals(value)) {
      IACSharpSensor.IACSharpSensor.SensorReached(341);
      Console.WriteLine("ERROR {0} != {1}", expected, value);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(342);
  }
}
