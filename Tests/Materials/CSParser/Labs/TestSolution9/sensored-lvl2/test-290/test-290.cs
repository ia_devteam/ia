using System;
class EntryPoint
{
  delegate void EventHandler(object sender);
  static event EventHandler FooEvent;
  static void bar_f(object sender)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1048);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1049);
    if (FooEvent != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(1050);
      FooEvent(null);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1051);
    object bar = new EventHandler(bar_f);
    IACSharpSensor.IACSharpSensor.SensorReached(1052);
  }
}
