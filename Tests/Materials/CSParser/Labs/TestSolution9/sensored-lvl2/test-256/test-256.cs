using System;
[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
public class TableColumn : Attribute
{
  public object MagicValue {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(805);
      return null;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(806); }
  }
  public object Value2;
}
class Bug
{
  [TableColumn(MagicValue = 2, Value2 = 0)]
  public int TInt {
    get {
      System.Int32 RNTRNTRNT_238 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(807);
      return RNTRNTRNT_238;
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(808);
    const object o = null;
    IACSharpSensor.IACSharpSensor.SensorReached(809);
  }
}
