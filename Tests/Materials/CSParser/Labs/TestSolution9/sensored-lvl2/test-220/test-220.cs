using System;
using System.Collections;
using System.Collections.Specialized;
namespace MonoBUG
{
  public class Bug
  {
    public static int Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(160);
      FooList l = new FooList();
      Foo f1 = new Foo("First");
      Foo f2 = new Foo("Second");
      l.Add(f1);
      l.Add(f2);
      IACSharpSensor.IACSharpSensor.SensorReached(161);
      foreach (Foo f in l) {
      }
      IACSharpSensor.IACSharpSensor.SensorReached(162);
      if (FooList.foo_current_called != true) {
        System.Int32 RNTRNTRNT_52 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(163);
        return RNTRNTRNT_52;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(164);
      if (FooList.ienumerator_current_called != false) {
        System.Int32 RNTRNTRNT_53 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(165);
        return RNTRNTRNT_53;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(166);
      Console.WriteLine("Test passes");
      System.Int32 RNTRNTRNT_54 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(167);
      return RNTRNTRNT_54;
    }
  }
  public class Foo
  {
    private string m_name;
    public Foo(string name)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(168);
      m_name = name;
      IACSharpSensor.IACSharpSensor.SensorReached(169);
    }
    public string Name {
      get {
        System.String RNTRNTRNT_55 = m_name;
        IACSharpSensor.IACSharpSensor.SensorReached(170);
        return RNTRNTRNT_55;
      }
    }
  }
  [Serializable()]
  public class FooList : DictionaryBase
  {
    public static bool foo_current_called = false;
    public static bool ienumerator_current_called = false;
    public FooList()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(171);
    }
    public void Add(Foo value)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(172);
      Dictionary.Add(value.Name, value);
      IACSharpSensor.IACSharpSensor.SensorReached(173);
    }
    public new FooEnumerator GetEnumerator()
    {
      FooEnumerator RNTRNTRNT_56 = new FooEnumerator(this);
      IACSharpSensor.IACSharpSensor.SensorReached(174);
      return RNTRNTRNT_56;
    }
    public class FooEnumerator : object, IEnumerator
    {
      private IEnumerator baseEnumerator;
      private IEnumerable temp;
      public FooEnumerator(FooList mappings)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(175);
        this.temp = (IEnumerable)(mappings);
        this.baseEnumerator = temp.GetEnumerator();
        IACSharpSensor.IACSharpSensor.SensorReached(176);
      }
      public Foo Current {
        get {
          IACSharpSensor.IACSharpSensor.SensorReached(177);
          Console.WriteLine("Foo Current()");
          foo_current_called = true;
          Foo RNTRNTRNT_57 = (Foo)((DictionaryEntry)(baseEnumerator.Current)).Value;
          IACSharpSensor.IACSharpSensor.SensorReached(178);
          return RNTRNTRNT_57;
        }
      }
      object IEnumerator.Current {
        get {
          IACSharpSensor.IACSharpSensor.SensorReached(179);
          Console.WriteLine("object IEnumerator.Current()");
          ienumerator_current_called = true;
          System.Object RNTRNTRNT_58 = baseEnumerator.Current;
          IACSharpSensor.IACSharpSensor.SensorReached(180);
          return RNTRNTRNT_58;
        }
      }
      public bool MoveNext()
      {
        System.Boolean RNTRNTRNT_59 = baseEnumerator.MoveNext();
        IACSharpSensor.IACSharpSensor.SensorReached(181);
        return RNTRNTRNT_59;
      }
      bool IEnumerator.MoveNext()
      {
        System.Boolean RNTRNTRNT_60 = baseEnumerator.MoveNext();
        IACSharpSensor.IACSharpSensor.SensorReached(182);
        return RNTRNTRNT_60;
      }
      public void Reset()
      {
        IACSharpSensor.IACSharpSensor.SensorReached(183);
        baseEnumerator.Reset();
        IACSharpSensor.IACSharpSensor.SensorReached(184);
      }
      void IEnumerator.Reset()
      {
        IACSharpSensor.IACSharpSensor.SensorReached(185);
        baseEnumerator.Reset();
        IACSharpSensor.IACSharpSensor.SensorReached(186);
      }
    }
  }
}
