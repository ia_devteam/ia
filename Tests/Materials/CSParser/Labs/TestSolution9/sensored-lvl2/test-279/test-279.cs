using System;
class FlagsAttributeDemo
{
  [FlagsAttribute()]
  enum MultiHue : short
  {
    Black = 0,
    Red = 1,
    Green = 2,
    Blue = 4
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(974);
    string s = ((MultiHue)7).ToString();
    Console.WriteLine(s);
    IACSharpSensor.IACSharpSensor.SensorReached(975);
    if (s != "Red, Green, Blue") {
      System.Int32 RNTRNTRNT_289 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(976);
      return RNTRNTRNT_289;
    }
    System.Int32 RNTRNTRNT_290 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(977);
    return RNTRNTRNT_290;
  }
}
