public class Params
{
  public static readonly object[] test = new object[] {
    1,
    "foo",
    3.14
  };
  public static readonly object[] test_types = new object[] {
    typeof(int),
    typeof(string),
    typeof(double)
  };
  public delegate void FOO(string s, params object[] args);
  public static void foo(string s, params object[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(707);
    if (args.Length != test.Length) {
      IACSharpSensor.IACSharpSensor.SensorReached(708);
      throw new System.Exception("Length mismatch during " + s + " invocation");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(709);
    for (int i = 0; i < args.Length; ++i) {
      IACSharpSensor.IACSharpSensor.SensorReached(710);
      if (args[i].GetType() != test_types[i]) {
        IACSharpSensor.IACSharpSensor.SensorReached(711);
        throw new System.Exception("Type mismatch: " + args[i].GetType() + " vs. " + test_types[i]);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(712);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(713);
    foo("normal", test);
    FOO f = new FOO(foo);
    f("delegate", test);
    IACSharpSensor.IACSharpSensor.SensorReached(714);
  }
}
