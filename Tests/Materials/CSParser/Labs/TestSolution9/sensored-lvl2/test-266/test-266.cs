using System;
enum Foo
{
  Bar
}
class T
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(855);
    Enum e = Foo.Bar;
    IConvertible convertible = (IConvertible)e;
    IComparable comparable = (IComparable)e;
    IFormattable formattable = (IFormattable)e;
    Console.WriteLine("PASS");
    System.Int32 RNTRNTRNT_248 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(856);
    return RNTRNTRNT_248;
  }
}
