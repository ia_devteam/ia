using System;
namespace BadRefTest
{
  public class CtorInc
  {
    static int x, y;
    static int IncByRef(ref int i)
    {
      System.Int32 RNTRNTRNT_95 = ++i;
      IACSharpSensor.IACSharpSensor.SensorReached(272);
      return RNTRNTRNT_95;
    }
    public CtorInc()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(273);
      IncByRef(ref x);
      ++y;
      IACSharpSensor.IACSharpSensor.SensorReached(274);
    }
    public static bool Results(int total)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(275);
      Console.WriteLine("CtorInc test {0}: x == {1}, y == {2}", x == y && x == total ? "passed" : "failed", x, y);
      System.Boolean RNTRNTRNT_96 = x == y && x == total;
      IACSharpSensor.IACSharpSensor.SensorReached(276);
      return RNTRNTRNT_96;
    }
  }
  public class Runner
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(277);
      int i = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(278);
      for (; i < 5; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(279);
        CtorInc t = new CtorInc();
      }
      System.Int32 RNTRNTRNT_97 = CtorInc.Results(i) ? 0 : 1;
      IACSharpSensor.IACSharpSensor.SensorReached(280);
      return RNTRNTRNT_97;
    }
  }
}
