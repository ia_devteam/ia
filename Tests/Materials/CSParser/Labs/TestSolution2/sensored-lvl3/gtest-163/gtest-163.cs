using System;
using System.Collections;
using System.Collections.Generic;
public class Foo<T>
{
  public IEnumerator<T> getEnumerator(int arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(343);
    if (arg == 1) {
      int foo = arg;
      Console.WriteLine(foo);
    }
    if (arg == 2) {
      int foo = arg;
      Console.WriteLine(foo);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(344);
    yield break;
  }
}
class X
{
  static void Main()
  {
  }
}
