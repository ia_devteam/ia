using System;
public interface IFoo<T>
{
}
public class Foo<T>
{
  public static bool Test(T x)
  {
    System.Boolean RNTRNTRNT_39 = x is IFoo<T>;
    IACSharpSensor.IACSharpSensor.SensorReached(149);
    return RNTRNTRNT_39;
  }
  public static bool Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(150);
    T t = default(T);
    System.Boolean RNTRNTRNT_40 = t is int;
    IACSharpSensor.IACSharpSensor.SensorReached(151);
    return RNTRNTRNT_40;
  }
  public static bool TestB()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(152);
    T t = default(T);
    System.Boolean RNTRNTRNT_41 = t is int?;
    IACSharpSensor.IACSharpSensor.SensorReached(153);
    return RNTRNTRNT_41;
  }
}
class Y<T> where T : struct
{
  public bool Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(154);
    object o = null;
    System.Boolean RNTRNTRNT_42 = o is System.Nullable<T>;
    IACSharpSensor.IACSharpSensor.SensorReached(155);
    return RNTRNTRNT_42;
  }
}
class X
{
  public static bool TestA(object o)
  {
    System.Boolean RNTRNTRNT_43 = o is int?;
    IACSharpSensor.IACSharpSensor.SensorReached(156);
    return RNTRNTRNT_43;
  }
  public static bool TestB<T>(T o)
  {
    System.Boolean RNTRNTRNT_44 = o is int[];
    IACSharpSensor.IACSharpSensor.SensorReached(157);
    return RNTRNTRNT_44;
  }
  public static int TestC()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(158);
    int? i = null;
    if (i is int) {
      System.Int32 RNTRNTRNT_45 = (int)i;
      IACSharpSensor.IACSharpSensor.SensorReached(159);
      return RNTRNTRNT_45;
    }
    System.Int32 RNTRNTRNT_46 = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(160);
    return RNTRNTRNT_46;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(161);
    if (Foo<int>.Test(3)) {
      System.Int32 RNTRNTRNT_47 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(162);
      return RNTRNTRNT_47;
    }
    if (!Foo<int>.Test()) {
      System.Int32 RNTRNTRNT_48 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(163);
      return RNTRNTRNT_48;
    }
    if (Foo<int?>.TestB()) {
      System.Int32 RNTRNTRNT_49 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(164);
      return RNTRNTRNT_49;
    }
    int? i = 0;
    if (!TestA(i)) {
      System.Int32 RNTRNTRNT_50 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(165);
      return RNTRNTRNT_50;
    }
    int[] a = new int[0];
    if (!TestB(a)) {
      System.Int32 RNTRNTRNT_51 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(166);
      return RNTRNTRNT_51;
    }
    if (TestC() != 3) {
      System.Int32 RNTRNTRNT_52 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(167);
      return RNTRNTRNT_52;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_53 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(168);
    return RNTRNTRNT_53;
  }
}
