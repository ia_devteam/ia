public delegate int T<X>(X x);
public class B
{
  public static T<X> M<X>()
  {
    T<X> RNTRNTRNT_92 = delegate(X x) { return 5; };
    IACSharpSensor.IACSharpSensor.SensorReached(320);
    return RNTRNTRNT_92;
  }
  public static T<long> N()
  {
    T<System.Int64> RNTRNTRNT_93 = delegate(long x) { return 6; };
    IACSharpSensor.IACSharpSensor.SensorReached(321);
    return RNTRNTRNT_93;
  }
}
public class D
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(322);
    B.M<int>();
    B.N();
    IACSharpSensor.IACSharpSensor.SensorReached(323);
  }
}
