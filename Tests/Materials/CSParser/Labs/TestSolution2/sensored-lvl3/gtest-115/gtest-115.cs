using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
public delegate R Mapper<A, R>(A x);
public interface IMyList<T> : IEnumerable<T>
{
  int Count { get; }
  T this[int i] { get; set; }
  void Add(T item);
  void Insert(int i, T item);
  void RemoveAt(int i);
  IMyList<U> Map<U>(Mapper<T, U> f);
}
public class LinkedList<T> : IMyList<T>
{
  protected int size;
  protected Node first, last;
  protected class Node
  {
    public Node prev, next;
    public T item;
    public Node(T item)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(88);
      this.item = item;
      IACSharpSensor.IACSharpSensor.SensorReached(89);
    }
    public Node(T item, Node prev, Node next)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(90);
      this.item = item;
      this.prev = prev;
      this.next = next;
      IACSharpSensor.IACSharpSensor.SensorReached(91);
    }
  }
  public LinkedList()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(92);
    first = last = null;
    size = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(93);
  }
  public LinkedList(params T[] arr) : this()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(94);
    foreach (T x in arr) {
      Add(x);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(95);
  }
  public int Count {
    get {
      System.Int32 RNTRNTRNT_20 = size;
      IACSharpSensor.IACSharpSensor.SensorReached(96);
      return RNTRNTRNT_20;
    }
  }
  public T this[int i] {
    get {
      T RNTRNTRNT_21 = @get(i).item;
      IACSharpSensor.IACSharpSensor.SensorReached(97);
      return RNTRNTRNT_21;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(98);
      @get(i).item = value;
      IACSharpSensor.IACSharpSensor.SensorReached(99);
    }
  }
  private Node @get(int n)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(100);
    if (n < 0 || n >= size) {
      throw new IndexOutOfRangeException();
    } else if (n < size / 2) {
      Node node = first;
      for (int i = 0; i < n; i++) {
        node = node.next;
      }
      Node RNTRNTRNT_23 = node;
      IACSharpSensor.IACSharpSensor.SensorReached(102);
      return RNTRNTRNT_23;
    } else {
      Node node = last;
      for (int i = size - 1; i > n; i--) {
        node = node.prev;
      }
      Node RNTRNTRNT_22 = node;
      IACSharpSensor.IACSharpSensor.SensorReached(101);
      return RNTRNTRNT_22;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(103);
  }
  public void Add(T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(104);
    Insert(size, item);
    IACSharpSensor.IACSharpSensor.SensorReached(105);
  }
  public void Insert(int i, T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(106);
    if (i == 0) {
      if (first == null) {
        first = last = new Node(item);
      } else {
        Node tmp = new Node(item, null, first);
        first.prev = tmp;
        first = tmp;
      }
      size++;
    } else if (i == size) {
      if (last == null) {
        first = last = new Node(item);
      } else {
        Node tmp = new Node(item, last, null);
        last.next = tmp;
        last = tmp;
      }
      size++;
    } else {
      Node node = @get(i);
      Node newnode = new Node(item, node.prev, node);
      node.prev.next = newnode;
      node.prev = newnode;
      size++;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(107);
  }
  public void RemoveAt(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(108);
    Node node = @get(i);
    if (node.prev == null) {
      first = node.next;
    } else {
      node.prev.next = node.next;
    }
    if (node.next == null) {
      last = node.prev;
    } else {
      node.next.prev = node.prev;
    }
    size--;
    IACSharpSensor.IACSharpSensor.SensorReached(109);
  }
  public override bool Equals(Object that)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(110);
    if (that != null && GetType() == that.GetType() && this.size == ((IMyList<T>)that).Count) {
      Node thisnode = this.first;
      IEnumerator<T> thatenm = ((IMyList<T>)that).GetEnumerator();
      while (thisnode != null) {
        if (!thatenm.MoveNext()) {
          throw new ApplicationException("Impossible: LinkedList<T>.Equals");
        }
        if (!thisnode.item.Equals(thatenm.Current)) {
          System.Boolean RNTRNTRNT_24 = false;
          IACSharpSensor.IACSharpSensor.SensorReached(111);
          return RNTRNTRNT_24;
        }
        thisnode = thisnode.next;
      }
      System.Boolean RNTRNTRNT_25 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(112);
      return RNTRNTRNT_25;
    } else {
      System.Boolean RNTRNTRNT_26 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(113);
      return RNTRNTRNT_26;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(114);
  }
  public override int GetHashCode()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(115);
    int hash = 0;
    foreach (T x in this) {
      hash ^= x.GetHashCode();
    }
    System.Int32 RNTRNTRNT_27 = hash;
    IACSharpSensor.IACSharpSensor.SensorReached(116);
    return RNTRNTRNT_27;
  }
  public static explicit operator LinkedList<T>(T[] arr)
  {
    LinkedList<T> RNTRNTRNT_28 = new LinkedList<T>(arr);
    IACSharpSensor.IACSharpSensor.SensorReached(117);
    return RNTRNTRNT_28;
  }
  public static LinkedList<T> operator +(LinkedList<T> xs1, LinkedList<T> xs2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(118);
    LinkedList<T> res = new LinkedList<T>();
    foreach (T x in xs1) {
      res.Add(x);
    }
    foreach (T x in xs2) {
      res.Add(x);
    }
    LinkedList<T> RNTRNTRNT_29 = res;
    IACSharpSensor.IACSharpSensor.SensorReached(119);
    return RNTRNTRNT_29;
  }
  public IMyList<U> Map<U>(Mapper<T, U> f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(120);
    LinkedList<U> res = new LinkedList<U>();
    foreach (T x in this) {
      res.Add(f(x));
    }
    IMyList<U> RNTRNTRNT_30 = res;
    IACSharpSensor.IACSharpSensor.SensorReached(121);
    return RNTRNTRNT_30;
  }
  public IEnumerator<T> GetEnumerator()
  {
    IEnumerator<T> RNTRNTRNT_31 = new LinkedListEnumerator(this);
    IACSharpSensor.IACSharpSensor.SensorReached(122);
    return RNTRNTRNT_31;
  }
  IEnumerator IEnumerable.GetEnumerator()
  {
    IEnumerator RNTRNTRNT_32 = new LinkedListEnumerator(this);
    IACSharpSensor.IACSharpSensor.SensorReached(123);
    return RNTRNTRNT_32;
  }
  private class LinkedListEnumerator : IEnumerator<T>
  {
    T curr;
    bool valid;
    Node next;
    public LinkedListEnumerator(LinkedList<T> lst)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(124);
      next = lst.first;
      valid = false;
      IACSharpSensor.IACSharpSensor.SensorReached(125);
    }
    public T Current {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(126);
        if (valid) {
          T RNTRNTRNT_33 = curr;
          IACSharpSensor.IACSharpSensor.SensorReached(127);
          return RNTRNTRNT_33;
        } else {
          throw new InvalidOperationException();
        }
      }
    }
    object IEnumerator.Current {
      get {
        System.Object RNTRNTRNT_34 = Current;
        IACSharpSensor.IACSharpSensor.SensorReached(128);
        return RNTRNTRNT_34;
      }
    }
    public bool MoveNext()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(129);
      if (next != null) {
        curr = next.item;
        next = next.next;
        valid = true;
      } else {
        valid = false;
      }
      System.Boolean RNTRNTRNT_35 = valid;
      IACSharpSensor.IACSharpSensor.SensorReached(130);
      return RNTRNTRNT_35;
    }
    public void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(131);
      throw new NotImplementedException();
    }
    public void Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(132);
      curr = default(T);
      next = null;
      valid = false;
      IACSharpSensor.IACSharpSensor.SensorReached(133);
    }
  }
}
class SortedList<T> : LinkedList<T> where T : IComparable<T>
{
  public void Insert(T x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(134);
    Node node = first;
    while (node != null && x.CompareTo(node.item) > 0) {
      node = node.next;
    }
    if (node == null) {
      Add(x);
    } else {
      Node newnode = new Node(x);
      if (node.prev == null) {
        first = newnode;
      } else {
        node.prev.next = newnode;
      }
      newnode.next = node;
      newnode.prev = node.prev;
      node.prev = newnode;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(135);
  }
}
interface IPrintable
{
  void Print(TextWriter fs);
}
class PrintableLinkedList<T> : LinkedList<T>, IPrintable where T : IPrintable
{
  public void Print(TextWriter fs)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(136);
    bool firstElement = true;
    foreach (T x in this) {
      x.Print(fs);
      if (firstElement) {
        firstElement = false;
      } else {
        fs.Write(", ");
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(137);
  }
}
class MyString : IComparable<MyString>
{
  private readonly String s;
  public MyString(String s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(138);
    this.s = s;
    IACSharpSensor.IACSharpSensor.SensorReached(139);
  }
  public int CompareTo(MyString that)
  {
    System.Int32 RNTRNTRNT_36 = String.Compare(that.Value, s);
    IACSharpSensor.IACSharpSensor.SensorReached(140);
    return RNTRNTRNT_36;
  }
  public bool Equals(MyString that)
  {
    System.Boolean RNTRNTRNT_37 = that.Value == s;
    IACSharpSensor.IACSharpSensor.SensorReached(141);
    return RNTRNTRNT_37;
  }
  public String Value {
    get {
      String RNTRNTRNT_38 = s;
      IACSharpSensor.IACSharpSensor.SensorReached(142);
      return RNTRNTRNT_38;
    }
  }
}
class MyTest
{
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(143);
    LinkedList<double> dLst = new LinkedList<double>(7.0, 9.0, 13.0, 0.0);
    foreach (double d in dLst) {
      Console.Write("{0} ", d);
    }
    Console.WriteLine();
    IMyList<int> iLst = dLst.Map<int>(new Mapper<double, int>(Math.Sign));
    foreach (int i in iLst) {
      Console.Write("{0} ", i);
    }
    Console.WriteLine();
    IMyList<String> sLst = dLst.Map<String>(delegate(double d) { return "s" + d; });
    foreach (String s in sLst) {
      Console.Write("{0} ", s);
    }
    Console.WriteLine();
    SortedList<MyString> sortedLst = new SortedList<MyString>();
    sortedLst.Insert(new MyString("New York"));
    sortedLst.Insert(new MyString("Rome"));
    sortedLst.Insert(new MyString("Dublin"));
    sortedLst.Insert(new MyString("Riyadh"));
    sortedLst.Insert(new MyString("Tokyo"));
    foreach (MyString s in sortedLst) {
      Console.Write("{0}   ", s.Value);
    }
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(144);
  }
}
