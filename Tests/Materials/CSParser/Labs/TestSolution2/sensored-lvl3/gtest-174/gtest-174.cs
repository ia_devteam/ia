public class B<T>
{
  public static B<T> _N_constant_object = new B<T>();
}
class M
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(395);
    A<int> x = A<int>._N_constant_object;
    B<int> y = B<int>._N_constant_object;
    IACSharpSensor.IACSharpSensor.SensorReached(396);
  }
}
