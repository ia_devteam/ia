using System;
using System.Reflection;
namespace FLMID.Bugs.ParametersOne
{
  public class Class<T>
  {
    public void Add(T x)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(225);
      System.Console.WriteLine("OK");
      IACSharpSensor.IACSharpSensor.SensorReached(226);
    }
  }
  public class Test
  {
    public static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(227);
      Class<string> instance = new Class<string>();
      MethodInfo _method = null;
      foreach (MethodInfo method in typeof(Class<string>).GetMethods(BindingFlags.Instance | BindingFlags.Public)) {
        if (method.Name.Equals("Add") && method.GetParameters().Length == 1) {
          _method = method;
          break;
        }
      }
      _method.Invoke(instance, new object[] { "1" });
      IACSharpSensor.IACSharpSensor.SensorReached(228);
    }
  }
}
