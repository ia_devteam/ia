using System;
interface AddMul<A, R>
{
  R Add(A e);
  R Mul(A e);
}
class Polynomial<E> : AddMul<E, Polynomial<E>>, AddMul<Polynomial<E>, Polynomial<E>> where E : AddMul<E, E>, new()
{
  private readonly E[] cs;
  public Polynomial()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(169);
    this.cs = new E[0];
    IACSharpSensor.IACSharpSensor.SensorReached(170);
  }
  public Polynomial(E[] cs)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(171);
    this.cs = cs;
    IACSharpSensor.IACSharpSensor.SensorReached(172);
  }
  public Polynomial<E> Add(Polynomial<E> that)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(173);
    int newlen = Math.Max(this.cs.Length, that.cs.Length);
    int minlen = Math.Min(this.cs.Length, that.cs.Length);
    E[] newcs = new E[newlen];
    if (this.cs.Length <= that.cs.Length) {
      for (int i = 0; i < minlen; i++) {
        newcs[i] = this.cs[i].Add(that.cs[i]);
      }
      for (int i = minlen; i < newlen; i++) {
        newcs[i] = that.cs[i];
      }
    } else {
      for (int i = 0; i < minlen; i++) {
        newcs[i] = this.cs[i].Add(that.cs[i]);
      }
      for (int i = minlen; i < newlen; i++) {
        newcs[i] = this.cs[i];
      }
    }
    Polynomial<E> RNTRNTRNT_54 = new Polynomial<E>(newcs);
    IACSharpSensor.IACSharpSensor.SensorReached(174);
    return RNTRNTRNT_54;
  }
  public Polynomial<E> Add(E that)
  {
    Polynomial<E> RNTRNTRNT_55 = this.Add(new Polynomial<E>(new E[] { that }));
    IACSharpSensor.IACSharpSensor.SensorReached(175);
    return RNTRNTRNT_55;
  }
  public Polynomial<E> Mul(E that)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(176);
    E[] newcs = new E[cs.Length];
    for (int i = 0; i < cs.Length; i++) {
      newcs[i] = that.Mul(cs[i]);
    }
    Polynomial<E> RNTRNTRNT_56 = new Polynomial<E>(newcs);
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    return RNTRNTRNT_56;
  }
  public Polynomial<E> Mul(Polynomial<E> that)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(178);
    int newlen = Math.Max(1, this.cs.Length + that.cs.Length - 1);
    E[] newcs = new E[newlen];
    for (int i = 0; i < newlen; i++) {
      E sum = new E();
      int start = Math.Max(0, i - that.cs.Length + 1);
      int stop = Math.Min(i, this.cs.Length - 1);
      for (int j = start; j <= stop; j++) {
        sum = sum.Add(this.cs[j].Mul(that.cs[i - j]));
      }
      newcs[i] = sum;
    }
    Polynomial<E> RNTRNTRNT_57 = new Polynomial<E>(newcs);
    IACSharpSensor.IACSharpSensor.SensorReached(179);
    return RNTRNTRNT_57;
  }
  public E Eval(E x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(180);
    E res = new E();
    for (int j = cs.Length - 1; j >= 0; j--) {
      res = res.Mul(x).Add(cs[j]);
    }
    E RNTRNTRNT_58 = res;
    IACSharpSensor.IACSharpSensor.SensorReached(181);
    return RNTRNTRNT_58;
  }
}
struct Int : AddMul<Int, Int>
{
  private readonly int i;
  public Int(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(182);
    this.i = i;
    IACSharpSensor.IACSharpSensor.SensorReached(183);
  }
  public Int Add(Int that)
  {
    Int RNTRNTRNT_59 = new Int(this.i + that.i);
    IACSharpSensor.IACSharpSensor.SensorReached(184);
    return RNTRNTRNT_59;
  }
  public Int Mul(Int that)
  {
    Int RNTRNTRNT_60 = new Int(this.i * that.i);
    IACSharpSensor.IACSharpSensor.SensorReached(185);
    return RNTRNTRNT_60;
  }
  public override String ToString()
  {
    String RNTRNTRNT_61 = i.ToString();
    IACSharpSensor.IACSharpSensor.SensorReached(186);
    return RNTRNTRNT_61;
  }
}
class TestPolynomial
{
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(187);
    Polynomial<Int> ip = new Polynomial<Int>(new Int[] {
      new Int(2),
      new Int(5),
      new Int(1)
    });
    Console.WriteLine(ip.Eval(new Int(10)));
    Console.WriteLine(ip.Add(ip).Eval(new Int(10)));
    Console.WriteLine(ip.Mul(ip).Eval(new Int(10)));
    IACSharpSensor.IACSharpSensor.SensorReached(188);
  }
}
