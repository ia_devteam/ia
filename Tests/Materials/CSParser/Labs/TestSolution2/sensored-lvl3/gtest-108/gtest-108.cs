using System;
using System.Collections.Generic;
public class Test<T>
{
  protected T item;
  public Test(T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    this.item = item;
    IACSharpSensor.IACSharpSensor.SensorReached(43);
  }
  public IEnumerator<T> GetEnumerator()
  {
    T RNTRNTRNT_8 = item;
    IACSharpSensor.IACSharpSensor.SensorReached(44);
    yield return RNTRNTRNT_8;
    IACSharpSensor.IACSharpSensor.SensorReached(45);
    IACSharpSensor.IACSharpSensor.SensorReached(46);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(47);
    Test<int> test = new Test<int>(3);
    foreach (int a in test) {
      ;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(48);
  }
}
