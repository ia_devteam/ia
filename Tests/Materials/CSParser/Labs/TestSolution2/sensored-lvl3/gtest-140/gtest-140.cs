using System;
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(266);
    int?[] bvals = new int?[] {
      null,
      3,
      4
    };
    foreach (long? x in bvals) {
      Console.WriteLine(x);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(267);
  }
}
