class Test
{
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(200);
    A<int> a = new A<int>(new A<int>.B(D), 3);
    a.Run();
    IACSharpSensor.IACSharpSensor.SensorReached(201);
  }
  public static void D(int y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(202);
    System.Console.WriteLine("Hello " + 3);
    IACSharpSensor.IACSharpSensor.SensorReached(203);
  }
}
class A<T>
{
  public delegate void B(T t);
  protected B _b;
  protected T _value;
  public A(B b, T value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(204);
    _b = b;
    _value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(205);
  }
  public void Run()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    _b(_value);
    IACSharpSensor.IACSharpSensor.SensorReached(207);
  }
}
