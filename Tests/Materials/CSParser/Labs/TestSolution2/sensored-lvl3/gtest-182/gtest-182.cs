interface IMember
{
  int GetId();
}
interface IMethod : IMember
{
}
class C1 : IMethod
{
  public int GetId()
  {
    System.Int32 RNTRNTRNT_149 = 42;
    IACSharpSensor.IACSharpSensor.SensorReached(445);
    return RNTRNTRNT_149;
  }
}
class X
{
  static void foo<a>(a e) where a : IMember
  {
    IACSharpSensor.IACSharpSensor.SensorReached(446);
    e.GetId();
    IACSharpSensor.IACSharpSensor.SensorReached(447);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(448);
    foo<IMethod>(new C1());
    IACSharpSensor.IACSharpSensor.SensorReached(449);
  }
}
