using System;
interface IFoo<T>
{
  T this[int index] { get; set; }
}
public class FooCollection<T> : IFoo<T>
{
  T IFoo<T>.this[int index] {
    get {
      T RNTRNTRNT_65 = default(T);
      IACSharpSensor.IACSharpSensor.SensorReached(208);
      return RNTRNTRNT_65;
    }
    set { }
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(209);
    IFoo<int> foo = new FooCollection<int>();
    int a = foo[3];
    Console.WriteLine(a);
    IACSharpSensor.IACSharpSensor.SensorReached(210);
  }
}
