using System;
using System.Diagnostics;
[Conditional("DEBUG")]
public class TestAttribute : Attribute
{
}
[Conditional("RELEASE")]
public class TestNotAttribute : Attribute
{
}
[Conditional("A")]
[Conditional("DEBUG")]
[Conditional("B")]
public class TestMultiAttribute : Attribute
{
}
[Test()]
class Class1
{
}
[TestNot()]
class Class2
{
}
[TestMulti()]
class Class3
{
}
public class TestClass
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(367);
    if (Attribute.GetCustomAttributes(typeof(Class1)).Length != 1) {
      System.Int32 RNTRNTRNT_108 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(368);
      return RNTRNTRNT_108;
    }
    if (Attribute.GetCustomAttributes(typeof(Class2)).Length != 0) {
      System.Int32 RNTRNTRNT_109 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(369);
      return RNTRNTRNT_109;
    }
    if (Attribute.GetCustomAttributes(typeof(Class3)).Length != 1) {
      System.Int32 RNTRNTRNT_110 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(370);
      return RNTRNTRNT_110;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_111 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(371);
    return RNTRNTRNT_111;
  }
}
