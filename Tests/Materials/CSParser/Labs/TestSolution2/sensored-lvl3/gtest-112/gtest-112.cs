using System;
public interface IComparer<T>
{
  void Compare(T a);
}
class IC : IComparer<Foo<int>>
{
  public void Compare(Foo<int> a)
  {
  }
}
public struct Foo<K>
{
  public K Value;
  public Foo(K value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(69);
    Value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(70);
  }
}
public class List<T>
{
  public virtual void Sort(IComparer<T> c, T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(71);
    Sorting.IntroSort<T>(c, t);
    IACSharpSensor.IACSharpSensor.SensorReached(72);
  }
}
public class Sorting
{
  public static void IntroSort<T>(IComparer<T> c, T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(73);
    new Sorter<T>(c, 4, t).InsertionSort(0);
    IACSharpSensor.IACSharpSensor.SensorReached(74);
  }
  class Sorter<T>
  {
    IComparer<T> c;
    T[] a;
    public Sorter(IComparer<T> c, int size, T item)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(75);
      this.c = c;
      a = new T[size];
      IACSharpSensor.IACSharpSensor.SensorReached(76);
    }
    internal void InsertionSort(int i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(77);
      T other;
      c.Compare(other = a[i]);
      IACSharpSensor.IACSharpSensor.SensorReached(78);
    }
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(79);
    List<Foo<int>> list = new List<Foo<int>>();
    Foo<int> foo = new Foo<int>(3);
    list.Sort(new IC(), foo);
    IACSharpSensor.IACSharpSensor.SensorReached(80);
  }
}
