using System;
public struct KeyValuePair<K, V>
{
  public K key;
  public V value;
  public KeyValuePair(K k, V v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(57);
    key = k;
    value = v;
    IACSharpSensor.IACSharpSensor.SensorReached(58);
  }
  public KeyValuePair(K k)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(59);
    key = k;
    value = default(V);
    IACSharpSensor.IACSharpSensor.SensorReached(60);
  }
}
public class Collection<T>
{
  public readonly T Item;
  public Collection(T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(61);
    this.Item = item;
    IACSharpSensor.IACSharpSensor.SensorReached(62);
  }
  public void Find(ref T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(63);
    item = Item;
    IACSharpSensor.IACSharpSensor.SensorReached(64);
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(65);
    KeyValuePair<int, long> p = new KeyValuePair<int, long>(3);
    KeyValuePair<int, long> q = new KeyValuePair<int, long>(5, 9);
    Collection<KeyValuePair<int, long>> c = new Collection<KeyValuePair<int, long>>(q);
    c.Find(ref p);
    if (p.key != 5) {
      System.Int32 RNTRNTRNT_14 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(66);
      return RNTRNTRNT_14;
    }
    if (p.value != 9) {
      System.Int32 RNTRNTRNT_15 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(67);
      return RNTRNTRNT_15;
    }
    System.Int32 RNTRNTRNT_16 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    return RNTRNTRNT_16;
  }
}
