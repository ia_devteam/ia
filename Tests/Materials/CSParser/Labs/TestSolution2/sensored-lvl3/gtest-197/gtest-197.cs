using System;
using System.Runtime.InteropServices;
namespace Martin.Collections.Generic
{
  [Serializable()]
  public abstract class EqualityComparer<T> : IEqualityComparer<T>
  {
    static EqualityComparer()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(514);
      if (typeof(IEquatable<T>).IsAssignableFrom(typeof(T))) {
        _default = (EqualityComparer<T>)Activator.CreateInstance(typeof(IEquatableOfTEqualityComparer<>).MakeGenericType(typeof(T)));
      } else {
        _default = new DefaultComparer();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(515);
    }
    public abstract int GetHashCode(T obj);
    public abstract bool Equals(T x, T y);
    static readonly EqualityComparer<T> _default;
    public static EqualityComparer<T> Default {
      get {
        EqualityComparer<T> RNTRNTRNT_161 = _default;
        IACSharpSensor.IACSharpSensor.SensorReached(516);
        return RNTRNTRNT_161;
      }
    }
    [Serializable()]
    class DefaultComparer : EqualityComparer<T>
    {
      public override int GetHashCode(T obj)
      {
        System.Int32 RNTRNTRNT_162 = obj.GetHashCode();
        IACSharpSensor.IACSharpSensor.SensorReached(517);
        return RNTRNTRNT_162;
      }
      public override bool Equals(T x, T y)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(518);
        if (x == null) {
          System.Boolean RNTRNTRNT_163 = y == null;
          IACSharpSensor.IACSharpSensor.SensorReached(519);
          return RNTRNTRNT_163;
        }
        System.Boolean RNTRNTRNT_164 = x.Equals(y);
        IACSharpSensor.IACSharpSensor.SensorReached(520);
        return RNTRNTRNT_164;
      }
    }
  }
  [Serializable()]
  class IEquatableOfTEqualityComparer<T> : EqualityComparer<T> where T : IEquatable<T>
  {
    public override int GetHashCode(T obj)
    {
      System.Int32 RNTRNTRNT_165 = obj.GetHashCode();
      IACSharpSensor.IACSharpSensor.SensorReached(521);
      return RNTRNTRNT_165;
    }
    public override bool Equals(T x, T y)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(522);
      if (x == null) {
        System.Boolean RNTRNTRNT_166 = y == null;
        IACSharpSensor.IACSharpSensor.SensorReached(523);
        return RNTRNTRNT_166;
      }
      System.Boolean RNTRNTRNT_167 = x.Equals(y);
      IACSharpSensor.IACSharpSensor.SensorReached(524);
      return RNTRNTRNT_167;
    }
  }
  public interface IEqualityComparer<T>
  {
    bool Equals(T x, T y);
    int GetHashCode(T obj);
  }
  class X
  {
    static void Main()
    {
    }
  }
}
