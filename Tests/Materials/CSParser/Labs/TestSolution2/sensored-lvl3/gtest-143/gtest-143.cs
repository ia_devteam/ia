using System;
class X
{
  static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(287);
    int? a = 5;
    int? b = a++;
    if (a != 6) {
      System.Int32 RNTRNTRNT_79 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(288);
      return RNTRNTRNT_79;
    }
    if (b != 5) {
      System.Int32 RNTRNTRNT_80 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(289);
      return RNTRNTRNT_80;
    }
    int? c = ++a;
    if (c != 7) {
      System.Int32 RNTRNTRNT_81 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(290);
      return RNTRNTRNT_81;
    }
    b++;
    ++b;
    if (b != 7) {
      System.Int32 RNTRNTRNT_82 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(291);
      return RNTRNTRNT_82;
    }
    int? d = b++ + ++a;
    if (a != 8) {
      System.Int32 RNTRNTRNT_83 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(292);
      return RNTRNTRNT_83;
    }
    if (b != 8) {
      System.Int32 RNTRNTRNT_84 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(293);
      return RNTRNTRNT_84;
    }
    if (d != 15) {
      System.Int32 RNTRNTRNT_85 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(294);
      return RNTRNTRNT_85;
    }
    System.Int32 RNTRNTRNT_86 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(295);
    return RNTRNTRNT_86;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(296);
    int result = Test();
    if (result != 0) {
      Console.WriteLine("ERROR: {0}", result);
    }
    System.Int32 RNTRNTRNT_87 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(297);
    return RNTRNTRNT_87;
  }
}
