class MainClass
{
  class Gen<T>
  {
    public void Test()
    {
    }
  }
  class Der : Gen<int>
  {
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(11);
    object o = new Der();
    Gen<int> b = (Gen<int>)o;
    b.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(12);
  }
}
