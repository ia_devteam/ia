class list<A>
{
  public class Cons<T> : list<T>
  {
  }
  public class Nil<T> : list<T>
  {
  }
}
class C
{
  public static void Rev<T>(list<T> y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(377);
    if (y is list<object>.Cons<T>) {
      System.Console.WriteLine("Cons");
    }
    if (y is list<object>.Nil<T>) {
      System.Console.WriteLine("Nil");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(378);
  }
}
class M
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(379);
    C.Rev(new list<object>.Cons<string>());
    C.Rev(new list<object>.Nil<string>());
    IACSharpSensor.IACSharpSensor.SensorReached(380);
  }
}
