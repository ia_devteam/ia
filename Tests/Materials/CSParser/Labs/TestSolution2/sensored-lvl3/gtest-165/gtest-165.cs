using System;
using System.Collections;
class X
{
  delegate void A();
  static IEnumerable GetIt(int[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(350);
    foreach (int arg in args) {
      Console.WriteLine("OUT: {0}", arg);
      A a = delegate {
        Console.WriteLine("arg: {0}", arg);
        IACSharpSensor.IACSharpSensor.SensorReached(351);
        return;
      };
      a();
      IEnumerable RNTRNTRNT_99 = arg;
      IACSharpSensor.IACSharpSensor.SensorReached(352);
      yield return RNTRNTRNT_99;
      IACSharpSensor.IACSharpSensor.SensorReached(353);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(354);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(355);
    int total = 0;
    foreach (int i in GetIt(new int[] {
      1,
      2,
      3
    })) {
      Console.WriteLine("Got: " + i);
      total += i;
    }
    if (total != 6) {
      System.Int32 RNTRNTRNT_100 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(356);
      return RNTRNTRNT_100;
    }
    System.Int32 RNTRNTRNT_101 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(357);
    return RNTRNTRNT_101;
  }
}
