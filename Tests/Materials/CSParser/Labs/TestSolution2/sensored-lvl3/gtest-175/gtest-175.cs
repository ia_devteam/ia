using System;
public class Foo
{
}
public class X
{
  public static Foo Test(Foo foo, Foo bar)
  {
    Foo RNTRNTRNT_115 = foo ?? bar;
    IACSharpSensor.IACSharpSensor.SensorReached(397);
    return RNTRNTRNT_115;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(398);
    Foo[] array = new Foo[1];
    Foo foo = new Foo();
    Foo bar = Test(array[0], foo);
    if (bar == null) {
      System.Int32 RNTRNTRNT_116 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(399);
      return RNTRNTRNT_116;
    }
    System.Int32 RNTRNTRNT_117 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(400);
    return RNTRNTRNT_117;
  }
}
