using System;
public class Test<T>
{
  private T[,] data;
  public Test(T[,] data)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(300);
    this.data = data;
    IACSharpSensor.IACSharpSensor.SensorReached(301);
  }
}
public class Program
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(302);
    Test<double> test = new Test<double>(new double[2, 2]);
    IACSharpSensor.IACSharpSensor.SensorReached(303);
  }
}
