using System;
public interface IList<R>
{
  int Map<S>(S item);
}
public class List<T> : IList<T>
{
  public int Map<U>(U item)
  {
    System.Int32 RNTRNTRNT_9 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(51);
    return RNTRNTRNT_9;
  }
}
public class SpecialList<V> : IList<V>
{
  public int Map<W>(W item)
  {
    System.Int32 RNTRNTRNT_10 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(52);
    return RNTRNTRNT_10;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    IList<int> list = new List<int>();
    int result = list.Map("Hello");
    if (result != 1) {
      System.Int32 RNTRNTRNT_11 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      return RNTRNTRNT_11;
    }
    IList<int> list2 = new SpecialList<int>();
    int result2 = list2.Map("World");
    if (result2 != 2) {
      System.Int32 RNTRNTRNT_12 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(55);
      return RNTRNTRNT_12;
    }
    System.Int32 RNTRNTRNT_13 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(56);
    return RNTRNTRNT_13;
  }
}
