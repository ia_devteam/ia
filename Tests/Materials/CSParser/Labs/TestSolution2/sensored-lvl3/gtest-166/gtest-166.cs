using System;
unsafe public struct TestNew
{
  private fixed char test_1[128];
  public fixed bool test2[4];
  public fixed int T[2];
  public fixed bool test20[4], test21[40];
  private int foo, foo2;
  public void SetTest()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(358);
    fixed (char* c = test_1) {
      *c = 'g';
    }
    IACSharpSensor.IACSharpSensor.SensorReached(359);
  }
}
public class C
{
  unsafe static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(360);
    TestNew tt = new TestNew();
    tt.SetTest();
    tt.test2[2] = false;
    tt.T[1] = 5544;
    if (tt.T[1] != 5544) {
      System.Int32 RNTRNTRNT_102 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(361);
      return RNTRNTRNT_102;
    }
    ExternalStruct es = new ExternalStruct();
    es.double_buffer[1] = 999999.8888;
    es.double_buffer[0] = es.double_buffer[1];
    if (Attribute.GetCustomAttribute(typeof(TestNew).GetField("test2"), typeof(System.Runtime.CompilerServices.FixedBufferAttribute)) == null) {
      System.Int32 RNTRNTRNT_103 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(362);
      return RNTRNTRNT_103;
    }
    if (typeof(TestNew).GetNestedTypes().Length != 5) {
      System.Int32 RNTRNTRNT_104 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(363);
      return RNTRNTRNT_104;
    }
    foreach (Type t in typeof(TestNew).GetNestedTypes()) {
      if (Attribute.GetCustomAttribute(t, typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute)) == null) {
        System.Int32 RNTRNTRNT_105 = 4;
        IACSharpSensor.IACSharpSensor.SensorReached(364);
        return RNTRNTRNT_105;
      }
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_106 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(365);
    return RNTRNTRNT_106;
  }
  public static int Main()
  {
    System.Int32 RNTRNTRNT_107 = Test();
    IACSharpSensor.IACSharpSensor.SensorReached(366);
    return RNTRNTRNT_107;
  }
}
