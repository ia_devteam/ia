using System;
using System.Collections.Generic;
class X
{
  static int[] x = new int[] {
    100,
    200
  };
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    IEnumerator<int> enumerator = X<int>.Y(x);
    int sum = 0;
    while (enumerator.MoveNext()) {
      sum += enumerator.Current;
    }
    if (sum != 300) {
      System.Int32 RNTRNTRNT_122 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(408);
      return RNTRNTRNT_122;
    }
    if (X<int>.Z(x, 0) != 100) {
      System.Int32 RNTRNTRNT_123 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(409);
      return RNTRNTRNT_123;
    }
    if (X<int>.Z(x, 1) != 200) {
      System.Int32 RNTRNTRNT_124 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(410);
      return RNTRNTRNT_124;
    }
    System.Int32 RNTRNTRNT_125 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(411);
    return RNTRNTRNT_125;
  }
}
class X<T>
{
  public static IEnumerator<T> Y(IEnumerable<T> x)
  {
    IEnumerator<T> RNTRNTRNT_126 = x.GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(412);
    return RNTRNTRNT_126;
  }
  public static T Z(IList<T> x, int index)
  {
    T RNTRNTRNT_127 = x[index];
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    return RNTRNTRNT_127;
  }
}
