using System;
namespace Martin
{
  public struct A
  {
    public readonly long Data;
    public A(long data)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(254);
      this.Data = data;
      IACSharpSensor.IACSharpSensor.SensorReached(255);
    }
    public static explicit operator B(A a)
    {
      B RNTRNTRNT_75 = new B((int)a.Data);
      IACSharpSensor.IACSharpSensor.SensorReached(256);
      return RNTRNTRNT_75;
    }
  }
  public struct B
  {
    public readonly int Data;
    public B(int data)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(257);
      this.Data = data;
      IACSharpSensor.IACSharpSensor.SensorReached(258);
    }
    public static implicit operator A(B b)
    {
      A RNTRNTRNT_76 = new A(b.Data);
      IACSharpSensor.IACSharpSensor.SensorReached(259);
      return RNTRNTRNT_76;
    }
  }
  class X
  {
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(260);
      B? b = new B(5);
      A? a = b;
      B? c = (B?)a;
      B? d = (Martin.B?)a;
      IACSharpSensor.IACSharpSensor.SensorReached(261);
    }
  }
}
