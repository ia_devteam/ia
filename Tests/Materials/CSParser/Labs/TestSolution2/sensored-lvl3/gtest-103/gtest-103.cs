public interface IFoo<T>
{
}
public class Foo : IFoo<string>
{
}
public class Hello
{
  public void World<U>(U u, IFoo<U> foo)
  {
  }
  public void World<V>(IFoo<V> foo)
  {
  }
  public void Test(Foo foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(9);
    World("Canada", foo);
    World(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(10);
  }
}
class X
{
  static void Main()
  {
  }
}
