using System;
namespace C5
{
  public interface ICollection<T>
  {
    void Test<U>();
  }
  public abstract class ArrayBase<T> : ICollection<T>
  {
    void ICollection<T>.Test()
    {
    }
  }
  public class ArrayList<V> : ArrayBase<V>
  {
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
    C5.ArrayList<int> array = new C5.ArrayList<int>();
    IACSharpSensor.IACSharpSensor.SensorReached(2);
  }
}
