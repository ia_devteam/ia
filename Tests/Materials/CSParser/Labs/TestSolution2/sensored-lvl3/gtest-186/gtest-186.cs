using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
[Serializable()]
public class Tuple<a, b>
{
  public a field1;
  public b field2;
  public Tuple(a x, b y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(462);
    field1 = x;
    field2 = y;
    IACSharpSensor.IACSharpSensor.SensorReached(463);
  }
}
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(464);
    TestSimpleObject obj = new TestSimpleObject();
    Console.WriteLine("Before serialization the object contains: ");
    obj.Print();
    Stream stream = File.Open("data.xml", FileMode.Create);
    BinaryFormatter formatter = new BinaryFormatter();
    formatter.Serialize(stream, obj);
    stream.Close();
    obj = null;
    stream = File.Open("data.xml", FileMode.Open);
    formatter = new BinaryFormatter();
    obj = (TestSimpleObject)formatter.Deserialize(stream);
    stream.Close();
    Console.WriteLine("");
    Console.WriteLine("After deserialization the object contains: ");
    obj.Print();
    IACSharpSensor.IACSharpSensor.SensorReached(465);
  }
}
[Serializable()]
public class TestSimpleObject
{
  public Tuple<string, int> member6;
  public TestSimpleObject()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(466);
    member6 = new Tuple<string, int>("aa", 22);
    IACSharpSensor.IACSharpSensor.SensorReached(467);
  }
  public void Print()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(468);
    Console.WriteLine("member6 = '{0}'", member6);
    IACSharpSensor.IACSharpSensor.SensorReached(469);
  }
}
