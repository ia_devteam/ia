using System;
public class Test
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(3);
    SimpleStruct<string> s = new SimpleStruct<string>();
    IACSharpSensor.IACSharpSensor.SensorReached(4);
  }
}
public struct SimpleStruct<T>
{
  T data;
  public SimpleStruct(T data)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(5);
    this.data = data;
    IACSharpSensor.IACSharpSensor.SensorReached(6);
  }
}
