using System;
public interface IFoo
{
  IFoo Hello();
}
public interface IFoo<T> : IFoo
{
  new IFoo<T> Hello();
}
public interface ICollectionValue<T> : IFoo<T>
{
}
public interface ICollection<T> : ICollectionValue<T>
{
}
public abstract class EnumerableBase<T> : IFoo<T>
{
  public abstract IFoo<T> Hello();
  IFoo IFoo.Hello()
  {
    IFoo RNTRNTRNT_90 = Hello();
    IACSharpSensor.IACSharpSensor.SensorReached(316);
    return RNTRNTRNT_90;
  }
}
public abstract class CollectionBase<T> : EnumerableBase<T>
{
}
public class HashBag<T> : CollectionBase<T>, ICollection<T>
{
  public override IFoo<T> Hello()
  {
    IFoo<T> RNTRNTRNT_91 = this;
    IACSharpSensor.IACSharpSensor.SensorReached(317);
    return RNTRNTRNT_91;
  }
}
class X
{
  static void Main()
  {
  }
}
