namespace A
{
  public struct KeyValuePair<X, Y>
  {
    public KeyValuePair(X x, Y y)
    {
    }
  }
  public interface IComparer<T>
  {
    int Compare(T x);
  }
  public class KeyValuePairComparer<K, V> : IComparer<KeyValuePair<K, V>>
  {
    public int Compare(KeyValuePair<K, V> a)
    {
      System.Int32 RNTRNTRNT_1 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(13);
      return RNTRNTRNT_1;
    }
  }
  public class TreeBag<T>
  {
    IComparer<T> comparer;
    public TreeBag(IComparer<T> comparer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(14);
      this.comparer = comparer;
      IACSharpSensor.IACSharpSensor.SensorReached(15);
    }
    public int Find(ref T item)
    {
      System.Int32 RNTRNTRNT_2 = comparer.Compare(item);
      IACSharpSensor.IACSharpSensor.SensorReached(16);
      return RNTRNTRNT_2;
    }
  }
  public class X
  {
    public static void Test()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(17);
      KeyValuePair<int, int> pair = new KeyValuePair<int, int>(3, 89);
      KeyValuePairComparer<int, int> comparer = new KeyValuePairComparer<int, int>();
      TreeBag<KeyValuePair<int, int>> bag = new TreeBag<KeyValuePair<int, int>>(comparer);
      bag.Find(ref pair);
      IACSharpSensor.IACSharpSensor.SensorReached(18);
    }
  }
}
namespace B
{
  public class KeyValuePair<X, Y>
  {
    public KeyValuePair(X x, Y y)
    {
    }
  }
  public interface IComparer<T>
  {
    int Compare(T x);
  }
  public class KeyValuePairComparer<K, V> : IComparer<KeyValuePair<K, V>>
  {
    public int Compare(KeyValuePair<K, V> a)
    {
      System.Int32 RNTRNTRNT_3 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(19);
      return RNTRNTRNT_3;
    }
  }
  public class TreeBag<T>
  {
    IComparer<T> comparer;
    public TreeBag(IComparer<T> comparer)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(20);
      this.comparer = comparer;
      IACSharpSensor.IACSharpSensor.SensorReached(21);
    }
    public int Find(ref T item)
    {
      System.Int32 RNTRNTRNT_4 = comparer.Compare(item);
      IACSharpSensor.IACSharpSensor.SensorReached(22);
      return RNTRNTRNT_4;
    }
  }
  public class X
  {
    public static void Test()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(23);
      KeyValuePair<int, int> pair = new KeyValuePair<int, int>(3, 89);
      KeyValuePairComparer<int, int> comparer = new KeyValuePairComparer<int, int>();
      TreeBag<KeyValuePair<int, int>> bag = new TreeBag<KeyValuePair<int, int>>(comparer);
      bag.Find(ref pair);
      IACSharpSensor.IACSharpSensor.SensorReached(24);
    }
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(25);
    A.X.Test();
    B.X.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(26);
  }
}
