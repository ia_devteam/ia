using System;
using System.Collections.Generic;
internal class RedBlackTree<S>
{
  public delegate int RangeTester(S item);
  public IEnumerable<S> EnumerateRange(RangeTester rangeTester)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(505);
    yield break;
  }
}
public class OrderedMultiDictionary<T, U>
{
  private RedBlackTree<KeyValuePair<T, U>> tree;
  private IEnumerator<T> EnumerateKeys(RedBlackTree<KeyValuePair<T, U>>.RangeTester rangeTester)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(506);
    tree.EnumerateRange(rangeTester);
    IACSharpSensor.IACSharpSensor.SensorReached(507);
    yield break;
  }
}
class X
{
  static void Main()
  {
  }
}
