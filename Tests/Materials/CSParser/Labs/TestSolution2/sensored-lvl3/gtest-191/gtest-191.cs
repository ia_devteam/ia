using System;
using System.Collections.Generic;
namespace Test1
{
  public static class Test
  {
    public static IEnumerable<T> Replace<T>()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(487);
      yield break;
    }
  }
}
namespace Test2
{
  public class Test<S>
  {
    public static IEnumerable<T> Replace<T>()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(488);
      yield break;
    }
  }
}
namespace Test3
{
  public class Test<S>
  {
    public static IEnumerable<KeyValuePair<S, T>> Replace<T>(IEnumerable<T> a, IEnumerable<S> b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(489);
      yield break;
    }
  }
}
namespace Test4
{
  public class Test
  {
    public static IEnumerable<T> Replace<T>() where T : class
    {
      IACSharpSensor.IACSharpSensor.SensorReached(490);
      yield break;
    }
  }
}
namespace Test5
{
  public class Test
  {
    public static IEnumerable<T> Replace<T>(T t)
    {
      T RNTRNTRNT_155 = t;
      IACSharpSensor.IACSharpSensor.SensorReached(491);
      yield return RNTRNTRNT_155;
      IACSharpSensor.IACSharpSensor.SensorReached(492);
      IACSharpSensor.IACSharpSensor.SensorReached(493);
    }
  }
}
namespace Test6
{
  public class Test
  {
    public static IEnumerable<T> Replace<T>(T t)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(494);
      T u = t;
      T RNTRNTRNT_156 = u;
      IACSharpSensor.IACSharpSensor.SensorReached(495);
      yield return RNTRNTRNT_156;
      IACSharpSensor.IACSharpSensor.SensorReached(496);
      IACSharpSensor.IACSharpSensor.SensorReached(497);
    }
  }
}
namespace Test7
{
  public class Test
  {
    public static IEnumerable<T[]> Replace<T>(T[] t)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(498);
      T[] array = t;
      T[] RNTRNTRNT_157 = array;
      IACSharpSensor.IACSharpSensor.SensorReached(499);
      yield return RNTRNTRNT_157;
      IACSharpSensor.IACSharpSensor.SensorReached(500);
      IACSharpSensor.IACSharpSensor.SensorReached(501);
    }
  }
}
class X
{
  static void Main()
  {
  }
}
