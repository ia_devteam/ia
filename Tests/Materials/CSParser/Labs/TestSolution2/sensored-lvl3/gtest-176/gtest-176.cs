class lis<a>
{
}
abstract class fn<a, b, r>
{
  public abstract r apply(a x, b y);
}
class fn1<a> : fn<lis<a>, lis<a>, lis<a>>
{
  public override lis<a> apply(lis<a> x, lis<a> y)
  {
    lis<a> RNTRNTRNT_118 = M.RevAppend(x, y);
    IACSharpSensor.IACSharpSensor.SensorReached(401);
    return RNTRNTRNT_118;
  }
}
class M
{
  public static b FoldLeft<a, b>(a x, b acc, fn<a, b, b> f)
  {
    b RNTRNTRNT_119 = f.apply(x, acc);
    IACSharpSensor.IACSharpSensor.SensorReached(402);
    return RNTRNTRNT_119;
  }
  public static lis<a> RevAppend<a>(lis<a> x, lis<a> y)
  {
    lis<a> RNTRNTRNT_120 = x;
    IACSharpSensor.IACSharpSensor.SensorReached(403);
    return RNTRNTRNT_120;
  }
  public static lis<lis<a>> Concat<a>(lis<lis<a>> l)
  {
    lis<lis<a>> RNTRNTRNT_121 = FoldLeft<lis<lis<a>>, lis<lis<a>>>(l, new lis<lis<a>>(), new fn1<lis<a>>());
    IACSharpSensor.IACSharpSensor.SensorReached(404);
    return RNTRNTRNT_121;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(405);
    M.Concat(new lis<lis<string>>());
    IACSharpSensor.IACSharpSensor.SensorReached(406);
  }
}
