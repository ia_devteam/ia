using System;
using System.Runtime.InteropServices;
[module: DefaultCharSet(CharSet.Unicode)]
struct foo1
{
}
enum E
{
}
[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
struct foo2
{
}
[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
delegate void D();
class C
{
  public class CC
  {
  }
}
class Program
{
  [DllImport("bah")]
  public static extern void test();
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(417);
    DllImportAttribute dia = Attribute.GetCustomAttribute(typeof(Program).GetMethod("test"), typeof(DllImportAttribute)) as DllImportAttribute;
    if (dia == null) {
      System.Int32 RNTRNTRNT_131 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(418);
      return RNTRNTRNT_131;
    }
    if (dia.CharSet != CharSet.Unicode) {
      System.Int32 RNTRNTRNT_132 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(419);
      return RNTRNTRNT_132;
    }
    if (!typeof(C).IsUnicodeClass) {
      System.Int32 RNTRNTRNT_133 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(420);
      return RNTRNTRNT_133;
    }
    if (!typeof(C.CC).IsUnicodeClass) {
      System.Int32 RNTRNTRNT_134 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(421);
      return RNTRNTRNT_134;
    }
    if (!typeof(D).IsUnicodeClass) {
      System.Int32 RNTRNTRNT_135 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(422);
      return RNTRNTRNT_135;
    }
    if (!typeof(E).IsUnicodeClass) {
      System.Int32 RNTRNTRNT_136 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(423);
      return RNTRNTRNT_136;
    }
    if (!typeof(foo1).IsUnicodeClass) {
      System.Int32 RNTRNTRNT_137 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(424);
      return RNTRNTRNT_137;
    }
    if (!typeof(foo2).IsAutoClass) {
      System.Int32 RNTRNTRNT_138 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(425);
      return RNTRNTRNT_138;
    }
    System.Int32 RNTRNTRNT_139 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(426);
    return RNTRNTRNT_139;
  }
}
