public class B<T>
{
  public int Add(T obj)
  {
    System.Int32 RNTRNTRNT_64 = -1;
    IACSharpSensor.IACSharpSensor.SensorReached(195);
    return RNTRNTRNT_64;
  }
  public void AddRange(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(196);
    T obj = (T)o;
    Add(obj);
    IACSharpSensor.IACSharpSensor.SensorReached(197);
  }
}
public interface IA
{
}
public class A : IA
{
}
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(198);
    B<IA> aux = new B<IA>();
    aux.AddRange(new A());
    IACSharpSensor.IACSharpSensor.SensorReached(199);
  }
}
