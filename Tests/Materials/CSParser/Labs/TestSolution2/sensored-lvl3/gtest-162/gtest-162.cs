using System;
using System.Reflection;
public class Generic<T>
{
  public delegate void Delegate(Generic<T> proxy, T value);
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(341);
    Type t = typeof(Generic<bool>);
    MemberInfo[] mi = t.FindMembers(MemberTypes.NestedType, BindingFlags.Static | BindingFlags.Public | BindingFlags.DeclaredOnly, null, null);
    System.Int32 RNTRNTRNT_97 = mi.Length - 1;
    IACSharpSensor.IACSharpSensor.SensorReached(342);
    return RNTRNTRNT_97;
  }
}
