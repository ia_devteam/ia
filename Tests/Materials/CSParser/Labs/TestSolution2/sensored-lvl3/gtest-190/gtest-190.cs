using System;
using System.Collections.Generic;
public class Foo<T>
{
  public abstract class Node
  {
  }
  public class ConcatNode : Node
  {
  }
  public Node GetRoot()
  {
    Node RNTRNTRNT_154 = new ConcatNode();
    IACSharpSensor.IACSharpSensor.SensorReached(482);
    return RNTRNTRNT_154;
  }
  public void Test(Node root)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(483);
    ConcatNode concat = root as ConcatNode;
    Console.WriteLine(concat);
    IACSharpSensor.IACSharpSensor.SensorReached(484);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(485);
    Foo<int> foo = new Foo<int>();
    Foo<int>.Node root = foo.GetRoot();
    foo.Test(root);
    IACSharpSensor.IACSharpSensor.SensorReached(486);
  }
}
