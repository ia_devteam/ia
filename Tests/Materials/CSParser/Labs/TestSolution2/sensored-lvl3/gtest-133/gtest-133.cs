using System;
struct S
{
  private int x;
  public int X {
    get {
      System.Int32 RNTRNTRNT_72 = x;
      IACSharpSensor.IACSharpSensor.SensorReached(238);
      return RNTRNTRNT_72;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(239);
      this.x = value;
      IACSharpSensor.IACSharpSensor.SensorReached(240);
    }
  }
  public void Set(int x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(241);
    this.x = x;
    IACSharpSensor.IACSharpSensor.SensorReached(242);
  }
}
class MyTest
{
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(243);
    S s = new S();
    s.Set(11);
    Console.WriteLine("s.X = {0}", s.X);
    S? ns = s;
    Console.WriteLine("s.X = {0} ns.Value.X = {1}", s.X, ns.Value.X);
    ns.Value.Set(22);
    Console.WriteLine("s.X = {0} ns.Value.X = {1}", s.X, ns.Value.X);
    s.Set(33);
    Console.WriteLine("s.X = {0} ns.Value.X = {1}", s.X, ns.Value.X);
    IACSharpSensor.IACSharpSensor.SensorReached(244);
  }
}
