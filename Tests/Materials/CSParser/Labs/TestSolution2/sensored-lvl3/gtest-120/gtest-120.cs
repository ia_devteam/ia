using System;
public interface ISequenced<T>
{
  bool Equals(ISequenced<T> that);
}
public class SequencedHasher<S, W> where S : ISequenced<W>
{
  public bool Equals(S i1, S i2)
  {
    System.Boolean RNTRNTRNT_62 = i1 == null ? i2 == null : i1.Equals(i2);
    IACSharpSensor.IACSharpSensor.SensorReached(191);
    return RNTRNTRNT_62;
  }
}
public class Sequenced<T> : ISequenced<T>
{
  public bool Equals(ISequenced<T> that)
  {
    System.Boolean RNTRNTRNT_63 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(192);
    return RNTRNTRNT_63;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(193);
    Sequenced<int> s = new Sequenced<int>();
    SequencedHasher<Sequenced<int>, int> hasher = new SequencedHasher<Sequenced<int>, int>();
    hasher.Equals(s, s);
    IACSharpSensor.IACSharpSensor.SensorReached(194);
  }
}
