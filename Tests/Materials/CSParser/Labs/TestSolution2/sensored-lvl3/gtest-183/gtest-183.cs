using System;
using System.Collections.Generic;
namespace test
{
  class Test<T>
  {
    public IEnumerable<T> Lookup(T item)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(450);
      byte i = 3;
      byte j = 3;
      T RNTRNTRNT_150 = item;
      IACSharpSensor.IACSharpSensor.SensorReached(451);
      yield return RNTRNTRNT_150;
      IACSharpSensor.IACSharpSensor.SensorReached(452);
      IACSharpSensor.IACSharpSensor.SensorReached(453);
    }
  }
  class Program
  {
    public static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(454);
      Test<string> test = new Test<string>();
      foreach (string s in test.Lookup("hi")) {
        Console.WriteLine(s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(455);
    }
  }
}
