class C<A>
{
  public static void foo<B>(C<B> x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(381);
    D.append(x);
    IACSharpSensor.IACSharpSensor.SensorReached(382);
  }
}
class D
{
  public static void append<A>(C<A> x)
  {
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(383);
    C<object>.foo<int>(null);
    IACSharpSensor.IACSharpSensor.SensorReached(384);
  }
}
