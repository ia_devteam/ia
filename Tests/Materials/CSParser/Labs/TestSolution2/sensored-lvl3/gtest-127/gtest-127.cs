public class A<T>
{
  public delegate void Changed(A<T> a);
  protected event Changed _changed;
  public void Register(Changed changed)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(219);
    _changed += changed;
    _changed(this);
    IACSharpSensor.IACSharpSensor.SensorReached(220);
  }
}
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(221);
    A<int> a = new A<int>();
    a.Register(new A<int>.Changed(Del));
    IACSharpSensor.IACSharpSensor.SensorReached(222);
  }
  public static void Del(A<int> a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(223);
    System.Console.WriteLine("Solved");
    IACSharpSensor.IACSharpSensor.SensorReached(224);
  }
}
