using System;
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(268);
    int?[] bvals = new int?[] {
      null,
      3,
      4
    };
    foreach (int? x in bvals) {
      Console.WriteLine(x);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(269);
  }
}
