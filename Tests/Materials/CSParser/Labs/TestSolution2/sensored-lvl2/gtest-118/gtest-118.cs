using System;
interface AddMul<A, R>
{
  R Add(A e);
  R Mul(A e);
}
class Polynomial<E> : AddMul<E, Polynomial<E>>, AddMul<Polynomial<E>, Polynomial<E>> where E : AddMul<E, E>, new()
{
  private readonly E[] cs;
  public Polynomial()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(267);
    this.cs = new E[0];
    IACSharpSensor.IACSharpSensor.SensorReached(268);
  }
  public Polynomial(E[] cs)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(269);
    this.cs = cs;
    IACSharpSensor.IACSharpSensor.SensorReached(270);
  }
  public Polynomial<E> Add(Polynomial<E> that)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(271);
    int newlen = Math.Max(this.cs.Length, that.cs.Length);
    int minlen = Math.Min(this.cs.Length, that.cs.Length);
    E[] newcs = new E[newlen];
    IACSharpSensor.IACSharpSensor.SensorReached(272);
    if (this.cs.Length <= that.cs.Length) {
      IACSharpSensor.IACSharpSensor.SensorReached(273);
      for (int i = 0; i < minlen; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(274);
        newcs[i] = this.cs[i].Add(that.cs[i]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(275);
      for (int i = minlen; i < newlen; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(276);
        newcs[i] = that.cs[i];
      }
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(277);
      for (int i = 0; i < minlen; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(278);
        newcs[i] = this.cs[i].Add(that.cs[i]);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(279);
      for (int i = minlen; i < newlen; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(280);
        newcs[i] = this.cs[i];
      }
    }
    Polynomial<E> RNTRNTRNT_54 = new Polynomial<E>(newcs);
    IACSharpSensor.IACSharpSensor.SensorReached(281);
    return RNTRNTRNT_54;
  }
  public Polynomial<E> Add(E that)
  {
    Polynomial<E> RNTRNTRNT_55 = this.Add(new Polynomial<E>(new E[] { that }));
    IACSharpSensor.IACSharpSensor.SensorReached(282);
    return RNTRNTRNT_55;
  }
  public Polynomial<E> Mul(E that)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    E[] newcs = new E[cs.Length];
    IACSharpSensor.IACSharpSensor.SensorReached(284);
    for (int i = 0; i < cs.Length; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(285);
      newcs[i] = that.Mul(cs[i]);
    }
    Polynomial<E> RNTRNTRNT_56 = new Polynomial<E>(newcs);
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    return RNTRNTRNT_56;
  }
  public Polynomial<E> Mul(Polynomial<E> that)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(287);
    int newlen = Math.Max(1, this.cs.Length + that.cs.Length - 1);
    E[] newcs = new E[newlen];
    IACSharpSensor.IACSharpSensor.SensorReached(288);
    for (int i = 0; i < newlen; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(289);
      E sum = new E();
      int start = Math.Max(0, i - that.cs.Length + 1);
      int stop = Math.Min(i, this.cs.Length - 1);
      IACSharpSensor.IACSharpSensor.SensorReached(290);
      for (int j = start; j <= stop; j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(291);
        sum = sum.Add(this.cs[j].Mul(that.cs[i - j]));
      }
      IACSharpSensor.IACSharpSensor.SensorReached(292);
      newcs[i] = sum;
    }
    Polynomial<E> RNTRNTRNT_57 = new Polynomial<E>(newcs);
    IACSharpSensor.IACSharpSensor.SensorReached(293);
    return RNTRNTRNT_57;
  }
  public E Eval(E x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(294);
    E res = new E();
    IACSharpSensor.IACSharpSensor.SensorReached(295);
    for (int j = cs.Length - 1; j >= 0; j--) {
      IACSharpSensor.IACSharpSensor.SensorReached(296);
      res = res.Mul(x).Add(cs[j]);
    }
    E RNTRNTRNT_58 = res;
    IACSharpSensor.IACSharpSensor.SensorReached(297);
    return RNTRNTRNT_58;
  }
}
struct Int : AddMul<Int, Int>
{
  private readonly int i;
  public Int(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(298);
    this.i = i;
    IACSharpSensor.IACSharpSensor.SensorReached(299);
  }
  public Int Add(Int that)
  {
    Int RNTRNTRNT_59 = new Int(this.i + that.i);
    IACSharpSensor.IACSharpSensor.SensorReached(300);
    return RNTRNTRNT_59;
  }
  public Int Mul(Int that)
  {
    Int RNTRNTRNT_60 = new Int(this.i * that.i);
    IACSharpSensor.IACSharpSensor.SensorReached(301);
    return RNTRNTRNT_60;
  }
  public override String ToString()
  {
    String RNTRNTRNT_61 = i.ToString();
    IACSharpSensor.IACSharpSensor.SensorReached(302);
    return RNTRNTRNT_61;
  }
}
class TestPolynomial
{
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(303);
    Polynomial<Int> ip = new Polynomial<Int>(new Int[] {
      new Int(2),
      new Int(5),
      new Int(1)
    });
    Console.WriteLine(ip.Eval(new Int(10)));
    Console.WriteLine(ip.Add(ip).Eval(new Int(10)));
    Console.WriteLine(ip.Mul(ip).Eval(new Int(10)));
    IACSharpSensor.IACSharpSensor.SensorReached(304);
  }
}
