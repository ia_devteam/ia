using System;
public class Tests
{
  unsafe public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(305);
    Console.WriteLine(typeof(void).Name);
    Console.WriteLine(typeof(void*).Name);
    Console.WriteLine(typeof(void**).Name);
    IACSharpSensor.IACSharpSensor.SensorReached(306);
  }
}
