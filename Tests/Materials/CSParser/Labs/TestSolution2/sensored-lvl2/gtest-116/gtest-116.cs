using System;
namespace Slow
{
  public interface ITest
  {
    void DoNothing<T>() where T : class;
  }
  public class Test : ITest
  {
    public void DoNothing<T>() where T : class
    {
      IACSharpSensor.IACSharpSensor.SensorReached(228);
      T x = null;
      IACSharpSensor.IACSharpSensor.SensorReached(229);
    }
  }
  class Program
  {
    static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(230);
      const int iterations = 10000;
      Test test = new Test();
      DateTime start = DateTime.Now;
      Console.Write("Calling Test.DoNothing<Program>() on an object reference...  ");
      IACSharpSensor.IACSharpSensor.SensorReached(231);
      for (int i = 0; i < iterations; ++i) {
        IACSharpSensor.IACSharpSensor.SensorReached(232);
        test.DoNothing<Program>();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(233);
      DateTime end = DateTime.Now;
      TimeSpan duration = end - start;
      Console.WriteLine("Took " + duration.TotalMilliseconds + " ms.");
      ITest testInterface = test;
      start = DateTime.Now;
      Console.Write("Calling Test.DoNothing<Program>() on an interface reference...  ");
      IACSharpSensor.IACSharpSensor.SensorReached(234);
      for (int i = 0; i < iterations; ++i) {
        IACSharpSensor.IACSharpSensor.SensorReached(235);
        testInterface.DoNothing<Program>();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(236);
      end = DateTime.Now;
      duration = end - start;
      Console.WriteLine("Took " + duration.TotalMilliseconds + " ms.");
      IACSharpSensor.IACSharpSensor.SensorReached(237);
    }
  }
}
