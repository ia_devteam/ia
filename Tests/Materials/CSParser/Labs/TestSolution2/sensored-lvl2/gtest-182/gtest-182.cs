interface IMember
{
  int GetId();
}
interface IMethod : IMember
{
}
class C1 : IMethod
{
  public int GetId()
  {
    System.Int32 RNTRNTRNT_149 = 42;
    IACSharpSensor.IACSharpSensor.SensorReached(684);
    return RNTRNTRNT_149;
  }
}
class X
{
  static void foo<a>(a e) where a : IMember
  {
    IACSharpSensor.IACSharpSensor.SensorReached(685);
    e.GetId();
    IACSharpSensor.IACSharpSensor.SensorReached(686);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(687);
    foo<IMethod>(new C1());
    IACSharpSensor.IACSharpSensor.SensorReached(688);
  }
}
