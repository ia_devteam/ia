using System;
public class Foo
{
}
public class X
{
  public static Foo Test(Foo foo, Foo bar)
  {
    Foo RNTRNTRNT_115 = foo ?? bar;
    IACSharpSensor.IACSharpSensor.SensorReached(611);
    return RNTRNTRNT_115;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(612);
    Foo[] array = new Foo[1];
    Foo foo = new Foo();
    Foo bar = Test(array[0], foo);
    IACSharpSensor.IACSharpSensor.SensorReached(613);
    if (bar == null) {
      System.Int32 RNTRNTRNT_116 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(614);
      return RNTRNTRNT_116;
    }
    System.Int32 RNTRNTRNT_117 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(615);
    return RNTRNTRNT_117;
  }
}
