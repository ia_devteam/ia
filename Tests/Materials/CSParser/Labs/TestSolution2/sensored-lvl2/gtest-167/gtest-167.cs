using System;
using System.Diagnostics;
[Conditional("DEBUG")]
public class TestAttribute : Attribute
{
}
[Conditional("RELEASE")]
public class TestNotAttribute : Attribute
{
}
[Conditional("A")]
[Conditional("DEBUG")]
[Conditional("B")]
public class TestMultiAttribute : Attribute
{
}
[Test()]
class Class1
{
}
[TestNot()]
class Class2
{
}
[TestMulti()]
class Class3
{
}
public class TestClass
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(572);
    if (Attribute.GetCustomAttributes(typeof(Class1)).Length != 1) {
      System.Int32 RNTRNTRNT_108 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(573);
      return RNTRNTRNT_108;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(574);
    if (Attribute.GetCustomAttributes(typeof(Class2)).Length != 0) {
      System.Int32 RNTRNTRNT_109 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(575);
      return RNTRNTRNT_109;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(576);
    if (Attribute.GetCustomAttributes(typeof(Class3)).Length != 1) {
      System.Int32 RNTRNTRNT_110 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(577);
      return RNTRNTRNT_110;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(578);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_111 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(579);
    return RNTRNTRNT_111;
  }
}
