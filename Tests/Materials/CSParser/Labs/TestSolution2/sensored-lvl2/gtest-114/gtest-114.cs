using System;
using System.Collections.Generic;
public class NaturalComparer<T> : IComparer<T> where T : IComparable<T>
{
  public int Compare(T a, T b)
  {
    System.Int32 RNTRNTRNT_17 = a.CompareTo(b);
    IACSharpSensor.IACSharpSensor.SensorReached(99);
    return RNTRNTRNT_17;
  }
}
public class X
{
  class Test : IComparable<Test>
  {
    public int CompareTo(Test that)
    {
      System.Int32 RNTRNTRNT_18 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(100);
      return RNTRNTRNT_18;
    }
    public bool Equals(Test that)
    {
      System.Boolean RNTRNTRNT_19 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(101);
      return RNTRNTRNT_19;
    }
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(102);
    IComparer<Test> cmp = new NaturalComparer<Test>();
    Test a = new Test();
    Test b = new Test();
    cmp.Compare(a, b);
    IACSharpSensor.IACSharpSensor.SensorReached(103);
  }
}
