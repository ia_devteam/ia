using System;
interface IFoo<T>
{
  T this[int index] { get; set; }
}
public class FooCollection<T> : IFoo<T>
{
  T IFoo<T>.this[int index] {
    get {
      T RNTRNTRNT_65 = default(T);
      IACSharpSensor.IACSharpSensor.SensorReached(325);
      return RNTRNTRNT_65;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(326); }
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(327);
    IFoo<int> foo = new FooCollection<int>();
    int a = foo[3];
    Console.WriteLine(a);
    IACSharpSensor.IACSharpSensor.SensorReached(328);
  }
}
