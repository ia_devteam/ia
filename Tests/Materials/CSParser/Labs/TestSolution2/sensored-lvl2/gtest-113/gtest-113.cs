using System;
public delegate V Mapper<T, V>(T item);
public class List<T>
{
  public void Map<V>(Mapper<T, V> mapper)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(96);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(97);
    List<int> list = new List<int>();
    list.Map(new Mapper<int, double>(delegate(int i) { return i / 10.0; }));
    IACSharpSensor.IACSharpSensor.SensorReached(98);
  }
}
