public class B<T>
{
  public int Add(T obj)
  {
    System.Int32 RNTRNTRNT_64 = -1;
    IACSharpSensor.IACSharpSensor.SensorReached(311);
    return RNTRNTRNT_64;
  }
  public void AddRange(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(312);
    T obj = (T)o;
    Add(obj);
    IACSharpSensor.IACSharpSensor.SensorReached(313);
  }
}
public interface IA
{
}
public class A : IA
{
}
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(314);
    B<IA> aux = new B<IA>();
    aux.AddRange(new A());
    IACSharpSensor.IACSharpSensor.SensorReached(315);
  }
}
