interface a<t>
{
  void x();
}
interface b<t> : a<t>
{
}
class kv<k, v>
{
}
interface c<k, v> : b<kv<k, v>>, a<kv<k, v>>
{
}
class m<k, v> : c<k, v>, b<kv<k, v>>
{
  void a<kv<k, v>>.x()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(512);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(513);
  }
}
