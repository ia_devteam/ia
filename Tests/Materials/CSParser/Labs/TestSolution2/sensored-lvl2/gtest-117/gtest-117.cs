using System;
public interface IFoo<T>
{
}
public class Foo<T>
{
  public static bool Test(T x)
  {
    System.Boolean RNTRNTRNT_39 = x is IFoo<T>;
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    return RNTRNTRNT_39;
  }
  public static bool Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(239);
    T t = default(T);
    System.Boolean RNTRNTRNT_40 = t is int;
    IACSharpSensor.IACSharpSensor.SensorReached(240);
    return RNTRNTRNT_40;
  }
  public static bool TestB()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(241);
    T t = default(T);
    System.Boolean RNTRNTRNT_41 = t is int?;
    IACSharpSensor.IACSharpSensor.SensorReached(242);
    return RNTRNTRNT_41;
  }
}
class Y<T> where T : struct
{
  public bool Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(243);
    object o = null;
    System.Boolean RNTRNTRNT_42 = o is System.Nullable<T>;
    IACSharpSensor.IACSharpSensor.SensorReached(244);
    return RNTRNTRNT_42;
  }
}
class X
{
  public static bool TestA(object o)
  {
    System.Boolean RNTRNTRNT_43 = o is int?;
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    return RNTRNTRNT_43;
  }
  public static bool TestB<T>(T o)
  {
    System.Boolean RNTRNTRNT_44 = o is int[];
    IACSharpSensor.IACSharpSensor.SensorReached(246);
    return RNTRNTRNT_44;
  }
  public static int TestC()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(247);
    int? i = null;
    IACSharpSensor.IACSharpSensor.SensorReached(248);
    if (i is int) {
      System.Int32 RNTRNTRNT_45 = (int)i;
      IACSharpSensor.IACSharpSensor.SensorReached(249);
      return RNTRNTRNT_45;
    }
    System.Int32 RNTRNTRNT_46 = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(250);
    return RNTRNTRNT_46;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(251);
    if (Foo<int>.Test(3)) {
      System.Int32 RNTRNTRNT_47 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(252);
      return RNTRNTRNT_47;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(253);
    if (!Foo<int>.Test()) {
      System.Int32 RNTRNTRNT_48 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(254);
      return RNTRNTRNT_48;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(255);
    if (Foo<int?>.TestB()) {
      System.Int32 RNTRNTRNT_49 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(256);
      return RNTRNTRNT_49;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(257);
    int? i = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(258);
    if (!TestA(i)) {
      System.Int32 RNTRNTRNT_50 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(259);
      return RNTRNTRNT_50;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(260);
    int[] a = new int[0];
    IACSharpSensor.IACSharpSensor.SensorReached(261);
    if (!TestB(a)) {
      System.Int32 RNTRNTRNT_51 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(262);
      return RNTRNTRNT_51;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(263);
    if (TestC() != 3) {
      System.Int32 RNTRNTRNT_52 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(264);
      return RNTRNTRNT_52;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(265);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_53 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(266);
    return RNTRNTRNT_53;
  }
}
