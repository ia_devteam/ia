using System;
using System.Collections.Generic;
public class OrderedMultiDictionary<T, U>
{
  private RedBlackTree<KeyValuePair<T, U>> tree;
  private void EnumerateKeys(RedBlackTree<KeyValuePair<T, U>>.RangeTester rangeTester)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(767);
    tree.EnumerateRange(rangeTester);
    IACSharpSensor.IACSharpSensor.SensorReached(768);
  }
}
internal class RedBlackTree<S>
{
  public delegate int RangeTester(S item);
  public void EnumerateRange(RangeTester rangeTester)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(769);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(770);
  }
}
