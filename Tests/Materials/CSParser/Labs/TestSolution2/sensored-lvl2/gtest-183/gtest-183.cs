using System;
using System.Collections.Generic;
namespace test
{
  class Test<T>
  {
    public IEnumerable<T> Lookup(T item)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(689);
      byte i = 3;
      byte j = 3;
      T RNTRNTRNT_150 = item;
      IACSharpSensor.IACSharpSensor.SensorReached(690);
      yield return RNTRNTRNT_150;
      IACSharpSensor.IACSharpSensor.SensorReached(691);
      IACSharpSensor.IACSharpSensor.SensorReached(692);
    }
  }
  class Program
  {
    public static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(693);
      Test<string> test = new Test<string>();
      IACSharpSensor.IACSharpSensor.SensorReached(694);
      foreach (string s in test.Lookup("hi")) {
        IACSharpSensor.IACSharpSensor.SensorReached(695);
        Console.WriteLine(s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(696);
    }
  }
}
