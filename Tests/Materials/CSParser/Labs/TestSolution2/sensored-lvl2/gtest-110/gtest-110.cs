using System;
public interface IList<R>
{
  int Map<S>(S item);
}
public class List<T> : IList<T>
{
  public int Map<U>(U item)
  {
    System.Int32 RNTRNTRNT_9 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(60);
    return RNTRNTRNT_9;
  }
}
public class SpecialList<V> : IList<V>
{
  public int Map<W>(W item)
  {
    System.Int32 RNTRNTRNT_10 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(61);
    return RNTRNTRNT_10;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    IList<int> list = new List<int>();
    int result = list.Map("Hello");
    IACSharpSensor.IACSharpSensor.SensorReached(63);
    if (result != 1) {
      System.Int32 RNTRNTRNT_11 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(64);
      return RNTRNTRNT_11;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(65);
    IList<int> list2 = new SpecialList<int>();
    int result2 = list2.Map("World");
    IACSharpSensor.IACSharpSensor.SensorReached(66);
    if (result2 != 2) {
      System.Int32 RNTRNTRNT_12 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(67);
      return RNTRNTRNT_12;
    }
    System.Int32 RNTRNTRNT_13 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    return RNTRNTRNT_13;
  }
}
