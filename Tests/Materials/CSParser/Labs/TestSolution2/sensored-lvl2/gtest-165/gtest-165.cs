using System;
using System.Collections;
class X
{
  delegate void A();
  static IEnumerable GetIt(int[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(542);
    foreach (int arg in args) {
      IACSharpSensor.IACSharpSensor.SensorReached(543);
      Console.WriteLine("OUT: {0}", arg);
      A a = delegate {
        IACSharpSensor.IACSharpSensor.SensorReached(544);
        Console.WriteLine("arg: {0}", arg);
        IACSharpSensor.IACSharpSensor.SensorReached(545);
        return;
      };
      a();
      IEnumerable RNTRNTRNT_99 = arg;
      IACSharpSensor.IACSharpSensor.SensorReached(546);
      yield return RNTRNTRNT_99;
      IACSharpSensor.IACSharpSensor.SensorReached(547);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(548);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(549);
    int total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(550);
    foreach (int i in GetIt(new int[] {
      1,
      2,
      3
    })) {
      IACSharpSensor.IACSharpSensor.SensorReached(551);
      Console.WriteLine("Got: " + i);
      total += i;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(552);
    if (total != 6) {
      System.Int32 RNTRNTRNT_100 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(553);
      return RNTRNTRNT_100;
    }
    System.Int32 RNTRNTRNT_101 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(554);
    return RNTRNTRNT_101;
  }
}
