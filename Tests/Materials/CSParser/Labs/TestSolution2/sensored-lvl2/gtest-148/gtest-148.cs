using System;
static class Test1
{
  public class IOp<T>
  {
  }
  static void Foo<S, OP>(uint v) where OP : IOp<S>
  {
    IACSharpSensor.IACSharpSensor.SensorReached(480);
  }
}
static class Test2
{
  public class IOp<T>
  {
  }
  static void Foo<T, OP>(uint v) where OP : IOp<T>
  {
    IACSharpSensor.IACSharpSensor.SensorReached(481);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(482);
  }
}
