using System;
using System.Collections;
using System.Collections.Generic;
public delegate int Int2Int(int i);
public class FunEnumerable
{
  int size;
  Int2Int f;
  public FunEnumerable(int size, Int2Int f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(536);
    this.size = size;
    this.f = f;
    IACSharpSensor.IACSharpSensor.SensorReached(537);
  }
  public IEnumerator<int> GetEnumerator()
  {
    System.Int32 RNTRNTRNT_98 = f(size);
    IACSharpSensor.IACSharpSensor.SensorReached(538);
    yield return RNTRNTRNT_98;
    IACSharpSensor.IACSharpSensor.SensorReached(539);
    IACSharpSensor.IACSharpSensor.SensorReached(540);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(541);
  }
}
