using System;
using System.Collections.Generic;
public class App
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(516);
    Dictionary<string, int> values = new Dictionary<string, int>();
    values["one"] = 1;
    values["two"] = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(517);
    foreach (string key in values.Keys) {
      IACSharpSensor.IACSharpSensor.SensorReached(518);
      System.Console.WriteLine("key: {0}", key);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(519);
  }
}
