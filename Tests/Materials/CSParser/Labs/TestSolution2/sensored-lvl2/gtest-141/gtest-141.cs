using System;
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(417);
    int?[] bvals = new int?[] {
      null,
      3,
      4
    };
    IACSharpSensor.IACSharpSensor.SensorReached(418);
    foreach (int? x in bvals) {
      IACSharpSensor.IACSharpSensor.SensorReached(419);
      Console.WriteLine(x);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(420);
  }
}
