using System;
public delegate V Mapper<T, V>(T item);
public interface ITree<T>
{
  void Map<V>(Mapper<T, V> mapper);
}
public class Tree<T> : ITree<T>
{
  T item;
  public Tree(T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(41);
    this.item = item;
    IACSharpSensor.IACSharpSensor.SensorReached(42);
  }
  public void Map<V>(Mapper<T, V> mapper)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(43);
    V new_item = mapper(item);
    IACSharpSensor.IACSharpSensor.SensorReached(44);
  }
}
class X
{
  private string themap(int i)
  {
    System.String RNTRNTRNT_7 = String.Format("AA {0,4} BB", i);
    IACSharpSensor.IACSharpSensor.SensorReached(45);
    return RNTRNTRNT_7;
  }
  void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(46);
    Tree<int> tree = new Tree<int>(3);
    tree.Map(new Mapper<int, string>(themap));
    IACSharpSensor.IACSharpSensor.SensorReached(47);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(48);
    X x = new X();
    x.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(49);
  }
}
