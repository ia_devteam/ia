public class Foo<T> where T : Foo<T>
{
  public T n;
  public T next()
  {
    T RNTRNTRNT_152 = n;
    IACSharpSensor.IACSharpSensor.SensorReached(717);
    return RNTRNTRNT_152;
  }
}
public class Goo : Foo<Goo>
{
  public int x;
}
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(718);
    Goo x = new Goo();
    x = x.next();
    IACSharpSensor.IACSharpSensor.SensorReached(719);
  }
}
