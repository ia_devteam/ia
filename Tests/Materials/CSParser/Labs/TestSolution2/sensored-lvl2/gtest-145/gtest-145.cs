using System;
public class Test<T>
{
  private T[,] data;
  public Test(T[,] data)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(469);
    this.data = data;
    IACSharpSensor.IACSharpSensor.SensorReached(470);
  }
}
public class Program
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(471);
    Test<double> test = new Test<double>(new double[2, 2]);
    IACSharpSensor.IACSharpSensor.SensorReached(472);
  }
}
