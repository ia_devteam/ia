class Test<T> where T : struct
{
  public Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(492);
    T s = new T();
    IACSharpSensor.IACSharpSensor.SensorReached(493);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(494);
  }
}
