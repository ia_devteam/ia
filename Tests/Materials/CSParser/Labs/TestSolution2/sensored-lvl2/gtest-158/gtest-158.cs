public class Moo<C> where C : Moo<C>.Foo
{
  public class Foo
  {
  }
}
public class Test : Moo<Test>.Foo
{
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(514);
    Moo<Test> moo = new Moo<Test>();
    IACSharpSensor.IACSharpSensor.SensorReached(515);
  }
}
