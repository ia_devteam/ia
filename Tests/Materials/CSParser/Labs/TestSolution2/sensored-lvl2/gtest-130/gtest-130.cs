using System;
class MyTest
{
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(356);
    Console.WriteLine("Note that null prints as blank or []\n");
    bool? b1 = null, b2 = false, b3 = true;
    bool? b4 = b1 ^ b2, b5 = b1 & b2, b6 = b1 | b2;
    Console.WriteLine("[{0}] [{1}] [{2}]", b4, b5, b6);
    bool? b7 = b1 ^ b3, b8 = b1 & b3, b9 = b1 | b3;
    Console.WriteLine("[{0}] [{1}] [{2}]", b7, b8, b9);
    Console.WriteLine(b1 != null ? "null is true" : "null is false");
    Console.WriteLine(b1 == null ? "!null is true" : "!null is false");
    Console.WriteLine();
    bool?[] bvals = new bool?[] {
      null,
      false,
      true
    };
    Console.WriteLine("{0,-6} {1,-6} {2,-6} {3,-6} {4,-6}", "x", "y", "x&y", "x|y", "x^y");
    IACSharpSensor.IACSharpSensor.SensorReached(357);
    foreach (bool? x in bvals) {
      IACSharpSensor.IACSharpSensor.SensorReached(358);
      foreach (bool? y in bvals) {
        IACSharpSensor.IACSharpSensor.SensorReached(359);
        Console.WriteLine("{0,-6} {1,-6} {2,-6} {3,-6} {4,-6}", x, y, x & y, x | y, x ^ y);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(360);
    Console.WriteLine();
    Console.WriteLine("{0,-6} {1,-6}", "x", "!x");
    IACSharpSensor.IACSharpSensor.SensorReached(361);
    foreach (bool? x in bvals) {
      IACSharpSensor.IACSharpSensor.SensorReached(362);
      Console.WriteLine("{0,-6} {1,-6}", x, !x);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(363);
  }
}
