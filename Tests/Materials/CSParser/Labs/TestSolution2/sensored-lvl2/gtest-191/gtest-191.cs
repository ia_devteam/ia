using System;
using System.Collections.Generic;
namespace Test1
{
  public static class Test
  {
    public static IEnumerable<T> Replace<T>()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(742);
      yield break;
    }
  }
}
namespace Test2
{
  public class Test<S>
  {
    public static IEnumerable<T> Replace<T>()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(743);
      yield break;
    }
  }
}
namespace Test3
{
  public class Test<S>
  {
    public static IEnumerable<KeyValuePair<S, T>> Replace<T>(IEnumerable<T> a, IEnumerable<S> b)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(744);
      yield break;
    }
  }
}
namespace Test4
{
  public class Test
  {
    public static IEnumerable<T> Replace<T>() where T : class
    {
      IACSharpSensor.IACSharpSensor.SensorReached(745);
      yield break;
    }
  }
}
namespace Test5
{
  public class Test
  {
    public static IEnumerable<T> Replace<T>(T t)
    {
      T RNTRNTRNT_155 = t;
      IACSharpSensor.IACSharpSensor.SensorReached(746);
      yield return RNTRNTRNT_155;
      IACSharpSensor.IACSharpSensor.SensorReached(747);
      IACSharpSensor.IACSharpSensor.SensorReached(748);
    }
  }
}
namespace Test6
{
  public class Test
  {
    public static IEnumerable<T> Replace<T>(T t)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(749);
      T u = t;
      T RNTRNTRNT_156 = u;
      IACSharpSensor.IACSharpSensor.SensorReached(750);
      yield return RNTRNTRNT_156;
      IACSharpSensor.IACSharpSensor.SensorReached(751);
      IACSharpSensor.IACSharpSensor.SensorReached(752);
    }
  }
}
namespace Test7
{
  public class Test
  {
    public static IEnumerable<T[]> Replace<T>(T[] t)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(753);
      T[] array = t;
      T[] RNTRNTRNT_157 = array;
      IACSharpSensor.IACSharpSensor.SensorReached(754);
      yield return RNTRNTRNT_157;
      IACSharpSensor.IACSharpSensor.SensorReached(755);
      IACSharpSensor.IACSharpSensor.SensorReached(756);
    }
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(757);
  }
}
