class List<t>
{
  public void foo<b>(List<t> x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(605);
    System.Console.WriteLine("{0} - {1}", typeof(t), x.GetType());
    IACSharpSensor.IACSharpSensor.SensorReached(606);
  }
}
class C
{
}
class D
{
}
class M
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(607);
    List<D> x = new List<D>();
    x.foo<C>(x);
    List<string> y = new List<string>();
    y.foo<C>(y);
    IACSharpSensor.IACSharpSensor.SensorReached(608);
  }
}
