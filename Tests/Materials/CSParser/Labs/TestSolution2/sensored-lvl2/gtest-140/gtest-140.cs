using System;
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    int?[] bvals = new int?[] {
      null,
      3,
      4
    };
    IACSharpSensor.IACSharpSensor.SensorReached(414);
    foreach (long? x in bvals) {
      IACSharpSensor.IACSharpSensor.SensorReached(415);
      Console.WriteLine(x);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(416);
  }
}
