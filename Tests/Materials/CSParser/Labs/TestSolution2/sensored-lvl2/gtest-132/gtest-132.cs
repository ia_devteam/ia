using System;
class MyTest
{
  public static int? Sqrt(int? x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(380);
    if (x.HasValue && x.Value >= 0) {
      System.Nullable<System.Int32> RNTRNTRNT_71 = (int)(Math.Sqrt(x.Value));
      IACSharpSensor.IACSharpSensor.SensorReached(381);
      return RNTRNTRNT_71;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(382);
      return null;
    }
  }
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(383);
    Console.WriteLine(":{0}:{1}:{2}:", Sqrt(5), Sqrt(null), Sqrt(-5));
    IACSharpSensor.IACSharpSensor.SensorReached(384);
  }
}
