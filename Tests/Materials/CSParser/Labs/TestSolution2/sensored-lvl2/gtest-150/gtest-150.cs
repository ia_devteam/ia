using System;
static class Test1
{
  public interface IOp<T>
  {
    T Func(uint v);
  }
  public struct Op : IOp<ushort>, IOp<uint>
  {
    ushort IOp<ushort>.Func(uint v)
    {
      System.UInt16 RNTRNTRNT_88 = (ushort)(v * 2);
      IACSharpSensor.IACSharpSensor.SensorReached(486);
      return RNTRNTRNT_88;
    }
    uint IOp<uint>.Func(uint v)
    {
      System.UInt32 RNTRNTRNT_89 = v * 4;
      IACSharpSensor.IACSharpSensor.SensorReached(487);
      return RNTRNTRNT_89;
    }
  }
  static void Foo<T, OP>(uint v) where T : struct where OP : IOp<T>
  {
    IACSharpSensor.IACSharpSensor.SensorReached(488);
    OP op = default(OP);
    System.Console.WriteLine(op.Func(v));
    IACSharpSensor.IACSharpSensor.SensorReached(489);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(490);
    Foo<ushort, Op>(100);
    Foo<uint, Op>(100);
    IACSharpSensor.IACSharpSensor.SensorReached(491);
  }
}
