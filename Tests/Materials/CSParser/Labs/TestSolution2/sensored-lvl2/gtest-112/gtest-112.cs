using System;
public interface IComparer<T>
{
  void Compare(T a);
}
class IC : IComparer<Foo<int>>
{
  public void Compare(Foo<int> a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(83);
  }
}
public struct Foo<K>
{
  public K Value;
  public Foo(K value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(84);
    Value = value;
    IACSharpSensor.IACSharpSensor.SensorReached(85);
  }
}
public class List<T>
{
  public virtual void Sort(IComparer<T> c, T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(86);
    Sorting.IntroSort<T>(c, t);
    IACSharpSensor.IACSharpSensor.SensorReached(87);
  }
}
public class Sorting
{
  public static void IntroSort<T>(IComparer<T> c, T t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(88);
    new Sorter<T>(c, 4, t).InsertionSort(0);
    IACSharpSensor.IACSharpSensor.SensorReached(89);
  }
  class Sorter<T>
  {
    IComparer<T> c;
    T[] a;
    public Sorter(IComparer<T> c, int size, T item)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(90);
      this.c = c;
      a = new T[size];
      IACSharpSensor.IACSharpSensor.SensorReached(91);
    }
    internal void InsertionSort(int i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(92);
      T other;
      c.Compare(other = a[i]);
      IACSharpSensor.IACSharpSensor.SensorReached(93);
    }
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(94);
    List<Foo<int>> list = new List<Foo<int>>();
    Foo<int> foo = new Foo<int>(3);
    list.Sort(new IC(), foo);
    IACSharpSensor.IACSharpSensor.SensorReached(95);
  }
}
