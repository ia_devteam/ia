public interface IFoo<T>
{
}
public class Foo : IFoo<string>
{
}
public class Hello
{
  public void World<U>(U u, IFoo<U> foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(10);
  }
  public void World<V>(IFoo<V> foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(11);
  }
  public void Test(Foo foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(12);
    World("Canada", foo);
    World(foo);
    IACSharpSensor.IACSharpSensor.SensorReached(13);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(14);
  }
}
