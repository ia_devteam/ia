using System;
public struct KeyValuePair<K, V>
{
  public K key;
  public V value;
  public KeyValuePair(K k, V v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(69);
    key = k;
    value = v;
    IACSharpSensor.IACSharpSensor.SensorReached(70);
  }
  public KeyValuePair(K k)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(71);
    key = k;
    value = default(V);
    IACSharpSensor.IACSharpSensor.SensorReached(72);
  }
}
public class Collection<T>
{
  public readonly T Item;
  public Collection(T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(73);
    this.Item = item;
    IACSharpSensor.IACSharpSensor.SensorReached(74);
  }
  public void Find(ref T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(75);
    item = Item;
    IACSharpSensor.IACSharpSensor.SensorReached(76);
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(77);
    KeyValuePair<int, long> p = new KeyValuePair<int, long>(3);
    KeyValuePair<int, long> q = new KeyValuePair<int, long>(5, 9);
    Collection<KeyValuePair<int, long>> c = new Collection<KeyValuePair<int, long>>(q);
    c.Find(ref p);
    IACSharpSensor.IACSharpSensor.SensorReached(78);
    if (p.key != 5) {
      System.Int32 RNTRNTRNT_14 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(79);
      return RNTRNTRNT_14;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(80);
    if (p.value != 9) {
      System.Int32 RNTRNTRNT_15 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(81);
      return RNTRNTRNT_15;
    }
    System.Int32 RNTRNTRNT_16 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(82);
    return RNTRNTRNT_16;
  }
}
