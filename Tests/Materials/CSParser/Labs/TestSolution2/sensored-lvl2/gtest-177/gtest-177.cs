using System;
using System.Collections.Generic;
class X
{
  static int[] x = new int[] {
    100,
    200
  };
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(622);
    IEnumerator<int> enumerator = X<int>.Y(x);
    int sum = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(623);
    while (enumerator.MoveNext()) {
      IACSharpSensor.IACSharpSensor.SensorReached(624);
      sum += enumerator.Current;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(625);
    if (sum != 300) {
      System.Int32 RNTRNTRNT_122 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(626);
      return RNTRNTRNT_122;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(627);
    if (X<int>.Z(x, 0) != 100) {
      System.Int32 RNTRNTRNT_123 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(628);
      return RNTRNTRNT_123;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(629);
    if (X<int>.Z(x, 1) != 200) {
      System.Int32 RNTRNTRNT_124 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(630);
      return RNTRNTRNT_124;
    }
    System.Int32 RNTRNTRNT_125 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(631);
    return RNTRNTRNT_125;
  }
}
class X<T>
{
  public static IEnumerator<T> Y(IEnumerable<T> x)
  {
    IEnumerator<T> RNTRNTRNT_126 = x.GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(632);
    return RNTRNTRNT_126;
  }
  public static T Z(IList<T> x, int index)
  {
    T RNTRNTRNT_127 = x[index];
    IACSharpSensor.IACSharpSensor.SensorReached(633);
    return RNTRNTRNT_127;
  }
}
