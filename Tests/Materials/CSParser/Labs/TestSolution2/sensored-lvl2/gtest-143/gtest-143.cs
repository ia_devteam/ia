using System;
class X
{
  static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(444);
    int? a = 5;
    int? b = a++;
    IACSharpSensor.IACSharpSensor.SensorReached(445);
    if (a != 6) {
      System.Int32 RNTRNTRNT_79 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(446);
      return RNTRNTRNT_79;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(447);
    if (b != 5) {
      System.Int32 RNTRNTRNT_80 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(448);
      return RNTRNTRNT_80;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(449);
    int? c = ++a;
    IACSharpSensor.IACSharpSensor.SensorReached(450);
    if (c != 7) {
      System.Int32 RNTRNTRNT_81 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(451);
      return RNTRNTRNT_81;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(452);
    b++;
    ++b;
    IACSharpSensor.IACSharpSensor.SensorReached(453);
    if (b != 7) {
      System.Int32 RNTRNTRNT_82 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(454);
      return RNTRNTRNT_82;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(455);
    int? d = b++ + ++a;
    IACSharpSensor.IACSharpSensor.SensorReached(456);
    if (a != 8) {
      System.Int32 RNTRNTRNT_83 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(457);
      return RNTRNTRNT_83;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(458);
    if (b != 8) {
      System.Int32 RNTRNTRNT_84 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(459);
      return RNTRNTRNT_84;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(460);
    if (d != 15) {
      System.Int32 RNTRNTRNT_85 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(461);
      return RNTRNTRNT_85;
    }
    System.Int32 RNTRNTRNT_86 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(462);
    return RNTRNTRNT_86;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(463);
    int result = Test();
    IACSharpSensor.IACSharpSensor.SensorReached(464);
    if (result != 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(465);
      Console.WriteLine("ERROR: {0}", result);
    }
    System.Int32 RNTRNTRNT_87 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(466);
    return RNTRNTRNT_87;
  }
}
