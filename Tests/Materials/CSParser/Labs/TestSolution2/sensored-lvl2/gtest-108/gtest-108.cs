using System;
using System.Collections.Generic;
public class Test<T>
{
  protected T item;
  public Test(T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(50);
    this.item = item;
    IACSharpSensor.IACSharpSensor.SensorReached(51);
  }
  public IEnumerator<T> GetEnumerator()
  {
    T RNTRNTRNT_8 = item;
    IACSharpSensor.IACSharpSensor.SensorReached(52);
    yield return RNTRNTRNT_8;
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    IACSharpSensor.IACSharpSensor.SensorReached(54);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(55);
    Test<int> test = new Test<int>(3);
    IACSharpSensor.IACSharpSensor.SensorReached(56);
    foreach (int a in test) {
      ;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(57);
  }
}
