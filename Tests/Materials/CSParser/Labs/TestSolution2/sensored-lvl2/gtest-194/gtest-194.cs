using System;
using System.Collections.Generic;
internal class RedBlackTree<S>
{
  public delegate int RangeTester(S item);
  public IEnumerable<S> EnumerateRange(RangeTester rangeTester)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(763);
    yield break;
  }
}
public class OrderedMultiDictionary<T, U>
{
  private RedBlackTree<KeyValuePair<T, U>> tree;
  private IEnumerator<T> EnumerateKeys(RedBlackTree<KeyValuePair<T, U>>.RangeTester rangeTester)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(764);
    tree.EnumerateRange(rangeTester);
    IACSharpSensor.IACSharpSensor.SensorReached(765);
    yield break;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(766);
  }
}
