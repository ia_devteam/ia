using System;
using System.Runtime.InteropServices;
namespace Martin.Collections.Generic
{
  [Serializable()]
  public abstract class EqualityComparer<T> : IEqualityComparer<T>
  {
    static EqualityComparer()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(778);
      if (typeof(IEquatable<T>).IsAssignableFrom(typeof(T))) {
        IACSharpSensor.IACSharpSensor.SensorReached(779);
        _default = (EqualityComparer<T>)Activator.CreateInstance(typeof(IEquatableOfTEqualityComparer<>).MakeGenericType(typeof(T)));
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(780);
        _default = new DefaultComparer();
      }
      IACSharpSensor.IACSharpSensor.SensorReached(781);
    }
    public abstract int GetHashCode(T obj);
    public abstract bool Equals(T x, T y);
    static readonly EqualityComparer<T> _default;
    public static EqualityComparer<T> Default {
      get {
        EqualityComparer<T> RNTRNTRNT_161 = _default;
        IACSharpSensor.IACSharpSensor.SensorReached(782);
        return RNTRNTRNT_161;
      }
    }
    [Serializable()]
    class DefaultComparer : EqualityComparer<T>
    {
      public override int GetHashCode(T obj)
      {
        System.Int32 RNTRNTRNT_162 = obj.GetHashCode();
        IACSharpSensor.IACSharpSensor.SensorReached(783);
        return RNTRNTRNT_162;
      }
      public override bool Equals(T x, T y)
      {
        IACSharpSensor.IACSharpSensor.SensorReached(784);
        if (x == null) {
          System.Boolean RNTRNTRNT_163 = y == null;
          IACSharpSensor.IACSharpSensor.SensorReached(785);
          return RNTRNTRNT_163;
        }
        System.Boolean RNTRNTRNT_164 = x.Equals(y);
        IACSharpSensor.IACSharpSensor.SensorReached(786);
        return RNTRNTRNT_164;
      }
    }
  }
  [Serializable()]
  class IEquatableOfTEqualityComparer<T> : EqualityComparer<T> where T : IEquatable<T>
  {
    public override int GetHashCode(T obj)
    {
      System.Int32 RNTRNTRNT_165 = obj.GetHashCode();
      IACSharpSensor.IACSharpSensor.SensorReached(787);
      return RNTRNTRNT_165;
    }
    public override bool Equals(T x, T y)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(788);
      if (x == null) {
        System.Boolean RNTRNTRNT_166 = y == null;
        IACSharpSensor.IACSharpSensor.SensorReached(789);
        return RNTRNTRNT_166;
      }
      System.Boolean RNTRNTRNT_167 = x.Equals(y);
      IACSharpSensor.IACSharpSensor.SensorReached(790);
      return RNTRNTRNT_167;
    }
  }
  public interface IEqualityComparer<T>
  {
    bool Equals(T x, T y);
    int GetHashCode(T obj);
  }
  class X
  {
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(791);
    }
  }
}
