using System;
class MyTest
{
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(392);
    Foo<int?> fni1 = new Foo<int?>(null);
    Console.WriteLine(fni1.Fmt());
    Foo<int?> fni2 = new Foo<int?>(17);
    Console.WriteLine(fni2.Fmt());
    Foo<int> fi = new Foo<int>(7);
    Console.WriteLine(fi.Fmt());
    Foo<String> fs1 = new Foo<String>(null);
    Console.WriteLine(fs1.Fmt());
    Foo<String> fs2 = new Foo<String>("haha");
    Console.WriteLine(fs2.Fmt());
    IACSharpSensor.IACSharpSensor.SensorReached(393);
  }
}
class Foo<T>
{
  T x;
  public Foo(T x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(394);
    this.x = x;
    IACSharpSensor.IACSharpSensor.SensorReached(395);
  }
  public String Fmt()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(396);
    if (x != null) {
      String RNTRNTRNT_73 = x.ToString();
      IACSharpSensor.IACSharpSensor.SensorReached(397);
      return RNTRNTRNT_73;
    } else {
      String RNTRNTRNT_74 = "null";
      IACSharpSensor.IACSharpSensor.SensorReached(398);
      return RNTRNTRNT_74;
    }
  }
}
