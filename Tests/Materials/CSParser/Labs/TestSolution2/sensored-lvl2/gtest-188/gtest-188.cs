using System;
public class Foo
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(720);
    new Foo(new object[] { "foo" });
    IACSharpSensor.IACSharpSensor.SensorReached(721);
  }
  public Foo(object[] array) : this(array, array[0])
  {
    IACSharpSensor.IACSharpSensor.SensorReached(722);
  }
  public Foo(object[] array, object context)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(723);
    if (array.GetType().IsArray) {
      IACSharpSensor.IACSharpSensor.SensorReached(724);
      Console.WriteLine("ok! array is correct type");
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(725);
      Console.WriteLine("boo! array is of type {0}", array.GetType());
    }
    IACSharpSensor.IACSharpSensor.SensorReached(726);
    if (array[0] == context) {
      IACSharpSensor.IACSharpSensor.SensorReached(727);
      Console.WriteLine("ok! array[0] == context!");
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(728);
      Console.WriteLine("boo! array[0] != context!");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(729);
    foreach (char ch in "123") {
      IACSharpSensor.IACSharpSensor.SensorReached(730);
      DoSomething += delegate(object obj, EventArgs args) { Console.WriteLine("{0}:{1}:{2}", ch, array[0], context); };
    }
    IACSharpSensor.IACSharpSensor.SensorReached(731);
  }
  public event EventHandler DoSomething;
}
