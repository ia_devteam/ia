using System;
class MyTest
{
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(364);
    Console.WriteLine("Note that null prints as blank or []\n");
    int? i1 = 11, i2 = 22, i3 = null, i4 = i1 + i2, i5 = i1 + i3;
    Console.WriteLine("[{0}] [{1}] [{2}] [{3}] [{4}]", i1, i2, i3, i4, i5);
    int i6 = (int)i1;
    int?[] iarr = {
      i1,
      i2,
      i3,
      i4,
      i5
    };
    i2 += i1;
    i2 += i4;
    Console.WriteLine("i2 = {0}", i2);
    int sum = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(365);
    for (int i = 0; i < iarr.Length; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(366);
      sum += iarr[i] != null ? iarr[i].Value : 0;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(367);
    Console.WriteLine("sum = {0}", sum);
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    for (int i = 0; i < iarr.Length; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(369);
      if (iarr[i] > 11) {
        IACSharpSensor.IACSharpSensor.SensorReached(370);
        Console.Write("[{0}] ", iarr[i]);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(371);
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(372);
    for (int i = 0; i < iarr.Length; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(373);
      if (iarr[i] != i1) {
        IACSharpSensor.IACSharpSensor.SensorReached(374);
        Console.Write("[{0}] ", iarr[i]);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(375);
    Console.WriteLine();
    Console.WriteLine();
    int?[] ivals = {
      null,
      2,
      5
    };
    Console.WriteLine("{0,6} {1,6} {2,6} {3,6} {4,-6} {5,-6} {6,-6} {7,-6}", "x", "y", "x+y", "x-y", "x<y", "x>=y", "x==y", "x!=y");
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(376);
    foreach (int? x in ivals) {
      IACSharpSensor.IACSharpSensor.SensorReached(377);
      foreach (int? y in ivals) {
        IACSharpSensor.IACSharpSensor.SensorReached(378);
        Console.WriteLine("{0,6} {1,6} {2,6} {3,6} {4,-6} {5,-6} {6,-6} {7,-6}", x, y, x + y, x - y, (x < y), (x >= y), x == y, x != y);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(379);
  }
}
