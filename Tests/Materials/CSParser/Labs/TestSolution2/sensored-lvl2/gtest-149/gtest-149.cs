using System;
static class Test1
{
  public class IOp<T>
  {
  }
  static void Foo<S, OP>(uint v) where OP : IOp<S>
  {
    IACSharpSensor.IACSharpSensor.SensorReached(483);
  }
}
static class Test2
{
  public class IOp<T>
  {
  }
  static void Foo<T, OP>(uint v) where OP : IOp<T>
  {
    IACSharpSensor.IACSharpSensor.SensorReached(484);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(485);
  }
}
