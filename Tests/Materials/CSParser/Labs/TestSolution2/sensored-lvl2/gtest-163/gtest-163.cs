using System;
using System.Collections;
using System.Collections.Generic;
public class Foo<T>
{
  public IEnumerator<T> getEnumerator(int arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(530);
    if (arg == 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(531);
      int foo = arg;
      Console.WriteLine(foo);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(532);
    if (arg == 2) {
      IACSharpSensor.IACSharpSensor.SensorReached(533);
      int foo = arg;
      Console.WriteLine(foo);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(534);
    yield break;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(535);
  }
}
