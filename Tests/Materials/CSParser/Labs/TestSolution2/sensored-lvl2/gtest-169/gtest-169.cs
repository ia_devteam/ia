class list<A>
{
  public class Cons<T> : list<T>
  {
  }
  public class Nil<T> : list<T>
  {
  }
}
class C
{
  public static void Rev<T>(list<T> y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(587);
    if (y is list<object>.Cons<T>) {
      IACSharpSensor.IACSharpSensor.SensorReached(588);
      System.Console.WriteLine("Cons");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(589);
    if (y is list<object>.Nil<T>) {
      IACSharpSensor.IACSharpSensor.SensorReached(590);
      System.Console.WriteLine("Nil");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(591);
  }
}
class M
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(592);
    C.Rev(new list<object>.Cons<string>());
    C.Rev(new list<object>.Nil<string>());
    IACSharpSensor.IACSharpSensor.SensorReached(593);
  }
}
