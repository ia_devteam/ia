class C<A>
{
  public static void foo<B>(C<B> x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(594);
    D.append(x);
    IACSharpSensor.IACSharpSensor.SensorReached(595);
  }
}
class D
{
  public static void append<A>(C<A> x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(596);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(597);
    C<object>.foo<int>(null);
    IACSharpSensor.IACSharpSensor.SensorReached(598);
  }
}
