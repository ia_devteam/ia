using System;
using System.Reflection;
namespace FLMID.Bugs.ParametersOne
{
  public class Class<T>
  {
    public void Add(T x)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(346);
      System.Console.WriteLine("OK");
      IACSharpSensor.IACSharpSensor.SensorReached(347);
    }
  }
  public class Test
  {
    public static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(348);
      Class<string> instance = new Class<string>();
      MethodInfo _method = null;
      IACSharpSensor.IACSharpSensor.SensorReached(349);
      foreach (MethodInfo method in typeof(Class<string>).GetMethods(BindingFlags.Instance | BindingFlags.Public)) {
        IACSharpSensor.IACSharpSensor.SensorReached(350);
        if (method.Name.Equals("Add") && method.GetParameters().Length == 1) {
          IACSharpSensor.IACSharpSensor.SensorReached(351);
          _method = method;
          IACSharpSensor.IACSharpSensor.SensorReached(352);
          break;
        }
      }
      IACSharpSensor.IACSharpSensor.SensorReached(353);
      _method.Invoke(instance, new object[] { "1" });
      IACSharpSensor.IACSharpSensor.SensorReached(354);
    }
  }
}
