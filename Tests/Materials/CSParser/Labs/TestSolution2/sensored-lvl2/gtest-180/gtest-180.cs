using System;
using System.Runtime.InteropServices;
[module: DefaultCharSet(CharSet.Unicode)]
struct foo1
{
}
enum E
{
}
[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
struct foo2
{
}
[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
delegate void D();
class C
{
  public class CC
  {
  }
}
class Program
{
  [DllImport("bah")]
  public static extern void test();
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(639);
    DllImportAttribute dia = Attribute.GetCustomAttribute(typeof(Program).GetMethod("test"), typeof(DllImportAttribute)) as DllImportAttribute;
    IACSharpSensor.IACSharpSensor.SensorReached(640);
    if (dia == null) {
      System.Int32 RNTRNTRNT_131 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(641);
      return RNTRNTRNT_131;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(642);
    if (dia.CharSet != CharSet.Unicode) {
      System.Int32 RNTRNTRNT_132 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(643);
      return RNTRNTRNT_132;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(644);
    if (!typeof(C).IsUnicodeClass) {
      System.Int32 RNTRNTRNT_133 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(645);
      return RNTRNTRNT_133;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(646);
    if (!typeof(C.CC).IsUnicodeClass) {
      System.Int32 RNTRNTRNT_134 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(647);
      return RNTRNTRNT_134;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(648);
    if (!typeof(D).IsUnicodeClass) {
      System.Int32 RNTRNTRNT_135 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(649);
      return RNTRNTRNT_135;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(650);
    if (!typeof(E).IsUnicodeClass) {
      System.Int32 RNTRNTRNT_136 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(651);
      return RNTRNTRNT_136;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(652);
    if (!typeof(foo1).IsUnicodeClass) {
      System.Int32 RNTRNTRNT_137 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(653);
      return RNTRNTRNT_137;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(654);
    if (!typeof(foo2).IsAutoClass) {
      System.Int32 RNTRNTRNT_138 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(655);
      return RNTRNTRNT_138;
    }
    System.Int32 RNTRNTRNT_139 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(656);
    return RNTRNTRNT_139;
  }
}
