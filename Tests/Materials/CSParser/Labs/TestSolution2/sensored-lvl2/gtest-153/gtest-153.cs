public interface IBase
{
  void DoSomeThing();
}
public interface IExtended : IBase
{
  void DoSomeThingElse();
}
public class MyClass<T> where T : IExtended, new()
{
  public MyClass()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(498);
    T instance = new T();
    instance.DoSomeThing();
    IACSharpSensor.IACSharpSensor.SensorReached(499);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(500);
  }
}
