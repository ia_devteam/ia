using System;
namespace Martin
{
  public struct A
  {
    public readonly long Data;
    public A(long data)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(401);
      this.Data = data;
      IACSharpSensor.IACSharpSensor.SensorReached(402);
    }
    public static explicit operator B(A a)
    {
      B RNTRNTRNT_75 = new B((int)a.Data);
      IACSharpSensor.IACSharpSensor.SensorReached(403);
      return RNTRNTRNT_75;
    }
  }
  public struct B
  {
    public readonly int Data;
    public B(int data)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(404);
      this.Data = data;
      IACSharpSensor.IACSharpSensor.SensorReached(405);
    }
    public static implicit operator A(B b)
    {
      A RNTRNTRNT_76 = new A(b.Data);
      IACSharpSensor.IACSharpSensor.SensorReached(406);
      return RNTRNTRNT_76;
    }
  }
  class X
  {
    static void Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(407);
      B? b = new B(5);
      A? a = b;
      B? c = (B?)a;
      B? d = (Martin.B?)a;
      IACSharpSensor.IACSharpSensor.SensorReached(408);
    }
  }
}
