interface IFoo
{
}
class Bar : IFoo
{
}
class Cont<T>
{
  T f;
  public Cont(T x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(732);
    f = x;
    IACSharpSensor.IACSharpSensor.SensorReached(733);
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_153 = f.ToString();
    IACSharpSensor.IACSharpSensor.SensorReached(734);
    return RNTRNTRNT_153;
  }
}
class M
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(735);
    Cont<IFoo> c = new Cont<IFoo>(new Bar());
    System.Console.WriteLine(c);
    IACSharpSensor.IACSharpSensor.SensorReached(736);
  }
}
