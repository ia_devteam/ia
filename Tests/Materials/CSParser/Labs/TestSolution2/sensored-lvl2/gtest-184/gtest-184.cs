class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(697);
    string[] s = {
      "a",
      "b",
      "a"
    };
    System.Array.FindAll(s, delegate(string str) { return str == "a"; });
    IACSharpSensor.IACSharpSensor.SensorReached(698);
  }
}
