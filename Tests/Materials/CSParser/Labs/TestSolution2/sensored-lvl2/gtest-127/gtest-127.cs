public class A<T>
{
  public delegate void Changed(A<T> a);
  protected event Changed _changed;
  public void Register(Changed changed)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(340);
    _changed += changed;
    _changed(this);
    IACSharpSensor.IACSharpSensor.SensorReached(341);
  }
}
public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(342);
    A<int> a = new A<int>();
    a.Register(new A<int>.Changed(Del));
    IACSharpSensor.IACSharpSensor.SensorReached(343);
  }
  public static void Del(A<int> a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(344);
    System.Console.WriteLine("Solved");
    IACSharpSensor.IACSharpSensor.SensorReached(345);
  }
}
