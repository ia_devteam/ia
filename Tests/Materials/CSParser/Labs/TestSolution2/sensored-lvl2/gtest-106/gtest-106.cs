public struct KeyValuePair<X, Y>
{
  public KeyValuePair(X x, Y y)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(34);
  }
}
public interface IComparer<T>
{
  int Compare(T x);
}
public class KeyValuePairComparer<K, V> : IComparer<KeyValuePair<K, V>>
{
  public int Compare(KeyValuePair<K, V> a)
  {
    System.Int32 RNTRNTRNT_5 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(35);
    return RNTRNTRNT_5;
  }
}
public class TreeBag<T>
{
  IComparer<T> comparer;
  T item;
  public TreeBag(IComparer<T> comparer, T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(36);
    this.comparer = comparer;
    this.item = item;
    IACSharpSensor.IACSharpSensor.SensorReached(37);
  }
  public int Find()
  {
    System.Int32 RNTRNTRNT_6 = comparer.Compare(item);
    IACSharpSensor.IACSharpSensor.SensorReached(38);
    return RNTRNTRNT_6;
  }
}
public class X
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(39);
    KeyValuePair<int, int> pair = new KeyValuePair<int, int>(3, 89);
    KeyValuePairComparer<int, int> comparer = new KeyValuePairComparer<int, int>();
    TreeBag<KeyValuePair<int, int>> bag = new TreeBag<KeyValuePair<int, int>>(comparer, pair);
    bag.Find();
    IACSharpSensor.IACSharpSensor.SensorReached(40);
  }
}
