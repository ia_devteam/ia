class MainClass
{
  class Gen<T>
  {
    public void Test()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(15);
    }
  }
  class Der : Gen<int>
  {
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    object o = new Der();
    Gen<int> b = (Gen<int>)o;
    b.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(17);
  }
}
