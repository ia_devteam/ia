using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
public delegate R Mapper<A, R>(A x);
public interface IMyList<T> : IEnumerable<T>
{
  int Count { get; }
  T this[int i] { get; set; }
  void Add(T item);
  void Insert(int i, T item);
  void RemoveAt(int i);
  IMyList<U> Map<U>(Mapper<T, U> f);
}
public class LinkedList<T> : IMyList<T>
{
  protected int size;
  protected Node first, last;
  protected class Node
  {
    public Node prev, next;
    public T item;
    public Node(T item)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(104);
      this.item = item;
      IACSharpSensor.IACSharpSensor.SensorReached(105);
    }
    public Node(T item, Node prev, Node next)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(106);
      this.item = item;
      this.prev = prev;
      this.next = next;
      IACSharpSensor.IACSharpSensor.SensorReached(107);
    }
  }
  public LinkedList()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(108);
    first = last = null;
    size = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(109);
  }
  public LinkedList(params T[] arr) : this()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(110);
    foreach (T x in arr) {
      IACSharpSensor.IACSharpSensor.SensorReached(111);
      Add(x);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(112);
  }
  public int Count {
    get {
      System.Int32 RNTRNTRNT_20 = size;
      IACSharpSensor.IACSharpSensor.SensorReached(113);
      return RNTRNTRNT_20;
    }
  }
  public T this[int i] {
    get {
      T RNTRNTRNT_21 = @get(i).item;
      IACSharpSensor.IACSharpSensor.SensorReached(114);
      return RNTRNTRNT_21;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(115);
      @get(i).item = value;
      IACSharpSensor.IACSharpSensor.SensorReached(116);
    }
  }
  private Node @get(int n)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(117);
    if (n < 0 || n >= size) {
      IACSharpSensor.IACSharpSensor.SensorReached(118);
      throw new IndexOutOfRangeException();
    } else if (n < size / 2) {
      IACSharpSensor.IACSharpSensor.SensorReached(123);
      Node node = first;
      IACSharpSensor.IACSharpSensor.SensorReached(124);
      for (int i = 0; i < n; i++) {
        IACSharpSensor.IACSharpSensor.SensorReached(125);
        node = node.next;
      }
      Node RNTRNTRNT_23 = node;
      IACSharpSensor.IACSharpSensor.SensorReached(126);
      return RNTRNTRNT_23;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(119);
      Node node = last;
      IACSharpSensor.IACSharpSensor.SensorReached(120);
      for (int i = size - 1; i > n; i--) {
        IACSharpSensor.IACSharpSensor.SensorReached(121);
        node = node.prev;
      }
      Node RNTRNTRNT_22 = node;
      IACSharpSensor.IACSharpSensor.SensorReached(122);
      return RNTRNTRNT_22;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(127);
  }
  public void Add(T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(128);
    Insert(size, item);
    IACSharpSensor.IACSharpSensor.SensorReached(129);
  }
  public void Insert(int i, T item)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(130);
    if (i == 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(131);
      if (first == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(132);
        first = last = new Node(item);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(133);
        Node tmp = new Node(item, null, first);
        first.prev = tmp;
        first = tmp;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(134);
      size++;
    } else if (i == size) {
      IACSharpSensor.IACSharpSensor.SensorReached(136);
      if (last == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(137);
        first = last = new Node(item);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(138);
        Node tmp = new Node(item, last, null);
        last.next = tmp;
        last = tmp;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(139);
      size++;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(135);
      Node node = @get(i);
      Node newnode = new Node(item, node.prev, node);
      node.prev.next = newnode;
      node.prev = newnode;
      size++;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(140);
  }
  public void RemoveAt(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(141);
    Node node = @get(i);
    IACSharpSensor.IACSharpSensor.SensorReached(142);
    if (node.prev == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(143);
      first = node.next;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(144);
      node.prev.next = node.next;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(145);
    if (node.next == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(146);
      last = node.prev;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(147);
      node.next.prev = node.prev;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(148);
    size--;
    IACSharpSensor.IACSharpSensor.SensorReached(149);
  }
  public override bool Equals(Object that)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(150);
    if (that != null && GetType() == that.GetType() && this.size == ((IMyList<T>)that).Count) {
      IACSharpSensor.IACSharpSensor.SensorReached(151);
      Node thisnode = this.first;
      IEnumerator<T> thatenm = ((IMyList<T>)that).GetEnumerator();
      IACSharpSensor.IACSharpSensor.SensorReached(152);
      while (thisnode != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(153);
        if (!thatenm.MoveNext()) {
          IACSharpSensor.IACSharpSensor.SensorReached(154);
          throw new ApplicationException("Impossible: LinkedList<T>.Equals");
        }
        IACSharpSensor.IACSharpSensor.SensorReached(155);
        if (!thisnode.item.Equals(thatenm.Current)) {
          System.Boolean RNTRNTRNT_24 = false;
          IACSharpSensor.IACSharpSensor.SensorReached(156);
          return RNTRNTRNT_24;
        }
        IACSharpSensor.IACSharpSensor.SensorReached(157);
        thisnode = thisnode.next;
      }
      System.Boolean RNTRNTRNT_25 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(158);
      return RNTRNTRNT_25;
    } else {
      System.Boolean RNTRNTRNT_26 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(159);
      return RNTRNTRNT_26;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(160);
  }
  public override int GetHashCode()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(161);
    int hash = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    foreach (T x in this) {
      IACSharpSensor.IACSharpSensor.SensorReached(163);
      hash ^= x.GetHashCode();
    }
    System.Int32 RNTRNTRNT_27 = hash;
    IACSharpSensor.IACSharpSensor.SensorReached(164);
    return RNTRNTRNT_27;
  }
  public static explicit operator LinkedList<T>(T[] arr)
  {
    LinkedList<T> RNTRNTRNT_28 = new LinkedList<T>(arr);
    IACSharpSensor.IACSharpSensor.SensorReached(165);
    return RNTRNTRNT_28;
  }
  public static LinkedList<T> operator +(LinkedList<T> xs1, LinkedList<T> xs2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(166);
    LinkedList<T> res = new LinkedList<T>();
    IACSharpSensor.IACSharpSensor.SensorReached(167);
    foreach (T x in xs1) {
      IACSharpSensor.IACSharpSensor.SensorReached(168);
      res.Add(x);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(169);
    foreach (T x in xs2) {
      IACSharpSensor.IACSharpSensor.SensorReached(170);
      res.Add(x);
    }
    LinkedList<T> RNTRNTRNT_29 = res;
    IACSharpSensor.IACSharpSensor.SensorReached(171);
    return RNTRNTRNT_29;
  }
  public IMyList<U> Map<U>(Mapper<T, U> f)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(172);
    LinkedList<U> res = new LinkedList<U>();
    IACSharpSensor.IACSharpSensor.SensorReached(173);
    foreach (T x in this) {
      IACSharpSensor.IACSharpSensor.SensorReached(174);
      res.Add(f(x));
    }
    IMyList<U> RNTRNTRNT_30 = res;
    IACSharpSensor.IACSharpSensor.SensorReached(175);
    return RNTRNTRNT_30;
  }
  public IEnumerator<T> GetEnumerator()
  {
    IEnumerator<T> RNTRNTRNT_31 = new LinkedListEnumerator(this);
    IACSharpSensor.IACSharpSensor.SensorReached(176);
    return RNTRNTRNT_31;
  }
  IEnumerator IEnumerable.GetEnumerator()
  {
    IEnumerator RNTRNTRNT_32 = new LinkedListEnumerator(this);
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    return RNTRNTRNT_32;
  }
  private class LinkedListEnumerator : IEnumerator<T>
  {
    T curr;
    bool valid;
    Node next;
    public LinkedListEnumerator(LinkedList<T> lst)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(178);
      next = lst.first;
      valid = false;
      IACSharpSensor.IACSharpSensor.SensorReached(179);
    }
    public T Current {
      get {
        IACSharpSensor.IACSharpSensor.SensorReached(180);
        if (valid) {
          T RNTRNTRNT_33 = curr;
          IACSharpSensor.IACSharpSensor.SensorReached(181);
          return RNTRNTRNT_33;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(182);
          throw new InvalidOperationException();
        }
      }
    }
    object IEnumerator.Current {
      get {
        System.Object RNTRNTRNT_34 = Current;
        IACSharpSensor.IACSharpSensor.SensorReached(183);
        return RNTRNTRNT_34;
      }
    }
    public bool MoveNext()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(184);
      if (next != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(185);
        curr = next.item;
        next = next.next;
        valid = true;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(186);
        valid = false;
      }
      System.Boolean RNTRNTRNT_35 = valid;
      IACSharpSensor.IACSharpSensor.SensorReached(187);
      return RNTRNTRNT_35;
    }
    public void Reset()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(188);
      throw new NotImplementedException();
    }
    public void Dispose()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(189);
      curr = default(T);
      next = null;
      valid = false;
      IACSharpSensor.IACSharpSensor.SensorReached(190);
    }
  }
}
class SortedList<T> : LinkedList<T> where T : IComparable<T>
{
  public void Insert(T x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(191);
    Node node = first;
    IACSharpSensor.IACSharpSensor.SensorReached(192);
    while (node != null && x.CompareTo(node.item) > 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(193);
      node = node.next;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(194);
    if (node == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(195);
      Add(x);
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(196);
      Node newnode = new Node(x);
      IACSharpSensor.IACSharpSensor.SensorReached(197);
      if (node.prev == null) {
        IACSharpSensor.IACSharpSensor.SensorReached(198);
        first = newnode;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(199);
        node.prev.next = newnode;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(200);
      newnode.next = node;
      newnode.prev = node.prev;
      node.prev = newnode;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(201);
  }
}
interface IPrintable
{
  void Print(TextWriter fs);
}
class PrintableLinkedList<T> : LinkedList<T>, IPrintable where T : IPrintable
{
  public void Print(TextWriter fs)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(202);
    bool firstElement = true;
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    foreach (T x in this) {
      IACSharpSensor.IACSharpSensor.SensorReached(204);
      x.Print(fs);
      IACSharpSensor.IACSharpSensor.SensorReached(205);
      if (firstElement) {
        IACSharpSensor.IACSharpSensor.SensorReached(206);
        firstElement = false;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(207);
        fs.Write(", ");
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(208);
  }
}
class MyString : IComparable<MyString>
{
  private readonly String s;
  public MyString(String s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(209);
    this.s = s;
    IACSharpSensor.IACSharpSensor.SensorReached(210);
  }
  public int CompareTo(MyString that)
  {
    System.Int32 RNTRNTRNT_36 = String.Compare(that.Value, s);
    IACSharpSensor.IACSharpSensor.SensorReached(211);
    return RNTRNTRNT_36;
  }
  public bool Equals(MyString that)
  {
    System.Boolean RNTRNTRNT_37 = that.Value == s;
    IACSharpSensor.IACSharpSensor.SensorReached(212);
    return RNTRNTRNT_37;
  }
  public String Value {
    get {
      String RNTRNTRNT_38 = s;
      IACSharpSensor.IACSharpSensor.SensorReached(213);
      return RNTRNTRNT_38;
    }
  }
}
class MyTest
{
  public static void Main(String[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(214);
    LinkedList<double> dLst = new LinkedList<double>(7.0, 9.0, 13.0, 0.0);
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    foreach (double d in dLst) {
      IACSharpSensor.IACSharpSensor.SensorReached(216);
      Console.Write("{0} ", d);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(217);
    Console.WriteLine();
    IMyList<int> iLst = dLst.Map<int>(new Mapper<double, int>(Math.Sign));
    IACSharpSensor.IACSharpSensor.SensorReached(218);
    foreach (int i in iLst) {
      IACSharpSensor.IACSharpSensor.SensorReached(219);
      Console.Write("{0} ", i);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    Console.WriteLine();
    IMyList<String> sLst = dLst.Map<String>(delegate(double d) { return "s" + d; });
    IACSharpSensor.IACSharpSensor.SensorReached(221);
    foreach (String s in sLst) {
      IACSharpSensor.IACSharpSensor.SensorReached(222);
      Console.Write("{0} ", s);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(223);
    Console.WriteLine();
    SortedList<MyString> sortedLst = new SortedList<MyString>();
    sortedLst.Insert(new MyString("New York"));
    sortedLst.Insert(new MyString("Rome"));
    sortedLst.Insert(new MyString("Dublin"));
    sortedLst.Insert(new MyString("Riyadh"));
    sortedLst.Insert(new MyString("Tokyo"));
    IACSharpSensor.IACSharpSensor.SensorReached(224);
    foreach (MyString s in sortedLst) {
      IACSharpSensor.IACSharpSensor.SensorReached(225);
      Console.Write("{0}   ", s.Value);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(226);
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(227);
  }
}
