using System.Collections.Generic;
interface IB
{
  bool foo();
}
class B : IB
{
  public bool foo()
  {
    System.Boolean RNTRNTRNT_66 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(330);
    return RNTRNTRNT_66;
  }
}
interface Filter<T> where T : IB
{
  T Is(IB x);
}
struct K : IB
{
  public bool foo()
  {
    System.Boolean RNTRNTRNT_67 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(331);
    return RNTRNTRNT_67;
  }
}
class MyFilter : Filter<K>
{
  public K Is(IB x)
  {
    K RNTRNTRNT_68 = new K();
    IACSharpSensor.IACSharpSensor.SensorReached(332);
    return RNTRNTRNT_68;
  }
}
class MyBFilter : Filter<B>
{
  public B Is(IB x)
  {
    B RNTRNTRNT_69 = new B();
    IACSharpSensor.IACSharpSensor.SensorReached(333);
    return RNTRNTRNT_69;
  }
}
class M
{
  static List<T> foo1<T>(Filter<T> x) where T : IB
  {
    IACSharpSensor.IACSharpSensor.SensorReached(334);
    List<T> result = new List<T>();
    T maybe = x.Is(new B());
    IACSharpSensor.IACSharpSensor.SensorReached(335);
    if (maybe != null) {
      IACSharpSensor.IACSharpSensor.SensorReached(336);
      result.Add(maybe);
    }
    List<T> RNTRNTRNT_70 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(337);
    return RNTRNTRNT_70;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(338);
    MyFilter m = new MyFilter();
    System.Console.WriteLine(foo1<K>(m).Count);
    MyBFilter mb = new MyBFilter();
    System.Console.WriteLine(foo1<B>(mb).Count);
    IACSharpSensor.IACSharpSensor.SensorReached(339);
  }
}
