using System;
public static class Assert
{
  public static int Errors {
    get {
      System.Int32 RNTRNTRNT_77 = errors;
      IACSharpSensor.IACSharpSensor.SensorReached(421);
      return RNTRNTRNT_77;
    }
  }
  static int errors = 0;
  static void Error(string method, string text)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(422);
    Console.WriteLine("Assert failed: {0} ({1})", method, text);
    errors++;
    IACSharpSensor.IACSharpSensor.SensorReached(423);
  }
  public static void IsTrue(string text, bool b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(424);
    if (!b) {
      IACSharpSensor.IACSharpSensor.SensorReached(425);
      Error("IsTrue", text);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(426);
  }
  public static void IsFalse(string text, bool b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(427);
    if (b) {
      IACSharpSensor.IACSharpSensor.SensorReached(428);
      Error("IsFalse", text);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(429);
  }
  public static void IsNull<T>(string text, Nullable<T> nullable) where T : struct
  {
    IACSharpSensor.IACSharpSensor.SensorReached(430);
    if (nullable.HasValue) {
      IACSharpSensor.IACSharpSensor.SensorReached(431);
      Error("IsNull", text);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(432);
  }
  public static void IsNotNull<T>(string text, Nullable<T> nullable) where T : struct
  {
    IACSharpSensor.IACSharpSensor.SensorReached(433);
    if (!nullable.HasValue) {
      IACSharpSensor.IACSharpSensor.SensorReached(434);
      Error("IsNotNull", text);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(435);
  }
  public static void IsTrue(string text, Nullable<bool> b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(436);
    if (!b.HasValue || !b.Value) {
      IACSharpSensor.IACSharpSensor.SensorReached(437);
      Error("IsTrue", text);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(438);
  }
  public static void IsFalse(string text, Nullable<bool> b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(439);
    if (!b.HasValue || b.Value) {
      IACSharpSensor.IACSharpSensor.SensorReached(440);
      Error("IsFalse", text);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(441);
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(442);
    bool? a = null, b = false, c = true;
    bool? d = null, e = false, f = true;
    Assert.IsNull("a", a);
    Assert.IsFalse("b", b);
    Assert.IsTrue("c", c);
    Assert.IsTrue("a == d", a == d);
    Assert.IsTrue("b == e", b == e);
    Assert.IsTrue("c == f", c == f);
    Assert.IsFalse("a != d", a != d);
    Assert.IsFalse("a == b", a == b);
    Assert.IsTrue("a != b", a != b);
    Assert.IsNull("d & a", d & a);
    Assert.IsFalse("d & b", d & b);
    Assert.IsNull("d & c", d & c);
    Assert.IsFalse("e & a", e & a);
    Assert.IsFalse("e & b", e & b);
    Assert.IsFalse("e & c", e & c);
    Assert.IsNull("f & a", f & a);
    Assert.IsFalse("f & b", f & b);
    Assert.IsTrue("f & c", f & c);
    Assert.IsNull("d | a", d | a);
    Assert.IsNull("d | b", d | b);
    Assert.IsTrue("d | c", d | c);
    Assert.IsNull("e | a", e | a);
    Assert.IsFalse("e | b", e | b);
    Assert.IsTrue("e | c", e | c);
    Assert.IsTrue("f | a", f | a);
    Assert.IsTrue("f | b", f | b);
    Assert.IsTrue("f | c", f | c);
    int? g = 3, h = null, i = 3, j = null;
    Assert.IsFalse("g == null", g == null);
    Assert.IsTrue("g != null", g != null);
    Assert.IsTrue("h == null", h == null);
    Assert.IsFalse("h != null", h != null);
    Assert.IsTrue("g == i", g == i);
    Assert.IsFalse("g != i", g != i);
    Assert.IsFalse("g == j", g == j);
    Assert.IsTrue("g != j", g != j);
    Assert.IsFalse("h == i", h == i);
    Assert.IsTrue("h != i", h != i);
    Assert.IsTrue("h == j", h == j);
    Assert.IsFalse("h != j", h != j);
    Console.WriteLine("{0} errors.", Assert.Errors);
    System.Int32 RNTRNTRNT_78 = Assert.Errors;
    IACSharpSensor.IACSharpSensor.SensorReached(443);
    return RNTRNTRNT_78;
  }
}
