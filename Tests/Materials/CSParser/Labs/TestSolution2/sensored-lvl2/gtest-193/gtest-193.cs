using System;
using System.Collections.Generic;
public class OrderedMultiDictionary<T, U>
{
  private RedBlackTree<KeyValuePair<T, U>> tree;
  private IEnumerator<T> EnumerateKeys(RedBlackTree<KeyValuePair<T, U>>.RangeTester rangeTester)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(759);
    tree.EnumerateRange(rangeTester);
    IACSharpSensor.IACSharpSensor.SensorReached(760);
    yield break;
  }
}
internal class RedBlackTree<S>
{
  public delegate int RangeTester(S item);
  public IEnumerable<S> EnumerateRange(RangeTester rangeTester)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(761);
    yield break;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(762);
  }
}
