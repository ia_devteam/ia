public class Test
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(260);
    new Test(delegate() { });
    IACSharpSensor.IACSharpSensor.SensorReached(261);
  }
  public Test(TestMethod test)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(262);
  }
  public Test(TestMethod2 test2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(263);
  }
}
public delegate void TestMethod();
public delegate void TestMethod2(object o);
