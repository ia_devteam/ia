static class Foo
{
  delegate string[,] SimpleDelegate();
  static void Baz(SimpleDelegate sd)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(611);
    sd();
    IACSharpSensor.IACSharpSensor.SensorReached(612);
  }
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(613);
    Baz(delegate() { return new string[,] {{ "aa" },{ "bb" }}; });
    IACSharpSensor.IACSharpSensor.SensorReached(614);
  }
}
