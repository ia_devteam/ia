using System;
using System.Collections;
delegate bool predicate(object a);
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(88);
    ArrayList a = new ArrayList();
    IACSharpSensor.IACSharpSensor.SensorReached(89);
    for (int i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(90);
      a.Add(i);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(91);
    ArrayList even = Find(delegate(object arg) { return ((((int)arg) % 2) == 0); }, a);
    Console.WriteLine("Even numbers");
    IACSharpSensor.IACSharpSensor.SensorReached(92);
    foreach (object r in even) {
      IACSharpSensor.IACSharpSensor.SensorReached(93);
      Console.WriteLine(r);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(94);
    if (even.Count != 5) {
      System.Int32 RNTRNTRNT_26 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(95);
      return RNTRNTRNT_26;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(96);
    if (((int)even[0]) != 0 || ((int)even[1]) != 2 || ((int)even[2]) != 4 || ((int)even[3]) != 6 || ((int)even[4]) != 8) {
      System.Int32 RNTRNTRNT_27 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(97);
      return RNTRNTRNT_27;
    }
    System.Int32 RNTRNTRNT_28 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(98);
    return RNTRNTRNT_28;
  }
  static ArrayList Find(predicate p, ArrayList source)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(99);
    ArrayList result = new ArrayList();
    IACSharpSensor.IACSharpSensor.SensorReached(100);
    foreach (object a in source) {
      IACSharpSensor.IACSharpSensor.SensorReached(101);
      if (p(a)) {
        IACSharpSensor.IACSharpSensor.SensorReached(102);
        result.Add(a);
      }
    }
    ArrayList RNTRNTRNT_29 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(103);
    return RNTRNTRNT_29;
  }
}
