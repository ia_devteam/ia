using System;
using System.Threading;
delegate void D(object o);
class T
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(604);
    D d = delegate(object state) {
      IACSharpSensor.IACSharpSensor.SensorReached(605);
      try {
      } catch {
        IACSharpSensor.IACSharpSensor.SensorReached(606);
        throw;
        IACSharpSensor.IACSharpSensor.SensorReached(607);
      } finally {
      }
    };
    IACSharpSensor.IACSharpSensor.SensorReached(608);
  }
  static void Test_1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(609);
    System.Threading.ThreadPool.QueueUserWorkItem(delegate(object o) {
      using (System.Threading.Timer t = new System.Threading.Timer(null, null, 1, 1)) {
      }
    });
    IACSharpSensor.IACSharpSensor.SensorReached(610);
  }
}
