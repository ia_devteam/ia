delegate void S();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(67);
    int a = 2;
    int b = 1;
    S d = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(68);
      a = b;
    };
    System.Int32 RNTRNTRNT_19 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(69);
    return RNTRNTRNT_19;
  }
}
