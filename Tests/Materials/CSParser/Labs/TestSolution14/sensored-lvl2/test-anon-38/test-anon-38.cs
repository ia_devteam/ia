using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public void Hello(long i, long j)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(266);
  }
  public void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(267);
    long j = 1 << i;
    Hello(i, j);
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(268);
      long k = 5 * j;
      Hello(j, k);
      void  RNTRNTRNT_59 = delegate { Hello(j, k); };
      IACSharpSensor.IACSharpSensor.SensorReached(269);
      return RNTRNTRNT_59;
    };
    Simple simple = foo();
    simple();
    IACSharpSensor.IACSharpSensor.SensorReached(270);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(271);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(272);
  }
}
