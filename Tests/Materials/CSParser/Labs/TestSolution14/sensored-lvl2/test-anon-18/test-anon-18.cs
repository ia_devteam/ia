using System;
delegate void A();
class DelegateTest
{
  static void Main(string[] argv)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(132);
    Console.WriteLine("Test");
    IACSharpSensor.IACSharpSensor.SensorReached(133);
    foreach (string arg in argv) {
      IACSharpSensor.IACSharpSensor.SensorReached(134);
      Console.WriteLine("OUT: {0}", arg);
      A a = delegate {
        IACSharpSensor.IACSharpSensor.SensorReached(135);
        Console.WriteLine("arg: {0}", arg);
      };
      a();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(136);
  }
}
