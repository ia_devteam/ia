using System;
public class X
{
  public delegate void TestDelegate();
  static long sum_i, sum_j, sum_k;
  static ulong sum_p;
  public static int Test(int p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(465);
    TestDelegate d = null;
    IACSharpSensor.IACSharpSensor.SensorReached(466);
    for (int i = 1; i <= 5; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(467);
      for (int j = i; j <= 8; j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(468);
        int k = i;
        TestDelegate temp = delegate {
          IACSharpSensor.IACSharpSensor.SensorReached(469);
          Console.WriteLine("i = {0}, j = {1}, k = {2}, p = {3}", i, j, k, p);
          sum_i += 1 << i;
          sum_j += 1 << j;
          sum_k += 1 << k;
          sum_p += (ulong)(1 << p);
          p += k;
        };
        temp();
        d += temp;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(470);
    Console.WriteLine("SUM i = {0}, j = {1}, k = {2}, p = {3}", sum_i, sum_j, sum_k, sum_p);
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(471);
    if (sum_i != 300) {
      System.Int32 RNTRNTRNT_98 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(472);
      return RNTRNTRNT_98;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(473);
    if (sum_j != 2498) {
      System.Int32 RNTRNTRNT_99 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(474);
      return RNTRNTRNT_99;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(475);
    if (sum_k != 300) {
      System.Int32 RNTRNTRNT_100 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(476);
      return RNTRNTRNT_100;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(477);
    if (sum_p != 1825434804) {
      System.Int32 RNTRNTRNT_101 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(478);
      return RNTRNTRNT_101;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(479);
    sum_i = sum_j = sum_k = 0;
    sum_p = 0;
    d();
    Console.WriteLine("SUM i = {0}, j = {1}, k = {2}, p = {3}", sum_i, sum_j, sum_k, sum_p);
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(480);
    if (sum_i != 1920) {
      System.Int32 RNTRNTRNT_102 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(481);
      return RNTRNTRNT_102;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(482);
    if (sum_j != 15360) {
      System.Int32 RNTRNTRNT_103 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(483);
      return RNTRNTRNT_103;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(484);
    if (sum_k != 300) {
      System.Int32 RNTRNTRNT_104 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(485);
      return RNTRNTRNT_104;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(486);
    if (sum_p != 18446744073385831629uL) {
      System.Int32 RNTRNTRNT_105 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(487);
      return RNTRNTRNT_105;
    }
    System.Int32 RNTRNTRNT_106 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(488);
    return RNTRNTRNT_106;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(489);
    int result = Test(5);
    IACSharpSensor.IACSharpSensor.SensorReached(490);
    if (result != 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(491);
      Console.WriteLine("ERROR: {0}", result);
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(492);
      Console.WriteLine("OK");
    }
    System.Int32 RNTRNTRNT_107 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(493);
    return RNTRNTRNT_107;
  }
}
