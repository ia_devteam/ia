using System;
public class X
{
  public delegate void TestDelegate();
  static long sum_i, sum_j, sum_k;
  static ulong sum_p, sum_q;
  public static int Test(int p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(494);
    TestDelegate d = null;
    IACSharpSensor.IACSharpSensor.SensorReached(495);
    for (int i = 1; i <= 5; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(496);
      for (int j = i; j <= 8; j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(497);
        int k = i;
        TestDelegate temp = delegate {
          IACSharpSensor.IACSharpSensor.SensorReached(498);
          Console.WriteLine("i = {0}, j = {1}, k = {2}, p = {3}", i, j, k, p);
          sum_i += 1 << i;
          sum_j += 1 << j;
          sum_k += 1 << k;
          sum_p += (ulong)(1 << p);
          p += k;
          ulong q = (ulong)(i * j);
          d += delegate {
            Console.WriteLine("Nested i = {0}, j = {1}, " + "k = {2}, p = {3}, q = {4}", i, j, k, p, q);
            sum_q += q;
          };
        };
        temp();
        d += temp;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(499);
    Console.WriteLine("SUM i = {0}, j = {1}, k = {2}, p = {3}", sum_i, sum_j, sum_k, sum_p);
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(500);
    if (sum_i != 300) {
      System.Int32 RNTRNTRNT_108 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(501);
      return RNTRNTRNT_108;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(502);
    if (sum_j != 2498) {
      System.Int32 RNTRNTRNT_109 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(503);
      return RNTRNTRNT_109;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(504);
    if (sum_k != 300) {
      System.Int32 RNTRNTRNT_110 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(505);
      return RNTRNTRNT_110;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(506);
    if (sum_p != 1825434804) {
      System.Int32 RNTRNTRNT_111 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(507);
      return RNTRNTRNT_111;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(508);
    sum_i = sum_j = sum_k = 0;
    sum_p = sum_q = 0;
    d();
    Console.WriteLine("SUM i = {0}, j = {1}, k = {2}, p = {3}, q = {4}", sum_i, sum_j, sum_k, sum_p, sum_q);
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(509);
    if (sum_i != 1920) {
      System.Int32 RNTRNTRNT_112 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(510);
      return RNTRNTRNT_112;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(511);
    if (sum_j != 15360) {
      System.Int32 RNTRNTRNT_113 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(512);
      return RNTRNTRNT_113;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(513);
    if (sum_k != 300) {
      System.Int32 RNTRNTRNT_114 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(514);
      return RNTRNTRNT_114;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(515);
    if (sum_p != 18446744073385831629uL) {
      System.Int32 RNTRNTRNT_115 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(516);
      return RNTRNTRNT_115;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(517);
    if (sum_q != 455) {
      System.Int32 RNTRNTRNT_116 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(518);
      return RNTRNTRNT_116;
    }
    System.Int32 RNTRNTRNT_117 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(519);
    return RNTRNTRNT_117;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(520);
    int result = Test(5);
    IACSharpSensor.IACSharpSensor.SensorReached(521);
    if (result != 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(522);
      Console.WriteLine("ERROR: {0}", result);
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(523);
      Console.WriteLine("OK");
    }
    System.Int32 RNTRNTRNT_118 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(524);
    return RNTRNTRNT_118;
  }
}
