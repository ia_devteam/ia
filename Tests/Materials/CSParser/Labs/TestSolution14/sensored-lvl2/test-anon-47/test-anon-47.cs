using System;
public delegate void Foo();
public class World
{
  public void Hello(long a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(330);
  }
  public void Test(int t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(331);
    Hello(t);
    long j = 1 << t;
    IACSharpSensor.IACSharpSensor.SensorReached(332);
    for (int u = 0; u < j; u++) {
      IACSharpSensor.IACSharpSensor.SensorReached(333);
      long test = t;
      Foo foo = delegate {
        IACSharpSensor.IACSharpSensor.SensorReached(334);
        long v = u * t * test;
        Hello(v);
      };
    }
    IACSharpSensor.IACSharpSensor.SensorReached(335);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(336);
    World world = new World();
    world.Test(5);
    IACSharpSensor.IACSharpSensor.SensorReached(337);
  }
}
