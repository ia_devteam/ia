using System;
using System.Collections;
using System.Text;
using System.Timers;
namespace Delegates
{
  class Space
  {
    public int Value;
    public delegate void DoCopy();
    public DoCopy CopyIt;
    public void Leak(bool useArray, int max)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(241);
      DoCopy one = null;
      IACSharpSensor.IACSharpSensor.SensorReached(242);
      if (useArray) {
        IACSharpSensor.IACSharpSensor.SensorReached(243);
        int[] work;
        one = delegate { work = new int[max]; };
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(244);
        one = delegate {
          int xans = (max + 1) * max / 2;
        };
      }
      IACSharpSensor.IACSharpSensor.SensorReached(245);
      Console.WriteLine("Here goes...");
      one();
      Console.WriteLine("... made it");
      IACSharpSensor.IACSharpSensor.SensorReached(246);
    }
  }
  class Program
  {
    static void SpaceLeak()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(247);
      Space s = new Space();
      Console.WriteLine(s.Value);
      s.Leak(false, 1);
      Console.WriteLine(s.Value);
      IACSharpSensor.IACSharpSensor.SensorReached(248);
    }
    static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(249);
      SpaceLeak();
      IACSharpSensor.IACSharpSensor.SensorReached(250);
    }
  }
}
