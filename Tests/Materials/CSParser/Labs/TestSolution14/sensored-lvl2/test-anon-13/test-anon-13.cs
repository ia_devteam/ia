using System;
delegate void D();
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(104);
    X x = new X(1);
    X y = new X(100);
    D a = x.T();
    D b = y.T();
    a();
    b();
    IACSharpSensor.IACSharpSensor.SensorReached(105);
  }
  X(int start)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(106);
    ins = start;
    IACSharpSensor.IACSharpSensor.SensorReached(107);
  }
  int ins;
  D T()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(108);
    D d = delegate() {
      IACSharpSensor.IACSharpSensor.SensorReached(109);
      Console.WriteLine("My state is: " + CALL());
    };
    D RNTRNTRNT_30 = d;
    IACSharpSensor.IACSharpSensor.SensorReached(110);
    return RNTRNTRNT_30;
  }
  string CALL()
  {
    System.String RNTRNTRNT_31 = "GOOD";
    IACSharpSensor.IACSharpSensor.SensorReached(111);
    return RNTRNTRNT_31;
  }
}
