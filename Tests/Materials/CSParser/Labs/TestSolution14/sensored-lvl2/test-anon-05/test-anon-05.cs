using System;
delegate void S();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(29);
    int i;
    S b = null;
    IACSharpSensor.IACSharpSensor.SensorReached(30);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(31);
      int j = 0;
      b = delegate {
        Console.WriteLine("i={0} j={1}", i, j);
        i = i + 1;
        j = j + 1;
      };
    }
    IACSharpSensor.IACSharpSensor.SensorReached(32);
    Console.WriteLine("i = {0}", i);
    b();
    Console.WriteLine("i = {0}", i);
    IACSharpSensor.IACSharpSensor.SensorReached(33);
    if (!t(i, 11)) {
      System.Int32 RNTRNTRNT_6 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(34);
      return RNTRNTRNT_6;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(35);
    b();
    IACSharpSensor.IACSharpSensor.SensorReached(36);
    if (!t(i, 12)) {
      System.Int32 RNTRNTRNT_7 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(37);
      return RNTRNTRNT_7;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(38);
    Console.WriteLine("i = {0}", i);
    Console.WriteLine("Test is OK");
    System.Int32 RNTRNTRNT_8 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(39);
    return RNTRNTRNT_8;
  }
  static bool t(int a, int b)
  {
    System.Boolean RNTRNTRNT_9 = a == b;
    IACSharpSensor.IACSharpSensor.SensorReached(40);
    return RNTRNTRNT_9;
  }
}
