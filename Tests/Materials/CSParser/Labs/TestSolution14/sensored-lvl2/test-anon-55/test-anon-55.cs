using System;
public class Foo
{
  protected delegate void Hello();
  protected void Test(Hello hello)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(411);
    hello();
    IACSharpSensor.IACSharpSensor.SensorReached(412);
  }
  private void Private()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    Console.WriteLine("Private!");
    IACSharpSensor.IACSharpSensor.SensorReached(414);
  }
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(415);
    Test(delegate { Private(); });
    IACSharpSensor.IACSharpSensor.SensorReached(416);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(417);
    Foo foo = new Foo();
    foo.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(418);
  }
}
