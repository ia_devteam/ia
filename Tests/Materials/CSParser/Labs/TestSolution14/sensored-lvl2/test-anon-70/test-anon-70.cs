using System;
unsafe class UnsafeClass
{
  public int* GetUnsafeValue()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(588);
    return null;
  }
}
public class C
{
  delegate void D();
  static void Test(D d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(589);
  }
  unsafe static void UnsafeTests()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(590);
    UnsafeClass v = new UnsafeClass();
    Test(delegate() {
      int i = *v.GetUnsafeValue();
    });
    IACSharpSensor.IACSharpSensor.SensorReached(591);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(592);
    Exception diffException;
    Test(delegate() {
      diffException = null;
      try {
      } catch (Exception ex) {
        diffException = ex;
      } finally {
      }
      try {
      } catch {
      }
    });
    int[] i_a = new int[] {
      1,
      2,
      3
    };
    Test(delegate() {
      foreach (int t in i_a) {
      }
    });
    Test(delegate() { Console.WriteLine(typeof(void)); });
    IACSharpSensor.IACSharpSensor.SensorReached(593);
  }
}
