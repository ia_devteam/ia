using System;
delegate void D();
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(119);
    X x = new X(1);
    X y = new X(100);
    D a = x.T();
    D b = y.T();
    a();
    b();
    IACSharpSensor.IACSharpSensor.SensorReached(120);
  }
  X(int start)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(121);
    ins = start;
    IACSharpSensor.IACSharpSensor.SensorReached(122);
  }
  int ins;
  D T()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(123);
    D d = delegate() {
      IACSharpSensor.IACSharpSensor.SensorReached(124);
      Console.WriteLine("My state is: " + ins);
    };
    D RNTRNTRNT_33 = d;
    IACSharpSensor.IACSharpSensor.SensorReached(125);
    return RNTRNTRNT_33;
  }
}
