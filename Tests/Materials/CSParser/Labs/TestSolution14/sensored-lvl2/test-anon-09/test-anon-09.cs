using System;
class X
{
  delegate void D();
  static int gt, gj;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(70);
    int times = 0;
    D d = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(71);
      int t = times++;
      int j = ++times;
      gt = t;
      gj = j;
    };
    d();
    IACSharpSensor.IACSharpSensor.SensorReached(72);
    if (gt != 0) {
      System.Int32 RNTRNTRNT_20 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(73);
      return RNTRNTRNT_20;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(74);
    if (gj != 2) {
      System.Int32 RNTRNTRNT_21 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(75);
      return RNTRNTRNT_21;
    }
    System.Int32 RNTRNTRNT_22 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(76);
    return RNTRNTRNT_22;
  }
}
