using System;
using System.Reflection;
[assembly: CLSCompliant(true)]
public class CLSClass
{
  [CLSCompliant(false)]
  public CLSClass(ulong l)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(684);
  }
  internal CLSClass(uint i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(685);
  }
  [CLSCompliant(false)]
  public ulong X_0 {
    set { IACSharpSensor.IACSharpSensor.SensorReached(686); }
  }
  [CLSCompliant(false)]
  protected ulong this[ulong i] {
    set { IACSharpSensor.IACSharpSensor.SensorReached(687); }
  }
  [CLSCompliant(false)]
  public ulong X_1;
  internal ulong X_2;
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(688);
  }
}
public class InnerTypeClasss
{
  public struct Struct
  {
  }
  public Struct Method()
  {
    Struct RNTRNTRNT_166 = new Struct();
    IACSharpSensor.IACSharpSensor.SensorReached(689);
    return RNTRNTRNT_166;
  }
}
