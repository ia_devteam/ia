using System;
using System.Collections;
class X
{
  delegate void A();
  static IEnumerator GetIt(int[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(383);
    foreach (int arg in args) {
      IACSharpSensor.IACSharpSensor.SensorReached(384);
      Console.WriteLine("OUT: {0}", arg);
      A a = delegate {
        IACSharpSensor.IACSharpSensor.SensorReached(385);
        Console.WriteLine("arg: {0}", arg);
        IACSharpSensor.IACSharpSensor.SensorReached(386);
        return;
      };
      a();
      Object RNTRNTRNT_81 = arg;
      IACSharpSensor.IACSharpSensor.SensorReached(387);
      yield return RNTRNTRNT_81;
      IACSharpSensor.IACSharpSensor.SensorReached(388);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(389);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(390);
    IEnumerator enumerator = GetIt(new int[] {
      1,
      2,
      3
    });
    enumerator.MoveNext();
    System.Int32 RNTRNTRNT_82 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(391);
    return RNTRNTRNT_82;
  }
}
