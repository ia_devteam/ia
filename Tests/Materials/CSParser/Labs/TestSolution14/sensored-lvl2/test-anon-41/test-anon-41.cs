using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public void Hello(long k)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(287);
  }
  public void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(288);
    long j = 1 << i;
    Hello(j);
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(289);
      long k = j;
      Hello(j);
      void  RNTRNTRNT_62 = delegate {
        long l = k;
        Hello(j);
      };
      IACSharpSensor.IACSharpSensor.SensorReached(290);
      return RNTRNTRNT_62;
    };
    Simple simple = foo();
    simple();
    IACSharpSensor.IACSharpSensor.SensorReached(291);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(292);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(293);
  }
}
