using System;
delegate void S();
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(24);
    int a = 1;
    S b = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(25);
      float f = 1;
      Console.WriteLine(a);
      IACSharpSensor.IACSharpSensor.SensorReached(26);
      if (f == 2) {
        IACSharpSensor.IACSharpSensor.SensorReached(27);
        return;
      }
    };
    b();
    Console.WriteLine("Back, got " + a);
    IACSharpSensor.IACSharpSensor.SensorReached(28);
  }
}
