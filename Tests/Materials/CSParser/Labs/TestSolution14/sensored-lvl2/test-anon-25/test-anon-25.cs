using System;
delegate int D(int arg);
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(183);
    D x = T(1);
    int v = x(10);
    Console.WriteLine("Should be 11={0}", v);
    System.Int32 RNTRNTRNT_49 = v == 11 ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(184);
    return RNTRNTRNT_49;
  }
  static D T(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(185);
    D d = delegate(int arg) {
      D RNTRNTRNT_50 = arg + a;
      IACSharpSensor.IACSharpSensor.SensorReached(186);
      return RNTRNTRNT_50;
    };
    D RNTRNTRNT_51 = d;
    IACSharpSensor.IACSharpSensor.SensorReached(187);
    return RNTRNTRNT_51;
  }
}
