using System;
delegate void D();
class X
{
  static D r;
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(164);
    D d = T();
    d();
    r();
    r();
    IACSharpSensor.IACSharpSensor.SensorReached(165);
  }
  static D T()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(166);
    int var1 = 0;
    D d = delegate() {
      IACSharpSensor.IACSharpSensor.SensorReached(167);
      int var2 = 1;
      r = delegate {
        Console.WriteLine("var1: {0} var2: {1}", var1, var2);
        var2 = var2 + 1;
      };
      var1 = var1 + 1;
    };
    D RNTRNTRNT_44 = d;
    IACSharpSensor.IACSharpSensor.SensorReached(168);
    return RNTRNTRNT_44;
  }
}
