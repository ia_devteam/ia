using System;
delegate void D1();
delegate void D2();
public class DelegateTest
{
  static void Foo(D1 d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(638);
    d();
    IACSharpSensor.IACSharpSensor.SensorReached(639);
  }
  static void Foo(D2 d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(640);
  }
  static int counter = 99;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(641);
    Foo(new D1(delegate {
      counter = 82;
      Console.WriteLine("In");
    }));
    IACSharpSensor.IACSharpSensor.SensorReached(642);
    if (counter != 82) {
      System.Int32 RNTRNTRNT_140 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(643);
      return RNTRNTRNT_140;
    }
    System.Int32 RNTRNTRNT_141 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(644);
    return RNTRNTRNT_141;
  }
}
