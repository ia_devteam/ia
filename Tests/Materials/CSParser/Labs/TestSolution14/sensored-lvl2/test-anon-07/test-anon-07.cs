using System;
delegate void D();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    int a = 0;
    D d1 = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      Console.WriteLine("First");
      a = 1;
    };
    D d2 = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(55);
      Console.WriteLine("Second");
      a = 2;
    };
    IACSharpSensor.IACSharpSensor.SensorReached(56);
    if (!t(a, 0)) {
      System.Int32 RNTRNTRNT_14 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(57);
      return RNTRNTRNT_14;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(58);
    d1();
    IACSharpSensor.IACSharpSensor.SensorReached(59);
    if (!t(a, 1)) {
      System.Int32 RNTRNTRNT_15 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      return RNTRNTRNT_15;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(61);
    d2();
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    if (!t(a, 2)) {
      System.Int32 RNTRNTRNT_16 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(63);
      return RNTRNTRNT_16;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(64);
    Console.WriteLine("Test passes OK");
    System.Int32 RNTRNTRNT_17 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(65);
    return RNTRNTRNT_17;
  }
  static bool t(int a, int b)
  {
    System.Boolean RNTRNTRNT_18 = a == b;
    IACSharpSensor.IACSharpSensor.SensorReached(66);
    return RNTRNTRNT_18;
  }
}
