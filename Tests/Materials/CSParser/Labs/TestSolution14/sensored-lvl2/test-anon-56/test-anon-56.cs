delegate void QueueHandler(Observable sender);
class Observable
{
  static QueueHandler Queue;
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(419);
    Queue += (QueueHandler)delegate { System.Console.WriteLine("OK"); };
    IACSharpSensor.IACSharpSensor.SensorReached(420);
  }
}
