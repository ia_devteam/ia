using System;
[assembly: CLSCompliant(true)]
[CLSCompliant(false)]
public delegate uint MyDelegate();
[CLSCompliant(false)]
public interface IFake
{
  [CLSCompliant(true)]
  long AA(long arg);
  [CLSCompliant(false)]
  ulong BB { get; }
  [CLSCompliant(false)]
  event MyDelegate MyEvent;
}
[CLSCompliant(false)]
internal interface I
{
  [CLSCompliant(false)]
  void Foo();
  [CLSCompliant(true)]
  ulong this[int indexA] { set; }
}
interface I2
{
  int Test(int arg1, bool arg2);
}
public class CLSClass
{
  [CLSCompliant(false)]
  public delegate uint MyDelegate();
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(707);
  }
}
public class CLSClass_2
{
  [CLSCompliant(false)]
  public CLSClass_2(int[,,] b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(708);
  }
  public CLSClass_2(int[,] b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(709);
  }
  public void Test(int[,] b, int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(710);
  }
  public void Test(int[,,] b, bool b2)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(711);
  }
}
public class X1
{
  [CLSCompliant(false)]
  public void M2(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(712);
  }
}
public class X2 : X1
{
  public void M2(ref int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(713);
  }
}
