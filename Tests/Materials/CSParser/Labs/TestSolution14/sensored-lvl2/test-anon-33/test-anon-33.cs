using System;
delegate void Do();
class T
{
  static void doit(int v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(230);
    Console.WriteLine(v);
    IACSharpSensor.IACSharpSensor.SensorReached(231);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(232);
    Do[] arr = new Do[5];
    IACSharpSensor.IACSharpSensor.SensorReached(233);
    for (int i = 0; i < 5; ++i) {
      IACSharpSensor.IACSharpSensor.SensorReached(234);
      arr[i] = delegate { doit(i); };
    }
    IACSharpSensor.IACSharpSensor.SensorReached(235);
    for (int i = 0; i < 5; ++i) {
      IACSharpSensor.IACSharpSensor.SensorReached(236);
      arr[i]();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(239);
    {
      IACSharpSensor.IACSharpSensor.SensorReached(237);
      for (int j = 0; j < 5; ++j) {
        IACSharpSensor.IACSharpSensor.SensorReached(238);
        arr[j] = delegate { doit(j); };
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(240);
  }
}
