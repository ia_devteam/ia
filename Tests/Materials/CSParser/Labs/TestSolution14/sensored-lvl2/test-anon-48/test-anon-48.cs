using System;
public delegate void Foo();
public class World
{
  public void Hello(long a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(338);
  }
  public void Test(int t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(339);
    for (long l = 0; l < t; l++) {
      IACSharpSensor.IACSharpSensor.SensorReached(340);
      for (long m = 0; m < l; m++) {
        IACSharpSensor.IACSharpSensor.SensorReached(341);
        for (int u = 0; u < t; u++) {
          IACSharpSensor.IACSharpSensor.SensorReached(342);
          Foo foo = delegate {
            IACSharpSensor.IACSharpSensor.SensorReached(343);
            Hello(u);
            Hello(l);
            Hello(m);
          };
          foo();
        }
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(344);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(345);
    World world = new World();
    world.Test(5);
    IACSharpSensor.IACSharpSensor.SensorReached(346);
  }
}
