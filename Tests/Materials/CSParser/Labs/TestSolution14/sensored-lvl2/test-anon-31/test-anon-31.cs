using System;
using System.Collections;
using System.Reflection;
class test
{
  public IEnumerable testen(int x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(211);
    for (int i = 0; i < x; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(212);
      if (i % 2 == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(213);
        int o = i;
        IEnumerable RNTRNTRNT_56 = o;
        IACSharpSensor.IACSharpSensor.SensorReached(214);
        yield return RNTRNTRNT_56;
        IACSharpSensor.IACSharpSensor.SensorReached(215);
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(216);
        int o = i * 2;
        IEnumerable RNTRNTRNT_57 = o;
        IACSharpSensor.IACSharpSensor.SensorReached(217);
        yield return RNTRNTRNT_57;
        IACSharpSensor.IACSharpSensor.SensorReached(218);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(219);
  }
}
class reflect
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    Hashtable ht = new Hashtable();
    Assembly asm = Assembly.GetAssembly(typeof(test));
    IACSharpSensor.IACSharpSensor.SensorReached(221);
    foreach (Type t in asm.GetTypes()) {
      IACSharpSensor.IACSharpSensor.SensorReached(222);
      ht.Clear();
      IACSharpSensor.IACSharpSensor.SensorReached(223);
      foreach (FieldInfo fi in t.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)) {
        IACSharpSensor.IACSharpSensor.SensorReached(224);
        ht.Add(fi.Name, fi);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(225);
  }
}
