using System;
class Source
{
  public event EventHandler ChildSourceAdded;
  public event EventHandler ChildSourceRemoved;
  Source FindSource(Source x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(565);
    return null;
  }
  private void AddSource(Source source, int position, object parent)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(566);
    if (!FindSource(source).Equals(source)) {
      IACSharpSensor.IACSharpSensor.SensorReached(567);
      return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(568);
    object iter = null;
    source.ChildSourceAdded += delegate(object t, EventArgs e) { AddSource((Source)(object)e, position, iter); };
    source.ChildSourceRemoved += delegate(object t, EventArgs e) { };
    IACSharpSensor.IACSharpSensor.SensorReached(569);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(570);
  }
}
