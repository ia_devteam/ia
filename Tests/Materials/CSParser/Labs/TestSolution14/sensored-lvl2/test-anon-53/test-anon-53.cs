using System;
public delegate void Foo();
class Test
{
  public Test(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(392);
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(393);
      Console.WriteLine(a);
    };
    foo();
    IACSharpSensor.IACSharpSensor.SensorReached(394);
  }
  static Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(395);
    int a = 5;
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(396);
      Console.WriteLine(a);
    };
    foo();
    IACSharpSensor.IACSharpSensor.SensorReached(397);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(398);
    Test test = new Test(9);
    Console.WriteLine(test);
    IACSharpSensor.IACSharpSensor.SensorReached(399);
  }
}
