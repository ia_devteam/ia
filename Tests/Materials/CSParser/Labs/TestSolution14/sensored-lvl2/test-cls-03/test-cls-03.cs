using System;
[assembly: CLSCompliant(true)]
public interface I1
{
}
public class CLSClass
{
  protected internal I1 Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(690);
    return null;
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(691);
  }
}
