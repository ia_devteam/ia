using System;
public delegate void Hello();
struct Foo
{
  public int ID;
  public Foo(int id)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(400);
    this.ID = id;
    IACSharpSensor.IACSharpSensor.SensorReached(401);
  }
  public void Test(Foo foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(402);
    Foo bar = this;
    Hello hello = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(403);
      foo.Hello(8);
      bar.Hello(3);
    };
    hello();
    IACSharpSensor.IACSharpSensor.SensorReached(404);
  }
  public void Hello(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(405);
    if (ID != value) {
      IACSharpSensor.IACSharpSensor.SensorReached(406);
      throw new InvalidOperationException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(407);
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_83 = String.Format("Foo ({0})", ID);
    IACSharpSensor.IACSharpSensor.SensorReached(408);
    return RNTRNTRNT_83;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(409);
    Foo foo = new Foo(3);
    foo.Test(new Foo(8));
    IACSharpSensor.IACSharpSensor.SensorReached(410);
  }
}
