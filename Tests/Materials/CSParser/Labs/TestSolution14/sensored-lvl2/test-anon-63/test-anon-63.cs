using System;
using System.Collections;
class X
{
  delegate int A();
  static IEnumerator GetIt(int[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(556);
    foreach (int arg in args) {
      IACSharpSensor.IACSharpSensor.SensorReached(557);
      Console.WriteLine("OUT: {0}", arg);
      A a = delegate {
        IACSharpSensor.IACSharpSensor.SensorReached(558);
        Console.WriteLine("arg: {0}", arg);
        Console.WriteLine("args: {0}", args);
        IEnumerator RNTRNTRNT_127 = arg;
        IACSharpSensor.IACSharpSensor.SensorReached(559);
        return RNTRNTRNT_127;
      };
      Object RNTRNTRNT_128 = a();
      IACSharpSensor.IACSharpSensor.SensorReached(560);
      yield return RNTRNTRNT_128;
      IACSharpSensor.IACSharpSensor.SensorReached(561);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(562);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(563);
    IEnumerator enumerator = GetIt(new int[] {
      4,
      8,
      9
    });
    enumerator.MoveNext();
    System.Int32 RNTRNTRNT_129 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(564);
    return RNTRNTRNT_129;
  }
}
