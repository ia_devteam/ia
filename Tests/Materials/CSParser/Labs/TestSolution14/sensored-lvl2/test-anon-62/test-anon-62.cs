public delegate void FooHandler();
public class X
{
  private string a;
  public X(string a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(549);
    this.a = a;
    IACSharpSensor.IACSharpSensor.SensorReached(550);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(551);
  }
}
public class Y : X
{
  private Z a;
  public Y(Z a) : base(a.A)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(552);
    this.a = a;
    FooHandler handler = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(553);
      a.Hello();
    };
    IACSharpSensor.IACSharpSensor.SensorReached(554);
  }
}
public class Z
{
  public string A;
  public void Hello()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(555);
  }
}
