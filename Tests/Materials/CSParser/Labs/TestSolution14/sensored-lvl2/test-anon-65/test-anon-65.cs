using System;
public class BaseClass
{
  public delegate void SomeDelegate();
  public BaseClass(SomeDelegate d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(571);
    d();
    IACSharpSensor.IACSharpSensor.SensorReached(572);
  }
}
public class TestClass : BaseClass
{
  public readonly int Result;
  public TestClass(int result) : base(delegate() { Console.WriteLine(result); })
  {
    IACSharpSensor.IACSharpSensor.SensorReached(573);
  }
  static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(574);
    TestClass c = new TestClass(1);
    System.Int32 RNTRNTRNT_130 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(575);
    return RNTRNTRNT_130;
  }
}
