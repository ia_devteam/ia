using System;
public class DelegateInit
{
  public delegate void FooDelegate();
  public static readonly FooDelegate _print = delegate() { Console.WriteLine("delegate!"); };
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(264);
    _print();
    IACSharpSensor.IACSharpSensor.SensorReached(265);
  }
}
