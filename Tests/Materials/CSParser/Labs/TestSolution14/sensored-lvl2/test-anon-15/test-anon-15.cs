public class Foo
{
  delegate Inner foo_fn(string s);
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(115);
    foo_fn f = delegate(string s) {
      void  RNTRNTRNT_32 = new Inner(s + s);
      IACSharpSensor.IACSharpSensor.SensorReached(116);
      return RNTRNTRNT_32;
    };
    f("Test");
    IACSharpSensor.IACSharpSensor.SensorReached(117);
  }
  class Inner
  {
    public Inner(string s)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(118);
    }
  }
}
