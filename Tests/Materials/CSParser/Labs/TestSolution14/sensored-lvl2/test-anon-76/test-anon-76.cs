using System;
delegate object FactoryDelegate();
public class C
{
  FactoryDelegate var;
  int counter;
  FactoryDelegate this[string s] {
    get {
      FactoryDelegate RNTRNTRNT_136 = var;
      IACSharpSensor.IACSharpSensor.SensorReached(623);
      return RNTRNTRNT_136;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(624);
      var = value;
      IACSharpSensor.IACSharpSensor.SensorReached(625);
    }
  }
  public void X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(626);
    this["ABC"] = delegate() {
      ++counter;
      Console.WriteLine("A");
      return "Return";
    };
    IACSharpSensor.IACSharpSensor.SensorReached(627);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(628);
    C o = new C();
    o.X();
    Console.WriteLine("B");
    Console.WriteLine(o["ABC"]());
    Console.WriteLine(o.counter);
    IACSharpSensor.IACSharpSensor.SensorReached(629);
    if (o.counter != 1) {
      System.Int32 RNTRNTRNT_137 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(630);
      return RNTRNTRNT_137;
    }
    System.Int32 RNTRNTRNT_138 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(631);
    return RNTRNTRNT_138;
  }
}
