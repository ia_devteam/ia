using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public void Hello(long k)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(273);
  }
  public void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(274);
    long j = 1 << i;
    Hello(j);
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(275);
      Hello(j);
      void  RNTRNTRNT_60 = delegate { Hello(j); };
      IACSharpSensor.IACSharpSensor.SensorReached(276);
      return RNTRNTRNT_60;
    };
    Simple simple = foo();
    simple();
    IACSharpSensor.IACSharpSensor.SensorReached(277);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(278);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(279);
  }
}
