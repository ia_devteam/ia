using System;
public class X
{
  public delegate void TestDelegate();
  static long sum_i, sum_k;
  public static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(421);
    TestDelegate d = null;
    IACSharpSensor.IACSharpSensor.SensorReached(422);
    for (int i = 1; i <= 5; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(423);
      int k = i;
      TestDelegate temp = delegate {
        IACSharpSensor.IACSharpSensor.SensorReached(424);
        Console.WriteLine("i = {0}, k = {1}", i, k);
        sum_i += 1 << i;
        sum_k += 1 << k;
      };
      temp();
      d += temp;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(425);
    Console.WriteLine("SUM i = {0}, k = {1}", sum_i, sum_k);
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(426);
    if (sum_i != 62) {
      System.Int32 RNTRNTRNT_84 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(427);
      return RNTRNTRNT_84;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(428);
    if (sum_k != 62) {
      System.Int32 RNTRNTRNT_85 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(429);
      return RNTRNTRNT_85;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(430);
    sum_i = sum_k = 0;
    d();
    Console.WriteLine("SUM i = {0}, k = {1}", sum_i, sum_k);
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(431);
    if (sum_i != 320) {
      System.Int32 RNTRNTRNT_86 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(432);
      return RNTRNTRNT_86;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(433);
    if (sum_k != 62) {
      System.Int32 RNTRNTRNT_87 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(434);
      return RNTRNTRNT_87;
    }
    System.Int32 RNTRNTRNT_88 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(435);
    return RNTRNTRNT_88;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(436);
    int result = Test();
    IACSharpSensor.IACSharpSensor.SensorReached(437);
    if (result != 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(438);
      Console.WriteLine("ERROR: {0}", result);
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(439);
      Console.WriteLine("OK");
    }
    System.Int32 RNTRNTRNT_89 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(440);
    return RNTRNTRNT_89;
  }
}
