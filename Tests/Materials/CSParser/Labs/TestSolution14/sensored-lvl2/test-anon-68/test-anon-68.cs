delegate void D1(int i);
delegate void D2(out string s);
public class C
{
  static void T(D1 d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(580);
  }
  static void T(D2 d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(581);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(582);
    T(delegate { });
    T(delegate(out string o) { o = "ab"; });
    IACSharpSensor.IACSharpSensor.SensorReached(583);
  }
}
