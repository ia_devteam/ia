public delegate void StringSender(string str);
public delegate void VoidDelegate();
public class MainClass
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(226);
    MainClass mc = new MainClass();
    VoidDelegate del = new VoidDelegate(delegate {
      StringSender ss = delegate(string s) { SimpleCallback(mc, s); };
      ss("Yo!");
    });
    del();
    IACSharpSensor.IACSharpSensor.SensorReached(227);
  }
  static void SimpleCallback(MainClass mc, string str)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(228);
    System.Console.WriteLine(str);
    IACSharpSensor.IACSharpSensor.SensorReached(229);
  }
}
