using System;
delegate void S();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(41);
    int a = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    if (a != 1) {
      System.Int32 RNTRNTRNT_10 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(43);
      return RNTRNTRNT_10;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(44);
    Console.WriteLine("A is = " + a);
    S b = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(45);
      Console.WriteLine("on delegate");
      a = 2;
    };
    IACSharpSensor.IACSharpSensor.SensorReached(46);
    if (a != 1) {
      System.Int32 RNTRNTRNT_11 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(47);
      return RNTRNTRNT_11;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(48);
    b();
    IACSharpSensor.IACSharpSensor.SensorReached(49);
    if (a != 2) {
      System.Int32 RNTRNTRNT_12 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(50);
      return RNTRNTRNT_12;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(51);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_13 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(52);
    return RNTRNTRNT_13;
  }
}
