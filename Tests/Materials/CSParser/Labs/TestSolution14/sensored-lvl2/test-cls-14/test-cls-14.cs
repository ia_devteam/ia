using System;
public class CLSClass
{
  [CLSCompliant(false)]
  public static implicit operator CLSClass(byte value)
  {
    CLSClass RNTRNTRNT_171 = new CLSClass();
    IACSharpSensor.IACSharpSensor.SensorReached(718);
    return RNTRNTRNT_171;
  }
  [CLSCompliant(true)]
  private void Error(bool arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(719);
  }
}
public class MainClass
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(720);
  }
}
