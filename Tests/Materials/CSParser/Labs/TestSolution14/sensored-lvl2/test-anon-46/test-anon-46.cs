using System;
public delegate void Foo();
public class World
{
  public void Hello(long a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(324);
  }
  public void Test(int t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(325);
    Hello(t);
    int u = 2 * t;
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(326);
      Hello(u);
    };
    foo();
    IACSharpSensor.IACSharpSensor.SensorReached(327);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(328);
    World world = new World();
    world.Test(5);
    IACSharpSensor.IACSharpSensor.SensorReached(329);
  }
}
