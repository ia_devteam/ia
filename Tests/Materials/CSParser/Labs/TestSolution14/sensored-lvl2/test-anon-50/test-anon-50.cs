using System;
using System.Collections;
public class Test
{
  public IEnumerable Foo(int a)
  {
    IEnumerable RNTRNTRNT_69 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(354);
    yield return RNTRNTRNT_69;
    IACSharpSensor.IACSharpSensor.SensorReached(355);
    IEnumerable RNTRNTRNT_70 = a * a;
    IACSharpSensor.IACSharpSensor.SensorReached(356);
    yield return RNTRNTRNT_70;
    IACSharpSensor.IACSharpSensor.SensorReached(357);
    IACSharpSensor.IACSharpSensor.SensorReached(358);
    yield break;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(359);
    Test test = new Test();
    IEnumerable a = test.Foo(5);
    IEnumerator c = a.GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(360);
    if (!c.MoveNext()) {
      System.Int32 RNTRNTRNT_71 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(361);
      return RNTRNTRNT_71;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(362);
    if ((int)c.Current != 5) {
      System.Int32 RNTRNTRNT_72 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(363);
      return RNTRNTRNT_72;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(364);
    if (!c.MoveNext()) {
      System.Int32 RNTRNTRNT_73 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(365);
      return RNTRNTRNT_73;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(366);
    if ((int)c.Current != 25) {
      System.Int32 RNTRNTRNT_74 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(367);
      return RNTRNTRNT_74;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    IEnumerator d = a.GetEnumerator();
    IACSharpSensor.IACSharpSensor.SensorReached(369);
    if ((int)c.Current != 25) {
      System.Int32 RNTRNTRNT_75 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(370);
      return RNTRNTRNT_75;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(371);
    if (!d.MoveNext()) {
      System.Int32 RNTRNTRNT_76 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(372);
      return RNTRNTRNT_76;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(373);
    if ((int)c.Current != 25) {
      System.Int32 RNTRNTRNT_77 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(374);
      return RNTRNTRNT_77;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(375);
    if ((int)d.Current != 5) {
      System.Int32 RNTRNTRNT_78 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(376);
      return RNTRNTRNT_78;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(377);
    if (c.MoveNext()) {
      System.Int32 RNTRNTRNT_79 = 13;
      IACSharpSensor.IACSharpSensor.SensorReached(378);
      return RNTRNTRNT_79;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(379);
    ((IDisposable)a).Dispose();
    System.Int32 RNTRNTRNT_80 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(380);
    return RNTRNTRNT_80;
  }
}
