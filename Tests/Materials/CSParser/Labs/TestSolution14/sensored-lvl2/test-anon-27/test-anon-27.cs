using System;
delegate void D();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(188);
    X x = new X();
    x.M(10);
    e();
    Console.WriteLine("J should be 11= {0}", j);
    e();
    Console.WriteLine("J should be 11= {0}", j);
    x.M(100);
    e();
    Console.WriteLine("J should be 101= {0}", j);
    IACSharpSensor.IACSharpSensor.SensorReached(189);
    if (j != 101) {
      System.Int32 RNTRNTRNT_52 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(190);
      return RNTRNTRNT_52;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(191);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_53 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(192);
    return RNTRNTRNT_53;
  }
  static int j;
  static D e;
  void M(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(193);
    Console.WriteLine("A is=" + a);
    D d = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(194);
      int b;
      b = 1;
      e = delegate {
        Console.WriteLine("IN NESTED DELEGATE: {0}", a);
        j = a + b;
      };
    };
    d();
    IACSharpSensor.IACSharpSensor.SensorReached(195);
  }
}
