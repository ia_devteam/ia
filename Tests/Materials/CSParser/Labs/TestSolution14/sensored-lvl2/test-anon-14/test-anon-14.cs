class X
{
  delegate void T();
  static event T Click;
  static void Method()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(112);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(113);
    T t;
    t = Method;
    Click += Method;
    IACSharpSensor.IACSharpSensor.SensorReached(114);
  }
}
