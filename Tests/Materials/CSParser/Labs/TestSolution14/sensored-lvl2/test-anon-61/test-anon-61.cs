using System;
public class X
{
  public int p;
  public delegate void TestDelegate();
  static long sum_i, sum_k, sum_p;
  public int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(525);
    TestDelegate d = null;
    IACSharpSensor.IACSharpSensor.SensorReached(526);
    for (int i = 1; i <= 5; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(527);
      int k = i;
      TestDelegate temp = delegate {
        IACSharpSensor.IACSharpSensor.SensorReached(528);
        Console.WriteLine("i = {0}, k = {1}, p = {2}", i, k, p);
        sum_i += 1 << i;
        sum_k += 1 << k;
        sum_p += 1 << p;
        p += k;
      };
      temp();
      d += temp;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(529);
    Console.WriteLine("SUM i = {0}, k = {1}, p = {2}", sum_i, sum_k, sum_p);
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(530);
    if (sum_i != 62) {
      System.Int32 RNTRNTRNT_119 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(531);
      return RNTRNTRNT_119;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(532);
    if (sum_k != 62) {
      System.Int32 RNTRNTRNT_120 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(533);
      return RNTRNTRNT_120;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(534);
    if (sum_p != 35168) {
      System.Int32 RNTRNTRNT_121 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(535);
      return RNTRNTRNT_121;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(536);
    sum_i = sum_k = sum_p = 0;
    d();
    Console.WriteLine("SUM i = {0}, k = {1}, p = {2}", sum_i, sum_k, sum_p);
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(537);
    if (sum_i != 320) {
      System.Int32 RNTRNTRNT_122 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(538);
      return RNTRNTRNT_122;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(539);
    if (sum_k != 62) {
      System.Int32 RNTRNTRNT_123 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(540);
      return RNTRNTRNT_123;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(541);
    if (sum_p != 1152385024) {
      System.Int32 RNTRNTRNT_124 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(542);
      return RNTRNTRNT_124;
    }
    System.Int32 RNTRNTRNT_125 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(543);
    return RNTRNTRNT_125;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(544);
    X x = new X();
    x.p = 5;
    int result = x.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(545);
    if (result != 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(546);
      Console.WriteLine("ERROR: {0}", result);
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(547);
      Console.WriteLine("OK");
    }
    System.Int32 RNTRNTRNT_126 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(548);
    return RNTRNTRNT_126;
  }
}
