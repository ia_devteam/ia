using System;
public class X
{
  public delegate void TestDelegate();
  static long sum_i, sum_k, sum_p;
  public static int Test(int p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(441);
    TestDelegate d = null;
    IACSharpSensor.IACSharpSensor.SensorReached(442);
    for (int i = 1; i <= 5; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(443);
      int k = i;
      TestDelegate temp = delegate {
        IACSharpSensor.IACSharpSensor.SensorReached(444);
        Console.WriteLine("i = {0}, k = {1}, p = {2}", i, k, p);
        sum_i += 1 << i;
        sum_k += 1 << k;
        sum_p += 1 << p;
        p += k;
      };
      temp();
      d += temp;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(445);
    Console.WriteLine("SUM i = {0}, k = {1}, p = {2}", sum_i, sum_k, sum_p);
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(446);
    if (sum_i != 62) {
      System.Int32 RNTRNTRNT_90 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(447);
      return RNTRNTRNT_90;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(448);
    if (sum_k != 62) {
      System.Int32 RNTRNTRNT_91 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(449);
      return RNTRNTRNT_91;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(450);
    if (sum_p != 35168) {
      System.Int32 RNTRNTRNT_92 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(451);
      return RNTRNTRNT_92;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(452);
    sum_i = sum_k = sum_p = 0;
    d();
    Console.WriteLine("SUM i = {0}, k = {1}, p = {2}", sum_i, sum_k, sum_p);
    Console.WriteLine();
    IACSharpSensor.IACSharpSensor.SensorReached(453);
    if (sum_i != 320) {
      System.Int32 RNTRNTRNT_93 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(454);
      return RNTRNTRNT_93;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(455);
    if (sum_k != 62) {
      System.Int32 RNTRNTRNT_94 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(456);
      return RNTRNTRNT_94;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(457);
    if (sum_p != 1152385024) {
      System.Int32 RNTRNTRNT_95 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(458);
      return RNTRNTRNT_95;
    }
    System.Int32 RNTRNTRNT_96 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(459);
    return RNTRNTRNT_96;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(460);
    int result = Test(5);
    IACSharpSensor.IACSharpSensor.SensorReached(461);
    if (result != 0) {
      IACSharpSensor.IACSharpSensor.SensorReached(462);
      Console.WriteLine("ERROR: {0}", result);
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(463);
      Console.WriteLine("OK");
    }
    System.Int32 RNTRNTRNT_97 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(464);
    return RNTRNTRNT_97;
  }
}
