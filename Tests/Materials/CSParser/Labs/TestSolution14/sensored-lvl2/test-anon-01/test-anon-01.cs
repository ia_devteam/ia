delegate void S();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
    int a;
    S b = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(2);
      a = 2;
    };
    System.Int32 RNTRNTRNT_1 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(3);
    return RNTRNTRNT_1;
  }
}
