using System;
delegate void S();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(4);
    int a = 1;
    Console.WriteLine("A is = " + a);
    int c = a;
    Console.WriteLine(c);
    IACSharpSensor.IACSharpSensor.SensorReached(5);
    if (a != 1) {
      System.Int32 RNTRNTRNT_2 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(6);
      return RNTRNTRNT_2;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(7);
    S b = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(8);
      if (a != 1) {
        IACSharpSensor.IACSharpSensor.SensorReached(9);
        Environment.Exit(1);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(10);
      Console.WriteLine("in Delegate");
      a = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(11);
      if (a != 2) {
        IACSharpSensor.IACSharpSensor.SensorReached(12);
        Environment.Exit(2);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(13);
      Console.WriteLine("Inside = " + a);
      a = 3;
      Console.WriteLine("After = " + a);
    };
    IACSharpSensor.IACSharpSensor.SensorReached(14);
    if (a != 1) {
      System.Int32 RNTRNTRNT_3 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(15);
      return RNTRNTRNT_3;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    b();
    IACSharpSensor.IACSharpSensor.SensorReached(17);
    if (a != 3) {
      System.Int32 RNTRNTRNT_4 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(18);
      return RNTRNTRNT_4;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(19);
    Console.WriteLine("Back, got " + a);
    Console.WriteLine("Test is ok");
    System.Int32 RNTRNTRNT_5 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(20);
    return RNTRNTRNT_5;
  }
}
