using System;
delegate void ClickEvent();
class Button
{
  public event ClickEvent Clicked;
  public void DoClick()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(126);
    Clicked();
    IACSharpSensor.IACSharpSensor.SensorReached(127);
  }
}
class X
{
  static bool called = false;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(128);
    Button b = new Button();
    b.Clicked += delegate {
      Console.WriteLine("This worked!");
      called = true;
    };
    b.DoClick();
    IACSharpSensor.IACSharpSensor.SensorReached(129);
    if (called) {
      System.Int32 RNTRNTRNT_34 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(130);
      return RNTRNTRNT_34;
    } else {
      System.Int32 RNTRNTRNT_35 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(131);
      return RNTRNTRNT_35;
    }
  }
}
