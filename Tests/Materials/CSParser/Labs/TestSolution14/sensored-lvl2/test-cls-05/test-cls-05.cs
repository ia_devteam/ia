using System;
[assembly: CLSCompliant(true)]
class X1
{
  public bool AA;
  internal bool aa;
}
class X2 : X1
{
  public bool aA;
}
public class X3
{
  internal void bb(bool arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(692);
  }
  internal bool bB;
  public void BB()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(693);
  }
}
class X4
{
  public void method(int arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(694);
  }
  public void method(bool arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(695);
  }
  public bool method()
  {
    System.Boolean RNTRNTRNT_167 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(696);
    return RNTRNTRNT_167;
  }
}
public class BaseClass
{
}
public class CLSClass : BaseClass
{
  public CLSClass()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(697);
  }
  public CLSClass(int arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(698);
  }
  public bool setItem;
  public static implicit operator CLSClass(bool value)
  {
    CLSClass RNTRNTRNT_168 = new CLSClass(2);
    IACSharpSensor.IACSharpSensor.SensorReached(699);
    return RNTRNTRNT_168;
  }
  public static implicit operator CLSClass(int value)
  {
    CLSClass RNTRNTRNT_169 = new CLSClass(2);
    IACSharpSensor.IACSharpSensor.SensorReached(700);
    return RNTRNTRNT_169;
  }
  [CLSCompliant(false)]
  public void Method()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(701);
  }
  internal int Method(bool arg)
  {
    System.Int32 RNTRNTRNT_170 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(702);
    return RNTRNTRNT_170;
  }
  internal void methoD()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(703);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(704);
  }
}
public class oBject : Object
{
}
namespace A
{
  public class C1
  {
  }
}
namespace B
{
  public class c1
  {
  }
}
public class c1
{
}
[System.CLSCompliant(false)]
public interface I1
{
}
public interface i1
{
}
enum AB
{
}
[CLSCompliant(false)]
public enum aB
{
}
public interface ab
{
}
public class CLSClass_2
{
  [CLSCompliant(false)]
  public void Method()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(705);
  }
  public void method()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(706);
  }
}
namespace System
{
  public class sByte
  {
  }
}
public enum CLSEnum
{
  label,
  [CLSCompliant(false)]
  Label
}
namespace System.Web
{
  public partial class HttpBrowserCapabilities
  {
  }
  public partial class HttpBrowserCapabilities
  {
  }
}
