using System;
[assembly: CLSCompliant(true)]
[CLSCompliant(false)]
[CLSAttribute(new bool[] {
  true,
  false
})]
public class CLSAttribute : Attribute
{
  public CLSAttribute(bool[] array)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(727);
  }
}
public class ClassMain
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(728);
  }
}
