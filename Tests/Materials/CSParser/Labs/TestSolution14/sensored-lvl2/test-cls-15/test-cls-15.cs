using System;
[assembly: CLSCompliant(true)]
public class CLSAttribute_1 : Attribute
{
  public CLSAttribute_1(int[] array)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(721);
  }
  public CLSAttribute_1(int array)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(722);
  }
}
[CLSCompliant(false)]
public class CLSAttribute_2 : Attribute
{
  private CLSAttribute_2(int arg)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(723);
  }
}
internal class CLSAttribute_3 : Attribute
{
  public CLSAttribute_3(int[] array)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(724);
  }
}
[CLSCompliant(false)]
public class CLSAttribute_4 : Attribute
{
  private CLSAttribute_4(int[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(725);
  }
}
public class ClassMain
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(726);
  }
}
