using System;
delegate bool D();
class Data
{
  public D d;
}
public class Test
{
  int value;
  D change;
  static void Foo(int i, D d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(615);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(616);
  }
  void TestMe()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(617);
    if (true) {
      IACSharpSensor.IACSharpSensor.SensorReached(618);
      Data data = null;
      IACSharpSensor.IACSharpSensor.SensorReached(619);
      if (data != null) {
        IACSharpSensor.IACSharpSensor.SensorReached(620);
        D d2 = delegate {
          void  RNTRNTRNT_135 = true;
          IACSharpSensor.IACSharpSensor.SensorReached(621);
          return RNTRNTRNT_135;
        };
        change += d2;
        data.d += delegate {
          change -= d2;
          Foo(10, delegate {
            data = null;
            return false;
          });
          return true;
        };
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(622);
  }
}
