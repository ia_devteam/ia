using System;
namespace MonoBug
{
  public sealed class MyTest
  {
    private sealed class EventHandlers
    {
      private EventHandler _handler = DoNothingEventHandler;
      public static EventHandler DoNothingEventHandler {
        get {
          EventHandler RNTRNTRNT_142 = delegate { };
          IACSharpSensor.IACSharpSensor.SensorReached(647);
          return RNTRNTRNT_142;
        }
      }
      private int i;
      public EventHandler DoSomethingEventHandler {
        get {
          EventHandler RNTRNTRNT_143 = delegate { ++i; };
          IACSharpSensor.IACSharpSensor.SensorReached(648);
          return RNTRNTRNT_143;
        }
      }
      public EventHandler Handler {
        get {
          EventHandler RNTRNTRNT_144 = _handler;
          IACSharpSensor.IACSharpSensor.SensorReached(649);
          return RNTRNTRNT_144;
        }
        set {
          IACSharpSensor.IACSharpSensor.SensorReached(650);
          _handler = value;
          IACSharpSensor.IACSharpSensor.SensorReached(651);
        }
      }
    }
    static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(652);
      EventHandlers handlers = new EventHandlers();
      handlers.Handler = handlers.DoSomethingEventHandler;
      Console.WriteLine("Is handlers.Handler == handlers.DoSomethingEventHandler (instance)?");
      Console.WriteLine("Expected: True");
      Console.Write("Actual:   ");
      bool instanceEqual = handlers.Handler == handlers.DoSomethingEventHandler;
      Console.WriteLine(instanceEqual);
      Console.WriteLine();
      handlers.Handler = EventHandlers.DoNothingEventHandler;
      Console.WriteLine("Is handlers.Handler == EventHandlers.DoNothingEventHandler (static)?");
      Console.WriteLine("Expected: True");
      Console.Write("Actual:   ");
      bool staticEqual = handlers.Handler == EventHandlers.DoNothingEventHandler;
      Console.WriteLine(staticEqual);
      IACSharpSensor.IACSharpSensor.SensorReached(653);
      if (instanceEqual) {
        IACSharpSensor.IACSharpSensor.SensorReached(654);
        if (staticEqual) {
          System.Int32 RNTRNTRNT_145 = 0;
          IACSharpSensor.IACSharpSensor.SensorReached(655);
          return RNTRNTRNT_145;
        } else {
          System.Int32 RNTRNTRNT_146 = 1;
          IACSharpSensor.IACSharpSensor.SensorReached(656);
          return RNTRNTRNT_146;
        }
      } else if (staticEqual) {
        System.Int32 RNTRNTRNT_148 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(658);
        return RNTRNTRNT_148;
      } else {
        System.Int32 RNTRNTRNT_147 = 3;
        IACSharpSensor.IACSharpSensor.SensorReached(657);
        return RNTRNTRNT_147;
      }
    }
  }
}
