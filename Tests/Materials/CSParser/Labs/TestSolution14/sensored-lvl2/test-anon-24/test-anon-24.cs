using System;
delegate int D();
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(177);
    D x = T(1);
    Console.WriteLine("Should be 2={0}", x());
    IACSharpSensor.IACSharpSensor.SensorReached(178);
  }
  static D T(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(179);
    D d = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(180);
      a = a + 1;
      D RNTRNTRNT_47 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(181);
      return RNTRNTRNT_47;
    };
    D RNTRNTRNT_48 = d;
    IACSharpSensor.IACSharpSensor.SensorReached(182);
    return RNTRNTRNT_48;
  }
}
