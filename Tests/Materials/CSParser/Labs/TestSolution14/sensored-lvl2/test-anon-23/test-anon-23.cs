using System;
delegate void D();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(169);
    X x = new X();
    x.M();
    e();
    Console.WriteLine("J should be 101= {0}", j);
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    if (j != 101) {
      System.Int32 RNTRNTRNT_45 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(171);
      return RNTRNTRNT_45;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(172);
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_46 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(173);
    return RNTRNTRNT_46;
  }
  static int j;
  static D e;
  void M()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(174);
    int l = 100;
    D d = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(175);
      int b;
      b = 1;
      e = delegate { j = l + b; };
    };
    d();
    IACSharpSensor.IACSharpSensor.SensorReached(176);
  }
}
