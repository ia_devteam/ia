using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public void Hello(long i, long j)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(301);
  }
  public void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(302);
    long j = 1 << i;
    Hello(i, j);
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(303);
      long k = 5 * j;
      Hello(j, k);
      void  RNTRNTRNT_64 = delegate { Hello(j, k); };
      IACSharpSensor.IACSharpSensor.SensorReached(304);
      return RNTRNTRNT_64;
    };
    IACSharpSensor.IACSharpSensor.SensorReached(305);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(306);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(307);
  }
}
