using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public static void Hello(long k)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(294);
  }
  public static void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(295);
    Hello(3);
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(296);
      Hello(4);
      void  RNTRNTRNT_63 = delegate { Hello(5); };
      IACSharpSensor.IACSharpSensor.SensorReached(297);
      return RNTRNTRNT_63;
    };
    Simple simple = foo();
    simple();
    IACSharpSensor.IACSharpSensor.SensorReached(298);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(299);
    X x = new X();
    X.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(300);
  }
}
