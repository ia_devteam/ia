using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public void Hello(long k)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(308);
  }
  public void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(309);
    Hello(3);
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(310);
      int a = i;
      Hello(4);
      void  RNTRNTRNT_65 = delegate {
        int b = a;
        Hello(5);
      };
      IACSharpSensor.IACSharpSensor.SensorReached(311);
      return RNTRNTRNT_65;
    };
    Foo bar = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(312);
      int c = i;
      Hello(6);
      void  RNTRNTRNT_66 = delegate {
        int d = i;
        Hello(7);
      };
      IACSharpSensor.IACSharpSensor.SensorReached(313);
      return RNTRNTRNT_66;
    };
    Simple simple = foo();
    simple();
    IACSharpSensor.IACSharpSensor.SensorReached(314);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(315);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(316);
  }
}
