using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public void Hello(long k)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(280);
  }
  public void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(281);
    long j = 1 << i;
    Hello(j);
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(282);
      Hello(j);
      void  RNTRNTRNT_61 = delegate { Hello(j); };
      IACSharpSensor.IACSharpSensor.SensorReached(283);
      return RNTRNTRNT_61;
    };
    Simple simple = foo();
    simple();
    IACSharpSensor.IACSharpSensor.SensorReached(284);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(285);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(286);
  }
}
