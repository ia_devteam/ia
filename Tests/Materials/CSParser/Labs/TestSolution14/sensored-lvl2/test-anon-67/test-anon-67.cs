public class ClassOne
{
  public delegate string ReturnStringDelegate();
  public ClassOne(ReturnStringDelegate d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(577);
  }
  public ClassOne(string s) : this(new ReturnStringDelegate(delegate() { return s; }))
  {
    IACSharpSensor.IACSharpSensor.SensorReached(578);
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(579);
  }
}
