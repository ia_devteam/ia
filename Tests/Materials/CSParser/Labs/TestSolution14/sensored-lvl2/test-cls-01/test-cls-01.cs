using System;
[assembly: CLSCompliant(true)]
public class CLSClass
{
  public byte XX {
    get {
      System.Byte RNTRNTRNT_165 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(680);
      return RNTRNTRNT_165;
    }
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(681);
  }
}
public class Big
{
  [CLSCompliant(false)]
  public static implicit operator Big(uint value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(682);
    return null;
  }
}
[CLSCompliant(false)]
public partial class C1
{
}
public partial class C1
{
  public void method(uint u)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(683);
  }
}
