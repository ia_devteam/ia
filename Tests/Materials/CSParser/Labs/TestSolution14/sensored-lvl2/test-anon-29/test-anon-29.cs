using System;
using System.Collections;
public class X
{
  string[] ABC = {
    "A",
    "B",
    "C"
  };
  string[,] EFGH = {
    {
      "E",
      "F"
    },
    {
      "G",
      "H"
    }
  };
  delegate string Foo();
  delegate void Bar(string s);
  public string Hello()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(198);
    Foo foo = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(199);
      foreach (string s in ABC) {
        IACSharpSensor.IACSharpSensor.SensorReached(200);
        Bar bar = delegate(string t) {
          IACSharpSensor.IACSharpSensor.SensorReached(201);
          Console.WriteLine(t);
        };
        bar(s);
      }
      IACSharpSensor.IACSharpSensor.SensorReached(202);
      foreach (string s in EFGH) {
        IACSharpSensor.IACSharpSensor.SensorReached(203);
        Bar bar = delegate(string t) {
          IACSharpSensor.IACSharpSensor.SensorReached(204);
          Console.WriteLine(t);
        };
        bar(s);
      }
      System.String RNTRNTRNT_54 = "Hello";
      IACSharpSensor.IACSharpSensor.SensorReached(205);
      return RNTRNTRNT_54;
    };
    System.String RNTRNTRNT_55 = foo();
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    return RNTRNTRNT_55;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(207);
    X x = new X();
    Console.WriteLine(x.Hello());
    IACSharpSensor.IACSharpSensor.SensorReached(208);
  }
}
