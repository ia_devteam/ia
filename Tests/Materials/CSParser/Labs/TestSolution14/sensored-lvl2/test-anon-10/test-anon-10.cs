using System;
class S
{
  delegate void T();
  T t;
  int f;
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(77);
    int a = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(78);
    for (int i = a; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(79);
      int j = i;
      t = delegate {
        Console.WriteLine("Before: {0} {1} {2}", f, i, j);
        f = i;
      };
    }
    IACSharpSensor.IACSharpSensor.SensorReached(80);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(81);
    S s = new S();
    s.Test();
    s.t();
    IACSharpSensor.IACSharpSensor.SensorReached(82);
    if (s.f == 10) {
      System.Int32 RNTRNTRNT_23 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(83);
      return RNTRNTRNT_23;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(84);
    Console.WriteLine("Failed:" + s.f);
    System.Int32 RNTRNTRNT_24 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(85);
    return RNTRNTRNT_24;
  }
}
