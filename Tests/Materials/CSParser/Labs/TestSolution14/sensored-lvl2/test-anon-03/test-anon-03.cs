using System;
delegate void S();
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(21);
    int a = 1;
    S b = delegate {
      IACSharpSensor.IACSharpSensor.SensorReached(22);
      a = 2;
    };
    b();
    Console.WriteLine("Back, got " + a);
    IACSharpSensor.IACSharpSensor.SensorReached(23);
  }
}
