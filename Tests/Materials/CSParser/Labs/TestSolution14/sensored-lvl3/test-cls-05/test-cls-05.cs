using System;
[assembly: CLSCompliant(true)]
class X1
{
  public bool AA;
  internal bool aa;
}
class X2 : X1
{
  public bool aA;
}
public class X3
{
  internal void bb(bool arg)
  {
  }
  internal bool bB;
  public void BB()
  {
  }
}
class X4
{
  public void method(int arg)
  {
  }
  public void method(bool arg)
  {
  }
  public bool method()
  {
    System.Boolean RNTRNTRNT_167 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(416);
    return RNTRNTRNT_167;
  }
}
public class BaseClass
{
}
public class CLSClass : BaseClass
{
  public CLSClass()
  {
  }
  public CLSClass(int arg)
  {
  }
  public bool setItem;
  public static implicit operator CLSClass(bool value)
  {
    CLSClass RNTRNTRNT_168 = new CLSClass(2);
    IACSharpSensor.IACSharpSensor.SensorReached(417);
    return RNTRNTRNT_168;
  }
  public static implicit operator CLSClass(int value)
  {
    CLSClass RNTRNTRNT_169 = new CLSClass(2);
    IACSharpSensor.IACSharpSensor.SensorReached(418);
    return RNTRNTRNT_169;
  }
  [CLSCompliant(false)]
  public void Method()
  {
  }
  internal int Method(bool arg)
  {
    System.Int32 RNTRNTRNT_170 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(419);
    return RNTRNTRNT_170;
  }
  internal void methoD()
  {
  }
  public static void Main()
  {
  }
}
public class oBject : Object
{
}
namespace A
{
  public class C1
  {
  }
}
namespace B
{
  public class c1
  {
  }
}
public class c1
{
}
[System.CLSCompliant(false)]
public interface I1
{
}
public interface i1
{
}
enum AB
{
}
[CLSCompliant(false)]
public enum aB
{
}
public interface ab
{
}
public class CLSClass_2
{
  [CLSCompliant(false)]
  public void Method()
  {
  }
  public void method()
  {
  }
}
namespace System
{
  public class sByte
  {
  }
}
public enum CLSEnum
{
  label,
  [CLSCompliant(false)]
  Label
}
namespace System.Web
{
  public partial class HttpBrowserCapabilities
  {
  }
  public partial class HttpBrowserCapabilities
  {
  }
}
