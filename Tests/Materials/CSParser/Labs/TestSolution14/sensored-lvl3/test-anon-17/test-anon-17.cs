using System;
delegate void ClickEvent();
class Button
{
  public event ClickEvent Clicked;
  public void DoClick()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(66);
    Clicked();
    IACSharpSensor.IACSharpSensor.SensorReached(67);
  }
}
class X
{
  static bool called = false;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    Button b = new Button();
    b.Clicked += delegate {
      Console.WriteLine("This worked!");
      called = true;
    };
    b.DoClick();
    if (called) {
      System.Int32 RNTRNTRNT_34 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(69);
      return RNTRNTRNT_34;
    } else {
      System.Int32 RNTRNTRNT_35 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(70);
      return RNTRNTRNT_35;
    }
  }
}
