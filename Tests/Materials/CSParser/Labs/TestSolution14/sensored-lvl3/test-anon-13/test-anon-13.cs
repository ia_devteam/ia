using System;
delegate void D();
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(48);
    X x = new X(1);
    X y = new X(100);
    D a = x.T();
    D b = y.T();
    a();
    b();
    IACSharpSensor.IACSharpSensor.SensorReached(49);
  }
  X(int start)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(50);
    ins = start;
    IACSharpSensor.IACSharpSensor.SensorReached(51);
  }
  int ins;
  D T()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(52);
    D d = delegate() { Console.WriteLine("My state is: " + CALL()); };
    D RNTRNTRNT_30 = d;
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    return RNTRNTRNT_30;
  }
  string CALL()
  {
    System.String RNTRNTRNT_31 = "GOOD";
    IACSharpSensor.IACSharpSensor.SensorReached(54);
    return RNTRNTRNT_31;
  }
}
