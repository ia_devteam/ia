delegate void S();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1);
    int a;
    S b = delegate { a = 2; };
    System.Int32 RNTRNTRNT_1 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(2);
    return RNTRNTRNT_1;
  }
}
