delegate void QueueHandler(Observable sender);
class Observable
{
  static QueueHandler Queue;
  static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(263);
    Queue += (QueueHandler)delegate { System.Console.WriteLine("OK"); };
    IACSharpSensor.IACSharpSensor.SensorReached(264);
  }
}
