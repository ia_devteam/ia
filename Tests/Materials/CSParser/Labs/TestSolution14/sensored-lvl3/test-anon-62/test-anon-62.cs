public delegate void FooHandler();
public class X
{
  private string a;
  public X(string a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(318);
    this.a = a;
    IACSharpSensor.IACSharpSensor.SensorReached(319);
  }
  static void Main()
  {
  }
}
public class Y : X
{
  private Z a;
  public Y(Z a) : base(a.A)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(320);
    this.a = a;
    FooHandler handler = delegate { a.Hello(); };
    IACSharpSensor.IACSharpSensor.SensorReached(321);
  }
}
public class Z
{
  public string A;
  public void Hello()
  {
  }
}
