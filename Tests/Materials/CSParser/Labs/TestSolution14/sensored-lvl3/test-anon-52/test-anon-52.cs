using System;
using System.Collections;
class X
{
  delegate void A();
  static IEnumerator GetIt(int[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(233);
    foreach (int arg in args) {
      Console.WriteLine("OUT: {0}", arg);
      A a = delegate {
        Console.WriteLine("arg: {0}", arg);
        IACSharpSensor.IACSharpSensor.SensorReached(234);
        return;
      };
      a();
      Object RNTRNTRNT_81 = arg;
      IACSharpSensor.IACSharpSensor.SensorReached(235);
      yield return RNTRNTRNT_81;
      IACSharpSensor.IACSharpSensor.SensorReached(236);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(237);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    IEnumerator enumerator = GetIt(new int[] {
      1,
      2,
      3
    });
    enumerator.MoveNext();
    System.Int32 RNTRNTRNT_82 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(239);
    return RNTRNTRNT_82;
  }
}
