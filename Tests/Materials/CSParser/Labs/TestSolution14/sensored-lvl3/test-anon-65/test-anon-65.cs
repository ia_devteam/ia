using System;
public class BaseClass
{
  public delegate void SomeDelegate();
  public BaseClass(SomeDelegate d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(333);
    d();
    IACSharpSensor.IACSharpSensor.SensorReached(334);
  }
}
public class TestClass : BaseClass
{
  public readonly int Result;
  public TestClass(int result) : base(delegate() { Console.WriteLine(result); })
  {
  }
  static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(335);
    TestClass c = new TestClass(1);
    System.Int32 RNTRNTRNT_130 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(336);
    return RNTRNTRNT_130;
  }
}
