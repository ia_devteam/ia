public class Test
{
  public delegate bool UnaryOperator(object self, out object res);
  public void AddOperator(UnaryOperator target)
  {
  }
  public bool TryGetValue(object self, out object value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(350);
    value = null;
    System.Boolean RNTRNTRNT_134 = false;
    IACSharpSensor.IACSharpSensor.SensorReached(351);
    return RNTRNTRNT_134;
  }
  public static void Main()
  {
  }
  void Foo()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(352);
    AddOperator(delegate(object self, out object res) {
      object value;
      if (TryGetValue(self, out value)) {
        res = value;
        if (res != null)
          return true;
      }
      res = null;
      return false;
    });
    IACSharpSensor.IACSharpSensor.SensorReached(353);
  }
}
