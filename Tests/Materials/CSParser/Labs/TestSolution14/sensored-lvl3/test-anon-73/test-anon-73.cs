using System;
using System.Threading;
delegate void D(object o);
class T
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(354);
    D d = delegate(object state) {
      try {
      } catch {
        throw;
      } finally {
      }
    };
    IACSharpSensor.IACSharpSensor.SensorReached(355);
  }
  static void Test_1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(356);
    System.Threading.ThreadPool.QueueUserWorkItem(delegate(object o) {
      using (System.Threading.Timer t = new System.Threading.Timer(null, null, 1, 1)) {
      }
    });
    IACSharpSensor.IACSharpSensor.SensorReached(357);
  }
}
