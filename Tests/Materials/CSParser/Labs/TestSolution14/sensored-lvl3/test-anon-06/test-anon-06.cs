using System;
delegate void S();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(18);
    int a = 1;
    if (a != 1) {
      System.Int32 RNTRNTRNT_10 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(19);
      return RNTRNTRNT_10;
    }
    Console.WriteLine("A is = " + a);
    S b = delegate {
      Console.WriteLine("on delegate");
      a = 2;
    };
    if (a != 1) {
      System.Int32 RNTRNTRNT_11 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(20);
      return RNTRNTRNT_11;
    }
    b();
    if (a != 2) {
      System.Int32 RNTRNTRNT_12 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(21);
      return RNTRNTRNT_12;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_13 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(22);
    return RNTRNTRNT_13;
  }
}
