using System;
public class Foo
{
  protected delegate void Hello();
  protected void Test(Hello hello)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(255);
    hello();
    IACSharpSensor.IACSharpSensor.SensorReached(256);
  }
  private void Private()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(257);
    Console.WriteLine("Private!");
    IACSharpSensor.IACSharpSensor.SensorReached(258);
  }
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(259);
    Test(delegate { Private(); });
    IACSharpSensor.IACSharpSensor.SensorReached(260);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(261);
    Foo foo = new Foo();
    foo.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(262);
  }
}
