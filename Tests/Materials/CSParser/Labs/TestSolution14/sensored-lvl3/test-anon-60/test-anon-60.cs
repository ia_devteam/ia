using System;
public class X
{
  public delegate void TestDelegate();
  static long sum_i, sum_j, sum_k;
  static ulong sum_p, sum_q;
  public static int Test(int p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(295);
    TestDelegate d = null;
    for (int i = 1; i <= 5; i++) {
      for (int j = i; j <= 8; j++) {
        int k = i;
        TestDelegate temp = delegate {
          Console.WriteLine("i = {0}, j = {1}, k = {2}, p = {3}", i, j, k, p);
          sum_i += 1 << i;
          sum_j += 1 << j;
          sum_k += 1 << k;
          sum_p += (ulong)(1 << p);
          p += k;
          ulong q = (ulong)(i * j);
          d += delegate {
            Console.WriteLine("Nested i = {0}, j = {1}, " + "k = {2}, p = {3}, q = {4}", i, j, k, p, q);
            sum_q += q;
          };
        };
        temp();
        d += temp;
      }
    }
    Console.WriteLine("SUM i = {0}, j = {1}, k = {2}, p = {3}", sum_i, sum_j, sum_k, sum_p);
    Console.WriteLine();
    if (sum_i != 300) {
      System.Int32 RNTRNTRNT_108 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(296);
      return RNTRNTRNT_108;
    }
    if (sum_j != 2498) {
      System.Int32 RNTRNTRNT_109 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(297);
      return RNTRNTRNT_109;
    }
    if (sum_k != 300) {
      System.Int32 RNTRNTRNT_110 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(298);
      return RNTRNTRNT_110;
    }
    if (sum_p != 1825434804) {
      System.Int32 RNTRNTRNT_111 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(299);
      return RNTRNTRNT_111;
    }
    sum_i = sum_j = sum_k = 0;
    sum_p = sum_q = 0;
    d();
    Console.WriteLine("SUM i = {0}, j = {1}, k = {2}, p = {3}, q = {4}", sum_i, sum_j, sum_k, sum_p, sum_q);
    Console.WriteLine();
    if (sum_i != 1920) {
      System.Int32 RNTRNTRNT_112 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(300);
      return RNTRNTRNT_112;
    }
    if (sum_j != 15360) {
      System.Int32 RNTRNTRNT_113 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(301);
      return RNTRNTRNT_113;
    }
    if (sum_k != 300) {
      System.Int32 RNTRNTRNT_114 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(302);
      return RNTRNTRNT_114;
    }
    if (sum_p != 18446744073385831629uL) {
      System.Int32 RNTRNTRNT_115 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(303);
      return RNTRNTRNT_115;
    }
    if (sum_q != 455) {
      System.Int32 RNTRNTRNT_116 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(304);
      return RNTRNTRNT_116;
    }
    System.Int32 RNTRNTRNT_117 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(305);
    return RNTRNTRNT_117;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(306);
    int result = Test(5);
    if (result != 0) {
      Console.WriteLine("ERROR: {0}", result);
    } else {
      Console.WriteLine("OK");
    }
    System.Int32 RNTRNTRNT_118 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(307);
    return RNTRNTRNT_118;
  }
}
