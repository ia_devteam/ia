using System;
using System.Collections;
public class X
{
  string[] ABC = {
    "A",
    "B",
    "C"
  };
  string[,] EFGH = {
    {
      "E",
      "F"
    },
    {
      "G",
      "H"
    }
  };
  delegate string Foo();
  delegate void Bar(string s);
  public string Hello()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(114);
    Foo foo = delegate {
      foreach (string s in ABC) {
        Bar bar = delegate(string t) { Console.WriteLine(t); };
        bar(s);
      }
      foreach (string s in EFGH) {
        Bar bar = delegate(string t) { Console.WriteLine(t); };
        bar(s);
      }
      System.String RNTRNTRNT_54 = "Hello";
      IACSharpSensor.IACSharpSensor.SensorReached(115);
      return RNTRNTRNT_54;
    };
    System.String RNTRNTRNT_55 = foo();
    IACSharpSensor.IACSharpSensor.SensorReached(116);
    return RNTRNTRNT_55;
  }
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(117);
    X x = new X();
    Console.WriteLine(x.Hello());
    IACSharpSensor.IACSharpSensor.SensorReached(118);
  }
}
