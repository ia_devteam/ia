using System;
delegate void D();
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(60);
    X x = new X(1);
    X y = new X(100);
    D a = x.T();
    D b = y.T();
    a();
    b();
    IACSharpSensor.IACSharpSensor.SensorReached(61);
  }
  X(int start)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    ins = start;
    IACSharpSensor.IACSharpSensor.SensorReached(63);
  }
  int ins;
  D T()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(64);
    D d = delegate() { Console.WriteLine("My state is: " + ins); };
    D RNTRNTRNT_33 = d;
    IACSharpSensor.IACSharpSensor.SensorReached(65);
    return RNTRNTRNT_33;
  }
}
