using System;
delegate void S();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(3);
    int a = 1;
    Console.WriteLine("A is = " + a);
    int c = a;
    Console.WriteLine(c);
    if (a != 1) {
      System.Int32 RNTRNTRNT_2 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(4);
      return RNTRNTRNT_2;
    }
    S b = delegate {
      if (a != 1) {
        Environment.Exit(1);
      }
      Console.WriteLine("in Delegate");
      a = 2;
      if (a != 2) {
        Environment.Exit(2);
      }
      Console.WriteLine("Inside = " + a);
      a = 3;
      Console.WriteLine("After = " + a);
    };
    if (a != 1) {
      System.Int32 RNTRNTRNT_3 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(5);
      return RNTRNTRNT_3;
    }
    b();
    if (a != 3) {
      System.Int32 RNTRNTRNT_4 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(6);
      return RNTRNTRNT_4;
    }
    Console.WriteLine("Back, got " + a);
    Console.WriteLine("Test is ok");
    System.Int32 RNTRNTRNT_5 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(7);
    return RNTRNTRNT_5;
  }
}
