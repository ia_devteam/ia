static class Foo
{
  delegate string[,] SimpleDelegate();
  static void Baz(SimpleDelegate sd)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(358);
    sd();
    IACSharpSensor.IACSharpSensor.SensorReached(359);
  }
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(360);
    Baz(delegate() { return new string[,] {{ "aa" },{ "bb" }}; });
    IACSharpSensor.IACSharpSensor.SensorReached(361);
  }
}
