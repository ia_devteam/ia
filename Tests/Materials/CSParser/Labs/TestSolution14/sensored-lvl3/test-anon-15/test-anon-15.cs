public class Foo
{
  delegate Inner foo_fn(string s);
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(57);
    foo_fn f = delegate(string s) {
      void  RNTRNTRNT_32 = new Inner(s + s);
      IACSharpSensor.IACSharpSensor.SensorReached(58);
      return RNTRNTRNT_32;
    };
    f("Test");
    IACSharpSensor.IACSharpSensor.SensorReached(59);
  }
  class Inner
  {
    public Inner(string s)
    {
    }
  }
}
