using System;
delegate void S();
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(10);
    int a = 1;
    S b = delegate {
      float f = 1;
      Console.WriteLine(a);
      if (f == 2) {
        IACSharpSensor.IACSharpSensor.SensorReached(11);
        return;
      }
    };
    b();
    Console.WriteLine("Back, got " + a);
    IACSharpSensor.IACSharpSensor.SensorReached(12);
  }
}
