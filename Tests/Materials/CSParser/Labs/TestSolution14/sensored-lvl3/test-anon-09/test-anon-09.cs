using System;
class X
{
  delegate void D();
  static int gt, gj;
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(31);
    int times = 0;
    D d = delegate {
      int t = times++;
      int j = ++times;
      gt = t;
      gj = j;
    };
    d();
    if (gt != 0) {
      System.Int32 RNTRNTRNT_20 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(32);
      return RNTRNTRNT_20;
    }
    if (gj != 2) {
      System.Int32 RNTRNTRNT_21 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(33);
      return RNTRNTRNT_21;
    }
    System.Int32 RNTRNTRNT_22 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(34);
    return RNTRNTRNT_22;
  }
}
