using System;
delegate void S();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(13);
    int i;
    S b = null;
    for (i = 0; i < 10; i++) {
      int j = 0;
      b = delegate {
        Console.WriteLine("i={0} j={1}", i, j);
        i = i + 1;
        j = j + 1;
      };
    }
    Console.WriteLine("i = {0}", i);
    b();
    Console.WriteLine("i = {0}", i);
    if (!t(i, 11)) {
      System.Int32 RNTRNTRNT_6 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(14);
      return RNTRNTRNT_6;
    }
    b();
    if (!t(i, 12)) {
      System.Int32 RNTRNTRNT_7 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(15);
      return RNTRNTRNT_7;
    }
    Console.WriteLine("i = {0}", i);
    Console.WriteLine("Test is OK");
    System.Int32 RNTRNTRNT_8 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(16);
    return RNTRNTRNT_8;
  }
  static bool t(int a, int b)
  {
    System.Boolean RNTRNTRNT_9 = a == b;
    IACSharpSensor.IACSharpSensor.SensorReached(17);
    return RNTRNTRNT_9;
  }
}
