using System;
delegate void Do();
class T
{
  static void doit(int v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(133);
    Console.WriteLine(v);
    IACSharpSensor.IACSharpSensor.SensorReached(134);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(135);
    Do[] arr = new Do[5];
    for (int i = 0; i < 5; ++i) {
      arr[i] = delegate { doit(i); };
    }
    for (int i = 0; i < 5; ++i) {
      arr[i]();
    }
    {
      for (int j = 0; j < 5; ++j) {
        arr[j] = delegate { doit(j); };
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(136);
  }
}
