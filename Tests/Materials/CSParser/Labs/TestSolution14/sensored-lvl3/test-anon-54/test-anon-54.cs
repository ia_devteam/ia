using System;
public delegate void Hello();
struct Foo
{
  public int ID;
  public Foo(int id)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(246);
    this.ID = id;
    IACSharpSensor.IACSharpSensor.SensorReached(247);
  }
  public void Test(Foo foo)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(248);
    Foo bar = this;
    Hello hello = delegate {
      foo.Hello(8);
      bar.Hello(3);
    };
    hello();
    IACSharpSensor.IACSharpSensor.SensorReached(249);
  }
  public void Hello(int value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(250);
    if (ID != value) {
      throw new InvalidOperationException();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(251);
  }
  public override string ToString()
  {
    System.String RNTRNTRNT_83 = String.Format("Foo ({0})", ID);
    IACSharpSensor.IACSharpSensor.SensorReached(252);
    return RNTRNTRNT_83;
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(253);
    Foo foo = new Foo(3);
    foo.Test(new Foo(8));
    IACSharpSensor.IACSharpSensor.SensorReached(254);
  }
}
