delegate void D(int x);
delegate void E(out int x);
delegate void F(params int[] x);
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(40);
    D d1 = delegate { };
    D d2 = delegate(int a) { };
    F f1 = delegate { };
    F f2 = delegate(int[] a) { };
    System.Int32 RNTRNTRNT_25 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(41);
    return RNTRNTRNT_25;
  }
}
