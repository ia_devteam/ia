using System;
delegate void D1();
delegate void D2();
public class DelegateTest
{
  static void Foo(D1 d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(377);
    d();
    IACSharpSensor.IACSharpSensor.SensorReached(378);
  }
  static void Foo(D2 d)
  {
  }
  static int counter = 99;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(379);
    Foo(new D1(delegate {
      counter = 82;
      Console.WriteLine("In");
    }));
    if (counter != 82) {
      System.Int32 RNTRNTRNT_140 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(380);
      return RNTRNTRNT_140;
    }
    System.Int32 RNTRNTRNT_141 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(381);
    return RNTRNTRNT_141;
  }
}
