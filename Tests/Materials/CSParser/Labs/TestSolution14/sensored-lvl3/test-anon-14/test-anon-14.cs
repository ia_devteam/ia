class X
{
  delegate void T();
  static event T Click;
  static void Method()
  {
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(55);
    T t;
    t = Method;
    Click += Method;
    IACSharpSensor.IACSharpSensor.SensorReached(56);
  }
}
