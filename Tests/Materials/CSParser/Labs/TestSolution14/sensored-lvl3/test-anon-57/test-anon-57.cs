using System;
public class X
{
  public delegate void TestDelegate();
  static long sum_i, sum_k;
  public static int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(265);
    TestDelegate d = null;
    for (int i = 1; i <= 5; i++) {
      int k = i;
      TestDelegate temp = delegate {
        Console.WriteLine("i = {0}, k = {1}", i, k);
        sum_i += 1 << i;
        sum_k += 1 << k;
      };
      temp();
      d += temp;
    }
    Console.WriteLine("SUM i = {0}, k = {1}", sum_i, sum_k);
    Console.WriteLine();
    if (sum_i != 62) {
      System.Int32 RNTRNTRNT_84 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(266);
      return RNTRNTRNT_84;
    }
    if (sum_k != 62) {
      System.Int32 RNTRNTRNT_85 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(267);
      return RNTRNTRNT_85;
    }
    sum_i = sum_k = 0;
    d();
    Console.WriteLine("SUM i = {0}, k = {1}", sum_i, sum_k);
    Console.WriteLine();
    if (sum_i != 320) {
      System.Int32 RNTRNTRNT_86 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(268);
      return RNTRNTRNT_86;
    }
    if (sum_k != 62) {
      System.Int32 RNTRNTRNT_87 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(269);
      return RNTRNTRNT_87;
    }
    System.Int32 RNTRNTRNT_88 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(270);
    return RNTRNTRNT_88;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(271);
    int result = Test();
    if (result != 0) {
      Console.WriteLine("ERROR: {0}", result);
    } else {
      Console.WriteLine("OK");
    }
    System.Int32 RNTRNTRNT_89 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(272);
    return RNTRNTRNT_89;
  }
}
