using System;
delegate void D();
class X
{
  static D GlobalStoreDelegate;
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(79);
    D d = MainHost();
    d();
    GlobalStoreDelegate();
    GlobalStoreDelegate();
    IACSharpSensor.IACSharpSensor.SensorReached(80);
  }
  static D MainHost()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(81);
    int toplevel_local = 0;
    D d = delegate() {
      int anonymous_local = 1;
      GlobalStoreDelegate = delegate {
        Console.WriteLine("var1: {0} var2: {1}", toplevel_local, anonymous_local);
        anonymous_local = anonymous_local + 1;
      };
      toplevel_local = toplevel_local + 1;
    };
    D RNTRNTRNT_41 = d;
    IACSharpSensor.IACSharpSensor.SensorReached(82);
    return RNTRNTRNT_41;
  }
}
