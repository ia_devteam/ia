using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public static void Hello(long k)
  {
  }
  public static void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(175);
    Hello(3);
    Foo foo = delegate {
      Hello(4);
      void  RNTRNTRNT_63 = delegate { Hello(5); };
      IACSharpSensor.IACSharpSensor.SensorReached(176);
      return RNTRNTRNT_63;
    };
    Simple simple = foo();
    simple();
    IACSharpSensor.IACSharpSensor.SensorReached(177);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(178);
    X x = new X();
    X.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(179);
  }
}
