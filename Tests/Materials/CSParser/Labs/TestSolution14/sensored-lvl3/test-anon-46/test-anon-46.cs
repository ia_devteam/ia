using System;
public delegate void Foo();
public class World
{
  public void Hello(long a)
  {
  }
  public void Test(int t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(197);
    Hello(t);
    int u = 2 * t;
    Foo foo = delegate { Hello(u); };
    foo();
    IACSharpSensor.IACSharpSensor.SensorReached(198);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(199);
    World world = new World();
    world.Test(5);
    IACSharpSensor.IACSharpSensor.SensorReached(200);
  }
}
