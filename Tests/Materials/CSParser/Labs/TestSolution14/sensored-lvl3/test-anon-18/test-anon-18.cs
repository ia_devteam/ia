using System;
delegate void A();
class DelegateTest
{
  static void Main(string[] argv)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(71);
    Console.WriteLine("Test");
    foreach (string arg in argv) {
      Console.WriteLine("OUT: {0}", arg);
      A a = delegate { Console.WriteLine("arg: {0}", arg); };
      a();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(72);
  }
}
