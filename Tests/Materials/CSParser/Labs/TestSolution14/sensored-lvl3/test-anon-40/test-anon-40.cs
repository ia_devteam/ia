using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public void Hello(long k)
  {
  }
  public void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(165);
    long j = 1 << i;
    Hello(j);
    Foo foo = delegate {
      Hello(j);
      void  RNTRNTRNT_61 = delegate { Hello(j); };
      IACSharpSensor.IACSharpSensor.SensorReached(166);
      return RNTRNTRNT_61;
    };
    Simple simple = foo();
    simple();
    IACSharpSensor.IACSharpSensor.SensorReached(167);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(168);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(169);
  }
}
