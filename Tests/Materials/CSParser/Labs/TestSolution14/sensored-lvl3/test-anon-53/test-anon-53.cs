using System;
public delegate void Foo();
class Test
{
  public Test(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(240);
    Foo foo = delegate { Console.WriteLine(a); };
    foo();
    IACSharpSensor.IACSharpSensor.SensorReached(241);
  }
  static Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(242);
    int a = 5;
    Foo foo = delegate { Console.WriteLine(a); };
    foo();
    IACSharpSensor.IACSharpSensor.SensorReached(243);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(244);
    Test test = new Test(9);
    Console.WriteLine(test);
    IACSharpSensor.IACSharpSensor.SensorReached(245);
  }
}
