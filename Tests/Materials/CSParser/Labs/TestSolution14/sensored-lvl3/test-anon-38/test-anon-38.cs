using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public void Hello(long i, long j)
  {
  }
  public void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(155);
    long j = 1 << i;
    Hello(i, j);
    Foo foo = delegate {
      long k = 5 * j;
      Hello(j, k);
      void  RNTRNTRNT_59 = delegate { Hello(j, k); };
      IACSharpSensor.IACSharpSensor.SensorReached(156);
      return RNTRNTRNT_59;
    };
    Simple simple = foo();
    simple();
    IACSharpSensor.IACSharpSensor.SensorReached(157);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(158);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(159);
  }
}
