using System;
public class X
{
  public delegate void TestDelegate();
  static long sum_i, sum_k, sum_p;
  public static int Test(int p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(273);
    TestDelegate d = null;
    for (int i = 1; i <= 5; i++) {
      int k = i;
      TestDelegate temp = delegate {
        Console.WriteLine("i = {0}, k = {1}, p = {2}", i, k, p);
        sum_i += 1 << i;
        sum_k += 1 << k;
        sum_p += 1 << p;
        p += k;
      };
      temp();
      d += temp;
    }
    Console.WriteLine("SUM i = {0}, k = {1}, p = {2}", sum_i, sum_k, sum_p);
    Console.WriteLine();
    if (sum_i != 62) {
      System.Int32 RNTRNTRNT_90 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(274);
      return RNTRNTRNT_90;
    }
    if (sum_k != 62) {
      System.Int32 RNTRNTRNT_91 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(275);
      return RNTRNTRNT_91;
    }
    if (sum_p != 35168) {
      System.Int32 RNTRNTRNT_92 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(276);
      return RNTRNTRNT_92;
    }
    sum_i = sum_k = sum_p = 0;
    d();
    Console.WriteLine("SUM i = {0}, k = {1}, p = {2}", sum_i, sum_k, sum_p);
    Console.WriteLine();
    if (sum_i != 320) {
      System.Int32 RNTRNTRNT_93 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(277);
      return RNTRNTRNT_93;
    }
    if (sum_k != 62) {
      System.Int32 RNTRNTRNT_94 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(278);
      return RNTRNTRNT_94;
    }
    if (sum_p != 1152385024) {
      System.Int32 RNTRNTRNT_95 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(279);
      return RNTRNTRNT_95;
    }
    System.Int32 RNTRNTRNT_96 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(280);
    return RNTRNTRNT_96;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(281);
    int result = Test(5);
    if (result != 0) {
      Console.WriteLine("ERROR: {0}", result);
    } else {
      Console.WriteLine("OK");
    }
    System.Int32 RNTRNTRNT_97 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(282);
    return RNTRNTRNT_97;
  }
}
