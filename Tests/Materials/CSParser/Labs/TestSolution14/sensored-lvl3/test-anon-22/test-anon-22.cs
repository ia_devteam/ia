using System;
delegate void D();
class X
{
  static D r;
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(88);
    D d = T();
    d();
    r();
    r();
    IACSharpSensor.IACSharpSensor.SensorReached(89);
  }
  static D T()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(90);
    int var1 = 0;
    D d = delegate() {
      int var2 = 1;
      r = delegate {
        Console.WriteLine("var1: {0} var2: {1}", var1, var2);
        var2 = var2 + 1;
      };
      var1 = var1 + 1;
    };
    D RNTRNTRNT_44 = d;
    IACSharpSensor.IACSharpSensor.SensorReached(91);
    return RNTRNTRNT_44;
  }
}
