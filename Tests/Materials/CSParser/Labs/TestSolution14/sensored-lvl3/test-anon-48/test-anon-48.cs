using System;
public delegate void Foo();
public class World
{
  public void Hello(long a)
  {
  }
  public void Test(int t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(205);
    for (long l = 0; l < t; l++) {
      for (long m = 0; m < l; m++) {
        for (int u = 0; u < t; u++) {
          Foo foo = delegate {
            Hello(u);
            Hello(l);
            Hello(m);
          };
          foo();
        }
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(206);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(207);
    World world = new World();
    world.Test(5);
    IACSharpSensor.IACSharpSensor.SensorReached(208);
  }
}
