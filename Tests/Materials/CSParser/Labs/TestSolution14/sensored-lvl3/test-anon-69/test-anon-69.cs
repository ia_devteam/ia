using System;
public delegate object TargetAccessDelegate(object user_data);
public class SingleSteppingEngine
{
  bool engine_stopped;
  object SendCommand(TargetAccessDelegate target)
  {
    System.Object RNTRNTRNT_131 = target(null);
    IACSharpSensor.IACSharpSensor.SensorReached(339);
    return RNTRNTRNT_131;
  }
  public void Detach()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(340);
    SendCommand(delegate {
      if (!engine_stopped) {
        throw new InvalidOperationException();
      }
      return null;
    });
    IACSharpSensor.IACSharpSensor.SensorReached(341);
  }
}
class X
{
  static void Main()
  {
  }
}
