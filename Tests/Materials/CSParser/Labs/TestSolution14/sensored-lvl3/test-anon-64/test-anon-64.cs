using System;
class Source
{
  public event EventHandler ChildSourceAdded;
  public event EventHandler ChildSourceRemoved;
  Source FindSource(Source x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(329);
    return null;
  }
  private void AddSource(Source source, int position, object parent)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(330);
    if (!FindSource(source).Equals(source)) {
      IACSharpSensor.IACSharpSensor.SensorReached(331);
      return;
    }
    object iter = null;
    source.ChildSourceAdded += delegate(object t, EventArgs e) { AddSource((Source)(object)e, position, iter); };
    source.ChildSourceRemoved += delegate(object t, EventArgs e) { };
    IACSharpSensor.IACSharpSensor.SensorReached(332);
  }
  static void Main()
  {
  }
}
