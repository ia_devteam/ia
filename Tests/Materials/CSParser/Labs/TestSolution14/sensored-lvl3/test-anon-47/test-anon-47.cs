using System;
public delegate void Foo();
public class World
{
  public void Hello(long a)
  {
  }
  public void Test(int t)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(201);
    Hello(t);
    long j = 1 << t;
    for (int u = 0; u < j; u++) {
      long test = t;
      Foo foo = delegate {
        long v = u * t * test;
        Hello(v);
      };
    }
    IACSharpSensor.IACSharpSensor.SensorReached(202);
  }
}
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    World world = new World();
    world.Test(5);
    IACSharpSensor.IACSharpSensor.SensorReached(204);
  }
}
