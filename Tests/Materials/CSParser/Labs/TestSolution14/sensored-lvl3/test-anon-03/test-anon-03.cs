using System;
delegate void S();
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(8);
    int a = 1;
    S b = delegate { a = 2; };
    b();
    Console.WriteLine("Back, got " + a);
    IACSharpSensor.IACSharpSensor.SensorReached(9);
  }
}
