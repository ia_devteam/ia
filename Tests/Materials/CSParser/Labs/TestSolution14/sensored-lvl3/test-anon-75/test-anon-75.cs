using System;
delegate bool D();
class Data
{
  public D d;
}
public class Test
{
  int value;
  D change;
  static void Foo(int i, D d)
  {
  }
  public static void Main()
  {
  }
  void TestMe()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(362);
    if (true) {
      Data data = null;
      if (data != null) {
        D d2 = delegate {
          void  RNTRNTRNT_135 = true;
          IACSharpSensor.IACSharpSensor.SensorReached(363);
          return RNTRNTRNT_135;
        };
        change += d2;
        data.d += delegate {
          change -= d2;
          Foo(10, delegate {
            data = null;
            return false;
          });
          return true;
        };
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(364);
  }
}
