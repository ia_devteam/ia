using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public void Hello(long k)
  {
  }
  public void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(160);
    long j = 1 << i;
    Hello(j);
    Foo foo = delegate {
      Hello(j);
      void  RNTRNTRNT_60 = delegate { Hello(j); };
      IACSharpSensor.IACSharpSensor.SensorReached(161);
      return RNTRNTRNT_60;
    };
    Simple simple = foo();
    simple();
    IACSharpSensor.IACSharpSensor.SensorReached(162);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(163);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(164);
  }
}
