using System;
using System.Collections;
using System.Reflection;
class test
{
  public IEnumerable testen(int x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(121);
    for (int i = 0; i < x; i++) {
      if (i % 2 == 0) {
        int o = i;
        IEnumerable RNTRNTRNT_56 = o;
        IACSharpSensor.IACSharpSensor.SensorReached(122);
        yield return RNTRNTRNT_56;
        IACSharpSensor.IACSharpSensor.SensorReached(123);
      } else {
        int o = i * 2;
        IEnumerable RNTRNTRNT_57 = o;
        IACSharpSensor.IACSharpSensor.SensorReached(124);
        yield return RNTRNTRNT_57;
        IACSharpSensor.IACSharpSensor.SensorReached(125);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(126);
  }
}
class reflect
{
  public static void Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(127);
    Hashtable ht = new Hashtable();
    Assembly asm = Assembly.GetAssembly(typeof(test));
    foreach (Type t in asm.GetTypes()) {
      ht.Clear();
      foreach (FieldInfo fi in t.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)) {
        ht.Add(fi.Name, fi);
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(128);
  }
}
