using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public void Hello(long i, long j)
  {
  }
  public void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(180);
    long j = 1 << i;
    Hello(i, j);
    Foo foo = delegate {
      long k = 5 * j;
      Hello(j, k);
      void  RNTRNTRNT_64 = delegate { Hello(j, k); };
      IACSharpSensor.IACSharpSensor.SensorReached(181);
      return RNTRNTRNT_64;
    };
    IACSharpSensor.IACSharpSensor.SensorReached(182);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(183);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(184);
  }
}
