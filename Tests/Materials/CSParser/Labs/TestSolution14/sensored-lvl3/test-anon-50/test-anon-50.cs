using System;
using System.Collections;
public class Test
{
  public IEnumerable Foo(int a)
  {
    IEnumerable RNTRNTRNT_69 = a;
    IACSharpSensor.IACSharpSensor.SensorReached(215);
    yield return RNTRNTRNT_69;
    IACSharpSensor.IACSharpSensor.SensorReached(216);
    IEnumerable RNTRNTRNT_70 = a * a;
    IACSharpSensor.IACSharpSensor.SensorReached(217);
    yield return RNTRNTRNT_70;
    IACSharpSensor.IACSharpSensor.SensorReached(218);
    IACSharpSensor.IACSharpSensor.SensorReached(219);
    yield break;
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(220);
    Test test = new Test();
    IEnumerable a = test.Foo(5);
    IEnumerator c = a.GetEnumerator();
    if (!c.MoveNext()) {
      System.Int32 RNTRNTRNT_71 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(221);
      return RNTRNTRNT_71;
    }
    if ((int)c.Current != 5) {
      System.Int32 RNTRNTRNT_72 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(222);
      return RNTRNTRNT_72;
    }
    if (!c.MoveNext()) {
      System.Int32 RNTRNTRNT_73 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(223);
      return RNTRNTRNT_73;
    }
    if ((int)c.Current != 25) {
      System.Int32 RNTRNTRNT_74 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(224);
      return RNTRNTRNT_74;
    }
    IEnumerator d = a.GetEnumerator();
    if ((int)c.Current != 25) {
      System.Int32 RNTRNTRNT_75 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(225);
      return RNTRNTRNT_75;
    }
    if (!d.MoveNext()) {
      System.Int32 RNTRNTRNT_76 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(226);
      return RNTRNTRNT_76;
    }
    if ((int)c.Current != 25) {
      System.Int32 RNTRNTRNT_77 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(227);
      return RNTRNTRNT_77;
    }
    if ((int)d.Current != 5) {
      System.Int32 RNTRNTRNT_78 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(228);
      return RNTRNTRNT_78;
    }
    if (c.MoveNext()) {
      System.Int32 RNTRNTRNT_79 = 13;
      IACSharpSensor.IACSharpSensor.SensorReached(229);
      return RNTRNTRNT_79;
    }
    ((IDisposable)a).Dispose();
    System.Int32 RNTRNTRNT_80 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(230);
    return RNTRNTRNT_80;
  }
}
