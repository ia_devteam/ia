using System;
delegate void D();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(107);
    X x = new X();
    x.M(10);
    e();
    Console.WriteLine("J should be 11= {0}", j);
    e();
    Console.WriteLine("J should be 11= {0}", j);
    x.M(100);
    e();
    Console.WriteLine("J should be 101= {0}", j);
    if (j != 101) {
      System.Int32 RNTRNTRNT_52 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(108);
      return RNTRNTRNT_52;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_53 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(109);
    return RNTRNTRNT_53;
  }
  static int j;
  static D e;
  void M(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(110);
    Console.WriteLine("A is=" + a);
    D d = delegate {
      int b;
      b = 1;
      e = delegate {
        Console.WriteLine("IN NESTED DELEGATE: {0}", a);
        j = a + b;
      };
    };
    d();
    IACSharpSensor.IACSharpSensor.SensorReached(111);
  }
}
