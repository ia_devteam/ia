using System;
using System.Reflection;
using System.ComponentModel;
[assembly: CLSCompliant(true)]
[assembly: AssemblyTitle("")]
public class CLSCLass_6
{
  private object disposedEvent = new object();
  public EventHandlerList event_handlers;
  public event Delegate Disposed {
    add { event_handlers.AddHandler(disposedEvent, value); }
    remove { event_handlers.RemoveHandler(disposedEvent, value); }
  }
}
public delegate CLSDelegate Delegate();
[Serializable()]
public class CLSDelegate
{
}
internal class CLSClass_5
{
  [CLSCompliant(true)]
  public uint Test()
  {
    System.UInt32 RNTRNTRNT_153 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(400);
    return RNTRNTRNT_153;
  }
}
[CLSCompliant(true)]
public class CLSClass_4
{
  [CLSCompliant(false)]
  public uint Test()
  {
    System.UInt32 RNTRNTRNT_154 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(401);
    return RNTRNTRNT_154;
  }
}
public class CLSClass_3
{
  [CLSCompliant(false)]
  public uint Test_3()
  {
    System.UInt32 RNTRNTRNT_155 = 6;
    IACSharpSensor.IACSharpSensor.SensorReached(402);
    return RNTRNTRNT_155;
  }
}
[CLSCompliant(false)]
public class CLSClass_2
{
  public sbyte XX {
    get {
      System.SByte RNTRNTRNT_156 = -1;
      IACSharpSensor.IACSharpSensor.SensorReached(403);
      return RNTRNTRNT_156;
    }
  }
}
class CLSClass_1
{
  public UInt32 Valid()
  {
    UInt32 RNTRNTRNT_157 = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(404);
    return RNTRNTRNT_157;
  }
}
[CLSCompliant(true)]
public class CLSClass
{
  private class C1
  {
    [CLSCompliant(true)]
    public class C11
    {
      protected ulong Foo3()
      {
        System.UInt64 RNTRNTRNT_158 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(405);
        return RNTRNTRNT_158;
      }
    }
    protected long Foo2()
    {
      System.Int64 RNTRNTRNT_159 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(406);
      return RNTRNTRNT_159;
    }
  }
  [CLSCompliant(false)]
  protected internal class CLSClass_2
  {
    public sbyte XX {
      get {
        System.SByte RNTRNTRNT_160 = -1;
        IACSharpSensor.IACSharpSensor.SensorReached(407);
        return RNTRNTRNT_160;
      }
    }
  }
  [CLSCompliant(true)]
  private ulong Valid()
  {
    System.UInt64 RNTRNTRNT_161 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(408);
    return RNTRNTRNT_161;
  }
  [CLSCompliant(true)]
  public byte XX {
    get {
      System.Byte RNTRNTRNT_162 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(409);
      return RNTRNTRNT_162;
    }
  }
  internal UInt32 FooInternal()
  {
    UInt32 RNTRNTRNT_163 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(410);
    return RNTRNTRNT_163;
  }
  private ulong Foo()
  {
    System.UInt64 RNTRNTRNT_164 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(411);
    return RNTRNTRNT_164;
  }
  public static void Main()
  {
  }
}
