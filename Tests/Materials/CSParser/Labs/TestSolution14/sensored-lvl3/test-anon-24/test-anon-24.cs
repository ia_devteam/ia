using System;
delegate int D();
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(97);
    D x = T(1);
    Console.WriteLine("Should be 2={0}", x());
    IACSharpSensor.IACSharpSensor.SensorReached(98);
  }
  static D T(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(99);
    D d = delegate {
      a = a + 1;
      D RNTRNTRNT_47 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(100);
      return RNTRNTRNT_47;
    };
    D RNTRNTRNT_48 = d;
    IACSharpSensor.IACSharpSensor.SensorReached(101);
    return RNTRNTRNT_48;
  }
}
