using System;
[assembly: CLSCompliant(true)]
public class CLSClass
{
  public byte XX {
    get {
      System.Byte RNTRNTRNT_165 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(412);
      return RNTRNTRNT_165;
    }
  }
  public static void Main()
  {
  }
}
public class Big
{
  [CLSCompliant(false)]
  public static implicit operator Big(uint value)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    return null;
  }
}
[CLSCompliant(false)]
public partial class C1
{
}
public partial class C1
{
  public void method(uint u)
  {
  }
}
