using System;
delegate void S();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(73);
    int i;
    int a = 0;
    S b = null;
    for (i = 0; i < 10; i++) {
      int j = 0;
      b = delegate {
        Console.WriteLine("i={0} j={1}", i, j);
        i = i + 1;
        j = j + 1;
        a = j;
      };
    }
    b();
    Console.WriteLine("i = {0}", i);
    if (!t(i, 11)) {
      System.Int32 RNTRNTRNT_36 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(74);
      return RNTRNTRNT_36;
    }
    b();
    if (!t(i, 12)) {
      System.Int32 RNTRNTRNT_37 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(75);
      return RNTRNTRNT_37;
    }
    Console.WriteLine("i = {0}", i);
    Console.WriteLine("a = {0}", a);
    if (!t(a, 2)) {
      System.Int32 RNTRNTRNT_38 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(76);
      return RNTRNTRNT_38;
    }
    System.Int32 RNTRNTRNT_39 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(77);
    return RNTRNTRNT_39;
  }
  static bool t(int a, int b)
  {
    System.Boolean RNTRNTRNT_40 = a == b;
    IACSharpSensor.IACSharpSensor.SensorReached(78);
    return RNTRNTRNT_40;
  }
}
