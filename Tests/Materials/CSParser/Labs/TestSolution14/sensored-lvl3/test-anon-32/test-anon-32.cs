public delegate void StringSender(string str);
public delegate void VoidDelegate();
public class MainClass
{
  public static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(129);
    MainClass mc = new MainClass();
    VoidDelegate del = new VoidDelegate(delegate {
      StringSender ss = delegate(string s) { SimpleCallback(mc, s); };
      ss("Yo!");
    });
    del();
    IACSharpSensor.IACSharpSensor.SensorReached(130);
  }
  static void SimpleCallback(MainClass mc, string str)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(131);
    System.Console.WriteLine(str);
    IACSharpSensor.IACSharpSensor.SensorReached(132);
  }
}
