using System;
delegate void D();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(92);
    X x = new X();
    x.M();
    e();
    Console.WriteLine("J should be 101= {0}", j);
    if (j != 101) {
      System.Int32 RNTRNTRNT_45 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(93);
      return RNTRNTRNT_45;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_46 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(94);
    return RNTRNTRNT_46;
  }
  static int j;
  static D e;
  void M()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(95);
    int l = 100;
    D d = delegate {
      int b;
      b = 1;
      e = delegate { j = l + b; };
    };
    d();
    IACSharpSensor.IACSharpSensor.SensorReached(96);
  }
}
