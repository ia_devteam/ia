using System;
using System.Collections;
using System.Text;
using System.Timers;
namespace Delegates
{
  class Space
  {
    public int Value;
    public delegate void DoCopy();
    public DoCopy CopyIt;
    public void Leak(bool useArray, int max)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(137);
      DoCopy one = null;
      if (useArray) {
        int[] work;
        one = delegate { work = new int[max]; };
      } else {
        one = delegate {
          int xans = (max + 1) * max / 2;
        };
      }
      Console.WriteLine("Here goes...");
      one();
      Console.WriteLine("... made it");
      IACSharpSensor.IACSharpSensor.SensorReached(138);
    }
  }
  class Program
  {
    static void SpaceLeak()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(139);
      Space s = new Space();
      Console.WriteLine(s.Value);
      s.Leak(false, 1);
      Console.WriteLine(s.Value);
      IACSharpSensor.IACSharpSensor.SensorReached(140);
    }
    static void Main(string[] args)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(141);
      SpaceLeak();
      IACSharpSensor.IACSharpSensor.SensorReached(142);
    }
  }
}
