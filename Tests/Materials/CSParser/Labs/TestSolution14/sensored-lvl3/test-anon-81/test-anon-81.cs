using System;
class C
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(392);
    if (new C().Test() != 6) {
      System.Int32 RNTRNTRNT_149 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(393);
      return RNTRNTRNT_149;
    }
    System.Int32 RNTRNTRNT_150 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(394);
    return RNTRNTRNT_150;
  }
  public delegate void Cmd();
  public delegate int Cmd2();
  int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(395);
    int r = Foo2(delegate() {
      int x = 4;
      Foo(delegate() {
        int y = 6;
        Foo(delegate() { x = y; });
      });
      return x;
    });
    Console.WriteLine(r);
    System.Int32 RNTRNTRNT_151 = r;
    IACSharpSensor.IACSharpSensor.SensorReached(396);
    return RNTRNTRNT_151;
  }
  int Foo2(Cmd2 cmd)
  {
    System.Int32 RNTRNTRNT_152 = cmd();
    IACSharpSensor.IACSharpSensor.SensorReached(397);
    return RNTRNTRNT_152;
  }
  void Foo(Cmd cmd)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(398);
    cmd();
    IACSharpSensor.IACSharpSensor.SensorReached(399);
  }
}
