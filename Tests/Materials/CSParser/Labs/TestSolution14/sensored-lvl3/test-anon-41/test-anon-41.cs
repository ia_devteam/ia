using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public void Hello(long k)
  {
  }
  public void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    long j = 1 << i;
    Hello(j);
    Foo foo = delegate {
      long k = j;
      Hello(j);
      void  RNTRNTRNT_62 = delegate {
        long l = k;
        Hello(j);
      };
      IACSharpSensor.IACSharpSensor.SensorReached(171);
      return RNTRNTRNT_62;
    };
    Simple simple = foo();
    simple();
    IACSharpSensor.IACSharpSensor.SensorReached(172);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(173);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(174);
  }
}
