using System;
using System.Collections;
delegate bool predicate(object a);
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    ArrayList a = new ArrayList();
    for (int i = 0; i < 10; i++) {
      a.Add(i);
    }
    ArrayList even = Find(delegate(object arg) { return ((((int)arg) % 2) == 0); }, a);
    Console.WriteLine("Even numbers");
    foreach (object r in even) {
      Console.WriteLine(r);
    }
    if (even.Count != 5) {
      System.Int32 RNTRNTRNT_26 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(43);
      return RNTRNTRNT_26;
    }
    if (((int)even[0]) != 0 || ((int)even[1]) != 2 || ((int)even[2]) != 4 || ((int)even[3]) != 6 || ((int)even[4]) != 8) {
      System.Int32 RNTRNTRNT_27 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(44);
      return RNTRNTRNT_27;
    }
    System.Int32 RNTRNTRNT_28 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(45);
    return RNTRNTRNT_28;
  }
  static ArrayList Find(predicate p, ArrayList source)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(46);
    ArrayList result = new ArrayList();
    foreach (object a in source) {
      if (p(a)) {
        result.Add(a);
      }
    }
    ArrayList RNTRNTRNT_29 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(47);
    return RNTRNTRNT_29;
  }
}
