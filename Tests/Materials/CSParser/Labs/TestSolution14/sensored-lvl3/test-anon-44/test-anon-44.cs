using System;
delegate void Simple();
delegate Simple Foo();
class X
{
  public void Hello(long k)
  {
  }
  public void Test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(185);
    Hello(3);
    Foo foo = delegate {
      int a = i;
      Hello(4);
      void  RNTRNTRNT_65 = delegate {
        int b = a;
        Hello(5);
      };
      IACSharpSensor.IACSharpSensor.SensorReached(186);
      return RNTRNTRNT_65;
    };
    Foo bar = delegate {
      int c = i;
      Hello(6);
      void  RNTRNTRNT_66 = delegate {
        int d = i;
        Hello(7);
      };
      IACSharpSensor.IACSharpSensor.SensorReached(187);
      return RNTRNTRNT_66;
    };
    Simple simple = foo();
    simple();
    IACSharpSensor.IACSharpSensor.SensorReached(188);
  }
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(189);
    X x = new X();
    x.Test(3);
    IACSharpSensor.IACSharpSensor.SensorReached(190);
  }
}
