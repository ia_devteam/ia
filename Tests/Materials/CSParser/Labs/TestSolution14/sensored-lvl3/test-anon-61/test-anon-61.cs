using System;
public class X
{
  public int p;
  public delegate void TestDelegate();
  static long sum_i, sum_k, sum_p;
  public int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(308);
    TestDelegate d = null;
    for (int i = 1; i <= 5; i++) {
      int k = i;
      TestDelegate temp = delegate {
        Console.WriteLine("i = {0}, k = {1}, p = {2}", i, k, p);
        sum_i += 1 << i;
        sum_k += 1 << k;
        sum_p += 1 << p;
        p += k;
      };
      temp();
      d += temp;
    }
    Console.WriteLine("SUM i = {0}, k = {1}, p = {2}", sum_i, sum_k, sum_p);
    Console.WriteLine();
    if (sum_i != 62) {
      System.Int32 RNTRNTRNT_119 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(309);
      return RNTRNTRNT_119;
    }
    if (sum_k != 62) {
      System.Int32 RNTRNTRNT_120 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(310);
      return RNTRNTRNT_120;
    }
    if (sum_p != 35168) {
      System.Int32 RNTRNTRNT_121 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(311);
      return RNTRNTRNT_121;
    }
    sum_i = sum_k = sum_p = 0;
    d();
    Console.WriteLine("SUM i = {0}, k = {1}, p = {2}", sum_i, sum_k, sum_p);
    Console.WriteLine();
    if (sum_i != 320) {
      System.Int32 RNTRNTRNT_122 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(312);
      return RNTRNTRNT_122;
    }
    if (sum_k != 62) {
      System.Int32 RNTRNTRNT_123 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(313);
      return RNTRNTRNT_123;
    }
    if (sum_p != 1152385024) {
      System.Int32 RNTRNTRNT_124 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(314);
      return RNTRNTRNT_124;
    }
    System.Int32 RNTRNTRNT_125 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(315);
    return RNTRNTRNT_125;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(316);
    X x = new X();
    x.p = 5;
    int result = x.Test();
    if (result != 0) {
      Console.WriteLine("ERROR: {0}", result);
    } else {
      Console.WriteLine("OK");
    }
    System.Int32 RNTRNTRNT_126 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(317);
    return RNTRNTRNT_126;
  }
}
