using System;
delegate void D();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(23);
    int a = 0;
    D d1 = delegate {
      Console.WriteLine("First");
      a = 1;
    };
    D d2 = delegate {
      Console.WriteLine("Second");
      a = 2;
    };
    if (!t(a, 0)) {
      System.Int32 RNTRNTRNT_14 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(24);
      return RNTRNTRNT_14;
    }
    d1();
    if (!t(a, 1)) {
      System.Int32 RNTRNTRNT_15 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(25);
      return RNTRNTRNT_15;
    }
    d2();
    if (!t(a, 2)) {
      System.Int32 RNTRNTRNT_16 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(26);
      return RNTRNTRNT_16;
    }
    Console.WriteLine("Test passes OK");
    System.Int32 RNTRNTRNT_17 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(27);
    return RNTRNTRNT_17;
  }
  static bool t(int a, int b)
  {
    System.Boolean RNTRNTRNT_18 = a == b;
    IACSharpSensor.IACSharpSensor.SensorReached(28);
    return RNTRNTRNT_18;
  }
}
