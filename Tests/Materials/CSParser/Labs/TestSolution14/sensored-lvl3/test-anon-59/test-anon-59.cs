using System;
public class X
{
  public delegate void TestDelegate();
  static long sum_i, sum_j, sum_k;
  static ulong sum_p;
  public static int Test(int p)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(283);
    TestDelegate d = null;
    for (int i = 1; i <= 5; i++) {
      for (int j = i; j <= 8; j++) {
        int k = i;
        TestDelegate temp = delegate {
          Console.WriteLine("i = {0}, j = {1}, k = {2}, p = {3}", i, j, k, p);
          sum_i += 1 << i;
          sum_j += 1 << j;
          sum_k += 1 << k;
          sum_p += (ulong)(1 << p);
          p += k;
        };
        temp();
        d += temp;
      }
    }
    Console.WriteLine("SUM i = {0}, j = {1}, k = {2}, p = {3}", sum_i, sum_j, sum_k, sum_p);
    Console.WriteLine();
    if (sum_i != 300) {
      System.Int32 RNTRNTRNT_98 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(284);
      return RNTRNTRNT_98;
    }
    if (sum_j != 2498) {
      System.Int32 RNTRNTRNT_99 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(285);
      return RNTRNTRNT_99;
    }
    if (sum_k != 300) {
      System.Int32 RNTRNTRNT_100 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(286);
      return RNTRNTRNT_100;
    }
    if (sum_p != 1825434804) {
      System.Int32 RNTRNTRNT_101 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(287);
      return RNTRNTRNT_101;
    }
    sum_i = sum_j = sum_k = 0;
    sum_p = 0;
    d();
    Console.WriteLine("SUM i = {0}, j = {1}, k = {2}, p = {3}", sum_i, sum_j, sum_k, sum_p);
    Console.WriteLine();
    if (sum_i != 1920) {
      System.Int32 RNTRNTRNT_102 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(288);
      return RNTRNTRNT_102;
    }
    if (sum_j != 15360) {
      System.Int32 RNTRNTRNT_103 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(289);
      return RNTRNTRNT_103;
    }
    if (sum_k != 300) {
      System.Int32 RNTRNTRNT_104 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(290);
      return RNTRNTRNT_104;
    }
    if (sum_p != 18446744073385831629uL) {
      System.Int32 RNTRNTRNT_105 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(291);
      return RNTRNTRNT_105;
    }
    System.Int32 RNTRNTRNT_106 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(292);
    return RNTRNTRNT_106;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(293);
    int result = Test(5);
    if (result != 0) {
      Console.WriteLine("ERROR: {0}", result);
    } else {
      Console.WriteLine("OK");
    }
    System.Int32 RNTRNTRNT_107 = result;
    IACSharpSensor.IACSharpSensor.SensorReached(294);
    return RNTRNTRNT_107;
  }
}
