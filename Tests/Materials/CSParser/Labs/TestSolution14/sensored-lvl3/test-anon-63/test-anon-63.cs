using System;
using System.Collections;
class X
{
  delegate int A();
  static IEnumerator GetIt(int[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(322);
    foreach (int arg in args) {
      Console.WriteLine("OUT: {0}", arg);
      A a = delegate {
        Console.WriteLine("arg: {0}", arg);
        Console.WriteLine("args: {0}", args);
        IEnumerator RNTRNTRNT_127 = arg;
        IACSharpSensor.IACSharpSensor.SensorReached(323);
        return RNTRNTRNT_127;
      };
      Object RNTRNTRNT_128 = a();
      IACSharpSensor.IACSharpSensor.SensorReached(324);
      yield return RNTRNTRNT_128;
      IACSharpSensor.IACSharpSensor.SensorReached(325);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(326);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(327);
    IEnumerator enumerator = GetIt(new int[] {
      4,
      8,
      9
    });
    enumerator.MoveNext();
    System.Int32 RNTRNTRNT_129 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(328);
    return RNTRNTRNT_129;
  }
}
