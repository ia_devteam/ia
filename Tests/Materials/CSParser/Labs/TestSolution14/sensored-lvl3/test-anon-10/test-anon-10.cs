using System;
class S
{
  delegate void T();
  T t;
  int f;
  public void Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(35);
    int a = 1;
    for (int i = a; i < 10; i++) {
      int j = i;
      t = delegate {
        Console.WriteLine("Before: {0} {1} {2}", f, i, j);
        f = i;
      };
    }
    IACSharpSensor.IACSharpSensor.SensorReached(36);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(37);
    S s = new S();
    s.Test();
    s.t();
    if (s.f == 10) {
      System.Int32 RNTRNTRNT_23 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(38);
      return RNTRNTRNT_23;
    }
    Console.WriteLine("Failed:" + s.f);
    System.Int32 RNTRNTRNT_24 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(39);
    return RNTRNTRNT_24;
  }
}
