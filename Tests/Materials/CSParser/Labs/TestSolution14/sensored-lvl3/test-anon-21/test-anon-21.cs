using System;
delegate void D();
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(83);
    X x = new X();
    x.M();
    e();
    Console.WriteLine("J should be 101= {0}", j);
    if (j != 101) {
      System.Int32 RNTRNTRNT_42 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(84);
      return RNTRNTRNT_42;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_43 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(85);
    return RNTRNTRNT_43;
  }
  static int j = 0;
  static D e;
  void M()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(86);
    int l = 100;
    D d = delegate {
      int b;
      b = 1;
      Console.WriteLine("Inside d");
      e = delegate {
        Console.WriteLine("Inside e");
        j = l + b;
        Console.WriteLine("j={0} l={1} b={2}", j, l, b);
      };
    };
    Console.WriteLine("Calling d");
    d();
    IACSharpSensor.IACSharpSensor.SensorReached(87);
  }
}
