using System;
delegate object FactoryDelegate();
public class C
{
  FactoryDelegate var;
  int counter;
  FactoryDelegate this[string s] {
    get {
      FactoryDelegate RNTRNTRNT_136 = var;
      IACSharpSensor.IACSharpSensor.SensorReached(365);
      return RNTRNTRNT_136;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(366);
      var = value;
      IACSharpSensor.IACSharpSensor.SensorReached(367);
    }
  }
  public void X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    this["ABC"] = delegate() {
      ++counter;
      Console.WriteLine("A");
      return "Return";
    };
    IACSharpSensor.IACSharpSensor.SensorReached(369);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(370);
    C o = new C();
    o.X();
    Console.WriteLine("B");
    Console.WriteLine(o["ABC"]());
    Console.WriteLine(o.counter);
    if (o.counter != 1) {
      System.Int32 RNTRNTRNT_137 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(371);
      return RNTRNTRNT_137;
    }
    System.Int32 RNTRNTRNT_138 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(372);
    return RNTRNTRNT_138;
  }
}
