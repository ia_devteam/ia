class X
{
  public static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2002);
    int a, b, c, d;
    a = b = 10;
    c = d = 14;
    IACSharpSensor.IACSharpSensor.SensorReached(2003);
    if ((a + b) != 20) {
      System.Int32 RNTRNTRNT_903 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(2004);
      return RNTRNTRNT_903;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2005);
    if ((a + d) != 24) {
      System.Int32 RNTRNTRNT_904 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(2006);
      return RNTRNTRNT_904;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2007);
    if ((c + d) != 28) {
      System.Int32 RNTRNTRNT_905 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(2008);
      return RNTRNTRNT_905;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2009);
    if ((b + c) != 24) {
      System.Int32 RNTRNTRNT_906 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(2010);
      return RNTRNTRNT_906;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2011);
    if (a++ != 10) {
      System.Int32 RNTRNTRNT_907 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(2012);
      return RNTRNTRNT_907;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2013);
    if (++a != 12) {
      System.Int32 RNTRNTRNT_908 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(2014);
      return RNTRNTRNT_908;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2015);
    if (b-- != 10) {
      System.Int32 RNTRNTRNT_909 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(2016);
      return RNTRNTRNT_909;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2017);
    if (--b != 8) {
      System.Int32 RNTRNTRNT_910 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(2018);
      return RNTRNTRNT_910;
    }
    System.Int32 RNTRNTRNT_911 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(2019);
    return RNTRNTRNT_911;
  }
}
