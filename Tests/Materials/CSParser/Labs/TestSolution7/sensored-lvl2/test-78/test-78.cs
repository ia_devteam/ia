namespace N1
{
  public enum A
  {
    A_1,
    A_2,
    A_3
  }
  public class B
  {
    static bool ShortCasting()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1987);
      short i = 0;
      N1.A a = N1.A.A_1;
      i = (short)a;
      a = (N1.A)i;
      IACSharpSensor.IACSharpSensor.SensorReached(1988);
      if (a != N1.A.A_1) {
        System.Boolean RNTRNTRNT_895 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1989);
        return RNTRNTRNT_895;
      }
      System.Boolean RNTRNTRNT_896 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1990);
      return RNTRNTRNT_896;
    }
    static bool IntCasting()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1991);
      int i = 0;
      N1.A a = N1.A.A_1;
      i = (int)a;
      a = (N1.A)i;
      IACSharpSensor.IACSharpSensor.SensorReached(1992);
      if (a != N1.A.A_1) {
        System.Boolean RNTRNTRNT_897 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1993);
        return RNTRNTRNT_897;
      }
      System.Boolean RNTRNTRNT_898 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1994);
      return RNTRNTRNT_898;
    }
    static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1995);
      if (!IntCasting()) {
        System.Int32 RNTRNTRNT_899 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1996);
        return RNTRNTRNT_899;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1997);
      if (!ShortCasting()) {
        System.Int32 RNTRNTRNT_900 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(1998);
        return RNTRNTRNT_900;
      }
      System.Int32 RNTRNTRNT_901 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1999);
      return RNTRNTRNT_901;
    }
  }
}
