using System;
namespace N1
{
  public class A
  {
    int x;
    string s;
    void Bar()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2026);
      x = int.Parse("0");
      s = string.Format("{0}", x);
      IACSharpSensor.IACSharpSensor.SensorReached(2027);
    }
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2028);
      A a = new A();
      a.Bar();
      IACSharpSensor.IACSharpSensor.SensorReached(2029);
      if (a.x != 0) {
        System.Int32 RNTRNTRNT_915 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(2030);
        return RNTRNTRNT_915;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2031);
      if (a.s != "0") {
        System.Int32 RNTRNTRNT_916 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(2032);
        return RNTRNTRNT_916;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2033);
      Console.WriteLine("Bar set s to " + a.s);
      System.Int32 RNTRNTRNT_917 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(2034);
      return RNTRNTRNT_917;
    }
  }
}
