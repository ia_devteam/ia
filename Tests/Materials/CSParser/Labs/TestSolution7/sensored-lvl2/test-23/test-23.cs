using System;
class X
{
  static int v;
  static X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(226);
    v = 10;
    IACSharpSensor.IACSharpSensor.SensorReached(227);
  }
  public static int Value {
    get {
      System.Int32 RNTRNTRNT_69 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(228);
      return RNTRNTRNT_69;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(229);
      v = value;
      IACSharpSensor.IACSharpSensor.SensorReached(230);
    }
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(231);
    if (Value != 10) {
      System.Int32 RNTRNTRNT_70 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(232);
      return RNTRNTRNT_70;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(233);
    Value = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(234);
    if (Value != 4) {
      System.Int32 RNTRNTRNT_71 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(235);
      return RNTRNTRNT_71;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(236);
    Y y = new Y("hello");
    IACSharpSensor.IACSharpSensor.SensorReached(237);
    if (y.Value != "hello") {
      System.Int32 RNTRNTRNT_72 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(238);
      return RNTRNTRNT_72;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(239);
    y.Value = "goodbye";
    IACSharpSensor.IACSharpSensor.SensorReached(240);
    if (y.Value != "goodbye") {
      System.Int32 RNTRNTRNT_73 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(241);
      return RNTRNTRNT_73;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(242);
    Z z = new Z();
    IACSharpSensor.IACSharpSensor.SensorReached(243);
    if (Z.IVal != 4) {
      System.Int32 RNTRNTRNT_74 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(244);
      return RNTRNTRNT_74;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(245);
    Z.IVal = 10;
    IACSharpSensor.IACSharpSensor.SensorReached(246);
    if (Z.IVal != 10) {
      System.Int32 RNTRNTRNT_75 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(247);
      return RNTRNTRNT_75;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(248);
    z.XVal = 23;
    IACSharpSensor.IACSharpSensor.SensorReached(249);
    if (z.XVal != 23) {
      System.Int32 RNTRNTRNT_76 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(250);
      return RNTRNTRNT_76;
    }
    System.Int32 RNTRNTRNT_77 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(251);
    return RNTRNTRNT_77;
  }
}
class Y
{
  string init;
  public Y(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(252);
    init = s;
    IACSharpSensor.IACSharpSensor.SensorReached(253);
  }
  public string Value {
    get {
      System.String RNTRNTRNT_78 = init;
      IACSharpSensor.IACSharpSensor.SensorReached(254);
      return RNTRNTRNT_78;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(255);
      init = value;
      IACSharpSensor.IACSharpSensor.SensorReached(256);
    }
  }
}
struct Z
{
  static int val;
  int xval;
  static Z()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(257);
    val = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(258);
  }
  public static int IVal {
    get {
      System.Int32 RNTRNTRNT_79 = val;
      IACSharpSensor.IACSharpSensor.SensorReached(259);
      return RNTRNTRNT_79;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(260);
      val = value;
      IACSharpSensor.IACSharpSensor.SensorReached(261);
    }
  }
  public int XVal {
    get {
      System.Int32 RNTRNTRNT_80 = xval;
      IACSharpSensor.IACSharpSensor.SensorReached(262);
      return RNTRNTRNT_80;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(263);
      xval = value;
      IACSharpSensor.IACSharpSensor.SensorReached(264);
    }
  }
}
