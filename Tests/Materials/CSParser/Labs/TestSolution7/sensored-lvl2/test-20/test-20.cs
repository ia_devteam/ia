using System;
class A
{
  public int a;
  public void X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(185);
    a = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(186);
  }
}
class B : A
{
  void X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(187);
    a = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(188);
  }
  public void TestB()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(189);
    X();
    IACSharpSensor.IACSharpSensor.SensorReached(190);
  }
}
class Ax
{
  public int a;
  public virtual void A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(191);
    a = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(192);
  }
  public virtual void B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(193);
    a = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(194);
  }
}
class Bx : Ax
{
  public override void A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(195);
    a = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(196);
  }
  public new void B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(197);
    a = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(198);
  }
}
class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(199);
    B b = new B();
    b.TestB();
    IACSharpSensor.IACSharpSensor.SensorReached(200);
    if (b.a != 2) {
      System.Int32 RNTRNTRNT_58 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(201);
      return RNTRNTRNT_58;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(202);
    Bx bx = new Bx();
    bx.A();
    IACSharpSensor.IACSharpSensor.SensorReached(203);
    if (b.a != 2) {
      System.Int32 RNTRNTRNT_59 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(204);
      return RNTRNTRNT_59;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(205);
    bx.B();
    Console.WriteLine("a=" + bx.a);
    IACSharpSensor.IACSharpSensor.SensorReached(206);
    if (bx.a != 4) {
      System.Int32 RNTRNTRNT_60 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(207);
      return RNTRNTRNT_60;
    }
    System.Int32 RNTRNTRNT_61 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(208);
    return RNTRNTRNT_61;
  }
}
