using c = System.Console;
using s = System;
using System2 = System;
namespace A
{
  namespace B
  {
    class C
    {
      public static void Hola()
      {
        IACSharpSensor.IACSharpSensor.SensorReached(1620);
        c.WriteLine("Hola!");
        IACSharpSensor.IACSharpSensor.SensorReached(1621);
      }
    }
  }
}
namespace X
{
  namespace Y
  {
    namespace Z
    {
      class W
      {
        public static void Ahoj()
        {
          IACSharpSensor.IACSharpSensor.SensorReached(1622);
          s.Console.WriteLine("Ahoj!");
          IACSharpSensor.IACSharpSensor.SensorReached(1623);
        }
      }
    }
  }
}
namespace Foo
{
  class System
  {
    static void X()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1624);
      System2.Console.WriteLine("FOO");
      IACSharpSensor.IACSharpSensor.SensorReached(1625);
    }
  }
}
class App
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1626);
    A.B.C.Hola();
    X.Y.Z.W.Ahoj();
    System2.Net.IPAddress[] addresses2;
    System.Int32 RNTRNTRNT_764 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1627);
    return RNTRNTRNT_764;
  }
}
