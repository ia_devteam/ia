using System;
class Base
{
  public int val;
  public void Add(int x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(364);
    Console.WriteLine("Incorrect method called");
    val = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(365);
  }
}
class Derived : Base
{
  public void Add(double x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(366);
    Console.WriteLine("Calling the derived class with double! Excellent!");
    val = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(367);
  }
}
class Demo
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(368);
    Derived d = new Derived();
    d.Add(1);
    IACSharpSensor.IACSharpSensor.SensorReached(369);
    if (d.val == 1) {
      System.Int32 RNTRNTRNT_115 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(370);
      return RNTRNTRNT_115;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(371);
    if (d.val == 2) {
      System.Int32 RNTRNTRNT_116 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(372);
      return RNTRNTRNT_116;
    }
    System.Int32 RNTRNTRNT_117 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(373);
    return RNTRNTRNT_117;
  }
}
