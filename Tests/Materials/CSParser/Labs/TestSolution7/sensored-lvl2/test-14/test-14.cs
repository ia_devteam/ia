using System;
namespace Obj
{
  interface Bah
  {
    int H();
  }
  class A : Bah
  {
    public int F()
    {
      System.Int32 RNTRNTRNT_18 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(74);
      return RNTRNTRNT_18;
    }
    public virtual int G()
    {
      System.Int32 RNTRNTRNT_19 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(75);
      return RNTRNTRNT_19;
    }
    public int H()
    {
      System.Int32 RNTRNTRNT_20 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(76);
      return RNTRNTRNT_20;
    }
  }
  class B : A
  {
    public new int F()
    {
      System.Int32 RNTRNTRNT_21 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(77);
      return RNTRNTRNT_21;
    }
    public override int G()
    {
      System.Int32 RNTRNTRNT_22 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(78);
      return RNTRNTRNT_22;
    }
    public new int H()
    {
      System.Int32 RNTRNTRNT_23 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(79);
      return RNTRNTRNT_23;
    }
  }
  class Test
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(80);
      int result = 0;
      B b = new B();
      A a = b;
      IACSharpSensor.IACSharpSensor.SensorReached(81);
      if (a.F() != 1) {
        IACSharpSensor.IACSharpSensor.SensorReached(82);
        result |= 1 << 0;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(83);
      if (b.F() != 3) {
        IACSharpSensor.IACSharpSensor.SensorReached(84);
        result |= 1 << 1;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(85);
      if (b.G() != 4) {
        IACSharpSensor.IACSharpSensor.SensorReached(86);
        result |= 1 << 2;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(87);
      if (a.G() != 4) {
        IACSharpSensor.IACSharpSensor.SensorReached(88);
        Console.WriteLine("oops: " + a.G());
        result |= 1 << 3;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(89);
      if (a.H() != 10) {
        IACSharpSensor.IACSharpSensor.SensorReached(90);
        result |= 1 << 4;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(91);
      if (b.H() != 11) {
        IACSharpSensor.IACSharpSensor.SensorReached(92);
        result |= 1 << 5;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(93);
      if (((A)b).H() != 10) {
        IACSharpSensor.IACSharpSensor.SensorReached(94);
        result |= 1 << 6;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(95);
      if (((B)a).H() != 11) {
        IACSharpSensor.IACSharpSensor.SensorReached(96);
        result |= 1 << 7;
      }
      System.Int32 RNTRNTRNT_24 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(97);
      return RNTRNTRNT_24;
    }
  }
}
