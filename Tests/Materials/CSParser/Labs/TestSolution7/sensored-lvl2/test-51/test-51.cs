using System;
class Base
{
  public int b_int_field;
  public string b_string_field;
  public const int b_const_three = 3;
  public int b_int_property {
    get {
      System.Int32 RNTRNTRNT_734 = b_int_field;
      IACSharpSensor.IACSharpSensor.SensorReached(1528);
      return RNTRNTRNT_734;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1529);
      b_int_field = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1530);
    }
  }
  public string b_get_id()
  {
    System.String RNTRNTRNT_735 = "Base";
    IACSharpSensor.IACSharpSensor.SensorReached(1531);
    return RNTRNTRNT_735;
  }
  public Base()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1532);
    b_int_field = 1;
    b_string_field = "base";
    IACSharpSensor.IACSharpSensor.SensorReached(1533);
  }
}
class Derived : Base
{
  new int b_int_field;
  new string b_string_field;
  new const int b_const_three = 4;
  new int b_int_property {
    get {
      System.Int32 RNTRNTRNT_736 = b_int_field;
      IACSharpSensor.IACSharpSensor.SensorReached(1534);
      return RNTRNTRNT_736;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1535);
      b_int_field = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1536);
    }
  }
  public Derived()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1537);
    b_int_field = 10;
    b_string_field = "derived";
    IACSharpSensor.IACSharpSensor.SensorReached(1538);
  }
  public int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1539);
    if (b_int_field != 10) {
      System.Int32 RNTRNTRNT_737 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1540);
      return RNTRNTRNT_737;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1541);
    if (base.b_int_field != 1) {
      System.Int32 RNTRNTRNT_738 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1542);
      return RNTRNTRNT_738;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1543);
    if (base.b_string_field != "base") {
      System.Int32 RNTRNTRNT_739 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1544);
      return RNTRNTRNT_739;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1545);
    if (b_string_field != "derived") {
      System.Int32 RNTRNTRNT_740 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1546);
      return RNTRNTRNT_740;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1547);
    base.b_int_property = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(1548);
    if (b_int_property != 10) {
      System.Int32 RNTRNTRNT_741 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(1549);
      return RNTRNTRNT_741;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1550);
    if (b_int_property != 10) {
      System.Int32 RNTRNTRNT_742 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(1551);
      return RNTRNTRNT_742;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1552);
    if (base.b_int_property != 4) {
      System.Int32 RNTRNTRNT_743 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(1553);
      return RNTRNTRNT_743;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1554);
    if (b_const_three != 4) {
      System.Int32 RNTRNTRNT_744 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(1555);
      return RNTRNTRNT_744;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1556);
    if (Base.b_const_three != 3) {
      System.Int32 RNTRNTRNT_745 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(1557);
      return RNTRNTRNT_745;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1558);
    System.Console.WriteLine("All tests pass");
    System.Int32 RNTRNTRNT_746 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1559);
    return RNTRNTRNT_746;
  }
}
class boot
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1560);
    Derived d = new Derived();
    System.Int32 RNTRNTRNT_747 = d.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(1561);
    return RNTRNTRNT_747;
  }
}
