using System.Collections;
abstract class A
{
  protected abstract int this[int a] { get; }
  public int EmulateIndexer(int a)
  {
    System.Int32 RNTRNTRNT_105 = this[a];
    IACSharpSensor.IACSharpSensor.SensorReached(342);
    return RNTRNTRNT_105;
  }
}
class B : A
{
  protected override int this[int a] {
    get {
      System.Int32 RNTRNTRNT_106 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(343);
      return RNTRNTRNT_106;
    }
  }
  public int M()
  {
    System.Int32 RNTRNTRNT_107 = this[0];
    IACSharpSensor.IACSharpSensor.SensorReached(344);
    return RNTRNTRNT_107;
  }
}
class X
{
  int v1, v2;
  int this[int a] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(345);
      if (a == 0) {
        System.Int32 RNTRNTRNT_108 = v1;
        IACSharpSensor.IACSharpSensor.SensorReached(346);
        return RNTRNTRNT_108;
      } else {
        System.Int32 RNTRNTRNT_109 = v2;
        IACSharpSensor.IACSharpSensor.SensorReached(347);
        return RNTRNTRNT_109;
      }
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(348);
      if (a == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(349);
        v1 = value;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(350);
        v2 = value;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(351);
    }
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(352);
    X x = new X();
    x[0] = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(353);
    if (x.v1 != 1) {
      System.Int32 RNTRNTRNT_110 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(354);
      return RNTRNTRNT_110;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(355);
    if (x[0] != 1) {
      System.Int32 RNTRNTRNT_111 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(356);
      return RNTRNTRNT_111;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(357);
    B bb = new B();
    IACSharpSensor.IACSharpSensor.SensorReached(358);
    if (bb.EmulateIndexer(10) != 10) {
      System.Int32 RNTRNTRNT_112 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(359);
      return RNTRNTRNT_112;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(360);
    Hashtable a = new Hashtable();
    int b = (int)(a[0] = 1);
    IACSharpSensor.IACSharpSensor.SensorReached(361);
    if (b != 1) {
      System.Int32 RNTRNTRNT_113 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(362);
      return RNTRNTRNT_113;
    }
    System.Int32 RNTRNTRNT_114 = new B().M();
    IACSharpSensor.IACSharpSensor.SensorReached(363);
    return RNTRNTRNT_114;
  }
}
