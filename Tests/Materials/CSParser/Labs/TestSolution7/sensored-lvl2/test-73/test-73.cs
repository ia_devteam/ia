public abstract class Abstract
{
  public abstract int A();
}
public class Concrete : Abstract
{
  public override int A()
  {
    System.Int32 RNTRNTRNT_881 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1950);
    return RNTRNTRNT_881;
  }
}
class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1951);
    Concrete c = new Concrete();
    IACSharpSensor.IACSharpSensor.SensorReached(1952);
    if (c.A() != 1) {
      System.Int32 RNTRNTRNT_882 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1953);
      return RNTRNTRNT_882;
    }
    System.Int32 RNTRNTRNT_883 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1954);
    return RNTRNTRNT_883;
  }
}
