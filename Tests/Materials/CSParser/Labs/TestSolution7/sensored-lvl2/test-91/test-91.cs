using System;
using System.Reflection;
abstract class Abstract
{
}
class Plain
{
}
class Test
{
  protected static internal void MyProtectedInternal()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2112);
  }
  static internal void MyInternal()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2113);
  }
  public static void MyPublic()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2114);
  }
  static void MyPrivate()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2115);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2116);
    Type myself = typeof(Test);
    BindingFlags bf = BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public;
    MethodAttributes mpia;
    MethodInfo mpi;
    mpi = myself.GetMethod("MyProtectedInternal", bf);
    mpia = mpi.Attributes & MethodAttributes.MemberAccessMask;
    IACSharpSensor.IACSharpSensor.SensorReached(2117);
    if (mpia != MethodAttributes.FamORAssem) {
      System.Int32 RNTRNTRNT_948 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(2118);
      return RNTRNTRNT_948;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2119);
    mpi = myself.GetMethod("MyInternal", bf);
    mpia = mpi.Attributes & MethodAttributes.MemberAccessMask;
    IACSharpSensor.IACSharpSensor.SensorReached(2120);
    if (mpia != MethodAttributes.Assembly) {
      System.Int32 RNTRNTRNT_949 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(2121);
      return RNTRNTRNT_949;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2122);
    mpi = myself.GetMethod("MyPublic", bf);
    mpia = mpi.Attributes & MethodAttributes.MemberAccessMask;
    IACSharpSensor.IACSharpSensor.SensorReached(2123);
    if (mpia != MethodAttributes.Public) {
      System.Int32 RNTRNTRNT_950 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(2124);
      return RNTRNTRNT_950;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2125);
    mpi = myself.GetMethod("MyPrivate", bf);
    mpia = mpi.Attributes & MethodAttributes.MemberAccessMask;
    IACSharpSensor.IACSharpSensor.SensorReached(2126);
    if (mpia != MethodAttributes.Private) {
      System.Int32 RNTRNTRNT_951 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(2127);
      return RNTRNTRNT_951;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2128);
    ConstructorInfo ci = typeof(Abstract).GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[0], new ParameterModifier[0]);
    IACSharpSensor.IACSharpSensor.SensorReached(2129);
    if (!ci.IsFamily) {
      System.Int32 RNTRNTRNT_952 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(2130);
      return RNTRNTRNT_952;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2131);
    ci = typeof(Plain).GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[0], new ParameterModifier[0]);
    IACSharpSensor.IACSharpSensor.SensorReached(2132);
    if (!ci.IsPublic) {
      System.Int32 RNTRNTRNT_953 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(2133);
      return RNTRNTRNT_953;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2134);
    Console.WriteLine("All tests pass");
    System.Int32 RNTRNTRNT_954 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(2135);
    return RNTRNTRNT_954;
  }
}
