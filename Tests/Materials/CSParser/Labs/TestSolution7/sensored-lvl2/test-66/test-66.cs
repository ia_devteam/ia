using System;
struct A
{
  public int a;
}
class Y
{
  public object a;
}
class X
{
  static A[] a_single = new A[10];
  static A[,] a_double = new A[10, 10];
  static Y[] o_single = new Y[10];
  static Y[,] o_double = new Y[10, 10];
  static void FillOne()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1784);
    a_single[0].a = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1785);
  }
  static void FillSingle()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1786);
    int i;
    IACSharpSensor.IACSharpSensor.SensorReached(1787);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(1788);
      a_single[i].a = i + 1;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1789);
  }
  static void FillDouble()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1790);
    int i, j;
    IACSharpSensor.IACSharpSensor.SensorReached(1791);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(1792);
      for (j = 0; j < 10; j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1793);
        a_double[i, j].a = i * j;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1794);
  }
  static void FillObject()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1795);
    int i;
    IACSharpSensor.IACSharpSensor.SensorReached(1796);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(1797);
      o_single[i] = new Y();
      o_single[i].a = (i + 1);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1798);
  }
  static void FillDoubleObject()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1799);
    int i, j;
    IACSharpSensor.IACSharpSensor.SensorReached(1800);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(1801);
      for (j = 0; j < 10; j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1802);
        o_double[i, j] = new Y();
        o_double[i, j].a = i * j;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1803);
  }
  static int TestSingle()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1804);
    int i;
    IACSharpSensor.IACSharpSensor.SensorReached(1805);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(1806);
      if (a_single[i].a != i + 1) {
        System.Int32 RNTRNTRNT_822 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1807);
        return RNTRNTRNT_822;
      }
    }
    System.Int32 RNTRNTRNT_823 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1808);
    return RNTRNTRNT_823;
  }
  static int TestDouble()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1809);
    int i, j;
    IACSharpSensor.IACSharpSensor.SensorReached(1810);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(1811);
      for (j = 0; j < 10; j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1812);
        if (a_double[i, j].a != (i * j)) {
          System.Int32 RNTRNTRNT_824 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(1813);
          return RNTRNTRNT_824;
        }
      }
    }
    System.Int32 RNTRNTRNT_825 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1814);
    return RNTRNTRNT_825;
  }
  static int TestObjectSingle()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1815);
    int i;
    IACSharpSensor.IACSharpSensor.SensorReached(1816);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(1817);
      if ((int)(o_single[i].a) != i + 1) {
        System.Int32 RNTRNTRNT_826 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1818);
        return RNTRNTRNT_826;
      }
    }
    System.Int32 RNTRNTRNT_827 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1819);
    return RNTRNTRNT_827;
  }
  static int TestObjectDouble()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1820);
    int i, j;
    IACSharpSensor.IACSharpSensor.SensorReached(1821);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(1822);
      for (j = 0; j < 10; j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(1823);
        if (((int)o_double[i, j].a) != (i * j)) {
          System.Int32 RNTRNTRNT_828 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(1824);
          return RNTRNTRNT_828;
        }
      }
    }
    System.Int32 RNTRNTRNT_829 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1825);
    return RNTRNTRNT_829;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1826);
    FillSingle();
    FillDouble();
    FillObject();
    FillDoubleObject();
    IACSharpSensor.IACSharpSensor.SensorReached(1827);
    if (TestSingle() != 0) {
      System.Int32 RNTRNTRNT_830 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1828);
      return RNTRNTRNT_830;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1829);
    if (TestDouble() != 0) {
      System.Int32 RNTRNTRNT_831 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1830);
      return RNTRNTRNT_831;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1831);
    if (TestObjectSingle() != 0) {
      System.Int32 RNTRNTRNT_832 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1832);
      return RNTRNTRNT_832;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1833);
    if (TestObjectDouble() != 0) {
      System.Int32 RNTRNTRNT_833 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1834);
      return RNTRNTRNT_833;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1835);
    Console.WriteLine("test passes");
    System.Int32 RNTRNTRNT_834 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1836);
    return RNTRNTRNT_834;
  }
}
