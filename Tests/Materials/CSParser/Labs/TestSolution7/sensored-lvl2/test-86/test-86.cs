using System;
namespace T
{
  public class T
  {
    static int method1(Type t, int val)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2057);
      Console.WriteLine("You passed in " + val);
      System.Int32 RNTRNTRNT_923 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(2058);
      return RNTRNTRNT_923;
    }
    static int method1(Type t, Type[] types)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2059);
      Console.WriteLine("Wrong method called !");
      System.Int32 RNTRNTRNT_924 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(2060);
      return RNTRNTRNT_924;
    }
    static int method2(Type t, int val)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2061);
      Console.WriteLine("MEthod2 : " + val);
      System.Int32 RNTRNTRNT_925 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(2062);
      return RNTRNTRNT_925;
    }
    static int method2(Type t, Type[] types)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2063);
      Console.WriteLine("Correct one this time!");
      System.Int32 RNTRNTRNT_926 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(2064);
      return RNTRNTRNT_926;
    }
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(2065);
      int i = method1(null, 1);
      IACSharpSensor.IACSharpSensor.SensorReached(2066);
      if (i != 1) {
        System.Int32 RNTRNTRNT_927 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(2067);
        return RNTRNTRNT_927;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(2068);
      i = method2(null, null);
      IACSharpSensor.IACSharpSensor.SensorReached(2069);
      if (i != 4) {
        System.Int32 RNTRNTRNT_928 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(2070);
        return RNTRNTRNT_928;
      }
      System.Int32 RNTRNTRNT_929 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(2071);
      return RNTRNTRNT_929;
    }
  }
}
