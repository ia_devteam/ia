using System;
public interface IVehicle
{
  int Start();
  int Stop();
  int Turn();
}
public class Base : IVehicle
{
  int IVehicle.Start()
  {
    System.Int32 RNTRNTRNT_957 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(2141);
    return RNTRNTRNT_957;
  }
  public int Stop()
  {
    System.Int32 RNTRNTRNT_958 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(2142);
    return RNTRNTRNT_958;
  }
  public virtual int Turn()
  {
    System.Int32 RNTRNTRNT_959 = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(2143);
    return RNTRNTRNT_959;
  }
}
public class Derived1 : Base
{
  public override int Turn()
  {
    System.Int32 RNTRNTRNT_960 = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(2144);
    return RNTRNTRNT_960;
  }
}
public class Derived2 : Base, IVehicle
{
  public new int Stop()
  {
    System.Int32 RNTRNTRNT_961 = 6;
    IACSharpSensor.IACSharpSensor.SensorReached(2145);
    return RNTRNTRNT_961;
  }
  int IVehicle.Start()
  {
    System.Int32 RNTRNTRNT_962 = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(2146);
    return RNTRNTRNT_962;
  }
  int IVehicle.Turn()
  {
    System.Int32 RNTRNTRNT_963 = 7;
    IACSharpSensor.IACSharpSensor.SensorReached(2147);
    return RNTRNTRNT_963;
  }
  public override int Turn()
  {
    System.Int32 RNTRNTRNT_964 = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(2148);
    return RNTRNTRNT_964;
  }
}
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2149);
    Derived1 d1 = new Derived1();
    Derived2 d2 = new Derived2();
    Base b1 = d1;
    Base b2 = d2;
    IACSharpSensor.IACSharpSensor.SensorReached(2150);
    if (d1.Turn() != 4) {
      System.Int32 RNTRNTRNT_965 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(2151);
      return RNTRNTRNT_965;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2152);
    if (((IVehicle)d1).Turn() != 4) {
      System.Int32 RNTRNTRNT_966 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(2153);
      return RNTRNTRNT_966;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2154);
    if (((IVehicle)d2).Turn() != 7) {
      System.Int32 RNTRNTRNT_967 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(2155);
      return RNTRNTRNT_967;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2156);
    if (b2.Turn() != 8) {
      System.Int32 RNTRNTRNT_968 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(2157);
      return RNTRNTRNT_968;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2158);
    if (((IVehicle)b2).Turn() != 7) {
      System.Int32 RNTRNTRNT_969 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(2159);
      return RNTRNTRNT_969;
    }
    System.Int32 RNTRNTRNT_970 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(2160);
    return RNTRNTRNT_970;
  }
}
