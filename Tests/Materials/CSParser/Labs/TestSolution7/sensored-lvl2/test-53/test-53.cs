using System;
using System.IO;
class MyDispose : IDisposable
{
  public bool disposed;
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1590);
    disposed = true;
    IACSharpSensor.IACSharpSensor.SensorReached(1591);
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1592);
    MyDispose copy_a, copy_b, copy_c;
    IACSharpSensor.IACSharpSensor.SensorReached(1593);
    using (MyDispose a = new MyDispose(), b = new MyDispose()) {
      IACSharpSensor.IACSharpSensor.SensorReached(1594);
      copy_a = a;
      copy_b = b;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1595);
    if (!copy_a.disposed) {
      System.Int32 RNTRNTRNT_756 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1596);
      return RNTRNTRNT_756;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1597);
    if (!copy_b.disposed) {
      System.Int32 RNTRNTRNT_757 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1598);
      return RNTRNTRNT_757;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1599);
    Console.WriteLine("Nested using clause disposed");
    copy_c = null;
    IACSharpSensor.IACSharpSensor.SensorReached(1600);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(1601);
      using (MyDispose c = new MyDispose()) {
        IACSharpSensor.IACSharpSensor.SensorReached(1602);
        copy_c = c;
        IACSharpSensor.IACSharpSensor.SensorReached(1603);
        throw new Exception();
      }
    } catch {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1604);
    if (!copy_c.disposed) {
      System.Int32 RNTRNTRNT_758 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1605);
      return RNTRNTRNT_758;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(1606);
      Console.WriteLine("Disposal on finally block works");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1607);
    using (MyDispose d = null) {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1608);
    Console.WriteLine("Null test passed");
    MyDispose bb = new MyDispose();
    IACSharpSensor.IACSharpSensor.SensorReached(1609);
    using (bb) {
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1610);
    if (bb.disposed == false) {
      System.Int32 RNTRNTRNT_759 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(1611);
      return RNTRNTRNT_759;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1612);
    Console.WriteLine("All tests pass");
    System.Int32 RNTRNTRNT_760 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1613);
    return RNTRNTRNT_760;
  }
}
