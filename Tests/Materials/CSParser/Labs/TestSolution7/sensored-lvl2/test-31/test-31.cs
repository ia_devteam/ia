using System;
class Base
{
  public int which;
  public virtual void A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(425);
    which = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(426);
  }
}
class Derived : Base
{
  public virtual void A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(427);
    which = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(428);
  }
}
class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(429);
    Derived d = new Derived();
    d.A();
    IACSharpSensor.IACSharpSensor.SensorReached(430);
    if (d.which == 1) {
      System.Int32 RNTRNTRNT_139 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(431);
      return RNTRNTRNT_139;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(432);
    Console.WriteLine("Test passes");
    System.Int32 RNTRNTRNT_140 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(433);
    return RNTRNTRNT_140;
  }
}
