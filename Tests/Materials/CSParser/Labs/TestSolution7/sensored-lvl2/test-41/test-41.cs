using System;
class X
{
  static void A(ref int a, ref uint b, ref sbyte c, ref byte d, ref long e, ref ulong f, ref short g, ref ushort h, ref char i, ref X x,
  ref float j, ref double k)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(664);
    if (a == 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(665);
      a = 2;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(666);
    if (b == 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(667);
      b = 2;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(668);
    if (c == 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(669);
      c = 2;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(670);
    if (d == 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(671);
      d = 2;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(672);
    if (e == 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(673);
      e = 2;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(674);
    if (f == 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(675);
      f = 2;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(676);
    if (g == 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(677);
      g = 2;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(678);
    if (h == 1) {
      IACSharpSensor.IACSharpSensor.SensorReached(679);
      h = 2;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(680);
    if (i == 'a') {
      IACSharpSensor.IACSharpSensor.SensorReached(681);
      i = 'b';
    }
    IACSharpSensor.IACSharpSensor.SensorReached(682);
    if (x == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(683);
      x = new X();
    }
    IACSharpSensor.IACSharpSensor.SensorReached(684);
    if (j == 1.0) {
      IACSharpSensor.IACSharpSensor.SensorReached(685);
      j = 2.0f;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(686);
    if (k == 1.0) {
      IACSharpSensor.IACSharpSensor.SensorReached(687);
      k = 2.0;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(688);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(689);
    int a = 1;
    uint b = 1;
    sbyte c = 1;
    byte d = 1;
    long e = 1;
    ulong f = 1;
    short g = 1;
    ushort h = 1;
    char i = 'a';
    float j = 1.0f;
    double k = 1.0;
    X x = null;
    A(ref a, ref b, ref c, ref d, ref e, ref f, ref g, ref h, ref i, ref x,
    ref j, ref k);
    IACSharpSensor.IACSharpSensor.SensorReached(690);
    if (a != 2) {
      System.Int32 RNTRNTRNT_207 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(691);
      return RNTRNTRNT_207;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(692);
    if (b != 2) {
      System.Int32 RNTRNTRNT_208 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(693);
      return RNTRNTRNT_208;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(694);
    if (c != 2) {
      System.Int32 RNTRNTRNT_209 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(695);
      return RNTRNTRNT_209;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(696);
    if (d != 2) {
      System.Int32 RNTRNTRNT_210 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(697);
      return RNTRNTRNT_210;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(698);
    if (e != 2) {
      System.Int32 RNTRNTRNT_211 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(699);
      return RNTRNTRNT_211;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(700);
    if (f != 2) {
      System.Int32 RNTRNTRNT_212 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(701);
      return RNTRNTRNT_212;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(702);
    if (g != 2) {
      System.Int32 RNTRNTRNT_213 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(703);
      return RNTRNTRNT_213;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(704);
    if (h != 2) {
      System.Int32 RNTRNTRNT_214 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(705);
      return RNTRNTRNT_214;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(706);
    if (i != 'b') {
      System.Int32 RNTRNTRNT_215 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(707);
      return RNTRNTRNT_215;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(708);
    if (j != 2.0) {
      System.Int32 RNTRNTRNT_216 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(709);
      return RNTRNTRNT_216;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(710);
    if (k != 2.0) {
      System.Int32 RNTRNTRNT_217 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(711);
      return RNTRNTRNT_217;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(712);
    if (x == null) {
      System.Int32 RNTRNTRNT_218 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(713);
      return RNTRNTRNT_218;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(714);
    Console.WriteLine("Test passed");
    System.Int32 RNTRNTRNT_219 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(715);
    return RNTRNTRNT_219;
  }
}
