using System;
public delegate void EventHandler(int i, int j);
public class Button
{
  private EventHandler click;
  public event EventHandler Click {
    add { click += value; }
    remove { click -= value; }
  }
  public void OnClick(int i, int j)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1670);
    if (click == null) {
      IACSharpSensor.IACSharpSensor.SensorReached(1671);
      Console.WriteLine("Nothing to click!");
      IACSharpSensor.IACSharpSensor.SensorReached(1672);
      return;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1673);
    click(i, j);
    IACSharpSensor.IACSharpSensor.SensorReached(1674);
  }
  public void Reset()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1675);
    click = null;
    IACSharpSensor.IACSharpSensor.SensorReached(1676);
  }
}
public class Blah
{
  Button Button1 = new Button();
  public void Connect()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1677);
    Button1.Click += new EventHandler(Button1_Click);
    Button1.Click += new EventHandler(Foo_Click);
    Button1.Click += null;
    IACSharpSensor.IACSharpSensor.SensorReached(1678);
  }
  public void Button1_Click(int i, int j)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1679);
    Console.WriteLine("Button1 was clicked !");
    Console.WriteLine("Answer : " + (i + j));
    IACSharpSensor.IACSharpSensor.SensorReached(1680);
  }
  public void Foo_Click(int i, int j)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1681);
    Console.WriteLine("Foo was clicked !");
    Console.WriteLine("Answer : " + (i + j));
    IACSharpSensor.IACSharpSensor.SensorReached(1682);
  }
  public void Disconnect()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1683);
    Console.WriteLine("Disconnecting Button1's handler ...");
    Button1.Click -= new EventHandler(Button1_Click);
    IACSharpSensor.IACSharpSensor.SensorReached(1684);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1685);
    Blah b = new Blah();
    b.Connect();
    b.Button1.OnClick(2, 3);
    b.Disconnect();
    Console.WriteLine("Now calling OnClick again");
    b.Button1.OnClick(3, 7);
    Console.WriteLine("Events test passes");
    System.Int32 RNTRNTRNT_782 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1686);
    return RNTRNTRNT_782;
  }
}
