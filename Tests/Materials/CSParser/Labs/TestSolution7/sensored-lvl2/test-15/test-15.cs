using System;
interface Iface
{
  int A();
}
class Implementor : Iface
{
  public int A()
  {
    System.Int32 RNTRNTRNT_25 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(98);
    return RNTRNTRNT_25;
  }
}
struct StructImplementor : Iface
{
  public int A()
  {
    System.Int32 RNTRNTRNT_26 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(99);
    return RNTRNTRNT_26;
  }
}
class Run
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(100);
    Iface iface;
    Implementor i = new Implementor();
    iface = i;
    IACSharpSensor.IACSharpSensor.SensorReached(101);
    if (iface.A() != 1) {
      System.Int32 RNTRNTRNT_27 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(102);
      return RNTRNTRNT_27;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(103);
    StructImplementor s = new StructImplementor();
    Iface xiface = (Iface)s;
    IACSharpSensor.IACSharpSensor.SensorReached(104);
    if (xiface.A() != 2) {
      System.Int32 RNTRNTRNT_28 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(105);
      return RNTRNTRNT_28;
    }
    System.Int32 RNTRNTRNT_29 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(106);
    return RNTRNTRNT_29;
  }
}
