using System;
using System.Reflection;
public class Blah
{
  public static int Main()
  {
    unsafe {
      IACSharpSensor.IACSharpSensor.SensorReached(1687);
      int* i;
      int foo = 10;
      void* bar;
      i = &foo;
      bar = i;
      Console.WriteLine("Address : {0}", (int)i);
    }
    System.Int32 RNTRNTRNT_783 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1688);
    return RNTRNTRNT_783;
  }
}
