using System;
class X
{
  static void m(int[] a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(536);
    a[0] = 0xdead;
    IACSharpSensor.IACSharpSensor.SensorReached(537);
  }
  static int test_int_single_dim()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(538);
    int[] a = new int[10];
    int i;
    IACSharpSensor.IACSharpSensor.SensorReached(539);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(540);
      a[i] = i;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(541);
    m(a);
    IACSharpSensor.IACSharpSensor.SensorReached(542);
    if (a[0] != 0xdead) {
      System.Int32 RNTRNTRNT_162 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(543);
      return RNTRNTRNT_162;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(544);
    a[0] = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(545);
    for (i = 9; i >= 0; i--) {
      IACSharpSensor.IACSharpSensor.SensorReached(546);
      if (a[i] != i) {
        System.Int32 RNTRNTRNT_163 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(547);
        return RNTRNTRNT_163;
      }
    }
    System.Int32 RNTRNTRNT_164 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(548);
    return RNTRNTRNT_164;
  }
  static int simple_test_double_dim()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(549);
    int[,] b = new int[10, 10];
    b[0, 0] = 1;
    b[4, 4] = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(550);
    if (b[0, 0] != b[4, 4]) {
      System.Int32 RNTRNTRNT_165 = 20;
      IACSharpSensor.IACSharpSensor.SensorReached(551);
      return RNTRNTRNT_165;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(552);
    if (b[1, 1] != b[5, 5]) {
      System.Int32 RNTRNTRNT_166 = 21;
      IACSharpSensor.IACSharpSensor.SensorReached(553);
      return RNTRNTRNT_166;
    }
    System.Int32 RNTRNTRNT_167 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(554);
    return RNTRNTRNT_167;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(555);
    int v;
    Console.WriteLine("hello");
    System.Int32 RNTRNTRNT_168 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(556);
    return RNTRNTRNT_168;
  }
}
