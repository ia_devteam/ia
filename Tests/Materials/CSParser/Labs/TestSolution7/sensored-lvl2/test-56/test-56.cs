using System;
interface I
{
  int P { get; set; }
}
abstract class A : I
{
  public int p;
  public int q;
  public int P {
    get {
      System.Int32 RNTRNTRNT_765 = p;
      IACSharpSensor.IACSharpSensor.SensorReached(1628);
      return RNTRNTRNT_765;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1629);
      p = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1630);
    }
  }
  public abstract int Q { get; set; }
  public int r;
  public virtual int R {
    get {
      System.Int32 RNTRNTRNT_766 = r;
      IACSharpSensor.IACSharpSensor.SensorReached(1631);
      return RNTRNTRNT_766;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1632);
      r = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1633);
    }
  }
}
class B : A
{
  public int bp;
  public new int P {
    get {
      System.Int32 RNTRNTRNT_767 = bp;
      IACSharpSensor.IACSharpSensor.SensorReached(1634);
      return RNTRNTRNT_767;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1635);
      bp = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1636);
    }
  }
  public override int Q {
    get {
      System.Int32 RNTRNTRNT_768 = q;
      IACSharpSensor.IACSharpSensor.SensorReached(1637);
      return RNTRNTRNT_768;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1638);
      q = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1639);
    }
  }
}
class C : A
{
  public override int Q {
    get {
      System.Int32 RNTRNTRNT_769 = q;
      IACSharpSensor.IACSharpSensor.SensorReached(1640);
      return RNTRNTRNT_769;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1641);
      q = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1642);
    }
  }
  public int rr;
  public override int R {
    get {
      System.Int32 RNTRNTRNT_770 = rr;
      IACSharpSensor.IACSharpSensor.SensorReached(1643);
      return RNTRNTRNT_770;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1644);
      rr = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1645);
    }
  }
}
class M
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1646);
    B b = new B();
    b.P = 1;
    b.R = 10;
    b.Q = 20;
    IACSharpSensor.IACSharpSensor.SensorReached(1647);
    if (b.P != 1) {
      System.Int32 RNTRNTRNT_771 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1648);
      return RNTRNTRNT_771;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1649);
    if (b.bp != 1) {
      System.Int32 RNTRNTRNT_772 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1650);
      return RNTRNTRNT_772;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1651);
    if (b.R != 10) {
      System.Int32 RNTRNTRNT_773 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1652);
      return RNTRNTRNT_773;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1653);
    if (b.r != 10) {
      System.Int32 RNTRNTRNT_774 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1654);
      return RNTRNTRNT_774;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1655);
    if (b.Q != 20) {
      System.Int32 RNTRNTRNT_775 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(1656);
      return RNTRNTRNT_775;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1657);
    if (b.q != 20) {
      System.Int32 RNTRNTRNT_776 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(1658);
      return RNTRNTRNT_776;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1659);
    C c = new C();
    c.R = 10;
    c.Q = 20;
    c.P = 30;
    IACSharpSensor.IACSharpSensor.SensorReached(1660);
    if (c.R != 10) {
      System.Int32 RNTRNTRNT_777 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(1661);
      return RNTRNTRNT_777;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1662);
    if (c.rr != 10) {
      System.Int32 RNTRNTRNT_778 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(1663);
      return RNTRNTRNT_778;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1664);
    if (c.P != 30) {
      System.Int32 RNTRNTRNT_779 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(1665);
      return RNTRNTRNT_779;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1666);
    if (c.p != 30) {
      System.Int32 RNTRNTRNT_780 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(1667);
      return RNTRNTRNT_780;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1668);
    Console.WriteLine("Test passes");
    System.Int32 RNTRNTRNT_781 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1669);
    return RNTRNTRNT_781;
  }
}
