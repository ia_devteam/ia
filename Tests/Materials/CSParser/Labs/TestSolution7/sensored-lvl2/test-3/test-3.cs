public class TestIntOps
{
  public static sbyte sbyte_add(sbyte a, sbyte b)
  {
    System.SByte RNTRNTRNT_118 = (sbyte)(a + b);
    IACSharpSensor.IACSharpSensor.SensorReached(374);
    return RNTRNTRNT_118;
  }
  public static short short_add(short a, short b)
  {
    System.Int16 RNTRNTRNT_119 = (short)(a + b);
    IACSharpSensor.IACSharpSensor.SensorReached(375);
    return RNTRNTRNT_119;
  }
  public static double double_add(double a, double b)
  {
    System.Double RNTRNTRNT_120 = a + b;
    IACSharpSensor.IACSharpSensor.SensorReached(376);
    return RNTRNTRNT_120;
  }
  public static int int_add(int a, int b)
  {
    System.Int32 RNTRNTRNT_121 = a + b;
    IACSharpSensor.IACSharpSensor.SensorReached(377);
    return RNTRNTRNT_121;
  }
  public static int int_sub(int a, int b)
  {
    System.Int32 RNTRNTRNT_122 = a - b;
    IACSharpSensor.IACSharpSensor.SensorReached(378);
    return RNTRNTRNT_122;
  }
  public static int int_mul(int a, int b)
  {
    System.Int32 RNTRNTRNT_123 = a * b;
    IACSharpSensor.IACSharpSensor.SensorReached(379);
    return RNTRNTRNT_123;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(380);
    int num = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(381);
    if (int_add(1, 1) != 2) {
      System.Int32 RNTRNTRNT_124 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(382);
      return RNTRNTRNT_124;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(383);
    num++;
    IACSharpSensor.IACSharpSensor.SensorReached(384);
    if (int_add(31, -1) != 30) {
      System.Int32 RNTRNTRNT_125 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(385);
      return RNTRNTRNT_125;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(386);
    num++;
    IACSharpSensor.IACSharpSensor.SensorReached(387);
    if (int_sub(31, -1) != 32) {
      System.Int32 RNTRNTRNT_126 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(388);
      return RNTRNTRNT_126;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(389);
    num++;
    IACSharpSensor.IACSharpSensor.SensorReached(390);
    if (int_mul(12, 12) != 144) {
      System.Int32 RNTRNTRNT_127 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(391);
      return RNTRNTRNT_127;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(392);
    num++;
    IACSharpSensor.IACSharpSensor.SensorReached(393);
    if (sbyte_add(1, 1) != 2) {
      System.Int32 RNTRNTRNT_128 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(394);
      return RNTRNTRNT_128;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(395);
    num++;
    IACSharpSensor.IACSharpSensor.SensorReached(396);
    if (sbyte_add(31, -1) != 30) {
      System.Int32 RNTRNTRNT_129 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(397);
      return RNTRNTRNT_129;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(398);
    num++;
    IACSharpSensor.IACSharpSensor.SensorReached(399);
    if (short_add(1, 1) != 2) {
      System.Int32 RNTRNTRNT_130 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(400);
      return RNTRNTRNT_130;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(401);
    num++;
    IACSharpSensor.IACSharpSensor.SensorReached(402);
    if (short_add(31, -1) != 30) {
      System.Int32 RNTRNTRNT_131 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(403);
      return RNTRNTRNT_131;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(404);
    num++;
    IACSharpSensor.IACSharpSensor.SensorReached(405);
    if (double_add(1.5, 1.5) != 3) {
      System.Int32 RNTRNTRNT_132 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(406);
      return RNTRNTRNT_132;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    num++;
    System.Int32 RNTRNTRNT_133 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(408);
    return RNTRNTRNT_133;
  }
}
