using System;
class Blah
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(122);
    Blah k = new Blah();
    float f = k;
    IACSharpSensor.IACSharpSensor.SensorReached(123);
    if (f == 2) {
      IACSharpSensor.IACSharpSensor.SensorReached(124);
      Console.WriteLine("Best implicit operator selected correctly");
      System.Int32 RNTRNTRNT_36 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(125);
      return RNTRNTRNT_36;
    }
    System.Int32 RNTRNTRNT_37 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(126);
    return RNTRNTRNT_37;
  }
  public static implicit operator byte(Blah i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(127);
    Console.WriteLine("Blah->byte");
    System.Byte RNTRNTRNT_38 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(128);
    return RNTRNTRNT_38;
  }
  public static implicit operator short(Blah i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(129);
    Console.WriteLine("Blah->short");
    System.Int16 RNTRNTRNT_39 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(130);
    return RNTRNTRNT_39;
  }
  public static implicit operator int(Blah i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(131);
    Console.WriteLine("Blah->int");
    System.Int32 RNTRNTRNT_40 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(132);
    return RNTRNTRNT_40;
  }
}
