class X
{
  static bool t = true;
  static bool f = false;
  static int j = 0;
  static void a()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(484);
    if (!t) {
      IACSharpSensor.IACSharpSensor.SensorReached(485);
      j = 1;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(486);
  }
  static void w(int x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(487);
    System.Console.WriteLine(" " + x);
    IACSharpSensor.IACSharpSensor.SensorReached(488);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(489);
    int ok = 0, error = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(490);
    if (!f) {
      IACSharpSensor.IACSharpSensor.SensorReached(491);
      ok = 1;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(492);
      error++;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(493);
    w(1);
    IACSharpSensor.IACSharpSensor.SensorReached(494);
    if (f) {
      IACSharpSensor.IACSharpSensor.SensorReached(495);
      error++;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(496);
      ok |= 2;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(497);
    w(2);
    IACSharpSensor.IACSharpSensor.SensorReached(498);
    if (t) {
      IACSharpSensor.IACSharpSensor.SensorReached(499);
      ok |= 4;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(500);
      error++;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(501);
    if (!t) {
      IACSharpSensor.IACSharpSensor.SensorReached(502);
      error++;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(503);
      ok |= 8;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(504);
    if (!(t && f == false)) {
      IACSharpSensor.IACSharpSensor.SensorReached(505);
      error++;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(506);
      ok |= 16;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(507);
    int i = 0;
    w(3);
    IACSharpSensor.IACSharpSensor.SensorReached(508);
    do {
      IACSharpSensor.IACSharpSensor.SensorReached(509);
      i++;
    } while (!(i > 5));
    IACSharpSensor.IACSharpSensor.SensorReached(510);
    if (i != 6) {
      IACSharpSensor.IACSharpSensor.SensorReached(511);
      error++;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(512);
      ok |= 32;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(513);
    w(100);
    System.Console.WriteLine("Value: " + t);
    IACSharpSensor.IACSharpSensor.SensorReached(514);
    do {
      IACSharpSensor.IACSharpSensor.SensorReached(515);
      i++;
    } while (!t);
    IACSharpSensor.IACSharpSensor.SensorReached(516);
    System.Console.WriteLine("Ok=" + ok + " Errors=" + error);
    System.Int32 RNTRNTRNT_156 = ((ok == 63) && (error == 0)) ? 0 : 1;
    IACSharpSensor.IACSharpSensor.SensorReached(517);
    return RNTRNTRNT_156;
  }
}
