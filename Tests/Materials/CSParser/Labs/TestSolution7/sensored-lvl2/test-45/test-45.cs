using System;
public class Blah
{
  private static int[] array = {
    0,
    1,
    2,
    3
  };
  private static int[,] bar = {
    {
      0,
      1
    },
    {
      4,
      5
    },
    {
      10,
      20
    }
  };
  static string[] names = {
    "Miguel",
    "Paolo",
    "Dietmar",
    "Dick",
    "Ravi"
  };
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(850);
    int[] i = new int[4] {
      0,
      1,
      2,
      3
    };
    short[,] j = new short[4, 2] {
      {
        0,
        1
      },
      {
        2,
        3
      },
      {
        4,
        5
      },
      {
        6,
        7
      }
    };
    ushort[] a = {
      4,
      5,
      6,
      7
    };
    long[,,] m = new long[2, 3, 2] {
      {
        {
          0,
          1
        },
        {
          2,
          3
        },
        {
          4,
          5
        }
      },
      {
        {
          6,
          7
        },
        {
          8,
          9
        },
        {
          10,
          11
        }
      }
    };
    int foo = 1;
    int[] k = new int[] {
      foo,
      foo + 1,
      foo + 4
    };
    int[,] boo = new int[,] {
      {
        foo,
        foo + 10
      },
      {
        foo + 3,
        foo + 10
      }
    };
    float[] f_array = new float[] {
      1.23f,
      4.5f,
      6.24f
    };
    double[] double_arr = new double[] {
      34.4567,
      90.1226,
      54.9823
    };
    char[] c_arr = {
      'A',
      'B',
      'C',
      'M',
      'R'
    };
    byte[] b_arr = {
      0,
      3,
      8,
      10,
      21
    };
    sbyte[] s_arr = {
      10,
      15,
      30,
      123
    };
    IACSharpSensor.IACSharpSensor.SensorReached(851);
    if (a[2] != 6) {
      System.Int32 RNTRNTRNT_265 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(852);
      return RNTRNTRNT_265;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(853);
    if (s_arr[3] != 123) {
      System.Int32 RNTRNTRNT_266 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(854);
      return RNTRNTRNT_266;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(855);
    if (i[2] != 2) {
      System.Int32 RNTRNTRNT_267 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(856);
      return RNTRNTRNT_267;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(857);
    if (j[1, 1] != 3) {
      System.Int32 RNTRNTRNT_268 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(858);
      return RNTRNTRNT_268;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(859);
    for (int t = 0; t < 4; ++t) {
      IACSharpSensor.IACSharpSensor.SensorReached(860);
      if (array[t] != t) {
        System.Int32 RNTRNTRNT_269 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(861);
        return RNTRNTRNT_269;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(862);
      if (a[t] != (t + 4)) {
        System.Int32 RNTRNTRNT_270 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(863);
        return RNTRNTRNT_270;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(864);
    if (bar[2, 1] != 20) {
      System.Int32 RNTRNTRNT_271 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(865);
      return RNTRNTRNT_271;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(866);
    if (k[2] != 5) {
      System.Int32 RNTRNTRNT_272 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(867);
      return RNTRNTRNT_272;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(868);
    if (m[1, 1, 1] != 9) {
      System.Int32 RNTRNTRNT_273 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(869);
      return RNTRNTRNT_273;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(870);
    if (boo[0, 1] != 11) {
      System.Int32 RNTRNTRNT_274 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(871);
      return RNTRNTRNT_274;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(872);
    if (f_array[0] != 1.23f) {
      System.Int32 RNTRNTRNT_275 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(873);
      return RNTRNTRNT_275;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(874);
    if (double_arr[1] != 90.1226) {
      System.Int32 RNTRNTRNT_276 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(875);
      return RNTRNTRNT_276;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(876);
    foreach (string s in names) {
      IACSharpSensor.IACSharpSensor.SensorReached(877);
      Console.WriteLine("Hello, " + s);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(878);
    if (names[0] != "Miguel") {
      System.Int32 RNTRNTRNT_277 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(879);
      return RNTRNTRNT_277;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(880);
    if (c_arr[4] != 'R') {
      System.Int32 RNTRNTRNT_278 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(881);
      return RNTRNTRNT_278;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(882);
    int count = 10;
    int[] x = new int[count];
    IACSharpSensor.IACSharpSensor.SensorReached(883);
    for (int idx = 0; idx < count; idx++) {
      IACSharpSensor.IACSharpSensor.SensorReached(884);
      x[idx] = idx + 1;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(885);
    for (int idx = count; idx > 0;) {
      IACSharpSensor.IACSharpSensor.SensorReached(886);
      idx--;
      IACSharpSensor.IACSharpSensor.SensorReached(887);
      if (x[idx] != idx + 1) {
        System.Int32 RNTRNTRNT_279 = 12;
        IACSharpSensor.IACSharpSensor.SensorReached(888);
        return RNTRNTRNT_279;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(889);
    IntPtr[] arr = { new System.IntPtr(1) };
    IACSharpSensor.IACSharpSensor.SensorReached(890);
    if (arr[0] != (IntPtr)1) {
      System.Int32 RNTRNTRNT_280 = 13;
      IACSharpSensor.IACSharpSensor.SensorReached(891);
      return RNTRNTRNT_280;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(892);
    IntPtr[] arr_i = { System.IntPtr.Zero };
    IACSharpSensor.IACSharpSensor.SensorReached(893);
    if (arr_i[0] != System.IntPtr.Zero) {
      System.Int32 RNTRNTRNT_281 = 14;
      IACSharpSensor.IACSharpSensor.SensorReached(894);
      return RNTRNTRNT_281;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(895);
    Console.WriteLine("Array initialization test okay.");
    System.Int32 RNTRNTRNT_282 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(896);
    return RNTRNTRNT_282;
  }
}
