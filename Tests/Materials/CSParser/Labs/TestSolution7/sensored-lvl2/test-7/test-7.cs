using System;
namespace Mine
{
  public class MyBoolean
  {
    public static implicit operator bool(MyBoolean x)
    {
      System.Boolean RNTRNTRNT_840 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1851);
      return RNTRNTRNT_840;
    }
  }
  public class MyTrueFalse
  {
    public static bool operator true(MyTrueFalse i)
    {
      System.Boolean RNTRNTRNT_841 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1852);
      return RNTRNTRNT_841;
    }
    public static bool operator false(MyTrueFalse i)
    {
      System.Boolean RNTRNTRNT_842 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(1853);
      return RNTRNTRNT_842;
    }
  }
  public class Blah
  {
    public int i;
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1854);
      Blah k, l;
      k = new Blah(2) + new Blah(3);
      IACSharpSensor.IACSharpSensor.SensorReached(1855);
      if (k.i != 5) {
        System.Int32 RNTRNTRNT_843 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1856);
        return RNTRNTRNT_843;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1857);
      k = ~new Blah(5);
      IACSharpSensor.IACSharpSensor.SensorReached(1858);
      if (k.i != -6) {
        System.Int32 RNTRNTRNT_844 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1859);
        return RNTRNTRNT_844;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1860);
      k = +new Blah(4);
      IACSharpSensor.IACSharpSensor.SensorReached(1861);
      if (k.i != 4) {
        System.Int32 RNTRNTRNT_845 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1862);
        return RNTRNTRNT_845;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1863);
      k = -new Blah(21);
      IACSharpSensor.IACSharpSensor.SensorReached(1864);
      if (k.i != -21) {
        System.Int32 RNTRNTRNT_846 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1865);
        return RNTRNTRNT_846;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1866);
      k = new Blah(22) - new Blah(21);
      IACSharpSensor.IACSharpSensor.SensorReached(1867);
      if (k.i != 1) {
        System.Int32 RNTRNTRNT_847 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1868);
        return RNTRNTRNT_847;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1869);
      if (!k) {
        IACSharpSensor.IACSharpSensor.SensorReached(1870);
        Console.WriteLine("! returned true");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1871);
      int number = k;
      IACSharpSensor.IACSharpSensor.SensorReached(1872);
      if (number != 1) {
        System.Int32 RNTRNTRNT_848 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1873);
        return RNTRNTRNT_848;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1874);
      k++;
      ++k;
      IACSharpSensor.IACSharpSensor.SensorReached(1875);
      if (k) {
        IACSharpSensor.IACSharpSensor.SensorReached(1876);
        Console.WriteLine("k is definitely true");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1877);
      k = new Blah(30);
      double f = (double)k;
      IACSharpSensor.IACSharpSensor.SensorReached(1878);
      if (f != 30.0) {
        System.Int32 RNTRNTRNT_849 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1879);
        return RNTRNTRNT_849;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1880);
      int i = new Blah(5) * new Blah(10);
      IACSharpSensor.IACSharpSensor.SensorReached(1881);
      if (i != 50) {
        System.Int32 RNTRNTRNT_850 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1882);
        return RNTRNTRNT_850;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1883);
      k = new Blah(50);
      l = new Blah(10);
      i = k / l;
      IACSharpSensor.IACSharpSensor.SensorReached(1884);
      if (i != 5) {
        System.Int32 RNTRNTRNT_851 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1885);
        return RNTRNTRNT_851;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1886);
      i = k % l;
      IACSharpSensor.IACSharpSensor.SensorReached(1887);
      if (i != 0) {
        System.Int32 RNTRNTRNT_852 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1888);
        return RNTRNTRNT_852;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1889);
      MyBoolean myb = new MyBoolean();
      IACSharpSensor.IACSharpSensor.SensorReached(1890);
      if (!myb) {
        System.Int32 RNTRNTRNT_853 = 10;
        IACSharpSensor.IACSharpSensor.SensorReached(1891);
        return RNTRNTRNT_853;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1892);
      MyTrueFalse mf = new MyTrueFalse();
      int x = mf ? 1 : 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1893);
      if (x != 1) {
        System.Int32 RNTRNTRNT_854 = 11;
        IACSharpSensor.IACSharpSensor.SensorReached(1894);
        return RNTRNTRNT_854;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(1895);
      Console.WriteLine("Test passed");
      System.Int32 RNTRNTRNT_855 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1896);
      return RNTRNTRNT_855;
    }
    public Blah(int v)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1897);
      i = v;
      IACSharpSensor.IACSharpSensor.SensorReached(1898);
    }
    public static Blah operator +(Blah i, Blah j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1899);
      Blah b = new Blah(i.i + j.i);
      Console.WriteLine("Overload binary + operator");
      Blah RNTRNTRNT_856 = b;
      IACSharpSensor.IACSharpSensor.SensorReached(1900);
      return RNTRNTRNT_856;
    }
    public static Blah operator +(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1901);
      Console.WriteLine("Overload unary + operator");
      Blah RNTRNTRNT_857 = new Blah(i.i);
      IACSharpSensor.IACSharpSensor.SensorReached(1902);
      return RNTRNTRNT_857;
    }
    public static Blah operator -(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1903);
      Console.WriteLine("Overloaded unary - operator");
      Blah RNTRNTRNT_858 = new Blah(-i.i);
      IACSharpSensor.IACSharpSensor.SensorReached(1904);
      return RNTRNTRNT_858;
    }
    public static Blah operator -(Blah i, Blah j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1905);
      Blah b = new Blah(i.i - j.i);
      Console.WriteLine("Overloaded binary - operator");
      Blah RNTRNTRNT_859 = b;
      IACSharpSensor.IACSharpSensor.SensorReached(1906);
      return RNTRNTRNT_859;
    }
    public static int operator *(Blah i, Blah j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1907);
      Console.WriteLine("Overloaded binary * operator");
      System.Int32 RNTRNTRNT_860 = i.i * j.i;
      IACSharpSensor.IACSharpSensor.SensorReached(1908);
      return RNTRNTRNT_860;
    }
    public static int operator /(Blah i, Blah j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1909);
      Console.WriteLine("Overloaded binary / operator");
      System.Int32 RNTRNTRNT_861 = i.i / j.i;
      IACSharpSensor.IACSharpSensor.SensorReached(1910);
      return RNTRNTRNT_861;
    }
    public static int operator %(Blah i, Blah j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1911);
      Console.WriteLine("Overloaded binary % operator");
      System.Int32 RNTRNTRNT_862 = i.i % j.i;
      IACSharpSensor.IACSharpSensor.SensorReached(1912);
      return RNTRNTRNT_862;
    }
    public static Blah operator ~(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1913);
      Console.WriteLine("Overloaded ~ operator");
      Blah RNTRNTRNT_863 = new Blah(~i.i);
      IACSharpSensor.IACSharpSensor.SensorReached(1914);
      return RNTRNTRNT_863;
    }
    public static bool operator !(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1915);
      Console.WriteLine("Overloaded ! operator");
      System.Boolean RNTRNTRNT_864 = (i.i == 1);
      IACSharpSensor.IACSharpSensor.SensorReached(1916);
      return RNTRNTRNT_864;
    }
    public static Blah operator ++(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1917);
      Blah b = new Blah(i.i + 1);
      Console.WriteLine("Incrementing i");
      Blah RNTRNTRNT_865 = b;
      IACSharpSensor.IACSharpSensor.SensorReached(1918);
      return RNTRNTRNT_865;
    }
    public static Blah operator --(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1919);
      Blah b = new Blah(i.i - 1);
      Console.WriteLine("Decrementing i");
      Blah RNTRNTRNT_866 = b;
      IACSharpSensor.IACSharpSensor.SensorReached(1920);
      return RNTRNTRNT_866;
    }
    public static bool operator true(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1921);
      Console.WriteLine("Overloaded true operator");
      System.Boolean RNTRNTRNT_867 = (i.i == 3);
      IACSharpSensor.IACSharpSensor.SensorReached(1922);
      return RNTRNTRNT_867;
    }
    public static bool operator false(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1923);
      Console.WriteLine("Overloaded false operator");
      System.Boolean RNTRNTRNT_868 = (i.i != 1);
      IACSharpSensor.IACSharpSensor.SensorReached(1924);
      return RNTRNTRNT_868;
    }
    public static implicit operator int(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1925);
      Console.WriteLine("Converting implicitly from Blah->int");
      System.Int32 RNTRNTRNT_869 = i.i;
      IACSharpSensor.IACSharpSensor.SensorReached(1926);
      return RNTRNTRNT_869;
    }
    public static explicit operator double(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1927);
      Console.WriteLine("Converting explicitly from Blah->double");
      System.Double RNTRNTRNT_870 = (double)i.i;
      IACSharpSensor.IACSharpSensor.SensorReached(1928);
      return RNTRNTRNT_870;
    }
  }
}
