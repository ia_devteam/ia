using System;
class X
{
  bool sbyte_selected;
  bool int_selected;
  void test(sbyte s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(618);
    sbyte_selected = true;
    IACSharpSensor.IACSharpSensor.SensorReached(619);
  }
  void test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(620);
    int_selected = true;
    IACSharpSensor.IACSharpSensor.SensorReached(621);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(622);
    X x = new X();
    x.test(1);
    IACSharpSensor.IACSharpSensor.SensorReached(623);
    if (x.sbyte_selected) {
      IACSharpSensor.IACSharpSensor.SensorReached(624);
      Console.WriteLine("FAILED: Sbyte selected on constant int argument");
      System.Int32 RNTRNTRNT_192 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(625);
      return RNTRNTRNT_192;
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(626);
      Console.WriteLine("OK: int selected for constant int");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(627);
    X y = new X();
    sbyte s = 10;
    y.test(s);
    IACSharpSensor.IACSharpSensor.SensorReached(628);
    if (y.sbyte_selected) {
      IACSharpSensor.IACSharpSensor.SensorReached(629);
      Console.WriteLine("OK: sbyte selected for sbyte argument");
    } else {
      IACSharpSensor.IACSharpSensor.SensorReached(630);
      Console.WriteLine("FAILED: sbyte not selected for sbyte argument");
      System.Int32 RNTRNTRNT_193 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(631);
      return RNTRNTRNT_193;
    }
    System.Int32 RNTRNTRNT_194 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(632);
    return RNTRNTRNT_194;
  }
}
