using System;
class X
{
  static int cast_int(object o)
  {
    System.Int32 RNTRNTRNT_283 = (int)o;
    IACSharpSensor.IACSharpSensor.SensorReached(897);
    return RNTRNTRNT_283;
  }
  static uint cast_uint(object o)
  {
    System.UInt32 RNTRNTRNT_284 = (uint)o;
    IACSharpSensor.IACSharpSensor.SensorReached(898);
    return RNTRNTRNT_284;
  }
  static short cast_short(object o)
  {
    System.Int16 RNTRNTRNT_285 = (short)o;
    IACSharpSensor.IACSharpSensor.SensorReached(899);
    return RNTRNTRNT_285;
  }
  static char cast_char(object o)
  {
    System.Char RNTRNTRNT_286 = (char)o;
    IACSharpSensor.IACSharpSensor.SensorReached(900);
    return RNTRNTRNT_286;
  }
  static ushort cast_ushort(object o)
  {
    System.UInt16 RNTRNTRNT_287 = (ushort)o;
    IACSharpSensor.IACSharpSensor.SensorReached(901);
    return RNTRNTRNT_287;
  }
  static byte cast_byte(object o)
  {
    System.Byte RNTRNTRNT_288 = (byte)o;
    IACSharpSensor.IACSharpSensor.SensorReached(902);
    return RNTRNTRNT_288;
  }
  static sbyte cast_sbyte(object o)
  {
    System.SByte RNTRNTRNT_289 = (sbyte)o;
    IACSharpSensor.IACSharpSensor.SensorReached(903);
    return RNTRNTRNT_289;
  }
  static long cast_long(object o)
  {
    System.Int64 RNTRNTRNT_290 = (long)o;
    IACSharpSensor.IACSharpSensor.SensorReached(904);
    return RNTRNTRNT_290;
  }
  static ulong cast_ulong(object o)
  {
    System.UInt64 RNTRNTRNT_291 = (ulong)o;
    IACSharpSensor.IACSharpSensor.SensorReached(905);
    return RNTRNTRNT_291;
  }
  static float cast_float(object o)
  {
    System.Single RNTRNTRNT_292 = (float)o;
    IACSharpSensor.IACSharpSensor.SensorReached(906);
    return RNTRNTRNT_292;
  }
  static double cast_double(object o)
  {
    System.Double RNTRNTRNT_293 = (double)o;
    IACSharpSensor.IACSharpSensor.SensorReached(907);
    return RNTRNTRNT_293;
  }
  static bool cast_bool(object o)
  {
    System.Boolean RNTRNTRNT_294 = (bool)o;
    IACSharpSensor.IACSharpSensor.SensorReached(908);
    return RNTRNTRNT_294;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(909);
    if (cast_int((object)-1) != -1) {
      System.Int32 RNTRNTRNT_295 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(910);
      return RNTRNTRNT_295;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(911);
    if (cast_int((object)1) != 1) {
      System.Int32 RNTRNTRNT_296 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(912);
      return RNTRNTRNT_296;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(913);
    if (cast_int((object)Int32.MaxValue) != Int32.MaxValue) {
      System.Int32 RNTRNTRNT_297 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(914);
      return RNTRNTRNT_297;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(915);
    if (cast_int((object)Int32.MinValue) != Int32.MinValue) {
      System.Int32 RNTRNTRNT_298 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(916);
      return RNTRNTRNT_298;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(917);
    if (cast_int((object)0) != 0) {
      System.Int32 RNTRNTRNT_299 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(918);
      return RNTRNTRNT_299;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(919);
    if (cast_uint((object)(uint)0) != 0) {
      System.Int32 RNTRNTRNT_300 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(920);
      return RNTRNTRNT_300;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(921);
    if (cast_uint((object)(uint)1) != 1) {
      System.Int32 RNTRNTRNT_301 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(922);
      return RNTRNTRNT_301;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(923);
    if (cast_uint((object)(uint)UInt32.MaxValue) != UInt32.MaxValue) {
      System.Int32 RNTRNTRNT_302 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(924);
      return RNTRNTRNT_302;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(925);
    if (cast_uint((object)(uint)UInt32.MinValue) != UInt32.MinValue) {
      System.Int32 RNTRNTRNT_303 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(926);
      return RNTRNTRNT_303;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(927);
    if (cast_ushort((object)(ushort)1) != 1) {
      System.Int32 RNTRNTRNT_304 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(928);
      return RNTRNTRNT_304;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(929);
    if (cast_ushort((object)(ushort)UInt16.MaxValue) != UInt16.MaxValue) {
      System.Int32 RNTRNTRNT_305 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(930);
      return RNTRNTRNT_305;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(931);
    if (cast_ushort((object)(ushort)UInt16.MinValue) != UInt16.MinValue) {
      System.Int32 RNTRNTRNT_306 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(932);
      return RNTRNTRNT_306;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(933);
    if (cast_ushort((object)(ushort)0) != 0) {
      System.Int32 RNTRNTRNT_307 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(934);
      return RNTRNTRNT_307;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(935);
    if (cast_short((object)(short)-1) != -1) {
      System.Int32 RNTRNTRNT_308 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(936);
      return RNTRNTRNT_308;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(937);
    if (cast_short((object)(short)1) != 1) {
      System.Int32 RNTRNTRNT_309 = 13;
      IACSharpSensor.IACSharpSensor.SensorReached(938);
      return RNTRNTRNT_309;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(939);
    if (cast_short((object)(short)Int16.MaxValue) != Int16.MaxValue) {
      System.Int32 RNTRNTRNT_310 = 14;
      IACSharpSensor.IACSharpSensor.SensorReached(940);
      return RNTRNTRNT_310;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(941);
    if (cast_short((object)(short)Int16.MinValue) != Int16.MinValue) {
      System.Int32 RNTRNTRNT_311 = 15;
      IACSharpSensor.IACSharpSensor.SensorReached(942);
      return RNTRNTRNT_311;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(943);
    if (cast_short((object)(short)0) != 0) {
      System.Int32 RNTRNTRNT_312 = 16;
      IACSharpSensor.IACSharpSensor.SensorReached(944);
      return RNTRNTRNT_312;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(945);
    if (cast_byte((object)(byte)1) != 1) {
      System.Int32 RNTRNTRNT_313 = 17;
      IACSharpSensor.IACSharpSensor.SensorReached(946);
      return RNTRNTRNT_313;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(947);
    if (cast_byte((object)(byte)Byte.MaxValue) != Byte.MaxValue) {
      System.Int32 RNTRNTRNT_314 = 18;
      IACSharpSensor.IACSharpSensor.SensorReached(948);
      return RNTRNTRNT_314;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(949);
    if (cast_byte((object)(byte)Byte.MinValue) != Byte.MinValue) {
      System.Int32 RNTRNTRNT_315 = 19;
      IACSharpSensor.IACSharpSensor.SensorReached(950);
      return RNTRNTRNT_315;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(951);
    if (cast_byte((object)(byte)0) != 0) {
      System.Int32 RNTRNTRNT_316 = 20;
      IACSharpSensor.IACSharpSensor.SensorReached(952);
      return RNTRNTRNT_316;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(953);
    if (cast_sbyte((object)(sbyte)-1) != -1) {
      System.Int32 RNTRNTRNT_317 = 21;
      IACSharpSensor.IACSharpSensor.SensorReached(954);
      return RNTRNTRNT_317;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(955);
    if (cast_sbyte((object)(sbyte)1) != 1) {
      System.Int32 RNTRNTRNT_318 = 22;
      IACSharpSensor.IACSharpSensor.SensorReached(956);
      return RNTRNTRNT_318;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(957);
    if (cast_sbyte((object)(sbyte)SByte.MaxValue) != SByte.MaxValue) {
      System.Int32 RNTRNTRNT_319 = 23;
      IACSharpSensor.IACSharpSensor.SensorReached(958);
      return RNTRNTRNT_319;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(959);
    if (cast_sbyte((object)(sbyte)SByte.MinValue) != SByte.MinValue) {
      System.Int32 RNTRNTRNT_320 = 24;
      IACSharpSensor.IACSharpSensor.SensorReached(960);
      return RNTRNTRNT_320;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(961);
    if (cast_sbyte((object)(sbyte)0) != 0) {
      System.Int32 RNTRNTRNT_321 = 25;
      IACSharpSensor.IACSharpSensor.SensorReached(962);
      return RNTRNTRNT_321;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(963);
    if (cast_long((object)(long)-1) != -1) {
      System.Int32 RNTRNTRNT_322 = 26;
      IACSharpSensor.IACSharpSensor.SensorReached(964);
      return RNTRNTRNT_322;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(965);
    if (cast_long((object)(long)1) != 1) {
      System.Int32 RNTRNTRNT_323 = 27;
      IACSharpSensor.IACSharpSensor.SensorReached(966);
      return RNTRNTRNT_323;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(967);
    if (cast_long((object)(long)Int64.MaxValue) != Int64.MaxValue) {
      System.Int32 RNTRNTRNT_324 = 28;
      IACSharpSensor.IACSharpSensor.SensorReached(968);
      return RNTRNTRNT_324;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(969);
    if (cast_long((object)(long)Int64.MinValue) != Int64.MinValue) {
      System.Int32 RNTRNTRNT_325 = 29;
      IACSharpSensor.IACSharpSensor.SensorReached(970);
      return RNTRNTRNT_325;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(971);
    if (cast_long((object)(long)0) != 0) {
      System.Int32 RNTRNTRNT_326 = 30;
      IACSharpSensor.IACSharpSensor.SensorReached(972);
      return RNTRNTRNT_326;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(973);
    if (cast_ulong((object)(ulong)0) != 0) {
      System.Int32 RNTRNTRNT_327 = 31;
      IACSharpSensor.IACSharpSensor.SensorReached(974);
      return RNTRNTRNT_327;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(975);
    if (cast_ulong((object)(ulong)1) != 1) {
      System.Int32 RNTRNTRNT_328 = 32;
      IACSharpSensor.IACSharpSensor.SensorReached(976);
      return RNTRNTRNT_328;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(977);
    if (cast_ulong((object)(ulong)UInt64.MaxValue) != UInt64.MaxValue) {
      System.Int32 RNTRNTRNT_329 = 33;
      IACSharpSensor.IACSharpSensor.SensorReached(978);
      return RNTRNTRNT_329;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(979);
    if (cast_ulong((object)(ulong)UInt64.MinValue) != UInt64.MinValue) {
      System.Int32 RNTRNTRNT_330 = 34;
      IACSharpSensor.IACSharpSensor.SensorReached(980);
      return RNTRNTRNT_330;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(981);
    if (cast_double((object)(double)-1) != -1) {
      System.Int32 RNTRNTRNT_331 = 35;
      IACSharpSensor.IACSharpSensor.SensorReached(982);
      return RNTRNTRNT_331;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(983);
    if (cast_double((object)(double)1) != 1) {
      System.Int32 RNTRNTRNT_332 = 36;
      IACSharpSensor.IACSharpSensor.SensorReached(984);
      return RNTRNTRNT_332;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(985);
    if (cast_double((object)(double)Double.MaxValue) != Double.MaxValue) {
      System.Int32 RNTRNTRNT_333 = 37;
      IACSharpSensor.IACSharpSensor.SensorReached(986);
      return RNTRNTRNT_333;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(987);
    if (cast_double((object)(double)Double.MinValue) != Double.MinValue) {
      System.Int32 RNTRNTRNT_334 = 38;
      IACSharpSensor.IACSharpSensor.SensorReached(988);
      return RNTRNTRNT_334;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(989);
    if (cast_double((object)(double)0) != 0) {
      System.Int32 RNTRNTRNT_335 = 39;
      IACSharpSensor.IACSharpSensor.SensorReached(990);
      return RNTRNTRNT_335;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(991);
    if (cast_float((object)(float)-1) != -1) {
      System.Int32 RNTRNTRNT_336 = 40;
      IACSharpSensor.IACSharpSensor.SensorReached(992);
      return RNTRNTRNT_336;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(993);
    if (cast_float((object)(float)1) != 1) {
      System.Int32 RNTRNTRNT_337 = 41;
      IACSharpSensor.IACSharpSensor.SensorReached(994);
      return RNTRNTRNT_337;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(995);
    if (cast_float((object)(float)Single.MaxValue) != Single.MaxValue) {
      System.Int32 RNTRNTRNT_338 = 42;
      IACSharpSensor.IACSharpSensor.SensorReached(996);
      return RNTRNTRNT_338;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(997);
    if (cast_float((object)(float)Single.MinValue) != Single.MinValue) {
      System.Int32 RNTRNTRNT_339 = 43;
      IACSharpSensor.IACSharpSensor.SensorReached(998);
      return RNTRNTRNT_339;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(999);
    if (cast_float((object)(float)0) != 0) {
      System.Int32 RNTRNTRNT_340 = 44;
      IACSharpSensor.IACSharpSensor.SensorReached(1000);
      return RNTRNTRNT_340;
    }
    System.Int32 RNTRNTRNT_341 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1001);
    return RNTRNTRNT_341;
  }
}
