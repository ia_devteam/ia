class X
{
  public virtual int A {
    get {
      System.Int32 RNTRNTRNT_871 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1929);
      return RNTRNTRNT_871;
    }
  }
  public virtual int B()
  {
    System.Int32 RNTRNTRNT_872 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1930);
    return RNTRNTRNT_872;
  }
}
class Y : X
{
  public override int A {
    get {
      System.Int32 RNTRNTRNT_873 = base.A + 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1931);
      return RNTRNTRNT_873;
    }
  }
  public override int B()
  {
    System.Int32 RNTRNTRNT_874 = base.B() + 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1932);
    return RNTRNTRNT_874;
  }
}
class Z
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1933);
    Y y = new Y();
    X x = new X();
    IACSharpSensor.IACSharpSensor.SensorReached(1934);
    if (y.B() != 2) {
      System.Int32 RNTRNTRNT_875 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1935);
      return RNTRNTRNT_875;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1936);
    if (y.A != 3) {
      System.Int32 RNTRNTRNT_876 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1937);
      return RNTRNTRNT_876;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1938);
    if (x.A != 1) {
      System.Int32 RNTRNTRNT_877 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1939);
      return RNTRNTRNT_877;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1940);
    if (x.B() != 1) {
      System.Int32 RNTRNTRNT_878 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1941);
      return RNTRNTRNT_878;
    }
    System.Int32 RNTRNTRNT_879 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1942);
    return RNTRNTRNT_879;
  }
}
