using System;
class X
{
  enum A : int
  {
    a = 1,
    b,
    c
  }
  enum Test : short
  {
    A = 1,
    B
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2172);
    int v = 1;
    object foo = (v + A.a);
    object foo2 = (1 + A.a);
    IACSharpSensor.IACSharpSensor.SensorReached(2173);
    if (foo.GetType().ToString() != "X+A") {
      IACSharpSensor.IACSharpSensor.SensorReached(2174);
      Console.WriteLine("Expression evaluator bug in E operator + (U x, E y)");
      System.Int32 RNTRNTRNT_976 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(2175);
      return RNTRNTRNT_976;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2176);
    if (foo2.GetType().ToString() != "X+A") {
      IACSharpSensor.IACSharpSensor.SensorReached(2177);
      Console.WriteLine("Constant folder bug in E operator + (U x, E y)");
      System.Int32 RNTRNTRNT_977 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(2178);
      return RNTRNTRNT_977;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2179);
    byte b = 1;
    short s = (short)(Test.A + b);
    IACSharpSensor.IACSharpSensor.SensorReached(2180);
    if (Test.A != Test.A) {
      System.Int32 RNTRNTRNT_978 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(2181);
      return RNTRNTRNT_978;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2182);
    if (Test.A == Test.B) {
      System.Int32 RNTRNTRNT_979 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(2183);
      return RNTRNTRNT_979;
    }
    System.Int32 RNTRNTRNT_980 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(2184);
    return RNTRNTRNT_980;
  }
}
