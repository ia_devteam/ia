using System.IO;
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1955);
    string s = "Hola\\";
    string d = "Hola\\";
    string e = "Co\"a";
    string f = "Co\"a";
    IACSharpSensor.IACSharpSensor.SensorReached(1956);
    if (s != d) {
      System.Int32 RNTRNTRNT_884 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1957);
      return RNTRNTRNT_884;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1958);
    if (e != f) {
      System.Int32 RNTRNTRNT_885 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1959);
      return RNTRNTRNT_885;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1960);
    string g = "Hello\nworld";
    IACSharpSensor.IACSharpSensor.SensorReached(1961);
    using (StreamReader sr = new StreamReader("test-74.cs")) {
      IACSharpSensor.IACSharpSensor.SensorReached(1962);
      int i = sr.Read();
      IACSharpSensor.IACSharpSensor.SensorReached(1963);
      if (sr.Read() <= 13) {
        IACSharpSensor.IACSharpSensor.SensorReached(1964);
        g = g.Replace("\n", "\r\n");
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1965);
    string h = "Hello\r\nworld";
    IACSharpSensor.IACSharpSensor.SensorReached(1966);
    if (g != h) {
      System.Int32 RNTRNTRNT_886 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1967);
      return RNTRNTRNT_886;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1968);
    System.Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_887 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1969);
    return RNTRNTRNT_887;
  }
}
