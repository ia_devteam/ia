using System;
class X
{
  static int test_explicit()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1689);
    object x_int = 1;
    object x_uint_1 = 1u;
    object x_uint_2 = 1u;
    object x_long_1 = 1L;
    object x_long_2 = 1L;
    object x_ulong_1 = 1uL;
    object x_ulong_2 = 1uL;
    object x_ulong_3 = 1uL;
    object x_ulong_4 = 1uL;
    object x_ulong_5 = 1uL;
    IACSharpSensor.IACSharpSensor.SensorReached(1690);
    if (!(x_int is int)) {
      System.Int32 RNTRNTRNT_784 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1691);
      return RNTRNTRNT_784;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1692);
    if (!(x_uint_1 is uint)) {
      System.Int32 RNTRNTRNT_785 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1693);
      return RNTRNTRNT_785;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1694);
    if (!(x_uint_2 is uint)) {
      System.Int32 RNTRNTRNT_786 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1695);
      return RNTRNTRNT_786;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1696);
    if (!(x_long_1 is long)) {
      System.Int32 RNTRNTRNT_787 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(1697);
      return RNTRNTRNT_787;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1698);
    if (!(x_long_2 is long)) {
      System.Int32 RNTRNTRNT_788 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(1699);
      return RNTRNTRNT_788;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1700);
    if (!(x_ulong_1 is ulong)) {
      System.Int32 RNTRNTRNT_789 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(1701);
      return RNTRNTRNT_789;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1702);
    if (!(x_ulong_2 is ulong)) {
      System.Int32 RNTRNTRNT_790 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(1703);
      return RNTRNTRNT_790;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1704);
    if (!(x_ulong_3 is ulong)) {
      System.Int32 RNTRNTRNT_791 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(1705);
      return RNTRNTRNT_791;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1706);
    if (!(x_ulong_4 is ulong)) {
      System.Int32 RNTRNTRNT_792 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(1707);
      return RNTRNTRNT_792;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1708);
    if (!(x_ulong_5 is ulong)) {
      System.Int32 RNTRNTRNT_793 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(1709);
      return RNTRNTRNT_793;
    }
    System.Int32 RNTRNTRNT_794 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1710);
    return RNTRNTRNT_794;
  }
  static int test_implicit()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1711);
    object i_int = 1;
    object i_uint = 0x80000000u;
    object i_long = 0x100000000L;
    object i_ulong = 0x8000000000000000uL;
    IACSharpSensor.IACSharpSensor.SensorReached(1712);
    if (!(i_int is int)) {
      System.Int32 RNTRNTRNT_795 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1713);
      return RNTRNTRNT_795;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1714);
    if (!(i_uint is uint)) {
      System.Int32 RNTRNTRNT_796 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1715);
      return RNTRNTRNT_796;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1716);
    if (!(i_long is long)) {
      System.Int32 RNTRNTRNT_797 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1717);
      return RNTRNTRNT_797;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1718);
    if (!(i_ulong is ulong)) {
      System.Int32 RNTRNTRNT_798 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1719);
      return RNTRNTRNT_798;
    }
    System.Int32 RNTRNTRNT_799 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1720);
    return RNTRNTRNT_799;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1721);
    int v;
    v = test_explicit();
    IACSharpSensor.IACSharpSensor.SensorReached(1722);
    if (v != 0) {
      System.Int32 RNTRNTRNT_800 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(1723);
      return RNTRNTRNT_800;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1724);
    v = test_implicit();
    IACSharpSensor.IACSharpSensor.SensorReached(1725);
    if (v != 0) {
      System.Int32 RNTRNTRNT_801 = 20 + v;
      IACSharpSensor.IACSharpSensor.SensorReached(1726);
      return RNTRNTRNT_801;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1727);
    ulong l = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1728);
    if (l != 0L) {
      ;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1729);
    ulong myulog = 0L;
    Console.WriteLine("Tests pass");
    System.Int32 RNTRNTRNT_802 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1730);
    return RNTRNTRNT_802;
  }
}
