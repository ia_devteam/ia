using System;
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1943);
    MethodSignature ms = new MethodSignature("hello", null, null);
    Console.WriteLine("About to look for: " + ms.Name);
    IACSharpSensor.IACSharpSensor.SensorReached(1944);
  }
}
struct MethodSignature
{
  public string Name;
  public Type RetType;
  public Type[] Parameters;
  public MethodSignature(string name, Type ret_type, Type[] parameters)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1945);
    Name = name;
    RetType = ret_type;
    Parameters = parameters;
    IACSharpSensor.IACSharpSensor.SensorReached(1946);
  }
}
