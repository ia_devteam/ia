using System;
class Foo
{
  public bool MyMethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(68);
    Console.WriteLine("Base class method !");
    System.Boolean RNTRNTRNT_15 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(69);
    return RNTRNTRNT_15;
  }
}
class Blah : Foo
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(70);
    Blah k = new Blah();
    Foo i = k;
    IACSharpSensor.IACSharpSensor.SensorReached(71);
    if (i.MyMethod()) {
      System.Int32 RNTRNTRNT_16 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(72);
      return RNTRNTRNT_16;
    } else {
      System.Int32 RNTRNTRNT_17 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(73);
      return RNTRNTRNT_17;
    }
  }
}
