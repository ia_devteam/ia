class X
{
  public static bool called = false;
  public static X operator +(X a, X b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1970);
    called = true;
    IACSharpSensor.IACSharpSensor.SensorReached(1971);
    return null;
  }
}
class Y : X
{
}
class Z : Y
{
}
class driver
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1972);
    Z a = new Z();
    Z b = new Z();
    X c = a + b;
    IACSharpSensor.IACSharpSensor.SensorReached(1973);
    if (X.called) {
      System.Int32 RNTRNTRNT_888 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1974);
      return RNTRNTRNT_888;
    }
    System.Int32 RNTRNTRNT_889 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1975);
    return RNTRNTRNT_889;
  }
}
