using System;
delegate void PersonArrivedHandler(object source, PersonArrivedArgs args);
class PersonArrivedArgs
{
  public string name;
  public PersonArrivedArgs(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2038);
    this.name = name;
    IACSharpSensor.IACSharpSensor.SensorReached(2039);
  }
}
class Greeter
{
  string greeting;
  public Greeter(string greeting)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2040);
    this.greeting = greeting;
    IACSharpSensor.IACSharpSensor.SensorReached(2041);
  }
  public void HandlePersonArrived(object source, PersonArrivedArgs args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2042);
    Console.WriteLine(greeting, args.name);
    IACSharpSensor.IACSharpSensor.SensorReached(2043);
  }
}
class Room
{
  public event PersonArrivedHandler PersonArrived;
  public Room()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2044);
    PersonArrived = null;
    IACSharpSensor.IACSharpSensor.SensorReached(2045);
  }
  public void AddPerson(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2046);
    PersonArrived(this, null);
    IACSharpSensor.IACSharpSensor.SensorReached(2047);
  }
}
class DelegateTest
{
  static int Main()
  {
    System.Int32 RNTRNTRNT_919 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(2048);
    return RNTRNTRNT_919;
  }
}
