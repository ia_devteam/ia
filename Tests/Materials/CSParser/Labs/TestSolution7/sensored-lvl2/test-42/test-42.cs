class X
{
  public int v, p;
  public int idx;
  public int this[int n] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(716);
      idx = n;
      System.Int32 RNTRNTRNT_220 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(717);
      return RNTRNTRNT_220;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(718);
      idx = n;
      v = value;
      IACSharpSensor.IACSharpSensor.SensorReached(719);
    }
  }
  public int P {
    get {
      System.Int32 RNTRNTRNT_221 = p;
      IACSharpSensor.IACSharpSensor.SensorReached(720);
      return RNTRNTRNT_221;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(721);
      p = value;
      IACSharpSensor.IACSharpSensor.SensorReached(722);
    }
  }
}
class Z
{
  int v;
  public Z P {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(723);
      return null;
    }
    set { IACSharpSensor.IACSharpSensor.SensorReached(724); }
  }
  public static Z operator ++(Z v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(725);
    v.v++;
    Z RNTRNTRNT_222 = v;
    IACSharpSensor.IACSharpSensor.SensorReached(726);
    return RNTRNTRNT_222;
  }
}
class Y
{
  static int p_pre_increment(X x)
  {
    System.Int32 RNTRNTRNT_223 = ++x.P;
    IACSharpSensor.IACSharpSensor.SensorReached(727);
    return RNTRNTRNT_223;
  }
  static int p_post_increment(X x)
  {
    System.Int32 RNTRNTRNT_224 = x.P++;
    IACSharpSensor.IACSharpSensor.SensorReached(728);
    return RNTRNTRNT_224;
  }
  static int i_pre_increment(X x)
  {
    System.Int32 RNTRNTRNT_225 = ++x[100];
    IACSharpSensor.IACSharpSensor.SensorReached(729);
    return RNTRNTRNT_225;
  }
  static int i_post_increment(X x)
  {
    System.Int32 RNTRNTRNT_226 = x[14]++;
    IACSharpSensor.IACSharpSensor.SensorReached(730);
    return RNTRNTRNT_226;
  }
  static Z overload_increment(Z z)
  {
    Z RNTRNTRNT_227 = z++;
    IACSharpSensor.IACSharpSensor.SensorReached(731);
    return RNTRNTRNT_227;
  }
  static Z overload_pre_increment(Z z)
  {
    Z RNTRNTRNT_228 = ++z;
    IACSharpSensor.IACSharpSensor.SensorReached(732);
    return RNTRNTRNT_228;
  }
  static Z ugly(Z z)
  {
    Z RNTRNTRNT_229 = z.P++;
    IACSharpSensor.IACSharpSensor.SensorReached(733);
    return RNTRNTRNT_229;
  }
  static int simple(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(734);
    if (++i != 11) {
      System.Int32 RNTRNTRNT_230 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(735);
      return RNTRNTRNT_230;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(736);
    if (--i != 10) {
      System.Int32 RNTRNTRNT_231 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(737);
      return RNTRNTRNT_231;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(738);
    if (i++ != 10) {
      System.Int32 RNTRNTRNT_232 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(739);
      return RNTRNTRNT_232;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(740);
    if (i-- != 11) {
      System.Int32 RNTRNTRNT_233 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(741);
      return RNTRNTRNT_233;
    }
    System.Int32 RNTRNTRNT_234 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(742);
    return RNTRNTRNT_234;
  }
  static int arrays()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(743);
    int[] a = new int[10];
    int i, j;
    IACSharpSensor.IACSharpSensor.SensorReached(744);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(745);
      a[i]++;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(746);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(747);
      if (a[i] != 1) {
        System.Int32 RNTRNTRNT_235 = 100;
        IACSharpSensor.IACSharpSensor.SensorReached(748);
        return RNTRNTRNT_235;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(749);
    int[,] b = new int[10, 10];
    IACSharpSensor.IACSharpSensor.SensorReached(750);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(751);
      for (j = 0; j < 10; j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(752);
        b[i, j] = i * 10 + j;
        IACSharpSensor.IACSharpSensor.SensorReached(753);
        if (i < 5) {
          IACSharpSensor.IACSharpSensor.SensorReached(754);
          b[i, j]++;
        } else {
          IACSharpSensor.IACSharpSensor.SensorReached(755);
          ++b[i, j];
        }
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(756);
    for (i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(757);
      for (j = 0; j < 10; j++) {
        IACSharpSensor.IACSharpSensor.SensorReached(758);
        if (b[i, j] != i * 10 + (j + 1)) {
          System.Int32 RNTRNTRNT_236 = 101;
          IACSharpSensor.IACSharpSensor.SensorReached(759);
          return RNTRNTRNT_236;
        }
      }
    }
    System.Int32 RNTRNTRNT_237 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(760);
    return RNTRNTRNT_237;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(761);
    X x = new X();
    int c;
    IACSharpSensor.IACSharpSensor.SensorReached(762);
    if ((c = simple(10)) != 0) {
      System.Int32 RNTRNTRNT_238 = c;
      IACSharpSensor.IACSharpSensor.SensorReached(763);
      return RNTRNTRNT_238;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(764);
    if (i_pre_increment(x) != 1) {
      System.Int32 RNTRNTRNT_239 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(765);
      return RNTRNTRNT_239;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(766);
    if (x.idx != 100) {
      System.Int32 RNTRNTRNT_240 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(767);
      return RNTRNTRNT_240;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(768);
    if (i_post_increment(x) != 1) {
      System.Int32 RNTRNTRNT_241 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(769);
      return RNTRNTRNT_241;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(770);
    if (x.idx != 14) {
      System.Int32 RNTRNTRNT_242 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(771);
      return RNTRNTRNT_242;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(772);
    if (p_pre_increment(x) != 1) {
      System.Int32 RNTRNTRNT_243 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(773);
      return RNTRNTRNT_243;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(774);
    if (x.p != 1) {
      System.Int32 RNTRNTRNT_244 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(775);
      return RNTRNTRNT_244;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(776);
    if (p_post_increment(x) != 1) {
      System.Int32 RNTRNTRNT_245 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(777);
      return RNTRNTRNT_245;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(778);
    if (x.p != 2) {
      System.Int32 RNTRNTRNT_246 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(779);
      return RNTRNTRNT_246;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(780);
    Z z = new Z();
    overload_increment(z);
    arrays();
    System.Int32 RNTRNTRNT_247 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(781);
    return RNTRNTRNT_247;
  }
}
