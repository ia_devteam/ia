using System;
using System.Threading;
using System.Reflection;
class I
{
  public delegate string GetTextFn(string a);
  public static GetTextFn GetText;
  static string fn(string s)
  {
    System.String RNTRNTRNT_48 = "(" + s + ")";
    IACSharpSensor.IACSharpSensor.SensorReached(160);
    return RNTRNTRNT_48;
  }
  static I()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(161);
    GetText = new GetTextFn(fn);
    IACSharpSensor.IACSharpSensor.SensorReached(162);
  }
}
class X
{
  public delegate int Foo(int i, int j);
  private void Thread_func()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(163);
    Console.WriteLine("Inside the thread !");
    IACSharpSensor.IACSharpSensor.SensorReached(164);
  }
  public int Func(int i, int j)
  {
    System.Int32 RNTRNTRNT_49 = i + j;
    IACSharpSensor.IACSharpSensor.SensorReached(165);
    return RNTRNTRNT_49;
  }
  public void Bar()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(166);
    Foo my_func = new Foo(Func);
    int result = my_func(2, 4);
    Console.WriteLine("Answer is : " + result);
    IACSharpSensor.IACSharpSensor.SensorReached(167);
  }
  static bool MyFilter(MemberInfo mi, object criteria)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(168);
    Console.WriteLine("You passed in : " + criteria);
    System.Boolean RNTRNTRNT_50 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(169);
    return RNTRNTRNT_50;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(170);
    I.GetTextFn _ = I.GetText;
    Console.WriteLine("Value: " + I.GetText);
    X x = new X();
    Thread thr = new Thread(new ThreadStart(x.Thread_func));
    thr.Start();
    Console.WriteLine("Inside main ");
    thr.Join();
    Console.WriteLine(_("Hello"));
    x.Bar();
    MemberFilter filter = new MemberFilter(MyFilter);
    Type t = x.GetType();
    MemberInfo[] mi = t.FindMembers(MemberTypes.Method, BindingFlags.Static | BindingFlags.NonPublic, Type.FilterName, "MyFilter");
    Console.WriteLine("FindMembers called, mi = " + mi);
    Console.WriteLine("   Count: " + mi.Length);
    IACSharpSensor.IACSharpSensor.SensorReached(171);
    if (!filter(mi[0], "MyFilter")) {
      System.Int32 RNTRNTRNT_51 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(172);
      return RNTRNTRNT_51;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(173);
    NameSpace.TestDelegate td = new NameSpace.TestDelegate(multiply_by_three);
    IACSharpSensor.IACSharpSensor.SensorReached(174);
    if (td(8) != 24) {
      System.Int32 RNTRNTRNT_52 = 30;
      IACSharpSensor.IACSharpSensor.SensorReached(175);
      return RNTRNTRNT_52;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(176);
    if (td.GetType().FullName != "NameSpace.TestDelegate") {
      System.Int32 RNTRNTRNT_53 = 31;
      IACSharpSensor.IACSharpSensor.SensorReached(177);
      return RNTRNTRNT_53;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(178);
    if (_.GetType().FullName != "I+GetTextFn") {
      System.Int32 RNTRNTRNT_54 = 32;
      IACSharpSensor.IACSharpSensor.SensorReached(179);
      return RNTRNTRNT_54;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(180);
    Console.WriteLine("Test passes");
    System.Int32 RNTRNTRNT_55 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(181);
    return RNTRNTRNT_55;
  }
  static int multiply_by_three(int v)
  {
    System.Int32 RNTRNTRNT_56 = v * 3;
    IACSharpSensor.IACSharpSensor.SensorReached(182);
    return RNTRNTRNT_56;
  }
}
namespace NameSpace
{
  public delegate int TestDelegate(int a);
}
namespace TestNamespace
{
  public class TestClass
  {
    public delegate float NotWorkingDelegate(float point, params object[] hiddenParams);
  }
}
