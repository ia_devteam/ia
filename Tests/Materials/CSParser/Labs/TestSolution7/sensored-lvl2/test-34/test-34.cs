using System;
public struct FancyInt
{
  public int value;
  public FancyInt(int v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(439);
    value = v;
    IACSharpSensor.IACSharpSensor.SensorReached(440);
  }
}
public class Blah
{
  static int got;
  public static void Foo(ref int i, ref int j)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(441);
    got = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(442);
  }
  public static int Bar(int j, params int[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(443);
    got = 2;
    int total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(444);
    foreach (int i in args) {
      IACSharpSensor.IACSharpSensor.SensorReached(445);
      Console.WriteLine("My argument: " + i);
      total += i;
    }
    System.Int32 RNTRNTRNT_144 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(446);
    return RNTRNTRNT_144;
  }
  public static void Foo(int i, int j)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(447);
    got = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(448);
  }
  static void In(ref int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(449);
    a++;
    IACSharpSensor.IACSharpSensor.SensorReached(450);
  }
  static void Out(ref int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(451);
    In(ref a);
    IACSharpSensor.IACSharpSensor.SensorReached(452);
  }
  static int AddArray(params int[] valores)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(453);
    int total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(454);
    for (int i = 0; i < valores.Length; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(455);
      total += valores[i];
    }
    System.Int32 RNTRNTRNT_145 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(456);
    return RNTRNTRNT_145;
  }
  static int AddFancy(params FancyInt[] vals)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(457);
    int total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(458);
    for (int i = 0; i < vals.Length; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(459);
      total += vals[i].value;
    }
    System.Int32 RNTRNTRNT_146 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(460);
    return RNTRNTRNT_146;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(461);
    int i = 1;
    int j = 2;
    int[] arr = new int[2] {
      0,
      1
    };
    Foo(i, j);
    IACSharpSensor.IACSharpSensor.SensorReached(462);
    if (got != 3) {
      System.Int32 RNTRNTRNT_147 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(463);
      return RNTRNTRNT_147;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(464);
    Foo(ref i, ref j);
    IACSharpSensor.IACSharpSensor.SensorReached(465);
    if (got != 1) {
      System.Int32 RNTRNTRNT_148 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(466);
      return RNTRNTRNT_148;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(467);
    if (Bar(i, j, 5, 4, 3, 3, 2) != 19) {
      System.Int32 RNTRNTRNT_149 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(468);
      return RNTRNTRNT_149;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(469);
    if (got != 2) {
      System.Int32 RNTRNTRNT_150 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(470);
      return RNTRNTRNT_150;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(471);
    int k = 10;
    Out(ref k);
    IACSharpSensor.IACSharpSensor.SensorReached(472);
    if (k != 11) {
      System.Int32 RNTRNTRNT_151 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(473);
      return RNTRNTRNT_151;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(474);
    int[] arr2 = new int[2] {
      1,
      2
    };
    IACSharpSensor.IACSharpSensor.SensorReached(475);
    if (AddArray(arr2) != 3) {
      System.Int32 RNTRNTRNT_152 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(476);
      return RNTRNTRNT_152;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(477);
    FancyInt f_one = new FancyInt(1);
    FancyInt f_two = new FancyInt(2);
    IACSharpSensor.IACSharpSensor.SensorReached(478);
    if (AddFancy(f_one) != 1) {
      System.Int32 RNTRNTRNT_153 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(479);
      return RNTRNTRNT_153;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(480);
    if (AddFancy(f_one, f_two) != 3) {
      System.Int32 RNTRNTRNT_154 = 13;
      IACSharpSensor.IACSharpSensor.SensorReached(481);
      return RNTRNTRNT_154;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(482);
    Console.WriteLine("Test passes");
    System.Int32 RNTRNTRNT_155 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(483);
    return RNTRNTRNT_155;
  }
}
