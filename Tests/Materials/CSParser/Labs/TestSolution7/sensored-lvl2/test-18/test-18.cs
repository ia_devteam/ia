using System;
class X
{
  static int i;
  static int j;
  static void m()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(133);
    i = 0;
    j = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(134);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(135);
      throw new ArgumentException("Blah");
    } catch (ArgumentException) {
      IACSharpSensor.IACSharpSensor.SensorReached(136);
      i = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(137);
    } catch (Exception) {
      IACSharpSensor.IACSharpSensor.SensorReached(138);
      i = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(139);
    } finally {
      IACSharpSensor.IACSharpSensor.SensorReached(140);
      j = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(141);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(142);
  }
  static int ret(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(143);
    try {
      IACSharpSensor.IACSharpSensor.SensorReached(144);
      if (a == 1) {
        IACSharpSensor.IACSharpSensor.SensorReached(145);
        throw new Exception();
      }
      System.Int32 RNTRNTRNT_41 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(146);
      return RNTRNTRNT_41;
    } catch {
      System.Int32 RNTRNTRNT_42 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(147);
      return RNTRNTRNT_42;
      IACSharpSensor.IACSharpSensor.SensorReached(148);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(149);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(150);
    m();
    IACSharpSensor.IACSharpSensor.SensorReached(151);
    if (i != 1) {
      System.Int32 RNTRNTRNT_43 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(152);
      return RNTRNTRNT_43;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(153);
    if (j != 1) {
      System.Int32 RNTRNTRNT_44 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(154);
      return RNTRNTRNT_44;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(155);
    if (ret(1) != 2) {
      System.Int32 RNTRNTRNT_45 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(156);
      return RNTRNTRNT_45;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(157);
    if (ret(10) != 1) {
      System.Int32 RNTRNTRNT_46 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(158);
      return RNTRNTRNT_46;
    }
    System.Int32 RNTRNTRNT_47 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(159);
    return RNTRNTRNT_47;
  }
}
