using System;
class X
{
  static int test_single(int[] a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(782);
    int total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(783);
    foreach (int i in a) {
      IACSharpSensor.IACSharpSensor.SensorReached(784);
      total += i;
    }
    System.Int32 RNTRNTRNT_248 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(785);
    return RNTRNTRNT_248;
  }
  static int test_continue(int[] a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(786);
    int total = 0;
    int j = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(787);
    foreach (int i in a) {
      IACSharpSensor.IACSharpSensor.SensorReached(788);
      j++;
      IACSharpSensor.IACSharpSensor.SensorReached(789);
      if (j == 5) {
        IACSharpSensor.IACSharpSensor.SensorReached(790);
        continue;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(791);
      total += i;
    }
    System.Int32 RNTRNTRNT_249 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(792);
    return RNTRNTRNT_249;
  }
  static bool test_double(double[] d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(793);
    float t = 1.0f;
    t += 2.0f + 3.0f;
    float x = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(794);
    foreach (float f in d) {
      IACSharpSensor.IACSharpSensor.SensorReached(795);
      x += f;
    }
    System.Boolean RNTRNTRNT_250 = x == t;
    IACSharpSensor.IACSharpSensor.SensorReached(796);
    return RNTRNTRNT_250;
  }
  static int test_break(int[] a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(797);
    int total = 0;
    int j = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(798);
    foreach (int i in a) {
      IACSharpSensor.IACSharpSensor.SensorReached(799);
      j++;
      IACSharpSensor.IACSharpSensor.SensorReached(800);
      if (j == 5) {
        IACSharpSensor.IACSharpSensor.SensorReached(801);
        break;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(802);
      total += i;
    }
    System.Int32 RNTRNTRNT_251 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(803);
    return RNTRNTRNT_251;
  }
  static bool test_multi(int[,] d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(804);
    int total = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(805);
    foreach (int a in d) {
      IACSharpSensor.IACSharpSensor.SensorReached(806);
      total += a;
    }
    System.Boolean RNTRNTRNT_252 = (total == 46);
    IACSharpSensor.IACSharpSensor.SensorReached(807);
    return RNTRNTRNT_252;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(808);
    int[] a = new int[10];
    int[] b = new int[2];
    IACSharpSensor.IACSharpSensor.SensorReached(809);
    for (int i = 0; i < 10; i++) {
      IACSharpSensor.IACSharpSensor.SensorReached(810);
      a[i] = 10 + i;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(811);
    for (int j = 0; j < 2; j++) {
      IACSharpSensor.IACSharpSensor.SensorReached(812);
      b[j] = 50 + j;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(813);
    if (test_single(a) != 145) {
      System.Int32 RNTRNTRNT_253 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(814);
      return RNTRNTRNT_253;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(815);
    if (test_single(b) != 101) {
      System.Int32 RNTRNTRNT_254 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(816);
      return RNTRNTRNT_254;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(817);
    if (test_continue(a) != 131) {
      IACSharpSensor.IACSharpSensor.SensorReached(818);
      Console.WriteLine("Expecting: 131, got " + test_continue(a));
      System.Int32 RNTRNTRNT_255 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(819);
      return RNTRNTRNT_255;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(820);
    if (test_break(a) != 46) {
      IACSharpSensor.IACSharpSensor.SensorReached(821);
      Console.WriteLine("Expecting: 46, got " + test_break(a));
      System.Int32 RNTRNTRNT_256 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(822);
      return RNTRNTRNT_256;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(823);
    double[] d = new double[] {
      1.0,
      2.0,
      3.0
    };
    IACSharpSensor.IACSharpSensor.SensorReached(824);
    if (!test_double(d)) {
      System.Int32 RNTRNTRNT_257 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(825);
      return RNTRNTRNT_257;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(826);
    int[,] jj = new int[2, 2];
    jj[0, 0] = 10;
    jj[0, 1] = 2;
    jj[1, 0] = 30;
    jj[1, 1] = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(827);
    if (!test_multi(jj)) {
      System.Int32 RNTRNTRNT_258 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(828);
      return RNTRNTRNT_258;
    }
    System.Int32 RNTRNTRNT_259 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(829);
    return RNTRNTRNT_259;
  }
}
