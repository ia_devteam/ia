using System;
namespace Mine
{
  public class Blah
  {
    public static int operator +(Blah i, Blah j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(107);
      Console.WriteLine("Base class binary + operator");
      System.Int32 RNTRNTRNT_30 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(108);
      return RNTRNTRNT_30;
    }
    public static implicit operator int(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(109);
      Console.WriteLine("Blah->int");
      System.Int32 RNTRNTRNT_31 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(110);
      return RNTRNTRNT_31;
    }
    public static implicit operator byte(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(111);
      Console.WriteLine("Blah->byte");
      System.Byte RNTRNTRNT_32 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(112);
      return RNTRNTRNT_32;
    }
    public static implicit operator short(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(113);
      Console.WriteLine("Blah->short");
      System.Int16 RNTRNTRNT_33 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(114);
      return RNTRNTRNT_33;
    }
  }
  public class Foo : Blah
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(115);
      int number = new Foo() + new Foo();
      Console.WriteLine(number);
      Foo tmp = new Foo();
      int k = tmp;
      Console.WriteLine("Convert from Foo to float");
      float f = tmp;
      Console.WriteLine("Converted");
      IACSharpSensor.IACSharpSensor.SensorReached(116);
      if (f == 3) {
        IACSharpSensor.IACSharpSensor.SensorReached(117);
        Console.WriteLine("Best implicit conversion selected correctly.");
      }
      IACSharpSensor.IACSharpSensor.SensorReached(118);
      Console.WriteLine("F is {0}", f);
      IACSharpSensor.IACSharpSensor.SensorReached(119);
      if (number == 2 && k == 3) {
        System.Int32 RNTRNTRNT_34 = 0;
        IACSharpSensor.IACSharpSensor.SensorReached(120);
        return RNTRNTRNT_34;
      } else {
        System.Int32 RNTRNTRNT_35 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(121);
        return RNTRNTRNT_35;
      }
    }
  }
}
