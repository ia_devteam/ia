class X
{
  public int v1, v2;
  int y;
  public int this[int a] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(557);
      if (a == 0) {
        System.Int32 RNTRNTRNT_169 = v1;
        IACSharpSensor.IACSharpSensor.SensorReached(558);
        return RNTRNTRNT_169;
      } else {
        System.Int32 RNTRNTRNT_170 = v2;
        IACSharpSensor.IACSharpSensor.SensorReached(559);
        return RNTRNTRNT_170;
      }
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(560);
      if (a == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(561);
        v1 = value;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(562);
        v2 = value;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(563);
    }
  }
  public int Foo()
  {
    System.Int32 RNTRNTRNT_171 = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(564);
    return RNTRNTRNT_171;
  }
  public int Bar {
    get {
      System.Int32 RNTRNTRNT_172 = y;
      IACSharpSensor.IACSharpSensor.SensorReached(565);
      return RNTRNTRNT_172;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(566);
      y = value;
      IACSharpSensor.IACSharpSensor.SensorReached(567);
    }
  }
}
class Y
{
  public uint v1, v2;
  uint y;
  public uint this[uint a] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(568);
      if (a == 0) {
        System.UInt32 RNTRNTRNT_173 = v1;
        IACSharpSensor.IACSharpSensor.SensorReached(569);
        return RNTRNTRNT_173;
      } else {
        System.UInt32 RNTRNTRNT_174 = v2;
        IACSharpSensor.IACSharpSensor.SensorReached(570);
        return RNTRNTRNT_174;
      }
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(571);
      if (a == 0) {
        IACSharpSensor.IACSharpSensor.SensorReached(572);
        v1 = value;
      } else {
        IACSharpSensor.IACSharpSensor.SensorReached(573);
        v2 = value;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(574);
    }
  }
  public uint Foo()
  {
    System.UInt32 RNTRNTRNT_175 = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(575);
    return RNTRNTRNT_175;
  }
  public uint Bar {
    get {
      System.UInt32 RNTRNTRNT_176 = y;
      IACSharpSensor.IACSharpSensor.SensorReached(576);
      return RNTRNTRNT_176;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(577);
      y = value;
      IACSharpSensor.IACSharpSensor.SensorReached(578);
    }
  }
}
class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(579);
    X x = new X();
    Y y = new Y();
    int b;
    x[0] = x[1] = 1;
    x[0] = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(580);
    if (x.v1 != 1) {
      System.Int32 RNTRNTRNT_177 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(581);
      return RNTRNTRNT_177;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(582);
    if (x[0] != 1) {
      System.Int32 RNTRNTRNT_178 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(583);
      return RNTRNTRNT_178;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(584);
    double d;
    long l;
    d = l = b = x[0] = x[1] = x.Bar = x[2] = x[3] = x[4] = x.Foo();
    IACSharpSensor.IACSharpSensor.SensorReached(585);
    if (x.Bar != 8) {
      System.Int32 RNTRNTRNT_179 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(586);
      return RNTRNTRNT_179;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(587);
    if (l != 8) {
      System.Int32 RNTRNTRNT_180 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(588);
      return RNTRNTRNT_180;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(589);
    uint e, f;
    e = 5;
    e = f = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(590);
    if (e != 8) {
      System.Int32 RNTRNTRNT_181 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(591);
      return RNTRNTRNT_181;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(592);
    y[0] = y[1] = 9;
    y[0] = y.Bar = 12;
    IACSharpSensor.IACSharpSensor.SensorReached(593);
    if (y.Bar != 12) {
      System.Int32 RNTRNTRNT_182 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(594);
      return RNTRNTRNT_182;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(595);
    y.Bar = 15;
    IACSharpSensor.IACSharpSensor.SensorReached(596);
    if (y.Bar != 15) {
      System.Int32 RNTRNTRNT_183 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(597);
      return RNTRNTRNT_183;
    }
    System.Int32 RNTRNTRNT_184 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(598);
    return RNTRNTRNT_184;
  }
}
