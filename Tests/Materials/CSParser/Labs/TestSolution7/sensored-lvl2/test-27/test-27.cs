using System;
public interface Hello
{
  bool MyMethod(int i);
}
public interface Another : Hello
{
  int AnotherMethod(int i);
}
public class Foo : Hello, Another
{
  public bool MyMethod(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(331);
    if (i == 22) {
      System.Boolean RNTRNTRNT_101 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(332);
      return RNTRNTRNT_101;
    } else {
      System.Boolean RNTRNTRNT_102 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(333);
      return RNTRNTRNT_102;
    }
  }
  public int AnotherMethod(int i)
  {
    System.Int32 RNTRNTRNT_103 = i * 10;
    IACSharpSensor.IACSharpSensor.SensorReached(334);
    return RNTRNTRNT_103;
  }
}
public interface ITest
{
  bool TestMethod(int i, float j);
}
public class Blah : Foo
{
  public delegate void MyDelegate(int i, int j);
  void Bar(int i, int j)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(335);
    Console.WriteLine(i + j);
    IACSharpSensor.IACSharpSensor.SensorReached(336);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(337);
    Blah k = new Blah();
    Foo f = k;
    object o = k;
    IACSharpSensor.IACSharpSensor.SensorReached(338);
    if (f is Foo) {
      IACSharpSensor.IACSharpSensor.SensorReached(339);
      Console.WriteLine("I am a Foo!");
    }
    IACSharpSensor.IACSharpSensor.SensorReached(340);
    Hello ihello = f;
    Another ianother = f;
    ihello = ianother;
    bool b = f.MyMethod(22);
    MyDelegate del = new MyDelegate(k.Bar);
    del(2, 3);
    Delegate tmp = del;
    MyDelegate adel = (MyDelegate)tmp;
    adel(4, 7);
    Blah l = (Blah)o;
    l.Bar(20, 30);
    l = (Blah)f;
    l.Bar(2, 5);
    f = (Foo)ihello;
    System.Int32 RNTRNTRNT_104 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(341);
    return RNTRNTRNT_104;
  }
}
