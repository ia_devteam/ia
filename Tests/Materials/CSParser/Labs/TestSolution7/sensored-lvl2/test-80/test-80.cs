using System;
public interface A
{
  int Add(int a, int b);
}
public class X
{
  public int Add(int a, int b)
  {
    System.Int32 RNTRNTRNT_912 = a + b;
    IACSharpSensor.IACSharpSensor.SensorReached(2020);
    return RNTRNTRNT_912;
  }
}
class Y : X, A
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(2021);
    Y y = new Y();
    IACSharpSensor.IACSharpSensor.SensorReached(2022);
    if (y.Add(1, 1) != 2) {
      System.Int32 RNTRNTRNT_913 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(2023);
      return RNTRNTRNT_913;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(2024);
    Console.WriteLine("parent interface implementation test passes");
    System.Int32 RNTRNTRNT_914 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(2025);
    return RNTRNTRNT_914;
  }
}
