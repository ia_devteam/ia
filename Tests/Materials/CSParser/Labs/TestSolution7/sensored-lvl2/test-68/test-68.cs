using System;
enum A
{
  Hello
}
class Y
{
  public Y()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1837);
    value = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(1838);
  }
  public int value;
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1839);
    if ("Hello" != A.Hello.ToString()) {
      System.Int32 RNTRNTRNT_835 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1840);
      return RNTRNTRNT_835;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1841);
    Console.WriteLine("value is: " + (5.ToString()));
    IACSharpSensor.IACSharpSensor.SensorReached(1842);
    if (5.ToString() != "5") {
      System.Int32 RNTRNTRNT_836 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1843);
      return RNTRNTRNT_836;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1844);
    Y y = new Y();
    IACSharpSensor.IACSharpSensor.SensorReached(1845);
    if (y.value.ToString() != "3") {
      IACSharpSensor.IACSharpSensor.SensorReached(1846);
      string x = y.value.ToString();
      Console.WriteLine("Got: {0} expected 3", x);
      System.Int32 RNTRNTRNT_837 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1847);
      return RNTRNTRNT_837;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1848);
    Console.WriteLine("Test ok");
    System.Int32 RNTRNTRNT_838 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1849);
    return RNTRNTRNT_838;
  }
}
