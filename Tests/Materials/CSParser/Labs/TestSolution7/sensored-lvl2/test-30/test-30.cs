using System;
interface IA
{
  void Draw();
}
interface IB
{
  void Draw();
}
class X : IA, IB
{
  public bool ia_called;
  public bool ib_called;
  void IA.Draw()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(409);
    ia_called = true;
    IACSharpSensor.IACSharpSensor.SensorReached(410);
  }
  void IB.Draw()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(411);
    ib_called = true;
    IACSharpSensor.IACSharpSensor.SensorReached(412);
  }
}
class test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    X x = new X();
    ((IA)x).Draw();
    Console.WriteLine("IA: " + x.ia_called);
    Console.WriteLine("IB: " + x.ib_called);
    IACSharpSensor.IACSharpSensor.SensorReached(414);
    if (x.ib_called) {
      System.Int32 RNTRNTRNT_134 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(415);
      return RNTRNTRNT_134;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(416);
    if (!x.ia_called) {
      System.Int32 RNTRNTRNT_135 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(417);
      return RNTRNTRNT_135;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(418);
    X y = new X();
    ((IB)y).Draw();
    Console.WriteLine("IA: " + x.ia_called);
    Console.WriteLine("IB: " + x.ib_called);
    IACSharpSensor.IACSharpSensor.SensorReached(419);
    if (!y.ib_called) {
      System.Int32 RNTRNTRNT_136 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(420);
      return RNTRNTRNT_136;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(421);
    if (y.ia_called) {
      System.Int32 RNTRNTRNT_137 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(422);
      return RNTRNTRNT_137;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(423);
    Console.WriteLine("All tests pass");
    System.Int32 RNTRNTRNT_138 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(424);
    return RNTRNTRNT_138;
  }
}
