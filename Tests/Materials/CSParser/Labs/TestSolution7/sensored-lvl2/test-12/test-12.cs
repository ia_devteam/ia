using System;
class X
{
  static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(46);
    string a = "hello";
    string b = "1";
    string c = a + b;
    string d = a + 1;
    string y;
    IACSharpSensor.IACSharpSensor.SensorReached(47);
    if (c != d) {
      System.Int32 RNTRNTRNT_5 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(48);
      return RNTRNTRNT_5;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(49);
    if (d != (a + b)) {
      System.Int32 RNTRNTRNT_6 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(50);
      return RNTRNTRNT_6;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(51);
    if (d != x(a, b)) {
      System.Int32 RNTRNTRNT_7 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(52);
      return RNTRNTRNT_7;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(53);
    if (d != x(a, 1)) {
      System.Int32 RNTRNTRNT_8 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      return RNTRNTRNT_8;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(55);
    y = c == d ? "equal" : "not-equal";
    IACSharpSensor.IACSharpSensor.SensorReached(56);
    if (y != "equal") {
      System.Int32 RNTRNTRNT_9 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(57);
      return RNTRNTRNT_9;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(58);
    y = b == a ? "oops" : "nice";
    IACSharpSensor.IACSharpSensor.SensorReached(59);
    if (y != "nice") {
      System.Int32 RNTRNTRNT_10 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(60);
      return RNTRNTRNT_10;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(61);
    string[] blah = { "A" + 'B' + "C" };
    IACSharpSensor.IACSharpSensor.SensorReached(62);
    if (blah[0] != "ABC") {
      System.Int32 RNTRNTRNT_11 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(63);
      return RNTRNTRNT_11;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(64);
    Console.WriteLine(c);
    System.Int32 RNTRNTRNT_12 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(65);
    return RNTRNTRNT_12;
  }
  static string s(string a, int o)
  {
    System.String RNTRNTRNT_13 = a + o;
    IACSharpSensor.IACSharpSensor.SensorReached(66);
    return RNTRNTRNT_13;
  }
  static string x(string s, object o)
  {
    System.String RNTRNTRNT_14 = s + o;
    IACSharpSensor.IACSharpSensor.SensorReached(67);
    return RNTRNTRNT_14;
  }
}
