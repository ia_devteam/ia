using System;
namespace Mine
{
  public class MyBoolean
  {
    public static implicit operator bool(MyBoolean x)
    {
      System.Boolean RNTRNTRNT_840 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1153);
      return RNTRNTRNT_840;
    }
  }
  public class MyTrueFalse
  {
    public static bool operator true(MyTrueFalse i)
    {
      System.Boolean RNTRNTRNT_841 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1154);
      return RNTRNTRNT_841;
    }
    public static bool operator false(MyTrueFalse i)
    {
      System.Boolean RNTRNTRNT_842 = false;
      IACSharpSensor.IACSharpSensor.SensorReached(1155);
      return RNTRNTRNT_842;
    }
  }
  public class Blah
  {
    public int i;
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1156);
      Blah k, l;
      k = new Blah(2) + new Blah(3);
      if (k.i != 5) {
        System.Int32 RNTRNTRNT_843 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1157);
        return RNTRNTRNT_843;
      }
      k = ~new Blah(5);
      if (k.i != -6) {
        System.Int32 RNTRNTRNT_844 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1158);
        return RNTRNTRNT_844;
      }
      k = +new Blah(4);
      if (k.i != 4) {
        System.Int32 RNTRNTRNT_845 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1159);
        return RNTRNTRNT_845;
      }
      k = -new Blah(21);
      if (k.i != -21) {
        System.Int32 RNTRNTRNT_846 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1160);
        return RNTRNTRNT_846;
      }
      k = new Blah(22) - new Blah(21);
      if (k.i != 1) {
        System.Int32 RNTRNTRNT_847 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1161);
        return RNTRNTRNT_847;
      }
      if (!k) {
        Console.WriteLine("! returned true");
      }
      int number = k;
      if (number != 1) {
        System.Int32 RNTRNTRNT_848 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1162);
        return RNTRNTRNT_848;
      }
      k++;
      ++k;
      if (k) {
        Console.WriteLine("k is definitely true");
      }
      k = new Blah(30);
      double f = (double)k;
      if (f != 30.0) {
        System.Int32 RNTRNTRNT_849 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1163);
        return RNTRNTRNT_849;
      }
      int i = new Blah(5) * new Blah(10);
      if (i != 50) {
        System.Int32 RNTRNTRNT_850 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1164);
        return RNTRNTRNT_850;
      }
      k = new Blah(50);
      l = new Blah(10);
      i = k / l;
      if (i != 5) {
        System.Int32 RNTRNTRNT_851 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1165);
        return RNTRNTRNT_851;
      }
      i = k % l;
      if (i != 0) {
        System.Int32 RNTRNTRNT_852 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1166);
        return RNTRNTRNT_852;
      }
      MyBoolean myb = new MyBoolean();
      if (!myb) {
        System.Int32 RNTRNTRNT_853 = 10;
        IACSharpSensor.IACSharpSensor.SensorReached(1167);
        return RNTRNTRNT_853;
      }
      MyTrueFalse mf = new MyTrueFalse();
      int x = mf ? 1 : 2;
      if (x != 1) {
        System.Int32 RNTRNTRNT_854 = 11;
        IACSharpSensor.IACSharpSensor.SensorReached(1168);
        return RNTRNTRNT_854;
      }
      Console.WriteLine("Test passed");
      System.Int32 RNTRNTRNT_855 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1169);
      return RNTRNTRNT_855;
    }
    public Blah(int v)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1170);
      i = v;
      IACSharpSensor.IACSharpSensor.SensorReached(1171);
    }
    public static Blah operator +(Blah i, Blah j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1172);
      Blah b = new Blah(i.i + j.i);
      Console.WriteLine("Overload binary + operator");
      Blah RNTRNTRNT_856 = b;
      IACSharpSensor.IACSharpSensor.SensorReached(1173);
      return RNTRNTRNT_856;
    }
    public static Blah operator +(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1174);
      Console.WriteLine("Overload unary + operator");
      Blah RNTRNTRNT_857 = new Blah(i.i);
      IACSharpSensor.IACSharpSensor.SensorReached(1175);
      return RNTRNTRNT_857;
    }
    public static Blah operator -(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1176);
      Console.WriteLine("Overloaded unary - operator");
      Blah RNTRNTRNT_858 = new Blah(-i.i);
      IACSharpSensor.IACSharpSensor.SensorReached(1177);
      return RNTRNTRNT_858;
    }
    public static Blah operator -(Blah i, Blah j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1178);
      Blah b = new Blah(i.i - j.i);
      Console.WriteLine("Overloaded binary - operator");
      Blah RNTRNTRNT_859 = b;
      IACSharpSensor.IACSharpSensor.SensorReached(1179);
      return RNTRNTRNT_859;
    }
    public static int operator *(Blah i, Blah j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1180);
      Console.WriteLine("Overloaded binary * operator");
      System.Int32 RNTRNTRNT_860 = i.i * j.i;
      IACSharpSensor.IACSharpSensor.SensorReached(1181);
      return RNTRNTRNT_860;
    }
    public static int operator /(Blah i, Blah j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1182);
      Console.WriteLine("Overloaded binary / operator");
      System.Int32 RNTRNTRNT_861 = i.i / j.i;
      IACSharpSensor.IACSharpSensor.SensorReached(1183);
      return RNTRNTRNT_861;
    }
    public static int operator %(Blah i, Blah j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1184);
      Console.WriteLine("Overloaded binary % operator");
      System.Int32 RNTRNTRNT_862 = i.i % j.i;
      IACSharpSensor.IACSharpSensor.SensorReached(1185);
      return RNTRNTRNT_862;
    }
    public static Blah operator ~(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1186);
      Console.WriteLine("Overloaded ~ operator");
      Blah RNTRNTRNT_863 = new Blah(~i.i);
      IACSharpSensor.IACSharpSensor.SensorReached(1187);
      return RNTRNTRNT_863;
    }
    public static bool operator !(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1188);
      Console.WriteLine("Overloaded ! operator");
      System.Boolean RNTRNTRNT_864 = (i.i == 1);
      IACSharpSensor.IACSharpSensor.SensorReached(1189);
      return RNTRNTRNT_864;
    }
    public static Blah operator ++(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1190);
      Blah b = new Blah(i.i + 1);
      Console.WriteLine("Incrementing i");
      Blah RNTRNTRNT_865 = b;
      IACSharpSensor.IACSharpSensor.SensorReached(1191);
      return RNTRNTRNT_865;
    }
    public static Blah operator --(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1192);
      Blah b = new Blah(i.i - 1);
      Console.WriteLine("Decrementing i");
      Blah RNTRNTRNT_866 = b;
      IACSharpSensor.IACSharpSensor.SensorReached(1193);
      return RNTRNTRNT_866;
    }
    public static bool operator true(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1194);
      Console.WriteLine("Overloaded true operator");
      System.Boolean RNTRNTRNT_867 = (i.i == 3);
      IACSharpSensor.IACSharpSensor.SensorReached(1195);
      return RNTRNTRNT_867;
    }
    public static bool operator false(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1196);
      Console.WriteLine("Overloaded false operator");
      System.Boolean RNTRNTRNT_868 = (i.i != 1);
      IACSharpSensor.IACSharpSensor.SensorReached(1197);
      return RNTRNTRNT_868;
    }
    public static implicit operator int(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1198);
      Console.WriteLine("Converting implicitly from Blah->int");
      System.Int32 RNTRNTRNT_869 = i.i;
      IACSharpSensor.IACSharpSensor.SensorReached(1199);
      return RNTRNTRNT_869;
    }
    public static explicit operator double(Blah i)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1200);
      Console.WriteLine("Converting explicitly from Blah->double");
      System.Double RNTRNTRNT_870 = (double)i.i;
      IACSharpSensor.IACSharpSensor.SensorReached(1201);
      return RNTRNTRNT_870;
    }
  }
}
