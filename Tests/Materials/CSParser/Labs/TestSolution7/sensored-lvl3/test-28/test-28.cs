using System.Collections;
abstract class A
{
  protected abstract int this[int a] { get; }
  public int EmulateIndexer(int a)
  {
    System.Int32 RNTRNTRNT_105 = this[a];
    IACSharpSensor.IACSharpSensor.SensorReached(211);
    return RNTRNTRNT_105;
  }
}
class B : A
{
  protected override int this[int a] {
    get {
      System.Int32 RNTRNTRNT_106 = a;
      IACSharpSensor.IACSharpSensor.SensorReached(212);
      return RNTRNTRNT_106;
    }
  }
  public int M()
  {
    System.Int32 RNTRNTRNT_107 = this[0];
    IACSharpSensor.IACSharpSensor.SensorReached(213);
    return RNTRNTRNT_107;
  }
}
class X
{
  int v1, v2;
  int this[int a] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(214);
      if (a == 0) {
        System.Int32 RNTRNTRNT_108 = v1;
        IACSharpSensor.IACSharpSensor.SensorReached(215);
        return RNTRNTRNT_108;
      } else {
        System.Int32 RNTRNTRNT_109 = v2;
        IACSharpSensor.IACSharpSensor.SensorReached(216);
        return RNTRNTRNT_109;
      }
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(217);
      if (a == 0) {
        v1 = value;
      } else {
        v2 = value;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(218);
    }
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(219);
    X x = new X();
    x[0] = 1;
    if (x.v1 != 1) {
      System.Int32 RNTRNTRNT_110 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(220);
      return RNTRNTRNT_110;
    }
    if (x[0] != 1) {
      System.Int32 RNTRNTRNT_111 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(221);
      return RNTRNTRNT_111;
    }
    B bb = new B();
    if (bb.EmulateIndexer(10) != 10) {
      System.Int32 RNTRNTRNT_112 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(222);
      return RNTRNTRNT_112;
    }
    Hashtable a = new Hashtable();
    int b = (int)(a[0] = 1);
    if (b != 1) {
      System.Int32 RNTRNTRNT_113 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(223);
      return RNTRNTRNT_113;
    }
    System.Int32 RNTRNTRNT_114 = new B().M();
    IACSharpSensor.IACSharpSensor.SensorReached(224);
    return RNTRNTRNT_114;
  }
}
