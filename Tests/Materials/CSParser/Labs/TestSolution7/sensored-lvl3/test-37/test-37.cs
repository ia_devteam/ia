using System;
class X
{
  static void m(int[] a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(311);
    a[0] = 0xdead;
    IACSharpSensor.IACSharpSensor.SensorReached(312);
  }
  static int test_int_single_dim()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(313);
    int[] a = new int[10];
    int i;
    for (i = 0; i < 10; i++) {
      a[i] = i;
    }
    m(a);
    if (a[0] != 0xdead) {
      System.Int32 RNTRNTRNT_162 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(314);
      return RNTRNTRNT_162;
    }
    a[0] = 0;
    for (i = 9; i >= 0; i--) {
      if (a[i] != i) {
        System.Int32 RNTRNTRNT_163 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(315);
        return RNTRNTRNT_163;
      }
    }
    System.Int32 RNTRNTRNT_164 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(316);
    return RNTRNTRNT_164;
  }
  static int simple_test_double_dim()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(317);
    int[,] b = new int[10, 10];
    b[0, 0] = 1;
    b[4, 4] = 1;
    if (b[0, 0] != b[4, 4]) {
      System.Int32 RNTRNTRNT_165 = 20;
      IACSharpSensor.IACSharpSensor.SensorReached(318);
      return RNTRNTRNT_165;
    }
    if (b[1, 1] != b[5, 5]) {
      System.Int32 RNTRNTRNT_166 = 21;
      IACSharpSensor.IACSharpSensor.SensorReached(319);
      return RNTRNTRNT_166;
    }
    System.Int32 RNTRNTRNT_167 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(320);
    return RNTRNTRNT_167;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(321);
    int v;
    Console.WriteLine("hello");
    System.Int32 RNTRNTRNT_168 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(322);
    return RNTRNTRNT_168;
  }
}
