using System;
class X
{
  enum A : int
  {
    a = 1,
    b,
    c
  }
  enum Test : short
  {
    A = 1,
    B
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1366);
    int v = 1;
    object foo = (v + A.a);
    object foo2 = (1 + A.a);
    if (foo.GetType().ToString() != "X+A") {
      Console.WriteLine("Expression evaluator bug in E operator + (U x, E y)");
      System.Int32 RNTRNTRNT_976 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1367);
      return RNTRNTRNT_976;
    }
    if (foo2.GetType().ToString() != "X+A") {
      Console.WriteLine("Constant folder bug in E operator + (U x, E y)");
      System.Int32 RNTRNTRNT_977 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1368);
      return RNTRNTRNT_977;
    }
    byte b = 1;
    short s = (short)(Test.A + b);
    if (Test.A != Test.A) {
      System.Int32 RNTRNTRNT_978 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1369);
      return RNTRNTRNT_978;
    }
    if (Test.A == Test.B) {
      System.Int32 RNTRNTRNT_979 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1370);
      return RNTRNTRNT_979;
    }
    System.Int32 RNTRNTRNT_980 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1371);
    return RNTRNTRNT_980;
  }
}
