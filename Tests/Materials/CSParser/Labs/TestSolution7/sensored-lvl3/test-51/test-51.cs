using System;
class Base
{
  public int b_int_field;
  public string b_string_field;
  public const int b_const_three = 3;
  public int b_int_property {
    get {
      System.Int32 RNTRNTRNT_734 = b_int_field;
      IACSharpSensor.IACSharpSensor.SensorReached(964);
      return RNTRNTRNT_734;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(965);
      b_int_field = value;
      IACSharpSensor.IACSharpSensor.SensorReached(966);
    }
  }
  public string b_get_id()
  {
    System.String RNTRNTRNT_735 = "Base";
    IACSharpSensor.IACSharpSensor.SensorReached(967);
    return RNTRNTRNT_735;
  }
  public Base()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(968);
    b_int_field = 1;
    b_string_field = "base";
    IACSharpSensor.IACSharpSensor.SensorReached(969);
  }
}
class Derived : Base
{
  new int b_int_field;
  new string b_string_field;
  new const int b_const_three = 4;
  new int b_int_property {
    get {
      System.Int32 RNTRNTRNT_736 = b_int_field;
      IACSharpSensor.IACSharpSensor.SensorReached(970);
      return RNTRNTRNT_736;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(971);
      b_int_field = value;
      IACSharpSensor.IACSharpSensor.SensorReached(972);
    }
  }
  public Derived()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(973);
    b_int_field = 10;
    b_string_field = "derived";
    IACSharpSensor.IACSharpSensor.SensorReached(974);
  }
  public int Test()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(975);
    if (b_int_field != 10) {
      System.Int32 RNTRNTRNT_737 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(976);
      return RNTRNTRNT_737;
    }
    if (base.b_int_field != 1) {
      System.Int32 RNTRNTRNT_738 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(977);
      return RNTRNTRNT_738;
    }
    if (base.b_string_field != "base") {
      System.Int32 RNTRNTRNT_739 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(978);
      return RNTRNTRNT_739;
    }
    if (b_string_field != "derived") {
      System.Int32 RNTRNTRNT_740 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(979);
      return RNTRNTRNT_740;
    }
    base.b_int_property = 4;
    if (b_int_property != 10) {
      System.Int32 RNTRNTRNT_741 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(980);
      return RNTRNTRNT_741;
    }
    if (b_int_property != 10) {
      System.Int32 RNTRNTRNT_742 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(981);
      return RNTRNTRNT_742;
    }
    if (base.b_int_property != 4) {
      System.Int32 RNTRNTRNT_743 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(982);
      return RNTRNTRNT_743;
    }
    if (b_const_three != 4) {
      System.Int32 RNTRNTRNT_744 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(983);
      return RNTRNTRNT_744;
    }
    if (Base.b_const_three != 3) {
      System.Int32 RNTRNTRNT_745 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(984);
      return RNTRNTRNT_745;
    }
    System.Console.WriteLine("All tests pass");
    System.Int32 RNTRNTRNT_746 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(985);
    return RNTRNTRNT_746;
  }
}
class boot
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(986);
    Derived d = new Derived();
    System.Int32 RNTRNTRNT_747 = d.Test();
    IACSharpSensor.IACSharpSensor.SensorReached(987);
    return RNTRNTRNT_747;
  }
}
