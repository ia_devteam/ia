public class TestIntOps
{
  public static sbyte sbyte_add(sbyte a, sbyte b)
  {
    System.SByte RNTRNTRNT_118 = (sbyte)(a + b);
    IACSharpSensor.IACSharpSensor.SensorReached(233);
    return RNTRNTRNT_118;
  }
  public static short short_add(short a, short b)
  {
    System.Int16 RNTRNTRNT_119 = (short)(a + b);
    IACSharpSensor.IACSharpSensor.SensorReached(234);
    return RNTRNTRNT_119;
  }
  public static double double_add(double a, double b)
  {
    System.Double RNTRNTRNT_120 = a + b;
    IACSharpSensor.IACSharpSensor.SensorReached(235);
    return RNTRNTRNT_120;
  }
  public static int int_add(int a, int b)
  {
    System.Int32 RNTRNTRNT_121 = a + b;
    IACSharpSensor.IACSharpSensor.SensorReached(236);
    return RNTRNTRNT_121;
  }
  public static int int_sub(int a, int b)
  {
    System.Int32 RNTRNTRNT_122 = a - b;
    IACSharpSensor.IACSharpSensor.SensorReached(237);
    return RNTRNTRNT_122;
  }
  public static int int_mul(int a, int b)
  {
    System.Int32 RNTRNTRNT_123 = a * b;
    IACSharpSensor.IACSharpSensor.SensorReached(238);
    return RNTRNTRNT_123;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(239);
    int num = 1;
    if (int_add(1, 1) != 2) {
      System.Int32 RNTRNTRNT_124 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(240);
      return RNTRNTRNT_124;
    }
    num++;
    if (int_add(31, -1) != 30) {
      System.Int32 RNTRNTRNT_125 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(241);
      return RNTRNTRNT_125;
    }
    num++;
    if (int_sub(31, -1) != 32) {
      System.Int32 RNTRNTRNT_126 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(242);
      return RNTRNTRNT_126;
    }
    num++;
    if (int_mul(12, 12) != 144) {
      System.Int32 RNTRNTRNT_127 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(243);
      return RNTRNTRNT_127;
    }
    num++;
    if (sbyte_add(1, 1) != 2) {
      System.Int32 RNTRNTRNT_128 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(244);
      return RNTRNTRNT_128;
    }
    num++;
    if (sbyte_add(31, -1) != 30) {
      System.Int32 RNTRNTRNT_129 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(245);
      return RNTRNTRNT_129;
    }
    num++;
    if (short_add(1, 1) != 2) {
      System.Int32 RNTRNTRNT_130 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(246);
      return RNTRNTRNT_130;
    }
    num++;
    if (short_add(31, -1) != 30) {
      System.Int32 RNTRNTRNT_131 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(247);
      return RNTRNTRNT_131;
    }
    num++;
    if (double_add(1.5, 1.5) != 3) {
      System.Int32 RNTRNTRNT_132 = num;
      IACSharpSensor.IACSharpSensor.SensorReached(248);
      return RNTRNTRNT_132;
    }
    num++;
    System.Int32 RNTRNTRNT_133 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(249);
    return RNTRNTRNT_133;
  }
}
