using System;
class Base
{
  public int which;
  public virtual void A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(260);
    which = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(261);
  }
}
class Derived : Base
{
  public virtual void A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(262);
    which = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(263);
  }
}
class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(264);
    Derived d = new Derived();
    d.A();
    if (d.which == 1) {
      System.Int32 RNTRNTRNT_139 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(265);
      return RNTRNTRNT_139;
    }
    Console.WriteLine("Test passes");
    System.Int32 RNTRNTRNT_140 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(266);
    return RNTRNTRNT_140;
  }
}
