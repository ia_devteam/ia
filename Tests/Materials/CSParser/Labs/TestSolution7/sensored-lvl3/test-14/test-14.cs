using System;
namespace Obj
{
  interface Bah
  {
    int H();
  }
  class A : Bah
  {
    public int F()
    {
      System.Int32 RNTRNTRNT_18 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(50);
      return RNTRNTRNT_18;
    }
    public virtual int G()
    {
      System.Int32 RNTRNTRNT_19 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(51);
      return RNTRNTRNT_19;
    }
    public int H()
    {
      System.Int32 RNTRNTRNT_20 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(52);
      return RNTRNTRNT_20;
    }
  }
  class B : A
  {
    public new int F()
    {
      System.Int32 RNTRNTRNT_21 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(53);
      return RNTRNTRNT_21;
    }
    public override int G()
    {
      System.Int32 RNTRNTRNT_22 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(54);
      return RNTRNTRNT_22;
    }
    public new int H()
    {
      System.Int32 RNTRNTRNT_23 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(55);
      return RNTRNTRNT_23;
    }
  }
  class Test
  {
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(56);
      int result = 0;
      B b = new B();
      A a = b;
      if (a.F() != 1) {
        result |= 1 << 0;
      }
      if (b.F() != 3) {
        result |= 1 << 1;
      }
      if (b.G() != 4) {
        result |= 1 << 2;
      }
      if (a.G() != 4) {
        Console.WriteLine("oops: " + a.G());
        result |= 1 << 3;
      }
      if (a.H() != 10) {
        result |= 1 << 4;
      }
      if (b.H() != 11) {
        result |= 1 << 5;
      }
      if (((A)b).H() != 10) {
        result |= 1 << 6;
      }
      if (((B)a).H() != 11) {
        result |= 1 << 7;
      }
      System.Int32 RNTRNTRNT_24 = result;
      IACSharpSensor.IACSharpSensor.SensorReached(57);
      return RNTRNTRNT_24;
    }
  }
}
