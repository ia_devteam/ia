using System;
class A
{
  public int a;
  public void X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(115);
    a = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(116);
  }
}
class B : A
{
  void X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(117);
    a = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(118);
  }
  public void TestB()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(119);
    X();
    IACSharpSensor.IACSharpSensor.SensorReached(120);
  }
}
class Ax
{
  public int a;
  public virtual void A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(121);
    a = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(122);
  }
  public virtual void B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(123);
    a = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(124);
  }
}
class Bx : Ax
{
  public override void A()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(125);
    a = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(126);
  }
  public new void B()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(127);
    a = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(128);
  }
}
class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(129);
    B b = new B();
    b.TestB();
    if (b.a != 2) {
      System.Int32 RNTRNTRNT_58 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(130);
      return RNTRNTRNT_58;
    }
    Bx bx = new Bx();
    bx.A();
    if (b.a != 2) {
      System.Int32 RNTRNTRNT_59 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(131);
      return RNTRNTRNT_59;
    }
    bx.B();
    Console.WriteLine("a=" + bx.a);
    if (bx.a != 4) {
      System.Int32 RNTRNTRNT_60 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(132);
      return RNTRNTRNT_60;
    }
    System.Int32 RNTRNTRNT_61 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(133);
    return RNTRNTRNT_61;
  }
}
