using System;
class Base
{
  public int val;
  public void Add(int x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(225);
    Console.WriteLine("Incorrect method called");
    val = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(226);
  }
}
class Derived : Base
{
  public void Add(double x)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(227);
    Console.WriteLine("Calling the derived class with double! Excellent!");
    val = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(228);
  }
}
class Demo
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(229);
    Derived d = new Derived();
    d.Add(1);
    if (d.val == 1) {
      System.Int32 RNTRNTRNT_115 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(230);
      return RNTRNTRNT_115;
    }
    if (d.val == 2) {
      System.Int32 RNTRNTRNT_116 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(231);
      return RNTRNTRNT_116;
    }
    System.Int32 RNTRNTRNT_117 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(232);
    return RNTRNTRNT_117;
  }
}
