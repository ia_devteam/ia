using System;
public class Blah
{
  enum Bar
  {
    a = MyEnum.Foo,
    b = A.c,
    c = MyEnum.Bar,
    d = myconstant,
    e = myconstant | 0x1fff
  }
  public enum MyEnum : byte
  {
    Foo = 254,
    Bar = (byte)B.y
  }
  enum A
  {
    a,
    b,
    c
  }
  enum B
  {
    x,
    y,
    z
  }
  enum AA : byte
  {
    a,
    b
  }
  enum BB : ulong
  {
    x = ulong.MaxValue - 1,
    y
  }
  const int myconstant = 30;
  enum Compute : ulong
  {
    two = AA.b + B.y,
    three = AA.b - B.y,
    four = A.a * BB.x,
    five = AA.b >> B.y
  }
  internal enum WindowAttributes : uint
  {
    kWindowNoAttributes = 0,
    kWindowCloseBoxAttribute = (1u << 0),
    kWindowHorizontalZoomAttribute = (1u << 1),
    kWindowVerticalZoomAttribute = (1u << 2),
    kWindowCollapseBoxAttribute = (1u << 3),
    kWindowNoConstrainAttribute = (1u << 31),
    kWindowStandardFloatingAttributes = (kWindowCloseBoxAttribute | kWindowCollapseBoxAttribute)
  }
  const Bar bar_assignment = 0;
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(370);
    byte b = (byte)MyEnum.Foo;
    Console.WriteLine("Foo has a value of " + b);
    if (b != 254) {
      System.Int32 RNTRNTRNT_195 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(371);
      return RNTRNTRNT_195;
    }
    int i = (int)A.a;
    int j = (int)B.x;
    int k = (int)A.c;
    int l = (int)AA.b + 1;
    if ((int)Compute.two != 2) {
      System.Int32 RNTRNTRNT_196 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(372);
      return RNTRNTRNT_196;
    }
    if (i != j) {
      System.Int32 RNTRNTRNT_197 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(373);
      return RNTRNTRNT_197;
    }
    if (k != l) {
      System.Int32 RNTRNTRNT_198 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(374);
      return RNTRNTRNT_198;
    }
    A var = A.b;
    i = (int)Bar.a;
    if (i != 254) {
      System.Int32 RNTRNTRNT_199 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(375);
      return RNTRNTRNT_199;
    }
    i = (int)Bar.b;
    if (i != 2) {
      System.Int32 RNTRNTRNT_200 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(376);
      return RNTRNTRNT_200;
    }
    j = (int)Bar.c;
    if (j != 1) {
      System.Int32 RNTRNTRNT_201 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(377);
      return RNTRNTRNT_201;
    }
    j = (int)Bar.d;
    if (j != 30) {
      System.Int32 RNTRNTRNT_202 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(378);
      return RNTRNTRNT_202;
    }
    Enum e = Bar.d;
    if (e.ToString() != "d") {
      System.Int32 RNTRNTRNT_203 = 15;
      IACSharpSensor.IACSharpSensor.SensorReached(379);
      return RNTRNTRNT_203;
    }
    if ((A.c - A.a) != 2) {
      System.Int32 RNTRNTRNT_204 = 16;
      IACSharpSensor.IACSharpSensor.SensorReached(380);
      return RNTRNTRNT_204;
    }
    if ((A.c - 1) != A.b) {
      System.Int32 RNTRNTRNT_205 = 17;
      IACSharpSensor.IACSharpSensor.SensorReached(381);
      return RNTRNTRNT_205;
    }
    Console.WriteLine("Value: " + e.ToString());
    Console.WriteLine("Enum emission test okay");
    System.Int32 RNTRNTRNT_206 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(382);
    return RNTRNTRNT_206;
  }
}
