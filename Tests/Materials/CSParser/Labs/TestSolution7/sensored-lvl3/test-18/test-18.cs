using System;
class X
{
  static int i;
  static int j;
  static void m()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(84);
    i = 0;
    j = 0;
    try {
      throw new ArgumentException("Blah");
    } catch (ArgumentException) {
      i = 1;
    } catch (Exception) {
      i = 2;
    } finally {
      j = 1;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(85);
  }
  static int ret(int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(86);
    try {
      if (a == 1) {
        throw new Exception();
      }
      System.Int32 RNTRNTRNT_41 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(87);
      return RNTRNTRNT_41;
    } catch {
      System.Int32 RNTRNTRNT_42 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(88);
      return RNTRNTRNT_42;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(89);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(90);
    m();
    if (i != 1) {
      System.Int32 RNTRNTRNT_43 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(91);
      return RNTRNTRNT_43;
    }
    if (j != 1) {
      System.Int32 RNTRNTRNT_44 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(92);
      return RNTRNTRNT_44;
    }
    if (ret(1) != 2) {
      System.Int32 RNTRNTRNT_45 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(93);
      return RNTRNTRNT_45;
    }
    if (ret(10) != 1) {
      System.Int32 RNTRNTRNT_46 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(94);
      return RNTRNTRNT_46;
    }
    System.Int32 RNTRNTRNT_47 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(95);
    return RNTRNTRNT_47;
  }
}
