namespace N1
{
  public enum A
  {
    A_1,
    A_2,
    A_3
  }
  public interface B
  {
    N1.A myProp { get; set; }
  }
  public interface C
  {
    A myProp { get; set; }
  }
  public class Blah
  {
    static int Main()
    {
      System.Int32 RNTRNTRNT_947 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1328);
      return RNTRNTRNT_947;
    }
  }
}
