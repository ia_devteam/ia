using System.IO;
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1223);
    string s = "Hola\\";
    string d = "Hola\\";
    string e = "Co\"a";
    string f = "Co\"a";
    if (s != d) {
      System.Int32 RNTRNTRNT_884 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1224);
      return RNTRNTRNT_884;
    }
    if (e != f) {
      System.Int32 RNTRNTRNT_885 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1225);
      return RNTRNTRNT_885;
    }
    string g = "Hello\nworld";
    using (StreamReader sr = new StreamReader("test-74.cs")) {
      int i = sr.Read();
      if (sr.Read() <= 13) {
        g = g.Replace("\n", "\r\n");
      }
    }
    string h = "Hello\r\nworld";
    if (g != h) {
      System.Int32 RNTRNTRNT_886 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1226);
      return RNTRNTRNT_886;
    }
    System.Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_887 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1227);
    return RNTRNTRNT_887;
  }
}
