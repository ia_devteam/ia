using System;
public class Blah
{
  public delegate int MyDelegate(int i, int j);
  public int Foo(int i, int j)
  {
    System.Int32 RNTRNTRNT_92 = i + j;
    IACSharpSensor.IACSharpSensor.SensorReached(190);
    return RNTRNTRNT_92;
  }
  public static int Test1()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(191);
    Blah f = new Blah();
    MyDelegate del = new MyDelegate(f.Foo);
    MyDelegate another = new MyDelegate(del);
    int number = del(2, 3);
    int i = another(4, 6);
    Console.WriteLine("Delegate invocation of one returned : " + number);
    Console.WriteLine("Delegate invocation of the other returned : " + i);
    if (number == 5 && i == 10) {
      System.Int32 RNTRNTRNT_93 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(192);
      return RNTRNTRNT_93;
    } else {
      System.Int32 RNTRNTRNT_94 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(193);
      return RNTRNTRNT_94;
    }
  }
  public delegate int List(params int[] args);
  public static int Adder(params int[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(194);
    int total = 0;
    foreach (int i in args) {
      total += i;
    }
    System.Int32 RNTRNTRNT_95 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(195);
    return RNTRNTRNT_95;
  }
  public static int Test2()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(196);
    List my_adder = new List(Adder);
    if (my_adder(1, 2, 3) != 6) {
      System.Int32 RNTRNTRNT_96 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(197);
      return RNTRNTRNT_96;
    }
    System.Int32 RNTRNTRNT_97 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(198);
    return RNTRNTRNT_97;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(199);
    int v;
    v = Test1();
    if (v != 0) {
      System.Int32 RNTRNTRNT_98 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(200);
      return RNTRNTRNT_98;
    }
    v = Test2();
    if (v != 0) {
      System.Int32 RNTRNTRNT_99 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(201);
      return RNTRNTRNT_99;
    }
    Console.WriteLine("All tests pass");
    System.Int32 RNTRNTRNT_100 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(202);
    return RNTRNTRNT_100;
  }
}
