using System;
class X
{
  bool sbyte_selected;
  bool int_selected;
  void test(sbyte s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(362);
    sbyte_selected = true;
    IACSharpSensor.IACSharpSensor.SensorReached(363);
  }
  void test(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(364);
    int_selected = true;
    IACSharpSensor.IACSharpSensor.SensorReached(365);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(366);
    X x = new X();
    x.test(1);
    if (x.sbyte_selected) {
      Console.WriteLine("FAILED: Sbyte selected on constant int argument");
      System.Int32 RNTRNTRNT_192 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(367);
      return RNTRNTRNT_192;
    } else {
      Console.WriteLine("OK: int selected for constant int");
    }
    X y = new X();
    sbyte s = 10;
    y.test(s);
    if (y.sbyte_selected) {
      Console.WriteLine("OK: sbyte selected for sbyte argument");
    } else {
      Console.WriteLine("FAILED: sbyte not selected for sbyte argument");
      System.Int32 RNTRNTRNT_193 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(368);
      return RNTRNTRNT_193;
    }
    System.Int32 RNTRNTRNT_194 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(369);
    return RNTRNTRNT_194;
  }
}
