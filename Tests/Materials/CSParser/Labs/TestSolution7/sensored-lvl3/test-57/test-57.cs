using System;
public delegate void EventHandler(int i, int j);
public class Button
{
  private EventHandler click;
  public event EventHandler Click {
    add { click += value; }
    remove { click -= value; }
  }
  public void OnClick(int i, int j)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1048);
    if (click == null) {
      Console.WriteLine("Nothing to click!");
      IACSharpSensor.IACSharpSensor.SensorReached(1049);
      return;
    }
    click(i, j);
    IACSharpSensor.IACSharpSensor.SensorReached(1050);
  }
  public void Reset()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1051);
    click = null;
    IACSharpSensor.IACSharpSensor.SensorReached(1052);
  }
}
public class Blah
{
  Button Button1 = new Button();
  public void Connect()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1053);
    Button1.Click += new EventHandler(Button1_Click);
    Button1.Click += new EventHandler(Foo_Click);
    Button1.Click += null;
    IACSharpSensor.IACSharpSensor.SensorReached(1054);
  }
  public void Button1_Click(int i, int j)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1055);
    Console.WriteLine("Button1 was clicked !");
    Console.WriteLine("Answer : " + (i + j));
    IACSharpSensor.IACSharpSensor.SensorReached(1056);
  }
  public void Foo_Click(int i, int j)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1057);
    Console.WriteLine("Foo was clicked !");
    Console.WriteLine("Answer : " + (i + j));
    IACSharpSensor.IACSharpSensor.SensorReached(1058);
  }
  public void Disconnect()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1059);
    Console.WriteLine("Disconnecting Button1's handler ...");
    Button1.Click -= new EventHandler(Button1_Click);
    IACSharpSensor.IACSharpSensor.SensorReached(1060);
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1061);
    Blah b = new Blah();
    b.Connect();
    b.Button1.OnClick(2, 3);
    b.Disconnect();
    Console.WriteLine("Now calling OnClick again");
    b.Button1.OnClick(3, 7);
    Console.WriteLine("Events test passes");
    System.Int32 RNTRNTRNT_782 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1062);
    return RNTRNTRNT_782;
  }
}
