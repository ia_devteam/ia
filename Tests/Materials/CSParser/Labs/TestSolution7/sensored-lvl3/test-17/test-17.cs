using System;
class Blah
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(75);
    Blah k = new Blah();
    float f = k;
    if (f == 2) {
      Console.WriteLine("Best implicit operator selected correctly");
      System.Int32 RNTRNTRNT_36 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(76);
      return RNTRNTRNT_36;
    }
    System.Int32 RNTRNTRNT_37 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(77);
    return RNTRNTRNT_37;
  }
  public static implicit operator byte(Blah i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(78);
    Console.WriteLine("Blah->byte");
    System.Byte RNTRNTRNT_38 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(79);
    return RNTRNTRNT_38;
  }
  public static implicit operator short(Blah i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(80);
    Console.WriteLine("Blah->short");
    System.Int16 RNTRNTRNT_39 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(81);
    return RNTRNTRNT_39;
  }
  public static implicit operator int(Blah i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(82);
    Console.WriteLine("Blah->int");
    System.Int32 RNTRNTRNT_40 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(83);
    return RNTRNTRNT_40;
  }
}
