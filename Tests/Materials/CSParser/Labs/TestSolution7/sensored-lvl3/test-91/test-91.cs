using System;
using System.Reflection;
abstract class Abstract
{
}
class Plain
{
}
class Test
{
  protected static internal void MyProtectedInternal()
  {
  }
  static internal void MyInternal()
  {
  }
  public static void MyPublic()
  {
  }
  static void MyPrivate()
  {
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1329);
    Type myself = typeof(Test);
    BindingFlags bf = BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public;
    MethodAttributes mpia;
    MethodInfo mpi;
    mpi = myself.GetMethod("MyProtectedInternal", bf);
    mpia = mpi.Attributes & MethodAttributes.MemberAccessMask;
    if (mpia != MethodAttributes.FamORAssem) {
      System.Int32 RNTRNTRNT_948 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1330);
      return RNTRNTRNT_948;
    }
    mpi = myself.GetMethod("MyInternal", bf);
    mpia = mpi.Attributes & MethodAttributes.MemberAccessMask;
    if (mpia != MethodAttributes.Assembly) {
      System.Int32 RNTRNTRNT_949 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1331);
      return RNTRNTRNT_949;
    }
    mpi = myself.GetMethod("MyPublic", bf);
    mpia = mpi.Attributes & MethodAttributes.MemberAccessMask;
    if (mpia != MethodAttributes.Public) {
      System.Int32 RNTRNTRNT_950 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1332);
      return RNTRNTRNT_950;
    }
    mpi = myself.GetMethod("MyPrivate", bf);
    mpia = mpi.Attributes & MethodAttributes.MemberAccessMask;
    if (mpia != MethodAttributes.Private) {
      System.Int32 RNTRNTRNT_951 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1333);
      return RNTRNTRNT_951;
    }
    ConstructorInfo ci = typeof(Abstract).GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[0], new ParameterModifier[0]);
    if (!ci.IsFamily) {
      System.Int32 RNTRNTRNT_952 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(1334);
      return RNTRNTRNT_952;
    }
    ci = typeof(Plain).GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[0], new ParameterModifier[0]);
    if (!ci.IsPublic) {
      System.Int32 RNTRNTRNT_953 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(1335);
      return RNTRNTRNT_953;
    }
    Console.WriteLine("All tests pass");
    System.Int32 RNTRNTRNT_954 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1336);
    return RNTRNTRNT_954;
  }
}
