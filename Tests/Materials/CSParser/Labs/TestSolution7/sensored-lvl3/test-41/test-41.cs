using System;
class X
{
  static void A(ref int a, ref uint b, ref sbyte c, ref byte d, ref long e, ref ulong f, ref short g, ref ushort h, ref char i, ref X x,
  ref float j, ref double k)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(383);
    if (a == 1) {
      a = 2;
    }
    if (b == 1) {
      b = 2;
    }
    if (c == 1) {
      c = 2;
    }
    if (d == 1) {
      d = 2;
    }
    if (e == 1) {
      e = 2;
    }
    if (f == 1) {
      f = 2;
    }
    if (g == 1) {
      g = 2;
    }
    if (h == 1) {
      h = 2;
    }
    if (i == 'a') {
      i = 'b';
    }
    if (x == null) {
      x = new X();
    }
    if (j == 1.0) {
      j = 2.0f;
    }
    if (k == 1.0) {
      k = 2.0;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(384);
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(385);
    int a = 1;
    uint b = 1;
    sbyte c = 1;
    byte d = 1;
    long e = 1;
    ulong f = 1;
    short g = 1;
    ushort h = 1;
    char i = 'a';
    float j = 1.0f;
    double k = 1.0;
    X x = null;
    A(ref a, ref b, ref c, ref d, ref e, ref f, ref g, ref h, ref i, ref x,
    ref j, ref k);
    if (a != 2) {
      System.Int32 RNTRNTRNT_207 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(386);
      return RNTRNTRNT_207;
    }
    if (b != 2) {
      System.Int32 RNTRNTRNT_208 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(387);
      return RNTRNTRNT_208;
    }
    if (c != 2) {
      System.Int32 RNTRNTRNT_209 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(388);
      return RNTRNTRNT_209;
    }
    if (d != 2) {
      System.Int32 RNTRNTRNT_210 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(389);
      return RNTRNTRNT_210;
    }
    if (e != 2) {
      System.Int32 RNTRNTRNT_211 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(390);
      return RNTRNTRNT_211;
    }
    if (f != 2) {
      System.Int32 RNTRNTRNT_212 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(391);
      return RNTRNTRNT_212;
    }
    if (g != 2) {
      System.Int32 RNTRNTRNT_213 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(392);
      return RNTRNTRNT_213;
    }
    if (h != 2) {
      System.Int32 RNTRNTRNT_214 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(393);
      return RNTRNTRNT_214;
    }
    if (i != 'b') {
      System.Int32 RNTRNTRNT_215 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(394);
      return RNTRNTRNT_215;
    }
    if (j != 2.0) {
      System.Int32 RNTRNTRNT_216 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(395);
      return RNTRNTRNT_216;
    }
    if (k != 2.0) {
      System.Int32 RNTRNTRNT_217 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(396);
      return RNTRNTRNT_217;
    }
    if (x == null) {
      System.Int32 RNTRNTRNT_218 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(397);
      return RNTRNTRNT_218;
    }
    Console.WriteLine("Test passed");
    System.Int32 RNTRNTRNT_219 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(398);
    return RNTRNTRNT_219;
  }
}
