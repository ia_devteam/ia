public abstract class Abstract
{
  public abstract int A();
}
public class Concrete : Abstract
{
  public override int A()
  {
    System.Int32 RNTRNTRNT_881 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1219);
    return RNTRNTRNT_881;
  }
}
class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1220);
    Concrete c = new Concrete();
    if (c.A() != 1) {
      System.Int32 RNTRNTRNT_882 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1221);
      return RNTRNTRNT_882;
    }
    System.Int32 RNTRNTRNT_883 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1222);
    return RNTRNTRNT_883;
  }
}
