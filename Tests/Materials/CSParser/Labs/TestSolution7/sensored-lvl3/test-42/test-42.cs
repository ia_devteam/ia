class X
{
  public int v, p;
  public int idx;
  public int this[int n] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(399);
      idx = n;
      System.Int32 RNTRNTRNT_220 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(400);
      return RNTRNTRNT_220;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(401);
      idx = n;
      v = value;
      IACSharpSensor.IACSharpSensor.SensorReached(402);
    }
  }
  public int P {
    get {
      System.Int32 RNTRNTRNT_221 = p;
      IACSharpSensor.IACSharpSensor.SensorReached(403);
      return RNTRNTRNT_221;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(404);
      p = value;
      IACSharpSensor.IACSharpSensor.SensorReached(405);
    }
  }
}
class Z
{
  int v;
  public Z P {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(406);
      return null;
    }
    set { }
  }
  public static Z operator ++(Z v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(407);
    v.v++;
    Z RNTRNTRNT_222 = v;
    IACSharpSensor.IACSharpSensor.SensorReached(408);
    return RNTRNTRNT_222;
  }
}
class Y
{
  static int p_pre_increment(X x)
  {
    System.Int32 RNTRNTRNT_223 = ++x.P;
    IACSharpSensor.IACSharpSensor.SensorReached(409);
    return RNTRNTRNT_223;
  }
  static int p_post_increment(X x)
  {
    System.Int32 RNTRNTRNT_224 = x.P++;
    IACSharpSensor.IACSharpSensor.SensorReached(410);
    return RNTRNTRNT_224;
  }
  static int i_pre_increment(X x)
  {
    System.Int32 RNTRNTRNT_225 = ++x[100];
    IACSharpSensor.IACSharpSensor.SensorReached(411);
    return RNTRNTRNT_225;
  }
  static int i_post_increment(X x)
  {
    System.Int32 RNTRNTRNT_226 = x[14]++;
    IACSharpSensor.IACSharpSensor.SensorReached(412);
    return RNTRNTRNT_226;
  }
  static Z overload_increment(Z z)
  {
    Z RNTRNTRNT_227 = z++;
    IACSharpSensor.IACSharpSensor.SensorReached(413);
    return RNTRNTRNT_227;
  }
  static Z overload_pre_increment(Z z)
  {
    Z RNTRNTRNT_228 = ++z;
    IACSharpSensor.IACSharpSensor.SensorReached(414);
    return RNTRNTRNT_228;
  }
  static Z ugly(Z z)
  {
    Z RNTRNTRNT_229 = z.P++;
    IACSharpSensor.IACSharpSensor.SensorReached(415);
    return RNTRNTRNT_229;
  }
  static int simple(int i)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(416);
    if (++i != 11) {
      System.Int32 RNTRNTRNT_230 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(417);
      return RNTRNTRNT_230;
    }
    if (--i != 10) {
      System.Int32 RNTRNTRNT_231 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(418);
      return RNTRNTRNT_231;
    }
    if (i++ != 10) {
      System.Int32 RNTRNTRNT_232 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(419);
      return RNTRNTRNT_232;
    }
    if (i-- != 11) {
      System.Int32 RNTRNTRNT_233 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(420);
      return RNTRNTRNT_233;
    }
    System.Int32 RNTRNTRNT_234 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(421);
    return RNTRNTRNT_234;
  }
  static int arrays()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(422);
    int[] a = new int[10];
    int i, j;
    for (i = 0; i < 10; i++) {
      a[i]++;
    }
    for (i = 0; i < 10; i++) {
      if (a[i] != 1) {
        System.Int32 RNTRNTRNT_235 = 100;
        IACSharpSensor.IACSharpSensor.SensorReached(423);
        return RNTRNTRNT_235;
      }
    }
    int[,] b = new int[10, 10];
    for (i = 0; i < 10; i++) {
      for (j = 0; j < 10; j++) {
        b[i, j] = i * 10 + j;
        if (i < 5) {
          b[i, j]++;
        } else {
          ++b[i, j];
        }
      }
    }
    for (i = 0; i < 10; i++) {
      for (j = 0; j < 10; j++) {
        if (b[i, j] != i * 10 + (j + 1)) {
          System.Int32 RNTRNTRNT_236 = 101;
          IACSharpSensor.IACSharpSensor.SensorReached(424);
          return RNTRNTRNT_236;
        }
      }
    }
    System.Int32 RNTRNTRNT_237 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(425);
    return RNTRNTRNT_237;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(426);
    X x = new X();
    int c;
    if ((c = simple(10)) != 0) {
      System.Int32 RNTRNTRNT_238 = c;
      IACSharpSensor.IACSharpSensor.SensorReached(427);
      return RNTRNTRNT_238;
    }
    if (i_pre_increment(x) != 1) {
      System.Int32 RNTRNTRNT_239 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(428);
      return RNTRNTRNT_239;
    }
    if (x.idx != 100) {
      System.Int32 RNTRNTRNT_240 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(429);
      return RNTRNTRNT_240;
    }
    if (i_post_increment(x) != 1) {
      System.Int32 RNTRNTRNT_241 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(430);
      return RNTRNTRNT_241;
    }
    if (x.idx != 14) {
      System.Int32 RNTRNTRNT_242 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(431);
      return RNTRNTRNT_242;
    }
    if (p_pre_increment(x) != 1) {
      System.Int32 RNTRNTRNT_243 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(432);
      return RNTRNTRNT_243;
    }
    if (x.p != 1) {
      System.Int32 RNTRNTRNT_244 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(433);
      return RNTRNTRNT_244;
    }
    if (p_post_increment(x) != 1) {
      System.Int32 RNTRNTRNT_245 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(434);
      return RNTRNTRNT_245;
    }
    if (x.p != 2) {
      System.Int32 RNTRNTRNT_246 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(435);
      return RNTRNTRNT_246;
    }
    Z z = new Z();
    overload_increment(z);
    arrays();
    System.Int32 RNTRNTRNT_247 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(436);
    return RNTRNTRNT_247;
  }
}
