using System;
class X
{
  static int cast_int(object o)
  {
    System.Int32 RNTRNTRNT_283 = (int)o;
    IACSharpSensor.IACSharpSensor.SensorReached(482);
    return RNTRNTRNT_283;
  }
  static uint cast_uint(object o)
  {
    System.UInt32 RNTRNTRNT_284 = (uint)o;
    IACSharpSensor.IACSharpSensor.SensorReached(483);
    return RNTRNTRNT_284;
  }
  static short cast_short(object o)
  {
    System.Int16 RNTRNTRNT_285 = (short)o;
    IACSharpSensor.IACSharpSensor.SensorReached(484);
    return RNTRNTRNT_285;
  }
  static char cast_char(object o)
  {
    System.Char RNTRNTRNT_286 = (char)o;
    IACSharpSensor.IACSharpSensor.SensorReached(485);
    return RNTRNTRNT_286;
  }
  static ushort cast_ushort(object o)
  {
    System.UInt16 RNTRNTRNT_287 = (ushort)o;
    IACSharpSensor.IACSharpSensor.SensorReached(486);
    return RNTRNTRNT_287;
  }
  static byte cast_byte(object o)
  {
    System.Byte RNTRNTRNT_288 = (byte)o;
    IACSharpSensor.IACSharpSensor.SensorReached(487);
    return RNTRNTRNT_288;
  }
  static sbyte cast_sbyte(object o)
  {
    System.SByte RNTRNTRNT_289 = (sbyte)o;
    IACSharpSensor.IACSharpSensor.SensorReached(488);
    return RNTRNTRNT_289;
  }
  static long cast_long(object o)
  {
    System.Int64 RNTRNTRNT_290 = (long)o;
    IACSharpSensor.IACSharpSensor.SensorReached(489);
    return RNTRNTRNT_290;
  }
  static ulong cast_ulong(object o)
  {
    System.UInt64 RNTRNTRNT_291 = (ulong)o;
    IACSharpSensor.IACSharpSensor.SensorReached(490);
    return RNTRNTRNT_291;
  }
  static float cast_float(object o)
  {
    System.Single RNTRNTRNT_292 = (float)o;
    IACSharpSensor.IACSharpSensor.SensorReached(491);
    return RNTRNTRNT_292;
  }
  static double cast_double(object o)
  {
    System.Double RNTRNTRNT_293 = (double)o;
    IACSharpSensor.IACSharpSensor.SensorReached(492);
    return RNTRNTRNT_293;
  }
  static bool cast_bool(object o)
  {
    System.Boolean RNTRNTRNT_294 = (bool)o;
    IACSharpSensor.IACSharpSensor.SensorReached(493);
    return RNTRNTRNT_294;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(494);
    if (cast_int((object)-1) != -1) {
      System.Int32 RNTRNTRNT_295 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(495);
      return RNTRNTRNT_295;
    }
    if (cast_int((object)1) != 1) {
      System.Int32 RNTRNTRNT_296 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(496);
      return RNTRNTRNT_296;
    }
    if (cast_int((object)Int32.MaxValue) != Int32.MaxValue) {
      System.Int32 RNTRNTRNT_297 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(497);
      return RNTRNTRNT_297;
    }
    if (cast_int((object)Int32.MinValue) != Int32.MinValue) {
      System.Int32 RNTRNTRNT_298 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(498);
      return RNTRNTRNT_298;
    }
    if (cast_int((object)0) != 0) {
      System.Int32 RNTRNTRNT_299 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(499);
      return RNTRNTRNT_299;
    }
    if (cast_uint((object)(uint)0) != 0) {
      System.Int32 RNTRNTRNT_300 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(500);
      return RNTRNTRNT_300;
    }
    if (cast_uint((object)(uint)1) != 1) {
      System.Int32 RNTRNTRNT_301 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(501);
      return RNTRNTRNT_301;
    }
    if (cast_uint((object)(uint)UInt32.MaxValue) != UInt32.MaxValue) {
      System.Int32 RNTRNTRNT_302 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(502);
      return RNTRNTRNT_302;
    }
    if (cast_uint((object)(uint)UInt32.MinValue) != UInt32.MinValue) {
      System.Int32 RNTRNTRNT_303 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(503);
      return RNTRNTRNT_303;
    }
    if (cast_ushort((object)(ushort)1) != 1) {
      System.Int32 RNTRNTRNT_304 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(504);
      return RNTRNTRNT_304;
    }
    if (cast_ushort((object)(ushort)UInt16.MaxValue) != UInt16.MaxValue) {
      System.Int32 RNTRNTRNT_305 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(505);
      return RNTRNTRNT_305;
    }
    if (cast_ushort((object)(ushort)UInt16.MinValue) != UInt16.MinValue) {
      System.Int32 RNTRNTRNT_306 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(506);
      return RNTRNTRNT_306;
    }
    if (cast_ushort((object)(ushort)0) != 0) {
      System.Int32 RNTRNTRNT_307 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(507);
      return RNTRNTRNT_307;
    }
    if (cast_short((object)(short)-1) != -1) {
      System.Int32 RNTRNTRNT_308 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(508);
      return RNTRNTRNT_308;
    }
    if (cast_short((object)(short)1) != 1) {
      System.Int32 RNTRNTRNT_309 = 13;
      IACSharpSensor.IACSharpSensor.SensorReached(509);
      return RNTRNTRNT_309;
    }
    if (cast_short((object)(short)Int16.MaxValue) != Int16.MaxValue) {
      System.Int32 RNTRNTRNT_310 = 14;
      IACSharpSensor.IACSharpSensor.SensorReached(510);
      return RNTRNTRNT_310;
    }
    if (cast_short((object)(short)Int16.MinValue) != Int16.MinValue) {
      System.Int32 RNTRNTRNT_311 = 15;
      IACSharpSensor.IACSharpSensor.SensorReached(511);
      return RNTRNTRNT_311;
    }
    if (cast_short((object)(short)0) != 0) {
      System.Int32 RNTRNTRNT_312 = 16;
      IACSharpSensor.IACSharpSensor.SensorReached(512);
      return RNTRNTRNT_312;
    }
    if (cast_byte((object)(byte)1) != 1) {
      System.Int32 RNTRNTRNT_313 = 17;
      IACSharpSensor.IACSharpSensor.SensorReached(513);
      return RNTRNTRNT_313;
    }
    if (cast_byte((object)(byte)Byte.MaxValue) != Byte.MaxValue) {
      System.Int32 RNTRNTRNT_314 = 18;
      IACSharpSensor.IACSharpSensor.SensorReached(514);
      return RNTRNTRNT_314;
    }
    if (cast_byte((object)(byte)Byte.MinValue) != Byte.MinValue) {
      System.Int32 RNTRNTRNT_315 = 19;
      IACSharpSensor.IACSharpSensor.SensorReached(515);
      return RNTRNTRNT_315;
    }
    if (cast_byte((object)(byte)0) != 0) {
      System.Int32 RNTRNTRNT_316 = 20;
      IACSharpSensor.IACSharpSensor.SensorReached(516);
      return RNTRNTRNT_316;
    }
    if (cast_sbyte((object)(sbyte)-1) != -1) {
      System.Int32 RNTRNTRNT_317 = 21;
      IACSharpSensor.IACSharpSensor.SensorReached(517);
      return RNTRNTRNT_317;
    }
    if (cast_sbyte((object)(sbyte)1) != 1) {
      System.Int32 RNTRNTRNT_318 = 22;
      IACSharpSensor.IACSharpSensor.SensorReached(518);
      return RNTRNTRNT_318;
    }
    if (cast_sbyte((object)(sbyte)SByte.MaxValue) != SByte.MaxValue) {
      System.Int32 RNTRNTRNT_319 = 23;
      IACSharpSensor.IACSharpSensor.SensorReached(519);
      return RNTRNTRNT_319;
    }
    if (cast_sbyte((object)(sbyte)SByte.MinValue) != SByte.MinValue) {
      System.Int32 RNTRNTRNT_320 = 24;
      IACSharpSensor.IACSharpSensor.SensorReached(520);
      return RNTRNTRNT_320;
    }
    if (cast_sbyte((object)(sbyte)0) != 0) {
      System.Int32 RNTRNTRNT_321 = 25;
      IACSharpSensor.IACSharpSensor.SensorReached(521);
      return RNTRNTRNT_321;
    }
    if (cast_long((object)(long)-1) != -1) {
      System.Int32 RNTRNTRNT_322 = 26;
      IACSharpSensor.IACSharpSensor.SensorReached(522);
      return RNTRNTRNT_322;
    }
    if (cast_long((object)(long)1) != 1) {
      System.Int32 RNTRNTRNT_323 = 27;
      IACSharpSensor.IACSharpSensor.SensorReached(523);
      return RNTRNTRNT_323;
    }
    if (cast_long((object)(long)Int64.MaxValue) != Int64.MaxValue) {
      System.Int32 RNTRNTRNT_324 = 28;
      IACSharpSensor.IACSharpSensor.SensorReached(524);
      return RNTRNTRNT_324;
    }
    if (cast_long((object)(long)Int64.MinValue) != Int64.MinValue) {
      System.Int32 RNTRNTRNT_325 = 29;
      IACSharpSensor.IACSharpSensor.SensorReached(525);
      return RNTRNTRNT_325;
    }
    if (cast_long((object)(long)0) != 0) {
      System.Int32 RNTRNTRNT_326 = 30;
      IACSharpSensor.IACSharpSensor.SensorReached(526);
      return RNTRNTRNT_326;
    }
    if (cast_ulong((object)(ulong)0) != 0) {
      System.Int32 RNTRNTRNT_327 = 31;
      IACSharpSensor.IACSharpSensor.SensorReached(527);
      return RNTRNTRNT_327;
    }
    if (cast_ulong((object)(ulong)1) != 1) {
      System.Int32 RNTRNTRNT_328 = 32;
      IACSharpSensor.IACSharpSensor.SensorReached(528);
      return RNTRNTRNT_328;
    }
    if (cast_ulong((object)(ulong)UInt64.MaxValue) != UInt64.MaxValue) {
      System.Int32 RNTRNTRNT_329 = 33;
      IACSharpSensor.IACSharpSensor.SensorReached(529);
      return RNTRNTRNT_329;
    }
    if (cast_ulong((object)(ulong)UInt64.MinValue) != UInt64.MinValue) {
      System.Int32 RNTRNTRNT_330 = 34;
      IACSharpSensor.IACSharpSensor.SensorReached(530);
      return RNTRNTRNT_330;
    }
    if (cast_double((object)(double)-1) != -1) {
      System.Int32 RNTRNTRNT_331 = 35;
      IACSharpSensor.IACSharpSensor.SensorReached(531);
      return RNTRNTRNT_331;
    }
    if (cast_double((object)(double)1) != 1) {
      System.Int32 RNTRNTRNT_332 = 36;
      IACSharpSensor.IACSharpSensor.SensorReached(532);
      return RNTRNTRNT_332;
    }
    if (cast_double((object)(double)Double.MaxValue) != Double.MaxValue) {
      System.Int32 RNTRNTRNT_333 = 37;
      IACSharpSensor.IACSharpSensor.SensorReached(533);
      return RNTRNTRNT_333;
    }
    if (cast_double((object)(double)Double.MinValue) != Double.MinValue) {
      System.Int32 RNTRNTRNT_334 = 38;
      IACSharpSensor.IACSharpSensor.SensorReached(534);
      return RNTRNTRNT_334;
    }
    if (cast_double((object)(double)0) != 0) {
      System.Int32 RNTRNTRNT_335 = 39;
      IACSharpSensor.IACSharpSensor.SensorReached(535);
      return RNTRNTRNT_335;
    }
    if (cast_float((object)(float)-1) != -1) {
      System.Int32 RNTRNTRNT_336 = 40;
      IACSharpSensor.IACSharpSensor.SensorReached(536);
      return RNTRNTRNT_336;
    }
    if (cast_float((object)(float)1) != 1) {
      System.Int32 RNTRNTRNT_337 = 41;
      IACSharpSensor.IACSharpSensor.SensorReached(537);
      return RNTRNTRNT_337;
    }
    if (cast_float((object)(float)Single.MaxValue) != Single.MaxValue) {
      System.Int32 RNTRNTRNT_338 = 42;
      IACSharpSensor.IACSharpSensor.SensorReached(538);
      return RNTRNTRNT_338;
    }
    if (cast_float((object)(float)Single.MinValue) != Single.MinValue) {
      System.Int32 RNTRNTRNT_339 = 43;
      IACSharpSensor.IACSharpSensor.SensorReached(539);
      return RNTRNTRNT_339;
    }
    if (cast_float((object)(float)0) != 0) {
      System.Int32 RNTRNTRNT_340 = 44;
      IACSharpSensor.IACSharpSensor.SensorReached(540);
      return RNTRNTRNT_340;
    }
    System.Int32 RNTRNTRNT_341 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(541);
    return RNTRNTRNT_341;
  }
}
