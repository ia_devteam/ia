using System;
class X
{
  static int dob(int[,] b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(455);
    int total = 0;
    foreach (int i in b) {
      total += i;
    }
    System.Int32 RNTRNTRNT_260 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(456);
    return RNTRNTRNT_260;
  }
  static int count(object o)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(457);
    int total = 0;
    foreach (int i in (int[])o) {
      total += i;
    }
    System.Int32 RNTRNTRNT_261 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(458);
    return RNTRNTRNT_261;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(459);
    int[,] b = new int[10, 10];
    for (int q = 0; q < 10; q++) {
      for (int w = 0; w < 10; w++) {
        b[q, w] = q * 10 + w;
      }
    }
    if (dob(b) != 4950) {
      System.Int32 RNTRNTRNT_262 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(460);
      return RNTRNTRNT_262;
    }
    int[] a = new int[10];
    for (int i = 0; i < 10; i++) {
      a[i] = 2;
    }
    if (count(a) != 20) {
      System.Int32 RNTRNTRNT_263 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(461);
      return RNTRNTRNT_263;
    }
    System.Int32 RNTRNTRNT_264 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(462);
    return RNTRNTRNT_264;
  }
}
