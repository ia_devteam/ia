using System;
interface I
{
  int P { get; set; }
}
abstract class A : I
{
  public int p;
  public int q;
  public int P {
    get {
      System.Int32 RNTRNTRNT_765 = p;
      IACSharpSensor.IACSharpSensor.SensorReached(1018);
      return RNTRNTRNT_765;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1019);
      p = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1020);
    }
  }
  public abstract int Q { get; set; }
  public int r;
  public virtual int R {
    get {
      System.Int32 RNTRNTRNT_766 = r;
      IACSharpSensor.IACSharpSensor.SensorReached(1021);
      return RNTRNTRNT_766;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1022);
      r = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1023);
    }
  }
}
class B : A
{
  public int bp;
  public new int P {
    get {
      System.Int32 RNTRNTRNT_767 = bp;
      IACSharpSensor.IACSharpSensor.SensorReached(1024);
      return RNTRNTRNT_767;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1025);
      bp = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1026);
    }
  }
  public override int Q {
    get {
      System.Int32 RNTRNTRNT_768 = q;
      IACSharpSensor.IACSharpSensor.SensorReached(1027);
      return RNTRNTRNT_768;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1028);
      q = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1029);
    }
  }
}
class C : A
{
  public override int Q {
    get {
      System.Int32 RNTRNTRNT_769 = q;
      IACSharpSensor.IACSharpSensor.SensorReached(1030);
      return RNTRNTRNT_769;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1031);
      q = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1032);
    }
  }
  public int rr;
  public override int R {
    get {
      System.Int32 RNTRNTRNT_770 = rr;
      IACSharpSensor.IACSharpSensor.SensorReached(1033);
      return RNTRNTRNT_770;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(1034);
      rr = value;
      IACSharpSensor.IACSharpSensor.SensorReached(1035);
    }
  }
}
class M
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1036);
    B b = new B();
    b.P = 1;
    b.R = 10;
    b.Q = 20;
    if (b.P != 1) {
      System.Int32 RNTRNTRNT_771 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1037);
      return RNTRNTRNT_771;
    }
    if (b.bp != 1) {
      System.Int32 RNTRNTRNT_772 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1038);
      return RNTRNTRNT_772;
    }
    if (b.R != 10) {
      System.Int32 RNTRNTRNT_773 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1039);
      return RNTRNTRNT_773;
    }
    if (b.r != 10) {
      System.Int32 RNTRNTRNT_774 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1040);
      return RNTRNTRNT_774;
    }
    if (b.Q != 20) {
      System.Int32 RNTRNTRNT_775 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(1041);
      return RNTRNTRNT_775;
    }
    if (b.q != 20) {
      System.Int32 RNTRNTRNT_776 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(1042);
      return RNTRNTRNT_776;
    }
    C c = new C();
    c.R = 10;
    c.Q = 20;
    c.P = 30;
    if (c.R != 10) {
      System.Int32 RNTRNTRNT_777 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(1043);
      return RNTRNTRNT_777;
    }
    if (c.rr != 10) {
      System.Int32 RNTRNTRNT_778 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(1044);
      return RNTRNTRNT_778;
    }
    if (c.P != 30) {
      System.Int32 RNTRNTRNT_779 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(1045);
      return RNTRNTRNT_779;
    }
    if (c.p != 30) {
      System.Int32 RNTRNTRNT_780 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(1046);
      return RNTRNTRNT_780;
    }
    Console.WriteLine("Test passes");
    System.Int32 RNTRNTRNT_781 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1047);
    return RNTRNTRNT_781;
  }
}
