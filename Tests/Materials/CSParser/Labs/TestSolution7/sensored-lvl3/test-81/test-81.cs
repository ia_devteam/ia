using System;
namespace N1
{
  public class A
  {
    int x;
    string s;
    void Bar()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1266);
      x = int.Parse("0");
      s = string.Format("{0}", x);
      IACSharpSensor.IACSharpSensor.SensorReached(1267);
    }
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1268);
      A a = new A();
      a.Bar();
      if (a.x != 0) {
        System.Int32 RNTRNTRNT_915 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1269);
        return RNTRNTRNT_915;
      }
      if (a.s != "0") {
        System.Int32 RNTRNTRNT_916 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1270);
        return RNTRNTRNT_916;
      }
      Console.WriteLine("Bar set s to " + a.s);
      System.Int32 RNTRNTRNT_917 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1271);
      return RNTRNTRNT_917;
    }
  }
}
