using System;
class Foo
{
  public bool MyMethod()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(45);
    Console.WriteLine("Base class method !");
    System.Boolean RNTRNTRNT_15 = true;
    IACSharpSensor.IACSharpSensor.SensorReached(46);
    return RNTRNTRNT_15;
  }
}
class Blah : Foo
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(47);
    Blah k = new Blah();
    Foo i = k;
    if (i.MyMethod()) {
      System.Int32 RNTRNTRNT_16 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(48);
      return RNTRNTRNT_16;
    } else {
      System.Int32 RNTRNTRNT_17 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(49);
      return RNTRNTRNT_17;
    }
  }
}
