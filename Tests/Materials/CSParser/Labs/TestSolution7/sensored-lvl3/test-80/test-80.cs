using System;
public interface A
{
  int Add(int a, int b);
}
public class X
{
  public int Add(int a, int b)
  {
    System.Int32 RNTRNTRNT_912 = a + b;
    IACSharpSensor.IACSharpSensor.SensorReached(1262);
    return RNTRNTRNT_912;
  }
}
class Y : X, A
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1263);
    Y y = new Y();
    if (y.Add(1, 1) != 2) {
      System.Int32 RNTRNTRNT_913 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1264);
      return RNTRNTRNT_913;
    }
    Console.WriteLine("parent interface implementation test passes");
    System.Int32 RNTRNTRNT_914 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1265);
    return RNTRNTRNT_914;
  }
}
