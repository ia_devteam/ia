using System;
delegate void PersonArrivedHandler(object source, PersonArrivedArgs args);
class PersonArrivedArgs
{
  public string name;
  public PersonArrivedArgs(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1273);
    this.name = name;
    IACSharpSensor.IACSharpSensor.SensorReached(1274);
  }
}
class Greeter
{
  string greeting;
  public Greeter(string greeting)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1275);
    this.greeting = greeting;
    IACSharpSensor.IACSharpSensor.SensorReached(1276);
  }
  public void HandlePersonArrived(object source, PersonArrivedArgs args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1277);
    Console.WriteLine(greeting, args.name);
    IACSharpSensor.IACSharpSensor.SensorReached(1278);
  }
}
class Room
{
  public event PersonArrivedHandler PersonArrived;
  public Room()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1279);
    PersonArrived = null;
    IACSharpSensor.IACSharpSensor.SensorReached(1280);
  }
  public void AddPerson(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1281);
    PersonArrived(this, null);
    IACSharpSensor.IACSharpSensor.SensorReached(1282);
  }
}
class DelegateTest
{
  static int Main()
  {
    System.Int32 RNTRNTRNT_919 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1283);
    return RNTRNTRNT_919;
  }
}
