using System;
interface IA
{
  void Draw();
}
interface IB
{
  void Draw();
}
class X : IA, IB
{
  public bool ia_called;
  public bool ib_called;
  void IA.Draw()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(250);
    ia_called = true;
    IACSharpSensor.IACSharpSensor.SensorReached(251);
  }
  void IB.Draw()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(252);
    ib_called = true;
    IACSharpSensor.IACSharpSensor.SensorReached(253);
  }
}
class test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(254);
    X x = new X();
    ((IA)x).Draw();
    Console.WriteLine("IA: " + x.ia_called);
    Console.WriteLine("IB: " + x.ib_called);
    if (x.ib_called) {
      System.Int32 RNTRNTRNT_134 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(255);
      return RNTRNTRNT_134;
    }
    if (!x.ia_called) {
      System.Int32 RNTRNTRNT_135 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(256);
      return RNTRNTRNT_135;
    }
    X y = new X();
    ((IB)y).Draw();
    Console.WriteLine("IA: " + x.ia_called);
    Console.WriteLine("IB: " + x.ib_called);
    if (!y.ib_called) {
      System.Int32 RNTRNTRNT_136 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(257);
      return RNTRNTRNT_136;
    }
    if (y.ia_called) {
      System.Int32 RNTRNTRNT_137 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(258);
      return RNTRNTRNT_137;
    }
    Console.WriteLine("All tests pass");
    System.Int32 RNTRNTRNT_138 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(259);
    return RNTRNTRNT_138;
  }
}
