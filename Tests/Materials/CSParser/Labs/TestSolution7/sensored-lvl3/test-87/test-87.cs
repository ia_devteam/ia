class Top
{
  class X
  {
  }
  class Y : X
  {
  }
  interface A
  {
    int get_one();
  }
  interface B : A
  {
    int get_two();
  }
  public class XA : A
  {
    public int get_one()
    {
      System.Int32 RNTRNTRNT_930 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1301);
      return RNTRNTRNT_930;
    }
  }
  class XB : B
  {
    public int get_one()
    {
      System.Int32 RNTRNTRNT_931 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1302);
      return RNTRNTRNT_931;
    }
    public int get_two()
    {
      System.Int32 RNTRNTRNT_932 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1303);
      return RNTRNTRNT_932;
    }
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1304);
    XA x = new XA();
    if (x.get_one() != 1) {
      System.Int32 RNTRNTRNT_933 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1305);
      return RNTRNTRNT_933;
    }
    XB b = new XB();
    if (x.get_one() != 1) {
      System.Int32 RNTRNTRNT_934 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1306);
      return RNTRNTRNT_934;
    }
    if (b.get_two() != 2) {
      System.Int32 RNTRNTRNT_935 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1307);
      return RNTRNTRNT_935;
    }
    XB[] xb = null;
    System.Int32 RNTRNTRNT_936 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1308);
    return RNTRNTRNT_936;
  }
}
class Other
{
  public void X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1309);
    Top.XA xa = null;
    Top.XA[] xb = null;
    IACSharpSensor.IACSharpSensor.SensorReached(1310);
  }
}
