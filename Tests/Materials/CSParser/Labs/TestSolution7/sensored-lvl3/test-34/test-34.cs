using System;
public struct FancyInt
{
  public int value;
  public FancyInt(int v)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(272);
    value = v;
    IACSharpSensor.IACSharpSensor.SensorReached(273);
  }
}
public class Blah
{
  static int got;
  public static void Foo(ref int i, ref int j)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(274);
    got = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(275);
  }
  public static int Bar(int j, params int[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(276);
    got = 2;
    int total = 0;
    foreach (int i in args) {
      Console.WriteLine("My argument: " + i);
      total += i;
    }
    System.Int32 RNTRNTRNT_144 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(277);
    return RNTRNTRNT_144;
  }
  public static void Foo(int i, int j)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(278);
    got = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(279);
  }
  static void In(ref int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(280);
    a++;
    IACSharpSensor.IACSharpSensor.SensorReached(281);
  }
  static void Out(ref int a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(282);
    In(ref a);
    IACSharpSensor.IACSharpSensor.SensorReached(283);
  }
  static int AddArray(params int[] valores)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(284);
    int total = 0;
    for (int i = 0; i < valores.Length; i++) {
      total += valores[i];
    }
    System.Int32 RNTRNTRNT_145 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(285);
    return RNTRNTRNT_145;
  }
  static int AddFancy(params FancyInt[] vals)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(286);
    int total = 0;
    for (int i = 0; i < vals.Length; i++) {
      total += vals[i].value;
    }
    System.Int32 RNTRNTRNT_146 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(287);
    return RNTRNTRNT_146;
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(288);
    int i = 1;
    int j = 2;
    int[] arr = new int[2] {
      0,
      1
    };
    Foo(i, j);
    if (got != 3) {
      System.Int32 RNTRNTRNT_147 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(289);
      return RNTRNTRNT_147;
    }
    Foo(ref i, ref j);
    if (got != 1) {
      System.Int32 RNTRNTRNT_148 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(290);
      return RNTRNTRNT_148;
    }
    if (Bar(i, j, 5, 4, 3, 3, 2) != 19) {
      System.Int32 RNTRNTRNT_149 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(291);
      return RNTRNTRNT_149;
    }
    if (got != 2) {
      System.Int32 RNTRNTRNT_150 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(292);
      return RNTRNTRNT_150;
    }
    int k = 10;
    Out(ref k);
    if (k != 11) {
      System.Int32 RNTRNTRNT_151 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(293);
      return RNTRNTRNT_151;
    }
    int[] arr2 = new int[2] {
      1,
      2
    };
    if (AddArray(arr2) != 3) {
      System.Int32 RNTRNTRNT_152 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(294);
      return RNTRNTRNT_152;
    }
    FancyInt f_one = new FancyInt(1);
    FancyInt f_two = new FancyInt(2);
    if (AddFancy(f_one) != 1) {
      System.Int32 RNTRNTRNT_153 = 12;
      IACSharpSensor.IACSharpSensor.SensorReached(295);
      return RNTRNTRNT_153;
    }
    if (AddFancy(f_one, f_two) != 3) {
      System.Int32 RNTRNTRNT_154 = 13;
      IACSharpSensor.IACSharpSensor.SensorReached(296);
      return RNTRNTRNT_154;
    }
    Console.WriteLine("Test passes");
    System.Int32 RNTRNTRNT_155 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(297);
    return RNTRNTRNT_155;
  }
}
