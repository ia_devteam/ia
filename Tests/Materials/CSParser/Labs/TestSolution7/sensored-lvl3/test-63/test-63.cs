using System;
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1100);
    bool one = false, two = false;
    try {
      try {
        throw new Exception();
      } catch (Exception e) {
        one = true;
        Console.WriteLine("Caught");
        throw;
      }
    } catch {
      two = true;
      Console.WriteLine("Again");
    }
    if (one && two) {
      Console.WriteLine("Ok");
      System.Int32 RNTRNTRNT_812 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1101);
      return RNTRNTRNT_812;
    } else {
      Console.WriteLine("Failed");
    }
    System.Int32 RNTRNTRNT_813 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1102);
    return RNTRNTRNT_813;
  }
}
