using System;
class X
{
  static int test_single(int[] a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(437);
    int total = 0;
    foreach (int i in a) {
      total += i;
    }
    System.Int32 RNTRNTRNT_248 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(438);
    return RNTRNTRNT_248;
  }
  static int test_continue(int[] a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(439);
    int total = 0;
    int j = 0;
    foreach (int i in a) {
      j++;
      if (j == 5) {
        continue;
      }
      total += i;
    }
    System.Int32 RNTRNTRNT_249 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(440);
    return RNTRNTRNT_249;
  }
  static bool test_double(double[] d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(441);
    float t = 1.0f;
    t += 2.0f + 3.0f;
    float x = 0;
    foreach (float f in d) {
      x += f;
    }
    System.Boolean RNTRNTRNT_250 = x == t;
    IACSharpSensor.IACSharpSensor.SensorReached(442);
    return RNTRNTRNT_250;
  }
  static int test_break(int[] a)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(443);
    int total = 0;
    int j = 0;
    foreach (int i in a) {
      j++;
      if (j == 5) {
        break;
      }
      total += i;
    }
    System.Int32 RNTRNTRNT_251 = total;
    IACSharpSensor.IACSharpSensor.SensorReached(444);
    return RNTRNTRNT_251;
  }
  static bool test_multi(int[,] d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(445);
    int total = 0;
    foreach (int a in d) {
      total += a;
    }
    System.Boolean RNTRNTRNT_252 = (total == 46);
    IACSharpSensor.IACSharpSensor.SensorReached(446);
    return RNTRNTRNT_252;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(447);
    int[] a = new int[10];
    int[] b = new int[2];
    for (int i = 0; i < 10; i++) {
      a[i] = 10 + i;
    }
    for (int j = 0; j < 2; j++) {
      b[j] = 50 + j;
    }
    if (test_single(a) != 145) {
      System.Int32 RNTRNTRNT_253 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(448);
      return RNTRNTRNT_253;
    }
    if (test_single(b) != 101) {
      System.Int32 RNTRNTRNT_254 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(449);
      return RNTRNTRNT_254;
    }
    if (test_continue(a) != 131) {
      Console.WriteLine("Expecting: 131, got " + test_continue(a));
      System.Int32 RNTRNTRNT_255 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(450);
      return RNTRNTRNT_255;
    }
    if (test_break(a) != 46) {
      Console.WriteLine("Expecting: 46, got " + test_break(a));
      System.Int32 RNTRNTRNT_256 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(451);
      return RNTRNTRNT_256;
    }
    double[] d = new double[] {
      1.0,
      2.0,
      3.0
    };
    if (!test_double(d)) {
      System.Int32 RNTRNTRNT_257 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(452);
      return RNTRNTRNT_257;
    }
    int[,] jj = new int[2, 2];
    jj[0, 0] = 10;
    jj[0, 1] = 2;
    jj[1, 0] = 30;
    jj[1, 1] = 4;
    if (!test_multi(jj)) {
      System.Int32 RNTRNTRNT_258 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(453);
      return RNTRNTRNT_258;
    }
    System.Int32 RNTRNTRNT_259 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(454);
    return RNTRNTRNT_259;
  }
}
