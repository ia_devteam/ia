using System;
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, AllowMultiple = true)]
public class SimpleAttribute : Attribute
{
  string name = null;
  public string MyNamedArg;
  private string secret;
  public SimpleAttribute(string name)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(350);
    this.name = name;
    IACSharpSensor.IACSharpSensor.SensorReached(351);
  }
  public string AnotherArg {
    get {
      System.String RNTRNTRNT_185 = secret;
      IACSharpSensor.IACSharpSensor.SensorReached(352);
      return RNTRNTRNT_185;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(353);
      secret = value;
      IACSharpSensor.IACSharpSensor.SensorReached(354);
    }
  }
  public long LongValue {
    get {
      System.Int64 RNTRNTRNT_186 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(355);
      return RNTRNTRNT_186;
    }
    set { }
  }
  public long[] ArrayValue {
    get {
      System.Int64[] RNTRNTRNT_187 = new long[0];
      IACSharpSensor.IACSharpSensor.SensorReached(356);
      return RNTRNTRNT_187;
    }
    set { }
  }
  public object D;
}
[Simple("Interface test")]
public interface IFoo
{
  void MethodOne(int x, int y);
  bool MethodTwo(float x, float y);
}
[Simple("Fifth", D = new double[] { -1 })]
class Blah2
{
}
[Simple("Fifth", D = new double[0])]
class Blah3
{
}
[Simple("Dummy", MyNamedArg = "Dude!")]
[Simple("Vids", MyNamedArg = "Raj", AnotherArg = "Foo")]
[Simple("Trip", LongValue = 0)]
[Simple("Fourth", ArrayValue = new long[] { 0 })]
public class Blah
{
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(357);
    object o = (((SimpleAttribute)typeof(Blah2).GetCustomAttributes(typeof(SimpleAttribute), false)[0]).D);
    if (o.ToString() != "System.Double[]") {
      System.Int32 RNTRNTRNT_188 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(358);
      return RNTRNTRNT_188;
    }
    if (((double[])o)[0].GetType() != typeof(double)) {
      System.Int32 RNTRNTRNT_189 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(359);
      return RNTRNTRNT_189;
    }
    o = (((SimpleAttribute)typeof(Blah3).GetCustomAttributes(typeof(SimpleAttribute), false)[0]).D);
    if (o.ToString() != "System.Double[]") {
      System.Int32 RNTRNTRNT_190 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(360);
      return RNTRNTRNT_190;
    }
    Console.WriteLine("OK");
    System.Int32 RNTRNTRNT_191 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(361);
    return RNTRNTRNT_191;
  }
}
