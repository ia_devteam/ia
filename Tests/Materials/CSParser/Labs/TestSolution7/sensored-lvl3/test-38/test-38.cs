class X
{
  public int v1, v2;
  int y;
  public int this[int a] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(323);
      if (a == 0) {
        System.Int32 RNTRNTRNT_169 = v1;
        IACSharpSensor.IACSharpSensor.SensorReached(324);
        return RNTRNTRNT_169;
      } else {
        System.Int32 RNTRNTRNT_170 = v2;
        IACSharpSensor.IACSharpSensor.SensorReached(325);
        return RNTRNTRNT_170;
      }
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(326);
      if (a == 0) {
        v1 = value;
      } else {
        v2 = value;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(327);
    }
  }
  public int Foo()
  {
    System.Int32 RNTRNTRNT_171 = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(328);
    return RNTRNTRNT_171;
  }
  public int Bar {
    get {
      System.Int32 RNTRNTRNT_172 = y;
      IACSharpSensor.IACSharpSensor.SensorReached(329);
      return RNTRNTRNT_172;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(330);
      y = value;
      IACSharpSensor.IACSharpSensor.SensorReached(331);
    }
  }
}
class Y
{
  public uint v1, v2;
  uint y;
  public uint this[uint a] {
    get {
      IACSharpSensor.IACSharpSensor.SensorReached(332);
      if (a == 0) {
        System.UInt32 RNTRNTRNT_173 = v1;
        IACSharpSensor.IACSharpSensor.SensorReached(333);
        return RNTRNTRNT_173;
      } else {
        System.UInt32 RNTRNTRNT_174 = v2;
        IACSharpSensor.IACSharpSensor.SensorReached(334);
        return RNTRNTRNT_174;
      }
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(335);
      if (a == 0) {
        v1 = value;
      } else {
        v2 = value;
      }
      IACSharpSensor.IACSharpSensor.SensorReached(336);
    }
  }
  public uint Foo()
  {
    System.UInt32 RNTRNTRNT_175 = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(337);
    return RNTRNTRNT_175;
  }
  public uint Bar {
    get {
      System.UInt32 RNTRNTRNT_176 = y;
      IACSharpSensor.IACSharpSensor.SensorReached(338);
      return RNTRNTRNT_176;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(339);
      y = value;
      IACSharpSensor.IACSharpSensor.SensorReached(340);
    }
  }
}
class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(341);
    X x = new X();
    Y y = new Y();
    int b;
    x[0] = x[1] = 1;
    x[0] = 1;
    if (x.v1 != 1) {
      System.Int32 RNTRNTRNT_177 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(342);
      return RNTRNTRNT_177;
    }
    if (x[0] != 1) {
      System.Int32 RNTRNTRNT_178 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(343);
      return RNTRNTRNT_178;
    }
    double d;
    long l;
    d = l = b = x[0] = x[1] = x.Bar = x[2] = x[3] = x[4] = x.Foo();
    if (x.Bar != 8) {
      System.Int32 RNTRNTRNT_179 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(344);
      return RNTRNTRNT_179;
    }
    if (l != 8) {
      System.Int32 RNTRNTRNT_180 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(345);
      return RNTRNTRNT_180;
    }
    uint e, f;
    e = 5;
    e = f = 8;
    if (e != 8) {
      System.Int32 RNTRNTRNT_181 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(346);
      return RNTRNTRNT_181;
    }
    y[0] = y[1] = 9;
    y[0] = y.Bar = 12;
    if (y.Bar != 12) {
      System.Int32 RNTRNTRNT_182 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(347);
      return RNTRNTRNT_182;
    }
    y.Bar = 15;
    if (y.Bar != 15) {
      System.Int32 RNTRNTRNT_183 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(348);
      return RNTRNTRNT_183;
    }
    System.Int32 RNTRNTRNT_184 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(349);
    return RNTRNTRNT_184;
  }
}
