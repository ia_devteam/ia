using System;
public interface IVehicle
{
  int Start();
  int Stop();
  int Turn();
}
public class Base : IVehicle
{
  int IVehicle.Start()
  {
    System.Int32 RNTRNTRNT_957 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1342);
    return RNTRNTRNT_957;
  }
  public int Stop()
  {
    System.Int32 RNTRNTRNT_958 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(1343);
    return RNTRNTRNT_958;
  }
  public virtual int Turn()
  {
    System.Int32 RNTRNTRNT_959 = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(1344);
    return RNTRNTRNT_959;
  }
}
public class Derived1 : Base
{
  public override int Turn()
  {
    System.Int32 RNTRNTRNT_960 = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(1345);
    return RNTRNTRNT_960;
  }
}
public class Derived2 : Base, IVehicle
{
  public new int Stop()
  {
    System.Int32 RNTRNTRNT_961 = 6;
    IACSharpSensor.IACSharpSensor.SensorReached(1346);
    return RNTRNTRNT_961;
  }
  int IVehicle.Start()
  {
    System.Int32 RNTRNTRNT_962 = 5;
    IACSharpSensor.IACSharpSensor.SensorReached(1347);
    return RNTRNTRNT_962;
  }
  int IVehicle.Turn()
  {
    System.Int32 RNTRNTRNT_963 = 7;
    IACSharpSensor.IACSharpSensor.SensorReached(1348);
    return RNTRNTRNT_963;
  }
  public override int Turn()
  {
    System.Int32 RNTRNTRNT_964 = 8;
    IACSharpSensor.IACSharpSensor.SensorReached(1349);
    return RNTRNTRNT_964;
  }
}
public class Test
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1350);
    Derived1 d1 = new Derived1();
    Derived2 d2 = new Derived2();
    Base b1 = d1;
    Base b2 = d2;
    if (d1.Turn() != 4) {
      System.Int32 RNTRNTRNT_965 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1351);
      return RNTRNTRNT_965;
    }
    if (((IVehicle)d1).Turn() != 4) {
      System.Int32 RNTRNTRNT_966 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1352);
      return RNTRNTRNT_966;
    }
    if (((IVehicle)d2).Turn() != 7) {
      System.Int32 RNTRNTRNT_967 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1353);
      return RNTRNTRNT_967;
    }
    if (b2.Turn() != 8) {
      System.Int32 RNTRNTRNT_968 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1354);
      return RNTRNTRNT_968;
    }
    if (((IVehicle)b2).Turn() != 7) {
      System.Int32 RNTRNTRNT_969 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(1355);
      return RNTRNTRNT_969;
    }
    System.Int32 RNTRNTRNT_970 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1356);
    return RNTRNTRNT_970;
  }
}
