using System;
class X
{
  static int test_explicit()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1064);
    object x_int = 1;
    object x_uint_1 = 1u;
    object x_uint_2 = 1u;
    object x_long_1 = 1L;
    object x_long_2 = 1L;
    object x_ulong_1 = 1uL;
    object x_ulong_2 = 1uL;
    object x_ulong_3 = 1uL;
    object x_ulong_4 = 1uL;
    object x_ulong_5 = 1uL;
    if (!(x_int is int)) {
      System.Int32 RNTRNTRNT_784 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1065);
      return RNTRNTRNT_784;
    }
    if (!(x_uint_1 is uint)) {
      System.Int32 RNTRNTRNT_785 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1066);
      return RNTRNTRNT_785;
    }
    if (!(x_uint_2 is uint)) {
      System.Int32 RNTRNTRNT_786 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1067);
      return RNTRNTRNT_786;
    }
    if (!(x_long_1 is long)) {
      System.Int32 RNTRNTRNT_787 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(1068);
      return RNTRNTRNT_787;
    }
    if (!(x_long_2 is long)) {
      System.Int32 RNTRNTRNT_788 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(1069);
      return RNTRNTRNT_788;
    }
    if (!(x_ulong_1 is ulong)) {
      System.Int32 RNTRNTRNT_789 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(1070);
      return RNTRNTRNT_789;
    }
    if (!(x_ulong_2 is ulong)) {
      System.Int32 RNTRNTRNT_790 = 8;
      IACSharpSensor.IACSharpSensor.SensorReached(1071);
      return RNTRNTRNT_790;
    }
    if (!(x_ulong_3 is ulong)) {
      System.Int32 RNTRNTRNT_791 = 9;
      IACSharpSensor.IACSharpSensor.SensorReached(1072);
      return RNTRNTRNT_791;
    }
    if (!(x_ulong_4 is ulong)) {
      System.Int32 RNTRNTRNT_792 = 10;
      IACSharpSensor.IACSharpSensor.SensorReached(1073);
      return RNTRNTRNT_792;
    }
    if (!(x_ulong_5 is ulong)) {
      System.Int32 RNTRNTRNT_793 = 11;
      IACSharpSensor.IACSharpSensor.SensorReached(1074);
      return RNTRNTRNT_793;
    }
    System.Int32 RNTRNTRNT_794 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1075);
    return RNTRNTRNT_794;
  }
  static int test_implicit()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1076);
    object i_int = 1;
    object i_uint = 0x80000000u;
    object i_long = 0x100000000L;
    object i_ulong = 0x8000000000000000uL;
    if (!(i_int is int)) {
      System.Int32 RNTRNTRNT_795 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1077);
      return RNTRNTRNT_795;
    }
    if (!(i_uint is uint)) {
      System.Int32 RNTRNTRNT_796 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1078);
      return RNTRNTRNT_796;
    }
    if (!(i_long is long)) {
      System.Int32 RNTRNTRNT_797 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1079);
      return RNTRNTRNT_797;
    }
    if (!(i_ulong is ulong)) {
      System.Int32 RNTRNTRNT_798 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1080);
      return RNTRNTRNT_798;
    }
    System.Int32 RNTRNTRNT_799 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1081);
    return RNTRNTRNT_799;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1082);
    int v;
    v = test_explicit();
    if (v != 0) {
      System.Int32 RNTRNTRNT_800 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(1083);
      return RNTRNTRNT_800;
    }
    v = test_implicit();
    if (v != 0) {
      System.Int32 RNTRNTRNT_801 = 20 + v;
      IACSharpSensor.IACSharpSensor.SensorReached(1084);
      return RNTRNTRNT_801;
    }
    ulong l = 1;
    if (l != 0L) {
      ;
    }
    ulong myulog = 0L;
    Console.WriteLine("Tests pass");
    System.Int32 RNTRNTRNT_802 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1085);
    return RNTRNTRNT_802;
  }
}
