using System;
class X
{
  static void Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1212);
    MethodSignature ms = new MethodSignature("hello", null, null);
    Console.WriteLine("About to look for: " + ms.Name);
    IACSharpSensor.IACSharpSensor.SensorReached(1213);
  }
}
struct MethodSignature
{
  public string Name;
  public Type RetType;
  public Type[] Parameters;
  public MethodSignature(string name, Type ret_type, Type[] parameters)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1214);
    Name = name;
    RetType = ret_type;
    Parameters = parameters;
    IACSharpSensor.IACSharpSensor.SensorReached(1215);
  }
}
