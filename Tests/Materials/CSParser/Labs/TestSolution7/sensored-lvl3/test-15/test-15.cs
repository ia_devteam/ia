using System;
interface Iface
{
  int A();
}
class Implementor : Iface
{
  public int A()
  {
    System.Int32 RNTRNTRNT_25 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(58);
    return RNTRNTRNT_25;
  }
}
struct StructImplementor : Iface
{
  public int A()
  {
    System.Int32 RNTRNTRNT_26 = 2;
    IACSharpSensor.IACSharpSensor.SensorReached(59);
    return RNTRNTRNT_26;
  }
}
class Run
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(60);
    Iface iface;
    Implementor i = new Implementor();
    iface = i;
    if (iface.A() != 1) {
      System.Int32 RNTRNTRNT_27 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(61);
      return RNTRNTRNT_27;
    }
    StructImplementor s = new StructImplementor();
    Iface xiface = (Iface)s;
    if (xiface.A() != 2) {
      System.Int32 RNTRNTRNT_28 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(62);
      return RNTRNTRNT_28;
    }
    System.Int32 RNTRNTRNT_29 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(63);
    return RNTRNTRNT_29;
  }
}
