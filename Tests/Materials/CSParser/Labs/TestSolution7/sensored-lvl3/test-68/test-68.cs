using System;
enum A
{
  Hello
}
class Y
{
  public Y()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1145);
    value = 3;
    IACSharpSensor.IACSharpSensor.SensorReached(1146);
  }
  public int value;
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1147);
    if ("Hello" != A.Hello.ToString()) {
      System.Int32 RNTRNTRNT_835 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1148);
      return RNTRNTRNT_835;
    }
    Console.WriteLine("value is: " + (5.ToString()));
    if (5.ToString() != "5") {
      System.Int32 RNTRNTRNT_836 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1149);
      return RNTRNTRNT_836;
    }
    Y y = new Y();
    if (y.value.ToString() != "3") {
      string x = y.value.ToString();
      Console.WriteLine("Got: {0} expected 3", x);
      System.Int32 RNTRNTRNT_837 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1150);
      return RNTRNTRNT_837;
    }
    Console.WriteLine("Test ok");
    System.Int32 RNTRNTRNT_838 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1151);
    return RNTRNTRNT_838;
  }
}
