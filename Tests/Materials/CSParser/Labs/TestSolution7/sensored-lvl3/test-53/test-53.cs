using System;
using System.IO;
class MyDispose : IDisposable
{
  public bool disposed;
  public void Dispose()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(998);
    disposed = true;
    IACSharpSensor.IACSharpSensor.SensorReached(999);
  }
}
class X
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1000);
    MyDispose copy_a, copy_b, copy_c;
    using (MyDispose a = new MyDispose(), b = new MyDispose()) {
      copy_a = a;
      copy_b = b;
    }
    if (!copy_a.disposed) {
      System.Int32 RNTRNTRNT_756 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1001);
      return RNTRNTRNT_756;
    }
    if (!copy_b.disposed) {
      System.Int32 RNTRNTRNT_757 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1002);
      return RNTRNTRNT_757;
    }
    Console.WriteLine("Nested using clause disposed");
    copy_c = null;
    try {
      using (MyDispose c = new MyDispose()) {
        copy_c = c;
        throw new Exception();
      }
    } catch {
    }
    if (!copy_c.disposed) {
      System.Int32 RNTRNTRNT_758 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1003);
      return RNTRNTRNT_758;
    } else {
      Console.WriteLine("Disposal on finally block works");
    }
    using (MyDispose d = null) {
    }
    Console.WriteLine("Null test passed");
    MyDispose bb = new MyDispose();
    using (bb) {
    }
    if (bb.disposed == false) {
      System.Int32 RNTRNTRNT_759 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(1004);
      return RNTRNTRNT_759;
    }
    Console.WriteLine("All tests pass");
    System.Int32 RNTRNTRNT_760 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1005);
    return RNTRNTRNT_760;
  }
}
