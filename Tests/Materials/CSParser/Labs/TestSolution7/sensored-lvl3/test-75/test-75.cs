class X
{
  public static bool called = false;
  public static X operator +(X a, X b)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1228);
    called = true;
    IACSharpSensor.IACSharpSensor.SensorReached(1229);
    return null;
  }
}
class Y : X
{
}
class Z : Y
{
}
class driver
{
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1230);
    Z a = new Z();
    Z b = new Z();
    X c = a + b;
    if (X.called) {
      System.Int32 RNTRNTRNT_888 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1231);
      return RNTRNTRNT_888;
    }
    System.Int32 RNTRNTRNT_889 = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1232);
    return RNTRNTRNT_889;
  }
}
