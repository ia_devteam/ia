using System;
public class Blah
{
  public class Foo
  {
    public Foo()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(134);
      Console.WriteLine("Inside the Foo constructor now");
      IACSharpSensor.IACSharpSensor.SensorReached(135);
    }
    public int Bar(int i, int j)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(136);
      Console.WriteLine("The Bar method");
      System.Int32 RNTRNTRNT_62 = i + j;
      IACSharpSensor.IACSharpSensor.SensorReached(137);
      return RNTRNTRNT_62;
    }
  }
  public static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(138);
    Foo f = new Foo();
    int j = f.Bar(2, 3);
    Console.WriteLine("Blah.Foo.Bar returned " + j);
    if (j == 5) {
      System.Int32 RNTRNTRNT_63 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(139);
      return RNTRNTRNT_63;
    } else {
      System.Int32 RNTRNTRNT_64 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(140);
      return RNTRNTRNT_64;
    }
  }
}
