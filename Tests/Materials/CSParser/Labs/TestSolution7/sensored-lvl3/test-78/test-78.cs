namespace N1
{
  public enum A
  {
    A_1,
    A_2,
    A_3
  }
  public class B
  {
    static bool ShortCasting()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1240);
      short i = 0;
      N1.A a = N1.A.A_1;
      i = (short)a;
      a = (N1.A)i;
      if (a != N1.A.A_1) {
        System.Boolean RNTRNTRNT_895 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1241);
        return RNTRNTRNT_895;
      }
      System.Boolean RNTRNTRNT_896 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1242);
      return RNTRNTRNT_896;
    }
    static bool IntCasting()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1243);
      int i = 0;
      N1.A a = N1.A.A_1;
      i = (int)a;
      a = (N1.A)i;
      if (a != N1.A.A_1) {
        System.Boolean RNTRNTRNT_897 = false;
        IACSharpSensor.IACSharpSensor.SensorReached(1244);
        return RNTRNTRNT_897;
      }
      System.Boolean RNTRNTRNT_898 = true;
      IACSharpSensor.IACSharpSensor.SensorReached(1245);
      return RNTRNTRNT_898;
    }
    static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1246);
      if (!IntCasting()) {
        System.Int32 RNTRNTRNT_899 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1247);
        return RNTRNTRNT_899;
      }
      if (!ShortCasting()) {
        System.Int32 RNTRNTRNT_900 = 2;
        IACSharpSensor.IACSharpSensor.SensorReached(1248);
        return RNTRNTRNT_900;
      }
      System.Int32 RNTRNTRNT_901 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1249);
      return RNTRNTRNT_901;
    }
  }
}
