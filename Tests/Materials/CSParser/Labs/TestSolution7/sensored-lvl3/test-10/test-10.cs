using System;
class X
{
  void asbyte(byte a, ushort b, uint c, ulong d, char e)
  {
  }
  void bsbyte()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(3);
    sbyte s = 0;
    asbyte((byte)s, (ushort)s, (uint)s, (ulong)s, (char)s);
    asbyte(checked((byte)s), checked((ushort)s), checked((uint)s), checked((ulong)s), checked((char)s));
    IACSharpSensor.IACSharpSensor.SensorReached(4);
  }
  void abyte(sbyte a, char b)
  {
  }
  void bbyte()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(5);
    byte b = 0;
    abyte((sbyte)b, (char)b);
    abyte(checked((sbyte)b), checked((char)b));
    IACSharpSensor.IACSharpSensor.SensorReached(6);
  }
  void ashort(sbyte a, byte b, ushort c, uint d, ulong e, char f)
  {
  }
  void bshort()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(7);
    short a = 1;
    ashort((sbyte)a, (byte)a, (ushort)a, (uint)a, (ulong)a, (char)a);
    ashort(checked((sbyte)a), checked((byte)a), checked((ushort)a), checked((uint)a), checked((ulong)a), checked((char)a));
    IACSharpSensor.IACSharpSensor.SensorReached(8);
  }
  void aushort(sbyte a, byte b, short c, char d)
  {
  }
  void bushort()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(9);
    ushort a = 1;
    aushort((sbyte)a, (byte)a, (short)a, (char)a);
    aushort(checked((sbyte)a), checked((byte)a), checked((short)a), checked((char)a));
    IACSharpSensor.IACSharpSensor.SensorReached(10);
  }
  void aint(sbyte a, byte b, short c, ushort d, uint e, ulong f, char g)
  {
  }
  void bint()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(11);
    int a = 1;
    aint((sbyte)a, (byte)a, (short)a, (ushort)a, (uint)a, (ulong)a, (char)a);
    aint(checked((sbyte)a), checked((byte)a), checked((short)a), checked((ushort)a), checked((uint)a), checked((ulong)a), checked((char)a));
    IACSharpSensor.IACSharpSensor.SensorReached(12);
  }
  void auint(sbyte a, byte b, short c, ushort d, int e, char f)
  {
  }
  void buint()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(13);
    uint a = 1;
    auint((sbyte)a, (byte)a, (short)a, (ushort)a, (int)a, (char)a);
    auint(checked((sbyte)a), checked((byte)a), checked((short)a), checked((ushort)a), checked((int)a), checked((char)a));
    IACSharpSensor.IACSharpSensor.SensorReached(14);
  }
  void along(sbyte a, byte b, short c, ushort d, int e, uint f, ulong g, char h)
  {
  }
  void blong()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(15);
    long a = 1;
    along((sbyte)a, (byte)a, (short)a, (ushort)a, (int)a, (uint)a, (ulong)a, (char)a);
    along(checked((sbyte)a), checked((byte)a), checked((short)a), checked((ushort)a), checked((int)a), checked((uint)a), checked((ulong)a), checked((char)a));
    IACSharpSensor.IACSharpSensor.SensorReached(16);
  }
  void aulong(sbyte a, byte b, short c, ushort d, int e, uint f, long g, char h)
  {
  }
  void bulong()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(17);
    ulong a = 1;
    aulong((sbyte)a, (byte)a, (short)a, (ushort)a, (int)a, (uint)a, (long)a, (char)a);
    aulong(checked((sbyte)a), checked((byte)a), checked((short)a), checked((ushort)a), checked((int)a), checked((uint)a), checked((long)a), checked((char)a));
    IACSharpSensor.IACSharpSensor.SensorReached(18);
  }
  void achar(sbyte a, byte b, short c)
  {
  }
  void bchar()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(19);
    char a = (char)1;
    achar((sbyte)a, (byte)a, (short)a);
    achar(checked((sbyte)a), checked((byte)a), checked((short)a));
    IACSharpSensor.IACSharpSensor.SensorReached(20);
  }
  void afloat(sbyte a, byte b, short c, ushort d, int e, uint f, long ll, ulong g, char h, decimal dd)
  {
  }
  void bfloat()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(21);
    float a = 1;
    afloat((sbyte)a, (byte)a, (short)a, (ushort)a, (int)a, (uint)a, (long)a, (ulong)a, (char)a, (decimal)a);
    afloat(checked((sbyte)a), checked((byte)a), checked((short)a), checked((ushort)a), checked((int)a), checked((uint)a), checked((long)a), checked((ulong)a), checked((char)a), checked((decimal)a));
    IACSharpSensor.IACSharpSensor.SensorReached(22);
  }
  void adouble(sbyte a, byte b, short c, ushort d, int e, uint f, long ll, ulong g, char h, float ff,
  decimal dd)
  {
  }
  void bdouble()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(23);
    double a = 1;
    adouble((sbyte)a, (byte)a, (short)a, (ushort)a, (int)a, (uint)a, (long)a, (ulong)a, (char)a, (float)a,
    (decimal)a);
    adouble(checked((sbyte)a), checked((byte)a), checked((short)a), checked((ushort)a), checked((int)a), checked((uint)a), checked((long)a), checked((ulong)a), checked((char)a), checked((float)a),
    (decimal)a);
    IACSharpSensor.IACSharpSensor.SensorReached(24);
  }
  void TestDecimal(decimal d)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(25);
    double dec = (double)d;
    decimal dec2 = (decimal)dec;
    IACSharpSensor.IACSharpSensor.SensorReached(26);
  }
  static void Main()
  {
  }
}
enum E : byte
{
  Min = 9
}
class Test2
{
  void ExtraTst()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(27);
    E error = E.Min - 9;
    string s = (string)null;
    const decimal d = -10.1m;
    const long l = (long)d;
    char ch = (char)E.Min;
    bool b = (DBNull)null == null;
    IACSharpSensor.IACSharpSensor.SensorReached(28);
  }
}
