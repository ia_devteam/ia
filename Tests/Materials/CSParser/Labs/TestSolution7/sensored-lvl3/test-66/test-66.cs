using System;
struct A
{
  public int a;
}
class Y
{
  public object a;
}
class X
{
  static A[] a_single = new A[10];
  static A[,] a_double = new A[10, 10];
  static Y[] o_single = new Y[10];
  static Y[,] o_double = new Y[10, 10];
  static void FillOne()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1117);
    a_single[0].a = 1;
    IACSharpSensor.IACSharpSensor.SensorReached(1118);
  }
  static void FillSingle()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1119);
    int i;
    for (i = 0; i < 10; i++) {
      a_single[i].a = i + 1;
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1120);
  }
  static void FillDouble()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1121);
    int i, j;
    for (i = 0; i < 10; i++) {
      for (j = 0; j < 10; j++) {
        a_double[i, j].a = i * j;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1122);
  }
  static void FillObject()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1123);
    int i;
    for (i = 0; i < 10; i++) {
      o_single[i] = new Y();
      o_single[i].a = (i + 1);
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1124);
  }
  static void FillDoubleObject()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1125);
    int i, j;
    for (i = 0; i < 10; i++) {
      for (j = 0; j < 10; j++) {
        o_double[i, j] = new Y();
        o_double[i, j].a = i * j;
      }
    }
    IACSharpSensor.IACSharpSensor.SensorReached(1126);
  }
  static int TestSingle()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1127);
    int i;
    for (i = 0; i < 10; i++) {
      if (a_single[i].a != i + 1) {
        System.Int32 RNTRNTRNT_822 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1128);
        return RNTRNTRNT_822;
      }
    }
    System.Int32 RNTRNTRNT_823 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1129);
    return RNTRNTRNT_823;
  }
  static int TestDouble()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1130);
    int i, j;
    for (i = 0; i < 10; i++) {
      for (j = 0; j < 10; j++) {
        if (a_double[i, j].a != (i * j)) {
          System.Int32 RNTRNTRNT_824 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(1131);
          return RNTRNTRNT_824;
        }
      }
    }
    System.Int32 RNTRNTRNT_825 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1132);
    return RNTRNTRNT_825;
  }
  static int TestObjectSingle()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1133);
    int i;
    for (i = 0; i < 10; i++) {
      if ((int)(o_single[i].a) != i + 1) {
        System.Int32 RNTRNTRNT_826 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1134);
        return RNTRNTRNT_826;
      }
    }
    System.Int32 RNTRNTRNT_827 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1135);
    return RNTRNTRNT_827;
  }
  static int TestObjectDouble()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1136);
    int i, j;
    for (i = 0; i < 10; i++) {
      for (j = 0; j < 10; j++) {
        if (((int)o_double[i, j].a) != (i * j)) {
          System.Int32 RNTRNTRNT_828 = 2;
          IACSharpSensor.IACSharpSensor.SensorReached(1137);
          return RNTRNTRNT_828;
        }
      }
    }
    System.Int32 RNTRNTRNT_829 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1138);
    return RNTRNTRNT_829;
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(1139);
    FillSingle();
    FillDouble();
    FillObject();
    FillDoubleObject();
    if (TestSingle() != 0) {
      System.Int32 RNTRNTRNT_830 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1140);
      return RNTRNTRNT_830;
    }
    if (TestDouble() != 0) {
      System.Int32 RNTRNTRNT_831 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1141);
      return RNTRNTRNT_831;
    }
    if (TestObjectSingle() != 0) {
      System.Int32 RNTRNTRNT_832 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1142);
      return RNTRNTRNT_832;
    }
    if (TestObjectDouble() != 0) {
      System.Int32 RNTRNTRNT_833 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1143);
      return RNTRNTRNT_833;
    }
    Console.WriteLine("test passes");
    System.Int32 RNTRNTRNT_834 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(1144);
    return RNTRNTRNT_834;
  }
}
