using System;
class X
{
  static int Main(string[] args)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(34);
    string a = "hello";
    string b = "1";
    string c = a + b;
    string d = a + 1;
    string y;
    if (c != d) {
      System.Int32 RNTRNTRNT_5 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(35);
      return RNTRNTRNT_5;
    }
    if (d != (a + b)) {
      System.Int32 RNTRNTRNT_6 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(36);
      return RNTRNTRNT_6;
    }
    if (d != x(a, b)) {
      System.Int32 RNTRNTRNT_7 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(37);
      return RNTRNTRNT_7;
    }
    if (d != x(a, 1)) {
      System.Int32 RNTRNTRNT_8 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(38);
      return RNTRNTRNT_8;
    }
    y = c == d ? "equal" : "not-equal";
    if (y != "equal") {
      System.Int32 RNTRNTRNT_9 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(39);
      return RNTRNTRNT_9;
    }
    y = b == a ? "oops" : "nice";
    if (y != "nice") {
      System.Int32 RNTRNTRNT_10 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(40);
      return RNTRNTRNT_10;
    }
    string[] blah = { "A" + 'B' + "C" };
    if (blah[0] != "ABC") {
      System.Int32 RNTRNTRNT_11 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(41);
      return RNTRNTRNT_11;
    }
    Console.WriteLine(c);
    System.Int32 RNTRNTRNT_12 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(42);
    return RNTRNTRNT_12;
  }
  static string s(string a, int o)
  {
    System.String RNTRNTRNT_13 = a + o;
    IACSharpSensor.IACSharpSensor.SensorReached(43);
    return RNTRNTRNT_13;
  }
  static string x(string s, object o)
  {
    System.String RNTRNTRNT_14 = s + o;
    IACSharpSensor.IACSharpSensor.SensorReached(44);
    return RNTRNTRNT_14;
  }
}
