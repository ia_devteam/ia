using System;
namespace T
{
  public class T
  {
    static int method1(Type t, int val)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1289);
      Console.WriteLine("You passed in " + val);
      System.Int32 RNTRNTRNT_923 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(1290);
      return RNTRNTRNT_923;
    }
    static int method1(Type t, Type[] types)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1291);
      Console.WriteLine("Wrong method called !");
      System.Int32 RNTRNTRNT_924 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(1292);
      return RNTRNTRNT_924;
    }
    static int method2(Type t, int val)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1293);
      Console.WriteLine("MEthod2 : " + val);
      System.Int32 RNTRNTRNT_925 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(1294);
      return RNTRNTRNT_925;
    }
    static int method2(Type t, Type[] types)
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1295);
      Console.WriteLine("Correct one this time!");
      System.Int32 RNTRNTRNT_926 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(1296);
      return RNTRNTRNT_926;
    }
    public static int Main()
    {
      IACSharpSensor.IACSharpSensor.SensorReached(1297);
      int i = method1(null, 1);
      if (i != 1) {
        System.Int32 RNTRNTRNT_927 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1298);
        return RNTRNTRNT_927;
      }
      i = method2(null, null);
      if (i != 4) {
        System.Int32 RNTRNTRNT_928 = 1;
        IACSharpSensor.IACSharpSensor.SensorReached(1299);
        return RNTRNTRNT_928;
      }
      System.Int32 RNTRNTRNT_929 = 0;
      IACSharpSensor.IACSharpSensor.SensorReached(1300);
      return RNTRNTRNT_929;
    }
  }
}
