using System;
class X
{
  static int v;
  static X()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(148);
    v = 10;
    IACSharpSensor.IACSharpSensor.SensorReached(149);
  }
  public static int Value {
    get {
      System.Int32 RNTRNTRNT_69 = v;
      IACSharpSensor.IACSharpSensor.SensorReached(150);
      return RNTRNTRNT_69;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(151);
      v = value;
      IACSharpSensor.IACSharpSensor.SensorReached(152);
    }
  }
  static int Main()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(153);
    if (Value != 10) {
      System.Int32 RNTRNTRNT_70 = 1;
      IACSharpSensor.IACSharpSensor.SensorReached(154);
      return RNTRNTRNT_70;
    }
    Value = 4;
    if (Value != 4) {
      System.Int32 RNTRNTRNT_71 = 2;
      IACSharpSensor.IACSharpSensor.SensorReached(155);
      return RNTRNTRNT_71;
    }
    Y y = new Y("hello");
    if (y.Value != "hello") {
      System.Int32 RNTRNTRNT_72 = 3;
      IACSharpSensor.IACSharpSensor.SensorReached(156);
      return RNTRNTRNT_72;
    }
    y.Value = "goodbye";
    if (y.Value != "goodbye") {
      System.Int32 RNTRNTRNT_73 = 4;
      IACSharpSensor.IACSharpSensor.SensorReached(157);
      return RNTRNTRNT_73;
    }
    Z z = new Z();
    if (Z.IVal != 4) {
      System.Int32 RNTRNTRNT_74 = 5;
      IACSharpSensor.IACSharpSensor.SensorReached(158);
      return RNTRNTRNT_74;
    }
    Z.IVal = 10;
    if (Z.IVal != 10) {
      System.Int32 RNTRNTRNT_75 = 6;
      IACSharpSensor.IACSharpSensor.SensorReached(159);
      return RNTRNTRNT_75;
    }
    z.XVal = 23;
    if (z.XVal != 23) {
      System.Int32 RNTRNTRNT_76 = 7;
      IACSharpSensor.IACSharpSensor.SensorReached(160);
      return RNTRNTRNT_76;
    }
    System.Int32 RNTRNTRNT_77 = 0;
    IACSharpSensor.IACSharpSensor.SensorReached(161);
    return RNTRNTRNT_77;
  }
}
class Y
{
  string init;
  public Y(string s)
  {
    IACSharpSensor.IACSharpSensor.SensorReached(162);
    init = s;
    IACSharpSensor.IACSharpSensor.SensorReached(163);
  }
  public string Value {
    get {
      System.String RNTRNTRNT_78 = init;
      IACSharpSensor.IACSharpSensor.SensorReached(164);
      return RNTRNTRNT_78;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(165);
      init = value;
      IACSharpSensor.IACSharpSensor.SensorReached(166);
    }
  }
}
struct Z
{
  static int val;
  int xval;
  static Z()
  {
    IACSharpSensor.IACSharpSensor.SensorReached(167);
    val = 4;
    IACSharpSensor.IACSharpSensor.SensorReached(168);
  }
  public static int IVal {
    get {
      System.Int32 RNTRNTRNT_79 = val;
      IACSharpSensor.IACSharpSensor.SensorReached(169);
      return RNTRNTRNT_79;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(170);
      val = value;
      IACSharpSensor.IACSharpSensor.SensorReached(171);
    }
  }
  public int XVal {
    get {
      System.Int32 RNTRNTRNT_80 = xval;
      IACSharpSensor.IACSharpSensor.SensorReached(172);
      return RNTRNTRNT_80;
    }
    set {
      IACSharpSensor.IACSharpSensor.SensorReached(173);
      xval = value;
      IACSharpSensor.IACSharpSensor.SensorReached(174);
    }
  }
}
