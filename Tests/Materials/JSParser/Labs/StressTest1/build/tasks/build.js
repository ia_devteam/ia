function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open("GET", "http://localhost:8080/"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} module.exports = function ( grunt ) {
"use strict";
SensorRnt(70);var fs = require("fs"), requirejs = require("requirejs"), srcFolder = __dirname+"/../../src/", rdefineEnd = /\}\);[^}\w]*$/, config = {
baseUrl : "src",
name : "jquery",
out : "dist/jquery.js",
optimize : "none",
findNestedDependencies : true,
skipSemiColonInsertion : true,
wrap : {
startFile : "src/intro.js",
endFile : "src/outro.js"
},
paths : {
sizzle : "../external/sizzle/dist/sizzle"
},
rawText : {

},
onBuildWrite : convert
};
function convert ( name, path, contents ) {
var amdName;
SensorRnt(72);if (/.\/var\//.test(path))
{ 
contents = contents.replace(/define\([\w\W]*?return/ , "var "+(/var\/([\w-]+)/.exec(name)[1])+" =").replace(rdefineEnd , "");SensorRnt(73);
}
else
{
SensorRnt(74);if (/^sizzle$/.test(name))
{ 
contents = "var Sizzle =\n"+contents.replace(/\/\/\s*EXPOSE[\w\W]*\/\/\s*EXPOSE/ , "return Sizzle;");SensorRnt(75);
}
else
{ 
SensorRnt(76);if (name!=="jquery")
{ 
contents = contents.replace(/\s*return\s+[^\}]+(\}\);[^\w\}]*)$/ , "$1").replace(/\s*exports\.\w+\s*=\s*\w+;/g , "");SensorRnt(77);
}
SensorRnt(78);contents = contents.replace(/define\([^{]*?{/ , "").replace(rdefineEnd , "");
contents = contents.replace(/\/\*\s*ExcludeStart\s*\*\/[\w\W]*?\/\*\s*ExcludeEnd\s*\*\//ig , "").replace(/\/\/\s*BuildExclude\n\r?[\w\W]*?\n\r?/ig , "");
contents = contents.replace(/define\(\[[^\]]+\]\)[\W\n]+$/ , "");
}
SensorRnt(79);}
SensorRnt(80);if ((amdName = grunt.option("amd"))!=null&&/^exports\/amd$/.test(name))
{ 
SensorRnt(81);if (amdName)
{ 
SensorRnt(82);grunt.log.writeln("Naming jQuery with AMD name: "+amdName);
}
else
{ 
SensorRnt(83);grunt.log.writeln("AMD name now anonymous");
}
contents = contents.replace(/(\s*)"jquery"(\,\s*)/ , amdName?"$1\""+amdName+"\"$2":"");SensorRnt(84);
}
RNTRETRET = contents;
SensorRnt(85);return RNTRETRET;}
grunt.registerMultiTask("build" , "Concatenate source, remove sub AMD definitions, "+"(include/exclude modules with +/- flags), embed date/version" , function () {
SensorRnt(86);var flag, index, done = this.async(), flags = this.flags, optIn = flags["*"], name = this.data.dest, minimum = this.data.minimum, removeWith = this.data.removeWith, excluded = [ ], included = [ ], version = grunt.config("pkg.version"), excludeList = function ( list, prepend ) {
SensorRnt(102);if (list)
{ 
prepend = prepend?prepend+"/":"";
SensorRnt(103);list.forEach(function ( module ) {
SensorRnt(105);if (module==="var")
{ 
SensorRnt(106);excludeList(fs.readdirSync(srcFolder+prepend+module) , prepend+module);
SensorRnt(107);return;
}
SensorRnt(108);if (prepend)
{ 
SensorRnt(109);if (! (module = /([\w-\/]+)\.js$/.exec(module)))
{ 
SensorRnt(110);return;SensorRnt(111);
}
module = prepend+module[1];
}
SensorRnt(112);if (excluded.indexOf(module)===- 1)
{ 
SensorRnt(113);excluder("-"+module);
}});SensorRnt(114);
};SensorRnt(104);}, excluder = function ( flag ) {
SensorRnt(115);var m = /^(\+|\-|)([\w\/-]+)$/.exec(flag), exclude = m[1]==="-", module = m[2];
SensorRnt(116);if (exclude)
{ 
SensorRnt(117);if (minimum.indexOf(module)===- 1)
{ 
SensorRnt(118);if (excluded.indexOf(module)===- 1)
{ 
SensorRnt(119);grunt.log.writeln(flag);
excluded.push(module);
SensorRnt(120);try{ 
SensorRnt(121);excludeList(fs.readdirSync(srcFolder+module) , module);
}
catch(e){ 
SensorRnt(122);grunt.verbose.writeln(e);
};SensorRnt(123);
}
SensorRnt(124);excludeList(removeWith[module]);
}
else
{ 
SensorRnt(125);grunt.log.error("Module \""+module+"\" is a minimum requirement.");
SensorRnt(126);if (module==="selector")
{ 
SensorRnt(127);grunt.log.error("If you meant to replace Sizzle, use -sizzle instead.");
};SensorRnt(128);
};SensorRnt(129);
}
else
{ 
SensorRnt(130);grunt.log.writeln(flag);
included.push(module);
};SensorRnt(131);};
SensorRnt(87);if (process.env.COMMIT)
{ 
SensorRnt(88);version += " "+process.env.COMMIT;
}
delete flags["*"];
SensorRnt(89);for(flag in flags){ 
SensorRnt(90);excluder(flag);
}
SensorRnt(91);if ((index = excluded.indexOf("sizzle")) > - 1)
{ 
config.rawText.selector = "define(['./selector-native']);";
SensorRnt(92);excluded.splice(index , 1);
}
SensorRnt(93);if ((index = excluded.indexOf("exports/global")) > - 1)
{ 
config.rawText["exports/global"] = "define(['../core'],"+"function( jQuery ) {\njQuery.noConflict = function() {};\n});";
SensorRnt(94);excluded.splice(index , 1);
}
SensorRnt(95);grunt.verbose.writeflags(excluded , "Excluded");
grunt.verbose.writeflags(included , "Included");
SensorRnt(96);if (excluded.length)
{ 
SensorRnt(97);version += " -"+excluded.join(",-");
grunt.config.set("pkg.version" , version);
grunt.verbose.writeln("Version changed to "+version);
config.excludeShallow = excluded;
}
config.include = included;
config.out = function ( compiled ) {
SensorRnt(132);compiled = compiled.replace(/@VERSION/g , version).replace(/@DATE/g , (new Date()).toISOString().replace(/:\d+\.\d+Z$/ , "Z"));
grunt.file.write(name , compiled);SensorRnt(133);};
SensorRnt(98);if (! optIn)
{ 
config.rawText.jquery = "define(["+(included.length?included.join(","):"")+"]);";SensorRnt(99);
}
SensorRnt(100);requirejs.optimize(config , function ( response ) {
SensorRnt(134);grunt.verbose.writeln(response);
grunt.log.ok("File '"+name+"' created.");
done();SensorRnt(135);} , function ( err ) {
SensorRnt(136);done(err);SensorRnt(137);});SensorRnt(101);});
grunt.registerTask("custom" , function () {
SensorRnt(138);var args = this.args, modules = args.length?args[0].replace(/,/g , ":"):"";
grunt.log.writeln("Creating custom build...\n");
grunt.task.run([ "build:*:*"+(modules?":"+modules:""), "uglify", "dist"]);SensorRnt(139);});};
SensorRnt(71);