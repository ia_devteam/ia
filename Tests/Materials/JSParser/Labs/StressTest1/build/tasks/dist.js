function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open("GET", "http://localhost:8080/"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} module.exports = function ( grunt ) {
"use strict";
SensorRnt(140);var fs = require("fs"), distpaths = [ "dist/jquery.js", "dist/jquery.min.map", "dist/jquery.min.js"];
grunt.registerTask("dist" , function () {
var stored, flags, paths, nonascii;
SensorRnt(142);stored = Object.keys(grunt.config("dst"));
flags = Object.keys(this.flags);
paths = [ ].concat(stored , flags).filter(function ( path ) {
RNTRETRET = path!=="*";
SensorRnt(144);return RNTRETRET;});
nonascii = false;
distpaths.forEach(function ( filename ) {
SensorRnt(145);var i, c, text = fs.readFileSync(filename , "utf8");
SensorRnt(146);if (/\x0d\x0a/.test(text))
{ 
SensorRnt(147);grunt.log.writeln(filename+": Incorrect line endings (\\r\\n)");
nonascii = true;
}
SensorRnt(148);if (text.length!==Buffer.byteLength(text , "utf8"))
{ 
SensorRnt(149);grunt.log.writeln(filename+": Non-ASCII characters detected:");
SensorRnt(150);for(i = 0; i < text.length; i++){ 
SensorRnt(151);c = text.charCodeAt(i);
SensorRnt(152);if (c > 127)
{ 
SensorRnt(153);grunt.log.writeln("- position "+i+": "+c);
grunt.log.writeln("-- "+text.substring(i-20 , i+20));
SensorRnt(154);break;
};SensorRnt(155);
}
SensorRnt(156);nonascii = true;
}
SensorRnt(157);paths.forEach(function ( path ) {
var created;
SensorRnt(159);if (! /\/$/.test(path))
{ 
SensorRnt(160);path += "/";
}
SensorRnt(161);created = path+filename.replace("dist/" , "");
grunt.file.write(created , text);
grunt.log.writeln("File '"+created+"' created.");SensorRnt(162);});SensorRnt(158);});
RNTRETRET = ! nonascii;
SensorRnt(143);return RNTRETRET;});};
SensorRnt(141);