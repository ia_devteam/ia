function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open("GET", "http://localhost:8080/"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} module.exports = function ( grunt ) {
"use strict";
SensorRnt(163);grunt.registerTask("testswarm" , function ( commit, configFile, projectName, browserSets,
			timeout ) {
SensorRnt(165);var jobName, config, tests, testswarm = require("testswarm"), runs = {

}, done = this.async(), pull = /PR-(\d+)/.exec(commit);
projectName = projectName||"jquery";
config = grunt.file.readJSON(configFile)[projectName];
browserSets = browserSets||config.browserSets;
SensorRnt(166);if (browserSets[0]==="[")
{ 
SensorRnt(167);browserSets = JSON.parse(browserSets);
}
timeout = timeout||1000* 60* 15;
SensorRnt(168);tests = grunt.config([ this.name, "tests"]);
SensorRnt(169);if (pull)
{ 
SensorRnt(170);jobName = "Pull <a href='https://github.com/jquery/jquery/pull/"+pull[1]+"'>#"+pull[1]+"</a>";
}
else
{ 
jobName = "Commit <a href='https://github.com/jquery/jquery/commit/"+commit+"'>"+commit.substr(0 , 10)+"</a>";SensorRnt(171);
}
SensorRnt(172);tests.forEach(function ( test ) {SensorRnt(174);
runs[test] = config.testUrl+commit+"/test/index.html?module="+test;});
testswarm.createClient({
url : config.swarmUrl
}).addReporter(testswarm.reporters.cli).auth({
id : config.authUsername,
token : config.authToken
}).addjob({
name : jobName,
runs : runs,
runMax : config.runMax,
browserSets : browserSets,
timeout : timeout
} , function ( err, passed ) {
SensorRnt(175);if (err)
{ 
SensorRnt(176);grunt.log.error(err);
}
SensorRnt(177);done(passed);SensorRnt(178);});SensorRnt(173);});};
SensorRnt(164);