function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open("GET", "http://localhost:8080/"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} "use strict";
SensorRnt(1610);if (typeof sinon=="undefined")
{ 
SensorRnt(1611);var sinon = {

};
}
(function (global) {
SensorRnt(1469);var timeoutResult = setTimeout(function () {SensorRnt(1475);} , 0);
var addTimerReturnsObject = typeof timeoutResult==='object';
clearTimeout(timeoutResult);
var id = 1;
function addTimer (args, recurring) {
SensorRnt(1476);if (args.length===0)
{ 
SensorRnt(1477);throw new Error("Function requires at least 1 parameter");
}
SensorRnt(1478);if (typeof args[0]==="undefined")
{ 
SensorRnt(1479);throw new Error("Callback must be provided to timer calls");
}
var toId = id++;
var delay = args[1]||0;
SensorRnt(1480);if (! this.timeouts)
{ 
SensorRnt(1481);this.timeouts = {

};
}
this.timeouts[toId] = {
id : toId,
func : args[0],
callAt : this.now+delay,
invokeArgs : Array.prototype.slice.call(args , 2)
};
SensorRnt(1482);if (recurring===true)
{ 
SensorRnt(1483);this.timeouts[toId].interval = delay;
}
SensorRnt(1484);if (addTimerReturnsObject)
{ 
RNTRETRET = {
id : toId,
ref : function () {SensorRnt(1488);},
unref : function () {SensorRnt(1489);}
};
SensorRnt(1485);return RNTRETRET;
}
else
{ 
RNTRETRET = toId;
SensorRnt(1486);return RNTRETRET;
};SensorRnt(1487);}
function parseTime (str) {
SensorRnt(1490);if (! str)
{ 
RNTRETRET = 0;
SensorRnt(1491);return RNTRETRET;
}
SensorRnt(1492);var strings = str.split(":");
var l = strings.length, i = l;
var ms = 0, parsed;
SensorRnt(1493);if (l > 3||! /^(\d\d:){0,2}\d\d?$/.test(str))
{ 
SensorRnt(1494);throw new Error("tick only understands numbers and 'h:m:s'");
}
SensorRnt(1495);while(i--){ 
parsed = parseInt(strings[i] , 10);
SensorRnt(1496);if (parsed >= 60)
{ 
SensorRnt(1497);throw new Error("Invalid time " + str);SensorRnt(1498);
}
ms += parsed* Math.pow(60 , (l-i-1));
}
RNTRETRET = ms* 1000;
SensorRnt(1499);return RNTRETRET;}
function createObject (object) {
var newObject;
SensorRnt(1500);if (Object.create)
{ 
SensorRnt(1501);newObject = Object.create(object);
}
else
{ 
var F = function () {SensorRnt(1504);};
F.prototype = object;
newObject = new F();SensorRnt(1502);
}
newObject.Date.clock = newObject;
RNTRETRET = newObject;
SensorRnt(1503);return RNTRETRET;}
sinon.clock = {
now : 0,
create : function create (now) {
var clock = createObject(this);
SensorRnt(1505);if (typeof now=="number")
{ 
SensorRnt(1506);clock.now = now;
}
SensorRnt(1507);if (! ! now&&typeof now=="object")
{ 
SensorRnt(1508);throw new TypeError("now should be milliseconds since UNIX epoch");
}
RNTRETRET = clock;
SensorRnt(1509);return RNTRETRET;},
setTimeout : function setTimeout (callback, timeout) {
RNTRETRET = addTimer.call(this , arguments , false);
SensorRnt(1510);return RNTRETRET;},
clearTimeout : function clearTimeout (timerId) {
SensorRnt(1511);if (! timerId)
{ 
SensorRnt(1512);return;
}
SensorRnt(1513);if (! this.timeouts)
{ 
SensorRnt(1514);this.timeouts = [ ];
}
SensorRnt(1515);if (typeof timerId==='object')
{ 
SensorRnt(1516);timerId = timerId.id
}
SensorRnt(1517);if (timerId in this.timeouts)
{ 
SensorRnt(1518);delete this.timeouts[timerId];
};SensorRnt(1519);},
setInterval : function setInterval (callback, timeout) {
RNTRETRET = addTimer.call(this , arguments , true);
SensorRnt(1520);return RNTRETRET;},
clearInterval : function clearInterval (timerId) {
SensorRnt(1521);this.clearTimeout(timerId);SensorRnt(1522);},
setImmediate : function setImmediate (callback) {
var passThruArgs = Array.prototype.slice.call(arguments , 1);
RNTRETRET = addTimer.call(this , [ callback, 0].concat(passThruArgs) , false);
SensorRnt(1523);return RNTRETRET;},
clearImmediate : function clearImmediate (timerId) {
SensorRnt(1524);this.clearTimeout(timerId);SensorRnt(1525);},
tick : function tick (ms) {
ms = typeof ms=="number"?ms:parseTime(ms);
var tickFrom = this.now, tickTo = this.now+ms, previous = this.now;
SensorRnt(1526);var timer = this.firstTimerInRange(tickFrom , tickTo);
var firstException;
SensorRnt(1527);while(timer&&tickFrom <= tickTo){ 
SensorRnt(1528);if (this.timeouts[timer.id])
{ 
tickFrom = this.now = timer.callAt;
SensorRnt(1529);try{ 
SensorRnt(1530);this.callTimer(timer);
}
catch(e){ 
SensorRnt(1531);firstException = firstException||e;
};SensorRnt(1532);
}
SensorRnt(1533);timer = this.firstTimerInRange(previous , tickTo);
previous = tickFrom;
}
this.now = tickTo;
SensorRnt(1534);if (firstException)
{ 
SensorRnt(1535);throw firstException;
}
RNTRETRET = this.now;
SensorRnt(1536);return RNTRETRET;},
firstTimerInRange : function (from, to) {
var timer, smallest = null, originalTimer;
SensorRnt(1537);for(var id in this.timeouts){ 
SensorRnt(1538);if (this.timeouts.hasOwnProperty(id))
{ 
SensorRnt(1539);if (this.timeouts[id].callAt < from||this.timeouts[id].callAt > to)
{ 
SensorRnt(1540);continue;
}
SensorRnt(1541);if (smallest===null||this.timeouts[id].callAt < smallest)
{ 
SensorRnt(1542);originalTimer = this.timeouts[id];
smallest = this.timeouts[id].callAt;
timer = {
func : this.timeouts[id].func,
callAt : this.timeouts[id].callAt,
interval : this.timeouts[id].interval,
id : this.timeouts[id].id,
invokeArgs : this.timeouts[id].invokeArgs
};
};SensorRnt(1543);
};SensorRnt(1544);
}
RNTRETRET = timer||null;
SensorRnt(1545);return RNTRETRET;},
callTimer : function (timer) {
SensorRnt(1546);if (typeof timer.interval=="number")
{ 
SensorRnt(1547);this.timeouts[timer.id].callAt += timer.interval;
}
else
{ 
SensorRnt(1548);delete this.timeouts[timer.id];
}
SensorRnt(1549);try{ 
SensorRnt(1550);if (typeof timer.func=="function")
{ 
SensorRnt(1551);timer.func.apply(null , timer.invokeArgs);
}
else
{ 
SensorRnt(1552);eval(timer.func);
};SensorRnt(1553);
}
catch(e){ 
SensorRnt(1554);var exception = e;
}
SensorRnt(1555);if (! this.timeouts[timer.id])
{ 
SensorRnt(1556);if (exception)
{ 
SensorRnt(1557);throw exception;
}
SensorRnt(1558);return;
}
SensorRnt(1559);if (exception)
{ 
SensorRnt(1560);throw exception;
};SensorRnt(1561);},
reset : function reset () {SensorRnt(1562);
this.timeouts = {

};},
Date : (function () {
var NativeDate = Date;
function ClockDate (year, month, date, hour, minute, second, ms) {
SensorRnt(1564);switch(arguments.length)
{
case 0:
RNTRETRET = new NativeDate(ClockDate.clock.now);
SensorRnt(1565);return RNTRETRET;
case 1:
RNTRETRET = new NativeDate(year);
SensorRnt(1566);return RNTRETRET;
case 2:
RNTRETRET = new NativeDate(year , month);
SensorRnt(1567);return RNTRETRET;
case 3:
RNTRETRET = new NativeDate(year , month , date);
SensorRnt(1568);return RNTRETRET;
case 4:
RNTRETRET = new NativeDate(year , month , date , hour);
SensorRnt(1569);return RNTRETRET;
case 5:
RNTRETRET = new NativeDate(year , month , date , hour , minute);
SensorRnt(1570);return RNTRETRET;
case 6:
RNTRETRET = new NativeDate(year , month , date , hour , minute , second);
SensorRnt(1571);return RNTRETRET;
default:
RNTRETRET = new NativeDate(year , month , date , hour , minute , second , ms);
SensorRnt(1572);return RNTRETRET;
};SensorRnt(1573);}
RNTRETRET = mirrorDateProperties(ClockDate , NativeDate);
SensorRnt(1563);return RNTRETRET;}())
};
function mirrorDateProperties (target, source) {
SensorRnt(1574);if (source.now)
{ 
SensorRnt(1575);target.now = function now () {
RNTRETRET = target.clock.now;
SensorRnt(1585);return RNTRETRET;};
}
else
{ 
SensorRnt(1576);delete target.now;
}
SensorRnt(1577);if (source.toSource)
{ 
SensorRnt(1578);target.toSource = function toSource () {
RNTRETRET = source.toSource();
SensorRnt(1586);return RNTRETRET;};
}
else
{ 
SensorRnt(1579);delete target.toSource;
}
target.toString = function toString () {
RNTRETRET = source.toString();
SensorRnt(1587);return RNTRETRET;};
target.prototype = source.prototype;
target.parse = source.parse;
target.UTC = source.UTC;
target.prototype.toUTCString = source.prototype.toUTCString;
SensorRnt(1580);for(var prop in source){ 
SensorRnt(1581);if (source.hasOwnProperty(prop))
{ 
SensorRnt(1582);target[prop] = source[prop];
};SensorRnt(1583);
}
RNTRETRET = target;
SensorRnt(1584);return RNTRETRET;}
var methods = [ "Date", "setTimeout", "setInterval", "clearTimeout", "clearInterval"];
SensorRnt(1470);if (typeof global.setImmediate!=="undefined")
{ 
SensorRnt(1471);methods.push("setImmediate");
}
SensorRnt(1472);if (typeof global.clearImmediate!=="undefined")
{ 
SensorRnt(1473);methods.push("clearImmediate");
}
function restore () {
var method;
SensorRnt(1588);for(var i = 0, l = this.methods.length; i < l; i++){ 
method = this.methods[i];
SensorRnt(1589);if (global[method].hadOwnProperty)
{ 
SensorRnt(1590);global[method] = this["_"+method];
}
else
{ 
SensorRnt(1591);try{ 
SensorRnt(1592);delete global[method];
}
catch(e){ 
};SensorRnt(1593);
};SensorRnt(1594);
}
this.methods = [ ];SensorRnt(1595);}
function stubGlobal (method, clock) {
clock[method].hadOwnProperty = Object.prototype.hasOwnProperty.call(global , method);
clock["_"+method] = global[method];
SensorRnt(1596);if (method=="Date")
{ 
SensorRnt(1597);var date = mirrorDateProperties(clock[method] , global[method]);
global[method] = date;
}
else
{ 
global[method] = function () {
RNTRETRET = clock[method].apply(clock , arguments);
SensorRnt(1604);return RNTRETRET;};
SensorRnt(1598);for(var prop in clock[method]){ 
SensorRnt(1599);if (clock[method].hasOwnProperty(prop))
{ 
SensorRnt(1600);global[method][prop] = clock[method][prop];
};SensorRnt(1601);
};SensorRnt(1602);
}
global[method].clock = clock;SensorRnt(1603);}
sinon.useFakeTimers = function useFakeTimers (now) {
var clock = sinon.clock.create(now);
clock.restore = restore;
clock.methods = Array.prototype.slice.call(arguments , typeof now=="number"?1:0);
SensorRnt(1605);if (clock.methods.length===0)
{ 
SensorRnt(1606);clock.methods = methods;
}
SensorRnt(1607);for(var i = 0, l = clock.methods.length; i < l; i++){ 
SensorRnt(1608);stubGlobal(clock.methods[i] , clock);
}
RNTRETRET = clock;
SensorRnt(1609);return RNTRETRET;};SensorRnt(1474);}(typeof global!="undefined"&&typeof global!=="function"?global:this));
sinon.timers = {
setTimeout : setTimeout,
clearTimeout : clearTimeout,
setImmediate : (typeof setImmediate!=="undefined"?setImmediate:undefined),
clearImmediate : (typeof clearImmediate!=="undefined"?clearImmediate:undefined),
setInterval : setInterval,
clearInterval : clearInterval,
Date : Date
};
SensorRnt(1612);if (typeof module!=='undefined'&&module.exports)
{ 
SensorRnt(1613);module.exports = sinon;
}
SensorRnt(1614);