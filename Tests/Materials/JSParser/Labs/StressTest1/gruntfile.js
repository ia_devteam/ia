function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open("GET", "http://localhost:8080/"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} module.exports = function ( grunt ) {
"use strict";
function readOptionalJSON ( filepath ) {
var data = {

};
SensorRnt(3);try{ 
SensorRnt(4);data = grunt.file.readJSON(filepath);
}
catch(e){ 
}
RNTRETRET = data;
SensorRnt(5);return RNTRETRET;}
SensorRnt(1);var gzip = require("gzip-js"), srcHintOptions = readOptionalJSON("src/.jshintrc");
delete srcHintOptions.onevar;
grunt.initConfig({
pkg : grunt.file.readJSON("package.json"),
dst : readOptionalJSON("dist/.destination.json"),
"compare_size" : {
files : [ "dist/jquery.js", "dist/jquery.min.js"],
options : {
compress : {
gz : function ( contents ) {
RNTRETRET = gzip.zip(contents , {

}).length;
SensorRnt(6);return RNTRETRET;}
},
cache : "build/.sizecache.json"
}
},
build : {
all : {
dest : "dist/jquery.js",
minimum : [ "core", "selector"],
removeWith : {
ajax : [ "manipulation/_evalUrl", "event/ajax"],
callbacks : [ "deferred"],
css : [ "effects", "dimensions", "offset"],
sizzle : [ "css/hiddenVisibleSelectors", "effects/animatedSelector"]
}
}
},
npmcopy : {
all : {
options : {
destPrefix : "external"
},
files : {
"sizzle/dist" : "sizzle/dist",
"sizzle/LICENSE.txt" : "sizzle/LICENSE.txt",
"qunit/qunit.js" : "qunitjs/qunit/qunit.js",
"qunit/qunit.css" : "qunitjs/qunit/qunit.css",
"qunit/MIT-LICENSE.txt" : "qunitjs/MIT-LICENSE.txt",
"requirejs/require.js" : "requirejs/require.js",
"sinon/fake_timers.js" : "sinon/lib/sinon/util/fake_timers.js",
"sinon/LICENSE.txt" : "sinon/LICENSE"
}
}
},
jsonlint : {
pkg : {
src : [ "package.json"]
},
bower : {
src : [ "bower.json"]
}
},
jshint : {
all : {
src : [ "src/**/*.js", "Gruntfile.js", "test/**/*.js", "build/**/*.js"],
options : {
jshintrc : true
}
},
dist : {
src : "dist/jquery.js",
options : srcHintOptions
}
},
jscs : {
src : "src/**/*.js",
gruntfile : "Gruntfile.js",
test : [ "test/data/testrunner.js"],
release : [ "build/*.js", "!build/release-notes.js"],
tasks : "build/tasks/*.js"
},
testswarm : {
tests : [ "ajax", "attributes", "callbacks", "core", "css", "data", "deferred", "dimensions", "effects", "event", "manipulation", "offset", "queue", "selector", "serialize", "support", "traversing"]
},
watch : {
files : [ "<%= jshint.all.src %>"],
tasks : "dev"
},
uglify : {
all : {
files : {
"dist/jquery.min.js" : [ "dist/jquery.js"]
},
options : {
preserveComments : false,
sourceMap : true,
sourceMapName : "dist/jquery.min.map",
report : "min",
beautify : {
"ascii_only" : true
},
banner : "/*! jQuery v<%= pkg.version %> | "+"(c) 2005, <%= grunt.template.today('yyyy') %> jQuery Foundation, Inc. | "+"jquery.org/license */",
compress : {
"hoist_funs" : false,
loops : false,
unused : false
}
}
}
}
});
require("load-grunt-tasks")(grunt);
grunt.loadTasks("build/tasks");
grunt.registerTask("lint" , [ "jshint", "jscs"]);
grunt.registerTask("dev" , [ "build:*:*", "lint"]);
grunt.registerTask("default" , [ "jsonlint", "dev", "uglify", "dist:*", "compare_size"]);};
SensorRnt(2);