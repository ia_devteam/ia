function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open("GET", "http://localhost:8080/"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} SensorRnt(8441);module("callbacks" , {
teardown : moduleTeardown
});
(function () {
var output, addToOutput = function ( string ) {
RNTRETRET = function () {SensorRnt(8380);
output += string;};
SensorRnt(8379);return RNTRETRET;SensorRnt(8377);}, outputA = addToOutput("A"), outputB = addToOutput("B"), outputC = addToOutput("C"), tests = {
"" : "XABC   X     XABCABCC  X  XBB X   XABA  X   XX",
"once" : "XABC   X     X         X  X   X   XABA  X   XX",
"memory" : "XABC   XABC  XABCABCCC XA XBB XB  XABA  XC  XX",
"unique" : "XABC   X     XABCA     X  XBB X   XAB   X   X",
"stopOnFalse" : "XABC   X     XABCABCC  X  XBB X   XA    X   XX",
"once memory" : "XABC   XABC  X         XA X   XA  XABA  XC  XX",
"once unique" : "XABC   X     X         X  X   X   XAB   X   X",
"once stopOnFalse" : "XABC   X     X         X  X   X   XA    X   XX",
"memory unique" : "XABC   XA    XABCA     XA XBB XB  XAB   XC  X",
"memory stopOnFalse" : "XABC   XABC  XABCABCCC XA XBB XB  XA    X   XX",
"unique stopOnFalse" : "XABC   X     XABCA     X  XBB X   XA    X   X"
}, filters = {
"no filter" : undefined,
"filter" : function ( fn ) {
RNTRETRET = function () {
RNTRETRET = fn.apply(this , arguments);
SensorRnt(8382);return RNTRETRET;};
SensorRnt(8381);return RNTRETRET;}
};
function showFlags ( flags ) {
SensorRnt(8383);if (typeof flags==="string")
{ 
RNTRETRET = "'"+flags+"'";
SensorRnt(8384);return RNTRETRET;
}
var output = [ ], key;
SensorRnt(8385);for(key in flags){ 
SensorRnt(8386);output.push("'"+key+"': "+flags[key]);
}
RNTRETRET = "{ "+output.join(", ")+" }";
SensorRnt(8387);return RNTRETRET;}
jQuery.each(tests , function ( strFlags, resultString ) {
var objectFlags = {

};
SensorRnt(8388);jQuery.each(strFlags.split(" ") , function () {
SensorRnt(8390);if (this.length)
{ 
SensorRnt(8391);objectFlags[this] = true;
};SensorRnt(8392);});
jQuery.each(filters , function ( filterLabel ) {
SensorRnt(8393);jQuery.each({
"string" : strFlags,
"object" : objectFlags
} , function ( flagsTypes, flags ) {
SensorRnt(8395);test("jQuery.Callbacks( "+showFlags(flags)+" ) - "+filterLabel , function () {
SensorRnt(8397);expect(21);
stop();
setTimeout(start , 0);
var cblist, results = resultString.split(/\s+/);
output = "X";
cblist = jQuery.Callbacks(flags);
cblist.add(function ( str ) {SensorRnt(8399);
output += str;});
cblist.fire("A");
strictEqual(output , "XA" , "Basic binding and firing");
strictEqual(cblist.fired() , true , ".fired() detects firing");
output = "X";
cblist.disable();
cblist.add(function ( str ) {SensorRnt(8400);
output += str;});
strictEqual(output , "X" , "Adding a callback after disabling");
cblist.fire("A");
strictEqual(output , "X" , "Firing after disabling");
cblist = jQuery.Callbacks(flags);
cblist.add(cblist.empty);
cblist.add(function () {
SensorRnt(8401);ok(false , "not emptied");SensorRnt(8402);});
cblist.fire();
cblist = jQuery.Callbacks(flags);
cblist.add(cblist.disable);
cblist.add(function () {
SensorRnt(8403);ok(false , "not disabled");SensorRnt(8404);});
cblist.fire();
output = "X";
cblist = jQuery.Callbacks(flags);
cblist.add(function () {
SensorRnt(8405);equal(this , window , "Basic binding and firing (context)");
output += Array.prototype.join.call(arguments , "");SensorRnt(8406);});
cblist.fireWith(window , [ "A", "B"]);
strictEqual(output , "XAB" , "Basic binding and firing (arguments)");
output = "";
cblist = jQuery.Callbacks(flags);
cblist.add(function () {
SensorRnt(8407);equal(this , window , "fireWith with no arguments (context is window)");
strictEqual(arguments.length , 0 , "fireWith with no arguments (no arguments)");SensorRnt(8408);});
cblist.fireWith();
output = "X";
cblist = jQuery.Callbacks(flags);
cblist.add(outputA , outputB , outputC);
cblist.remove(outputB , outputC);
cblist.fire();
strictEqual(output , "XA" , "Basic binding, removing and firing");
output = "X";
cblist = jQuery.Callbacks(flags);
cblist.add(outputA);
cblist.add(outputB);
cblist.add(outputC);
cblist.empty();
cblist.fire();
strictEqual(output , "X" , "Empty");
output = "X";
cblist = jQuery.Callbacks(flags);
cblist.add(function ( str ) {SensorRnt(8409);
output += str;});
cblist.lock();
cblist.add(function ( str ) {SensorRnt(8410);
output += str;});
cblist.fire("A");
cblist.add(function ( str ) {SensorRnt(8411);
output += str;});
strictEqual(output , "X" , "Lock early");
output = "X";
cblist = jQuery.Callbacks(flags);
cblist.add(function () {
SensorRnt(8412);cblist.add(outputC);
outputA();SensorRnt(8413);} , outputB);
cblist.fire();
strictEqual(output , results.shift() , "Proper ordering");
output = "X";
cblist.add(function () {
SensorRnt(8414);cblist.add(outputC);
outputA();SensorRnt(8415);} , outputB);
strictEqual(output , results.shift() , "Add after fire");
output = "X";
cblist.fire();
strictEqual(output , results.shift() , "Fire again");
output = "X";
cblist = jQuery.Callbacks(flags);
cblist.add(function ( str ) {SensorRnt(8416);
output += str;});
cblist.fire("A");
strictEqual(output , "XA" , "Multiple fire (first fire)");
output = "X";
cblist.add(function ( str ) {SensorRnt(8417);
output += str;});
strictEqual(output , results.shift() , "Multiple fire (first new callback)");
output = "X";
cblist.fire("B");
strictEqual(output , results.shift() , "Multiple fire (second fire)");
output = "X";
cblist.add(function ( str ) {SensorRnt(8418);
output += str;});
strictEqual(output , results.shift() , "Multiple fire (second new callback)");
output = "X";
cblist = jQuery.Callbacks(flags);
cblist.add(outputA , function () {
RNTRETRET = false;
SensorRnt(8419);return RNTRETRET;} , outputB);
cblist.add(outputA);
cblist.fire();
strictEqual(output , results.shift() , "Callback returning false");
output = "X";
cblist.add(outputC);
strictEqual(output , results.shift() , "Adding a callback after one returned false");
output = "";
function handler () {SensorRnt(8420);
output += "X";}
handler.method = function () {SensorRnt(8421);
output += "!";};
cblist = jQuery.Callbacks(flags);
cblist.add(handler);
cblist.add(handler);
cblist.fire();
strictEqual(output , results.shift() , "No callback iteration");SensorRnt(8398);});SensorRnt(8396);});SensorRnt(8394);});SensorRnt(8389);});SensorRnt(8378);})();
test("jQuery.Callbacks( options ) - options are copied" , function () {
SensorRnt(8422);expect(1);
var options = {
"unique" : true
}, cb = jQuery.Callbacks(options), count = 0, fn = function () {
SensorRnt(8424);ok(! (count++) , "called once");SensorRnt(8425);};
options["unique"] = false;
cb.add(fn , fn);
cb.fire();SensorRnt(8423);});
test("jQuery.Callbacks.fireWith - arguments are copied" , function () {
SensorRnt(8426);expect(1);
var cb = jQuery.Callbacks("memory"), args = [ "hello"];
cb.fireWith(null , args);
args[0] = "world";
cb.add(function ( hello ) {
SensorRnt(8428);strictEqual(hello , "hello" , "arguments are copied internally");SensorRnt(8429);});SensorRnt(8427);});
test("jQuery.Callbacks.remove - should remove all instances" , function () {
SensorRnt(8430);expect(1);
var cb = jQuery.Callbacks();
function fn () {
SensorRnt(8432);ok(false , "function wasn't removed");SensorRnt(8433);}
cb.add(fn , fn , function () {
ok(true , "end of test");}).remove(fn).fire();SensorRnt(8431);});
test("jQuery.Callbacks.has" , function () {
SensorRnt(8434);expect(13);
var cb = jQuery.Callbacks();
function getA () {
RNTRETRET = "A";
SensorRnt(8436);return RNTRETRET;}
function getB () {
RNTRETRET = "B";
SensorRnt(8437);return RNTRETRET;}
function getC () {
RNTRETRET = "C";
SensorRnt(8438);return RNTRETRET;}
cb.add(getA , getB , getC);
strictEqual(cb.has() , true , "No arguments to .has() returns whether callback function(s) are attached or not");
strictEqual(cb.has(getA) , true , "Check if a specific callback function is in the Callbacks list");
cb.remove(getB);
strictEqual(cb.has(getB) , false , "Remove a specific callback function and make sure its no longer there");
strictEqual(cb.has(getA) , true , "Remove a specific callback function and make sure other callback function is still there");
cb.empty();
strictEqual(cb.has() , false , "Empty list and make sure there are no callback function(s)");
strictEqual(cb.has(getA) , false , "Check for a specific function in an empty() list");
cb.add(getA , getB , function () {
strictEqual(cb.has() , true , "Check if list has callback function(s) from within a callback function");
strictEqual(cb.has(getA) , true , "Check if list has a specific callback from within a callback function");}).fire();
strictEqual(cb.has() , true , "Callbacks list has callback function(s) after firing");
cb.disable();
strictEqual(cb.has() , false , "disabled() list has no callback functions (returns false)");
strictEqual(cb.has(getA) , false , "Check for a specific function in a disabled() list");
cb = jQuery.Callbacks("unique");
cb.add(getA);
cb.add(getA);
strictEqual(cb.has() , true , "Check if unique list has callback function(s) attached");
cb.lock();
strictEqual(cb.has() , false , "locked() list is empty and returns false");SensorRnt(8435);});
test("jQuery.Callbacks() - adding a string doesn't cause a stack overflow" , function () {
SensorRnt(8439);expect(1);
jQuery.Callbacks().add("hello world");
ok(true , "no stack overflow");});
SensorRnt(8440);