function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open("GET", "http://localhost:8080/"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} SensorRnt(9364);module("event" , {
setup : function () {
SensorRnt(8870);document.body.focus();SensorRnt(8871);},
teardown : moduleTeardown
});
test("null or undefined handler" , function () {
SensorRnt(8872);expect(2);
SensorRnt(8873);try{ 
SensorRnt(8874);jQuery("#firstp").on("click" , null);
ok(true , "Passing a null handler will not throw an exception");
}
catch(e){ 
}
SensorRnt(8875);try{ 
SensorRnt(8876);jQuery("#firstp").on("click" , undefined);
ok(true , "Passing an undefined handler will not throw an exception");
}
catch(e){ 
};SensorRnt(8877);});
test("on() with non-null,defined data" , function () {
SensorRnt(8878);expect(2);
var handler = function ( event, data ) {
SensorRnt(8880);equal(data , 0 , "non-null, defined data (zero) is correctly passed");SensorRnt(8881);};
jQuery("#foo").on("foo.on" , handler);
jQuery("div").on("foo.delegate" , "#foo" , handler);
jQuery("#foo").trigger("foo" , 0);
jQuery("#foo").off("foo.on" , handler);
jQuery("div").off("foo.delegate" , "#foo");SensorRnt(8879);});
test("Handler changes and .trigger() order" , function () {
SensorRnt(8882);expect(1);
var markup = jQuery("<div><div><p><span><b class=\"a\">b</b></span></p></div></div>"), path = "";
markup.find("*").addBack().on("click" , function () {
path += this.nodeName.toLowerCase()+" ";}).filter("b").on("click" , function ( e ) {
SensorRnt(8884);if (e.target===this)
{ 
jQuery(this).parent().remove();SensorRnt(8885);
};SensorRnt(8886);});
markup.find("b").trigger("click");
equal(path , "b p div div " , "Delivered all events");
markup.remove();SensorRnt(8883);});
test("on(), with data" , function () {
SensorRnt(8887);expect(4);
var test, handler, handler2;
handler = function (event) {
SensorRnt(8889);ok(event.data , "on() with data, check passed data exists");
equal(event.data["foo"] , "bar" , "on() with data, Check value of passed data");SensorRnt(8890);};
jQuery("#firstp").on("click" , {
"foo" : "bar"
} , handler).trigger("click").off("click" , handler);
ok(! jQuery._data(jQuery("#firstp")[0] , "events") , "Event handler unbound when using data.");
test = function () {SensorRnt(8891);};
handler2 = function (event) {
SensorRnt(8892);equal(event.data , test , "on() with function data, Check value of passed data");SensorRnt(8893);};
jQuery("#firstp").on("click" , test , handler2).trigger("click").off("click" , handler2);SensorRnt(8888);});
test("click(), with data" , function () {
SensorRnt(8894);expect(3);
var handler = function (event) {
SensorRnt(8896);ok(event.data , "on() with data, check passed data exists");
equal(event.data["foo"] , "bar" , "on() with data, Check value of passed data");SensorRnt(8897);};
jQuery("#firstp").on("click" , {
"foo" : "bar"
} , handler).trigger("click").off("click" , handler);
ok(! jQuery._data(jQuery("#firstp")[0] , "events") , "Event handler unbound when using data.");SensorRnt(8895);});
test("on(), with data, trigger with data" , function () {
SensorRnt(8898);expect(4);
var handler = function (event, data) {
SensorRnt(8900);ok(event.data , "check passed data exists");
equal(event.data.foo , "bar" , "Check value of passed data");
ok(data , "Check trigger data");
equal(data.bar , "foo" , "Check value of trigger data");SensorRnt(8901);};
jQuery("#firstp").on("click" , {
foo : "bar"
} , handler).trigger("click" , [ {
bar : "foo"
}]).off("click" , handler);SensorRnt(8899);});
test("on(), multiple events at once" , function () {
SensorRnt(8902);expect(2);
var handler, clickCounter = 0, mouseoverCounter = 0;
handler = function (event) {
SensorRnt(8904);if (event.type==="click")
{ 
SensorRnt(8905);clickCounter += 1;
}
else
{
SensorRnt(8906);if (event.type==="mouseover")
{ 
SensorRnt(8907);mouseoverCounter += 1;
}
SensorRnt(8908);}SensorRnt(8909);};
jQuery("#firstp").on("click mouseover" , handler).trigger("click").trigger("mouseover");
equal(clickCounter , 1 , "on() with multiple events at once");
equal(mouseoverCounter , 1 , "on() with multiple events at once");SensorRnt(8903);});
test("on(), five events at once" , function () {
SensorRnt(8910);expect(1);
var count = 0, handler = function () {SensorRnt(8912);
count++;};
jQuery("#firstp").on("click mouseover foo bar baz" , handler).trigger("click").trigger("mouseover").trigger("foo").trigger("bar").trigger("baz");
equal(count , 5 , "on() five events at once");SensorRnt(8911);});
test("on(), multiple events at once and namespaces" , function () {
SensorRnt(8913);expect(7);
var cur, div, obj = {

};
div = jQuery("<div/>").on("focusin.a" , function (e) {
SensorRnt(8915);equal(e.type , cur , "Verify right single event was fired.");SensorRnt(8916);});
cur = "focusin";
div.trigger("focusin.a");
div.remove();
div = jQuery("<div/>").on("click mouseover" , obj , function (e) {
SensorRnt(8917);equal(e.type , cur , "Verify right multi event was fired.");
equal(e.data , obj , "Make sure the data came in correctly.");SensorRnt(8918);});
cur = "click";
div.trigger("click");
cur = "mouseover";
div.trigger("mouseover");
div.remove();
div = jQuery("<div/>").on("focusin.a focusout.b" , function (e) {
SensorRnt(8919);equal(e.type , cur , "Verify right multi event was fired.");SensorRnt(8920);});
cur = "focusin";
div.trigger("focusin.a");
cur = "focusout";
div.trigger("focusout.b");
div.remove();SensorRnt(8914);});
test("on(), namespace with special add" , function () {
SensorRnt(8921);expect(27);
var i = 0, div = jQuery("<div/>").appendTo("#qunit-fixture").on("test" , function () {
SensorRnt(8923);ok(true , "Test event fired.");SensorRnt(8924);});
jQuery.event.special["test"] = {
_default : function ( e, data ) {
SensorRnt(8925);equal(e.type , "test" , "Make sure we're dealing with a test event.");
ok(data , "And that trigger data was passed.");
strictEqual(e.target , div[0] , "And that the target is correct.");
equal(this , window , "And that the context is correct.");SensorRnt(8926);},
setup : function () {SensorRnt(8927);},
teardown : function () {
SensorRnt(8928);ok(true , "Teardown called.");SensorRnt(8929);},
add : function ( handleObj ) {SensorRnt(8930);
var handler = handleObj.handler;
handleObj.handler = function ( e ) {
e.xyz = ++ i;
SensorRnt(8931);handler.apply(this , arguments);SensorRnt(8932);};},
remove : function () {
SensorRnt(8933);ok(true , "Remove called.");SensorRnt(8934);}
};
div.on("test.a" , {
x : 1
} , function ( e ) {
SensorRnt(8935);ok(! ! e.xyz , "Make sure that the data is getting passed through.");
equal(e.data["x"] , 1 , "Make sure data is attached properly.");SensorRnt(8936);});
div.on("test.b" , {
x : 2
} , function ( e ) {
SensorRnt(8937);ok(! ! e.xyz , "Make sure that the data is getting passed through.");
equal(e.data["x"] , 2 , "Make sure data is attached properly.");SensorRnt(8938);});
div.trigger("test" , 33.33);
div.trigger("test.a" , "George Harrison");
div.trigger("test.b" , {
year : 1982
});
div.off("test");
div = jQuery("<div/>").on("test" , function () {
SensorRnt(8939);ok(true , "Test event fired.");SensorRnt(8940);});
div.appendTo("#qunit-fixture").remove();
delete jQuery.event.special["test"];SensorRnt(8922);});
test("on(), no data" , function () {
SensorRnt(8941);expect(1);
var handler = function (event) {
SensorRnt(8943);ok(! event.data , "Check that no data is added to the event object");SensorRnt(8944);};
jQuery("#firstp").on("click" , handler).trigger("click");SensorRnt(8942);});
test("on/one/off(Object)" , function () {
SensorRnt(8945);expect(6);
var $elem, clickCounter = 0, mouseoverCounter = 0;
function handler (event) {
SensorRnt(8947);if (event.type==="click")
{ 
SensorRnt(8948);clickCounter++;
}
else
{
SensorRnt(8949);if (event.type==="mouseover")
{ 
SensorRnt(8950);mouseoverCounter++;
}
SensorRnt(8951);}SensorRnt(8952);}
function handlerWithData (event) {
SensorRnt(8953);if (event.type==="click")
{ 
SensorRnt(8954);clickCounter += event.data;
}
else
{
SensorRnt(8955);if (event.type==="mouseover")
{ 
SensorRnt(8956);mouseoverCounter += event.data;
}
SensorRnt(8957);}SensorRnt(8958);}
function trigger () {
SensorRnt(8959);$elem.trigger("click").trigger("mouseover");SensorRnt(8960);}
$elem = jQuery("#firstp").on({
"click" : handler,
"mouseover" : handler
}).one({
"click" : handlerWithData,
"mouseover" : handlerWithData
} , 2);
trigger();
equal(clickCounter , 3 , "on(Object)");
equal(mouseoverCounter , 3 , "on(Object)");
trigger();
equal(clickCounter , 4 , "on(Object)");
equal(mouseoverCounter , 4 , "on(Object)");
jQuery("#firstp").off({
"click" : handler,
"mouseover" : handler
});
trigger();
equal(clickCounter , 4 , "on(Object)");
equal(mouseoverCounter , 4 , "on(Object)");SensorRnt(8946);});
test("on/off(Object), on/off(Object, String)" , function () {
SensorRnt(8961);expect(6);
var events, clickCounter = 0, mouseoverCounter = 0, $p = jQuery("#firstp"), $a = $p.find("a").eq(0);
events = {
"click" : function ( event ) {SensorRnt(8963);
clickCounter += (event.data||1);},
"mouseover" : function ( event ) {SensorRnt(8964);
mouseoverCounter += (event.data||1);}
};
function trigger () {
SensorRnt(8965);$a.trigger("click").trigger("mouseover");SensorRnt(8966);}
jQuery(document).on(events , "#firstp a");
$p.on(events , "a" , 2);
trigger();
equal(clickCounter , 3 , "on");
equal(mouseoverCounter , 3 , "on");
$p.off(events , "a");
trigger();
equal(clickCounter , 4 , "off");
equal(mouseoverCounter , 4 , "off");
jQuery(document).off(events , "#firstp a");
trigger();
equal(clickCounter , 4 , "off");
equal(mouseoverCounter , 4 , "off");SensorRnt(8962);});
test("on immediate propagation" , function () {
SensorRnt(8967);expect(2);
var lastClick, $p = jQuery("#firstp"), $a = $p.find("a").eq(0);
lastClick = "";
jQuery(document).on("click" , "#firstp a" , function (e) {
lastClick = "click1";
SensorRnt(8969);e.stopImmediatePropagation();SensorRnt(8970);});
jQuery(document).on("click" , "#firstp a" , function () {SensorRnt(8971);
lastClick = "click2";});
$a.trigger("click");
equal(lastClick , "click1" , "on stopImmediatePropagation");
jQuery(document).off("click" , "#firstp a");
lastClick = "";
$p.on("click" , "a" , function (e) {
lastClick = "click1";
SensorRnt(8972);e.stopImmediatePropagation();SensorRnt(8973);});
$p.on("click" , "a" , function () {SensorRnt(8974);
lastClick = "click2";});
$a.trigger("click");
equal(lastClick , "click1" , "on stopImmediatePropagation");
$p.off("click" , "**");SensorRnt(8968);});
test("on bubbling, isDefaultPrevented, stopImmediatePropagation" , function () {
SensorRnt(8975);expect(3);
var $anchor2 = jQuery("#anchor2"), $main = jQuery("#qunit-fixture"), neverCallMe = function () {
SensorRnt(8980);ok(false , "immediate propagation should have been stopped");SensorRnt(8981);}, fakeClick = function ($jq) {
SensorRnt(8982);var e = document.createEvent("MouseEvents");
e.initEvent("click" , true , true);
$jq[0].dispatchEvent(e);SensorRnt(8983);};
$anchor2.on("click" , function (e) {
SensorRnt(8984);e.preventDefault();SensorRnt(8985);});
$main.on("click" , "#foo" , function ( e ) {
SensorRnt(8986);equal(e.isDefaultPrevented() , true , "isDefaultPrevented true passed to bubbled event");SensorRnt(8987);});
fakeClick($anchor2);
$anchor2.off("click");
$main.off("click" , "**");
$anchor2.on("click" , function () {SensorRnt(8988);});
$main.on("click" , "#foo" , function (e) {
SensorRnt(8989);equal(e.isDefaultPrevented() , false , "isDefaultPrevented false passed to bubbled event");SensorRnt(8990);});
fakeClick($anchor2);
$anchor2.off("click");
$main.off("click" , "**");
SensorRnt(8976);if (/android 2\.3/i.test(navigator.userAgent))
{ 
SensorRnt(8977);ok(true , "Android 2.3, skipping native stopImmediatePropagation check");
}
else
{ 
SensorRnt(8978);$anchor2.on("click" , function ( e ) {
SensorRnt(8991);e.stopImmediatePropagation();
ok(true , "anchor was clicked and prop stopped");SensorRnt(8992);});
$anchor2[0].addEventListener("click" , neverCallMe , false);
fakeClick($anchor2);
$anchor2[0].removeEventListener("click" , neverCallMe);
};SensorRnt(8979);});
test("on(), iframes" , function () {
SensorRnt(8993);expect(1);
var doc = jQuery("#loadediframe").contents();
jQuery("div" , doc).on("click" , function () {
ok(true , "Binding to element inside iframe");}).trigger("click").off("click");SensorRnt(8994);});
test("on(), trigger change on select" , function () {
SensorRnt(8995);expect(5);
var counter = 0;
function selectOnChange (event) {
SensorRnt(8997);equal(event.data , counter++ , "Event.data is not a global event object");SensorRnt(8998);}
jQuery("#form select").each(function (i) {
jQuery(this).on("change" , i , selectOnChange);}).trigger("change");SensorRnt(8996);});
test("on(), namespaced events, cloned events" , 18 , function () {
SensorRnt(8999);var firstp = jQuery("#firstp");
firstp.on("custom.test" , function () {
SensorRnt(9001);ok(false , "Custom event triggered");SensorRnt(9002);});
firstp.on("click" , function (e) {
SensorRnt(9003);ok(true , "Normal click triggered");
equal(e.type+e.namespace , "click" , "Check that only click events trigger this fn");SensorRnt(9004);});
firstp.on("click.test" , function (e) {
var check = "click";
SensorRnt(9005);ok(true , "Namespaced click triggered");
SensorRnt(9006);if (e.namespace)
{ 
SensorRnt(9007);check += "test";
}
SensorRnt(9008);equal(e.type+e.namespace , check , "Check that only click/click.test events trigger this fn");SensorRnt(9009);});
firstp = firstp.add(firstp.clone(true).attr("id" , "firstp2").insertBefore(firstp));
firstp.trigger("click");
firstp.trigger("click.test");
firstp.off("click.test");
firstp.trigger("click");
firstp.off(".test");
firstp.trigger("custom");
jQuery("#nonnodes").contents().on("tester" , function () {
equal(this.nodeType , 1 , "Check node,textnode,comment on just does real nodes");}).trigger("tester");
jQuery("<a href='#fail' class='test'>test</a>").on("click" , function () {
RNTRETRET = false;
return RNTRETRET;}).appendTo("#qunit-fixture");
ok(jQuery("a.test").eq(0).triggerHandler("click")===false , "Handler is bound to appendTo'd elements");SensorRnt(9000);});
test("on(), multi-namespaced events" , function () {
SensorRnt(9010);expect(6);
var order = [ "click.test.abc", "click.test.abc", "click.test", "click.test.abc", "click.test", "custom.test2"];
function check (name, msg) {
SensorRnt(9012);deepEqual(name , order.shift() , msg);SensorRnt(9013);}
jQuery("#firstp").on("custom.test" , function () {
SensorRnt(9014);check("custom.test" , "Custom event triggered");SensorRnt(9015);});
jQuery("#firstp").on("custom.test2" , function () {
SensorRnt(9016);check("custom.test2" , "Custom event triggered");SensorRnt(9017);});
jQuery("#firstp").on("click.test" , function () {
SensorRnt(9018);check("click.test" , "Normal click triggered");SensorRnt(9019);});
jQuery("#firstp").on("click.test.abc" , function () {
SensorRnt(9020);check("click.test.abc" , "Namespaced click triggered");SensorRnt(9021);});
jQuery("#firstp").trigger("click.a.test");
jQuery("#firstp").off("click.a.test");
jQuery("#firstp").trigger("click.test.abc");
jQuery("#firstp").trigger("click.abc");
jQuery("#firstp").trigger("click.test");
jQuery("#firstp").off("click.abc");
jQuery("#firstp").trigger("click");
jQuery("#firstp").off(".test");
jQuery("#firstp").trigger("custom");SensorRnt(9011);});
test("namespace-only event binding is a no-op" , function () {
SensorRnt(9022);expect(2);
jQuery("#firstp").on(".whoops" , function () {
ok(false , "called a namespace-only event");}).on("whoops" , function () {
ok(true , "called whoops");}).trigger("whoops").off(".whoops").trigger("whoops").off("whoops");SensorRnt(9023);});
test("on(), with same function" , function () {
SensorRnt(9024);expect(2);
var count = 0, func = function () {SensorRnt(9026);
count++;};
jQuery("#liveHandlerOrder").on("foo.bar" , func).on("foo.zar" , func);
jQuery("#liveHandlerOrder").trigger("foo.bar");
equal(count , 1 , "Verify binding function with multiple namespaces.");
jQuery("#liveHandlerOrder").off("foo.bar" , func).off("foo.zar" , func);
jQuery("#liveHandlerOrder").trigger("foo.bar");
equal(count , 1 , "Verify that removing events still work.");SensorRnt(9025);});
test("on(), make sure order is maintained" , function () {
SensorRnt(9027);expect(1);
var elem = jQuery("#firstp"), log = [ ], check = [ ];
jQuery.each(new Array(100) , function ( i ) {
SensorRnt(9029);elem.on("click" , function () {
SensorRnt(9031);log.push(i);SensorRnt(9032);});
check.push(i);SensorRnt(9030);});
elem.trigger("click");
equal(log.join(",") , check.join(",") , "Make sure order was maintained.");
elem.off("click");SensorRnt(9028);});
test("on(), with different this object" , function () {
SensorRnt(9033);expect(4);
var thisObject = {
myThis : true
}, data = {
myData : true
}, handler1 = function () {
SensorRnt(9035);equal(this , thisObject , "on() with different this object");SensorRnt(9036);}, handler2 = function ( event ) {
SensorRnt(9037);equal(this , thisObject , "on() with different this object and data");
equal(event.data , data , "on() with different this object and data");SensorRnt(9038);};
jQuery("#firstp").on("click" , jQuery.proxy(handler1 , thisObject)).trigger("click").off("click" , handler1).on("click" , data , jQuery.proxy(handler2 , thisObject)).trigger("click").off("click" , handler2);
ok(! jQuery._data(jQuery("#firstp")[0] , "events") , "Event handler unbound when using different this object and data.");SensorRnt(9034);});
test("on(name, false), off(name, false)" , function () {
SensorRnt(9039);expect(3);
var main = 0;
jQuery("#qunit-fixture").on("click" , function () {
SensorRnt(9041);main++;});
jQuery("#ap").trigger("click");
equal(main , 1 , "Verify that the trigger happened correctly.");
main = 0;
jQuery("#ap").on("click" , false);
jQuery("#ap").trigger("click");
equal(main , 0 , "Verify that no bubble happened.");
main = 0;
jQuery("#ap").off("click" , false);
jQuery("#ap").trigger("click");
equal(main , 1 , "Verify that the trigger happened correctly.");
jQuery("#qunit-fixture").off("click");SensorRnt(9040);});
test("on(name, selector, false), off(name, selector, false)" , function () {
SensorRnt(9042);expect(3);
var main = 0;
jQuery("#qunit-fixture").on("click" , "#ap" , function () {
SensorRnt(9044);main++;});
jQuery("#ap").trigger("click");
equal(main , 1 , "Verify that the trigger happened correctly.");
main = 0;
jQuery("#ap").on("click" , "#groups" , false);
jQuery("#groups").trigger("click");
equal(main , 0 , "Verify that no bubble happened.");
main = 0;
jQuery("#ap").off("click" , "#groups" , false);
jQuery("#groups").trigger("click");
equal(main , 1 , "Verify that the trigger happened correctly.");
jQuery("#qunit-fixture").off("click" , "#ap");SensorRnt(9043);});
test("on()/trigger()/off() on plain object" , function () {
SensorRnt(9045);expect(7);
var events, obj = {

};
jQuery(obj).trigger("test");
jQuery(obj).off("test");
jQuery(obj).on({
"test" : function () {
SensorRnt(9047);ok(true , "Custom event run.");SensorRnt(9048);},
"submit" : function () {
SensorRnt(9049);ok(true , "Custom submit event run.");SensorRnt(9050);}
});
events = jQuery._data(obj , "events");
ok(events , "Object has events bound.");
equal(obj["events"] , undefined , "Events object on plain objects is not events");
equal(obj["test"] , undefined , "Make sure that test event is not on the plain object.");
equal(obj["handle"] , undefined , "Make sure that the event handler is not on the plain object.");
jQuery(obj).trigger("test");
jQuery(obj).trigger("submit");
jQuery(obj).off("test");
jQuery(obj).off("submit");
jQuery(obj).trigger("test");
jQuery(obj).off("test");
equal(obj&&obj[jQuery.expando]&&obj[jQuery.expando][jQuery.expando]&&obj[jQuery.expando][jQuery.expando]["events"] , undefined , "Make sure events object is removed");SensorRnt(9046);});
test("off(type)" , function () {
SensorRnt(9051);expect(1);
var message, func, $elem = jQuery("#firstp");
function error () {
SensorRnt(9053);ok(false , message);SensorRnt(9054);}
message = "unbind passing function";
$elem.on("error1" , error).off("error1" , error).triggerHandler("error1");
message = "unbind all from event";
$elem.on("error1" , error).off("error1").triggerHandler("error1");
message = "unbind all";
$elem.on("error1" , error).off().triggerHandler("error1");
message = "unbind many with function";
$elem.on("error1 error2" , error).off("error1 error2" , error).trigger("error1").triggerHandler("error2");
message = "unbind many";
$elem.on("error1 error2" , error).off("error1 error2").trigger("error1").triggerHandler("error2");
message = "unbind without a type or handler";
$elem.on("error1 error2.test" , error).off().trigger("error1").triggerHandler("error2");
jQuery(document).on("click" , function () {
SensorRnt(9055);ok(true , "called handler after selective removal");SensorRnt(9056);});
func = function () {SensorRnt(9057);};
jQuery(document).on("click" , func).off("click" , func).trigger("click").off("click");SensorRnt(9052);});
test("off(eventObject)" , function () {
SensorRnt(9058);expect(4);
var $elem = jQuery("#firstp"), num;
function assert ( expected ) {
num = 0;
SensorRnt(9060);$elem.trigger("foo").triggerHandler("bar");
equal(num , expected , "Check the right handlers are triggered");SensorRnt(9061);}
$elem.on("foo" , function () {
num += 1;}).on("foo" , function (e) {
$elem.off(e);
num += 2;}).on("bar" , function () {SensorRnt(9062);
num += 4;});
assert(7);
assert(5);
$elem.off("bar");
assert(1);
$elem.off();
assert(0);SensorRnt(9059);});
SensorRnt(9365);if (jQuery.fn.hover)
{ 
SensorRnt(9366);test("hover() mouseenter mouseleave" , function () {
SensorRnt(9063);expect(1);
var times = 0, handler1 = function () {
SensorRnt(9065);++ times;}, handler2 = function () {
SensorRnt(9066);++ times;};
jQuery("#firstp").hover(handler1 , handler2).mouseenter().mouseleave().off("mouseenter" , handler1).off("mouseleave" , handler2).hover(handler1).mouseenter().mouseleave().off("mouseenter mouseleave" , handler1).mouseenter().mouseleave();
equal(times , 4 , "hover handlers fired");});SensorRnt(9064);
}
SensorRnt(9367);test("mouseover triggers mouseenter" , function () {
SensorRnt(9067);expect(1);
var count = 0, elem = jQuery("<a />");
elem.on("mouseenter" , function () {SensorRnt(9069);
count++;});
elem.trigger("mouseover");
equal(count , 1 , "make sure mouseover triggers a mouseenter");
elem.remove();SensorRnt(9068);});
test("pointerover triggers pointerenter" , function () {
SensorRnt(9070);expect(1);
var count = 0, elem = jQuery("<a />");
elem.on("pointerenter" , function () {SensorRnt(9072);
count++;});
elem.trigger("pointerover");
equal(count , 1 , "make sure pointerover triggers a pointerenter");
elem.remove();SensorRnt(9071);});
test("withinElement implemented with jQuery.contains()" , function () {
SensorRnt(9073);expect(1);
jQuery("#qunit-fixture").append("<div id='jc-outer'><div id='jc-inner'></div></div>");
jQuery("#jc-outer").on("mouseenter mouseleave" , function ( event ) {
equal(this.id , "jc-outer" , this.id+" "+event.type);}).trigger("mouseenter");
jQuery("#jc-inner").trigger("mousenter");
jQuery("#jc-outer").off("mouseenter mouseleave").remove();
jQuery("#jc-inner").remove();SensorRnt(9074);});
test("mouseenter, mouseleave don't catch exceptions" , function () {
SensorRnt(9075);expect(2);
var elem = jQuery("#firstp").on("mouseenter mouseleave" , function () {
SensorRnt(9083);throw "an Exception";});
SensorRnt(9076);try{ 
SensorRnt(9077);elem.trigger("mouseenter");
}
catch(e){ 
SensorRnt(9078);equal(e , "an Exception" , "mouseenter doesn't catch exceptions");
}
SensorRnt(9079);try{ 
SensorRnt(9080);elem.trigger("mouseleave");
}
catch(e){ 
SensorRnt(9081);equal(e , "an Exception" , "mouseleave doesn't catch exceptions");
};SensorRnt(9082);});
SensorRnt(9368);if (jQuery.fn.click)
{ 
SensorRnt(9369);test("trigger() shortcuts" , function () {
SensorRnt(9084);expect(6);
var counter, clickCounter, elem = jQuery("<li><a href='#'>Change location</a></li>").prependTo("#firstUL");
elem.find("a").on("click" , function () {
var close = jQuery("spanx" , this);
equal(close.length , 0 , "Context element does not exist, length must be zero");
ok(! close[0] , "Context element does not exist, direct access to element must return undefined");
RNTRETRET = false;
return RNTRETRET;}).click();
elem.remove();
jQuery("#check1").click(function () {
ok(true , "click event handler for checkbox gets fired twice, see #815");}).click();
counter = 0;
jQuery("#firstp")[0].onclick = function () {SensorRnt(9086);
counter++;};
jQuery("#firstp").click();
equal(counter , 1 , "Check that click, triggers onclick event handler also");
clickCounter = 0;
jQuery("#simon1")[0].onclick = function () {SensorRnt(9087);
clickCounter++;};
jQuery("#simon1").click();
equal(clickCounter , 1 , "Check that click, triggers onclick event handler on an a tag also");
elem = jQuery("<img />").load(function () {
ok(true , "Trigger the load event, using the shortcut .load() (#2819)");}).load();
elem.remove();
jQuery("<xml:namespace ns='urn:schemas-microsoft-com:vml' prefix='v' />").appendTo("head");
jQuery("<v:oval id='oval' style='width:100pt;height:75pt;' fillcolor='red'> </v:oval>").appendTo("#form");
jQuery("#oval").click().keydown();});SensorRnt(9085);
}
SensorRnt(9370);test("trigger() bubbling" , function () {
SensorRnt(9088);expect(18);
var win = 0, doc = 0, html = 0, body = 0, main = 0, ap = 0;
jQuery(window).on("click" , function () {
SensorRnt(9090);win++;});
jQuery(document).on("click" , function ( e ) {
SensorRnt(9091);if (e.target!==document)
{ 
SensorRnt(9092);doc++;
};SensorRnt(9093);});
jQuery("html").on("click" , function () {
SensorRnt(9094);html++;});
jQuery("body").on("click" , function () {
SensorRnt(9095);body++;});
jQuery("#qunit-fixture").on("click" , function () {
SensorRnt(9096);main++;});
jQuery("#ap").on("click" , function () {
ap++;
RNTRETRET = false;
SensorRnt(9097);return RNTRETRET;});
jQuery("html").trigger("click");
equal(win , 1 , "HTML bubble");
equal(doc , 1 , "HTML bubble");
equal(html , 1 , "HTML bubble");
jQuery("body").trigger("click");
equal(win , 2 , "Body bubble");
equal(doc , 2 , "Body bubble");
equal(html , 2 , "Body bubble");
equal(body , 1 , "Body bubble");
jQuery("#qunit-fixture").trigger("click");
equal(win , 3 , "Main bubble");
equal(doc , 3 , "Main bubble");
equal(html , 3 , "Main bubble");
equal(body , 2 , "Main bubble");
equal(main , 1 , "Main bubble");
jQuery("#ap").trigger("click");
equal(doc , 3 , "ap bubble");
equal(html , 3 , "ap bubble");
equal(body , 2 , "ap bubble");
equal(main , 1 , "ap bubble");
equal(ap , 1 , "ap bubble");
jQuery(document).trigger("click");
equal(win , 4 , "doc bubble");
jQuery(document).off("click");
jQuery("html, body, #qunit-fixture").off("click");SensorRnt(9089);});
test("trigger(type, [data], [fn])" , function () {
SensorRnt(9098);expect(16);
var $elem, pass, form, elem2, handler = function (event, a, b, c) {
SensorRnt(9108);equal(event.type , "click" , "check passed data");
equal(a , 1 , "check passed data");
equal(b , "2" , "check passed data");
equal(c , "abc" , "check passed data");
RNTRETRET = "test";
SensorRnt(9109);return RNTRETRET;};
$elem = jQuery("#firstp");
$elem[0].click = function () {
SensorRnt(9110);ok(true , "Native call was triggered");SensorRnt(9111);};
jQuery(document).on("mouseenter" , "#firstp" , function () {
SensorRnt(9112);ok(true , "Trigger mouseenter bound by on");SensorRnt(9113);});
jQuery(document).on("mouseleave" , "#firstp" , function () {
SensorRnt(9114);ok(true , "Trigger mouseleave bound by on");SensorRnt(9115);});
$elem.trigger("mouseenter");
$elem.trigger("mouseleave");
jQuery(document).off("mouseenter mouseleave" , "#firstp");
$elem.on("click" , handler).trigger("click" , [ 1, "2", "abc"]);
$elem[0].click = function () {
SensorRnt(9116);ok(false , "Native call was triggered");SensorRnt(9117);};
equal($elem.triggerHandler("click" , [ 1, "2", "abc"]) , "test" , "Verify handler response");
pass = true;
SensorRnt(9099);try{ 
SensorRnt(9100);elem2 = jQuery("#form input").eq(0);
elem2.get(0).style.display = "none";
elem2.trigger("focus");
}
catch(e){ 
SensorRnt(9101);pass = false;
}
SensorRnt(9102);ok(pass , "Trigger focus on hidden element");
pass = true;
SensorRnt(9103);try{ 
jQuery("#qunit-fixture table").eq(0).on("test:test" , function () {SensorRnt(9104);}).trigger("test:test");
}
catch(e){ 
SensorRnt(9105);pass = false;
}
SensorRnt(9106);ok(pass , "Trigger on a table with a colon in the even type, see #3533");
form = jQuery("<form action=''></form>").appendTo("body");
form.on("submit" , function () {
SensorRnt(9118);ok(true , "Local `on` still works.");
RNTRETRET = false;
SensorRnt(9119);return RNTRETRET;});
form.trigger("submit");
form.off("submit");
jQuery(document).on("submit" , function () {
SensorRnt(9120);ok(true , "Make sure bubble works up to document.");
RNTRETRET = false;
SensorRnt(9121);return RNTRETRET;});
form.trigger("submit");
jQuery(document).off("submit");
form.remove();SensorRnt(9107);});
test("submit event bubbles on copied forms (#11649)" , function () {
SensorRnt(9122);expect(3);
var $formByClone, $formByHTML, $testForm = jQuery("#testForm"), $fixture = jQuery("#qunit-fixture"), $wrapperDiv = jQuery("<div/>").appendTo($fixture);
function noSubmit ( e ) {
SensorRnt(9124);e.preventDefault();SensorRnt(9125);}
function delegatedSubmit () {
SensorRnt(9126);ok(true , "Make sure submit event bubbles up.");
RNTRETRET = false;
SensorRnt(9127);return RNTRETRET;}
$fixture.on("submit" , "form" , delegatedSubmit);
$testForm.on("submit" , noSubmit).find("input[name=sub1]").trigger("click");
$formByClone = $testForm.clone(true , true).removeAttr("id");
$formByHTML = jQuery(jQuery.parseHTML($fixture.html())).filter("#testForm").removeAttr("id");
$wrapperDiv.append($formByClone , $formByHTML);
$wrapperDiv.find("form").on("submit" , noSubmit).find("input[name=sub1]").trigger("click");
$wrapperDiv.remove();
$fixture.off("submit" , "form" , delegatedSubmit);
$testForm.off("submit" , noSubmit);SensorRnt(9123);});
test("change event bubbles on copied forms (#11796)" , function () {
SensorRnt(9128);expect(3);
var $formByClone, $formByHTML, $form = jQuery("#form"), $fixture = jQuery("#qunit-fixture"), $wrapperDiv = jQuery("<div/>").appendTo($fixture);
function delegatedChange () {
SensorRnt(9130);ok(true , "Make sure change event bubbles up.");
RNTRETRET = false;
SensorRnt(9131);return RNTRETRET;}
$fixture.on("change" , "form" , delegatedChange);
$form.find("select[name=select1]").val("1").trigger("change");
$formByClone = $form.clone(true , true).removeAttr("id");
$formByHTML = jQuery(jQuery.parseHTML($fixture.html())).filter("#form").removeAttr("id");
$wrapperDiv.append($formByClone , $formByHTML);
$wrapperDiv.find("form select[name=select1]").val("2").trigger("change");
$wrapperDiv.remove();
$fixture.off("change" , "form" , delegatedChange);SensorRnt(9129);});
test("trigger(eventObject, [data], [fn])" , function () {
SensorRnt(9132);expect(28);
var event, $parent = jQuery("<div id='par' />").appendTo("body"), $child = jQuery("<p id='child'>foo</p>").appendTo($parent);
$parent.get(0).style.display = "none";
event = jQuery.Event("noNew");
ok(event!==window , "Instantiate jQuery.Event without the 'new' keyword");
equal(event.type , "noNew" , "Verify its type");
equal(event.isDefaultPrevented() , false , "Verify isDefaultPrevented");
equal(event.isPropagationStopped() , false , "Verify isPropagationStopped");
equal(event.isImmediatePropagationStopped() , false , "Verify isImmediatePropagationStopped");
event.preventDefault();
equal(event.isDefaultPrevented() , true , "Verify isDefaultPrevented");
event.stopPropagation();
equal(event.isPropagationStopped() , true , "Verify isPropagationStopped");
event.isPropagationStopped = function () {
RNTRETRET = false;
SensorRnt(9134);return RNTRETRET;};
event.stopImmediatePropagation();
equal(event.isPropagationStopped() , true , "Verify isPropagationStopped");
equal(event.isImmediatePropagationStopped() , true , "Verify isPropagationStopped");
$parent.on("foo" , function ( e ) {
SensorRnt(9135);equal(e.type , "foo" , "Verify event type when passed passing an event object");
equal(e.target.id , "child" , "Verify event.target when passed passing an event object");
equal(e.currentTarget.id , "par" , "Verify event.currentTarget when passed passing an event object");
equal(e.secret , "boo!" , "Verify event object's custom attribute when passed passing an event object");SensorRnt(9136);});
event = new jQuery.Event("foo");
event.secret = "boo!";
$child.trigger(event);
$child.trigger({
"type" : "foo",
"secret" : "boo!"
});
$parent.off();
function error () {
SensorRnt(9137);ok(false , "This assertion shouldn't be reached");SensorRnt(9138);}
$parent.on("foo" , error);
$child.on("foo" , function (e, a, b, c ) {
SensorRnt(9139);equal(arguments.length , 4 , "Check arguments length");
equal(a , 1 , "Check first custom argument");
equal(b , 2 , "Check second custom argument");
equal(c , 3 , "Check third custom argument");
equal(e.isDefaultPrevented() , false , "Verify isDefaultPrevented");
equal(e.isPropagationStopped() , false , "Verify isPropagationStopped");
equal(e.isImmediatePropagationStopped() , false , "Verify isImmediatePropagationStopped");
e.stopImmediatePropagation();
RNTRETRET = "result";
SensorRnt(9140);return RNTRETRET;});
event = new jQuery.Event("foo");
$child.trigger(event , [ 1, 2, 3]).off();
equal(event.result , "result" , "Check event.result attribute");
$child.triggerHandler("foo");
$child.off();
$parent.off().remove();
event = jQuery.Event("zowie");
jQuery(document).triggerHandler(event);
equal(event.type , "zowie" , "Verify its type");
equal(event.isPropagationStopped() , false , "propagation not stopped");
equal(event.isDefaultPrevented() , false , "default not prevented");SensorRnt(9133);});
test(".trigger() bubbling on disconnected elements (#10489)" , function () {
SensorRnt(9141);expect(2);
jQuery(window).on("click" , function () {
SensorRnt(9143);ok(false , "click fired on window");SensorRnt(9144);});
jQuery("<div><p>hi</p></div>").on("click" , function () {
ok(true , "click fired on div");}).find("p").on("click" , function () {
ok(true , "click fired on p");}).trigger("click").off("click").end().off("click").remove();
jQuery(window).off("click");SensorRnt(9142);});
test(".trigger() doesn't bubble load event (#10717)" , function () {
SensorRnt(9145);expect(1);
jQuery(window).on("load" , function () {
SensorRnt(9147);ok(false , "load fired on window");SensorRnt(9148);});
jQuery("<img src='index.html' />").appendTo("body").on("load" , function () {
ok(true , "load fired on img");}).trigger("load").remove();
jQuery(window).off("load");SensorRnt(9146);});
test("Delegated events in SVG (#10791; #13180)" , function () {
SensorRnt(9149);expect(2);
var useElem, e, svg = jQuery("<svg height='1' version='1.1' width='1' xmlns='http://www.w3.org/2000/svg'>"+"<defs><rect id='ref' x='10' y='20' width='100' height='60' r='10' rx='10' ry='10'></rect></defs>"+"<rect class='svg-by-class' x='10' y='20' width='100' height='60' r='10' rx='10' ry='10'></rect>"+"<rect id='svg-by-id' x='10' y='20' width='100' height='60' r='10' rx='10' ry='10'></rect>"+"<use id='use' xlink:href='#ref'></use>"+"</svg>");
jQuery("#qunit-fixture").append(svg).on("click" , "#svg-by-id" , function () {
ok(true , "delegated id selector");}).on("click" , "[class~='svg-by-class']" , function () {
ok(true , "delegated class selector");}).find("#svg-by-id, [class~='svg-by-class']").trigger("click").end();
useElem = svg.find("#use")[0];
SensorRnt(9150);if (document.createEvent&&useElem&&useElem.instanceRoot)
{ 
SensorRnt(9151);e = document.createEvent("MouseEvents");
e.initEvent("click" , true , true);
useElem.instanceRoot.dispatchEvent(e);
}
SensorRnt(9152);jQuery("#qunit-fixture").off("click");SensorRnt(9153);});
test("Delegated events in forms (#10844; #11145; #8165; #11382, #11764)" , function () {
SensorRnt(9154);expect(5);
var form = jQuery("<form id='myform'>"+"<input type='text' name='id' value='secret agent man' />"+"</form>").on("submit" , function ( event ) {
event.preventDefault();}).appendTo("body");
jQuery("body").on("submit" , "#myform" , function () {
ok(true , "delegated id selector with aliased id");}).find("#myform").trigger("submit").end().off("submit");
form.append("<input type='text' name='disabled' value='differently abled' />");
jQuery("body").on("submit" , "#myform" , function () {
ok(true , "delegated id selector with aliased disabled");}).find("#myform").trigger("submit").end().off("submit");
form.append("<button id='nestyDisabledBtn'><span>Zing</span></button>").on("click" , "#nestyDisabledBtn" , function () {
ok(true , "click on enabled/disabled button with nesty elements");}).on("mouseover" , "#nestyDisabledBtn" , function () {
ok(true , "mouse on enabled/disabled button with nesty elements");}).find("span").trigger("click").trigger("mouseover").end().find("#nestyDisabledBtn").prop("disabled" , true).end().find("span").trigger("click").trigger("mouseover").end().off("click");
form.remove();SensorRnt(9155);});
test("Submit event can be stopped (#11049)" , function () {
SensorRnt(9156);expect(1);
var form = jQuery("<form id='myform'>"+"<input type='text' name='sue' value='bawls' />"+"<input type='submit' />"+"</form>").appendTo("body");
jQuery("body").on("submit" , function () {
ok(true , "submit bubbled on first handler");
RNTRETRET = false;
return RNTRETRET;}).find("#myform input[type=submit]").each(function () {
this.click();}).end().on("submit" , function () {
ok(false , "submit bubbled on second handler");
RNTRETRET = false;
return RNTRETRET;}).find("#myform input[type=submit]").each(function () {
jQuery(this.form).on("submit" , function ( e ) {
e.preventDefault();
e.stopPropagation();});
this.click();}).end().off("submit");
form.remove();SensorRnt(9157);});
SensorRnt(9371);if (window.onbeforeunload===null&&! /(ipad|iphone|ipod|android 2\.3)/i.test(navigator.userAgent))
{ 
SensorRnt(9372);asyncTest("on(beforeunload)" , 1 , function () {
SensorRnt(9158);var iframe = jQuery(jQuery.parseHTML("<iframe src='data/event/onbeforeunload.html'><iframe>"));
window.onmessage = function ( event ) {
SensorRnt(9160);var payload = JSON.parse(event.data);
ok(payload.event , "beforeunload" , "beforeunload event");
iframe.remove();
window.onmessage = null;
start();SensorRnt(9161);};
iframe.appendTo("#qunit-fixture");});SensorRnt(9159);
}
SensorRnt(9373);test("jQuery.Event( type, props )" , function () {
SensorRnt(9162);expect(6);
var event = jQuery.Event("keydown" , {
keyCode : 64
}), handler = function ( event ) {
SensorRnt(9164);ok("keyCode" in event , "Special property 'keyCode' exists");
equal(event.keyCode , 64 , "event.keyCode has explicit value '64'");SensorRnt(9165);};
equal(event.type , "keydown" , "Verify type");
equal(jQuery.inArray("type" , jQuery.event.props) , - 1 , "'type' property not in props (#10375)");
ok("keyCode" in event , "Special 'keyCode' property exists");
strictEqual(jQuery.isPlainObject(event) , false , "Instances of $.Event should not be identified as a plain object.");
jQuery("body").on("keydown" , handler).trigger(event);
jQuery("body").off("keydown");SensorRnt(9163);});
test("jQuery.Event properties" , function () {
SensorRnt(9166);expect(12);
var handler, $structure = jQuery("<div id='ancestor'><p id='delegate'><span id='target'>shiny</span></p></div>"), $target = $structure.find("#target");
handler = function ( e ) {
SensorRnt(9168);strictEqual(e.currentTarget , this , "currentTarget at "+this.id);
equal(e.isTrigger , 3 , "trigger at "+this.id);SensorRnt(9169);};
$structure.one("click" , handler);
$structure.one("click" , "p" , handler);
$target.one("click" , handler);
$target[0].onclick = function ( e ) {
SensorRnt(9170);strictEqual(e.currentTarget , this , "currentTarget at target (native handler)");
equal(e.isTrigger , 3 , "trigger at target (native handler)");SensorRnt(9171);};
$target.trigger("click");
$target.one("click" , function ( e ) {
SensorRnt(9172);equal(e.isTrigger , 2 , "triggerHandler at target");SensorRnt(9173);});
$target[0].onclick = function ( e ) {
SensorRnt(9174);equal(e.isTrigger , 2 , "triggerHandler at target (native handler)");SensorRnt(9175);};
$target.triggerHandler("click");
handler = function ( e ) {
SensorRnt(9176);strictEqual(e.isTrigger , undefined , "native event at "+this.id);SensorRnt(9177);};
$target.one("click" , handler);
$target[0].onclick = function ( e ) {
SensorRnt(9178);strictEqual(e.isTrigger , undefined , "native event at target (native handler)");SensorRnt(9179);};
fireNative($target[0] , "click");SensorRnt(9167);});
test(".on()/.off()" , function () {
SensorRnt(9180);expect(65);
var event, clicked, hash, called, livec, lived, livee, submit = 0, div = 0, livea = 0, liveb = 0;
jQuery("#body").on("submit" , "#qunit-fixture div" , function () {
submit++;
RNTRETRET = false;
SensorRnt(9182);return RNTRETRET;});
jQuery("#body").on("click" , "#qunit-fixture div" , function () {
SensorRnt(9183);div++;});
jQuery("#body").on("click" , "div#nothiddendiv" , function () {
SensorRnt(9184);livea++;});
jQuery("#body").on("click" , "div#nothiddendivchild" , function () {
SensorRnt(9185);liveb++;});
jQuery("body").trigger("click");
equal(submit , 0 , "Click on body");
equal(div , 0 , "Click on body");
equal(livea , 0 , "Click on body");
equal(liveb , 0 , "Click on body");
submit = 0;
div = 0;
livea = 0;
liveb = 0;
jQuery("div#nothiddendiv").trigger("click");
equal(submit , 0 , "Click on div");
equal(div , 1 , "Click on div");
equal(livea , 1 , "Click on div");
equal(liveb , 0 , "Click on div");
submit = 0;
div = 0;
livea = 0;
liveb = 0;
jQuery("div#nothiddendivchild").trigger("click");
equal(submit , 0 , "Click on inner div");
equal(div , 2 , "Click on inner div");
equal(livea , 1 , "Click on inner div");
equal(liveb , 1 , "Click on inner div");
submit = 0;
div = 0;
livea = 0;
liveb = 0;
jQuery("div#nothiddendivchild").trigger("submit");
equal(submit , 1 , "Submit on div");
equal(div , 0 , "Submit on div");
equal(livea , 0 , "Submit on div");
equal(liveb , 0 , "Submit on div");
submit = 0;
div = 0;
livea = 0;
liveb = 0;
jQuery("div#nothiddendivchild").trigger("click");
equal(submit , 0 , "off Click on inner div");
equal(div , 2 , "off Click on inner div");
equal(livea , 1 , "off Click on inner div");
equal(liveb , 1 , "off Click on inner div");
submit = 0;
div = 0;
livea = 0;
liveb = 0;
jQuery("#body").off("click" , "div#nothiddendivchild");
jQuery("div#nothiddendivchild").trigger("click");
equal(submit , 0 , "off Click on inner div");
equal(div , 2 , "off Click on inner div");
equal(livea , 1 , "off Click on inner div");
equal(liveb , 0 , "off Click on inner div");
submit = 0;
div = 0;
livea = 0;
liveb = 0;
jQuery("div#nothiddendiv").trigger("click");
equal(submit , 0 , "off Click on inner div");
equal(div , 1 , "off Click on inner div");
equal(livea , 1 , "off Click on inner div");
equal(liveb , 0 , "off Click on inner div");
submit = 0;
div = 0;
livea = 0;
liveb = 0;
jQuery("#body").on("click" , "div#nothiddendivchild" , function ( e ) {
liveb++;
SensorRnt(9186);e.stopPropagation();SensorRnt(9187);});
jQuery("div#nothiddendivchild").trigger("click");
equal(submit , 0 , "stopPropagation Click on inner div");
equal(div , 1 , "stopPropagation Click on inner div");
equal(livea , 0 , "stopPropagation Click on inner div");
equal(liveb , 1 , "stopPropagation Click on inner div");
submit = 0;
div = 0;
livea = 0;
liveb = 0;
event = jQuery.Event("click");
event.button = 1;
jQuery("div#nothiddendiv").trigger(event);
equal(livea , 0 , "on secondary click");
jQuery("#body").off("click" , "div#nothiddendivchild");
jQuery("#body").off("click" , "div#nothiddendiv");
jQuery("#body").off("click" , "#qunit-fixture div");
jQuery("#body").off("submit" , "#qunit-fixture div");
clicked = 0;
jQuery("#qunit-fixture").on("click" , "#foo" , function () {
SensorRnt(9188);clicked++;});
jQuery("#qunit-fixture div").trigger("click");
jQuery("#foo").trigger("click");
jQuery("#qunit-fixture").trigger("click");
jQuery("body").trigger("click");
equal(clicked , 2 , "on with a context");
jQuery("#qunit-fixture").off("click" , "#foo");
jQuery("#foo").trigger("click");
equal(clicked , 2 , "off with a context");
jQuery("#body").on("click" , "#foo" , true , function ( e ) {
SensorRnt(9189);equal(e.data , true , "on with event data");SensorRnt(9190);});
jQuery("#foo").trigger("click");
jQuery("#body").off("click" , "#foo");
jQuery("#body").on("click" , "#foo" , function (e, data) {
SensorRnt(9191);equal(data , true , "on with trigger data");SensorRnt(9192);});
jQuery("#foo").trigger("click" , true);
jQuery("#body").off("click" , "#foo");
jQuery("#body").on("click" , "#foo" , jQuery.proxy(function () {
SensorRnt(9193);equal(this["foo"] , "bar" , "on with event scope");SensorRnt(9194);} , {
"foo" : "bar"
}));
jQuery("#foo").trigger("click");
jQuery("#body").off("click" , "#foo");
jQuery("#body").on("click" , "#foo" , true , jQuery.proxy(function (e, data) {
SensorRnt(9195);equal(e.data , true , "on with with different this object, event data, and trigger data");
equal(this.foo , "bar" , "on with with different this object, event data, and trigger data");
equal(data , true , "on with with different this object, event data, and trigger data");SensorRnt(9196);} , {
"foo" : "bar"
}));
jQuery("#foo").trigger("click" , true);
jQuery("#body").off("click" , "#foo");
jQuery("#body").on("click" , "#anchor2" , function () {
RNTRETRET = false;
SensorRnt(9197);return RNTRETRET;});
hash = window.location.hash;
jQuery("#anchor2").trigger("click");
equal(window.location.hash , hash , "return false worked");
jQuery("#body").off("click" , "#anchor2");
jQuery("#body").on("click" , "#anchor2" , function (e) {
SensorRnt(9198);e.preventDefault();SensorRnt(9199);});
hash = window.location.hash;
jQuery("#anchor2").trigger("click");
equal(window.location.hash , hash , "e.preventDefault() worked");
jQuery("#body").off("click" , "#anchor2");
called = 0;
function callback () {
called++;
RNTRETRET = false;
SensorRnt(9200);return RNTRETRET;}
jQuery("#body").on("click" , "#nothiddendiv" , callback);
jQuery("#body").on("click" , "#anchor2" , callback);
jQuery("#nothiddendiv").trigger("click");
equal(called , 1 , "Verify that only one click occurred.");
called = 0;
jQuery("#anchor2").trigger("click");
equal(called , 1 , "Verify that only one click occurred.");
jQuery("#body").off("click" , "#anchor2" , callback);
called = 0;
jQuery("#nothiddendiv").trigger("click");
equal(called , 1 , "Verify that only one click occurred.");
called = 0;
jQuery("#anchor2").trigger("click");
equal(called , 0 , "Verify that no click occurred.");
jQuery("#body").on("foo" , "#nothiddendiv" , callback);
jQuery("#body").off("click" , "#nothiddendiv" , callback);
called = 0;
jQuery("#nothiddendiv").trigger("click");
equal(called , 0 , "Verify that no click occurred.");
called = 0;
jQuery("#nothiddendiv").trigger("foo");
equal(called , 1 , "Verify that one foo occurred.");
jQuery("#body").off("foo" , "#nothiddendiv" , callback);
livec = 0;
jQuery("#nothiddendivchild").html("<span></span>");
jQuery("#body").on("click" , "#nothiddendivchild" , function () {
SensorRnt(9201);jQuery("#nothiddendivchild").html("");SensorRnt(9202);});
jQuery("#body").on("click" , "#nothiddendivchild" , function (e) {
SensorRnt(9203);if (e.target)
{ 
SensorRnt(9204);livec++;
};SensorRnt(9205);});
jQuery("#nothiddendiv span").trigger("click");
equal(jQuery("#nothiddendiv span").length , 0 , "Verify that first handler occurred and modified the DOM.");
equal(livec , 1 , "Verify that second handler occurred even with nuked target.");
jQuery("#body").off("click" , "#nothiddendivchild");
lived = 0;
livee = 0;
jQuery("#body").on("click" , "span#liveSpan1 a" , function () {
lived++;
RNTRETRET = false;
SensorRnt(9206);return RNTRETRET;});
jQuery("#body").on("click" , "span#liveSpan1" , function () {
SensorRnt(9207);livee++;});
jQuery("span#liveSpan1 a").trigger("click");
equal(lived , 1 , "Verify that only one first handler occurred.");
equal(livee , 0 , "Verify that second handler doesn't.");
jQuery("#body").on("click" , "span#liveSpan2" , function () {
SensorRnt(9208);livee++;});
jQuery("#body").on("click" , "span#liveSpan2 a" , function () {
lived++;
RNTRETRET = false;
SensorRnt(9209);return RNTRETRET;});
lived = 0;
livee = 0;
jQuery("span#liveSpan2 a").trigger("click");
equal(lived , 1 , "Verify that only one first handler occurred.");
equal(livee , 0 , "Verify that second handler doesn't.");
jQuery("#body").off("click" , "**");
jQuery("#body").on("click" , "span#liveSpan1" , function ( e ) {
SensorRnt(9210);equal(this.id , "liveSpan1" , "Check the this within a on handler");
equal(e.currentTarget.id , "liveSpan1" , "Check the event.currentTarget within a on handler");
equal(e.delegateTarget , document.body , "Check the event.delegateTarget within a on handler");
equal(e.target.nodeName.toUpperCase() , "A" , "Check the event.target within a on handler");SensorRnt(9211);});
jQuery("span#liveSpan1 a").trigger("click");
jQuery("#body").off("click" , "span#liveSpan1");
livee = 0;
function clickB () {
SensorRnt(9212);livee++;}
jQuery("#body").on("click" , "#nothiddendiv div" , function () {
SensorRnt(9213);livee++;});
jQuery("#body").on("click" , "#nothiddendiv div" , clickB);
jQuery("#body").on("mouseover" , "#nothiddendiv div" , function () {
SensorRnt(9214);livee++;});
equal(livee , 0 , "No clicks, deep selector.");
livee = 0;
jQuery("#nothiddendivchild").trigger("click");
equal(livee , 2 , "Click, deep selector.");
livee = 0;
jQuery("#nothiddendivchild").trigger("mouseover");
equal(livee , 1 , "Mouseover, deep selector.");
jQuery("#body").off("mouseover" , "#nothiddendiv div");
livee = 0;
jQuery("#nothiddendivchild").trigger("click");
equal(livee , 2 , "Click, deep selector.");
livee = 0;
jQuery("#nothiddendivchild").trigger("mouseover");
equal(livee , 0 , "Mouseover, deep selector.");
jQuery("#body").off("click" , "#nothiddendiv div" , clickB);
livee = 0;
jQuery("#nothiddendivchild").trigger("click");
equal(livee , 1 , "Click, deep selector.");
jQuery("#body").off("click" , "#nothiddendiv div");SensorRnt(9181);});
test("jQuery.off using dispatched jQuery.Event" , function () {
SensorRnt(9215);expect(1);
var markup = jQuery("<p><a href='#'>target</a></p>"), count = 0;
markup.on("click.name" , "a" , function ( event ) {
equal(++ count , 1 , "event called once before removal");
jQuery().off(event);}).find("a").trigger("click").trigger("click").end().remove();SensorRnt(9216);});
test("delegated event with delegateTarget-relative selector" , function () {
SensorRnt(9217);expect(3);
var markup = jQuery("<div><ul><li><a id=\"a0\"></a><ul id=\"ul0\"><li class=test><a id=\"a0_0\"></a></li><li><a id=\"a0_1\"></a></li></ul></li></ul></div>").appendTo("#qunit-fixture");
markup.find("#ul0").on("click" , "div li a" , function () {
ok(false , "div is ABOVE the delegation point!");}).on("click" , "ul a" , function () {
ok(false , "ul IS the delegation point!");}).on("click" , "li.test a" , function () {
ok(true , "li.test is below the delegation point.");}).find("#a0_0").trigger("click").end().off("click");
markup.find("ul").eq(0).on("click" , ">li>a" , function () {
ok(this.id==="a0" , "child li was clicked");}).find("#ul0").on("click" , "li:first>a" , function () {
ok(this.id==="a0_0" , "first li under #u10 was clicked");}).end().find("a").trigger("click").end().find("#ul0").off();
markup.remove();SensorRnt(9218);});
test("delegated event with selector matching Object.prototype property (#13203)" , function () {
SensorRnt(9219);expect(1);
var matched = 0;
jQuery("#foo").on("click" , "toString" , function () {SensorRnt(9221);
matched++;});
jQuery("#anchor2").trigger("click");
equal(matched , 0 , "Nothing matched 'toString'");SensorRnt(9220);});
test("stopPropagation() stops directly-bound events on delegated target" , function () {
SensorRnt(9222);expect(1);
var markup = jQuery("<div><p><a href=\"#\">target</a></p></div>");
markup.on("click" , function () {
ok(false , "directly-bound event on delegate target was called");}).on("click" , "a" , function ( e ) {
e.stopPropagation();
ok(true , "delegated handler was called");}).find("a").trigger("click").end().remove();SensorRnt(9223);});
test("off all bound delegated events" , function () {
SensorRnt(9224);expect(2);
var count = 0, clicks = 0, div = jQuery("#body");
div.on("click submit" , "div#nothiddendivchild" , function () {
SensorRnt(9226);count++;});
div.on("click" , function () {
SensorRnt(9227);clicks++;});
div.off(undefined , "**");
jQuery("div#nothiddendivchild").trigger("click");
jQuery("div#nothiddendivchild").trigger("submit");
equal(count , 0 , "Make sure no events were triggered.");
div.trigger("click");
equal(clicks , 2 , "Make sure delegated and directly bound event occurred.");
div.off("click");SensorRnt(9225);});
test("on with multiple delegated events" , function () {
SensorRnt(9228);expect(1);
var count = 0, div = jQuery("#body");
div.on("click submit" , "div#nothiddendivchild" , function () {
SensorRnt(9230);count++;});
jQuery("div#nothiddendivchild").trigger("click");
jQuery("div#nothiddendivchild").trigger("submit");
equal(count , 2 , "Make sure both the click and submit were triggered.");
jQuery("#body").off(undefined , "**");SensorRnt(9229);});
test("delegated on with change" , function () {
SensorRnt(9231);expect(8);
var select, checkbox, checkboxFunction, text, textChange, oldTextVal, password, passwordChange, oldPasswordVal, selectChange = 0, checkboxChange = 0;
select = jQuery("select[name='S1']");
jQuery("#body").on("change" , "select[name='S1']" , function () {SensorRnt(9233);
selectChange++;});
checkbox = jQuery("#check2");
checkboxFunction = function () {SensorRnt(9234);
checkboxChange++;};
jQuery("#body").on("change" , "#check2" , checkboxFunction);
selectChange = 0;
select[0].selectedIndex = select[0].selectedIndex?0:1;
select.trigger("change");
equal(selectChange , 1 , "Change on click.");
selectChange = 0;
select[0].selectedIndex = select[0].selectedIndex?0:1;
select.trigger("change");
equal(selectChange , 1 , "Change on keyup.");
checkbox.trigger("change");
equal(checkboxChange , 1 , "Change on checkbox.");
text = jQuery("#name");
textChange = 0;
oldTextVal = text.val();
jQuery("#body").on("change" , "#name" , function () {SensorRnt(9235);
textChange++;});
text.val(oldTextVal+"foo");
text.trigger("change");
equal(textChange , 1 , "Change on text input.");
text.val(oldTextVal);
jQuery("#body").off("change" , "#name");
password = jQuery("#name");
passwordChange = 0;
oldPasswordVal = password.val();
jQuery("#body").on("change" , "#name" , function () {SensorRnt(9236);
passwordChange++;});
password.val(oldPasswordVal+"foo");
password.trigger("change");
equal(passwordChange , 1 , "Change on password input.");
password.val(oldPasswordVal);
jQuery("#body").off("change" , "#name");
selectChange = 0;
jQuery("#body").off("change" , "select[name='S1']");
select[0].selectedIndex = select[0].selectedIndex?0:1;
select.trigger("change");
equal(selectChange , 0 , "Die on click works.");
selectChange = 0;
select[0].selectedIndex = select[0].selectedIndex?0:1;
select.trigger("change");
equal(selectChange , 0 , "Die on keyup works.");
jQuery("#body").off("change" , "#check2" , checkboxFunction);
checkbox.trigger("change");
equal(checkboxChange , 1 , "Die on checkbox.");SensorRnt(9232);});
test("delegated on with submit" , function () {
SensorRnt(9237);expect(2);
var count1 = 0, count2 = 0;
jQuery("#body").on("submit" , "#testForm" , function (ev) {
count1++;
SensorRnt(9239);ev.preventDefault();SensorRnt(9240);});
jQuery(document).on("submit" , "body" , function (ev) {
count2++;
SensorRnt(9241);ev.preventDefault();SensorRnt(9242);});
jQuery("#testForm input[name=sub1]").trigger("submit");
equal(count1 , 1 , "Verify form submit.");
equal(count2 , 1 , "Verify body submit.");
jQuery("#body").off(undefined , "**");
jQuery(document).off(undefined , "**");SensorRnt(9238);});
test("delegated off() with only namespaces" , function () {
SensorRnt(9243);expect(2);
var $delegate = jQuery("#liveHandlerOrder"), count = 0;
$delegate.on("click.ns" , "a" , function () {SensorRnt(9245);
count++;});
jQuery("a" , $delegate).eq(0).trigger("click.ns");
equal(count , 1 , "delegated click.ns");
$delegate.off(".ns" , "**");
jQuery("a" , $delegate).eq(1).trigger("click.ns");
equal(count , 1 , "no more .ns after off");SensorRnt(9244);});
test("Non DOM element events" , function () {
SensorRnt(9246);expect(1);
var o = {

};
jQuery(o).on("nonelementobj" , function () {
SensorRnt(9248);ok(true , "Event on non-DOM object triggered");SensorRnt(9249);});
jQuery(o).trigger("nonelementobj");SensorRnt(9247);});
test("inline handler returning false stops default" , function () {
SensorRnt(9250);expect(1);
var markup = jQuery("<div><a href=\"#\" onclick=\"return false\">x</a></div>");
markup.on("click" , function (e) {
SensorRnt(9252);ok(e.isDefaultPrevented() , "inline handler prevented default");
RNTRETRET = false;
SensorRnt(9253);return RNTRETRET;});
markup.find("a").trigger("click");
markup.off("click");SensorRnt(9251);});
test("window resize" , function () {
SensorRnt(9254);expect(2);
jQuery(window).off();
jQuery(window).on("resize" , function () {
ok(true , "Resize event fired.");}).trigger("resize").off("resize");
ok(! jQuery._data(window , "events") , "Make sure all the events are gone.");SensorRnt(9255);});
test("focusin bubbles" , function () {
SensorRnt(9256);expect(2);
var input = jQuery("<input type='text' />").prependTo("body"), order = 0;
input[0].focus();
jQuery("body").on("focusin.focusinBubblesTest" , function () {
SensorRnt(9258);equal(1 , order++ , "focusin on the body second");SensorRnt(9259);});
input.on("focusin.focusinBubblesTest" , function () {
SensorRnt(9260);equal(0 , order++ , "focusin on the element first");SensorRnt(9261);});
order = 0;
input.trigger("focus");
input.remove();
jQuery("body").off("focusin.focusinBubblesTest");SensorRnt(9257);});
test("custom events with colons (#3533, #8272)" , function () {
SensorRnt(9262);expect(1);
var tab = jQuery("<table><tr><td>trigger</td></tr></table>").appendTo("body");
SensorRnt(9263);try{ 
SensorRnt(9264);tab.trigger("back:forth");
ok(true , "colon events don't throw");
}
catch(e){ 
SensorRnt(9265);ok(false , "colon events die");
}
SensorRnt(9266);tab.remove();SensorRnt(9267);});
test(".on and .off" , function () {
SensorRnt(9268);expect(9);
var counter, mixfn, data, $onandoff = jQuery("<div id=\"onandoff\"><p>on<b>and</b>off</p><div>worked<em>or</em>borked?</div></div>").appendTo("body");
jQuery("#onandoff").on("whip" , function () {
ok(true , "whipped it good");}).trigger("whip").off();
counter = 0;
jQuery("#onandoff b").on("click" , 5 , function ( e, trig ) {
counter += e.data+(trig||9);}).one("click" , 7 , function ( e, trig ) {
counter += e.data+(trig||11);}).trigger("click").trigger("click" , 17).off("click");
equal(counter , 54 , "direct event bindings with data");
counter = 0;
jQuery("#onandoff").on("click" , "em" , 5 , function ( e, trig ) {
counter += e.data+(trig||9);}).one("click" , "em" , 7 , function ( e, trig ) {
counter += e.data+(trig||11);}).find("em").trigger("click").trigger("click" , 17).end().off("click" , "em");
equal(counter , 54 , "delegated event bindings with data");
counter = 0;
mixfn = function (e, trig) {SensorRnt(9270);
counter += (e.data||0)+(trig||1);};
jQuery("#onandoff").on(" click  clack cluck " , "em" , 2 , mixfn).on("cluck" , "b" , 7 , mixfn).on("cluck" , mixfn).trigger("what!").each(function () {
equal(counter , 0 , "nothing triggered yet");}).find("em").one("cluck" , 3 , mixfn).trigger("cluck" , 8).off().trigger("cluck" , 9).end().each(function () {
equal(counter , 49 , "after triggering em element");}).off("cluck" , function () {}).trigger("cluck" , 2).each(function () {
equal(counter , 51 , "after triggering #onandoff cluck");}).find("b").on("click" , 95 , mixfn).on("clack" , "p" , 97 , mixfn).one("cluck" , 3 , mixfn).trigger("quack" , 19).off("click clack cluck").end().each(function () {
equal(counter , 51 , "after triggering b");}).trigger("cluck" , 3).off("clack" , "em" , mixfn).find("em").trigger("clack").end().each(function () {
equal(counter , 54 , "final triggers");}).off("click cluck");
data = jQuery.data[jQuery("#onandoff")[0].expando]||{

};
equal(data["events"] , undefined , "no events left");
$onandoff.remove();SensorRnt(9269);});
test("special on name mapping" , function () {
SensorRnt(9271);expect(7);
jQuery.event.special["slap"] = {
bindType : "click",
delegateType : "swing",
handle : function ( event ) {
SensorRnt(9273);equal(event.handleObj.origType , "slap" , "slapped your mammy, "+event.type);SensorRnt(9274);}
};
var comeback = function ( event ) {
SensorRnt(9275);ok(true , "event "+event.type+" triggered");SensorRnt(9276);};
jQuery("<div><button id=\"mammy\">Are We Not Men?</button></div>").on("slap" , "button" , jQuery.noop).on("swing" , "button" , comeback).find("button").on("slap" , jQuery.noop).on("click" , comeback).trigger("click").off("slap").trigger("click").off("click").trigger("swing").end().off("slap swing" , "button").find("button").trigger("slap").trigger("click").trigger("swing").end().remove();
delete jQuery.event.special["slap"];
jQuery.event.special["gutfeeling"] = {
bindType : "click",
delegateType : "click",
handle : function ( event ) {
SensorRnt(9277);equal(event.handleObj.origType , "gutfeeling" , "got a gutfeeling");
RNTRETRET = event.handleObj.handler.call(this , event);
SensorRnt(9278);return RNTRETRET;}
};
jQuery("<p>Gut Feeling</p>").on("click" , jQuery.noop).on("gutfeeling" , jQuery.noop).off("click").trigger("gutfeeling").remove();
jQuery("<p>Gut Feeling</p>").on("gutfeeling.Devo" , jQuery.noop).off(".Devo").trigger("gutfeeling").remove();
jQuery("<p>Gut Feeling</p>").one("gutfeeling" , jQuery.noop).trigger("gutfeeling").trigger("gutfeeling").remove();
delete jQuery.event.special["gutfeeling"];SensorRnt(9272);});
test(".on and .off, selective mixed removal (#10705)" , function () {
SensorRnt(9279);expect(7);
var timingx = function ( e ) {
SensorRnt(9281);ok(true , "triggered "+e.type);SensorRnt(9282);};
jQuery("<p>Strange Pursuit</p>").on("click" , timingx).on("click.duty" , timingx).on("click.now" , timingx).on("devo" , timingx).on("future" , timingx).trigger("click").trigger("devo").off(".duty devo ").trigger("future").trigger("click").off("future click").trigger("click");SensorRnt(9280);});
test(".on( event-map, null-selector, data ) #11130" , function () {
SensorRnt(9283);expect(1);
var $p = jQuery("<p>Strange Pursuit</p>"), data = "bar", map = {
"foo" : function ( event ) {
SensorRnt(9285);equal(event.data , "bar" , "event.data correctly relayed with null selector");
$p.remove();SensorRnt(9286);}
};
$p.on(map , null , data).trigger("foo");SensorRnt(9284);});
test("clone() delegated events (#11076)" , function () {
SensorRnt(9287);expect(3);
var counter = {
"center" : 0,
"fold" : 0,
"centerfold" : 0
}, clicked = function () {
SensorRnt(9289);counter[jQuery(this).text().replace(/\s+/ , "")]++;SensorRnt(9290);}, table = jQuery("<table><tr><td>center</td><td>fold</td></tr></table>").on("click" , "tr" , clicked).on("click" , "td:first-child" , clicked).on("click" , "td:last-child" , clicked), clone = table.clone(true);
clone.find("td").trigger("click");
equal(counter["center"] , 1 , "first child");
equal(counter["fold"] , 1 , "last child");
equal(counter["centerfold"] , 2 , "all children");
table.remove();
clone.remove();SensorRnt(9288);});
test("checkbox state (#3827)" , function () {
SensorRnt(9291);expect(9);
var markup = jQuery("<div><input type=checkbox><div>").appendTo("#qunit-fixture"), cb = markup.find("input")[0];
jQuery(cb).on("click" , function () {
SensorRnt(9293);equal(this.checked , false , "just-clicked checkbox is not checked");SensorRnt(9294);});
markup.on("click" , function () {
SensorRnt(9295);equal(cb.checked , false , "checkbox is not checked in bubbled event");SensorRnt(9296);});
cb.checked = true;
equal(cb.checked , true , "native - checkbox is initially checked");
cb.click();
equal(cb.checked , false , "native - checkbox is no longer checked");
cb.checked = true;
equal(cb.checked , true , "jQuery - checkbox is initially checked");
jQuery(cb).trigger("click");
equal(cb.checked , false , "jQuery - checkbox is no longer checked");
jQuery(cb).triggerHandler("click");SensorRnt(9292);});
test("hover event no longer special since 1.9" , function () {
SensorRnt(9297);expect(1);
jQuery("<div>craft</div>").on("hover" , function ( e ) {
equal(e.type , "hover" , "I am hovering!");}).trigger("hover").off("hover");SensorRnt(9298);});
test("fixHooks extensions" , function () {
SensorRnt(9299);expect(2);
var $fixture = jQuery("<input type='text' id='hook-fixture' />").appendTo("body"), saved = jQuery.event.fixHooks.click;
$fixture.on("click" , function ( event ) {
SensorRnt(9301);ok(! ("blurrinessLevel" in event) , "event.blurrinessLevel does not exist");SensorRnt(9302);});
fireNative($fixture[0] , "click");
$fixture.off("click");
jQuery.event.fixHooks.click = {
filter : function ( event ) {
event.blurrinessLevel = 42;
RNTRETRET = event;
SensorRnt(9303);return RNTRETRET;}
};
$fixture.on("click" , function ( event ) {
SensorRnt(9304);equal(event.blurrinessLevel , 42 , "event.blurrinessLevel was set");SensorRnt(9305);});
fireNative($fixture[0] , "click");
delete jQuery.event.fixHooks.click;
$fixture.off("click").remove();
jQuery.event.fixHooks.click = saved;SensorRnt(9300);});
test("focusin using non-element targets" , function () {
SensorRnt(9306);expect(2);
jQuery(document).on("focusin" , function ( e ) {
ok(e.type==="focusin" , "got a focusin event on a document");}).trigger("focusin").off("focusin");
jQuery(window).on("focusin" , function ( e ) {
ok(e.type==="focusin" , "got a focusin event on a window");}).trigger("focusin").off("focusin");SensorRnt(9307);});
testIframeWithCallback("focusin from an iframe" , "event/focusinCrossFrame.html" , function ( frameDoc ) {
SensorRnt(9308);expect(1);
var input = jQuery(frameDoc).find("#frame-input");
jQuery("body").on("focusin.iframeTest" , function () {
SensorRnt(9310);ok(false , "fired a focusin event in the parent document");SensorRnt(9311);});
input.on("focusin" , function () {
SensorRnt(9312);ok(true , "fired a focusin event in the iframe");SensorRnt(9313);});
input.trigger("focusin");
input.remove();
input.trigger("focusin");
jQuery("body").off("focusin.iframeTest");SensorRnt(9309);});
testIframeWithCallback("jQuery.ready promise" , "event/promiseReady.html" , function ( isOk ) {
SensorRnt(9314);expect(1);
ok(isOk , "$.when( $.ready ) works");SensorRnt(9315);});
testIframeWithCallback("Focusing iframe element" , "event/focusElem.html" , function ( isOk ) {
SensorRnt(9316);expect(1);
ok(isOk , "Focused an element in an iframe");SensorRnt(9317);});
testIframeWithCallback("triggerHandler(onbeforeunload)" , "event/triggerunload.html" , function ( isOk ) {
SensorRnt(9318);expect(1);
ok(isOk , "Triggered onbeforeunload without an error");SensorRnt(9319);});
SensorRnt(9374);if (hasPHP)
{ 
SensorRnt(9375);testIframeWithCallback("jQuery.ready synchronous load with long loading subresources" , "event/syncReady.html" , function ( isOk ) {
SensorRnt(9320);expect(1);
ok(isOk , "jQuery loaded synchronously fires ready when the DOM can truly be interacted with");});SensorRnt(9321);
}
SensorRnt(9376);test("change handler should be detached from element" , function () {
SensorRnt(9322);expect(2);
var $fixture = jQuery("<input type='text' id='change-ie-leak' />").appendTo("body"), originRemoveEvent = jQuery.removeEvent, wrapperRemoveEvent = function (elem, type, handle) {
SensorRnt(9324);equal("change" , type , "Event handler for 'change' event should be removed");
equal("change-ie-leak" , jQuery(elem).attr("id") , "Event handler for 'change' event should be removed from appropriate element");
originRemoveEvent(elem , type , handle);SensorRnt(9325);};
jQuery.removeEvent = wrapperRemoveEvent;
$fixture.on("change" , function () {SensorRnt(9326);});
$fixture.off("change");
$fixture.remove();
jQuery.removeEvent = originRemoveEvent;SensorRnt(9323);});
asyncTest("trigger click on checkbox, fires change event" , function () {
SensorRnt(9327);expect(1);
var check = jQuery("#check2");
check.on("change" , function () {
check.off("change");
ok(true , "Change event fired as a result of triggered click");
start();}).trigger("click");SensorRnt(9328);});
test("Namespace preserved when passed an Event (#12739)" , function () {
SensorRnt(9329);expect(4);
var markup = jQuery("<div id='parent'><div id='child'></div></div>"), triggered = 0, fooEvent;
markup.find("div").addBack().on("foo.bar" , function ( e ) {
if (! e.handled)
{ 
triggered++;
e.handled = true;
equal(e.namespace , "bar" , "namespace is bar");
jQuery(e.target).find("div").each(function () {
jQuery(this).triggerHandler(e);});
}}).on("foo.bar2" , function () {
SensorRnt(9331);ok(false , "foo.bar2 called on trigger "+triggered+" id "+this.id);SensorRnt(9332);});
markup.trigger("foo.bar");
markup.trigger(jQuery.Event("foo.bar"));
fooEvent = jQuery.Event("foo");
fooEvent.namespace = "bar";
markup.trigger(fooEvent);
markup.remove();
equal(triggered , 3 , "foo.bar triggered");SensorRnt(9330);});
test("make sure events cloned correctly" , 18 , function () {
SensorRnt(9333);var clone, fixture = jQuery("#qunit-fixture"), checkbox = jQuery("#check1"), p = jQuery("#firstp");
fixture.on("click change" , function ( event, result ) {
ok(result , event.type+" on original element is fired");}).on("click" , "#firstp" , function ( event, result ) {
ok(result , "Click on original child element though delegation is fired");}).on("change" , "#check1" , function ( event, result ) {
SensorRnt(9335);ok(result , "Change on original child element though delegation is fired");SensorRnt(9336);});
p.on("click" , function () {
SensorRnt(9337);ok(true , "Click on original child element is fired");SensorRnt(9338);});
checkbox.on("change" , function () {
SensorRnt(9339);ok(true , "Change on original child element is fired");SensorRnt(9340);});
fixture.clone().trigger("click").trigger("change");
clone = fixture.clone(true);
clone.find("p").eq(0).trigger("click" , true);
clone.find("#check1").trigger("change" , true);
clone.remove();
clone = fixture.clone(true , true);
clone.find("p").eq(0).trigger("click" , true);
clone.find("#check1").trigger("change" , true);
fixture.off();
p.off();
checkbox.off();
p.trigger("click");
checkbox.trigger("change");
clone.find("p").eq(0).trigger("click" , true);
clone.find("#check1").trigger("change" , true);
clone.remove();
clone.find("p").eq(0).trigger("click");
clone.find("#check1").trigger("change");SensorRnt(9334);});
test("String.prototype.namespace does not cause trigger() to throw (#13360)" , function () {
SensorRnt(9341);expect(1);
var errored = false;
String.prototype.namespace = function () {SensorRnt(9347);};
SensorRnt(9342);try{ 
SensorRnt(9343);jQuery("<p>").trigger("foo.bar");
}
catch(e){ 
SensorRnt(9344);errored = true;
}
SensorRnt(9345);equal(errored , false , "trigger() did not throw exception");
delete String.prototype.namespace;SensorRnt(9346);});
test("Inline event result is returned (#13993)" , function () {
SensorRnt(9348);expect(1);
var result = jQuery("<p onclick='return 42'>hello</p>").triggerHandler("click");
equal(result , 42 , "inline handler returned value");SensorRnt(9349);});
SensorRnt(9377);if (! (/firefox/i.test(window.navigator.userAgent)))
{ 
SensorRnt(9378);test("Check order of focusin/focusout events" , 2 , function () {
SensorRnt(9350);var focus, blur, input = jQuery("#name");
input.on("focus" , function () {
focus = true;}).on("focusin" , function () {
ok(! focus , "Focusin event should fire before focus does");}).on("blur" , function () {
blur = true;}).on("focusout" , function () {
SensorRnt(9352);ok(! blur , "Focusout event should fire before blur does");SensorRnt(9353);});
input.trigger("focus");
jQuery("#search").trigger("focus");
input.off();SensorRnt(9351);});
test("focus-blur order (#12868)" , function () {
SensorRnt(9354);expect(5);
var order, $text = jQuery("#text1"), $radio = jQuery("#radio1").trigger("focus");
stop();
$radio[0].focus();
setTimeout(function () {
$text.on("focus" , function () {
equal(order++ , 1 , "text focus");}).on("blur" , function () {SensorRnt(9356);
SensorRnt(9358);equal(order++ , 0 , "text blur");SensorRnt(9359);});
$radio.on("focus" , function () {
equal(order++ , 1 , "radio focus");}).on("blur" , function () {
SensorRnt(9360);equal(order++ , 0 , "radio blur");SensorRnt(9361);});
order = 0;
equal(document.activeElement , $radio[0] , "radio has focus");
$text.trigger("focus");
setTimeout(function () {
SensorRnt(9362);equal(document.activeElement , $text[0] , "text has focus");
order = 1;
$radio.triggerHandler("focus");
$text.off();
start();SensorRnt(9363);} , 50);SensorRnt(9357);} , 50);});SensorRnt(9355);
}
SensorRnt(9379);