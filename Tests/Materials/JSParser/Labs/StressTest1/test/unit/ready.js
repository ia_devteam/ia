function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open("GET", "http://localhost:8080/"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} SensorRnt(9978);module("event");
(function () {
var notYetReady, noEarlyExecution, order = [ ], args = {

};
notYetReady = ! jQuery.isReady;
SensorRnt(9969);test("jQuery.isReady" , function () {
SensorRnt(9971);expect(2);
equal(notYetReady , true , "jQuery.isReady should not be true before DOM ready");
equal(jQuery.isReady , true , "jQuery.isReady should be true once DOM is ready");SensorRnt(9972);});
function makeHandler ( testId ) {
RNTRETRET = function ( arg ) {
SensorRnt(9974);order.push(testId);
args[testId] = arg;SensorRnt(9975);};
SensorRnt(9973);return RNTRETRET;}
jQuery(makeHandler("a"));
jQuery(document).ready(makeHandler("b"));
jQuery(document).on("ready.readytest" , makeHandler("c"));
jQuery(makeHandler("d"));
jQuery(document).ready(makeHandler("e"));
jQuery(document).on("ready.readytest" , makeHandler("f"));
noEarlyExecution = order.length===0;
test("jQuery ready" , function () {
SensorRnt(9976);expect(10);
ok(noEarlyExecution , "Handlers bound to DOM ready should not execute before DOM ready");
deepEqual(order , [ "a", "b", "d", "e", "c", "f"] , "Bound DOM ready handlers should execute in on-order, but those bound with jQuery(document).on( 'ready', fn ) will always execute last");
equal(args["a"] , jQuery , "Argument passed to fn in jQuery( fn ) should be jQuery");
equal(args["b"] , jQuery , "Argument passed to fn in jQuery(document).ready( fn ) should be jQuery");
ok(args["c"] instanceof jQuery.Event , "Argument passed to fn in jQuery(document).on( 'ready', fn ) should be an event object");
order = [ ];
jQuery(makeHandler("g"));
equal(order.pop() , "g" , "Event handler should execute immediately");
equal(args["g"] , jQuery , "Argument passed to fn in jQuery( fn ) should be jQuery");
jQuery(document).ready(makeHandler("h"));
equal(order.pop() , "h" , "Event handler should execute immediately");
equal(args["h"] , jQuery , "Argument passed to fn in jQuery(document).ready( fn ) should be jQuery");
jQuery(document).on("ready.readytest" , makeHandler("never"));
equal(order.length , 0 , "Event handler should never execute since DOM ready has already passed");
jQuery(document).off("ready.readytest");SensorRnt(9977);});SensorRnt(9970);})();
