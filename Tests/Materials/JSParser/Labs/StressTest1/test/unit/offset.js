function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open("GET", "http://localhost:8080/"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} (function () {
SensorRnt(9809);if (! jQuery.fn.offset)
{ 
SensorRnt(9810);return;
}
var supportsScroll, supportsFixedPosition, forceScroll = jQuery("<div/>").css({
width : 2000,
height : 2000
}), checkSupport = function () {SensorRnt(9811);
checkSupport = false;
SensorRnt(9813);var checkFixed = jQuery("<div/>").css({
position : "fixed",
top : "20px"
}).appendTo("#qunit-fixture");
forceScroll.appendTo("body");
window.scrollTo(200 , 200);
supportsScroll = document.documentElement.scrollTop||document.body.scrollTop;
forceScroll.detach();
supportsFixedPosition = checkFixed[0].offsetTop===20;
checkFixed.remove();SensorRnt(9814);};
module("offset" , {
setup : function () {
SensorRnt(9815);if (typeof checkSupport==="function")
{ 
checkSupport();SensorRnt(9816);
}
SensorRnt(9817);forceScroll.appendTo("body");
window.scrollTo(1 , 1);
forceScroll.detach();SensorRnt(9818);},
teardown : moduleTeardown
});
test("empty set" , function () {
SensorRnt(9819);expect(2);
strictEqual(jQuery().offset() , undefined , "offset() returns undefined for empty set (#11962)");
strictEqual(jQuery().position() , undefined , "position() returns undefined for empty set (#11962)");SensorRnt(9820);});
test("object without getBoundingClientRect" , function () {
SensorRnt(9821);expect(2);
var result = jQuery({
ownerDocument : document
}).offset();
equal(result.top , 0 , "Check top");
equal(result.left , 0 , "Check left");SensorRnt(9822);});
test("disconnected node" , function () {
SensorRnt(9823);expect(2);
var result = jQuery(document.createElement("div")).offset();
equal(result.top , 0 , "Check top");
equal(result.left , 0 , "Check left");SensorRnt(9824);});
testIframe("offset/absolute" , "absolute" , function ($, iframe) {
SensorRnt(9825);expect(4);
var doc = iframe.document, tests;
tests = [ {
"id" : "#absolute-1",
"top" : 1,
"left" : 1
}];
jQuery.each(tests , function () {
SensorRnt(9827);equal(jQuery(this["id"] , doc).offset().top , this["top"] , "jQuery('"+this["id"]+"').offset().top");
equal(jQuery(this["id"] , doc).offset().left , this["left"] , "jQuery('"+this["id"]+"').offset().left");SensorRnt(9828);});
tests = [ {
"id" : "#absolute-1",
"top" : 0,
"left" : 0
}];
jQuery.each(tests , function () {
SensorRnt(9829);equal(jQuery(this["id"] , doc).position().top , this["top"] , "jQuery('"+this["id"]+"').position().top");
equal(jQuery(this["id"] , doc).position().left , this["left"] , "jQuery('"+this["id"]+"').position().left");SensorRnt(9830);});SensorRnt(9826);});
testIframe("offset/absolute" , "absolute" , function ( $ ) {
SensorRnt(9831);expect(178);
var tests, offset;
tests = [ {
"id" : "#absolute-1",
"top" : 1,
"left" : 1
}, {
"id" : "#absolute-1-1",
"top" : 5,
"left" : 5
}, {
"id" : "#absolute-1-1-1",
"top" : 9,
"left" : 9
}, {
"id" : "#absolute-2",
"top" : 20,
"left" : 20
}];
jQuery.each(tests , function () {
SensorRnt(9833);equal($(this["id"]).offset().top , this["top"] , "jQuery('"+this["id"]+"').offset().top");
equal($(this["id"]).offset().left , this["left"] , "jQuery('"+this["id"]+"').offset().left");SensorRnt(9834);});
tests = [ {
"id" : "#absolute-1",
"top" : 0,
"left" : 0
}, {
"id" : "#absolute-1-1",
"top" : 1,
"left" : 1
}, {
"id" : "#absolute-1-1-1",
"top" : 1,
"left" : 1
}, {
"id" : "#absolute-2",
"top" : 19,
"left" : 19
}];
jQuery.each(tests , function () {
SensorRnt(9835);equal($(this["id"]).position().top , this["top"] , "jQuery('"+this["id"]+"').position().top");
equal($(this["id"]).position().left , this["left"] , "jQuery('"+this["id"]+"').position().left");SensorRnt(9836);});
offset = $("#positionTest").offset({
"top" : 10,
"left" : 10
}).offset();
equal(offset.top , 10 , "Setting offset on element with position absolute but 'auto' values.");
equal(offset.left , 10 , "Setting offset on element with position absolute but 'auto' values.");
tests = [ {
"id" : "#absolute-2",
"top" : 30,
"left" : 30
}, {
"id" : "#absolute-2",
"top" : 10,
"left" : 10
}, {
"id" : "#absolute-2",
"top" : - 1,
"left" : - 1
}, {
"id" : "#absolute-2",
"top" : 19,
"left" : 19
}, {
"id" : "#absolute-1-1-1",
"top" : 15,
"left" : 15
}, {
"id" : "#absolute-1-1-1",
"top" : 5,
"left" : 5
}, {
"id" : "#absolute-1-1-1",
"top" : - 1,
"left" : - 1
}, {
"id" : "#absolute-1-1-1",
"top" : 9,
"left" : 9
}, {
"id" : "#absolute-1-1",
"top" : 10,
"left" : 10
}, {
"id" : "#absolute-1-1",
"top" : 0,
"left" : 0
}, {
"id" : "#absolute-1-1",
"top" : - 1,
"left" : - 1
}, {
"id" : "#absolute-1-1",
"top" : 5,
"left" : 5
}, {
"id" : "#absolute-1",
"top" : 2,
"left" : 2
}, {
"id" : "#absolute-1",
"top" : 0,
"left" : 0
}, {
"id" : "#absolute-1",
"top" : - 1,
"left" : - 1
}, {
"id" : "#absolute-1",
"top" : 1,
"left" : 1
}];
jQuery.each(tests , function () {
SensorRnt(9837);$(this["id"]).offset({
"top" : this["top"],
"left" : this["left"]
});
equal($(this["id"]).offset().top , this["top"] , "jQuery('"+this["id"]+"').offset({ top: "+this["top"]+" })");
equal($(this["id"]).offset().left , this["left"] , "jQuery('"+this["id"]+"').offset({ left: "+this["left"]+" })");
var top = this["top"], left = this["left"];
$(this["id"]).offset(function (i, val) {
SensorRnt(9839);equal(val.top , top , "Verify incoming top position.");
equal(val.left , left , "Verify incoming top position.");
RNTRETRET = {
"top" : top+1,
"left" : left+1
};
SensorRnt(9840);return RNTRETRET;});
equal($(this["id"]).offset().top , this["top"]+1 , "jQuery('"+this["id"]+"').offset({ top: "+(this["top"]+1)+" })");
equal($(this["id"]).offset().left , this["left"]+1 , "jQuery('"+this["id"]+"').offset({ left: "+(this["left"]+1)+" })");
$(this["id"]).offset({
"left" : this["left"]+2
}).offset({
"top" : this["top"]+2
});
equal($(this["id"]).offset().top , this["top"]+2 , "Setting one property at a time.");
equal($(this["id"]).offset().left , this["left"]+2 , "Setting one property at a time.");
$(this["id"]).offset({
"top" : this["top"],
"left" : this["left"],
"using" : function ( props ) {
SensorRnt(9841);$(this).css({
"top" : props.top+1,
"left" : props.left+1
});SensorRnt(9842);}
});
equal($(this["id"]).offset().top , this["top"]+1 , "jQuery('"+this["id"]+"').offset({ top: "+(this["top"]+1)+", using: fn })");
equal($(this["id"]).offset().left , this["left"]+1 , "jQuery('"+this["id"]+"').offset({ left: "+(this["left"]+1)+", using: fn })");SensorRnt(9838);});SensorRnt(9832);});
testIframe("offset/relative" , "relative" , function ( $ ) {
SensorRnt(9843);expect(60);
var tests = [ {
"id" : "#relative-1",
"top" : 7,
"left" : 7
}, {
"id" : "#relative-1-1",
"top" : 15,
"left" : 15
}, {
"id" : "#relative-2",
"top" : 142,
"left" : 27
}];
jQuery.each(tests , function () {
SensorRnt(9845);equal($(this["id"]).offset().top , this["top"] , "jQuery('"+this["id"]+"').offset().top");
equal($(this["id"]).offset().left , this["left"] , "jQuery('"+this["id"]+"').offset().left");SensorRnt(9846);});
tests = [ {
"id" : "#relative-1",
"top" : 6,
"left" : 6
}, {
"id" : "#relative-1-1",
"top" : 5,
"left" : 5
}, {
"id" : "#relative-2",
"top" : 141,
"left" : 26
}];
jQuery.each(tests , function () {
SensorRnt(9847);equal($(this["id"]).position().top , this["top"] , "jQuery('"+this["id"]+"').position().top");
equal($(this["id"]).position().left , this["left"] , "jQuery('"+this["id"]+"').position().left");SensorRnt(9848);});
tests = [ {
"id" : "#relative-2",
"top" : 200,
"left" : 50
}, {
"id" : "#relative-2",
"top" : 100,
"left" : 10
}, {
"id" : "#relative-2",
"top" : - 5,
"left" : - 5
}, {
"id" : "#relative-2",
"top" : 142,
"left" : 27
}, {
"id" : "#relative-1-1",
"top" : 100,
"left" : 100
}, {
"id" : "#relative-1-1",
"top" : 5,
"left" : 5
}, {
"id" : "#relative-1-1",
"top" : - 1,
"left" : - 1
}, {
"id" : "#relative-1-1",
"top" : 15,
"left" : 15
}, {
"id" : "#relative-1",
"top" : 100,
"left" : 100
}, {
"id" : "#relative-1",
"top" : 0,
"left" : 0
}, {
"id" : "#relative-1",
"top" : - 1,
"left" : - 1
}, {
"id" : "#relative-1",
"top" : 7,
"left" : 7
}];
jQuery.each(tests , function () {
SensorRnt(9849);$(this["id"]).offset({
"top" : this["top"],
"left" : this["left"]
});
equal($(this["id"]).offset().top , this["top"] , "jQuery('"+this["id"]+"').offset({ top: "+this["top"]+" })");
equal($(this["id"]).offset().left , this["left"] , "jQuery('"+this["id"]+"').offset({ left: "+this["left"]+" })");
$(this["id"]).offset({
"top" : this["top"],
"left" : this["left"],
"using" : function ( props ) {
SensorRnt(9851);$(this).css({
"top" : props.top+1,
"left" : props.left+1
});SensorRnt(9852);}
});
equal($(this["id"]).offset().top , this["top"]+1 , "jQuery('"+this["id"]+"').offset({ top: "+(this["top"]+1)+", using: fn })");
equal($(this["id"]).offset().left , this["left"]+1 , "jQuery('"+this["id"]+"').offset({ left: "+(this["left"]+1)+", using: fn })");SensorRnt(9850);});SensorRnt(9844);});
testIframe("offset/static" , "static" , function ( $ ) {
SensorRnt(9853);expect(80);
var tests = [ {
"id" : "#static-1",
"top" : 7,
"left" : 7
}, {
"id" : "#static-1-1",
"top" : 15,
"left" : 15
}, {
"id" : "#static-1-1-1",
"top" : 23,
"left" : 23
}, {
"id" : "#static-2",
"top" : 122,
left : 7
}];
jQuery.each(tests , function () {
SensorRnt(9855);equal($(this["id"]).offset().top , this["top"] , "jQuery('"+this["id"]+"').offset().top");
equal($(this["id"]).offset().left , this["left"] , "jQuery('"+this["id"]+"').offset().left");SensorRnt(9856);});
tests = [ {
"id" : "#static-1",
"top" : 6,
"left" : 6
}, {
"id" : "#static-1-1",
"top" : 14,
"left" : 14
}, {
"id" : "#static-1-1-1",
"top" : 22,
"left" : 22
}, {
"id" : "#static-2",
"top" : 121,
"left" : 6
}];
jQuery.each(tests , function () {
SensorRnt(9857);equal($(this["id"]).position().top , this["top"] , "jQuery('"+this["top"]+"').position().top");
equal($(this["id"]).position().left , this["left"] , "jQuery('"+this["left"]+"').position().left");SensorRnt(9858);});
tests = [ {
"id" : "#static-2",
"top" : 200,
"left" : 200
}, {
"id" : "#static-2",
"top" : 100,
"left" : 100
}, {
"id" : "#static-2",
"top" : - 2,
"left" : - 2
}, {
"id" : "#static-2",
"top" : 121,
"left" : 6
}, {
"id" : "#static-1-1-1",
"top" : 50,
"left" : 50
}, {
"id" : "#static-1-1-1",
"top" : 10,
"left" : 10
}, {
"id" : "#static-1-1-1",
"top" : - 1,
"left" : - 1
}, {
"id" : "#static-1-1-1",
"top" : 22,
"left" : 22
}, {
"id" : "#static-1-1",
"top" : 25,
"left" : 25
}, {
"id" : "#static-1-1",
"top" : 10,
"left" : 10
}, {
"id" : "#static-1-1",
"top" : - 3,
"left" : - 3
}, {
"id" : "#static-1-1",
"top" : 14,
"left" : 14
}, {
"id" : "#static-1",
"top" : 30,
"left" : 30
}, {
"id" : "#static-1",
"top" : 2,
"left" : 2
}, {
"id" : "#static-1",
"top" : - 2,
"left" : - 2
}, {
"id" : "#static-1",
"top" : 7,
"left" : 7
}];
jQuery.each(tests , function () {
SensorRnt(9859);$(this["id"]).offset({
"top" : this["top"],
"left" : this["left"]
});
equal($(this["id"]).offset().top , this["top"] , "jQuery('"+this["id"]+"').offset({ top: "+this["top"]+" })");
equal($(this["id"]).offset().left , this["left"] , "jQuery('"+this["id"]+"').offset({ left: "+this["left"]+" })");
$(this["id"]).offset({
"top" : this["top"],
"left" : this["left"],
"using" : function ( props ) {
SensorRnt(9861);$(this).css({
"top" : props.top+1,
"left" : props.left+1
});SensorRnt(9862);}
});
equal($(this["id"]).offset().top , this["top"]+1 , "jQuery('"+this["id"]+"').offset({ top: "+(this["top"]+1)+", using: fn })");
equal($(this["id"]).offset().left , this["left"]+1 , "jQuery('"+this["id"]+"').offset({ left: "+(this["left"]+1)+", using: fn })");SensorRnt(9860);});SensorRnt(9854);});
testIframe("offset/fixed" , "fixed" , function ( $ ) {
SensorRnt(9863);expect(34);
var tests, $noTopLeft;
tests = [ {
"id" : "#fixed-1",
"offsetTop" : 1001,
"offsetLeft" : 1001,
"positionTop" : 0,
"positionLeft" : 0
}, {
"id" : "#fixed-2",
"offsetTop" : 1021,
"offsetLeft" : 1021,
"positionTop" : 20,
"positionLeft" : 20
}];
jQuery.each(tests , function () {
SensorRnt(9868);if (! window.supportsScroll)
{ 
SensorRnt(9869);ok(true , "Browser doesn't support scroll position.");
ok(true , "Browser doesn't support scroll position.");
ok(true , "Browser doesn't support scroll position.");
ok(true , "Browser doesn't support scroll position.");
}
else
{
SensorRnt(9870);if (window.supportsFixedPosition)
{ 
SensorRnt(9871);equal($(this["id"]).offset().top , this["offsetTop"] , "jQuery('"+this["id"]+"').offset().top");
equal($(this["id"]).position().top , this["positionTop"] , "jQuery('"+this["id"]+"').position().top");
equal($(this["id"]).offset().left , this["offsetLeft"] , "jQuery('"+this["id"]+"').offset().left");
equal($(this["id"]).position().left , this["positionLeft"] , "jQuery('"+this["id"]+"').position().left");
}
else
{ 
SensorRnt(9872);ok(true , "Fixed position is not supported");
ok(true , "Fixed position is not supported");
ok(true , "Fixed position is not supported");
ok(true , "Fixed position is not supported");
}
SensorRnt(9873);}SensorRnt(9874);});
tests = [ {
"id" : "#fixed-1",
"top" : 100,
"left" : 100
}, {
"id" : "#fixed-1",
"top" : 0,
"left" : 0
}, {
"id" : "#fixed-1",
"top" : - 4,
"left" : - 4
}, {
"id" : "#fixed-2",
"top" : 200,
"left" : 200
}, {
"id" : "#fixed-2",
"top" : 0,
"left" : 0
}, {
"id" : "#fixed-2",
"top" : - 5,
"left" : - 5
}];
jQuery.each(tests , function () {
SensorRnt(9875);if (window.supportsFixedPosition)
{ 
SensorRnt(9876);$(this["id"]).offset({
"top" : this["top"],
"left" : this["left"]
});
equal($(this["id"]).offset().top , this["top"] , "jQuery('"+this["id"]+"').offset({ top: "+this["top"]+" })");
equal($(this["id"]).offset().left , this["left"] , "jQuery('"+this["id"]+"').offset({ left: "+this["left"]+" })");
$(this["id"]).offset({
"top" : this["top"],
"left" : this["left"],
"using" : function ( props ) {
SensorRnt(9879);$(this).css({
"top" : props.top+1,
"left" : props.left+1
});SensorRnt(9880);}
});
equal($(this["id"]).offset().top , this["top"]+1 , "jQuery('"+this["id"]+"').offset({ top: "+(this["top"]+1)+", using: fn })");
equal($(this["id"]).offset().left , this["left"]+1 , "jQuery('"+this["id"]+"').offset({ left: "+(this["left"]+1)+", using: fn })");
}
else
{ 
SensorRnt(9877);ok(true , "Fixed position is not supported");
ok(true , "Fixed position is not supported");
ok(true , "Fixed position is not supported");
ok(true , "Fixed position is not supported");
};SensorRnt(9878);});
$noTopLeft = $("#fixed-no-top-left");
SensorRnt(9864);if (window.supportsFixedPosition)
{ 
SensorRnt(9865);equal($noTopLeft.offset().top , 1007 , "Check offset top for fixed element with no top set");
equal($noTopLeft.offset().left , 1007 , "Check offset left for fixed element with no left set");
}
else
{ 
SensorRnt(9866);ok(true , "Fixed position is not supported");
ok(true , "Fixed position is not supported");
};SensorRnt(9867);});
testIframe("offset/table" , "table" , function ( $ ) {
SensorRnt(9881);expect(4);
equal($("#table-1").offset().top , 6 , "jQuery('#table-1').offset().top");
equal($("#table-1").offset().left , 6 , "jQuery('#table-1').offset().left");
equal($("#th-1").offset().top , 10 , "jQuery('#th-1').offset().top");
equal($("#th-1").offset().left , 10 , "jQuery('#th-1').offset().left");SensorRnt(9882);});
testIframe("offset/scroll" , "scroll" , function ( $, win ) {
SensorRnt(9883);expect(24);
equal($("#scroll-1").offset().top , 7 , "jQuery('#scroll-1').offset().top");
equal($("#scroll-1").offset().left , 7 , "jQuery('#scroll-1').offset().left");
equal($("#scroll-1-1").offset().top , 11 , "jQuery('#scroll-1-1').offset().top");
equal($("#scroll-1-1").offset().left , 11 , "jQuery('#scroll-1-1').offset().left");
equal($("#scroll-1").scrollTop() , 5 , "jQuery('#scroll-1').scrollTop()");
equal($("#scroll-1").scrollLeft() , 5 , "jQuery('#scroll-1').scrollLeft()");
equal($("#scroll-1-1").scrollTop() , 0 , "jQuery('#scroll-1-1').scrollTop()");
equal($("#scroll-1-1").scrollLeft() , 0 , "jQuery('#scroll-1-1').scrollLeft()");
equal($("#scroll-1").scrollTop(undefined).scrollTop() , 5 , ".scrollTop(undefined) is chainable (#5571)");
equal($("#scroll-1").scrollLeft(undefined).scrollLeft() , 5 , ".scrollLeft(undefined) is chainable (#5571)");
win.name = "test";
SensorRnt(9884);if (! window.supportsScroll)
{ 
SensorRnt(9885);ok(true , "Browser doesn't support scroll position.");
ok(true , "Browser doesn't support scroll position.");
ok(true , "Browser doesn't support scroll position.");
ok(true , "Browser doesn't support scroll position.");
}
else
{ 
SensorRnt(9886);equal($(win).scrollTop() , 1000 , "jQuery(window).scrollTop()");
equal($(win).scrollLeft() , 1000 , "jQuery(window).scrollLeft()");
equal($(win.document).scrollTop() , 1000 , "jQuery(document).scrollTop()");
equal($(win.document).scrollLeft() , 1000 , "jQuery(document).scrollLeft()");
}
SensorRnt(9887);window.scrollTo(0 , 0);
equal($(window).scrollTop() , 0 , "jQuery(window).scrollTop() other window");
equal($(window).scrollLeft() , 0 , "jQuery(window).scrollLeft() other window");
equal($(document).scrollTop() , 0 , "jQuery(window).scrollTop() other document");
equal($(document).scrollLeft() , 0 , "jQuery(window).scrollLeft() other document");
notEqual($().scrollTop(100) , null , "jQuery().scrollTop(100) testing setter on empty jquery object");
notEqual($().scrollLeft(100) , null , "jQuery().scrollLeft(100) testing setter on empty jquery object");
notEqual($().scrollTop(null) , null , "jQuery().scrollTop(null) testing setter on empty jquery object");
notEqual($().scrollLeft(null) , null , "jQuery().scrollLeft(null) testing setter on empty jquery object");
strictEqual($().scrollTop() , null , "jQuery().scrollTop(100) testing setter on empty jquery object");
strictEqual($().scrollLeft() , null , "jQuery().scrollLeft(100) testing setter on empty jquery object");SensorRnt(9888);});
testIframe("offset/body" , "body" , function ( $ ) {
SensorRnt(9889);expect(4);
equal($("body").offset().top , 1 , "jQuery('#body').offset().top");
equal($("body").offset().left , 1 , "jQuery('#body').offset().left");
equal($("#firstElement").position().left , 5 , "$('#firstElement').position().left");
equal($("#firstElement").position().top , 5 , "$('#firstElement').position().top");SensorRnt(9890);});
test("chaining" , function () {
SensorRnt(9891);expect(3);
var coords = {
"top" : 1,
"left" : 1
};
equal(jQuery("#absolute-1").offset(coords).selector , "#absolute-1" , "offset(coords) returns jQuery object");
equal(jQuery("#non-existent").offset(coords).selector , "#non-existent" , "offset(coords) with empty jQuery set returns jQuery object");
equal(jQuery("#absolute-1").offset(undefined).selector , "#absolute-1" , "offset(undefined) returns jQuery object (#5571)");SensorRnt(9892);});
test("offsetParent" , function () {
SensorRnt(9893);expect(13);
var body, header, div, area;
body = jQuery("body").offsetParent();
equal(body.length , 1 , "Only one offsetParent found.");
equal(body[0] , document.documentElement , "The html element is the offsetParent of the body.");
header = jQuery("#qunit").offsetParent();
equal(header.length , 1 , "Only one offsetParent found.");
equal(header[0] , document.documentElement , "The html element is the offsetParent of #qunit.");
div = jQuery("#nothiddendivchild").offsetParent();
equal(div.length , 1 , "Only one offsetParent found.");
equal(div[0] , document.getElementById("qunit-fixture") , "The #qunit-fixture is the offsetParent of #nothiddendivchild.");
jQuery("#nothiddendiv").css("position" , "relative");
div = jQuery("#nothiddendivchild").offsetParent();
equal(div.length , 1 , "Only one offsetParent found.");
equal(div[0] , jQuery("#nothiddendiv")[0] , "The div is the offsetParent.");
div = jQuery("body, #nothiddendivchild").offsetParent();
equal(div.length , 2 , "Two offsetParent found.");
equal(div[0] , document.documentElement , "The html element is the offsetParent of the body.");
equal(div[1] , jQuery("#nothiddendiv")[0] , "The div is the offsetParent.");
area = jQuery("#imgmap area").offsetParent();
equal(area[0] , document.documentElement , "The html element is the offsetParent of the body.");
div = jQuery("<div>").css({
"position" : "absolute"
}).appendTo("body");
equal(div.offsetParent()[0] , document.documentElement , "Absolutely positioned div returns html as offset parent, see #12139");
div.remove();SensorRnt(9894);});
test("fractions (see #7730 and #7885)" , function () {
SensorRnt(9895);expect(2);
jQuery("body").append("<div id='fractions'/>");
var result, expected = {
"top" : 1000,
"left" : 1000
}, div = jQuery("#fractions");
div.css({
"position" : "absolute",
"left" : "1000.7432222px",
"top" : "1000.532325px",
"width" : 100,
"height" : 100
});
div.offset(expected);
result = div.offset();
equal(result.top , expected.top , "Check top");
equal(result.left , expected.left , "Check left");
div.remove();SensorRnt(9896);});SensorRnt(9812);})();
