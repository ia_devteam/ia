function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open("GET", "http://localhost:8080/"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} SensorRnt(8815);module("deferred" , {
teardown : moduleTeardown
});
jQuery.each([ "", " - new operator"] , function ( _, withNew ) {
function createDeferred ( fn ) {
RNTRETRET = withNew?new jQuery.Deferred(fn):jQuery.Deferred(fn);
SensorRnt(8712);return RNTRETRET;}
SensorRnt(8710);test("jQuery.Deferred"+withNew , function () {
SensorRnt(8713);expect(23);
var defer = createDeferred();
strictEqual(defer.pipe , defer.then , "pipe is an alias of then");
createDeferred().resolve().done(function () {
ok(true , "Success on resolve");
strictEqual(this.state() , "resolved" , "Deferred is resolved (state)");}).fail(function () {
ok(false , "Error on resolve");}).always(function () {
SensorRnt(8715);ok(true , "Always callback on resolve");SensorRnt(8716);});
createDeferred().reject().done(function () {
ok(false , "Success on reject");}).fail(function () {
ok(true , "Error on reject");
strictEqual(this.state() , "rejected" , "Deferred is rejected (state)");}).always(function () {
SensorRnt(8717);ok(true , "Always callback on reject");SensorRnt(8718);});
createDeferred(function ( defer ) {
ok(this===defer , "Defer passed as this & first argument");
this.resolve("done");}).done(function ( value ) {
SensorRnt(8719);strictEqual(value , "done" , "Passed function executed");SensorRnt(8720);});
createDeferred(function ( defer ) {
SensorRnt(8721);var promise = defer.promise(), func = function () {SensorRnt(8723);}, funcPromise = defer.promise(func);
strictEqual(defer.promise() , promise , "promise is always the same");
strictEqual(funcPromise , func , "non objects get extended");
jQuery.each(promise , function ( key ) {
SensorRnt(8724);if (! jQuery.isFunction(promise[key]))
{ 
SensorRnt(8725);ok(false , key+" is a function ("+jQuery.type(promise[key])+")");
}
SensorRnt(8726);if (promise[key]!==func[key])
{ 
SensorRnt(8727);strictEqual(func[key] , promise[key] , key+" is the same");
};SensorRnt(8728);});SensorRnt(8722);});
jQuery.expandedEach = jQuery.each;
jQuery.expandedEach("resolve reject".split(" ") , function ( _, change ) {
SensorRnt(8729);createDeferred(function ( defer ) {
SensorRnt(8731);strictEqual(defer.state() , "pending" , "pending after creation");
var checked = 0;
defer.progress(function ( value ) {
SensorRnt(8736);strictEqual(value , checked , "Progress: right value ("+value+") received");SensorRnt(8737);});
SensorRnt(8732);for(checked = 0; checked < 3; checked++){ 
SensorRnt(8733);defer.notify(checked);
}
SensorRnt(8734);strictEqual(defer.state() , "pending" , "pending after notification");
defer[change]();
notStrictEqual(defer.state() , "pending" , "not pending after "+change);
defer.notify();SensorRnt(8735);});SensorRnt(8730);});SensorRnt(8714);});SensorRnt(8711);});
test("jQuery.Deferred - chainability" , function () {
SensorRnt(8738);var defer = jQuery.Deferred();
expect(10);
jQuery.expandedEach = jQuery.each;
jQuery.expandedEach("resolve reject notify resolveWith rejectWith notifyWith done fail progress always".split(" ") , function ( _, method ) {
var object = {
m : defer[method]
};
SensorRnt(8740);strictEqual(object.m() , object , method+" is chainable");SensorRnt(8741);});SensorRnt(8739);});
test("jQuery.Deferred.then - filtering (done)" , function () {
SensorRnt(8742);expect(4);
var value1, value2, value3, defer = jQuery.Deferred(), piped = defer.then(function ( a, b ) {
RNTRETRET = a* b;
SensorRnt(8744);return RNTRETRET;});
piped.done(function ( result ) {SensorRnt(8745);
value3 = result;});
defer.done(function ( a, b ) {SensorRnt(8746);
value1 = a;
value2 = b;});
defer.resolve(2 , 3);
strictEqual(value1 , 2 , "first resolve value ok");
strictEqual(value2 , 3 , "second resolve value ok");
strictEqual(value3 , 6 , "result of filter ok");
jQuery.Deferred().reject().then(function () {
SensorRnt(8747);ok(false , "then should not be called on reject");SensorRnt(8748);});
jQuery.Deferred().resolve().then(jQuery.noop).done(function ( value ) {
SensorRnt(8749);strictEqual(value , undefined , "then done callback can return undefined/null");SensorRnt(8750);});SensorRnt(8743);});
test("jQuery.Deferred.then - filtering (fail)" , function () {
SensorRnt(8751);expect(4);
var value1, value2, value3, defer = jQuery.Deferred(), piped = defer.then(null , function ( a, b ) {
RNTRETRET = a* b;
SensorRnt(8753);return RNTRETRET;});
piped.fail(function ( result ) {SensorRnt(8754);
value3 = result;});
defer.fail(function ( a, b ) {SensorRnt(8755);
value1 = a;
value2 = b;});
defer.reject(2 , 3);
strictEqual(value1 , 2 , "first reject value ok");
strictEqual(value2 , 3 , "second reject value ok");
strictEqual(value3 , 6 , "result of filter ok");
jQuery.Deferred().resolve().then(null , function () {
SensorRnt(8756);ok(false , "then should not be called on resolve");SensorRnt(8757);});
jQuery.Deferred().reject().then(null , jQuery.noop).fail(function ( value ) {
SensorRnt(8758);strictEqual(value , undefined , "then fail callback can return undefined/null");SensorRnt(8759);});SensorRnt(8752);});
test("jQuery.Deferred.then - filtering (progress)" , function () {
SensorRnt(8760);expect(3);
var value1, value2, value3, defer = jQuery.Deferred(), piped = defer.then(null , null , function ( a, b ) {
RNTRETRET = a* b;
SensorRnt(8762);return RNTRETRET;});
piped.progress(function ( result ) {SensorRnt(8763);
value3 = result;});
defer.progress(function ( a, b ) {SensorRnt(8764);
value1 = a;
value2 = b;});
defer.notify(2 , 3);
strictEqual(value1 , 2 , "first progress value ok");
strictEqual(value2 , 3 , "second progress value ok");
strictEqual(value3 , 6 , "result of filter ok");SensorRnt(8761);});
test("jQuery.Deferred.then - deferred (done)" , function () {
SensorRnt(8765);expect(3);
var value1, value2, value3, defer = jQuery.Deferred(), piped = defer.then(function ( a, b ) {
RNTRETRET = jQuery.Deferred(function ( defer ) {
SensorRnt(8768);defer.reject(a* b);SensorRnt(8769);});
SensorRnt(8767);return RNTRETRET;});
piped.fail(function ( result ) {SensorRnt(8770);
value3 = result;});
defer.done(function ( a, b ) {SensorRnt(8771);
value1 = a;
value2 = b;});
defer.resolve(2 , 3);
strictEqual(value1 , 2 , "first resolve value ok");
strictEqual(value2 , 3 , "second resolve value ok");
strictEqual(value3 , 6 , "result of filter ok");SensorRnt(8766);});
test("jQuery.Deferred.then - deferred (fail)" , function () {
SensorRnt(8772);expect(3);
var value1, value2, value3, defer = jQuery.Deferred(), piped = defer.then(null , function ( a, b ) {
RNTRETRET = jQuery.Deferred(function ( defer ) {
SensorRnt(8775);defer.resolve(a* b);SensorRnt(8776);});
SensorRnt(8774);return RNTRETRET;});
piped.done(function ( result ) {SensorRnt(8777);
value3 = result;});
defer.fail(function ( a, b ) {SensorRnt(8778);
value1 = a;
value2 = b;});
defer.reject(2 , 3);
strictEqual(value1 , 2 , "first reject value ok");
strictEqual(value2 , 3 , "second reject value ok");
strictEqual(value3 , 6 , "result of filter ok");SensorRnt(8773);});
test("jQuery.Deferred.then - deferred (progress)" , function () {
SensorRnt(8779);expect(3);
var value1, value2, value3, defer = jQuery.Deferred(), piped = defer.then(null , null , function ( a, b ) {
RNTRETRET = jQuery.Deferred(function ( defer ) {
SensorRnt(8782);defer.resolve(a* b);SensorRnt(8783);});
SensorRnt(8781);return RNTRETRET;});
piped.done(function ( result ) {SensorRnt(8784);
value3 = result;});
defer.progress(function ( a, b ) {SensorRnt(8785);
value1 = a;
value2 = b;});
defer.notify(2 , 3);
strictEqual(value1 , 2 , "first progress value ok");
strictEqual(value2 , 3 , "second progress value ok");
strictEqual(value3 , 6 , "result of filter ok");SensorRnt(8780);});
test("jQuery.Deferred.then - context" , function () {
SensorRnt(8786);expect(7);
var defer, piped, defer2, piped2, context = {

};
jQuery.Deferred().resolveWith(context , [ 2]).then(function ( value ) {
RNTRETRET = value* 3;
return RNTRETRET;}).done(function ( value ) {
SensorRnt(8788);strictEqual(this , context , "custom context correctly propagated");
strictEqual(value , 6 , "proper value received");SensorRnt(8789);});
jQuery.Deferred().resolve().then(function () {
RNTRETRET = jQuery.Deferred().resolveWith(context);
return RNTRETRET;}).done(function () {
SensorRnt(8790);strictEqual(this , context , "custom context of returned deferred correctly propagated");SensorRnt(8791);});
defer = jQuery.Deferred();
piped = defer.then(function ( value ) {
RNTRETRET = value* 3;
SensorRnt(8792);return RNTRETRET;});
defer.resolve(2);
piped.done(function ( value ) {
SensorRnt(8793);strictEqual(this , piped , "default context gets updated to latest promise in the chain");
strictEqual(value , 6 , "proper value received");SensorRnt(8794);});
defer2 = jQuery.Deferred();
piped2 = defer2.then();
defer2.resolve(2);
piped2.done(function ( value ) {
SensorRnt(8795);strictEqual(this , piped2 , "default context gets updated to latest promise in the chain (without passing function)");
strictEqual(value , 2 , "proper value received (without passing function)");SensorRnt(8796);});SensorRnt(8787);});
test("jQuery.when" , function () {
SensorRnt(8797);expect(37);
jQuery.each({
"an empty string" : "",
"a non-empty string" : "some string",
"zero" : 0,
"a number other than zero" : 1,
"true" : true,
"false" : false,
"null" : null,
"undefined" : undefined,
"a plain object" : {

},
"an array" : [ 1, 2, 3]
} , function ( message, value ) {
SensorRnt(8799);ok(jQuery.isFunction(jQuery.when(value).done(function ( resolveValue ) {
strictEqual(this , window , "Context is the global object with "+message);
strictEqual(resolveValue , value , "Test the promise was resolved with "+message);}).promise) , "Test "+message+" triggers the creation of a new Promise");SensorRnt(8800);});
ok(jQuery.isFunction(jQuery.when().done(function ( resolveValue ) {
strictEqual(this , window , "Test the promise was resolved with window as its context");
strictEqual(resolveValue , undefined , "Test the promise was resolved with no parameter");}).promise) , "Test calling when with no parameter triggers the creation of a new Promise");
var cache, context = {

};
jQuery.when(jQuery.Deferred().resolveWith(context)).done(function () {
SensorRnt(8801);strictEqual(this , context , "when( promise ) propagates context");SensorRnt(8802);});
jQuery.each([ 1, 2, 3] , function ( k, i ) {
jQuery.when(cache||jQuery.Deferred(function () {
this.resolve(i);})).done(function ( value ) {SensorRnt(8803);
SensorRnt(8805);strictEqual(value , 1 , "Function executed"+(i > 1?" only once":""));
cache = value;SensorRnt(8806);});SensorRnt(8804);});SensorRnt(8798);});
test("jQuery.when - joined" , function () {
SensorRnt(8807);expect(119);
var deferreds = {
value : 1,
success : jQuery.Deferred().resolve(1),
error : jQuery.Deferred().reject(0),
futureSuccess : jQuery.Deferred().notify(true),
futureError : jQuery.Deferred().notify(true),
notify : jQuery.Deferred().notify(true)
}, willSucceed = {
value : true,
success : true,
futureSuccess : true
}, willError = {
error : true,
futureError : true
}, willNotify = {
futureSuccess : true,
futureError : true,
notify : true
};
jQuery.each(deferreds , function ( id1, defer1 ) {
SensorRnt(8809);jQuery.each(deferreds , function ( id2, defer2 ) {
SensorRnt(8811);var shouldResolve = willSucceed[id1]&&willSucceed[id2], shouldError = willError[id1]||willError[id2], shouldNotify = willNotify[id1]||willNotify[id2], expected = shouldResolve?[ 1, 1]:[ 0, undefined], expectedNotify = shouldNotify&&[ willNotify[id1], willNotify[id2]], code = id1+"/"+id2, context1 = defer1&&jQuery.isFunction(defer1.promise)?defer1.promise():undefined, context2 = defer2&&jQuery.isFunction(defer2.promise)?defer2.promise():undefined;
jQuery.when(defer1 , defer2).done(function ( a, b ) {
if (shouldResolve)
{ 
deepEqual([ a, b] , expected , code+" => resolve");
strictEqual(this[0] , context1 , code+" => first context OK");
strictEqual(this[1] , context2 , code+" => second context OK");
}
else
{ 
ok(false , code+" => resolve");
}}).fail(function ( a, b ) {
if (shouldError)
{ 
deepEqual([ a, b] , expected , code+" => reject");
}
else
{ 
ok(false , code+" => reject");
}}).progress(function ( a, b ) {
SensorRnt(8813);deepEqual([ a, b] , expectedNotify , code+" => progress");
strictEqual(this[0] , expectedNotify[0]?context1:undefined , code+" => first context OK");
strictEqual(this[1] , expectedNotify[1]?context2:undefined , code+" => second context OK");SensorRnt(8814);});SensorRnt(8812);});SensorRnt(8810);});
deferreds.futureSuccess.resolve(1);
deferreds.futureError.reject(0);});
SensorRnt(8808);