function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open("GET", "http://localhost:8080/"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} SensorRnt(9805);module("manipulation" , {
teardown : moduleTeardown
});
Array.prototype.arrayProtoFn = function () {
SensorRnt(9383);throw("arrayProtoFn should not be called");};
function manipulationBareObj ( value ) {
RNTRETRET = value;
SensorRnt(9384);return RNTRETRET;}
function manipulationFunctionReturningObj ( value ) {
RNTRETRET = function () {
RNTRETRET = value;
SensorRnt(9386);return RNTRETRET;};
SensorRnt(9385);return RNTRETRET;}
test("text()" , function () {
SensorRnt(9387);expect(5);
var expected, frag, $newLineTest;
expected = "This link has class=\"blog\": Simon Willison's Weblog";
equal(jQuery("#sap").text() , expected , "Check for merged text of more then one element.");
equal(jQuery(document.createTextNode("foo")).text() , "foo" , "Text node was retrieved from .text().");
notEqual(jQuery(document).text() , "" , "Retrieving text for the document retrieves all text (#10724).");
frag = document.createDocumentFragment();
frag.appendChild(document.createTextNode("foo"));
equal(jQuery(frag).text() , "foo" , "Document Fragment Text node was retrieved from .text().");
$newLineTest = jQuery("<div>test<br/>testy</div>").appendTo("#moretests");
$newLineTest.find("br").replaceWith("\n");
equal($newLineTest.text() , "test\ntesty" , "text() does not remove new lines (#11153)");
$newLineTest.remove();SensorRnt(9388);});
test("text(undefined)" , function () {
SensorRnt(9389);expect(1);
equal(jQuery("#foo").text("<div").text(undefined)[0].innerHTML , "&lt;div" , ".text(undefined) is chainable (#5571)");SensorRnt(9390);});
function testText ( valueObj ) {
SensorRnt(9391);expect(7);
var val, j, expected, $multipleElements, $parentDiv, $childDiv;
val = valueObj("<div><b>Hello</b> cruel world!</div>");
equal(jQuery("#foo").text(val)[0].innerHTML.replace(/>/g , "&gt;") , "&lt;div&gt;&lt;b&gt;Hello&lt;/b&gt; cruel world!&lt;/div&gt;" , "Check escaped text");
j = jQuery("#nonnodes").contents();
j.text(valueObj("hi!"));
equal(jQuery(j[0]).text() , "hi!" , "Check node,textnode,comment with text()");
equal(j[1].nodeValue , " there " , "Check node,textnode,comment with text()");
equal(j[2].nodeType , 8 , "Check node,textnode,comment with text()");
expected = "New";
$multipleElements = jQuery("<div>Hello</div>").add("<div>World</div>");
$multipleElements.text(expected);
equal($multipleElements.eq(0).text() , expected , "text() updates multiple elements (#11809)");
equal($multipleElements.eq(1).text() , expected , "text() updates multiple elements (#11809)");
$childDiv = jQuery("<div/>");
$childDiv.data("leak" , true);
$parentDiv = jQuery("<div/>");
$parentDiv.append($childDiv);
$parentDiv.text("Dry off");
equal($childDiv.data("leak") , undefined , "Check for leaks (#11809)");SensorRnt(9392);}
test("text(String)" , function () {
SensorRnt(9393);testText(manipulationBareObj);SensorRnt(9394);});
test("text(Function)" , function () {
SensorRnt(9395);testText(manipulationFunctionReturningObj);SensorRnt(9396);});
test("text(Function) with incoming value" , function () {
SensorRnt(9397);expect(2);
var old = "This link has class=\"blog\": Simon Willison's Weblog";
jQuery("#sap").text(function ( i, val ) {
SensorRnt(9399);equal(val , old , "Make sure the incoming value is correct.");
RNTRETRET = "foobar";
SensorRnt(9400);return RNTRETRET;});
equal(jQuery("#sap").text() , "foobar" , "Check for merged text of more then one element.");SensorRnt(9398);});
function testAppendForObject ( valueObj, isFragment ) {
SensorRnt(9401);var $base, type = isFragment?" (DocumentFragment)":" (Element)", text = "This link has class=\"blog\": Simon Willison's Weblog", el = document.getElementById("sap").cloneNode(true), first = document.getElementById("first"), yahoo = document.getElementById("yahoo");
SensorRnt(9402);if (isFragment)
{ 
SensorRnt(9403);$base = document.createDocumentFragment();
jQuery(el).contents().each(function () {
SensorRnt(9407);$base.appendChild(this);SensorRnt(9408);});
$base = jQuery($base);
}
else
{ 
SensorRnt(9404);$base = jQuery(el);
}
SensorRnt(9405);equal($base.clone().append(valueObj(first.cloneNode(true))).text() , text+"Try them out:" , "Check for appending of element"+type);
equal($base.clone().append(valueObj([ first.cloneNode(true), yahoo.cloneNode(true)])).text() , text+"Try them out:Yahoo" , "Check for appending of array of elements"+type);
equal($base.clone().append(valueObj(jQuery("#yahoo, #first").clone())).text() , text+"YahooTry them out:" , "Check for appending of jQuery object"+type);
equal($base.clone().append(valueObj(5)).text() , text+"5" , "Check for appending a number"+type);
equal($base.clone().append(valueObj([ jQuery("#first").clone(), jQuery("#yahoo, #google").clone()])).text() , text+"Try them out:GoogleYahoo" , "Check for appending of array of jQuery objects");
equal($base.clone().append(valueObj(" text with spaces ")).text() , text+" text with spaces " , "Check for appending text with spaces"+type);
equal($base.clone().append(valueObj([ ])).text() , text , "Check for appending an empty array"+type);
equal($base.clone().append(valueObj("")).text() , text , "Check for appending an empty string"+type);
equal($base.clone().append(valueObj(document.getElementsByTagName("foo"))).text() , text , "Check for appending an empty nodelist"+type);
equal($base.clone().append("<span></span>" , "<span></span>" , "<span></span>").children().length , $base.children().length+3 , "Make sure that multiple arguments works."+type);
equal($base.clone().append(valueObj(document.getElementById("form").cloneNode(true))).children("form").length , 1 , "Check for appending a form (#910)"+type);SensorRnt(9406);}
function testAppend ( valueObj ) {
SensorRnt(9409);expect(78);
testAppendForObject(valueObj , false);
testAppendForObject(valueObj , true);
var defaultText, result, message, iframe, iframeDoc, j, d, $input, $radioChecked, $radioUnchecked, $radioParent, $map, $table;
defaultText = "Try them out:";
result = jQuery("#first").append(valueObj("<b>buga</b>"));
equal(result.text() , defaultText+"buga" , "Check if text appending works");
equal(jQuery("#select3").append(valueObj("<option value='appendTest'>Append Test</option>")).find("option:last-child").attr("value") , "appendTest" , "Appending html options to select element");
jQuery("form").append(valueObj("<input name='radiotest' type='radio' checked='checked' />"));
jQuery("form input[name=radiotest]").each(function () {
ok(jQuery(this).is(":checked") , "Append checked radio");}).remove();
jQuery("form").append(valueObj("<input name='radiotest2' type='radio' checked    =   'checked' />"));
jQuery("form input[name=radiotest2]").each(function () {
ok(jQuery(this).is(":checked") , "Append alternately formated checked radio");}).remove();
jQuery("form").append(valueObj("<input name='radiotest3' type='radio' checked />"));
jQuery("form input[name=radiotest3]").each(function () {
ok(jQuery(this).is(":checked") , "Append HTML5-formated checked radio");}).remove();
jQuery("form").append(valueObj("<input type='radio' checked='checked' name='radiotest4' />"));
jQuery("form input[name=radiotest4]").each(function () {
ok(jQuery(this).is(":checked") , "Append with name attribute after checked attribute");}).remove();
message = "Test for appending a DOM node to the contents of an iframe";
iframe = jQuery("#iframe")[0];
iframeDoc = iframe.contentDocument||iframe.contentWindow&&iframe.contentWindow.document;
SensorRnt(9410);try{ 
SensorRnt(9411);if (iframeDoc&&iframeDoc.body)
{ 
SensorRnt(9412);equal(jQuery(iframeDoc.body).append(valueObj("<div id='success'>test</div>"))[0].lastChild.id , "success" , message);
}
else
{ 
SensorRnt(9413);ok(true , message+" - can't test");
};SensorRnt(9414);
}
catch(e){ 
SensorRnt(9415);strictEqual(e.message||e , undefined , message);
}
SensorRnt(9416);jQuery("<fieldset/>").appendTo("#form").append(valueObj("<legend id='legend'>test</legend>"));
t("Append legend" , "#legend" , [ "legend"]);
$map = jQuery("<map/>").append(valueObj("<area id='map01' shape='rect' coords='50,50,150,150' href='http://www.jquery.com/' alt='jQuery'>"));
equal($map[0].childNodes.length , 1 , "The area was inserted.");
equal($map[0].firstChild.nodeName.toLowerCase() , "area" , "The area was inserted.");
jQuery("#select1").append(valueObj("<OPTION>Test</OPTION>"));
equal(jQuery("#select1 option:last-child").text() , "Test" , "Appending OPTION (all caps)");
jQuery("#select1").append(valueObj("<optgroup label='optgroup'><option>optgroup</option></optgroup>"));
equal(jQuery("#select1 optgroup").attr("label") , "optgroup" , "Label attribute in newly inserted optgroup is correct");
equal(jQuery("#select1 option").last().text() , "optgroup" , "Appending optgroup");
$table = jQuery("#table");
jQuery.each("thead tbody tfoot colgroup caption tr th td".split(" ") , function ( i, name ) {
SensorRnt(9418);$table.append(valueObj("<"+name+"/>"));
equal($table.find(name).length , 1 , "Append "+name);
ok(jQuery.parseHTML("<"+name+"/>").length , name+" wrapped correctly");SensorRnt(9419);});
jQuery("#table colgroup").append(valueObj("<col/>"));
equal(jQuery("#table colgroup col").length , 1 , "Append col");
jQuery("#form").append(valueObj("<select id='appendSelect1'></select>")).append(valueObj("<select id='appendSelect2'><option>Test</option></select>"));
t("Append Select" , "#appendSelect1, #appendSelect2" , [ "appendSelect1", "appendSelect2"]);
equal("Two nodes" , jQuery("<div />").append("Two" , " nodes").text() , "Appending two text nodes (#4011)");
equal(jQuery("<div />").append("1" , "" , 3).text() , "13" , "If median is false-like value, subsequent arguments should not be ignored");
j = jQuery("#nonnodes").contents();
d = jQuery("<div/>").appendTo("#nonnodes").append(j);
equal(jQuery("#nonnodes").length , 1 , "Check node,textnode,comment append moved leaving just the div");
equal(d.contents().length , 3 , "Check node,textnode,comment append works");
d.contents().appendTo("#nonnodes");
d.remove();
equal(jQuery("#nonnodes").contents().length , 3 , "Check node,textnode,comment append cleanup worked");
$input = jQuery("<input type='checkbox'/>").prop("checked" , true).appendTo("#testForm");
equal($input[0].checked , true , "A checked checkbox that is appended stays checked");
$radioChecked = jQuery("input[type='radio'][name='R1']").eq(1);
$radioParent = $radioChecked.parent();
$radioUnchecked = jQuery("<input type='radio' name='R1' checked='checked'/>").appendTo($radioParent);
$radioChecked.trigger("click");
$radioUnchecked[0].checked = false;
jQuery("<div/>").insertBefore($radioParent).append($radioParent);
equal($radioChecked[0].checked , true , "Reappending radios uphold which radio is checked");
equal($radioUnchecked[0].checked , false , "Reappending radios uphold not being checked");
equal(jQuery("<div/>").append(valueObj("option<area/>"))[0].childNodes.length , 2 , "HTML-string with leading text should be processed correctly");SensorRnt(9417);}
test("append(String|Element|Array<Element>|jQuery)" , function () {
SensorRnt(9420);testAppend(manipulationBareObj);SensorRnt(9421);});
test("append(Function)" , function () {
SensorRnt(9422);testAppend(manipulationFunctionReturningObj);SensorRnt(9423);});
test("append(param) to object, see #11280" , function () {
SensorRnt(9424);expect(5);
var object = jQuery(document.createElement("object")).appendTo(document.body);
equal(object.children().length , 0 , "object does not start with children");
object.append(jQuery("<param type='wmode' name='foo'>"));
equal(object.children().length , 1 , "appended param");
equal(object.children().eq(0).attr("name") , "foo" , "param has name=foo");
object = jQuery("<object><param type='baz' name='bar'></object>");
equal(object.children().length , 1 , "object created with child param");
equal(object.children().eq(0).attr("name") , "bar" , "param has name=bar");SensorRnt(9425);});
test("append(Function) returns String" , function () {
SensorRnt(9426);expect(4);
var defaultText, result, select, old;
defaultText = "Try them out:";
old = jQuery("#first").html();
result = jQuery("#first").append(function ( i, val ) {
SensorRnt(9428);equal(val , old , "Make sure the incoming value is correct.");
RNTRETRET = "<b>buga</b>";
SensorRnt(9429);return RNTRETRET;});
equal(result.text() , defaultText+"buga" , "Check if text appending works");
select = jQuery("#select3");
old = select.html();
equal(select.append(function ( i, val ) {
equal(val , old , "Make sure the incoming value is correct.");
RNTRETRET = "<option value='appendTest'>Append Test</option>";
return RNTRETRET;}).find("option:last-child").attr("value") , "appendTest" , "Appending html options to select element");SensorRnt(9427);});
test("append(Function) returns Element" , function () {
SensorRnt(9430);expect(2);
var expected = "This link has class=\"blog\": Simon Willison's WeblogTry them out:", old = jQuery("#sap").html();
jQuery("#sap").append(function ( i, val ) {
SensorRnt(9432);equal(val , old , "Make sure the incoming value is correct.");
RNTRETRET = document.getElementById("first");
SensorRnt(9433);return RNTRETRET;});
equal(jQuery("#sap").text() , expected , "Check for appending of element");SensorRnt(9431);});
test("append(Function) returns Array<Element>" , function () {
SensorRnt(9434);expect(2);
var expected = "This link has class=\"blog\": Simon Willison's WeblogTry them out:Yahoo", old = jQuery("#sap").html();
jQuery("#sap").append(function ( i, val ) {
SensorRnt(9436);equal(val , old , "Make sure the incoming value is correct.");
RNTRETRET = [ document.getElementById("first"), document.getElementById("yahoo")];
SensorRnt(9437);return RNTRETRET;});
equal(jQuery("#sap").text() , expected , "Check for appending of array of elements");SensorRnt(9435);});
test("append(Function) returns jQuery" , function () {
SensorRnt(9438);expect(2);
var expected = "This link has class=\"blog\": Simon Willison's WeblogYahooTry them out:", old = jQuery("#sap").html();
jQuery("#sap").append(function ( i, val ) {
SensorRnt(9440);equal(val , old , "Make sure the incoming value is correct.");
RNTRETRET = jQuery("#yahoo, #first");
SensorRnt(9441);return RNTRETRET;});
equal(jQuery("#sap").text() , expected , "Check for appending of jQuery object");SensorRnt(9439);});
test("append(Function) returns Number" , function () {
SensorRnt(9442);expect(2);
var old = jQuery("#sap").html();
jQuery("#sap").append(function ( i, val ) {
SensorRnt(9444);equal(val , old , "Make sure the incoming value is correct.");
RNTRETRET = 5;
SensorRnt(9445);return RNTRETRET;});
ok(jQuery("#sap")[0].innerHTML.match(/5$/) , "Check for appending a number");SensorRnt(9443);});
test("XML DOM manipulation (#9960)" , function () {
SensorRnt(9446);expect(5);
var scxml1Adopted, xmlDoc1 = jQuery.parseXML("<scxml xmlns='http://www.w3.org/2005/07/scxml' version='1.0'><state x='100' y='100' initial='actions' id='provisioning'></state><state x='100' y='100' id='error'></state><state x='100' y='100' id='finished' final='true'></state></scxml>"), xmlDoc2 = jQuery.parseXML("<scxml xmlns='http://www.w3.org/2005/07/scxml' version='1.0'><state id='provisioning3'></state></scxml>"), xml1 = jQuery(xmlDoc1), xml2 = jQuery(xmlDoc2), scxml1 = jQuery("scxml" , xml1), scxml2 = jQuery("scxml" , xml2), state = scxml2.find("state");
SensorRnt(9447);if (/android 2\.3/i.test(navigator.userAgent))
{ 
SensorRnt(9448);state = jQuery(xmlDoc1.adoptNode(state[0]));
}
SensorRnt(9449);scxml1.append(state);
strictEqual(scxml1[0].lastChild , state[0] , "append");
scxml1.prepend(state);
strictEqual(scxml1[0].firstChild , state[0] , "prepend");
scxml1.find("#finished").after(state);
strictEqual(scxml1[0].lastChild , state[0] , "after");
scxml1.find("#provisioning").before(state);
strictEqual(scxml1[0].firstChild , state[0] , "before");
SensorRnt(9450);if (/android 2\.3/i.test(navigator.userAgent))
{ 
SensorRnt(9451);scxml1Adopted = jQuery(xmlDoc2.adoptNode(scxml1[0]));
scxml2.replaceWith(scxml1Adopted);
}
else
{ 
SensorRnt(9452);scxml2.replaceWith(scxml1);
}
SensorRnt(9453);deepEqual(jQuery("state" , xml2).get() , scxml1.find("state").get() , "replaceWith");SensorRnt(9454);});
test("append HTML5 sectioning elements (Bug #6485)" , function () {
SensorRnt(9455);expect(2);
var article, aside;
jQuery("#qunit-fixture").append("<article style='font-size:10px'><section><aside>HTML5 elements</aside></section></article>");
article = jQuery("article");
aside = jQuery("aside");
equal(article.get(0).style.fontSize , "10px" , "HTML5 elements are styleable");
equal(aside.length , 1 , "HTML5 elements do not collapse their children");SensorRnt(9456);});
SensorRnt(9806);if (jQuery.css)
{ 
SensorRnt(9807);test("HTML5 Elements inherit styles from style rules (Bug #10501)" , function () {
SensorRnt(9457);expect(1);
jQuery("#qunit-fixture").append("<article id='article'></article>");
jQuery("#article").append("<section>This section should have a pink background.</section>");
notEqual(jQuery("section").css("background-color") , "transparent" , "HTML5 elements inherit styles");});SensorRnt(9458);
}
SensorRnt(9808);test("html(String) with HTML5 (Bug #6485)" , function () {
SensorRnt(9459);expect(2);
jQuery("#qunit-fixture").html("<article><section><aside>HTML5 elements</aside></section></article>");
equal(jQuery("#qunit-fixture").children().children().length , 1 , "Make sure HTML5 article elements can hold children. innerHTML shortcut path");
equal(jQuery("#qunit-fixture").children().children().children().length , 1 , "Make sure nested HTML5 elements can hold children.");SensorRnt(9460);});
test("IE8 serialization bug" , function () {
SensorRnt(9461);expect(2);
var wrapper = jQuery("<div></div>");
wrapper.html("<div></div><article></article>");
equal(wrapper.children("article").length , 1 , "HTML5 elements are insertable with .html()");
wrapper.html("<div></div><link></link>");
equal(wrapper.children("link").length , 1 , "Link elements are insertable with .html()");SensorRnt(9462);});
test("html() object element #10324" , function () {
SensorRnt(9463);expect(1);
var object = jQuery("<object id='object2'><param name='object2test' value='test'></param></object>?").appendTo("#qunit-fixture"), clone = object.clone();
equal(clone.html() , object.html() , "html() returns correct innerhtml of cloned object elements");SensorRnt(9464);});
test("append(xml)" , function () {
SensorRnt(9465);expect(1);
var xmlDoc, xml1, xml2;
function createXMLDoc () {
var elem, n, len, aActiveX = [ "MSXML6.DomDocument", "MSXML3.DomDocument", "MSXML2.DomDocument", "MSXML.DomDocument", "Microsoft.XmlDom"];
SensorRnt(9467);if (document.implementation&&"createDocument" in document.implementation)
{ 
RNTRETRET = document.implementation.createDocument("" , "" , null);
SensorRnt(9468);return RNTRETRET;
}
else
{ 
SensorRnt(9469);for(n = 0, len = aActiveX.length; n < len; n++){ 
SensorRnt(9470);try{ 
SensorRnt(9471);elem = new ActiveXObject(aActiveX[n]);
RNTRETRET = elem;
SensorRnt(9472);return RNTRETRET;
}
catch(_){ 
};SensorRnt(9473);
};SensorRnt(9474);
};SensorRnt(9475);}
xmlDoc = createXMLDoc();
xml1 = xmlDoc.createElement("head");
xml2 = xmlDoc.createElement("test");
ok(jQuery(xml1).append(xml2) , "Append an xml element to another without raising an exception.");SensorRnt(9466);});
test("appendTo(String)" , function () {
SensorRnt(9476);expect(4);
var l, defaultText;
defaultText = "Try them out:";
jQuery("<b>buga</b>").appendTo("#first");
equal(jQuery("#first").text() , defaultText+"buga" , "Check if text appending works");
equal(jQuery("<option value='appendTest'>Append Test</option>").appendTo("#select3").parent().find("option:last-child").attr("value") , "appendTest" , "Appending html options to select element");
l = jQuery("#first").children().length+2;
jQuery("<strong>test</strong>");
jQuery("<strong>test</strong>");
jQuery([ jQuery("<strong>test</strong>")[0], jQuery("<strong>test</strong>")[0]]).appendTo("#first");
equal(jQuery("#first").children().length , l , "Make sure the elements were inserted.");
equal(jQuery("#first").children().last()[0].nodeName.toLowerCase() , "strong" , "Verify the last element.");SensorRnt(9477);});
test("appendTo(Element|Array<Element>)" , function () {
SensorRnt(9478);expect(2);
var expected = "This link has class=\"blog\": Simon Willison's WeblogTry them out:";
jQuery(document.getElementById("first")).appendTo("#sap");
equal(jQuery("#sap").text() , expected , "Check for appending of element");
expected = "This link has class=\"blog\": Simon Willison's WeblogTry them out:Yahoo";
jQuery([ document.getElementById("first"), document.getElementById("yahoo")]).appendTo("#sap");
equal(jQuery("#sap").text() , expected , "Check for appending of array of elements");SensorRnt(9479);});
test("appendTo(jQuery)" , function () {
SensorRnt(9480);expect(10);
var expected, num, div;
ok(jQuery(document.createElement("script")).appendTo("body").length , "Make sure a disconnected script can be appended.");
expected = "This link has class=\"blog\": Simon Willison's WeblogYahooTry them out:";
jQuery("#yahoo, #first").appendTo("#sap");
equal(jQuery("#sap").text() , expected , "Check for appending of jQuery object");
jQuery("#select1").appendTo("#foo");
t("Append select" , "#foo select" , [ "select1"]);
div = jQuery("<div/>").on("click" , function () {
SensorRnt(9482);ok(true , "Running a cloned click.");SensorRnt(9483);});
div.appendTo("#qunit-fixture, #moretests");
jQuery("#qunit-fixture div").last().trigger("click");
jQuery("#moretests div").last().trigger("click");
div = jQuery("<div/>").appendTo("#qunit-fixture, #moretests");
equal(div.length , 2 , "appendTo returns the inserted elements");
div.addClass("test");
ok(jQuery("#qunit-fixture div").last().hasClass("test") , "appendTo element was modified after the insertion");
ok(jQuery("#moretests div").last().hasClass("test") , "appendTo element was modified after the insertion");
div = jQuery("<div/>");
jQuery("<span>a</span><b>b</b>").filter("span").appendTo(div);
equal(div.children().length , 1 , "Make sure the right number of children were inserted.");
div = jQuery("#moretests div");
num = jQuery("#qunit-fixture div").length;
div.remove().appendTo("#qunit-fixture");
equal(jQuery("#qunit-fixture div").length , num , "Make sure all the removed divs were inserted.");SensorRnt(9481);});
test("prepend(String)" , function () {
SensorRnt(9484);expect(2);
var result, expected;
expected = "Try them out:";
result = jQuery("#first").prepend("<b>buga</b>");
equal(result.text() , "buga"+expected , "Check if text prepending works");
equal(jQuery("#select3").prepend("<option value='prependTest'>Prepend Test</option>").find("option:first-child").attr("value") , "prependTest" , "Prepending html options to select element");SensorRnt(9485);});
test("prepend(Element)" , function () {
SensorRnt(9486);expect(1);
var expected;
expected = "Try them out:This link has class=\"blog\": Simon Willison's Weblog";
jQuery("#sap").prepend(document.getElementById("first"));
equal(jQuery("#sap").text() , expected , "Check for prepending of element");SensorRnt(9487);});
test("prepend(Array<Element>)" , function () {
SensorRnt(9488);expect(1);
var expected;
expected = "Try them out:YahooThis link has class=\"blog\": Simon Willison's Weblog";
jQuery("#sap").prepend([ document.getElementById("first"), document.getElementById("yahoo")]);
equal(jQuery("#sap").text() , expected , "Check for prepending of array of elements");SensorRnt(9489);});
test("prepend(jQuery)" , function () {
SensorRnt(9490);expect(1);
var expected;
expected = "YahooTry them out:This link has class=\"blog\": Simon Willison's Weblog";
jQuery("#sap").prepend(jQuery("#yahoo, #first"));
equal(jQuery("#sap").text() , expected , "Check for prepending of jQuery object");SensorRnt(9491);});
test("prepend(Array<jQuery>)" , function () {
SensorRnt(9492);expect(1);
var expected;
expected = "Try them out:GoogleYahooThis link has class=\"blog\": Simon Willison's Weblog";
jQuery("#sap").prepend([ jQuery("#first"), jQuery("#yahoo, #google")]);
equal(jQuery("#sap").text() , expected , "Check for prepending of array of jQuery objects");SensorRnt(9493);});
test("prepend(Function) with incoming value -- String" , function () {
SensorRnt(9494);expect(4);
var defaultText, old, result;
defaultText = "Try them out:";
old = jQuery("#first").html();
result = jQuery("#first").prepend(function ( i, val ) {
SensorRnt(9496);equal(val , old , "Make sure the incoming value is correct.");
RNTRETRET = "<b>buga</b>";
SensorRnt(9497);return RNTRETRET;});
equal(result.text() , "buga"+defaultText , "Check if text prepending works");
old = jQuery("#select3").html();
equal(jQuery("#select3").prepend(function ( i, val ) {
equal(val , old , "Make sure the incoming value is correct.");
RNTRETRET = "<option value='prependTest'>Prepend Test</option>";
return RNTRETRET;}).find("option:first-child").attr("value") , "prependTest" , "Prepending html options to select element");SensorRnt(9495);});
test("prepend(Function) with incoming value -- Element" , function () {
SensorRnt(9498);expect(2);
var old, expected;
expected = "Try them out:This link has class=\"blog\": Simon Willison's Weblog";
old = jQuery("#sap").html();
jQuery("#sap").prepend(function ( i, val ) {
SensorRnt(9500);equal(val , old , "Make sure the incoming value is correct.");
RNTRETRET = document.getElementById("first");
SensorRnt(9501);return RNTRETRET;});
equal(jQuery("#sap").text() , expected , "Check for prepending of element");SensorRnt(9499);});
test("prepend(Function) with incoming value -- Array<Element>" , function () {
SensorRnt(9502);expect(2);
var old, expected;
expected = "Try them out:YahooThis link has class=\"blog\": Simon Willison's Weblog";
old = jQuery("#sap").html();
jQuery("#sap").prepend(function ( i, val ) {
SensorRnt(9504);equal(val , old , "Make sure the incoming value is correct.");
RNTRETRET = [ document.getElementById("first"), document.getElementById("yahoo")];
SensorRnt(9505);return RNTRETRET;});
equal(jQuery("#sap").text() , expected , "Check for prepending of array of elements");SensorRnt(9503);});
test("prepend(Function) with incoming value -- jQuery" , function () {
SensorRnt(9506);expect(2);
var old, expected;
expected = "YahooTry them out:This link has class=\"blog\": Simon Willison's Weblog";
old = jQuery("#sap").html();
jQuery("#sap").prepend(function ( i, val ) {
SensorRnt(9508);equal(val , old , "Make sure the incoming value is correct.");
RNTRETRET = jQuery("#yahoo, #first");
SensorRnt(9509);return RNTRETRET;});
equal(jQuery("#sap").text() , expected , "Check for prepending of jQuery object");SensorRnt(9507);});
test("prependTo(String)" , function () {
SensorRnt(9510);expect(2);
var defaultText;
defaultText = "Try them out:";
jQuery("<b>buga</b>").prependTo("#first");
equal(jQuery("#first").text() , "buga"+defaultText , "Check if text prepending works");
equal(jQuery("<option value='prependTest'>Prepend Test</option>").prependTo("#select3").parent().find("option:first-child").attr("value") , "prependTest" , "Prepending html options to select element");SensorRnt(9511);});
test("prependTo(Element)" , function () {
SensorRnt(9512);expect(1);
var expected;
expected = "Try them out:This link has class=\"blog\": Simon Willison's Weblog";
jQuery(document.getElementById("first")).prependTo("#sap");
equal(jQuery("#sap").text() , expected , "Check for prepending of element");SensorRnt(9513);});
test("prependTo(Array<Element>)" , function () {
SensorRnt(9514);expect(1);
var expected;
expected = "Try them out:YahooThis link has class=\"blog\": Simon Willison's Weblog";
jQuery([ document.getElementById("first"), document.getElementById("yahoo")]).prependTo("#sap");
equal(jQuery("#sap").text() , expected , "Check for prepending of array of elements");SensorRnt(9515);});
test("prependTo(jQuery)" , function () {
SensorRnt(9516);expect(1);
var expected;
expected = "YahooTry them out:This link has class=\"blog\": Simon Willison's Weblog";
jQuery("#yahoo, #first").prependTo("#sap");
equal(jQuery("#sap").text() , expected , "Check for prepending of jQuery object");SensorRnt(9517);});
test("prependTo(Array<jQuery>)" , function () {
SensorRnt(9518);expect(1);
jQuery("<select id='prependSelect1'></select>").prependTo("#form");
jQuery("<select id='prependSelect2'><option>Test</option></select>").prependTo("#form");
t("Prepend Select" , "#prependSelect2, #prependSelect1" , [ "prependSelect2", "prependSelect1"]);SensorRnt(9519);});
test("before(String)" , function () {
SensorRnt(9520);expect(1);
var expected;
expected = "This is a normal link: bugaYahoo";
jQuery("#yahoo").before(manipulationBareObj("<b>buga</b>"));
equal(jQuery("#en").text() , expected , "Insert String before");SensorRnt(9521);});
test("before(Element)" , function () {
SensorRnt(9522);expect(1);
var expected;
expected = "This is a normal link: Try them out:Yahoo";
jQuery("#yahoo").before(manipulationBareObj(document.getElementById("first")));
equal(jQuery("#en").text() , expected , "Insert element before");SensorRnt(9523);});
test("before(Array<Element>)" , function () {
SensorRnt(9524);expect(1);
var expected;
expected = "This is a normal link: Try them out:diveintomarkYahoo";
jQuery("#yahoo").before(manipulationBareObj([ document.getElementById("first"), document.getElementById("mark")]));
equal(jQuery("#en").text() , expected , "Insert array of elements before");SensorRnt(9525);});
test("before(jQuery)" , function () {
SensorRnt(9526);expect(1);
var expected;
expected = "This is a normal link: diveintomarkTry them out:Yahoo";
jQuery("#yahoo").before(manipulationBareObj(jQuery("#mark, #first")));
equal(jQuery("#en").text() , expected , "Insert jQuery before");SensorRnt(9527);});
test("before(Array<jQuery>)" , function () {
SensorRnt(9528);expect(1);
var expected;
expected = "This is a normal link: Try them out:GooglediveintomarkYahoo";
jQuery("#yahoo").before(manipulationBareObj([ jQuery("#first"), jQuery("#mark, #google")]));
equal(jQuery("#en").text() , expected , "Insert array of jQuery objects before");SensorRnt(9529);});
test("before(Function) -- Returns String" , function () {
SensorRnt(9530);expect(1);
var expected;
expected = "This is a normal link: bugaYahoo";
jQuery("#yahoo").before(manipulationFunctionReturningObj("<b>buga</b>"));
equal(jQuery("#en").text() , expected , "Insert String before");SensorRnt(9531);});
test("before(Function) -- Returns Element" , function () {
SensorRnt(9532);expect(1);
var expected;
expected = "This is a normal link: Try them out:Yahoo";
jQuery("#yahoo").before(manipulationFunctionReturningObj(document.getElementById("first")));
equal(jQuery("#en").text() , expected , "Insert element before");SensorRnt(9533);});
test("before(Function) -- Returns Array<Element>" , function () {
SensorRnt(9534);expect(1);
var expected;
expected = "This is a normal link: Try them out:diveintomarkYahoo";
jQuery("#yahoo").before(manipulationFunctionReturningObj([ document.getElementById("first"), document.getElementById("mark")]));
equal(jQuery("#en").text() , expected , "Insert array of elements before");SensorRnt(9535);});
test("before(Function) -- Returns jQuery" , function () {
SensorRnt(9536);expect(1);
var expected;
expected = "This is a normal link: diveintomarkTry them out:Yahoo";
jQuery("#yahoo").before(manipulationFunctionReturningObj(jQuery("#mark, #first")));
equal(jQuery("#en").text() , expected , "Insert jQuery before");SensorRnt(9537);});
test("before(Function) -- Returns Array<jQuery>" , function () {
SensorRnt(9538);expect(1);
var expected;
expected = "This is a normal link: Try them out:GooglediveintomarkYahoo";
jQuery("#yahoo").before(manipulationFunctionReturningObj([ jQuery("#first"), jQuery("#mark, #google")]));
equal(jQuery("#en").text() , expected , "Insert array of jQuery objects before");SensorRnt(9539);});
test("before(no-op)" , function () {
SensorRnt(9540);expect(2);
var set;
set = jQuery("<div/>").before("<span>test</span>");
equal(set[0].nodeName.toLowerCase() , "div" , "Insert before a disconnected node should be a no-op");
equal(set.length , 1 , "Insert the element before the disconnected node. should be a no-op");SensorRnt(9541);});
test("before and after w/ empty object (#10812)" , function () {
SensorRnt(9542);expect(1);
var res;
res = jQuery("#notInTheDocument").before("(").after(")");
equal(res.length , 0 , "didn't choke on empty object");SensorRnt(9543);});
test(".before() and .after() disconnected node" , function () {
SensorRnt(9544);expect(2);
equal(jQuery("<input type='checkbox'/>").before("<div/>").length , 1 , "before() on disconnected node is no-op");
equal(jQuery("<input type='checkbox'/>").after("<div/>").length , 1 , "after() on disconnected node is no-op");SensorRnt(9545);});
test("insert with .before() on disconnected node last" , function () {
SensorRnt(9546);expect(1);
var expectedBefore = "This is a normal link: bugaYahoo";
jQuery("#yahoo").add("<span/>").before("<b>buga</b>");
equal(jQuery("#en").text() , expectedBefore , "Insert String before with disconnected node last");SensorRnt(9547);});
test("insert with .before() on disconnected node first" , function () {
SensorRnt(9548);expect(1);
var expectedBefore = "This is a normal link: bugaYahoo";
jQuery("<span/>").add("#yahoo").before("<b>buga</b>");
equal(jQuery("#en").text() , expectedBefore , "Insert String before with disconnected node first");SensorRnt(9549);});
test("insert with .before() on disconnected node last" , function () {
SensorRnt(9550);expect(1);
var expectedAfter = "This is a normal link: Yahoobuga";
jQuery("#yahoo").add("<span/>").after("<b>buga</b>");
equal(jQuery("#en").text() , expectedAfter , "Insert String after with disconnected node last");SensorRnt(9551);});
test("insert with .before() on disconnected node last" , function () {
SensorRnt(9552);expect(1);
var expectedAfter = "This is a normal link: Yahoobuga";
jQuery("<span/>").add("#yahoo").after("<b>buga</b>");
equal(jQuery("#en").text() , expectedAfter , "Insert String after with disconnected node first");SensorRnt(9553);});
test("insertBefore(String)" , function () {
SensorRnt(9554);expect(1);
var expected = "This is a normal link: bugaYahoo";
jQuery("<b>buga</b>").insertBefore("#yahoo");
equal(jQuery("#en").text() , expected , "Insert String before");SensorRnt(9555);});
test("insertBefore(Element)" , function () {
SensorRnt(9556);expect(1);
var expected = "This is a normal link: Try them out:Yahoo";
jQuery(document.getElementById("first")).insertBefore("#yahoo");
equal(jQuery("#en").text() , expected , "Insert element before");SensorRnt(9557);});
test("insertBefore(Array<Element>)" , function () {
SensorRnt(9558);expect(1);
var expected = "This is a normal link: Try them out:diveintomarkYahoo";
jQuery([ document.getElementById("first"), document.getElementById("mark")]).insertBefore("#yahoo");
equal(jQuery("#en").text() , expected , "Insert array of elements before");SensorRnt(9559);});
test("insertBefore(jQuery)" , function () {
SensorRnt(9560);expect(1);
var expected = "This is a normal link: diveintomarkTry them out:Yahoo";
jQuery("#mark, #first").insertBefore("#yahoo");
equal(jQuery("#en").text() , expected , "Insert jQuery before");SensorRnt(9561);});
test(".after(String)" , function () {
SensorRnt(9562);expect(1);
var expected = "This is a normal link: Yahoobuga";
jQuery("#yahoo").after("<b>buga</b>");
equal(jQuery("#en").text() , expected , "Insert String after");SensorRnt(9563);});
test(".after(Element)" , function () {
SensorRnt(9564);expect(1);
var expected = "This is a normal link: YahooTry them out:";
jQuery("#yahoo").after(document.getElementById("first"));
equal(jQuery("#en").text() , expected , "Insert element after");SensorRnt(9565);});
test(".after(Array<Element>)" , function () {
SensorRnt(9566);expect(1);
var expected = "This is a normal link: YahooTry them out:diveintomark";
jQuery("#yahoo").after([ document.getElementById("first"), document.getElementById("mark")]);
equal(jQuery("#en").text() , expected , "Insert array of elements after");SensorRnt(9567);});
test(".after(jQuery)" , function () {
SensorRnt(9568);expect(1);
var expected = "This is a normal link: YahooTry them out:Googlediveintomark";
jQuery("#yahoo").after([ jQuery("#first"), jQuery("#mark, #google")]);
equal(jQuery("#en").text() , expected , "Insert array of jQuery objects after");SensorRnt(9569);});
test(".after(Function) returns String" , function () {
SensorRnt(9570);expect(1);
var expected = "This is a normal link: Yahoobuga", val = manipulationFunctionReturningObj;
jQuery("#yahoo").after(val("<b>buga</b>"));
equal(jQuery("#en").text() , expected , "Insert String after");SensorRnt(9571);});
test(".after(Function) returns Element" , function () {
SensorRnt(9572);expect(1);
var expected = "This is a normal link: YahooTry them out:", val = manipulationFunctionReturningObj;
jQuery("#yahoo").after(val(document.getElementById("first")));
equal(jQuery("#en").text() , expected , "Insert element after");SensorRnt(9573);});
test(".after(Function) returns Array<Element>" , function () {
SensorRnt(9574);expect(1);
var expected = "This is a normal link: YahooTry them out:diveintomark", val = manipulationFunctionReturningObj;
jQuery("#yahoo").after(val([ document.getElementById("first"), document.getElementById("mark")]));
equal(jQuery("#en").text() , expected , "Insert array of elements after");SensorRnt(9575);});
test(".after(Function) returns jQuery" , function () {
SensorRnt(9576);expect(1);
var expected = "This is a normal link: YahooTry them out:Googlediveintomark", val = manipulationFunctionReturningObj;
jQuery("#yahoo").after(val([ jQuery("#first"), jQuery("#mark, #google")]));
equal(jQuery("#en").text() , expected , "Insert array of jQuery objects after");SensorRnt(9577);});
test(".after(disconnected node)" , function () {
SensorRnt(9578);expect(2);
var set = jQuery("<div/>").before("<span>test</span>");
equal(set[0].nodeName.toLowerCase() , "div" , "Insert after a disconnected node should be a no-op");
equal(set.length , 1 , "Insert the element after the disconnected node should be a no-op");SensorRnt(9579);});
test("insertAfter(String)" , function () {
SensorRnt(9580);expect(1);
var expected = "This is a normal link: Yahoobuga";
jQuery("<b>buga</b>").insertAfter("#yahoo");
equal(jQuery("#en").text() , expected , "Insert String after");SensorRnt(9581);});
test("insertAfter(Element)" , function () {
SensorRnt(9582);expect(1);
var expected = "This is a normal link: YahooTry them out:";
jQuery(document.getElementById("first")).insertAfter("#yahoo");
equal(jQuery("#en").text() , expected , "Insert element after");SensorRnt(9583);});
test("insertAfter(Array<Element>)" , function () {
SensorRnt(9584);expect(1);
var expected = "This is a normal link: YahooTry them out:diveintomark";
jQuery([ document.getElementById("first"), document.getElementById("mark")]).insertAfter("#yahoo");
equal(jQuery("#en").text() , expected , "Insert array of elements after");SensorRnt(9585);});
test("insertAfter(jQuery)" , function () {
SensorRnt(9586);expect(1);
var expected = "This is a normal link: YahoodiveintomarkTry them out:";
jQuery("#mark, #first").insertAfter("#yahoo");
equal(jQuery("#en").text() , expected , "Insert jQuery after");SensorRnt(9587);});
function testReplaceWith ( val ) {
var tmp, y, child, child2, set, non_existent, $div, expected = 29;
SensorRnt(9588);expect(expected);
jQuery("#yahoo").replaceWith(val("<b id='replace'>buga</b>"));
ok(jQuery("#replace")[0] , "Replace element with element from string");
ok(! jQuery("#yahoo")[0] , "Verify that original element is gone, after string");
jQuery("#anchor2").replaceWith(val(document.getElementById("first")));
ok(jQuery("#first")[0] , "Replace element with element");
ok(! jQuery("#anchor2")[0] , "Verify that original element is gone, after element");
jQuery("#qunit-fixture").append("<div id='bar'><div id='baz'></div></div>");
jQuery("#baz").replaceWith(val("Baz"));
equal(jQuery("#bar").text() , "Baz" , "Replace element with text");
ok(! jQuery("#baz")[0] , "Verify that original element is gone, after element");
jQuery("#bar").replaceWith("<div id='yahoo'></div>" , "..." , "<div id='baz'></div>");
deepEqual(jQuery("#yahoo, #baz").get() , q("yahoo" , "baz") , "Replace element with multiple arguments (#13722)");
strictEqual(jQuery("#yahoo")[0].nextSibling , jQuery("#baz")[0].previousSibling , "Argument order preserved");
deepEqual(jQuery("#bar").get() , [ ] , "Verify that original element is gone, after multiple arguments");
jQuery("#google").replaceWith(val([ document.getElementById("first"), document.getElementById("mark")]));
deepEqual(jQuery("#mark, #first").get() , q("first" , "mark") , "Replace element with array of elements");
ok(! jQuery("#google")[0] , "Verify that original element is gone, after array of elements");
jQuery("#groups").replaceWith(val(jQuery("#mark, #first")));
deepEqual(jQuery("#mark, #first").get() , q("first" , "mark") , "Replace element with jQuery collection");
ok(! jQuery("#groups")[0] , "Verify that original element is gone, after jQuery collection");
jQuery("#mark, #first").replaceWith(val("<span class='replacement'></span><span class='replacement'></span>"));
equal(jQuery("#qunit-fixture .replacement").length , 4 , "Replace multiple elements (#12449)");
deepEqual(jQuery("#mark, #first").get() , [ ] , "Verify that original elements are gone, after replace multiple");
tmp = jQuery("<b>content</b>")[0];
jQuery("#anchor1").contents().replaceWith(val(tmp));
deepEqual(jQuery("#anchor1").contents().get() , [ tmp] , "Replace text node with element");
tmp = jQuery("<div/>").appendTo("#qunit-fixture").on("click" , function () {
SensorRnt(9590);ok(true , "Newly bound click run.");SensorRnt(9591);});
y = jQuery("<div/>").appendTo("#qunit-fixture").on("click" , function () {
SensorRnt(9592);ok(false , "Previously bound click run.");SensorRnt(9593);});
child = y.append("<b>test</b>").find("b").on("click" , function () {
SensorRnt(9594);ok(true , "Child bound click run.");
RNTRETRET = false;
SensorRnt(9595);return RNTRETRET;});
y.replaceWith(val(tmp));
tmp.trigger("click");
y.trigger("click");
child.trigger("click");
y = jQuery("<div/>").appendTo("#qunit-fixture").on("click" , function () {
SensorRnt(9596);ok(false , "Previously bound click run.");SensorRnt(9597);});
child2 = y.append("<u>test</u>").find("u").on("click" , function () {
SensorRnt(9598);ok(true , "Child 2 bound click run.");
RNTRETRET = false;
SensorRnt(9599);return RNTRETRET;});
y.replaceWith(val(child2));
child2.trigger("click");
set = jQuery("<div/>").replaceWith(val("<span>test</span>"));
equal(set[0].nodeName.toLowerCase() , "div" , "No effect on a disconnected node.");
equal(set.length , 1 , "No effect on a disconnected node.");
equal(set[0].childNodes.length , 0 , "No effect on a disconnected node.");
child = jQuery("#qunit-fixture").children().first();
$div = jQuery("<div class='pathological'/>").insertBefore(child);
$div.replaceWith($div);
deepEqual(jQuery(".pathological" , "#qunit-fixture").get() , $div.get() , "Self-replacement");
$div.replaceWith(child);
deepEqual(jQuery("#qunit-fixture").children().first().get() , child.get() , "Replacement with following sibling (#13810)");
deepEqual(jQuery(".pathological" , "#qunit-fixture").get() , [ ] , "Replacement with following sibling (context removed)");
non_existent = jQuery("#does-not-exist").replaceWith(val("<b>should not throw an error</b>"));
equal(non_existent.length , 0 , "Length of non existent element.");
$div = jQuery("<div class='replacewith'></div>").appendTo("#qunit-fixture");
$div.replaceWith(val("<div class='replacewith'></div><script>"+"equal( jQuery('.replacewith').length, 1, 'Check number of elements in page.' );"+"</script>"));
jQuery("#qunit-fixture").append("<div id='replaceWith'></div>");
equal(jQuery("#qunit-fixture").find("div[id=replaceWith]").length , 1 , "Make sure only one div exists.");
jQuery("#replaceWith").replaceWith(val("<div id='replaceWith'></div>"));
equal(jQuery("#qunit-fixture").find("div[id=replaceWith]").length , 1 , "Make sure only one div exists after replacement.");
jQuery("#replaceWith").replaceWith(val("<div id='replaceWith'></div>"));
equal(jQuery("#qunit-fixture").find("div[id=replaceWith]").length , 1 , "Make sure only one div exists after subsequent replacement.");
RNTRETRET = expected;
SensorRnt(9589);return RNTRETRET;}
test("replaceWith(String|Element|Array<Element>|jQuery)" , function () {
SensorRnt(9600);testReplaceWith(manipulationBareObj);SensorRnt(9601);});
test("replaceWith(Function)" , function () {
SensorRnt(9602);expect(testReplaceWith(manipulationFunctionReturningObj)+1);
var y = jQuery("#foo")[0];
jQuery(y).replaceWith(function () {
SensorRnt(9604);equal(this , y , "Make sure the context is coming in correctly.");SensorRnt(9605);});SensorRnt(9603);});
test("replaceWith(string) for more than one element" , function () {
SensorRnt(9606);expect(3);
equal(jQuery("#foo p").length , 3 , "ensuring that test data has not changed");
jQuery("#foo p").replaceWith("<span>bar</span>");
equal(jQuery("#foo span").length , 3 , "verify that all the three original element have been replaced");
equal(jQuery("#foo p").length , 0 , "verify that all the three original element have been replaced");SensorRnt(9607);});
test("Empty replaceWith (#13401; #13596)" , 8 , function () {
SensorRnt(9608);var $el = jQuery("<div/>"), tests = {
"empty string" : "",
"empty array" : [ ],
"empty collection" : jQuery("#nonexistent"),
"empty undefined" : undefined
};
jQuery.each(tests , function ( label, input ) {
SensorRnt(9610);$el.html("<a/>").children().replaceWith(input);
strictEqual($el.html() , "" , "replaceWith("+label+")");
$el.html("<b/>").children().replaceWith(function () {
RNTRETRET = input;
SensorRnt(9612);return RNTRETRET;});
strictEqual($el.html() , "" , "replaceWith(function returning "+label+")");SensorRnt(9611);});SensorRnt(9609);});
test("replaceAll(String)" , function () {
SensorRnt(9613);expect(2);
jQuery("<b id='replace'>buga</b>").replaceAll("#yahoo");
ok(jQuery("#replace")[0] , "Replace element with string");
ok(! jQuery("#yahoo")[0] , "Verify that original element is gone, after string");SensorRnt(9614);});
test("replaceAll(Element)" , function () {
SensorRnt(9615);expect(2);
jQuery(document.getElementById("first")).replaceAll("#yahoo");
ok(jQuery("#first")[0] , "Replace element with element");
ok(! jQuery("#yahoo")[0] , "Verify that original element is gone, after element");SensorRnt(9616);});
test("replaceAll(Array<Element>)" , function () {
SensorRnt(9617);expect(3);
jQuery([ document.getElementById("first"), document.getElementById("mark")]).replaceAll("#yahoo");
ok(jQuery("#first")[0] , "Replace element with array of elements");
ok(jQuery("#mark")[0] , "Replace element with array of elements");
ok(! jQuery("#yahoo")[0] , "Verify that original element is gone, after array of elements");SensorRnt(9618);});
test("replaceAll(jQuery)" , function () {
SensorRnt(9619);expect(3);
jQuery("#mark, #first").replaceAll("#yahoo");
ok(jQuery("#first")[0] , "Replace element with set of elements");
ok(jQuery("#mark")[0] , "Replace element with set of elements");
ok(! jQuery("#yahoo")[0] , "Verify that original element is gone, after set of elements");SensorRnt(9620);});
test("jQuery.clone() (#8017)" , function () {
SensorRnt(9621);expect(2);
ok(jQuery.clone&&jQuery.isFunction(jQuery.clone) , "jQuery.clone() utility exists and is a function.");
var main = jQuery("#qunit-fixture")[0], clone = jQuery.clone(main);
equal(main.childNodes.length , clone.childNodes.length , "Simple child length to ensure a large dom tree copies correctly");SensorRnt(9622);});
test("append to multiple elements (#8070)" , function () {
SensorRnt(9623);expect(2);
var selects = jQuery("<select class='test8070'></select><select class='test8070'></select>").appendTo("#qunit-fixture");
selects.append("<OPTION>1</OPTION><OPTION>2</OPTION>");
equal(selects[0].childNodes.length , 2 , "First select got two nodes");
equal(selects[1].childNodes.length , 2 , "Second select got two nodes");SensorRnt(9624);});
test("table manipulation" , function () {
SensorRnt(9625);expect(2);
var table = jQuery("<table style='font-size:16px'></table>").appendTo("#qunit-fixture").empty(), height = table[0].offsetHeight;
table.append("<tr><td>DATA</td></tr>");
ok(table[0].offsetHeight-height >= 15 , "appended rows are visible");
table.empty();
height = table[0].offsetHeight;
table.prepend("<tr><td>DATA</td></tr>");
ok(table[0].offsetHeight-height >= 15 , "prepended rows are visible");SensorRnt(9626);});
test("clone()" , function () {
SensorRnt(9627);expect(45);
var div, clone, form, body;
equal(jQuery("#en").text() , "This is a normal link: Yahoo" , "Assert text for #en");
equal(jQuery("#first").append(jQuery("#yahoo").clone()).text() , "Try them out:Yahoo" , "Check for clone");
equal(jQuery("#en").text() , "This is a normal link: Yahoo" , "Reassert text for #en");
jQuery.each("table thead tbody tfoot tr td div button ul ol li select option textarea iframe".split(" ") , function ( i, nodeName ) {
SensorRnt(9629);equal(jQuery("<"+nodeName+"/>").clone()[0].nodeName.toLowerCase() , nodeName , "Clone a "+nodeName);SensorRnt(9630);});
equal(jQuery("<input type='checkbox' />").clone()[0].nodeName.toLowerCase() , "input" , "Clone a <input type='checkbox' />");
equal(jQuery("#nonnodes").contents().clone().length , 3 , "Check node,textnode,comment clone works (some browsers delete comments on clone)");
div = jQuery("<div><ul><li>test</li></ul></div>").on("click" , function () {
SensorRnt(9631);ok(true , "Bound event still exists.");SensorRnt(9632);});
clone = div.clone(true);
div.remove();
div = clone.clone(true);
clone.remove();
equal(div.length , 1 , "One element cloned");
equal(div[0].nodeName.toUpperCase() , "DIV" , "DIV element cloned");
div.trigger("click");
div.remove();
div = jQuery("<div/>").append([ document.createElement("table"), document.createElement("table")]);
div.find("table").on("click" , function () {
SensorRnt(9633);ok(true , "Bound event still exists.");SensorRnt(9634);});
clone = div.clone(true);
equal(clone.length , 1 , "One element cloned");
equal(clone[0].nodeName.toUpperCase() , "DIV" , "DIV element cloned");
clone.find("table").trigger("click");
div.remove();
clone.remove();
div = jQuery("<div><ul><li>test</li></ul></div>").on("click" , function () {
SensorRnt(9635);ok(false , "Bound event still exists after .clone().");SensorRnt(9636);});
clone = div.clone();
clone.trigger("click");
clone.remove();
div.remove();
div = jQuery("<div/>").html("<embed height='355' width='425' src='http://www.youtube.com/v/3KANI2dpXLw&amp;hl=en'></embed>");
clone = div.clone(true);
equal(clone.length , 1 , "One element cloned");
equal(clone.html() , div.html() , "Element contents cloned");
equal(clone[0].nodeName.toUpperCase() , "DIV" , "DIV element cloned");
div = jQuery("<div/>").html("<object height='355' width='425' classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'>  <param name='movie' value='http://www.youtube.com/v/3KANI2dpXLw&amp;hl=en'>  <param name='wmode' value='transparent'> </object>");
clone = div.clone(true);
equal(clone.length , 1 , "One element cloned");
equal(clone[0].nodeName.toUpperCase() , "DIV" , "DIV element cloned");
div = div.find("object");
clone = clone.find("object");
jQuery.each([ "height", "width", "classid"] , function ( i, attr ) {
SensorRnt(9637);equal(clone.attr(attr) , div.attr(attr) , "<object> attribute cloned: "+attr);SensorRnt(9638);});
(function () {
var params = {

};
SensorRnt(9639);clone.find("param").each(function ( index, param ) {
SensorRnt(9641);params[param.attributes.name.nodeValue.toLowerCase()] = param.attributes.value.nodeValue.toLowerCase();SensorRnt(9642);});
div.find("param").each(function ( index, param ) {
SensorRnt(9643);var key = param.attributes.name.nodeValue.toLowerCase();
equal(params[key] , param.attributes.value.nodeValue.toLowerCase() , "<param> cloned: "+key);SensorRnt(9644);});SensorRnt(9640);})();
div = jQuery("<div/>").html("<object height='355' width='425' type='application/x-shockwave-flash' data='http://www.youtube.com/v/3KANI2dpXLw&amp;hl=en'>  <param name='movie' value='http://www.youtube.com/v/3KANI2dpXLw&amp;hl=en'>  <param name='wmode' value='transparent'> </object>");
clone = div.clone(true);
equal(clone.length , 1 , "One element cloned");
equal(clone.html() , div.html() , "Element contents cloned");
equal(clone[0].nodeName.toUpperCase() , "DIV" , "DIV element cloned");
div = jQuery("<div/>").data({
"a" : true
});
clone = div.clone(true);
equal(clone.data("a") , true , "Data cloned.");
clone.data("a" , false);
equal(clone.data("a") , false , "Ensure cloned element data object was correctly modified");
equal(div.data("a") , true , "Ensure cloned element data object is copied, not referenced");
div.remove();
clone.remove();
form = document.createElement("form");
form.action = "/test/";
div = document.createElement("div");
div.appendChild(document.createTextNode("test"));
form.appendChild(div);
equal(jQuery(form).clone().children().length , 1 , "Make sure we just get the form back.");
body = jQuery("body").clone();
equal(body.children()[0].id , "qunit" , "Make sure cloning body works");
body.remove();SensorRnt(9628);});
test("clone(script type=non-javascript) (#11359)" , function () {
SensorRnt(9645);expect(3);
var src = jQuery("<script type='text/filler'>Lorem ipsum dolor sit amet</script><q><script type='text/filler'>consectetur adipiscing elit</script></q>"), dest = src.clone();
equal(dest[0].text , "Lorem ipsum dolor sit amet" , "Cloning preserves script text");
equal(dest.last().html() , src.last().html() , "Cloning preserves nested script text");
ok(/^\s*<scr.pt\s+type=['"]?text\/filler['"]?\s*>consectetur adipiscing elit<\/scr.pt>\s*$/i.test(dest.last().html()) , "Cloning preserves nested script text");
dest.remove();SensorRnt(9646);});
test("clone(form element) (Bug #3879, #6655)" , function () {
SensorRnt(9647);expect(5);
var clone, element;
element = jQuery("<select><option>Foo</option><option value='selected' selected>Bar</option></select>");
equal(element.clone().find("option").filter(function () {
RNTRETRET = this.selected;
return RNTRETRET;}).val() , "selected" , "Selected option cloned correctly");
element = jQuery("<input type='checkbox' value='foo'>").attr("checked" , "checked");
clone = element.clone();
equal(clone.is(":checked") , element.is(":checked") , "Checked input cloned correctly");
equal(clone[0].defaultValue , "foo" , "Checked input defaultValue cloned correctly");
element = jQuery("<input type='text' value='foo'>");
clone = element.clone();
equal(clone[0].defaultValue , "foo" , "Text input defaultValue cloned correctly");
element = jQuery("<textarea>foo</textarea>");
clone = element.clone();
equal(clone[0].defaultValue , "foo" , "Textarea defaultValue cloned correctly");SensorRnt(9648);});
test("clone(multiple selected options) (Bug #8129)" , function () {
SensorRnt(9649);expect(1);
var element = jQuery("<select><option>Foo</option><option selected>Bar</option><option selected>Baz</option></select>");
equal(element.clone().find("option:selected").length , element.find("option:selected").length , "Multiple selected options cloned correctly");SensorRnt(9650);});
test("clone() on XML nodes" , function () {
SensorRnt(9651);expect(2);
var xml = createDashboardXML(), root = jQuery(xml.documentElement).clone(), origTab = jQuery("tab" , xml).eq(0), cloneTab = jQuery("tab" , root).eq(0);
origTab.text("origval");
cloneTab.text("cloneval");
equal(origTab.text() , "origval" , "Check original XML node was correctly set");
equal(cloneTab.text() , "cloneval" , "Check cloned XML node was correctly set");SensorRnt(9652);});
test("clone() on local XML nodes with html5 nodename" , function () {
SensorRnt(9653);expect(2);
var $xmlDoc = jQuery(jQuery.parseXML("<root><meter /></root>")), $meter = $xmlDoc.find("meter").clone();
equal($meter[0].nodeName , "meter" , "Check if nodeName was not changed due to cloning");
equal($meter[0].nodeType , 1 , "Check if nodeType is not changed due to cloning");SensorRnt(9654);});
test("html(undefined)" , function () {
SensorRnt(9655);expect(1);
equal(jQuery("#foo").html("<i>test</i>").html(undefined).html().toLowerCase() , "<i>test</i>" , ".html(undefined) is chainable (#5571)");SensorRnt(9656);});
test("html() on empty set" , function () {
SensorRnt(9657);expect(1);
strictEqual(jQuery().html() , undefined , ".html() returns undefined for empty sets (#11962)");SensorRnt(9658);});
function childNodeNames ( node ) {
RNTRETRET = jQuery.map(node.childNodes , function ( child ) {
RNTRETRET = child.nodeName.toUpperCase();
return RNTRETRET;}).join(" ");
SensorRnt(9659);return RNTRETRET;}
function testHtml ( valueObj ) {
SensorRnt(9660);expect(40);
var actual, expected, tmp, div = jQuery("<div></div>"), fixture = jQuery("#qunit-fixture");
div.html(valueObj("<div id='parent_1'><div id='child_1'/></div><div id='parent_2'/>"));
equal(div.children().length , 2 , "Found children");
equal(div.children().children().length , 1 , "Found grandchild");
actual = [ ];
expected = [ ];
tmp = jQuery("<map/>").html(valueObj("<area alt='area'/>")).each(function () {
SensorRnt(9662);expected.push("AREA");
actual.push(childNodeNames(this));SensorRnt(9663);});
equal(expected.length , 1 , "Expecting one parent");
deepEqual(actual , expected , "Found the inserted area element");
equal(div.html(valueObj(5)).html() , "5" , "Setting a number as html");
equal(div.html(valueObj(0)).html() , "0" , "Setting a zero as html");
equal(div.html(valueObj(Infinity)).html() , "Infinity" , "Setting Infinity as html");
equal(div.html(valueObj(NaN)).html() , "" , "Setting NaN as html");
equal(div.html(valueObj(1e2)).html() , "100" , "Setting exponential number notation as html");
div.html(valueObj("&#160;&amp;"));
equal(div[0].innerHTML.replace(/\xA0/ , "&nbsp;") , "&nbsp;&amp;" , "Entities are passed through correctly");
tmp = "&lt;div&gt;hello1&lt;/div&gt;";
equal(div.html(valueObj(tmp)).html().replace(/>/g , "&gt;") , tmp , "Escaped html");
tmp = "x"+tmp;
equal(div.html(valueObj(tmp)).html().replace(/>/g , "&gt;") , tmp , "Escaped html, leading x");
tmp = " "+tmp.slice(1);
equal(div.html(valueObj(tmp)).html().replace(/>/g , "&gt;") , tmp , "Escaped html, leading space");
actual = [ ];
expected = [ ];
tmp = {

};
jQuery("#nonnodes").contents().html(valueObj("<b>bold</b>")).each(function () {
SensorRnt(9664);var html = jQuery(this).html();
tmp[this.nodeType] = true;
expected.push(this.nodeType===1?"<b>bold</b>":undefined);
actual.push(html?html.toLowerCase():html);SensorRnt(9665);});
deepEqual(actual , expected , "Set containing element, text node, comment");
ok(tmp[1] , "element");
ok(tmp[3] , "text node");
ok(tmp[8] , "comment");
actual = [ ];
expected = [ ];
fixture.children("div").html(valueObj("<b>test</b>")).each(function () {
SensorRnt(9666);expected.push("B");
actual.push(childNodeNames(this));SensorRnt(9667);});
equal(expected.length , 7 , "Expecting many parents");
deepEqual(actual , expected , "Correct childNodes after setting HTML");
actual = [ ];
expected = [ ];
fixture.html(valueObj("<style>.foobar{color:green;}</style>")).each(function () {
SensorRnt(9668);expected.push("STYLE");
actual.push(childNodeNames(this));SensorRnt(9669);});
equal(expected.length , 1 , "Expecting one parent");
deepEqual(actual , expected , "Found the inserted style element");
fixture.html(valueObj("<select/>"));
jQuery("#qunit-fixture select").html(valueObj("<option>O1</option><option selected='selected'>O2</option><option>O3</option>"));
equal(jQuery("#qunit-fixture select").val() , "O2" , "Selected option correct");
tmp = fixture.html(valueObj([ "<script type='something/else'>ok( false, 'evaluated: non-script' );</script>", "<script type='text/javascript'>ok( true, 'evaluated: text/javascript' );</script>", "<script type='text/ecmascript'>ok( true, 'evaluated: text/ecmascript' );</script>", "<script>ok( true, 'evaluated: no type' );</script>", "<div>", "<script type='something/else'>ok( false, 'evaluated: inner non-script' );</script>", "<script type='text/javascript'>ok( true, 'evaluated: inner text/javascript' );</script>", "<script type='text/ecmascript'>ok( true, 'evaluated: inner text/ecmascript' );</script>", "<script>ok( true, 'evaluated: inner no type' );</script>", "</div>"].join(""))).find("script");
equal(tmp.length , 8 , "All script tags remain.");
equal(tmp[0].type , "something/else" , "Non-evaluated type.");
equal(tmp[1].type , "text/javascript" , "Evaluated type.");
fixture.html(valueObj("<script type='text/javascript'>ok( true, 'Injection of identical script' );</script>"));
fixture.html(valueObj("<script type='text/javascript'>ok( true, 'Injection of identical script' );</script>"));
fixture.html(valueObj("<script type='text/javascript'>ok( true, 'Injection of identical script' );</script>"));
fixture.html(valueObj("foo <form><script type='text/javascript'>ok( true, 'Injection of identical script (#975)' );</script></form>"));
jQuery.scriptorder = 0;
fixture.html(valueObj([ "<script>", "equal( jQuery('#scriptorder').length, 1,'Execute after html' );", "equal( jQuery.scriptorder++, 0, 'Script is executed in order' );", "</script>", "<span id='scriptorder'><script>equal( jQuery.scriptorder++, 1, 'Script (nested) is executed in order');</script></span>", "<script>equal( jQuery.scriptorder++, 2, 'Script (unnested) is executed in order' );</script>"].join("")));
fixture.html(valueObj(fixture.text()));
ok(/^[^<]*[^<\s][^<]*$/.test(fixture.html()) , "Replace html with text");SensorRnt(9661);}
test("html(String|Number)" , function () {
SensorRnt(9670);testHtml(manipulationBareObj);SensorRnt(9671);});
test("html(Function)" , function () {
SensorRnt(9672);testHtml(manipulationFunctionReturningObj);SensorRnt(9673);});
test("html(Function) with incoming value -- direct selection" , function () {
SensorRnt(9674);expect(4);
var els, actualhtml, pass;
els = jQuery("#foo > p");
actualhtml = els.map(function () {
RNTRETRET = jQuery(this).html();
SensorRnt(9676);return RNTRETRET;});
els.html(function ( i, val ) {
SensorRnt(9677);equal(val , actualhtml[i] , "Make sure the incoming value is correct.");
RNTRETRET = "<b>test</b>";
SensorRnt(9678);return RNTRETRET;});
pass = true;
els.each(function () {
SensorRnt(9679);if (this.childNodes.length!==1)
{ 
SensorRnt(9680);pass = false;
};SensorRnt(9681);});
ok(pass , "Set HTML");SensorRnt(9675);});
test("html(Function) with incoming value -- jQuery.contents()" , function () {
SensorRnt(9682);expect(14);
var actualhtml, j, $div, $div2, insert;
j = jQuery("#nonnodes").contents();
actualhtml = j.map(function () {
RNTRETRET = jQuery(this).html();
SensorRnt(9687);return RNTRETRET;});
j.html(function ( i, val ) {
SensorRnt(9688);equal(val , actualhtml[i] , "Make sure the incoming value is correct.");
RNTRETRET = "<b>bold</b>";
SensorRnt(9689);return RNTRETRET;});
SensorRnt(9683);if (j.length===2)
{ 
SensorRnt(9684);equal(null , null , "Make sure the incoming value is correct.");
}
SensorRnt(9685);equal(j.html().replace(/ xmlns="[^"]+"/g , "").toLowerCase() , "<b>bold</b>" , "Check node,textnode,comment with html()");
$div = jQuery("<div />");
equal($div.html(function ( i, val ) {
equal(val , "" , "Make sure the incoming value is correct.");
RNTRETRET = 5;
return RNTRETRET;}).html() , "5" , "Setting a number as html");
equal($div.html(function ( i, val ) {
equal(val , "5" , "Make sure the incoming value is correct.");
RNTRETRET = 0;
return RNTRETRET;}).html() , "0" , "Setting a zero as html");
$div2 = jQuery("<div/>");
insert = "&lt;div&gt;hello1&lt;/div&gt;";
equal($div2.html(function ( i, val ) {
equal(val , "" , "Make sure the incoming value is correct.");
RNTRETRET = insert;
return RNTRETRET;}).html().replace(/>/g , "&gt;") , insert , "Verify escaped insertion.");
equal($div2.html(function ( i, val ) {
equal(val.replace(/>/g , "&gt;") , insert , "Make sure the incoming value is correct.");
RNTRETRET = "x"+insert;
return RNTRETRET;}).html().replace(/>/g , "&gt;") , "x"+insert , "Verify escaped insertion.");
equal($div2.html(function ( i, val ) {
equal(val.replace(/>/g , "&gt;") , "x"+insert , "Make sure the incoming value is correct.");
RNTRETRET = " "+insert;
return RNTRETRET;}).html().replace(/>/g , "&gt;") , " "+insert , "Verify escaped insertion.");SensorRnt(9686);});
test("clone()/html() don't expose jQuery/Sizzle expandos (#12858)" , function () {
SensorRnt(9690);expect(2);
var $content = jQuery("<div><b><i>text</i></b></div>").appendTo("#qunit-fixture"), expected = /^<b><i>text<\/i><\/b>$/i;
SensorRnt(9691);try{ 
SensorRnt(9692);$content.find(":nth-child(1):lt(4)").data("test" , true);
}
catch(e){ 
SensorRnt(9693);$content.find("*").data("test" , true);
}
SensorRnt(9694);ok(expected.test($content.clone(false)[0].innerHTML) , "clone()");
ok(expected.test($content.html()) , "html()");SensorRnt(9695);});
test("remove() no filters" , function () {
SensorRnt(9696);expect(3);
var first = jQuery("#ap").children().first();
first.data("foo" , "bar");
jQuery("#ap").children().remove();
ok(jQuery("#ap").text().length > 10 , "Check text is not removed");
equal(jQuery("#ap").children().length , 0 , "Check remove");
equal(first.data("foo") , null , "first data");SensorRnt(9697);});
test("remove() with filters" , function () {
SensorRnt(9698);expect(8);
var markup, div;
jQuery("#ap").children().remove("a");
ok(jQuery("#ap").text().length > 10 , "Check text is not removed");
equal(jQuery("#ap").children().length , 1 , "Check filtered remove");
jQuery("#ap").children().remove("a, code");
equal(jQuery("#ap").children().length , 0 , "Check multi-filtered remove");
markup = "<div><span>1</span><span>2</span><span>3</span><span>4</span></div>";
div = jQuery(markup);
div.children().remove("span:nth-child(2n)");
equal(div.text() , "13" , "relative selector in remove");
div = jQuery(markup);
div.children().remove("span:first");
equal(div.text() , "234" , "positional selector in remove");
div = jQuery(markup);
div.children().remove("span:last");
equal(div.text() , "123" , "positional selector in remove");
ok(jQuery("#nonnodes").contents().length >= 2 , "Check node,textnode,comment remove works");
jQuery("#nonnodes").contents().remove();
equal(jQuery("#nonnodes").contents().length , 0 , "Check node,textnode,comment remove works");SensorRnt(9699);});
test("remove() event cleaning " , 1 , function () {
var count, first, cleanUp;
count = 0;
SensorRnt(9700);first = jQuery("#ap").children().first();
cleanUp = first.on("click" , function () {
count++;}).remove().appendTo("#qunit-fixture").trigger("click");
strictEqual(0 , count , "Event handler has been removed");
cleanUp.remove();SensorRnt(9701);});
test("remove() in document order #13779" , 1 , function () {
var last, cleanData = jQuery.cleanData;
jQuery.cleanData = function ( nodes ) {
SensorRnt(9704);last = jQuery.text(nodes[0]);
cleanData.call(this , nodes);SensorRnt(9705);};
SensorRnt(9702);jQuery("#qunit-fixture").append(jQuery.parseHTML("<div class='removal-fixture'>1</div>"+"<div class='removal-fixture'>2</div>"+"<div class='removal-fixture'>3</div>"));
jQuery(".removal-fixture").remove();
equal(last , 3 , "The removal fixtures were removed in document order");
jQuery.cleanData = cleanData;SensorRnt(9703);});
test("detach() no filters" , function () {
SensorRnt(9706);expect(3);
var first = jQuery("#ap").children().first();
first.data("foo" , "bar");
jQuery("#ap").children().detach();
ok(jQuery("#ap").text().length > 10 , "Check text is not removed");
equal(jQuery("#ap").children().length , 0 , "Check remove");
equal(first.data("foo") , "bar");
first.remove();SensorRnt(9707);});
test("detach() with filters" , function () {
SensorRnt(9708);expect(8);
var markup, div;
jQuery("#ap").children().detach("a");
ok(jQuery("#ap").text().length > 10 , "Check text is not removed");
equal(jQuery("#ap").children().length , 1 , "Check filtered remove");
jQuery("#ap").children().detach("a, code");
equal(jQuery("#ap").children().length , 0 , "Check multi-filtered remove");
markup = "<div><span>1</span><span>2</span><span>3</span><span>4</span></div>";
div = jQuery(markup);
div.children().detach("span:nth-child(2n)");
equal(div.text() , "13" , "relative selector in detach");
div = jQuery(markup);
div.children().detach("span:first");
equal(div.text() , "234" , "positional selector in detach");
div = jQuery(markup);
div.children().detach("span:last");
equal(div.text() , "123" , "positional selector in detach");
ok(jQuery("#nonnodes").contents().length >= 2 , "Check node,textnode,comment remove works");
jQuery("#nonnodes").contents().detach();
equal(jQuery("#nonnodes").contents().length , 0 , "Check node,textnode,comment remove works");SensorRnt(9709);});
test("detach() event cleaning " , 1 , function () {
var count, first, cleanUp;
count = 0;
SensorRnt(9710);first = jQuery("#ap").children().first();
cleanUp = first.on("click" , function () {
count++;}).detach().appendTo("#qunit-fixture").trigger("click");
strictEqual(1 , count , "Event handler has not been removed");
cleanUp.remove();SensorRnt(9711);});
test("empty()" , function () {
SensorRnt(9712);expect(3);
equal(jQuery("#ap").children().empty().text().length , 0 , "Check text is removed");
equal(jQuery("#ap").children().length , 4 , "Check elements are not removed");
var j = jQuery("#nonnodes").contents();
j.empty();
equal(j.html() , "" , "Check node,textnode,comment empty works");SensorRnt(9713);});
test("jQuery.cleanData" , function () {
SensorRnt(9714);expect(14);
var type, pos, div, child;
type = "remove";
div = getDiv().remove();
pos = "Outer";
div.trigger("click");
pos = "Inner";
div.children().trigger("click");
type = "empty";
div = getDiv();
child = div.children();
div.empty();
pos = "Outer";
div.trigger("click");
pos = "Inner";
child.trigger("click");
div.remove();
type = "html";
div = getDiv();
child = div.children();
div.html("<div></div>");
pos = "Outer";
div.trigger("click");
pos = "Inner";
child.trigger("click");
div.remove();
function getDiv () {
var div = jQuery("<div class='outer'><div class='inner'></div></div>").on("click" , function () {
ok(true , type+" "+pos+" Click event fired.");}).on("focus" , function () {
ok(true , type+" "+pos+" Focus event fired.");}).find("div").on("click" , function () {
ok(false , type+" "+pos+" Click event fired.");}).on("focus" , function () {
ok(false , type+" "+pos+" Focus event fired.");SensorRnt(9716);}).end().appendTo("body");
div[0].detachEvent = div[0].removeEventListener = function ( t ) {
SensorRnt(9718);ok(true , type+" Outer "+t+" event unbound");SensorRnt(9719);};
div[0].firstChild.detachEvent = div[0].firstChild.removeEventListener = function ( t ) {
SensorRnt(9720);ok(true , type+" Inner "+t+" event unbound");SensorRnt(9721);};
RNTRETRET = div;
SensorRnt(9717);return RNTRETRET;};SensorRnt(9715);});
test("jQuery.buildFragment - no plain-text caching (Bug #6779)" , function () {
SensorRnt(9722);expect(1);
var i, $f = jQuery("<div />").appendTo("#qunit-fixture"), bad = [ "start-", "toString", "hasOwnProperty", "append", "here&there!", "-end"];
SensorRnt(9723);for(i = 0; i < bad.length; i++){ 
SensorRnt(9724);try{ 
SensorRnt(9725);$f.append(bad[i]);
}
catch(e){ 
};SensorRnt(9726);
}
SensorRnt(9727);equal($f.text() , bad.join("") , "Cached strings that match Object properties");
$f.remove();SensorRnt(9728);});
test("jQuery.html - execute scripts escaped with html comment or CDATA (#9221)" , function () {
SensorRnt(9729);expect(3);
jQuery([ "<script type='text/javascript'>", "<!--", "ok( true, '<!-- handled' );", "//-->", "</script>"].join("\n")).appendTo("#qunit-fixture");
jQuery([ "<script type='text/javascript'>", "<![CDATA[", "ok( true, '<![CDATA[ handled' );", "//]]>", "</script>"].join("\n")).appendTo("#qunit-fixture");
jQuery([ "<script type='text/javascript'>", "<!--//--><![CDATA[//><!--", "ok( true, '<!--//--><![CDATA[//><!-- (Drupal case) handled' );", "//--><!]]>", "</script>"].join("\n")).appendTo("#qunit-fixture");SensorRnt(9730);});
test("jQuery.buildFragment - plain objects are not a document #8950" , function () {
SensorRnt(9731);expect(1);
SensorRnt(9732);try{ 
SensorRnt(9733);jQuery("<input type='hidden'>" , {

});
ok(true , "Does not allow attribute object to be treated like a doc object");
}
catch(e){ 
};SensorRnt(9734);});
test("jQuery.clone - no exceptions for object elements #9587" , function () {
SensorRnt(9735);expect(1);
SensorRnt(9736);try{ 
SensorRnt(9737);jQuery("#no-clone-exception").clone();
ok(true , "cloned with no exceptions");
}
catch(e){ 
SensorRnt(9738);ok(false , e.message);
};SensorRnt(9739);});
test("Cloned, detached HTML5 elems (#10667,10670)" , function () {
SensorRnt(9740);expect(7);
var $clone, $section = jQuery("<section>").appendTo("#qunit-fixture");
$clone = $section.clone();
equal($clone[0].nodeName , "SECTION" , "detached clone nodeName matches 'SECTION'");
$section.on("click" , function () {
SensorRnt(9742);ok(true , "clone fired event");SensorRnt(9743);});
$clone = $section.clone(true);
$clone.trigger("click");
$clone.off("click");
$section.append("<p>Hello</p>");
$clone = $section.clone(true);
equal($clone.find("p").text() , "Hello" , "Assert text in child of clone");
$clone.trigger("click");
$clone.off("click");
$section.attr({
"class" : "foo bar baz",
"title" : "This is a title"
});
$clone = $section.clone(true);
equal($clone.attr("class") , $section.attr("class") , "clone and element have same class attribute");
equal($clone.attr("title") , $section.attr("title") , "clone and element have same title attribute");
$section.remove();
$section = $clone.clone(true);
$clone.remove();
$section.trigger("click");
$section.off("click");
$clone.off("click");SensorRnt(9741);});
test("Guard against exceptions when clearing safeChildNodes" , function () {
SensorRnt(9744);expect(1);
var div;
SensorRnt(9745);try{ 
SensorRnt(9746);div = jQuery("<div/><hr/><code/><b/>");
}
catch(e){ 
}
SensorRnt(9747);ok(div&&div.jquery , "Created nodes safely, guarded against exceptions on safeChildNodes[ -1 ]");SensorRnt(9748);});
test("Ensure oldIE creates a new set on appendTo (#8894)" , function () {
SensorRnt(9749);expect(5);
strictEqual(jQuery("<div/>").clone().addClass("test").appendTo("<div/>").end().end().hasClass("test") , false , "Check jQuery.fn.appendTo after jQuery.clone");
strictEqual(jQuery("<div/>").find("p").end().addClass("test").appendTo("<div/>").end().end().hasClass("test") , false , "Check jQuery.fn.appendTo after jQuery.fn.find");
strictEqual(jQuery("<div/>").text("test").addClass("test").appendTo("<div/>").end().end().hasClass("test") , false , "Check jQuery.fn.appendTo after jQuery.fn.text");
strictEqual(jQuery("<bdi/>").clone().addClass("test").appendTo("<div/>").end().end().hasClass("test") , false , "Check jQuery.fn.appendTo after clone html5 element");
strictEqual(jQuery("<p/>").appendTo("<div/>").end().length , jQuery("<p>test</p>").appendTo("<div/>").end().length , "Elements created with createElement and with createDocumentFragment should be treated alike");SensorRnt(9750);});
asyncTest("html() - script exceptions bubble (#11743)" , 2 , function () {
var onerror = window.onerror;
SensorRnt(9751);setTimeout(function () {
window.onerror = onerror;
SensorRnt(9753);start();SensorRnt(9754);} , 1000);
window.onerror = function () {
SensorRnt(9755);ok(true , "Exception thrown");
SensorRnt(9756);if (jQuery.ajax)
{ 
window.onerror = function () {
SensorRnt(9760);ok(true , "Exception thrown in remote script");SensorRnt(9761);};
SensorRnt(9757);jQuery("#qunit-fixture").html("<script src='data/badcall.js'></script>");
ok(true , "Exception ignored");
}
else
{ 
SensorRnt(9758);ok(true , "No jQuery.ajax");
ok(true , "No jQuery.ajax");
};SensorRnt(9759);};
jQuery("#qunit-fixture").html("<script>undefined();</script>");SensorRnt(9752);});
test("checked state is cloned with clone()" , function () {
SensorRnt(9762);expect(2);
var elem = jQuery.parseHTML("<input type='checkbox' checked='checked'/>")[0];
elem.checked = false;
equal(jQuery(elem).clone().attr("id" , "clone")[0].checked , false , "Checked false state correctly cloned");
elem = jQuery.parseHTML("<input type='checkbox'/>")[0];
elem.checked = true;
equal(jQuery(elem).clone().attr("id" , "clone")[0].checked , true , "Checked true state correctly cloned");SensorRnt(9763);});
test("manipulate mixed jQuery and text (#12384, #12346)" , function () {
SensorRnt(9764);expect(2);
var div = jQuery("<div>a</div>").append("&nbsp;" , jQuery("<span>b</span>") , "&nbsp;" , jQuery("<span>c</span>")), nbsp = String.fromCharCode(160);
equal(div.text() , "a"+nbsp+"b"+nbsp+"c" , "Appending mixed jQuery with text nodes");
div = jQuery("<div><div></div></div>").find("div").after("<p>a</p>" , "<p>b</p>").parent();
equal(div.find("*").length , 3 , "added 2 paragraphs after inner div");SensorRnt(9765);});
testIframeWithCallback("buildFragment works even if document[0] is iframe's window object in IE9/10 (#12266)" , "manipulation/iframe-denied.html" , function ( test ) {
SensorRnt(9766);expect(1);
ok(test.status , test.description);SensorRnt(9767);});
test("script evaluation (#11795)" , function () {
SensorRnt(9768);expect(13);
var scriptsIn, scriptsOut, fixture = jQuery("#qunit-fixture").empty(), objGlobal = (function () {
RNTRETRET = this;
SensorRnt(9773);return RNTRETRET;})(), isOk = objGlobal.ok, notOk = function () {
var args = arguments;
args[0] = ! args[0];
RNTRETRET = isOk.apply(this , args);
SensorRnt(9774);return RNTRETRET;};
objGlobal.ok = notOk;
scriptsIn = jQuery([ "<script type='something/else'>ok( false, 'evaluated: non-script' );</script>", "<script type='text/javascript'>ok( true, 'evaluated: text/javascript' );</script>", "<script type='text/ecmascript'>ok( true, 'evaluated: text/ecmascript' );</script>", "<script>ok( true, 'evaluated: no type' );</script>", "<div>", "<script type='something/else'>ok( false, 'evaluated: inner non-script' );</script>", "<script type='text/javascript'>ok( true, 'evaluated: inner text/javascript' );</script>", "<script type='text/ecmascript'>ok( true, 'evaluated: inner text/ecmascript' );</script>", "<script>ok( true, 'evaluated: inner no type' );</script>", "</div>"].join(""));
scriptsIn.appendTo(jQuery("<div class='detached'/>"));
objGlobal.ok = isOk;
scriptsOut = fixture.append(scriptsIn).find("script");
equal(scriptsOut[0].type , "something/else" , "Non-evaluated type.");
equal(scriptsOut[1].type , "text/javascript" , "Evaluated type.");
deepEqual(scriptsOut.get() , fixture.find("script").get() , "All script tags remain.");
objGlobal.ok = notOk;
scriptsOut = scriptsOut.add(scriptsOut.clone()).appendTo(fixture.find("div"));
deepEqual(fixture.find("div script").get() , scriptsOut.get() , "Scripts cloned without reevaluation");
fixture.append(scriptsOut.detach());
deepEqual(fixture.children("script").get() , scriptsOut.get() , "Scripts detached without reevaluation");
objGlobal.ok = isOk;
SensorRnt(9769);if (jQuery.ajax)
{ 
SensorRnt(9770);Globals.register("testBar");
jQuery("#qunit-fixture").append("<script src='"+url("data/testbar.php")+"'/>");
strictEqual(window["testBar"] , "bar" , "Global script evaluation");
}
else
{ 
SensorRnt(9771);ok(true , "No jQuery.ajax");
ok(true , "No jQuery.ajax");
};SensorRnt(9772);});
test("jQuery._evalUrl (#12838)" , function () {
SensorRnt(9775);expect(5);
var message, expectedArgument, ajax = jQuery.ajax, evalUrl = jQuery._evalUrl;
message = "jQuery.ajax implementation";
expectedArgument = 1;
jQuery.ajax = function ( input ) {
SensorRnt(9777);equal((input.url||input).slice(- 1) , expectedArgument , message);
expectedArgument++;SensorRnt(9778);};
jQuery("#qunit-fixture").append("<script src='1'/><script src='2'/>");
equal(expectedArgument , 3 , "synchronous execution");
message = "custom implementation";
expectedArgument = 3;
jQuery._evalUrl = jQuery.ajax;
jQuery.ajax = function ( options ) {
SensorRnt(9779);strictEqual(options , {

} , "Unexpected call to jQuery.ajax");SensorRnt(9780);};
jQuery("#qunit-fixture").append("<script src='3'/><script src='4'/>");
jQuery.ajax = ajax;
jQuery._evalUrl = evalUrl;SensorRnt(9776);});
test("insertAfter, insertBefore, etc do not work when destination is original element. Element is removed (#4087)" , function () {
SensorRnt(9781);expect(10);
var elems;
jQuery.each([ "appendTo", "prependTo", "insertBefore", "insertAfter", "replaceAll"] , function ( index, name ) {
SensorRnt(9783);elems = jQuery([ "<ul id='test4087-complex'><li class='test4087'><div>c1</div>h1</li><li><div>c2</div>h2</li></ul>", "<div id='test4087-simple'><div class='test4087-1'>1<div class='test4087-2'>2</div><div class='test4087-3'>3</div></div></div>", "<div id='test4087-multiple'><div class='test4087-multiple'>1</div><div class='test4087-multiple'>2</div></div>"].join("")).appendTo("#qunit-fixture");
jQuery("#test4087-complex div")[name]("#test4087-complex li:last-child div:last-child");
equal(jQuery("#test4087-complex li:last-child div").length , name==="replaceAll"?1:2 , name+" a node to itself, complex case.");
jQuery(".test4087-1")[name](".test4087-1");
equal(jQuery(".test4087-1").length , 1 , name+" a node to itself, simple case.");
jQuery("#test4087-complex").remove();
jQuery("#test4087-simple").remove();
jQuery("#test4087-multiple").remove();SensorRnt(9784);});SensorRnt(9782);});
test("Index for function argument should be received (#13094)" , 2 , function () {
var i = 0;
SensorRnt(9785);jQuery("<div/><div/>").before(function ( index ) {
SensorRnt(9787);equal(index , i++ , "Index should be correct");SensorRnt(9788);});SensorRnt(9786);});
test("Make sure jQuery.fn.remove can work on elements in documentFragment" , 1 , function () {
SensorRnt(9789);var fragment = document.createDocumentFragment(), div = fragment.appendChild(document.createElement("div"));
jQuery(div).remove();
equal(fragment.childNodes.length , 0 , "div element was removed from documentFragment");SensorRnt(9790);});
test("Make sure specific elements with content created correctly (#13232)" , 20 , function () {
var results = [ ], args = [ ], elems = {
thead : "<tr><td>thead</td></tr>",
tbody : "<tr><td>tbody</td></tr>",
tfoot : "<tr><td>tfoot</td></tr>",
colgroup : "<col span='5' />",
caption : "caption",
tr : "<td>tr</td>",
th : "th",
td : "<div>td</div>",
optgroup : "<option>optgroup</option>",
option : "option"
};
SensorRnt(9791);jQuery.each(elems , function ( name, value ) {
var html = "<"+name+">"+value+"</"+name+">";
SensorRnt(9793);ok(jQuery.nodeName(jQuery.parseHTML("<"+name+">"+value+"</"+name+">")[0] , name) , name+" is created correctly");
results.push(name);
args.push(html);SensorRnt(9794);});
jQuery.fn.append.apply(jQuery("<div/>") , args).children().each(function ( i ) {
SensorRnt(9795);ok(jQuery.nodeName(this , results[i]));SensorRnt(9796);});SensorRnt(9792);});
test("Validate creation of multiple quantities of certain elements (#13818)" , 44 , function () {
var tags = [ "thead", "tbody", "tfoot", "colgroup", "col", "caption", "tr", "th", "td", "optgroup", "option"];
SensorRnt(9797);jQuery.each(tags , function ( index, tag ) {
SensorRnt(9799);jQuery("<"+tag+"/><"+tag+"/>").each(function () {
SensorRnt(9801);ok(jQuery.nodeName(this , tag) , tag+" empty elements created correctly");SensorRnt(9802);});
jQuery("<"+this+"></"+tag+"><"+tag+"></"+tag+">").each(function () {
SensorRnt(9803);ok(jQuery.nodeName(this , tag) , tag+" elements with closing tag created correctly");SensorRnt(9804);});SensorRnt(9800);});});
SensorRnt(9798);