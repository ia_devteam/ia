function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open("GET", "http://localhost:8080/"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} SensorRnt(9965);module("queue" , {
teardown : moduleTeardown
});
test("queue() with other types" , 14 , function () {
SensorRnt(9897);stop();
var $div = jQuery({

}), counter = 0;
$div.promise("foo").done(function () {
SensorRnt(9899);equal(counter , 0 , "Deferred for collection with no queue is automatically resolved");SensorRnt(9900);});
$div.queue("foo" , function () {
equal(++ counter , 1 , "Dequeuing");
jQuery.dequeue(this , "foo");}).queue("foo" , function () {
equal(++ counter , 2 , "Dequeuing");
jQuery(this).dequeue("foo");}).queue("foo" , function () {
equal(++ counter , 3 , "Dequeuing");}).queue("foo" , function () {
SensorRnt(9901);equal(++ counter , 4 , "Dequeuing");SensorRnt(9902);});
$div.promise("foo").done(function () {
SensorRnt(9903);equal(counter , 4 , "Testing previous call to dequeue in deferred");
start();SensorRnt(9904);});
equal($div.queue("foo").length , 4 , "Testing queue length");
equal($div.queue("foo" , undefined).queue("foo").length , 4 , ".queue('name',undefined) does nothing but is chainable (#5571)");
$div.dequeue("foo");
equal(counter , 3 , "Testing previous call to dequeue");
equal($div.queue("foo").length , 1 , "Testing queue length");
$div.dequeue("foo");
equal(counter , 4 , "Testing previous call to dequeue");
equal($div.queue("foo").length , 0 , "Testing queue length");
$div.dequeue("foo");
equal(counter , 4 , "Testing previous call to dequeue");
equal($div.queue("foo").length , 0 , "Testing queue length");SensorRnt(9898);});
test("queue(name) passes in the next item in the queue as a parameter" , function () {
SensorRnt(9905);expect(2);
var div = jQuery({

}), counter = 0;
div.queue("foo" , function (next) {
equal(++ counter , 1 , "Dequeueing");
next();}).queue("foo" , function (next) {
equal(++ counter , 2 , "Next was called");
next();}).queue("bar" , function () {
SensorRnt(9907);equal(++ counter , 3 , "Other queues are not triggered by next()");SensorRnt(9908);});
div.dequeue("foo");SensorRnt(9906);});
test("queue() passes in the next item in the queue as a parameter to fx queues" , function () {
SensorRnt(9909);expect(3);
stop();
var div = jQuery({

}), counter = 0;
div.queue(function (next) {
equal(++ counter , 1 , "Dequeueing");
setTimeout(function () {
next();} , 500);}).queue(function (next) {
equal(++ counter , 2 , "Next was called");
next();}).queue("bar" , function () {
SensorRnt(9911);equal(++ counter , 3 , "Other queues are not triggered by next()");SensorRnt(9912);});
jQuery.when(div.promise("fx") , div).done(function () {
SensorRnt(9913);equal(counter , 2 , "Deferreds resolved");
start();SensorRnt(9914);});SensorRnt(9910);});
test("callbacks keep their place in the queue" , function () {
SensorRnt(9915);expect(5);
stop();
var div = jQuery("<div>"), counter = 0;
div.queue(function ( next ) {
equal(++ counter , 1 , "Queue/callback order: first called");
setTimeout(next , 200);}).delay(100).queue(function ( next ) {
equal(++ counter , 2 , "Queue/callback order: second called");
jQuery(this).delay(100).queue(function ( next ) {
equal(++ counter , 4 , "Queue/callback order: fourth called");
next();});
next();}).queue(function ( next ) {
SensorRnt(9917);equal(++ counter , 3 , "Queue/callback order: third called");
next();SensorRnt(9918);});
div.promise("fx").done(function () {
SensorRnt(9919);equal(counter , 4 , "Deferreds resolved");
start();SensorRnt(9920);});SensorRnt(9916);});
test("jQuery.queue should return array while manipulating the queue" , 1 , function () {
SensorRnt(9921);var div = document.createElement("div");
ok(jQuery.isArray(jQuery.queue(div , "fx" , jQuery.noop)) , "jQuery.queue should return an array while manipulating the queue");SensorRnt(9922);});
test("delay()" , function () {
SensorRnt(9923);expect(2);
stop();
var foo = jQuery({

}), run = 0;
foo.delay(100).queue(function () {
run = 1;
SensorRnt(9925);ok(true , "The function was dequeued.");
start();SensorRnt(9926);});
equal(run , 0 , "The delay delayed the next function from running.");SensorRnt(9924);});
test("clearQueue(name) clears the queue" , function () {
SensorRnt(9927);expect(2);
stop();
var div = jQuery({

}), counter = 0;
div.queue("foo" , function ( next ) {
counter++;
jQuery(this).clearQueue("foo");
next();}).queue("foo" , function () {SensorRnt(9929);
counter++;});
div.promise("foo").done(function () {
SensorRnt(9930);ok(true , "dequeue resolves the deferred");
start();SensorRnt(9931);});
div.dequeue("foo");
equal(counter , 1 , "the queue was cleared");SensorRnt(9928);});
test("clearQueue() clears the fx queue" , function () {
SensorRnt(9932);expect(1);
var div = jQuery({

}), counter = 0;
div.queue(function ( next ) {
counter++;
var self = this;
setTimeout(function () {
jQuery(self).clearQueue();
next();} , 50);}).queue(function () {SensorRnt(9934);
counter++;});
equal(counter , 1 , "the queue was cleared");
div.removeData();SensorRnt(9933);});
asyncTest("fn.promise() - called when fx queue is empty" , 3 , function () {
SensorRnt(9935);var foo = jQuery("#foo").clone().addBack(), promised = false;
foo.queue(function ( next ) {
SensorRnt(9937);ok(! promised , "Promised hasn't been called");
setTimeout(next , 10);SensorRnt(9938);});
foo.promise().done(function () {
SensorRnt(9939);ok(promised = true , "Promised");
start();SensorRnt(9940);});SensorRnt(9936);});
asyncTest("fn.promise( \"queue\" ) - called whenever last queue function is dequeued" , 5 , function () {
SensorRnt(9941);var foo = jQuery("#foo"), test;
foo.promise("queue").done(function () {
SensorRnt(9943);strictEqual(test , undefined , "called immediately when queue was already empty");SensorRnt(9944);});
test = 1;
foo.queue("queue" , function ( next ) {
strictEqual(test++ , 1 , "step one");
setTimeout(next , 0);}).queue("queue" , function ( next ) {
strictEqual(test++ , 2 , "step two");
setTimeout(function () {
next();
strictEqual(test++ , 4 , "step four");
start();} , 10);}).promise("queue").done(function () {
SensorRnt(9945);strictEqual(test++ , 3 , "step three");SensorRnt(9946);});
foo.dequeue("queue");SensorRnt(9942);});
asyncTest("fn.promise( \"queue\" ) - waits for animation to complete before resolving" , 2 , function () {
SensorRnt(9947);var foo = jQuery("#foo"), test = 1;
foo.animate({
top : 100
} , {
duration : 1,
queue : "queue",
complete : function () {
strictEqual(test++ , 1 , "step one");}
}).dequeue("queue");
foo.promise("queue").done(function () {
SensorRnt(9949);strictEqual(test++ , 2 , "step two");
start();SensorRnt(9950);});SensorRnt(9948);});
test(".promise(obj)" , function () {
SensorRnt(9951);expect(2);
var obj = {

}, promise = jQuery("#foo").promise("promise" , obj);
ok(jQuery.isFunction(promise.promise) , ".promise(type, obj) returns a promise");
strictEqual(promise , obj , ".promise(type, obj) returns obj");SensorRnt(9952);});
SensorRnt(9966);if (jQuery.fn.stop)
{ 
SensorRnt(9967);test("delay() can be stopped" , function () {
SensorRnt(9953);expect(3);
stop();
var done = {

};
jQuery({

}).queue("alternate" , function ( next ) {
done.alt1 = true;
ok(true , "This first function was dequeued");
next();}).delay(1000 , "alternate").queue("alternate" , function () {
done.alt2 = true;
ok(true , "The function was dequeued immediately, the delay was stopped");}).dequeue("alternate").stop("alternate" , false , false).delay(1).queue(function () {
done.default1 = true;
ok(false , "This queue should never run");}).stop(true , false);
deepEqual(done , {
alt1 : true,
alt2 : true
} , "Queue ran the proper functions");
setTimeout(function () {
SensorRnt(9955);start();SensorRnt(9956);} , 1500);SensorRnt(9954);});
asyncTest("queue stop hooks" , 2 , function () {
SensorRnt(9957);var foo = jQuery("#foo");
foo.queue(function ( next, hooks ) {SensorRnt(9959);
hooks.stop = function ( gotoEnd ) {
SensorRnt(9960);equal(! ! gotoEnd , false , "Stopped without gotoEnd");SensorRnt(9961);};});
foo.stop();
foo.queue(function ( next, hooks ) {SensorRnt(9962);
hooks.stop = function ( gotoEnd ) {
SensorRnt(9963);equal(gotoEnd , true , "Stopped with gotoEnd");
start();SensorRnt(9964);};});
foo.stop(false , true);});SensorRnt(9958);
}
SensorRnt(9968);