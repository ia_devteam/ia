function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open("GET", "http://localhost:8080/"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} (function () {
SensorRnt(10229);if (! jQuery.fn.wrap)
{ 
SensorRnt(10230);return;
}
SensorRnt(10231);module("wrap" , {
teardown : moduleTeardown
});
function manipulationBareObj ( value ) {
RNTRETRET = value;
SensorRnt(10233);return RNTRETRET;}
function manipulationFunctionReturningObj ( value ) {
RNTRETRET = function () {
RNTRETRET = value;
SensorRnt(10235);return RNTRETRET;};
SensorRnt(10234);return RNTRETRET;}
function testWrap ( val ) {
SensorRnt(10236);expect(19);
var defaultText, result, j, i, cacheLength;
defaultText = "Try them out:";
result = jQuery("#first").wrap(val("<div class='red'><span></span></div>")).text();
equal(defaultText , result , "Check for wrapping of on-the-fly html");
ok(jQuery("#first").parent().parent().is(".red") , "Check if wrapper has class 'red'");
result = jQuery("#first").wrap(val(document.getElementById("empty"))).parent();
ok(result.is("ol") , "Check for element wrapping");
equal(result.text() , defaultText , "Check for element wrapping");
jQuery("#check1").on("click" , function () {
var checkbox = this;
ok(checkbox.checked , "Checkbox's state is erased after wrap() action, see #769");
jQuery(checkbox).wrap(val("<div id='c1' style='display:none;'></div>"));
ok(checkbox.checked , "Checkbox's state is erased after wrap() action, see #769");}).prop("checked" , false)[0].click();
j = jQuery("#nonnodes").contents();
j.wrap(val("<i></i>"));
equal(jQuery("#nonnodes > i").length , jQuery("#nonnodes")[0].childNodes.length , "Check node,textnode,comment wraps ok");
equal(jQuery("#nonnodes > i").text() , j.text() , "Check node,textnode,comment wraps doesn't hurt text");
cacheLength = 0;
SensorRnt(10237);for(i in jQuery.cache){ 
cacheLength++;SensorRnt(10238);
}
SensorRnt(10239);j = jQuery("<label/>").wrap(val("<li/>"));
equal(j[0].nodeName.toUpperCase() , "LABEL" , "Element is a label");
equal(j[0].parentNode.nodeName.toUpperCase() , "LI" , "Element has been wrapped");
SensorRnt(10240);for(i in jQuery.cache){ 
cacheLength--;SensorRnt(10241);
}
SensorRnt(10242);equal(cacheLength , 0 , "No memory leak in jQuery.cache (bug #7165)");
j = jQuery("<span/>").wrap("<div>test</div>");
equal(j[0].previousSibling.nodeType , 3 , "Make sure the previous node is a text element");
equal(j[0].parentNode.nodeName.toUpperCase() , "DIV" , "And that we're in the div element.");
j = jQuery("<div><span></span></div>").children().wrap("<p></p><div></div>");
equal(j[0].parentNode.parentNode.childNodes.length , 1 , "There should only be one element wrapping.");
equal(j.length , 1 , "There should only be one element (no cloning).");
equal(j[0].parentNode.nodeName.toUpperCase() , "P" , "The span should be in the paragraph.");
j = jQuery("<span/>").wrap(jQuery("<div></div>"));
equal(j[0].parentNode.nodeName.toLowerCase() , "div" , "Wrapping works.");
result = jQuery("<div></div>").on("click" , function () {
SensorRnt(10244);ok(true , "Event triggered.");
result.off();
jQuery(this).off();SensorRnt(10245);});
j = jQuery("<span/>").wrap(result);
equal(j[0].parentNode.nodeName.toLowerCase() , "div" , "Wrapping works.");
j.parent().trigger("click");SensorRnt(10243);}
test("wrap(String|Element)" , function () {SensorRnt(10246);
testWrap(manipulationBareObj);});
test("wrap(Function)" , function () {SensorRnt(10247);
testWrap(manipulationFunctionReturningObj);});
test("wrap(Function) with index (#10177)" , function () {
SensorRnt(10248);var expectedIndex = 0, targets = jQuery("#qunit-fixture p");
expect(targets.length);
targets.wrap(function (i) {
SensorRnt(10250);equal(i , expectedIndex , "Check if the provided index ("+i+") is as expected ("+expectedIndex+")");
expectedIndex++;
RNTRETRET = "<div id='wrap_index_'"+i+"'></div>";
SensorRnt(10251);return RNTRETRET;});SensorRnt(10249);});
test("wrap(String) consecutive elements (#10177)" , function () {
SensorRnt(10252);var targets = jQuery("#qunit-fixture p");
expect(targets.length* 2);
targets.wrap("<div class='wrapper'></div>");
targets.each(function () {
SensorRnt(10254);var $this = jQuery(this);
ok($this.parent().is(".wrapper") , "Check each elements parent is correct (.wrapper)");
equal($this.siblings().length , 0 , "Each element should be wrapped individually");SensorRnt(10255);});SensorRnt(10253);});
test("wrapAll(String)" , function () {
SensorRnt(10256);expect(5);
var prev, p, result;
prev = jQuery("#firstp")[0].previousSibling;
p = jQuery("#firstp,#first")[0].parentNode;
result = jQuery("#firstp,#first").wrapAll("<div class='red'><div class='tmp'></div></div>");
equal(result.parent().length , 1 , "Check for wrapping of on-the-fly html");
ok(jQuery("#first").parent().parent().is(".red") , "Check if wrapper has class 'red'");
ok(jQuery("#firstp").parent().parent().is(".red") , "Check if wrapper has class 'red'");
equal(jQuery("#first").parent().parent()[0].previousSibling , prev , "Correct Previous Sibling");
equal(jQuery("#first").parent().parent()[0].parentNode , p , "Correct Parent");SensorRnt(10257);});
test("wrapAll(Element)" , function () {
SensorRnt(10258);expect(3);
var prev, p;
prev = jQuery("#firstp")[0].previousSibling;
p = jQuery("#first")[0].parentNode;
jQuery("#firstp,#first").wrapAll(document.getElementById("empty"));
equal(jQuery("#first").parent()[0] , jQuery("#firstp").parent()[0] , "Same Parent");
equal(jQuery("#first").parent()[0].previousSibling , prev , "Correct Previous Sibling");
equal(jQuery("#first").parent()[0].parentNode , p , "Correct Parent");SensorRnt(10259);});
test("wrapInner(String)" , function () {
SensorRnt(10260);expect(6);
var num;
num = jQuery("#first").children().length;
jQuery("#first").wrapInner("<div class='red'><div id='tmp'></div></div>");
equal(jQuery("#first").children().length , 1 , "Only one child");
ok(jQuery("#first").children().is(".red") , "Verify Right Element");
equal(jQuery("#first").children().children().children().length , num , "Verify Elements Intact");
num = jQuery("#first").html("foo<div>test</div><div>test2</div>").children().length;
jQuery("#first").wrapInner("<div class='red'><div id='tmp'></div></div>");
equal(jQuery("#first").children().length , 1 , "Only one child");
ok(jQuery("#first").children().is(".red") , "Verify Right Element");
equal(jQuery("#first").children().children().children().length , num , "Verify Elements Intact");SensorRnt(10261);});
test("wrapInner(Element)" , function () {
SensorRnt(10262);expect(5);
var num, div = jQuery("<div/>");
num = jQuery("#first").children().length;
jQuery("#first").wrapInner(document.getElementById("empty"));
equal(jQuery("#first").children().length , 1 , "Only one child");
ok(jQuery("#first").children().is("#empty") , "Verify Right Element");
equal(jQuery("#first").children().children().length , num , "Verify Elements Intact");
div.wrapInner("<span></span>");
equal(div.children().length , 1 , "The contents were wrapped.");
equal(div.children()[0].nodeName.toLowerCase() , "span" , "A span was inserted.");SensorRnt(10263);});
test("wrapInner(Function) returns String" , function () {
SensorRnt(10264);expect(6);
var num, val = manipulationFunctionReturningObj;
num = jQuery("#first").children().length;
jQuery("#first").wrapInner(val("<div class='red'><div id='tmp'></div></div>"));
equal(jQuery("#first").children().length , 1 , "Only one child");
ok(jQuery("#first").children().is(".red") , "Verify Right Element");
equal(jQuery("#first").children().children().children().length , num , "Verify Elements Intact");
num = jQuery("#first").html("foo<div>test</div><div>test2</div>").children().length;
jQuery("#first").wrapInner(val("<div class='red'><div id='tmp'></div></div>"));
equal(jQuery("#first").children().length , 1 , "Only one child");
ok(jQuery("#first").children().is(".red") , "Verify Right Element");
equal(jQuery("#first").children().children().children().length , num , "Verify Elements Intact");SensorRnt(10265);});
test("wrapInner(Function) returns Element" , function () {
SensorRnt(10266);expect(5);
var num, val = manipulationFunctionReturningObj, div = jQuery("<div/>");
num = jQuery("#first").children().length;
jQuery("#first").wrapInner(val(document.getElementById("empty")));
equal(jQuery("#first").children().length , 1 , "Only one child");
ok(jQuery("#first").children().is("#empty") , "Verify Right Element");
equal(jQuery("#first").children().children().length , num , "Verify Elements Intact");
div.wrapInner(val("<span></span>"));
equal(div.children().length , 1 , "The contents were wrapped.");
equal(div.children()[0].nodeName.toLowerCase() , "span" , "A span was inserted.");SensorRnt(10267);});
test("unwrap()" , function () {
SensorRnt(10268);expect(9);
jQuery("body").append("  <div id='unwrap' style='display: none;'> <div id='unwrap1'> <span class='unwrap'>a</span> <span class='unwrap'>b</span> </div> <div id='unwrap2'> <span class='unwrap'>c</span> <span class='unwrap'>d</span> </div> <div id='unwrap3'> <b><span class='unwrap unwrap3'>e</span></b> <b><span class='unwrap unwrap3'>f</span></b> </div> </div>");
var abcd = jQuery("#unwrap1 > span, #unwrap2 > span").get(), abcdef = jQuery("#unwrap span").get();
equal(jQuery("#unwrap1 span").add("#unwrap2 span:first-child").unwrap().length , 3 , "make #unwrap1 and #unwrap2 go away");
deepEqual(jQuery("#unwrap > span").get() , abcd , "all four spans should still exist");
deepEqual(jQuery("#unwrap3 span").unwrap().get() , jQuery("#unwrap3 > span").get() , "make all b in #unwrap3 go away");
deepEqual(jQuery("#unwrap3 span").unwrap().get() , jQuery("#unwrap > span.unwrap3").get() , "make #unwrap3 go away");
deepEqual(jQuery("#unwrap").children().get() , abcdef , "#unwrap only contains 6 child spans");
deepEqual(jQuery("#unwrap > span").unwrap().get() , jQuery("body > span.unwrap").get() , "make the 6 spans become children of body");
deepEqual(jQuery("body > span.unwrap").unwrap().get() , jQuery("body > span.unwrap").get() , "can't unwrap children of body");
deepEqual(jQuery("body > span.unwrap").unwrap().get() , abcdef , "can't unwrap children of body");
deepEqual(jQuery("body > span.unwrap").get() , abcdef , "body contains 6 .unwrap child spans");
jQuery("body > span.unwrap").remove();SensorRnt(10269);});
test("jQuery(<tag>) & wrap[Inner/All]() handle unknown elems (#10667)" , function () {
SensorRnt(10270);expect(2);
var $wraptarget = jQuery("<div id='wrap-target'>Target</div>").appendTo("#qunit-fixture"), $section = jQuery("<section>").appendTo("#qunit-fixture");
$wraptarget.wrapAll("<aside style='background-color:green'></aside>");
notEqual($wraptarget.parent("aside").get(0).style.backgroundColor , "transparent" , "HTML5 elements created with wrapAll inherit styles");
notEqual($section.get(0).style.backgroundColor , "transparent" , "HTML5 elements create with jQuery( string ) inherit styles");SensorRnt(10271);});
test("wrapping scripts (#10470)" , function () {
SensorRnt(10272);expect(2);
var script = document.createElement("script");
script.text = script.textContent = "ok( !document.eval10470, 'script evaluated once' ); document.eval10470 = true;";
document.eval10470 = false;
jQuery("#qunit-fixture").empty()[0].appendChild(script);
jQuery("#qunit-fixture script").wrap("<b></b>");
strictEqual(script.parentNode , jQuery("#qunit-fixture > b")[0] , "correctly wrapped");
jQuery(script).remove();SensorRnt(10273);});SensorRnt(10232);})();
