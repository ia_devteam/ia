<?
if(!isset($_SERVER['DOCUMENT_ROOT'])){
    switch (true) {
        case preg_match('/(^.*)spo_modules/',$_SERVER['SCRIPT_FILENAME'], $laParts):
                      $lsDocumentRoot = $laParts[1];
                    break;
        case preg_match('/(^.*)resource/',$_SERVER['SCRIPT_FILENAME'], $laParts):
                     $lsDocumentRoot = $laParts[1];
                    break;
        default:
                    $lsDocumentRoot = dirname($_SERVER['SCRIPT_FILENAME']);
    }
    require_once( $lsDocumentRoot . '/spo_modules/config/document_root.php' );
}
$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
require_once ($DOCUMENT_ROOT . '/spo_modules/lib/php/GetPermission.php');

function CheckINS( $table )
{
	$perm = GetPermission( $table );
	
	if ( $perm & PERM_INS )
		return true;
	return false;
}
?>