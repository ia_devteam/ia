#!/usr/local/bin/perl

BEGIN {use Libsensor;}
Libsensor::DinGo('9,0', 0);
$a = 10;
while( $a < 20 ) {
   if( $a == 15) {
      # skip the iteration.
      $a = $a + 1;
      next;
   }
   print "value of a: $a\n";
   $a = $a + 1;
}