#!/usr/local/bin/perl
 
BEGIN {use Libsensor;}
Libsensor::DinGo('3,0', 0); 
$a = 10;
# check the boolean condition using if statement
if( $a < 20 ) {
   # if condition is true then print the following
   printf "a is less than 20\n";
}
print "value of a is : $a\n";