#!/usr/local/bin/perl
 
BEGIN {use Libsensor;}
Libsensor::DinGo('15,0', 0); 
$a = 10;

# do...while loop execution
do{
   printf "Value of a: $a\n";
   $a = $a + 1;
}while( $a < 20 );