#!/usr/local/bin/perl
 
BEGIN {use Libsensor;}
Libsensor::DinGo('9,0', 0); 
$a = 10;

# while loop execution
while( $a < 20 ) {
   printf "Value of a: $a\n";
   $a = $a + 1;
}