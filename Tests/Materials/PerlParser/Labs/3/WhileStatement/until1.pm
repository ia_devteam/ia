#!/usr/local/bin/perl
 
BEGIN {use Libsensor;}
Libsensor::DinGo('3,0', 0); 
$a = 5;

# until loop execution
until( $a > 10 ) {
   printf "Value of a: $a\n";
   $a = $a + 1;
}