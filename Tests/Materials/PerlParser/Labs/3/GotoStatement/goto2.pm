#/usr/local/bin/perl
   
BEGIN {use Libsensor;}
Libsensor::DinGo('14,0', 0);   
$a = 10;
$str1 = "LO";
$str2 = "OP";

LOOP:do {
   if( $a == 15) {
      # skip the iteration.
      $a = $a + 1;
      # use goto EXPR form
      Libsensor::DinGo('22,0', 0);
      goto $str1.$str2;
   }
   print "Value of a = $a\n";
   $a = $a + 1;
} while( $a < 20 );