#/usr/local/bin/perl
   
BEGIN {use Libsensor;}
Libsensor::DinGo('21,0', 0);   
$a = 0;
$b = 0;

# outer while loop
Libsensor::DinGo('23,0', 0);while($a < 3) {
   Libsensor::DinGo('25,0', 0);
   $b = 0;
   # inner while loop
   while( $b < 3 ) {
      Libsensor::DinGo('28,0', 0);
      print "value of a = $a, b = $b\n";
      $b = $b + 1;
   }
   $a = $a + 1;
   print "Value of a = $a\n\n";
}