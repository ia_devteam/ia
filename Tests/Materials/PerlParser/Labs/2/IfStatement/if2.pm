#!/usr/local/bin/perl
 
BEGIN {use Libsensor;}
Libsensor::DinGo('9,0', 0); 
$a = 100;
# check the boolean condition using if statement
if( $a < 20 ) {
   # if condition is true then print the following
   Libsensor::DinGo('12,0', 0);
   printf "a is less than 20\n";
} else { 
   # if condition is false then print the following
   Libsensor::DinGo('13,0', 0);
   printf "a is greater than 20\n";
}
Libsensor::DinGo('14,0', 0);
print "value of a is : $a\n";