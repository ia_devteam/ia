#!/usr/local/bin/perl
 
BEGIN {use Libsensor;}
Libsensor::DinGo('37,0', 0); 
$a = 20;
# check the boolean condition using if statement
Libsensor::DinGo('38,0', 0);unless( $a  ==  30 ) {
   # if condition is false then print the following
   Libsensor::DinGo('40,0', 0);
   printf "a has a value which is not 20\n";
} elsif( $a ==  30 ) {
   # if condition is true then print the following
   Libsensor::DinGo('42,0', 0);
   printf "a has a value which is 30\n";
} else {
   # if none of the above conditions is met
   Libsensor::DinGo('43,0', 0);
   printf "a has a value which is $a\n";
}