#!/usr/local/bin/perl
 
BEGIN {use Libsensor;}
Libsensor::DinGo('30,0', 0); 
$a = 100;
# check the boolean condition using unless statement
unless( $a == 20 ) {
   # if condition is false then print the following
   Libsensor::DinGo('33,0', 0);
   printf "given condition is false\n";
} else { 
   # if condition is true then print the following
   Libsensor::DinGo('34,0', 0);
   printf "given condition is true\n";
}
Libsensor::DinGo('35,0', 0);
print "value of a is : $a\n";