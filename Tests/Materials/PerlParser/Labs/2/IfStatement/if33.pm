#!/usr/local/bin/perl
 
BEGIN {use Libsensor;}
Libsensor::DinGo('16,0', 0); 
$a = 100;
# check the boolean condition using if statement
Libsensor::DinGo('17,0', 0);if( $a  ==  20 ) {
   # if condition is true then print the following
   Libsensor::DinGo('19,0', 0);
   printf "a has a value which is 20\n";
} elsif( $a ==  30 ) {
   # if condition is true then print the following
   Libsensor::DinGo('21,0', 0);
   printf "a has a value which is 30\n";
} else {
   # if none of the above conditions is true
   Libsensor::DinGo('22,0', 0);
   printf "a has a value which is $a\n";
}