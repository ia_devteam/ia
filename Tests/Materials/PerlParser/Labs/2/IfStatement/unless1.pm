#!/usr/local/bin/perl
 
BEGIN {use Libsensor;}
Libsensor::DinGo('24,0', 0); 
$a = 20;
# check the boolean condition using unless statement
unless( $a < 20 ) {
   # if condition is false then print the following
   Libsensor::DinGo('27,0', 0);
   printf "a is not less than 20\n";
}
Libsensor::DinGo('28,0', 0);
print "value of a is : $a\n";