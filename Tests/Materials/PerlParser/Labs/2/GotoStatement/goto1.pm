#/usr/local/bin/perl
   
BEGIN {use Libsensor;}
Libsensor::DinGo('3,0', 0);   
$a = 10;

LOOP:Libsensor::DinGo('5,0', 0);do {
   if( $a == 15) {
      # skip the iteration.
      Libsensor::DinGo('8,0', 0);
      $a = $a + 1;
      # use goto LABEL form
      Libsensor::DinGo('9,0', 0);
      goto LOOP;
   }
   print "Value of a = $a\n";
   $a = $a + 1;
} while( $a < 20 );