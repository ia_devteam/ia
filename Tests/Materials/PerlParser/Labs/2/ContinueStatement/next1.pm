#!/usr/local/bin/perl

BEGIN {use Libsensor;}
Libsensor::DinGo('9,0', 0);
$a = 10;
Libsensor::DinGo('10,0', 0);
while( $a < 20 ) {
   Libsensor::DinGo('12,0', 0);
   if( $a == 15) {
      # skip the iteration.
      Libsensor::DinGo('14,0', 0);
      $a = $a + 1;
      Libsensor::DinGo('15,0', 0);
      next;
   }
   print "value of a: $a\n";
   $a = $a + 1;
}