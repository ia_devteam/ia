#!/usr/local/bin/perl

BEGIN {use Libsensor;}
Libsensor::DinGo('3,0', 0);
$a = 10;
Libsensor::DinGo('4,0', 0);
while( $a < 20 ) {
   Libsensor::DinGo('6,0', 0);
   if( $a == 15) {
      # terminate the loop.
      Libsensor::DinGo('8,0', 0);
      $a = $a + 1;
      Libsensor::DinGo('9,0', 0);
      last;
   }
   print "value of a: $a\n";
   $a = $a + 1;
}