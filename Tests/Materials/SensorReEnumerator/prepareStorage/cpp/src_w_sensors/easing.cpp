#include "/root/sensor/user.h"
/*
Disclaimer for Robert Penner's Easing Equations license:

TERMS OF USE - EASING EQUATIONS

Open source under the BSD License.

Copyright © 2001 Robert Penner
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    * Neither the name of the author nor the names of contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtCore/qmath.h>
#include <math.h>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#ifndef M_PI_2
#define M_PI_2 (M_PI / 2)
#endif

QT_USE_NAMESPACE

/**
 * Easing equation function for a simple linear tweening, with no easing.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeNone(qreal progress)
{
    ____Din_Go ("1555038209",1);/*5u=1*/  return progress;
}

/**
 * Easing equation function for a quadratic (t^2) easing in: accelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInQuad(qreal t)
{
    ____Din_Go ("1555038210",2);/*5u=1*/  return t*t;
}

/**
* Easing equation function for a quadratic (t^2) easing out: decelerating to zero velocity.
*
* @param t		Current time (in frames or seconds).
* @return		The correct value.
*/
 static qreal easeOutQuad(qreal t)
{
    ____Din_Go ("1555038211",3);/*5u=1*/  return -t*(t-2);
}

/**
 * Easing equation function for a quadratic (t^2) easing in/out: acceleration until halfway, then deceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInOutQuad(qreal t)
{
    t*=2.0;
    ____Din_Go ("1555038212",4);/*5u=1*/  if (t < 1) {
        ____Din_Go ("1555038213",5);/*1u=2*/  return t*t/qreal(2);
    } else {
        --t;
        ____Din_Go ("1555038214",6);/*1u=2*/  return -0.5 * (t*(t-2) - 1);
    }
}

/**
 * Easing equation function for a quadratic (t^2) easing out/in: deceleration until halfway, then acceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeOutInQuad(qreal t)
{
    ____Din_Go ("1555038215",7);/*5u=1*/  if (t < 0.5) { ____Din_Go ("1555038216",8);/*2u=1*/  return easeOutQuad (t*2)/2; }
    ____Din_Go ("1555038217",9);/*1u=1*/  return easeInQuad((2*t)-1)/2 + 0.5;
}

/**
 * Easing equation function for a cubic (t^3) easing in: accelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInCubic(qreal t)
{
    ____Din_Go ("1555038218",10);/*5u=1*/  return t*t*t;
}

/**
 * Easing equation function for a cubic (t^3) easing out: decelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeOutCubic(qreal t)
{
    t-=1.0;
    ____Din_Go ("1555038219",11);/*5u=1*/  return t*t*t + 1;
}

/**
 * Easing equation function for a cubic (t^3) easing in/out: acceleration until halfway, then deceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInOutCubic(qreal t)
{
    t*=2.0;
    ____Din_Go ("1555038220",12);/*5u=1*/  if(t < 1) {
        ____Din_Go ("1555038221",13);/*1u=2*/  return 0.5*t*t*t;
    } else {
        t -= qreal(2.0);
        ____Din_Go ("1555038222",14);/*1u=2*/  return 0.5*(t*t*t + 2);
    }
}

/**
 * Easing equation function for a cubic (t^3) easing out/in: deceleration until halfway, then acceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeOutInCubic(qreal t)
{
    ____Din_Go ("1555038223",15);/*5u=1*/  if (t < 0.5) { ____Din_Go ("1555038224",16);/*2u=1*/  return easeOutCubic (2*t)/2; }
    ____Din_Go ("1555038225",17);/*1u=1*/  return easeInCubic(2*t - 1)/2 + 0.5;
}

/**
 * Easing equation function for a quartic (t^4) easing in: accelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInQuart(qreal t)
{
    ____Din_Go ("1555038226",18);/*5u=1*/  return t*t*t*t;
}

/**
 * Easing equation function for a quartic (t^4) easing out: decelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeOutQuart(qreal t)
{
    t-= qreal(1.0);
    ____Din_Go ("1555038227",19);/*5u=1*/  return - (t*t*t*t- 1);
}

/**
 * Easing equation function for a quartic (t^4) easing in/out: acceleration until halfway, then deceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInOutQuart(qreal t)
{
    t*=2;
    ____Din_Go ("1555038228",20);/*5u=1*/  if (t < 1) { ____Din_Go ("1555038229",21);/*2u=1*/  return 0.5*t*t*t*t; }
    else {
        t -= 2.0f;
        ____Din_Go ("1555038230",22);/*1u=2*/  return -0.5 * (t*t*t*t- 2);
    }
}

/**
 * Easing equation function for a quartic (t^4) easing out/in: deceleration until halfway, then acceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeOutInQuart(qreal t)
{
    ____Din_Go ("1555038231",23);/*5u=1*/  if (t < 0.5) { ____Din_Go ("1555038232",24);/*2u=1*/  return easeOutQuart (2*t)/2; }
    ____Din_Go ("1555038233",25);/*1u=1*/  return easeInQuart(2*t-1)/2 + 0.5;
}

/**
 * Easing equation function for a quintic (t^5) easing in: accelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInQuint(qreal t)
{
    ____Din_Go ("1555038234",26);/*5u=1*/  return t*t*t*t*t;
}

/**
 * Easing equation function for a quintic (t^5) easing out: decelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeOutQuint(qreal t)
{
    t-=1.0;
    ____Din_Go ("1555038235",27);/*5u=1*/  return t*t*t*t*t + 1;
}

/**
 * Easing equation function for a quintic (t^5) easing in/out: acceleration until halfway, then deceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInOutQuint(qreal t)
{
    t*=2.0;
    ____Din_Go ("1555038236",28);/*5u=1*/  if (t < 1) { ____Din_Go ("1555038237",29);/*2u=1*/  return 0.5*t*t*t*t*t; }
    else {
        t -= 2.0;
        ____Din_Go ("1555038238",30);/*1u=2*/  return 0.5*(t*t*t*t*t + 2);
    }
}

/**
 * Easing equation function for a quintic (t^5) easing out/in: deceleration until halfway, then acceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeOutInQuint(qreal t)
{
    ____Din_Go ("1555038239",31);/*5u=1*/  if (t < 0.5) { ____Din_Go ("1555038240",32);/*2u=1*/  return easeOutQuint (2*t)/2; }
    ____Din_Go ("1555038241",33);/*1u=1*/  return easeInQuint(2*t - 1)/2 + 0.5;
}

/**
 * Easing equation function for a sinusoidal (sin(t)) easing in: accelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInSine(qreal t)
{
    ____Din_Go ("1555038242",34);/*5u=1*/  return (t == 1.0) ? 1.0 : -::qCos(t * M_PI_2) + 1.0;
}

/**
 * Easing equation function for a sinusoidal (sin(t)) easing out: decelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeOutSine(qreal t)
{
    ____Din_Go ("1555038243",35);/*5u=1*/  return ::qSin(t* M_PI_2);
}

/**
 * Easing equation function for a sinusoidal (sin(t)) easing in/out: acceleration until halfway, then deceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInOutSine(qreal t)
{
    ____Din_Go ("1555038244",36);/*5u=1*/  return -0.5 * (::qCos(M_PI*t) - 1);
}

/**
 * Easing equation function for a sinusoidal (sin(t)) easing out/in: deceleration until halfway, then acceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeOutInSine(qreal t)
{
    ____Din_Go ("1555038245",37);/*5u=1*/  if (t < 0.5) { ____Din_Go ("1555038246",38);/*2u=1*/  return easeOutSine (2*t)/2; }
    ____Din_Go ("1555038247",39);/*1u=1*/  return easeInSine(2*t - 1)/2 + 0.5;
}

/**
 * Easing equation function for an exponential (2^t) easing in: accelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInExpo(qreal t)
{
    ____Din_Go ("1555038248",40);/*5u=1*/  return (t==0 || t == 1.0) ? t : ::qPow(2.0, 10 * (t - 1)) - qreal(0.001);
}

/**
 * Easing equation function for an exponential (2^t) easing out: decelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeOutExpo(qreal t)
{
    ____Din_Go ("1555038249",41);/*5u=1*/  return (t==1.0) ? 1.0 : 1.001 * (-::qPow(2.0f, -10 * t) + 1);
}

/**
 * Easing equation function for an exponential (2^t) easing in/out: acceleration until halfway, then deceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInOutExpo(qreal t)
{
    ____Din_Go ("1555038250",42);/*5u=1*/  if (t==0.0) { ____Din_Go ("1555038251",43);/*2u=1*/  return qreal(0.0); }
    if (t==1.0) { ____Din_Go ("1555038252",44);/*2u=1*/  return qreal(1.0); }
    t*=2.0;
    if (t < 1) { ____Din_Go ("1555038253",45);/*2u=1*/  return 0.5 * ::qPow(qreal(2.0), 10 * (t - 1)) - 0.0005; }
    ____Din_Go ("1555038254",46);/*1u=1*/  return 0.5 * 1.0005 * (-::qPow(qreal(2.0), -10 * (t - 1)) + 2);
}

/**
 * Easing equation function for an exponential (2^t) easing out/in: deceleration until halfway, then acceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeOutInExpo(qreal t)
{
    ____Din_Go ("1555038255",47);/*5u=1*/  if (t < 0.5) { ____Din_Go ("1555038256",48);/*2u=1*/  return easeOutExpo (2*t)/2; }
    ____Din_Go ("1555038257",49);/*1u=1*/  return easeInExpo(2*t - 1)/2 + 0.5;
}

/**
 * Easing equation function for a circular (sqrt(1-t^2)) easing in: accelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInCirc(qreal t)
{
    ____Din_Go ("1555038258",50);/*5u=1*/  return -(::sqrt(1 - t*t) - 1);
}

/**
 * Easing equation function for a circular (sqrt(1-t^2)) easing out: decelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeOutCirc(qreal t)
{
    t-= qreal(1.0);
    ____Din_Go ("1555038259",51);/*5u=1*/  return ::sqrt(1 - t* t);
}

/**
 * Easing equation function for a circular (sqrt(1-t^2)) easing in/out: acceleration until halfway, then deceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeInOutCirc(qreal t)
{
    t*=qreal(2.0);
    ____Din_Go ("1555038260",52);/*5u=1*/  if (t < 1) {
        ____Din_Go ("1555038261",53);/*1u=2*/  return -0.5 * (::sqrt(1 - t*t) - 1);
    } else {
        t -= qreal(2.0);
        ____Din_Go ("1555038262",54);/*1u=2*/  return 0.5 * (::sqrt(1 - t*t) + 1);
    }
}

/**
 * Easing equation function for a circular (sqrt(1-t^2)) easing out/in: deceleration until halfway, then acceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @return		The correct value.
 */
 static qreal easeOutInCirc(qreal t)
{
    ____Din_Go ("1555038263",55);/*5u=1*/  if (t < 0.5) { ____Din_Go ("1555038264",56);/*2u=1*/  return easeOutCirc (2*t)/2; }
    ____Din_Go ("1555038265",57);/*1u=1*/  return easeInCirc(2*t - 1)/2 + 0.5;
}

 static qreal easeInElastic_helper(qreal t, qreal b, qreal c, qreal d, qreal a, qreal p)
{
    ____Din_Go ("1555038266",58);/*5u=1*/  if (t==0) { ____Din_Go ("1555038267",59);/*2u=1*/  return b; }
    qreal t_adj = (qreal)t / (qreal)d;
    if (t_adj==1) { ____Din_Go ("1555038268",60);/*2u=1*/  return b+c; }

    qreal s;
    if(a < ::qFabs(c)) {
        a = c;
        s = p / 4.0f;
    } else {
        s = p / (2 * M_PI) * ::qAsin(c / a);
    }

    t_adj -= 1.0f;
    ____Din_Go ("1555038269",61);/*1u=1*/  return -(a*::qPow(2.0f,10*t_adj) * ::qSin( (t_adj*d-s)*(2*M_PI)/p )) + b;
}

/**
 * Easing equation function for an elastic (exponentially decaying sine wave) easing in: accelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @param a		Amplitude.
 * @param p		Period.
 * @return		The correct value.
 */
 static qreal easeInElastic(qreal t, qreal a, qreal p)
{
    ____Din_Go ("1555038270",62);/*5u=1*/  return easeInElastic_helper(t, 0, 1, 1, a, p);
}

 static qreal easeOutElastic_helper(qreal t, qreal /*b*/, qreal c, qreal /*d*/, qreal a, qreal p)
{
    ____Din_Go ("1555038271",63);/*5u=1*/  if (t==0) { ____Din_Go ("1555038272",64);/*2u=1*/  return 0; }
    if (t==1) { ____Din_Go ("1555038273",65);/*2u=1*/  return c; }

    qreal s;
    if(a < c) {
        a = c;
        s = p / 4.0f;
    } else {
        s = p / (2 * M_PI) * ::qAsin(c / a);
    }

    ____Din_Go ("1555038274",66);/*1u=1*/  return (a*::qPow(2.0f,-10*t) * ::qSin( (t-s)*(2*M_PI)/p ) + c);
}

/**
 * Easing equation function for an elastic (exponentially decaying sine wave) easing out: decelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @param a		Amplitude.
 * @param p		Period.
 * @return		The correct value.
 */
 static qreal easeOutElastic(qreal t, qreal a, qreal p)
{
    ____Din_Go ("1555038275",67);/*5u=1*/  return easeOutElastic_helper(t, 0, 1, 1, a, p);
}

/**
 * Easing equation function for an elastic (exponentially decaying sine wave) easing in/out: acceleration until halfway, then deceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @param a		Amplitude.
 * @param p		Period.
 * @return		The correct value.
 */
 static qreal easeInOutElastic(qreal t, qreal a, qreal p)
{
    ____Din_Go ("1555038276",68);/*5u=1*/  if (t==0) { ____Din_Go ("1555038277",69);/*2u=1*/  return 0.0; }
    t*=2.0;
    if (t==2) { ____Din_Go ("1555038278",70);/*2u=1*/  return 1.0; }

    qreal s;
    if(a < 1.0) {
        a = 1.0;
        s = p / 4.0f;
    } else {
        s = p / (2 * M_PI) * ::qAsin(1.0 / a);
    }

    if (t < 1) { ____Din_Go ("1555038279",71);/*2u=1*/  return -.5*(a*::qPow(2.0f,10*(t-1)) * ::qSin( (t-1-s)*(2*M_PI)/p )); }
    ____Din_Go ("1555038280",72);/*1u=1*/  return a*::qPow(2.0f,-10*(t-1)) * ::qSin( (t-1-s)*(2*M_PI)/p )*.5 + 1.0;
}

/**
 * Easing equation function for an elastic (exponentially decaying sine wave) easing out/in: deceleration until halfway, then acceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @param a		Amplitude.
 * @param p		Period.
 * @return		The correct value.
 */
 static qreal easeOutInElastic(qreal t, qreal a, qreal p)
{
    ____Din_Go ("1555038281",73);/*5u=1*/  if (t < 0.5) { ____Din_Go ("1555038282",74);/*2u=1*/  return easeOutElastic_helper(t*2, 0, 0.5, 1.0, a, p); }
    ____Din_Go ("1555038283",75);/*1u=1*/  return easeInElastic_helper(2*t - 1.0, 0.5, 0.5, 1.0, a, p);
}

/**
 * Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing in: accelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @param s		Overshoot ammount: higher s means greater overshoot (0 produces cubic easing with no overshoot, and the default value of 1.70158 produces an overshoot of 10 percent).
 * @return		The correct value.
 */
 static qreal easeInBack(qreal t, qreal s)
{
    ____Din_Go ("1555038284",76);/*5u=1*/  return t*t*((s+1)*t - s);
}

/**
 * Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing out: decelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @param s		Overshoot ammount: higher s means greater overshoot (0 produces cubic easing with no overshoot, and the default value of 1.70158 produces an overshoot of 10 percent).
 * @return		The correct value.
 */
 static qreal easeOutBack(qreal t, qreal s)
{
    t-= qreal(1.0);
    ____Din_Go ("1555038285",77);/*5u=1*/  return t*t*((s+1)*t+ s) + 1;
}

/**
 * Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing in/out: acceleration until halfway, then deceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @param s		Overshoot ammount: higher s means greater overshoot (0 produces cubic easing with no overshoot, and the default value of 1.70158 produces an overshoot of 10 percent).
 * @return		The correct value.
 */
 static qreal easeInOutBack(qreal t, qreal s)
{
    t *= 2.0;
    ____Din_Go ("1555038286",78);/*5u=1*/  if (t < 1) {
        s *= 1.525f;
        ____Din_Go ("1555038287",79);/*1u=2*/  return 0.5*(t*t*((s+1)*t - s));
    } else {
        t -= 2;
        s *= 1.525f;
        ____Din_Go ("1555038288",80);/*1u=2*/  return 0.5*(t*t*((s+1)*t+ s) + 2);
    }
}

/**
 * Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing out/in: deceleration until halfway, then acceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @param s		Overshoot ammount: higher s means greater overshoot (0 produces cubic easing with no overshoot, and the default value of 1.70158 produces an overshoot of 10 percent).
 * @return		The correct value.
 */
 static qreal easeOutInBack(qreal t, qreal s)
{
    ____Din_Go ("1555038289",81);/*5u=1*/  if (t < 0.5) { ____Din_Go ("1555038290",82);/*2u=1*/  return easeOutBack (2*t, s)/2; }
    ____Din_Go ("1555038291",83);/*1u=1*/  return easeInBack(2*t - 1, s)/2 + 0.5;
}

 static qreal easeOutBounce_helper(qreal t, qreal c, qreal a)
{
    ____Din_Go ("1555038292",84);/*5u=1*/  if (t == 1.0) { ____Din_Go ("1555038293",85);/*2u=1*/  return c; }
    if (t < (4/11.0)) {
        ____Din_Go ("1555038294",86);/*1u=2*/  return c*(7.5625*t*t);
    } else if (t < (8/11.0)) {
        t -= (6/11.0);
        ____Din_Go ("1555038295",87);/*1u=2*/  return -a * (1. - (7.5625*t*t + .75)) + c;
    } else if (t < (10/11.0)) {
        t -= (9/11.0);
        ____Din_Go ("1555038296",88);/*1u=2*/  return -a * (1. - (7.5625*t*t + .9375)) + c;
    } else {
        t -= (21/22.0);
        ____Din_Go ("1555038297",89);/*1u=2*/  return -a * (1. - (7.5625*t*t + .984375)) + c;
    }
}

/**
 * Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out: decelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @param a		Amplitude.
 * @return		The correct value.
 */
 static qreal easeOutBounce(qreal t, qreal a)
{
    ____Din_Go ("1555038298",90);/*5u=1*/  return easeOutBounce_helper(t, 1, a);
}

/**
 * Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in: accelerating from zero velocity.
 *
 * @param t		Current time (in frames or seconds).
 * @param a		Amplitude.
 * @return		The correct value.
 */
 static qreal easeInBounce(qreal t, qreal a)
{
    ____Din_Go ("1555038299",91);/*5u=1*/  return 1.0 - easeOutBounce_helper(1.0-t, 1.0, a);
}


/**
 * Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in/out: acceleration until halfway, then deceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @param a		Amplitude.
 * @return		The correct value.
 */
 static qreal easeInOutBounce(qreal t, qreal a)
{
    ____Din_Go ("1555038300",92);/*5u=1*/  if (t < 0.5) { ____Din_Go ("1555038301",93);/*2u=1*/  return easeInBounce (2*t, a)/2; }
    else { ____Din_Go ("1555038302",94);/*2u=1*/  return (t == 1.0) ? 1.0 : easeOutBounce (2*t - 1, a)/2 + 0.5; }
}

/**
 * Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out/in: deceleration until halfway, then acceleration.
 *
 * @param t		Current time (in frames or seconds).
 * @param a		Amplitude.
 * @return		The correct value.
 */
 static qreal easeOutInBounce(qreal t, qreal a)
{
    ____Din_Go ("1555038303",95);/*5u=1*/  if (t < 0.5) { ____Din_Go ("1555038304",96);/*2u=1*/  return easeOutBounce_helper(t*2, 0.5, a); }
    ____Din_Go ("1555038305",97);/*1u=1*/  return 1.0 - easeOutBounce_helper (2.0-2*t, 0.5, a);
}

 static inline qreal qt_sinProgress(qreal value)
{
    ____Din_Go ("1555038306",98);/*5u=1*/  return qSin((value * M_PI) - M_PI_2) / 2 + qreal(0.5);
}

 static inline qreal qt_smoothBeginEndMixFactor(qreal value)
{
    ____Din_Go ("1555038307",99);/*5u=1*/  return qMin(qMax(1 - value * 2 + qreal(0.3), qreal(0.0)), qreal(1.0));
}

// SmoothBegin blends Smooth and Linear Interpolation.
// Progress 0 - 0.3      : Smooth only
// Progress 0.3 - ~ 0.5  : Mix of Smooth and Linear
// Progress ~ 0.5  - 1   : Linear only

/**
 * Easing function that starts growing slowly, then increases in speed. At the end of the curve the speed will be constant.
 */
 static qreal easeInCurve(qreal t)
{
    const qreal sinProgress = qt_sinProgress(t);
    const qreal mix = qt_smoothBeginEndMixFactor(t);
    ____Din_Go ("1555038308",100);/*5u=1*/  return sinProgress * mix + t * (1 - mix);
}

/**
 * Easing function that starts growing steadily, then ends slowly. The speed will be constant at the beginning of the curve.
 */
 static qreal easeOutCurve(qreal t)
{
    const qreal sinProgress = qt_sinProgress(t);
    const qreal mix = qt_smoothBeginEndMixFactor(1 - t);
    ____Din_Go ("1555038309",101);/*5u=1*/  return sinProgress * mix + t * (1 - mix);
}

/**
 * Easing function where the value grows sinusoidally. Note that the calculated  end value will be 0 rather than 1.
 */
 static qreal easeSineCurve(qreal t)
{
    ____Din_Go ("1555038310",102);/*5u=1*/  return (qSin(((t * M_PI * 2)) - M_PI_2) + 1) / 2;
}

/**
 * Easing function where the value grows cosinusoidally. Note that the calculated start value will be 0.5 and the end value will be 0.5
 * contrary to the usual 0 to 1 easing curve.
 */
 static qreal easeCosineCurve(qreal t)
{
    ____Din_Go ("1555038311",103);/*5u=1*/  return (qCos(((t * M_PI * 2)) - M_PI_2) + 1) / 2;
}


