#include "/root/sensor/user.h"
/*
 * Copyright (c) 2013-2014 Jean Niklas L'orange. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "interval.h"

IntervalArray* interval_array_create() {
  IntervalArray *container = malloc(sizeof(IntervalArray));
  Interval *arr = malloc(32 * sizeof(Interval));
  container->len = 0;
  container->cap = 32;
  container->arr = arr;
  ____Din_Go ("1555038210",2);/*5u=1*/  return container;
}

void interval_array_destroy(IntervalArray *int_arr) {
  free(int_arr->arr);
  free(int_arr);
 ____Din_Go ("1555038211",3);/*6u=0*/ }

void interval_array_add(IntervalArray *int_arr, Interval data) {
  ____Din_Go ("1555038212",4);/*5u=1*/  if (int_arr->len == int_arr->cap) {
    int_arr->cap *= 2;
    int_arr->arr = realloc(int_arr->arr, int_arr->cap * sizeof(Interval));
  }
  int_arr->arr[int_arr->len] = data;
  int_arr->len++;
 ____Din_Go ("1555038213",5);/*4u=0*/ }

Interval interval_array_nth(IntervalArray *int_arr, uint32_t index) {
  ____Din_Go ("1555038214",6);/*5u=1*/  if (index >= int_arr->len) {
    Interval empty = {0, 0};
    ____Din_Go ("1555038215",7);/*1u=2*/  return empty;
  }
  else {
    ____Din_Go ("1555038216",8);/*1u=2*/  return int_arr->arr[index];
  }
}

void interval_array_concat(IntervalArray *left, IntervalArray *right) {
  ____Din_Go ("1555038217",9);/*5u=1*/  if (left->cap < left->len + right->len) {
    left->cap = left->len + right->len;
    left->arr = realloc(left->arr, left->cap * sizeof(Interval));
  }
  memcpy(&left->arr[left->len], &right->arr[0], right->len * sizeof(Interval));
  left->len = left->len + right->len;
 ____Din_Go ("1555038218",10);/*4u=0*/ }


uint64_t interval_to_uint64_t(Interval interval) {
  uint64_t val = (((uint64_t) interval.to) << 32) | interval.from;
  ____Din_Go ("1555038219",11);/*5u=1*/  return val;
}

Interval uint64_t_to_interval(uint64_t interval) {
  Interval val = {.to = interval >> 32, .from = interval & 0xFFFFFFFF};
  return val;
 ____Din_Go ("1555038220",12);/*6u=0*/ }

