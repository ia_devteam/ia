#!/usr/bin/perl

# perl ParserRun.pm test_input.txt 100000 PerlHandler/t/testData/ test_with_sensor_dir output_dir/output_dir 2

use 5.010;
use strict;
use warnings;

BEGIN {
	use FindBin qw($Bin);
	print "Bin = $Bin\n";
	use lib "$Bin/PerlHandler/lib/";
}

use PerlHandler;
PerlHandler::run_parse(PerlHandler::create_params_hash(@ARGV));
