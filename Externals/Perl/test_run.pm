#!/usr/bin/perl
use strict;
use warnings;

print "Run command line test";
system('perl PerlHandler/lib/PerlHandler.pm test_input.txt PerlHandler/t/testData test_with_sensor_dir output_dir/output_dir 2 "%DAT% %SID%" 100000 0 .') == 0 or die "Error in command line run";

print "Run json test";
system('perl PerlHandler/lib/PerlHandlerJson.pm jsonconfigexample.json') == 0 or die "Error in json run.";

print "Run plaintext test";
system('perl PerlHandler/lib/PerlHandlerPlain.pm plainconfigexample.txt') == 0 or die "Error in plain run.";
