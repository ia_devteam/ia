{

  # is an unnamed complex
  Classes =>
  { # sequence of Class

    # is a ClassType
    Class =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:string
      # attribute Name is required
      Name => "example",

      # is a xs:string
      # becomes an attribute
      Namespace => "example",

      # sequence of ParrentClassId, FieldId, MethodId

      # is a xs:decimal
      # occurs any number of times
      ParrentClassId => [ 3.1415, ],

      # is a xs:decimal
      # occurs any number of times
      FieldId => [ 3.1415, ],

      # is a xs:decimal
      # occurs any number of times
      MethodId => [ 3.1415, ], }, },

  # is an unnamed complex
  Variables =>
  { # sequence of Variable, Field

    # is a VariableType
    Variable =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:string
      # attribute Name is required
      Name => "example",

      # is a xs:string
      # becomes an attribute
      Namespace => "example",

      # is a xs:boolean
      # attribute isGlobal is required
      isGlobal => "true",

      # sequence of Definition, AssignmentFrom, PointsToFunctions,
      #   ValueCallPosition, ValueGetPosition, ValueSetPosition

      # is a xs:anyType
      # occurs any number of times
      Definition => [ "anything", ],

      # is a xs:anyType
      # occurs any number of times
      AssignmentFrom => [ "anything", ],

      # is a xs:anyType
      # occurs any number of times
      PointsToFunctions => [ "anything", ],

      # is a xs:anyType
      # occurs any number of times
      ValueCallPosition => [ "anything", ],

      # is a xs:anyType
      # occurs any number of times
      ValueGetPosition => [ "anything", ],

      # is a xs:anyType
      # occurs any number of times
      ValueSetPosition => [ "anything", ], },

    # is a FieldType
    Field =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:string
      # attribute Name is required
      Name => "example",

      # is a xs:string
      # becomes an attribute
      Namespace => "example",

      # is a xs:boolean
      # attribute isGlobal is required
      isGlobal => "true",

      # is a xs:boolean
      # attribute isStatic is required
      isStatic => "true",

      # is a xs:decimal
      # becomes an attribute
      InitializerId => 3.1415,

      # sequence of Definition, AssignmentFrom, PointsToFunctions,
      #   ValueCallPosition, ValueGetPosition, ValueSetPosition

      # is a xs:anyType
      # occurs any number of times
      Definition => [ "anything", ],

      # is a xs:anyType
      # occurs any number of times
      AssignmentFrom => [ "anything", ],

      # is a xs:anyType
      # occurs any number of times
      PointsToFunctions => [ "anything", ],

      # is a xs:anyType
      # occurs any number of times
      ValueCallPosition => [ "anything", ],

      # is a xs:anyType
      # occurs any number of times
      ValueGetPosition => [ "anything", ],

      # is a xs:anyType
      # occurs any number of times
      ValueSetPosition => [ "anything", ], }, },

  # is an unnamed complex
  Functions =>
  { # sequence of Function, Method

    # is a FunctionType
    Function =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:string
      # attribute Name is required
      Name => "example",

      # is a xs:string
      # becomes an attribute
      Namespace => "example",

      # sequence of Definition, Declaration, EntryStatement

      # is a xs:anyType
      # occurs any number of times
      Definition => [ "anything", ],

      # is a xs:anyType
      # occurs any number of times
      Declaration => [ "anything", ],

      # is a StatementReferenceType
      # is optional
      EntryStatement =>
      { # is a xs:anyType
        # attribute ID is required
        ID => "anything",

        # is a xs:string
        # attribute kind is required
        # Enum: STBreak STContinue STDoAndWhile STFor STForEach
        #    STGoto STIf STOperational STReturn STSwitch STThrow
        #    STTryCatchFinally STUsing STYieldReturn
        kind => "STBreak", }, },

    # is a MethodType
    Method =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:string
      # attribute Name is required
      Name => "example",

      # is a xs:string
      # becomes an attribute
      Namespace => "example",

      # is a xs:boolean
      # attribute isOverrideBase is required
      isOverrideBase => "true",

      # sequence of Definition, Declaration, EntryStatement

      # is a xs:anyType
      # occurs any number of times
      Definition => [ "anything", ],

      # is a xs:anyType
      # occurs any number of times
      Declaration => [ "anything", ],

      # is a StatementReferenceType
      # complex structure shown above
      # is optional
      EntryStatement => {ID=>1, kind => "STBreak"}, }, },

  # is an unnamed complex
  Statements =>
  { # sequence of STBreak, STContinue, STDoAndWhile, STFor,
    #   STForEach, STGoto, STIf, STOperational, STReturn, STSwitch,
    #   STThrow, STTryCatchFinally, STUsing, STYieldReturn

    # is a STBreakType
    STBreak =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # sequence of NextInLinearBlock, SensorBeforeTheStatement,
      #   FirstSymbolLocation, LastSymbolLocation

      # is a StatementReferenceType
      # is optional
      NextInLinearBlock =>
      { # is a xs:anyType
        # attribute ID is required
        ID => "anything",

        # is a xs:string
        # attribute kind is required
        # Enum: STBreak STContinue STDoAndWhile STFor STForEach
        #    STGoto STIf STOperational STReturn STSwitch STThrow
        #    STTryCatchFinally STUsing STYieldReturn
        kind => "STBreak", },

      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything",

      # sequence of breakingLoop

      # is a StatementReferenceType
      # is optional
      breakingLoop =>
      { # is a xs:anyType
        # attribute ID is required
        ID => "anything",

        # is a xs:string
        # attribute kind is required
        # Enum: STBreak STContinue STDoAndWhile STFor STForEach
        #    STGoto STIf STOperational STReturn STSwitch STThrow
        #    STTryCatchFinally STUsing STYieldReturn
        kind => "STBreak", }, },

    # is a STContinueType
    STContinue =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # sequence of NextInLinearBlock, SensorBeforeTheStatement,
      #   FirstSymbolLocation, LastSymbolLocation
      
      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything",

      # sequence of continuingLoop

      # is a StatementReferenceType
      # is optional
      continuingLoop =>
      { # is a xs:anyType
        # attribute ID is required
        ID => "anything",

        # is a xs:string
        # attribute kind is required
        # Enum: STBreak STContinue STDoAndWhile STFor STForEach
        #    STGoto STIf STOperational STReturn STSwitch STThrow
        #    STTryCatchFinally STUsing STYieldReturn
        kind => "STBreak", }, },

    # is a STDoAndWhileType
    STDoAndWhile =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:decimal
      # attribute condition is required
      condition => 3.1415,

      # is a xs:decimal
      # attribute isCheckConditionBeforeFirstRun is required
      isCheckConditionBeforeFirstRun => 3.1415,
      
      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything",

      # sequence of body

      # is a StatementReferenceType
      body =>
      { # is a xs:anyType
        # attribute ID is required
        ID => "anything",

        # is a xs:string
        # attribute kind is required
        # Enum: STBreak STContinue STDoAndWhile STFor STForEach
        #    STGoto STIf STOperational STReturn STSwitch STThrow
        #    STTryCatchFinally STUsing STYieldReturn
        kind => "STBreak", }, },

    # is a STForType
    STFor =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:decimal
      # attribute start is required
      start => 3.1415,

      # is a xs:decimal
      # attribute condition is required
      condition => 3.1415,

      # is a xs:decimal
      # attribute iteration is required
      iteration => 3.1415,
      
      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything",

      # sequence of body

      # is a StatementReferenceType
      # complex structure shown above
      body => {ID=>12, kind=>"STBreak"}, },

    # is a STForEachType
    STForEach =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:decimal
      # attribute head is required
      head => 3.1415,
      
      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything",

      # sequence of body

      # is a StatementReferenceType
      # complex structure shown above
      body => {ID=>13, kind=>"STBreak"}, },

    # is a STGotoType
    STGoto =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,
      
      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything",

      # sequence of destination

      # is a StatementReferenceType
      destination =>
      { # is a xs:anyType
        # attribute ID is required
        ID => "anything",

        # is a xs:string
        # attribute kind is required
        # Enum: STBreak STContinue STDoAndWhile STFor STForEach
        #    STGoto STIf STOperational STReturn STSwitch STThrow
        #    STTryCatchFinally STUsing STYieldReturn
        kind => "STBreak", }, },

    # is a STIfType
    STIf =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:decimal
      # attribute condition is required
      condition => 3.1415,
      
      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything",

      # sequence of then, else

      # is a StatementReferenceType
      then =>
      { # is a xs:anyType
        # attribute ID is required
        ID => "anything",

        # is a xs:string
        # attribute kind is required
        # Enum: STBreak STContinue STDoAndWhile STFor STForEach
        #    STGoto STIf STOperational STReturn STSwitch STThrow
        #    STTryCatchFinally STUsing STYieldReturn
        kind => "STBreak", },

      # is a StatementReferenceType
      # is optional
      else =>
      { # is a xs:anyType
        # attribute ID is required
        ID => "anything",

        # is a xs:string
        # attribute kind is required
        # Enum: STBreak STContinue STDoAndWhile STFor STForEach
        #    STGoto STIf STOperational STReturn STSwitch STThrow
        #    STTryCatchFinally STUsing STYieldReturn
        kind => "STBreak", }, },

    # is a STOperationalType
    STOperational =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:decimal
      # attribute operation is required
      operation => 3.1415,
      
      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything", },

    # is a STReturnType
    STReturn =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:decimal
      # attribute ReturnOperation is required
      ReturnOperation => 3.1415,
      
      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything", },

    # is a STSwitchType
    STSwitch =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:decimal
      # attribute head is required
      head => 3.1415,
      
      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything",

      # sequence of case, default

      # is an unnamed complex
      # occurs any number of times
      case =>
      [ { # is a xs:decimal
          # attribute condition is required
          condition => 3.1415,

          # sequence of block

          # is a xs:anyType
          block => "anything", }, ],

      # is an unnamed complex
      # is optional
      default =>
      { # sequence of block

        # is a xs:anyType
        block => "anything", }, },

    # is a STThrowType
    STThrow =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:decimal
      # attribute exception is required
      exception => 3.1415,
      
      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything", },

    # is a STTryCatchFinallyType
    STTryCatchFinally =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,
      
      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything",

      # sequence of tryBlock, catchBlock, finallyBlock

      # is a StatementReferenceType
      tryBlock =>
      { # is a xs:anyType
        # attribute ID is required
        ID => "anything",

        # is a xs:string
        # attribute kind is required
        # Enum: STBreak STContinue STDoAndWhile STFor STForEach
        #    STGoto STIf STOperational STReturn STSwitch STThrow
        #    STTryCatchFinally STUsing STYieldReturn
        kind => "STBreak", },

      # is a StatementReferenceType
      # occurs any number of times
      catchBlock =>
      [ { # is a xs:anyType
          # attribute ID is required
          ID => "anything",

          # is a xs:string
          # attribute kind is required
          # Enum: STBreak STContinue STDoAndWhile STFor STForEach
          #    STGoto STIf STOperational STReturn STSwitch STThrow
          #    STTryCatchFinally STUsing STYieldReturn
          kind => "STBreak", }, ],

      # is a StatementReferenceType
      # is optional
      finallyBlock =>
      { # is a xs:anyType
        # attribute ID is required
        ID => "anything",

        # is a xs:string
        # attribute kind is required
        # Enum: STBreak STContinue STDoAndWhile STFor STForEach
        #    STGoto STIf STOperational STReturn STSwitch STThrow
        #    STTryCatchFinally STUsing STYieldReturn
        kind => "STBreak", }, },

    # is a STUsingType
    STUsing =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:decimal
      # attribute head is required
      head => 3.1415,
      
      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything",

      # sequence of body

      # is a StatementReferenceType
      # complex structure shown above
      body => {ID=>14, kind=>"STBreak"}, },

    # is a STYieldReturnType
    STYieldReturn =>
    { # is a xs:decimal
      # attribute ID is required
      ID => 3.1415,

      # is a xs:decimal
      # attribute YieldReturnOperation is required
      YieldReturnOperation => 3.1415,
      
      # is a xs:anyType
      # is optional
      SensorBeforeTheStatement => "anything",

      # is a xs:anyType
      FirstSymbolLocation => "anything",

      # is a xs:anyType
      LastSymbolLocation => "anything", }, }, };
