#!/usr/bin/perl

package Libsensor;
use strict;
use warnings;
use File::Path qw(make_path);
use threads;

# Settings:
my $TEXT_MODE = 1; # Текстовый (1) / Бинарный (0) режим
my $PREFIX = $^O; # Подстановка директории (далее)
if		($PREFIX eq "linux")	{ $PREFIX = $ENV{"HOME"} . "/usr/tmp/";	}	# Linux
elsif	($PREFIX eq "MSWin32")	{ $PREFIX = "C:\\SensorLogs\\";		}	# Windows
else							{ $PREFIX = "";						}	# Other (Относительный путь)
if ($PREFIX && ! -d $PREFIX) {make_path $PREFIX;}

use Readonly;
use Exporter q'import';

our Readonly::Array @EXPORT = qw( DinGo DinGoList );
our Readonly::Array @EXPORT_OK = qw( DinGo DinGoList );
our Readonly::Hash %EXPORT_TAGS = ( all => [qw( DinGo DinGoList)] );

# BEGIN # Module requirements
# {
# 	use Exporter ();
# 	@ISA = "Exporter";
# 	@EXPORT = "DinGo";
# }

# BEGIN # Package requirements
# {
# 	use strict;
# 	use warnings;
# 	use File::Path qw(make_path);
# 	use threads;
# }

=pod
=head2 DinGo $sensId $result

Немного поменяем сенсор, чтобы можно было 
использовать с return
=cut
sub DinGo {
	my $sensId	= shift;
    my $uId = shift;
	#my $uId		= "$<_"; # real user_id, потому что по идее не должен меняться
	
	my $FILE_NAME = sprintf("%s%s_sensor_%d_%d.log", $PREFIX, $uId, $$, threads->self()->tid());
	
	open(my $File_descriptor, '>>', $FILE_NAME) or die "Can't open file '$FILE_NAME' $!";
	
	if ($TEXT_MODE) {
		print $File_descriptor sprintf("%s\n",$sensId);
	} else {
		binmode($File_descriptor);
		print $File_descriptor pack("i",$sensId);
	}
	close $File_descriptor;
	if (wantarray) {
		return @_;
	} else {
		if (scalar @_ == 1) {
			return shift;
		} else {
			#иногда должно быть не длина, а последний элемент как определить см. тест?
			#как правило это будет ошибкой
			return @_; 
		}
	}
}

sub DinGoList {
	my $sensId	= shift;
    my $uId = shift;
	#my $uId		= "$<_"; # real user_id, потому что по идее не должен меняться
	
	my $FILE_NAME = sprintf("%s%s_sensor_%d_%d.log", $PREFIX, $uId, $$, threads->self()->tid());
	
	open(my $File_descriptor, '>>', $FILE_NAME) or die "Can't open file '$FILE_NAME' $!";
	
	if ($TEXT_MODE) {
		print $File_descriptor sprintf("%s\n",$sensId);
	} else {
		binmode($File_descriptor);
		print $File_descriptor pack("i",$sensId);
	}
	close $File_descriptor;
	if (wantarray) {
		return @_;
	} else {
		if (scalar @_ == 1) {
			return shift;
		} else {
			#иногда должно быть не длина, а последний элемент как определить см. тест?
			#как правило это будет ошибкой
			return $_[$#_];
		}
	}
}


=pod
=head2 DinGoDefault $sensId $uId

Исходный вариант сенсора
=cut
sub DinGoDefault
{
	#if ($PREFIX) { make_path $PREFIX; }

	my $sensId	= shift;
	my $uId		= shift;
	
	if		(!$uId) { $uId = ""; } # Чтобы не подставлять нули в имя
	else	{ $uId .= "_"; }
	
	my $FILE_NAME = sprintf("%s%ssensor_%d_%d.log",$PREFIX,$uId,$$,threads->self()->tid());
	
	open(my $File_descriptor, '>>', $FILE_NAME) or die; # "Can't open file '$filename' $!";
	
	if ($TEXT_MODE)
	{
		print $File_descriptor sprintf("%d\n",$sensId);
	}
	else
	{
		binmode($File_descriptor);
		print $File_descriptor pack("i",$sensId);
	}
	
	close $File_descriptor;
}

END {}

1; # Don't remove this line! It's perl magic
