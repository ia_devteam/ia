@echo off
set mypath=%~dp0
rem set mypath=%mypath:~0,-1%

set where=%1
set mylist=

pushd %mypath%
for /f %%i in (filelist2copy.txt) DO call :concat %%i
echo Will copy from %mypath% to %where% files form list: "%mylist%"

robocopy  /NJS /NJH /NP /NDL /NFL /NC /NS /S /PURGE %mypath%\PerlHandler %where%\PerlHandler %mylist%
IF %ERRORLEVEL% GTR 1 exit /b 111
copy ParserRun.pm %where% > NUL
copy createPPI.pm %where% > NUL
IF %ERRORLEVEL% GTR 0 exit /b 112
robocopy /NJS /NJH /NP /NDL /NFL /NC /NS /S /PURGE %mypath%\Strawberry %where%\Strawberry
IF %ERRORLEVEL% GTR 1 exit /b 113

popd
goto :eof

:concat
set mylist=%mylist% %1
goto :eof