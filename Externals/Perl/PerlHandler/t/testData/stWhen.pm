#!/usr/bin/perl

use v5.14;
package stif::pa_name;
use strict;
use warnings;

my ($var, $abc, $def, $xyz, $nothing);

given ($var) {
	when (/^abc/) { $abc = 1 }
	when (/^def/) { $def = 1 }
	when (/^xyz/) { $xyz = 1 }
	default       { $nothing = 1 }
}

for ($var) {
	$abc = 1 when /^abc/;
	$def = 1 when /^def/;
	$xyz = 1 when /^xyz/;
	default { $nothing = 1 }
}

given ($var) {
	when (/^abc/) { $abc = 1 }
	when (/^def/) { $def = 1 }
	when (/^xyz/) { $xyz = 1 }
	default       { $nothing = 1 }
}
