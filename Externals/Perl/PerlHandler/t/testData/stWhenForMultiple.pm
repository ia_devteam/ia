#!/usr/bin/perl

use v5.14;
package stif::pa_name;
use strict;
use warnings;

my ($var, $abc, $def, $xyz, $nothing);

$var = "abcsome";

print "var = $var\n";

no warnings 'experimental::smartmatch';
given ($var) {
	when (/^abc/) {
        $abc = 1;
        print "abc match continue \n";
        #$var = "defsome";
        continue;
    }
    print "something something1\n";
	when (/^def/) { print "def1\n"; $def = 1; break; }
    print "something something2\n";
    when (/^def/) { $def = 1;  }
    print "something something3\n";
    when (/^def/) { $def = 1;  }
    print "something something4\n";
    break;
	when (/^xyz/) { $xyz = 1; break; }
    print "something something5\n";
    when (/^xyz/) { break; }
    print "something something6\n";
	default       {     print "something something7\n"; break; }
}
use warnings;

my $b = 10;
my $e = 10;
my $k = 10;

no warnings 'experimental::smartmatch';
for (qw(abc def xyz)) {
	($abc = 1 and print "applyed for abc\n" and continue) when /^abc/;
    print "check for def $_\n";
	($def = 1 and print "applyed for def\n")  when /^def/;
    print "check for xyz $_\n";
	($xyz = 1 and print "applyed for xyz\n") when /^xyz/;
	default { $nothing = 1 }
}
use warnings;

no warnings 'experimental::smartmatch';
given ($var) {
	when (/^abc/) { $abc = 1 }
	when (/^def/) { $def = 1 }
	when (/^xyz/) { $xyz = 1 }
	default       { $nothing = 1 }
}
use warnings;
