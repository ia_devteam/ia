#!/usr/bin/perl

package st_return::pa_name;

use strict;
use warnings;

sub some_function {}

sub sum {
	my ($a, $b);
	if (1 > 2) {
		return ;
	} else {
		return &some_function($a, $b);
	}
	return ($a, $b);
	return (&some_function($a), $b);
	return qw/ a b c d /;
}

&sum;
