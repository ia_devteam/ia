#!/usr/bin/perl

my $i = 0;
for (;$i<10;) {
	$i ++;
	print "$i\n";
};

my $name;
for ( ; ; ) {
    $name = tmpnam();
    sysopen( $fh, $name, O_RDWR | O_CREAT | O_EXCL ) && last;
}


# пример взят из
# 't/testData/crash11/perl_bureport_07/perl-5.16.3~643/BUILD/perl-5.16.3/t/op/sub_lval.t'
#  когда 
for my $sub (
         sub :lvalue { my $x = 72; $x },
         sub :lvalue { my $x = 72; return $x }
) {
    is scalar(&$sub), 72, "sub returning pad var in scalar context$suffix";
    is +(&$sub)[0], 72, "sub returning pad var in list context$suffix";
}

for our $_ ("bar") {
	is($_, "bar", 'for our $_');
	/(.)/;
	is($1, "b", '...m// in for our $_');
}
