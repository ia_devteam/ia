#!/usr/bin/perl

package test::declaration;

$test::declaration::testVar = 10;

sub test_f {
	my $hash = { e=> { d => 10}};

	my $a = 10;
	my ($b, $c, $d, undef, $e) = (10, 11, undef, 12, undef);
	my @k = qw (1 2 abc);
	my %l = ( a => 1, b => 2 );
	my ($n, $o, $p) = @k;

	my $r = &test_1;

	my $link = $hash->{e}->{d};

	my $declarations = declarations->test_f();

	my $link_method_call = $declarations->test_1();

	return;
}

sub test_1 {};


sub foo ($first, $, $third = 10) {
	return "first=$first, third=$third";
}

sub foo_with_proto ($left, $right) : prototype($$) {
	return $left + $right;
}

sub foo_nr ($first, $, $third = 10) {
	"first=$first, third=$third";
}

sub foo_with_proto_nr ($left, $right) : prototype($$) {
	$left + $right;
}

BEGIN {
	undef;
}
