#!/usr/bin/perl

package stif::pa_name;
use strict;
use warnings;

LABEL: for (qw(1 2 3)) {
	 print $_;
}

goto LABEL;

sub sum {}

sub sum1 {
    print 'some';
	goto &sum;
}

sub _carp {
    require Carp;
    goto &Carp::carp;
}

#усложнение для goto:
#оба goto должны найти одну и ту же метку
sub f1 {
	LABEL: for (qw(1 2 3 4 5)) {
		for my $t1 (qw(5 6 7)) {
			goto LABEL;
		}
	}
	goto LABEL;
}

sub require_version { goto &UNIVERSAL::VERSION; }


