#!/usr/bin/perl

while (my($filter,$data) = each %_mod210_restrict) {
    next unless exists $opts{$filter};
    $min = $data->{min} if $min < $data->{min};
    my %thismod;
    undef @thismod{ @{$data->{mod}} };
    foreach my $m (keys %mods_left) {
        delete $mods_left{$m} unless exists $thismod{$m};
    }
}
