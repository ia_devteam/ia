#!/usr/bin/perl
use v5.010;

foreach my $i (1 .. 3) {
    print "$i \n";
    when ($i >= 2) { print "($i * 2) \n"; continue; }
    when ($i >= 3) { print "($i * 2) \n"; continue; }
    default { print "nothing \n"; continue; }
    print "after all when \n";
}

foreach my $i (1 .. 3) {
    given (1) {
        print "second loop $i \n";
        when ($i >= 2) { print "second loop ($i * 2) \n"; continue; }
        when ($i >= 3) { print "second loop ($i * 2) \n"; continue; }
        default { print "second loop nothing \n"; continue; }
        print "second loop after all when \n";
    }
}

# foreach
#     body
#     print ?
#     case () {}
#     case () {}
#     default () {}
#     print ?


