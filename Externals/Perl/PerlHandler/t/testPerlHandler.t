#!/usr/bin/perl

BEGIN {
	unless (caller) {
		use FindBin qw($Bin);
		use lib "$Bin/../lib/";
	}
}

use Carp 'verbose';
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use PPI;
use PerlHandler;
use PerlHandler qw(parse_file);
use PerlHandler::Utils;
use Test::Simple tests => 2;
use Data::Dumper;

our $templateFileName = "testStructure.pl";
our $templateFileNameBackup = "testStructureBackup.pl";

#test 1. Генерируем perl шаблон и генерируем по нему xml
#перегенерируем шаблон
# &PerlXsd::parse_xsd("../../Sources/ParserToStorageData.xsd", 
# 					$templateFileName,
# 					"PERL");
# &PerlHandler::Utils::fix_generated_template($templateFileName);

# open my $struct_file, "<", $templateFileName or die $!;
# my $Database;
# {
# 	local $/;    # slurp mode аналогично $/, позволяет считать весь файл
# 	$Database = eval <$struct_file>;
# }
# close $struct_file;

# ok(&PerlHandler::write_xml($Database) == PerlHandler::defaultOutput, 
#    "Test write xml by template"); 

### test 2. Проверяем генерацию минимальной структуры для одного файла
###         и генерацию по нему xml

my $file = "t/testData/RemoveTest.pm";            
my $t3Document = PPI::Document->new($file);

sub crt_ppi_example {
	my $ppiExampleFileName = 'ppiExample.pl';
	open my $struct_file, ">", $ppiExampleFileName or die $!;
	print $struct_file Dumper $t3Document;
	close $struct_file;
}
&crt_ppi_example();

print "t3Document created\n";
 
use PerlHandler::StructGenerator;
use PerlHandler::TreeGenerator;

my $treeGenerator = PerlHandler::TreeGenerator->new([[0, "t/testData/RemoveTest.pm"]]);
print "struct generator created\n";

$treeGenerator->generate();

my $structGenerator = PerlHandler::StructGenerator->new($treeGenerator);
print "struct generator created\n";

$structGenerator->gen_structure();

print "doc handled\n";

my $struct = $structGenerator->get_structure();

print "Struct created\n";
#print Dumper $structGenerator;

my $output_file_name = &PerlHandler::write_xml($struct, "struct_simple.xml");

print "file_name = $file_name\n";

ok($output_file_name == "struct_simple.xml", "Check gen and write simple struct");

