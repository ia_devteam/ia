#!/usr/bin/perl

#TEST FOR File **exceptions.pl** that has undocumented syntax &'throw
#         and meet often.

BEGIN {
	unless (caller) {
        use utf8;
		use FindBin qw($Bin);
        use lib "$Bin/../";
		use lib "$Bin/../lib/";
        use lib "$Bin/lib/";
	}
}


use Test::Simple tests => 1;
use t::lib::Helper;

use Data::Dumper;

my $testName = "testCrashException";
my $fileList = ['t/testData/exceptions.pl'];

my $count = 0;
my $tempFileList = [];
my $errorFiles = [];

while ($count < scalar(@$fileList)) {
    my $tempFileList = [$fileList->[$count]];
    eval {
        my $testHelper = Helper->new($testName, $tempFileList);
        my $xmlFileName = $testHelper->gen_file_list_xml();
    };

    if ( $@ ) {
        print "ERRRRROR: $@ $fileList->[$count]\n";
        push @$errorFiles, $fileList->[$count];
    }
    $count ++;
}

print "Files with errors: @$errorFiles \n";

ok (1 == 1, 'Test finished');

1;
