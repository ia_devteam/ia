#!/usr/bin/perl

BEGIN {
	unshift @INC, "..";
}
use Carp 'verbose';
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use 5.010;
use strict;
use warnings;

use Test::Simple tests => 1;
use Data::Dumper;

use PerlHandler::TreeGenerator;
=pod
=head2 Проверяем работе генератора на небольшом файле
=cut

my $listOfFiles = [[0, 't/testData/stGoto.pm']];
my $treeGenerator = PerlHandler::TreeGenerator->new($listOfFiles);
$treeGenerator->generate();

ok(1 == 1, "Generate Executed");
1;
