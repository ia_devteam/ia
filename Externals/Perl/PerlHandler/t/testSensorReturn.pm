#!/usr/bin/perl

=pod
=head1 Данный файл является примером обработки передачи return в сенсор

Описание проблемы:

Задача:

 - Нужно вставить датчик для return.

B<Идея 1>:

 - Вставляем датчик перед return

Например:

C<< retrun $a; >> ->  C<< DinGo(1); return $a; >>

Вставлять удобно, но для случая:

C<< retrun f(g($x)); >> -> C<< DinGo(1);  return f(g($x)); >>

Получим, что сенсор 1 вызовется до сенсоров в функциях B<f>, B<g>.

B<Идея 2>:

Нужно результат f(g($x)) куда-то сохранить вызвать сенсор потом вернуть результат.

Тут возможны две реализации:

1. вставка кода для return

C<< return f(g($x)); >>

Станет:

C<<   if (wantarray) {             >>
C<<       my @result = f(g($x))    >>
C<<       DinGo(1);                >>
C<<       return @result;          >>
C<<   } else {                     >>
C<<       my $result = f(g($x));   >>
C<<       DinGo(1);                >>
C<<       return $result;          >>
C<<   }                            >>

2. Передача выражения return как параметра в сенсор и возврат результат сенсора.
   Сенсор при этом должен будет выполнить запись датчика и вернуть переданные параметры как есть:

C<<  return f(g($x)); >>

B <Проблема данной реализации>:

При реализации варианта 1 идеи 2 проблема будет на выражениях например таких:

C << return f(g(x)) if "some"; >>

Для выполнения корректного преобразования придется разбирать кода и в
зависимости от действия выполнять разные преобразования, что чревато ошибками.

Станет:

C<<   return DinGo(1, f(g($x)); >>

B<В данном случае имеем следующую проблему>:

I<немного теории>.

В perl любая переменная может быть скаляром или массивом. Собственно можно
считать, что есть два контекста скаляр и массив(список, list).  В зависимости от
контекста одна и та же функция или одна и таже операция дает разные результаты.
Определить контекст можно вызовом wantarray
http://perldoc.perl.org/functions/wantarray.html, которая если контекст спиcок
вернет true.  Согласно: http://citforum.ru/internet/perl_tut/dat.shtml

B<Контекст.>

Большое значение для правильного употребления встроенных функций имеет контекст
использования результата этих функций, т.к. в противном случае они возвращают
совершенно "непонятный" результат. В Perl имеется два главных контекста:
скалярный и список (list). Проще говоря, если в левой части выражения имеется
ввиду одно единственное значение, то это скалярный контекст. Если множество
значений - список.

Пример:

	$var1 = <>;	# Прочитать одну строку файла

	@var1 = <>;	# Прочитать все строки файла в массив @var1

	$var1 = (1,2,3); # $var = 3 - количество элементов

	@var1 = (1,2,3); # Создание массива @var1 с элементами 1,2,3


Проблема заключается в том, что $var1 = (1,2,3); # $var = 3 - количество
элементов на практике возвращает не количество элементов, последний элемент.

можно выполнить такой скрипт:

C<< perl -e 'my $var = (1,2,5); print "$var\n"; >>

И выводится не 3, а 5.

При этом:

C<< perl -e 'my @var1 = (1, 2, 5);my $var = @var1; print "$var\n";'>>

Выведет  3.

Суть в том, что списочные выражения (), qw, @a[1 .. 3] в скалярном контексте
возвращают последний элемент, а массивы которые не являются списочным выражением
возвращают длину.

Достаточно хорошее описание есть тут:

B<< http://www.perlmonks.org/?node_id=72247 >>

При передаче выражения как параметры в функцию DinGo выражение вычисляется
в списочном контексте, и сохраняется в массив @_, что приводит к возможному
расхождению с оригинальным результатом.

B<Частичное решение>.

Соответственно в качестве решения можно использовать следующее:

B<< Сделать специальную обработку оператора B<()> и аналогичных >>.

Сейчас имеется следующий список операторов: (); qw! !; qw/ /; qw# #; qw( );
qw{}; qw[ ]; qw< >, @var[1 .. 3];

Будем называть их B<оператор списка>.

Специальная обработка может быть выполнена тремя способами:

1. Если обнаружен оператор списка, вызывать специальный сенсор.
2. Если обнаружен оператор списка, вставлять сенсор перед return.
3. Если обнаружен оператор списка, делать return с использованием if.

B<Проблемы остаются>:
Проблемы всех этих способов в том, что нужно определять операторы списков, что нельзя сделать наверняка.
Нет гарантии того возврат 1 элемента списка эта правильный результат.

B<Идея 3>:

Так как проблема возникает только в случае вычисления в скалярном контексте
и вычисление выражения в return в скалярном контексте даст корректный результат,
нужно перед передачей результата в функцию DinGo вычислить выражение в скалярном контексте.
Для этого есть функция scalar.

B<Идея реализации 1>, в начале функции заводим функцию, которая в случае если функция вызывается в скалярном
контексте является функцией scalar, в противном случае функцией которая возвращает свои параметры как есть.

B<Проблема>:

Завести переменную функцию можно только через ссылку.
Для функции scalar ссылка заводится как my $scalar_link = \&*CORE::scalar;
Но при вызове этой функции как &{$scalar_link} (1, 2, 3, 4, 5) Получаем ошибку о большом количестве параметров,при это scalar (1, 2, 3, 4, 5) вычисляется корректно и возвращает 5.

B<Идея реализации 2>. Воспользоваться выражением eval

В начале функции завести переменную $scalar_function, в которую положить либо scalar либо пусто.
Выражения return заменить на выражения eval $scalar_function . expression.

B<Пример>:

C<<return f(g($x));>> ->
    C<<my $scalar_function = !wantarray ? 'scalar' : ''; return eval DinGo(1, $scalar_function . 'f(g($x))');


Еще ссылки по данной теме:

* http://stackoverflow.com/questions/10031455/using-my-with-parentheses-and-only-one-variable
* http://stackoverflow.com/questions/9307137/list-assignment-in-scalar-context
* http://www.perlmonks.org/?node_id=72247

=cut

BEGIN {
	unless (caller) {
		use FindBin qw($Bin);
		use lib "$Bin/../lib/";
		use lib "$Bin/../../"; #место сенсора
	}
}

sub DinGo {
	my $sensId	= shift;
	#print "DinGo args:  @_ \n";

    # if (wantarray) {
    #     print 'wantarray' . "\n";
    # } elsif (!defined(wantarray)) {
    #     print 'wantarray undef'. "\n";
    # } else {
    #     print 'wantscalar' . "\n";
    # }

	if (wantarray) {
		return @_;
	} else {
		if (scalar @_ == 1) {
			return shift;
		} else {
			#иногда должно быть не длина а последний элемент как определить ?
			#как правило это будет ошибкой
			return @_;
		}
	}
}

sub DinGoList {
	my $sensId	= shift;
	#print "DinGoList args:  @_ \n";

    # if (wantarray) {
    #     print 'DinGoList wantarray' . "\n";
    # } elsif (!defined(wantarray)) {
    #     print 'DinGoList wantarray undef'. "\n";
    # } else {
    #     print 'DinGoList wantscalar' . "\n";
    # }
    
	if (wantarray) {
		return @_;
	} else {
		if (scalar @_ == 1) {
			return shift;
		} else {
			#иногда должно быть не длина а последний элемент как определить ?
			#как правило это будет ошибкой
			return $_[-1];
		}
	}
}

use Test::Simple tests => 19;
#use warnings;
#use strict;


############### Check scalars  ##################
sub returnS {
	return 2;
}

sub returnSD {
	return DinGo(1, 2);
}

print "\n", '###############################',"\n",'';
print 'Для выражений которые являются скалярами все хорошо', "\n";
ok (returnS == returnSD, "1. Check scalar " . returnS . '= '. returnSD);
ok (returnSD == 2, "2. Check scalar ". returnS . '= '. returnSD);

############### Check arrays  ##################
print "\n", '###############################',"\n",'';
print 'Сенсор со списочным выражением.', "\n";
print "\n";
print 'scalar (2, 3, 4, 5, 10) = ', scalar(2, 3, 4, 5, 10), "\n";

sub returnA {
	return (2, 3, 4, 5, 10);
}

sub returnAD {
	return DinGo(1, (2, 3, 4, 5, 10));
}

sub returnADL {
	return DinGoList(1, (2, 3, 4, 5, 10));
}

sub returnAA {
	my @var = (2, 3, 4, 5, 10);
	return @var;
}

sub returnAAD {
	my @var = (2, 3, 4, 5, 10);
	return DinGo(1, @var);
}

ok (returnA != returnAD, "3. Check array ". returnA . '!= '. returnAD);
ok (returnAD != (2, 3, 4, 5, 10), "4. Check array " . returnA . '!= '. returnAD); # <-- предупрежедение

ok (returnAA == returnAAD, "5. Check if params not list by array: " . returnAA . ' == ' . returnAAD);
ok (returnAAD != (2, 3, 4, 5, 10), "6. Check array and list expression " . returnAAD . ' != ' . (2, 3, 4, 5, 10));

#массив в скалярном контексте
my $s = returnA();
my $s1 = returnAD();
ok($s != $s1, "7. Check equal array to scalar by using vars: " . $s . ' != ' . $s1);

#массив в скалярном контексте
my @a = returnA();
my @a1 = returnAD();
ok(@a == @a1, "8. Check equal arrays by using vars: " . @a . ' = ' . @a1);

my $sa = returnAA();
my $sa1 = returnAAD();
ok($sa == $sa1, "9. Check equal array to scalar" . $sa . ' = ' . $sa1);

print "\n";
print "Как видим вызов сенсора со списочным выражением \n";
print "не эквивалентно списочному выражению.";

print "\n\n", '###############################',"\n",'';
print 'Проверяем вариант 3 с eval.', "\n";

print "\n";
print "Проверим возможность вызова scalar по ссылки: \n";
my $temp =\&{*CORE::scalar};
print "scalar link = $temp \n";
print "scalar by link:", 'eval { &{$scalar_function} (1, 2, 3, 4, 5); }; warn $@ if $@;', " \n";
eval { &{$temp}(1, 2, 3, 4, 5); }; warn $@ if $@;
print "Как видим по ссылки вызвать не получается\n";

print "\n";
print "Проверим вызов в качестве строки:\n";
print 'scalar (1, 2, 3, 4, 8) = ', eval 'scalar' . '(1, 2, 3, 4, 8)'; warn $@ if $@;
print "\n";
print "Вычисление прошло успешно.\n";

print "\n";
print "Проведем тесты:\n";

sub returnAFixed {
	return (2, 3, 4, 5, 10);
}

sub returnADFixed {
    my $f = !wantarray ? 'scalar' : '';
	return DinGo(1, eval $f. '(2, 3, 4, 5, 10)');
}

print "\n";
print "returnAFixed: ", returnAFixed(), "\n";
print "returnADFixed: ", returnADFixed(), "\n";
print 'list (2, 3, 4, 5, 10): ', (2, 3, 4, 5, 10), "\n";
print "\n";

sub returnADLFixed {
    my $f = !wantarray ? 'scalar' : '';
	return DinGoList(1, eval $f . '(2, 3, 4, 5, 10)');
}

sub returnAAFixed {
	my @var = (2, 3, 4, 5, 10);
	return @var;
}

sub returnAADFixed {
    my $f = !wantarray ? 'scalar' : '';
	my @var = (2, 3, 4, 5, 10);
	return DinGo(1, eval $f . '@var');
}

ok (returnAFixed == returnADFixed, "10. Check array ". returnAFixed . '= '. returnADFixed);
ok (returnADFixed == (2, 3, 4, 5, 10), "11. Check array " . returnAFixed . '= '. 10); # <-- предупрежедение

ok (returnAFixed == returnADLFixed, "12. Check array ". returnAFixed . '= '. returnADLFixed);
ok (returnADLFixed == (2, 3, 4, 5, 10), "13. Check array " . returnAFixed . '= '. 10);

#массив в скалярном контексте
my $sf = returnAFixed();
my $s1f = returnADFixed();
ok($sf == $s1f, "14. Check equal array to scalar by using vars: " . $sf . ' = ' . $s1f);

#массив в скалярном контексте
my @af = returnAFixed();
my @a1f = returnADFixed();
ok(@af == @a1f, "15. Check equal arrays by using vars: " . @af . ' = ' . @a1f);

my $saf = returnAAFixed();
my $sa1f = returnAADFixed();
ok($saf == $sa1f, "16. Check equal array to scalar" . $saf . ' = ' . $sa1f);


print "\nКак видно результаты с eval совпадают.\n";
print "Причем для обоих вариантов сенсора, потому что\n";
print "В случае скалярного контекста в сенсор передается \n";
print "уже преобразованное к скаляру выражение.\n";

print "\n##################################\n";
print "Проверка передачи контекста внутрь вызовов:\n";

sub returnA1 {
	return (2, 3, 4, 5, 10);
}

sub returnA2 {
	if (wantarray) {
		print "Array context. \n";
	} elsif (!defined(wantarray)) {
		print "Nothing context. \n";
	} else {
        print "Scalar context. \n";
    }
	return &returnA1;
}

print "Array context:\n ";
my @acr = &returnA2();
print "acr = @acr \n";

print "\nScalar context: \n";
my $acr1 = &returnA2();
print "acr1 = $acr1 \n";

print "\n",'#####################' . "\n\n";
print "Проверка контекста для разных вызовов: \n\n";

sub test_context {
    if (wantarray) {
        print '\'list\' context.' . "\n";
    } elsif (!defined(wantarray)) {
        print '\'undef\' context. '. "\n";
    } else {
        print '\'scalar\' context.' . "\n";
    }
    return;
}

sub test_caller {};

print "1. in call: f(g()) in g: ";
&test_caller(&test_context());

print '2. in my $var = f() in g:  ';
my $result = &test_context();

print "3. in scalar f() in f:  ";
my @result = scalar(&test_context());

print "4. in call: f(scalar (g()) in g: ";
my @result_array = &test_caller(scalar(&test_context()));

print '5. in call with asignment: my $var = f(scalar(g()) in g:  ';
my $result_array = &test_caller(scalar(&test_context()));


print "\n",'#####################' . "\n\n";
print "Значения списочных выражений в скалярном контексте: \n\n";

print "6. list in scolar context return len-1 elem check: \n";
ok(13 == scalar((1, 2, 3, 4, 6, (7, 8, 13))), "17. 13 = scalar((1, 2, 3, 4, 6, (7, 8, 13)).");

print "7. check array slice in scalar context return len - 1 elem on slice \n";
my @av = (5, 6, 8);
my $v = @av[1 .. 2];
ok($v == 8, '18. 8 = $v where $v = @av[1 .. 2] where @av = (5, 6, 7).');
ok(8 == scalar(@av[1 .. 2]), '19. 8 =  scalar(@av[1 .. 2]) where @av = (5, 6, 7).');

print "\n########################### \n";
