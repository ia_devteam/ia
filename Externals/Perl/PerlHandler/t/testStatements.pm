#!/usr/bin/perl

BEGIN {
	unless (caller) {
		use FindBin qw($Bin);
		use lib "$Bin/../lib/";
	}
}
use Carp 'verbose';
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use 5.010;
use strict;
use warnings;

use Test::Simple tests => 1;

use PerlHandler::TreeGenerator;
use PerlHandler::TestHelper;
use PerlHandler::Utils qw(get_file_data);

use JSON;	

sub check_parse_ok {
	my ($stFile, $stCheckJson) = @_;
	my $treegenerator = PerlHandler::TreeGenerator->new([[0, $stFile]])->generate();

	#нужно вывести ppi, xml, struct, json в testData
    $treegenerator->create_dump("t/testData/testOutput");
	$treegenerator->create_ppi("t/testData/testOutput");
	$treegenerator->create_xml("t/testData/testOutput");
	$treegenerator->create_struct("t/testData/testOutput");
	
	my $checkData = decode_json(get_file_data($stCheckJson));
	my $checker = PerlHandler::TestHelper->new($treegenerator, $checkData);
	my $check = $checker->check();
	#ok($check, "Check for $stFile, $stCheckJson ok!");
	return $check;
}

sub check {
	my ($stFile, $stCheckJson) = @_;
	my $check = &check_parse_ok(@_);
    print "check = $check\n";
	ok($check == 1, "Check for $stFile, $stCheckJson ok!");
	return $check;
}

&check("t/testData/stOperational.pm", "t/testData/stOperational.json");

1;
