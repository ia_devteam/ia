#!/usr/bin/perl


BEGIN {
	unless (caller) {
		use FindBin qw($Bin);
		use lib "$Bin/../lib/";
	}
}

use Carp 'verbose';
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use PPI;
use PerlHandler;
use PerlHandler qw(parse_file);
use PerlHandler::Utils;
use Test::Simple tests => 2;
use Data::Dumper;

print "t3Document created\n";
 
use PerlHandler::StructGenerator;
use PerlHandler::TreeGenerator;

my $treeGenerator = PerlHandler::TreeGenerator->new([[0, $ARGV[0]]]);
print "struct generator created\n";

$treeGenerator->generate();

my $structGenerator = PerlHandler::StructGenerator->new($treeGenerator);
print "struct generator created\n";

$structGenerator->gen_structure();

print "doc handled\n";

my $struct = $structGenerator->get_structure();

print "Struct created\n";
#print Dumper $structGenerator;

my $output_file_name = &PerlHandler::write_xml($struct, "struct_simple.xml");

print "file_name = $file_name\n";

$treeGenerator->print_variables();
$treeGenerator->print_subs();

ok($output_file_name == "struct_simple.xml", "Check gen and write simple struct");

