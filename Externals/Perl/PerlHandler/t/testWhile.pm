#!/usr/bin/perl

BEGIN {
	unless (caller) {
		use FindBin qw($Bin);
        use lib "$Bin/../";
		use lib "$Bin/../lib/";
        use lib "$Bin/lib/";
	}
}


use Test::Simple tests => 1;
use t::lib::Helper;

my $testName = "testWhile";
my $fileList = [
    't/testData/stWhileVariables.pm'
    ];

my $testHelper = Helper->new($testName, $fileList);
my $xmlFileName = $testHelper->gen_file_list_xml();
ok($xmlFileName == 'simple_multiple.xml', 'Test parse while expression.');

1;
