#!/usr/bin/perl

package Walker;

use File::Basename;
use PPI;
use v5.10;
use strict;
use warnings;
use Data::Dumper;
use IO::File;

BEGIN {
	unshift @INC, ".";
}

sub new {
    my ($cls, $dir_func, $file_func) = @_;
    my $class = ref $cls || $cls;

    my $self = bless {
        dir_func => $dir_func,
        file_func => $file_func,
        result => [],
        need_finish => 0
    }, $class;

    return $self;
}

sub walk {
    my ($self, $top) = @_;
    $self->{need_finish} = 0;
    $self->{result} = [];
    $self->dir_walk($top);
    return $self->{result};
}

sub dir_walk {
  my ($self, $top) = @_;
  my $DIR;

  if (-d $top) {
    my $file;
    unless (opendir $DIR, $top) {
      warn "Couldn't open directory code: $!; skipping.\n";
      return;
    }

	sub fp {
		my $rl;
		my $top = $_[1];
		$rl = "$top/$_[0]";
		# my $isf = -f $rl;
		# my $nisf = not(-f $rl);
		#print "rl = $rl is f: $isf nisf: $nisf \n";
		return $rl;
	}

    #почему 1 идет в конец списка ?
	my @files = reverse sort {
		($a eq '.' || $a eq '..') ? 1 :
		($b eq '.' || $b eq '..') ? -1 :
		((-f fp($a, $top)) && (not (-f fp($b, $top)))) ? 1 :
		((not (-f fp($a, $top))) && (-f fp($b, $top))) ? -1 :
		$a cmp $b } readdir($DIR);

	#print " $top: \n@files\n";
	while (($file = shift @files) && ($self->{need_finish} == 0)) {
        next if $file eq '.' || $file eq '..';
        $self->dir_walk("$top/$file");
    }
    return $self->{dir_func}->($self, $top);

  } else {

	  my $result = $self->{file_func}->($self, $top);
	  if ($result eq "first_handled") {
		  $self->{need_finish} = 1;
	  }
  }
}

1;
