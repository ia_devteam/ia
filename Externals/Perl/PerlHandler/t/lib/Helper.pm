#!/usr/bin/perl

package Helper;

BEGIN {
	unless (caller) {
		use FindBin qw($Bin);
		use lib "$Bin/../lib/";
        use lib "$Bin/lib/";
	}
}

use Carp 'verbose';
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use PPI;
#use Test::Simple tests => 1;
use Data::Dumper;

use File::Spec;
use File::Copy;
use File::Basename;

use PerlHandler::PerlHandlerLogger;
our $log = $PerlHandler::PerlHandlerLogger::logger;
$log->init_logger();

use PerlHandler;
use PerlHandler qw(parse_file);
use PerlHandler::StructGenerator;
use PerlHandler::TreeGenerator;
use PerlHandler::Utils qw(files_equal create_ppi);

use t::lib::Walker;

=pod
=head2 gen_dir_file_list dir

Получить список perl файлов из каталога
=cut
sub gen_file_list_from_dir {
    my ($cls, $dir) = @_;
    my $class = ref $cls || $cls;
    my $fileList;

    my $dir_func = sub {};
    my $file_func = sub {
        my ($self, $file) = @_;
        my($filename, $dirs, $suffix) = fileparse($file, qr/\.[^.]*/);
        my $usuf = uc($suffix);
        if ($usuf eq '.PM' || $usuf eq '.PL' || $usuf eq '.T') {
            #print 'file = ', $file, "\n";
            push @{$self->{result}}, $file;
        }
    };

    my $walker = Walker->new($dir_func, $file_func);
    $fileList = $walker->walk($dir);
    return $fileList;
}

=pod
=head1 Вспомогательный модуль для выполнения тестирования,
       Параметрами являются имя теста и набор файлов на которых проводится тестирование
       Файлы копируются в каталог теста, после чего выполняется разбор.
       Проверка разбора на данном этапе проводится в ручную.
=cut

=pod
=head2 new $cls $testName $fileList $params

       Create new test helper class.
=cut
sub new {
    my ($cls, $testName, $fileList, $params) = @_;
    my $class = ref $cls || $cls;

    my $testDataFolder = File::Spec->catfile("t", "testData", $testName);
    my $outputFolder = File::Spec->catfile("t", "testData", $testName, "output");
    my $withSensorsFolder = File::Spec->catfile("t", "testData", $testName, "withSensors");

    my $self = bless {
        testName => $testName,
        fileList => $fileList,
        testDataFolder => $testDataFolder,
        outputFolder => $outputFolder,
        withSensorsFolder => $withSensorsFolder
    }, $class;

    return $self;
}

=pod
=head2 move_perl_files_to_test_data

       копирует файлы которые будут использоваться для тестирования
       в каталог с файлами для тестирования
=cut
sub move_perl_files_to_test_data {
	my $self = shift;

    my $testDataFolder = $self->{testDataFolder};
    my $outputFolder = $self->{outputFolder};
    my $withSensorsFolder = $self->{withSensorsFolder};
    my $fileList = $self->{fileList};
	my $testFileName = "";
	my @listOfLists = ();
	my $num = 0;

	for my $name (@$fileList) {
		my ($fname,$fpath,$fsuffix) = fileparse($name, qr/\.[^.]*/);
		my $new_file_name = $fname . $fsuffix;
		print "new_file_name = ", $new_file_name, "\n";
        if (! -d $testDataFolder) {
            File::Path::make_path($testDataFolder);
        }
		$testFileName = File::Spec->catfile($testDataFolder, $new_file_name);
		if (! files_equal($name, $testFileName)) {
			copy($name, $testFileName);
		}
		push @listOfLists, [$num, $testFileName];
		$num += 1;
	}
    if (! -d $outputFolder) {
        File::Path::make_path($outputFolder);
    }
    if (! -d $withSensorsFolder) {
        print "withSensorsFolder = $withSensorsFolder \n";
        File::Path::make_path($withSensorsFolder);
    }
	return \@listOfLists;
}

=pod
=head2 write_struct_to_file $self $struct

       Записать структуру полученную в результате разбора в файл.
=cut
sub write_struct_to_file {
	my ($self, $struct) = @_;
    my $outputFolder = $self->{outputFolder};
    my $structFileName = File::Spec->catfile($outputFolder, "testStructureGen.pl");
	open $outputStruct, ">", $structFileName;
	print $outputStruct Dumper $struct;
	close $outputStruct;
    return $self;
}

=pod
=head2 create_ppi_dumps $self $fileList
       Create ppi dumps files for each input files.
=cut
sub write_ppi_dumps {
    my ($self, $fileList) = @_;
    my $testDataFolder = $self->{testDataFolder};
    my $testName = $self->{testName};
    my $fileName = undef;
    my $ppiFileName = undef;
    for my $filePair (@$fileList) {
        $fileName = $filePair->[1];
        $ppiFileName = $self->create_ppi_name($fileName);
        print "fileName = $fileName, ppiFileName = $ppiFileName\n";
        create_ppi($fileName, $ppiFileName);
    }
    return $self;
}

sub create_ppi_name {
    my ($self, $fileName) = @_;
    my ($fname,$fpath,$fsuffix) = fileparse($fileName, qr/\.[^.]*/);
    return File::Spec->catfile($fpath,  $fname . '.ppi');
}

=pod
=head2 gen_file_list_xml $self

       Основной метод который выполняет:
       - копирование файлов
       - разбор и создание структуры
       - запись структуры в файл
       - сохранение полученной xml
=cut
sub gen_file_list_xml {
    my ($self) = @_;
    my $testDataFolder = $self->{testDataFolder};
    my $outputFolder = $self->{outputFolder};
    my $withSensorsFolder = $self->{withSensorsFolder};

	my $listOfLists = $self->move_perl_files_to_test_data();
    $self->write_ppi_dumps($listOfLists);
	my $len = @$listOfLists;
	print "len = $len\n";
    $log->warn("listoflists = $listoflists\n");
	my $treeGenerator = PerlHandler::TreeGenerator->new($listOfLists, {
        withoutSensorsDir => $testDataFolder,
        withSensorsDir => $withSensorsFolder,
        output_folder => $self->{outputFolder}
    });
	$treeGenerator->generate();
	#my $struct = $structGenerator->get_structure();
	print "stuct Generated \n";

	my $structgenerator = PerlHandler::StructGenerator->new($treeGenerator);
	$structgenerator->gen_structure();
	my $struct = $structgenerator->get_structure();

    $self->write_struct_to_file($struct);

	my $xml_file_name = 'simple_multiple.xml';
    my $xml_file_name = File::Spec->catfile($outputFolder, $xml_file_name);

	$xml_file_name = &PerlHandler::write_xml($struct, $xml_file_name);
	return $xml_file_name;
}

1;
