# https://www.cpan.org/misc/how-to-mirror.html
# https://metacpan.org/pod/CPAN::Mini

my $mirror = 'http://mirror.yandex.ru/mirrors/cpan/';
my $local_cpan_path = $ENV{"HOME"} . '/cpan_mirror';

use CPAN::Mini;
CPAN::Mini->update_mirror(
  remote => $mirror,
  local  => $local_cpan_path,
  log_level => 'debug',
);
