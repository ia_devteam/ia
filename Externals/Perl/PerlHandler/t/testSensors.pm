#!/usr/bin/perl

BEGIN {use Libsensor;}

package TestSensor;

print("[1010] Function body\n");
Libsensor::DinGo(1010);

sub some {}

print("[1020] Function body\n");
Libsensor::DinGo(1020);

sub someFunction
{
	undef;
	print("[1030] Function body\n");
	Libsensor::DinGo(1030);
	print("[1040] Function body\n");
	Libsensor::DinGo(1040);
	return &some();
}

print("[1050] Function body\n");
Libsensor::DinGo(1050);
&someFunction();
1;
