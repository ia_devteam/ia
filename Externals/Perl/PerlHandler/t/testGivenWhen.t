#!/usr/bin/perl

BEGIN {
	unless (caller) {
		use FindBin qw($Bin);
		use lib "$Bin/../lib/";
	}
}

use Carp 'verbose';
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use PPI;
use Test::Simple tests => 1;
use Data::Dumper;

use File::Spec;
use File::Copy;
use File::Basename;

use PerlHandler::PerlHandlerLogger;
our $log = $PerlHandler::PerlHandlerLogger::logger;
$log->init_logger({logConsole=>"INFO"});

use PerlHandler;
use PerlHandler qw(parse_file);
use PerlHandler::StructGenerator;
use PerlHandler::TreeGenerator;
use PerlHandler::Utils qw(files_equal create_ppi);

=pod
=head1 Выполняем генерацию id, filename  для каталога
       Заданный набор передаем в StructGenerator и генерируем
       xml для группы perl файлов
=cut

# 1. записать копию perl files to testData
# 2. Generate (id, file_name) list of lists
# 3. generate xml

#1.

my $testName = 'testGivenWhen';
my $testDataFolder = File::Spec->catfile("t", "testData", $testName);
my $outputFolder = File::Spec->catfile("t", "testData", $testName, "output");
my $withSensorsFolder = File::Spec->catfile("t", "testData", $testName, "withSensors");

our @fileList = qw(
	t/testData/stWhenForMultiple.pm
    );

=pod
=head2 move_perl_files_to_test_data копирует файлы
       которые будут использоваться для тестирования
       в каталог с файлами для тестирования
=cut
sub move_perl_files_to_test_data {
	our @fileList;
	my $testFileName = "";

	my @listOfLists = ();

	my $num = 0;
	for my $name (@fileList) {
		my ($fname,$fpath,$fsuffix) = fileparse($name, qr/\.[^.]*/);
		my $new_file_name = $fname . $fsuffix;
		print "new_file_name = ", $new_file_name, "\n";
        $log->info('TestDataFolder: ' . $testDataFolder . "\n");
        if (! -d $testDataFolder) {
            $log->info('Create folder: ' . $testDataFolder);
            File::Path::make_path($testDataFolder);
        }
		$testFileName = File::Spec->catfile($testDataFolder, $new_file_name);
		if (! files_equal($name, $testFileName)) {
			copy($name, $testFileName);
		}
		push @listOfLists, [$num, $testFileName];
		$num += 1;
	}
    if (! -d $outputFolder) {
        File::Path::make_path($outputFolder);
    }
    if (! -d $withSensorsFolder) {
        File::Path::make_path($withSensorsFolder);
    }
	return \@listOfLists;
}

sub write_struct_to_file {
	my ($struct) = @_;
    my $structFileName = File::Spec->catfile($outputFolder, "testStructureGen.pl");
	open $outputStruct, ">", $structFileName;
	print $outputStruct Dumper $struct;
	close $outputStruct;
}

=pod
=head2 gen_file_list_xml
=cut
sub gen_file_list_xml {
	my $listOfLists = &move_perl_files_to_test_data;
	my $len = @$listOfLists;
	print "len = $len\n";
	# my $max_num = $listOfLists->[$len-1]->[0];
	# print "max_num = $max_num\n";
	#my $listOfLists = []; my $max_num = 0;
	# push @$listOfLists, [$max_num + 1, "t/testData/RemoveTest.pm"];
    $log->warn("listoflists = $listoflists\n");
	my $treeGenerator = PerlHandler::TreeGenerator->new($listOfLists, {
        withoutSensorsDir => $testDataFolder,
        withSensorsDir=>$withSensorsFolder});
	$treeGenerator->generate();
	#my $struct = $structGenerator->get_structure();
	print "stuct Generated \n";

    create_ppi(File::Spec->catfile($testDataFolder, "stWhenForMultiple.pm"),
               File::Spec->catfile($testDataFolder, "stWhenForMultiple.ppi"));

	my $structgenerator = PerlHandler::StructGenerator->new($treeGenerator);
	$structgenerator->gen_structure();
	my $struct = $structgenerator->get_structure();

    write_struct_to_file($struct);

	my $xml_file_name = 'simple_multiple.xml';
    my $xml_file_name = File::Spec->catfile($outputFolder, $xml_file_name);

	$xml_file_name = &PerlHandler::write_xml($struct, $xml_file_name);
	return $xml_file_name;
}

my $file_name = gen_file_list_xml();
ok($file_name == 'simple_multiple.xml', 'Test parse multiple files');

1;
