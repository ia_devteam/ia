#!/usr/bin/perl

BEGIN {
	unless (caller) {
        use utf8;
		use FindBin qw($Bin);
        use lib "$Bin/../";
		use lib "$Bin/../lib/";
        use lib "$Bin/lib/";
	}
}


use Test::Simple tests => 1;
use t::lib::Helper;

use Data::Dumper;
my $tFileListCrash = Helper->gen_file_list_from_dir('!!!INSERT_FOLDER_NAME_HERE!!!');

my $testName = "testCrashTemplate"; #!!!CHANGE_NAME!!!
my $fileList = [
    #FILL FILES
    't/testData/stGoto.pm',
    't/testData/stIf.pm'
    #!!!ADD YOUR FILES!!!
    ];


##add files from folders
push @$fileList, @$tFileListCrash;

# my @tempList =  grep {/.*labels.t.*/} @$fileList;
# $fileList = \@tempList;
# print 'fileList = ' . @$fileList . "\n";

my $count = 0;
my $tempFileList = [];
my $errorFiles = [];

while ($count < scalar(@$fileList)) {
    my $tempFileList = [$fileList->[$count]];
    eval {
        my $testHelper = Helper->new($testName, $tempFileList);
        my $xmlFileName = $testHelper->gen_file_list_xml();
    };

    if ( $@ ) {
        print "ERRRRROR: $@ $fileList->[$count]\n";
        push @$errorFiles, $fileList->[$count];
    }
    $count ++;
}

print "Files with errors: @$errorFiles \n";

ok (1 == 1, 'Test finished');

1;
