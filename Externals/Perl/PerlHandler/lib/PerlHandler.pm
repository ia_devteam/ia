#!/usr/bin/perl

package PerlHandler;

use 5.010;
use strict;
use warnings;

BEGIN {
	unless (caller) {
		use FindBin qw($Bin);
		use lib "$Bin";
	}
}

use File::Basename;
use File::Path;
use File::Spec;
use File::Copy;

use Carp 'verbose';
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

use PerlHandler::PerlHandlerLogger;
use PerlHandler::PerlXsd;
use PerlHandler::TreeGenerator;
use PerlHandler::StructGenerator;

use Log::Message::Simple qw[msg error debug
							carp croak cluck confess];
our $log = $PerlHandler::PerlHandlerLogger::logger;

sub write_xml {
	return &PerlHandler::PerlXsd::write_xml(@_);
}

sub run_parse {
    my $params = shift;
    my $filename = $params->{filename};
    my $withoutSensorsDir = $params->{withoutSensorsDir};
    my $withSensorsDir = $params->{withSensorsDir};
    my $xmlAndLogDir = $params->{xmlAndLogDir};
    my $level = $params->{level};
    my $sensorTemplate = $params->{sensorTemplate};
    my $sensorStartId = $params->{sensorStartId};
    my $moduleId = $params->{moduleId};
    my $xsdPath = $params->{xsdPath};
    my $return_mode = $params->{return_mode};
    my $logConfig = $params->{logConfig} // {logFilePath => $xmlAndLogDir}; # параметры логирования

    $log->init_logger($logConfig);

	$log->info('required arguments: <filewithpath> <sensStartId> <withoutSensDir> <withSensDir> <xmlAndLogDir> <level> <xsdPath> <return_mode>');

    if (!$filename) {
        $log->error("Missing file with files ");
        die "Missing file with files";
    }

	$log->info("filename: $filename");
	$log->info("withoutSensorsDir $withoutSensorsDir");
	$log->info("withSensorsDir $withSensorsDir");
	$log->info("xmlAndLogDir $xmlAndLogDir");
	$log->info("level $level");
    $log->info("sensorTemplate $sensorTemplate");
    $log->info("sensorStartId: $sensorStartId");
    $log->info("moduleId $moduleId");

    if (defined $xsdPath) {
        $log->info("xsdPath $xsdPath");
    } else {
        $log->info('xsdPath undef');
    }

    $return_mode = $return_mode // '';
	$log->warn("return_mode $return_mode");

	my $_cwd = Cwd::getcwd;
	if (!$xsdPath) {$xsdPath = $_cwd;}
	if (Cwd::abs_path($xsdPath) ne Cwd::abs_path($_cwd)) {
		my ($_name,$_path,$_suffix) = fileparse($xsdPath, qr/\.[^.]*/);
		my $_fname = $_name . $_suffix;
		my $_dst = File::Spec->catfile($_cwd, $_name . $_suffix);
		$log->info("Copy file from $xsdPath to $_dst");
		File::Copy::copy($xsdPath, $_dst);
	}

	my @listOfFiles = ();
	my $num = 0;
	open my $filesFile, '<', $filename or die "File $filename не найден";
	while (<$filesFile>) {
		$_ =~ s/^\s+|\s+$//g;
		push @listOfFiles, [$num, $_];
		$num += 1;
	}
	close $filesFile;

	$log->info("FileCount $#listOfFiles");

    my $tree_gen_params = {
        sensorStartId => $sensorStartId + 0,
        level => $level + 0,
        withoutSensorsDir => $withoutSensorsDir,
        withSensorsDir => $withSensorsDir,
        returnMode => $return_mode,
        sensorTemplate => $sensorTemplate,
        moduleId => $moduleId,
        output_folder => $xmlAndLogDir
    };
	my $treeGenerator = PerlHandler::TreeGenerator->new(\@listOfFiles, $tree_gen_params);
	$treeGenerator->generate();

	my $structgenerator = PerlHandler::StructGenerator->new($treeGenerator);
	$structgenerator->gen_structure();
	my $struct = $structgenerator->get_structure();

	if (!-d $xmlAndLogDir) {
		$log->info("create directory $xmlAndLogDir");
		File::Path::make_path($xmlAndLogDir);
	}
	my $xml_file_name = File::Spec->catfile($xmlAndLogDir, 'Perl.xml');
	$log->info("output file name ", $xml_file_name);
	$xml_file_name = &write_xml($struct, $xml_file_name);

	$log->info("Разбор закончен");
}

=pod
=head2 create_params_hash
       преобразование списка параметров
=cut
sub create_params_hash {
    my ($filename, $withoutSensorsDir, $withSensorsDir, $xmlAndLogDir, $level, $sensorTemplate, $sensorStartId, $moduleId, $xsdPath, $return_mode) = @_;

    my $params = {
        filename => $filename,
        withoutSensorsDir=> $withoutSensorsDir,
        withSensorsDir => $withSensorsDir,
        xmlAndLogDir => $xmlAndLogDir,
        level => $level,
        sensorTemplate => $sensorTemplate,
        sensorStartId => $sensorStartId,
        moduleId => $moduleId,
        xsdPath => $xsdPath,
        return_mode => $return_mode
    };

    return $params;
}

unless (caller) {
    my $params = &create_params_hash(@ARGV);
	&run_parse($params);
}

1;

__END__
