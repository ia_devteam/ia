#!/usr/bin/perl

package PerlHandlerJson;

use 5.010;
use strict;
use warnings;

BEGIN {
	unless (caller) {
		use FindBin qw($Bin);
		print "Bin = $Bin\n";
		use lib "$Bin";
	}
}

use PerlHandler;
use JSON::Parse 'json_file_to_perl';

=pod
=head2 run_json filename Запустить parser задав параметры в json файле
C<filename> name of json file
=cut
sub run_json {
    my $jsonfilename = shift;
    my $params = json_file_to_perl($jsonfilename);
    (ref $params eq 'HASH') or die 'Parameters in file ' . $jsonfilename . ' should be a dict. ';

    my $filename = $params->{"sources"};
    my $withoutSensorsDir= $params->{"basepath"};
    my $withSensorsDir= $params->{"with-sensor-dir"} || $params->{withSensorDir};
    my $xmlAndLogDir= $params->{"xml-log-dir"} || $params->{xmlLogDir};
    my $level= $params->{"level"};
    my $sensorTemplate = $params->{"sensor-template"} || $params->{sensorTemplate};
    my $sensorStartId = $params->{"start-sensor"} || $params->{startSensor};
    my $moduleId = $params->{"module-id"} || $params->{moduleId};
    my $xsdPath= $params->{"xsd-path"} || $params->{xsdPath};
    my $return_mode = $params->{"return-mode"} || $params->{returnMode};

    my $params_hash = &PerlHandler::create_params_hash(
        $filename, $withoutSensorsDir, $withSensorsDir,
        $xmlAndLogDir, $level, $sensorTemplate,
        $sensorStartId, $moduleId, $xsdPath, $return_mode);

    $params_hash->{logConfig} = $params->{"logConfig"} // {logFilePath => $xmlAndLogDir};
    $params_hash->{logConfig}->{logFilePath} |= $xmlAndLogDir;

    &PerlHandler::run_parse($params_hash);
}

unless (caller) {
	&run_json(@ARGV);
}

1;

__END__
