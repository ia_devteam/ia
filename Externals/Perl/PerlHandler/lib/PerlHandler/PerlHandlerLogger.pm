#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

package PerlHandler::PerlHandlerLogger;

use Log::Log4perl;
use Log::Log4perl::Logger;

use File::Path;
use File::Spec;
use File::Basename;
use Cwd;

use Carp qw(carp);

=pod
=head1 Логгер для парсера

Параметры:
1. logConsole -- default ERROR, valid:(TRACE, DEBUG, INFO, WARN, ERROR, FATAL)
2. logFile    -- default DEBUG, valid:(TRACE, DEBUG, INFO, WARN, ERROR, FATAL)
3. logFilePath -- default log         relative or absolute path
4. logFileName -- default run.log     name of the file
5. logIfFileExists -- default ROTATE  valid: (ROTATE, APPEND, REPLACE)

#For upnderstand create method wrappers see:
#http://log4perl.sourceforge.net/releases/Log-Log4perl/docs/html/Log/Log4perl.html
#Using Log::Log4perl from wrapper classes
=cut

sub new {
	my ($cls, $params) = @_;
	my $class = ref $cls || $cls;
	return bless {
		params => $params,
        log => undef
	}, $class;
}

sub init_logger {
    my $self = shift;
    my $params = shift || {};
    if (! (ref $params eq ref {})) {
        $params = {};
    }

    my $logConsole = $params->{logConsole} || 'WARN';
    my $logFile = $params->{logFile} || 'DEBUG';
    my $logFilePath = $params->{logFilePath} || 'log';
    my $logFileName = $params->{logFileName} || 'parserRun.log';
    my $logIfFileExists = $params->{logIfFileExists} || 'ROTATE';

    #если путь не абсолютный, то считаем относительно каталога запуска
    if (!File::Spec->file_name_is_absolute($logFilePath)) {
        $logFilePath = File::Spec->catfile(Cwd::getcwd, $logFilePath);
    }

    if (!-d $logFilePath) {
        print "create directory for log \n";
        File::Path::make_path($logFilePath);
    }

    my $logDir = $logFilePath;

    #каждый запуск будет с новым именем файла
    my ($fileName, $filePath, $fileExt) = File::Basename::fileparse($logFileName, qr/\.[^.]*/);

    my $fileLogName;
    if ($logIfFileExists eq 'ROTATE') {
        my $num = 0;
        $fileLogName = File::Spec->catfile($logDir, "$fileName$fileExt");
        while ( -e $fileLogName) {
            $num ++;
            $fileLogName = File::Spec->catfile($logDir, "$fileName$num$fileExt");
        }
    } elsif ($logIfFileExists == "REPLACE") {
        $fileLogName = File::Spec->catfile($logDir, $logFileName);
        open my $tfile, '>', $fileLogName;
        close $tfile;
    } else {
        $fileLogName = File::Spec->catfile($logDir, $logFileName);
    }

    my $conf = q(
    log4perl.rootLogger          = FILELEVEL, Logfile, Screen
    log4perl.appender.Logfile          = Log::Log4perl::Appender::File
    log4perl.appender.Logfile.filename = MYFILELOGNAME
    log4perl.appender.Logfile.layout   = Log::Log4perl::Layout::PatternLayout
    log4perl.appender.Logfile.layout.ConversionPattern = %p %d{HH:mm} [ %M:%L ] %m%n

    log4perl.appender.Screen         = Log::Log4perl::Appender::Screen
    log4perl.appender.Screen.stderr  = 0
	log4perl.appender.Screen.Threshold = CONSOLELEVEL
    log4perl.appender.Screen.layout = Log::Log4perl::Layout::PatternLayout
	log4perl.appender.Screen.layout.ConversionPattern = %p %d{HH:mm} [ %M:%L ] %m%n
    );

    $conf =~ s/MYFILELOGNAME/$fileLogName/;
    $conf =~ s/CONSOLELEVEL/$logConsole/;
    $conf =~ s/FILELEVEL/$logFile/;

    #print "conf = $conf";

    # ... passed as a reference to init()
    Log::Log4perl::init( \$conf );

    # use Log::Log4perl qw(:easy);
    # Log::Log4perl->easy_init($DEBUG);
    $self->{log} = Log::Log4perl->get_logger('PerlHandlerLogger');
    $Log::Log4perl::caller_depth = 1; # for correct log line
    return $self;
}

sub trace {
    my $self = shift;
    my $log = $self->{log};
    if (defined($log) && $log) {
        return $log->trace(@_);
    } else {
        carp 'WARN: get method trace of logger, logger now undefined\n';
    }
}

sub debug {
    my $self = shift;
    my $log = $self->{log};
    if (defined($log) && $log) {
        return $log->debug(@_);
    } else {
        carp 'WARN: get method debug of logger, logger now undefined\n';
    }
    $log->debug(@_);
}

sub info {
    my $self = shift;
    my $log = $self->{log};
    if (defined($log) && $log) {
        return $log->info(@_);
    } else {
        carp 'WARN: get method info of logger, logger now undefined\n';
    }
}

sub warn {
    my $self = shift;
    my $log = $self->{log};
    if (defined($log) && $log) {
        return $log->warn(@_);
    } else {
        carp 'WARN: get method warn of logger, logger now undefined\n';
    }
}

sub error {
    my $self = shift;
    my $log = $self->{log};
    if (defined($log) && $log) {
        return $log->error(@_);
    } else {
        carp 'WARN: get method error of logger, logger now undefined\n';
    }
}

sub fatal {
    my $self = shift;
    my $log = $self->{log};
    if (defined($log) && $log) {
        return $log->fatal(@_);
    } else {
        carp 'WARN: get method fatal of logger, logger now undefined\n';
    }
}

sub DESTROY {
    my $self = shift;
    my $log = $self->{log};
    if (defined($log) && $log) {
        $log->DESTROY(@_);
        $self->{log} = undef;
    }
}

# для остальных методов
sub AUTOLOAD {
    our $AUTOLOAD;
    (my $method = $AUTOLOAD) =~ s/.*:://s; # удалить имя пакета
    my $var = q{
       sub $method {
           my $self = shift;
           my $log = $self->{log};
           if (defined($log) && $log) {
              return $log->$method(@_);
           } else {
              carp 'Error of get method $method of logger, logger missing\n';
           }
       }
    }; # конец строки q{ }
    $var =~ s/\$method/$method/g;
    print "var = $var";
    eval $var;
    die $@ if $@; # если вкралась опечатка
    goto &$method; # вызвать метод
}

our $logger = &new("PerlHandler::PerlHandlerLogger");

1;

__END__

# my $conf = q(
#     log4perl.rootLogger          = DEBUG, Logfile, Screen
#     log4perl.appender.Logfile          = Log::Log4perl::Appender::File
#     log4perl.appender.Logfile.filename = MYFILELOGNAME
#     log4perl.appender.Logfile.layout   = Log::Log4perl::Layout::PatternLayout
#     log4perl.appender.Logfile.layout.ConversionPattern = %p %d{HH:mm} [ %F:%M:%L ] %m%n

#     log4perl.appender.Screen         = Log::Log4perl::Appender::Screen
#     log4perl.appender.Screen.stderr  = 0
#     log4perl.appender.Screen.layout = Log::Log4perl::Layout::PatternLayout
# 	log4perl.appender.Screen.layout.ConversionPattern = %p %d{HH:mm} [ %F:%M:%L ] %m%n
# 	);
