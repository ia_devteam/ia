#!/usr/bin/perl
package PerlHandler::Utils;

use 5.010;
use strict;
use warnings;
use Data::Dumper;
use IO::File;
use PPI;
use PPI::Dumper;

use Log::Message::Simple qw[msg error debug
	carp croak cluck confess];

use Readonly;
use Exporter q'import';

our Readonly::Array @EXPORT = qw( first_line );
our Readonly::Array @EXPORT_OK = qw (
    first_line get_file_data
    files_equal create_dump create_ppi);
our Readonly::Hash  %EXPORT_TAGS = (all => [@EXPORT_OK]);

=pod
=head2 fix_generated_template функция пытается выполнить корекцию
       сгенерированного шаблона

       # нужно поправить сгенерированный шаблон
       # в первой и предпоследней строке удалить '
       # удалить  EntryStatement => [{},],
       # удалить  NextInLinearBlock => [{},],

# NOTE: template генератор все равно временами тупит и строит глючные шаблоны
# в таком случае нужно выполнять перезапуск тестового скрипта

=cut
sub fix_generated_template {
	my ($templateFileName) = @_;
	my $templateFileNameBackup = $templateFileName . "_backup";
	die "File $templateFileNameBackup exists " if (-f $templateFileNameBackup);

	open my $src, "<", $templateFileName;
	open my $dst, ">", $templateFileNameBackup;

	my $num = 0;
	my $prevline = '';
	my $line;

	while (<$src>) {
		$line = $_;
		if ($num == 0) {
			$line =~ s/'//;
		}
		$line =~ s/EntryStatement => \[\{\},\],//s;
		$line =~ s/NextInLinearBlock => \[\{\},\],//s;
		if ($num > 0) {
			print {$dst} $prevline;
		}
		$prevline = $line;
		$num += 1;
	}
	$prevline =~ s/'//;
	print {$dst} $prevline;
	close $src;
	close $dst;

	rename $templateFileNameBackup, $templateFileName;
	return;
};


=pod
=head check_doc_suitable проверить, что документ подходящий
=cut
sub check_doc_suitable {
	my ($docCfg) = @_;
	if (!$docCfg || ref $docCfg ne "ARRAY" || @{$docCfg} != 2){
		carp("docCfg $docCfg is incorrect");
		return;
	}
	return &check_file_suitable($docCfg->[1]);
}

=pod
=head2 check_file_suitable $file
       проверяет, что файл perl модуль по расширению
=cut
sub check_file_suitable {
	my ($file) =  @_;
	my($filename, $dirs, $suffix) = fileparse($file, qr/\.[^.]*/);
	my $usuf = uc($suffix);
	if ($usuf eq '.PM' || $usuf eq '.PL' || $usuf eq '.T') {
		return 1;
	}
	return 0;
}

=pod
=head2 Создать ppi файл по заданному файлу используя стандартный Dumper
=cut
sub create_ppi {
	my ($inputfile, $outputfile) = @_;
	my $t3Document = PPI::Document->new($inputfile);
	open my $struct_file, ">", $outputfile or die $!;
	my $obj = Data::Dumper->new([$t3Document]);
	$obj->Indent(1);
	print $struct_file $obj->Dump;
	close $struct_file;
	return;
}

=pod
=head2 create_dump $filename $object
       Create Dump of object into file with name $filename
=cut
sub create_dump {
	my ($outputfile, $object) = @_;
	open my $struct_file, ">", $outputfile or die $!;
	$Data::Dumper::Indent = 1;
	print $struct_file Dumper $object;
	close $struct_file;
	return;
}

=pod
=head2 Создать ppi файл по заданному файлу используя PPI::Dumper
=cut
sub create_ppi_ppi {
	my ($inputfile, $outputfile) = @_;
	my $t3Document = PPI::Document->new($inputfile);
	my $Dumper = PPI::Dumper->new( $t3Document );
	open my $struct_file, ">", $outputfile or die $!;
	print $struct_file $Dumper->string;
	close $struct_file;
	return;
}

=pod

=head2 first_line $string

The C<first_line> return first line of text.

=cut

sub first_line {
	my ($val) = @_;
	return ( split /\n/, $val )[0];
}

=pod

=head2 ppi_node_default_location $ppiNode

The C<ppi_node_default_location> return ppiNode default location.

=cut
sub ppi_node_default_location {
    my ($ppiNode) = @_;
    my $treeNode = $ppiNode->{_treeNode};
    my $location = undef;
    my $about = 0;

	if ($treeNode) {
		$location = $treeNode->{location_start};
	} else {
		#NOTE: в процессе вставок в дерево может меняться location для элементов
		#      поэтому используются location сохраненные на начало работы
		if ($ppiNode->{_default_location}) {
			$location = $ppiNode->{_default_location};
		} else {
			# (~) означает что позиция определяется вычислением
			# и может быть ошибка
            # NOTE: когда сенсор вставляется для вставленной undef вершины
            #       location для такой вершины может быть undef,
            #       и в коде будет предупреждение, вероятно это правильно, но стоит
            #       иметь ввиду

			$location = $ppiNode->location();
			if (!$location && $ppiNode->parent) {
				$location = $ppiNode->parent()->location();
			}
            $about = 0;
		}
	}
    return ($location, $about);
}

=pod

=head2 ppi_node_location $ppiNode

The C<ppi_node_default_location> return action ppiNode location.

=cut
sub ppi_node_location {
    my ($ppiNode) = @_;
    my $cls = ref $ppiNode || $ppiNode;
    my $location = $ppiNode->location();
    if (!$location && $ppiNode->parent) {
        $location = $ppiNode->parent()->location();
    }
    return $location;
}

=pod
=head2 get_file_data $fileName
       получить данные файла одной строкой
=cut
sub get_file_data {
	my $templateFileName = shift;
	open my $struct_file, "<", $templateFileName or die $!;
	my $data;
	{
		local $/;    # slurp mode аналогично $/, позволяет считать весь файл
		$data = <$struct_file>;
	}
	close $struct_file;
	return $data;
}

=pod
=head2 file_eql $name1, $name2
       выполняет проверку на равенство двух файлов
=cut
sub files_equal {
	my ($name1, $name2) = @_;
	return 0 if ((! -f $name2) || (! -f $name1));
	return 0 if ((-s $name1) != (-s $name2));

    my ($file1, $file2);
	unless(open $file1, '<', $name1) {return 0;}
	unless(open $file2, '<', $name2) {return 0;}

	my $bufferSize = 512;
	my($buffer1, $buffer2);
	my $result=1;
	while() {
		my $n1=read $file1, $buffer1, $bufferSize;
		unless(defined $n1) {
			$result = 0;
			last;
		}
		my $n2=read $file2, $buffer2, $bufferSize;
		unless(defined $n2) {
			$result = 0;
			last;
		}
		last unless $n1 or $n2;
		if($buffer1 ne $buffer2)
		{
			$result=0;
			last;
		}
	}
    close $file2;
	close $file1;
	return $result;
}

1;
