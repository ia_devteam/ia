#!/usr/bin/perl

package PerlHandler::TestHelper;

use 5.010;
use strict;
use warnings;
use PPI;
use Data::Dumper;

use Log::Message::Simple qw[msg error debug
	carp croak cluck confess];

use PerlHandler::PerlHandlerLogger;
our $log = $PerlHandler::PerlHandlerLogger::logger;

sub new {
	my ($cls, $tree_gen, $check) = @_;
	my $class = ref $cls || $cls;
	return bless {
		tree => $tree_gen,
		check => $check,
	}, $class;
}

sub check {
	my $self = shift;
	my $result = 1;
	$result &= $self->check_vars();
	return $result;
}

sub check_vars {
	my $self = shift;
	$log->info("check vars ...");
	my $vars = $self->{check}->{vars};
	my $result = 1;
	for my $name (keys %$vars) {
		$result &= $self->check_var($name, $vars->{$name});
	}
	return $result;
}

sub check_var {
	my ($self, $name, $var) = @_;
	my $tree = $self->{tree};
	my $treeVar;
	#print Dumper $var;
	if ($var->{global}) {
		$treeVar = $tree->{global_vars}->{$name};
	} else {
		$treeVar = $tree->{vars_hash}->{$name};
	}
	if (!$treeVar) {
		$log->error("Missing var $name");
		return 0;
	}
	# $self->check_var_get_set();
    # $self->check_var_get_set();
	return 1;
}

1;
