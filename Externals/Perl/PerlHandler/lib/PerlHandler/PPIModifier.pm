#!/usr/bin/perl

package PerlHandler::PPIModifier;

use 5.010;
use strict;
use warnings;
use PPI;

use PerlHandler::PerlHandlerLogger;
our $log = $PerlHandler::PerlHandlerLogger::logger;

use PerlHandler::Utils;

sub new {
	my ($cls, $tree_generator) = @_;
	my $class = ref $cls || $cls;
	return bless {
		tree_generator => $tree_generator
	}, $class;
}

=pod
=head2 insert_sensor_before Вставка сенсора перед выражением
       Проблема в том, что после удаления документа теряются и node,
       видимо, потому что они создаются как слабые ссылки
=cut
sub insert_sensor_before {
	my ($self, $node, $id, $insertNullStatement) = @_;
    my $statement_value = $self->get_sensor_expression($id);
	my $stmnt = $self->sens_val($statement_value);
	my $snode = $self->insert_statement_into($node->parent(), $stmnt, $node, 'notBlock', $insertNullStatement);
	$snode->{_sensorId} = $id;
	return $snode;
}

=pod
=head2 insert_sensor_into
       Вставка сенсора в блок, вставляется первым элементом
=cut
sub insert_sensor_into {
    my ($self, $node, $id) = @_;
    my $statement_value = $self->get_sensor_expression($id);
    return $self->insert_statement_into($node, $self->sens_val($statement_value));
}

=pod
=head2 insert_null_into
       Вставка сенсора в блок, вставляется первым элементом
=cut
sub insert_null_into {
	my ($self, $node) = @_;
	my $result = $self->insert_statement_into($node, "undef;\n");
	return $result;
}

=pod
=head2 insert_sensor_into
       Вставка сенсора в блок, вставляется первым элементом
=cut
sub insert_sensor_into_end {
	my ($self, $node, $id) = @_;
    my $statement_value = $self->get_sensor_expression($id);
	return $self->insert_statement_into_end($node, $self->sens_val($statement_value));
}

sub sens_val {
    my ($self, $statement_value) = @_;
    my $module_id = $self->{tree_generator}->get_module_id_param();
    return "Libsensor::DinGo($statement_value, $module_id);\n";
}

=pod
=head2 insert_null_into
       Вставка сенсора в блок, вставляется первым элементом
=cut
sub insert_null_into_end {
	my ($self, $node) = @_;
	my $result = $self->insert_statement_into_end($node, "undef;\n");
	return $result;
}

=pod
=head2 insert_statement_into $node $stmnt

       Выполнить вставку $stment в $node вершину дерева
       Для большей надежности проверим, что node PPI::Structure::Block
=cut
sub insert_statement_into {
	my ($self, $node, $stmnt, $beforeNode, $isNotBlock, $insertNullStatement) = @_;
	if (!$isNotBlock) {$self->_check_block($node);}
	if (!$beforeNode) {$beforeNode = $node->schild(0);}

	my $tdoc = PPI::Document->new([$stmnt]);
	my $sensorNode = $tdoc->schild(0);
	my $snode = $sensorNode->clone();
	my $snodeNext = $sensorNode->next_sibling()->clone();
	my $insertResult;
	my $prevNodeClone;
	my $prevNode;
	if ($beforeNode) {
		$prevNode = $beforeNode->previous_sibling();
		if ($prevNode && $prevNode->isa("PPI::Token::Whitespace")) {
			$prevNodeClone = $prevNode->clone();
		}
        #выполняем вставку ; перед сенсором для случая если предыдущее выражение не закончено
        if ($insertNullStatement) {
            my $endSemicolumn = PPI::Token::Structure->new(';');
            $self->_check_result($node->__insert_before_child($beforeNode, $endSemicolumn), $node, $stmnt);
        }
		$self->_check_result($node->__insert_before_child($beforeNode, $snode), $node, $stmnt);
		if ($prevNodeClone && index($prevNodeClone->content(), "\n") == -1) {
			$self->_check_result($node->__insert_before_child($beforeNode, $snodeNext), $node, $stmnt, 'space');
			$self->_check_result($node->__insert_before_child($beforeNode, $prevNodeClone), $node, $stmnt, 'space');
		} elsif ($prevNodeClone) {
			$self->_check_result(
				$node->__insert_before_child($beforeNode, $prevNodeClone), $node, $stmnt, 'space');
		}
		return $snode;
	}
	$self->_check_result($node->add_element($snode), $node, $stmnt);
	$self->_check_result($node->add_element($snodeNext), $node, $stmnt);
	return $snode;
}

=pod
=head2 insert_statement_into_end $node $stmnt

Выполнить вставку $stment в $node вершину дерева
Вставка делается в конец, либо после заданной вершины
FIXME: убрать copypaste
=cut
sub insert_statement_into_end {
	my ($self, $node, $stmnt, $beforeNode, $isNotBlock, $insertNullStatement) = @_;
	if (!$isNotBlock) {$self->_check_block($node);}
	if (!$beforeNode) {
		my $childlen = $node->schildren();
		if ($childlen > 0) {
			$beforeNode = $node->schild($childlen - 1);
		}
	}

	my $tdoc = PPI::Document->new([$stmnt]);
	my $sensorNode = $tdoc->schild(0);
	my $snode = $sensorNode->clone();
	my $snodeNext = $sensorNode->next_sibling()->clone();
	my $insertResult;
	my $prevNodeClone;
	my $prevNode;

	if ($beforeNode) {
		$prevNode = $beforeNode->next_sibling();
		if ($prevNode && $prevNode->isa("PPI::Token::Whitespace")) {
			$prevNodeClone = $prevNode->clone();
		}
		if ($prevNodeClone && index($prevNodeClone->content(), "\n") == -1) {
			$self->_check_result($node->__insert_after_child($beforeNode, $snodeNext), $node, $stmnt, 'space');
			$self->_check_result($node->__insert_after_child($beforeNode, $prevNodeClone), $node, $stmnt, 'space');
		} elsif ($prevNodeClone) {
			$self->_check_result(
				$node->__insert_after_child($beforeNode, $prevNodeClone), $node, $stmnt, 'space');
		}
		$self->_check_result($node->__insert_after_child($beforeNode, $snode), $node, $stmnt);

        #выполняем вставку ; перед сенсором для случая если предыдущее выражение не закончено
        if ($insertNullStatement) {
            my $endSemicolumn = PPI::Token::Structure->new(';');
            $self->_check_result($node->__insert_after_child($beforeNode, $endSemicolumn), $node, $stmnt);
        }
		return $snode;
	}
	$self->_check_result($node->add_element($snode), $node, $stmnt);
	$self->_check_result($node->add_element($snodeNext), $node, $stmnt);
	return $snode;
}


=pod
=head2 _check_result

Проверяет результат, если закончился с ошибкой, то
вставка не прошла
=cut
sub _check_result {
	my ($self, $result, $node, $stmnt, $space) = @_;
	my $spcMsg = !$space ? '' :  ', на вставке пробела';
	if (!$result) {
		$self->debug_ppi_node($node, ' node to insert statement: ');
		die "Вставка выражения $stmnt не удалась$spcMsg.";
	}
}

=pod
=head2 _check_block

Проверить, что вершина является bloком, и завершить 
программу если это не так
=cut
sub _check_block {
	my ($self, $node) = @_;
	if (!$node->isa('PPI::Structure::Block')) {
		$self->debug_ppi_node($node);
		die 'Node для вставки выражения должна быть Block';
	}
}


=pod
=head2 insert_use_libsensor 

       Вставка use Libsensor; в perl модуль
       чтобы можно было использовать DinGo функцию.
       use Libsensor нужно вставлять после любого package, 
       Либо можно вставлять use Libsensor в начало и вызывать 
       Libsensor::DinGo(), так будет меньше сложностей
=cut
sub insert_use_libsensor {
	my ($self, $doc, $fileId) = @_;
	my $tdoc = PPI::Document->new(["BEGIN {use Libsensor;}\n"]);
	#$tdoc->index_locations();
	my $useNode = $tdoc->schild(0);
	my $snode = $useNode->clone();
	my $spaceNode = $useNode->next_sibling()->clone();
	my $ch = $doc->schild(0);

    # if module started with package package_name;
    # then sensor inserted after this statement
    # second part in TreeGenerator::fill_tree_references
    my $func;
    if ($ch && $ch->content() && $ch->content() =~ /^package[^{]*$/g) {
        $func = $doc->can('__insert_after_child');
    } else {
        $func = $doc->can('__insert_before_child');
    }

	my $file_name = $self->get_file_by_id($fileId);
	if (!$ch) {
		$log->error("Doc $fileId: $file_name has not significant children");
		return;
	}
	my $insertResult;
	$self->debug_ppi_node($ch, 'Child for insert before: ');
	$insertResult = $func->($doc, $ch, $snode);
	$insertResult = $func->($doc, $ch, $spaceNode);
	$log->debug("File: $fileId: $file_name, insertResult: $insertResult");
	$self->debug_ppi_node($doc->schild(0), 'Child after insert: ');
	return $insertResult;
}

sub get_file_by_id {
	my $self = shift;
	return ($self->{tree_generator}->get_file_by_id(@_));
}

sub debug_ppi_node {
	my $self = shift;
	$self->{tree_generator}->debug_ppi_node(@_);
	return $self;
}

sub get_sensor_expression {
    my $self = shift;
    return $self->{tree_generator}->get_sensor_expression(@_);
}

1;
