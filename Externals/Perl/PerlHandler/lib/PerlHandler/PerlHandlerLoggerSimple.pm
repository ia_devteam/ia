#!/usr/bin/perl

use 5.010;
use strict;
use warnings;
use Log::Message::Simple;

package PerlHandler::PerlHandlerLoggerSimple;

sub new {
	my ($cls, $params) = @_;
	my $class = ref $cls || $cls;
	return bless {
		verbose => $params->{verbose}
	}, $class;
}

sub debug {
	Log::Message::Simple::debug($_[1], $_[0]->{verbose})
}

sub info {
	Log::Message::Simple::info($_[1], $_[0]->{verbose})
}

sub error {
	Log::Message::Simple::error($_[1], $_[0]->{verbose})
}

1;
