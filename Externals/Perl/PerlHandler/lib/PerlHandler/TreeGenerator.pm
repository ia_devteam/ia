#!/usr/bin/perl

package PerlHandler::TreeGenerator;

use 5.010;
use strict;
use warnings;
#use utf8; if in file exists utf8 characters
use PPI;

use Data::Dumper;
use Scalar::Util qw(refaddr);

use Log::Message::Simple qw[msg error debug
	carp croak cluck confess];

use PerlHandler::PerlHandlerLogger;
our $log = $PerlHandler::PerlHandlerLogger::logger;

use PerlHandler::Utils;
use PerlHandler::PPIModifier;
use PerlHandler::StructGenerator;
use PerlHandler::PerlXsd;
use PerlHandler::ReturnLogger;

use File::Basename;
use File::Path;
use File::Spec;

use constant FOR_TYPES => {'for' => 1, 'foreach' => 1};

=pod
=head1 Генератор Дерева для Perl

Генерация выполняется в 2 прохода:

1 Обход:
   - Для каждого файла делаем ppi Document.
   - Обходим все ppi дерево и собираем Statement.
   - Для каждого statement генерируем вершину дерева (treeNode) и собираем дочерние:

	При первом проходе для вершин заполняем:
	 - id,
	 - тип,
	 - locationline
	 - ppiNode вершина ppi

     Для ppi вершины прописываем ссылку на treeNode.

  То есть суть обхода в том, чтобы пройти по дереву определить:
  - statement,
  - subs
  - variables (тут проблема так как variables без определений ну будут тут отображены)
              (как вариант проверять все PPI::Token::Symbol)

2. Обход:

    - Проходим по treeNode
    - Заполняем ссылки, которые требуются в xsd
      например:
			для for ссылки на выражения и на внутреннюю операцию
    - заполняем ссылки на места вызова для функций и переменных
    - вставляем сенсоры

* Генерация структуры для xml из внутреннего представления будет делаться отдельно
  выполняется объектом StructGenerator
  Хотя большая часть полей заполняется тут, StructGenerator позволяет
  пользоваться свой структурой и сравнительно просто менять генерацию при изменении
  формата xsd.

** Алгоритм генерации за один проход, пришлось отложит,
   так как на этапе разбора for цикла уже
   возникали затруднения, которые видимо в дальнейшем видимо бы только увеличились.

=cut

=pod
=head2 new конструктор $file_list, $start_id

$file_list список списков [(fileId1, fileName1), ... ,(fileId1, fileName1)]
$start_id начальный id начиная с которого будут задаваться id для элементов

=cut
sub new {
	my ($cls, $file_list, $params) = @_;
    if (!$params) {
        $params = {};
    }

    my $start_id = $params->{sensorStartId};
    my $level = $params->{level};
    my $without_sensors_dir = $params->{withoutSensorsDir};
    my $with_sensors_dir = $params->{withSensorsDir};
    my $return_mode = $params->{returnMode};
    my $sensor_template = $params->{sensorTemplate} || "%DAT%";
    $log->debug('moduleId = ' . ($params->{moduleId} // 'undef') . "\n");
    my $module_id = $params->{moduleId} // '';

	$log->debug('Start TreeGenerator constructor ... ');
	$log->debug("file_list[0] = $file_list->[0]");
	my $class = ref $cls || $cls;
	$level = $level || 2; # по умолчанию 2
	my $without_sensors = $without_sensors_dir || '';
	my $with_sensors = $with_sensors_dir || 'with_sensors';
	$return_mode = $return_mode || 'default';
	if ($return_mode ne 'default' && $return_mode ne 'safe' && $return_mode ne 'unsafe') {
		$return_mode = 'default';
	}
	$log->warn('return_mode = ', $return_mode, "\n");

	my $refflist = ref $file_list;
	if (!$file_list || !defined($file_list) || ref $file_list ne "ARRAY") {
		confess("File list should be not empty ");
	};

    $log->debug("TreeGenerator: withoutSensorsDir $without_sensors");
    $log->debug("TreeGenerator: withSensorsDir $with_sensors");
    $log->debug("TreeGenerator: level $level");
    $log->debug("TreeGenerator: sensorTemplate $sensor_template");
    $log->debug('TreeGenerator: sensorStartId: ' . ($start_id // 'undef') . "\n");
    $log->debug("TreeGenerator: moduleId $module_id");
    $log->debug("TreeGenerator: return_mode $return_mode");

	my $self = bless {
		# список обрабатываемых документов
		files => $file_list,
		#хеш хешей в котором ключ id файла
	    #{ppi=>ppiDocument, tree=>statementTree}
		trees => {},
		cur_doc => undef,
		cur_ppi => undef,
		cur_namespace => undef,
		cur_file_id => undef,
		start_id => $start_id || 0,          # текущий id
        sensor_template => $sensor_template, # шаблон сообщения сенсора
        module_id => $module_id,             # номер модуля
		valid_child_operations => [
			'STOperational', 'STIf', 'STFor',
			'STForeach', 'STDoAndWhile', 'STBreak', 'STContinue',
			'STGoto', 'STReturn', 'STSwitch', 'STCase'],
		global_vars => {}, # хеш глобальных переменных
		vars_hash => {},   # хеш с переменными
		sub_hash => {},    # хеш с функциями
        scheduled_hash => {}, # хеш функций уровня компиляции
		st_list => undef,  # список выражений, соберем их сразу при генерации
		ids => {},         # хеш с ключем по id (id->treeNode)
		tdocs=>[],         # хранилище для документов иначе теряются
		level => $level,   # вставлять сенсоры в ветвление
		with_sensors => $with_sensors,       # путь до каталога с сенсорами
		without_sensors => $without_sensors, # путь до каталога с исходными данными
		struct_gen => undef,
		return_mode => $return_mode          # режим вставки сенсоров для return
	}, $class;
	$self->{struct_gen} = PerlHandler::StructGenerator->new($self);
	$self->{ppiModifier} = PerlHandler::PPIModifier->new($self);
    my $output_folder =  $params->{output_folder} || 'output';
    $self->{returnLogger} = PerlHandler::ReturnLogger->new({
            folder => $output_folder
        });
	return $self;
}

=pod
=head2 get_module_id_param получить modul_id как параметр
=cut
sub get_module_id_param {
    my $self = shift;
    my $module_id = $self->{module_id};
    if ("$module_id" eq '') {
        return '\'\'';
    }
    return $module_id;
}

=pod
=head2 Генератор id для вершин
=cut
sub gen_next_id {
	my $self = shift;
	return ++($self->{start_id});
}

=pod
=head2 set_cur_namespace $new_namespace
    Установка текущего namespace задается именем пакета

    #в начале каждого пакета namespace main
	#как только встретится Package, namespace становится именем
	#этого пакета
	#FIXME: не разобран случай когда 2 пакета в 1 модуле,
	#       между ними может быть main часть:
	#       package p1 {}; <...main....> package p2 {};
	#FIXME: пакеты не рассматриваются как классы, считаем
	#       все функции просто функциями
	#       нужно ли пытаться определить классы вопрос открытый,
	#       так как они могут быть реализованы по разному
=cut
sub set_cur_namespace {
	my ($self, $new_namespace) = @_;
	$self->{cur_namespace} = $new_namespace;
	$log->debug("Change namespace new: ", $new_namespace);
	return $self;
}

=pod
=head2 get_cur_namespace Вернуть текущее namespace
=cut
sub get_cur_namespace {
	my $self = shift;
	return $self->{cur_namespace} || 'Undefined';
}

=pod
=head2 get_cur_treehash
       Функция возвращает treeHash для текущего
       обрабатываемого документа
=cut
sub get_cur_treehash {
	my $self = shift;
	return $self->{trees}->{$self->{cur_file_id}}
}

=pod
=head2 Вернуть имя текущего обрабатываемого файла
=cut
sub get_cur_filename {
	my $self = shift;
	return $self->{cur_doc}->[1];
}

=pod
=head2 get_file_by_name
       Получить файл по id
=cut
sub get_file_by_id {
	my ($self, $fileId) = @_;
	for my $fl (@{$self->{files}}) {
		if ($fl->[0] == $fileId) {
			return $fl->[1];
		}
	}
	return ;
}

=pod
=head2 get_doccfg_by_id file_id
C<fileId> получить docCfg B<(id, filepath)> по C<fileId>
=cut
sub get_doccfg_by_id {
    my ($self, $fileId) = @_;
	for my $fl (@{$self->{files}}) {
		if ($fl->[0] == $fileId) {
			return $fl;
		}
	}
	return;
}

=pod
=head2 get_sensor_expression $id
       Сгенерировать выражение для номера сенсора
=cut
sub get_sensor_expression {
    my ($self, $id) = @_;
    my $sensor_template = $self->{sensor_template};
    my $module_id = $self->{module_id};
    $id = "" . $id;
    $sensor_template =~ s/%DAT%/$id/g;
    if (!!$module_id || ("" . $module_id eq "0")) {
        $module_id = "" . $module_id;
        $sensor_template =~ s/%SID%/$module_id/g;
    } else {
        $sensor_template =~ s/%SID%//g;
    }
    return "'" . $sensor_template . "'";
}

=pod
=head2 Основная функция выполняющая генерацию Tree для Statements
       И Хешей Переменных и Функций
       А так же построение связей между Переменными, Функциями и выражениями
=cut
sub generate {
	my ($self) = @_;
	# проходим по документам собираем структуру
	$log->info("Начало обхода 1...");
	for my $docCfg (@{$self->{files}}) {
		$log->info('Обрабатываю файл(id/name): ', $docCfg->[0], " ", $docCfg->[1]);
		$self->gen_doc($docCfg);
		$log->info('Файл(id/name): ', $docCfg->[0], " ", $docCfg->[1], ' обработан!');
	}
	$log->info("Обход 1 выполнен!");
	$self->fill_references();
	return $self;
}

=pod
=head2 Выполняет инициализацию treeHash
       и запуск обработчика для корневой вершины ppi и tree
=cut
sub gen_doc {
	my ($self, $docCfg) = @_;
	my ($id, $fname) = @{$docCfg};
    my $ppiDoc;

    #ХАК: for excpetions.pl file.
    #данный файл имеет вызов функции через конструкцию &'throw
    #найти который в описании не удалось и который ppi не понимает
    my $exception_file_data = $self->is_exception_file($fname);
    if (!$exception_file_data) {
        $ppiDoc = PPI::Document->new($fname);
    } else {
        $log->warn('Exception file ' . $fname . q{ detected $'throw replaced!});
        $exception_file_data = $self->replace_exception_file_data($exception_file_data);
        $ppiDoc = PPI::Document->new(\$exception_file_data);
    }
    if (! defined $ppiDoc) {
        $log->warn('ppiDoc is undefined may be utf-8 characters in file, try it:');
        my $data = $self->read_file_data_as_utf8($fname);
        $ppiDoc = PPI::Document->new(\$data);
    }

    # ppi может не обработать файл, тогда он возвращает undef вместо документа
    # такое может быть из-за не спец. символом см. PPINotHandled.pm там  при записи LABEL
    # используются спец. символы
    if (!$ppiDoc) {
        $log->error('PPI could not handle file ' . $fname);
        $self->{trees}->{$id} = {
            status=>'ppiError'
        };
        return $self;
    }

	$ppiDoc->index_locations();

	$self->{cur_doc} = $docCfg;
	$self->{cur_file_id} = $id;
	$self->{cur_ppi} = $ppiDoc;

	my $treeHash = {
        ppi=>$ppiDoc,
        tree=>{nodeType=>'Root', children=>[]},
        linked_modules=>{}, #список связанных модулей
        labels=>{},
        doc_config => $docCfg
	};

	$self->{trees}->{$id} = $treeHash;
	$self->set_cur_namespace("main");

    # создание функции файла
    # Если файл полностью пустой создаем сообщение об ошибке
    # и пропускаем, приме в FullEmpty.pm.
    if (scalar($ppiDoc->children) == 0 ) {
        $log->error('File ' . $fname . 'is empty.');
        $self->{trees}->{$id} = {
            status=>'ppiError'
        };
        return $self;
    }

    my $filefunc = $self->gen_sub($ppiDoc, $treeHash->{tree}, 'filefunction');
    push @{$treeHash->{tree}->{children}}, $filefunc;

	$self->handle_node($ppiDoc, $treeHash->{tree});
	return $self;
}

=pod
=head2 read_file_data_as_utf8 $self $filename

Read file as utf-8, need for files with utf8 characters, that ppi can't
parse and handle by default.
=cut
sub read_file_data_as_utf8 {
    my ($self, $filename) = @_;
    my $name = 't/testData/notascii.t';
    open my $handle, '<:encoding(UTF-8)', $name
        or die "Can't open '$name' for reading: $!";
    my $data = '';
    while (my $row = <$handle>) {
        $data = $data . $row;
    }
    close $handle;
    return $data;
}


sub is_exception_file {
    my ($self, $filename) = @_;
    if ($filename =~ /.*exceptions.pl/) {
        my $data = $self->get_file_data($filename);
        if ($data =~ /&'throw/) { return $data; }
        return;
    }
    return;
}

sub set_file_data {
    my ($self, $filename, $data) = @_;
    my $mode = '>';
    open my $handle, $mode, $filename
        or die "Can't open '$filename' for writing: $!";
    print {$handle} $data;
    close $handle;
}

sub get_file_data {
    my ($self, $filename, $utf8) = @_;
    my $mode = '<';
    if (defined $utf8 && $utf8 == 1) {
        $mode = '<:encoding(UTF-8)';
    }
    open my $handle, $mode, $filename
        or die "Can't open '$filename' for reading: $!";
    my $data = '';
    while (my $row = <$handle>) {
        $data = $data . $row;
    }
    close $handle;
    return $data;
}

sub replace_exception_file_data {
    my ($self, $data) = @_;
    $data =~ s/&'throw/&throw/g;
    #print "data = $data";
    return $data;
}

sub restore_exception_file_data {
    my ($self, $data) = @_;
    $data =~ s/&throw/&'throw/g;
    #print "data = $data";
    return $data;
}

=pod
=head2 handle_node $node $treeNode
       C<node> обрабатываемая вершина ppi
       C<treeNode> родительская вершина StatementTree

       Обработка очередной вершины ppi документа
       Создать дочерние и поместить в C<children> B<treeNode>
=cut
sub handle_node {
	my ($self, $node, $st_parent) = @_;
	my $cur_parent = $st_parent;

	#сохраняем позиции
	my $loc = $node->location();
	$node->{_default_location} = [$loc->[0], $loc->[1]];
	$node->{_fid} = $self->{cur_file_id};
	#если вершина statement, она становится новой родительской вершиной
    #либо если block является частью анонимной функции
    #либо если block относится к eval
	if ($node->isa('PPI::Statement') ||
        (($node->isa('PPI::Structure::Block') && $self->is_anonymous_func($node))) ||
        ($node->isa('PPI::Structure::Block') &&
         $node->sprevious_sibling &&
         $node->sprevious_sibling()->content eq 'eval')) {
		$cur_parent = $self->create_node($node, $st_parent);
		push @{$st_parent->{children}}, $cur_parent;
	}

	if (!$cur_parent->{nodeType}) {
		$self->debug_ppi_node($node, 'Unknown node type: ', 'error');
		$log->error("nodetype = $cur_parent->{nodeType}\n");
        return $self;
		#die "Unknown nodeType Error\n";
	}

	if ($node->isa("PPI::Node")) {
		foreach my $subnode ($node->elements()) {
			$self->handle_node($subnode, $cur_parent);
		}
	}
	return $self;
}

=pod
=head2 is_anonymous_func $self $node

Проверить, что вершина представляет собой анонимную функцию
Считаем что вершина является анонимной функцией если:

1. Вершина PPI::Structure::Block
2. Перед вершиной на том же уровне стоит PPI::Token::Whitespace
3. Перед пробелом стоит PPI::Token::Word == sub

FIX: на самом деле описание анонимной функции может быть более сложным
     и содержать сигнатуру, прототип и атрибуты
     https://perldoc.perl.org/perlsub.html#Prototypes

     указанные возможности используются значительно реже чем рассматриваемая схема
     и должны быть добавлены позднее
=cut
sub is_anonymous_func {
    my ($self, $node) = @_;
    if (!$node->isa('PPI::Structure::Block')) {return 0;}
    my $previous = $node->previous_sibling();
    if (!$previous || !$previous->isa('PPI::Token::Whitespace')) {return 0;}
    my $previous_previsous = $previous->previous_sibling();
    if (!$previous_previsous ||
        !$previous_previsous->isa('PPI::Token::Word')
        || !($previous_previsous->content() eq 'sub')) {
        return 0;
    }
    return 1;
}

# =pod
# =head2 is_eval_block $self $tnode

# Check that $tnode is eval node
# First word is eval and next is PPI::Structure::Block

# Eval block should be handled as anonymous func with
# call in definition place.
# =cut
# sub is_eval_block {
#     my ($self, $ppiNode) = @_;
#     if ($ppiNode->schildren < 2) {return;}
#     my ($first, $second) = $ppiNode->schildren;
#     if ($first->isa('PPI::Token::Word') &&
#         $first->content eq 'eval' &&
#         $second->isa('PPI::Structure::Block')) {
#         return 1;
#     }
#     return;
# }

=pod
=head2 create_node $node $treeNode
       Запустить в зависимости от типа генератор
=cut
no warnings 'experimental::smartmatch'; # отключаем warning на given
sub create_node {
  my ($self, $node, $prnt) = @_;
  my $st;
  given (ref $node) {
	when("PPI::Statement::Scheduled")	  { $st = $self->gen_scheduled($node, $prnt); break; }
	when("PPI::Statement::Sub")			  { $st = $self->gen_sub($node, $prnt); break; }
	when("PPI::Statement::Include")		  { $st = $self->gen_include($node, $prnt);break;}
	when("PPI::Statement::Package")		  { $st = $self->gen_package($node, $prnt); break;}
	when("PPI::Statement::Variable")	  { $st = $self->gen_variables($node, $prnt); break; }
	when("PPI::Statement::Expression")	  { $st = $self->gen_expression($node, $prnt);break;}
	when("PPI::Statement::Compound")	  { $st = $self->gen_compound($node, $prnt); break; }
	when("PPI::Statement::Break")		  { $st = $self->gen_break($node, $prnt); break;}
	when("PPI::Statement::Given")		  { $st = $self->gen_given($node, $prnt); break;}
	when("PPI::Statement::When")		  { $st = $self->gen_when($node, $prnt); break;}
	when("PPI::Statement::Data")		  { $st = $self->gen_data($node, $prnt); break;}
	when("PPI::Statement::End")			  { $st = $self->gen_end($node, $prnt); break;}
	when("PPI::Statement::Null")		  { $st = $self->gen_null($node, $prnt); break;}
	when("PPI::Statement::UnmatchedBrace"){ $st = $self->gen_unmatchedbrace($node, $prnt); break;}
	when("PPI::Statement::Unknown")		  { $st = $self->gen_unknown($node, $prnt); break;}
	when("PPI::Statement")				  { $st = $self->gen_statement($node, $prnt); break; }
    when("PPI::Structure::Block") {
        #FIX: в дальнейшем сюда может еще попасть do while
        #     но сейчас они не должны проходить
        $st = $self->gen_anonymous($node, $prnt); break;
    }
	default {die "Unknown type\n ", break;}
  }
  return $st;
}
use warnings;


=pod
=head2  gen_scheduled $node $treeNode

Помещаем их в список функций, непонятно, только куда ставить их вызов, так как данные
функции будут вызываться без участия в callNode.
- BEGIN вызывается как только определен, END перед завершением программы,
- UNITCHECK , CHECK and INIT  -- должны выполняться перед runtime программы
- END После выполнения программы

Таким образом по идее сначала выполняются BEGIN, потом UNITCHECK, CHECK, INIT

PPI cite: A scheduled code block is one that is intended to be run at a specific time during the loading process.
          Technically these scheduled blocks are actually subroutines, and in fact may have 'sub' in front of them.
          http://search.cpan.org/~mithaldu/PPI-1.220/lib/PPI/Statement/Scheduled.pm
=cut
sub gen_scheduled {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = 'Scheduled';
	$st->{Name} = $node->type();
	# особый namespace, так как выполняются не в runtime
	$st->{Namespace} = "__COMPILE__";
    push @{$self->{scheduled_hash}->{$st->{Name}}}, $st;
	push @{$self->{st_list}}, $st;
	return $st;
}

=pod
=head2 gen_sub $node $tNode $filefunc
Создание вершины для функции или для файла
=cut
sub gen_sub {
	my ($self, $node, $prnt, $filefunc)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = 'Function';
    if ($filefunc) { # функция уровня файла
        $st->{Name} = $self->file_rel_path($self->get_cur_filename());
        $st->{FileLevelFunc} = 1;
    } else {
        $st->{Name} = $node->name();
        $st->{FileLevelFunc} = undef;
    }
	$st->{Namespace} = $self->get_cur_namespace($node);
	push @{$self->{sub_hash}->{$st->{Name}}}, $st;
	push @{$self->{st_list}}, $st;
	return $st;
}

=pod
=head2 gen_anonymous $self $node $prnt
=cut
sub gen_anonymous {
    my ($self, $node, $prnt, $filefunc)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = 'Anonymous';
    $st->{Name} = $self->create_anonymous_name($node);
    $st->{FileLevelFunc} = undef;
	$st->{Namespace} = $self->get_cur_namespace($node);
	push @{$self->{sub_hash}->{$st->{Name}}}, $st;
	push @{$self->{st_list}}, $st;
	return $st;
}

=pod
=head2 create_anonymous_name $self $node

Создание имени для анонимной функции, определяем как имя файла + номер строки + номер символа
=cut
sub create_anonymous_name {
    my ($self, $node) = @_;
    my $loc = $node->location();
    my $name = 'anonymous_' . $self->file_rel_path($self->get_cur_filename()) . '_' . $loc->[0] . '_' . $loc->[1];
    return $name;
}

=pod
=head2 include пока что в настройках нет, при обработке include
       смотрим список связанных модулей
=cut
sub gen_include {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = "Include";
	$st->{Module} = $node->module();
	$st->{Type} = $node->type();

	#TODO: use constant фактически создает функцию
	#нужен специальный обработчик

	if ($node->type() ne 'no' && $st->{Module} && $st->{Module} ne "constant") {
		my $treeHash = $self->get_cur_treehash();
	    $treeHash->{linked_modules}->{$st->{Module}} = 1;
		my $cur_file = $self->get_cur_filename();
		$log->debug($cur_file, " use ", $st->{Module});
	}
	push @{$self->{st_list}}, $st;
	return $st;
}

=pod
=head2 Обработка ppi вершины Package
=cut
sub gen_package {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = "Package";
	$log->debug("PackageNode: ", $node->content);
	$self->set_cur_namespace($node->namespace());
	$st->{Name} = $node->namespace();
	push @{$self->{st_list}}, $st;
	return $st;
}

=pod
=head2 gen_variables
       Генерируем Variables
       В одной ppiNode Variables может быть определено множество variable
       Поэтому для каждой из них нужно создать 1 Variable
       Определение переменных может быть как:
       my $var        -- локальная переменная
       our $var или $PackageName::name = val; -- глобальная переменная пакета
       local $var  -- глобальное переменной временно присваивается
                      другое значение, исходное восстанавливается при выходе из блока
       state $var  -- аналогично static в C
=cut
sub gen_variables {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = "Variables";
	$self->add_to_st_list($st);
	$st->{variables} = [];
	my $var_type = $node->type();
	my $var;
	for my $symbol ($node->symbols()) {
		$var = $self->gen_variable($node, $prnt, $symbol, $var_type);
		push @{$st->{variables}}, $var;
	}
	return $st;
}

=pod
=head2 gen_variable
       Сгенерировать 1 переменную
=cut
sub gen_variable {
	my ($self, $node, $prnt, $symbol, $var_type)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	#$log->debug('gen_variable st->fileId = ', $st->{fileId}, ' ', $symbol->symbol());
	$st->{nodeType} = "Variable";
	$st->{Name} = $symbol->symbol();
	$st->{Namespace} = $self->get_cur_namespace();

	#не совсем правильно так как вообще говоря local тоже используется
	#для глобальных переменных, но при этом задает значение для части программы
	#TODO: для глобальных переменных нужно проверять чтобы они создавались только 1 раз
	#для local нужно данное место помечать как set value
	#точку следующую за блоком тоже нужно помечать как set value

	$st->{isGlobal} = ($var_type eq "our" || $var_type eq "local" ? "true" : "false");

	#если переменная глобальная добавляем ссылку на вершину
	if ($st->{isGlobal} eq "true") {
		push @{$self->{global_vars}->{$symbol->symbol}}, $st;
	} else {
		#иначе добавляем ссылку просто в список переменных
		push @{$self->{vars_hash}->{$symbol->symbol}}, $st;
	}

	$st->{AssignmentFrom} = [];
	$st->{PointsToFunctions} = [];
	$st->{ValueCallPosition} = [];
	$st->{ValueGetPosition} = [];
	$st->{ValueSetPosition} = [];
	return $st;
}

sub gen_expression {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = 'Expression';
	$self->add_to_st_list($st);
	return $st;
}

sub gen_compound {
	my ($self, $node, $prnt)  = @_;
	my $st;
	my $compoundType = $node->type();
	if ($compoundType eq 'if') {
		$st = $self->gen_compound_if($node, $prnt);
	} elsif ($compoundType eq 'while') {
		$st = $self->gen_compound_while($node, $prnt);
	} elsif ($compoundType eq 'for') {
		$st = $self->gen_compound_for($node, $prnt);
	} elsif ($compoundType eq 'foreach') {
		$st = $self->gen_compound_foreach($node, $prnt);
	} elsif ($compoundType eq 'continue') {
		$st = $self->gen_compound_block($node, $prnt);
	} else {
		$log->warn('Unsupported compound type: ', $compoundType);
        $st = $self->gen_st_tmplt($node, $prnt);
        $st->{nodeType} = 'UnsupportedCompound';
        $st->{compoundType} = $compoundType;
		$self->debug_ppi_node($node, '', 'warn');
	}
	return $st;
}

# в данном случае дальнейший проход создаст statements для условий
# нужно будет пройти по ним следующее которое в condition положить в условие
# которое за ним положить в выражение выполняемое в if
# если дальше будет else или ничего, то заполнить else
# если дальше else if, то нужно будет создать из него новый if и сослаться на него в текущем
# else, но это будет делать на втором проходе
sub gen_compound_if {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = "If";
	$self->add_to_st_list($st);
	return $st;
}

sub gen_compound_for {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = 'For';
	$self->add_to_st_list($st);
	return $st;
}

sub gen_compound_foreach {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = 'Foreach';
	$self->add_to_st_list($st);
	return $st;
}


sub gen_compound_while {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = 'While';
	$self->add_to_st_list($st);
	return $st;
}

sub gen_compound_block {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
    #$self->debug_ppi_node($node, 'Block', 'warn');
	$st->{nodeType} = 'Block';
	#$self->add_to_st_list($st);
	return $st;
}

=pod
=head2 gen_break ($self, $node, $prnt)
       Generate break node: redo, goto, next, last, return.
=cut
sub gen_break {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	my $nodeType = $self->get_break_type($node);
	$self->add_to_st_list($st);
	$st->{nodeType} = $nodeType;
    if ($nodeType eq 'GoTo') {
        my $gototype = $self->determine_goto_type($node);
        $st->{gototype} = $gototype;
    }
	return $st;
}

sub get_break_type {
	my ($self, $node)  = @_;
	my ($nodetype, $has_label) = $self->get_first_child_without_label_expr($node);
	#print 'content = ', $nodetype->content;
	$nodetype = $nodetype->content;
	if (!defined $nodetype) {return ;} # ошибка
	elsif ($nodetype eq 'next') { return 'Continue';}
	elsif ($nodetype eq 'last') { return 'Break';}
	elsif ($nodetype eq 'return') { return 'Return';}
	elsif ($nodetype eq 'goto') { return 'GoTo';}
	elsif ($nodetype eq 'redo') { return 'Redo';} # наверно нужно обрабатывать как goto
	else {return ;}
};

=pod
=head2 C<determine_goto_type ($self, $node)>

Determine type of goto node:
  1. C<goto LABEL;>
  2. C<goto EXPR;>
  3. C<goto &func;> <-- be regarded as return;
=cut
sub determine_goto_type {
    my ($self, $node) = @_;
    my $fChild = $node->schild(0);
    my $nextNode =  $fChild->snext_sibling();
    my $nextNextNode = $nextNode->snext_sibling();
    my $gototype = 2;
    if ($nextNode->isa("PPI::Token::Word") &&
        (!$nextNextNode ||
         ($nextNextNode->isa("PPI::Token::Structure") &&
          $nextNextNode->content eq ";")
        )) {
        $gototype = 1;
    } elsif ($nextNode->isa("PPI::Token::Symbol") &&
			 $nextNode->symbol_type eq '&' &&
			 (!$nextNextNode || ($nextNextNode->isa("PPI::Token::Structure") &&
                                 $nextNextNode->content eq ";"))) {
        $gototype = 3;
    } else {
        $gototype = 2;
    }
    return $gototype;
}

sub gen_given {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType}="Switch";
	$self->add_to_st_list($st);
	return $st;
}

=pod
=head2 gen_when $node $prnt
       Generate node of B<Case> type.
       if given parent of node was not found, that node should be handled as stIf.
=cut
sub gen_when {
	my ($self, $node, $prnt) = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
    my ($caseOwner, $caseType) = $self->find_case_owner($node);
    $st->{nodeType} = 'Case';
    $st->{caseType} = $caseType;
    $st->{caseOwner} = $caseOwner;
    if (!$caseType || $caseType ne 'given') {
        $self->add_to_st_list($st);
    }
	return $st;
}


=pod
=head2 compute_case_type $node
       Walk up to tree and try to find parent given, if it is not found than when handle as if.
       For Example:

           use v5.010;
           when (2 > 1) { print "1 > 2 \n"; continue; }

       will work correct, and when will work such as if.
       Returns a L<PPI::Element> parent node or undefined if doesn't found.
=cut
sub find_case_owner {
    my ($self, $node) = @_;
    while ($node) {
        if ($node->isa('PPI::Statement::Given'))  {return ($node, 'given');}
        elsif ($node->isa('PPI::Statement::Compound') && exists(FOR_TYPES->{$node->type()})) {
            return ('$node', 'for');
        }
        if ($node->isa('PPI::Statement::Sub')) {return (undef, undef);}
        $node = $node->parent;
    }
    return (undef, undef);
}

#когда нужно данные для обработки хранить в том же файле
sub gen_data {
	my ($self, $node, $prnt)  = @_;

    my $st_variables = $self->gen_st_tmplt($node, $prnt);
	$st_variables->{nodeType} = "Variables";
    $self->add_to_st_list($st_variables);
    $st_variables->{variables} = [];

	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = "Variable";
    push @{$st_variables->{variables}}, $st;

    # ? <DATA> объявляется переменная как __DATA__,
    # но используется как package::DATA
    # поэтому имя будет как в использование, потому что
    # повышает вероятность нахождения связи
    # альтернатива использовать __DATA__, но

	$st->{Name} = "DATA";
	push @{$self->{vars_hash}->{'DATA'}}, $st;
	$st->{Namespace} = $self->get_cur_namespace();
	$st->{isGlobal} = "false";
	$st->{AssignmentFrom} = []; # текущая локация
	$st->{PointsToFunctions} = []; # не будет
	$st->{ValueCallPosition} = []; # ну будет
	$st->{ValueGetPosition} = []; # ? если в файле встретится <DATA>
	$st->{ValueSetPosition} = []; # не будет
	return $st;
}

#специальная секция в которую обычно помещают комментарии
sub gen_end {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = "__END__"; # пока никак не обрабатываем
	return $st;
}

sub gen_null {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = "Null";
	$self->add_to_st_list($st); # будет operational без ничего;
	return $st;
}


sub gen_unmatchedbrace {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
    $self->debug_ppi_node($st->{ppiNode}, 'UnmatchedBrace: ', 'error');
	$st->{nodeType} = "Error"; # в документе ошибка
    my $cur_hash = $self->get_cur_treehash();
    $cur_hash->{status} = 'ppiError';
	return $st;
}


sub gen_unknown {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = "Warning"; # вероятно в документе ошибка
	return $st;
}


sub gen_statement {
	my ($self, $node, $prnt)  = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	push @{$self->{st_list}}, $st;
    $st->{nodeType} = 'Statement'; # эквивалентно expression
	return $st;
}

=pod
=head2 Сгенерировать шаблон вершины содержит информацию о location
       id, нужные ссылки
       Дополнительные поля задаются для каждого отдельного типа
=cut
sub gen_st_tmplt {
	my ($self, $node, $prnt) = @_;
	my $loc = $node->{_default_location} ||
			($node->{_treeNode} ?
			 $node->{_treeNode}->{location_start} :
			 $node->location());
	#my $loc = $node->location(); #при выполнении вставок location начинает лететь
	#FIXME: что делать с location_finish в этом случае ?
	my $last_token = $node->last_token();
	my $fin_loc = $last_token->location();
	my $fin_loc_len = length $last_token->content;
	$fin_loc->[1] += $fin_loc_len;
	$fin_loc->[2] += $fin_loc_len;
	my $tmplt = {
		ID=>$self->gen_next_id(),
		location_start=>$loc,      # по умолчанию
		location_finish=>$fin_loc, # по умолчанию
		ppiNode=>$node,
		treeParent=>$prnt,
		fileId => $self->{cur_file_id}
	};
	#$log->debug('$tmplt->{fileId}', $tmplt->{fileId});
	$self->{ids}->{$tmplt->{ID}} = $tmplt; # добавляем значение в хеш
	if (!$node->{_treeNode}) {
		$node->{_treeNode} = $tmplt; # делаем ссылку на вершину нашего дерева
		push @{$node->{_treeNodes}}, $tmplt; # делаем ссылку на вершину нашего дерева
	} else {
		push @{$node->{_treeNodes}}, $tmplt; # делаем ссылку на вершину нашего дерева
	}

	#если у вершины есть метка,
	#то сохраняем ее в список меток для модуля

	if ($node->can("label") && $self->ppi_state_label($node)) {
		my $lbl = $self->ppi_state_label($node);
		my $treeHash = $self->get_cur_treehash();
		push @{$treeHash->{labels}->{$lbl}}, $tmplt;
	}
	return $tmplt;
}

sub gen_st_tmplt_null {
	my ($self, $node, $prnt) = @_;
	my $st = $self->gen_st_tmplt($node, $prnt);
	$st->{nodeType} = "insertedNull";
	return $st;
}

sub add_to_st_list {
	my ($self, $tnode) = @_;
	push @{$self->{st_list}}, $tnode;
	return $self;
}

=pod
=head2 fill_references
Заполнить связи
=cut
sub fill_references {
	my ($self) = @_;
	#нужно обойти все trees
	#для statements построить кроссылки
	#для expression, statement построить ссылки для переменных и ссылки для функций
	for my $treeId (sort keys %{$self->{trees}}) {
        my $fname = $self->get_file_by_id($treeId);
        my $exception_file = $self->is_exception_file($fname);
        my $treeHash = $self->{trees}->{$treeId};
        $self->{returnLogger}->init_doc_log($treeHash);

        if ($treeHash->{status} && $treeHash->{status} eq 'ppiError') {
            next;
        }

		my $tree = $self->{trees}->{$treeId}->{tree};

        $log->info('start handle file: ' . $fname);

		$self->fill_tree_references($treeId, $tree);

		my $doc = $self->{trees}->{$treeId}->{ppi};

        #превращаем путь в относительный
        $fname = $self->file_rel_path($fname);

        #$fname = $self->{with_sensors} . '/' . $fname;
		$fname = File::Spec->catfile($self->{with_sensors} , $fname);
		$log->debug('fname with sensors = ', $fname);

		my ($_name,$_path,$_suffix) = fileparse($fname, qr/\.[^.]*/);
		$log->debug('dirname = ', $_path);
		if ( ! -d $_path) {
			$log->debug('create directory = ', $_path);
            File::Path::make_path($_path);
			#mkpath([$_path], 1, 777) or die $!;
		}
		$self->insert_use_libsensor($doc, $treeId);
		$doc->save($fname);
        $self->{returnLogger}->save_doc_log();
        if (defined $exception_file && !!$exception_file) {
            my $filedata = $self->get_file_data($fname);
            $filedata = $self->restore_exception_file_data($filedata);
            $self->set_file_data($fname, $filedata);
            $log->warn('Exception file ' . $fname . q{ detected $'throw replaced!});
        }
        $log->info('finish handle file: ' . $fname);
	}
	return $self;
}

=pod
=head2 file_rel_path получить относительный путь до файла
=cut
sub file_rel_path {
    my ($self, $filename) = @_;
    if ($self->{without_sensors}) {
        $filename = File::Spec->abs2rel($filename, $self->{without_sensors});
        # $log->debug('relative with sensors = ', $fname);
    }
    $filename =~ s{^\w:/}{}; #убираем диск
    $filename =~ s{^/}{};    #убираем первый /
    return $filename;
}

=pod

=head2 fill_tree_references $treeId $tree

Handle generated tree for one L<PPI::Document>, fill
references beside variables and functions.

=cut

sub fill_tree_references {
	my ($self, $treeId, $tree) = @_;
	$self->{cur_file_id} = $treeId;
    $self->{cur_doc} = $self->get_doccfg_by_id($treeId);
	$self->handle_tree_node($tree);

	my $curHash = $self->get_cur_treehash();
    my $doc = $curHash->{ppi};
	my $schild = $doc->schild(0);

    #insert start sensor call
    #sensor inserted afer package declaration
    #or before first statement if it is not package
	if ($schild && $schild->{_treeNode}) {
		my $id = $schild->{_treeNode}->{ID};
        my $sensor_value = $self->get_sensor_expression($id);
        my $module_id = $self->get_module_id_param();
        my $sensor_expr = "Libsensor::DinGo($sensor_value, $module_id);\n";
        if ($schild && $schild->content() && $schild->content() =~ /^package[^{]*$/g) {
            $self->{ppiModifier}->insert_statement_into_end($doc, $sensor_expr, $schild, 'notBlock');
        } else {
            $self->{ppiModifier}->insert_statement_into($doc, $sensor_expr, $schild, 'notBlock');
        }
		$schild->{_treeNode}->{SensorBeforeTheStatement} = $id;
	}
    #insert finish sensor call
    #sensor inserted before last statement that is not
    #in general this statement is "1;"

    #В perl, завершающее выражение используется,
    #как индикация корректности загрузки модуля.
    #Как правило для этого используется выражение 1;
    #Поэтому сенсор в конец файла помещается перед найденным завершающим
    #выражением, исключая __END__ и __DATA__.
    my @schildren = $doc->schildren();
    foreach my $child (reverse @schildren) {
        if ($child->isa('PPI::Statement') &&
            !$child->isa('PPI::Statement::End') &&
            !$child->isa('PPI::Statement::Data')) {
            if ($child->{_treeNode}) {
                my $tnode = $child->{_treeNode};
                if (!$tnode->{SensorBeforeTheStatement}) {
                    $tnode->{SensorBeforeTheStatement} = $self->insert_sensor_before($tnode->{ppiNode}, $tnode)->{_sensorId};
                }
                last;
            }
        }
    }
	return $self;
}

sub handle_tree_node {
	my ($self, $tnode) = @_;

	$self->hndl_node($tnode);

	my $children = $tnode->{children};
	if ($children) {
		foreach my $child (@{$children}) {
            #$self->debug_ppi_node($child->{ppiNode}, "Child: ", 'info');
			$self->handle_tree_node($child);
		}
	}

	return $self;
}

no warnings 'experimental::smartmatch'; # отключаем warning на given
sub hndl_node {
	my ($self, $tnode) = @_;
	my $line;
	$line = ${$tnode->{ppiNode}->location()}[0] if $tnode->{nodeType} ne "Root";

	given ($tnode->{nodeType}) {
		when ("Scheduled")	{$self->hndl_scheduled($tnode); break;}
		when ("Function")	{$self->hndl_function($tnode); break};
        when ("Anonymous")	{$self->hndl_anonymous($tnode); break};
		when ("Include")	{$self->hndl_include($tnode);break};
		when ("Package")	{$self->hndl_package($tnode);break};
		when ("Variables")	{$self->hndl_variables($tnode);break};
		when ("Variable")	{$self->hndl_variable($tnode);break};
		when ("Expression") {$self->hndl_expression($tnode);break};
		when ("If")			{$self->hndl_if($tnode);break};
		when ("For")		{$self->hndl_for($tnode);break};
		when ("Foreach")	{$self->hndl_foreach($tnode);break};
		when ("While")		{$self->hndl_while($tnode);break};
		when ("Block")		{$self->hndl_block($tnode);break};
		when ("Break")		{$self->hndl_break($tnode);break};
	    when ("Continue")   {$self->hndl_continue($tnode);break};
		when ("Redo")       {$self->hndl_redo($tnode);break};
		when ("Return")     {$self->hndl_return($tnode);break};
		when ("GoTo")       {$self->hndl_goto($tnode);break};
		when ("Switch")		{$self->hndl_switch($tnode);break};
		when ("Case")		{$self->hndl_case($tnode);break};
		when ("Null")		{$self->hndl_null($tnode);break};
		when ("Error")		{$self->hndl_error($tnode);break};
		when ("Statement")	{$self->hndl_statement($tnode); break;}
        when ('UnsupportedCompound') {
            $self->debug_ppi_node($tnode->{ppiNode},
                                  'Skip handle for unsupported compound ' .
                                  ($tnode->{compoundType} // '__minssing__'));
            break;
        }
		when ("Root")	    {break;}
		when ("__END__")    {break;}
		default {
			$log->debug($tnode->{treeParent}->{nodeType}, ' ',
						$tnode->{ppiNode}->content);
			die "Unknown type $tnode->{nodeType} \n";}
	}
}

sub get_tnode_kind {
	my ($self, $tnode) = @_;
	my $kind = "Unknown";
	given ($tnode->{nodeType}) {
		when ("Scheduled")	{$kind="STOperational"; break;}
		when ("Function")	{$kind="STOperational"; break;}
		when ("Include")	{$kind="STOperational";break;} # как это обрабатывать ?
		when ("Package")	{$kind="STOperational";break;}
		when ("Variables")	{$kind="STOperational";break;}
		when ("Variable")	{$kind="STVariable";break;}
		when ("Expression") {$kind="STOperational";break;}
		when ("If")			{$kind="STIf";break;}
		when ("For")		{$kind="STFor";break;}
		when ("Foreach")	{$kind="STForEach";break;}
		when ("While")		{$kind="STDoAndWhile";break;}
		when ("Break")		{$kind="STBreak";break;}
	    when ("Continue")   {$kind="STContinue";break;}
		when ("Redo")       {$kind="STContinue";break;}
		when ("Return")     {$kind="STReturn";break;}
		when ("GoTo")       {$kind= $tnode->{gototype} == 3 ? 'STReturn' : 'STGoto'; break;}
		when ("Switch")		{$kind="STSwitch";break;}
		when ("Case")		{$kind= ($tnode->{ppiNode} && $tnode->{ppiNode}->{caseType} && $tnode->{ppiNode}->{caseType} eq 'given') ?
                                  'STCase' : 'STIf'; break; } # ?
		when ("Null")		{$kind="STOperational";break;}
		when ("insertedNull")  {$kind="STOperational";break;} # ? если нет действия
		when ("Error")		{$kind="STError";break;}
		when ("Statement")	{$kind="STOperational"; break;}
		when ("Block")	{
			$self->debug_ppi_node($tnode->{ppiNode});
			die 'Что-то пошло не так брать kind для block не должно быть необходимости \n';
		}
	    default {
			$self->debug_ppi_node($tnode->{ppiNode});
			die 'Unknown type \n';}
	}
	return $kind;
}
use warnings;

=pod
=head2 desc_tree_node $treeNode

Вернуть описание для вершины ID, kind.

=cut
sub desc_tree_node {
	my ($self, $tnode) = @_;
	return {
		ID => $tnode->{ID},
		kind => $self->get_tnode_kind($tnode)
	};
}

=pod
=head2 hndl_scheduled

=cut
sub hndl_scheduled {
	my ($self, $tnode) = @_;
    $self->get_and_set_next_in_linear_block($tnode);
	my $ppiNode = $tnode->{ppiNode};
	my $subWordOrSchedName = $ppiNode->schild(0);
	my $nextBlock = $self->get_next_block($subWordOrSchedName);
	if (!$nextBlock) {
		$log->error('Для sheduled нет блока.');
        $self->debug_ppi_node($ppiNode, 'Scheduled без блока: ', 'error');
        return $self;
		#die('Для sub/sheduled не определен блок');
	}
	my $nextSt = $self->insert_sensor_next_block($nextBlock, $tnode, 3);
	$tnode->{EntryStatement} = $self->desc_tree_node($nextSt);

	#поставим sensor + undef в конец блока
    #может быть ситуация, что предыдущее выражение не заканчивается ;
    #например *DEBUG = sub () {0} unless defined &DEBUG
    #тогда в результате такого insert
    # в этом случае нужно выполнять вставку еще символа ;
	my $nullTNode = $self->insert_null_tnode_into_block($nextBlock, $tnode, 'end');
	$self->debug_ppi_node($nullTNode->{ppiNode}, 'hndl_scheduled nullTNode: ');
	if (!$nullTNode->{SensorBeforeTheStatement}) {
		my $sensNode = $self->insert_sensor_before($nullTNode->{ppiNode}, $nullTNode, 'needNullStatement');
		$nullTNode->{SensorBeforeTheStatement} = $sensNode->{_sensorId};
	}
	#после того как в block создали вершины, для первой undef может быть задан
	#nextInLinearBlock
	#в других частях программы по идее такого быть не должно
	$self->get_and_set_next_in_linear_block($nextSt);
	return $self;
}

sub get_next_block {
	my ($self, $ppiNode) = @_;
	while ($ppiNode && !($ppiNode->isa('PPI::Structure::Block'))) {
		$ppiNode = $ppiNode->snext_sibling();
		#$self->debug_ppi_node($ppiNode, 'get_next_block: ');
	}
	return $ppiNode;
}

# sub add_entry_statement_simple {
# 	my ($self, $tnode) = @_;
# 	#нужно сделать ссылку на первую children операцию, которая
# 	#может выполняться
# 	if (defined $tnode->{children}) {
# 		for my $child (@{$tnode->{children}}) {
# 			if ($child) {
# 				my $kind = $self->get_tnode_kind($child);
# 				if ($self->is_st_kind($kind)) {
# 					$tnode->{EntryStatement} = $self->desc_tree_node($child);
# 					last;
# 				}
# 			}
# 		}
# 	}
# 	return $self;
# }

sub is_st_kind {
	my ($self, $kind) = @_;
	my $count = grep {/$kind/} @{$self->{valid_child_operations}};
	if ($count > 0) {return 1;}
	return;
}

=pod
=head2 hndl_function $treeNode
FIXME: избавится от copypaste
=cut
sub hndl_function{
	my ($self, $tnode) = @_;
    $self->get_and_set_next_in_linear_block($tnode);

    if ($tnode->{FileLevelFunc} && $tnode->{FileLevelFunc} == 1) {
        return &hndl_file_function(@_);
    }

	# 1. как для sheduled
	my $ppiNode = $tnode->{ppiNode};
	my $subWordOrSchedName = $ppiNode->schild(0);
	my $nextBlock = $self->get_next_block($subWordOrSchedName);
	if (!$nextBlock) {
		$log->error('Для sub нет блока.');
        $self->debug_ppi_node($ppiNode, 'Функция без блока: ', 'error');
        #die('Для sub/sheduled не определен блок');
        return $self;
	}
	my $blockChildCount = $nextBlock->schildren();

	my $nullNode;
    my $nullNodeFirst;
	my $needIndsertFirstSensor = 1;
	#если функция пустая undef вставляем дважды
	if ($blockChildCount == 0) {
		$nullNode = $self->insert_null_tnode_into_block($nextBlock, $tnode);
		$tnode->{EntryStatement} = $self->desc_tree_node($nullNode);

        $nullNodeFirst = $nullNode;

		$nullNode = $self->insert_null_tnode_into_block($nextBlock, $tnode);
		$tnode->{EntryStatement} = $self->desc_tree_node($nullNode);

        #порядок:
        #nullNode
        #nullNodeFirst
        #потому что nullNode ставится второй, и получается перед nullNodeFirst
        $nullNode->{NextInLinearBlock} = $self->desc_tree_node($nullNodeFirst);

	} elsif ($blockChildCount == 1 &&
			  $self->is_lastchild_return($nextBlock->schild($blockChildCount - 1))) {

        my $nextNode = $nextBlock->schild(0)->{_treeNode};

		#если в функции 1 запись и она будет return, то вставлять второй undef не нужно
		$nullNode = $self->insert_null_tnode_into_block($nextBlock, $tnode);
		$tnode->{EntryStatement} = $self->desc_tree_node($nullNode);

        $self->set_next_in_linear_block($nullNode, $nextNode);
	}

	$self->debug_ppi_node($nextBlock, 'Hndl Function next block: ');
	my $nextSt = $self->insert_sensor_next_block($nextBlock, $tnode, 3);
	$tnode->{EntryStatement} = $self->desc_tree_node($nextSt);

	#2. взять конец блока, если это не return,
	#   завернуть в return и поставить сенсор
	$blockChildCount = $nextBlock->schildren();
	my $lastChild;
	if ($blockChildCount == 0) {
		#нужно вставить еще один сенсор на выход
	} else {
		$lastChild = $nextBlock->schild($blockChildCount - 1);
		if ($lastChild->isa("PPI::Statement::Break") &&
				$lastChild->schild(0)->content eq "return") {
			#будет обработано в hdnl_return
			return;
		} elsif ($self->is_simple_statement($lastChild)) {
			$self->create_return($lastChild);
		} else {
			$self->debug_ppi_node($nextBlock, 'Hndl sub block: ', 'warn');
			$log->warn('Sub ', $tnode->{Name}, ' Return не определен!');
		}
	}
	$self->get_and_set_next_in_linear_block($nextSt);
	return $self;
}

=pod
=head2 hndl_anonymous $treeNode
FIXME: избавится от copypaste (c hndl_function)
=cut
sub hndl_anonymous {
	my ($self, $tnode) = @_;
    #$self->get_and_set_next_in_linear_block($tnode);

    # if ($tnode->{FileLevelFunc} && $tnode->{FileLevelFunc} == 1) {
    #     return &hndl_file_function(@_);
    # }

	# 1. как для sheduled
	# my $ppiNode = $tnode->{ppiNode};
	# my $subWordOrSchedName = $ppiNode->schild(0);

    my $ppiNode = $tnode->{ppiNode};
	my $nextBlock = $tnode->{ppiNode};
	if (!$nextBlock) {
        #по идее такое не возможно
		$log->error('Для sub нет блока.');
        $self->debug_ppi_node($ppiNode, 'Анонимная функция без блока: ', 'error');
        return $self;
	}
	my $blockChildCount = $nextBlock->schildren();

	my $nullNode;
    my $nullNodeFirst;
	my $needIndsertFirstSensor = 1;
	#если функция пустая undef вставляем дважды
	if ($blockChildCount == 0) {
		$nullNode = $self->insert_null_tnode_into_block($nextBlock, $tnode);
		$tnode->{EntryStatement} = $self->desc_tree_node($nullNode);

        $nullNodeFirst = $nullNode;

		$nullNode = $self->insert_null_tnode_into_block($nextBlock, $tnode);
		$tnode->{EntryStatement} = $self->desc_tree_node($nullNode);

        #порядок:
        #nullNode
        #nullNodeFirst
        #потому что nullNode ставится второй, и получается перед nullNodeFirst
        $nullNode->{NextInLinearBlock} = $self->desc_tree_node($nullNodeFirst);

	} elsif ($blockChildCount == 1 &&
			  $self->is_lastchild_return($nextBlock->schild($blockChildCount - 1))) {

        my $nextNode = $nextBlock->schild(0)->{_treeNode};

		#если в функции 1 запись и она будет return, то вставлять второй undef не нужно
		$nullNode = $self->insert_null_tnode_into_block($nextBlock, $tnode);
		$tnode->{EntryStatement} = $self->desc_tree_node($nullNode);

        $self->set_next_in_linear_block($nullNode, $nextNode);
	}

	$self->debug_ppi_node($nextBlock, 'Hndl Function next block: ');
	my $nextSt = $self->insert_sensor_next_block($nextBlock, $tnode, 3);
	$tnode->{EntryStatement} = $self->desc_tree_node($nextSt);

	#2. взять конец блока, если это не return,
	#   завернуть в return и поставить сенсор
	$blockChildCount = $nextBlock->schildren();
	my $lastChild;
	if ($blockChildCount == 0) {
		#нужно вставить еще один сенсор на выход
	} else {
		$lastChild = $nextBlock->schild($blockChildCount - 1);
		if ($lastChild->isa("PPI::Statement::Break") &&
				$lastChild->schild(0)->content eq "return") {
			#будет обработано в hdnl_return
			return;
		} elsif ($self->is_simple_statement($lastChild)) {
			$self->create_return($lastChild);
		} else {
			$self->debug_ppi_node($nextBlock, 'Hndl sub block: ', 'warn');
			$log->warn('Sub ', $tnode->{Name}, ' Return не определен!');
		}
	}
	$self->get_and_set_next_in_linear_block($nextSt);
	return $self;
}

=pod
=head2 hndl_file_function $treeNode
Функция обработки функции файла, нужно заполнить EntryStatement
=cut
sub hndl_file_function {
	my ($self, $tnode) = @_;
	# 1. как для sheduled
	my $ppiNode = $tnode->{ppiNode};
    my $entryStatement = $self->get_first_tree_node($ppiNode);
    if (!$entryStatement) {
        $log->warn('missing entry statement in file ', $tnode->{Name});
    } else {
        $tnode->{EntryStatement} = $self->desc_tree_node($entryStatement);
    }
	return $self;
}

sub is_lastchild_return {
	my ($self, $lastChild) = @_;
	if (($lastChild->isa("PPI::Statement::Break") &&
		      $lastChild->schild(0)->content eq "return") ||
		$self->is_simple_statement($lastChild)
		) {return 1;}
	return;
}

=pod
=head2 is_simple_statement $ppiNode

Проверить, что выражение является простым выражением
 - не содержит PPI::Statement::Compound
 - не содержит PPI::Structure::Block
=cut
sub is_simple_statement {
	my ($self, $ppiNode) = @_;
	my $not_simple = $self->find($ppiNode, \&is_not_simple_node);
	if ($not_simple) {return;}
	return 1;
}

=pod
=head2 is_not_simple_node $ppiNode

Проверить что вершина простая
=cut
sub is_not_simple_node {
    my ($ppiNode) = @_;
	if ($ppiNode->isa('PPI::Statement::Compound') ||
		$ppiNode->isa('PPI::Structure::Block') ||
		$ppiNode->isa('PPI::Statement::Given') ||
		$ppiNode->isa('PPI::Statement::When') ||
		$ppiNode->isa('PPI::Statement::Variable') ||
		$ppiNode->isa('PPI::Statement::Package') ||
		$ppiNode->isa('PPI::Statement::Include') ||
		$ppiNode->isa('PPI::Statement::Unknown') ||
		$ppiNode->isa('PPI::Statement::UnmatchedBrace') ||
		$ppiNode->isa('PPI::Statement::Break')) {
		return 1;
	}
	return;
}

=pod
=head2 find $ppiNode $sub

Найти вершину с использованием функции
=cut
sub find {
	my ($self, $ppiNode, $sub) = @_;
	if (&$sub($ppiNode)) {return $ppiNode;}
	if (!$ppiNode->isa("PPI::Node")) {return;}
	my $res;
	for my $child ($ppiNode->schildren) {
		$res = $self->find($child, $sub);
		if ($res) {return $res;}
	}
	return;
}

=pod
=head2 create_return $ppiNode

Проблема:
  - может быть: return something if expr;
                return something unless expr;
  - пока решаем так:
                return DinGo(ID, something) if expr;
                return DinGo(ID, something) unless expr;
=cut
sub create_return {
	my ($self, $ppiNode) = @_;
	my $return_mode = $self->{return_mode};
	if (!$ppiNode->isa("PPI::Statement")) {
		$self->debug_ppi_node($ppiNode, 'create_return node:  ', 'warn');
		$log->warn("Return выражение для функции не выражение");
	}
	my $treeNode = $ppiNode->{_treeNode};
	if (!$treeNode) {
		$self->debug_ppi_node($ppiNode, 'create_return node:  ', 'warn');
		$log->warn("Return для функции не содержит _treeNode");
	}

	###если return режим safe или в return выражении нет вызовов функций
	###то просто ставим сенсор перед
	if ($return_mode eq 'safe' || ($return_mode eq 'unsafe' && !$self->is_function_in_statement($ppiNode))) {
		if (!$treeNode->{SensorBeforeTheStatement}) {
			$treeNode->{SensorBeforeTheStatement} = $self->insert_sensor_before($treeNode->{ppiNode}, $treeNode)->{_sensorId};
		}
		return $self;
	}
	if ($return_mode eq 'unsafe') {
		$self->debug_ppi_node($ppiNode, 'Return with funcall: ', 'warn');
	}

	### по умолчанию ставим оборачивающий сенсор
    $self->{returnLogger}->log($ppiNode);
	my ($fchild, $has_label) = $self->get_first_child_without_label_expr($ppiNode);
    my $spaceStart = '';
	$self->debug_ppi_node($fchild, 'Create return: ');
	if ($fchild->content eq 'return') {
		$fchild = $fchild->snext_sibling();
        if ($fchild && $fchild->isa('PPI::Token::Structure') && $fchild->content eq ';') {
            $spaceStart = ' ';
        }
	}
	$self->debug_ppi_node($fchild, 'Create return: ');

	my $sText = 'Libsensor::DinGo(';
	if ($fchild && ($fchild->isa("PPI::Structure::List") || $fchild->isa("PPI::Token::QuoteLike::Words"))) {
		$self->debug_ppi_node($fchild, 'Return List: ', 'warn');
		$sText = 'Libsensor::DinGoList(';
	}
    $sText = $spaceStart . $sText;

	my $sens = PPI::Token::Word->new($sText);
	my $id = PPI::Token::Word->new($self->get_sensor_expression($treeNode->{ID}));
    my $opModuleId = PPI::Token::Operator->new(',');
    my $module_id_param = $self->get_module_id_param();
    my $moduleId = PPI::Token::Word->new("$module_id_param");
	my $op = PPI::Token::Operator->new(',');
	my $clsBrack = PPI::Token::Word->new(')');
	my $space = PPI::Token::Whitespace->new(' ');

	my $ppiModifier = $self->{ppiModifier};

	$ppiModifier->_check_result(
		$ppiNode->__insert_before_child($fchild, $sens),
		$fchild,
		$sText);
	$ppiModifier->_check_result(
		$ppiNode->__insert_before_child($fchild, $id),
		$fchild,
		"$treeNode->{ID}");
    $ppiModifier->_check_result(
		$ppiNode->__insert_before_child($fchild, $opModuleId),
		$fchild,
		',');
    $ppiModifier->_check_result(
		$ppiNode->__insert_before_child($fchild, $moduleId),
		$fchild,
		"$module_id_param");
	$ppiModifier->_check_result(
		$ppiNode->__insert_before_child($fchild, $op),
		$fchild,
		',');

	my $insertNode = $self->get_insert_close_break($ppiNode);
	if (!$insertNode) {
        $self->debug_ppi_node($ppiNode, 'Не закрытое выражение', 'warn');
        $log->warn(') будет поставлено в конце');
		#die 'Не закрытое выражение';
        $ppiModifier->_check_result(
            $ppiNode->add_element($clsBrack),
            $fchild,
            '<)>');
	} else {
		$self->debug_ppi_node($insertNode, 'Return close ) before: ');
		$ppiModifier->_check_result(
		$ppiNode->__insert_before_child($insertNode, $clsBrack),
		$fchild,
		'<)>');
		$ppiModifier->_check_result(
		$ppiNode->__insert_before_child($insertNode, $space),
		$fchild,
		'<space>');
	}
	$treeNode->{SensorBeforeTheStatement} = $treeNode->{ID};
	return $self;
}

=pod
=head2 is_function_in_statement $self $ppiNode

Проверить, что выражения содержит вызовы функций
Упрощенная версия fill_links_node
=cut
sub is_function_in_statement {
	my ($self, $ppiNode) = @_;
	my $symbolType;
	#my ($self, $node, $tnode, $need_fill, $has_set) = @_;
	my $result;
	my $node = $ppiNode;
	for my $child ($node->schildren()) {
		#использование символа
		if ($child->isa("PPI::Token::Symbol")) {
			$symbolType = $self->define_type($child);
			if ($symbolType eq 'function') {return 1;}
		}
		#вызов метода, если функция так вызывается можно считать, что она метод
		elsif ($child->isa("PPI::Token::Word")) {
			my $prev = $child->previous_sibling();
			my $next = $child->snext_sibling();
			if (($prev && $prev->isa("PPI::Token::Operator") && $prev->content() eq "->") ||
				($next && $next->isa("PPI::Structure::List"))) {
				return 1;
			}
		}
		if ($child->isa("PPI::Node")) {
			$result = $self->is_function_in_statement($child);
			if ($result && $result == 1) {
				return 1;
			}
		}
	}
	return;
}

=pod
=head2 get_insert_close_break $self, $ppiNode
       Получить места вставки закрывающей скобки
=cut
sub get_insert_close_break {
	my ($self, $ppiNode) = @_;
	#теперь пойдем с конца по дочерним, как встретим ; по идее крайний,
	#запомним, дальше если не встретим if, unless, вставим перед ; иначе
	#перед if, unless
	my $childCound = $ppiNode->schildren();
	my $curChild = $ppiNode->schild($childCound-1);
	my $firstSemi;
	my $result;
	while ($curChild) {
		if ($curChild->isa('PPI::Token::Structure') &&
				$curChild->content eq ';' && !$firstSemi
			) {$firstSemi = $curChild;}
		if ($curChild->isa('PPI::Token::Word') && ($curChild->content eq 'if') || ($curChild->content eq 'unless')) {
			return $curChild;
		}
		$curChild = $curChild->sprevious_sibling();
	}
	return $firstSemi;
}

sub hndl_include{
	my ($self, $tnode) = @_;
    $self->get_and_set_next_in_linear_block($tnode);
	#пока видимо ничего
	return $self;
}

sub hndl_package{
	my ($self, $tnode) = @_;
    $self->get_and_set_next_in_linear_block($tnode);
	#пока видимо тоже ничего
	return $self;
}

sub hndl_variables{
	my ($self, $tnode) = @_;
	$self->get_and_set_next_in_linear_block($tnode);
	#variables это expression,
	#нужно для переменных которые слева
	#заполнить присвоение значение (по умолчанию undef)
	#для переменных которые справа взятие значения
	#
	#нужно пройти по всем дочерним вершинам
	#для всех symbol после operator = нужно найти соответствующие переменные или функции
	#и проставить для них get
	my $node = $tnode->{ppiNode};
	$self->fill_links_node($node, $tnode);
	return $self;
}

sub fill_links_node {
	my ($self, $node, $tnode, $need_fill, $has_set) = @_;
	for my $child ($node->schildren()) {
		if (!$need_fill) {

			if (defined $has_set && $has_set == 1) {
				#если должно быть присваивание
				#использование символа
				if ($child->isa("PPI::Token::Symbol")) {
					$self->create_symbol_link($child, $tnode, "variable", "set");
				}
				#вызов метода, если функция так вызывается можно считать, что она метод
				elsif ($child->isa("PPI::Token::Word")) {
					my $prev = $child->previous_sibling();
					my $next = $child->snext_sibling();
					# if ($child->content eq "f") {
					# 	$self->debug_ppi_node($next, 'Next: ');
					# }
					if (($prev && $prev->isa("PPI::Token::Operator") && $prev->content() eq "->") ||
						($next && $next->isa("PPI::Structure::List"))) {
						$self->create_symbol_link($child, $tnode, "method");
					}
				}
			}

			if ($child->isa("PPI::Token::Operator") && (
					$child->content eq "=" ||
					$child->content eq "=~"))
			{
				$need_fill = 1;
			}
		} else {
			#использование символа
			if ($child->isa("PPI::Token::Symbol")) {
				$self->create_symbol_link($child, $tnode);
			}
			#вызов метода, если функция так вызывается можно считать, что она метод
			elsif ($child->isa("PPI::Token::Word")) {
				my $prev = $child->previous_sibling();
				my $next = $child->snext_sibling();
					# if ($child->content eq "f") {
					# 	$self->debug_ppi_node($next, 'Next: ');
					# }
				if (($prev && $prev->isa("PPI::Token::Operator") && $prev->content() eq "->") ||
						($next && $next->isa("PPI::Structure::List"))) {
					$self->create_symbol_link($child, $tnode, "method");
				}
			}
		}

        # если выражение eval {}, то ставим вызов анонимной функции
        # иначе разбираем дочерние вершины
        if ($child->isa('PPI::Structure::Block') &&
            $child->sprevious_sibling &&
            $child->sprevious_sibling->content eq 'eval') {
            $self->debug_ppi_node($child, 'fill links for anonymous', 'warn');
            $self->create_symbol_link($child, $tnode, 'function');
        } elsif ($child->isa("PPI::Node")) {
			$self->fill_links_node($child, $tnode, $need_fill, $has_set);
		}
	}
	return;
}

#создание ссылки при использовании переменной
#или функции,
#нужно определить что используется переменная или функция
#определяем как $, @, % переменная
#& или ничего значит функция
#еще как-то нужно определять методы вызываемые через -> ???
#потом находим соответствующую переменную, если нет
#то создаем новую
#и ставим ей link на get
sub create_symbol_link {
	my ($self, $node, $tparent, $type, $get_or_set) = @_;
	if (!defined $type) {
		$type = $self->define_type($node);
	}
	if ($type eq "variable") {
		$self->create_variable_link($node, $tparent, $get_or_set);
	} elsif ($type eq "method") {
		$self->create_method_link($node, $tparent, $get_or_set);
	} elsif ($type eq "function") {
		$self->create_function_link($node, $tparent, $get_or_set);
	} else {
        $log->error("Unknown symbol type: ");
        $self->debug_ppi_node($node, 'Ppi node with unknown type: ', 'error');
		#die "Unknown symbol type";
	}
	return $self;
}

sub create_variable_link {
	my ($self, $node, $tparent, $get_or_set) = @_;
	my $variables = $self->get_variables($node, $tparent);
	my $symbolName;
	if ($node->can('symbol')) {
		$symbolName = $node->symbol();
	} else {
		$symbolName = $node->content();
	}

	if ($variables) {
		#$log->info("Create link on $symbolName $variables");
	} else {
		$self->debug_ppi_node($node, 'Variabale that bring to exit.', 'error');
        $log->error('Что пошло не так переменная не получена, чего быть не должно');
        return $self;
		#die 'Что пошло не так переменная не получена, чего быть не должно';
	}


	#в StExcpression добавим ссылка на вызываемые переменные
	if (!$get_or_set || $get_or_set eq 'get') {
		push @{$tparent->{variableGetPosition}}, $variables;
	} else {
		push @{$tparent->{variableSetPosition}}, $variables;
	}

    #если у вершины нет _treeNode, то сохраним fileId, который потребуется при
    #генерации структуры для xml
    if (!$node->{_treeNode}) {
        $node->{_fileId} = $tparent->{fileId};
    }
	for my $var (@$variables) {
		if (!$get_or_set || $get_or_set eq 'get') {
			#print "var = $var  $var->{Name}\n";
			push @{$var->{ValueGetPosition}}, $node; #просто ссылка на ppi
		                                         #при генерации структуры сгенерим location
		} elsif ($get_or_set eq 'set') {
			push @{$var->{ValueSetPosition}}, $node; #просто ссылка на ppi
		                                         #при генерации структуры сгенерим location
		}
	}
	return $self;
}

#для функции get_or_set не имеет смысла
sub create_method_link {
	my ($self, $node, $parent, $get_or_set) = @_;
	$self->create_function_link($node, $parent, $get_or_set);
	return $self;
}

sub create_function_link {
	my ($self, $node, $tparent) = @_;
	my $functions = $self->get_functions($node, $tparent);
	#в StExcpression добавим ссылку на вызываемые переменные
	if ($functions) {
		$self->debug_ppi_node($tparent->{ppiNode}, 'Statement with Func: ');
		$log->debug('Function ', $node->content(), ' nodeType: ', $tparent->{nodeType});
	}
	push @{$tparent->{functionsCall}}, $functions;
	for my $var (@$functions) {
		push @{$var->{UsedOperations}}, $node;
		#просто ссылка на ppi
		#при генерации будем брать первую внешнюю операцию
		#и класть вызовы туда
	}
	return $self;
}

sub define_type {
	my ($self, $node) = @_;
	if ($node->isa("PPI::Token::Symbol")) {
		if ($node->raw_type() eq '&') {
			return "function";
		} else {
			return "variable";
		}
	} else {
		return "variable";
	}
}

=pod
=head2 get_variables $node

Попытаться найти определение переменной во внешнем блоке

#сначала попытаться найти нужную переменную в объемлющем блоке
#если получилось, то возвращаем ее, если нет,
#то возвращаем вообще все глобальные переменные с заданным именем
=cut
sub get_variables {
	my ($self, $node, $tparent) = @_;
	my $curnode = $node;
	my $prevSibl;
	#проходим сначала по previoussibling,
	#потом вверх
	#FIXME: такой алгоритм может быть достаточно долгим
	while ($curnode) {
		#$self->debug_ppi_node($curnode, 'getVariablesNode: ');
		if (
			($curnode->isa("PPI::Statement::Variable") && $curnode->{_treeNode}) ||
			($curnode->isa("PPI::Statement::Compound") && $curnode->{_treeNode})
			) {
			my $treeNode = $curnode->{_treeNode};
			my $var = $self->get_variable_from_tree_node($node, $treeNode);
			if ($var) {return [$var];}
		}
		$prevSibl = $curnode->sprevious_sibling();
		if (!$prevSibl) {
			$curnode = $curnode->parent;
		} else {
			$curnode = $prevSibl;
		}
	}
	my $gl = $self->get_global_symbol($node);
	if ($gl) {return $gl};
	#итак переменная найдена не была,
	#значит нужно ее добавить
	if (substr($node->symbol, 1) eq 'i') {
		$log->debug('defloc: ', $node->{_default_location}->[0], ' ', $node->{_default_location}->[1]);
		$self->debug_ppi_node($node, 'In: ');
		$self->debug_ppi_node($tparent->{ppiNode}, 'In: ');
	}
	my $var = $self->gen_variable($tparent->{ppiNode}, $tparent, $node, 'our');
	if (substr($node->symbol, 1) eq 'i') {
		$log->debug('defloc: ', $var->{location_start}->[0], ' ', $var->{location_start}->[1]);
		$self->debug_ppi_node($node, 'In: ');
		$self->debug_ppi_node($tparent->{ppiNode}, 'In: ');
	}

	return [$var];
}

sub get_variable_from_tree_node {
	my ($self, $node, $treeNode) = @_;
	for my $var (@{$treeNode->{variables}}) {
		#$log->debug("var = $var");
		if ($var->{Name} eq $node->symbol()) {
			return $var;
		}
	}
	return;
}

sub get_global_symbol {
	my ($self, $node) = @_;
	return $self->{global_vars}->{$node->symbol()};
}

sub get_functions {
	my ($self, $node, $tnode) = @_;

    #проверим сначала на то является ли node анонимной функцией
    if ($node->isa('PPI::Structure::Block') &&
        $node->{_treeNode} &&
        $node->{_treeNode}->{nodeType} eq 'Anonymous') {
        my $funcTnode = $node->{_treeNode};
        my $funcname = $funcTnode->{Name};
        my $funcs = $self->{sub_hash}->{$funcname};
        if (!$funcs) {
            $log->fatal('Anonymous fun ' . $funcname . ' missing in sub has' . "\n");
            return [];
        } else {
            $log->warn('functions hash founded');
        }

        return $funcs;
    }

	#$self->debug_hash_length($self->{sub_hash}, 'sub_hash');
	my $funcname = $node->isa("PPI::Token::Symbol") ?
		substr($node->symbol(), 1) : $node->content();
	# $log->warn($tnode->{ppiNode}->content);
	# $log->warn('Get function ', $funcname);
	my $funcs = $self->{sub_hash}->{$funcname};
	if ($funcs) {
		$log->debug('found count ', $#{$funcs});
		return $funcs;}

	my $func = $self->gen_st_tmplt($tnode->{ppiNode}, $tnode);
	$func->{nodeType} = 'Function';
	$func->{Name} = $funcname;
	$func->{Namespace} = '__UNDEFINED__'; # значит не найдено
	push @{$self->{sub_hash}->{$funcname}}, $func;
	return [$func];

	# if ($node->isa("PPI::Token::Symbol")) {
	# 	my $funcname = substr($node->symbol(), 1);
	# 	$log->debug('node->symbol: ', $funcname);
	# 	return $self->{sub_hash}->{$funcname};
	# } else {
	# 	return $self->{sub_hash}->{$node->content()};
	# }
}

sub hndl_variable{
	my ($self, $tnode) = @_;
	#на эти вершины не должны попасть
	return $self;
}


#обработка на данном этапе почти такая же как для
#variables с тем лишь исключением, что symbols которые стоят
#до операторов присваивания нужно делать link не в get а в set
#правда оператор присваивания может отсутствовать, тогда все variables
#нужно помечать как get
#fixme: может ли таких операторов быть несколько и что делать в этом случае ???
sub hndl_expression {
	my ($self, $tnode) = @_;
	$self->get_and_set_next_in_linear_block($tnode);

	my $node = $tnode->{ppiNode};
	my $assign = undef;

	#определяем есть ли в выражении присваивание
	my $operators = $node->find("Token::Operator");
	if ($operators) {
		for my $op (@$operators) {
			my $cnt = $op->content;
			if ($cnt eq '=' || $cnt eq '=~') {
				$assign = 1;
				last;
			}
		}
	}

	if ($assign) {
		#нужно заполнить линки в которых есть присваивание
		$self->fill_links_node($node, $tnode, undef, 1);
	} else {
		#заполнить линки, могут быть только взятия переменных
		$self->fill_links_node($node, $tnode, 1, undef);
	}
	return $self;
}


=pod
=head2 hdnl_if $self, $tnode, $iselseif

Обработать вершину if
если вершина compound то это просто if,
после обработки if, смотрим на следующее выражение, если это eslif,
то раскрываем как else {
  if () {

  }
}
и рекурсивно вызываем hndl_if

- условие первое выражение PPI::Structure::Condition
- следующее PPI::Structure::Block => then
- Если дальше elseif, создаем еще один if и рекурсия
- Если дальше else PPI::Structure::Block => else
=cut
sub hndl_if {
	my ($self, $tnode, $iselseif) = @_;
	my $ppiNode = $tnode->{ppiNode};

	#nextInLinearBlock нужно брать у compound
	#а это parent для if и для elseIf
	my $ifcompound = $tnode;
	if ($iselseif) {
		$ifcompound = $tnode->{ppiNode}->parent()->{_treeNode};
	}

	#устанавливать сразу нельзя, потому что для elseif ifcompound != tnode
	my $nextInLinearBlock = $self->get_next_in_linear_block($ifcompound->{ppiNode});
	if ($nextInLinearBlock) {
        $self->set_next_in_linear_block($tnode, $nextInLinearBlock);
	} else {
		$log->debug("nextInLinearBlock missing !");
	}
	$self->debug_ppi_node($ifcompound->{ppiNode}, "nextSt: ");

	#FIXME: может быть метка
	my ($lcondition, $has_label);
	if ($iselseif) {
		$lcondition = $ppiNode->snext_sibling();
	} else {
		($lcondition, $has_label) = $self->get_first_child_without_label($ppiNode);
	}
	if ($lcondition->isa("PPI::Structure::Condition")) {
		my $needExpression = $self->get_first_tree_node($lcondition);
		if (!$needExpression) {
			$self->debug_ppi_node($lcondition, 'needExpression: ', 'error');
            $log->error('что-то пошло не так, в if условии нет выражений');
			#die 'что-то пошло не так, в if условии нет выражений';
            return $self;
		}
		$tnode->{condition} = $needExpression->{ID};
	} else {
        $log->error('что-то пошло не так, if без условия.');
        $self->debug_ppi_node($ppiNode, 'If без условия: ', 'error');
		#die 'что-то пошло не так, if без условия';
        return $self;
	}

	#нужно взять block следующий за expression
	my $nextBlock = $lcondition->snext_sibling();
	if (!$nextBlock->isa('PPI::Structure::Block')) {
		$self->debug_ppi_node($nextBlock, 'If nextBlock: ', 'error');
        $log->error('что-то пошло не так, за if нет block');
		#die "что-то пошло не так, за if нет block";
        return $self;
	}

	my $thenExpr = $self->insert_sensor_next_block($nextBlock, $tnode);
	$tnode->{then} = $self->desc_tree_node($thenExpr);

	#слeдующее выражение либо else, либо elsif
	my $elseOfElsIf = $nextBlock->snext_sibling();

	# если не определено значит else, нет и просто выходим
	if (!$elseOfElsIf)  {return $self;}

	if ($elseOfElsIf->content eq "else") {
		my $elseBlock = $elseOfElsIf->snext_sibling();
		if (!$nextBlock->isa('PPI::Structure::Block')) {
			$self->debug_ppi_node($nextBlock, 'Else nextBlock: ', 'error');
            $log->error('что-то пошло не так, за else нет block');
			#die "что-то пошло не так, за else нет block";
            return $self;
		}
		my $nextSt = $self->insert_sensor_next_block($elseBlock, $tnode);
		$tnode->{else} = $self->desc_tree_node($nextSt);

	} elsif ($elseOfElsIf->content eq "elsif") {
		my $nextIf = $self->gen_st_tmplt($elseOfElsIf, $tnode);
		$nextIf->{nodeType} = "If";
        $self->add_to_st_list($nextIf);

		##нужно перекинуть children из if
		my $condition = $elseOfElsIf->snext_sibling();
		my $firstChild = $self->get_first_tree_node($condition);
		my $children = $tnode->{children};
		$nextIf->{children} = [];

		my $find;
		for my $child (@{$tnode->{children}}) {
			if (!$find &&  $child ==  $firstChild) {
				$find = 1;
				push @{$nextIf->{children}}, $child;
			} elsif ($find) {
				push @{$nextIf->{children}}, $child;
			}
		}
		$tnode->{else} = $self->desc_tree_node($nextIf);
		$self->hndl_if($nextIf, 1);
	}
	return $self;
}

=pod
=head2 get_first_tree_node $node

Получить первую ВНУТРЕННЮЮ вершину дерева
=cut
sub get_first_tree_node {
	my ($self, $node) = @_;
	if (!$node->isa("PPI::Node")) {return;}
	for my $ch ($node->schildren()) {
		#FIXME: нужно пропускать определения функций
		if ($ch->isa("PPI::Statement") &&
            !$self->is_block_compound_continue($ch) &&
            !$self->is_block_unsupported_compound($ch)) {
			return $ch->{_treeNode};
		}
		my $chTreeNode = $self->get_first_tree_node($ch);
		if (defined $chTreeNode) {return $chTreeNode;}
	}
	return;
}

=pod
=head2 is_block_compound_continue

Проверить что statement block с типом continue,
который по сути не влияет на порядок исполнения
=cut
sub is_block_compound_continue {
	my ($self, $node) = @_;
	return ($node->isa("PPI::Statement::Compound") && $node->type() eq "continue");
}

=pod
=head2 is_block_unsuppoted_compound

Проверить что statement block поддерживаемый compound, compound label сейчас не поддерживается
=cut
sub is_block_unsupported_compound {
	my ($self, $node) = @_;
    my @supported = qw(if while for foreach continue);
	return ($node->isa("PPI::Statement::Compound") &&
            !grep {$_ eq $node->type() } @supported);
}


=pod
=head2 get_tree_node $node

Получить вершину дерева начиная с заданной
Нужно чтобы понять на какую вершину ссылаться
(не обязательно внутреннюю)
=cut
sub get_tree_node {
	my ($self, $node) = @_;
	if (!$node->isa("PPI::Node")) {return;}
	if ($node->isa("PPI::Statement")) {return $node->{_treeNode};}
	for my $ch ($node->schildren()) {
		#FIXME: нужно пропускать определения функций ?
		if ($ch->isa("PPI::Statement") && !$self->is_block_compound_continue($ch)) {
			return $ch->{_treeNode};
		}
		my $chTreeNode = $self->get_first_tree_node($ch);
		if ($chTreeNode) {return $chTreeNode;}
	}
	return;
}

=pod
=head2 hndl_while $tnode

аналогично другим выражениям:
  - берем expression condition, потом первое внутреннее выражение
=cut
sub hndl_while {
	my ($self, $tnode) = @_;
	$self->get_and_set_next_in_linear_block($tnode);
	my $ppiNode = $tnode->{ppiNode};
	my ($cond, $has_label) = $self->get_first_child_without_label($ppiNode);

    if (!$cond->isa("PPI::Node")) {
        $self->debug_ppi_node($ppiNode, 'While ', 'warn');
        $self->debug_ppi_node($cond, 'Not node ', 'warn');
    }

    #поднимаем переменные из условия до уровня while
    if ($cond->isa("PPI::Node") &&
        scalar($cond->schildren) > 0 &&
        $cond->schild(0)->{_treeNode} &&
        ($cond->schild(0)->{_treeNode}->{variables}))  {
		$tnode->{variables} = $cond->schild(0)->{_treeNode}->{variables};
	}

	my $condLen = $cond->schildren();
	my $firstExpr;
	if ($cond) {
		$self->debug_ppi_node($cond, "cond: ");
	}
	if (!$cond->isa("PPI::Structure::Condition")) {
		$self->debug_ppi_node($cond, "While conditioin: ", 'error');
        $log->error('Что-то пошло не так у While нет () условия');
		#die "Что-то пошло не так у While нет () условия";
        return $self;
	}

	if ($condLen == 0) {
		$tnode->{condition} = 'true'; #как-то надо обозначить true
	} else {
		$self->fill_links_node($cond, $tnode);
		$firstExpr = $self->get_first_tree_node($cond);
		#FIXME: что делать если нет
		if ($firstExpr) {
			$tnode->{condition} = $firstExpr->{ID};
		}
	}

	#нужно взять block следующий за expression
	my $nextBlock = $cond->snext_sibling();
	$self->debug_ppi_node($nextBlock, "nextBlock: ");
	if (!$nextBlock->isa("PPI::Structure::Block")) {
        $self->debug_ppi_node($ppiNode, 'While без блока: ', 'error');
        $log->error('что-то пошло не так, за while() нет block');
		#die "что-то пошло не так, за while() нет block";
        return $self;
	}
	my $nextSt = $self->insert_sensor_next_block($nextBlock, $tnode);
	$tnode->{body} = $self->desc_tree_node($nextSt);
	return $self;
}

=pod
=head2 get_first_child_without_label $node

Получить первую значимую вершину не являющуюся меткой
=cut
sub get_first_child_without_label {
	my ($self, $node) = @_;
	my $ch0 = $node->schild(0);
	if ($ch0 && $ch0->isa("PPI::Token::Label")) {
		return ($node->schild(2), 1);
	} else {
		return ($node->schild(1), 0);
	}
}

=pod
=head2 get_first_child_without_label $node

Получить первую значимую вершину не являющуюся меткой
Для выражений
=cut
sub get_first_child_without_label_expr {
	my ($self, $node) = @_;
	my $ch0 = $node->schild(0);
	if ($ch0 && $ch0->isa("PPI::Token::LABEL")) {
		return ($node->schild(1), 1);
	} else {
		return ($node->schild(0), 0);
	}
}

=pod
=head2 hndl_block $tnode

На всякий случай запишем для него связи, но вообще он должен
пропускаться в дереве
=cut
sub hndl_block {
	my ($self, $tnode) = @_;
	$self->get_and_set_next_in_linear_block($tnode);
	my $node = $tnode->{ppiNode};
	my $nextBlock = $node->schild(0);
	my $nextSt = $self->get_first_tree_node($nextBlock);
	if ($nextSt) {
		$tnode->{firstStatement} = $nextSt;
		$tnode->{body} = $self->desc_tree_node($nextSt);
	}
	return $self;
}


=pod
=head2 hndl_for $tnode

#нужно взять первую вершину она будет PPI::Structure::For
#по ней получить start, codnition, iteration
#следующая вершина будет block, дальше как в while
=cut
sub hndl_for {
	my ($self, $tnode) = @_;
	$self->get_and_set_next_in_linear_block($tnode);
	my $ppiNode = $tnode->{ppiNode};
	my ($forStNode, $has_label) = $self->get_first_child_without_label($ppiNode);
	if (!$forStNode->isa("PPI::Structure::For")) {

		$self->debug_ppi_node($ppiNode, 'For без PPI::Structure::For: ', 'error');
		$self->debug_ppi_node($forStNode, 'Это должно быть PPI::Structure::For: ', 'error');
        $log->error('Что-то пошло не так For не содержит PPI::Structure::For.');
        return $self;

		#carp "Что-то пошло не так For не содержит PPI::Structure::For";
		#ppi не всегда правильно определяет for поэтому просто выводим сообщение об ошибке
		#что выводить в отладку
		#die "STFor";
		#return;
	}

    #если есть все части для for то вычисляем по простому
    #если нет, разбираем более подробно
    if (scalar($forStNode->schildren) == 3) {
        $tnode->{start} = $forStNode->schild(0)->{_treeNode}->{ID};
        #variables добавляем в for
        #FIXME: надо это как-то получше сделать
        if ($forStNode->schild(0)->{_treeNode} &&
            $forStNode->schild(0)->{_treeNode}->{variables})  {
            $tnode->{variables} = $forStNode->schild(0)->{_treeNode}->{variables};
        }

        $tnode->{condition} = $forStNode->schild(1)->{_treeNode}->{ID};
        $tnode->{iteration} = $forStNode->schild(2)->{_treeNode}->{ID};
    } else {
        $self->hndl_for_structure($tnode, $forStNode);
    }

	#copypaste if, while
	my $nextBlock = $forStNode->snext_sibling();
	if (!$nextBlock->isa('PPI::Structure::Block')) {
        $self->debug_ppi_node($ppiNode, 'For без block: ', 'error');
        $log->error('что-то пошло не так, за for нет block');
		#die "что-то пошло не так, за for нет block";
        return $self;
	}
	my $nextSt = $self->insert_sensor_next_block($nextBlock, $tnode);
	$tnode->{body} = $self->desc_tree_node($nextSt);
	return $self;
}

=pod
=head2 hdnl_for_structure
       обработка for структуры если какие-то части не заданы
=cut
sub hndl_for_structure {
    my ($self, $tnode, $forStructure) = @_;
    #должно быть в хотя бы 2 statement
    #3 statement возможно придется создать
    my $curpart = 0;
    my $propName = undef;

    for my $child ($forStructure->children) {
        if ($child->isa('PPI::Statement')) {
            if ($curpart < 3) {
                $propName = $self->for_structure_prop_by_num($curpart);
                $tnode->{$propName} = $child->{_treeNode}->{ID};
                $curpart ++;
            } else {
                $self->debug_ppi_node($tnode->{ppiNode}, 'For with more 3 statement: ', 'error');
                $log->error('More then 3 statement in for');
            }
        }
    }
    if ($curpart == 2) {
        #значит iteration не заполнен
        my $st = $self->gen_st_tmplt($forStructure->{finish}, $tnode);
        $st->{nodeType} = 'Null';
        $self->add_to_st_list($st);
        $tnode->{iteration} = $st->{ID};

    } elsif ($curpart < 2) {
        $self->debug_ppi_node($tnode->{ppiNode}, 'For with less 2 statement: ', 'error');
        $log->error('For node for less 2 statement');
    } elsif ($curpart > 3) {
        $self->debug_ppi_node($tnode->{ppiNode}, 'For with more 3 statement: ', 'error');
        $log->error('More then 3 statement in for');
    }
    return $self;
}

sub for_structure_prop_by_num {
    my ($self, $num) = @_;
    if ($num == 0) {return 'start';}
    elsif ($num == 1) {return 'condition';}
    elsif ($num == 2) {return 'iteration';}
    else {
        $log->error('More then 2 statement in for');
    }
    return 'unknown';
}

=pod
=head2 hndl_foreach

Обработка foreach
#нужно проверить есть ли my $name, если есть нужно определить
#объявление переменной
#нужно взять первый statement и проверить что он лежит внутри StructureList,
#если это так заполнить head
#body получаем как для if, for, while
=cut
sub hndl_foreach {
	my ($self, $tnode) = @_;
	$self->get_and_set_next_in_linear_block($tnode);
	my $ppiNode = $tnode->{ppiNode};
	$self->debug_ppi_node($ppiNode, 'Hndl Foreach: ');
	$tnode->{isCheckConditionBeforeFirstRun} = 1;

	my ($nextSign, $has_label) = $self->get_first_child_without_label($ppiNode);
	$self->debug_ppi_node($nextSign);

	#пытаемся распознать по чему делается проход
	my $listStruct = $nextSign;
	#если for my $some, то тут определение переменной
	#по идее мы еще не должны были обрабатывать выражения, где эта
	#переменная могла использоваться
	if ($nextSign->isa("PPI::Token::Word") && ($nextSign eq "my" || $nextSign eq "our")) {
		my $nextNextSign =$nextSign->snext_sibling();
		$self->debug_ppi_node($nextNextSign, '{foreach}nextNextSign: ');
		if ($nextNextSign->isa("PPI::Token::Symbol")) {
			my $vars = $self->gen_st_tmplt($nextSign, $tnode);
			$vars->{nodeType} = "Variables";
			$vars->{variables} = [];
			# может $nextNextNode
			# иначе for будет по переменной $_, как это выделять ?
			my $var = $self->gen_variable($nextSign, $vars, $nextNextSign, "my");
			push @{$vars->{variables}}, $var;
			push @{$tnode->{variables}}, $var; #FIXME: предыдущее наверно избыточно
			$listStruct = $nextNextSign->snext_sibling();
		}
	} elsif ($nextSign->isa("PPI::Token::Symbol")) {
		$listStruct = $listStruct->snext_sibling();
	} else {
		#$listStruct = $listStruct->snext_sibling();
	}

	if (!$listStruct->isa('PPI::Structure::List')) {
		$self->debug_ppi_node($listStruct, 'Hndl foreach listStruct: ', 'error');
        $log->error('Что-то пошло не так для foreach нет списка по которому делается перебор');
		#die "Что-то пошло не так для foreach нет списка по которому делается перебор";
        if (!$listStruct->isa('PPI::Structure::For')) {
            $log->error('Список не определился и как PPI::Structure::For.');
            return $self;
        }
	}

	my $headNode = $self->get_first_tree_node($listStruct);
	if (!defined $headNode || !$headNode) {
		$self->debug_ppi_node($headNode, 'Foreach без списка: ', 'error');
        $log->error('Что-то пошло не так для foreach в списке цикла ничего нет');
        return $self;
		#die "Что-то пошло не так для foreach в списке цикла ничего нет";
	}
	$tnode->{head} = $headNode->{ID};

	#copypaste if, while, for
	my $nextBlock = $listStruct->snext_sibling();
	if (!$nextBlock->isa("PPI::Structure::Block")) {
        $self->debug_ppi_node($ppiNode, 'Foreach без блока: ', 'error');
		$log->debug("nextBlock: ", ref $nextBlock, ' ', $self->first_cont_line($nextBlock));
        $log->error('что-то пошло не так, за for нет block');
        return $self;
		#die "что-то пошло не так, за for нет block";
	}
	my $nextSt = $self->insert_sensor_next_block($nextBlock, $tnode);
	$tnode->{body} = $self->desc_tree_node($nextSt);
	return $self;
}


=pod
=head2 hndl_break обработка break оператора

Нужно:
 - breakingLoop

Просто: пойти вверх до цикла
Проблемы:
  1. часть циклов не обнаруживаются,
  2. у цикла может быть метка: last LABEL

Проблема 1 решается нахождением недостающих циклов,
 чего пока нет и следовательно воспользуемся простым вариантом.

Ссылку на loop кладем в outerLoop =>
 - будет одинаковый обработчик для break(last), continue(next, redo)
 - breakingLoop, continuingLoop проставить при генерации
=cut
sub hndl_break{
	my ($self, $tnode) = @_;
	$self->get_and_set_next_in_linear_block($tnode);
	if ($self->{level} <= 2) {
		if (!$tnode->{SensorBeforeTheStatement}) {
			$tnode->{SensorBeforeTheStatement} = $self->insert_sensor_before($tnode->{ppiNode}, $tnode)->{_sensorId};
		}
	}

	my $cur_node = $self->get_outer_loop($tnode);
	if ($cur_node) {
		$tnode->{outerLoop} = $self->desc_tree_node($cur_node);
	} else {
		#ошибка внешний цикл не найден
        #Можно помечать так:
		# $tnode->{outerLoop} = {
		# 	ID => -1,
		# 	kind=>"STOperational" # NotFound
		# };
        #сейчас не заполняем.
		$self->debug_ppi_node($tnode->{ppiNode}, 'Hndl Break: ', 'warn');
		$log->warn('Hndl Break outerLoop Not Found.');
	}
	return $self;
}

=pod
=head2 get_outer_loop $tnode

Получить внешний цикл,
#FIXME: возможно более правильно идти по ppi вершинам
=cut
sub get_outer_loop {
	my ($self, $tnode) = @_;
	my $cur_node = $tnode;
	$self->debug_ppi_node($tnode->{ppiNode}, 'Get outerloop for: ');
	while ($cur_node) {
		if ($cur_node->{nodeType} eq 'For' ||
			$cur_node->{nodeType} eq 'Foreach' ||
			$cur_node->{nodeType} eq 'While') {
			$self->debug_ppi_node($tnode->{ppiNode}, 'Finded outer loop: ');
			return $cur_node;
		}
		$cur_node = $cur_node->{treeParent};
		if ($tnode->{ppiNode}) {
			$self->debug_ppi_node($tnode->{ppiNode}, 'Check outer loop: ');
		}
	}
	return ;
}

=pod
=head2 get_and_set_next_in_linear_block $tnode

Получить следующую линейную вершину и установить
на нее ссылку
=cut
sub get_and_set_next_in_linear_block {
	my ($self, $tnode) = @_;
	my $ppiNode = $tnode->{ppiNode};
	my $sn = $ppiNode;
	if ($sn) {
		my $snt = $self->get_next_in_linear_block($sn);
		#вершина end не является выражением, а
		#скорее идентификатором конца файла
        return $self->set_next_in_linear_block($tnode, $snt);
	}
	return ;
}

=pod
=head2 set_next_in_linear_block

Установить NextInLinearBlock выполнив проверку что следующий
элемент не __END__
=cut
sub set_next_in_linear_block {
    my ($self, $tnode, $next) = @_;
    if ($next && $next->{nodeType} eq '__END__') {return ;}
    if ($next) {
        $tnode->{NextInLinearBlock} = $self->desc_tree_node($next);
    }
    return $next;
}

=pod
=head2 get_next_in_linear_block $node

Получить вершину treeNode следующую в линейном блоке
за заданной ppi вершиной
=cut
sub get_next_in_linear_block {
	my ($self, $node) = @_;
	#берем следующую вершину за заданной
	#и ищем следующее выражение
	my $cur_node = $node->next_sibling();
	my $st = undef;
	while ($cur_node) {
		$st = $self->get_tree_node($cur_node);

        #пропускаем не поддерживаемые compound
        #да данный момент такое встречено в if
        #см. тест LabelBeforeIfTest.pm
        if ($st && $st->{nodeType} eq 'UnsupportedCompound') {
            $cur_node = $cur_node -> next_sibling();
            next;
        }

		if ($st && $st->{nodeType} eq 'Block') {

            # block может быть еще не обработан,
            # поэтому firstStatement может быть еще не задан
            # ищем его если его еще нет
            my $first = $st->{firstStatement};
            if (!$first) {
                my $nextBlock = $st->{ppiNode}->schild(0);
                my $nextSt = $self->get_first_tree_node($nextBlock);
                if ($nextSt) {
                    $first = $nextSt;
                }
            }

			if ($first) {
				#была ошибка проверяем, чтобы бала variables, а не variable
				if ($st->{nodeType} eq "Variable") {
					$self->debug_ppi_node($node, 'node for nextInLinearVariable: ');
					$self->debug_ppi_node($st->{ppiNode}, 'nextInLinearVariable: ');
					die "Variable";
				}
				return $first;
			} else {
				$cur_node = $cur_node->next_sibling();
				next;
			}
		} # elsif ($st && ($st->{nodeType} eq "Function" || $st->{nodeType} eq "Scheduled"
		# 				 || $st->{nodeType} eq "Include")
		# 	) {
		# 	$cur_node = $cur_node->snext_sibling();
		# 	next;
		# }

        #FIX: в perl nextInLinearBlock может быть STCase
        #     если мы их добавим, то генератор xml упадет, поэтому просто будем пропускать
        #     до первого не STCase
		if ($st && $st->{nodeType} ne 'Case') {
			#была ошибка проверяем, чтобы бала variables, а не variable
			if ($st->{nodeType} eq "Variable") {
				$self->debug_ppi_node($node, 'node for nextInLinearVariable: ', 'warn');
				$self->debug_ppi_node($st->{ppiNode}, "nextInLinearVariable: ", 'warn');
				die "Variable";
			}
			return $st;
		}
		$cur_node = $cur_node->next_sibling();
	}

    # если мы находимся внутри 'Block' {}, который не является частью какой-нибудь
    # конструкции, то этот block не будет отражен в потоке выполнения
    # поэтому если для вершины не найдена nextInLinearBlock
    # и она находится в таком блоке, нужно взять nextInLinearBlock
    # от такого блока
    my $parent = $node->parent;
    if (!$parent) { return; }
    my $parent_statement = $parent->parent;
    if (!$parent_statement) { return; }

    if ($parent->isa('PPI::Structure::Block') &&
        $parent_statement->schildren == 1 &&
        $parent_statement->isa('PPI::Statement::Compound') &&
        $parent_statement->{_treeNode} &&
        $parent_statement->{_treeNode}->{nodeType} eq 'Block' &&
        $parent_statement->{_treeNode}->{NextInLinearBlock}) {
        my $node_id = $parent_statement->{_treeNode}->{NextInLinearBlock}->{ID};
        return $self->{ids}->{$node_id};
    }

	return;
}

sub hndl_continue{
	my ($self, $tnode) = @_;
    $self->hndl_break($tnode);
	return $self;
}

#для анализа по идее нет разницы между redo и continue
sub hndl_redo {
	my ($self, $tnode) = @_;
    $self->hndl_continue($tnode);
	return $self;
}

=pod
=head2 hndl_goto $tnode

Обработка выражения goto
Возможно три случая:
  - goto LABEL; # перейти на метку ищем метку
  - goto &func; # перейти на функцию ищем функцию
  - got EXPR;   # перейти по выражению, не обрабатываем
=cut
sub hndl_goto {
	my ($self, $tnode) = @_;
	$self->get_and_set_next_in_linear_block($tnode); # ???
	if (!$tnode->{SensorBeforeTheStatement}) {
		$tnode->{SensorBeforeTheStatement} = $self->insert_sensor_before($tnode->{ppiNode}, $tnode)->{_sensorId};
	}

	my $ppiNode = $tnode->{ppiNode};
	$self->debug_ppi_node($ppiNode);

	#определим тип goto
	my $fChild = $ppiNode->schild(0);
	$log->debug('hndl firstChild:', $self->first_cont_line($fChild));

	my $nextNode =  $fChild->snext_sibling();
	my $nextNextNode = $nextNode->snext_sibling();
    if (!!$nextNextNode) {
        $log->debug('nextNextNode:', ref $nextNextNode, ' ', $self->first_cont_line($nextNextNode));
    }
	if ($nextNode->isa("PPI::Token::Word") &&
        (!$nextNextNode ||
         ($nextNextNode->isa("PPI::Token::Structure") &&
          $nextNextNode->content eq ";")
        )) {
		$log->debug("first type of goto");

		# 1-ый тип goto LABEL;
		my $label_node = $nextNode->content();
		my $tree = $self->{trees}->{$self->{cur_file_id}};
		my $labels = $tree->{labels};

		$log->debug('label_node = ', $label_node, ' labels ', $labels);

		#нужно брать первую label из того-же scope
		my $tn = undef;
		if ($labels->{$label_node}) {
			for my $lnode (@{$labels->{$label_node}}) {
				$self->debug_ppi_node($lnode->{ppiNode}, 'label: ');

                if ($self->is_block_unsupported_compound($lnode->{ppiNode})) {
                    next;
                }

				if ($self->is_same_scope_goto($lnode->{ppiNode}, $ppiNode)) {
					$tn = $lnode;
                    #FIX: по идее для labels сразу стоит сохранять внутреннюю вершину
                    #     но когда создается labels,
                    #     взять первую tree_node может быть еще не возможно
                    if ($tn->{nodeType} eq 'Block') {
                        $tn = $self->get_first_tree_node($tn->{ppiNode});
                        if (!$tn) { last; }
                    }
					my $kind = $self->get_tnode_kind($tn);
					$log->debug('Node find ID', $tn->{ID}, ' kind = ', $kind,
								' ', $self->first_cont_line($tn->{ppiNode}));
					$tnode->{destination} = {
						ID=>$tn->{ID},
						kind=> $kind
					};
					last;
				}
			}
		}
		if (!$tn) {
			my $id = $tnode->{ID};
			$log->info("Node with label for $id was not found");
            my $destination = $self->get_first_tree_node($ppiNode);
            if ($destination) {
                $tnode->{destination} = $self->desc_tree_node($destination);
            }
			# $tnode->{destination} = {
			# 	ID=>-1,
			# 	kind=>"STOperational" #NotFound
			# };
			$self->debug_ppi_node($ppiNode, 'Hndl Goto: ', 'warn');
			$log->warn('Hndl Goto destination Not Found');
		}
		$tnode->{gototype} = 1;
		#нужно найти вершину на которую она ссылается
		#нужно пройти по все satement с labels и заданным именем,
		#так мы определим потенциальные вершины

	} elsif ($nextNode->isa("PPI::Token::Symbol") &&
			 $nextNode->symbol_type eq '&' &&
			 (!$nextNextNode || ($nextNextNode->isa("PPI::Token::Structure") &&
			 $nextNextNode->content eq ";"))) {
		# 3-ий тип goto &func;
		# !!FIXME: функцию конечно нужно искать не так,
		#
		$log->debug("third type of goto");

		my $functions = $self->get_functions($nextNode, $tnode);

		#создадим ссылку на функцию, и будем считать
		#что goto sum <==> return &sum(@_);
		$self->create_function_link($nextNode, $tnode);
		my $func = $functions->[0];
		if ($func && $func->{children} && $func->{children}->[0]) {

			#!!!FIXME
			my $tn = $func->{children}->[0];
			my $kind = $self->get_tnode_kind($tn);
			$log->debug('Node finded ID', $tn->{ID}, ' kind = ', $kind,
						' ', $self->first_cont_line($tn->{ppiNode}));

            my $st = $self->gen_st_tmplt($nextNode, $tnode);
            $st->{nodeType} = 'Expression';
            $self->add_to_st_list($st);
            push @{$st->{functionsCall}}, $functions;
            $tnode->{ReturnOperation} = $st->{ID};
            $tnode->{nodeType} = 'Return';
            $tnode->{defaultNodeType} = 'Goto';
			# $tnode->{destination} = {
			# 	ID=>$func->{children}->[0]->{ID},
			# 	kind =>$self->get_tnode_kind($func->{children}->[0])
			# }
		} else {
			# $tnode->{destination} = {
			# 	ID => -1,
			# 	kind => "STOperational" # NotFound
			# };
            my $destination = $self->get_first_tree_node($ppiNode);
            if ($destination) {
                $tnode->{destination} = $self->desc_tree_node($destination);
            }
			$self->debug_ppi_node($ppiNode, 'Hndl Goto: ', 'warn');
			$log->warn('Hndl Goto destination Not Found');
		}
        $tnode->{gototype} = 3;
	} else {
		$tnode->{gototype} = 2;
		#2-ой тип goto EXPR;
		#не обрабатываем вообще
		$log->debug("second type of goto");
        my $destination = $self->get_first_tree_node($ppiNode);
        if ($destination) {
            $tnode->{destination} = $self->desc_tree_node($destination);
        }
		# $tnode->{destination} = {
		# 	ID => -1,
		# 	kind => "STOperational" # NotFound
		# };
		$self->debug_ppi_node($ppiNode, 'Hndl Goto: ', 'warn');
		$log->warn('Hndl Goto destination Not Found');
	}

	return $self;
}

=pod
=head2 is_same_scope_goto
       Проверяем, что вершины node1, node2 находится в одной области видимости
       #две вершины в одной области видимости
	   #нужно найти первого общего родителя
	   #потом построить путь до каждой из вершин и проверить,
	   #что набор вершин sub, compound (for, while, foreach) один и тот же
=cut
sub is_same_scope_goto {
	my ($self, $node1, $node2) = @_;
	$log->debug("Run is same scope goto");
	my $curnode = $node1;
	my @path = ();
	#$curnode наименьший общий предок;
	while ($curnode) {
		if ($curnode->contains($node2)) {
			last;
		}
		$self->debug_ppi_node($curnode);
		if ((refaddr $curnode != refaddr $node1) &&
			($curnode->isa("PPI::Statement::Sub") ||
			($curnode->isa("PPI::Statement::Compound") && $curnode->type ne "if"))) {
			push @path, $curnode;
		}
		$curnode = $curnode->parent;
	}
	if (!!@path) {@path = reverse @path;}
	$log->debug('find first parent ', $self->first_cont_line($curnode));
	$self->debug_node_path(\@path);

	#если общий предок найден,
	#а это как минимум корень документа
	#нужно построить путь до него из другой вершины и сравнить пути
	#путь до goto должен совпадать с путем до метки, но если он содержит
	#дополнительные части пути, то тоже подойдет, если там нет sub
	if ($curnode) {
		my @path2 = @{$self->get_path_to_parent_goto($curnode, $node2)};
		$self->debug_node_path(\@path2);
		if ($#path <= $#path2) {
			for (my $i=0; $i < $#path; $i++) {
				if (refaddr($path[$i]) != refaddr($path2[$i])) {
					return;
				}
			}
			#проверка что в более длинной части пути нет sub
			for (my $i=$#path; $i < $#path2; $i++) {
				if ($path2[$i]->isa("PPI::Statement::Sub")) {
					return;
				}
			}
			return 1;
		} else {
			return;
		}
	}
	return;
}

=pod
=head2 get_path_to_parent_goto
       Получить путь до вершины upto из вершины node
=cut
sub get_path_to_parent_goto {
	my ($self, $upto, $node) = @_;
	my $curnode = $node;
	my @path;
	while ($curnode && refaddr($curnode) != refaddr($upto)) {
		if ($curnode->isa("PPI::Statement::Sub") ||
			($curnode->isa("PPI::Statement::Compound") && $curnode->type ne "if")) {
			push @path, $curnode;
		}
		$curnode = $curnode->parent;
	}
	if (!$curnode) {return;}
	@path = reverse @path;
	return \@path;
}

#нужно заполнить returnOperation
#берем все до ; заворачиваем в expression и разбираем
sub hndl_return {
	my ($self, $tnode) = @_;
    my $emptyReturn = 0;
	$self->get_and_set_next_in_linear_block($tnode);
	my $ppiNode = $tnode->{ppiNode};
	my ($first, $has_label) = $self->get_first_child_without_label_expr($ppiNode);
	#FIXME: в ряде случаев тут сложное выражение
	if ($first && $first->isa("PPI::Node")) {
		$self->fill_expression_links($tnode);
	}

    my $retNode = $ppiNode->schild(0);
	my $startSt = $retNode->snext_sibling;

    if (!$startSt) {
        $startSt = $retNode;
        $emptyReturn = 1;
    }
	#FIXME: нужно наверно в первый проход делать
	my $tempNode = $self->gen_st_tmplt($startSt, $tnode);
	$tempNode->{nodeType} = "Expression";
	#FIXME: если token нужно ли что-то делать
	#нужно как-то завернуть в expression
	if (!$startSt->isa("PPI::Token")) {
		$self->hndl_expression($tempNode);
	}
	#возможно тут должна быть ссылку сразу на operation
	#без промежуточного STOpertaion
	$tnode->{ReturnOperation} = $tempNode->{ID};


    if ($emptyReturn == 1) {
        if (!$tnode->{SensorBeforeTheStatement}) {
			$tnode->{SensorBeforeTheStatement} = $self->insert_sensor_before($tnode->{ppiNode}, $tnode)->{_sensorId};
		}
    } else {
        $self->create_return($ppiNode);
    }
	return $self;
}

sub fill_expression_links {
	my ($self, $tnode) = @_;
	my $node = $tnode->{ppiNode};
	my $assign;
	#определяем есть ли в выражении присваивание
	my $operators = $node->find("Token::Operator");
	if ($operators) {
		for my $op (@$operators) {
			my $cnt = $op->content;
			if ($cnt eq '=' || $cnt eq '=~') {
				$assign = 1;
				last;
			}
		}
	}

	if ($assign) {
		#нужно заполнить линки в которых есть присваивание
		$self->fill_links_node($node, $tnode, undef, 1);
	} else {
		#заполнить линки, могут быть только взятия переменных
		$self->fill_links_node($node, $tnode, 1, undef);
	}
	return $self;
}

sub hndl_switch {
	my ($self, $tnode) = @_;
	#нужно заполнить head
	$self->get_and_set_next_in_linear_block($tnode); # ???
	my $ppiNode = $tnode->{ppiNode};
    my ($structGiven, $has_label) = $self->get_first_child_without_label($ppiNode);
	if (!$structGiven->isa('PPI::Structure::Given')) {
		$self->debug_ppi_node($ppiNode, 'Given Node:  ', 'warn');
        $self->debug_ppi_node($structGiven, 'Given Node:  ', 'warn');
		$log->warn("Что-то пошло не так у given нет структуры given");
        return $self;
		#die ?
	}

	#заполни связи
	$self->fill_links_node($structGiven, $tnode);
	return $self;
}

=pod
=head2 hndl_case $tnode
 * If given was found for case than place it to case.
 * If for,foreach was found for case, than when should be handled as if (TODO).
      default shoud be equal to if (1) { }
 * TODO handle:  {body} when {condition}, such construction doesn't detected
                 by the ppi and handled as statement and when as handled as
                 a variable..
=cut
sub hndl_case {
    # Если для case нет given обрабатываем в соответствующей функциию.
    # Иначе заполняем:
    # - condition (для case).
    # - body
    # Получаем родительский given и добавляем вершину к
    # соответствующему типу.
    #
	#В when break ставится автоматом, для перехода к следующему шагу нужно ставить continue.
	# Поскольку считаем, что case выполняются в произвольном порядке порядок игнорируем,
	# нужно только следить, чтобы они не использовались как переменные.
	my ($self, $tnode) = @_;
	my $ppiNode = $tnode->{ppiNode};

     # if case node has no parent or parent is for, foreach
    if (!$ppiNode->{caseType} || $ppiNode->{caseType} eq 'for') {
        return $self->hndl_case_as_if($tnode);
    }

    my ($type, $expr, $body, $when, $caseOwner) = ('case');
	if ($ppiNode->schild(0)->content eq 'default') {
		$type = 'default';
		$body = $ppiNode->schild(1);
	} else {
		$when = $ppiNode->schild(1);
		if ($when->isa('PPI::Structure::When')) {
			$expr = $self->get_first_tree_node($when);
			$tnode->{condition} = $expr;
			$self->fill_links_node($when, $tnode);
		}
		$body = $ppiNode->schild(2);
	}

	if ($body->isa('PPI::Structure::Block')) {
		$body = $self->get_first_tree_node($body);
        $tnode->{body} = $body; # NOTE: body только для block
	};

    if (!$tnode->{body}) {
        $self->debug_ppi_node($ppiNode, 'Case without body: ', 'error');
    }
    $caseOwner = $tnode->{caseOwner};
    if (!$caseOwner || !$caseOwner->isa('PPI::Statement::Given')) {
        $self->debug_ppi_node($ppiNode, 'Given for case missing or incorrect: ', 'error');
        return;
    }
    push @{$caseOwner->{_treeNode}->{$type}}, $tnode;
	return $self;
}

=pod
=head2 hndl_case_as_if $tnode
       Handle when(case) statement such as "if statement" without then of elif.
       Such handling need for when that are in the for,foreach statements or that has no for.
=cut
sub hndl_case_as_if {
    my ($self, $tnode) = @_;
	my $ppiNode = $tnode->{ppiNode};
    my ($lcondition, $has_label);
    my $nextInLinearBlock = $self->get_next_in_linear_block($ppiNode);

    if ($nextInLinearBlock) {
        $self->set_next_in_linear_block($tnode, $nextInLinearBlock);
	} else {
		$log->debug('nextInLinearBlock missing !');
	}
    my ($type, $expr, $body, $when, $caseOwner) = ('case');
	if ($ppiNode->schild(0)->content eq 'default') {
		$type = 'default';
        my $st = $self->gen_st_tmplt($ppiNode, $tnode);
        $st->{nodeType} = 'Null';
        $self->add_to_st_list($st);
        $tnode->{condition} = $st->{ID};
        ($lcondition, $has_label) = $self->get_first_child_without_label_expr($ppiNode);
	} else {
		$when = $ppiNode->schild(1);
		if ($when->isa('PPI::Structure::When')) {
            ($lcondition, $has_label) = $self->get_first_child_without_label($ppiNode);
            if ($lcondition->isa('PPI::Structure::When')) {
                my $needExpression = $self->get_first_tree_node($lcondition);
                if (!$needExpression) {
                    $self->debug_ppi_node($lcondition, 'needExpression: ', 'error');
                    $log->error('что-то пошло не так, в when условии нет выражений');
                    return $self;
                }
                $tnode->{condition} = $needExpression->{ID};
            } else {
                $log->error('что-то пошло не так, when без условия.');
                $self->debug_ppi_node($ppiNode, 'When без условия: ', 'error');
                $self->debug_ppi_node($lcondition, 'Condition: ', 'error');
                return $self;
            }
		}
		$body = $ppiNode->schild(2);
	}
    #нужно взять block следующий за expression
	my $nextBlock = $lcondition->snext_sibling();
	if (!$nextBlock->isa('PPI::Structure::Block')) {
		$self->debug_ppi_node($nextBlock, 'When nextBlock: ', 'error');
        $log->error('что-то пошло не так, за when нет block');
        return $self;
	}
	my $thenExpr = $self->insert_sensor_next_block($nextBlock, $tnode);
	$tnode->{then} = $self->desc_tree_node($thenExpr);
    return $self;
}

sub hndl_null{
	my ($self, $tnode) = @_;
    $self->get_and_set_next_in_linear_block($tnode);
    # обработка пока не требуется
	return $self;
}
sub hndl_error{
	my ($self, $tnode) = @_;
	# ошибка
	return $self;
}

sub hndl_statement {
    return &hndl_expression(@_);
}

=pod

=head2 insert_sensor_next_block $nextBlock $treeNode $level

Метод используется для вставки сенсора в блок, перед ветвлением,
и вставки ссылки на него в следующее выражение
=cut
sub insert_sensor_next_block {
	my ($self, $nextBlock, $treeNode, $level) = @_;
	$level = $level || 2;
	#получается senor должен быть вставлен сюда
	#при для $nextSt должен быть прописан SensorBeforeTheStatement
	my $nextSt = $self->get_first_tree_node($nextBlock, $treeNode);
	if (!$nextSt) {
		my $nullNode = $self->insert_null_into($nextBlock); #вставляем null statement
		$self->debug_ppi_node($nullNode, 'nullNode: ');
		my $nullTnode = $self->gen_st_tmplt($nullNode, $treeNode);
		$nullTnode->{"nodeType"} = "Null";
		$nullTnode->{"inserted"} = "true";

		# привязывает на nextBlock
		$log->debug("default_location line = ", $nextBlock->{_default_location});
		$nullTnode->{location_start} = $nextBlock->{_default_location};
		my @def_loc = @{$nextBlock->{_default_location}};
		$def_loc[1] += length($nullTnode->{ppiNode}->content);
		$def_loc[2] += length($nullTnode->{ppiNode}->content);
		$nullTnode->{location_finish} = \@def_loc;
		$nullTnode->{ppiNode}->{_default_location} = $nextBlock->{_default_location};

		push @{$self->{st_list}}, $nullTnode;
		$log->debug('null_node ', $nullTnode->{ID}, ' inserted');
		$self->debug_ppi_node($nullTnode->{ppiNode}, 'Null node: ');
		$nextSt = $self->get_first_tree_node($nextBlock, $treeNode);
		$self->debug_ppi_node($nextSt->{ppiNode}, 'Nextst: ');
		if (!$nextSt || (refaddr $nextSt != refaddr $nullTnode)) {
			$self->debug_ppi_node($nextBlock, 'NextBlock ', 'warn');
			die "Как-то только что вставленная вершина не стала firstTreeNode";
		}
	}
	if ($self->{level} <= $level) {
		#вставляем sensor, только в случае если его еще нет
		#my ($fch, $has) =$self->get_first_child_without_label_expr($nextSt->{ppiNode});
		#$self->debug_ppi_node($fch, 'fch: ', 'warn');
		if (!$nextSt->{SensorBeforeTheStatement} &&
			(!$nextSt->{ppiNode}->isa('PPI::Statement::Break')
			|| ($self->get_first_child_without_label_expr($nextSt->{ppiNode}))[0] ne 'return')) {
			my $node = $self->insert_sensor_into($nextBlock, $nextSt->{ID});
			$nextSt->{SensorBeforeTheStatement} = $nextSt->{ID};
		}
	}
	return $nextSt;
}

sub insert_null_tnode_into_block {
	my ($self, $ppiNode, $treeNodeParent, $end) = @_;
	my $nullNode;
	if (!$end) {
		$nullNode = $self->insert_null_into($ppiNode); #вставляем null statement
	} else {
		$nullNode = $self->insert_null_into_end($ppiNode); #вставляем null statement
	}
	$self->debug_ppi_node($nullNode, 'nullNode: ');
	my $nullTnode = $self->gen_st_tmplt($nullNode, $treeNodeParent);
	$nullTnode->{"nodeType"} = "Null";
	$nullTnode->{"inserted"} = "true";
	# привязывает на nextBlock
	$nullTnode->{location_start} = $ppiNode->{_default_location};
	my @def_loc = @{$ppiNode->{_default_location}};
	$def_loc[1] += length ($nullTnode->{ppiNode}->content);
	$def_loc[2] += length ($nullTnode->{ppiNode}->content);
	$nullTnode->{location_finish} = \@def_loc;
	$nullTnode->{ppiNode}->{_default_location} = $ppiNode->{_default_location};
	$log->debug("null location start = ", $ppiNode->{_default_location});

	$log->debug('null_node ', $nullTnode->{ID}, ' inserted');
	$self->debug_ppi_node($nullTnode->{ppiNode}, 'Null node: ');

	push @{$self->{st_list}}, $nullTnode;
	return $nullTnode;
}

sub insert_sensor_before {
	my $self = shift;
	return $self->{ppiModifier}->insert_sensor_before(shift, shift->{ID}, shift);
}

sub insert_sensor_into {
	my $self = shift;
	return $self->{ppiModifier}->insert_sensor_into(@_);
}

sub insert_null_into {
	my $self = shift;
	return $self->{ppiModifier}->insert_null_into(@_);
}

sub insert_use_libsensor {
	my $self = shift;
	return $self->{ppiModifier}->insert_use_libsensor(@_);
}

sub insert_sensor_into_end {
	my $self = shift;
	return $self->{ppiModifier}->insert_sensor_into_end(@_);
}

sub insert_null_into_end {
	my $self = shift;
	return $self->{ppiModifier}->insert_null_into_end(@_);
}

sub need_sensor {
	my ($self, $node) = @_;
	my $curnode = $node;
	while ($curnode) {
		if ($curnode->isa("PPI::Structure") && !$curnode->isa("PPI::Structure::Block")) {
			return ;
		}
		$curnode = $curnode->parent;
	}
	return 1;
}

=pod
=head2 debug_node_path
       Метод печатает ключи хеша и количество длину массива для
       этого хеша
=cut
sub debug_hash_length {
	my ($self, $hash, $hash_name) = @_;
	$log->debug("debug hash: $hash_name");
	foreach (keys %{$hash}) {
		$log->debug('key: ', $_, ' counts: ', $#{$hash->{$_}} + 1);
	}
	return $self;
}

=pod
=head2 debug_node_path
       Распечатать путь для вершины
=cut
sub debug_node_path {
	my ($self, $path) = @_;
	my $spath = '';
	my $indent = '';
	for (@$path) {
		$spath = $spath . '-> (' . $self->first_cont_line($_) . ')';
		#$indent = $indent . '  ';
	}
	$log->debug('path: ', $spath);
	return $self;
}

=pod
=head2 debug_ppi_node
       Распечатать информацию по ppiNode
       C<node> вершина информацию по которой мы выводим
       C<prefix> сообщение перед командой
       C<level> уровень лога на котором выводится сообщение
                default debug
=cut
sub debug_ppi_node {
	my ($self, $node, $prefix, $level) = @_;
	my ($tree_id, $file, $location) = qw('' '' '');
	my $treeNode = $node->{_treeNode};

	my ($package, $filename, $line, $sub) = caller(0);
	# my $tree_id = '';
	# my $file = '';

	$prefix = $prefix || '';

	if ($treeNode) {
		$tree_id = ' treeId: ' . $treeNode->{ID};
		$file = ' file: ' . $self->get_file_by_id($treeNode->{fileId}); # FIXME: оптимизировать
		$location = $treeNode->{location_start}->[0];
	} else {
		$file = ' file: ' . $self->get_cur_filename();
		#NOTE: в процессе вставок в дерево может меняться location для элементов
		#      поэтому используются location сохраненные на начало работы
		if ($node->{_default_location}) {
			$location = $node->{_default_location}->[0];
			$location = "$location";
		} else {
			# (~) означает что позиция определяется вычислением
			# и может быть ошибка

            # NOTE: когда сенсор вставляется для вставленной undef вершины
            #       location для такой вершины может быть undef,
            #       и в коде будет предупреждение, вероятно это правильно, но стоит
            #       иметь ввиду

			$location = $node->location()->[0];
			if (!$location && $node->parent) {
				$location = $node->parent()->location()->[0];
			}
            if (!defined $location) {
                $log->debug("Location is not defined;");
            }
            $location = $location // '';
			$location = "(~) $location";
		}
	}
	# $log->debug($prefix, "nodeType: ", ref $node, $tree_id, ' line: ',
	# 			$node->location()->[0] ,' ', $node->content,
	# 			$file
	# 	);
	if (!$level) {$level = 'debug';}
	$log->$level('[', $line, '] ',  $prefix, "nodeType: ", ref $node, $tree_id, ' line: ',
				$location ,' ', $self->first_cont_line($node),
				$file);
	return $self;
}

=pod
=head2 first_cont_line
       Напечатать первую строку содержимого ppi вершины
       Метод нужен для отладки
=cut
sub first_cont_line {
	my ($self, $ppinode) = @_;
	return &first_line($ppinode->content);
}

=pod
=head2 ppi_node_label
       Взять label для statement в ppi почему-то берется schild(1),
       почему не понятно, но судя по всему это ошибка
=cut
sub ppi_state_label {
	my ($self, $node) = @_;
	my $first = $node->schild(0) or return '';
	return $first->isa('PPI::Token::Label')
		? substr($first, 0, length($first) - 1)
		: '';
}

=pod
=head2 print_variables
       Печать всех переменных
=cut
sub print_variables {
	my ($self) = @_;
	$log->debug('Print variables:');
	for my $var (keys %{$self->{vars_hash}}) {
		for my $sym (@{$self->{vars_hash}->{$var}}) {
			$self->print_variable($sym);
		}
	}
	return $self;
}

=pod
=head2 print_variable $var
       Напечатать информацию по переменной
=cut
sub print_variable {
	my ($self, $var) = @_;
	my ($id, $name, $nmspc, $glbl) = ($var->{ID}, $var->{Name}, $var->{Namespace}, $var->{isGlobal});
	$log->debug("ID: $id, Name: $name, Namespace: $nmspc gl: $glbl");
	$log->debug('    ValueGetPosition:');
	for my $node (@{$var->{ValueGetPosition}}) {
		$log->debug($self->node_def_location_str($node));
	}
	$log->debug('    ValueSetPosition:');
	for my $node (@{$var->{ValueSetPosition}}) {
		$log->debug($self->node_def_location_str($node));
	}
	return $self;
}

=pod
=head2 print_variables
       Печать всех переменных
=cut
sub print_subs {
	my ($self) = @_;
	$log->debug('Print subs:');
	for my $var (keys %{$self->{sub_hash}}) {
		for my $sym (@{$self->{sub_hash}->{$var}}) {
			$self->print_sub($sym);
		}
	}
	return $self;
}

=pod
=head2 print_variable $var
       Напечатать информацию по переменной
=cut
sub print_sub {
	my ($self, $var) = @_;
	my ($id, $name, $nmspc) = ($var->{ID}, $var->{Name}, $var->{Namespace});
	$log->debug("ID: $id, Name: $name, Namespace: $nmspc");
	return $self;
}

sub node_def_line {
	my ($self, $node) = @_;
	my ($line, $file);
	if ($node->{_default_location}) {
		$line = $node->{_default_location}->[0];
		$file = $node->{_fid};
	} else {
		$line = $node->location->[0];
		$file = $self->{cur_file_id};
	}
	return ($file, $line);
}

sub node_def_location_str {
	my ($file, $line) = &node_def_line(@_);
	return "fl: $file, line: $line";
}

sub create_dump {
	my ($self, $folderName) = @_;
	if ( ! -d $folderName) {
		$log->debug('create directory = ', $folderName);
		File::Path::make_path($folderName);
	}
	my $fname = $self->{files}->[0]->[1];
	my ($_name,$_path,$_suffix) = fileparse($fname, qr/\.[^.]*/);
	my $newName = "dump" . ucfirst($_name) . $_suffix;
	my $fullName = File::Spec->catfile($folderName, $newName);
	&PerlHandler::Utils::create_ppi($fname, $fullName);
	#&PerlHandler::Utils::create_dump($fullName, $self->{trees}->{0}->{ppi});
}

sub create_ppi {
	my ($self, $folderName) = @_;
	if ( ! -d $folderName) {
		$log->debug('create directory = ', $folderName);
		File::Path::make_path($folderName);
	}
	my $fname = $self->{files}->[0]->[1];
	my ($_name,$_path,$_suffix) = fileparse($fname, qr/\.[^.]*/);
	my $newName = "ppi" . ucfirst($_name) . $_suffix;
	my $fullName = File::Spec->catfile($folderName, $newName);
	&PerlHandler::Utils::create_ppi_ppi($fname, $fullName);
}

sub create_struct {
	my ($self, $folderName) = @_;
	my $newName = $self->_create_output_file_name($folderName, 'struct');
	$self->{struct_gen}->gen_structure();
	my $struct = $self->{struct_gen}->get_structure();
	&PerlHandler::Utils::create_dump($newName, $struct);
}

sub create_xml {
	my ($self, $folderName) = @_;
	my $newName = $self->_create_output_file_name($folderName, 'xml');
	my ($_name,$_path,$_suffix) = fileparse($newName, qr/\.[^.]*/);
	$newName = File::Spec->catfile($_path, $_name . '.xml');
	$self->{struct_gen}->gen_structure();
	my $struct = $self->{struct_gen}->get_structure();
	&PerlHandler::PerlXsd::write_xml($struct, $newName);
}

sub _create_output_file_name {
	my ($self, $folderName, $prefix) = @_;
	if ( ! -d $folderName) {
		$log->debug('create directory = ', $folderName);
		File::Path::make_path($folderName);
	}
	my $fname = $self->{files}->[0]->[1];
	my ($_name,$_path,$_suffix) = fileparse($fname, qr/\.[^.]*/);
	my $newName = $prefix . ucfirst($_name) . $_suffix;
	my $fullName = File::Spec->catfile($folderName, $newName);
	return $fullName;
}

1;
