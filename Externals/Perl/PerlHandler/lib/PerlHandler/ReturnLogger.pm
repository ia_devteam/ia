#!/usr/bin/perl

package PerlHandler::ReturnLogger;

=pod
=head1 NAME

PerlHandler::ReturnLogger Class that perform logging of places where unsafe return was inserted.

=head1 METHODS

=cut

use 5.010;
use strict;
use warnings;

use File::Spec;
use File::Path;

our $first_line = \&PerlHandler::Utils::first_line;
our $ppi_node_default_location = \&PerlHandler::Utils::ppi_node_default_location;
our $ppi_node_location = \&PerlHandler::Utils::ppi_node_location;

our $description = '
  В данном файле содержится список return выражений,
  которые описывают места вставки return.

  В случае режима safe файл будет пустой.
  В случае режима unsafe сюда попадают, return выражения в которых были обнаружены вызовы.
  В случае режима default сюда попадают, все return выражения.

  Формат вывода:
   - файл
   - начальный номер строки
   - начальный номер столбца
   - номер столбца в файле с сенсорами
   - номер столбца в файле с сенсорами
   - первая строка кода return выражения

   Подробнее описание проблемы см. в файле Perl/PerlHandler/t/testSensorReturn.pm.

';


use PerlHandler::PerlHandlerLogger;
our $log = $PerlHandler::PerlHandlerLogger::logger;

sub new {
	my ($cls, $params) = @_;
    $params = $params || {};
    my $folder = $params->{folder};


    if (! -d $folder) {
        File::Path::make_path($folder);
        #mkpath([$_path], 1, 777) or die $!;
    }

    my $file_name = 'return.log';
    $file_name = File::Spec->catfile($folder , $file_name);

    my $class = ref $cls || $cls;
    my $self = bless {
        file_name => $file_name,
        nodes => {},
        cur_tree => undef,
        nextId => 0,
        desc_inserted => 0
    }, $class;
    return $self;
}

=pod

=head2 scope

The C<scope> method returns true if the node represents a lexical scope
boundary, or false if it does not.

=cut
sub init_doc_log {
    my ($self, $treeHash) = @_;
    $self->{cur_tree} = $treeHash;
    return $self;
}

sub log {
    my ($self, $ppiNode) = @_;
    my $cls = ref $ppiNode;
    if (!$ppiNode->{returnLoggerId}) {
        $ppiNode->{returnLoggerId} = $self->{nextId} ++;
    }
    $self->{nodes}->{$ppiNode->{returnLoggerId}} = $self->create_node_description($ppiNode);
    return $self;
}

sub create_node_description {
    my ($self, $ppiNode) = @_;
    my ($location, $about) = &$ppi_node_default_location($ppiNode);
    if (!$location) {
        $location = ['Unknown', 'Unknown'];
    }
    my $desc = {
        line => "$location->[0]",
        row  => "$location->[1]",
        code_start => &$first_line($ppiNode->content),
        default_node => $ppiNode
    };
    return $desc;
}

sub save_doc_log {
    my ($self) = @_;
    $self->fill_new_lines();
    my $nodes = $self->{nodes};
    my $desc = undef;
    my $file_name = $self->{file_name};
    my $mode = '>';
    if ($self->{desc_inserted} == 1) {
        $mode = '>>';
    }
    open my $handle, $mode, $file_name
        or die "Can't open '$file_name' for writing: $!";
    my $ppiNode;
    if ($self->{desc_inserted} == 0) {
        $log->warn('save return log to file: ' . $file_name . "\n");
        print {$handle} $description;
        $self->{desc_inserted} = 1;
    }
    foreach my $ppiNodeId (sort { ($nodes->{$a}->{line}) <=> ($nodes->{$b}->{line})} (keys %{$nodes})) {
        $desc = $nodes->{$ppiNodeId};
        $self->write_msg($handle, $desc);
    }
    close $handle;
    return $self;
}

sub write_msg {
    my ($self, $handle, $desc) = @_;
    my $file = $self->{cur_tree}->{doc_config}->[1];
    my $line = $desc->{line};
    my $row = $desc->{row};
    my $new_line = $desc->{new_line};
    my $new_row = $desc->{new_row};
    my $code = $desc->{code_start};
    my $msg = "return fl:\"$file\"; ln:\"$line\"; rw:\"$row\"; nln:\"$new_line\"; nrw:\"$new_row\"; code:\"$code\"\n";
    print {$handle} $msg;
    return $self;
}

sub fill_new_lines {
    my ($self) = @_;
    my $tree = $self->{cur_tree};
    my $doc = $tree->{ppi};
    $doc->flush_locations();
    $doc->index_locations();
    my $desc = undef;
    my $new_loc = undef;
    my $ppiNode = undef;
    foreach my $ppiNodeId (keys %{$self->{nodes}}) {
        $desc = $self->{nodes}->{$ppiNodeId};
        $ppiNode = $desc->{default_node};
        $new_loc = &{$ppi_node_location}($ppiNode);
        if (!$new_loc) {
            $new_loc = ['Unknown', 'Unknown'];
        }
        $desc->{new_line} = $new_loc->[0];
        $desc->{new_row} = $new_loc->[1];
    }
    return $self;
}

1;
