#!/usr/bin/perl

package PerlHandler::StructGenerator;

use 5.010;
use strict;
use warnings;
use PPI;

use Log::Message::Simple qw[msg error debug
	carp croak cluck confess];

use PerlHandler::PerlHandlerLogger;
our $log = $PerlHandler::PerlHandlerLogger::logger;

=pod
=head1 Генератор структуры perl
       На вход получаем объект TreeGenerator
       из него собираем структуру
=cut

=pod
=head2 new конструктор
       Так как нужно будет строить связи имеет смысл
       хранить тут все документы
=cut
sub new {
	my ($cls, $tree_gen) = @_;
	my $class = ref $cls || $cls;

	return bless {
		tree => $tree_gen,
		structure => {           # собираемая структура
			Files => {File=>[]},
			Classes => {},
			Variables => {},
			Functions => {},
			Statements => {cho_STBreak =>[]},
			Operations => {Operation =>[]}
		},
	}, $class;
}

=pod
=head2 get_structure вернуть структуру
=cut
sub get_structure {
	my $self = shift;
	return $self -> {structure};
}

=pod
=head2 gen_structure
       Собрать структуру по результатам работы генератора дерева
       Функция запускает функции генераторы для отдельных типов и собирает это все вместе
=cut
sub gen_structure {
	my $self = shift;
	for my $file (@{$self->{tree}->{files}}) {
		push @{$self->{structure}->{Files}->{File}}, {
			ID=> $file->[0],
			fullname => $file->[1]
		};
	}

	for my $symb (sort keys %{$self->{tree}->{global_vars}}) {
		for my $gvar (@{$self->{tree}->{global_vars}->{$symb}}) {
			push @{$self->{structure}->{Variables}->{cho_Variable}}, $self->gen_variable($gvar);
		}
	}
	for my $symb (sort keys %{$self->{tree}->{vars_hash}}) {
		for my $gvar (@{$self->{tree}->{vars_hash}->{$symb}}) {
			push @{$self->{structure}->{Variables}->{cho_Variable}}, $self->gen_variable($gvar);
		}
	}
	for my $symb (sort keys %{$self->{tree}->{sub_hash}}) {
		for my $gvar (@{$self->{tree}->{sub_hash}->{$symb}}) {
			push @{$self->{structure}->{Functions}->{cho_Function}}, $self->gen_function($gvar);
		}
	}
    for my $symb (sort keys %{$self->{tree}->{scheduled_hash}}) {
		for my $gvar (@{$self->{tree}->{scheduled_hash}->{$symb}}) {
			push @{$self->{structure}->{Functions}->{cho_Function}}, $self->gen_function($gvar);
		}
	}
	my $nd;
	for my $st (@{$self->{tree}->{st_list}}) {
		$nd = $self->gen_statement($st);
		if ($nd) {
			push @{$self->{structure}->{Statements}->{cho_STBreak}}, $nd;
		}
	}
	return $self;
}

sub gen_variable {
	my ($self, $var) = @_;
	if ($var->{Name} eq '$func') {
		$self->{tree}->debug_ppi_node($var->{ppiNode});
	}
	my $var_desc = {
		Variable => {
			ID=>$var->{ID},
			Name=>$var->{Name},
			Namespace=>$var->{Namespace}, #в данном случае в котором определена
			isGlobal=>$var->{isGlobal},
			Definition=>{
				LocationLine=> {
					line=>$var->{location_start}->[0],
					column=>$var->{location_start}->[1],
					file=> $var->{fileId}
						#$self->{tree}->get_file_by_id($var->{fileId})
				},
				# LocationOffset=>{
				# 	offset=>-1,
				# 	file=>"$var->{fileId}"
				# }
			},
			AssignmentFrom => [],
			ValueGetPosition => $self->get_values($var->{ValueGetPosition}, $var->{fileId}),
			PointsToFunctions => [],
			ValueCallPosition => [],
			ValueSetPosition => $self->get_values($var->{ValueSetPosition}, $var->{fileId})
		}
	};
	return $var_desc;
}

sub get_values {
	my ($self, $nodelist, $fileId) = @_;
	my $list = [];
	my ($loc, $nFileId);
	for my $nd (@$nodelist) {
		$loc = $nd->{_default_location} ||
			($nd->{_treeNode} ?
			 $nd->{_treeNode}->{location_start} :
			 $nd->location());
        $nFileId = $nd->{_treeNode} ?
            $nd->{_treeNode}->{fileId} :
            $nd->{_fileId};

		push @$list, {
			LocationLine=> {
				line=>$loc->[0],
				column=>$loc->[1],
				file=>"$nFileId"
			}
			# ,
			# LocationOffset=>{
			# 	offset=>-1,
			# 	file=>"$fileId"
			# }
		};
	}
	return $list;
}

sub gen_function {
	my ($self, $var, $glob) = @_;
	my $var_desc = {
		Function => {
			ID=>$var->{ID},
			Name=>$var->{Name},
			Namespace=>$var->{Namespace}, #в данном случае в котором определена
			Definition=> {
				LocationLine=> {
					line=>$var->{location_start}->[0],
					column=>$var->{location_start}->[1],
					file=>"$var->{fileId}"
				},
				# LocationOffset=>{
				# 	offset=>-1,
				# 	file=>"$var->{fileId}"
				# }
			},
			Declaration=>  {
				#зачем тут выбор а в остальных местах нет ???
				cho_LocationLine => {
					LocationLine=> {
						line=>$var->{location_start}->[0],
						column=>$var->{location_start}->[1],
						file=>"$var->{fileId}"
					}
				}
			},
			EntryStatement=> $var->{EntryStatement}
		}
	};
	return $var_desc;
}

=pod
=head2 gen_statement
       Для разных statement нужно по разному делать вывод, хотя
       большая часть полей заполнена в TreeGenerator, придется все равно пройтись
       по всем типам и поправить
=cut
no warnings 'experimental::smartmatch'; # отключаем warning на given
sub gen_statement {
	my ($self, $tnode) = @_;

	#print "tnode = ", $tnode, " $tnode->{nodeType} \n";
	my $line;
	$line = ${$tnode->{ppiNode}->location()}[0] if $tnode->{nodeType} ne "Root";
	#print "line: $line \n ";

	my $st;
	#$log->info('nodeId = ', $tnode->{ID}, ' nodeType =', $tnode->{nodeType});
	given ($tnode->{nodeType}) {
		when ("Scheduled")	{ $st = $self->hndl_scheduled($tnode); break;}
		when ("Function")	{ $st = $self->hndl_function($tnode); break;}
        when ("Anonymous")	{ $st = $self->hndl_function($tnode); break;}
		when ("Include")	{ $st = $self->hndl_include($tnode);break;}
		when ("Package")	{ $st = $self->hndl_package($tnode);break;}
		when ("Variables")	{ $st = $self->hndl_variables($tnode);break;}
		when ("Variable")	{ $st = $self->hndl_variable($tnode);break;}
		when ("Expression") { $st = $self->hndl_expression($tnode);break;}
		when ("If")			{ $st = $self->hndl_if($tnode);break;}
		when ("For")		{ $st = $self->hndl_for($tnode);break;}
		when ("Foreach")	{ $st = $self->hndl_foreach($tnode);break;}
		when ("While")		{ $st = $self->hndl_while($tnode);break;}
		when ("Break")		{ $st = $self->hndl_break($tnode);break;}
		when ("Continue")   { $st = $self->hndl_continue($tnode);break;}
		when ("Redo")       { $st = $self->hndl_redo($tnode);break;}
		when ("Return")     { $st = $self->hndl_return($tnode);break;}
		when ("GoTo")       { $st = $self->hndl_goto($tnode);break;}
		when ("Switch")		{ $st = $self->hndl_switch($tnode);break;}
		when ("Case")		{ $st = $self->hndl_case($tnode);break;}
		when ("Null")		{ $st = $self->hndl_null($tnode);break;}
		when ("Error")		{ $st = $self->hndl_error($tnode);break;}
		when ("Statement")	{ $st = $self->hndl_statement($tnode); break;}
		when ("Root")	    {break;}
		when ("__END__")    {break;}
		default {
			$log->debug($tnode->{treeParent}->{nodeType}, ' ',
						$tnode->{ppiNode}->content);
			die "Unknown type $tnode->{nodeType} \n";}
	}
	return $st;
}
use warnings;

use warnings;

sub hndl_scheduled {
	my ($self, $tnode) = @_;
	return &hndl_variables(@_);
	my $st = {};
	#не обрабатываем, не должно быть
	return $st;
}

sub hndl_function{
	my ($self, $tnode) = @_;
	return &hndl_variables(@_);
	my $st = {};
	#не обрабатываем, не должно быть
	return $st;
}

sub hndl_include {
	my ($self, $tnode) = @_;
	return &hndl_variables(@_);
	my $st = {};
	#не обрабатываем, не должно быть
	return $st;
}

sub hndl_package {
	my ($self, $tnode) = @_;
	return &hndl_variables(@_);
	my $st = {};
	#не обрабатываем, не должно быть
	return $st;
}

sub hndl_variables {
	my ($self, $tnode) = @_;
	my $op = $self->gen_operation($tnode);
	my $st = {
		STOperational=> {
			ID=>$tnode->{ID},
			operation=>$op->{ID},
		}
	};
	$self->addStRefType($st->{STOperational}, $tnode);
	if (!$st->{STOperational}->{FirstSymbolLocation}->{LocationLine}->{line}) {
		$self->{tree}->debug_ppi_node($tnode->{ppiNode}, 'Var Node:');
		$log->debug('line = ', $st->{STOperational}->{FirstSymbolLocation}->{LocationLine}->{line});
	}
	return $st;
}

sub addStRefType {
	my ($self, $st, $tnode) = @_;
	$st->{ID} = $tnode->{ID};
	$st->{FirstSymbolLocation} = {
		LocationLine=> {
			line=>$tnode->{location_start}->[0],
			column=>$tnode->{location_start}->[1],
			file=>"$tnode->{fileId}"
		}# ,
		# LocationOffset=> {
		# 	offset => -1,
		# 	file=>"$tnode->{fileId}"
		# }
	};
	$st->{LastSymbolLocation} = {
		cho_LocationLine => {
			LocationLine=> {
				line =>$tnode->{location_finish}->[0],
				column=>$tnode->{location_finish}->[1],
				file=>"$tnode->{fileId}"
			}
		}# ,
		# LocationOffset=> {
		# 	offset => -1,
		# 	file=>"$tnode->{fileId}"
		# }
	};
	if ($tnode->{SensorBeforeTheStatement}) {
		$st->{SensorBeforeTheStatement} = $tnode->{SensorBeforeTheStatement};
	}
	if ($tnode->{NextInLinearBlock}) {

        #TODO: для случая nextInLinearBlock STGoto, нужно
        #      в случае если gototype == 3 изменить kind c STGoto на STReturn
        #      данный способ большое является хаком, и желательно
        #      реализовать правильное заполнение типа еще на этапе разбора
        if ($tnode->{NextInLinearBlock}->{kind} eq 'STGoto') {
            my $id = $tnode->{NextInLinearBlock}->{ID};
            my $st = $self->{tree}->{ids}->{$id};
            if ($st->{gototype} == 3) {
                $tnode->{NextInLinearBlock}->{kind} = 'STReturn';
            }
        }

        $st->{NextInLinearBlock} = $tnode->{NextInLinearBlock};
	}
	return $st;
}

sub hndl_variable {
	my ($self, $tnode) = @_;
	my $st = {};
	#не обрабатываем, не должно быть
	return $st;
}

sub hndl_expression {
	my ($self, $tnode) = @_;
	return $self->hndl_variables($tnode);
}

sub hndl_if {
	my ($self, $tnode) = @_;
	my $st = {
		STIf=> {
			ID=>$tnode->{ID},
			condition=>$tnode->{condition},
			then=>$tnode->{then},
			else=>$tnode->{else}
		}
	};
	$self->addStRefType($st->{STIf}, $tnode);
	return $st;
}

sub hndl_for  {
	my ($self, $tnode) = @_;
	my $st = {
		STFor => {
			ID=>$tnode->{ID},
			start=>$tnode->{start},
			condition=>$tnode->{condition},
			iteration=>$tnode->{iteration},
			body=>$tnode->{body}
		}
	};
	$self->addStRefType($st->{STFor}, $tnode);
	return $st;
}

sub hndl_foreach {
	my ($self, $tnode) = @_;
	my $st = {
		STForEach => {
			ID=>$tnode->{ID},
			head=>$tnode->{head},
			body=>$tnode->{body}
		}
	};
	$self->addStRefType($st->{STForEach}, $tnode);
	return $st;
}

sub hndl_while {
	my ($self, $tnode) = @_;
	my $op = $self->gen_operation($tnode);
	my $st = {
		STDoAndWhile => {
			ID=>$tnode->{ID},
			condition=>$op->{ID},
			isCheckConditionBeforeFirstRun=>1,
			body=>$tnode->{body}
		}
	};
	$self->addStRefType($st->{STDoAndWhile}, $tnode);
	return $st;
};

sub hndl_break {
	my ($self, $tnode) = @_;
	my $st = {
		STBreak => {
			breakingLoop => $tnode->{outerLoop}
		}
	};
	$self->addStRefType($st->{STBreak}, $tnode);
	return $st;
};

sub hndl_continue  {
	my ($self, $tnode) = @_;
	my $st = {
		STContinue => {
			continuingLoop=>$tnode->{outerLoop}
		}
	};
	$self->addStRefType($st->{STContinue}, $tnode);
	return $st;
};

sub hndl_redo  {
	my ($self, $tnode) = @_;
	return $self->hndl_continue($tnode);
};

sub hndl_return {
	my ($self, $tnode) = @_;
	#FIXME: в return может быть сложное выражение
	#или return c if переводить в if и return
	my $op = $self->gen_operation($tnode);
	my $st = {
		STReturn => {
			ReturnOperation => $op->{ID}
		}
	};
	$self->addStRefType($st->{STReturn}, $tnode);
	return $st;
};

sub hndl_goto {
	my ($self, $tnode) = @_;
	my $st;
	if ($tnode->{gototype} == 2 || $tnode->{gototype} == 1) {
		$st = {
			STGoto => {
				destination => $tnode->{destination}
			}
		};
		$self->addStRefType($st->{STGoto}, $tnode);
	} else {
		#3 тип, заменяем на вызов return Sum;
		my $op = $self->gen_operation($tnode);
		$st = {
			STReturn => {
				ReturnOperation => $op->{ID}
			}
		};
		$self->addStRefType($st->{STReturn}, $tnode);
	}
	return $st;
};

sub hndl_switch {
	my ($self, $tnode) = @_;
	my $operation = $self->gen_operation($tnode);
	my $st = {
		STSwitch => {
			head => $operation->{ID}
		}
	};
	if ($tnode->{default} &&
        (ref $tnode->{default} eq 'ARRAY') &&
        (scalar($tnode->{default}) > 0) &&
        $tnode->{default}->[0]->{body}) {
		$st->{STSwitch}->{default} = {
			block => '' . $tnode->{default}->[0]->{body}->{ID}
		}
	}
	if ($tnode->{case}) {
		$st->{STSwitch}->{case} = [];
		for my $cs (@{$tnode->{case}}) {
            #$self->{tree}->debug_ppi_node($cs->{condition}->{ppiNode}, "Condition", 'info');
			my $operation = $self->gen_operation($cs->{condition});
			push @{$st->{STSwitch}->{case}}, {
				condition => $operation->{ID},
				body => $cs->{body} ? $self->{tree}->desc_tree_node($cs->{body}) : undef
			}
		}
	}
	$self->addStRefType($st->{STSwitch}, $tnode);
	return $st;
};

#никак
sub hndl_case {
	my ($self, $tnode) = @_;
    if ($tnode && !$tnode->{caseType} || $tnode->{caseType} ne 'given') {
        return &hndl_if(@_);
    }
	return undef;
};

#?
sub hndl_null {
#	$log->debug('hndl_null');
	return &hndl_variables(@_);
};

#?
sub hndl_error {
	my ($self, $tnode) = @_;
	my $st = {};
	return $st;
};

sub hndl_statement {
	return &hndl_variables(@_);
}

=pod
=head2 gen_operation
       Генерация операции, выполняется только для некоторых выражений
       Операция сразу кладется в $self->{structure}->{Operations}->{Operation}
=cut
sub gen_operation {
	my ($self, $tnode) = @_;
	#if (!$tnode->{functionsCall}) {return; }
	my $operation = {
		ID=>$self->{tree}->gen_next_id(),
		callNode => []
	};
	my $callNode = {cho_callFunction=>[]};
	for my $funcgrp (@{$tnode->{functionsCall}}) {
		for my $func (@$funcgrp) {
			#$log->info('Function = ', $func->{ID}, $func->{Name}, $func->{Namespace});
			push @{$callNode->{cho_callFunction}}, {callFunction => {
				ID=>$func->{ID},
				kind=>"function"
				}
			};
		}
	}
	push @{$operation->{callNode}}, $callNode;
	push @{$self->{structure}->{Operations}->{Operation}}, $operation;
	return $operation;
}

1;

__END__


=pod

Класс нужен для генерации структуры:

     structure = > {
			Classes => {},
			Variables => {},
			Functiosn => {},
			Statements = {},
			Operations => {}
		}

по которой можно сегенерировать xml, по дереву PPI.

Структура dom ppi

   PPI::Element
      PPI::Node
         PPI::Document
            PPI::Document::Fragment
         PPI::Statement
            PPI::Statement::Package
            PPI::Statement::Include
            PPI::Statement::Sub
               PPI::Statement::Scheduled
            PPI::Statement::Compound
            PPI::Statement::Break
            PPI::Statement::Given
            PPI::Statement::When
            PPI::Statement::Data
            PPI::Statement::End
            PPI::Statement::Expression
               PPI::Statement::Variable
            PPI::Statement::Null
            PPI::Statement::UnmatchedBrace
            PPI::Statement::Unknown
         PPI::Structure
            PPI::Structure::Block
            PPI::Structure::Subscript
            PPI::Structure::Constructor
            PPI::Structure::Condition
            PPI::Structure::List
            PPI::Structure::For
            PPI::Structure::Given
            PPI::Structure::When
            PPI::Structure::Unknown
      PPI::Token
         PPI::Token::Whitespace
         PPI::Token::Comment
         PPI::Token::Pod
         PPI::Token::Number
            PPI::Token::Number::Binary
            PPI::Token::Number::Octal
            PPI::Token::Number::Hex
            PPI::Token::Number::Float
               PPI::Token::Number::Exp
            PPI::Token::Number::Version
         PPI::Token::Word
         PPI::Token::DashedWord
         PPI::Token::Symbol
            PPI::Token::Magic
         PPI::Token::ArrayIndex
         PPI::Token::Operator
         PPI::Token::Quote
            PPI::Token::Quote::Single
            PPI::Token::Quote::Double
            PPI::Token::Quote::Literal
            PPI::Token::Quote::Interpolate
         PPI::Token::QuoteLike
            PPI::Token::QuoteLike::Backtick
            PPI::Token::QuoteLike::Command
            PPI::Token::QuoteLike::Regexp
            PPI::Token::QuoteLike::Words
            PPI::Token::QuoteLike::Readline
         PPI::Token::Regexp
            PPI::Token::Regexp::Match
            PPI::Token::Regexp::Substitute
            PPI::Token::Regexp::Transliterate
         PPI::Token::HereDoc
         PPI::Token::Cast
         PPI::Token::Structure
         PPI::Token::Label
         PPI::Token::Separator
         PPI::Token::Data
         PPI::Token::End
         PPI::Token::Prototype
         PPI::Token::Attribute
         PPI::Token::Unknown
=cut
