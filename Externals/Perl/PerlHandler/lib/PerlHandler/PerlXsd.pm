#!/usr/bin/perl

package PerlHandler::PerlXsd;

use strict;
use warnings;

use XML::Compile::Schema;
use Data::Dumper;

use File::Spec;
use File::Basename;

use vars qw{@EXPORT_OK};
BEGIN {
	@EXPORT_OK = qw(make_valid_xsd write_xml);
}

use Log::Message::Simple qw[msg error debug
	carp croak cluck confess];

use PerlHandler::PerlHandlerLogger;
our $log = $PerlHandler::PerlHandlerLogger::logger;

=pod
=head2 defaultOutput имя xml файла в который по умолчанию будет выводится структура
=cut
our $defaultOutput = "structure.xml";

=pod
=head2 defaultXsd переменная которая хранит путь до подправленного
       xsd файла, который может быть использован для генерации xml
=cut
our $defaultXsd = "ParserToStorageData.xsd";

=pod
=head2 fileXsdSafeCreated флаг был ли создан уже упрощенный xsd файл
=cut
our $fileXsdSafeCreated = 0;
=pod
=head2 fileXsd переменная которая хранит путь до подправленного
       xsd файла, который может быть использован для генерации xml
=cut
our $fileXsd = '';

# нужно сделать копию filename вырезав из файла 
# targetNamespace="https://bitbucket.org/dadymax/ia/ParserToStorageData.xsd" 
# elementFormDefault="qualified"											   
# xmlns="https://bitbucket.org/dadymax/ia/ParserToStorageData.xsd"
# иначе не работает

sub gen_file_name() {
	my $filename = $_[0];
	$log->info("file_name= ", $filename);
	my ($name,$path,$suffix) = fileparse($filename, qr/\.[^.]*/);
	print "name,path,suffix = ($name,$path,$suffix)\n";
	my $num = 0;
	my $new_file_name = File::Spec->catfile($path, $name . "_safe" . $suffix);
	print "new_file_name = $new_file_name\n";
	$new_file_name;
}


my @dropprops = qw (targetNamespace elementFormDefault xmlns );
=pod
=head2 check_valid $filename
Проверить, что шаблон корректный
=cut
sub check_valid {
    my $filename = shift;
	open my $xsd_file, "<", $filename;
	my $in_schema = 0;
	while (defined(my $line = <$xsd_file>)) {
		if ($line =~ /^\s*<xs:schema/) {
			$in_schema = 1;
		}
		if ($in_schema == 1) {
			for my $name (@dropprops) {
				$line =~ /$name=\"[^\"]*\"/ ;
                if ($line) {
                    close $xsd_file;
                    return ;
                }
			};
		}
		if ($in_schema == 1 && $line =~ />\s*$/) {
			$in_schema = 0;
		}
	}
	close $xsd_file;
    return 1;
}

sub make_valid_xsd () {
	my $filename = $_[0];
    #если xsd корректная то преобразование не делаем
    if (! &check_valid($filename)) {
        return $filename;
    }

	my $new_file_name = &gen_file_name($filename);
    $log->warn("filename = $filename is not safe xsd try create safe.");

	open DEFAULT, "<", $filename;
	open SAFE, ">", $new_file_name;

	my $in_schema = 0;
	while (defined(my $line = <DEFAULT>)) {
		if ($line =~ /^\s*<xs:schema/) {
			$in_schema = 1;
		}
		if ($in_schema == 1) {
			for my $name (@dropprops) {
				$line =~ s/$name=\"[^\"]*\"// ;
			};
		}
		if ($in_schema == 1 && $line =~ />\s*$/) {
			$in_schema = 0;
		}
		print SAFE $line;
	}
	close DEFAULT;
	close SAFE;
	$new_file_name;
};

=pod
=head2 parse_xsd $file $output_file_name $output_type
       Метода получает на вход файл $file 
       создает пример структуры на языке PERL либо XML
       в файле $output_file_name если задан, 
       тип примера определяется третьим параметром $output_type
=cut
sub parse_xsd() {
	my $filename = $_[0];
	my $output_file_name = $_[1];
	my $output_type = $_[2] || "PERL";

	if(!$filename) {
		warn "Please provide the WSDL definition file.";
		exit 10;
	}
	$filename = &make_valid_xsd($filename);

	print "filename =  $filename\n";

	my $schema = XML::Compile::Schema->new($filename);
	my $hash;

	$schema->printIndex();

	if (! defined($output_file_name)) {
		print Dumper $schema->template($output_type => 'Database');
	} else {
		open my $temp_out, ">", $output_file_name;
		print $temp_out Dumper $schema->template($output_type => 'Database');
		close $temp_out;
	}
}

=pod
=head2 create_schema $file
       создает объект для генерации xml по хеш функциям
=cut
sub create_schema {
	my $filename = $_[0];

	if(!$filename) {
		warn "Please provide the WSDL definition file.";
		exit 10;
	}
	print "filename =  $filename\n";

	my $schema = XML::Compile::Schema->new($filename);
	
	return \$schema;
}

sub get_file_xsd {
	our $defaultXsd;
	our $fileXsdSafeCreated;
	our $fileXsd;
	if ($fileXsdSafeCreated == 1) {return $fileXsd;}
	$fileXsd = &make_valid_xsd($defaultXsd);
	$fileXsdSafeCreated = 1;
	return $fileXsd;
}

=pod
=head2 C<handle_perl_files \@filelist $output_xml $start_id>
       \@filelist список обрабатываемых документов
                  в формате C<[(id, path) (id, path), ... ]>
       C<$output_xml> имя файла в который будет сохранена структура
       C<$start_id> начальный номер с которого будет заполняться структура
=cut
sub handle_perl_files {
	my ($file_list, $output_xml, $start_id) = @_;
	my $generator = PerlHandler::StructGenerator -> new($file_list);
	$generator -> generate();
	my $struct = $generator->get_structure();
	my $res_output_xml = &write_xml($struct, $output_xml);
	return $res_output_xml;
}

=pod
=head2 write_xml $structure, $output_file
       Функция write_xml выводит структуру C<$structure> в C<output_file>
       по умолчанию используется имя structure.xml
=cut
sub write_xml {
	my ($structure, $output_file) =  @_;
	
	my $fileXsd = &get_file_xsd();
	my $schemaXsd = ${&create_schema($fileXsd)};
	$output_file = $output_file || $PerlXsd::defaultOutput;
	msg("Xml output file is $output_file");
	
	my $doc = XML::LibXML::Document->new('1.0', 'UTF-8');
	my $write = $schemaXsd->compile(WRITER => "Database");
	my $xml = $write->($doc, $structure);

	$doc->setDocumentElement($xml);

	open my $outf, ">", $output_file;
	print $outf $doc->toString(1);
	close $outf;
	msg('Xml write finish');
	return $output_file;
}

1;

#parse_xsd_main()
