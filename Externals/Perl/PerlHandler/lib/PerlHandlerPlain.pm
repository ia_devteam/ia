#!/usr/bin/perl

package PerlHandlerPlain;

use 5.010;
use strict;
use warnings;

BEGIN {
	unless (caller) {
		use FindBin qw($Bin);
		print "Bin = $Bin\n";
		use lib "$Bin";
	}
}

use PerlHandler;

=pod
=head2 run_json filename Запустить parser задав параметры в json файле
C<filename> name of json file
=cut
sub run_plain {
    my $jsonfilename = shift;
    my $file;
    unless (open $file, '<', $jsonfilename) { die 'Couldn\'t open file ' . $jsonfilename . '!'; };

    my %param_name_map = (
        1 => "sources",
        2 => "basepath",
        3 => "with-sensor-dir",
        4 => "xml-log-dir",
        5 => "level",
        6 => "sensor-template",
        7 => "start-sensor",
        8 => "module-id",
        9 => "xsd-path",
        10 => "return-mode",
        11 => "logConsole",
        12 => "logFile",
        13 => "logFilePath",
        14 => "logFileName",
        15 => "logIfFileExists"
        );

    my $lineNumber = 1;
    my $params = {};

    while (my $line = <$file>) {
        chomp $line;
        $params->{$param_name_map{$lineNumber}} = $line;
        $lineNumber++;
    }

    close $file;

    my $filename = $params->{"sources"};
    my $withoutSensorsDir= $params->{"basepath"};
    my $withSensorsDir= $params->{"with-sensor-dir"};
    my $xmlAndLogDir= $params->{"xml-log-dir"};
    my $level= $params->{"level"};
    my $sensorTemplate = $params->{"sensor-template"};
    my $sensorStartId = $params->{"start-sensor"};
    my $moduleId = $params->{"module-id"};
    my $xsdPath= $params->{"xsd-path"};
    my $return_mode = $params->{"return-mode"};

    my $params_hash = &PerlHandler::create_params_hash(
        $filename, $withoutSensorsDir, $withSensorsDir,
        $xmlAndLogDir, $level, $sensorTemplate,
        $sensorStartId, $moduleId, $xsdPath, $return_mode);

    $params_hash->{logConfig} = {
        logConsole => $params->{logConsole},
        logFile => $params->{logFile},
        logFilePath => $params->{logFilePath} // $xmlAndLogDir,
        logFileName => $params->{logFileName},
        logIfFileExists => $params->{logIfFileExists}
    };

    &PerlHandler::run_parse($params_hash);
}

unless (caller) {
	&run_plain(@ARGV);
}

1;

__END__
