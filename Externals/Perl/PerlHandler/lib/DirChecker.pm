#!/usr/bin/perl

#модуль выполняет проверку директории
#на наличие файлов
#которые парсер не может обработать

# Запускать так:
# cd git/ia/Externals/Perl/PerlHandler
# perl DirChecker.pm --dir path_to_directory
# Результат:
# - errors.txt
# - errors
# Файлы с которыми парсер не справился.

package DirChecker;

use 5.010;
use strict;
use warnings;

use Getopt::Long;

our $errorFileName = 'errors.txt';
our $errorDir = 'errors';

BEGIN {
	unless (caller) {
		use FindBin qw($Bin);
		use lib "$Bin/../t/lib/";
        use lib "$Bin/lib/";
	}
}

use t::lib::Helper;

use File::Basename;
use File::Spec;
use File::Copy;

use Data::Dumper;

sub check_dir {
    my ($dir_name) = @_;

    my $tFileList = Helper->gen_file_list_from_dir($dir_name);
    my $fileList = [];

    push @$fileList, @$tFileList;

    my $count = 0;
    my $tempFileList = [];
    my $errorFiles = [];
    my $errors = [];

    my $testName = 'tempTest';

    while ($count < scalar(@$fileList)) {
        my $tempFileList = [$fileList->[$count]];
        eval {
            my $testHelper = Helper->new($testName, $tempFileList);
            my $xmlFileName = $testHelper->gen_file_list_xml();
        };

        if ( $@ ) {
            print "ERRRRROR: $@ \n";
            push @$errorFiles, $fileList->[$count];
            push @$errors, $@;
        }
        $count ++;
    }

    write_errors($errorFiles, $errors);
}

=pod
=head2 write_errors Функция записывания ошибки
1. Создает файл с путями до всех файлов в ошибками
2. Копирует все файлы с ошибками с созданный каталог
=cut
sub write_errors {
    my ($errorFiles, $errors) = @_;
    my $filename = $errorFileName;

    my $fileNames = copy_files($errorFiles);

    open my $file, '>', $filename;
    my $num = 0;
    my $copyName = '';
    for my $name (@$errorFiles) {
        $copyName = $fileNames->[$num];
        print {$file} "$num. $name -> $copyName \n";
        $num += 1;
    }
    close $file;
}

sub copy_files {
    my ($errorFiles) = @_;
    my $errDir = $errorDir;
    my $fileNames = [];
    if (! -d $errorDir ) {
        File::Path::make_path($errorDir);
    } else {
        unlink glob "$errorDir/*.*"
    }
    my $copyName;
    for my $name (@$errorFiles) {
        $copyName = copy_file($name, $errorDir);
        push @$fileNames, $copyName;
        print 'compyName = ', "$name\n";
    }
    return $fileNames;
}

sub copy_file {
    my ($name, $dir) = @_;
    my ($_name,$_path,$_suffix) = fileparse($name, qr/\.[^.]*/);
    my $num = 1;
    my $fname = File::Spec->catfile($dir , $_name . $_suffix);
    while (-f $fname) {
        $fname = File::Spec->catfile($dir , $_name . "_$num" . $_suffix);
        $num += 1;
    }
    copy($name, $fname);
    return $fname;
}

unless (caller) {
    my $dirname = '';
    GetOptions('dir=s' => \$dirname);
    check_dir($dirname);
}

1;
