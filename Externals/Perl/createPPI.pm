#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

use Log::Message::Simple qw[msg error debug
	carp croak cluck confess];

=pod
=head1 Модуль используется для создания ppi файлов по заданным модулям
=cut

BEGIN {
	use FindBin qw($Bin);
	print "Bin = $Bin\n";
	use lib "$Bin/PerlHandler/lib/";
}

use PerlHandler::Utils;
&PerlHandler::Utils::create_ppi($ARGV[0], $ARGV[1]);
