#coding:cp1251
import os, sys
import redbaron
from redbaron import RedBaron
import baron.parser, redbaron.nodes
#import logs
import glbls, dtbase, sensors, logs
import nodenames

#������ ������� ��� ������ � ���������� node-���>>>>>>>>>>
#>>>>>>>>>>>>>>>>strange_nodes>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#additional_node
def UniversalNodeExec(node, database):
 node.op = mk_op_by_node(node)
 database.operations.append(node.op)
 st = stoperational_by_node(node, database)
 database.statements.append(st)
 node.sttnmt = st
 node.hasstat = True
#additional_node

def CodeBlockNodeExec(node, database):
 return

def CommaProxyListExec(node, database):
 return

def DotProxyListExec(node, database):
 return

def ElseAttributeNodeExec(node, database):
 info = str(glbl.current_module) + "   " + str(node.type) + "   " + str(node.dumps()) + "\n"
 logs.log_file(9, info)

def IfElseBlockSiblingNodeExec(node, database):
 info = str(glbl.current_module) + "   " + str(node.type) + "   " + str(node.dumps()) + "\n"
 logs.log_file(9, info)

def LineProxyListExec(node, database):
 return

def LiteralyEvaluableExec(node, database):
 info = str(glbl.current_module) + "   " + str(node.type) + "   " + str(node.dumps()) + "\n"
 logs.log_file(9, info)

def NodeExec(node, database):
 return

def NodeListExec(node, database):
 return
#<<<<<<<<<<<<<<<<strange_nodes<<<<<<<<<<<<<<<<<<<<

#>>>>>>>>>>>>>>>>base_nodes>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def ArgumentGeneratorComprehensionNodeExec(node, database):
 #node.result
 pass

#������ node
def AssertNodeExec(node, database):
 #value - what checking for "True"
 #message - what talking, when "false"
 pass

'''
def AssignmentExec(node, database):
 pass
'''

def AssignmentNodeExec(node, database):
 '''
 if type(node.target) == redbaron.nodes.NameNode:
  if not hasattr(node, "processed"):
   NameNodeExec(node.target, database, True)
   node.target.processed = True
  if (node.target.namekind == 'var'):
   name = node.name
   for i in glbl.var_names_to_ids.get(name):
 '''
 pass

def AssociativeParenthesisNodeExec(node, database):
   pass

def AtomtrailersNodeExec(node, stek, across_tree):
 '''
 i = len(node.value.data)
 prevname = ""; y = 1
 if i>2:
  logs.log_file(1, str(type(node.value.data[0][0])) + "\n", 0)
  prevname = str(node.value.data[0][0].value)
  while y < i - 2:
   logs.log_file(1, str(type(node.value.data[y][0])) + "\n", 0)
   prevname = prevname + "." + str(node.value.data[y][0].value)
   y+=1
 logs.log_file(1, str(type(node.value.data[i-2][0])) + "\n", 0)
 if type(node.value.data[i-1][0]) == redbaron.nodes.CallNode:
  #!!!
  for nod in node.value.data[i-1]:
   if nod:
    across_tree(nod,stek)
 elif type(node.value.data[i-1][0]) == redbaron.nodes.GetitemNode:
  #!!!
  pass
 '''

def AtomtrailersNodeExec(node, database):
 return #������ ���� ���������

def BinaryNodeExec(node, database):
   return #nothing_t0_do

def BinaryOperatorNodeExec(node, database):
   pass

def BinaryRawStringNodeExec(node, database):
   return #nothing_t0_do

def BinaryStringNodeExec(node, database):
   return #nothing_to_do

def BooleanOperatorNodeExec(node, database):
   pass

def BreakNodeExec(node, database):
 set_sens_num_to_sttmnt(node)
 st = stbrk_by_node(node)
 if (st != None):
  database.statements.append(st)
  node.hasstat = True


def CallNodeExec(node, database):
 return #nothing_to_do

def CallArgumentNodeExec(node, database):
   pass

def ClassNodeExec(node, database):
   #������ ���� ��� ���������
   return

def CommaNodeExec(node, database):
   return #noting_to_do

def ComparisonNodeExec(node, database):
   pass

def ComparisonOperatorNodeExec(node, database):
   pass

def ComplexNodeExec(node, database):
   return #nothind_to_do
#------------------------------------------------
#������ Nod-�
def ComprehensionIfNodeExec(node, database):
   pass

def ComprehensionLoopNodeExec(node, database):
   pass
#------------------------------------------------

def ContinueNodeExec(node, database):
 set_sens_num_to_sttmnt(node)
 st = stcntn_by_node(node)
 if (st != None):
  database.statements.append(st)
  node.hasstat = True

def DecoratorNodeExec(node, database):
   #������ ���� ��� ���������
   return

def DefNodeExec(node, database, inclass = None):
 if not(hasattr(node, 'id')):
  node.id = set_id_to_obj(node)
 if not(hasattr(node, 'kind')):
  node.kind = dtbase.EnumeratedObjects.FunctionKindType.get('0')
 index = len(node.value.data) - 1
 indexx = index
 while (node.value.data[index][0].type == "endl"):
    index = index - 1
 #��������� return, ���� ��������� statement �� return>>>>>>
 if ((node.value.data[index][0].type != "return")and(node.value.data[index][0].type != "raise")):
   plc = glbls.PlcInText(glbl.current_file, node.value.data[indexx][0].absolute_bounding_box.bottom_right.line,
    node.value.data[indexx][0].absolute_bounding_box.bottom_right.column)
   nd = mk_empty_node_sttmnt(plc, 8, node.value.data[index][0].indentation)
   sens = sensors.sensor(nd,
    glbl.ret_free_num(glbl.used_sens, glbl.free_sens, "last_used_sens", glbl),
    1, glbl.ndv_level,
    (nd.absolute_bounding_box.bottom_right.line - 1), 1)
   '''
   sens = sensors.sensor(node.value.data[index][0],
    glbl.ret_free_num(glbl.used_sens, glbl.free_sens, "last_used_sens", glbl),
    1, glbl.ndv_level,
    (node.absolute_bounding_box.bottom_right.line - 1), 1)
   '''
   nd.bfsens = sens
   node.op = dtbase.OperationType(
    glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl), [])
   st = stret_by_node(nd, database)
   refst = dtbase.StatementReferenceType(st.id, 8)
   node.value.data[indexx][0].nextinlinearblock = refst
   database.statements.append(st)
   nd.hasstat = True
   glbl.sens_stor.appnd(sens)
   delattr(nd, 'bfsens')
   nd.processed = True
 #<<<<<��������� return, ���� ��������� statement �� return

 #��������� ����������
 decs = []
 if (len(node.decorators.data) > 0):
  for dec in node.decorators.data:
   dec[0].value.value.data[0].processed = True
   s = str(dec[0].value.value.data[0].value)
   if not glbls.PythonStandartNames.Decorators.has_key(s):
    decs.append(s)
 if len(decs) > 0:
  glbl.f_decs[node.id] = decs
 node.id = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)
 nextnode = node.value.data[0][0]
 if not hasattr(nextnode,'id'):
  nextnode.id = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)
 if not hasattr(nextnode, "bfsens"):
  #nextnode.bfsens = set_sens_num_to_sttmnt(nextnode)
  set_sens_num_to_sttmnt(nextnode)
 #else:

 st = dtbase.StatementReferenceType(nextnode.id, nodenames.ret_sttmnt_kind(nextnode))

 #���� ��� ��������� �� �����, �� � ������ �������
 glbl.stnodes.append(nextnode)
 plc = glbls.PlcInText()
 plc.init_by_node(node, node.id)
 path = node.path().to_baron_path()
 if inclass:
  fnc = dtbase.MethodType(plc, st, node.id, node.name, glbl.current_module, decs, path)
  fnc.kind = dtbase.EnumeratedObjects.FunctionKindType.get('1')
 else:
  fnc= dtbase.FunctionType(plc, st, node.id, node.name, glbl.current_module, decs, path)
  fnc.kind = dtbase.EnumeratedObjects.FunctionKindType.get('0')
 database.functions.append(fnc)
 node.processed = True
 node.hasstat = True
 return fnc

def DefArgumentNodeExec(node, database):
   pass

def DelNodeExec(node, database):
   pass

def DictArgumentNodeExec(node, database):
   pass

def DictNodeExec(node, database):
   pass

def DictitemNodeExec(node, database):
   pass

def DictComprehensionNodeExec(node, database):
   pass

def DottedAsNameNodeExec(node, database):
   pass

def DotNodeExec(node, database):
   pass

def ElifNodeExec(node, database):
   #������ ���� ��� ���������
   return

def EllipsisNodeExec(node, database):
   return #nothing_to_do

def ElseNodeExec(node, database):
   #������ ���� ��� ���������
   return

def EndlNodeExec(node, database):
   return #nothing_to_do

def ExceptNodeExec(node, database):
 #������ ���� ��� ���������
   return

def ExecNodeExec(node, database):
   pass

def FloatExponantComplexNodeExec(node, database):
   return #nothing_to_do

def FloatExponantNodeExec(node, database):
   return #nothing_to_do

def FloatNodeExec(node, database):
   return #nothing_to_do

def FinallyNodeExec(node, database):
   #������ ���� ��� ���������
   return

def ForNodeExec(node, database):
 #node.bfsens = set_sens_num_to_sttmnt(node.value.data[0][0])
 set_sens_num_to_sttmnt(node.value.data[0][0])
 st = stfch_by_node(node, database)
 database.statements.append(st)
 node.hasstat = True

def FromImportNodeExec(node, database):
   pass

def GeneratorComprehensionNodeExec(node, database):
   pass

def GetitemNodeExec(node, database):
   pass

def GlobalNodeExec(node, database):
   #������ ���� ��� ���������
   return

def IfNodeExec(node, database):
   #������ ���� ��� ���������
   return

def IfelseblockNodeExec(node, database):
   num = -1
   for nn in node.value.data:
    set_sens_num_to_sttmnt(nn.value.data[0][0])
    #if not hasattr(node, "bfsens"):
    # node.bfsens = sens
   st = stif_by_node(node, database)
   database.statements.append(st)
   node.hasstat = True

def ImportNodeExec(node, database):
   pass

def IntNodeExec(node, database):
   return #nothing_to_do

def LambdaNodeExec(node, database):
   pass

def LeftParenthesisNodeExec(node, database):
   return #nothing_to_d

def ListNodeExec(node, database):
   pass

def ListArgumentNodeExec(node, database):
   pass

def ListComprehensionNodeExec(node, database):
   pass

def LongNodeExec(node, database):
   return #nothing_to_d

def NameNodeExec(node, database, inclass = None):
 if hasattr(node, "processed"):
  return node
 if (node.__class__.__name__ != "NameNode"):
  info = "!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
  logs.log_file(9, info)
  node.namekind = 'other'
  return
 prnt = node.parent
 prnttype = prnt.__class__.__name__
 if (prnttype == "FromImportNode"):
  #���� ��� �������������� ������
  if not(node.on_attribute == "target"):
   node.processed = True
   node.namekind = 'other'
   return
 #------------------------------------------------------
 #������� � ���������� ������������ ������������
 if prnt.__class__.__name__ == "ComprehensionLoopNodeExec":
  #ComprehensionLoopNodeExec(pr, database) #!!!!!!!!!!!!!!!!!!!!!
  prnt.processed = True
  node.processed = True
  node.namekind = 'other'
  return
 if (prnttype == "ImportNode"
   or prnttype == "DictitemNode"):
  #���� ��� �������������� ������:
  node.processed = True
  node.namekind = 'other'
  return
 #� ��������� ������ - ������������ ����������
 name = node.value
 #����� �������
 if is_name_called(node):
  node.namekind = 'fnc'
  node.kind = 0 #!!!!!!!!!!!
  name = node.value
  #����� ������� � ���������� id
  if (glbl.func_names_to_ids.has_key(name)):
   for i in glbl.func_names_to_ids.get(name):
    if (glbl.obj_modules.get(i) == glbl.current_module):
     node.id = i
     break
   else:
    node.id = glbl.func_names_to_ids.get(name)[0]
  #����� - ������� ����� ������ �������
  else:
   set_id_to_obj(node)
   plc = glbls.PlcInText()
   plc.init_by_node(node, node.id)
   path = node.path().to_baron_path()
   fnc = dtbase.FunctionType(plc, dtbase.StatementReferenceType(),
      node.id, node.value, glbl.current_module, [], path)
   database.functions.append(fnc)
   glbl.refresh_dics(0,fnc)
  node.processed = True
  return node
#------------------------------------------------------
 #��������� � ����������
 else:
  node.namekind = 'var'
  node.kind = 0
  set_id_to_obj(node)
  if (inclass == True):
   nm = dtbase.FieldType()
   node.kind = 1
  else:
   nm = dtbase.VariableType()
   node.kind = 0
  nm.init_by_node(node)
  nm.add_place(node)
  glbl.refresh_dics(1,nm)
  return nm
#------------------------------------------------------
 return


def NameAsNameNodeExec(node, database):
   pass

def PrintNodeExec(node, database):
   return #nothing_to_d

def RaiseNodeExec(node, database):
 set_sens_num_to_sttmnt(node)
 st = stthr_by_node(node, database)
 database.statements.append(st)
 node.hasstat = True


def ReprNodeExec(node, database):
   pass

def ReturnNodeExec(node, database, app = None):
 set_sens_num_to_sttmnt(node, app)
 st = stret_by_node(node, database)
 database.statements.append(st)
 node.hasstat = True

def RightParenthesisNodeExec(node, database):
   return #nothing_to_d

def SemicolonNodeExec(node, database):
   return #nothing_to_d

def SetNodeExec(node, database):
   pass

def SetComprehensionNodeExec(node, database):
   pass

def SliceNodeExec(node, database):
   return #nothing_to_d

def SpaceNodeExec(node, database):
   return #nothing_to_d

def StringNodeExec(node, database):
   return #nothing_to_d

def StringChainNodeExec(node, database):
   return #nothing_to_d

def TernaryOperatorNodeExec(node, database):
   pass

def TryNodeExec(node, database):
 st = sttry_by_node(node)
 database.statements.append(st)
 node.hasstat = True

def TupleNodeExec(node, database):
   pass

def UnicodeStringNodeExec(node, database):
   return #nothing_to_d

def UnicodeRawStringNodeExec(node, database):
   return #nothing_to_d

def UnitaryOperatorNodeExec(node, database):
   pass

def YieldNodeExec(node, database):
 #node.bfsens = set_sens_num_to_sttmnt(node.value)
 set_sens_num_to_sttmnt(node.value)
 st = styldret_by_node(node, database)
 database.statements.append(st)
 node.hasstat = True

def YieldAtomNodeExec(node, database):
 YieldNodeExec(node, database)

def WhileNodeExec(node, database):
 #node.bfsens = set_sens_num_to_sttmnt(node.value.data[0][0])
 set_sens_num_to_sttmnt(node.value.data[0][0])
 st = stwhl_by_node(node, database)
 database.statements.append(st)
 node.hasstat = True

def WithContextItemNodeExec(node, database):
 st = stus_by_node(node, database)
 database.statements.append(st)
 node.hasstat = True

def WithNodeExec(node, database):
 for wth in node.contexts:
  st = stus_by_node(wth, database)
  database.statements.append(st)
  node.hasstat = True

def CommentNodeExec(node, database):
   return #nothing_to_do

def PassNodeExec(node, database):
   return #nothing_to_d
#<<<<<<<<<<<<<<<base_nodes<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

#>>>>>>>>>>>>>>additional_nodes>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#addsens - ������ ��� return � �� return
def EmptyNode(node, database, addsensfl, app = None, special = None, red = None):
 if (hasattr(node, 'id')):
  i = node.id
 else:
  i = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)
  node.id = i
 if not(hasattr(node, 'stkind')):
  node.stkind = 7
  node.op = dtbase.OperationType(i, [])
  database.operations.append(node.op)
 if (addsensfl) and (not hasattr(node, 'bfsens')):
  set_sens_num_to_sttmnt(node, app)
 #���� ��� �� � ����
 if not(hasattr(node, 'processed')):
  st = stoperational_by_node(node, database)
  node.processed = True
  node.sttnmt = st
 else:
  if not(hasattr(node, 'sttnmt')):
   st = stoperational_by_node(node, database)
   node.sttnmt = st
 if (special != None):
  st.firstsymbollocation = glbls.PlcInText(glbl.current_file,
    red.absolute_bounding_box.bottom_right.line,
    red.absolute_bounding_box.bottom_right.column,
    None, None,glbl.current_module)
  st.lastsymbollocation = glbls.PlcInText(glbl.current_file,
    red.absolute_bounding_box.bottom_right.line + 1,
    1,
    None, None,glbl.current_module)
 database.statements.append(st)
 node.hasstat = True
 return st
#<<<<<<<<<<<<<<<additional_nodes<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>st_by_node>>>>>>>>>>>>>>>>>>>>>>>>>>
#� ������� ������ ������ ������� �� ����� �����
#node-� ������ ���� ��� ������������:
#���� ��������������� ������, ���� ��������� �������� - �� ������
#>>>>>>>>>>>>>������������ ����������� �� ������ node-��>>>>>>>>>>>>>
def st_init_by_node(st, node):
 if not(hasattr(node,'id')):
  node.id = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)
 plst = glbls.PlcInText()
 plst.init_by_node(node, node.id)
 plnd = glbls.PlcInText()
 plnd.init_end_by_node(node, node.id)
 #init
 st.id = node.id
 st.firstsymbollocation = plst
 st.lastsymbollocation = plnd
 if (hasattr(node,'bfsens')):
  if hasattr(node.bfsens, 'sensnum'):#!!!!!!!!!!!!!!!!
   st.sensorbeforethestatement = node.bfsens.sensnum
  else:
    logs.log_file(9, "propal sensor" + str(node.absolute_bounding_box.top_left.line) + "\t" + str(node.absolute_bounding_box.top_left.column))
 if (node.next != None):
  st.nextinlinearblock = mk_stref_by_node(node.next)
 else:
  if hasattr(node, 'nextinlinearblock'):
   st.nextinlinearblock = node.nextinlinearblock
 set_kind(node, st)
 node.sttnmt = st

def stbrk_by_node(node):
 node.stkind = 0
 st = dtbase.STBreakType()
 st_init_by_node(st, node)
 it_node = find_nearly_iterator(node)
 if (it_node == None):
  #print "cawabanga!"
  return None
 if not(hasattr(it_node,'id')):
  it_node.id = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)
 i = nodenames.LoopSttmnts.get(it_node.type)
 st.breakingloop = dtbase.StatementReferenceType(it_node.id, i)
 return st

def stcntn_by_node(node):
 node.stkind = 1
 st = dtbase.STContinueType()
 st_init_by_node(st, node)
 it_node = find_nearly_iterator(node)
 if (it_node == None):
  #print "cawabanga!"
  return None
 if not(hasattr(it_node,'id')):
  it_node.id = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)
 i = nodenames.LoopSttmnts.get(it_node.type)
 st.continuingloop = dtbase.StatementReferenceType(it_node.id, nodenames.LoopSttmnts.get(i))
 return st

def stfch_by_node(node, database):
 node.stkind = 4
 st = dtbase.STForEachType()
 if not(hasattr(node.value.data[0][0],'id')):
  node.value.data[0][0].id = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)
 knd = nodenames.ret_sttmnt_kind(node.value.data[0][0])
 st.body = dtbase.StatementReferenceType(node.value.data[0][0].id, knd)
 set_sens_num_to_sttmnt(node.value.data[0][0])
 st_init_by_node(st, node)
 rer_node = node.value.data[0][0]
 st.head = mk_op_by_node(node.target)
 database.operations.append(st.head)
 return st

def stwhl_by_node(node, database):
 node.stkind = 2
 st = dtbase.STDoAndWhileType()
 st_init_by_node(st, node)
 st.body = mk_stref_by_node(node.value.data[0][0])
 if getattr(node, 'else') != None:
  st.els = mk_stref_by_node(getattr(node,'else').value.data[0][0], 'else')
 else:
  st.els = None
 op = mk_op_by_node(node.test)
 database.operations.append(op)
 st.condition = op.id
 return st

def stif_by_node(node, database):
 node.stkind = 6
 st = dtbase.STIfType()
 st_init_by_node(st, node)
 op = mk_op_by_node(node.value.data[0].test)
 database.operations.append(op)
 st.condition = op.id
 #op = mk_op_by_node(node.value.data[0].value.data[0][0])
 #database.operations.append(op)
 st.then = mk_stref_by_node(node.value.data[0].value.data[0][0])
 for i, n in enumerate(node.value.data[1:]):
  if (n.type is 'elif'):
   lif = dtbase.elseIf()
   lif.then = mk_stref_by_node(n.value.data[0][0])
   op = mk_op_by_node(n.test)
   database.operations.append(op)
   lif.condition = op.id
   st.elseif.append(lif)
  else:
   st.els = mk_stref_by_node(n.value.data[0][0])
 return st

def stoperational_by_node(node, database):
 #�������� ������ ���� ���������!!!
 node.stkind = 7
 st = dtbase.STOperationalType()
 st_init_by_node(st, node)
 if hasattr(node, 'op'):
  st.operationid = node.op.id
 else:
  opp = mk_op_by_node(node)
  database.operations.append(opp)
  st.operationid = opp.id
 return st

def stret_by_node(node, database):
 node.stkind = 8
 st = dtbase.STReturnType()
 st_init_by_node(st, node)
 st.returnoperation = mk_op_by_node(node.value)
 database.operations.append(st.returnoperation)
 return st

def stthr_by_node(node, database):
 node.stkind = 10
 st = dtbase.STThrowType()
 st_init_by_node(st, node)
 st.exception = mk_op_by_node(node.value)
 database.operations.append(st.exception)
 return st

def sttry_by_node(node):
 node.stkind = 11
 st = dtbase.STTryCatchFinallyType()
 st_init_by_node(st, node)
 #-----------------------------
 if hasattr(getattr(node, 'else'), 'value'):
  if(len(getattr(node, 'else').value.data[0]) > 0):
   ls = getattr(node,'else').value.data[0][0]
   if (ls != None):
    ref = mk_stref_by_node(ls)
    set_sens_num_to_sttmnt(ls)
 else:
  ref = None
 st.els = ref
 if hasattr(getattr(node, 'finally'), 'value'):
  if(len(getattr(node, 'finally').value.data[0]) > 0):
   fn = getattr(node, 'finally').value.data[0][0]
 else:
  fn = None
 #-----------------------------
 if (fn != None):
  ref = mk_stref_by_node(fn)
  set_sens_num_to_sttmnt(fn)
 else:
  ref = None
 st.finall = ref
 #-----------------------------
 exs = []
 if (len(node.excepts.data) > 0):
  for ex in node.excepts.data:
   ref = mk_stref_by_node(ex.value.data[0][0])
   set_sens_num_to_sttmnt(ex.value.data[0][0])
   exs.append(ref)
 st.ctch = exs
 #-----------------------------
 tr = node.value.data[0][0]
 ref = mk_stref_by_node(tr)
 set_sens_num_to_sttmnt(tr)
 #sss = set_sens_num_to_sttmnt(tr)
 #st.sensorbeforethestatement = sss.sensnum
 st.tr = ref
 return st

def stus_by_node(node, database):
 node.stkind = 12
 st = dtbase.STUsingType()
 st_init_by_node(st, node)
 st.body = mk_stref_by_node(node.parent.value.data[0][0])
 st.head = mk_op_by_node(node.value)
 database.operations.append(st.head)
 return st

def styldret_by_node(node, database):
 node.stkind = 13
 st = dtbase.STYieldReturnType()
 st_init_by_node(st, node)
 st.yieldreturnoperation = mk_op_by_node(node.value)
 database.operations.append(st.yieldreturnoperation)
 return st

#<<<<<<<<<<<<<<<<<<<<<<<st_by_node<<<<<<<<<<<<<<<<<<<<<



#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def mk_empty_node_sttmnt(plc, kind, ind):
 #node = type(SynteticNode, (), {id = -1})
 node = glbls.Empty()
 node.id = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)
 node.absolute_bounding_box = glbls.Empty()
 node.absolute_bounding_box.top_left = baron.path.Position(plc)
 node.absolute_bounding_box.bottom_right = baron.path.Position(plc)
 node.stkind = kind
 node.indentation = ind
 node.next = None
 node.value = None
 return node

def mk_op_by_node(node):
 if (node == None):
  return dtbase.OperationType(
    glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl), [])
 if not hasattr(node, 'op'):
  op = dtbase.OperationType()
  dtbase.set_id_to_dtbase_obj(op)
  v = []
  f = []
  c = []
  names = node.find_all("NameNode")
  for name in names:
   if not(hasattr(name, 'namekind')):
    info = str(glbl.current_module) + "   " + str(node.type) + "   " + str(node.dumps()) + "\n"
    logs.log_file(9, info)
   if name.namekind == 'var':
    if not hasattr(name, 'kind'):
     print "cawabanga!"
     name.kind = 0
    vv = dtbase.VariableReferenceType(name.id, name.kind)
    v.append(vv)
   elif name.namekind == 'fnc':
    if name.parent.__class__.__name__ != 'DefNode':
     if glbl.f_decs.has_key(name.id):
      c.append(dtbase.CallNodeType(f, v))
      op.callnode.append(c)
      f = []; v = []
      decs = glbl.f_decs.get(name.id)
      for dec in decs:
       ids = glbl.func_names_to_ids.get(dec)
       #��������� ��� ������� � ����� ������
       for i in ids:
        ff = dtbase.FunctionReferenceType(i, glbl.f_knds.get(i))
        f.append(ff)
       c.append(dtbase.CallNodeType(f, v))
       op.callnode.append(c)
     ff = dtbase.FunctionReferenceType(name.id, name.kind)
     f.append(ff)
    else:
     continue      #���� ��� ����������� (��� ����������)
  c.append(dtbase.CallNodeType(f, v))
  op.callnode.append(c)
 else:
  op = node.op
 return op

def mk_stref_by_node(node, elflag = None):
 if not hasattr(node, 'stkind'):
  node.stkind = nodenames.ret_sttmnt_kind(node)
 if not hasattr(node, 'id'):
  dtbase.set_id_to_dtbase_obj(node)
 #��������� ������� ������ ������, ����� �����
 #���������, ��� node ����� ���������
 if (hasattr (node, "hasstat")):
  if (node.hasstat != True):
   node.hasstat = False
 glbl.stnodes.append(node)
 if elflag != None:
  return dtbase.ElsType(node.id, node.stkind)
 else:
  return dtbase.StatementReferenceType.init_by_node(node)
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<




#>>>>>>>>>>������� ��� ������ � ����������>>>>>>>>>>>>>>>
def set_id_to_obj(obj):
 if(hasattr(obj, 'id')):
  return
 else:
  obj.id = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)

def set_sens_num_to_sttmnt(node, app = None):
 ret = 0; pref = 1
 if (hasattr(node,'bfsens')):
  if (node.type == 'return'):
    #if not(hasattr(node.bfsens, "sensnum")):
       sensnum = glbl.ret_free_num(glbl.used_sens, glbl.free_sens, "last_used_sens", glbl)
       sens = sensors.sensor(node, sensnum, 0, glbl.ndv_level, node.absolute_bounding_box.top_left.line, 1)
       glbl.sens_stor.storage.append(sens)
       #print("something strange (set_sens_num): " + str(sensnum))
  return node.bfsens
 else:
  if (node.type == 'return'):
   ret = 1
  if (node.parent.__class__.__name__ == 'YieldAtomNode') or (node.parent.__class__.__name__ == 'YieldNode'):
   ret = 2
  sensnum = glbl.ret_free_num(glbl.used_sens, glbl.free_sens, "last_used_sens", glbl)
  sens = sensors.sensor(node, sensnum, ret, glbl.ndv_level, node.absolute_bounding_box.top_left.line, pref)
  if(app == None):
   glbl.sens_stor.storage.append(sens)
  glbl.lastsens = sensnum
  node.bfsens = sens
  return node.bfsens

def add_processed_label_to_node(node):
 node.processed = True

def was_this_node_processed(node):
 if hasattr(node, 'processed'):
  return 1
 else:
  return 0

def del_processed_label_from_node(node):
 delattr(node, 'processed')

def set_kind(node, st):
 if not (hasattr(node, 'stkind')):
  node.stkind = 7
 kind = node.stkind
 st.kind = dtbase.EnumeratedObjects.StatementKindType.get(kind)


def is_node_operational(node, database):
 if not hasattr(node, 'stkind'):
  st = mk_op_by_node(node)
  database.statements.append(st)
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


#>>>>>>>>>>>>>>>>>see_other_nodes>>>>>>>>>>>>>>>>
def ret_next_node(node):
 if(node.next != None):
  return True
 else:
  return None

def find_nearly_iterator(node):
 nearly = None
 while (nearly == None):
  if hasattr(node, "parent"):
   if hasattr(node.parent, "root"):
    root = node.parent.root
    index = node.index_on_parent
    path = node.path().to_baron_path()
    ln = len(path)
    if ln > 2:
     i = 0
     while i < ln:
      temppath = path[0:ln-i-2]
      tempnode = redbaron.Path.from_baron_path(root, temppath)
      if(hasattr(tempnode.node, 'iterator')):
       nearly = tempnode.node
      i+=2
    node = root
   else:
    return None
  else:
   return None
 return nearly

#���� �� ��������� �������, �� ������� �� �����
def is_name_called(node):
 root = node.parent.root
 path = node.path().to_baron_path()
 ln = len(path)
 if (type(path[ln-1])==int):
  path[ln-1] = int(path[ln-1]) + 1
  tempnode = redbaron.Path.from_baron_path(root, path)
  if hasattr(tempnode, 'node'):
   if type(tempnode.node) is redbaron.nodes.CallNode:
    node.namekind = 'fnc'
    node.kind = 0 #!!!!!!!!!!!!!!!!!!!!
    return True
 return False

def is_node_global(node):
 answer = []
 root = node.parent.root
 path = node.path().to_baron_path()
 i = 1; temppath = []; inclass = 0
 while (i < len(path)):
  temppath = path[0:i]
  tempnode = redbaron.Path.from_baron_path(root, temppath)
  if hasattr(tempnode, 'node'):
   if type(tempnode.node) is redbaron.nodes.ClassNode:
    inclass = 1
   elif type(tempnode.node) is redbaron.nodes.DefNode:
    return 'local'
    break;
  i+=2
 else:
  if inclass == 1:
   return 'inclass'
  return 'global'
#<<<<<<<<<<<<<<<<<see_other_nodes<<<<<<<<<<<<<<<<<<<

#>>>>>>>>>>>>>comparing_nodes>>>>>>>>>>>>>>>>>>>>>>>
def compare_nodes(fnode, snode):
 if glbl.current_module != glbl.obj_modules.get(snode.id):
  return False
 if not compare_paths(fnode, snode):
  return False
 #global
 return True

#first - ���, second - � ��� ����������
def compare_paths(first, second):
 match_path = False
 pathfirst = first.path().to_baron_path()
 pathsecond = second.path().to_baron_path()
 ln = min(len(pathfirst), len(pathsecond))
 #ln = ret_min_lenpath(pathfirst, pathsecond, 0)
 last_wrong = -1
 match_path = True
 for i in range(0, ln):
  if (pathfirst[i] != pathsecond[i]):
   match_path = False
   last_wrong = i
 if match_path == False:
  '''
  if i == ln - 1:
   return True        #�� ����� ������, �� � ������ ��������
  '''
  if(find_def_in_path(first, pathfirst, i)) and (find_def_in_path(second, pathsecond, i)):
   #����������� ������� ��� ������� �� ����
   return True
 return False

#mask = 0 - min; mask = 1 - max
def ret_min_lenpath(first, second, mask):
 flen = len(first)
 slen = len(second)
 if (flen >= slen) and mask:
  return flen
 if (flen <= slen) and (not mask):
  return slen

def find_def_in_path(node, path, start):
 ln= len(path)
 root = node.root
 while start < ln:
  temppath = path[0:start]
  tempnode = redbaron.Path.from_baron_path(root, temppath)
  if hasattr(tempnode, 'node'):
   if (tempnode.node.__class__.__name__ == "DefNode" or
      tempnode.node.__class__.__name__ == "ClassNode"):
    return False
  start+=1
 return True

def ret_shortest_path(paths):
 minpath = paths[0]
 index = 0
 if (len(paths) > 1):
  i = 1
  for p in paths:
   if len(p) < len(minpath):
    index = i
    minpath = p
    i+=1
    continue
   elif len(p)==len(minpath):
    if ((type(p[len(p)-1]) is int) and (type(minpath[len(minpath)-1]) is int)):
     if p[len(p)-1] < minpath[len(minpath)-1]:
      index = i
      minpath = p
      i+=1
      continue
   else:
    i+=1
 return index

#<<<<<<<<<<<<<comparing_nodes<<<<<<<<<<<<<<<<<<<<<<<



#code block
glbl = glbls.Glbls()