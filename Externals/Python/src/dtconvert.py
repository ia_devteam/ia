#coding:cp1251

import parserxsd as xsd
import dtbase

def convert(dbInp):
 #convert DataBase -> xsd.Database
 db = xsd.Database()
 db.Classes = convert_classes(dbInp.classes)
 db.Variables = convert_variables(dbInp.variables)
 db.Functions = convert_functions(dbInp.functions)
 db.Statements = convert_statements(dbInp.statements)
 db.Operations = convert_operations(dbInp.operations)
 db.Files = convert_files(dbInp.files)
 return db

# with open('test.xml', 'w') as f:
#  db.export(f, 0)

def empty_el(val):
 if hasattr(val, 'id') and val.id == -1: return True
 return False

def convert_classes(prms):
 rslt = []
 for i in prms:
  if empty_el(i): continue
  rslt.append(
   xsd.ClassType(
    ID = i.id,
    Name = i.name,
    Namespace = i.namespace,
    MethodId = i.methodid,
    FieldId = i.fieldid,
    ParrentClassId = i.parentclassid)) #!!!!
 return xsd.ClassesType(Class = rslt)

#-----------------------------------------------------------------------------
def convert_variables(prms):
 _vars = []
 _fields = []
 for i in prms:
  if not isinstance(i, dtbase.FieldType):
   if empty_el(i): continue
   v = xsd.VariableType(
     ID = i.id,
     Name = i.name,
     Namespace = i.namespace,
     isGlobal = i.isglobal,
     Definition = convert_definition(i.definition),
     AssignmentFrom = convert_assignment_from(i.assignmentfrom),
     PointsToFunctions = convert_points_to_functions(i.pointstofunctions),
     ValueCallPosition = convert_value_call_position(i.valuecallposition), #equal
     ValueGetPosition = convert_value_get_position(i.valuegetposition),
     ValueSetPosition = convert_value_set_position(i.valuesetposition)
    )
  else:
    if i.initializerid == -1: continue
    v = xsd.FieldType(
     ID = i.id,
     Name = i.name,
     Namespace = i.namespace,
     isGlobal = i.isglobal,
     Definition = convert_definition(i.definition),
     AssignmentFrom = convert_assignment_from(i.assignmentfrom),
     PointsToFunctions = convert_points_to_functions(i.pointstofunctions),
     ValueCallPosition = convert_value_call_position(i.valuecallposition), #equal
     ValueGetPosition = convert_value_get_position(i.valuegetposition),
     ValueSetPosition = convert_value_set_position(i.valuesetposition),

     isStatic = i.isstatic,
     InitializerId = i.initializerid)
  if isinstance(i, dtbase.FieldType):
   _fields.append(v)
  else:
   _vars.append(v)
 return xsd.VariablesType(Variable = _vars, Field = _fields)

def convert_definition(prms):
 return xsd.DefinitionType(convert_location_line(prms))

def convert_location_line(prms):
 return xsd.LocationLine(line = prms.line, column = prms.column, file = prms.fileid)

def convert_assignment_from(prms):
 rslt = []
 for i in prms:
  rslt.append(xsd.AssignmentFromType(
   VariableReference = convert_variable_reference_type(i.variablereference),
   LocationLine = convert_location_line(i.location)))
 return rslt

def convert_points_to_functions(prms):
 rslt = []
 for i in prms:
  if empty_el(i.variablereference): continue
  rslt.append(xsd.AssignmentFromType(
   VariableReference = convert_function_reference_type(i.variablereference),
   LocationLine = convert_location_line(i.location)))
 return rslt
convert_value_call_position = convert_points_to_functions

def convert_value_get_position(prms):
 rslt = []
 for i in prms:
  rslt.append(xsd.ValueGetPositionType(LocationLine = convert_location_line(i)))
 return rslt
convert_value_set_position = convert_value_get_position

#-----------------------------------------------------------------------------
def convert_functions(prms):
 _functions = []
 _methods = []
 for i in prms:
  if empty_el(i): continue
  v = xsd.FunctionType(
    ID = i.id,
    Name = i.name,
    Namespace = i.namespace,
    Definition = convert_definition(i.definition),
    EntryStatement = convert_statement_reference_type(i.entrystatement))
  if isinstance(i, dtbase.MethodType):
   v.IsOverrideBase = i.isoverridebase
   _methods.append(v)
  else:
   _functions.append(v)
 return xsd.FunctionsType(Function = _functions, Method = _methods)

def convert_statement_reference_type(prms):
 if prms == None or empty_el(prms):
  return None
 return xsd.StatementReferenceType(
  ID = prms.id,
  kind = prms.kind)

#-----------------------------------------------------------------------------

def convert_statements(prms):
 _stbreak = []
 _stcontinue = []
 _stdoandwhile = []
 _stforeach = []
 _stgoto = []
 _stif = []
 _stoperational = []
 _streturn = []
 _stswitch = []
 _stthrow = []
 _sttrycatchfinally = []
 _stusing = []
 _styieldreturn = []
 for i in prms:
  if empty_el(i): continue
  if isinstance(i,dtbase.STBreakType):
   converted_statement = convert_stbreak(i)
   if converted_statement != None:
    _stbreak.append(converted_statement)
  elif isinstance(i, dtbase.STContinueType):
   converted_statement = convert_stcontinue(i)
   if converted_statement != None:
    _stcontinue.append(converted_statement)
  elif isinstance(i, dtbase.STDoAndWhileType):
   converted_statement = convert_stdoandwhile(i)
   print "converted statement = ", converted_statement
   if converted_statement != None:
    _stdoandwhile.append(converted_statement)
  elif isinstance(i, dtbase.STForEachType):
   converted_statement = convert_stforeach(i)
   if converted_statement != None:
    _stforeach.append(converted_statement)
  elif  isinstance(i, dtbase.STIfType):
   converted_statement = convert_stif(i)
   if converted_statement != None:
    _stif.append(converted_statement)
  elif  isinstance(i, dtbase.STOperationalType):
   converted_statement = convert_stoperational(i)
   if converted_statement != None:
    _stoperational.append(converted_statement)
  elif isinstance(i, dtbase.STReturnType):
   converted_statement = convert_streturn(i)
   if converted_statement != None:
    _streturn.append(converted_statement)
  elif isinstance(i, dtbase.STThrowType):
   converted_statement = convert_stthrow(i)
   if converted_statement != None:
    _stthrow.append(converted_statement)
  elif isinstance(i, dtbase.STTryCatchFinallyType):
   converted_statement = convert_sttrycatchfinally(i)
   if converted_statement != None:
    _sttrycatchfinally.append(converted_statement)
  elif isinstance(i, dtbase.STUsingType):
   converted_statement = convert_stusing(i)
   if converted_statement != None:
    _stusing.append(converted_statement)
  elif isinstance(i, dtbase.STYieldReturnType):
   converted_statement = convert_styieldreturn(i)
   if converted_statement != None:
    _styieldreturn.append(converted_statement)
 return xsd.StatementsType(
  STBreak=_stbreak,
  STContinue=_stcontinue,
  STDoAndWhile=_stdoandwhile,
  #STFor=_stfor,
  STForEach=_stforeach,
  STGoto=_stgoto,
  STIf=_stif,
  STOperational=_stoperational,
  STReturn=_streturn,
  STSwitch=_stswitch,
  STThrow=_stthrow,
  STTryCatchFinally=_sttrycatchfinally,
  STUsing=_stusing,
  STYieldReturn=_styieldreturn)

def convert_statement_type(prms):
 chk_list = ["id", "nextinlinearblock", "sensorbeforethestatement", "firstsymbollocation", "lastsymbollocation"]
 for k in dir(prms):
  if isinstance(getattr(prms, k), dtbase.OperationType) and k not in chk_list:
   setattr(prms, k, getattr(prms, k).id)

 rslt = {
  "ID": prms.id,
  "NextInLinearBlock": convert_statement_reference_type(prms.nextinlinearblock),
  "SensorBeforeTheStatement": (prms.sensorbeforethestatement if prms.sensorbeforethestatement != -1 else None),
  "FirstSymbolLocation": xsd.FirstSymbolLocationType(LocationLine = convert_location_line(prms.firstsymbollocation)),
  "LastSymbolLocation": xsd.LastSymbolLocationType(LocationLine = [convert_location_line(prms.lastsymbollocation)])
 }
 return rslt

# def convert_statement_reference_type_dict(prms):
#  rslt = {
#   "ID": prms.id,
#   "kind": prms.kind
#  }
#  return rslt

def convert_stbreak(prms):
 r = convert_statement_type(prms)
 r["breakingLoop"] = convert_statement_reference_type(prms.breakingloop)
 return xsd.STBreakType(r["ID"], r["NextInLinearBlock"], r["SensorBeforeTheStatement"], r["FirstSymbolLocation"], r["LastSymbolLocation"], r["breakingLoop"])

def convert_stcontinue(prms):
 r = convert_statement_type(prms)
 r["continuingLoop"] = convert_statement_reference_type(prms.continuingloop)
 return xsd.STContinueType(r["ID"], r["NextInLinearBlock"], r["SensorBeforeTheStatement"], r["FirstSymbolLocation"], r["LastSymbolLocation"], r["continuingLoop"])

def convert_stdoandwhile(prms):
 r = convert_statement_type(prms)
 r["body"] = convert_statement_reference_type(prms.body)
 if r["body"] == None:
  return None
 r["else_"] = filter(lambda x: x != None, [convert_else_type(prms.els)])
 if isinstance(prms.condition, dtbase.OperationType):
  if empty_el(prms.condition):
   return None
  r["condition"] = prms.condition.id
 else:
  if prms.condition == -1:
   return None
  r["condition"] = prms.condition
 r["isCheckConditionBeforeFirstRun"] = prms.isCheckConditionBeforeFirstRun
 return xsd.STDoAndWhileType(r["ID"], r["NextInLinearBlock"], r["SensorBeforeTheStatement"], r["FirstSymbolLocation"], r["LastSymbolLocation"], r["condition"], r["isCheckConditionBeforeFirstRun"], r["body"], r["else_"])

def convert_stforeach(prms):
 r = convert_statement_type(prms)
 r["head"] = prms.head
 if r["head"] == -1: return None
 if isinstance(prms.head, dtbase.OperationType):
  if empty_el(prms.head): return None
  r["head"] = prms.head.id
 r["body"] = convert_statement_reference_type(prms.body)
 if r["body"] == None: return None
 return xsd.STForEachType(r["ID"], r["NextInLinearBlock"], r["SensorBeforeTheStatement"], r["FirstSymbolLocation"], r["LastSymbolLocation"], r["head"], r["body"])

def convert_stif(prms):
 r = convert_statement_type(prms)
 if isinstance(prms.condition, dtbase.OperationType):
  if empty_el(prms.condition): return None
  r["condition"] = prms.condition.id
 else:
  if prms.condition == -1: return None
  r["condition"] = prms.condition
 r["then"] = convert_statement_reference_type(prms.then)
 if r["then"] == None: return None
 r["elseIf"] = convert_else_ifs(prms.elseif)
 r["else_"] = convert_statement_reference_type(prms.els)
 return xsd.STIfType(r["ID"], r["NextInLinearBlock"], r["SensorBeforeTheStatement"], r["FirstSymbolLocation"], r["LastSymbolLocation"], r["condition"], r["then"], r["elseIf"], r["else_"])

def convert_else_ifs(prms):
 rslt = []
 for i in prms:
  if empty_el(i): continue
  rslt.append(convert_else_if(i))
 return rslt

def convert_else_if(prms):
 return xsd.elseIfType(
  condition = prms.condition,
  then = convert_statement_reference_type(prms.then))

def convert_else_type(prms):
 if prms == None or empty_el(prms):
  return None
 return xsd.elseType(prms.id, prms.kind, prms.isonbreak)

def convert_stoperational(prms):
 r = convert_statement_type(prms)
 if prms.operationid == -1:
  return None
 r["operation"] = prms.operationid
 return xsd.STOperationalType(r["ID"], r["NextInLinearBlock"], r["SensorBeforeTheStatement"], r["FirstSymbolLocation"], r["LastSymbolLocation"], r["operation"])

def convert_streturn(prms):
 r = convert_statement_type(prms)
 if isinstance(prms.returnoperation, (dtbase.OperationType, dtbase.STOperationalType)):
  if empty_el(prms.returnoperation): return None
  r["ReturnOperation"] = prms.returnoperation.id
 else:
  r["ReturnOperation"] = prms.returnoperation
 return xsd.STReturnType(r["ID"], r["NextInLinearBlock"], r["SensorBeforeTheStatement"], r["FirstSymbolLocation"], r["LastSymbolLocation"], r["ReturnOperation"])

def convert_stthrow(prms):
 r = convert_statement_type(prms)
 r["exception"] = prms.exception
 if isinstance(prms.exception, dtbase.OperationType):
  if empty_el(prms.exception): return None
  r["exception"] = prms.exception.id
 return xsd.STThrowType(r["ID"], r["NextInLinearBlock"], r["SensorBeforeTheStatement"], r["FirstSymbolLocation"], r["LastSymbolLocation"], r["exception"])

def convert_sttrycatchfinally(prms):
 r = convert_statement_type(prms)
 r["tryBlock"] = convert_statement_reference_type(prms.tr)
 if r["tryBlock"] == None:
  return None
 r["catchBlock"] = filter(lambda x: x != None, [convert_statement_reference_type(i) for i in prms.ctch])
 r["finallyBlock"] = convert_statement_reference_type(prms.finall)
 r["else_"] = convert_statement_reference_type(prms.els)
 return xsd.STTryCatchFinallyType(r["ID"], r["NextInLinearBlock"], r["SensorBeforeTheStatement"], r["FirstSymbolLocation"], r["LastSymbolLocation"], r["tryBlock"], r["catchBlock"], r["finallyBlock"], r["else_"])

def convert_stusing(prms):
 r = convert_statement_type(prms)
 r["body"] = convert_statement_reference_type(prms.body)
 r["head"] = prms.head
 if r["body"] == None or r["head"] == -1:
  return None
 return xsd.STUsingType(r["ID"], r["NextInLinearBlock"], r["SensorBeforeTheStatement"], r["FirstSymbolLocation"], r["LastSymbolLocation"], r["head"], r["body"])

def convert_styieldreturn(prms):
 r = convert_statement_type(prms)
 if isinstance(prms.yieldreturnoperation, dtbase.OperationType):
  if empty_el(prms.yieldreturnoperation): return None
  r["YieldReturnOperation"] = prms.yieldreturnoperation.id
 else:
  if prms.yieldreturnoperation == -1: return None
  r["YieldReturnOperation"] = prms.yieldreturnoperation
 return xsd.STYieldReturnType(r["ID"], r["NextInLinearBlock"], r["SensorBeforeTheStatement"], r["FirstSymbolLocation"], r["LastSymbolLocation"], r["YieldReturnOperation"])

#------------------------------------------------------------------------------
def convert_operations(prms):
 rslt = []
 for i in prms:
  if empty_el(i): continue
  rslt.append(
   xsd.OperationType(
    ID = i.id,
    callNode = convert_call_node(i.callnode)
   ))
 return xsd.OperationsType(Operation = rslt)

def convert_call_node(prms):
 rslt = []
 for i in prms:
  if isinstance(i, list):
   for j in i:
    if isinstance(j, dtbase.CallNodeType):
      rslt.append(
       xsd.callNodeType(
         callFunction = convert_function_reference_type(j.callfunction),
         callVariable = convert_variable_reference_type(j.callvariable)))
  if isinstance(i, dtbase.CallNodeType):
    rslt.append(
     xsd.callNodeType(
      callFunction = convert_function_reference_type(i.callfunction),
      callVariable = convert_variable_reference_type(i.callvariable)))
 return rslt

def convert_function_reference_type(prms):
 rslt = []
 for i in prms:
  if empty_el(i): continue
  rslt.append(xsd.FunctionReferenceType(
   ID = i.id,
   kind = i.kind))
 return rslt

def convert_variable_reference_type(prms):
 rslt = []
 for i in prms:
  if empty_el(i): continue
  rslt.append(xsd.VariableReferenceType(
   ID = i.id,
   kind = i.kind))
 return rslt

#----------------------------------------------------------------
def convert_files(prms):
 rslt = []
 for i in prms:
  if empty_el(i): continue
  rslt.append(
   xsd.FileType(
    fullname = i.fullname,
    ID = i.id))
 return xsd.FilesType(File = rslt)
