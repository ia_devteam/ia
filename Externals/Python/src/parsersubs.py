#!/usr/bin/env python

#
# Generated Mon Apr 24 11:29:38 2017 by generateDS.py version 2.24a.
#
# Command line options:
#   ('-o', 'parserxsd.py')
#   ('-s', 'parsersubs.py')
#
# Command line arguments:
#   ParserToStorageData.xsd
#
# Command line:
#   /usr/local/bin/generateDS -o "parserxsd.py" -s "parsersubs.py" ParserToStorageData.xsd
#
# Current working directory (os.getcwd()):
#   python_parser
#

import sys
from lxml import etree as etree_

import ??? as supermod

def parsexml_(infile, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        parser = etree_.ETCompatXMLParser()
    doc = etree_.parse(infile, parser=parser, **kwargs)
    return doc

#
# Globals
#

ExternalEncoding = 'ascii'

#
# Data representation classes
#


class DatabaseSub(supermod.Database):
    def __init__(self, Files=None, Classes=None, Variables=None, Functions=None, Statements=None, Operations=None):
        super(DatabaseSub, self).__init__(Files, Classes, Variables, Functions, Statements, Operations, )
supermod.Database.subclass = DatabaseSub
# end class DatabaseSub


class NamespaceTypeSub(supermod.NamespaceType):
    def __init__(self, ID=None, Name=None):
        super(NamespaceTypeSub, self).__init__(ID, Name, )
supermod.NamespaceType.subclass = NamespaceTypeSub
# end class NamespaceTypeSub


class ClassTypeSub(supermod.ClassType):
    def __init__(self, ID=None, Name=None, Namespace=None, ParrentClassId=None, FieldId=None, MethodId=None):
        super(ClassTypeSub, self).__init__(ID, Name, Namespace, ParrentClassId, FieldId, MethodId, )
supermod.ClassType.subclass = ClassTypeSub
# end class ClassTypeSub


class VariableReferenceTypeSub(supermod.VariableReferenceType):
    def __init__(self, ID=None, kind=None):
        super(VariableReferenceTypeSub, self).__init__(ID, kind, )
supermod.VariableReferenceType.subclass = VariableReferenceTypeSub
# end class VariableReferenceTypeSub


class VariableTypeSub(supermod.VariableType):
    def __init__(self, ID=None, Name=None, Namespace=None, isGlobal=None, Definition=None, AssignmentFrom=None, PointsToFunctions=None, ValueCallPosition=None, ValueGetPosition=None, ValueSetPosition=None, extensiontype_=None):
        super(VariableTypeSub, self).__init__(ID, Name, Namespace, isGlobal, Definition, AssignmentFrom, PointsToFunctions, ValueCallPosition, ValueGetPosition, ValueSetPosition, extensiontype_, )
supermod.VariableType.subclass = VariableTypeSub
# end class VariableTypeSub


class FieldTypeSub(supermod.FieldType):
    def __init__(self, ID=None, Name=None, Namespace=None, isGlobal=None, Definition=None, AssignmentFrom=None, PointsToFunctions=None, ValueCallPosition=None, ValueGetPosition=None, ValueSetPosition=None, isStatic=None, InitializerId=None):
        super(FieldTypeSub, self).__init__(ID, Name, Namespace, isGlobal, Definition, AssignmentFrom, PointsToFunctions, ValueCallPosition, ValueGetPosition, ValueSetPosition, isStatic, InitializerId, )
supermod.FieldType.subclass = FieldTypeSub
# end class FieldTypeSub


class FunctionReferenceTypeSub(supermod.FunctionReferenceType):
    def __init__(self, ID=None, kind=None):
        super(FunctionReferenceTypeSub, self).__init__(ID, kind, )
supermod.FunctionReferenceType.subclass = FunctionReferenceTypeSub
# end class FunctionReferenceTypeSub


class FunctionTypeSub(supermod.FunctionType):
    def __init__(self, ID=None, Name=None, Namespace=None, Definition=None, Declaration=None, EntryStatement=None, extensiontype_=None):
        super(FunctionTypeSub, self).__init__(ID, Name, Namespace, Definition, Declaration, EntryStatement, extensiontype_, )
supermod.FunctionType.subclass = FunctionTypeSub
# end class FunctionTypeSub


class MethodTypeSub(supermod.MethodType):
    def __init__(self, ID=None, Name=None, Namespace=None, Definition=None, Declaration=None, EntryStatement=None, isOverrideBase=None):
        super(MethodTypeSub, self).__init__(ID, Name, Namespace, Definition, Declaration, EntryStatement, isOverrideBase, )
supermod.MethodType.subclass = MethodTypeSub
# end class MethodTypeSub


class StatementReferenceTypeSub(supermod.StatementReferenceType):
    def __init__(self, ID=None, kind=None, extensiontype_=None):
        super(StatementReferenceTypeSub, self).__init__(ID, kind, extensiontype_, )
supermod.StatementReferenceType.subclass = StatementReferenceTypeSub
# end class StatementReferenceTypeSub


class StatementTypeSub(supermod.StatementType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, extensiontype_=None):
        super(StatementTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, extensiontype_, )
supermod.StatementType.subclass = StatementTypeSub
# end class StatementTypeSub


class STBreakTypeSub(supermod.STBreakType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, breakingLoop=None):
        super(STBreakTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, breakingLoop, )
supermod.STBreakType.subclass = STBreakTypeSub
# end class STBreakTypeSub


class STContinueTypeSub(supermod.STContinueType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, continuingLoop=None):
        super(STContinueTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, continuingLoop, )
supermod.STContinueType.subclass = STContinueTypeSub
# end class STContinueTypeSub


class STDoAndWhileTypeSub(supermod.STDoAndWhileType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, condition=None, isCheckConditionBeforeFirstRun=None, body=None, else_=None):
        super(STDoAndWhileTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, condition, isCheckConditionBeforeFirstRun, body, else_, )
supermod.STDoAndWhileType.subclass = STDoAndWhileTypeSub
# end class STDoAndWhileTypeSub


class STForTypeSub(supermod.STForType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, start=None, condition=None, iteration=None, body=None, else_=None):
        super(STForTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, start, condition, iteration, body, else_, )
supermod.STForType.subclass = STForTypeSub
# end class STForTypeSub


class STForEachTypeSub(supermod.STForEachType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, head=None, body=None):
        super(STForEachTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, head, body, )
supermod.STForEachType.subclass = STForEachTypeSub
# end class STForEachTypeSub


class STGotoTypeSub(supermod.STGotoType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, destination=None):
        super(STGotoTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, destination, )
supermod.STGotoType.subclass = STGotoTypeSub
# end class STGotoTypeSub


class STIfTypeSub(supermod.STIfType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, condition=None, then=None, elseIf=None, else_=None):
        super(STIfTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, condition, then, elseIf, else_, )
supermod.STIfType.subclass = STIfTypeSub
# end class STIfTypeSub


class STOperationalTypeSub(supermod.STOperationalType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, operation=None):
        super(STOperationalTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, operation, )
supermod.STOperationalType.subclass = STOperationalTypeSub
# end class STOperationalTypeSub


class STReturnTypeSub(supermod.STReturnType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, ReturnOperation=None):
        super(STReturnTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, ReturnOperation, )
supermod.STReturnType.subclass = STReturnTypeSub
# end class STReturnTypeSub


class STSwitchTypeSub(supermod.STSwitchType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, head=None, case=None, default=None):
        super(STSwitchTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, head, case, default, )
supermod.STSwitchType.subclass = STSwitchTypeSub
# end class STSwitchTypeSub


class STThrowTypeSub(supermod.STThrowType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, exception=None):
        super(STThrowTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, exception, )
supermod.STThrowType.subclass = STThrowTypeSub
# end class STThrowTypeSub


class STTryCatchFinallyTypeSub(supermod.STTryCatchFinallyType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, tryBlock=None, catchBlock=None, elseBlock=None, finallyBlock=None, else_=None):
        super(STTryCatchFinallyTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, tryBlock, catchBlock, elseBlock, finallyBlock, else_, )
supermod.STTryCatchFinallyType.subclass = STTryCatchFinallyTypeSub
# end class STTryCatchFinallyTypeSub


class STUsingTypeSub(supermod.STUsingType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, head=None, body=None):
        super(STUsingTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, head, body, )
supermod.STUsingType.subclass = STUsingTypeSub
# end class STUsingTypeSub


class STYieldReturnTypeSub(supermod.STYieldReturnType):
    def __init__(self, ID=None, NextInLinearBlock=None, SensorBeforeTheStatement=None, FirstSymbolLocation=None, LastSymbolLocation=None, YieldReturnOperation=None):
        super(STYieldReturnTypeSub, self).__init__(ID, NextInLinearBlock, SensorBeforeTheStatement, FirstSymbolLocation, LastSymbolLocation, YieldReturnOperation, )
supermod.STYieldReturnType.subclass = STYieldReturnTypeSub
# end class STYieldReturnTypeSub


class OperationTypeSub(supermod.OperationType):
    def __init__(self, ID=None, callNode=None):
        super(OperationTypeSub, self).__init__(ID, callNode, )
supermod.OperationType.subclass = OperationTypeSub
# end class OperationTypeSub


class LocationLineSub(supermod.LocationLine):
    def __init__(self, line=None, column=None, file=None, function=None):
        super(LocationLineSub, self).__init__(line, column, file, function, )
supermod.LocationLine.subclass = LocationLineSub
# end class LocationLineSub


class LocationOffsetSub(supermod.LocationOffset):
    def __init__(self, offset=None, file=None, function=None):
        super(LocationOffsetSub, self).__init__(offset, file, function, )
supermod.LocationOffset.subclass = LocationOffsetSub
# end class LocationOffsetSub


class FileTypeSub(supermod.FileType):
    def __init__(self, fullname=None, ID=None):
        super(FileTypeSub, self).__init__(fullname, ID, )
supermod.FileType.subclass = FileTypeSub
# end class FileTypeSub


class FilesTypeSub(supermod.FilesType):
    def __init__(self, File=None):
        super(FilesTypeSub, self).__init__(File, )
supermod.FilesType.subclass = FilesTypeSub
# end class FilesTypeSub


class ClassesTypeSub(supermod.ClassesType):
    def __init__(self, Class=None):
        super(ClassesTypeSub, self).__init__(Class, )
supermod.ClassesType.subclass = ClassesTypeSub
# end class ClassesTypeSub


class VariablesTypeSub(supermod.VariablesType):
    def __init__(self, Variable=None, Field=None):
        super(VariablesTypeSub, self).__init__(Variable, Field, )
supermod.VariablesType.subclass = VariablesTypeSub
# end class VariablesTypeSub


class FunctionsTypeSub(supermod.FunctionsType):
    def __init__(self, Function=None, Method=None):
        super(FunctionsTypeSub, self).__init__(Function, Method, )
supermod.FunctionsType.subclass = FunctionsTypeSub
# end class FunctionsTypeSub


class StatementsTypeSub(supermod.StatementsType):
    def __init__(self, STBreak=None, STContinue=None, STDoAndWhile=None, STFor=None, STForEach=None, STGoto=None, STIf=None, STOperational=None, STReturn=None, STSwitch=None, STThrow=None, STTryCatchFinally=None, STUsing=None, STYieldReturn=None):
        super(StatementsTypeSub, self).__init__(STBreak, STContinue, STDoAndWhile, STFor, STForEach, STGoto, STIf, STOperational, STReturn, STSwitch, STThrow, STTryCatchFinally, STUsing, STYieldReturn, )
supermod.StatementsType.subclass = StatementsTypeSub
# end class StatementsTypeSub


class OperationsTypeSub(supermod.OperationsType):
    def __init__(self, Operation=None):
        super(OperationsTypeSub, self).__init__(Operation, )
supermod.OperationsType.subclass = OperationsTypeSub
# end class OperationsTypeSub


class DefinitionTypeSub(supermod.DefinitionType):
    def __init__(self, LocationLine=None, LocationOffset=None):
        super(DefinitionTypeSub, self).__init__(LocationLine, LocationOffset, )
supermod.DefinitionType.subclass = DefinitionTypeSub
# end class DefinitionTypeSub


class AssignmentFromTypeSub(supermod.AssignmentFromType):
    def __init__(self, VariableReference=None, LocationLine=None, LocationOffset=None):
        super(AssignmentFromTypeSub, self).__init__(VariableReference, LocationLine, LocationOffset, )
supermod.AssignmentFromType.subclass = AssignmentFromTypeSub
# end class AssignmentFromTypeSub


class PointsToFunctionsTypeSub(supermod.PointsToFunctionsType):
    def __init__(self, FunctionReference=None, LocationLine=None, LocationOffset=None):
        super(PointsToFunctionsTypeSub, self).__init__(FunctionReference, LocationLine, LocationOffset, )
supermod.PointsToFunctionsType.subclass = PointsToFunctionsTypeSub
# end class PointsToFunctionsTypeSub


class ValueCallPositionTypeSub(supermod.ValueCallPositionType):
    def __init__(self, FunctionReference=None, LocationLine=None, LocationOffset=None):
        super(ValueCallPositionTypeSub, self).__init__(FunctionReference, LocationLine, LocationOffset, )
supermod.ValueCallPositionType.subclass = ValueCallPositionTypeSub
# end class ValueCallPositionTypeSub


class ValueGetPositionTypeSub(supermod.ValueGetPositionType):
    def __init__(self, LocationLine=None, LocationOffset=None):
        super(ValueGetPositionTypeSub, self).__init__(LocationLine, LocationOffset, )
supermod.ValueGetPositionType.subclass = ValueGetPositionTypeSub
# end class ValueGetPositionTypeSub


class ValueSetPositionTypeSub(supermod.ValueSetPositionType):
    def __init__(self, LocationLine=None, LocationOffset=None):
        super(ValueSetPositionTypeSub, self).__init__(LocationLine, LocationOffset, )
supermod.ValueSetPositionType.subclass = ValueSetPositionTypeSub
# end class ValueSetPositionTypeSub


class DefinitionType1Sub(supermod.DefinitionType1):
    def __init__(self, LocationLine=None, LocationOffset=None):
        super(DefinitionType1Sub, self).__init__(LocationLine, LocationOffset, )
supermod.DefinitionType1.subclass = DefinitionType1Sub
# end class DefinitionType1Sub


class DeclarationTypeSub(supermod.DeclarationType):
    def __init__(self, LocationLine=None, LocationOffset=None):
        super(DeclarationTypeSub, self).__init__(LocationLine, LocationOffset, )
supermod.DeclarationType.subclass = DeclarationTypeSub
# end class DeclarationTypeSub


class FirstSymbolLocationTypeSub(supermod.FirstSymbolLocationType):
    def __init__(self, LocationLine=None, LocationOffset=None):
        super(FirstSymbolLocationTypeSub, self).__init__(LocationLine, LocationOffset, )
supermod.FirstSymbolLocationType.subclass = FirstSymbolLocationTypeSub
# end class FirstSymbolLocationTypeSub


class LastSymbolLocationTypeSub(supermod.LastSymbolLocationType):
    def __init__(self, LocationLine=None, LocationOffset=None):
        super(LastSymbolLocationTypeSub, self).__init__(LocationLine, LocationOffset, )
supermod.LastSymbolLocationType.subclass = LastSymbolLocationTypeSub
# end class LastSymbolLocationTypeSub


class elseTypeSub(supermod.elseType):
    def __init__(self, ID=None, kind=None, isOnBreak=None):
        super(elseTypeSub, self).__init__(ID, kind, isOnBreak, )
supermod.elseType.subclass = elseTypeSub
# end class elseTypeSub


class elseType2Sub(supermod.elseType2):
    def __init__(self, ID=None, kind=None, isOnBreak=None):
        super(elseType2Sub, self).__init__(ID, kind, isOnBreak, )
supermod.elseType2.subclass = elseType2Sub
# end class elseType2Sub


class elseIfTypeSub(supermod.elseIfType):
    def __init__(self, condition=None, then=None):
        super(elseIfTypeSub, self).__init__(condition, then, )
supermod.elseIfType.subclass = elseIfTypeSub
# end class elseIfTypeSub


class caseTypeSub(supermod.caseType):
    def __init__(self, condition=None, body=None):
        super(caseTypeSub, self).__init__(condition, body, )
supermod.caseType.subclass = caseTypeSub
# end class caseTypeSub


class defaultTypeSub(supermod.defaultType):
    def __init__(self, block=None):
        super(defaultTypeSub, self).__init__(block, )
supermod.defaultType.subclass = defaultTypeSub
# end class defaultTypeSub


class callNodeTypeSub(supermod.callNodeType):
    def __init__(self, callFunction=None, callVariable=None):
        super(callNodeTypeSub, self).__init__(callFunction, callVariable, )
supermod.callNodeType.subclass = callNodeTypeSub
# end class callNodeTypeSub


def get_root_tag(node):
    tag = supermod.Tag_pattern_.match(node.tag).groups()[-1]
    rootClass = None
    rootClass = supermod.GDSClassesMapping.get(tag)
    if rootClass is None and hasattr(supermod, tag):
        rootClass = getattr(supermod, tag)
    return tag, rootClass


def parse(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'Database'
        rootClass = supermod.Database
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    doc = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='',
            pretty_print=True)
    return rootObj


def parseEtree(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'Database'
        rootClass = supermod.Database
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    doc = None
    mapping = {}
    rootElement = rootObj.to_etree(None, name_=rootTag, mapping_=mapping)
    reverse_mapping = rootObj.gds_reverse_node_mapping(mapping)
    if not silence:
        content = etree_.tostring(
            rootElement, pretty_print=True,
            xml_declaration=True, encoding="utf-8")
        sys.stdout.write(content)
        sys.stdout.write('\n')
    return rootObj, rootElement, mapping, reverse_mapping


def parseString(inString, silence=False):
    from StringIO import StringIO
    parser = None
    doc = parsexml_(StringIO(inString), parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'Database'
        rootClass = supermod.Database
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    doc = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='')
    return rootObj


def parseLiteral(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'Database'
        rootClass = supermod.Database
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    doc = None
    if not silence:
        sys.stdout.write('#from ??? import *\n\n')
        sys.stdout.write('import ??? as model_\n\n')
        sys.stdout.write('rootObj = model_.rootClass(\n')
        rootObj.exportLiteral(sys.stdout, 0, name_=rootTag)
        sys.stdout.write(')\n')
    return rootObj


USAGE_TEXT = """
Usage: python ???.py <infilename>
"""


def usage():
    print(USAGE_TEXT)
    sys.exit(1)


def main():
    args = sys.argv[1:]
    if len(args) != 1:
        usage()
    infilename = args[0]
    parse(infilename)


if __name__ == '__main__':
    #import pdb; pdb.set_trace()
    main()
