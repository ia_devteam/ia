#coding:cp1251
import os
import sys

import sensors
#from dtbase import VariableType, FunctionType

class Singleton(object):
 #_instance = None
 def __new__(cls, *args, **kwargs):
  if not cls._instance:
   cls._instance = super(GlblsObj, cls).__new__(cls, *args, **kwargs)
  return cls._instance

#-------basic classes-----

#�����, �������� ������ ���� ������������ ���������
#�������� ������ � ������� ���������
class GlblsObj(object):
#Init while first using
 def __init_plus__(self, flnms=None, startsens=None, clpth=None, labpath=None, wrkdir=None, ndvlevel=None, template = None, ui = None):
  #arg_params:
  #1 - file with filenames
  #2 - start sensor number
  #3 - clear source path
  #4 - lab source path
  #5 - workdir for xml and logs
  #6 - ndv-level
  #7 - module
  #8 - sensor template string
  if flnms != None:
   self.file_with_names = flnms
   self.start_sens_number = startsens
   self.clear_path = clpth
   self.lab_path = labpath
   self.working_dir = wrkdir
   self.ndv_level = ndvlevel
   self.uid = ui
   self.sensor_template = str(template).replace("%SID%", ui)
   #----init---------------------
   self.last_used_sens = startsens
  else:
   self.file_with_names = ""
   self.start_sens_number = 0
   self.clear_path = ""
   self.lab_path = ""
   self.working_dir = ""
   self.ndv_level = 2
   self.uid = ""
   self.last_used_sens = 0
   self.sens_stor = sensors.sensor_storage()

  self.sens_stor = sensors.sensor_storage()
  self.start_filenames = []


#�������� �������� ��������� �������:>>>>>>>>
  self.current_file = 0
  self.current_module = ""
  self.current_imports = []

  #������� ��� ���������
  self.func_names_to_ids = {}      #name: [id, id, id, ...]
  self.var_names_to_ids = {}
  self.nmspc_names_to_ids = {}
  self.cl_ids = {}
  self.places = {}

  self.f_ids = {}    #id: name (��������� � ����� ������)
  self.f_knds = {}   #id: kind
  self.f_paths = {}
  self.f_decs = {}

  self.var_ids = {}
  self.var_knds = {}
  self.var_paths = {}
  self.var_glbl = {}

  self.obj_modules = {}

  self.used_sens = []
  self.free_sens = []

  self.used_ids = []
  self.last_used_id = 0
  self.free_ids = []
  #������� ��� ���������

  #����� ���������, ��� ��� ����������, �� �������
  #���� ������, �������� � ����???????
  self.stnodes = []

#  self.prepare_sens_array()
#�������� �������� ��������� �������-----<<<<<<<<<<<<

 def prepare_sens_array(self):
  if (self.last_used_sens  !=0):
   i = 0
   while(i <= self.last_used_sens):
    self.used_sens.append(1)
    i+=1

 def get_start_filenames(self):
  with open(glbl.file_with_names, "r") as source_code:
   files = source_code.readlines()
   for fl in files:
    self.start_filenames.append(fl)

 def add_place(self, plc, id):
  self.places[id] = plc

#nums - ������ ��������������, free - ��������� ����� ��������������
 def ret_free_num(self, nums, free, last, gl):
  num = -1
  if len(free)>0:
   num = free.pop[len-1]
   nums[num] = 1
  else:
   num = len(nums)
   nums.append(1)
  setattr(gl, last, num)
  #last = num
  return num

 def del_num(self, num, nums, free):
  if(len(nums) > num):
   nums[num] = 0
   free.append(num)

 def refresh_dics(self, label, obj):
#-------------�������-------------------
  if (label == 0):
   glbl.f_ids[obj.id]=obj.name
   if (obj.__class__.__name__ ==  'FunctionType'):
    glbl.f_knds[obj.id]=0
   else:
    glbl.f_knds[obj.id]=1
   if not(glbl.func_names_to_ids.has_key(obj.name)):
    glbl.func_names_to_ids[obj.name] = []
   glbl.func_names_to_ids.get(obj.name).append(obj.id)
   glbl.obj_modules[obj.id] = glbl.current_module
   glbl.f_paths[obj.id] = obj.def_path
   glbl.f_decs[obj.id] = obj.decorators
#------------����������------------------
  elif(label == 1):
   glbl.var_ids[obj.id]=obj.name
   if (obj.__class__.__name__ == 'VariableType'):
    glbl.var_knds[obj.id]=0
   else:
    glbl.var_knds[obj.id]=1
   if not(glbl.var_names_to_ids.has_key(obj.name)):
    glbl.var_names_to_ids[obj.name] = []
   glbl.var_names_to_ids.get(obj.name).append(obj.id)
   glbl.obj_modules[obj.id] = glbl.current_module
   glbl.var_paths[obj.id] = obj.def_paths
#-----------------------------------------
  elif(label == 2):
   glbl.cl_ids[obj.id]=obj.name
  elif(label == 3):
   glbl.st_ids[obj.id]=obj.name
  elif(label == 4):
   glbl.op_ids[obj.id]=obj.name

class Glbls(Singleton, GlblsObj):
 _instance = None

class Tree_stek:
 def __init__(self):
  self.numlayer = 0
  self.lrs = []

 def add_layer(trlr):
  self.lrs.append(trlr)
  self.numlayer = numlayer + 1

 def del_layer():
  self.numlayer = self.numlayer - 1
  return self.lrs.pop()

class Treelayer:
  def __init__(self, cur = -1):
   self.cur_lyr = cur_lyr
   self.var_ids = {}
   self.fnc_ids = {}

#-------other classes-----

class PlcInText:
#place has params: file id, line, column, [optional: id, offset, name]
#service param id - from what object this place came (for futher processing)
#filename is usually empty
 def __init__(self, f = None, l = None, c = None, i = None,  o = None, n = None):
  if (f == None):
   self.fileid = 0
   self.line = -1
   self.column = -1
   self.offset = 0
   self.id = -1
   self.filename = n
  else:
   self.fileid = f
   self.line = l
   self.column = c
   if (i != None):
    self.id = i
   if (o != None):
    self.offset = o
   if (n != None):
    self.filename = n          #���� �� ���������
  self.verify_place()

 def init_by_node(self, node, objid):
  self.fileid = glbl.current_file
  self.line = node.absolute_bounding_box.top_left.line
  self.column = node.absolute_bounding_box.top_left.column
  self.verify_place()
  self.id = objid

 def verify_place(self):
  if (self.column == 0):
    self.column = 1
  if (self.line == 0):
    self.line = 1

 def init_end_by_node(self, node, objid):
  self.fileid = glbl.current_file
  self.line = node.absolute_bounding_box.bottom_right.line
  self.column = node.absolute_bounding_box.bottom_right.column
  self.verify_place()
  self.id = objid

 def set_offset(self, o):
  self.offset = o

class PythonStandartNames:
 Decorators = {'classmethod':0,
 'staticmethod':1,
 'property':2,
 'lru_cache':3,
 'total_ordering':4,
 'partialmethod':5,
 'abstractmethod':6,
 'reduce':7,
 'singledispatch':8,
 'wraps':9,

 }

class FuncsInNmspcs:
 def __init__(self):
  self.classes = []
  self.funcs = [[]]

class Empty(object):
 pass

'''
class SynteticNode():
 pass
'''

#-------other classes-----<<<<<<<


#code block
glbl = Glbls()