#coding:cp1251
#>>>>>>>>>>>>>>>???????? ?? ?????>>>>>>>>>>>>>>>>>>>
#???? ??????????? ?????? ??? nod-?, ?? ??? ?? ?????????? ???


NotStTypeNodes = {
'CommentNode'   :0,
'SpaceNode'     :1,
'ReprNode'      :2,
'FromImportNode':3,
'ClassNode'     :4,
'DefNode'       :5,
'DecoratorNode' :6,
'ImportNode'    :7,
'EndlNode'      :8
}

LoopSttmnts = {
'while':2,
'for':4
}

StKns = [
['BreakNode', 'break'],           #0
['ContinueNode', 'continue'],           #1
['WhileNode', 'while'],           #2
['Empty'],           #3----------
['ForNode','for'],           #4
['Empty'],           #5----------
['ComprehensionIfNode', 'IfNode', 'TernaryOperatorNode', 'IfelseblockNode',
 'if', 'comprehensionif', 'ifelseblock', 'ternaryoperator'],           #6
['Operational'],           #7-Other Nodes
['ReturnNode', 'return'],           #8
['Empty'],           #9----------
['RaiseNode', 'raise'],           #10
['TryNode', 'try'],           #11
['WithNode', 'WithContextItemNode', 'with', 'withcontextitem'],           #12
['YieldNode','YieldAtomNode', 'yield', 'yield_atom']            #13
]

def ret_sttmnt_kind(node):
 #t = str(type(node))
 #t = t.replace("redbaron.nodes.","")
 t = str(node.type)
 for i in range(0, 14):
  if t in StKns[i]:
   return i
 return 7


