#coding:cp1251
#base modules
import os, sys, re, traceback
#additional modules
from redbaron import RedBaron
import baron.parser, redbaron.nodes
#from easygui import msgbox
#custom modules
import glbls, fsfuncs, m1, logs

sys.setrecursionlimit(65565)

if __name__ == '__main__':
 try:
  glbl = glbls.Glbls()
#1 - file with filenames
#2 - start sensor number
#3 - clear source path
#4 - lab source path
#5 - workdir for xml and logs
#6 - ndv-level
#7 - uid

#params initializing and verify
#�������� ���������� ����� ����>>>>>>>>>>>>>>>>>
  if (len(sys.argv) == 2):
   fsfuncs.load_params_from_file(glbl, sys.argv[1])
  else:
   print("Wrong Args Error\nInvalid number of parameters.\n App will be closed\n")
   #logs.log_file(9, "Wrong Args Error\nInvalid number of parameters.\n App will be closed\n")\
   #msgbox("Invalid number of parameters.\n App will be closed", "Wrong Args Error")
   exit(10)
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

  fsfuncs.check_path_args()
#params initializing and verify

#preparing
  glbl.prepare_sens_array()
  lognodes = [glbl.working_dir + "\\" + "errors.txt", glbl.working_dir + "\\" + "inserted_sensors.txt",
  glbl.working_dir + "\\" + "not_implemented_nodes.txt", glbl.working_dir + "\\" + "imp_nodes.txt",
  glbl.working_dir + "\\" + "parsed_files.txt"]
  for filenames in lognodes:
   if (os.path.isfile(filenames)):
    os.remove(filenames)
#preparing
 except(Exception) as e:
  print(traceback.format_exc(e))

 try:
  m1.start(glbl)
 except(Exception) as e:
  with open(glbl.working_dir + "\\" + "errors.txt", "a+") as filename:
   filename.write(traceback.format_exc(e))
   filename.close()

