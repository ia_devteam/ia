#coding:cp1251
import os

import glbls

class Singleton(object):
 #_instance = None
 def __new__(cls, *args, **kwargs):
  if not cls._instance:
   cls._instance = super(DataBaseObj, cls).__new__(cls, *args, **kwargs)
  return cls._instance

class DataBaseObj(object):
 def __init__(self):
  self.classes = []
  self.variables = []
  self.functions = []
  self.statements = []
  self.operations = []
  self.files = []
  #additional
  self.namespaces = []

class DataBase(Singleton,DataBaseObj):
 _instance = None

class NamespaceType:
 def __init__(self, i = None, n = None):
  if i == None:
   self.id = -1
   self.name = ""
  else:
   self.id = i
   self.name = n
  self.related_nmspcs_names = []

class FileType:
 def __init__(self, i = None, n = None):
  if (i == None):
   self.id = -1
   self.fullname = ""
  else:
   self.id = i
   self.fullname = n.strip()

class ClassType:
 def __init__(self, iid = None, name = None, nmspc = None):
  if iid==None:
   self.id = -1
   self.name = ""
   self.namespace = ""      #���������, ����� ��������
  else:
   self.id = iid
   self.name = name
   self.namespace = nmspc      #���������, ����� ��������
  self.parentclassid = []  #ClassType
  self.fieldid = []        #FieldType
  self.methodid = []       #MethodType
  self.nparents = []       #NamesOfClassParents

class OperationType:
 def __init__(self, i = None, clnd = None):
  if (i == None):
   self.id = -1
   self.callnode = []       #CallNodeType
  else:
   self.id = i
   self.callnode = clnd

class VariableType:
 def __init__(self):
  self.definition = glbls.PlcInText()
  self.assignmentfrom = []             #������ �������� ���� AssignmentFrom
  self.pointstofunctions = []          #������ �������� ���� PointsToFunctions
  self.valuecallposition = []          #������ �������� ���� PointsToFunctions
  self.valuegetposition = []           #������ �������� ���� glbls.PlcInText()
  self.valuesetposition = []           #������ �������� ���� glbls.PlcInText()
  self.id = -1
  self.name = ""
  self.namespace = ""
  self.kind = 0
  self.isglobal = True
  self.plc_uses = []
  self.def_paths = []

 def init_by_node(self, node):
  if not hasattr(node, 'id'):
   i = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)
   self.id = i
   node.id = i
  else:
   self.id = node.id
  self.name = node.name
  self.namespace = glbl.current_module

 def init_by_classnode(self, node, clnode, stat):
  self.init_from_node(node)
  self.isglobal = false
  pl = glbls.PlcInText()
  if (self.__class__.__name__== "VariableType"):
   node.kind = 0
  else:
   node.kind = 1
  self.definition = pl.init_by_node(node)

 def add_place(self, nm):
  set_id_to_dtbase_obj(nm)
  plc = glbls.PlcInText()
  plc.init_by_node(nm, nm.id)
  if (nm.on_attribute == 'target'):
   self.valuesetposition.append(plc)
  else:
   self.valuegetposition.append(plc)
  self.plc_uses.append(plc)
  self.def_paths.append(nm.path().to_baron_path())
  nm.namekind = 'var'
  nm.id = self.id
  nm.processed = True

class FieldType(VariableType):
 def __init__(self):
  VariableType.__init__(self)
  self.isstatic = False
  self.kind = 1
  self.initializerid = -1             #������ �� ��������, ��� ���������� �������������

class FunctionType:
 def __init__(self, defin = None, stt=None, idd = None, nm = None, nmspc = None, decs = None, path = None):
  if defin == None:
   self.definition = glbls.PlcInText()
   #self.declaration = None
   self.entrystatement = None #StatementReferenceType()
   self.id = -1
   self.name = ""
   self.namespace = ""
  else:
   self.definition = defin
   self.entrystatement = stt
   self.id = idd
   self.name = nm
   self.namespace = nmspc
  if (decs != None):
   self.decorators = decs
  else:
   self.decorators = []    #Names
  self.plc_uses = []
  self.def_path = path

class MethodType(FunctionType):
 def __init__(self, defin = None, stt=None, idd = None, nm = None, nmspc = None, decs = None):
  if defin == None:
   FunctionType.__init__(self)
  else:
   FunctionType.__init__(self, defin, stt, idd, nm, nmspc, decs)
  self.isoverridebase = False        #�� �����������, ������� ������ ����� false

class StatementType:
 def __init__(self):
  self.nextinlinearblock = None #StatementReferenceType()
  self.sensorbeforethestatement = -1
  self.firstsymbollocation = None #glbls.PlcInText()
  self.lastsymbollocation = None #glbls.PlcInText()
  self.id = -1

class STBreakType(StatementType):
 def __init__(self):
  StatementType.__init__(self)
  self.breakingloop = StatementReferenceType()

class STContinueType(StatementType):
 def __init__(self):
  StatementType.__init__(self)
  self.continuingloop = StatementReferenceType()

class STForEachType(StatementType):
 def __init__(self):
  StatementType.__init__(self)
  self.body = StatementReferenceType()
  self.head = -1            #������ �� �������� � �������

class STDoAndWhileType(StatementType):
 def __init__(self):
  StatementType.__init__(self)
  self.body = StatementReferenceType()
  self.isCheckConditionBeforeFirstRun = 1         #������
  self.els = None #ElsType()
  self.condition = -1       #������ �� �������� � �������

class STIfType(StatementType):
 def __init__(self):
  StatementType.__init__(self)
  self.then = StatementReferenceType()
  self.elseif = []          #elseIf()
  self.els = None #StatementReferenceType()
  self.condition = -1       #������ �� �������� � �������

class STOperationalType(StatementType):
 def __init__(self):
  StatementType.__init__(self)
  self.operationid = -1

class STReturnType(StatementType):
 def __init__(self):
  StatementType.__init__(self)
  self.returnoperation = -1  #������ �� ��������

class STThrowType(StatementType):
 def __init__(self):
  StatementType.__init__(self)
  self.exception = -1        #������ �� �������� � ��������� raise

class STTryCatchFinallyType(StatementType):
 def __init__(self):
  StatementType.__init__(self)
  self.tr = StatementReferenceType()
  self.ctch = []                          #StatementReferenceType
  self.els = ElsType()
  self.finall = StatementReferenceType()

#python with
class STUsingType(StatementType):
 def __init__(self):
  StatementType.__init__(self)
  self.body = StatementReferenceType()
  self.head = -1                         #������ �� operation

class STYieldReturnType(StatementType):
 def __init__(self):
  StatementType.__init__(self)
  self.yieldreturnoperation = -1

#Service Function and Statement----------------------------
class CallNodeType:
 def __init__(self, f = None, v = None):
  if f == None:
   self.callfunction = []           #FunctionReferenceType
   self.callvariable = []           #VariableReferenceType
  else:
   self.callfunction = f           #FunctionReferenceType
   self.callvariable = v           #VariableReferenceType

class StatementReferenceType:
 def __init__(self, i = None, k = None):
  if i == None:
   self.id = -1
   self.kind = ""
  else:
   self.id = i
   self.kind = EnumeratedObjects.StatementKindType.get(k)

 @staticmethod
 def init_by_node(node):
  if (node != None):
   set_id_to_dtbase_obj(node)
   self = StatementReferenceType(node.id, node.stkind)
  else:
   self = None
  return self

class ElsType(StatementReferenceType):
 def __init__(self, i = None, k = None, isonbreak = None):
  StatementReferenceType.__init__(self)
  self.isonbreak = "OnlyOnBreak"
  if i != None:
   self.id = i
   self.kind = EnumeratedObjects.StatementKindType.get(k)
  if isonbreak != None:
   self.isonbreak = isonbreak

class elseIf:
 def __init__(self):
  self.then = StatementReferenceType()
  self.condition = -1              #������ �� operation
#Service Function----------------------------

#Service Variable----------------------------
class AssignmentFrom:
 def __init__(self, v = None, l = None):
  if v != None:
   self.variablereference = v
   self.location = l
  else:
   self.variablereference = VariableReferenceType()
   self.location = glbls.PlcInText()

class PointsToFunctions:
 def __init__(self, f = None, l = None):
  if f != None:
   self.functionreference = f
   self.location = l
  else:
   self.functionreference = FunctionReferenceType()
   self.location = glbls.PlcInText()

class FunctionReferenceType:
 def __init__(self, i = None, k = None):
  if i == None:
   self.id = -1
   self.kind = ""
  else:
   self.id = i
   self.kind = EnumeratedObjects.FunctionKindType[k]

class VariableReferenceType:
 def __init__(self, i = None, k = None):
  if i == None:
   self.id = -1
   self.kind = ""
  else:
   self.id = i
   self.kind = EnumeratedObjects.VariableKindType[k]
#Service Variable----------------------------

class EnumeratedObjects:
  VariableKindType = {0:'variable', 1:'field'}
  FunctionKindType = {0:'function',
   1:'method'}
  StatementKindType =  {0:'STBreak',
    1:'STContinue',
    2:'STDoAndWhile',
    3:'STFor',       #notused
    4:'STForEach',
    5:'STGoto',      #notused
    6:'STIf',
    7:'STOperational',
    8:'STReturn',
    9:'STSwitch',    #notused
   10:'STThrow',
   11:'STTryCatchFinally',
   12:'STUsing',     #notused_or_nosensor
   13:'STYieldReturn'}
  elseKindType = {0:'OnlyOnBreak', 1: 'OnlyNotOnBreak', 2:'Everytime'}

#common functions--------------------
def add_files_to_database():
 for fl in glbl.start_filenames:
  fll = fl.strip()
  f = FileType(glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl), fll)
  database.files.append(f)
 for nm in glbl.start_filenames:
  nmspc = NamespaceType(
     glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl),
        os.path.splitext(os.path.basename(nm))[0])
  database.namespaces.append(nmspc)
  glbl.nmspc_names_to_ids[nmspc.name] = nmspc.id

def set_id_to_dtbase_obj(obj):
 if(hasattr(obj, 'id')):
  if (obj.id == -1):
   obj.id = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)
   return
 else:
  obj.id = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)

def was_id_in_obj(obj, attr, i):
 try:
  if (getattr(obj, attr).index (i) >= 0):
   return False
 except:
  return True
#common functions--------------------

#code block
glbl = glbls.Glbls()
database = DataBase()