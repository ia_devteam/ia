#coding:cp1251
#base modules
import os, sys, re, traceback
#additional modules
from redbaron import RedBaron
import baron.parser, redbaron.nodes
#from easygui import msgbox

#custom modules
import glbls, fsfuncs
import logs, processnode, nodemethods
import dtbase
from dtconvert import convert
from dtbase import was_id_in_obj

def export_red_to_file(red, flnm):
 if os.path.isfile(flnm):
  os.remove(flnm)
 with open(flnm, "w") as source_code:
  source_code.writelines(red.dumps())

def export_file_to_temp(fil, flnm):
 if os.path.isfile(flnm):
  os.remove(flnm)
 if not(os.path.isdir(os.path.dirname(flnm))):
    os.makedirs(os.path.dirname(flnm))
 with open(flnm, "w") as temp:
  with open(fil, "r") as source:
   lines = source.readlines()
   for line in lines:
    if (line.strip().startswith('#')) and not("coding:" in line.replace(" ","")):
     temp.write("\n")
    else:
     temp.write(line)


def collect_py_files(curdir):
 files = fsfuncs.get_files_from_dir(curdir)
 pyfiles = []
 for item in files:
  result = re.search(r'.py$', item)
  if(result!=None):
   pyfiles.append(item)
 return pyfiles

def parse_files_one_by_one(glbl, database):
 counter = 0
 for filename in glbl.start_filenames:
  logs.log_file(0, filename, 0)
  glbl.current_module = os.path.splitext(os.path.basename(filename))[0]
  glbl.current_file = counter
  parse_pyfile(filename, glbl, database)
  counter+=1

def parse_pyfile(fil, glbl, database):
  try:
   fil = fil.strip().lower()
   tempfile = fil.replace(glbl.clear_path.lower(), glbl.lab_path.lower())
   tempfile = tempfile.replace(".py", "_temp.py")
   export_file_to_temp(fil, tempfile)
   fil = tempfile
   with open(fil, "r") as source_code:
    red = RedBaron(source_code.read())
  except baron.parser.ParsingError as e:
   logs.log_file(9, "Internal Parse Error\n!!!Parsing file has syntax error.\n You can find filename last in 'parsedfiles.txt'\n")
   print(traceback.format_exc(e))
   exit(20)

 #RedBaron ����� �������� �����, ������� ��������� ��� "�������" �����
 # tempfile = fil.replace(glbl.clear_path, glbl.lab_path)
 # glbl.tempfile = tempfile.replace(".py", "_temp.py")
 # export_to_file(red,glbl.tempfile)

  glbl.tempfile = fil.replace(glbl.clear_path, glbl.lab_path)

 #try:
#��������� ������: �� ������ ���� ����������� ������� �������,
#�������� ������ �� ���������, �� ��������� ����������� ���������, ...
#----------------------------------------------
  '''
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  r = red.find_all("ComprehensionLoopNodeExec")
  for node in r.data:
   pass
  '''
#----------------------------------------------
  r = red.find_all("ClassNode")
  for node in r:
   vrs = {}
   node.id = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)
   cls = dtbase.ClassType(node.id, node.name, glbl.current_module)
   for st in node.inherit_from.data:
    cls.nparents.append(str(st))
   cl = node
   for n in cl.data:
    if type(n) == redbaron.nodes.AssignmentNode:
     if type(n.target) == redbaron.nodes.NameNode:
      name = nodemethods.NameNodeExec(n.target, database, True)
      n.kind = 1
      if (n.target.namekind == 'var'):
       n.target.processed = True
       names = cl.find_all("NameNode", value = n.target.value)
       for nm in names:
        if not(hasattr(nm, 'processed')):
         name.add_place(nm)
        if (not(hasattr(nm, "id"))):
         name.id = name.id
        nm.kind = 1
       index = nodemethods.ret_shortest_path(name.def_paths)
       name.definition = name.plc_uses[index]
       database.variables.append(name)
       if was_id_in_obj(cls, 'fieldid', name.id):
        cls.fieldid.append(name.id)
       vrs[n.target.value] = name.id
      n.processed = True
    if type(n) == redbaron.nodes.DefNode:
     if not hasattr(n,'id'):
      fnc = nodemethods.DefNodeExec(n, database, True)
      fnc.kind = dtbase.EnumeratedObjects.FunctionKindType.get('1')
      cls.methodid.append(fnc.id)
      n.processed = True
      glbl.refresh_dics(0, fnc)
     if was_id_in_obj(cls, 'methodid', name.id):
      cls.methodid.append(n.id)

   slf = cl.find_all("AtomtrailersNode")
   for sl in slf:
    if hasattr(sl.value[0],"value"):
     if (sl.value[0].value == "self"):
      if (not (nodemethods.is_name_called(sl.value[1])) and (not hasattr(sl.value[1], "processed"))):
       name = nodemethods.NameNodeExec(sl.value[1], database, True)
       if (sl.value[1].namekind == 'var'):
        names = cl.find_all("NameNode", value = sl.value[1].value)
        for nm in names:
         if not(hasattr(nm, 'processed')):
          name.add_place(nm)
         if (not(hasattr(nm, "id"))):
          name.id = name.id
         nm.kind = name.kind
        index = nodemethods.ret_shortest_path(name.def_paths)
        name.definition = name.plc_uses[index]
        database.variables.append(name)
        sl.value[1].processed = True
        if was_id_in_obj(cls, 'fieldid', name.id): cls.fieldid.append(name.id)
       if hasattr(name, "id"):
        if was_id_in_obj(cls, 'fieldid', name.id): cls.fieldid.append(name.id)
   database.classes.append(cls)
#----------------------------------------------
  r = red.find_all("DefNode")
  for n in r:
   if hasattr(n,'processed'):
    continue   #���� ��������� ��� �������
   else:
    fnc = nodemethods.DefNodeExec(n, database)
  '''
#----------------------------------------------
  r = red.find_all("GlobalNode")
  for n in r:
   for name in n.value.data[0]:
    nodemethods.set_id_to_obj(name)
    glbl.var_glbl[name.id] = 1
   n.processed = True
#----------------------------------------------
  '''
  r = red.find_all("NameNode")
  for n in r:
   if hasattr(n,'processed'):
    continue   #���� ��������� �����
   else:
    name = nodemethods.NameNodeExec(n, database)
    if (n.namekind == 'var'):
     names = r.find_all("NameNode", value = n.value)
     for nm in names:
        if not(hasattr(nm, 'processed')):
         if (nodemethods.compare_nodes(nm, n)):
          name.add_place(nm)
         if (not(hasattr(nm, "id"))):
          name.id = n.id
         nm.kind = n.kind
     index = nodemethods.ret_shortest_path(name.def_paths)
     name.definition = name.plc_uses[index]
     database.variables.append(name)

#������ ������������ Nod-�, ������� ��� �� ���� ����������
  r = red.find_all("re:^")
  for node in r:
   processnode.process_node(node, database)

#�� ������ ������ ������ ��� ��� ��� Node-�,
#�� ������� �������� ������
  for node in glbl.stnodes:
   processnode.process_node(node, database, True)

  '''
 #�������� ����� ����� �����, ��� �����!!!!!!!!!!!!!!!!
 except AttributeError:
  msgbox(
    "Analizing file has error.\n You can find filename last in 'parsedfiles.txt'",
    "File Has \"Bad\" Node")
  exit(21)
  '''

  processnode.add_statement_at_the_start_of_file(red)
  processnode.add_statement_to_the_end_of_file(red)

  flnm = glbl.tempfile.replace("_temp.py", ".py")

  #flnm = fil.replace(glbl.clear_path, glbl.lab_path)
  #glbl.sens_stor.inserting_new(glbl.tempfile, flnm, glbl.ndv_level, glbl.sensor_template, glbl.uid, glbl.lastsens)
  #os.remove(glbl.tempfile)

  #glbl.sens_stor.inserting_new(fil, glbl.tempfile, glbl.ndv_level, glbl.sensor_template, glbl.uid, glbl.lastsens)
  glbl.sens_stor.inserting_new(fil, flnm, glbl.ndv_level, glbl.sensor_template, glbl.uid, glbl.lastsens)
  os.remove(fil)
  #glbl.sens_stor.inserting(glbl.ndv_level, glbl.sensor_template, glbl.uid)
  #processnode.add_sens_import_to_the_start_of_file(red)

  #export_to_file(red, flnm)
  glbl.sens_stor.flush()
  glbl.current_file+=1
  print str(os.path.basename(flnm)) + ": the end"
#<<<<<<<<<<<<<<<parse_pyfile<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

def start(glbl):
 glbl.get_start_filenames()
 database = dtbase.DataBase()
 dtbase.add_files_to_database()
 parse_files_one_by_one(glbl, database)

 #����������� � ����� ����������� ����>>>>>
 dtbsconv = convert(database)
 filename = os.path.join(glbl.working_dir, str(glbl.uid) + "_final.xml")
 with open(filename, 'w') as f:
  dtbsconv.export(f, 0)
 #����������� � ����� ����������� ����<<<<<


#--------Dedugging----------------------


#--------Dedugging----------------------
if __name__ == '__main__':
 glbl = glbls.Glbls()

#1 - file with filenames
#2 - start sensor number
#3 - clear source path
#4 - lab source path
#5 - workdir for xml and logs
#6 - ndv-level

#testtesttesttest
 sys.argv.append("Z:\\_FT\\projs\\py_scripterpy\\testdir\\filenames.txt")
 sys.argv.append("100")
 sys.argv.append("Z:\\_FT\\projs\\py_scripterpy\\testdir\\Clear")
 sys.argv.append("Z:\\_FT\\projs\\py_scripterpy\\testdir\\Lab")
 sys.argv.append("Z:\\_FT\\projs\\py_scripterpy\\testdir\\output")
 sys.argv.append("2")
 sys.argv.append("debug")
 filename = "Z:\\_FT\\projs\\py_scripterpy\\testdir\\ast.py"
#testtesttesttest

#params initializing and verify
 if (len(sys.argv) == 8):
  glbls.Glbls.__init_plus__(glbl, sys.argv[1], int(sys.argv[2]), sys.argv[3], sys.argv[4], sys.argv[5], int(sys.argv[6]), sys.argv[7])
 else:
  logs.log_file(9, "Wrong Args Error\nInvalid number of parameters.\n App will be closed\n")
  #msgbox("Invalid number of parameters.\n App will be closed", "Wrong Args Error")
  exit(10)
 fsfuncs.check_path_args()
#params initializing and verify

#preparing
 glbl.prepare_sens_array()
 lognodes = [glbl.working_dir + "\\" + "nodes.txt", glbl.working_dir + "\\" + "inserted_sensors.txt"]
 for filenames in lognodes:
  if (os.path.isfile(filenames)):
   os.remove(filenames)
#preparing

 start(glbl)

