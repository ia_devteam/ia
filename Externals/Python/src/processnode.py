#coding:cp1251
import logs
import redbaron
from redbaron import RedBaron
#custom classes
import nodemethods, nodenames, glbls, dtbase
import sensors

NodeTypes = {}

#>>>>>>>>>>>>>>>>>>>Process_Node>>>>>>>>>>>>>>>>>>>>>>>
#>>>>>>>>>>>������ ������ ������ node-��>>>>>>>>>>>>>>>>
def process_node(node, database, second_round = None):
  if (second_round != None):
   if not hasattr(node, 'processed'):
     nodemethods.UniversalNodeExec(node, database)
   elif not hasattr(node, "hasstat"):
     nodemethods.UniversalNodeExec(node, database)
   elif node.hasstat == False:
     nodemethods.UniversalNodeExec(node, database)
   return
  if hasattr(node, 'processed') and (second_round == None):
   return
  #info = str(node.__class__.__name__) + "\t" + str(node.absolute_bounding_box.top_left.line) + "\t" +  str(node.absolute_bounding_box.top_left.column) +"\n"
  #logs.log_file(7, info)
  logs.log_file(1,(str(type(node)) + "\n"), 0)
  funcname = node.__class__.__name__ + "Exec"
  if hasattr(nodemethods, funcname):
   f = getattr(nodemethods, funcname)
   f(node, database)
   if (second_round == None):
    node.processed = True
   #��� �������� ������������� Node-��
   else:
    #print("something strange (set_sens_num): " + str(sensnum))
    if not hasattr(node, 'processed'):
     nodemethods.UniversalNodeExec(node, database)
  else:
   #��� Node-� �� �����������
   info = str(glbl.current_module) + "\n  " + str(node.type) + "\n   " + str(node.dumps()) + "\n\n\n"
   logs.log_file(9, info)
  node = node
  return
#<<<<<<<<<<<Process_Node<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

#>>>>>>>>>>>>>>>>work_with_module>>>>>>>>>>>>>>>>>>>>>>>
def add_statement_to_the_end_of_file(r):    #(+-)
 node = RedBaron("\n")
 '''
 r.append(node[0])
 node = RedBaron("return")
 r.append(node[0])
 index = len(r.data) - 1
 while(r.data[index][0].indentation != ""):
  r.data[index][0].decrease_indentation(1)
 nodemethods.ReturnNodeExec(r.data[index][0], database)
 node = RedBaron("\n")
 r.append(node[0])
 '''
 #nodemethods.stret_by_node(node, database)
 addsens = 1
 nodemethods.EmptyNode(node[0], database, addsens, 1, 1, r)
 #database.statements.append(st)
 r.append(node[0])

def add_statement_at_the_start_of_file(r): #(+-)
 node = r.data[0][0]
 if not hasattr(node, 'befsens'):
  addsens = 1
  st = nodemethods.EmptyNode(node, database, addsens)
  #database.statements.append(st)
 else:
  st = node.sttmnt
 iid = glbl.ret_free_num(glbl.used_ids, glbl.free_ids, "last_used_id", glbl)
 plc = glbls.PlcInText()
 plc.init_by_node(node, node.id)
 #���� � ������� ���� ���� Id, �� ������ ������ �� ����, ����� ������ ������ �������� �� ������ ���
 #������������ � ������ �� �� ������
 name = glbl.start_filenames[glbl.current_file].replace(glbl.clear_path, "clear_path").replace("\\","-")
 fnc = dtbase.FunctionType(plc, st, iid, name, glbl.current_module)
 database.functions.append(fnc)

def add_sens_import_to_the_start_of_file(r):
 r.insert(0,sensors.return_import_string_first())
 r.insert(0,sensors.return_import_string_second())
#<<<<<<<<<<<<<<<<<<<work_with_module<<<<<<<<<<<<<<<<<<<<<<<

#code block
glbl = glbls.Glbls()
database = dtbase.DataBase()
