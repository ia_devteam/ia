#coding:cp1251
import os
import redbaron
from redbaron import RedBaron
from redbaron.base_nodes import ProxyList
import logs

def return_sensor_string(num, template, typ=None):
 out = template.replace("%DAT%", str(num))
 return "ia_python_sensor(\"" + out + "\")"

def return_ret_sensor_string(num, template, b = None):
 out = template.replace("%DAT%", str(num))
 if b == None:
  b = ""
 else:
  if hasattr(b, 'dumps'):
   b = b.dumps()
  else:
   b = str(b)
 if b.find("\n"):
  i = b.count("\n")
 return "return ret_ia_python_sensor(\"" + out + "\", " + b + ")"

def return_yild_sensor_string(num, template, b = None):
 out = template.replace("%DAT%", str(num))
 if b == None:
  b = ""
 else:
  if hasattr(b, 'dumps'):
   b = b.dumps()
  else:
   b = str(b)
 return "(ret_ia_python_sensor(\"" + out + "\", " + str(b) + "))"

def return_import_string_first():
 return "from ia_python_sensor_module import ia_python_sensor"

def return_import_string_second():
 return "from ia_python_sensor_module import ret_ia_python_sensor"

def sort_offset(obj):
 return obj.offset

def sort_nodeline(obj):
 #return obj.red.absolute_bounding_box.top_left.line
 return obj.line

class sensor:
 def __init__(self, r=None, n = None, boo =None, level=None, line = None, flag=None, o=None):
  if r != None:
   self.red = r
   self.line = line
   self.sensnum = n
   self.is_ret = boo   #0 or 1 or 2
   self.is_two = level #2 or 3
   self.prefix = flag  #0 or 1
   if (flag == 0):
    self.ind = r.indentation
   self.offset = o

class sensor_storage:
 def __init__(self):
  self.storage = []

 def flush(self):
  self.storage = []

 def appnd(self,sensor):
  self.storage.append(sensor)

 def crt_snsr(self, red, sensnum, boo, level, line, flag):
  s = sensor(red, sensnum, boo, level, line, flag)
  self.storage.append(s)

 def sort(self):
  self.storage.sort(key = sort_offset)

 def sortline(self):
  self.storage.sort(key = sort_nodeline)

 #filename - ��� �����
 def inserting(self, level, template, uid, filename=None):
  #����� ��� ����������� (��� �����)
  if (level == 2):
   for sens in self.storage:
    if filename==None:
     insert_sensor(sens, template, uid)
    else:
     insert_sensor(sens, template, uid, filename)
  if (level == 3):
   for sens in self.storage:
    if (sens.is_two == 0):
     if filename==None:
      insert_sensor(sens, template, uid)
     else:
      insert_sensor(sens, template, uid, filename)

 def inserting_new(self, fileclear, filelab, level, template, uid, lastnum):
  i = 0
  line = "."
  lnght = len(self.storage)
  self.storage.sort(key = sort_nodeline)
  current_line = 1
  with open(fileclear, "r") as clear:
   if not(os.path.isdir(os.path.dirname(filelab))):
    os.makedirs(os.path.dirname(filelab))
   with open(filelab, "w") as lab:
    lab.write(return_import_string_first() + "\n")
    lab.write(return_import_string_second() + "\n")
    if (level == 2):
     while (i < lnght) and line:
      sens = self.storage[i]
      sensstr = ""
      while (current_line < sens.line):
       line = clear.readline()
       current_line+=1
       if (line):
        lab.write(line)
       else:
        lab.write("\n" + return_sensor_string(lastnum, template))
        return
      if(sens.prefix == 0):
       line = clear.readline()
       current_line+=1
       if (i+1 < lnght):
        if(self.storage[i+1].line == sens.line) and (self.storage[i+1].is_ret==0):
           lab.write(self.storage[i+1].red.indentation + return_sensor_string(self.storage[i+1].sensnum, template) + "\n")
           i = i+1
       if (line):
        lab.write(line + "\n")
       else:
        lab.write("\n")
       if(sens.ind == ""):
        sens.ind = sens.red.parent.indentation + "\t"
       lab.write(sens.ind + return_ret_sensor_string(sens.sensnum, template, None) + "\n")
      else:
       if (sens.is_ret == 0):
        sensstr = return_sensor_string(sens.sensnum, template)
        lab.write(sens.red.indentation + sensstr + "\n")
       elif(sens.is_ret == 1):
        h = ret_line_count(sens.red.value)
        if (h > 0):
         while (h > 0):
          line = clear.readline()
          current_line+=1
          h = h - 1
        sensstr = return_ret_sensor_string(sens.sensnum, template, sens.red.value)
        if (i+1 < lnght):
         if(self.storage[i+1].line == sens.line) and (self.storage[i+1].is_ret==0):
          lab.write(self.storage[i+1].red.indentation + return_sensor_string(self.storage[i+1].sensnum, template) + "\n")
          i = i+1
        line = clear.readline()
        current_line+=1
        lab.write(sens.red.indentation + sensstr + "\n")
       elif(sens.is_ret == 2):
        h = ret_line_count(sens.red.value)
        if (h > 0):
         while (h > 0):
          line = clear.readline()
          current_line+=1
          h = h - 1
        sensstr = return_yild_sensor_string(sens.sensnum, template, sens.red.value)
        if (i+1 < lnght):
         if(self.storage[i+1].line == sens.line) and (self.storage[i+1].is_ret==0):
          lab.write(self.storage[i+1].red.indentation + return_sensor_string(self.storage[i+1].sensnum, template) + "\n")
          i = i+1
        line = clear.readline()
        current_line+=1
      i+=1
    if (level == 3):
     while (i < lnght) and line:
      if (sens.is_two == 0):
       sens = self.storage[i]
       sensstr = ""
       while (current_line < sens.line):
        line = clear.readline()
        current_line+=1
        if (line):
         lab.write(line)
        else:
         lab.write("\n" + return_sensor_string(lastnum, template))
         return
       if(sens.prefix == 0):
        line = clear.readline()
        current_line+=1
        if (i+1 < lnght):
         if(self.storage[i+1].line == sens.line) and (self.storage[i+1].is_ret==0):
            lab.write(self.storage[i+1].red.indentation + return_sensor_string(self.storage[i+1].sensnum, template) + "\n")
            i = i+1
        if (line):
         lab.write(line + "\n")
        else:
         lab.write("\n")
        if(sens.ind == ""):
         sens.ind = "\t"
        lab.write(sens.ind + return_ret_sensor_string(sens.sensnum, template, None) + "\n")
       else:
        if (sens.is_ret == 0):
         sensstr = return_sensor_string(sens.sensnum, template)
         lab.write(sens.red.indentation + sensstr + "\n")
        elif(sens.is_ret == 1):
         h = ret_line_count(sens.red.value)
         if (h > 0):
          while (h > 0):
           line = clear.readline()
           current_line+=1
           h = h - 1
         sensstr = return_ret_sensor_string(sens.sensnum, template, sens.red.value)
         if (i+1 < lnght):
          if(self.storage[i+1].line == sens.line) and (self.storage[i+1].is_ret==0):
           lab.write(self.storage[i+1].red.indentation + return_sensor_string(self.storage[i+1].sensnum, template) + "\n")
           i = i+1
         line = clear.readline()
         current_line+=1
         lab.write(sens.red.indentation + sensstr + "\n")
        elif(sens.is_ret == 2):
         h = ret_line_count(sens.red.value)
         if (h > 0):
          while (h > 0):
           line = clear.readline()
           current_line+=1
           h = h - 1
         sensstr = return_yild_sensor_string(sens.sensnum, template, sens.red.value)
         if (i+1 < lnght):
          if(self.storage[i+1].line == sens.line) and (self.storage[i+1].is_ret==0):
           lab.write(self.storage[i+1].red.indentation + return_sensor_string(self.storage[i+1].sensnum, template) + "\n")
           i = i+1
         line = clear.readline()
         current_line+=1
       i+=1
    #���� �� ����� ����� - ����������
    line = clear.readline()
    while(line):
     lab.write(line)
     line = clear.readline()
    lab.write("\n" + return_sensor_string(lastnum, template))

def insert_sensor(sens, template, uid, filename = None, senstype=None):
 if not hasattr(sens.red, "inserted"):
  if (sens.prefix == 0):
   #�������� return (�������� ����������� ����������,
   # ������ �� ����������� ����������, ����� ��������� �� ���)
   s = return_ret_sensor_string(sens.sensnum, template, #sens.red.value, uid)
                                None)
   temp = RedBaron(s)
   sens.red.parent.append(temp)
  else:
   if (sens.is_ret == 0):
    s = return_sensor_string(sens.sensnum, template)
    temp = RedBaron(s)
    if (hasattr(sens.red, 'value')):
     sens_to_node(sens.red, s)
    else:
     #index = ind_of_parent(sens.red)
     #index = int(sens.red.index_on_parent)
     if (hasattr(sens.red,'parent')):
       index = sens.red.parent.index(sens.red)
       sens.red.parent.insert(index, s)
     else:
      sens.red.append(s)
   elif (sens.is_ret == 1):
    h = ret_line_count(sens.red.value)
    if (h > 0):
     while (h > 0):
      line = clear.readline()
      current_line+=1
      h = h - 1
    s = return_ret_sensor_string(sens.sensnum, template, sens.red.value)
    temp = RedBaron(s)
    #���� return, ����� ������
    try:
     index = sens.red.parent.index(sens.red)
     #index = int(sens.red.index_on_parent)
     #index = ind_of_parent(sens.red)
     sens.red.parent.value[index] = temp.data[0][0]
    except:
     sens.red = temp.data[0][0]
   else:
    #��� yield ����� �������
    index = sens.red.parent.index(sens.red)
    h = ret_line_count(sens.red.value)
    if (h > 0):
     while (h > 0):
      line = clear.readline()
      current_line+=1
      h = h - 1
    s = return_yild_sensor_string(sens.sensnum, template, sens.red.value)
    temp = RedBaron(s)
    sens.red.parent.value[index] = temp.data[0][0]

  if (filename != None):
   log_info = ", ".join([filename, str(sens.red.absolute_bounding_box.bottom_right), str(sens.sensnum), "\n"])
   logs.log_file(2, log_info, 0)
  sens.red.iserted = True
 return sens

def sens_to_node(node, sensstr):
 i = node.indentation
 st = node.dumps()
 st = sensstr + "\n" + st
 node = (RedBaron(st))[0]
 redbaron.base_nodes.GenericNodesUtils._string_to_node_list(st)
 GenericNodesUtils._string
 redbaron.nodes.LineProxyList()
 '''
 if hasattr(node, "value"):
  node.value = RedBaron(st)
 else:
  node = (RedBaron(st))[0]
 '''
 for n in node:
  while (n.indentation < i):
   n.increase_indentation(1)
  while (n.indentation > i):
   n.decrease_indentation(1)

def ind_of_parent(node):
 try:
  prnt = node.parent
  index = node.parent.index(node)
  return index
 except:
  return 0
  #return -1

def ret_line_count(red):
 i = 0
 if red == None:
  b = ""
 else:
  if hasattr(red, 'dumps'):
   b = red.dumps()
  else:
   b = str(red)
 if b.find("\n"):
  i = b.count("\n")
 return i


'''
Sensors types:
0 - STBreak
1 - STContinue
2  - STDoAndWhile
4  - STForEach (for-else)
6  - STIf (if-else)
7 - STOperational (callfunc)
8 - STReturn
10 - STThrow (raise)
11 - STTryCatchFinally (try-except-else-finally)
13 - STYieldReturn

No sensors:
12 - STUsing

No such labels:
STFor
STSwitch
STGoto

Prefix:
Special STReturn

Postfix:
Other
'''