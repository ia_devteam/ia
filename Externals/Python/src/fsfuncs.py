#coding:cp1251
import os, sys, shutil
#from easygui import logs.log_file
import glbls, logs


#�������� �� ����� ������� ���������� �������
def load_params_from_file(glbl, filename):
 params = []
 with open(filename) as params_file:
  params = params_file.readlines()
 if len(params)<7:
  logs.log_file(9, "Wrong Args Error\nInvalid number of parameters.\n App will be closed\n\n")
  exit(10)
 i = 0
 for p in params:
  params[i] = params[i].strip()
  i+=1
 if len(params) == 7:
  params.append(None)
  #arg_params:
  #0 - file with filenames
  #1 - dir_prefix
  #2 - lab source path
  #3 - workdir for xml and logs
  #4 - ndv-level
  #5 - sensor template string
  #6 - start sensor naumber
  #7 - module number

 glbl = glbls.Glbls.__init_plus__(glbl, params[0], int(params[6]), params[1], params[2], params[3], int(params[4]), params[5], params[7])


#�������� ������� ���������� �������
#������ ��� ���� �������������������
def check_path_args():
 if os.path.isfile(glbl.file_with_names):
  pass
 else:
  logs.log_file(9, "Wrong FileFilenames Error\nInvalid Filename at first arg.\n App will be closed\n\n")
  exit(11)
 #------------
 if type(glbl.start_sens_number) is int:
  pass
 else:
  logs.log_file(9, "Wrong SensNum Error\nInvalid Value at second arg.\n App will be closed\n\n")
  exit(12)
 #------------
 if os.path.isdir(glbl.clear_path):
  pass
 else:
  logs.log_file(9, "Wrong ClearPath Error\nInvalid Path at third arg.\n App will be closed\n\n")
  exit(13)
 #------------
 if os.path.isdir(glbl.lab_path):
  pass
 elif os.mkdir(glbl.lab_path):
  logs.log_file(9, "Wrong LabPath Error\nInvalid Path at fouth arg.\n App will be closed\n\n")
  exit(14)
 #------------
 if os.path.isdir(glbl.working_dir):
  pass
 elif os.mkdir(glbl.working_dir):
  logs.log_file(9, "Wrong WorkPath Error\nInvalid Path at fifth arg.\n App will be closed\n\n")
  exit(15)
 #------------
 if (glbl.ndv_level==2)or(glbl.ndv_level==3):
  pass
 else:
  logs.log_file(9, "Wrong NdvLevel Error\nInvalid Value at sixth arg.\n App will be closed\n\n")
  exit(16)
 if type(glbl.uid) is str:
  pass
 else:
  logs.log_file(9, "Wrong ModuleID Error\nInvalid Value at seventh arg.\n App will be closed\n\n")
  exit(16)
 #------------
 return(0)

def check_path_exists(path):
 if os.path.isdir(path):
  pass
 else:
  logs.log_file(9, "Wrong Path Error\nInvalid Path: Not exist!\n App will be closed\n\n")
  exit(14)

def prepare_dir_for_sensors(working_dir, sensoring_dir):
 name_dir = get_dir_name(working_dir)
 dir = os.path.join(sensoring_dir, name_dir)
 if os.path.isdir(dir):
  shutil.rmtree(dir)
 os.mkdir(dir)
 copy_subdirs_in_dir(working_dir, dir)
 return dir

def copy_subdirs_in_dir(source, target):
 dirs = os.listdir(source)
 for dr in dirs:
  srcname = os.path.join(source, dr)
  if os.path.isdir(srcname):
   dstname = os.path.join(target, dr)
   try:
    os.mkdir(dstname)
    copy_subdirs_in_dir(srcname, dstname)
   except (IOError, os.error):
    msg = "Can't copy " + srcname + " to " + dstname
    logs.log_file(9, msg + "\n App will be closed", "Wrong SensorPath Error")
    exit(13)

def get_dir_name(filename):
 name = ""
 if os.path.isdir(filename):
  name = filename[filename.rindex('\\')+1:len(filename)]
 elif os.path.isfile(filename):
  filename = filename[0:filename.rindex('\\')]
  name = filename[filename.rindex('\\')+1:len(filename)]
 else:
  logs.log_file(9, "Wrong SensorPath Error\nInvalid Path while processing namedir.\n App will be closed\n\n")
  exit(13)
 return name

def get_files_from_dir(curdir):
 files = []
 for file in os.listdir(curdir):
  path = os.path.join(curdir, file)
  if not os.path.isdir(path):
   files.append(path)
  else:
   files += get_files_from_dir(path)
 return files

#active_test_code
#prepare_dir_for_sensors("Z:\\_FT\\projs\\ecl_py", "Z:\\_FT\\projs\\py_scripterpy\\testdir")
#get_dir_name("Z:\\_FT\\projs\\py_scripterpy\\sensoringdir\\temp.py")

#����� ������� ��� �������� ��������
def get_offset(plcntxts, flnm):
 f = open(flnm)
 lines = f.readlines()
 f.close()
 offset = 0; i =0; j =0; c = 0
 curline = 1; curcol = 1
 ln = len(plcntxts)
 while (c < len):
  plc = plcntxts[c]
  if plc.f != f: continue
  if (plc.line == i) and (plc.column == j):
   plc.set_offset(offset)
   c+=1; continue
  i = plc.line
  j = plc.column
  while (i > curline):
   offset = offset + len(lines[curline - 1])
   curline+=1
  if curcol < plc.column:
   t = plc.column - curcol
   curcol = plc.column
   offset = offset + t
   plc.set_offset(offset)
   c+=1

def sort_line(p):
 return p.line

def sort_column(p):
 return p.clmn

def sort_id(p):
 return p.id

def sort_locations(plcntxt):
 plcntxt.sort(key = sort_line)
 plcntxt.sort(key = sort_column)

#������������ ������������ ������ ����
def processing_offset(plcs):
 sort_locations(plcs)
 filename = plcs[0].file
 get_offset(plcs, filename)
 plcs.sort(key = sort_id)


#code block
glbl = glbls.Glbls()