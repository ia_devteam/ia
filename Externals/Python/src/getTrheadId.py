#coding: utf-8

import threading
try:
    import ctypes
    libc = ctypes.cdll.LoadLibrary('libc.so.6')

    libc.syscall(186)

    def getThreadId():
        """Returns OS thread id - Specific to Linux"""
        return libc.syscall(186) # SYS_gettid value - System dependent
except Exception as e:
    import sys
    if sys.version_info < (2, 6):
        _get_threading = threading.currentThread
    else:
        _get_threading = threading.current_thread

    def getThreadId():
        return _get_threading().ident


################ TEST  ####################
# def worker():
#     """thread worker function"""
#     r = 0
#     while r < 10:
#         print 'Worker ',  getThreadId()
#         r += 1
#     return

# threads = []
# for i in range(5):
#     t = threading.Thread(target=worker)
#     threads.append(t)
#     t.start()
