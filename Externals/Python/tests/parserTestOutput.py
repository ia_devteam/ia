#coding: utf-8

import parserxsd as xsd

import logging
logging.basicConfig(level = logging.INFO)
log = logging.getLogger()

var = xsd.VariableType(
    ID = 1,
    Name = "Some",
    Namespace = "Some1",
    isGlobal = False,
    Definition = xsd.DefinitionType(xsd.LocationLine(line=1, column=1, file = 10)),
    AssignmentFrom = None,
    PointsToFunctions = None,
    ValueCallPosition = None,
    ValueGetPosition = None,
    ValueSetPosition = None,
)
varst = xsd.VariablesType(Variable = [var])

db = xsd.Database()
db.Variables = varst
log.info("db created")

with open('test.xml', 'w') as f:
    db.export(f, 0)


#проверка конвертирования
import dtbase
dtdb = dtbase.DataBaseObj()
v= dtbase.VariableType()
dtdb.variables.append(v)

v= dtbase.FieldType()
v.isstatic = True
dtdb.variables.append(v)

c = dtbase.ClassType()
dtdb.classes.append(c)

f = dtbase.FunctionType()
dtdb.functions.append(f)
f = dtbase.MethodType()
dtdb.functions.append(f)

o = dtbase.OperationType()
dtdb.operations.append(o)

f = dtbase.FileType()
dtdb.files.append(f)

st = dtbase.STBreakType()
dtdb.statements.append(st)

st = dtbase.STContinueType()
dtdb.statements.append(st)

st = dtbase.STDoAndWhileType()
dtdb.statements.append(st)

st = dtbase.STForEachType()
dtdb.statements.append(st)

st = dtbase.STIfType()
dtdb.statements.append(st)

st = dtbase.STOperationalType()
dtdb.statements.append(st)

st = dtbase.STReturnType()
dtdb.statements.append(st);

st = dtbase.STThrowType()
dtdb.statements.append(st)

st = dtbase.STTryCatchFinallyType()
dtdb.statements.append(st)

st = dtbase.STUsingType()
dtdb.statements.append(st)

st = dtbase.STYieldReturnType()
dtdb.statements.append(st)

from dtconvert import convert
dbC = convert(dtdb)
with open('testConverted.xml', 'w') as f:
    dbC.export(f, 0)

#проверка конвертирования не пустого
import dtbase
dtdb = dtbase.DataBaseObj()
v= dtbase.VariableType()
v.id = 1
dtdb.variables.append(v)

v= dtbase.FieldType()
v.isstatic = True
v.id = 2
v.initializerid = 2
dtdb.variables.append(v)

c = dtbase.ClassType()
c.id = 3
dtdb.classes.append(c)

f = dtbase.FunctionType()
f.id = 4
dtdb.functions.append(f)
f = dtbase.MethodType()
f.id = 5
dtdb.functions.append(f)

o = dtbase.OperationType()
f.id = 6
f.operationid = 10
dtdb.operations.append(o)

f = dtbase.FileType()
f.id = 7
f.initializerid = 7
dtdb.files.append(f)

import glbls
st = dtbase.STBreakType()
st.id = 8
st.firstsymbollocation = glbls.PlcInText()
st.lastsymbollocation = glbls.PlcInText()
dtdb.statements.append(st)

st = dtbase.STContinueType()
st.id = 9
st.firstsymbollocation = glbls.PlcInText()
st.lastsymbollocation = glbls.PlcInText()
dtdb.statements.append(st)

st = dtbase.STDoAndWhileType()
st.id = 8
st.firstsymbollocation = glbls.PlcInText()
st.lastsymbollocation = glbls.PlcInText()
st.body = dtbase.StatementReferenceType()
st.body.id = 10
st.body.kind = "STContinue"
st.condition = 12
dtdb.statements.append(st)

st = dtbase.STForEachType()
st.id = 9
st.firstsymbollocation = glbls.PlcInText()
st.lastsymbollocation = glbls.PlcInText()
st.head = 13
st.body = dtbase.StatementReferenceType()
st.body.id = 10
st.body.kind = "STContinue"
dtdb.statements.append(st)

st = dtbase.STIfType()
st.id = 10
st.condition = 15
st.then = dtbase.StatementReferenceType()
st.then.id = 16
st.then.kind = "STIf"
st.firstsymbollocation = glbls.PlcInText()
st.lastsymbollocation = glbls.PlcInText()
dtdb.statements.append(st)

st = dtbase.STOperationalType()
st.id = 11
st.operationid = 17
st.firstsymbollocation = glbls.PlcInText()
st.lastsymbollocation = glbls.PlcInText()
dtdb.statements.append(st)

st = dtbase.STReturnType()
st.id = 12
st.returnoperation = 18
st.firstsymbollocation = glbls.PlcInText()
st.lastsymbollocation = glbls.PlcInText()
dtdb.statements.append(st);

st = dtbase.STThrowType()
st.id = 13
st.exception = 19
st.firstsymbollocation = glbls.PlcInText()
st.lastsymbollocation = glbls.PlcInText()
dtdb.statements.append(st)

st = dtbase.STTryCatchFinallyType()
st.id = 14
st.firstsymbollocation = glbls.PlcInText()
st.lastsymbollocation = glbls.PlcInText()
st.tr = dtbase.StatementReferenceType(20, "STBreak")
dtdb.statements.append(st)

st = dtbase.STUsingType()
st.id = 15
st.firstsymbollocation = glbls.PlcInText()
st.lastsymbollocation = glbls.PlcInText()
st.body = dtbase.StatementReferenceType(21, "STReturn")
st.head = 22
dtdb.statements.append(st)

st = dtbase.STYieldReturnType()
st.id = 16
st.firstsymbollocation = glbls.PlcInText()
st.lastsymbollocation = glbls.PlcInText()
st.yieldreturnoperation = dtbase.OperationType()
st.yieldreturnoperation.id = 23
dtdb.statements.append(st)

st = dtbase.OperationType()
st.id = 25

cn = dtbase.CallNodeType()
cf = dtbase.FunctionReferenceType()
cf.id = 26
cf.kind = "function"
cn.callfunction.append(cf)
st.callnode.append(cn)

cn = dtbase.CallNodeType()
cf = dtbase.FunctionReferenceType()
cf.id = 26
cf.kind = "method"
cn.callfunction.append(cf)
st.callnode.append(cn)

cn = dtbase.CallNodeType()
cv = dtbase.VariableReferenceType()
cv.id =27
cv.kind = "variable"
cn.callvariable.append(cv)
st.callnode.append(cn)

cn = dtbase.CallNodeType()
cv = dtbase.VariableReferenceType()
cv.id =25
cv.kind = "field"
cn.callvariable.append(cv)
st.callnode.append(cn)

st.firstsymbollocation = glbls.PlcInText()
st.lastsymbollocation = glbls.PlcInText()
dtdb.operations.append(st)

from dtconvert import convert
dbC = convert(dtdb)
with open('testConvertedFull.xml', 'w') as f:
    dbC.export(f, 0)
