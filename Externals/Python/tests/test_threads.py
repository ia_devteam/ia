import sensor
import sensor_module
import threading
from get_threadid import getThreadId

sensor.sensor(100)

def worker():
    """thread worker function"""
    import sensor
    r = 0
    while r < 10:
        sensor.sensor(100 + r)
        print 'Worker ',  getThreadId()
        r += 1
    return

if __name__ == "__main__":
    threads = []
    for i in range(5):
        t = threading.Thread(target=worker)
        threads.append(t)
        t.start()
    threads = []
    for i in range(5):
        t = threading.Thread(target=worker)
        threads.append(t)
        t.start()
    while True:
        pass
        
    print "100"
