@echo off
set mypath=%~dp0
rem set mypath=%mypath:~0,-1%

set where=%1
set mylist=

pushd %mypath%
echo Will copy from %mypath% to %where% 

robocopy  /NJS /NJH /NP /NDL /NFL /NC /NS /S /PURGE %mypath%\pypy2-v5.7.1-win32\ %where%\pypy2-v5.7.1-win32\
IF %ERRORLEVEL% GTR 1 exit /b 111
robocopy /NJS /NJH /NP /NDL /NFL /NC /NS /S /PURGE %mypath%\src %where%\src
IF %ERRORLEVEL% GTR 1 exit /b 112

popd
goto :eof

:concat
set mylist=%mylist% %1
goto :eof