#coding:cp1251
import os
import datetime
import sys
import threading

## params ##
base_dir_name = os.path.join("c:\\", "pySensors\\", "maps") 	#������� �������
date_format  = "%Y.%m.%dT%H.%M" 					#������ ����
max_calls_number = 1000           					#����������� �� ���������� �������� ������������ � ������ �������
threads = {}             						#������� ��������
threads_start_times = {}        						#����� ������

### getThreadId
try:
 import ctypes
 libc = ctypes.cdll.LoadLibrary('libc.so.6')

 libc.syscall(186)

 def getThreadId():
  """Returns OS thread id - Specific to Linux"""
  return libc.syscall(186) # SYS_gettid value - System dependent
except Exception as e:
 if sys.version_info < (2, 6):
  cur_thread = threading.currentThread
 else:
  cur_thread = threading.current_thread

 def getThreadId():
  return cur_thread().ident

jn = os.path.join

if not os.path.exists(base_dir_name):
 os.makedirs(base_dir_name)

def create_file_name(key, dts):
 name = "%s_%s_%s_%s" % (key[0], key[1], key[2], dts)
 return jn(base_dir_name, name)

def get_file_name(key):
 return threads_start_times.get(key).get("filename")

def get_key(call_label = None):
    return (str(os.getpid()), str(getThreadId()), call_label)

def flush_messages(key):
 msgs = threads.get(key)
 if len(msgs) == 0:
  return
 with open (get_file_name(key), "a+") as f:
  for i in msgs:
   f.write(str(i) + "\n")
  f.close()
  threads[key] = []

def flush_all_messages(*argv):
 global max_calls_number
 for k in threads.iterkeys():
  flush_messages(k)
 max_calls_number = 1

def ia_python_sensor(sensor_num, call_label=None):
 key = get_key()
 msgs = threads.setdefault(key, [])
 if not threads_start_times.has_key(key):
  dts = datetime.datetime.today().strftime(date_format)
  threads_start_times[key] = {
   "dt": dts,
   "filename": create_file_name(key, dts)
 }

 msgs.append(sensor_num)
 if len(msgs) > max_calls_number:
  flush_messages(key)

def ret_ia_python_sensor(num, a):
 b = a
 sensor(num)
 return b

import atexit
atexit.register(flush_all_messages)

