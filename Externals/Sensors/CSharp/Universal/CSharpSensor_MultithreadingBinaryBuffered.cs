﻿using System;
using System.IO;

namespace IACSharpSensor
{
    /// <summary>
    /// Реализация датчика с кешированием результата в локальном хранилище потока.
    /// </summary>
    internal static class CSharpSensor_MultithreadingBinaryBuffered
    {
        static int BUFFERSIZE = 10240;/*~10Kb*/

        //для каждого потока данное поле - своё.
        [ThreadStatic]
        static Finalizable finalizable;

        public static void SensorReached(int sensId, int sId)
        {
            if (finalizable == null)
                finalizable = new Finalizable(sId);

            finalizable.WriteSensor(sensId);
        }

        sealed class Finalizable
        {
            /// <summary>
            /// Размер буффера при записи на диск. Должен быть кратен int.
            /// </summary>
            static int BUFFERSIZE = CSharpSensor_MultithreadingBinaryBuffered.BUFFERSIZE;

            int[] sensArray;
            byte[] bytes;
            int threadId;
            string sensorsFilePath;

            int cursor;
            int bytesBufferUsedSize;

            public Finalizable(int sId)
            {
                sensArray = new int[BUFFERSIZE / sizeof(int)];

                bytes = new byte[BUFFERSIZE];

                threadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
                sensorsFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "IA_CSharp_" + sId + "_sensors_" + threadId + ".log");

                bytesBufferUsedSize = BUFFERSIZE;
            }

            public void WriteSensor(int sensId)
            {
                sensArray[cursor++] = sensId;
                if (cursor == sensArray.Length)
                {
                    Flush();
                    cursor = 0;
                }
            }

            private void Flush()
            {
                //конвертация интов в байты копированием. Для скорости - unsafe.
#if USE_UNSAFE__LUKE
                unsafe
                { //http://stackoverflow.com/questions/2133858/in-c-convert-ulong64-to-byte512-faster
                    fixed (int* arr = sensArray)
                    {
                        System.Runtime.InteropServices.Marshal.Copy(new IntPtr((void*)arr), bytes, 0, cursor * sizeof(int));
                    }
                }
#else
                Buffer.BlockCopy(sensArray, 0, bytes, 0, bytesBufferUsedSize);
#endif

                using (BinaryWriter bw = new BinaryWriter(File.Open(sensorsFilePath, FileMode.Append, FileAccess.Write, FileShare.None)))
                    bw.Write(bytes, 0, bytesBufferUsedSize);
            }

            /// <summary>
            /// Вызовется только когда держащий поток будет уничтожаться. 
            /// Вопрос с пулом потоков - там, по идее, не происходит уничтожения потока. Что будет с хранилищем локального потока при передаче от одной функциональности другой.
            /// </summary>
            ~Finalizable()
            {
                WriteSensor(-1);
                bytesBufferUsedSize = cursor * sizeof(int);
                Flush();
            }
        }
    }
}
