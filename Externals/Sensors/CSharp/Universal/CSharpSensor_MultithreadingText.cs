﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace IACSharpSensor
{
    internal static class CSharpSensor_MultithreadingText
    {
        [ThreadStatic]
        static Finalizable finalizable;

        public static void SensorReached(int sensId)
        {
            if (finalizable == null)
                finalizable = new Finalizable();

            finalizable.WriteSensor(sensId);
        }

        sealed class Finalizable
        {
            int threadId;
            string sensorsFilePath;

            public Finalizable()
            {
                threadId = System.Threading.Thread.CurrentThread.ManagedThreadId;

                sensorsFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "sensors" + threadId + ".log");
            }

            public void WriteSensor(int sensId)
            {
                File.AppendAllText(sensorsFilePath, sensId.ToString() + "\r\n");
            }
        }
    }
}
