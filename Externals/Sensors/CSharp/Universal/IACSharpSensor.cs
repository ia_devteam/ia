﻿#if (!SIMPLETEXT && !SIMPLEBINARY && !MULTITHREADINGTEXT && !MULTITHREADINGBUFFERED)
#define MULTITHREADINGBUFFERED
#endif

using System;
using System.Collections.Generic;
using System.Text;

namespace IACSharpSensor
{
    public static class IACSharpSensor
    {
        /*must be inlined when in action...*/
        public static void SensorReached(int num, int id)
        {
#if SIMPLETEXT
            CSharpSensor_SimpleText.SensorReached(num, id);
#elif SIMPLEBINARY
            CSharpSensor_SimpleBinary.SensorReached(num, id);
#elif MULTITHREADINGTEXT
            CSharpSensor_MultithreadingText.SensorReached(num, id);
#elif MULTITHREADINGBUFFERED
            CSharpSensor_MultithreadingBinaryBuffered.SensorReached(num, id);
#endif
        }

        public static void SensorReached(int num)
        {
#if SIMPLETEXT
            CSharpSensor_SimpleText.SensorReached(num, 0);
#elif SIMPLEBINARY
            CSharpSensor_SimpleBinary.SensorReached(num, 0);
#elif MULTITHREADINGTEXT
            CSharpSensor_MultithreadingText.SensorReached(num, 0);
#elif MULTITHREADINGBUFFERED
            CSharpSensor_MultithreadingBinaryBuffered.SensorReached(num, 0);
#endif
        }
    }
}
