﻿using System;
using System.IO;

namespace IACSharpSensor
{
    /// <summary>
    /// Простейшая реализация датчика - по приёму сигнала тупо дописывает в файл "user.home/sensors.log" полученное число по одному в строку. Вероятны сбои при многопоточном вызове.
    /// </summary>    
    internal static class CSharpSensor_SimpleText
    {
        public static void SensorReached(int sensId, int sId)
        {
            File.AppendAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "IA" + sId + "sensors.log"), sensId.ToString() + "\r\n");
        }
    }
}
