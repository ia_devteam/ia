﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace IACSharpSensor
{
    internal static class CSharpSensor_SimpleBinary
    {
        static string sensorsFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "sensors.log");
        public static void SensorReached(int sensId)
        {   
            using (BinaryWriter bw = new BinaryWriter(File.Open(sensorsFilePath, FileMode.Append, FileAccess.Write, FileShare.None)))
                    bw.Write(sensId);
        }
    }
}
