#!/bin/bash
SENSORDIR=/var/tmp/IACSSENSOR

echo Setting MONO_PATH environment variable to include ${SENSORDIR}.
MONO_PATH=${MONO_PATH:=/usr/lib}:${SENSORDIR}
echo MONO_PATH=${MONO_PATH} >> /etc/environment
echo MONO_PATH is ${MONO_PATH}

echo Creating dir ${SENSORDIR} for sensor assembly.
mkdir -p ${SENSORDIR}
export MONO_PATH

echo Copy assembly to destination.
cp -u IACSharpSensor.dll ${SENSORDIR}/IACSharpSensor.dll

echo Done. Reboot required.
