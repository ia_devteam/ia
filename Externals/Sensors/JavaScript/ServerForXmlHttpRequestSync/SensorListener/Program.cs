﻿using System;

namespace SensorListener
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isDebugMode = false;
#if DEBUG
            isDebugMode = true;
#endif
            if (args.Length != 2 && !isDebugMode)
            {
                Usage();
                return;
            }

            string bindingAddress = String.Empty;
            string sensorDirectory = String.Empty;

            if (isDebugMode)
            {
                bindingAddress = "http://192.168.50.1:65432/Simple/";
                sensorDirectory = "d:/tmp/";
            }
            else
            {
                bindingAddress = args[0];
                sensorDirectory = args[1];
            }

            if (!System.IO.Directory.Exists(sensorDirectory))
            {
                Console.WriteLine("Directory <" + sensorDirectory + "> not found!");
                return;
            }

            

            var ws = new SimpleWebServer(bindingAddress);

            var worker = new Worker(sensorDirectory);

            //ConsoleExitEvent.Closing += worker.Flush;
            AppDomain.CurrentDomain.ProcessExit += new EventHandler((_, __) => { worker.Flush(); });
            ws.RequestReceived += worker.HttpReceivehandler;

            ws.Start();
            Console.WriteLine("Listening to " + bindingAddress);
            Console.WriteLine("Press <Enter> key to stop...");
            Console.WriteLine("WARNING! Closing window not by <Enter> may drop sensors.");
            Console.ReadLine();
            ws.Stop();

            worker.Flush();
        }

        static private void Usage()
        {
            Console.WriteLine(
@"Usage:
    {0} {ip-address:port/path/} {path_to_sensor_dir}
        {ip-address:port/path/} = http address to bind for. Ex.: http://localhost:1234/Simple/
        {path_to_sensor_dir} = fully qualified"
                );
        }

    }
}
