﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Runtime.InteropServices;

//namespace SensorListener
//{
//    internal static class ConsoleExitEvent
//    {
//        [DllImport("Kernel32")]
//        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

//        private delegate bool EventHandler(CtrlType sig);
//        private static EventHandler _handler;

//        internal static event Action Closing;

//        static ConsoleExitEvent()
//        {
//            _handler += new EventHandler(Handler);
//            SetConsoleCtrlHandler(_handler, true);
//        }

//        private enum CtrlType
//        {
//            CTRL_C_EVENT = 0,
//            CTRL_BREAK_EVENT = 1,
//            CTRL_CLOSE_EVENT = 2,
//            CTRL_LOGOFF_EVENT = 5,
//            CTRL_SHUTDOWN_EVENT = 6
//        }

//        private static bool Handler(CtrlType sig)
//        {
//            switch (sig)
//            {
//                case CtrlType.CTRL_C_EVENT:
//                case CtrlType.CTRL_LOGOFF_EVENT:
//                case CtrlType.CTRL_SHUTDOWN_EVENT:
//                case CtrlType.CTRL_CLOSE_EVENT:
//                    if (Closing != null)
//                        Closing();
//                    break;
//                default:
//                    return false;
//            }
//            return true;
//        }
//    }


//}
