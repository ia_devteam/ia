﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;

namespace SensorListener
{

    class Worker
    {
        /// <summary>
        /// Объём буффера принимаемых сенсоров перед записью в файл
        /// </summary>
        const int FLUSH_THRESHOLD = 128000 - 1 /*на 10 Кб по 2 int в пачке*/;

        static readonly byte[] responseBody;

        static Worker()
        {
            responseBody = System.Text.Encoding.Default.GetBytes("<HTML><BODY>1</BODY></HTML>");
        }

        private ConcurrentDictionary<string, ConcurrentQueue<SensorWithSN>> acceptedSensorsCache = new ConcurrentDictionary<string, ConcurrentQueue<SensorWithSN>>();
        private Dictionary<string, object> fileLocks = new Dictionary<string, object>();

        private string sensorDirectory;

        public Worker(string sensorDir)
        {
            sensorDirectory = sensorDir;

        }

        public void HttpReceivehandler(object sender, HttpListenerContext context)
        {
            context.Response.AddHeader("Access-Control-Allow-Origin", "*");

            if (context.Request.HttpMethod == "OPTIONS")
            {
                context.Response.AddHeader("Access-Control-Allow-Methods", "POST");
                context.Response.AddHeader("Access-Control-Allow-Headers", "*");
                context.Response.AddHeader("Access-Control-Max-Age", "1728000");
                //context.Response.OutputStream.Write(responseBody, 0, responseBody.Length);

                context.Response.StatusCode = (int)HttpStatusCode.OK;
                context.Response.Close();
                return;
            }

            var body = string.Empty;

            using (var reader = new System.IO.StreamReader(context.Request.InputStream))
            {
                //ожидаемый формат: "s=<sensornum>&sn=<serialnum>&p=<webpath>"
                body = reader.ReadToEnd();
            }

            //Console.WriteLine(body);
            if (String.IsNullOrWhiteSpace(body))
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                Console.WriteLine("Incoming request empty.");
            }
            else
                try
                {
                    int bodyFirstAmpersand = body.IndexOf('&');
                    int bodySecondAmpersand = body.IndexOf('&', bodyFirstAmpersand + 1);
                    int bodyFirstEqual = body.IndexOf('=') + 1;
                    int bodySecondEqual = body.IndexOf('=', bodyFirstAmpersand) + 1;
                    int bodyThrirdEqual = body.IndexOf('=', bodySecondAmpersand) + 1;

                    if (bodyFirstAmpersand == -1 || bodyFirstEqual == 0 || bodySecondEqual == 0)
                        throw new ArgumentException("Input message not in accepted format: s=SENSORNUM&sn=SERIALNUM&p=WEBPATH");

                    int sensornumber;
                    int serialnumber;
                    string uri;

                    if (!int.TryParse(body.Substring(bodyFirstEqual, bodyFirstAmpersand - bodyFirstEqual), out sensornumber)
                        || !int.TryParse(body.Substring(bodySecondEqual, bodySecondAmpersand - bodySecondEqual), out serialnumber))
                        throw new ArgumentException("Cannot parse items <sensornumber> and <serialnumber> as int from " + body);

                    uri = body.Substring(bodyThrirdEqual, body.Length - bodyThrirdEqual);
                    //на случай наличия в присланной строке адреса всякой гадости - обрубаем по первому амперсанду
                    int uriAmpersand = uri.IndexOf('&');
                    if (uriAmpersand != -1)
                        uri = uri.Substring(0, uriAmpersand);

                    var hashbytes = MD5.Create().ComputeHash(System.Text.Encoding.UTF8.GetBytes(uri));

                    var hash = Convert.ToBase64String(hashbytes).Replace("/", "_");

                    var key = hash;

                    acceptedSensorsCache.GetOrAdd(key, new ConcurrentQueue<SensorWithSN>()).Enqueue(new SensorWithSN() { sensorNumber = sensornumber, sensorSerialNumber = serialnumber });

                    //var sensorFile = System.IO.Path.Combine(sensorDirectory, hash);

                    if (acceptedSensorsCache[key].Count > FLUSH_THRESHOLD)
                    {
                        //заранее подготавливаем объект для подмены
                        var replace = new ConcurrentQueue<SensorWithSN>();

                        //фиксируем текущее состояние и подменяем в словаре на новое хранилище
                        var listToFixate = acceptedSensorsCache[key];
                        acceptedSensorsCache.AddOrUpdate(key, replace, (x, y) => replace);

                        System.Threading.Tasks.Task.Factory.StartNew(() => Flush(listToFixate, System.IO.Path.Combine(sensorDirectory, hash)));


                    }

                    context.Response.StatusCode = (int)HttpStatusCode.OK;

                }
                catch (Exception e)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                    Console.WriteLine("Processing error: " + e.Message);
                }

            //context.Response.OutputStream.Write(responseBody, 0, responseBody.Length);
            try
            {
                context.Response.Close();
            }
            catch
            {
            }
        }

        private void Flush(IProducerConsumerCollection<SensorWithSN> list1, string filename)
        {
            int count = list1.Count;
            var arr = list1.ToArray();
            var bytes = new byte[4 * count * 2];

            for (int i = 0; i < count; i++)
                arr[i].GetPairAsBytes().CopyTo(bytes, i * 8);

            byte[] header2 = null;

            object objToLockOn = null;
            lock (fileLocks)
            {
                if (!fileLocks.ContainsKey(filename))
                    fileLocks.Add(filename, new object());
                objToLockOn = fileLocks[filename];
            }

            lock (objToLockOn)
            {
                if (!File.Exists(filename))
                {
                    //формируем заголовок трассы если файл не существует
                    var header = BitConverter.GetBytes((int)-2);
                    header2 = new byte[8];
                    header.CopyTo(header2, 0);
                    header.CopyTo(header2, 4);
                }

                using (BinaryWriter bw = new BinaryWriter(File.Open(filename, FileMode.Append, FileAccess.Write, FileShare.None)))
                {
                    if (header2 != null)
                        bw.Write(header2, 0, 8);
                    bw.Write(bytes, 0, bytes.Length);
                }
            }
        }

        public void Flush()
        {
            foreach (var key in acceptedSensorsCache.Keys)
                Flush(acceptedSensorsCache[key], System.IO.Path.Combine(sensorDirectory, key));
        }
    }

    struct SensorWithSN
    {
        public int sensorNumber;
        public int sensorSerialNumber;

        public byte[] GetPairAsBytes()
        {
            var result = new byte[8];
            BitConverter.GetBytes(sensorNumber).CopyTo(result, 0);
            BitConverter.GetBytes(sensorSerialNumber).CopyTo(result, 4);
            return result;
        }
    }

}
