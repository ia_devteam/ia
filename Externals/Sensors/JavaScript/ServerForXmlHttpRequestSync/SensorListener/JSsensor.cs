﻿using System;

namespace SensorListener
{
    /*
    JSSensor text
    * 
    * 
    ia_sensorcatcher_host - прописать в hosts системы с нужным ip.
    * 
    * */
    public static class JSsensor
    {
        public const string JQueryAsync = 
@"
var ia_JS_sensor_Number; 
function ia_JS_sensor(num) { 
    if(ia_JS_sensor_Number == null) {
        ia_JS_sensor_Number = 0;
    } else {
        ia_JS_sensor_Number++;
    }
    $.post(
        'http://localhost:666/Simple',
        {s:num,sn:ia_JS_sensor_Number,p:window.location.href},
        function() {}
    );
}
";
        public const string XmlHttpRequestSync =
@"
var ia_JS_sensor_SerialNumber;
var ia_JS_sensor_xmlHttpRequest;
function ia_JS_sensor(num){
    if(ia_JS_sensor_SerialNumber == null) {
    ia_JS_sensor_SerialNumber = 0;
    } else {
    ia_JS_sensor_SerialNumber++;
    }

    if (ia_JS_sensor_xmlHttpRequest==null)
    {
        try {
            ia_JS_sensor_xmlHttpRequest = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (e) {
            try {
                ia_JS_sensor_xmlHttpRequest = new ActiveXObject('Microsoft.XMLHTTP');
            } catch (E) {
                ia_JS_sensor_xmlHttpRequest = false;
            }
        }
        if (!ia_JS_sensor_xmlHttpRequest && typeof XMLHttpRequest!='undefined') {
            ia_JS_sensor_xmlHttpRequest = new XMLHttpRequest();
        }	
    }

    ia_JS_sensor_xmlHttpRequest.open('POST', 'http://ia_sensorcatcher_host:65432/Simple', false); // Открываем синхронное соединение
    var params = 's=' + num + '&sn=' + ia_JS_sensor_SerialNumber + '&p=' + window.location.href;
    ia_JS_sensor_xmlHttpRequest.send(params); // Отправляем POST-запрос
}
";
        public const string XmlHttpRequestAsync =
@"
var ia_JS_sensor_SerialNumber;
var ia_JS_sensor_xmlHttpRequest;
function ia_JS_sensor(num) {
    if (ia_JS_sensor_SerialNumber == null) {
        ia_JS_sensor_SerialNumber = 0;
    } else {
        ia_JS_sensor_SerialNumber++;
    }

    if (ia_JS_sensor_xmlHttpRequest == null) {
        try {
            ia_JS_sensor_xmlHttpRequest = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (e) {
            try {
                ia_JS_sensor_xmlHttpRequest = new ActiveXObject('Microsoft.XMLHTTP');
            } catch (E) {
                ia_JS_sensor_xmlHttpRequest = false;
            }
        }
        if (!ia_JS_sensor_xmlHttpRequest && typeof XMLHttpRequest != 'undefined') {
            ia_JS_sensor_xmlHttpRequest = new XMLHttpRequest();
        }
    }

    ia_JS_sensor_xmlHttpRequest.open('POST', 'http://ia_sensorcatcher_host:65432/Simple', true); // Открываем синхронное соединение
    var params = 's=' + num + '&sn=' + ia_JS_sensor_SerialNumber + '&p=' + window.location.href;
    ia_JS_sensor_xmlHttpRequest.send(params); // Отправляем POST-запрос
}
";
    }
}

