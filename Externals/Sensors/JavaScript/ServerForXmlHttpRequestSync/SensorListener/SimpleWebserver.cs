﻿using System;
using System.Net;
using System.Threading;

namespace SensorListener
{

    class SimpleWebServer
    {
        public delegate void RequestReceivedHandler(object sender, HttpListenerContext context);
        public event RequestReceivedHandler RequestReceived;

        private readonly HttpListener _listener;
        
        private readonly System.Threading.Tasks.Task _connthread;
        private readonly CancellationTokenSource _cts;
        //private readonly Thread _connthread;

        public SimpleWebServer(string prefix)
        {
            _listener = new HttpListener();
            _listener.Prefixes.Add(prefix);
            //_listener.IgnoreWriteExceptions = true;
            //_connthread = new Thread(ConnectionThread);
            _cts = new CancellationTokenSource();

            _connthread = new System.Threading.Tasks.Task(() => ConnectionThread(_cts.Token), _cts.Token);
        }

        public void Start()
        {
            _connthread.Start();
        }

        public void Stop()
        {
            _cts.Cancel();
            try
            {
                _listener.Stop();
            }
            catch (ObjectDisposedException)
            {
            }
        }

        private void ConnectionThread(CancellationToken cancellationToken)
        {
            _listener.Start();
            while (!cancellationToken.IsCancellationRequested)
            {
                ProcessRequest();
            }

        }

        private void ProcessRequest()
        {
            IAsyncResult result = _listener.BeginGetContext(ListenerCallback, _listener);
            result.AsyncWaitHandle.WaitOne();
        }

        protected void ListenerCallback(IAsyncResult result)
        {
            if (_listener == null || !_listener.IsListening) return;
            var context = _listener.EndGetContext(result);
            //OnRequestReceived(context);
            if (RequestReceived != null) RequestReceived(this, context);
        }

    }

}
