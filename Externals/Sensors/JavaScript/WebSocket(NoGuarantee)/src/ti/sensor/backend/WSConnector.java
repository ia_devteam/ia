package ti.sensor.backend;

import sun.misc.BASE64Encoder;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

/**
 * Created by denis on 16.06.14.
 * Класс для подключения web-сокета
 */
public class WSConnector implements Runnable {
    private Socket socket;
    private OutputStream socketOutput;
    private InputStream socketInput;

    public WSConnector(Socket socket) throws IOException {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            socketInput = new DataInputStream(socket.getInputStream());
            socketOutput = new DataOutputStream(socket.getOutputStream());
            handleShake();
            BufferedWriter bw=new BufferedWriter(new FileWriter("log.txt"));
            int buffLenth = 1;
            while (true) {
//                int bufSize = socketInput.available();
//                if (bufSize > 0) {
//                    byte[] buf = new byte[bufSize];
//                    socketInput.read(buf);
//                    System.out.println("new String(buf,Charset.forName(\"UTF-8\")) = " + new String(buf, Charset.forName("UTF-8")));

//                    {
                int len = 0;

                //rawIn is a Socket.getInputStream();
                int msgLength =0;
                while (true) {
                    msgLength = socketInput.available();
                    //System.out.println("msgLength = " + msgLength);

//
                    if(msgLength==0){
                        continue;
                    }
                    brodcast("stop!");



                    byte[] b = new byte[msgLength];
                    System.out.println("START " + msgLength);

                    len = socketInput.read(b);
                    //READ READ READ....
                    


                    if (len != -1) {

                        byte rLength = 0;
                        int rMaskIndex = 2;
                        int rDataStart = 0;
                        //b[0] is always text in my case so no need to check;
                        byte data = b[1];
                        byte op = (byte) 127;
                        rLength = (byte) (data & op);

                        if (rLength == (byte) 126) rMaskIndex = 4;
                        if (rLength == (byte) 127) rMaskIndex = 10;

                        byte[] masks = new byte[4];

                        int j = 0;
                        int i = 0;
                        for (i = rMaskIndex; i < (rMaskIndex + 4); i++) {
                            masks[j] = b[i];
                            j++;
                        }

                        rDataStart = rMaskIndex + 4;

                        int messLen = len - rDataStart;

                        byte[] message = new byte[messLen];

                        for (i = rDataStart, j = 0; i < len; i++, j++) {
                            message[j] = (byte) (b[i] ^ masks[j % 4]);
                        }

//                        System.out.println();
                        //parseMessage(new String(message));
                        bw.write(new String(message));
                        bw.newLine();
                        b = new byte[buffLenth];

                    }
//                        }
//                    }

//                }
//                System.out.println("bufSize = " + bufSize);
//                buf = bfr.readLine();
//                if(buf!=null && !buf.isEmpty()){
//                    System.out.println(buf);
//                }
//                    Thread.sleep(200);
//                }


//            brodcast("{\"command\":\"status\",\"stat\":\"" + "Запуск интеграционной среды" + "\"}");
//            brodcast("{\"command\":\"status\",\"stat\":\"" + "Запуск интеграционной среды1" + "\"}");
//            brodcast("{\"command\":\"status\",\"stat\":\"" + "Запуск интеграционной среды2" + "\"}");
//            brodcast("{\"command\":\"status\",\"stat\":\"" + "Запуск интеграционной среды3" + "\"}");

//
//            BufferedReader bf = new BufferedReader(new InputStreamReader(deployManagerConnector.accept().getInputStream()));
//            String message;
//            while ((message = bf.readLine()) != null) {
//                System.out.
//                System.setOut(new PrintStream(System.out, true, "Cp866"));
//                System.out.println("message = " + message);
//                if (message.length() > 32)
//                    brodcast("{\"command\":\"status\",\"stat\":\"" + message.replace('\n', ' ').substring(0, 32) + "\"}");
//                else
//                    brodcast("{\"command\":\"status\",\"stat\":\"" + message.replace('\n', ' ') + "\"}");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
//
        }
    }
//        try {
//            brodcast("{\"command\":\"redirect\",\"url\":\"http://localhost:8080/strix\"}");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        StartupService.STARTUP = true;

    private String base64(byte[] input) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(input);
    }

    public void brodcast(String mess) throws IOException {
//        System.out.println("WSConnector.brodcast " + mess);
        byte[] rawData = mess.getBytes(Charset.forName("UTF-8"));
        int frameCount;
        byte[] frame = new byte[10];

        frame[0] = (byte) 129;

        if (rawData.length <= 125) {
            frame[1] = (byte) rawData.length;
            frameCount = 2;
        } else if (rawData.length >= 126 && rawData.length <= 65535) {
            frame[1] = (byte) 126;
            byte len = (byte) rawData.length;
            frame[2] = (byte) ((len >> 8) & (byte) 255);
            frame[3] = (byte) (len & (byte) 255);
            frameCount = 4;
        } else {
            frame[1] = (byte) 127;
            byte len = (byte) rawData.length;
            frame[2] = (byte) ((len >> 56) & (byte) 255);
            frame[3] = (byte) ((len >> 48) & (byte) 255);
            frame[4] = (byte) ((len >> 40) & (byte) 255);
            frame[5] = (byte) ((len >> 32) & (byte) 255);
            frame[6] = (byte) ((len >> 24) & (byte) 255);
            frame[7] = (byte) ((len >> 16) & (byte) 255);
            frame[8] = (byte) ((len >> 8) & (byte) 255);
            frame[9] = (byte) (len & (byte) 255);
            frameCount = 10;
        }

        int bLength = frameCount + rawData.length;

        byte[] reply = new byte[bLength];

        int bLim = 0;
        for (int i = 0; i < frameCount; i++) {
            reply[bLim] = frame[i];
            bLim++;
        }
        for (int i = 0; i < rawData.length; i++) {
            reply[bLim] = rawData[i];
            bLim++;
        }
        socketOutput.write(reply);
        socketOutput.flush();
    }

    private void handleShake() throws IOException, NoSuchAlgorithmException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socketInput));
        PrintWriter printwriter = new PrintWriter(socket.getOutputStream(), false);
        String request;
        Properties props = new Properties();
        while (!(request = bufferedReader.readLine()).isEmpty()) {
            System.out.println("req = " + request);
            if (request.contains(":")) {
                String[] q = request.split(": ");
                props.put(q[0], q[1]);
            }
        }
        String key = (String) props.get("Sec-WebSocket-Key");
        String r = key + "" + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"; // magic key
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.reset();
        md.update(r.getBytes());
        byte[] sha1hash = md.digest();
        String returnBase = null;
        try {
            returnBase = base64(sha1hash);
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        printwriter.println("HTTP/1.1 101 Switching Protocols");
        printwriter.println("Upgrade: websocket");
        printwriter.println("Connection: Upgrade");
        printwriter.println("Sec-WebSocket-Accept: " + returnBase);
        printwriter.println("");
        printwriter.flush();
    }
}
