//package java.ti.sensor.backend;
//
//import java.io.*;
//import java.lang.reflect.InvocationTargetException;
//import java.net.Socket;
//import java.security.NoSuchAlgorithmException;
//
//public class RequestExecuter implements Runnable {
//    private Socket socket;
//    private OutputStream socketOutput;
//    private InputStream socketInput;
//
//    public RequestExecuter(Socket socket) {
//        this.socket = socket;
//    }
//
//    @Override
//    public void run() {
//        try {
//            socketInput = socket.getInputStream();
//            socketOutput = socket.getOutputStream();
//            String resource = readInputHeaders();
//            if (resource != null) {
//                InputStream fileInputStream = RequestExecuter.class.getResourceAsStream(resource.substring(1));
//                if (fileInputStream != null) {
//                    writeResponce(fileInputStream);
//                } else {
//                    StringBuilder response = new StringBuilder();
//                    response.append("HTTP/1.1 200 OK \r\n")
//                            .append("Server: strixStartupService/2014-06-16\r\n")
//                            .append("Content-Length:").append(0).append("\r\n")
//                            .append("Connection: close\r\n\r\n")
//                            .append("");
//                    socketOutput.write(resource.getBytes());
//                    socketOutput.flush();
//                }
//            }
//        } catch (Throwable e) {
//            System.out.println("RequestExecuter не удалось обработать входящий запрос на получение ресурса");
//            e.printStackTrace();
//        }
//
//    }
//
//    private String readInputHeaders() throws NoSuchMethodException, NoSuchAlgorithmException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
//        String resource = null;
//        try {
//            BufferedReader br = new BufferedReader(new InputStreamReader(socketInput));
//            String header;
//            while ((header = br.readLine()) != null && !header.trim().isEmpty()) {
//                System.out.println("header = " + header);
//                String[] request = header.split(" ");
//                if (request[0].equals("GET")) {
//                    resource = request[1];
//                }
//            }
//        } catch (IOException e) {
//            System.out.println("RequestExecuter не удалось прочитать запрос на получение ресурса");
//            e.printStackTrace();
//        }
//        return resource;
//    }
//
//    private void writeResponce(InputStream stream) throws IOException {
//        StringBuilder responce = new StringBuilder();
//        byte[] content = new byte[100500];
//        int length = stream.read(content);
//        responce.append("HTTP/1.1 200 OK \r\n")
//                .append("Server: strixStartupService/2014-06-16\r\n")
//                .append("Content-Length:").append(length).append("\r\n")
//                .append("Connection: close\r\n\r\n");
//        socketOutput.write(responce.toString().getBytes());
//        socketOutput.write(content);
//        socketOutput.flush();
//    }
//
//
//}
