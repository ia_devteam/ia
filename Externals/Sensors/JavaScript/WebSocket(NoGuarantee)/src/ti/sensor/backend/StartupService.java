package ti.sensor.backend;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by denis on 16.06.14.
 */
public class StartupService {
    public static boolean STARTUP = false;
    public static final int PORT = 8088;
    public static void main(String[] args) throws IOException {
        System.getProperties().setProperty("line.separator", "\r\n");
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(PORT);
            while (true) {
                new Thread(new WSConnector(ss.accept())).start();
            }
        }finally {
            if(ss!=null){
                ss.close();
            }
        }


//        new Thread(new WSServiceThread()).start();
//            ServerSocket ss = new ServerSocket(8081);
//            Socket s = ss.accept();
//            RequestExecuter r = new RequestExecuter(s);
//            new Thread(r).start();
//            ss.close();
    }

//    static class WSServiceThread implements Runnable {
//
//        @Override
//        public void run() {
//            System.getProperties().setProperty("line.separator", "\r\n");
//            try {
//                    ServerSocket wss = new ServerSocket(8082);
//                    Socket ws = wss.accept();
//                    WSConnector wsc = new WSConnector(ws);
//                    new Thread(wsc).start();
//                    wss.close();
//            }catch (IOException e){
//                System.out.println("WSServiceThread не удалось установить сетевое подключение");
//                e.printStackTrace();
//            }
//        }
//    }
}
