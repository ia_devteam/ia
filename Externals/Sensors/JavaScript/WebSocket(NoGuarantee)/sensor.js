ws = new WebSocket("ws://127.0.0.1:8088");

var WEB_SOC_OPEN = "0"

ws.onopen = function() {
	WEB_SOC_OPEN = "1"
};

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

while (WEB_SOC_OPEN != 1) {
	sleep(1000);
}

ws.onmessage = function (msg) {
    //TODO
}

ws.onerror = function (error) {
    //TODO
}

ws.onopen = function () {
    //TODO
}

var LENGTH_DATA_LENGTH = 8;
var buf = "";
var coefficent = 4;
var maxMessageLength = 4096 * coefficent;

function Sensor(param1) {
    buf += param1 + ";" + window.location.href + "\n";
    if (buf.length > maxMessageLength) {
        var dataLength = getDataLength(buf);
        ws.send(dataLength);
        ws.send(buf);
        buf = "";
    }
}

function getDataLength(msg) {
    var length = (msg.length + "").length;
    var result = "";
    for (var i = 0; i < LENGTH_DATA_LENGTH - length; i++) {
        result += "0";
    }
    return result + msg.length;
}
