#include <pthread.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/stat.h>
#include <algorithm>
#include <errno.h>
#include <fstream>
#include <iostream>

using namespace std;

__thread int threadId = 0;

void DinGo(int sensId)
{
	if (threadId == 0)
	      threadId = syscall(SYS_gettid);
	
	char fname[50];
	int n = sprintf(fname, "/var/tmp/iaJS_Csensor_t%06d.log", threadId);
	FILE *file_var = fopen(fname, "a+b");
	fwrite(&sensId, sizeof (int), 1, file_var);
	fclose(file_var);
}

