package java.lang;

import java.io.FileWriter;
import java.io.BufferedWriter;

public final class IAJavaSensorWriter {
	
	private IAJavaSensorWriter() {};
	
	private static native void writeSensor(int id);
	
	public static void Write(int num)
	{
		writeSensor(num);
	}
	
	public static void ____Din_Go(String n, int num)
	{
		Write(num);
	}
	
	public static void Din_Go(String n, int num)
	{
		Write(num);
	}
	
	public static void Din_Go(int num)
	{
		Write(num);
	}	
}