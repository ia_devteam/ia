#ifdef __IA_SENSOR_EXPORTS
#define SENSOR_API __declspec(dllexport)
#else
#define SENSOR_API __declspec(dllimport)
#endif

#ifndef IA_CPP_WIN_SENSOR
#define IA_CPP_WIN_SENSOR


#ifdef __X64__
#define SENSORLIBTEXT "sensor_64.lib"
#else
#define SENSORLIBTEXT "sensor_32.lib"
#endif

#ifndef __IA_SENSOR_EXPORTS
#pragma comment(lib, SENSORLIBTEXT)
#pragma warning(disable: 4189)
#endif

#ifdef __cplusplus
#define EXTERNC	extern "C" 
#else
#define EXTERNC
#endif

EXTERNC SENSOR_API int Din_Go(unsigned int RecivedID);

#endif
