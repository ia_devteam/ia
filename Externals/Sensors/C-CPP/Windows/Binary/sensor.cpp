#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>

#define __IA_SENSOR_EXPORTS
#include "sensor.h"

#define TLS_FAILED 0xFFFFFFFF

int const DimentionMasID = 16*1024;//количество элементов накапливаемого буфера

DWORD dwTlsIndex = TLS_FAILED; //индекс локальной памяти процесса


//структура локальной памяти отдельного потока
struct ThreadLocalMemory {
 int gt_Index_MasID; //всегда следующий индекс в массиве
 int gt_MasID_ForAll[DimentionMasID];//массив для накопления инфо
};

ThreadLocalMemory* CheckThreadLocalSlot(){
	//данная функция введена для случая не-динамической
	//загрузки библиотеки
	ThreadLocalMemory *TLM = (ThreadLocalMemory *)TlsGetValue(dwTlsIndex);
	if (TLM == NULL){//динамическое выделение памяти по мере необходимости
		TLM = new ThreadLocalMemory;
		if (TLM != NULL){
			TLM->gt_Index_MasID = 0;//начать новое заполнение буфера
			TlsSetValue(dwTlsIndex, TLM);
		}
//		else
//			MessageBox(NULL,"Unable to Allocate Thread Local Storage","SensorLib",MB_ICONERROR);
	}
	return TLM;
}

void Flush(bool WorkDone)
{
	DWORD err = GetLastError();
	ThreadLocalMemory *TLM = (ThreadLocalMemory *)TlsGetValue(dwTlsIndex);
	if (TLM == NULL) return;//нет смысла выполнять формирование внешнего потока

	DWORD threadID = GetCurrentThreadId();

	FILE *fptr;
	char StrID[260] = "C:\\";
	char Str[35];

	itoa(threadID, Str, 10);
	strcat(StrID, Str);
	strcat(StrID, ".IA_LOG");

	fptr = fopen(StrID ,"ab");//данные ведь двоичные
	if (fptr){
		if (TLM->gt_Index_MasID > 0)
			fwrite(&TLM->gt_MasID_ForAll[0], sizeof(int), TLM->gt_Index_MasID, fptr);

		if (WorkDone)
		{	
			int i = -1;
			fwrite(&i, sizeof(int), 1, fptr);
		}
		fclose(fptr);
	}
	else {
		char ErrMsg[300] = "File ";
		strcat(ErrMsg, StrID);
		strcat(ErrMsg, " not accessible");
		MessageBoxA(NULL,ErrMsg,"SensorLib",MB_ICONERROR);
	}
	SetLastError(err);
}

extern "C"{

SENSOR_API int Din_Go(unsigned int RecivedID)
{
	DWORD err = GetLastError();
	ThreadLocalMemory *TLM = CheckThreadLocalSlot();

	TLM->gt_MasID_ForAll[TLM->gt_Index_MasID] = RecivedID;//разместить идентификатор вызова
	TLM->gt_Index_MasID++;		//подготовить следующее размещение
	if (TLM->gt_Index_MasID >= DimentionMasID)//необходимо выполнить сохранение буфера
	{
		Flush(false);
		TLM->gt_Index_MasID = 0;//начать новое заполнение буфера
	}
	SetLastError(err);
	return 0;
}
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fwdreason, LPVOID lpvReserved)
{
	ThreadLocalMemory *TLM;

    switch (fwdreason)
    {
    case DLL_PROCESS_ATTACH:
		{
        if ((dwTlsIndex = TlsAlloc()) == TLS_FAILED) 
            return false; 
		break;//выделение рабочей памяти потока оставим до
			  //реальной потребности в этом
		}
	case DLL_THREAD_ATTACH:
		{
		break;
		}
	case DLL_THREAD_DETACH:
		{
		//сбросим накопленный буфер инфо и освободим ресурсы
		Flush(true);
		TLM = (ThreadLocalMemory *) TlsGetValue(dwTlsIndex);
		if (TLM)	delete TLM;
        break;
		}
	case DLL_PROCESS_DETACH:
		{
		//сбросим накопленный буфер инфо и освободим ресурсы
		Flush(true);
		TLM = (ThreadLocalMemory *)TlsGetValue(dwTlsIndex);
		if (TLM)	delete TLM;
		TlsFree(dwTlsIndex);
		break;
		}
    }
    return true;
}
//---------------------------------------------------------------------------
