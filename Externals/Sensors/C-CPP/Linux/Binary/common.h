#ifndef IACLINUXSENSORCOMMON
#define IACLINUXSENSORCOMMON

#include <mqueue.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#define QUEUE_NAME  "/IACLINUXSENSOR"

#define HEIGHT 30 //max process count
#define WIDTH 10240 //max sensors before flush
#define MAX_WAITING_SENS 50
#define CHECK(x) \
    do { \
        if (!(x)) { \
            fprintf(stderr, "%s:%d: ", __func__, __LINE__); \
            perror(#x); \
            exit(-1); \
        } \
    } while (0) \
	
struct sending {
	pid_t tid;
	int sensId;	
	int sId;
};

#define MAX_SIZE 129 // pthread_t + int //min 128 :(

#endif /* #ifndef IACLINUXSENSORCOMMON */

