#define IACLINUXSENSORBUILD
#define IACLINUXSENSORCLIENT
#include <pthread.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <errno.h>

#include "common.h"
#include "sensor.h"

__thread int i;

void DinGo(int sensId, int sId)
{	
#ifndef NULLIMPL
	long OLDERRNO = errno;
	mqd_t mq = mq_open(QUEUE_NAME, O_WRONLY);		
	CHECK((mqd_t)-1 != mq);

	sending s;
	//pthread_self: Thread IDs are only guaranteed to be unique within a process. A thread ID may be reused after a terminated thread has been joined, or a detached thread has terminated.
	if (i == 0)
		i = syscall(SYS_gettid);
	s.tid = i;
	s.sensId = sensId;
	s.sId = sId;
	mq_send(mq, (const char *) &s, sizeof(s), 0);

	mq_close(mq);
	errno = OLDERRNO;
#else
	i++;
#endif	
}

void Din_Go(int sensId, int sId)
{
	DinGo(sensId, sId);
}

