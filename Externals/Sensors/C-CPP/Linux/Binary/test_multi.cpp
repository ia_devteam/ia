#include <pthread.h>     /* pthread functions and data structures */

#include "sensor.h"

void* do_loop(void* data) {
    int me = *((int*)data);

    for (int i=1; i<100000; i++) {
	DinGo(me*100 + i, me);
    }
}

int arr[30];

int main(int argc, char* argv[]) {

for (int i = 1; i <= 30; i++) {
	arr[i] = i;
	pthread_t  p_thread;       
	pthread_create(&p_thread, NULL, do_loop, (void*)&(arr[i]));
}    
    pthread_exit(NULL);
    /* NOT REACHED */
    return 0;
}
