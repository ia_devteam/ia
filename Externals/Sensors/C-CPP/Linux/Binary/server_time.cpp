#define IACLINUXSENSORBUILD
#define IACLINUXSENSORSERVER

#include <algorithm>
#include <errno.h>
#include <fstream>
#include <iostream>
#include <signal.h>
#include <vector>
#include <time.h>  //Для clock_gettime
#include "common.h"

using namespace std;
vector<vector<int> > data_vector;
vector<pid_t> pointer_vector;
bool stop = false;
int sId = -1;

void interrupt_handler(int signo);
void flush(pid_t id, vector<int> *data, bool endOfThread);

int main(int argc, char **argv)
{
   mqd_t mq;
   struct mq_attr attr;
   struct timespec tm;
   struct sending s;   

   char buffer[MAX_SIZE];

   pointer_vector.reserve(HEIGHT);

   signal(SIGUSR1, interrupt_handler);
   signal(SIGINT, interrupt_handler);

   //initialize the queue attributes
   attr.mq_flags = 0;
   attr.mq_maxmsg = 10;
   attr.mq_msgsize = 128;
   attr.mq_curmsgs = 0;

   //create the message queue 
   mode_t omask;
   omask = umask(0777);
   mq = mq_open(QUEUE_NAME, O_CREAT | O_RDONLY, 0777, &attr);
   umask(omask);
   CHECK((mqd_t)-1 != mq);   

   fchmod(mq, 0777);
   clock_gettime(CLOCK_REALTIME, &tm);
   tm.tv_sec += 1; //1sec listening period
         
   do {
      ssize_t bytes_read;
      //receive the message or stop signal
      do {
         
         bytes_read = mq_timedreceive(mq, buffer, MAX_SIZE, NULL, &tm);
         if (bytes_read != -1) {
            break;
         }
         clock_gettime(CLOCK_REALTIME, &tm);
         tm.tv_sec += 1; //1sec listening period
      } while (!stop);
      
      if (stop) break;
#ifndef NULLIMPL
      memcpy(&s, buffer, sizeof(s));
      memset(buffer,0,sizeof(s));

      if (sId == -1)
      {
         sId = s.sId;
      }
      //Структуры для сохранения определенного времени
	  struct timespec mt1, mt2; 
		//Переменная для расчета дельты времени
		long int tt;      
   
   //Определяем текущее время
   clock_gettime (CLOCK_REALTIME, &mt1);
   
   //Выводим определенное время на экран консоли
   printf (“Секунды: %ld\n”,mt1.tv_sec);
   printf (“Наносекунды: %ld\n”,mt1.tv_nsec);
	  fprintf(stdout,"%10d %ld.%ld\n",s.sensId ,mt1.tv_sec , mt1.tv_nsec);	  
#endif
   } while (!stop);

   //cleanup
   CHECK((mqd_t)-1 != mq_close(mq));
   CHECK((mqd_t)-1 != mq_unlink(QUEUE_NAME));
#ifndef NULLIMPL
   //dumping the values
   for (int i = 0; i < pointer_vector.size(); i++)
   {
      flush(pointer_vector[i], &data_vector[i], true);
   }
#endif
   return 0;
}

#ifndef NULLIMPL
void flush(pid_t id, vector<int> *data, bool endOfThread)
{
   char fname[sizeof(pid_t)+17];
   sprintf(fname, "%05d_sensor%d.log", sId, id);
   FILE *file_var = fopen(fname, "a+b");
   int end_label = -1;

   //write all vector at once, atomically. Must be fast when compared with one int at time writing.
   if (data->size() > 0) {
      fwrite(&data->at(0), sizeof (int) * data->size(), 1, file_var);
   }

   if (endOfThread) {
      fwrite(&end_label, sizeof(int), 1, file_var);
   }
   //if not endOfThread - then work not done and need to clean buffer vector.
   data->clear();
   fclose(file_var);
}
#endif

void interrupt_handler(int signo)
{
   cout << "GOT SIGNAL!" << endl;
   stop = true;
}
