#!/bin/bash
echo -n Removing old binaries...\ 
rm -f server.out libsensor.a sensor.o test.out && echo Done. || echo Failed.
echo

echo -n Build server...\ 
g++ server.cpp -lrt -o server.out && echo Build success. || echo Build failed.
echo

echo -n Build client static lib...\ 
g++ -c sensor.cpp -o sensor.o && ar rcs libsensor.a sensor.o  && echo Build success. || echo Build failed.
echo

echo -n Build test...\ 
g++ test.cpp sensor.o -lrt -o test.out  && echo Build success. || echo Build failed.
echo

echo -n Build multithread test...\ 
g++ test_multi.cpp sensor.o -lrt -o test_multi.out  && echo Build success. || echo Build failed. 
echo

echo -n Build client shared lib...\ 
rm -f sensor.o && g++ -fPIC -shared -lrt sensor.cpp -o libsensor.so && echo Build success. || echo Build failed.
echo

echo -n Build test...\ 
g++ test.cpp -lsensor -lrt -o testShared.out  && echo Build success. || echo Build failed.
echo

echo -n Build multithread test...\ 
g++ test_multi.cpp -lsensor -lrt -o testShared_multi.out  && echo Build success. || echo Build failed.
echo


