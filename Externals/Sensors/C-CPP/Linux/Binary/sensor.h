#ifndef IACLINUXSENSOR
#define IACLINUXSENSOR

#ifdef __cplusplus
#define IASENSOR_EXTERNC extern "C"
#define __IAENUM__ 
#define __IASTRUCT__
#define __IAUNION__
#else
#define IASENSOR_EXTERNC
#define __IAENUM__ enum
#define __IASTRUCT__ struct
#define __IAUNION__ union
#endif

//https://www.gnu.org/software/gnulib/manual/html_node/Exported-Symbols-of-Shared-Libraries.html
#if HAVE_VISIBILITY
#define IASENSOR_VISIBILITY_EXPORTED __attribute__((__visibility__("default")))
#else
#define IASENSOR_VISIBILITY_EXPORTED
#endif

IASENSOR_EXTERNC IASENSOR_VISIBILITY_EXPORTED void DinGo(int sensId, int sId);
IASENSOR_EXTERNC IASENSOR_VISIBILITY_EXPORTED void Din_Go(int sensId, int sId);

#endif /* #ifndef IACLINUXSENSOR */

