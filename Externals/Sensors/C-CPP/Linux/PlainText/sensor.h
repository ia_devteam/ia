#ifndef IACLINUXSENSOR
#define IACLINUXSENSOR

#ifndef _GNU_SOURCE
#define IACLINUXSENSOR_UNDEFINE_GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <signal.h>

#include <stdio.h>
#include <errno.h>

#ifdef __cplusplus
#define IASENSOR_EXTERNC extern "C"
#define __IAENUM__ 
#define __IASTRUCT__
#define __IAUNION__
#else
#define IASENSOR_EXTERNC
#define __IAENUM__ enum
#define __IASTRUCT__ struct
#define __IAUNION__ union
#endif

#define Din_Go(sensId, sId)\
do { \
	int errsv = errno;\
	char filename[128];\
	static pid_t t_pid = (pid_t)0;\
	if (!t_pid)\
		t_pid = (pid_t) syscall (SYS_gettid);\
	sprintf(filename, "/var/tmp/%05u__%05u__PROBE.txt", sId, t_pid);\
	FILE *__PROBE__FILE = fopen(filename,"a+");\
	if (__PROBE__FILE)\
	{\
		fprintf(__PROBE__FILE, "%u\n",sensId);\
		fclose(__PROBE__FILE);\
	}\
	errno = errsv;\
} while(0)

#ifdef IACLINUXSENSOR_UNDEFINE_GNU_SOURCE	
#undef _GNU_SOURCE
#undef IACLINUXSENSOR_UNDEFINE_GNU_SOURCE
#endif

#endif /* #ifndef IACLINUXSENSOR */

