# ������ ������������ ��� ����������� AssemblyVersion �� ���� ������ ������� �� ������� ������ + ����� ������� �������, �� ������� �������
# ������� ApplyVersionToAssemblies.ps1 �� https://curah.microsoft.com/8047/run-scripts-in-your-team-foundation-build-process
[CmdletBinding()]

param([switch]$Disable)
if ($PSBoundParameters.ContainsKey('Disable'))
{
	exit 0
}

if(-not $Env:TF_BUILD -and -not ($Env:TF_BUILD_SOURCESDIRECTORY -and $Env:TF_BUILD_BUILDNUMBER -and $Env:TF_BUILD_SOURCEGETVERSION))
{
	Write-Error "You must set the following environment variables"
	Write-Error "to test this script interactively."
	Write-Host '$Env:TF_BUILD_SOURCESDIRECTORY - For example, enter something like:'
	Write-Host '$Env:TF_BUILD_SOURCESDIRECTORY = "C:\code\FabrikamTFVC\HelloWorld"'
	Write-Host '$Env:TF_BUILD_BUILDNUMBER - For example, enter something like:'
	Write-Host '$Env:TF_BUILD_BUILDNUMBER = "Build HelloWorld_0000.00.00.0"'
	Write-Host '$Env:TF_BUILD_SOURCEGETVERSION - For example, enter something like:'
	Write-Host '$Env:TF_BUILD_SOURCEGETVERSION = "12345"'
	exit 1
}

$VersionRegex = "\d+\.\d+\.\d+\.\d+"
$VersionData = [regex]::matches($Env:TF_BUILD_BUILDNUMBER,$VersionRegex)
switch($VersionData.Count)
{
   0		
      { 
         Write-Error "Could not find version number data in TF_BUILD_BUILDNUMBER."
         exit 1
      }
   1 {}
}

$NewVersion = $VersionData[0].ToString()
$NewVersion = $NewVersion.Split(".") | select -First 3 
$NewVersion = [string]::Join('.', $NewVersion) + "." + $Env:TF_BUILD_SOURCEGETVERSION.Trim("C")

$onlysourcespath = Join-Path $Env:TF_BUILD_SOURCESDIRECTORY 'Sources'
gci $onlysourcespath -recurse | %{attrib -r $_.FullName}
# Apply the version to the assembly property files
$files =  gci $onlysourcespath -Recurse -include "Properties" | 
	?{ $_.PSIsContainer } | 
	foreach { gci -Path $_.FullName -Recurse -include AssemblyInfo.cs }
	
foreach ($file in $files) 
{			
	$filecontent = Get-Content($file)
#	if ($file.IsReadOnly -eq $true)  
#	{ $file.IsReadOnly = $false }
	
	$filecontent -replace $VersionRegex, $NewVersion | Out-File $file -Encoding 'Default'
}

