# ������ ������������ ��� ���������� ������������ � ������� ������ ������������� � ������������ ����������.

#
# �����:
# Disable - ������������ ������
# NoInstallers - ��������� ������ �������������
#  CertifiedSpecialBuildList.txt - ���� �� ������� ������������� ����� ��������� ��������, ������� ���� �������� �������� ��� ������ Certified. ���� ��������� ������������ �������� Sources.
# SignBinaries - ��������� ������������ �������� ������
#  ExternalToUnsign.txt - ���� �� ������� ��� �������� ������ � �������� ������ (��-���������, Binaries), ���������� �� �������� �������, ������� ����������� ������ ��� �� ����.
# SignCert - ������ � ������������� ������ ����� ����� ��� ������������. ���� ��������� ������������ �������� Sources. ��-���������, RNTCert.pfx


[CmdletBinding()]

param(
[switch]$Disable,
[switch]$NoInstallers,
[switch]$SignBinaries,
[string]$SignCert='RNTCert.pfx'
)
if ($PSBoundParameters.ContainsKey('Disable'))
{
	exit 0
}

if(-not $Env:TF_BUILD -and -not ($Env:TF_BUILD_SOURCESDIRECTORY))
{
	Write-Error "You must set the following environment variables"
	Write-Error "to test this script interactively."
	Write-Host '$Env:TF_BUILD_SOURCESDIRECTORY - For example, enter something like:'
	Write-Host '$Env:TF_BUILD_SOURCESDIRECTORY = "C:\code\FabrikamTFVC\HelloWorld"'	
	exit 2
}

#
#
#	FUNCS
#
#

Function SIGN
{
	if ($SignBinaries)
	{
		$notSignFiles = gci $sourcedirs["Externals"] -include ("*.exe", "*.dll") -Recurse | %{$_.Name}
		$notSignFiles += "Test*.dll"
		$notSignFiles += Get-Content $(Join-Path $sourcedirs["Scripts"] 'ExternalToUnsign.txt')
		
		$SIGNTOOL = "c:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\Bin\signtool.exe"
		gci $sourcedirs['Binaries'] -include ("*.exe", "*.dll") -exclude $notSignFiles -recurse | %{& $SIGNTOOL sign /f $(Join-Path $sourcedirs['Sources'] $SignCert) /p "iamme" /n "RNT ZAO" $_.FullName}
	}
}

#first initialize dirs
. "$(Join-Path $Env:TF_BUILD_SOURCESDIRECTORY 'Externals\_BuildScripts\InitializeNames.ps1')"

#call SIGN func
SIGN

#
# dirs creating
#

$bindirs.Values | %{ md -f $_ } | Out-Null

#
# copy builded files
#

robocopy $sourcedirs['Binaries'] $bindirs['FullPlus'] /s | Out-Null

#if make installers - do the job
if ( !$NoInstallers)
{
	#full
	cd $(Join-Path $Env:TF_BUILD_SOURCESDIRECTORY "Sources\Installers\ClientInstaller")
	& c:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe /t:build /p:Configuration=Full | Out-Null
	cd $(Join-Path $Env:TF_BUILD_SOURCESDIRECTORY "Sources\Installers\ServerInstaller")
	& c:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe /t:build /p:Configuration=Full | Out-Null

	#certified
	$projectsToRebuild = Get-Content $(Join-Path $sourcedirs["Scripts"] 'CertifiedSpecialBuildList.txt')
	
	foreach ($projPath in $projectsToRebuild)
	{
		cd $(Join-Path $sourcedirs["Sources"] $projPath)
		& c:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe /t:build /p:Configuration=Release_Certified | Out-Null
	}
	
	if ($projectsToRebuild.Count -gt 0) 
	{
		#call SIGN to sign freshly rebuilded binaries
		SIGN
	}
	
	cd $(Join-Path $Env:TF_BUILD_SOURCESDIRECTORY "Sources\Installers\ClientInstaller")
	& c:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe /t:build /p:Configuration=Certified | Out-Null
	cd $(Join-Path $Env:TF_BUILD_SOURCESDIRECTORY "Sources\Installers\ServerInstaller")
	& c:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe /t:build /p:Configuration=Certified | Out-Null
	
	#second initialize dirs in way when installers are created
	. "$(Join-Path $Env:TF_BUILD_SOURCESDIRECTORY 'Externals\_BuildScripts\InitializeNames.ps1')"
	
	$installers.Keys | %{robocopy $sourcedirs.$_ $bindirs.$_ /s | Out-Null}
}

