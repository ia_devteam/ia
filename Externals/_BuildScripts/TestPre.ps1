# C����� ������������ ��� ������������ �������� �������, ��������� �� ����������� ���������� (�������� ��������� �������) � �������� ���������� (���� �� ����).
# C����� ������ ��� ������� ���������� ������������ � ����� ��������� �������������� ������ ��� ���������� � �������� ���������.
#
# ���� ���� �������� ������������ msi - ������������ �� ���������� �� ��������� ��������, 
# ����������� ���� �������� ������ � �� ��������������� (� ��� ����������� ����� �������� ������������).
# 
#


[CmdletBinding()]
Param
(
[switch]$Disable
)
if ($PSBoundParameters.ContainsKey('Disable'))
{
#	Write-Verbose "Script disabled; no actions will be taken on the files."
exit 0
}
$FileTypes = @("*.exe", "*.dll", "config.xml" , "*.dot", "*.egt", "*.msi", "*.config", "*.ico", "Data", "Grammar", "GraphViz", "config6")

# load initialized $installers, $bindirs, $sourcedirs, $persistTestDir vars
. "$(Join-Path $Env:TF_BUILD_SOURCESDIRECTORY 'Externals\_BuildScripts\InitializeNames.ps1')"

#
# dirs creating
#

$bindirs.Values | %{ md -f $_ } | Out-Null

#unpack installers
# pause need to be for msiexec can run normally. If pause abscent then randomly glitches may appear
$installers.Keys | %{&'msiexec' /a "$($installers.$_.FullMsiName)" /qn targetdir="$($bindirs[$installers.$_.UnpackedDirName])"; Start-Sleep -s 10}

foreach ($iname in $installers.Keys) 
{
	gci $bindirs[$installers.$iname.UnpackedDirName] -include $FileTypes -recurse | mi -Destination $bindirs[$installers.$iname.UnpackedDirName] -force | Out-Null
}

#
# remove unneeded files
#

gci $bindirs['FullPlus'] -exclude $FileTypes | ri -force -recurse

$testAllDlls = gci (Join-Path $bindirs['FullPlus'] 'test*.dll')

#
# copy and rename test dlls
#

foreach ($iname in $installers.Keys) 
{
	$temp = @(gci (Join-Path $bindirs[$installers.$iname.UnpackedDirName] '*.dll') | %{ $('Test{0}' -f $_.Name)})
	if ($temp.Count -eq 0) {continue}
	$testDllsToThisInstall = @($testAllDlls | ?{$temp -contains $_.Name})
	$testDllsToThisInstall | %{cp -Path $_.FullName -Destination (Join-Path $bindirs[$installers.$iname.UnpackedDirName] ($('{0}_{1}' -f $iname,$_.Name)))} 

	cp -Path $(Join-Path $bindirs['FullPlus'] 'TestUtils.dll') -Destination $bindirs[$installers.$iname.UnpackedDirName]
}


# remove all old tests
if (Test-Path $bindirs['Tests'])
{
	ri $bindirs['Tests'] -Force -Recurse
}

# move test materials from sources to binaries
mi -Path $sourcedirs['Tests'] -Destination $Env:TF_BUILD_BINARIESDIRECTORY -force
gci $bindirs['Tests'] -Recurse | %{ if ($_.PSObject.Properties["IsReadOnly"]) {$_.IsReadOnly = $false}}

# create temp dirs
md (Join-Path $bindirs['Tests'] 'RunTemp') -force
md (Join-Path $bindirs['Tests'] 'Cache') -force

#copy some files for TestProjectValidation pass
md (Join-Path $Env:TF_BUILD_BINARIESDIRECTORY 'Sources') -Force | Out-Null
robocopy $sourcedirs['Sources'] (Join-Path $Env:TF_BUILD_BINARIESDIRECTORY 'Sources') *.sln *.csproj *.wixproj *.vcxproj /s | Out-Null
md (Join-Path $Env:TF_BUILD_BINARIESDIRECTORY 'Binaries') -Force | Out-Null

#
# prepairing done. Next must be tests
#