# Вспомогательный скрипт инициализации переменных, используемых другими скриптами. 
# Написан для конкретной реализации структуры каталогов проекта IA и особенностей сборки в условиях TFS.
#

[CmdletBinding()]
Param
(
[switch]$Disable
)
if ($PSBoundParameters.ContainsKey('Disable'))
{
#	Write-Verbose "Script disabled; no actions will be taken on the files."
exit 0
}

#
# initialiZation
#

#hashtable of installers with dirs
$installers = @{}

# $bindirs is points to bin folder from which TFS copies files to pub folder on share
$bindirs = @{
"FullPlus" = Join-Path $Env:TF_BUILD_BINARIESDIRECTORY 'FullPlus';
"Tests" = Join-Path $Env:TF_BUILD_BINARIESDIRECTORY 'Tests'
}

# $sourcedirs is points to src folder in which TFS make MSBUILD of project.
$sourcedirs = @{
"Binaries" = Join-Path $Env:TF_BUILD_SOURCESDIRECTORY 'Binaries';
"Sources" = Join-Path $Env:TF_BUILD_SOURCESDIRECTORY 'Sources';
"Tests" = Join-Path $Env:TF_BUILD_SOURCESDIRECTORY 'Tests';
"Externals" = Join-Path $Env:TF_BUILD_SOURCESDIRECTORY 'Externals';
"Scripts" = Join-Path $Env:TF_BUILD_SOURCESDIRECTORY 'Externals\_BuildScripts'
}

# $persistTestDir is directory to/from tests move after/before test run to prevent extensively copying. Need to update on every build with robocopy with current version
$persistTestDirParentDir = [io.Path]::GetFullPath((Join-Path $ENV:TF_BUILD_BUILDDIRECTORY '..'))
$persistTestDir = Join-Path $persistTestDirParentDir 'Tests'

foreach ($installer in (gci (Join-Path $Env:TF_BUILD_SOURCESDIRECTORY 'Installers') -include '*Installer*.msi' -recurse))
{	
	if ($installer.DirectoryName.EndsWith("ru-ru")) #WiX
	{
		$installername = [io.path]::GetFileName($([io.path]::GetDirectoryName($installer.DirectoryName)))
	}
	else #VS add-in
	{
		$installername = [io.path]::GetFileNameWithoutExtension($installer.FullName)
	}

	if ( $installer.Name -like "Server*" )
	{
		$installername = "Server" + $installername
	}
	else
	{
		$installername = "Client" + $installername
	}
	
	$dummyname = "Hash4$installername"
	New-Variable -Name $dummyname -Value @{}
	$installers.Add($installername, (iex "`$$dummyname"))
	$installers.$installername.FullMsiName = $installer.FullName
	$installers.$installername.MsiName = $installer.Name
	$installers.$installername.SourceDir = $installer.DirectoryName
	$installers.$installername.UnpackedDirName = ('{0}_unpacked' -f $installername)
}

$installers.Keys | %{ $bindirs.Add($_, (Join-Path $Env:TF_BUILD_BINARIESDIRECTORY $_)) }
$installers.Keys | %{ $bindirs.Add($installers.$_.UnpackedDirName, (Join-Path $Env:TF_BUILD_BINARIESDIRECTORY $installers.$_.UnpackedDirName)) }
$installers.Keys | %{ $sourcedirs.Add($_, $installers.$_.SourceDir) }
