# Скрипт предназначен для удаления тестовых материалов из каталогов выкладки TFS.
#
#

[CmdletBinding()]
Param
(
[switch]$Disable
)
if ($PSBoundParameters.ContainsKey('Disable'))
{
#	Write-Verbose "Script disabled; no actions will be taken on the files."
exit 0
}

. "$(Join-Path $Env:TF_BUILD_SOURCESDIRECTORY 'Externals\_BuildScripts\InitializeNames.ps1')"

#
# remove temp dirs
#

$installers.Keys | %{ri $bindirs[$installers.$_.UnpackedDirName] -Recurse -Force}

# at the end of tests 
if (Test-Path $persistTestDir)
{
	if (Test-Path (Join-Path $persistTestDir 'RunTemp'))
	{
		ri (Join-Path $persistTestDir 'RunTemp') -Force -Recurse
	}
	
	if (Test-Path (Join-Path $persistTestDir 'Cache'))
	{
		ri (Join-Path $persistTestDir 'Cache') -Force -Recurse
	}
}
else
{
	md $persistTestDir -force
}
# preserve last failed tests for examining

mi -Path (Join-Path $bindirs['Tests'] 'RunTemp') -Destination $persistTestDir -Force
mi -Path (Join-Path $bindirs['Tests'] 'Cache') -Destination $persistTestDir -Force
# remove test materials from binaries - no need to send them to share
ri $bindirs['Tests'] -force -Recurse

#remove materials for TestProjectsValidation
ri (Join-Path $Env:TF_BUILD_BINARIESDIRECTORY 'Sources') -force -Recurse
ri (Join-Path $Env:TF_BUILD_BINARIESDIRECTORY 'Binaries') -force -Recurse

gci $Env:TF_BUILD_BINARIESDIRECTORY -include 'test*.*' -recurse | ri -force