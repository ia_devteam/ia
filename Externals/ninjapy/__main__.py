import argparse
import os
import errno
import ntpath
from pyparser import *
from xmlgenerator import *
import traceback


def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


file_array = []


def parseFile(file_path, prefix, new_prefix, s_f_n, s_c, level, fixed_identifier, module_name):
    try:
        global current_file
        global file_array

        current_file = file_path.replace(prefix, "", 1)
        file_array.append(current_file)

        file_contents = open(file_path, 'r').read()
        ast_tree = ast.parse(file_contents)
        ast_tree = parse_ast(ast_tree, current_file, s_f_n, s_c, level, fixed_identifier)

        new_file = file_path.replace(prefix, new_prefix, 1)
        if not os.path.exists(os.path.dirname(new_file)):
            try:
                os.makedirs(os.path.dirname(new_file))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        output_file = open(new_file, 'w')
        source_to_write = astor.to_source(ast_tree)
        future_index = source_to_write.find('from __future__ import')
        sensor_string = 'from ' + module_name + ' import ' + s_f_n + '\n\n\n'
        if future_index != -1:
            index = source_to_write.find('\n', future_index) + 1
            source = source_to_write[:index] + sensor_string + source_to_write[index:]
        else:
            source = sensor_string + source_to_write
        output_file.write(source)
    except Exception as e:
        print 'Got exception while parsing ' + file_path + ' ' + traceback.format_exc() + '\n\r'


parser = argparse.ArgumentParser(description='Description of your program')
parser.add_argument('-f', '--file-list', help='List of files', required=True)
parser.add_argument('-s', '--source-prefix', help='Source prefix', required=True)
parser.add_argument('-o', '--output-prefix', help='Output prefix', required=True)
parser.add_argument('-n', '--sensor-name', help='Sensor function name', required=True)
parser.add_argument('-i', '--initial-sensor-number', type=int, help='Initial sensor number', required=True)
parser.add_argument('-l', '--insertion-level', type=int, help='Insertion level', required=True)
parser.add_argument('-x', '--xml-file-path', help='XML file path', required=True)
parser.add_argument('-d', '--fixed-identifier', help='Fixed identifier', required=False)
parser.add_argument('-m', '--module-name', help='Sensor module name', required=False)
args = vars(parser.parse_args())

xml_file_name = args['xml_file_path']  # 'e:\Work\PY0129\output4\pyninja.xml'
py_list_file_name = args['file_list']  # 'e:\Work\PY0129\\filelist.txt'
source_prefix = args['source_prefix']  # 'e:\Work\PY0129\input\\'
lab_prefix = args['output_prefix']  # 'e:\Work\PY0129\output4\\'
sensor_function_name = args['sensor_name']  # 'SensorRrr'
start_sensor_number = args['initial_sensor_number']  # 10
insertion_level = args['insertion_level']
fixed_identifier = None
if args['fixed_identifier'] is not None:
    fixed_identifier = args['fixed_identifier']

module_name = 'sensor'
if args['module_name'] is not None:
    module_name = args['module_name']

current_file = ''

with open(py_list_file_name, "r") as myfile:
    data = myfile.readlines()
    for x in data:
        if x:
            parseFile(x.replace('\n', ''), source_prefix, lab_prefix, sensor_function_name, start_sensor_number - 1, insertion_level, fixed_identifier, module_name)

output_xml = generate_xml(file_array, xml_file_name)
print('Done')

