from ast_structure import *
import ast
import astor


class Scope:
    def __init__(self, scope_type, entry):
        self.scope_type = scope_type
        self.entry = entry

    def get_last_statement(self):
        start = self.entry
        while start.get_next_statement() is not None:
            start = start.get_next_statement()
        return start

    def get_scope_type(self):
        return self.scope_type

    def get_entry(self):
        return self.entry


def get_name_from_node(node):
    if type(node).__name__ == 'Attribute':
        return node.attr
    elif type(node).__name__ == 'Name':
        return node.id
    elif hasattr(node, 'func'):
        return get_name_from_node(node.func)
    else:
        for property, child in vars(node).iteritems():
            if type(child).__name__ == 'Attribute':
                return child.attr
            elif type(child).__name__ == 'Name':
                return child.id
        return None


def get_full_name_from_node(node):
    if type(node).__name__ == 'Attribute':
        ret = get_full_name_from_node(node.value)
        if not isinstance(ret, basestring):
            return ret + [node.attr]
        else:
            return [ret, node.attr]
    else:
        return node.id


sensor_function_name = None
sensor_counter = None
insertion_level = None
fixed_identifier = None

def GenerateFunctionCall():
    global sensor_counter
    global sensor_function_name
    global fixed_identifier
    print_func = ast.Name(sensor_function_name, ast.Load())
    sensor_counter += 1
    message = ast.Num(sensor_counter)
    if fixed_identifier is not None:
        fixed_identifier_node = ast.Num(fixed_identifier)
        expr = ast.Expr(ast.Call(print_func, [message, fixed_identifier_node], [], None, None))
    else:
        expr = ast.Expr(ast.Call(print_func, [message], [], None, None))
    expr.visited = True
    return expr

def GenerateAssignment(return_node):
    global sensor_counter
    ass_target = ast.Name('py_ass_parser' + str(sensor_counter), ast.Load())
    ass_value = return_node.value
    ass_node = ast.Assign([ass_target], ass_value)
    return_node.value = ass_target
    ass_node.visited = True
    return_node.visited = True
    return ass_node

scope_stack = []
declaration_stack = []
current_file = None
ignore_level = False


class WalkThatAst(astor.TreeWalk):
    def pre_FunctionDef(self):
        global current_file
        global scope_stack
        global ignore_level
        cur_node = self.cur_node

        function_type = 'function'
        if not hasattr(scope_stack[-1], 'get_entry_statement'):
            function_type = 'method'

        function_definition = FunctionDeclaration(cur_node.name, function_type, Location(cur_node.lineno or 1, cur_node.col_offset, current_file))
        first_statement = cur_node.body[0]
        entry_statement = EntryStatement(Location(first_statement.lineno or 1, first_statement.col_offset, current_file))
        function_definition.set_entry_statement(entry_statement)

        declaration_stack[-1].get_inner_functions().append(function_definition)
        declaration_stack.append(function_definition)
        scope_stack.append(Scope('statement', entry_statement))

        ignore_level = True
        for child in cur_node.body:
            if not hasattr(child, 'visited'):
                self.walk(child)
            child.visited = True

        last_statement = scope_stack[-1].get_last_statement()
        if not isinstance(last_statement, BreakStatement):
            statement = OperationalStatement(Location(cur_node.body[-1].lineno, cur_node.body[-1].col_offset, current_file))
            ignore_level = True
            cur_node.body.append(GenerateFunctionCall())
            statement.set_sensor(sensor_counter)
            last_statement.set_next_statement(statement)

        scope_stack.pop()
        declaration_stack.pop()
        return True

    def post_Module(self):
        global current_file
        global scope_stack
        cur_node = self.cur_node

        first_statement = scope_stack[0].get_entry()
        if first_statement.get_next_statement() is not None:
            # if there is something useful
            firstlineno = 1
            firstcolumnno = 0

            index_of_first = 0
            for child in cur_node.body:
                if type(child).__name__ != 'ImportFrom':
                    if not (type(child).__name__ == 'Expr' and hasattr(child, 'col_offset') and child.col_offset == -1):
                        break
                index_of_first += 1

            first_node = cur_node.body[index_of_first]
            if not (type(first_node).__name__ == 'Expr' and not hasattr(first_node, 'col_offset')):
                # if it's not a sensor
                statement = OperationalStatement(Location(firstlineno, firstcolumnno, current_file))
                cur_node.body.insert(index_of_first, GenerateFunctionCall())
                statement.set_sensor(sensor_counter)
                statement.set_next_statement(first_statement.get_next_statement())
                first_statement.set_next_statement(statement)

            lastlineno = 0
            lastcolumnno = 0

            if hasattr(cur_node.body[-1], 'lineno'):
                lastlineno = cur_node.body[-1].lineno
                lastcolumnno = cur_node.body[-1].col_offset

            exit_statement = OperationalStatement(Location(lastlineno, lastcolumnno or 1, current_file))
            last_statement = scope_stack[0].get_last_statement()
            cur_node.body.append(GenerateFunctionCall())
            exit_statement.set_sensor(sensor_counter)
            last_statement.set_next_statement(exit_statement)
        return True

    def pre_ClassDef(self):
        global current_file
        global scope_stack
        cur_node = self.cur_node

        class_definition = ClassDeclaration(cur_node.name, Location(cur_node.lineno, cur_node.col_offset, current_file))
        declaration_stack[-1].get_inner_classes().append(class_definition)
        declaration_stack.append(class_definition)
        scope_stack.append(Scope('none', None))

        for child in cur_node.body:
            if type(child).__name__ == 'FunctionDef' or type(child).__name__ == 'ClassDef':
                self.walk(child)
        scope_stack.pop()
        declaration_stack.pop()
        return True

    def pre_Return(self):
        return self.generic_pre_break_statement('return')

    def post_Return(self):
        return self.generic_post_statement()

    def pre_Delete(self):
        return self.generic_pre_statement()

    def post_Delete(self):
        return self.generic_post_statement()

    def pre_Assign(self):
        return self.generic_pre_statement()

    def post_Assign(self):
        return self.generic_post_statement()

    def pre_AugAssign(self):
        return self.generic_pre_statement()

    def post_AugAssign(self):
        return self.generic_post_statement()

    def pre_Print(self):
        return self.generic_pre_statement()

    def post_Print(self):
        return self.generic_post_statement()

    def pre_For(self):
        global current_file
        global scope_stack
        assert scope_stack[-1].get_scope_type() == 'statement'
        cur_node = self.cur_node

        inserted_sensor = paste_sensor(self)

        location = Location(cur_node.lineno, cur_node.col_offset, current_file)
        last_statement = scope_stack[-1].get_last_statement()
        statement = ForStatement(location)

        statement.set_sensor(inserted_sensor)

        last_statement.set_next_statement(statement)
        first_statement = cur_node.body[0]
        entry_statement = EntryStatement(Location(first_statement.lineno, first_statement.col_offset, current_file))
        statement.set_body(entry_statement)

        scope_stack.append(Scope('operation', statement.get_operation()))
        self.walk(cur_node.target)
        self.walk(cur_node.iter)
        scope_stack.pop()

        scope_stack.append(Scope('statement', entry_statement))
        for child in cur_node.body:
            if not hasattr(child, 'visited'):
                self.walk(child)
            child.visited = True
        scope_stack.pop()

        fix_bad_case(entry_statement, cur_node.body)

        if cur_node.orelse is not None and len(cur_node.orelse):
            first_statement = cur_node.orelse[0]
            entry_statement = EntryStatement(Location(first_statement.lineno, first_statement.col_offset, current_file))
            statement.set_else_statement(entry_statement)
            scope_stack.append(Scope('statement', entry_statement))
            for child in cur_node.orelse:
                if not hasattr(child, 'visited'):
                    self.walk(child)
                child.visited = True
            scope_stack.pop()

        return True

    def pre_While(self):
        global current_file
        global scope_stack
        assert scope_stack[-1].get_scope_type() == 'statement'
        cur_node = self.cur_node

        inserted_sensor = paste_sensor(self)

        location = Location(cur_node.lineno, cur_node.col_offset, current_file)
        last_statement = scope_stack[-1].get_last_statement()
        statement = WhileStatement(location)

        statement.set_sensor(inserted_sensor)

        last_statement.set_next_statement(statement)
        first_statement = cur_node.body[0]
        entry_statement = EntryStatement(Location(first_statement.lineno, first_statement.col_offset, current_file))
        statement.set_body(entry_statement)

        scope_stack.append(Scope('operation', statement.get_operation()))
        self.walk(cur_node.test)
        scope_stack.pop()

        scope_stack.append(Scope('statement', entry_statement))
        for child in cur_node.body:
            if not hasattr(child, 'visited'):
                self.walk(child)
            child.visited = True
        scope_stack.pop()

        fix_bad_case(entry_statement, cur_node.body)

        if cur_node.orelse is not None and len(cur_node.orelse):
            first_statement = cur_node.orelse[0]
            entry_statement = EntryStatement(Location(first_statement.lineno, first_statement.col_offset, current_file))
            statement.set_else_statement(entry_statement)
            scope_stack.append(Scope('statement', entry_statement))
            for child in cur_node.orelse:
                if not hasattr(child, 'visited'):
                    self.walk(child)
                child.visited = True
            scope_stack.pop()

        return True

    def pre_If(self):
        global current_file
        global scope_stack
        assert scope_stack[-1].get_scope_type() == 'statement'
        cur_node = self.cur_node

        inserted_sensor = paste_sensor(self)

        location = Location(cur_node.lineno, cur_node.col_offset, current_file)
        last_statement = scope_stack[-1].get_last_statement()
        statement = IfStatement(location)

        statement.set_sensor(inserted_sensor)

        last_statement.set_next_statement(statement)
        first_statement = cur_node.body[0]
        entry_statement = EntryStatement(Location(first_statement.lineno, first_statement.col_offset, current_file))
        statement.set_body(entry_statement)

        scope_stack.append(Scope('operation', statement.get_operation()))
        self.walk(cur_node.test)
        scope_stack.pop()

        scope_stack.append(Scope('statement', entry_statement))
        for child in cur_node.body:
            if not hasattr(child, 'visited'):
                self.walk(child)
            child.visited = True
        scope_stack.pop()

        fix_bad_case(entry_statement, cur_node.body)

        if cur_node.orelse is not None and len(cur_node.orelse):
            first_statement = cur_node.orelse[0]
            entry_statement = EntryStatement(Location(first_statement.lineno, first_statement.col_offset, current_file))
            statement.set_else_statement(entry_statement)
            scope_stack.append(Scope('statement', entry_statement))
            for child in cur_node.orelse:
                if not hasattr(child, 'visited'):
                    self.walk(child)
                child.visited = True
            scope_stack.pop()

            fix_bad_case(entry_statement, cur_node.orelse)

        return True

    def pre_With(self):
        return False

    def pre_Raise(self):
        return self.generic_pre_break_statement('raise')

    def post_Raise(self):
        return self.generic_post_statement()

    def pre_TryExcept(self):
        global current_file
        global scope_stack
        assert scope_stack[-1].get_scope_type() == 'statement'
        cur_node = self.cur_node

        inserted_sensor = paste_sensor(self)

        location = Location(cur_node.lineno, cur_node.col_offset, current_file)
        last_statement = scope_stack[-1].get_last_statement()
        statement = TryStatement(location)

        statement.set_sensor(inserted_sensor)

        last_statement.set_next_statement(statement)
        first_statement = cur_node.body[0]
        entry_statement = EntryStatement(Location(first_statement.lineno, first_statement.col_offset, current_file))
        statement.set_body(entry_statement)

        scope_stack.append(Scope('statement', entry_statement))
        for child in cur_node.body:
            if not hasattr(child, 'visited'):
                self.walk(child)
            child.visited = True
        scope_stack.pop()

        fix_bad_case(entry_statement, cur_node.body)

        for handler in cur_node.handlers:
            handler_entry_statement = EntryStatement(Location(handler.lineno, handler.col_offset, current_file))
            statement.get_except_statements().append(handler_entry_statement)
            scope_stack.append(Scope('statement', handler_entry_statement))
            for child in handler.body:
                if not hasattr(child, 'visited'):
                    self.walk(child)
                child.visited = True
            scope_stack.pop()
            fix_bad_case(handler_entry_statement, handler.body)

        if cur_node.orelse is not None and len(cur_node.orelse):
            first_statement = cur_node.orelse[0]
            entry_statement = EntryStatement(Location(first_statement.lineno, first_statement.col_offset, current_file))
            statement.set_else_statement(entry_statement)
            scope_stack.append(Scope('statement', entry_statement))
            for child in cur_node.orelse:
                if not hasattr(child, 'visited'):
                    self.walk(child)
                child.visited = True
            scope_stack.pop()
            fix_bad_case(entry_statement, cur_node.orelse)

        return True

    def pre_TryFinally(self):
        global current_file
        global scope_stack
        cur_node = self.cur_node

        if type(cur_node.body[0]).__name__ == 'TryExcept':
            self.walk(cur_node.body[0])
        else:
            inserted_sensor = paste_sensor(self)

            location = Location(cur_node.lineno, cur_node.col_offset, current_file)
            last_statement = scope_stack[-1].get_last_statement()
            statement = TryStatement(location)

            statement.set_sensor(inserted_sensor)

            last_statement.set_next_statement(statement)
            first_statement = cur_node.body[0]
            entry_statement = EntryStatement(
                Location(first_statement.lineno, first_statement.col_offset, current_file))
            statement.set_body(entry_statement)

            scope_stack.append(Scope('statement', entry_statement))
            for child in cur_node.body:
                if not hasattr(child, 'visited'):
                    self.walk(child)
                child.visited = True
            scope_stack.pop()

            fix_bad_case(entry_statement, cur_node.body)

        final_body = cur_node.finalbody[0]
        location = Location(final_body.lineno, final_body.col_offset, current_file)
        last_statement = scope_stack[-1].get_last_statement()
        entry_statement = EntryStatement(location)
        last_statement.set_finally_statement(entry_statement)

        scope_stack.append(Scope('statement', entry_statement))
        for child in cur_node.finalbody:
            if not hasattr(child, 'visited'):
                self.walk(child)
            child.visited = True
        scope_stack.pop()

        fix_bad_case(entry_statement, cur_node.finalbody)
        return True

    def pre_Assert(self):
        return self.generic_pre_statement()

    def post_Assert(self):
        return self.generic_post_statement()

    def pre_Exec(self):
        return self.generic_pre_statement()

    def post_Exec(self):
        return self.generic_post_statement()

    def pre_Expr(self):
        if self.cur_node.col_offset == -1:
            return True
        return self.generic_pre_statement()

    def post_Expr(self):
        return self.generic_post_statement()

    def pre_Pass(self):
        return self.generic_pre_statement()

    def post_Pass(self):
        return self.generic_post_statement()

    def pre_Break(self):
        return self.generic_pre_break_statement('break')

    def post_Break(self):
        return self.generic_post_statement()

    def pre_Continue(self):
        return self.generic_pre_break_statement('continue')

    def post_Continue(self):
        return self.generic_post_statement()

    def pre_Call(self):
        global current_file
        global scope_stack
        cur_node = self.cur_node

        node = cur_node

        if scope_stack[-1].get_scope_type() != 'statement':
            if type(node.func).__name__ == 'Attribute':
                scope_stack[-1].get_entry().get_referenced_names().append(get_name_from_node(node.func))
            else:
                scope_stack[-1].get_entry().get_referenced_names().append(get_name_from_node(node.func))
            return False
        else:
            if not hasattr(cur_node, '__getitem__'):
                return False
            self.generic_pre_statement()
            for i, child in enumerate(node[:]):
                self.walk(child)
            self.generic_post_statement()
            return True

    def generic_pre_statement(self):
        global current_file
        global scope_stack
        assert scope_stack[-1].get_scope_type() == 'statement'
        cur_node = self.cur_node

        location = Location(cur_node.lineno, cur_node.col_offset, current_file)
        last_statement = scope_stack[-1].get_last_statement()
        if not isinstance(last_statement, OperationalStatement):
            inserted_sensor = paste_sensor(self)

            statement = OperationalStatement(location)

            statement.set_sensor(inserted_sensor)

            last_statement.set_next_statement(statement)
            last_statement = statement
        scope_stack.append(Scope('operation', last_statement.get_operation()))
        return False

    def generic_post_statement(self):
        global scope_stack
        scope_stack.pop()
        return True

    def generic_pre_break_statement(self, break_type):
        global current_file
        global scope_stack
        assert scope_stack[-1].get_scope_type() == 'statement'
        cur_node = self.cur_node

        inserted_sensor = paste_sensor(self, break_type == 'return' and cur_node.value is not None)

        location = Location(cur_node.lineno, cur_node.col_offset, current_file)
        return_statement = BreakStatement(location, break_type)

        return_statement.set_sensor(inserted_sensor)

        scope_stack[-1].get_last_statement().set_next_statement(return_statement)
        scope_stack.append(Scope('operation', return_statement.get_operation()))

        if break_type == 'break' or break_type == 'continue':
            for scope in scope_stack:
                if scope.get_scope_type() == 'statement':
                    last_in_scope = scope.get_last_statement()
                    if isinstance(last_in_scope, ForStatement) or isinstance(last_in_scope, WhileStatement):
                        return_statement.set_breaking_loop(last_in_scope)
            assert return_statement.get_breaking_loop() is not None

        return False


def fix_bad_case(entry_statement, cur_array):
    if entry_statement.get_next_statement() is None:
        global sensor_counter
        missing_location = Location(cur_array[0].lineno, cur_array[0].col_offset, current_file)
        insert_action(cur_array, 0, GenerateFunctionCall())
        missing_statement = OperationalStatement(missing_location)
        missing_statement.set_sensor(sensor_counter)
        entry_statement.set_next_statement(missing_statement)


def paste_sensor(node, is_return=False):
    global insertion_level
    global sensor_counter
    global ignore_level
    
    if insertion_level == 3 and not is_return and not ignore_level:
        return None

    ignore_level = False

    if isinstance(node.parent, list):
        ind = node.parent.index(node.cur_node)
        if not is_return:
            insert_action(node.parent, ind, GenerateFunctionCall())
        else:
            insert_action(node.parent, ind, GenerateFunctionCall(), GenerateAssignment(node.cur_node))
        return sensor_counter
    else:
        for attr, value in node.parent.__dict__.iteritems():
            if isinstance(value, list):
                if node.cur_node in value:
                    if not is_return:
                        insert_action(value, value.index(node.cur_node), GenerateFunctionCall())
                    else:
                        insert_action(value, value.index(node.cur_node), GenerateFunctionCall(), GenerateAssignment(node.cur_node))
                    return sensor_counter
                else:
                    for inner in value:
                        if hasattr(inner, 'body') and isinstance(inner.body, list):
                            if node.cur_node in inner.body:
                                if not is_return:
                                    insert_action(inner.body, inner.body.index(node.cur_node), GenerateFunctionCall())
                                else:
                                    insert_action(inner.body, inner.body.index(node.cur_node), GenerateFunctionCall(), GenerateAssignment(node.cur_node))
                                return sensor_counter
    assert 1 == 2


def insert_action(list_obj, index, value, value2 = None):
    list_obj.insert(index, value)
    if value2 is not None:
        list_obj.insert(index, value2)


def parse_ast(tree, file_name, s_f_n, s_c, level, fixed_idnt):
    global current_file
    global scope_stack
    global declaration_stack
    global sensor_function_name
    global sensor_counter
    global insertion_level
    global fixed_identifier

    insertion_level = level
    if sensor_function_name is None:
        sensor_function_name = s_f_n
    if sensor_counter is None:
        sensor_counter = s_c
    fixed_identifier = fixed_idnt

    location = Location(1, 1, file_name)
    entry_statement = EntryStatement(location)
    scope_stack.append(Scope('statement', entry_statement))
    module_definition = ModuleDeclaration(location)
    module_definition.set_entry_statement(entry_statement)
    declaration_stack.append(module_definition)
    current_file = file_name

    walker = WalkThatAst()
    walker.walk(tree)

    declaration_stack.pop()
    scope_stack.pop()
    return tree

